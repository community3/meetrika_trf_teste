/*
               File: PromptContratoServicosIndicador
        Description: Selecione Contrato Servicos Indicador
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:40:51.79
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptcontratoservicosindicador : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptcontratoservicosindicador( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptcontratoservicosindicador( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutContratoServicosIndicador_Codigo ,
                           ref String aP1_InOutContratoServicosIndicador_Indicador )
      {
         this.AV7InOutContratoServicosIndicador_Codigo = aP0_InOutContratoServicosIndicador_Codigo;
         this.AV8InOutContratoServicosIndicador_Indicador = aP1_InOutContratoServicosIndicador_Indicador;
         executePrivate();
         aP0_InOutContratoServicosIndicador_Codigo=this.AV7InOutContratoServicosIndicador_Codigo;
         aP1_InOutContratoServicosIndicador_Indicador=this.AV8InOutContratoServicosIndicador_Indicador;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         cmbContratoServicosIndicador_Tipo = new GXCombobox();
         cmbContratoServicosIndicador_Periodicidade = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_80 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_80_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_80_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17ContratoServicosIndicador_Indicador1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoServicosIndicador_Indicador1", AV17ContratoServicosIndicador_Indicador1);
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
               AV21ContratoServicosIndicador_Indicador2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoServicosIndicador_Indicador2", AV21ContratoServicosIndicador_Indicador2);
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
               AV25ContratoServicosIndicador_Indicador3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoServicosIndicador_Indicador3", AV25ContratoServicosIndicador_Indicador3);
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV32TFContratoServicosIndicador_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFContratoServicosIndicador_Codigo), 6, 0)));
               AV33TFContratoServicosIndicador_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TFContratoServicosIndicador_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33TFContratoServicosIndicador_Codigo_To), 6, 0)));
               AV36TFContratoServicosIndicador_CntSrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContratoServicosIndicador_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFContratoServicosIndicador_CntSrvCod), 6, 0)));
               AV37TFContratoServicosIndicador_CntSrvCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFContratoServicosIndicador_CntSrvCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37TFContratoServicosIndicador_CntSrvCod_To), 6, 0)));
               AV40TFContratoServicosIndicador_ContratoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContratoServicosIndicador_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40TFContratoServicosIndicador_ContratoCod), 6, 0)));
               AV41TFContratoServicosIndicador_ContratoCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFContratoServicosIndicador_ContratoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41TFContratoServicosIndicador_ContratoCod_To), 6, 0)));
               AV44TFContratoServicosIndicador_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContratoServicosIndicador_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFContratoServicosIndicador_AreaTrabalhoCod), 6, 0)));
               AV45TFContratoServicosIndicador_AreaTrabalhoCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContratoServicosIndicador_AreaTrabalhoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFContratoServicosIndicador_AreaTrabalhoCod_To), 6, 0)));
               AV48TFContratoServicosIndicador_Numero = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContratoServicosIndicador_Numero", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48TFContratoServicosIndicador_Numero), 4, 0)));
               AV49TFContratoServicosIndicador_Numero_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContratoServicosIndicador_Numero_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49TFContratoServicosIndicador_Numero_To), 4, 0)));
               AV52TFContratoServicosIndicador_Indicador = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFContratoServicosIndicador_Indicador", AV52TFContratoServicosIndicador_Indicador);
               AV53TFContratoServicosIndicador_Indicador_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContratoServicosIndicador_Indicador_Sel", AV53TFContratoServicosIndicador_Indicador_Sel);
               AV56TFContratoServicosIndicador_Finalidade = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFContratoServicosIndicador_Finalidade", AV56TFContratoServicosIndicador_Finalidade);
               AV57TFContratoServicosIndicador_Finalidade_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFContratoServicosIndicador_Finalidade_Sel", AV57TFContratoServicosIndicador_Finalidade_Sel);
               AV60TFContratoServicosIndicador_Meta = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContratoServicosIndicador_Meta", AV60TFContratoServicosIndicador_Meta);
               AV61TFContratoServicosIndicador_Meta_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFContratoServicosIndicador_Meta_Sel", AV61TFContratoServicosIndicador_Meta_Sel);
               AV64TFContratoServicosIndicador_InstrumentoMedicao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFContratoServicosIndicador_InstrumentoMedicao", AV64TFContratoServicosIndicador_InstrumentoMedicao);
               AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel", AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel);
               AV76TFContratoServicosIndicador_Vigencia = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFContratoServicosIndicador_Vigencia", AV76TFContratoServicosIndicador_Vigencia);
               AV77TFContratoServicosIndicador_Vigencia_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFContratoServicosIndicador_Vigencia_Sel", AV77TFContratoServicosIndicador_Vigencia_Sel);
               AV80TFContratoServicosIndicador_QtdeFaixas = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV80TFContratoServicosIndicador_QtdeFaixas), 4, 0)));
               AV81TFContratoServicosIndicador_QtdeFaixas_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFContratoServicosIndicador_QtdeFaixas_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV81TFContratoServicosIndicador_QtdeFaixas_To), 4, 0)));
               AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace", AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace);
               AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace", AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace);
               AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace", AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace);
               AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace", AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace);
               AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace", AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace);
               AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace", AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace);
               AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace", AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace);
               AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace", AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace);
               AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace", AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace);
               AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace", AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace);
               AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace", AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace);
               AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace", AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace);
               AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace", AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV69TFContratoServicosIndicador_Tipo_Sels);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV73TFContratoServicosIndicador_Periodicidade_Sels);
               AV90Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoServicosIndicador_Indicador1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoServicosIndicador_Indicador2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoServicosIndicador_Indicador3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV32TFContratoServicosIndicador_Codigo, AV33TFContratoServicosIndicador_Codigo_To, AV36TFContratoServicosIndicador_CntSrvCod, AV37TFContratoServicosIndicador_CntSrvCod_To, AV40TFContratoServicosIndicador_ContratoCod, AV41TFContratoServicosIndicador_ContratoCod_To, AV44TFContratoServicosIndicador_AreaTrabalhoCod, AV45TFContratoServicosIndicador_AreaTrabalhoCod_To, AV48TFContratoServicosIndicador_Numero, AV49TFContratoServicosIndicador_Numero_To, AV52TFContratoServicosIndicador_Indicador, AV53TFContratoServicosIndicador_Indicador_Sel, AV56TFContratoServicosIndicador_Finalidade, AV57TFContratoServicosIndicador_Finalidade_Sel, AV60TFContratoServicosIndicador_Meta, AV61TFContratoServicosIndicador_Meta_Sel, AV64TFContratoServicosIndicador_InstrumentoMedicao, AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel, AV76TFContratoServicosIndicador_Vigencia, AV77TFContratoServicosIndicador_Vigencia_Sel, AV80TFContratoServicosIndicador_QtdeFaixas, AV81TFContratoServicosIndicador_QtdeFaixas_To, AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace, AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace, AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace, AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace, AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace, AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace, AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace, AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace, AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace, AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace, AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace, AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace, AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace, AV69TFContratoServicosIndicador_Tipo_Sels, AV73TFContratoServicosIndicador_Periodicidade_Sels, AV90Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutContratoServicosIndicador_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContratoServicosIndicador_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutContratoServicosIndicador_Indicador = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContratoServicosIndicador_Indicador", AV8InOutContratoServicosIndicador_Indicador);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAJG2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV90Pgmname = "PromptContratoServicosIndicador";
               context.Gx_err = 0;
               WSJG2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEJG2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299405238");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptcontratoservicosindicador.aspx") + "?" + UrlEncode("" +AV7InOutContratoServicosIndicador_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV8InOutContratoServicosIndicador_Indicador))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOSERVICOSINDICADOR_INDICADOR1", AV17ContratoServicosIndicador_Indicador1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOSERVICOSINDICADOR_INDICADOR2", AV21ContratoServicosIndicador_Indicador2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOSERVICOSINDICADOR_INDICADOR3", AV25ContratoServicosIndicador_Indicador3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSINDICADOR_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32TFContratoServicosIndicador_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSINDICADOR_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV33TFContratoServicosIndicador_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36TFContratoServicosIndicador_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV37TFContratoServicosIndicador_CntSrvCod_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV40TFContratoServicosIndicador_ContratoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV41TFContratoServicosIndicador_ContratoCod_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44TFContratoServicosIndicador_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV45TFContratoServicosIndicador_AreaTrabalhoCod_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSINDICADOR_NUMERO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV48TFContratoServicosIndicador_Numero), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSINDICADOR_NUMERO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV49TFContratoServicosIndicador_Numero_To), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSINDICADOR_INDICADOR", AV52TFContratoServicosIndicador_Indicador);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSINDICADOR_INDICADOR_SEL", AV53TFContratoServicosIndicador_Indicador_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSINDICADOR_FINALIDADE", AV56TFContratoServicosIndicador_Finalidade);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSINDICADOR_FINALIDADE_SEL", AV57TFContratoServicosIndicador_Finalidade_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSINDICADOR_META", AV60TFContratoServicosIndicador_Meta);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSINDICADOR_META_SEL", AV61TFContratoServicosIndicador_Meta_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO", AV64TFContratoServicosIndicador_InstrumentoMedicao);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_SEL", AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSINDICADOR_VIGENCIA", AV76TFContratoServicosIndicador_Vigencia);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSINDICADOR_VIGENCIA_SEL", AV77TFContratoServicosIndicador_Vigencia_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV80TFContratoServicosIndicador_QtdeFaixas), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV81TFContratoServicosIndicador_QtdeFaixas_To), 4, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_80", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_80), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV85GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV86GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV83DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV83DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSERVICOSINDICADOR_CODIGOTITLEFILTERDATA", AV31ContratoServicosIndicador_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSERVICOSINDICADOR_CODIGOTITLEFILTERDATA", AV31ContratoServicosIndicador_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSERVICOSINDICADOR_CNTSRVCODTITLEFILTERDATA", AV35ContratoServicosIndicador_CntSrvCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSERVICOSINDICADOR_CNTSRVCODTITLEFILTERDATA", AV35ContratoServicosIndicador_CntSrvCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSERVICOSINDICADOR_CONTRATOCODTITLEFILTERDATA", AV39ContratoServicosIndicador_ContratoCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSERVICOSINDICADOR_CONTRATOCODTITLEFILTERDATA", AV39ContratoServicosIndicador_ContratoCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSERVICOSINDICADOR_AREATRABALHOCODTITLEFILTERDATA", AV43ContratoServicosIndicador_AreaTrabalhoCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSERVICOSINDICADOR_AREATRABALHOCODTITLEFILTERDATA", AV43ContratoServicosIndicador_AreaTrabalhoCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSERVICOSINDICADOR_NUMEROTITLEFILTERDATA", AV47ContratoServicosIndicador_NumeroTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSERVICOSINDICADOR_NUMEROTITLEFILTERDATA", AV47ContratoServicosIndicador_NumeroTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSERVICOSINDICADOR_INDICADORTITLEFILTERDATA", AV51ContratoServicosIndicador_IndicadorTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSERVICOSINDICADOR_INDICADORTITLEFILTERDATA", AV51ContratoServicosIndicador_IndicadorTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSERVICOSINDICADOR_FINALIDADETITLEFILTERDATA", AV55ContratoServicosIndicador_FinalidadeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSERVICOSINDICADOR_FINALIDADETITLEFILTERDATA", AV55ContratoServicosIndicador_FinalidadeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSERVICOSINDICADOR_METATITLEFILTERDATA", AV59ContratoServicosIndicador_MetaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSERVICOSINDICADOR_METATITLEFILTERDATA", AV59ContratoServicosIndicador_MetaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAOTITLEFILTERDATA", AV63ContratoServicosIndicador_InstrumentoMedicaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAOTITLEFILTERDATA", AV63ContratoServicosIndicador_InstrumentoMedicaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSERVICOSINDICADOR_TIPOTITLEFILTERDATA", AV67ContratoServicosIndicador_TipoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSERVICOSINDICADOR_TIPOTITLEFILTERDATA", AV67ContratoServicosIndicador_TipoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSERVICOSINDICADOR_PERIODICIDADETITLEFILTERDATA", AV71ContratoServicosIndicador_PeriodicidadeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSERVICOSINDICADOR_PERIODICIDADETITLEFILTERDATA", AV71ContratoServicosIndicador_PeriodicidadeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSERVICOSINDICADOR_VIGENCIATITLEFILTERDATA", AV75ContratoServicosIndicador_VigenciaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSERVICOSINDICADOR_VIGENCIATITLEFILTERDATA", AV75ContratoServicosIndicador_VigenciaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSERVICOSINDICADOR_QTDEFAIXASTITLEFILTERDATA", AV79ContratoServicosIndicador_QtdeFaixasTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSERVICOSINDICADOR_QTDEFAIXASTITLEFILTERDATA", AV79ContratoServicosIndicador_QtdeFaixasTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFCONTRATOSERVICOSINDICADOR_TIPO_SELS", AV69TFContratoServicosIndicador_Tipo_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFCONTRATOSERVICOSINDICADOR_TIPO_SELS", AV69TFContratoServicosIndicador_Tipo_Sels);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFCONTRATOSERVICOSINDICADOR_PERIODICIDADE_SELS", AV73TFContratoServicosIndicador_Periodicidade_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFCONTRATOSERVICOSINDICADOR_PERIODICIDADE_SELS", AV73TFContratoServicosIndicador_Periodicidade_Sels);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV90Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTCONTRATOSERVICOSINDICADOR_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutContratoServicosIndicador_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTCONTRATOSERVICOSINDICADOR_INDICADOR", AV8InOutContratoServicosIndicador_Indicador);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Caption", StringUtil.RTrim( Ddo_contratoservicosindicador_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Tooltip", StringUtil.RTrim( Ddo_contratoservicosindicador_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Cls", StringUtil.RTrim( Ddo_contratoservicosindicador_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicosindicador_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoservicosindicador_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosindicador_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosindicador_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosindicador_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosindicador_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosindicador_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosindicador_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Filtertype", StringUtil.RTrim( Ddo_contratoservicosindicador_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicosindicador_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosindicador_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Sortasc", StringUtil.RTrim( Ddo_contratoservicosindicador_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosindicador_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosindicador_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoservicosindicador_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_contratoservicosindicador_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosindicador_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Caption", StringUtil.RTrim( Ddo_contratoservicosindicador_cntsrvcod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Tooltip", StringUtil.RTrim( Ddo_contratoservicosindicador_cntsrvcod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Cls", StringUtil.RTrim( Ddo_contratoservicosindicador_cntsrvcod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicosindicador_cntsrvcod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoservicosindicador_cntsrvcod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosindicador_cntsrvcod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosindicador_cntsrvcod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosindicador_cntsrvcod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosindicador_cntsrvcod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosindicador_cntsrvcod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosindicador_cntsrvcod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Filtertype", StringUtil.RTrim( Ddo_contratoservicosindicador_cntsrvcod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicosindicador_cntsrvcod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosindicador_cntsrvcod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Sortasc", StringUtil.RTrim( Ddo_contratoservicosindicador_cntsrvcod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosindicador_cntsrvcod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosindicador_cntsrvcod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoservicosindicador_cntsrvcod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Rangefilterto", StringUtil.RTrim( Ddo_contratoservicosindicador_cntsrvcod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosindicador_cntsrvcod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Caption", StringUtil.RTrim( Ddo_contratoservicosindicador_contratocod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Tooltip", StringUtil.RTrim( Ddo_contratoservicosindicador_contratocod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Cls", StringUtil.RTrim( Ddo_contratoservicosindicador_contratocod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicosindicador_contratocod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoservicosindicador_contratocod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosindicador_contratocod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosindicador_contratocod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosindicador_contratocod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosindicador_contratocod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosindicador_contratocod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosindicador_contratocod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Filtertype", StringUtil.RTrim( Ddo_contratoservicosindicador_contratocod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicosindicador_contratocod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosindicador_contratocod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Sortasc", StringUtil.RTrim( Ddo_contratoservicosindicador_contratocod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosindicador_contratocod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosindicador_contratocod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoservicosindicador_contratocod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Rangefilterto", StringUtil.RTrim( Ddo_contratoservicosindicador_contratocod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosindicador_contratocod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Caption", StringUtil.RTrim( Ddo_contratoservicosindicador_areatrabalhocod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Tooltip", StringUtil.RTrim( Ddo_contratoservicosindicador_areatrabalhocod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Cls", StringUtil.RTrim( Ddo_contratoservicosindicador_areatrabalhocod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicosindicador_areatrabalhocod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoservicosindicador_areatrabalhocod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosindicador_areatrabalhocod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosindicador_areatrabalhocod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosindicador_areatrabalhocod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosindicador_areatrabalhocod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosindicador_areatrabalhocod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosindicador_areatrabalhocod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Filtertype", StringUtil.RTrim( Ddo_contratoservicosindicador_areatrabalhocod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicosindicador_areatrabalhocod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosindicador_areatrabalhocod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Sortasc", StringUtil.RTrim( Ddo_contratoservicosindicador_areatrabalhocod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosindicador_areatrabalhocod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosindicador_areatrabalhocod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoservicosindicador_areatrabalhocod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Rangefilterto", StringUtil.RTrim( Ddo_contratoservicosindicador_areatrabalhocod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosindicador_areatrabalhocod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Caption", StringUtil.RTrim( Ddo_contratoservicosindicador_numero_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Tooltip", StringUtil.RTrim( Ddo_contratoservicosindicador_numero_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Cls", StringUtil.RTrim( Ddo_contratoservicosindicador_numero_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicosindicador_numero_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoservicosindicador_numero_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosindicador_numero_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosindicador_numero_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosindicador_numero_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosindicador_numero_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosindicador_numero_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosindicador_numero_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Filtertype", StringUtil.RTrim( Ddo_contratoservicosindicador_numero_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicosindicador_numero_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosindicador_numero_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Sortasc", StringUtil.RTrim( Ddo_contratoservicosindicador_numero_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosindicador_numero_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosindicador_numero_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoservicosindicador_numero_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Rangefilterto", StringUtil.RTrim( Ddo_contratoservicosindicador_numero_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosindicador_numero_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Caption", StringUtil.RTrim( Ddo_contratoservicosindicador_indicador_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Tooltip", StringUtil.RTrim( Ddo_contratoservicosindicador_indicador_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Cls", StringUtil.RTrim( Ddo_contratoservicosindicador_indicador_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicosindicador_indicador_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoservicosindicador_indicador_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosindicador_indicador_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosindicador_indicador_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosindicador_indicador_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosindicador_indicador_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosindicador_indicador_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosindicador_indicador_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Filtertype", StringUtil.RTrim( Ddo_contratoservicosindicador_indicador_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicosindicador_indicador_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosindicador_indicador_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Datalisttype", StringUtil.RTrim( Ddo_contratoservicosindicador_indicador_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Datalistproc", StringUtil.RTrim( Ddo_contratoservicosindicador_indicador_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratoservicosindicador_indicador_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Sortasc", StringUtil.RTrim( Ddo_contratoservicosindicador_indicador_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosindicador_indicador_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Loadingdata", StringUtil.RTrim( Ddo_contratoservicosindicador_indicador_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosindicador_indicador_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Noresultsfound", StringUtil.RTrim( Ddo_contratoservicosindicador_indicador_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosindicador_indicador_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Caption", StringUtil.RTrim( Ddo_contratoservicosindicador_finalidade_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Tooltip", StringUtil.RTrim( Ddo_contratoservicosindicador_finalidade_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Cls", StringUtil.RTrim( Ddo_contratoservicosindicador_finalidade_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicosindicador_finalidade_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoservicosindicador_finalidade_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosindicador_finalidade_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosindicador_finalidade_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosindicador_finalidade_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosindicador_finalidade_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosindicador_finalidade_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosindicador_finalidade_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Filtertype", StringUtil.RTrim( Ddo_contratoservicosindicador_finalidade_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicosindicador_finalidade_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosindicador_finalidade_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Datalisttype", StringUtil.RTrim( Ddo_contratoservicosindicador_finalidade_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Datalistproc", StringUtil.RTrim( Ddo_contratoservicosindicador_finalidade_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratoservicosindicador_finalidade_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Sortasc", StringUtil.RTrim( Ddo_contratoservicosindicador_finalidade_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosindicador_finalidade_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Loadingdata", StringUtil.RTrim( Ddo_contratoservicosindicador_finalidade_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosindicador_finalidade_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Noresultsfound", StringUtil.RTrim( Ddo_contratoservicosindicador_finalidade_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosindicador_finalidade_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_META_Caption", StringUtil.RTrim( Ddo_contratoservicosindicador_meta_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_META_Tooltip", StringUtil.RTrim( Ddo_contratoservicosindicador_meta_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_META_Cls", StringUtil.RTrim( Ddo_contratoservicosindicador_meta_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_META_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicosindicador_meta_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_META_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoservicosindicador_meta_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_META_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosindicador_meta_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_META_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosindicador_meta_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_META_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosindicador_meta_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_META_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosindicador_meta_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_META_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosindicador_meta_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_META_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosindicador_meta_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_META_Filtertype", StringUtil.RTrim( Ddo_contratoservicosindicador_meta_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_META_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicosindicador_meta_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_META_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosindicador_meta_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_META_Datalisttype", StringUtil.RTrim( Ddo_contratoservicosindicador_meta_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_META_Datalistproc", StringUtil.RTrim( Ddo_contratoservicosindicador_meta_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_META_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratoservicosindicador_meta_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_META_Sortasc", StringUtil.RTrim( Ddo_contratoservicosindicador_meta_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_META_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosindicador_meta_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_META_Loadingdata", StringUtil.RTrim( Ddo_contratoservicosindicador_meta_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_META_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosindicador_meta_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_META_Noresultsfound", StringUtil.RTrim( Ddo_contratoservicosindicador_meta_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_META_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosindicador_meta_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Caption", StringUtil.RTrim( Ddo_contratoservicosindicador_instrumentomedicao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Tooltip", StringUtil.RTrim( Ddo_contratoservicosindicador_instrumentomedicao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Cls", StringUtil.RTrim( Ddo_contratoservicosindicador_instrumentomedicao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicosindicador_instrumentomedicao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoservicosindicador_instrumentomedicao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosindicador_instrumentomedicao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosindicador_instrumentomedicao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosindicador_instrumentomedicao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosindicador_instrumentomedicao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosindicador_instrumentomedicao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosindicador_instrumentomedicao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Filtertype", StringUtil.RTrim( Ddo_contratoservicosindicador_instrumentomedicao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicosindicador_instrumentomedicao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosindicador_instrumentomedicao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Datalisttype", StringUtil.RTrim( Ddo_contratoservicosindicador_instrumentomedicao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Datalistproc", StringUtil.RTrim( Ddo_contratoservicosindicador_instrumentomedicao_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratoservicosindicador_instrumentomedicao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Sortasc", StringUtil.RTrim( Ddo_contratoservicosindicador_instrumentomedicao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosindicador_instrumentomedicao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Loadingdata", StringUtil.RTrim( Ddo_contratoservicosindicador_instrumentomedicao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosindicador_instrumentomedicao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Noresultsfound", StringUtil.RTrim( Ddo_contratoservicosindicador_instrumentomedicao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosindicador_instrumentomedicao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_TIPO_Caption", StringUtil.RTrim( Ddo_contratoservicosindicador_tipo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_TIPO_Tooltip", StringUtil.RTrim( Ddo_contratoservicosindicador_tipo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_TIPO_Cls", StringUtil.RTrim( Ddo_contratoservicosindicador_tipo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_TIPO_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoservicosindicador_tipo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_TIPO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosindicador_tipo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_TIPO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosindicador_tipo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_TIPO_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosindicador_tipo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_TIPO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosindicador_tipo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_TIPO_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosindicador_tipo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_TIPO_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosindicador_tipo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_TIPO_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosindicador_tipo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_TIPO_Datalisttype", StringUtil.RTrim( Ddo_contratoservicosindicador_tipo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_TIPO_Allowmultipleselection", StringUtil.BoolToStr( Ddo_contratoservicosindicador_tipo_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_TIPO_Datalistfixedvalues", StringUtil.RTrim( Ddo_contratoservicosindicador_tipo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_TIPO_Sortasc", StringUtil.RTrim( Ddo_contratoservicosindicador_tipo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_TIPO_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosindicador_tipo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_TIPO_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosindicador_tipo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_TIPO_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosindicador_tipo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE_Caption", StringUtil.RTrim( Ddo_contratoservicosindicador_periodicidade_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE_Tooltip", StringUtil.RTrim( Ddo_contratoservicosindicador_periodicidade_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE_Cls", StringUtil.RTrim( Ddo_contratoservicosindicador_periodicidade_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoservicosindicador_periodicidade_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosindicador_periodicidade_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosindicador_periodicidade_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosindicador_periodicidade_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosindicador_periodicidade_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosindicador_periodicidade_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosindicador_periodicidade_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosindicador_periodicidade_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE_Datalisttype", StringUtil.RTrim( Ddo_contratoservicosindicador_periodicidade_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE_Allowmultipleselection", StringUtil.BoolToStr( Ddo_contratoservicosindicador_periodicidade_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE_Datalistfixedvalues", StringUtil.RTrim( Ddo_contratoservicosindicador_periodicidade_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE_Sortasc", StringUtil.RTrim( Ddo_contratoservicosindicador_periodicidade_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosindicador_periodicidade_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosindicador_periodicidade_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosindicador_periodicidade_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Caption", StringUtil.RTrim( Ddo_contratoservicosindicador_vigencia_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Tooltip", StringUtil.RTrim( Ddo_contratoservicosindicador_vigencia_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Cls", StringUtil.RTrim( Ddo_contratoservicosindicador_vigencia_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicosindicador_vigencia_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoservicosindicador_vigencia_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosindicador_vigencia_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosindicador_vigencia_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosindicador_vigencia_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosindicador_vigencia_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosindicador_vigencia_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosindicador_vigencia_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Filtertype", StringUtil.RTrim( Ddo_contratoservicosindicador_vigencia_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicosindicador_vigencia_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosindicador_vigencia_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Datalisttype", StringUtil.RTrim( Ddo_contratoservicosindicador_vigencia_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Datalistproc", StringUtil.RTrim( Ddo_contratoservicosindicador_vigencia_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratoservicosindicador_vigencia_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Sortasc", StringUtil.RTrim( Ddo_contratoservicosindicador_vigencia_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosindicador_vigencia_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Loadingdata", StringUtil.RTrim( Ddo_contratoservicosindicador_vigencia_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosindicador_vigencia_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Noresultsfound", StringUtil.RTrim( Ddo_contratoservicosindicador_vigencia_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosindicador_vigencia_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Caption", StringUtil.RTrim( Ddo_contratoservicosindicador_qtdefaixas_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Tooltip", StringUtil.RTrim( Ddo_contratoservicosindicador_qtdefaixas_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Cls", StringUtil.RTrim( Ddo_contratoservicosindicador_qtdefaixas_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicosindicador_qtdefaixas_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoservicosindicador_qtdefaixas_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosindicador_qtdefaixas_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosindicador_qtdefaixas_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosindicador_qtdefaixas_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosindicador_qtdefaixas_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosindicador_qtdefaixas_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Filtertype", StringUtil.RTrim( Ddo_contratoservicosindicador_qtdefaixas_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicosindicador_qtdefaixas_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosindicador_qtdefaixas_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosindicador_qtdefaixas_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoservicosindicador_qtdefaixas_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Rangefilterto", StringUtil.RTrim( Ddo_contratoservicosindicador_qtdefaixas_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosindicador_qtdefaixas_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosindicador_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicosindicador_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoservicosindicador_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosindicador_cntsrvcod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicosindicador_cntsrvcod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoservicosindicador_cntsrvcod_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosindicador_contratocod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicosindicador_contratocod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoservicosindicador_contratocod_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosindicador_areatrabalhocod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicosindicador_areatrabalhocod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoservicosindicador_areatrabalhocod_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosindicador_numero_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicosindicador_numero_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoservicosindicador_numero_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosindicador_indicador_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicosindicador_indicador_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoservicosindicador_indicador_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosindicador_finalidade_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicosindicador_finalidade_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoservicosindicador_finalidade_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_META_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosindicador_meta_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_META_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicosindicador_meta_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_META_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoservicosindicador_meta_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosindicador_instrumentomedicao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicosindicador_instrumentomedicao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoservicosindicador_instrumentomedicao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_TIPO_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosindicador_tipo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_TIPO_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoservicosindicador_tipo_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosindicador_periodicidade_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoservicosindicador_periodicidade_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosindicador_vigencia_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicosindicador_vigencia_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoservicosindicador_vigencia_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosindicador_qtdefaixas_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicosindicador_qtdefaixas_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoservicosindicador_qtdefaixas_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormJG2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptContratoServicosIndicador" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Contrato Servicos Indicador" ;
      }

      protected void WBJG0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_JG2( true) ;
         }
         else
         {
            wb_table1_2_JG2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_JG2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(99, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,99);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(100, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,100);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosindicador_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32TFContratoServicosIndicador_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV32TFContratoServicosIndicador_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,101);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosindicador_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosindicador_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicosIndicador.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosindicador_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV33TFContratoServicosIndicador_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV33TFContratoServicosIndicador_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,102);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosindicador_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosindicador_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicosIndicador.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosindicador_cntsrvcod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36TFContratoServicosIndicador_CntSrvCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV36TFContratoServicosIndicador_CntSrvCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,103);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosindicador_cntsrvcod_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosindicador_cntsrvcod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicosIndicador.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosindicador_cntsrvcod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV37TFContratoServicosIndicador_CntSrvCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV37TFContratoServicosIndicador_CntSrvCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,104);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosindicador_cntsrvcod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosindicador_cntsrvcod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicosIndicador.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosindicador_contratocod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV40TFContratoServicosIndicador_ContratoCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV40TFContratoServicosIndicador_ContratoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,105);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosindicador_contratocod_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosindicador_contratocod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicosIndicador.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosindicador_contratocod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV41TFContratoServicosIndicador_ContratoCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV41TFContratoServicosIndicador_ContratoCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,106);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosindicador_contratocod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosindicador_contratocod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicosIndicador.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosindicador_areatrabalhocod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44TFContratoServicosIndicador_AreaTrabalhoCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV44TFContratoServicosIndicador_AreaTrabalhoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,107);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosindicador_areatrabalhocod_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosindicador_areatrabalhocod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicosIndicador.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosindicador_areatrabalhocod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV45TFContratoServicosIndicador_AreaTrabalhoCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV45TFContratoServicosIndicador_AreaTrabalhoCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,108);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosindicador_areatrabalhocod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosindicador_areatrabalhocod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicosIndicador.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosindicador_numero_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV48TFContratoServicosIndicador_Numero), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV48TFContratoServicosIndicador_Numero), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,109);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosindicador_numero_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosindicador_numero_Visible, 1, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicosIndicador.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosindicador_numero_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV49TFContratoServicosIndicador_Numero_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV49TFContratoServicosIndicador_Numero_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,110);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosindicador_numero_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosindicador_numero_to_Visible, 1, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicosIndicador.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfcontratoservicosindicador_indicador_Internalname, AV52TFContratoServicosIndicador_Indicador, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,111);\"", 0, edtavTfcontratoservicosindicador_indicador_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptContratoServicosIndicador.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfcontratoservicosindicador_indicador_sel_Internalname, AV53TFContratoServicosIndicador_Indicador_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,112);\"", 0, edtavTfcontratoservicosindicador_indicador_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptContratoServicosIndicador.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfcontratoservicosindicador_finalidade_Internalname, AV56TFContratoServicosIndicador_Finalidade, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,113);\"", 0, edtavTfcontratoservicosindicador_finalidade_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptContratoServicosIndicador.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfcontratoservicosindicador_finalidade_sel_Internalname, AV57TFContratoServicosIndicador_Finalidade_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,114);\"", 0, edtavTfcontratoservicosindicador_finalidade_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptContratoServicosIndicador.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfcontratoservicosindicador_meta_Internalname, AV60TFContratoServicosIndicador_Meta, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,115);\"", 0, edtavTfcontratoservicosindicador_meta_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptContratoServicosIndicador.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfcontratoservicosindicador_meta_sel_Internalname, AV61TFContratoServicosIndicador_Meta_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,116);\"", 0, edtavTfcontratoservicosindicador_meta_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptContratoServicosIndicador.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfcontratoservicosindicador_instrumentomedicao_Internalname, AV64TFContratoServicosIndicador_InstrumentoMedicao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,117);\"", 0, edtavTfcontratoservicosindicador_instrumentomedicao_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptContratoServicosIndicador.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfcontratoservicosindicador_instrumentomedicao_sel_Internalname, AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,118);\"", 0, edtavTfcontratoservicosindicador_instrumentomedicao_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptContratoServicosIndicador.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosindicador_vigencia_Internalname, AV76TFContratoServicosIndicador_Vigencia, StringUtil.RTrim( context.localUtil.Format( AV76TFContratoServicosIndicador_Vigencia, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,119);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosindicador_vigencia_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosindicador_vigencia_Visible, 1, 0, "text", "", 100, "%", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoServicosIndicador.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 120,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosindicador_vigencia_sel_Internalname, AV77TFContratoServicosIndicador_Vigencia_Sel, StringUtil.RTrim( context.localUtil.Format( AV77TFContratoServicosIndicador_Vigencia_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,120);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosindicador_vigencia_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosindicador_vigencia_sel_Visible, 1, 0, "text", "", 100, "%", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoServicosIndicador.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosindicador_qtdefaixas_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV80TFContratoServicosIndicador_QtdeFaixas), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV80TFContratoServicosIndicador_QtdeFaixas), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,121);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosindicador_qtdefaixas_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosindicador_qtdefaixas_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicosIndicador.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 122,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosindicador_qtdefaixas_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV81TFContratoServicosIndicador_QtdeFaixas_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV81TFContratoServicosIndicador_QtdeFaixas_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,122);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosindicador_qtdefaixas_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosindicador_qtdefaixas_to_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicosIndicador.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSERVICOSINDICADOR_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 124,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosindicador_codigotitlecontrolidtoreplace_Internalname, AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,124);\"", 0, edtavDdo_contratoservicosindicador_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoServicosIndicador.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 126,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosindicador_cntsrvcodtitlecontrolidtoreplace_Internalname, AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,126);\"", 0, edtavDdo_contratoservicosindicador_cntsrvcodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoServicosIndicador.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 128,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosindicador_contratocodtitlecontrolidtoreplace_Internalname, AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,128);\"", 0, edtavDdo_contratoservicosindicador_contratocodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoServicosIndicador.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 130,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosindicador_areatrabalhocodtitlecontrolidtoreplace_Internalname, AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,130);\"", 0, edtavDdo_contratoservicosindicador_areatrabalhocodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoServicosIndicador.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSERVICOSINDICADOR_NUMEROContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 132,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosindicador_numerotitlecontrolidtoreplace_Internalname, AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,132);\"", 0, edtavDdo_contratoservicosindicador_numerotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoServicosIndicador.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSERVICOSINDICADOR_INDICADORContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 134,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosindicador_indicadortitlecontrolidtoreplace_Internalname, AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,134);\"", 0, edtavDdo_contratoservicosindicador_indicadortitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoServicosIndicador.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSERVICOSINDICADOR_FINALIDADEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 136,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosindicador_finalidadetitlecontrolidtoreplace_Internalname, AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,136);\"", 0, edtavDdo_contratoservicosindicador_finalidadetitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoServicosIndicador.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSERVICOSINDICADOR_METAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 138,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosindicador_metatitlecontrolidtoreplace_Internalname, AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,138);\"", 0, edtavDdo_contratoservicosindicador_metatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoServicosIndicador.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 140,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosindicador_instrumentomedicaotitlecontrolidtoreplace_Internalname, AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,140);\"", 0, edtavDdo_contratoservicosindicador_instrumentomedicaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoServicosIndicador.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSERVICOSINDICADOR_TIPOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 142,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosindicador_tipotitlecontrolidtoreplace_Internalname, AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,142);\"", 0, edtavDdo_contratoservicosindicador_tipotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoServicosIndicador.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 144,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosindicador_periodicidadetitlecontrolidtoreplace_Internalname, AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,144);\"", 0, edtavDdo_contratoservicosindicador_periodicidadetitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoServicosIndicador.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSERVICOSINDICADOR_VIGENCIAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 146,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosindicador_vigenciatitlecontrolidtoreplace_Internalname, AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,146);\"", 0, edtavDdo_contratoservicosindicador_vigenciatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoServicosIndicador.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXASContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 148,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosindicador_qtdefaixastitlecontrolidtoreplace_Internalname, AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,148);\"", 0, edtavDdo_contratoservicosindicador_qtdefaixastitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoServicosIndicador.htm");
         }
         wbLoad = true;
      }

      protected void STARTJG2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Contrato Servicos Indicador", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPJG0( ) ;
      }

      protected void WSJG2( )
      {
         STARTJG2( ) ;
         EVTJG2( ) ;
      }

      protected void EVTJG2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11JG2 */
                           E11JG2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSINDICADOR_CODIGO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12JG2 */
                           E12JG2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13JG2 */
                           E13JG2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14JG2 */
                           E14JG2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15JG2 */
                           E15JG2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSINDICADOR_NUMERO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16JG2 */
                           E16JG2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17JG2 */
                           E17JG2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18JG2 */
                           E18JG2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSINDICADOR_META.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19JG2 */
                           E19JG2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20JG2 */
                           E20JG2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSINDICADOR_TIPO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E21JG2 */
                           E21JG2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E22JG2 */
                           E22JG2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E23JG2 */
                           E23JG2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E24JG2 */
                           E24JG2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E25JG2 */
                           E25JG2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E26JG2 */
                           E26JG2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E27JG2 */
                           E27JG2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E28JG2 */
                           E28JG2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E29JG2 */
                           E29JG2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E30JG2 */
                           E30JG2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E31JG2 */
                           E31JG2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E32JG2 */
                           E32JG2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E33JG2 */
                           E33JG2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E34JG2 */
                           E34JG2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_80_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
                           SubsflControlProps_802( ) ;
                           AV28Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV89Select_GXI : context.convertURL( context.PathToRelativeUrl( AV28Select))));
                           A1269ContratoServicosIndicador_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosIndicador_Codigo_Internalname), ",", "."));
                           A1270ContratoServicosIndicador_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosIndicador_CntSrvCod_Internalname), ",", "."));
                           A1296ContratoServicosIndicador_ContratoCod = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosIndicador_ContratoCod_Internalname), ",", "."));
                           n1296ContratoServicosIndicador_ContratoCod = false;
                           A1295ContratoServicosIndicador_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosIndicador_AreaTrabalhoCod_Internalname), ",", "."));
                           n1295ContratoServicosIndicador_AreaTrabalhoCod = false;
                           A1271ContratoServicosIndicador_Numero = (short)(context.localUtil.CToN( cgiGet( edtContratoServicosIndicador_Numero_Internalname), ",", "."));
                           A1274ContratoServicosIndicador_Indicador = cgiGet( edtContratoServicosIndicador_Indicador_Internalname);
                           A1305ContratoServicosIndicador_Finalidade = cgiGet( edtContratoServicosIndicador_Finalidade_Internalname);
                           n1305ContratoServicosIndicador_Finalidade = false;
                           A1306ContratoServicosIndicador_Meta = cgiGet( edtContratoServicosIndicador_Meta_Internalname);
                           n1306ContratoServicosIndicador_Meta = false;
                           A1307ContratoServicosIndicador_InstrumentoMedicao = cgiGet( edtContratoServicosIndicador_InstrumentoMedicao_Internalname);
                           n1307ContratoServicosIndicador_InstrumentoMedicao = false;
                           cmbContratoServicosIndicador_Tipo.Name = cmbContratoServicosIndicador_Tipo_Internalname;
                           cmbContratoServicosIndicador_Tipo.CurrentValue = cgiGet( cmbContratoServicosIndicador_Tipo_Internalname);
                           A1308ContratoServicosIndicador_Tipo = cgiGet( cmbContratoServicosIndicador_Tipo_Internalname);
                           n1308ContratoServicosIndicador_Tipo = false;
                           cmbContratoServicosIndicador_Periodicidade.Name = cmbContratoServicosIndicador_Periodicidade_Internalname;
                           cmbContratoServicosIndicador_Periodicidade.CurrentValue = cgiGet( cmbContratoServicosIndicador_Periodicidade_Internalname);
                           A1309ContratoServicosIndicador_Periodicidade = cgiGet( cmbContratoServicosIndicador_Periodicidade_Internalname);
                           n1309ContratoServicosIndicador_Periodicidade = false;
                           A1310ContratoServicosIndicador_Vigencia = StringUtil.Upper( cgiGet( edtContratoServicosIndicador_Vigencia_Internalname));
                           n1310ContratoServicosIndicador_Vigencia = false;
                           A1298ContratoServicosIndicador_QtdeFaixas = (short)(context.localUtil.CToN( cgiGet( edtContratoServicosIndicador_QtdeFaixas_Internalname), ",", "."));
                           n1298ContratoServicosIndicador_QtdeFaixas = false;
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E35JG2 */
                                 E35JG2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E36JG2 */
                                 E36JG2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E37JG2 */
                                 E37JG2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratoservicosindicador_indicador1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOSERVICOSINDICADOR_INDICADOR1"), AV17ContratoServicosIndicador_Indicador1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratoservicosindicador_indicador2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOSERVICOSINDICADOR_INDICADOR2"), AV21ContratoServicosIndicador_Indicador2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratoservicosindicador_indicador3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOSERVICOSINDICADOR_INDICADOR3"), AV25ContratoServicosIndicador_Indicador3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicosindicador_codigo Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_CODIGO"), ",", ".") != Convert.ToDecimal( AV32TFContratoServicosIndicador_Codigo )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicosindicador_codigo_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV33TFContratoServicosIndicador_Codigo_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicosindicador_cntsrvcod Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD"), ",", ".") != Convert.ToDecimal( AV36TFContratoServicosIndicador_CntSrvCod )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicosindicador_cntsrvcod_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD_TO"), ",", ".") != Convert.ToDecimal( AV37TFContratoServicosIndicador_CntSrvCod_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicosindicador_contratocod Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD"), ",", ".") != Convert.ToDecimal( AV40TFContratoServicosIndicador_ContratoCod )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicosindicador_contratocod_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD_TO"), ",", ".") != Convert.ToDecimal( AV41TFContratoServicosIndicador_ContratoCod_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicosindicador_areatrabalhocod Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD"), ",", ".") != Convert.ToDecimal( AV44TFContratoServicosIndicador_AreaTrabalhoCod )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicosindicador_areatrabalhocod_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_TO"), ",", ".") != Convert.ToDecimal( AV45TFContratoServicosIndicador_AreaTrabalhoCod_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicosindicador_numero Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_NUMERO"), ",", ".") != Convert.ToDecimal( AV48TFContratoServicosIndicador_Numero )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicosindicador_numero_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_NUMERO_TO"), ",", ".") != Convert.ToDecimal( AV49TFContratoServicosIndicador_Numero_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicosindicador_indicador Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_INDICADOR"), AV52TFContratoServicosIndicador_Indicador) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicosindicador_indicador_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_INDICADOR_SEL"), AV53TFContratoServicosIndicador_Indicador_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicosindicador_finalidade Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_FINALIDADE"), AV56TFContratoServicosIndicador_Finalidade) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicosindicador_finalidade_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_FINALIDADE_SEL"), AV57TFContratoServicosIndicador_Finalidade_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicosindicador_meta Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_META"), AV60TFContratoServicosIndicador_Meta) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicosindicador_meta_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_META_SEL"), AV61TFContratoServicosIndicador_Meta_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicosindicador_instrumentomedicao Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO"), AV64TFContratoServicosIndicador_InstrumentoMedicao) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicosindicador_instrumentomedicao_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_SEL"), AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicosindicador_vigencia Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_VIGENCIA"), AV76TFContratoServicosIndicador_Vigencia) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicosindicador_vigencia_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_VIGENCIA_SEL"), AV77TFContratoServicosIndicador_Vigencia_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicosindicador_qtdefaixas Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS"), ",", ".") != Convert.ToDecimal( AV80TFContratoServicosIndicador_QtdeFaixas )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicosindicador_qtdefaixas_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS_TO"), ",", ".") != Convert.ToDecimal( AV81TFContratoServicosIndicador_QtdeFaixas_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E38JG2 */
                                       E38JG2 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEJG2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormJG2( ) ;
            }
         }
      }

      protected void PAJG2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTRATOSERVICOSINDICADOR_INDICADOR", "Indicador", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTRATOSERVICOSINDICADOR_INDICADOR", "Indicador", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CONTRATOSERVICOSINDICADOR_INDICADOR", "Indicador", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            }
            GXCCtl = "CONTRATOSERVICOSINDICADOR_TIPO_" + sGXsfl_80_idx;
            cmbContratoServicosIndicador_Tipo.Name = GXCCtl;
            cmbContratoServicosIndicador_Tipo.WebTags = "";
            cmbContratoServicosIndicador_Tipo.addItem("", "(Nenhum)", 0);
            cmbContratoServicosIndicador_Tipo.addItem("P", "Pontualidade (demanda)", 0);
            cmbContratoServicosIndicador_Tipo.addItem("PL", "Frequ�ncia de atrasos (lote)", 0);
            cmbContratoServicosIndicador_Tipo.addItem("D", "Diverg�ncias (demanda)", 0);
            cmbContratoServicosIndicador_Tipo.addItem("FD", "Frequ�ncia de diverg�ncias (lote)", 0);
            cmbContratoServicosIndicador_Tipo.addItem("FP", "Frequ�ncia de pend�ncias (�ndice de rejei��o de demandas) (lote)", 0);
            cmbContratoServicosIndicador_Tipo.addItem("AC", "Ajustes de contagens (qtd) (lote)", 0);
            cmbContratoServicosIndicador_Tipo.addItem("AV", "Ajustes de contagens (soma do valor bruto) (lote)", 0);
            cmbContratoServicosIndicador_Tipo.addItem("T", "Tempestividade", 0);
            if ( cmbContratoServicosIndicador_Tipo.ItemCount > 0 )
            {
               A1308ContratoServicosIndicador_Tipo = cmbContratoServicosIndicador_Tipo.getValidValue(A1308ContratoServicosIndicador_Tipo);
               n1308ContratoServicosIndicador_Tipo = false;
            }
            GXCCtl = "CONTRATOSERVICOSINDICADOR_PERIODICIDADE_" + sGXsfl_80_idx;
            cmbContratoServicosIndicador_Periodicidade.Name = GXCCtl;
            cmbContratoServicosIndicador_Periodicidade.WebTags = "";
            cmbContratoServicosIndicador_Periodicidade.addItem("M", "Mensal", 0);
            if ( cmbContratoServicosIndicador_Periodicidade.ItemCount > 0 )
            {
               A1309ContratoServicosIndicador_Periodicidade = cmbContratoServicosIndicador_Periodicidade.getValidValue(A1309ContratoServicosIndicador_Periodicidade);
               n1309ContratoServicosIndicador_Periodicidade = false;
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_802( ) ;
         while ( nGXsfl_80_idx <= nRC_GXsfl_80 )
         {
            sendrow_802( ) ;
            nGXsfl_80_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_80_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_80_idx+1));
            sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
            SubsflControlProps_802( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       String AV17ContratoServicosIndicador_Indicador1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       short AV20DynamicFiltersOperator2 ,
                                       String AV21ContratoServicosIndicador_Indicador2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       short AV24DynamicFiltersOperator3 ,
                                       String AV25ContratoServicosIndicador_Indicador3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       int AV32TFContratoServicosIndicador_Codigo ,
                                       int AV33TFContratoServicosIndicador_Codigo_To ,
                                       int AV36TFContratoServicosIndicador_CntSrvCod ,
                                       int AV37TFContratoServicosIndicador_CntSrvCod_To ,
                                       int AV40TFContratoServicosIndicador_ContratoCod ,
                                       int AV41TFContratoServicosIndicador_ContratoCod_To ,
                                       int AV44TFContratoServicosIndicador_AreaTrabalhoCod ,
                                       int AV45TFContratoServicosIndicador_AreaTrabalhoCod_To ,
                                       short AV48TFContratoServicosIndicador_Numero ,
                                       short AV49TFContratoServicosIndicador_Numero_To ,
                                       String AV52TFContratoServicosIndicador_Indicador ,
                                       String AV53TFContratoServicosIndicador_Indicador_Sel ,
                                       String AV56TFContratoServicosIndicador_Finalidade ,
                                       String AV57TFContratoServicosIndicador_Finalidade_Sel ,
                                       String AV60TFContratoServicosIndicador_Meta ,
                                       String AV61TFContratoServicosIndicador_Meta_Sel ,
                                       String AV64TFContratoServicosIndicador_InstrumentoMedicao ,
                                       String AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel ,
                                       String AV76TFContratoServicosIndicador_Vigencia ,
                                       String AV77TFContratoServicosIndicador_Vigencia_Sel ,
                                       short AV80TFContratoServicosIndicador_QtdeFaixas ,
                                       short AV81TFContratoServicosIndicador_QtdeFaixas_To ,
                                       String AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace ,
                                       String AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace ,
                                       String AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace ,
                                       String AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace ,
                                       String AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace ,
                                       String AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace ,
                                       String AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace ,
                                       String AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace ,
                                       String AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace ,
                                       String AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace ,
                                       String AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace ,
                                       String AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace ,
                                       String AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace ,
                                       IGxCollection AV69TFContratoServicosIndicador_Tipo_Sels ,
                                       IGxCollection AV73TFContratoServicosIndicador_Periodicidade_Sels ,
                                       String AV90Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFJG2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSINDICADOR_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1269ContratoServicosIndicador_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSINDICADOR_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSINDICADOR_CNTSRVCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSINDICADOR_CNTSRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSINDICADOR_NUMERO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1271ContratoServicosIndicador_Numero), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSINDICADOR_NUMERO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1271ContratoServicosIndicador_Numero), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSINDICADOR_INDICADOR", GetSecureSignedToken( "", A1274ContratoServicosIndicador_Indicador));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSINDICADOR_INDICADOR", A1274ContratoServicosIndicador_Indicador);
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSINDICADOR_FINALIDADE", GetSecureSignedToken( "", A1305ContratoServicosIndicador_Finalidade));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSINDICADOR_FINALIDADE", A1305ContratoServicosIndicador_Finalidade);
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSINDICADOR_META", GetSecureSignedToken( "", A1306ContratoServicosIndicador_Meta));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSINDICADOR_META", A1306ContratoServicosIndicador_Meta);
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO", GetSecureSignedToken( "", A1307ContratoServicosIndicador_InstrumentoMedicao));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO", A1307ContratoServicosIndicador_InstrumentoMedicao);
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSINDICADOR_TIPO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1308ContratoServicosIndicador_Tipo, ""))));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSINDICADOR_TIPO", StringUtil.RTrim( A1308ContratoServicosIndicador_Tipo));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSINDICADOR_PERIODICIDADE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1309ContratoServicosIndicador_Periodicidade, ""))));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSINDICADOR_PERIODICIDADE", StringUtil.RTrim( A1309ContratoServicosIndicador_Periodicidade));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSINDICADOR_VIGENCIA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1310ContratoServicosIndicador_Vigencia, "@!"))));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSINDICADOR_VIGENCIA", A1310ContratoServicosIndicador_Vigencia);
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFJG2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV90Pgmname = "PromptContratoServicosIndicador";
         context.Gx_err = 0;
      }

      protected void RFJG2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 80;
         /* Execute user event: E36JG2 */
         E36JG2 ();
         nGXsfl_80_idx = 1;
         sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
         SubsflControlProps_802( ) ;
         nGXsfl_80_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_802( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A1308ContratoServicosIndicador_Tipo ,
                                                 AV69TFContratoServicosIndicador_Tipo_Sels ,
                                                 A1309ContratoServicosIndicador_Periodicidade ,
                                                 AV73TFContratoServicosIndicador_Periodicidade_Sels ,
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV16DynamicFiltersOperator1 ,
                                                 AV17ContratoServicosIndicador_Indicador1 ,
                                                 AV18DynamicFiltersEnabled2 ,
                                                 AV19DynamicFiltersSelector2 ,
                                                 AV20DynamicFiltersOperator2 ,
                                                 AV21ContratoServicosIndicador_Indicador2 ,
                                                 AV22DynamicFiltersEnabled3 ,
                                                 AV23DynamicFiltersSelector3 ,
                                                 AV24DynamicFiltersOperator3 ,
                                                 AV25ContratoServicosIndicador_Indicador3 ,
                                                 AV32TFContratoServicosIndicador_Codigo ,
                                                 AV33TFContratoServicosIndicador_Codigo_To ,
                                                 AV36TFContratoServicosIndicador_CntSrvCod ,
                                                 AV37TFContratoServicosIndicador_CntSrvCod_To ,
                                                 AV40TFContratoServicosIndicador_ContratoCod ,
                                                 AV41TFContratoServicosIndicador_ContratoCod_To ,
                                                 AV44TFContratoServicosIndicador_AreaTrabalhoCod ,
                                                 AV45TFContratoServicosIndicador_AreaTrabalhoCod_To ,
                                                 AV48TFContratoServicosIndicador_Numero ,
                                                 AV49TFContratoServicosIndicador_Numero_To ,
                                                 AV53TFContratoServicosIndicador_Indicador_Sel ,
                                                 AV52TFContratoServicosIndicador_Indicador ,
                                                 AV57TFContratoServicosIndicador_Finalidade_Sel ,
                                                 AV56TFContratoServicosIndicador_Finalidade ,
                                                 AV61TFContratoServicosIndicador_Meta_Sel ,
                                                 AV60TFContratoServicosIndicador_Meta ,
                                                 AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel ,
                                                 AV64TFContratoServicosIndicador_InstrumentoMedicao ,
                                                 AV69TFContratoServicosIndicador_Tipo_Sels.Count ,
                                                 AV73TFContratoServicosIndicador_Periodicidade_Sels.Count ,
                                                 AV77TFContratoServicosIndicador_Vigencia_Sel ,
                                                 AV76TFContratoServicosIndicador_Vigencia ,
                                                 A1274ContratoServicosIndicador_Indicador ,
                                                 A1269ContratoServicosIndicador_Codigo ,
                                                 A1270ContratoServicosIndicador_CntSrvCod ,
                                                 A1296ContratoServicosIndicador_ContratoCod ,
                                                 A1295ContratoServicosIndicador_AreaTrabalhoCod ,
                                                 A1271ContratoServicosIndicador_Numero ,
                                                 A1305ContratoServicosIndicador_Finalidade ,
                                                 A1306ContratoServicosIndicador_Meta ,
                                                 A1307ContratoServicosIndicador_InstrumentoMedicao ,
                                                 A1310ContratoServicosIndicador_Vigencia ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 AV80TFContratoServicosIndicador_QtdeFaixas ,
                                                 A1298ContratoServicosIndicador_QtdeFaixas ,
                                                 AV81TFContratoServicosIndicador_QtdeFaixas_To },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                                 TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                                 TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT
                                                 }
            });
            lV17ContratoServicosIndicador_Indicador1 = StringUtil.Concat( StringUtil.RTrim( AV17ContratoServicosIndicador_Indicador1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoServicosIndicador_Indicador1", AV17ContratoServicosIndicador_Indicador1);
            lV17ContratoServicosIndicador_Indicador1 = StringUtil.Concat( StringUtil.RTrim( AV17ContratoServicosIndicador_Indicador1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoServicosIndicador_Indicador1", AV17ContratoServicosIndicador_Indicador1);
            lV21ContratoServicosIndicador_Indicador2 = StringUtil.Concat( StringUtil.RTrim( AV21ContratoServicosIndicador_Indicador2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoServicosIndicador_Indicador2", AV21ContratoServicosIndicador_Indicador2);
            lV21ContratoServicosIndicador_Indicador2 = StringUtil.Concat( StringUtil.RTrim( AV21ContratoServicosIndicador_Indicador2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoServicosIndicador_Indicador2", AV21ContratoServicosIndicador_Indicador2);
            lV25ContratoServicosIndicador_Indicador3 = StringUtil.Concat( StringUtil.RTrim( AV25ContratoServicosIndicador_Indicador3), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoServicosIndicador_Indicador3", AV25ContratoServicosIndicador_Indicador3);
            lV25ContratoServicosIndicador_Indicador3 = StringUtil.Concat( StringUtil.RTrim( AV25ContratoServicosIndicador_Indicador3), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoServicosIndicador_Indicador3", AV25ContratoServicosIndicador_Indicador3);
            lV52TFContratoServicosIndicador_Indicador = StringUtil.Concat( StringUtil.RTrim( AV52TFContratoServicosIndicador_Indicador), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFContratoServicosIndicador_Indicador", AV52TFContratoServicosIndicador_Indicador);
            lV56TFContratoServicosIndicador_Finalidade = StringUtil.Concat( StringUtil.RTrim( AV56TFContratoServicosIndicador_Finalidade), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFContratoServicosIndicador_Finalidade", AV56TFContratoServicosIndicador_Finalidade);
            lV60TFContratoServicosIndicador_Meta = StringUtil.Concat( StringUtil.RTrim( AV60TFContratoServicosIndicador_Meta), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContratoServicosIndicador_Meta", AV60TFContratoServicosIndicador_Meta);
            lV64TFContratoServicosIndicador_InstrumentoMedicao = StringUtil.Concat( StringUtil.RTrim( AV64TFContratoServicosIndicador_InstrumentoMedicao), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFContratoServicosIndicador_InstrumentoMedicao", AV64TFContratoServicosIndicador_InstrumentoMedicao);
            lV76TFContratoServicosIndicador_Vigencia = StringUtil.Concat( StringUtil.RTrim( AV76TFContratoServicosIndicador_Vigencia), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFContratoServicosIndicador_Vigencia", AV76TFContratoServicosIndicador_Vigencia);
            /* Using cursor H00JG3 */
            pr_default.execute(0, new Object[] {AV80TFContratoServicosIndicador_QtdeFaixas, AV80TFContratoServicosIndicador_QtdeFaixas, AV81TFContratoServicosIndicador_QtdeFaixas_To, AV81TFContratoServicosIndicador_QtdeFaixas_To, lV17ContratoServicosIndicador_Indicador1, lV17ContratoServicosIndicador_Indicador1, lV21ContratoServicosIndicador_Indicador2, lV21ContratoServicosIndicador_Indicador2, lV25ContratoServicosIndicador_Indicador3, lV25ContratoServicosIndicador_Indicador3, AV32TFContratoServicosIndicador_Codigo, AV33TFContratoServicosIndicador_Codigo_To, AV36TFContratoServicosIndicador_CntSrvCod, AV37TFContratoServicosIndicador_CntSrvCod_To, AV40TFContratoServicosIndicador_ContratoCod, AV41TFContratoServicosIndicador_ContratoCod_To, AV44TFContratoServicosIndicador_AreaTrabalhoCod, AV45TFContratoServicosIndicador_AreaTrabalhoCod_To, AV48TFContratoServicosIndicador_Numero, AV49TFContratoServicosIndicador_Numero_To, lV52TFContratoServicosIndicador_Indicador, AV53TFContratoServicosIndicador_Indicador_Sel, lV56TFContratoServicosIndicador_Finalidade, AV57TFContratoServicosIndicador_Finalidade_Sel, lV60TFContratoServicosIndicador_Meta, AV61TFContratoServicosIndicador_Meta_Sel, lV64TFContratoServicosIndicador_InstrumentoMedicao, AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel, lV76TFContratoServicosIndicador_Vigencia, AV77TFContratoServicosIndicador_Vigencia_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_80_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1310ContratoServicosIndicador_Vigencia = H00JG3_A1310ContratoServicosIndicador_Vigencia[0];
               n1310ContratoServicosIndicador_Vigencia = H00JG3_n1310ContratoServicosIndicador_Vigencia[0];
               A1309ContratoServicosIndicador_Periodicidade = H00JG3_A1309ContratoServicosIndicador_Periodicidade[0];
               n1309ContratoServicosIndicador_Periodicidade = H00JG3_n1309ContratoServicosIndicador_Periodicidade[0];
               A1308ContratoServicosIndicador_Tipo = H00JG3_A1308ContratoServicosIndicador_Tipo[0];
               n1308ContratoServicosIndicador_Tipo = H00JG3_n1308ContratoServicosIndicador_Tipo[0];
               A1307ContratoServicosIndicador_InstrumentoMedicao = H00JG3_A1307ContratoServicosIndicador_InstrumentoMedicao[0];
               n1307ContratoServicosIndicador_InstrumentoMedicao = H00JG3_n1307ContratoServicosIndicador_InstrumentoMedicao[0];
               A1306ContratoServicosIndicador_Meta = H00JG3_A1306ContratoServicosIndicador_Meta[0];
               n1306ContratoServicosIndicador_Meta = H00JG3_n1306ContratoServicosIndicador_Meta[0];
               A1305ContratoServicosIndicador_Finalidade = H00JG3_A1305ContratoServicosIndicador_Finalidade[0];
               n1305ContratoServicosIndicador_Finalidade = H00JG3_n1305ContratoServicosIndicador_Finalidade[0];
               A1274ContratoServicosIndicador_Indicador = H00JG3_A1274ContratoServicosIndicador_Indicador[0];
               A1271ContratoServicosIndicador_Numero = H00JG3_A1271ContratoServicosIndicador_Numero[0];
               A1295ContratoServicosIndicador_AreaTrabalhoCod = H00JG3_A1295ContratoServicosIndicador_AreaTrabalhoCod[0];
               n1295ContratoServicosIndicador_AreaTrabalhoCod = H00JG3_n1295ContratoServicosIndicador_AreaTrabalhoCod[0];
               A1296ContratoServicosIndicador_ContratoCod = H00JG3_A1296ContratoServicosIndicador_ContratoCod[0];
               n1296ContratoServicosIndicador_ContratoCod = H00JG3_n1296ContratoServicosIndicador_ContratoCod[0];
               A1270ContratoServicosIndicador_CntSrvCod = H00JG3_A1270ContratoServicosIndicador_CntSrvCod[0];
               A1269ContratoServicosIndicador_Codigo = H00JG3_A1269ContratoServicosIndicador_Codigo[0];
               A1298ContratoServicosIndicador_QtdeFaixas = H00JG3_A1298ContratoServicosIndicador_QtdeFaixas[0];
               n1298ContratoServicosIndicador_QtdeFaixas = H00JG3_n1298ContratoServicosIndicador_QtdeFaixas[0];
               A1296ContratoServicosIndicador_ContratoCod = H00JG3_A1296ContratoServicosIndicador_ContratoCod[0];
               n1296ContratoServicosIndicador_ContratoCod = H00JG3_n1296ContratoServicosIndicador_ContratoCod[0];
               A1295ContratoServicosIndicador_AreaTrabalhoCod = H00JG3_A1295ContratoServicosIndicador_AreaTrabalhoCod[0];
               n1295ContratoServicosIndicador_AreaTrabalhoCod = H00JG3_n1295ContratoServicosIndicador_AreaTrabalhoCod[0];
               A1298ContratoServicosIndicador_QtdeFaixas = H00JG3_A1298ContratoServicosIndicador_QtdeFaixas[0];
               n1298ContratoServicosIndicador_QtdeFaixas = H00JG3_n1298ContratoServicosIndicador_QtdeFaixas[0];
               /* Execute user event: E37JG2 */
               E37JG2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 80;
            WBJG0( ) ;
         }
         nGXsfl_80_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A1308ContratoServicosIndicador_Tipo ,
                                              AV69TFContratoServicosIndicador_Tipo_Sels ,
                                              A1309ContratoServicosIndicador_Periodicidade ,
                                              AV73TFContratoServicosIndicador_Periodicidade_Sels ,
                                              AV15DynamicFiltersSelector1 ,
                                              AV16DynamicFiltersOperator1 ,
                                              AV17ContratoServicosIndicador_Indicador1 ,
                                              AV18DynamicFiltersEnabled2 ,
                                              AV19DynamicFiltersSelector2 ,
                                              AV20DynamicFiltersOperator2 ,
                                              AV21ContratoServicosIndicador_Indicador2 ,
                                              AV22DynamicFiltersEnabled3 ,
                                              AV23DynamicFiltersSelector3 ,
                                              AV24DynamicFiltersOperator3 ,
                                              AV25ContratoServicosIndicador_Indicador3 ,
                                              AV32TFContratoServicosIndicador_Codigo ,
                                              AV33TFContratoServicosIndicador_Codigo_To ,
                                              AV36TFContratoServicosIndicador_CntSrvCod ,
                                              AV37TFContratoServicosIndicador_CntSrvCod_To ,
                                              AV40TFContratoServicosIndicador_ContratoCod ,
                                              AV41TFContratoServicosIndicador_ContratoCod_To ,
                                              AV44TFContratoServicosIndicador_AreaTrabalhoCod ,
                                              AV45TFContratoServicosIndicador_AreaTrabalhoCod_To ,
                                              AV48TFContratoServicosIndicador_Numero ,
                                              AV49TFContratoServicosIndicador_Numero_To ,
                                              AV53TFContratoServicosIndicador_Indicador_Sel ,
                                              AV52TFContratoServicosIndicador_Indicador ,
                                              AV57TFContratoServicosIndicador_Finalidade_Sel ,
                                              AV56TFContratoServicosIndicador_Finalidade ,
                                              AV61TFContratoServicosIndicador_Meta_Sel ,
                                              AV60TFContratoServicosIndicador_Meta ,
                                              AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel ,
                                              AV64TFContratoServicosIndicador_InstrumentoMedicao ,
                                              AV69TFContratoServicosIndicador_Tipo_Sels.Count ,
                                              AV73TFContratoServicosIndicador_Periodicidade_Sels.Count ,
                                              AV77TFContratoServicosIndicador_Vigencia_Sel ,
                                              AV76TFContratoServicosIndicador_Vigencia ,
                                              A1274ContratoServicosIndicador_Indicador ,
                                              A1269ContratoServicosIndicador_Codigo ,
                                              A1270ContratoServicosIndicador_CntSrvCod ,
                                              A1296ContratoServicosIndicador_ContratoCod ,
                                              A1295ContratoServicosIndicador_AreaTrabalhoCod ,
                                              A1271ContratoServicosIndicador_Numero ,
                                              A1305ContratoServicosIndicador_Finalidade ,
                                              A1306ContratoServicosIndicador_Meta ,
                                              A1307ContratoServicosIndicador_InstrumentoMedicao ,
                                              A1310ContratoServicosIndicador_Vigencia ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              AV80TFContratoServicosIndicador_QtdeFaixas ,
                                              A1298ContratoServicosIndicador_QtdeFaixas ,
                                              AV81TFContratoServicosIndicador_QtdeFaixas_To },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT
                                              }
         });
         lV17ContratoServicosIndicador_Indicador1 = StringUtil.Concat( StringUtil.RTrim( AV17ContratoServicosIndicador_Indicador1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoServicosIndicador_Indicador1", AV17ContratoServicosIndicador_Indicador1);
         lV17ContratoServicosIndicador_Indicador1 = StringUtil.Concat( StringUtil.RTrim( AV17ContratoServicosIndicador_Indicador1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoServicosIndicador_Indicador1", AV17ContratoServicosIndicador_Indicador1);
         lV21ContratoServicosIndicador_Indicador2 = StringUtil.Concat( StringUtil.RTrim( AV21ContratoServicosIndicador_Indicador2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoServicosIndicador_Indicador2", AV21ContratoServicosIndicador_Indicador2);
         lV21ContratoServicosIndicador_Indicador2 = StringUtil.Concat( StringUtil.RTrim( AV21ContratoServicosIndicador_Indicador2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoServicosIndicador_Indicador2", AV21ContratoServicosIndicador_Indicador2);
         lV25ContratoServicosIndicador_Indicador3 = StringUtil.Concat( StringUtil.RTrim( AV25ContratoServicosIndicador_Indicador3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoServicosIndicador_Indicador3", AV25ContratoServicosIndicador_Indicador3);
         lV25ContratoServicosIndicador_Indicador3 = StringUtil.Concat( StringUtil.RTrim( AV25ContratoServicosIndicador_Indicador3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoServicosIndicador_Indicador3", AV25ContratoServicosIndicador_Indicador3);
         lV52TFContratoServicosIndicador_Indicador = StringUtil.Concat( StringUtil.RTrim( AV52TFContratoServicosIndicador_Indicador), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFContratoServicosIndicador_Indicador", AV52TFContratoServicosIndicador_Indicador);
         lV56TFContratoServicosIndicador_Finalidade = StringUtil.Concat( StringUtil.RTrim( AV56TFContratoServicosIndicador_Finalidade), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFContratoServicosIndicador_Finalidade", AV56TFContratoServicosIndicador_Finalidade);
         lV60TFContratoServicosIndicador_Meta = StringUtil.Concat( StringUtil.RTrim( AV60TFContratoServicosIndicador_Meta), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContratoServicosIndicador_Meta", AV60TFContratoServicosIndicador_Meta);
         lV64TFContratoServicosIndicador_InstrumentoMedicao = StringUtil.Concat( StringUtil.RTrim( AV64TFContratoServicosIndicador_InstrumentoMedicao), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFContratoServicosIndicador_InstrumentoMedicao", AV64TFContratoServicosIndicador_InstrumentoMedicao);
         lV76TFContratoServicosIndicador_Vigencia = StringUtil.Concat( StringUtil.RTrim( AV76TFContratoServicosIndicador_Vigencia), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFContratoServicosIndicador_Vigencia", AV76TFContratoServicosIndicador_Vigencia);
         /* Using cursor H00JG5 */
         pr_default.execute(1, new Object[] {AV80TFContratoServicosIndicador_QtdeFaixas, AV80TFContratoServicosIndicador_QtdeFaixas, AV81TFContratoServicosIndicador_QtdeFaixas_To, AV81TFContratoServicosIndicador_QtdeFaixas_To, lV17ContratoServicosIndicador_Indicador1, lV17ContratoServicosIndicador_Indicador1, lV21ContratoServicosIndicador_Indicador2, lV21ContratoServicosIndicador_Indicador2, lV25ContratoServicosIndicador_Indicador3, lV25ContratoServicosIndicador_Indicador3, AV32TFContratoServicosIndicador_Codigo, AV33TFContratoServicosIndicador_Codigo_To, AV36TFContratoServicosIndicador_CntSrvCod, AV37TFContratoServicosIndicador_CntSrvCod_To, AV40TFContratoServicosIndicador_ContratoCod, AV41TFContratoServicosIndicador_ContratoCod_To, AV44TFContratoServicosIndicador_AreaTrabalhoCod, AV45TFContratoServicosIndicador_AreaTrabalhoCod_To, AV48TFContratoServicosIndicador_Numero, AV49TFContratoServicosIndicador_Numero_To, lV52TFContratoServicosIndicador_Indicador, AV53TFContratoServicosIndicador_Indicador_Sel, lV56TFContratoServicosIndicador_Finalidade, AV57TFContratoServicosIndicador_Finalidade_Sel, lV60TFContratoServicosIndicador_Meta, AV61TFContratoServicosIndicador_Meta_Sel, lV64TFContratoServicosIndicador_InstrumentoMedicao, AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel, lV76TFContratoServicosIndicador_Vigencia, AV77TFContratoServicosIndicador_Vigencia_Sel});
         GRID_nRecordCount = H00JG5_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoServicosIndicador_Indicador1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoServicosIndicador_Indicador2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoServicosIndicador_Indicador3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV32TFContratoServicosIndicador_Codigo, AV33TFContratoServicosIndicador_Codigo_To, AV36TFContratoServicosIndicador_CntSrvCod, AV37TFContratoServicosIndicador_CntSrvCod_To, AV40TFContratoServicosIndicador_ContratoCod, AV41TFContratoServicosIndicador_ContratoCod_To, AV44TFContratoServicosIndicador_AreaTrabalhoCod, AV45TFContratoServicosIndicador_AreaTrabalhoCod_To, AV48TFContratoServicosIndicador_Numero, AV49TFContratoServicosIndicador_Numero_To, AV52TFContratoServicosIndicador_Indicador, AV53TFContratoServicosIndicador_Indicador_Sel, AV56TFContratoServicosIndicador_Finalidade, AV57TFContratoServicosIndicador_Finalidade_Sel, AV60TFContratoServicosIndicador_Meta, AV61TFContratoServicosIndicador_Meta_Sel, AV64TFContratoServicosIndicador_InstrumentoMedicao, AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel, AV76TFContratoServicosIndicador_Vigencia, AV77TFContratoServicosIndicador_Vigencia_Sel, AV80TFContratoServicosIndicador_QtdeFaixas, AV81TFContratoServicosIndicador_QtdeFaixas_To, AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace, AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace, AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace, AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace, AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace, AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace, AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace, AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace, AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace, AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace, AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace, AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace, AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace, AV69TFContratoServicosIndicador_Tipo_Sels, AV73TFContratoServicosIndicador_Periodicidade_Sels, AV90Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoServicosIndicador_Indicador1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoServicosIndicador_Indicador2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoServicosIndicador_Indicador3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV32TFContratoServicosIndicador_Codigo, AV33TFContratoServicosIndicador_Codigo_To, AV36TFContratoServicosIndicador_CntSrvCod, AV37TFContratoServicosIndicador_CntSrvCod_To, AV40TFContratoServicosIndicador_ContratoCod, AV41TFContratoServicosIndicador_ContratoCod_To, AV44TFContratoServicosIndicador_AreaTrabalhoCod, AV45TFContratoServicosIndicador_AreaTrabalhoCod_To, AV48TFContratoServicosIndicador_Numero, AV49TFContratoServicosIndicador_Numero_To, AV52TFContratoServicosIndicador_Indicador, AV53TFContratoServicosIndicador_Indicador_Sel, AV56TFContratoServicosIndicador_Finalidade, AV57TFContratoServicosIndicador_Finalidade_Sel, AV60TFContratoServicosIndicador_Meta, AV61TFContratoServicosIndicador_Meta_Sel, AV64TFContratoServicosIndicador_InstrumentoMedicao, AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel, AV76TFContratoServicosIndicador_Vigencia, AV77TFContratoServicosIndicador_Vigencia_Sel, AV80TFContratoServicosIndicador_QtdeFaixas, AV81TFContratoServicosIndicador_QtdeFaixas_To, AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace, AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace, AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace, AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace, AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace, AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace, AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace, AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace, AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace, AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace, AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace, AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace, AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace, AV69TFContratoServicosIndicador_Tipo_Sels, AV73TFContratoServicosIndicador_Periodicidade_Sels, AV90Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoServicosIndicador_Indicador1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoServicosIndicador_Indicador2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoServicosIndicador_Indicador3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV32TFContratoServicosIndicador_Codigo, AV33TFContratoServicosIndicador_Codigo_To, AV36TFContratoServicosIndicador_CntSrvCod, AV37TFContratoServicosIndicador_CntSrvCod_To, AV40TFContratoServicosIndicador_ContratoCod, AV41TFContratoServicosIndicador_ContratoCod_To, AV44TFContratoServicosIndicador_AreaTrabalhoCod, AV45TFContratoServicosIndicador_AreaTrabalhoCod_To, AV48TFContratoServicosIndicador_Numero, AV49TFContratoServicosIndicador_Numero_To, AV52TFContratoServicosIndicador_Indicador, AV53TFContratoServicosIndicador_Indicador_Sel, AV56TFContratoServicosIndicador_Finalidade, AV57TFContratoServicosIndicador_Finalidade_Sel, AV60TFContratoServicosIndicador_Meta, AV61TFContratoServicosIndicador_Meta_Sel, AV64TFContratoServicosIndicador_InstrumentoMedicao, AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel, AV76TFContratoServicosIndicador_Vigencia, AV77TFContratoServicosIndicador_Vigencia_Sel, AV80TFContratoServicosIndicador_QtdeFaixas, AV81TFContratoServicosIndicador_QtdeFaixas_To, AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace, AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace, AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace, AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace, AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace, AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace, AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace, AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace, AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace, AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace, AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace, AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace, AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace, AV69TFContratoServicosIndicador_Tipo_Sels, AV73TFContratoServicosIndicador_Periodicidade_Sels, AV90Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoServicosIndicador_Indicador1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoServicosIndicador_Indicador2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoServicosIndicador_Indicador3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV32TFContratoServicosIndicador_Codigo, AV33TFContratoServicosIndicador_Codigo_To, AV36TFContratoServicosIndicador_CntSrvCod, AV37TFContratoServicosIndicador_CntSrvCod_To, AV40TFContratoServicosIndicador_ContratoCod, AV41TFContratoServicosIndicador_ContratoCod_To, AV44TFContratoServicosIndicador_AreaTrabalhoCod, AV45TFContratoServicosIndicador_AreaTrabalhoCod_To, AV48TFContratoServicosIndicador_Numero, AV49TFContratoServicosIndicador_Numero_To, AV52TFContratoServicosIndicador_Indicador, AV53TFContratoServicosIndicador_Indicador_Sel, AV56TFContratoServicosIndicador_Finalidade, AV57TFContratoServicosIndicador_Finalidade_Sel, AV60TFContratoServicosIndicador_Meta, AV61TFContratoServicosIndicador_Meta_Sel, AV64TFContratoServicosIndicador_InstrumentoMedicao, AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel, AV76TFContratoServicosIndicador_Vigencia, AV77TFContratoServicosIndicador_Vigencia_Sel, AV80TFContratoServicosIndicador_QtdeFaixas, AV81TFContratoServicosIndicador_QtdeFaixas_To, AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace, AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace, AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace, AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace, AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace, AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace, AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace, AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace, AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace, AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace, AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace, AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace, AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace, AV69TFContratoServicosIndicador_Tipo_Sels, AV73TFContratoServicosIndicador_Periodicidade_Sels, AV90Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoServicosIndicador_Indicador1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoServicosIndicador_Indicador2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoServicosIndicador_Indicador3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV32TFContratoServicosIndicador_Codigo, AV33TFContratoServicosIndicador_Codigo_To, AV36TFContratoServicosIndicador_CntSrvCod, AV37TFContratoServicosIndicador_CntSrvCod_To, AV40TFContratoServicosIndicador_ContratoCod, AV41TFContratoServicosIndicador_ContratoCod_To, AV44TFContratoServicosIndicador_AreaTrabalhoCod, AV45TFContratoServicosIndicador_AreaTrabalhoCod_To, AV48TFContratoServicosIndicador_Numero, AV49TFContratoServicosIndicador_Numero_To, AV52TFContratoServicosIndicador_Indicador, AV53TFContratoServicosIndicador_Indicador_Sel, AV56TFContratoServicosIndicador_Finalidade, AV57TFContratoServicosIndicador_Finalidade_Sel, AV60TFContratoServicosIndicador_Meta, AV61TFContratoServicosIndicador_Meta_Sel, AV64TFContratoServicosIndicador_InstrumentoMedicao, AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel, AV76TFContratoServicosIndicador_Vigencia, AV77TFContratoServicosIndicador_Vigencia_Sel, AV80TFContratoServicosIndicador_QtdeFaixas, AV81TFContratoServicosIndicador_QtdeFaixas_To, AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace, AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace, AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace, AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace, AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace, AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace, AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace, AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace, AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace, AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace, AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace, AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace, AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace, AV69TFContratoServicosIndicador_Tipo_Sels, AV73TFContratoServicosIndicador_Periodicidade_Sels, AV90Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return (int)(0) ;
      }

      protected void STRUPJG0( )
      {
         /* Before Start, stand alone formulas. */
         AV90Pgmname = "PromptContratoServicosIndicador";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E35JG2 */
         E35JG2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV83DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSERVICOSINDICADOR_CODIGOTITLEFILTERDATA"), AV31ContratoServicosIndicador_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSERVICOSINDICADOR_CNTSRVCODTITLEFILTERDATA"), AV35ContratoServicosIndicador_CntSrvCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSERVICOSINDICADOR_CONTRATOCODTITLEFILTERDATA"), AV39ContratoServicosIndicador_ContratoCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSERVICOSINDICADOR_AREATRABALHOCODTITLEFILTERDATA"), AV43ContratoServicosIndicador_AreaTrabalhoCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSERVICOSINDICADOR_NUMEROTITLEFILTERDATA"), AV47ContratoServicosIndicador_NumeroTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSERVICOSINDICADOR_INDICADORTITLEFILTERDATA"), AV51ContratoServicosIndicador_IndicadorTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSERVICOSINDICADOR_FINALIDADETITLEFILTERDATA"), AV55ContratoServicosIndicador_FinalidadeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSERVICOSINDICADOR_METATITLEFILTERDATA"), AV59ContratoServicosIndicador_MetaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAOTITLEFILTERDATA"), AV63ContratoServicosIndicador_InstrumentoMedicaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSERVICOSINDICADOR_TIPOTITLEFILTERDATA"), AV67ContratoServicosIndicador_TipoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSERVICOSINDICADOR_PERIODICIDADETITLEFILTERDATA"), AV71ContratoServicosIndicador_PeriodicidadeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSERVICOSINDICADOR_VIGENCIATITLEFILTERDATA"), AV75ContratoServicosIndicador_VigenciaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSERVICOSINDICADOR_QTDEFAIXASTITLEFILTERDATA"), AV79ContratoServicosIndicador_QtdeFaixasTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            AV17ContratoServicosIndicador_Indicador1 = cgiGet( edtavContratoservicosindicador_indicador1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoServicosIndicador_Indicador1", AV17ContratoServicosIndicador_Indicador1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            AV21ContratoServicosIndicador_Indicador2 = cgiGet( edtavContratoservicosindicador_indicador2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoServicosIndicador_Indicador2", AV21ContratoServicosIndicador_Indicador2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            AV25ContratoServicosIndicador_Indicador3 = cgiGet( edtavContratoservicosindicador_indicador3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoServicosIndicador_Indicador3", AV25ContratoServicosIndicador_Indicador3);
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSINDICADOR_CODIGO");
               GX_FocusControl = edtavTfcontratoservicosindicador_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV32TFContratoServicosIndicador_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFContratoServicosIndicador_Codigo), 6, 0)));
            }
            else
            {
               AV32TFContratoServicosIndicador_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFContratoServicosIndicador_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSINDICADOR_CODIGO_TO");
               GX_FocusControl = edtavTfcontratoservicosindicador_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV33TFContratoServicosIndicador_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TFContratoServicosIndicador_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33TFContratoServicosIndicador_Codigo_To), 6, 0)));
            }
            else
            {
               AV33TFContratoServicosIndicador_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TFContratoServicosIndicador_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33TFContratoServicosIndicador_Codigo_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_cntsrvcod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_cntsrvcod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD");
               GX_FocusControl = edtavTfcontratoservicosindicador_cntsrvcod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV36TFContratoServicosIndicador_CntSrvCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContratoServicosIndicador_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFContratoServicosIndicador_CntSrvCod), 6, 0)));
            }
            else
            {
               AV36TFContratoServicosIndicador_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_cntsrvcod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContratoServicosIndicador_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFContratoServicosIndicador_CntSrvCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_cntsrvcod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_cntsrvcod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD_TO");
               GX_FocusControl = edtavTfcontratoservicosindicador_cntsrvcod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV37TFContratoServicosIndicador_CntSrvCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFContratoServicosIndicador_CntSrvCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37TFContratoServicosIndicador_CntSrvCod_To), 6, 0)));
            }
            else
            {
               AV37TFContratoServicosIndicador_CntSrvCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_cntsrvcod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFContratoServicosIndicador_CntSrvCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37TFContratoServicosIndicador_CntSrvCod_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_contratocod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_contratocod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD");
               GX_FocusControl = edtavTfcontratoservicosindicador_contratocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV40TFContratoServicosIndicador_ContratoCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContratoServicosIndicador_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40TFContratoServicosIndicador_ContratoCod), 6, 0)));
            }
            else
            {
               AV40TFContratoServicosIndicador_ContratoCod = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_contratocod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContratoServicosIndicador_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40TFContratoServicosIndicador_ContratoCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_contratocod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_contratocod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD_TO");
               GX_FocusControl = edtavTfcontratoservicosindicador_contratocod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV41TFContratoServicosIndicador_ContratoCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFContratoServicosIndicador_ContratoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41TFContratoServicosIndicador_ContratoCod_To), 6, 0)));
            }
            else
            {
               AV41TFContratoServicosIndicador_ContratoCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_contratocod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFContratoServicosIndicador_ContratoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41TFContratoServicosIndicador_ContratoCod_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_areatrabalhocod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_areatrabalhocod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD");
               GX_FocusControl = edtavTfcontratoservicosindicador_areatrabalhocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV44TFContratoServicosIndicador_AreaTrabalhoCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContratoServicosIndicador_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFContratoServicosIndicador_AreaTrabalhoCod), 6, 0)));
            }
            else
            {
               AV44TFContratoServicosIndicador_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_areatrabalhocod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContratoServicosIndicador_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFContratoServicosIndicador_AreaTrabalhoCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_areatrabalhocod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_areatrabalhocod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_TO");
               GX_FocusControl = edtavTfcontratoservicosindicador_areatrabalhocod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV45TFContratoServicosIndicador_AreaTrabalhoCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContratoServicosIndicador_AreaTrabalhoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFContratoServicosIndicador_AreaTrabalhoCod_To), 6, 0)));
            }
            else
            {
               AV45TFContratoServicosIndicador_AreaTrabalhoCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_areatrabalhocod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContratoServicosIndicador_AreaTrabalhoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFContratoServicosIndicador_AreaTrabalhoCod_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_numero_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_numero_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSINDICADOR_NUMERO");
               GX_FocusControl = edtavTfcontratoservicosindicador_numero_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV48TFContratoServicosIndicador_Numero = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContratoServicosIndicador_Numero", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48TFContratoServicosIndicador_Numero), 4, 0)));
            }
            else
            {
               AV48TFContratoServicosIndicador_Numero = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_numero_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContratoServicosIndicador_Numero", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48TFContratoServicosIndicador_Numero), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_numero_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_numero_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSINDICADOR_NUMERO_TO");
               GX_FocusControl = edtavTfcontratoservicosindicador_numero_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV49TFContratoServicosIndicador_Numero_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContratoServicosIndicador_Numero_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49TFContratoServicosIndicador_Numero_To), 4, 0)));
            }
            else
            {
               AV49TFContratoServicosIndicador_Numero_To = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_numero_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContratoServicosIndicador_Numero_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49TFContratoServicosIndicador_Numero_To), 4, 0)));
            }
            AV52TFContratoServicosIndicador_Indicador = cgiGet( edtavTfcontratoservicosindicador_indicador_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFContratoServicosIndicador_Indicador", AV52TFContratoServicosIndicador_Indicador);
            AV53TFContratoServicosIndicador_Indicador_Sel = cgiGet( edtavTfcontratoservicosindicador_indicador_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContratoServicosIndicador_Indicador_Sel", AV53TFContratoServicosIndicador_Indicador_Sel);
            AV56TFContratoServicosIndicador_Finalidade = cgiGet( edtavTfcontratoservicosindicador_finalidade_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFContratoServicosIndicador_Finalidade", AV56TFContratoServicosIndicador_Finalidade);
            AV57TFContratoServicosIndicador_Finalidade_Sel = cgiGet( edtavTfcontratoservicosindicador_finalidade_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFContratoServicosIndicador_Finalidade_Sel", AV57TFContratoServicosIndicador_Finalidade_Sel);
            AV60TFContratoServicosIndicador_Meta = cgiGet( edtavTfcontratoservicosindicador_meta_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContratoServicosIndicador_Meta", AV60TFContratoServicosIndicador_Meta);
            AV61TFContratoServicosIndicador_Meta_Sel = cgiGet( edtavTfcontratoservicosindicador_meta_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFContratoServicosIndicador_Meta_Sel", AV61TFContratoServicosIndicador_Meta_Sel);
            AV64TFContratoServicosIndicador_InstrumentoMedicao = cgiGet( edtavTfcontratoservicosindicador_instrumentomedicao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFContratoServicosIndicador_InstrumentoMedicao", AV64TFContratoServicosIndicador_InstrumentoMedicao);
            AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel = cgiGet( edtavTfcontratoservicosindicador_instrumentomedicao_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel", AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel);
            AV76TFContratoServicosIndicador_Vigencia = StringUtil.Upper( cgiGet( edtavTfcontratoservicosindicador_vigencia_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFContratoServicosIndicador_Vigencia", AV76TFContratoServicosIndicador_Vigencia);
            AV77TFContratoServicosIndicador_Vigencia_Sel = StringUtil.Upper( cgiGet( edtavTfcontratoservicosindicador_vigencia_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFContratoServicosIndicador_Vigencia_Sel", AV77TFContratoServicosIndicador_Vigencia_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_qtdefaixas_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_qtdefaixas_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS");
               GX_FocusControl = edtavTfcontratoservicosindicador_qtdefaixas_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV80TFContratoServicosIndicador_QtdeFaixas = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV80TFContratoServicosIndicador_QtdeFaixas), 4, 0)));
            }
            else
            {
               AV80TFContratoServicosIndicador_QtdeFaixas = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_qtdefaixas_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV80TFContratoServicosIndicador_QtdeFaixas), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_qtdefaixas_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_qtdefaixas_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS_TO");
               GX_FocusControl = edtavTfcontratoservicosindicador_qtdefaixas_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV81TFContratoServicosIndicador_QtdeFaixas_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFContratoServicosIndicador_QtdeFaixas_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV81TFContratoServicosIndicador_QtdeFaixas_To), 4, 0)));
            }
            else
            {
               AV81TFContratoServicosIndicador_QtdeFaixas_To = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_qtdefaixas_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFContratoServicosIndicador_QtdeFaixas_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV81TFContratoServicosIndicador_QtdeFaixas_To), 4, 0)));
            }
            AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosindicador_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace", AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace);
            AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosindicador_cntsrvcodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace", AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace);
            AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosindicador_contratocodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace", AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace);
            AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosindicador_areatrabalhocodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace", AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace);
            AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosindicador_numerotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace", AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace);
            AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosindicador_indicadortitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace", AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace);
            AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosindicador_finalidadetitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace", AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace);
            AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosindicador_metatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace", AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace);
            AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosindicador_instrumentomedicaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace", AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace);
            AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosindicador_tipotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace", AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace);
            AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosindicador_periodicidadetitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace", AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace);
            AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosindicador_vigenciatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace", AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace);
            AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosindicador_qtdefaixastitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace", AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_80 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_80"), ",", "."));
            AV85GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV86GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contratoservicosindicador_codigo_Caption = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Caption");
            Ddo_contratoservicosindicador_codigo_Tooltip = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Tooltip");
            Ddo_contratoservicosindicador_codigo_Cls = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Cls");
            Ddo_contratoservicosindicador_codigo_Filteredtext_set = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Filteredtext_set");
            Ddo_contratoservicosindicador_codigo_Filteredtextto_set = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Filteredtextto_set");
            Ddo_contratoservicosindicador_codigo_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Dropdownoptionstype");
            Ddo_contratoservicosindicador_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Titlecontrolidtoreplace");
            Ddo_contratoservicosindicador_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Includesortasc"));
            Ddo_contratoservicosindicador_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Includesortdsc"));
            Ddo_contratoservicosindicador_codigo_Sortedstatus = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Sortedstatus");
            Ddo_contratoservicosindicador_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Includefilter"));
            Ddo_contratoservicosindicador_codigo_Filtertype = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Filtertype");
            Ddo_contratoservicosindicador_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Filterisrange"));
            Ddo_contratoservicosindicador_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Includedatalist"));
            Ddo_contratoservicosindicador_codigo_Sortasc = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Sortasc");
            Ddo_contratoservicosindicador_codigo_Sortdsc = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Sortdsc");
            Ddo_contratoservicosindicador_codigo_Cleanfilter = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Cleanfilter");
            Ddo_contratoservicosindicador_codigo_Rangefilterfrom = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Rangefilterfrom");
            Ddo_contratoservicosindicador_codigo_Rangefilterto = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Rangefilterto");
            Ddo_contratoservicosindicador_codigo_Searchbuttontext = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Searchbuttontext");
            Ddo_contratoservicosindicador_cntsrvcod_Caption = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Caption");
            Ddo_contratoservicosindicador_cntsrvcod_Tooltip = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Tooltip");
            Ddo_contratoservicosindicador_cntsrvcod_Cls = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Cls");
            Ddo_contratoservicosindicador_cntsrvcod_Filteredtext_set = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Filteredtext_set");
            Ddo_contratoservicosindicador_cntsrvcod_Filteredtextto_set = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Filteredtextto_set");
            Ddo_contratoservicosindicador_cntsrvcod_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Dropdownoptionstype");
            Ddo_contratoservicosindicador_cntsrvcod_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Titlecontrolidtoreplace");
            Ddo_contratoservicosindicador_cntsrvcod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Includesortasc"));
            Ddo_contratoservicosindicador_cntsrvcod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Includesortdsc"));
            Ddo_contratoservicosindicador_cntsrvcod_Sortedstatus = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Sortedstatus");
            Ddo_contratoservicosindicador_cntsrvcod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Includefilter"));
            Ddo_contratoservicosindicador_cntsrvcod_Filtertype = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Filtertype");
            Ddo_contratoservicosindicador_cntsrvcod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Filterisrange"));
            Ddo_contratoservicosindicador_cntsrvcod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Includedatalist"));
            Ddo_contratoservicosindicador_cntsrvcod_Sortasc = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Sortasc");
            Ddo_contratoservicosindicador_cntsrvcod_Sortdsc = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Sortdsc");
            Ddo_contratoservicosindicador_cntsrvcod_Cleanfilter = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Cleanfilter");
            Ddo_contratoservicosindicador_cntsrvcod_Rangefilterfrom = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Rangefilterfrom");
            Ddo_contratoservicosindicador_cntsrvcod_Rangefilterto = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Rangefilterto");
            Ddo_contratoservicosindicador_cntsrvcod_Searchbuttontext = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Searchbuttontext");
            Ddo_contratoservicosindicador_contratocod_Caption = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Caption");
            Ddo_contratoservicosindicador_contratocod_Tooltip = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Tooltip");
            Ddo_contratoservicosindicador_contratocod_Cls = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Cls");
            Ddo_contratoservicosindicador_contratocod_Filteredtext_set = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Filteredtext_set");
            Ddo_contratoservicosindicador_contratocod_Filteredtextto_set = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Filteredtextto_set");
            Ddo_contratoservicosindicador_contratocod_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Dropdownoptionstype");
            Ddo_contratoservicosindicador_contratocod_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Titlecontrolidtoreplace");
            Ddo_contratoservicosindicador_contratocod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Includesortasc"));
            Ddo_contratoservicosindicador_contratocod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Includesortdsc"));
            Ddo_contratoservicosindicador_contratocod_Sortedstatus = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Sortedstatus");
            Ddo_contratoservicosindicador_contratocod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Includefilter"));
            Ddo_contratoservicosindicador_contratocod_Filtertype = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Filtertype");
            Ddo_contratoservicosindicador_contratocod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Filterisrange"));
            Ddo_contratoservicosindicador_contratocod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Includedatalist"));
            Ddo_contratoservicosindicador_contratocod_Sortasc = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Sortasc");
            Ddo_contratoservicosindicador_contratocod_Sortdsc = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Sortdsc");
            Ddo_contratoservicosindicador_contratocod_Cleanfilter = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Cleanfilter");
            Ddo_contratoservicosindicador_contratocod_Rangefilterfrom = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Rangefilterfrom");
            Ddo_contratoservicosindicador_contratocod_Rangefilterto = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Rangefilterto");
            Ddo_contratoservicosindicador_contratocod_Searchbuttontext = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Searchbuttontext");
            Ddo_contratoservicosindicador_areatrabalhocod_Caption = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Caption");
            Ddo_contratoservicosindicador_areatrabalhocod_Tooltip = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Tooltip");
            Ddo_contratoservicosindicador_areatrabalhocod_Cls = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Cls");
            Ddo_contratoservicosindicador_areatrabalhocod_Filteredtext_set = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Filteredtext_set");
            Ddo_contratoservicosindicador_areatrabalhocod_Filteredtextto_set = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Filteredtextto_set");
            Ddo_contratoservicosindicador_areatrabalhocod_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Dropdownoptionstype");
            Ddo_contratoservicosindicador_areatrabalhocod_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Titlecontrolidtoreplace");
            Ddo_contratoservicosindicador_areatrabalhocod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Includesortasc"));
            Ddo_contratoservicosindicador_areatrabalhocod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Includesortdsc"));
            Ddo_contratoservicosindicador_areatrabalhocod_Sortedstatus = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Sortedstatus");
            Ddo_contratoservicosindicador_areatrabalhocod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Includefilter"));
            Ddo_contratoservicosindicador_areatrabalhocod_Filtertype = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Filtertype");
            Ddo_contratoservicosindicador_areatrabalhocod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Filterisrange"));
            Ddo_contratoservicosindicador_areatrabalhocod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Includedatalist"));
            Ddo_contratoservicosindicador_areatrabalhocod_Sortasc = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Sortasc");
            Ddo_contratoservicosindicador_areatrabalhocod_Sortdsc = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Sortdsc");
            Ddo_contratoservicosindicador_areatrabalhocod_Cleanfilter = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Cleanfilter");
            Ddo_contratoservicosindicador_areatrabalhocod_Rangefilterfrom = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Rangefilterfrom");
            Ddo_contratoservicosindicador_areatrabalhocod_Rangefilterto = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Rangefilterto");
            Ddo_contratoservicosindicador_areatrabalhocod_Searchbuttontext = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Searchbuttontext");
            Ddo_contratoservicosindicador_numero_Caption = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Caption");
            Ddo_contratoservicosindicador_numero_Tooltip = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Tooltip");
            Ddo_contratoservicosindicador_numero_Cls = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Cls");
            Ddo_contratoservicosindicador_numero_Filteredtext_set = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Filteredtext_set");
            Ddo_contratoservicosindicador_numero_Filteredtextto_set = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Filteredtextto_set");
            Ddo_contratoservicosindicador_numero_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Dropdownoptionstype");
            Ddo_contratoservicosindicador_numero_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Titlecontrolidtoreplace");
            Ddo_contratoservicosindicador_numero_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Includesortasc"));
            Ddo_contratoservicosindicador_numero_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Includesortdsc"));
            Ddo_contratoservicosindicador_numero_Sortedstatus = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Sortedstatus");
            Ddo_contratoservicosindicador_numero_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Includefilter"));
            Ddo_contratoservicosindicador_numero_Filtertype = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Filtertype");
            Ddo_contratoservicosindicador_numero_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Filterisrange"));
            Ddo_contratoservicosindicador_numero_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Includedatalist"));
            Ddo_contratoservicosindicador_numero_Sortasc = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Sortasc");
            Ddo_contratoservicosindicador_numero_Sortdsc = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Sortdsc");
            Ddo_contratoservicosindicador_numero_Cleanfilter = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Cleanfilter");
            Ddo_contratoservicosindicador_numero_Rangefilterfrom = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Rangefilterfrom");
            Ddo_contratoservicosindicador_numero_Rangefilterto = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Rangefilterto");
            Ddo_contratoservicosindicador_numero_Searchbuttontext = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Searchbuttontext");
            Ddo_contratoservicosindicador_indicador_Caption = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Caption");
            Ddo_contratoservicosindicador_indicador_Tooltip = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Tooltip");
            Ddo_contratoservicosindicador_indicador_Cls = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Cls");
            Ddo_contratoservicosindicador_indicador_Filteredtext_set = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Filteredtext_set");
            Ddo_contratoservicosindicador_indicador_Selectedvalue_set = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Selectedvalue_set");
            Ddo_contratoservicosindicador_indicador_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Dropdownoptionstype");
            Ddo_contratoservicosindicador_indicador_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Titlecontrolidtoreplace");
            Ddo_contratoservicosindicador_indicador_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Includesortasc"));
            Ddo_contratoservicosindicador_indicador_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Includesortdsc"));
            Ddo_contratoservicosindicador_indicador_Sortedstatus = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Sortedstatus");
            Ddo_contratoservicosindicador_indicador_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Includefilter"));
            Ddo_contratoservicosindicador_indicador_Filtertype = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Filtertype");
            Ddo_contratoservicosindicador_indicador_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Filterisrange"));
            Ddo_contratoservicosindicador_indicador_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Includedatalist"));
            Ddo_contratoservicosindicador_indicador_Datalisttype = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Datalisttype");
            Ddo_contratoservicosindicador_indicador_Datalistproc = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Datalistproc");
            Ddo_contratoservicosindicador_indicador_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratoservicosindicador_indicador_Sortasc = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Sortasc");
            Ddo_contratoservicosindicador_indicador_Sortdsc = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Sortdsc");
            Ddo_contratoservicosindicador_indicador_Loadingdata = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Loadingdata");
            Ddo_contratoservicosindicador_indicador_Cleanfilter = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Cleanfilter");
            Ddo_contratoservicosindicador_indicador_Noresultsfound = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Noresultsfound");
            Ddo_contratoservicosindicador_indicador_Searchbuttontext = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Searchbuttontext");
            Ddo_contratoservicosindicador_finalidade_Caption = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Caption");
            Ddo_contratoservicosindicador_finalidade_Tooltip = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Tooltip");
            Ddo_contratoservicosindicador_finalidade_Cls = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Cls");
            Ddo_contratoservicosindicador_finalidade_Filteredtext_set = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Filteredtext_set");
            Ddo_contratoservicosindicador_finalidade_Selectedvalue_set = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Selectedvalue_set");
            Ddo_contratoservicosindicador_finalidade_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Dropdownoptionstype");
            Ddo_contratoservicosindicador_finalidade_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Titlecontrolidtoreplace");
            Ddo_contratoservicosindicador_finalidade_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Includesortasc"));
            Ddo_contratoservicosindicador_finalidade_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Includesortdsc"));
            Ddo_contratoservicosindicador_finalidade_Sortedstatus = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Sortedstatus");
            Ddo_contratoservicosindicador_finalidade_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Includefilter"));
            Ddo_contratoservicosindicador_finalidade_Filtertype = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Filtertype");
            Ddo_contratoservicosindicador_finalidade_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Filterisrange"));
            Ddo_contratoservicosindicador_finalidade_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Includedatalist"));
            Ddo_contratoservicosindicador_finalidade_Datalisttype = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Datalisttype");
            Ddo_contratoservicosindicador_finalidade_Datalistproc = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Datalistproc");
            Ddo_contratoservicosindicador_finalidade_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratoservicosindicador_finalidade_Sortasc = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Sortasc");
            Ddo_contratoservicosindicador_finalidade_Sortdsc = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Sortdsc");
            Ddo_contratoservicosindicador_finalidade_Loadingdata = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Loadingdata");
            Ddo_contratoservicosindicador_finalidade_Cleanfilter = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Cleanfilter");
            Ddo_contratoservicosindicador_finalidade_Noresultsfound = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Noresultsfound");
            Ddo_contratoservicosindicador_finalidade_Searchbuttontext = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Searchbuttontext");
            Ddo_contratoservicosindicador_meta_Caption = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_META_Caption");
            Ddo_contratoservicosindicador_meta_Tooltip = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_META_Tooltip");
            Ddo_contratoservicosindicador_meta_Cls = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_META_Cls");
            Ddo_contratoservicosindicador_meta_Filteredtext_set = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_META_Filteredtext_set");
            Ddo_contratoservicosindicador_meta_Selectedvalue_set = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_META_Selectedvalue_set");
            Ddo_contratoservicosindicador_meta_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_META_Dropdownoptionstype");
            Ddo_contratoservicosindicador_meta_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_META_Titlecontrolidtoreplace");
            Ddo_contratoservicosindicador_meta_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_META_Includesortasc"));
            Ddo_contratoservicosindicador_meta_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_META_Includesortdsc"));
            Ddo_contratoservicosindicador_meta_Sortedstatus = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_META_Sortedstatus");
            Ddo_contratoservicosindicador_meta_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_META_Includefilter"));
            Ddo_contratoservicosindicador_meta_Filtertype = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_META_Filtertype");
            Ddo_contratoservicosindicador_meta_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_META_Filterisrange"));
            Ddo_contratoservicosindicador_meta_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_META_Includedatalist"));
            Ddo_contratoservicosindicador_meta_Datalisttype = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_META_Datalisttype");
            Ddo_contratoservicosindicador_meta_Datalistproc = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_META_Datalistproc");
            Ddo_contratoservicosindicador_meta_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_META_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratoservicosindicador_meta_Sortasc = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_META_Sortasc");
            Ddo_contratoservicosindicador_meta_Sortdsc = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_META_Sortdsc");
            Ddo_contratoservicosindicador_meta_Loadingdata = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_META_Loadingdata");
            Ddo_contratoservicosindicador_meta_Cleanfilter = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_META_Cleanfilter");
            Ddo_contratoservicosindicador_meta_Noresultsfound = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_META_Noresultsfound");
            Ddo_contratoservicosindicador_meta_Searchbuttontext = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_META_Searchbuttontext");
            Ddo_contratoservicosindicador_instrumentomedicao_Caption = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Caption");
            Ddo_contratoservicosindicador_instrumentomedicao_Tooltip = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Tooltip");
            Ddo_contratoservicosindicador_instrumentomedicao_Cls = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Cls");
            Ddo_contratoservicosindicador_instrumentomedicao_Filteredtext_set = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Filteredtext_set");
            Ddo_contratoservicosindicador_instrumentomedicao_Selectedvalue_set = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Selectedvalue_set");
            Ddo_contratoservicosindicador_instrumentomedicao_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Dropdownoptionstype");
            Ddo_contratoservicosindicador_instrumentomedicao_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Titlecontrolidtoreplace");
            Ddo_contratoservicosindicador_instrumentomedicao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Includesortasc"));
            Ddo_contratoservicosindicador_instrumentomedicao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Includesortdsc"));
            Ddo_contratoservicosindicador_instrumentomedicao_Sortedstatus = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Sortedstatus");
            Ddo_contratoservicosindicador_instrumentomedicao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Includefilter"));
            Ddo_contratoservicosindicador_instrumentomedicao_Filtertype = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Filtertype");
            Ddo_contratoservicosindicador_instrumentomedicao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Filterisrange"));
            Ddo_contratoservicosindicador_instrumentomedicao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Includedatalist"));
            Ddo_contratoservicosindicador_instrumentomedicao_Datalisttype = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Datalisttype");
            Ddo_contratoservicosindicador_instrumentomedicao_Datalistproc = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Datalistproc");
            Ddo_contratoservicosindicador_instrumentomedicao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratoservicosindicador_instrumentomedicao_Sortasc = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Sortasc");
            Ddo_contratoservicosindicador_instrumentomedicao_Sortdsc = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Sortdsc");
            Ddo_contratoservicosindicador_instrumentomedicao_Loadingdata = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Loadingdata");
            Ddo_contratoservicosindicador_instrumentomedicao_Cleanfilter = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Cleanfilter");
            Ddo_contratoservicosindicador_instrumentomedicao_Noresultsfound = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Noresultsfound");
            Ddo_contratoservicosindicador_instrumentomedicao_Searchbuttontext = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Searchbuttontext");
            Ddo_contratoservicosindicador_tipo_Caption = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_TIPO_Caption");
            Ddo_contratoservicosindicador_tipo_Tooltip = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_TIPO_Tooltip");
            Ddo_contratoservicosindicador_tipo_Cls = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_TIPO_Cls");
            Ddo_contratoservicosindicador_tipo_Selectedvalue_set = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_TIPO_Selectedvalue_set");
            Ddo_contratoservicosindicador_tipo_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_TIPO_Dropdownoptionstype");
            Ddo_contratoservicosindicador_tipo_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_TIPO_Titlecontrolidtoreplace");
            Ddo_contratoservicosindicador_tipo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_TIPO_Includesortasc"));
            Ddo_contratoservicosindicador_tipo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_TIPO_Includesortdsc"));
            Ddo_contratoservicosindicador_tipo_Sortedstatus = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_TIPO_Sortedstatus");
            Ddo_contratoservicosindicador_tipo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_TIPO_Includefilter"));
            Ddo_contratoservicosindicador_tipo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_TIPO_Includedatalist"));
            Ddo_contratoservicosindicador_tipo_Datalisttype = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_TIPO_Datalisttype");
            Ddo_contratoservicosindicador_tipo_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_TIPO_Allowmultipleselection"));
            Ddo_contratoservicosindicador_tipo_Datalistfixedvalues = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_TIPO_Datalistfixedvalues");
            Ddo_contratoservicosindicador_tipo_Sortasc = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_TIPO_Sortasc");
            Ddo_contratoservicosindicador_tipo_Sortdsc = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_TIPO_Sortdsc");
            Ddo_contratoservicosindicador_tipo_Cleanfilter = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_TIPO_Cleanfilter");
            Ddo_contratoservicosindicador_tipo_Searchbuttontext = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_TIPO_Searchbuttontext");
            Ddo_contratoservicosindicador_periodicidade_Caption = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE_Caption");
            Ddo_contratoservicosindicador_periodicidade_Tooltip = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE_Tooltip");
            Ddo_contratoservicosindicador_periodicidade_Cls = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE_Cls");
            Ddo_contratoservicosindicador_periodicidade_Selectedvalue_set = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE_Selectedvalue_set");
            Ddo_contratoservicosindicador_periodicidade_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE_Dropdownoptionstype");
            Ddo_contratoservicosindicador_periodicidade_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE_Titlecontrolidtoreplace");
            Ddo_contratoservicosindicador_periodicidade_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE_Includesortasc"));
            Ddo_contratoservicosindicador_periodicidade_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE_Includesortdsc"));
            Ddo_contratoservicosindicador_periodicidade_Sortedstatus = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE_Sortedstatus");
            Ddo_contratoservicosindicador_periodicidade_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE_Includefilter"));
            Ddo_contratoservicosindicador_periodicidade_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE_Includedatalist"));
            Ddo_contratoservicosindicador_periodicidade_Datalisttype = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE_Datalisttype");
            Ddo_contratoservicosindicador_periodicidade_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE_Allowmultipleselection"));
            Ddo_contratoservicosindicador_periodicidade_Datalistfixedvalues = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE_Datalistfixedvalues");
            Ddo_contratoservicosindicador_periodicidade_Sortasc = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE_Sortasc");
            Ddo_contratoservicosindicador_periodicidade_Sortdsc = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE_Sortdsc");
            Ddo_contratoservicosindicador_periodicidade_Cleanfilter = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE_Cleanfilter");
            Ddo_contratoservicosindicador_periodicidade_Searchbuttontext = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE_Searchbuttontext");
            Ddo_contratoservicosindicador_vigencia_Caption = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Caption");
            Ddo_contratoservicosindicador_vigencia_Tooltip = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Tooltip");
            Ddo_contratoservicosindicador_vigencia_Cls = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Cls");
            Ddo_contratoservicosindicador_vigencia_Filteredtext_set = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Filteredtext_set");
            Ddo_contratoservicosindicador_vigencia_Selectedvalue_set = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Selectedvalue_set");
            Ddo_contratoservicosindicador_vigencia_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Dropdownoptionstype");
            Ddo_contratoservicosindicador_vigencia_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Titlecontrolidtoreplace");
            Ddo_contratoservicosindicador_vigencia_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Includesortasc"));
            Ddo_contratoservicosindicador_vigencia_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Includesortdsc"));
            Ddo_contratoservicosindicador_vigencia_Sortedstatus = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Sortedstatus");
            Ddo_contratoservicosindicador_vigencia_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Includefilter"));
            Ddo_contratoservicosindicador_vigencia_Filtertype = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Filtertype");
            Ddo_contratoservicosindicador_vigencia_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Filterisrange"));
            Ddo_contratoservicosindicador_vigencia_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Includedatalist"));
            Ddo_contratoservicosindicador_vigencia_Datalisttype = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Datalisttype");
            Ddo_contratoservicosindicador_vigencia_Datalistproc = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Datalistproc");
            Ddo_contratoservicosindicador_vigencia_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratoservicosindicador_vigencia_Sortasc = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Sortasc");
            Ddo_contratoservicosindicador_vigencia_Sortdsc = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Sortdsc");
            Ddo_contratoservicosindicador_vigencia_Loadingdata = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Loadingdata");
            Ddo_contratoservicosindicador_vigencia_Cleanfilter = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Cleanfilter");
            Ddo_contratoservicosindicador_vigencia_Noresultsfound = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Noresultsfound");
            Ddo_contratoservicosindicador_vigencia_Searchbuttontext = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Searchbuttontext");
            Ddo_contratoservicosindicador_qtdefaixas_Caption = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Caption");
            Ddo_contratoservicosindicador_qtdefaixas_Tooltip = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Tooltip");
            Ddo_contratoservicosindicador_qtdefaixas_Cls = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Cls");
            Ddo_contratoservicosindicador_qtdefaixas_Filteredtext_set = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Filteredtext_set");
            Ddo_contratoservicosindicador_qtdefaixas_Filteredtextto_set = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Filteredtextto_set");
            Ddo_contratoservicosindicador_qtdefaixas_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Dropdownoptionstype");
            Ddo_contratoservicosindicador_qtdefaixas_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Titlecontrolidtoreplace");
            Ddo_contratoservicosindicador_qtdefaixas_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Includesortasc"));
            Ddo_contratoservicosindicador_qtdefaixas_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Includesortdsc"));
            Ddo_contratoservicosindicador_qtdefaixas_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Includefilter"));
            Ddo_contratoservicosindicador_qtdefaixas_Filtertype = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Filtertype");
            Ddo_contratoservicosindicador_qtdefaixas_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Filterisrange"));
            Ddo_contratoservicosindicador_qtdefaixas_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Includedatalist"));
            Ddo_contratoservicosindicador_qtdefaixas_Cleanfilter = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Cleanfilter");
            Ddo_contratoservicosindicador_qtdefaixas_Rangefilterfrom = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Rangefilterfrom");
            Ddo_contratoservicosindicador_qtdefaixas_Rangefilterto = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Rangefilterto");
            Ddo_contratoservicosindicador_qtdefaixas_Searchbuttontext = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contratoservicosindicador_codigo_Activeeventkey = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Activeeventkey");
            Ddo_contratoservicosindicador_codigo_Filteredtext_get = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Filteredtext_get");
            Ddo_contratoservicosindicador_codigo_Filteredtextto_get = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CODIGO_Filteredtextto_get");
            Ddo_contratoservicosindicador_cntsrvcod_Activeeventkey = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Activeeventkey");
            Ddo_contratoservicosindicador_cntsrvcod_Filteredtext_get = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Filteredtext_get");
            Ddo_contratoservicosindicador_cntsrvcod_Filteredtextto_get = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD_Filteredtextto_get");
            Ddo_contratoservicosindicador_contratocod_Activeeventkey = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Activeeventkey");
            Ddo_contratoservicosindicador_contratocod_Filteredtext_get = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Filteredtext_get");
            Ddo_contratoservicosindicador_contratocod_Filteredtextto_get = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD_Filteredtextto_get");
            Ddo_contratoservicosindicador_areatrabalhocod_Activeeventkey = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Activeeventkey");
            Ddo_contratoservicosindicador_areatrabalhocod_Filteredtext_get = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Filteredtext_get");
            Ddo_contratoservicosindicador_areatrabalhocod_Filteredtextto_get = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_Filteredtextto_get");
            Ddo_contratoservicosindicador_numero_Activeeventkey = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Activeeventkey");
            Ddo_contratoservicosindicador_numero_Filteredtext_get = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Filteredtext_get");
            Ddo_contratoservicosindicador_numero_Filteredtextto_get = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Filteredtextto_get");
            Ddo_contratoservicosindicador_indicador_Activeeventkey = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Activeeventkey");
            Ddo_contratoservicosindicador_indicador_Filteredtext_get = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Filteredtext_get");
            Ddo_contratoservicosindicador_indicador_Selectedvalue_get = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Selectedvalue_get");
            Ddo_contratoservicosindicador_finalidade_Activeeventkey = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Activeeventkey");
            Ddo_contratoservicosindicador_finalidade_Filteredtext_get = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Filteredtext_get");
            Ddo_contratoservicosindicador_finalidade_Selectedvalue_get = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE_Selectedvalue_get");
            Ddo_contratoservicosindicador_meta_Activeeventkey = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_META_Activeeventkey");
            Ddo_contratoservicosindicador_meta_Filteredtext_get = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_META_Filteredtext_get");
            Ddo_contratoservicosindicador_meta_Selectedvalue_get = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_META_Selectedvalue_get");
            Ddo_contratoservicosindicador_instrumentomedicao_Activeeventkey = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Activeeventkey");
            Ddo_contratoservicosindicador_instrumentomedicao_Filteredtext_get = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Filteredtext_get");
            Ddo_contratoservicosindicador_instrumentomedicao_Selectedvalue_get = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_Selectedvalue_get");
            Ddo_contratoservicosindicador_tipo_Activeeventkey = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_TIPO_Activeeventkey");
            Ddo_contratoservicosindicador_tipo_Selectedvalue_get = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_TIPO_Selectedvalue_get");
            Ddo_contratoservicosindicador_periodicidade_Activeeventkey = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE_Activeeventkey");
            Ddo_contratoservicosindicador_periodicidade_Selectedvalue_get = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE_Selectedvalue_get");
            Ddo_contratoservicosindicador_vigencia_Activeeventkey = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Activeeventkey");
            Ddo_contratoservicosindicador_vigencia_Filteredtext_get = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Filteredtext_get");
            Ddo_contratoservicosindicador_vigencia_Selectedvalue_get = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA_Selectedvalue_get");
            Ddo_contratoservicosindicador_qtdefaixas_Activeeventkey = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Activeeventkey");
            Ddo_contratoservicosindicador_qtdefaixas_Filteredtext_get = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Filteredtext_get");
            Ddo_contratoservicosindicador_qtdefaixas_Filteredtextto_get = cgiGet( "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOSERVICOSINDICADOR_INDICADOR1"), AV17ContratoServicosIndicador_Indicador1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOSERVICOSINDICADOR_INDICADOR2"), AV21ContratoServicosIndicador_Indicador2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOSERVICOSINDICADOR_INDICADOR3"), AV25ContratoServicosIndicador_Indicador3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_CODIGO"), ",", ".") != Convert.ToDecimal( AV32TFContratoServicosIndicador_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV33TFContratoServicosIndicador_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD"), ",", ".") != Convert.ToDecimal( AV36TFContratoServicosIndicador_CntSrvCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD_TO"), ",", ".") != Convert.ToDecimal( AV37TFContratoServicosIndicador_CntSrvCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD"), ",", ".") != Convert.ToDecimal( AV40TFContratoServicosIndicador_ContratoCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD_TO"), ",", ".") != Convert.ToDecimal( AV41TFContratoServicosIndicador_ContratoCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD"), ",", ".") != Convert.ToDecimal( AV44TFContratoServicosIndicador_AreaTrabalhoCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_TO"), ",", ".") != Convert.ToDecimal( AV45TFContratoServicosIndicador_AreaTrabalhoCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_NUMERO"), ",", ".") != Convert.ToDecimal( AV48TFContratoServicosIndicador_Numero )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_NUMERO_TO"), ",", ".") != Convert.ToDecimal( AV49TFContratoServicosIndicador_Numero_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_INDICADOR"), AV52TFContratoServicosIndicador_Indicador) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_INDICADOR_SEL"), AV53TFContratoServicosIndicador_Indicador_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_FINALIDADE"), AV56TFContratoServicosIndicador_Finalidade) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_FINALIDADE_SEL"), AV57TFContratoServicosIndicador_Finalidade_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_META"), AV60TFContratoServicosIndicador_Meta) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_META_SEL"), AV61TFContratoServicosIndicador_Meta_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO"), AV64TFContratoServicosIndicador_InstrumentoMedicao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_SEL"), AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_VIGENCIA"), AV76TFContratoServicosIndicador_Vigencia) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_VIGENCIA_SEL"), AV77TFContratoServicosIndicador_Vigencia_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS"), ",", ".") != Convert.ToDecimal( AV80TFContratoServicosIndicador_QtdeFaixas )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS_TO"), ",", ".") != Convert.ToDecimal( AV81TFContratoServicosIndicador_QtdeFaixas_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E35JG2 */
         E35JG2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E35JG2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "CONTRATOSERVICOSINDICADOR_INDICADOR";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "CONTRATOSERVICOSINDICADOR_INDICADOR";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "CONTRATOSERVICOSINDICADOR_INDICADOR";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfcontratoservicosindicador_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosindicador_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosindicador_codigo_Visible), 5, 0)));
         edtavTfcontratoservicosindicador_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosindicador_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosindicador_codigo_to_Visible), 5, 0)));
         edtavTfcontratoservicosindicador_cntsrvcod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosindicador_cntsrvcod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosindicador_cntsrvcod_Visible), 5, 0)));
         edtavTfcontratoservicosindicador_cntsrvcod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosindicador_cntsrvcod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosindicador_cntsrvcod_to_Visible), 5, 0)));
         edtavTfcontratoservicosindicador_contratocod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosindicador_contratocod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosindicador_contratocod_Visible), 5, 0)));
         edtavTfcontratoservicosindicador_contratocod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosindicador_contratocod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosindicador_contratocod_to_Visible), 5, 0)));
         edtavTfcontratoservicosindicador_areatrabalhocod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosindicador_areatrabalhocod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosindicador_areatrabalhocod_Visible), 5, 0)));
         edtavTfcontratoservicosindicador_areatrabalhocod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosindicador_areatrabalhocod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosindicador_areatrabalhocod_to_Visible), 5, 0)));
         edtavTfcontratoservicosindicador_numero_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosindicador_numero_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosindicador_numero_Visible), 5, 0)));
         edtavTfcontratoservicosindicador_numero_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosindicador_numero_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosindicador_numero_to_Visible), 5, 0)));
         edtavTfcontratoservicosindicador_indicador_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosindicador_indicador_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosindicador_indicador_Visible), 5, 0)));
         edtavTfcontratoservicosindicador_indicador_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosindicador_indicador_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosindicador_indicador_sel_Visible), 5, 0)));
         edtavTfcontratoservicosindicador_finalidade_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosindicador_finalidade_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosindicador_finalidade_Visible), 5, 0)));
         edtavTfcontratoservicosindicador_finalidade_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosindicador_finalidade_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosindicador_finalidade_sel_Visible), 5, 0)));
         edtavTfcontratoservicosindicador_meta_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosindicador_meta_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosindicador_meta_Visible), 5, 0)));
         edtavTfcontratoservicosindicador_meta_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosindicador_meta_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosindicador_meta_sel_Visible), 5, 0)));
         edtavTfcontratoservicosindicador_instrumentomedicao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosindicador_instrumentomedicao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosindicador_instrumentomedicao_Visible), 5, 0)));
         edtavTfcontratoservicosindicador_instrumentomedicao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosindicador_instrumentomedicao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosindicador_instrumentomedicao_sel_Visible), 5, 0)));
         edtavTfcontratoservicosindicador_vigencia_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosindicador_vigencia_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosindicador_vigencia_Visible), 5, 0)));
         edtavTfcontratoservicosindicador_vigencia_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosindicador_vigencia_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosindicador_vigencia_sel_Visible), 5, 0)));
         edtavTfcontratoservicosindicador_qtdefaixas_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosindicador_qtdefaixas_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosindicador_qtdefaixas_Visible), 5, 0)));
         edtavTfcontratoservicosindicador_qtdefaixas_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosindicador_qtdefaixas_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosindicador_qtdefaixas_to_Visible), 5, 0)));
         Ddo_contratoservicosindicador_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosIndicador_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_codigo_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosindicador_codigo_Titlecontrolidtoreplace);
         AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace = Ddo_contratoservicosindicador_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace", AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace);
         edtavDdo_contratoservicosindicador_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoservicosindicador_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosindicador_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicosindicador_cntsrvcod_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosIndicador_CntSrvCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_cntsrvcod_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosindicador_cntsrvcod_Titlecontrolidtoreplace);
         AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace = Ddo_contratoservicosindicador_cntsrvcod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace", AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace);
         edtavDdo_contratoservicosindicador_cntsrvcodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoservicosindicador_cntsrvcodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosindicador_cntsrvcodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicosindicador_contratocod_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosIndicador_ContratoCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_contratocod_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosindicador_contratocod_Titlecontrolidtoreplace);
         AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace = Ddo_contratoservicosindicador_contratocod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace", AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace);
         edtavDdo_contratoservicosindicador_contratocodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoservicosindicador_contratocodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosindicador_contratocodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicosindicador_areatrabalhocod_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosIndicador_AreaTrabalhoCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_areatrabalhocod_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosindicador_areatrabalhocod_Titlecontrolidtoreplace);
         AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace = Ddo_contratoservicosindicador_areatrabalhocod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace", AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace);
         edtavDdo_contratoservicosindicador_areatrabalhocodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoservicosindicador_areatrabalhocodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosindicador_areatrabalhocodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicosindicador_numero_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosIndicador_Numero";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_numero_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosindicador_numero_Titlecontrolidtoreplace);
         AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace = Ddo_contratoservicosindicador_numero_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace", AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace);
         edtavDdo_contratoservicosindicador_numerotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoservicosindicador_numerotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosindicador_numerotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicosindicador_indicador_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosIndicador_Indicador";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_indicador_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosindicador_indicador_Titlecontrolidtoreplace);
         AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace = Ddo_contratoservicosindicador_indicador_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace", AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace);
         edtavDdo_contratoservicosindicador_indicadortitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoservicosindicador_indicadortitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosindicador_indicadortitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicosindicador_finalidade_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosIndicador_Finalidade";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_finalidade_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosindicador_finalidade_Titlecontrolidtoreplace);
         AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace = Ddo_contratoservicosindicador_finalidade_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace", AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace);
         edtavDdo_contratoservicosindicador_finalidadetitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoservicosindicador_finalidadetitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosindicador_finalidadetitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicosindicador_meta_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosIndicador_Meta";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_meta_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosindicador_meta_Titlecontrolidtoreplace);
         AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace = Ddo_contratoservicosindicador_meta_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace", AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace);
         edtavDdo_contratoservicosindicador_metatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoservicosindicador_metatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosindicador_metatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicosindicador_instrumentomedicao_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosIndicador_InstrumentoMedicao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_instrumentomedicao_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosindicador_instrumentomedicao_Titlecontrolidtoreplace);
         AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace = Ddo_contratoservicosindicador_instrumentomedicao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace", AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace);
         edtavDdo_contratoservicosindicador_instrumentomedicaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoservicosindicador_instrumentomedicaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosindicador_instrumentomedicaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicosindicador_tipo_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosIndicador_Tipo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_tipo_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosindicador_tipo_Titlecontrolidtoreplace);
         AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace = Ddo_contratoservicosindicador_tipo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace", AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace);
         edtavDdo_contratoservicosindicador_tipotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoservicosindicador_tipotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosindicador_tipotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicosindicador_periodicidade_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosIndicador_Periodicidade";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_periodicidade_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosindicador_periodicidade_Titlecontrolidtoreplace);
         AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace = Ddo_contratoservicosindicador_periodicidade_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace", AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace);
         edtavDdo_contratoservicosindicador_periodicidadetitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoservicosindicador_periodicidadetitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosindicador_periodicidadetitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicosindicador_vigencia_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosIndicador_Vigencia";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_vigencia_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosindicador_vigencia_Titlecontrolidtoreplace);
         AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace = Ddo_contratoservicosindicador_vigencia_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace", AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace);
         edtavDdo_contratoservicosindicador_vigenciatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoservicosindicador_vigenciatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosindicador_vigenciatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicosindicador_qtdefaixas_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosIndicador_QtdeFaixas";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_qtdefaixas_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosindicador_qtdefaixas_Titlecontrolidtoreplace);
         AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace = Ddo_contratoservicosindicador_qtdefaixas_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace", AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace);
         edtavDdo_contratoservicosindicador_qtdefaixastitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoservicosindicador_qtdefaixastitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosindicador_qtdefaixastitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione Indicador";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Codigo", 0);
         cmbavOrderedby.addItem("2", "servi�o", 0);
         cmbavOrderedby.addItem("3", "Contrato", 0);
         cmbavOrderedby.addItem("4", "de Trabalho", 0);
         cmbavOrderedby.addItem("5", "N�mero", 0);
         cmbavOrderedby.addItem("6", "Indicador", 0);
         cmbavOrderedby.addItem("7", "Finalidade", 0);
         cmbavOrderedby.addItem("8", "a cumprir", 0);
         cmbavOrderedby.addItem("9", "de Medi��o", 0);
         cmbavOrderedby.addItem("10", "de Indicador", 0);
         cmbavOrderedby.addItem("11", "Periodicidade", 0);
         cmbavOrderedby.addItem("12", "Vig�ncia", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV83DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV83DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E36JG2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV31ContratoServicosIndicador_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV35ContratoServicosIndicador_CntSrvCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV39ContratoServicosIndicador_ContratoCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV43ContratoServicosIndicador_AreaTrabalhoCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV47ContratoServicosIndicador_NumeroTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV51ContratoServicosIndicador_IndicadorTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV55ContratoServicosIndicador_FinalidadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV59ContratoServicosIndicador_MetaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV63ContratoServicosIndicador_InstrumentoMedicaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV67ContratoServicosIndicador_TipoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV71ContratoServicosIndicador_PeriodicidadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV75ContratoServicosIndicador_VigenciaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV79ContratoServicosIndicador_QtdeFaixasTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContratoServicosIndicador_Codigo_Titleformat = 2;
         edtContratoServicosIndicador_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Codigo", AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicador_Codigo_Internalname, "Title", edtContratoServicosIndicador_Codigo_Title);
         edtContratoServicosIndicador_CntSrvCod_Titleformat = 2;
         edtContratoServicosIndicador_CntSrvCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "servi�o", AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicador_CntSrvCod_Internalname, "Title", edtContratoServicosIndicador_CntSrvCod_Title);
         edtContratoServicosIndicador_ContratoCod_Titleformat = 2;
         edtContratoServicosIndicador_ContratoCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Contrato", AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicador_ContratoCod_Internalname, "Title", edtContratoServicosIndicador_ContratoCod_Title);
         edtContratoServicosIndicador_AreaTrabalhoCod_Titleformat = 2;
         edtContratoServicosIndicador_AreaTrabalhoCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "de Trabalho", AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicador_AreaTrabalhoCod_Internalname, "Title", edtContratoServicosIndicador_AreaTrabalhoCod_Title);
         edtContratoServicosIndicador_Numero_Titleformat = 2;
         edtContratoServicosIndicador_Numero_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "N�mero", AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicador_Numero_Internalname, "Title", edtContratoServicosIndicador_Numero_Title);
         edtContratoServicosIndicador_Indicador_Titleformat = 2;
         edtContratoServicosIndicador_Indicador_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Indicador", AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicador_Indicador_Internalname, "Title", edtContratoServicosIndicador_Indicador_Title);
         edtContratoServicosIndicador_Finalidade_Titleformat = 2;
         edtContratoServicosIndicador_Finalidade_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Finalidade", AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicador_Finalidade_Internalname, "Title", edtContratoServicosIndicador_Finalidade_Title);
         edtContratoServicosIndicador_Meta_Titleformat = 2;
         edtContratoServicosIndicador_Meta_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "a cumprir", AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicador_Meta_Internalname, "Title", edtContratoServicosIndicador_Meta_Title);
         edtContratoServicosIndicador_InstrumentoMedicao_Titleformat = 2;
         edtContratoServicosIndicador_InstrumentoMedicao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "de Medi��o", AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicador_InstrumentoMedicao_Internalname, "Title", edtContratoServicosIndicador_InstrumentoMedicao_Title);
         cmbContratoServicosIndicador_Tipo_Titleformat = 2;
         cmbContratoServicosIndicador_Tipo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "de Indicador", AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicosIndicador_Tipo_Internalname, "Title", cmbContratoServicosIndicador_Tipo.Title.Text);
         cmbContratoServicosIndicador_Periodicidade_Titleformat = 2;
         cmbContratoServicosIndicador_Periodicidade.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Periodicidade", AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicosIndicador_Periodicidade_Internalname, "Title", cmbContratoServicosIndicador_Periodicidade.Title.Text);
         edtContratoServicosIndicador_Vigencia_Titleformat = 2;
         edtContratoServicosIndicador_Vigencia_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Vig�ncia", AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicador_Vigencia_Internalname, "Title", edtContratoServicosIndicador_Vigencia_Title);
         edtContratoServicosIndicador_QtdeFaixas_Titleformat = 2;
         edtContratoServicosIndicador_QtdeFaixas_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Faixas", AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicador_QtdeFaixas_Internalname, "Title", edtContratoServicosIndicador_QtdeFaixas_Title);
         AV85GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV85GridCurrentPage), 10, 0)));
         AV86GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV86GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV31ContratoServicosIndicador_CodigoTitleFilterData", AV31ContratoServicosIndicador_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35ContratoServicosIndicador_CntSrvCodTitleFilterData", AV35ContratoServicosIndicador_CntSrvCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV39ContratoServicosIndicador_ContratoCodTitleFilterData", AV39ContratoServicosIndicador_ContratoCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV43ContratoServicosIndicador_AreaTrabalhoCodTitleFilterData", AV43ContratoServicosIndicador_AreaTrabalhoCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV47ContratoServicosIndicador_NumeroTitleFilterData", AV47ContratoServicosIndicador_NumeroTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV51ContratoServicosIndicador_IndicadorTitleFilterData", AV51ContratoServicosIndicador_IndicadorTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV55ContratoServicosIndicador_FinalidadeTitleFilterData", AV55ContratoServicosIndicador_FinalidadeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV59ContratoServicosIndicador_MetaTitleFilterData", AV59ContratoServicosIndicador_MetaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV63ContratoServicosIndicador_InstrumentoMedicaoTitleFilterData", AV63ContratoServicosIndicador_InstrumentoMedicaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV67ContratoServicosIndicador_TipoTitleFilterData", AV67ContratoServicosIndicador_TipoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV71ContratoServicosIndicador_PeriodicidadeTitleFilterData", AV71ContratoServicosIndicador_PeriodicidadeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV75ContratoServicosIndicador_VigenciaTitleFilterData", AV75ContratoServicosIndicador_VigenciaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV79ContratoServicosIndicador_QtdeFaixasTitleFilterData", AV79ContratoServicosIndicador_QtdeFaixasTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11JG2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV84PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV84PageToGo) ;
         }
      }

      protected void E12JG2( )
      {
         /* Ddo_contratoservicosindicador_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosindicador_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_codigo_Internalname, "SortedStatus", Ddo_contratoservicosindicador_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosindicador_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_codigo_Internalname, "SortedStatus", Ddo_contratoservicosindicador_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV32TFContratoServicosIndicador_Codigo = (int)(NumberUtil.Val( Ddo_contratoservicosindicador_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFContratoServicosIndicador_Codigo), 6, 0)));
            AV33TFContratoServicosIndicador_Codigo_To = (int)(NumberUtil.Val( Ddo_contratoservicosindicador_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TFContratoServicosIndicador_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33TFContratoServicosIndicador_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13JG2( )
      {
         /* Ddo_contratoservicosindicador_cntsrvcod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_cntsrvcod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosindicador_cntsrvcod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_cntsrvcod_Internalname, "SortedStatus", Ddo_contratoservicosindicador_cntsrvcod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_cntsrvcod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosindicador_cntsrvcod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_cntsrvcod_Internalname, "SortedStatus", Ddo_contratoservicosindicador_cntsrvcod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_cntsrvcod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV36TFContratoServicosIndicador_CntSrvCod = (int)(NumberUtil.Val( Ddo_contratoservicosindicador_cntsrvcod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContratoServicosIndicador_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFContratoServicosIndicador_CntSrvCod), 6, 0)));
            AV37TFContratoServicosIndicador_CntSrvCod_To = (int)(NumberUtil.Val( Ddo_contratoservicosindicador_cntsrvcod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFContratoServicosIndicador_CntSrvCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37TFContratoServicosIndicador_CntSrvCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14JG2( )
      {
         /* Ddo_contratoservicosindicador_contratocod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_contratocod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosindicador_contratocod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_contratocod_Internalname, "SortedStatus", Ddo_contratoservicosindicador_contratocod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_contratocod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosindicador_contratocod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_contratocod_Internalname, "SortedStatus", Ddo_contratoservicosindicador_contratocod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_contratocod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV40TFContratoServicosIndicador_ContratoCod = (int)(NumberUtil.Val( Ddo_contratoservicosindicador_contratocod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContratoServicosIndicador_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40TFContratoServicosIndicador_ContratoCod), 6, 0)));
            AV41TFContratoServicosIndicador_ContratoCod_To = (int)(NumberUtil.Val( Ddo_contratoservicosindicador_contratocod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFContratoServicosIndicador_ContratoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41TFContratoServicosIndicador_ContratoCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E15JG2( )
      {
         /* Ddo_contratoservicosindicador_areatrabalhocod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_areatrabalhocod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosindicador_areatrabalhocod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_areatrabalhocod_Internalname, "SortedStatus", Ddo_contratoservicosindicador_areatrabalhocod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_areatrabalhocod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosindicador_areatrabalhocod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_areatrabalhocod_Internalname, "SortedStatus", Ddo_contratoservicosindicador_areatrabalhocod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_areatrabalhocod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV44TFContratoServicosIndicador_AreaTrabalhoCod = (int)(NumberUtil.Val( Ddo_contratoservicosindicador_areatrabalhocod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContratoServicosIndicador_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFContratoServicosIndicador_AreaTrabalhoCod), 6, 0)));
            AV45TFContratoServicosIndicador_AreaTrabalhoCod_To = (int)(NumberUtil.Val( Ddo_contratoservicosindicador_areatrabalhocod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContratoServicosIndicador_AreaTrabalhoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFContratoServicosIndicador_AreaTrabalhoCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E16JG2( )
      {
         /* Ddo_contratoservicosindicador_numero_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_numero_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosindicador_numero_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_numero_Internalname, "SortedStatus", Ddo_contratoservicosindicador_numero_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_numero_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosindicador_numero_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_numero_Internalname, "SortedStatus", Ddo_contratoservicosindicador_numero_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_numero_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV48TFContratoServicosIndicador_Numero = (short)(NumberUtil.Val( Ddo_contratoservicosindicador_numero_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContratoServicosIndicador_Numero", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48TFContratoServicosIndicador_Numero), 4, 0)));
            AV49TFContratoServicosIndicador_Numero_To = (short)(NumberUtil.Val( Ddo_contratoservicosindicador_numero_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContratoServicosIndicador_Numero_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49TFContratoServicosIndicador_Numero_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E17JG2( )
      {
         /* Ddo_contratoservicosindicador_indicador_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_indicador_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosindicador_indicador_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_indicador_Internalname, "SortedStatus", Ddo_contratoservicosindicador_indicador_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_indicador_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosindicador_indicador_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_indicador_Internalname, "SortedStatus", Ddo_contratoservicosindicador_indicador_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_indicador_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV52TFContratoServicosIndicador_Indicador = Ddo_contratoservicosindicador_indicador_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFContratoServicosIndicador_Indicador", AV52TFContratoServicosIndicador_Indicador);
            AV53TFContratoServicosIndicador_Indicador_Sel = Ddo_contratoservicosindicador_indicador_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContratoServicosIndicador_Indicador_Sel", AV53TFContratoServicosIndicador_Indicador_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E18JG2( )
      {
         /* Ddo_contratoservicosindicador_finalidade_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_finalidade_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosindicador_finalidade_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_finalidade_Internalname, "SortedStatus", Ddo_contratoservicosindicador_finalidade_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_finalidade_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosindicador_finalidade_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_finalidade_Internalname, "SortedStatus", Ddo_contratoservicosindicador_finalidade_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_finalidade_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV56TFContratoServicosIndicador_Finalidade = Ddo_contratoservicosindicador_finalidade_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFContratoServicosIndicador_Finalidade", AV56TFContratoServicosIndicador_Finalidade);
            AV57TFContratoServicosIndicador_Finalidade_Sel = Ddo_contratoservicosindicador_finalidade_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFContratoServicosIndicador_Finalidade_Sel", AV57TFContratoServicosIndicador_Finalidade_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E19JG2( )
      {
         /* Ddo_contratoservicosindicador_meta_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_meta_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosindicador_meta_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_meta_Internalname, "SortedStatus", Ddo_contratoservicosindicador_meta_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_meta_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosindicador_meta_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_meta_Internalname, "SortedStatus", Ddo_contratoservicosindicador_meta_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_meta_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV60TFContratoServicosIndicador_Meta = Ddo_contratoservicosindicador_meta_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContratoServicosIndicador_Meta", AV60TFContratoServicosIndicador_Meta);
            AV61TFContratoServicosIndicador_Meta_Sel = Ddo_contratoservicosindicador_meta_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFContratoServicosIndicador_Meta_Sel", AV61TFContratoServicosIndicador_Meta_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E20JG2( )
      {
         /* Ddo_contratoservicosindicador_instrumentomedicao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_instrumentomedicao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 9;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosindicador_instrumentomedicao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_instrumentomedicao_Internalname, "SortedStatus", Ddo_contratoservicosindicador_instrumentomedicao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_instrumentomedicao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 9;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosindicador_instrumentomedicao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_instrumentomedicao_Internalname, "SortedStatus", Ddo_contratoservicosindicador_instrumentomedicao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_instrumentomedicao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV64TFContratoServicosIndicador_InstrumentoMedicao = Ddo_contratoservicosindicador_instrumentomedicao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFContratoServicosIndicador_InstrumentoMedicao", AV64TFContratoServicosIndicador_InstrumentoMedicao);
            AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel = Ddo_contratoservicosindicador_instrumentomedicao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel", AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E21JG2( )
      {
         /* Ddo_contratoservicosindicador_tipo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_tipo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 10;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosindicador_tipo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_tipo_Internalname, "SortedStatus", Ddo_contratoservicosindicador_tipo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_tipo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 10;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosindicador_tipo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_tipo_Internalname, "SortedStatus", Ddo_contratoservicosindicador_tipo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_tipo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV68TFContratoServicosIndicador_Tipo_SelsJson = Ddo_contratoservicosindicador_tipo_Selectedvalue_get;
            AV69TFContratoServicosIndicador_Tipo_Sels.FromJSonString(AV68TFContratoServicosIndicador_Tipo_SelsJson);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV69TFContratoServicosIndicador_Tipo_Sels", AV69TFContratoServicosIndicador_Tipo_Sels);
      }

      protected void E22JG2( )
      {
         /* Ddo_contratoservicosindicador_periodicidade_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_periodicidade_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 11;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosindicador_periodicidade_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_periodicidade_Internalname, "SortedStatus", Ddo_contratoservicosindicador_periodicidade_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_periodicidade_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 11;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosindicador_periodicidade_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_periodicidade_Internalname, "SortedStatus", Ddo_contratoservicosindicador_periodicidade_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_periodicidade_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV72TFContratoServicosIndicador_Periodicidade_SelsJson = Ddo_contratoservicosindicador_periodicidade_Selectedvalue_get;
            AV73TFContratoServicosIndicador_Periodicidade_Sels.FromJSonString(AV72TFContratoServicosIndicador_Periodicidade_SelsJson);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV73TFContratoServicosIndicador_Periodicidade_Sels", AV73TFContratoServicosIndicador_Periodicidade_Sels);
      }

      protected void E23JG2( )
      {
         /* Ddo_contratoservicosindicador_vigencia_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_vigencia_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 12;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosindicador_vigencia_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_vigencia_Internalname, "SortedStatus", Ddo_contratoservicosindicador_vigencia_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_vigencia_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 12;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosindicador_vigencia_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_vigencia_Internalname, "SortedStatus", Ddo_contratoservicosindicador_vigencia_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_vigencia_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV76TFContratoServicosIndicador_Vigencia = Ddo_contratoservicosindicador_vigencia_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFContratoServicosIndicador_Vigencia", AV76TFContratoServicosIndicador_Vigencia);
            AV77TFContratoServicosIndicador_Vigencia_Sel = Ddo_contratoservicosindicador_vigencia_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFContratoServicosIndicador_Vigencia_Sel", AV77TFContratoServicosIndicador_Vigencia_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E24JG2( )
      {
         /* Ddo_contratoservicosindicador_qtdefaixas_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_qtdefaixas_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV80TFContratoServicosIndicador_QtdeFaixas = (short)(NumberUtil.Val( Ddo_contratoservicosindicador_qtdefaixas_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV80TFContratoServicosIndicador_QtdeFaixas), 4, 0)));
            AV81TFContratoServicosIndicador_QtdeFaixas_To = (short)(NumberUtil.Val( Ddo_contratoservicosindicador_qtdefaixas_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFContratoServicosIndicador_QtdeFaixas_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV81TFContratoServicosIndicador_QtdeFaixas_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
      }

      private void E37JG2( )
      {
         /* Grid_Load Routine */
         AV28Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV28Select);
         AV89Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 80;
         }
         sendrow_802( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_80_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(80, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E38JG2 */
         E38JG2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E38JG2( )
      {
         /* Enter Routine */
         AV7InOutContratoServicosIndicador_Codigo = A1269ContratoServicosIndicador_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContratoServicosIndicador_Codigo), 6, 0)));
         AV8InOutContratoServicosIndicador_Indicador = A1274ContratoServicosIndicador_Indicador;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContratoServicosIndicador_Indicador", AV8InOutContratoServicosIndicador_Indicador);
         context.setWebReturnParms(new Object[] {(int)AV7InOutContratoServicosIndicador_Codigo,(String)AV8InOutContratoServicosIndicador_Indicador});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E25JG2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E30JG2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E26JG2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoServicosIndicador_Indicador1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoServicosIndicador_Indicador2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoServicosIndicador_Indicador3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV32TFContratoServicosIndicador_Codigo, AV33TFContratoServicosIndicador_Codigo_To, AV36TFContratoServicosIndicador_CntSrvCod, AV37TFContratoServicosIndicador_CntSrvCod_To, AV40TFContratoServicosIndicador_ContratoCod, AV41TFContratoServicosIndicador_ContratoCod_To, AV44TFContratoServicosIndicador_AreaTrabalhoCod, AV45TFContratoServicosIndicador_AreaTrabalhoCod_To, AV48TFContratoServicosIndicador_Numero, AV49TFContratoServicosIndicador_Numero_To, AV52TFContratoServicosIndicador_Indicador, AV53TFContratoServicosIndicador_Indicador_Sel, AV56TFContratoServicosIndicador_Finalidade, AV57TFContratoServicosIndicador_Finalidade_Sel, AV60TFContratoServicosIndicador_Meta, AV61TFContratoServicosIndicador_Meta_Sel, AV64TFContratoServicosIndicador_InstrumentoMedicao, AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel, AV76TFContratoServicosIndicador_Vigencia, AV77TFContratoServicosIndicador_Vigencia_Sel, AV80TFContratoServicosIndicador_QtdeFaixas, AV81TFContratoServicosIndicador_QtdeFaixas_To, AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace, AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace, AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace, AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace, AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace, AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace, AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace, AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace, AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace, AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace, AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace, AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace, AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace, AV69TFContratoServicosIndicador_Tipo_Sels, AV73TFContratoServicosIndicador_Periodicidade_Sels, AV90Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E31JG2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E32JG2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E27JG2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoServicosIndicador_Indicador1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoServicosIndicador_Indicador2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoServicosIndicador_Indicador3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV32TFContratoServicosIndicador_Codigo, AV33TFContratoServicosIndicador_Codigo_To, AV36TFContratoServicosIndicador_CntSrvCod, AV37TFContratoServicosIndicador_CntSrvCod_To, AV40TFContratoServicosIndicador_ContratoCod, AV41TFContratoServicosIndicador_ContratoCod_To, AV44TFContratoServicosIndicador_AreaTrabalhoCod, AV45TFContratoServicosIndicador_AreaTrabalhoCod_To, AV48TFContratoServicosIndicador_Numero, AV49TFContratoServicosIndicador_Numero_To, AV52TFContratoServicosIndicador_Indicador, AV53TFContratoServicosIndicador_Indicador_Sel, AV56TFContratoServicosIndicador_Finalidade, AV57TFContratoServicosIndicador_Finalidade_Sel, AV60TFContratoServicosIndicador_Meta, AV61TFContratoServicosIndicador_Meta_Sel, AV64TFContratoServicosIndicador_InstrumentoMedicao, AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel, AV76TFContratoServicosIndicador_Vigencia, AV77TFContratoServicosIndicador_Vigencia_Sel, AV80TFContratoServicosIndicador_QtdeFaixas, AV81TFContratoServicosIndicador_QtdeFaixas_To, AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace, AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace, AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace, AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace, AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace, AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace, AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace, AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace, AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace, AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace, AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace, AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace, AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace, AV69TFContratoServicosIndicador_Tipo_Sels, AV73TFContratoServicosIndicador_Periodicidade_Sels, AV90Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E33JG2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E28JG2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoServicosIndicador_Indicador1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoServicosIndicador_Indicador2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoServicosIndicador_Indicador3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV32TFContratoServicosIndicador_Codigo, AV33TFContratoServicosIndicador_Codigo_To, AV36TFContratoServicosIndicador_CntSrvCod, AV37TFContratoServicosIndicador_CntSrvCod_To, AV40TFContratoServicosIndicador_ContratoCod, AV41TFContratoServicosIndicador_ContratoCod_To, AV44TFContratoServicosIndicador_AreaTrabalhoCod, AV45TFContratoServicosIndicador_AreaTrabalhoCod_To, AV48TFContratoServicosIndicador_Numero, AV49TFContratoServicosIndicador_Numero_To, AV52TFContratoServicosIndicador_Indicador, AV53TFContratoServicosIndicador_Indicador_Sel, AV56TFContratoServicosIndicador_Finalidade, AV57TFContratoServicosIndicador_Finalidade_Sel, AV60TFContratoServicosIndicador_Meta, AV61TFContratoServicosIndicador_Meta_Sel, AV64TFContratoServicosIndicador_InstrumentoMedicao, AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel, AV76TFContratoServicosIndicador_Vigencia, AV77TFContratoServicosIndicador_Vigencia_Sel, AV80TFContratoServicosIndicador_QtdeFaixas, AV81TFContratoServicosIndicador_QtdeFaixas_To, AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace, AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace, AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace, AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace, AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace, AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace, AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace, AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace, AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace, AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace, AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace, AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace, AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace, AV69TFContratoServicosIndicador_Tipo_Sels, AV73TFContratoServicosIndicador_Periodicidade_Sels, AV90Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E34JG2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E29JG2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV69TFContratoServicosIndicador_Tipo_Sels", AV69TFContratoServicosIndicador_Tipo_Sels);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV73TFContratoServicosIndicador_Periodicidade_Sels", AV73TFContratoServicosIndicador_Periodicidade_Sels);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contratoservicosindicador_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_codigo_Internalname, "SortedStatus", Ddo_contratoservicosindicador_codigo_Sortedstatus);
         Ddo_contratoservicosindicador_cntsrvcod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_cntsrvcod_Internalname, "SortedStatus", Ddo_contratoservicosindicador_cntsrvcod_Sortedstatus);
         Ddo_contratoservicosindicador_contratocod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_contratocod_Internalname, "SortedStatus", Ddo_contratoservicosindicador_contratocod_Sortedstatus);
         Ddo_contratoservicosindicador_areatrabalhocod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_areatrabalhocod_Internalname, "SortedStatus", Ddo_contratoservicosindicador_areatrabalhocod_Sortedstatus);
         Ddo_contratoservicosindicador_numero_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_numero_Internalname, "SortedStatus", Ddo_contratoservicosindicador_numero_Sortedstatus);
         Ddo_contratoservicosindicador_indicador_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_indicador_Internalname, "SortedStatus", Ddo_contratoservicosindicador_indicador_Sortedstatus);
         Ddo_contratoservicosindicador_finalidade_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_finalidade_Internalname, "SortedStatus", Ddo_contratoservicosindicador_finalidade_Sortedstatus);
         Ddo_contratoservicosindicador_meta_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_meta_Internalname, "SortedStatus", Ddo_contratoservicosindicador_meta_Sortedstatus);
         Ddo_contratoservicosindicador_instrumentomedicao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_instrumentomedicao_Internalname, "SortedStatus", Ddo_contratoservicosindicador_instrumentomedicao_Sortedstatus);
         Ddo_contratoservicosindicador_tipo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_tipo_Internalname, "SortedStatus", Ddo_contratoservicosindicador_tipo_Sortedstatus);
         Ddo_contratoservicosindicador_periodicidade_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_periodicidade_Internalname, "SortedStatus", Ddo_contratoservicosindicador_periodicidade_Sortedstatus);
         Ddo_contratoservicosindicador_vigencia_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_vigencia_Internalname, "SortedStatus", Ddo_contratoservicosindicador_vigencia_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_contratoservicosindicador_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_codigo_Internalname, "SortedStatus", Ddo_contratoservicosindicador_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_contratoservicosindicador_cntsrvcod_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_cntsrvcod_Internalname, "SortedStatus", Ddo_contratoservicosindicador_cntsrvcod_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_contratoservicosindicador_contratocod_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_contratocod_Internalname, "SortedStatus", Ddo_contratoservicosindicador_contratocod_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_contratoservicosindicador_areatrabalhocod_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_areatrabalhocod_Internalname, "SortedStatus", Ddo_contratoservicosindicador_areatrabalhocod_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_contratoservicosindicador_numero_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_numero_Internalname, "SortedStatus", Ddo_contratoservicosindicador_numero_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_contratoservicosindicador_indicador_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_indicador_Internalname, "SortedStatus", Ddo_contratoservicosindicador_indicador_Sortedstatus);
         }
         else if ( AV13OrderedBy == 7 )
         {
            Ddo_contratoservicosindicador_finalidade_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_finalidade_Internalname, "SortedStatus", Ddo_contratoservicosindicador_finalidade_Sortedstatus);
         }
         else if ( AV13OrderedBy == 8 )
         {
            Ddo_contratoservicosindicador_meta_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_meta_Internalname, "SortedStatus", Ddo_contratoservicosindicador_meta_Sortedstatus);
         }
         else if ( AV13OrderedBy == 9 )
         {
            Ddo_contratoservicosindicador_instrumentomedicao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_instrumentomedicao_Internalname, "SortedStatus", Ddo_contratoservicosindicador_instrumentomedicao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 10 )
         {
            Ddo_contratoservicosindicador_tipo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_tipo_Internalname, "SortedStatus", Ddo_contratoservicosindicador_tipo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 11 )
         {
            Ddo_contratoservicosindicador_periodicidade_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_periodicidade_Internalname, "SortedStatus", Ddo_contratoservicosindicador_periodicidade_Sortedstatus);
         }
         else if ( AV13OrderedBy == 12 )
         {
            Ddo_contratoservicosindicador_vigencia_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_vigencia_Internalname, "SortedStatus", Ddo_contratoservicosindicador_vigencia_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavContratoservicosindicador_indicador1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicosindicador_indicador1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicosindicador_indicador1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 )
         {
            edtavContratoservicosindicador_indicador1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicosindicador_indicador1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicosindicador_indicador1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavContratoservicosindicador_indicador2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicosindicador_indicador2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicosindicador_indicador2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 )
         {
            edtavContratoservicosindicador_indicador2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicosindicador_indicador2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicosindicador_indicador2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavContratoservicosindicador_indicador3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicosindicador_indicador3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicosindicador_indicador3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 )
         {
            edtavContratoservicosindicador_indicador3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicosindicador_indicador3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicosindicador_indicador3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "CONTRATOSERVICOSINDICADOR_INDICADOR";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV20DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         AV21ContratoServicosIndicador_Indicador2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoServicosIndicador_Indicador2", AV21ContratoServicosIndicador_Indicador2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "CONTRATOSERVICOSINDICADOR_INDICADOR";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV24DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         AV25ContratoServicosIndicador_Indicador3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoServicosIndicador_Indicador3", AV25ContratoServicosIndicador_Indicador3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'CLEANFILTERS' Routine */
         AV32TFContratoServicosIndicador_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFContratoServicosIndicador_Codigo), 6, 0)));
         Ddo_contratoservicosindicador_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_codigo_Internalname, "FilteredText_set", Ddo_contratoservicosindicador_codigo_Filteredtext_set);
         AV33TFContratoServicosIndicador_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TFContratoServicosIndicador_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33TFContratoServicosIndicador_Codigo_To), 6, 0)));
         Ddo_contratoservicosindicador_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_codigo_Internalname, "FilteredTextTo_set", Ddo_contratoservicosindicador_codigo_Filteredtextto_set);
         AV36TFContratoServicosIndicador_CntSrvCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContratoServicosIndicador_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFContratoServicosIndicador_CntSrvCod), 6, 0)));
         Ddo_contratoservicosindicador_cntsrvcod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_cntsrvcod_Internalname, "FilteredText_set", Ddo_contratoservicosindicador_cntsrvcod_Filteredtext_set);
         AV37TFContratoServicosIndicador_CntSrvCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFContratoServicosIndicador_CntSrvCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37TFContratoServicosIndicador_CntSrvCod_To), 6, 0)));
         Ddo_contratoservicosindicador_cntsrvcod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_cntsrvcod_Internalname, "FilteredTextTo_set", Ddo_contratoservicosindicador_cntsrvcod_Filteredtextto_set);
         AV40TFContratoServicosIndicador_ContratoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContratoServicosIndicador_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40TFContratoServicosIndicador_ContratoCod), 6, 0)));
         Ddo_contratoservicosindicador_contratocod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_contratocod_Internalname, "FilteredText_set", Ddo_contratoservicosindicador_contratocod_Filteredtext_set);
         AV41TFContratoServicosIndicador_ContratoCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFContratoServicosIndicador_ContratoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41TFContratoServicosIndicador_ContratoCod_To), 6, 0)));
         Ddo_contratoservicosindicador_contratocod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_contratocod_Internalname, "FilteredTextTo_set", Ddo_contratoservicosindicador_contratocod_Filteredtextto_set);
         AV44TFContratoServicosIndicador_AreaTrabalhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContratoServicosIndicador_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFContratoServicosIndicador_AreaTrabalhoCod), 6, 0)));
         Ddo_contratoservicosindicador_areatrabalhocod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_areatrabalhocod_Internalname, "FilteredText_set", Ddo_contratoservicosindicador_areatrabalhocod_Filteredtext_set);
         AV45TFContratoServicosIndicador_AreaTrabalhoCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContratoServicosIndicador_AreaTrabalhoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFContratoServicosIndicador_AreaTrabalhoCod_To), 6, 0)));
         Ddo_contratoservicosindicador_areatrabalhocod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_areatrabalhocod_Internalname, "FilteredTextTo_set", Ddo_contratoservicosindicador_areatrabalhocod_Filteredtextto_set);
         AV48TFContratoServicosIndicador_Numero = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContratoServicosIndicador_Numero", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48TFContratoServicosIndicador_Numero), 4, 0)));
         Ddo_contratoservicosindicador_numero_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_numero_Internalname, "FilteredText_set", Ddo_contratoservicosindicador_numero_Filteredtext_set);
         AV49TFContratoServicosIndicador_Numero_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContratoServicosIndicador_Numero_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49TFContratoServicosIndicador_Numero_To), 4, 0)));
         Ddo_contratoservicosindicador_numero_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_numero_Internalname, "FilteredTextTo_set", Ddo_contratoservicosindicador_numero_Filteredtextto_set);
         AV52TFContratoServicosIndicador_Indicador = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFContratoServicosIndicador_Indicador", AV52TFContratoServicosIndicador_Indicador);
         Ddo_contratoservicosindicador_indicador_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_indicador_Internalname, "FilteredText_set", Ddo_contratoservicosindicador_indicador_Filteredtext_set);
         AV53TFContratoServicosIndicador_Indicador_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContratoServicosIndicador_Indicador_Sel", AV53TFContratoServicosIndicador_Indicador_Sel);
         Ddo_contratoservicosindicador_indicador_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_indicador_Internalname, "SelectedValue_set", Ddo_contratoservicosindicador_indicador_Selectedvalue_set);
         AV56TFContratoServicosIndicador_Finalidade = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFContratoServicosIndicador_Finalidade", AV56TFContratoServicosIndicador_Finalidade);
         Ddo_contratoservicosindicador_finalidade_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_finalidade_Internalname, "FilteredText_set", Ddo_contratoservicosindicador_finalidade_Filteredtext_set);
         AV57TFContratoServicosIndicador_Finalidade_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFContratoServicosIndicador_Finalidade_Sel", AV57TFContratoServicosIndicador_Finalidade_Sel);
         Ddo_contratoservicosindicador_finalidade_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_finalidade_Internalname, "SelectedValue_set", Ddo_contratoservicosindicador_finalidade_Selectedvalue_set);
         AV60TFContratoServicosIndicador_Meta = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContratoServicosIndicador_Meta", AV60TFContratoServicosIndicador_Meta);
         Ddo_contratoservicosindicador_meta_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_meta_Internalname, "FilteredText_set", Ddo_contratoservicosindicador_meta_Filteredtext_set);
         AV61TFContratoServicosIndicador_Meta_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFContratoServicosIndicador_Meta_Sel", AV61TFContratoServicosIndicador_Meta_Sel);
         Ddo_contratoservicosindicador_meta_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_meta_Internalname, "SelectedValue_set", Ddo_contratoservicosindicador_meta_Selectedvalue_set);
         AV64TFContratoServicosIndicador_InstrumentoMedicao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFContratoServicosIndicador_InstrumentoMedicao", AV64TFContratoServicosIndicador_InstrumentoMedicao);
         Ddo_contratoservicosindicador_instrumentomedicao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_instrumentomedicao_Internalname, "FilteredText_set", Ddo_contratoservicosindicador_instrumentomedicao_Filteredtext_set);
         AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel", AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel);
         Ddo_contratoservicosindicador_instrumentomedicao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_instrumentomedicao_Internalname, "SelectedValue_set", Ddo_contratoservicosindicador_instrumentomedicao_Selectedvalue_set);
         AV69TFContratoServicosIndicador_Tipo_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_contratoservicosindicador_tipo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_tipo_Internalname, "SelectedValue_set", Ddo_contratoservicosindicador_tipo_Selectedvalue_set);
         AV73TFContratoServicosIndicador_Periodicidade_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_contratoservicosindicador_periodicidade_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_periodicidade_Internalname, "SelectedValue_set", Ddo_contratoservicosindicador_periodicidade_Selectedvalue_set);
         AV76TFContratoServicosIndicador_Vigencia = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFContratoServicosIndicador_Vigencia", AV76TFContratoServicosIndicador_Vigencia);
         Ddo_contratoservicosindicador_vigencia_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_vigencia_Internalname, "FilteredText_set", Ddo_contratoservicosindicador_vigencia_Filteredtext_set);
         AV77TFContratoServicosIndicador_Vigencia_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFContratoServicosIndicador_Vigencia_Sel", AV77TFContratoServicosIndicador_Vigencia_Sel);
         Ddo_contratoservicosindicador_vigencia_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_vigencia_Internalname, "SelectedValue_set", Ddo_contratoservicosindicador_vigencia_Selectedvalue_set);
         AV80TFContratoServicosIndicador_QtdeFaixas = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV80TFContratoServicosIndicador_QtdeFaixas), 4, 0)));
         Ddo_contratoservicosindicador_qtdefaixas_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_qtdefaixas_Internalname, "FilteredText_set", Ddo_contratoservicosindicador_qtdefaixas_Filteredtext_set);
         AV81TFContratoServicosIndicador_QtdeFaixas_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFContratoServicosIndicador_QtdeFaixas_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV81TFContratoServicosIndicador_QtdeFaixas_To), 4, 0)));
         Ddo_contratoservicosindicador_qtdefaixas_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosindicador_qtdefaixas_Internalname, "FilteredTextTo_set", Ddo_contratoservicosindicador_qtdefaixas_Filteredtextto_set);
         AV15DynamicFiltersSelector1 = "CONTRATOSERVICOSINDICADOR_INDICADOR";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         AV17ContratoServicosIndicador_Indicador1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoServicosIndicador_Indicador1", AV17ContratoServicosIndicador_Indicador1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17ContratoServicosIndicador_Indicador1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoServicosIndicador_Indicador1", AV17ContratoServicosIndicador_Indicador1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 )
               {
                  AV20DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
                  AV21ContratoServicosIndicador_Indicador2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoServicosIndicador_Indicador2", AV21ContratoServicosIndicador_Indicador2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 )
                  {
                     AV24DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
                     AV25ContratoServicosIndicador_Indicador3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoServicosIndicador_Indicador3", AV25ContratoServicosIndicador_Indicador3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (0==AV32TFContratoServicosIndicador_Codigo) && (0==AV33TFContratoServicosIndicador_Codigo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSINDICADOR_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV32TFContratoServicosIndicador_Codigo), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV33TFContratoServicosIndicador_Codigo_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV36TFContratoServicosIndicador_CntSrvCod) && (0==AV37TFContratoServicosIndicador_CntSrvCod_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSINDICADOR_CNTSRVCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV36TFContratoServicosIndicador_CntSrvCod), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV37TFContratoServicosIndicador_CntSrvCod_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV40TFContratoServicosIndicador_ContratoCod) && (0==AV41TFContratoServicosIndicador_ContratoCod_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSINDICADOR_CONTRATOCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV40TFContratoServicosIndicador_ContratoCod), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV41TFContratoServicosIndicador_ContratoCod_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV44TFContratoServicosIndicador_AreaTrabalhoCod) && (0==AV45TFContratoServicosIndicador_AreaTrabalhoCod_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV44TFContratoServicosIndicador_AreaTrabalhoCod), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV45TFContratoServicosIndicador_AreaTrabalhoCod_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV48TFContratoServicosIndicador_Numero) && (0==AV49TFContratoServicosIndicador_Numero_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSINDICADOR_NUMERO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV48TFContratoServicosIndicador_Numero), 4, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV49TFContratoServicosIndicador_Numero_To), 4, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52TFContratoServicosIndicador_Indicador)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSINDICADOR_INDICADOR";
            AV11GridStateFilterValue.gxTpr_Value = AV52TFContratoServicosIndicador_Indicador;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53TFContratoServicosIndicador_Indicador_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSINDICADOR_INDICADOR_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV53TFContratoServicosIndicador_Indicador_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56TFContratoServicosIndicador_Finalidade)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSINDICADOR_FINALIDADE";
            AV11GridStateFilterValue.gxTpr_Value = AV56TFContratoServicosIndicador_Finalidade;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57TFContratoServicosIndicador_Finalidade_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSINDICADOR_FINALIDADE_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV57TFContratoServicosIndicador_Finalidade_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFContratoServicosIndicador_Meta)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSINDICADOR_META";
            AV11GridStateFilterValue.gxTpr_Value = AV60TFContratoServicosIndicador_Meta;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61TFContratoServicosIndicador_Meta_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSINDICADOR_META_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV61TFContratoServicosIndicador_Meta_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64TFContratoServicosIndicador_InstrumentoMedicao)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO";
            AV11GridStateFilterValue.gxTpr_Value = AV64TFContratoServicosIndicador_InstrumentoMedicao;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV69TFContratoServicosIndicador_Tipo_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSINDICADOR_TIPO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV69TFContratoServicosIndicador_Tipo_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV73TFContratoServicosIndicador_Periodicidade_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSINDICADOR_PERIODICIDADE_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV73TFContratoServicosIndicador_Periodicidade_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76TFContratoServicosIndicador_Vigencia)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSINDICADOR_VIGENCIA";
            AV11GridStateFilterValue.gxTpr_Value = AV76TFContratoServicosIndicador_Vigencia;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77TFContratoServicosIndicador_Vigencia_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSINDICADOR_VIGENCIA_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV77TFContratoServicosIndicador_Vigencia_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV80TFContratoServicosIndicador_QtdeFaixas) && (0==AV81TFContratoServicosIndicador_QtdeFaixas_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV80TFContratoServicosIndicador_QtdeFaixas), 4, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV81TFContratoServicosIndicador_QtdeFaixas_To), 4, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV90Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17ContratoServicosIndicador_Indicador1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17ContratoServicosIndicador_Indicador1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV21ContratoServicosIndicador_Indicador2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV21ContratoServicosIndicador_Indicador2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV20DynamicFiltersOperator2;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ContratoServicosIndicador_Indicador3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV25ContratoServicosIndicador_Indicador3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV24DynamicFiltersOperator3;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_JG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_JG2( true) ;
         }
         else
         {
            wb_table2_5_JG2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_JG2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_74_JG2( true) ;
         }
         else
         {
            wb_table3_74_JG2( false) ;
         }
         return  ;
      }

      protected void wb_table3_74_JG2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_JG2e( true) ;
         }
         else
         {
            wb_table1_2_JG2e( false) ;
         }
      }

      protected void wb_table3_74_JG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_77_JG2( true) ;
         }
         else
         {
            wb_table4_77_JG2( false) ;
         }
         return  ;
      }

      protected void wb_table4_77_JG2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_74_JG2e( true) ;
         }
         else
         {
            wb_table3_74_JG2e( false) ;
         }
      }

      protected void wb_table4_77_JG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"80\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosIndicador_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosIndicador_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosIndicador_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosIndicador_CntSrvCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosIndicador_CntSrvCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosIndicador_CntSrvCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosIndicador_ContratoCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosIndicador_ContratoCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosIndicador_ContratoCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosIndicador_AreaTrabalhoCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosIndicador_AreaTrabalhoCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosIndicador_AreaTrabalhoCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(40), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosIndicador_Numero_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosIndicador_Numero_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosIndicador_Numero_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(410), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosIndicador_Indicador_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosIndicador_Indicador_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosIndicador_Indicador_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(410), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosIndicador_Finalidade_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosIndicador_Finalidade_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosIndicador_Finalidade_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(410), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosIndicador_Meta_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosIndicador_Meta_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosIndicador_Meta_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(410), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosIndicador_InstrumentoMedicao_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosIndicador_InstrumentoMedicao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosIndicador_InstrumentoMedicao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContratoServicosIndicador_Tipo_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContratoServicosIndicador_Tipo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContratoServicosIndicador_Tipo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContratoServicosIndicador_Periodicidade_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContratoServicosIndicador_Periodicidade.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContratoServicosIndicador_Periodicidade.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(260), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosIndicador_Vigencia_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosIndicador_Vigencia_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosIndicador_Vigencia_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosIndicador_QtdeFaixas_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosIndicador_QtdeFaixas_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosIndicador_QtdeFaixas_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosIndicador_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicador_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosIndicador_CntSrvCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicador_CntSrvCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1296ContratoServicosIndicador_ContratoCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosIndicador_ContratoCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicador_ContratoCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1295ContratoServicosIndicador_AreaTrabalhoCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosIndicador_AreaTrabalhoCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicador_AreaTrabalhoCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1271ContratoServicosIndicador_Numero), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosIndicador_Numero_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicador_Numero_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A1274ContratoServicosIndicador_Indicador);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosIndicador_Indicador_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicador_Indicador_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A1305ContratoServicosIndicador_Finalidade);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosIndicador_Finalidade_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicador_Finalidade_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A1306ContratoServicosIndicador_Meta);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosIndicador_Meta_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicador_Meta_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A1307ContratoServicosIndicador_InstrumentoMedicao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosIndicador_InstrumentoMedicao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicador_InstrumentoMedicao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1308ContratoServicosIndicador_Tipo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContratoServicosIndicador_Tipo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContratoServicosIndicador_Tipo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1309ContratoServicosIndicador_Periodicidade));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContratoServicosIndicador_Periodicidade.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContratoServicosIndicador_Periodicidade_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A1310ContratoServicosIndicador_Vigencia);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosIndicador_Vigencia_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicador_Vigencia_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosIndicador_QtdeFaixas_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicador_QtdeFaixas_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 80 )
         {
            wbEnd = 0;
            nRC_GXsfl_80 = (short)(nGXsfl_80_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_77_JG2e( true) ;
         }
         else
         {
            wb_table4_77_JG2e( false) ;
         }
      }

      protected void wb_table2_5_JG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptContratoServicosIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptContratoServicosIndicador.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptContratoServicosIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_14_JG2( true) ;
         }
         else
         {
            wb_table5_14_JG2( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_JG2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_JG2e( true) ;
         }
         else
         {
            wb_table2_5_JG2e( false) ;
         }
      }

      protected void wb_table5_14_JG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoServicosIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_19_JG2( true) ;
         }
         else
         {
            wb_table6_19_JG2( false) ;
         }
         return  ;
      }

      protected void wb_table6_19_JG2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptContratoServicosIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_JG2e( true) ;
         }
         else
         {
            wb_table5_14_JG2e( false) ;
         }
      }

      protected void wb_table6_19_JG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoServicosIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_PromptContratoServicosIndicador.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoServicosIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_JG2( true) ;
         }
         else
         {
            wb_table7_28_JG2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_JG2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoServicosIndicador.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoServicosIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoServicosIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,41);\"", "", true, "HLP_PromptContratoServicosIndicador.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoServicosIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_45_JG2( true) ;
         }
         else
         {
            wb_table8_45_JG2( false) ;
         }
         return  ;
      }

      protected void wb_table8_45_JG2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoServicosIndicador.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoServicosIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoServicosIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,58);\"", "", true, "HLP_PromptContratoServicosIndicador.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoServicosIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_62_JG2( true) ;
         }
         else
         {
            wb_table9_62_JG2( false) ;
         }
         return  ;
      }

      protected void wb_table9_62_JG2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoServicosIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_19_JG2e( true) ;
         }
         else
         {
            wb_table6_19_JG2e( false) ;
         }
      }

      protected void wb_table9_62_JG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,65);\"", "", true, "HLP_PromptContratoServicosIndicador.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavContratoservicosindicador_indicador3_Internalname, AV25ContratoServicosIndicador_Indicador3, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,67);\"", 0, edtavContratoservicosindicador_indicador3_Visible, 1, 0, 80, "chr", 2, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "", "HLP_PromptContratoServicosIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_62_JG2e( true) ;
         }
         else
         {
            wb_table9_62_JG2e( false) ;
         }
      }

      protected void wb_table8_45_JG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,48);\"", "", true, "HLP_PromptContratoServicosIndicador.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavContratoservicosindicador_indicador2_Internalname, AV21ContratoServicosIndicador_Indicador2, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", 0, edtavContratoservicosindicador_indicador2_Visible, 1, 0, 80, "chr", 2, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "", "HLP_PromptContratoServicosIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_45_JG2e( true) ;
         }
         else
         {
            wb_table8_45_JG2e( false) ;
         }
      }

      protected void wb_table7_28_JG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_PromptContratoServicosIndicador.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavContratoservicosindicador_indicador1_Internalname, AV17ContratoServicosIndicador_Indicador1, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", 0, edtavContratoservicosindicador_indicador1_Visible, 1, 0, 80, "chr", 2, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "", "HLP_PromptContratoServicosIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_JG2e( true) ;
         }
         else
         {
            wb_table7_28_JG2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutContratoServicosIndicador_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContratoServicosIndicador_Codigo), 6, 0)));
         AV8InOutContratoServicosIndicador_Indicador = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContratoServicosIndicador_Indicador", AV8InOutContratoServicosIndicador_Indicador);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAJG2( ) ;
         WSJG2( ) ;
         WEJG2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205299411032");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptcontratoservicosindicador.js", "?20205299411032");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_802( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_80_idx;
         edtContratoServicosIndicador_Codigo_Internalname = "CONTRATOSERVICOSINDICADOR_CODIGO_"+sGXsfl_80_idx;
         edtContratoServicosIndicador_CntSrvCod_Internalname = "CONTRATOSERVICOSINDICADOR_CNTSRVCOD_"+sGXsfl_80_idx;
         edtContratoServicosIndicador_ContratoCod_Internalname = "CONTRATOSERVICOSINDICADOR_CONTRATOCOD_"+sGXsfl_80_idx;
         edtContratoServicosIndicador_AreaTrabalhoCod_Internalname = "CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_"+sGXsfl_80_idx;
         edtContratoServicosIndicador_Numero_Internalname = "CONTRATOSERVICOSINDICADOR_NUMERO_"+sGXsfl_80_idx;
         edtContratoServicosIndicador_Indicador_Internalname = "CONTRATOSERVICOSINDICADOR_INDICADOR_"+sGXsfl_80_idx;
         edtContratoServicosIndicador_Finalidade_Internalname = "CONTRATOSERVICOSINDICADOR_FINALIDADE_"+sGXsfl_80_idx;
         edtContratoServicosIndicador_Meta_Internalname = "CONTRATOSERVICOSINDICADOR_META_"+sGXsfl_80_idx;
         edtContratoServicosIndicador_InstrumentoMedicao_Internalname = "CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_"+sGXsfl_80_idx;
         cmbContratoServicosIndicador_Tipo_Internalname = "CONTRATOSERVICOSINDICADOR_TIPO_"+sGXsfl_80_idx;
         cmbContratoServicosIndicador_Periodicidade_Internalname = "CONTRATOSERVICOSINDICADOR_PERIODICIDADE_"+sGXsfl_80_idx;
         edtContratoServicosIndicador_Vigencia_Internalname = "CONTRATOSERVICOSINDICADOR_VIGENCIA_"+sGXsfl_80_idx;
         edtContratoServicosIndicador_QtdeFaixas_Internalname = "CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_"+sGXsfl_80_idx;
      }

      protected void SubsflControlProps_fel_802( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_80_fel_idx;
         edtContratoServicosIndicador_Codigo_Internalname = "CONTRATOSERVICOSINDICADOR_CODIGO_"+sGXsfl_80_fel_idx;
         edtContratoServicosIndicador_CntSrvCod_Internalname = "CONTRATOSERVICOSINDICADOR_CNTSRVCOD_"+sGXsfl_80_fel_idx;
         edtContratoServicosIndicador_ContratoCod_Internalname = "CONTRATOSERVICOSINDICADOR_CONTRATOCOD_"+sGXsfl_80_fel_idx;
         edtContratoServicosIndicador_AreaTrabalhoCod_Internalname = "CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_"+sGXsfl_80_fel_idx;
         edtContratoServicosIndicador_Numero_Internalname = "CONTRATOSERVICOSINDICADOR_NUMERO_"+sGXsfl_80_fel_idx;
         edtContratoServicosIndicador_Indicador_Internalname = "CONTRATOSERVICOSINDICADOR_INDICADOR_"+sGXsfl_80_fel_idx;
         edtContratoServicosIndicador_Finalidade_Internalname = "CONTRATOSERVICOSINDICADOR_FINALIDADE_"+sGXsfl_80_fel_idx;
         edtContratoServicosIndicador_Meta_Internalname = "CONTRATOSERVICOSINDICADOR_META_"+sGXsfl_80_fel_idx;
         edtContratoServicosIndicador_InstrumentoMedicao_Internalname = "CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_"+sGXsfl_80_fel_idx;
         cmbContratoServicosIndicador_Tipo_Internalname = "CONTRATOSERVICOSINDICADOR_TIPO_"+sGXsfl_80_fel_idx;
         cmbContratoServicosIndicador_Periodicidade_Internalname = "CONTRATOSERVICOSINDICADOR_PERIODICIDADE_"+sGXsfl_80_fel_idx;
         edtContratoServicosIndicador_Vigencia_Internalname = "CONTRATOSERVICOSINDICADOR_VIGENCIA_"+sGXsfl_80_fel_idx;
         edtContratoServicosIndicador_QtdeFaixas_Internalname = "CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_"+sGXsfl_80_fel_idx;
      }

      protected void sendrow_802( )
      {
         SubsflControlProps_802( ) ;
         WBJG0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_80_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_80_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_80_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 81,'',false,'',80)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV28Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV89Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV89Select_GXI : context.PathToRelativeUrl( AV28Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_80_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV28Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosIndicador_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1269ContratoServicosIndicador_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosIndicador_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosIndicador_CntSrvCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosIndicador_CntSrvCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosIndicador_ContratoCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1296ContratoServicosIndicador_ContratoCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1296ContratoServicosIndicador_ContratoCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosIndicador_ContratoCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosIndicador_AreaTrabalhoCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1295ContratoServicosIndicador_AreaTrabalhoCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1295ContratoServicosIndicador_AreaTrabalhoCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosIndicador_AreaTrabalhoCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosIndicador_Numero_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1271ContratoServicosIndicador_Numero), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A1271ContratoServicosIndicador_Numero), "ZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosIndicador_Numero_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)40,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosIndicador_Indicador_Internalname,(String)A1274ContratoServicosIndicador_Indicador,(String)A1274ContratoServicosIndicador_Indicador,(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosIndicador_Indicador_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)410,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)0,(short)80,(short)1,(short)0,(short)-1,(bool)true,(String)"DescricaoLonga",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosIndicador_Finalidade_Internalname,(String)A1305ContratoServicosIndicador_Finalidade,(String)A1305ContratoServicosIndicador_Finalidade,(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosIndicador_Finalidade_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)410,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)0,(short)80,(short)1,(short)0,(short)-1,(bool)true,(String)"DescricaoLonga",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosIndicador_Meta_Internalname,(String)A1306ContratoServicosIndicador_Meta,(String)A1306ContratoServicosIndicador_Meta,(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosIndicador_Meta_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)410,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)0,(short)80,(short)1,(short)0,(short)-1,(bool)true,(String)"DescricaoLonga",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosIndicador_InstrumentoMedicao_Internalname,(String)A1307ContratoServicosIndicador_InstrumentoMedicao,(String)A1307ContratoServicosIndicador_InstrumentoMedicao,(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosIndicador_InstrumentoMedicao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)410,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)0,(short)80,(short)1,(short)0,(short)-1,(bool)true,(String)"DescricaoLonga",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_80_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTRATOSERVICOSINDICADOR_TIPO_" + sGXsfl_80_idx;
               cmbContratoServicosIndicador_Tipo.Name = GXCCtl;
               cmbContratoServicosIndicador_Tipo.WebTags = "";
               cmbContratoServicosIndicador_Tipo.addItem("", "(Nenhum)", 0);
               cmbContratoServicosIndicador_Tipo.addItem("P", "Pontualidade (demanda)", 0);
               cmbContratoServicosIndicador_Tipo.addItem("PL", "Frequ�ncia de atrasos (lote)", 0);
               cmbContratoServicosIndicador_Tipo.addItem("D", "Diverg�ncias (demanda)", 0);
               cmbContratoServicosIndicador_Tipo.addItem("FD", "Frequ�ncia de diverg�ncias (lote)", 0);
               cmbContratoServicosIndicador_Tipo.addItem("FP", "Frequ�ncia de pend�ncias (�ndice de rejei��o de demandas) (lote)", 0);
               cmbContratoServicosIndicador_Tipo.addItem("AC", "Ajustes de contagens (qtd) (lote)", 0);
               cmbContratoServicosIndicador_Tipo.addItem("AV", "Ajustes de contagens (soma do valor bruto) (lote)", 0);
               cmbContratoServicosIndicador_Tipo.addItem("T", "Tempestividade", 0);
               if ( cmbContratoServicosIndicador_Tipo.ItemCount > 0 )
               {
                  A1308ContratoServicosIndicador_Tipo = cmbContratoServicosIndicador_Tipo.getValidValue(A1308ContratoServicosIndicador_Tipo);
                  n1308ContratoServicosIndicador_Tipo = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContratoServicosIndicador_Tipo,(String)cmbContratoServicosIndicador_Tipo_Internalname,StringUtil.RTrim( A1308ContratoServicosIndicador_Tipo),(short)1,(String)cmbContratoServicosIndicador_Tipo_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContratoServicosIndicador_Tipo.CurrentValue = StringUtil.RTrim( A1308ContratoServicosIndicador_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicosIndicador_Tipo_Internalname, "Values", (String)(cmbContratoServicosIndicador_Tipo.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_80_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTRATOSERVICOSINDICADOR_PERIODICIDADE_" + sGXsfl_80_idx;
               cmbContratoServicosIndicador_Periodicidade.Name = GXCCtl;
               cmbContratoServicosIndicador_Periodicidade.WebTags = "";
               cmbContratoServicosIndicador_Periodicidade.addItem("M", "Mensal", 0);
               if ( cmbContratoServicosIndicador_Periodicidade.ItemCount > 0 )
               {
                  A1309ContratoServicosIndicador_Periodicidade = cmbContratoServicosIndicador_Periodicidade.getValidValue(A1309ContratoServicosIndicador_Periodicidade);
                  n1309ContratoServicosIndicador_Periodicidade = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContratoServicosIndicador_Periodicidade,(String)cmbContratoServicosIndicador_Periodicidade_Internalname,StringUtil.RTrim( A1309ContratoServicosIndicador_Periodicidade),(short)1,(String)cmbContratoServicosIndicador_Periodicidade_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContratoServicosIndicador_Periodicidade.CurrentValue = StringUtil.RTrim( A1309ContratoServicosIndicador_Periodicidade);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicosIndicador_Periodicidade_Internalname, "Values", (String)(cmbContratoServicosIndicador_Periodicidade.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosIndicador_Vigencia_Internalname,(String)A1310ContratoServicosIndicador_Vigencia,StringUtil.RTrim( context.localUtil.Format( A1310ContratoServicosIndicador_Vigencia, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosIndicador_Vigencia_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)260,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)-1,(bool)true,(String)"Descricao",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosIndicador_QtdeFaixas_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), "ZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosIndicador_QtdeFaixas_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSINDICADOR_CODIGO"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( (decimal)(A1269ContratoServicosIndicador_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSINDICADOR_CNTSRVCOD"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSINDICADOR_NUMERO"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( (decimal)(A1271ContratoServicosIndicador_Numero), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSINDICADOR_INDICADOR"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, A1274ContratoServicosIndicador_Indicador));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSINDICADOR_FINALIDADE"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, A1305ContratoServicosIndicador_Finalidade));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSINDICADOR_META"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, A1306ContratoServicosIndicador_Meta));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, A1307ContratoServicosIndicador_InstrumentoMedicao));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSINDICADOR_TIPO"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, StringUtil.RTrim( context.localUtil.Format( A1308ContratoServicosIndicador_Tipo, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSINDICADOR_PERIODICIDADE"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, StringUtil.RTrim( context.localUtil.Format( A1309ContratoServicosIndicador_Periodicidade, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSINDICADOR_VIGENCIA"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, StringUtil.RTrim( context.localUtil.Format( A1310ContratoServicosIndicador_Vigencia, "@!"))));
            GridContainer.AddRow(GridRow);
            nGXsfl_80_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_80_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_80_idx+1));
            sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
            SubsflControlProps_802( ) ;
         }
         /* End function sendrow_802 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavContratoservicosindicador_indicador1_Internalname = "vCONTRATOSERVICOSINDICADOR_INDICADOR1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavContratoservicosindicador_indicador2_Internalname = "vCONTRATOSERVICOSINDICADOR_INDICADOR2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavContratoservicosindicador_indicador3_Internalname = "vCONTRATOSERVICOSINDICADOR_INDICADOR3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtContratoServicosIndicador_Codigo_Internalname = "CONTRATOSERVICOSINDICADOR_CODIGO";
         edtContratoServicosIndicador_CntSrvCod_Internalname = "CONTRATOSERVICOSINDICADOR_CNTSRVCOD";
         edtContratoServicosIndicador_ContratoCod_Internalname = "CONTRATOSERVICOSINDICADOR_CONTRATOCOD";
         edtContratoServicosIndicador_AreaTrabalhoCod_Internalname = "CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD";
         edtContratoServicosIndicador_Numero_Internalname = "CONTRATOSERVICOSINDICADOR_NUMERO";
         edtContratoServicosIndicador_Indicador_Internalname = "CONTRATOSERVICOSINDICADOR_INDICADOR";
         edtContratoServicosIndicador_Finalidade_Internalname = "CONTRATOSERVICOSINDICADOR_FINALIDADE";
         edtContratoServicosIndicador_Meta_Internalname = "CONTRATOSERVICOSINDICADOR_META";
         edtContratoServicosIndicador_InstrumentoMedicao_Internalname = "CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO";
         cmbContratoServicosIndicador_Tipo_Internalname = "CONTRATOSERVICOSINDICADOR_TIPO";
         cmbContratoServicosIndicador_Periodicidade_Internalname = "CONTRATOSERVICOSINDICADOR_PERIODICIDADE";
         edtContratoServicosIndicador_Vigencia_Internalname = "CONTRATOSERVICOSINDICADOR_VIGENCIA";
         edtContratoServicosIndicador_QtdeFaixas_Internalname = "CONTRATOSERVICOSINDICADOR_QTDEFAIXAS";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfcontratoservicosindicador_codigo_Internalname = "vTFCONTRATOSERVICOSINDICADOR_CODIGO";
         edtavTfcontratoservicosindicador_codigo_to_Internalname = "vTFCONTRATOSERVICOSINDICADOR_CODIGO_TO";
         edtavTfcontratoservicosindicador_cntsrvcod_Internalname = "vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD";
         edtavTfcontratoservicosindicador_cntsrvcod_to_Internalname = "vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD_TO";
         edtavTfcontratoservicosindicador_contratocod_Internalname = "vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD";
         edtavTfcontratoservicosindicador_contratocod_to_Internalname = "vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD_TO";
         edtavTfcontratoservicosindicador_areatrabalhocod_Internalname = "vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD";
         edtavTfcontratoservicosindicador_areatrabalhocod_to_Internalname = "vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_TO";
         edtavTfcontratoservicosindicador_numero_Internalname = "vTFCONTRATOSERVICOSINDICADOR_NUMERO";
         edtavTfcontratoservicosindicador_numero_to_Internalname = "vTFCONTRATOSERVICOSINDICADOR_NUMERO_TO";
         edtavTfcontratoservicosindicador_indicador_Internalname = "vTFCONTRATOSERVICOSINDICADOR_INDICADOR";
         edtavTfcontratoservicosindicador_indicador_sel_Internalname = "vTFCONTRATOSERVICOSINDICADOR_INDICADOR_SEL";
         edtavTfcontratoservicosindicador_finalidade_Internalname = "vTFCONTRATOSERVICOSINDICADOR_FINALIDADE";
         edtavTfcontratoservicosindicador_finalidade_sel_Internalname = "vTFCONTRATOSERVICOSINDICADOR_FINALIDADE_SEL";
         edtavTfcontratoservicosindicador_meta_Internalname = "vTFCONTRATOSERVICOSINDICADOR_META";
         edtavTfcontratoservicosindicador_meta_sel_Internalname = "vTFCONTRATOSERVICOSINDICADOR_META_SEL";
         edtavTfcontratoservicosindicador_instrumentomedicao_Internalname = "vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO";
         edtavTfcontratoservicosindicador_instrumentomedicao_sel_Internalname = "vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_SEL";
         edtavTfcontratoservicosindicador_vigencia_Internalname = "vTFCONTRATOSERVICOSINDICADOR_VIGENCIA";
         edtavTfcontratoservicosindicador_vigencia_sel_Internalname = "vTFCONTRATOSERVICOSINDICADOR_VIGENCIA_SEL";
         edtavTfcontratoservicosindicador_qtdefaixas_Internalname = "vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS";
         edtavTfcontratoservicosindicador_qtdefaixas_to_Internalname = "vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS_TO";
         Ddo_contratoservicosindicador_codigo_Internalname = "DDO_CONTRATOSERVICOSINDICADOR_CODIGO";
         edtavDdo_contratoservicosindicador_codigotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSERVICOSINDICADOR_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_contratoservicosindicador_cntsrvcod_Internalname = "DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD";
         edtavDdo_contratoservicosindicador_cntsrvcodtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSERVICOSINDICADOR_CNTSRVCODTITLECONTROLIDTOREPLACE";
         Ddo_contratoservicosindicador_contratocod_Internalname = "DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD";
         edtavDdo_contratoservicosindicador_contratocodtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSERVICOSINDICADOR_CONTRATOCODTITLECONTROLIDTOREPLACE";
         Ddo_contratoservicosindicador_areatrabalhocod_Internalname = "DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD";
         edtavDdo_contratoservicosindicador_areatrabalhocodtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCODTITLECONTROLIDTOREPLACE";
         Ddo_contratoservicosindicador_numero_Internalname = "DDO_CONTRATOSERVICOSINDICADOR_NUMERO";
         edtavDdo_contratoservicosindicador_numerotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSERVICOSINDICADOR_NUMEROTITLECONTROLIDTOREPLACE";
         Ddo_contratoservicosindicador_indicador_Internalname = "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR";
         edtavDdo_contratoservicosindicador_indicadortitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSERVICOSINDICADOR_INDICADORTITLECONTROLIDTOREPLACE";
         Ddo_contratoservicosindicador_finalidade_Internalname = "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE";
         edtavDdo_contratoservicosindicador_finalidadetitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSERVICOSINDICADOR_FINALIDADETITLECONTROLIDTOREPLACE";
         Ddo_contratoservicosindicador_meta_Internalname = "DDO_CONTRATOSERVICOSINDICADOR_META";
         edtavDdo_contratoservicosindicador_metatitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSERVICOSINDICADOR_METATITLECONTROLIDTOREPLACE";
         Ddo_contratoservicosindicador_instrumentomedicao_Internalname = "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO";
         edtavDdo_contratoservicosindicador_instrumentomedicaotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAOTITLECONTROLIDTOREPLACE";
         Ddo_contratoservicosindicador_tipo_Internalname = "DDO_CONTRATOSERVICOSINDICADOR_TIPO";
         edtavDdo_contratoservicosindicador_tipotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSERVICOSINDICADOR_TIPOTITLECONTROLIDTOREPLACE";
         Ddo_contratoservicosindicador_periodicidade_Internalname = "DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE";
         edtavDdo_contratoservicosindicador_periodicidadetitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADETITLECONTROLIDTOREPLACE";
         Ddo_contratoservicosindicador_vigencia_Internalname = "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA";
         edtavDdo_contratoservicosindicador_vigenciatitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSERVICOSINDICADOR_VIGENCIATITLECONTROLIDTOREPLACE";
         Ddo_contratoservicosindicador_qtdefaixas_Internalname = "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS";
         edtavDdo_contratoservicosindicador_qtdefaixastitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXASTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtContratoServicosIndicador_QtdeFaixas_Jsonclick = "";
         edtContratoServicosIndicador_Vigencia_Jsonclick = "";
         cmbContratoServicosIndicador_Periodicidade_Jsonclick = "";
         cmbContratoServicosIndicador_Tipo_Jsonclick = "";
         edtContratoServicosIndicador_InstrumentoMedicao_Jsonclick = "";
         edtContratoServicosIndicador_Meta_Jsonclick = "";
         edtContratoServicosIndicador_Finalidade_Jsonclick = "";
         edtContratoServicosIndicador_Indicador_Jsonclick = "";
         edtContratoServicosIndicador_Numero_Jsonclick = "";
         edtContratoServicosIndicador_AreaTrabalhoCod_Jsonclick = "";
         edtContratoServicosIndicador_ContratoCod_Jsonclick = "";
         edtContratoServicosIndicador_CntSrvCod_Jsonclick = "";
         edtContratoServicosIndicador_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         edtContratoServicosIndicador_QtdeFaixas_Titleformat = 0;
         edtContratoServicosIndicador_Vigencia_Titleformat = 0;
         cmbContratoServicosIndicador_Periodicidade_Titleformat = 0;
         cmbContratoServicosIndicador_Tipo_Titleformat = 0;
         edtContratoServicosIndicador_InstrumentoMedicao_Titleformat = 0;
         edtContratoServicosIndicador_Meta_Titleformat = 0;
         edtContratoServicosIndicador_Finalidade_Titleformat = 0;
         edtContratoServicosIndicador_Indicador_Titleformat = 0;
         edtContratoServicosIndicador_Numero_Titleformat = 0;
         edtContratoServicosIndicador_AreaTrabalhoCod_Titleformat = 0;
         edtContratoServicosIndicador_ContratoCod_Titleformat = 0;
         edtContratoServicosIndicador_CntSrvCod_Titleformat = 0;
         edtContratoServicosIndicador_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavContratoservicosindicador_indicador3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavContratoservicosindicador_indicador2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavContratoservicosindicador_indicador1_Visible = 1;
         edtContratoServicosIndicador_QtdeFaixas_Title = "Faixas";
         edtContratoServicosIndicador_Vigencia_Title = "Vig�ncia";
         cmbContratoServicosIndicador_Periodicidade.Title.Text = "Periodicidade";
         cmbContratoServicosIndicador_Tipo.Title.Text = "de Indicador";
         edtContratoServicosIndicador_InstrumentoMedicao_Title = "de Medi��o";
         edtContratoServicosIndicador_Meta_Title = "a cumprir";
         edtContratoServicosIndicador_Finalidade_Title = "Finalidade";
         edtContratoServicosIndicador_Indicador_Title = "Indicador";
         edtContratoServicosIndicador_Numero_Title = "N�mero";
         edtContratoServicosIndicador_AreaTrabalhoCod_Title = "de Trabalho";
         edtContratoServicosIndicador_ContratoCod_Title = "Contrato";
         edtContratoServicosIndicador_CntSrvCod_Title = "servi�o";
         edtContratoServicosIndicador_Codigo_Title = "Codigo";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_contratoservicosindicador_qtdefaixastitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicosindicador_vigenciatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicosindicador_periodicidadetitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicosindicador_tipotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicosindicador_instrumentomedicaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicosindicador_metatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicosindicador_finalidadetitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicosindicador_indicadortitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicosindicador_numerotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicosindicador_areatrabalhocodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicosindicador_contratocodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicosindicador_cntsrvcodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicosindicador_codigotitlecontrolidtoreplace_Visible = 1;
         edtavTfcontratoservicosindicador_qtdefaixas_to_Jsonclick = "";
         edtavTfcontratoservicosindicador_qtdefaixas_to_Visible = 1;
         edtavTfcontratoservicosindicador_qtdefaixas_Jsonclick = "";
         edtavTfcontratoservicosindicador_qtdefaixas_Visible = 1;
         edtavTfcontratoservicosindicador_vigencia_sel_Jsonclick = "";
         edtavTfcontratoservicosindicador_vigencia_sel_Visible = 1;
         edtavTfcontratoservicosindicador_vigencia_Jsonclick = "";
         edtavTfcontratoservicosindicador_vigencia_Visible = 1;
         edtavTfcontratoservicosindicador_instrumentomedicao_sel_Visible = 1;
         edtavTfcontratoservicosindicador_instrumentomedicao_Visible = 1;
         edtavTfcontratoservicosindicador_meta_sel_Visible = 1;
         edtavTfcontratoservicosindicador_meta_Visible = 1;
         edtavTfcontratoservicosindicador_finalidade_sel_Visible = 1;
         edtavTfcontratoservicosindicador_finalidade_Visible = 1;
         edtavTfcontratoservicosindicador_indicador_sel_Visible = 1;
         edtavTfcontratoservicosindicador_indicador_Visible = 1;
         edtavTfcontratoservicosindicador_numero_to_Jsonclick = "";
         edtavTfcontratoservicosindicador_numero_to_Visible = 1;
         edtavTfcontratoservicosindicador_numero_Jsonclick = "";
         edtavTfcontratoservicosindicador_numero_Visible = 1;
         edtavTfcontratoservicosindicador_areatrabalhocod_to_Jsonclick = "";
         edtavTfcontratoservicosindicador_areatrabalhocod_to_Visible = 1;
         edtavTfcontratoservicosindicador_areatrabalhocod_Jsonclick = "";
         edtavTfcontratoservicosindicador_areatrabalhocod_Visible = 1;
         edtavTfcontratoservicosindicador_contratocod_to_Jsonclick = "";
         edtavTfcontratoservicosindicador_contratocod_to_Visible = 1;
         edtavTfcontratoservicosindicador_contratocod_Jsonclick = "";
         edtavTfcontratoservicosindicador_contratocod_Visible = 1;
         edtavTfcontratoservicosindicador_cntsrvcod_to_Jsonclick = "";
         edtavTfcontratoservicosindicador_cntsrvcod_to_Visible = 1;
         edtavTfcontratoservicosindicador_cntsrvcod_Jsonclick = "";
         edtavTfcontratoservicosindicador_cntsrvcod_Visible = 1;
         edtavTfcontratoservicosindicador_codigo_to_Jsonclick = "";
         edtavTfcontratoservicosindicador_codigo_to_Visible = 1;
         edtavTfcontratoservicosindicador_codigo_Jsonclick = "";
         edtavTfcontratoservicosindicador_codigo_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_contratoservicosindicador_qtdefaixas_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicosindicador_qtdefaixas_Rangefilterto = "At�";
         Ddo_contratoservicosindicador_qtdefaixas_Rangefilterfrom = "Desde";
         Ddo_contratoservicosindicador_qtdefaixas_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosindicador_qtdefaixas_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoservicosindicador_qtdefaixas_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_qtdefaixas_Filtertype = "Numeric";
         Ddo_contratoservicosindicador_qtdefaixas_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_qtdefaixas_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_contratoservicosindicador_qtdefaixas_Includesortasc = Convert.ToBoolean( 0);
         Ddo_contratoservicosindicador_qtdefaixas_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosindicador_qtdefaixas_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosindicador_qtdefaixas_Cls = "ColumnSettings";
         Ddo_contratoservicosindicador_qtdefaixas_Tooltip = "Op��es";
         Ddo_contratoservicosindicador_qtdefaixas_Caption = "";
         Ddo_contratoservicosindicador_vigencia_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicosindicador_vigencia_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratoservicosindicador_vigencia_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosindicador_vigencia_Loadingdata = "Carregando dados...";
         Ddo_contratoservicosindicador_vigencia_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosindicador_vigencia_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosindicador_vigencia_Datalistupdateminimumcharacters = 0;
         Ddo_contratoservicosindicador_vigencia_Datalistproc = "GetPromptContratoServicosIndicadorFilterData";
         Ddo_contratoservicosindicador_vigencia_Datalisttype = "Dynamic";
         Ddo_contratoservicosindicador_vigencia_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_vigencia_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratoservicosindicador_vigencia_Filtertype = "Character";
         Ddo_contratoservicosindicador_vigencia_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_vigencia_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_vigencia_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_vigencia_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosindicador_vigencia_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosindicador_vigencia_Cls = "ColumnSettings";
         Ddo_contratoservicosindicador_vigencia_Tooltip = "Op��es";
         Ddo_contratoservicosindicador_vigencia_Caption = "";
         Ddo_contratoservicosindicador_periodicidade_Searchbuttontext = "Filtrar Selecionados";
         Ddo_contratoservicosindicador_periodicidade_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosindicador_periodicidade_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosindicador_periodicidade_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosindicador_periodicidade_Datalistfixedvalues = "M:Mensal";
         Ddo_contratoservicosindicador_periodicidade_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_periodicidade_Datalisttype = "FixedValues";
         Ddo_contratoservicosindicador_periodicidade_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_periodicidade_Includefilter = Convert.ToBoolean( 0);
         Ddo_contratoservicosindicador_periodicidade_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_periodicidade_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_periodicidade_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosindicador_periodicidade_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosindicador_periodicidade_Cls = "ColumnSettings";
         Ddo_contratoservicosindicador_periodicidade_Tooltip = "Op��es";
         Ddo_contratoservicosindicador_periodicidade_Caption = "";
         Ddo_contratoservicosindicador_tipo_Searchbuttontext = "Filtrar Selecionados";
         Ddo_contratoservicosindicador_tipo_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosindicador_tipo_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosindicador_tipo_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosindicador_tipo_Datalistfixedvalues = "P:Pontualidade (demanda),PL:Frequ�ncia de atrasos (lote),D:Diverg�ncias (demanda),FD:Frequ�ncia de diverg�ncias (lote),FP:Frequ�ncia de pend�ncias (�ndice de rejei��o de demandas) (lote),AC:Ajustes de contagens (qtd) (lote),AV:Ajustes de contagens (soma do valor bruto) (lote),T:Tempestividade";
         Ddo_contratoservicosindicador_tipo_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_tipo_Datalisttype = "FixedValues";
         Ddo_contratoservicosindicador_tipo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_tipo_Includefilter = Convert.ToBoolean( 0);
         Ddo_contratoservicosindicador_tipo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_tipo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_tipo_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosindicador_tipo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosindicador_tipo_Cls = "ColumnSettings";
         Ddo_contratoservicosindicador_tipo_Tooltip = "Op��es";
         Ddo_contratoservicosindicador_tipo_Caption = "";
         Ddo_contratoservicosindicador_instrumentomedicao_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicosindicador_instrumentomedicao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratoservicosindicador_instrumentomedicao_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosindicador_instrumentomedicao_Loadingdata = "Carregando dados...";
         Ddo_contratoservicosindicador_instrumentomedicao_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosindicador_instrumentomedicao_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosindicador_instrumentomedicao_Datalistupdateminimumcharacters = 0;
         Ddo_contratoservicosindicador_instrumentomedicao_Datalistproc = "GetPromptContratoServicosIndicadorFilterData";
         Ddo_contratoservicosindicador_instrumentomedicao_Datalisttype = "Dynamic";
         Ddo_contratoservicosindicador_instrumentomedicao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_instrumentomedicao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratoservicosindicador_instrumentomedicao_Filtertype = "Character";
         Ddo_contratoservicosindicador_instrumentomedicao_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_instrumentomedicao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_instrumentomedicao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_instrumentomedicao_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosindicador_instrumentomedicao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosindicador_instrumentomedicao_Cls = "ColumnSettings";
         Ddo_contratoservicosindicador_instrumentomedicao_Tooltip = "Op��es";
         Ddo_contratoservicosindicador_instrumentomedicao_Caption = "";
         Ddo_contratoservicosindicador_meta_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicosindicador_meta_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratoservicosindicador_meta_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosindicador_meta_Loadingdata = "Carregando dados...";
         Ddo_contratoservicosindicador_meta_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosindicador_meta_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosindicador_meta_Datalistupdateminimumcharacters = 0;
         Ddo_contratoservicosindicador_meta_Datalistproc = "GetPromptContratoServicosIndicadorFilterData";
         Ddo_contratoservicosindicador_meta_Datalisttype = "Dynamic";
         Ddo_contratoservicosindicador_meta_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_meta_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratoservicosindicador_meta_Filtertype = "Character";
         Ddo_contratoservicosindicador_meta_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_meta_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_meta_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_meta_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosindicador_meta_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosindicador_meta_Cls = "ColumnSettings";
         Ddo_contratoservicosindicador_meta_Tooltip = "Op��es";
         Ddo_contratoservicosindicador_meta_Caption = "";
         Ddo_contratoservicosindicador_finalidade_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicosindicador_finalidade_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratoservicosindicador_finalidade_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosindicador_finalidade_Loadingdata = "Carregando dados...";
         Ddo_contratoservicosindicador_finalidade_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosindicador_finalidade_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosindicador_finalidade_Datalistupdateminimumcharacters = 0;
         Ddo_contratoservicosindicador_finalidade_Datalistproc = "GetPromptContratoServicosIndicadorFilterData";
         Ddo_contratoservicosindicador_finalidade_Datalisttype = "Dynamic";
         Ddo_contratoservicosindicador_finalidade_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_finalidade_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratoservicosindicador_finalidade_Filtertype = "Character";
         Ddo_contratoservicosindicador_finalidade_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_finalidade_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_finalidade_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_finalidade_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosindicador_finalidade_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosindicador_finalidade_Cls = "ColumnSettings";
         Ddo_contratoservicosindicador_finalidade_Tooltip = "Op��es";
         Ddo_contratoservicosindicador_finalidade_Caption = "";
         Ddo_contratoservicosindicador_indicador_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicosindicador_indicador_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratoservicosindicador_indicador_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosindicador_indicador_Loadingdata = "Carregando dados...";
         Ddo_contratoservicosindicador_indicador_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosindicador_indicador_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosindicador_indicador_Datalistupdateminimumcharacters = 0;
         Ddo_contratoservicosindicador_indicador_Datalistproc = "GetPromptContratoServicosIndicadorFilterData";
         Ddo_contratoservicosindicador_indicador_Datalisttype = "Dynamic";
         Ddo_contratoservicosindicador_indicador_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_indicador_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratoservicosindicador_indicador_Filtertype = "Character";
         Ddo_contratoservicosindicador_indicador_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_indicador_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_indicador_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_indicador_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosindicador_indicador_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosindicador_indicador_Cls = "ColumnSettings";
         Ddo_contratoservicosindicador_indicador_Tooltip = "Op��es";
         Ddo_contratoservicosindicador_indicador_Caption = "";
         Ddo_contratoservicosindicador_numero_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicosindicador_numero_Rangefilterto = "At�";
         Ddo_contratoservicosindicador_numero_Rangefilterfrom = "Desde";
         Ddo_contratoservicosindicador_numero_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosindicador_numero_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosindicador_numero_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosindicador_numero_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoservicosindicador_numero_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_numero_Filtertype = "Numeric";
         Ddo_contratoservicosindicador_numero_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_numero_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_numero_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_numero_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosindicador_numero_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosindicador_numero_Cls = "ColumnSettings";
         Ddo_contratoservicosindicador_numero_Tooltip = "Op��es";
         Ddo_contratoservicosindicador_numero_Caption = "";
         Ddo_contratoservicosindicador_areatrabalhocod_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicosindicador_areatrabalhocod_Rangefilterto = "At�";
         Ddo_contratoservicosindicador_areatrabalhocod_Rangefilterfrom = "Desde";
         Ddo_contratoservicosindicador_areatrabalhocod_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosindicador_areatrabalhocod_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosindicador_areatrabalhocod_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosindicador_areatrabalhocod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoservicosindicador_areatrabalhocod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_areatrabalhocod_Filtertype = "Numeric";
         Ddo_contratoservicosindicador_areatrabalhocod_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_areatrabalhocod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_areatrabalhocod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_areatrabalhocod_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosindicador_areatrabalhocod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosindicador_areatrabalhocod_Cls = "ColumnSettings";
         Ddo_contratoservicosindicador_areatrabalhocod_Tooltip = "Op��es";
         Ddo_contratoservicosindicador_areatrabalhocod_Caption = "";
         Ddo_contratoservicosindicador_contratocod_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicosindicador_contratocod_Rangefilterto = "At�";
         Ddo_contratoservicosindicador_contratocod_Rangefilterfrom = "Desde";
         Ddo_contratoservicosindicador_contratocod_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosindicador_contratocod_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosindicador_contratocod_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosindicador_contratocod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoservicosindicador_contratocod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_contratocod_Filtertype = "Numeric";
         Ddo_contratoservicosindicador_contratocod_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_contratocod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_contratocod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_contratocod_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosindicador_contratocod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosindicador_contratocod_Cls = "ColumnSettings";
         Ddo_contratoservicosindicador_contratocod_Tooltip = "Op��es";
         Ddo_contratoservicosindicador_contratocod_Caption = "";
         Ddo_contratoservicosindicador_cntsrvcod_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicosindicador_cntsrvcod_Rangefilterto = "At�";
         Ddo_contratoservicosindicador_cntsrvcod_Rangefilterfrom = "Desde";
         Ddo_contratoservicosindicador_cntsrvcod_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosindicador_cntsrvcod_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosindicador_cntsrvcod_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosindicador_cntsrvcod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoservicosindicador_cntsrvcod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_cntsrvcod_Filtertype = "Numeric";
         Ddo_contratoservicosindicador_cntsrvcod_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_cntsrvcod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_cntsrvcod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_cntsrvcod_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosindicador_cntsrvcod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosindicador_cntsrvcod_Cls = "ColumnSettings";
         Ddo_contratoservicosindicador_cntsrvcod_Tooltip = "Op��es";
         Ddo_contratoservicosindicador_cntsrvcod_Caption = "";
         Ddo_contratoservicosindicador_codigo_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicosindicador_codigo_Rangefilterto = "At�";
         Ddo_contratoservicosindicador_codigo_Rangefilterfrom = "Desde";
         Ddo_contratoservicosindicador_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosindicador_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosindicador_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosindicador_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoservicosindicador_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_codigo_Filtertype = "Numeric";
         Ddo_contratoservicosindicador_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_codigo_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosindicador_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosindicador_codigo_Cls = "ColumnSettings";
         Ddo_contratoservicosindicador_codigo_Tooltip = "Op��es";
         Ddo_contratoservicosindicador_codigo_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Contrato Servicos Indicador";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CNTSRVCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INDICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_FINALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_METATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_VIGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV32TFContratoServicosIndicador_Codigo',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV33TFContratoServicosIndicador_Codigo_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratoServicosIndicador_CntSrvCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV37TFContratoServicosIndicador_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV40TFContratoServicosIndicador_ContratoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV41TFContratoServicosIndicador_ContratoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratoServicosIndicador_AreaTrabalhoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV45TFContratoServicosIndicador_AreaTrabalhoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratoServicosIndicador_Numero',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO',pic:'ZZZ9',nv:0},{av:'AV49TFContratoServicosIndicador_Numero_To',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO_TO',pic:'ZZZ9',nv:0},{av:'AV52TFContratoServicosIndicador_Indicador',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR',pic:'',nv:''},{av:'AV53TFContratoServicosIndicador_Indicador_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR_SEL',pic:'',nv:''},{av:'AV56TFContratoServicosIndicador_Finalidade',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE',pic:'',nv:''},{av:'AV57TFContratoServicosIndicador_Finalidade_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE_SEL',pic:'',nv:''},{av:'AV60TFContratoServicosIndicador_Meta',fld:'vTFCONTRATOSERVICOSINDICADOR_META',pic:'',nv:''},{av:'AV61TFContratoServicosIndicador_Meta_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_META_SEL',pic:'',nv:''},{av:'AV64TFContratoServicosIndicador_InstrumentoMedicao',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO',pic:'',nv:''},{av:'AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_SEL',pic:'',nv:''},{av:'AV69TFContratoServicosIndicador_Tipo_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_TIPO_SELS',pic:'',nv:null},{av:'AV73TFContratoServicosIndicador_Periodicidade_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_PERIODICIDADE_SELS',pic:'',nv:null},{av:'AV76TFContratoServicosIndicador_Vigencia',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA',pic:'@!',nv:''},{av:'AV77TFContratoServicosIndicador_Vigencia_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA_SEL',pic:'@!',nv:''},{av:'AV80TFContratoServicosIndicador_QtdeFaixas',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS',pic:'ZZZ9',nv:0},{av:'AV81TFContratoServicosIndicador_QtdeFaixas_To',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS_TO',pic:'ZZZ9',nv:0},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratoServicosIndicador_Indicador1',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ContratoServicosIndicador_Indicador2',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ContratoServicosIndicador_Indicador3',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0}],oparms:[{av:'AV31ContratoServicosIndicador_CodigoTitleFilterData',fld:'vCONTRATOSERVICOSINDICADOR_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV35ContratoServicosIndicador_CntSrvCodTitleFilterData',fld:'vCONTRATOSERVICOSINDICADOR_CNTSRVCODTITLEFILTERDATA',pic:'',nv:null},{av:'AV39ContratoServicosIndicador_ContratoCodTitleFilterData',fld:'vCONTRATOSERVICOSINDICADOR_CONTRATOCODTITLEFILTERDATA',pic:'',nv:null},{av:'AV43ContratoServicosIndicador_AreaTrabalhoCodTitleFilterData',fld:'vCONTRATOSERVICOSINDICADOR_AREATRABALHOCODTITLEFILTERDATA',pic:'',nv:null},{av:'AV47ContratoServicosIndicador_NumeroTitleFilterData',fld:'vCONTRATOSERVICOSINDICADOR_NUMEROTITLEFILTERDATA',pic:'',nv:null},{av:'AV51ContratoServicosIndicador_IndicadorTitleFilterData',fld:'vCONTRATOSERVICOSINDICADOR_INDICADORTITLEFILTERDATA',pic:'',nv:null},{av:'AV55ContratoServicosIndicador_FinalidadeTitleFilterData',fld:'vCONTRATOSERVICOSINDICADOR_FINALIDADETITLEFILTERDATA',pic:'',nv:null},{av:'AV59ContratoServicosIndicador_MetaTitleFilterData',fld:'vCONTRATOSERVICOSINDICADOR_METATITLEFILTERDATA',pic:'',nv:null},{av:'AV63ContratoServicosIndicador_InstrumentoMedicaoTitleFilterData',fld:'vCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV67ContratoServicosIndicador_TipoTitleFilterData',fld:'vCONTRATOSERVICOSINDICADOR_TIPOTITLEFILTERDATA',pic:'',nv:null},{av:'AV71ContratoServicosIndicador_PeriodicidadeTitleFilterData',fld:'vCONTRATOSERVICOSINDICADOR_PERIODICIDADETITLEFILTERDATA',pic:'',nv:null},{av:'AV75ContratoServicosIndicador_VigenciaTitleFilterData',fld:'vCONTRATOSERVICOSINDICADOR_VIGENCIATITLEFILTERDATA',pic:'',nv:null},{av:'AV79ContratoServicosIndicador_QtdeFaixasTitleFilterData',fld:'vCONTRATOSERVICOSINDICADOR_QTDEFAIXASTITLEFILTERDATA',pic:'',nv:null},{av:'edtContratoServicosIndicador_Codigo_Titleformat',ctrl:'CONTRATOSERVICOSINDICADOR_CODIGO',prop:'Titleformat'},{av:'edtContratoServicosIndicador_Codigo_Title',ctrl:'CONTRATOSERVICOSINDICADOR_CODIGO',prop:'Title'},{av:'edtContratoServicosIndicador_CntSrvCod_Titleformat',ctrl:'CONTRATOSERVICOSINDICADOR_CNTSRVCOD',prop:'Titleformat'},{av:'edtContratoServicosIndicador_CntSrvCod_Title',ctrl:'CONTRATOSERVICOSINDICADOR_CNTSRVCOD',prop:'Title'},{av:'edtContratoServicosIndicador_ContratoCod_Titleformat',ctrl:'CONTRATOSERVICOSINDICADOR_CONTRATOCOD',prop:'Titleformat'},{av:'edtContratoServicosIndicador_ContratoCod_Title',ctrl:'CONTRATOSERVICOSINDICADOR_CONTRATOCOD',prop:'Title'},{av:'edtContratoServicosIndicador_AreaTrabalhoCod_Titleformat',ctrl:'CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',prop:'Titleformat'},{av:'edtContratoServicosIndicador_AreaTrabalhoCod_Title',ctrl:'CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',prop:'Title'},{av:'edtContratoServicosIndicador_Numero_Titleformat',ctrl:'CONTRATOSERVICOSINDICADOR_NUMERO',prop:'Titleformat'},{av:'edtContratoServicosIndicador_Numero_Title',ctrl:'CONTRATOSERVICOSINDICADOR_NUMERO',prop:'Title'},{av:'edtContratoServicosIndicador_Indicador_Titleformat',ctrl:'CONTRATOSERVICOSINDICADOR_INDICADOR',prop:'Titleformat'},{av:'edtContratoServicosIndicador_Indicador_Title',ctrl:'CONTRATOSERVICOSINDICADOR_INDICADOR',prop:'Title'},{av:'edtContratoServicosIndicador_Finalidade_Titleformat',ctrl:'CONTRATOSERVICOSINDICADOR_FINALIDADE',prop:'Titleformat'},{av:'edtContratoServicosIndicador_Finalidade_Title',ctrl:'CONTRATOSERVICOSINDICADOR_FINALIDADE',prop:'Title'},{av:'edtContratoServicosIndicador_Meta_Titleformat',ctrl:'CONTRATOSERVICOSINDICADOR_META',prop:'Titleformat'},{av:'edtContratoServicosIndicador_Meta_Title',ctrl:'CONTRATOSERVICOSINDICADOR_META',prop:'Title'},{av:'edtContratoServicosIndicador_InstrumentoMedicao_Titleformat',ctrl:'CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO',prop:'Titleformat'},{av:'edtContratoServicosIndicador_InstrumentoMedicao_Title',ctrl:'CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO',prop:'Title'},{av:'cmbContratoServicosIndicador_Tipo'},{av:'cmbContratoServicosIndicador_Periodicidade'},{av:'edtContratoServicosIndicador_Vigencia_Titleformat',ctrl:'CONTRATOSERVICOSINDICADOR_VIGENCIA',prop:'Titleformat'},{av:'edtContratoServicosIndicador_Vigencia_Title',ctrl:'CONTRATOSERVICOSINDICADOR_VIGENCIA',prop:'Title'},{av:'edtContratoServicosIndicador_QtdeFaixas_Titleformat',ctrl:'CONTRATOSERVICOSINDICADOR_QTDEFAIXAS',prop:'Titleformat'},{av:'edtContratoServicosIndicador_QtdeFaixas_Title',ctrl:'CONTRATOSERVICOSINDICADOR_QTDEFAIXAS',prop:'Title'},{av:'AV85GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV86GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11JG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicador_Indicador1',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosIndicador_Indicador2',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosIndicador_Indicador3',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFContratoServicosIndicador_Codigo',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV33TFContratoServicosIndicador_Codigo_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratoServicosIndicador_CntSrvCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV37TFContratoServicosIndicador_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV40TFContratoServicosIndicador_ContratoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV41TFContratoServicosIndicador_ContratoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratoServicosIndicador_AreaTrabalhoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV45TFContratoServicosIndicador_AreaTrabalhoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratoServicosIndicador_Numero',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO',pic:'ZZZ9',nv:0},{av:'AV49TFContratoServicosIndicador_Numero_To',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO_TO',pic:'ZZZ9',nv:0},{av:'AV52TFContratoServicosIndicador_Indicador',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR',pic:'',nv:''},{av:'AV53TFContratoServicosIndicador_Indicador_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR_SEL',pic:'',nv:''},{av:'AV56TFContratoServicosIndicador_Finalidade',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE',pic:'',nv:''},{av:'AV57TFContratoServicosIndicador_Finalidade_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE_SEL',pic:'',nv:''},{av:'AV60TFContratoServicosIndicador_Meta',fld:'vTFCONTRATOSERVICOSINDICADOR_META',pic:'',nv:''},{av:'AV61TFContratoServicosIndicador_Meta_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_META_SEL',pic:'',nv:''},{av:'AV64TFContratoServicosIndicador_InstrumentoMedicao',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO',pic:'',nv:''},{av:'AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_SEL',pic:'',nv:''},{av:'AV76TFContratoServicosIndicador_Vigencia',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA',pic:'@!',nv:''},{av:'AV77TFContratoServicosIndicador_Vigencia_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA_SEL',pic:'@!',nv:''},{av:'AV80TFContratoServicosIndicador_QtdeFaixas',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS',pic:'ZZZ9',nv:0},{av:'AV81TFContratoServicosIndicador_QtdeFaixas_To',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS_TO',pic:'ZZZ9',nv:0},{av:'AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CNTSRVCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INDICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_FINALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_METATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_VIGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69TFContratoServicosIndicador_Tipo_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_TIPO_SELS',pic:'',nv:null},{av:'AV73TFContratoServicosIndicador_Periodicidade_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_PERIODICIDADE_SELS',pic:'',nv:null},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTRATOSERVICOSINDICADOR_CODIGO.ONOPTIONCLICKED","{handler:'E12JG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicador_Indicador1',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosIndicador_Indicador2',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosIndicador_Indicador3',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFContratoServicosIndicador_Codigo',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV33TFContratoServicosIndicador_Codigo_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratoServicosIndicador_CntSrvCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV37TFContratoServicosIndicador_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV40TFContratoServicosIndicador_ContratoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV41TFContratoServicosIndicador_ContratoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratoServicosIndicador_AreaTrabalhoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV45TFContratoServicosIndicador_AreaTrabalhoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratoServicosIndicador_Numero',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO',pic:'ZZZ9',nv:0},{av:'AV49TFContratoServicosIndicador_Numero_To',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO_TO',pic:'ZZZ9',nv:0},{av:'AV52TFContratoServicosIndicador_Indicador',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR',pic:'',nv:''},{av:'AV53TFContratoServicosIndicador_Indicador_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR_SEL',pic:'',nv:''},{av:'AV56TFContratoServicosIndicador_Finalidade',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE',pic:'',nv:''},{av:'AV57TFContratoServicosIndicador_Finalidade_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE_SEL',pic:'',nv:''},{av:'AV60TFContratoServicosIndicador_Meta',fld:'vTFCONTRATOSERVICOSINDICADOR_META',pic:'',nv:''},{av:'AV61TFContratoServicosIndicador_Meta_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_META_SEL',pic:'',nv:''},{av:'AV64TFContratoServicosIndicador_InstrumentoMedicao',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO',pic:'',nv:''},{av:'AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_SEL',pic:'',nv:''},{av:'AV76TFContratoServicosIndicador_Vigencia',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA',pic:'@!',nv:''},{av:'AV77TFContratoServicosIndicador_Vigencia_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA_SEL',pic:'@!',nv:''},{av:'AV80TFContratoServicosIndicador_QtdeFaixas',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS',pic:'ZZZ9',nv:0},{av:'AV81TFContratoServicosIndicador_QtdeFaixas_To',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS_TO',pic:'ZZZ9',nv:0},{av:'AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CNTSRVCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INDICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_FINALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_METATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_VIGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69TFContratoServicosIndicador_Tipo_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_TIPO_SELS',pic:'',nv:null},{av:'AV73TFContratoServicosIndicador_Periodicidade_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_PERIODICIDADE_SELS',pic:'',nv:null},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratoservicosindicador_codigo_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosindicador_codigo_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CODIGO',prop:'FilteredText_get'},{av:'Ddo_contratoservicosindicador_codigo_Filteredtextto_get',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosindicador_codigo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CODIGO',prop:'SortedStatus'},{av:'AV32TFContratoServicosIndicador_Codigo',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV33TFContratoServicosIndicador_Codigo_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoservicosindicador_cntsrvcod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_contratocod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_areatrabalhocod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_numero_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_indicador_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_INDICADOR',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_finalidade_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_meta_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_META',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_instrumentomedicao_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_tipo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_TIPO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_periodicidade_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_vigencia_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD.ONOPTIONCLICKED","{handler:'E13JG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicador_Indicador1',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosIndicador_Indicador2',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosIndicador_Indicador3',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFContratoServicosIndicador_Codigo',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV33TFContratoServicosIndicador_Codigo_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratoServicosIndicador_CntSrvCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV37TFContratoServicosIndicador_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV40TFContratoServicosIndicador_ContratoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV41TFContratoServicosIndicador_ContratoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratoServicosIndicador_AreaTrabalhoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV45TFContratoServicosIndicador_AreaTrabalhoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratoServicosIndicador_Numero',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO',pic:'ZZZ9',nv:0},{av:'AV49TFContratoServicosIndicador_Numero_To',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO_TO',pic:'ZZZ9',nv:0},{av:'AV52TFContratoServicosIndicador_Indicador',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR',pic:'',nv:''},{av:'AV53TFContratoServicosIndicador_Indicador_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR_SEL',pic:'',nv:''},{av:'AV56TFContratoServicosIndicador_Finalidade',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE',pic:'',nv:''},{av:'AV57TFContratoServicosIndicador_Finalidade_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE_SEL',pic:'',nv:''},{av:'AV60TFContratoServicosIndicador_Meta',fld:'vTFCONTRATOSERVICOSINDICADOR_META',pic:'',nv:''},{av:'AV61TFContratoServicosIndicador_Meta_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_META_SEL',pic:'',nv:''},{av:'AV64TFContratoServicosIndicador_InstrumentoMedicao',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO',pic:'',nv:''},{av:'AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_SEL',pic:'',nv:''},{av:'AV76TFContratoServicosIndicador_Vigencia',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA',pic:'@!',nv:''},{av:'AV77TFContratoServicosIndicador_Vigencia_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA_SEL',pic:'@!',nv:''},{av:'AV80TFContratoServicosIndicador_QtdeFaixas',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS',pic:'ZZZ9',nv:0},{av:'AV81TFContratoServicosIndicador_QtdeFaixas_To',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS_TO',pic:'ZZZ9',nv:0},{av:'AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CNTSRVCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INDICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_FINALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_METATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_VIGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69TFContratoServicosIndicador_Tipo_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_TIPO_SELS',pic:'',nv:null},{av:'AV73TFContratoServicosIndicador_Periodicidade_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_PERIODICIDADE_SELS',pic:'',nv:null},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratoservicosindicador_cntsrvcod_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosindicador_cntsrvcod_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD',prop:'FilteredText_get'},{av:'Ddo_contratoservicosindicador_cntsrvcod_Filteredtextto_get',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosindicador_cntsrvcod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD',prop:'SortedStatus'},{av:'AV36TFContratoServicosIndicador_CntSrvCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV37TFContratoServicosIndicador_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoservicosindicador_codigo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_contratocod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_areatrabalhocod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_numero_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_indicador_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_INDICADOR',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_finalidade_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_meta_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_META',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_instrumentomedicao_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_tipo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_TIPO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_periodicidade_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_vigencia_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD.ONOPTIONCLICKED","{handler:'E14JG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicador_Indicador1',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosIndicador_Indicador2',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosIndicador_Indicador3',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFContratoServicosIndicador_Codigo',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV33TFContratoServicosIndicador_Codigo_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratoServicosIndicador_CntSrvCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV37TFContratoServicosIndicador_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV40TFContratoServicosIndicador_ContratoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV41TFContratoServicosIndicador_ContratoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratoServicosIndicador_AreaTrabalhoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV45TFContratoServicosIndicador_AreaTrabalhoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratoServicosIndicador_Numero',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO',pic:'ZZZ9',nv:0},{av:'AV49TFContratoServicosIndicador_Numero_To',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO_TO',pic:'ZZZ9',nv:0},{av:'AV52TFContratoServicosIndicador_Indicador',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR',pic:'',nv:''},{av:'AV53TFContratoServicosIndicador_Indicador_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR_SEL',pic:'',nv:''},{av:'AV56TFContratoServicosIndicador_Finalidade',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE',pic:'',nv:''},{av:'AV57TFContratoServicosIndicador_Finalidade_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE_SEL',pic:'',nv:''},{av:'AV60TFContratoServicosIndicador_Meta',fld:'vTFCONTRATOSERVICOSINDICADOR_META',pic:'',nv:''},{av:'AV61TFContratoServicosIndicador_Meta_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_META_SEL',pic:'',nv:''},{av:'AV64TFContratoServicosIndicador_InstrumentoMedicao',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO',pic:'',nv:''},{av:'AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_SEL',pic:'',nv:''},{av:'AV76TFContratoServicosIndicador_Vigencia',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA',pic:'@!',nv:''},{av:'AV77TFContratoServicosIndicador_Vigencia_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA_SEL',pic:'@!',nv:''},{av:'AV80TFContratoServicosIndicador_QtdeFaixas',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS',pic:'ZZZ9',nv:0},{av:'AV81TFContratoServicosIndicador_QtdeFaixas_To',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS_TO',pic:'ZZZ9',nv:0},{av:'AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CNTSRVCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INDICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_FINALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_METATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_VIGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69TFContratoServicosIndicador_Tipo_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_TIPO_SELS',pic:'',nv:null},{av:'AV73TFContratoServicosIndicador_Periodicidade_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_PERIODICIDADE_SELS',pic:'',nv:null},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratoservicosindicador_contratocod_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosindicador_contratocod_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD',prop:'FilteredText_get'},{av:'Ddo_contratoservicosindicador_contratocod_Filteredtextto_get',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosindicador_contratocod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD',prop:'SortedStatus'},{av:'AV40TFContratoServicosIndicador_ContratoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV41TFContratoServicosIndicador_ContratoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoservicosindicador_codigo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_cntsrvcod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_areatrabalhocod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_numero_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_indicador_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_INDICADOR',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_finalidade_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_meta_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_META',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_instrumentomedicao_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_tipo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_TIPO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_periodicidade_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_vigencia_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD.ONOPTIONCLICKED","{handler:'E15JG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicador_Indicador1',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosIndicador_Indicador2',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosIndicador_Indicador3',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFContratoServicosIndicador_Codigo',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV33TFContratoServicosIndicador_Codigo_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratoServicosIndicador_CntSrvCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV37TFContratoServicosIndicador_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV40TFContratoServicosIndicador_ContratoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV41TFContratoServicosIndicador_ContratoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratoServicosIndicador_AreaTrabalhoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV45TFContratoServicosIndicador_AreaTrabalhoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratoServicosIndicador_Numero',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO',pic:'ZZZ9',nv:0},{av:'AV49TFContratoServicosIndicador_Numero_To',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO_TO',pic:'ZZZ9',nv:0},{av:'AV52TFContratoServicosIndicador_Indicador',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR',pic:'',nv:''},{av:'AV53TFContratoServicosIndicador_Indicador_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR_SEL',pic:'',nv:''},{av:'AV56TFContratoServicosIndicador_Finalidade',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE',pic:'',nv:''},{av:'AV57TFContratoServicosIndicador_Finalidade_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE_SEL',pic:'',nv:''},{av:'AV60TFContratoServicosIndicador_Meta',fld:'vTFCONTRATOSERVICOSINDICADOR_META',pic:'',nv:''},{av:'AV61TFContratoServicosIndicador_Meta_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_META_SEL',pic:'',nv:''},{av:'AV64TFContratoServicosIndicador_InstrumentoMedicao',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO',pic:'',nv:''},{av:'AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_SEL',pic:'',nv:''},{av:'AV76TFContratoServicosIndicador_Vigencia',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA',pic:'@!',nv:''},{av:'AV77TFContratoServicosIndicador_Vigencia_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA_SEL',pic:'@!',nv:''},{av:'AV80TFContratoServicosIndicador_QtdeFaixas',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS',pic:'ZZZ9',nv:0},{av:'AV81TFContratoServicosIndicador_QtdeFaixas_To',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS_TO',pic:'ZZZ9',nv:0},{av:'AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CNTSRVCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INDICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_FINALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_METATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_VIGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69TFContratoServicosIndicador_Tipo_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_TIPO_SELS',pic:'',nv:null},{av:'AV73TFContratoServicosIndicador_Periodicidade_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_PERIODICIDADE_SELS',pic:'',nv:null},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratoservicosindicador_areatrabalhocod_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosindicador_areatrabalhocod_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',prop:'FilteredText_get'},{av:'Ddo_contratoservicosindicador_areatrabalhocod_Filteredtextto_get',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosindicador_areatrabalhocod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',prop:'SortedStatus'},{av:'AV44TFContratoServicosIndicador_AreaTrabalhoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV45TFContratoServicosIndicador_AreaTrabalhoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoservicosindicador_codigo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_cntsrvcod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_contratocod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_numero_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_indicador_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_INDICADOR',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_finalidade_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_meta_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_META',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_instrumentomedicao_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_tipo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_TIPO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_periodicidade_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_vigencia_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSINDICADOR_NUMERO.ONOPTIONCLICKED","{handler:'E16JG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicador_Indicador1',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosIndicador_Indicador2',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosIndicador_Indicador3',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFContratoServicosIndicador_Codigo',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV33TFContratoServicosIndicador_Codigo_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratoServicosIndicador_CntSrvCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV37TFContratoServicosIndicador_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV40TFContratoServicosIndicador_ContratoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV41TFContratoServicosIndicador_ContratoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratoServicosIndicador_AreaTrabalhoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV45TFContratoServicosIndicador_AreaTrabalhoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratoServicosIndicador_Numero',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO',pic:'ZZZ9',nv:0},{av:'AV49TFContratoServicosIndicador_Numero_To',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO_TO',pic:'ZZZ9',nv:0},{av:'AV52TFContratoServicosIndicador_Indicador',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR',pic:'',nv:''},{av:'AV53TFContratoServicosIndicador_Indicador_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR_SEL',pic:'',nv:''},{av:'AV56TFContratoServicosIndicador_Finalidade',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE',pic:'',nv:''},{av:'AV57TFContratoServicosIndicador_Finalidade_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE_SEL',pic:'',nv:''},{av:'AV60TFContratoServicosIndicador_Meta',fld:'vTFCONTRATOSERVICOSINDICADOR_META',pic:'',nv:''},{av:'AV61TFContratoServicosIndicador_Meta_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_META_SEL',pic:'',nv:''},{av:'AV64TFContratoServicosIndicador_InstrumentoMedicao',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO',pic:'',nv:''},{av:'AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_SEL',pic:'',nv:''},{av:'AV76TFContratoServicosIndicador_Vigencia',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA',pic:'@!',nv:''},{av:'AV77TFContratoServicosIndicador_Vigencia_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA_SEL',pic:'@!',nv:''},{av:'AV80TFContratoServicosIndicador_QtdeFaixas',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS',pic:'ZZZ9',nv:0},{av:'AV81TFContratoServicosIndicador_QtdeFaixas_To',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS_TO',pic:'ZZZ9',nv:0},{av:'AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CNTSRVCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INDICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_FINALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_METATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_VIGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69TFContratoServicosIndicador_Tipo_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_TIPO_SELS',pic:'',nv:null},{av:'AV73TFContratoServicosIndicador_Periodicidade_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_PERIODICIDADE_SELS',pic:'',nv:null},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratoservicosindicador_numero_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_NUMERO',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosindicador_numero_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_NUMERO',prop:'FilteredText_get'},{av:'Ddo_contratoservicosindicador_numero_Filteredtextto_get',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_NUMERO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosindicador_numero_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_NUMERO',prop:'SortedStatus'},{av:'AV48TFContratoServicosIndicador_Numero',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO',pic:'ZZZ9',nv:0},{av:'AV49TFContratoServicosIndicador_Numero_To',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO_TO',pic:'ZZZ9',nv:0},{av:'Ddo_contratoservicosindicador_codigo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_cntsrvcod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_contratocod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_areatrabalhocod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_indicador_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_INDICADOR',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_finalidade_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_meta_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_META',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_instrumentomedicao_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_tipo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_TIPO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_periodicidade_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_vigencia_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSINDICADOR_INDICADOR.ONOPTIONCLICKED","{handler:'E17JG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicador_Indicador1',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosIndicador_Indicador2',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosIndicador_Indicador3',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFContratoServicosIndicador_Codigo',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV33TFContratoServicosIndicador_Codigo_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratoServicosIndicador_CntSrvCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV37TFContratoServicosIndicador_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV40TFContratoServicosIndicador_ContratoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV41TFContratoServicosIndicador_ContratoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratoServicosIndicador_AreaTrabalhoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV45TFContratoServicosIndicador_AreaTrabalhoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratoServicosIndicador_Numero',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO',pic:'ZZZ9',nv:0},{av:'AV49TFContratoServicosIndicador_Numero_To',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO_TO',pic:'ZZZ9',nv:0},{av:'AV52TFContratoServicosIndicador_Indicador',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR',pic:'',nv:''},{av:'AV53TFContratoServicosIndicador_Indicador_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR_SEL',pic:'',nv:''},{av:'AV56TFContratoServicosIndicador_Finalidade',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE',pic:'',nv:''},{av:'AV57TFContratoServicosIndicador_Finalidade_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE_SEL',pic:'',nv:''},{av:'AV60TFContratoServicosIndicador_Meta',fld:'vTFCONTRATOSERVICOSINDICADOR_META',pic:'',nv:''},{av:'AV61TFContratoServicosIndicador_Meta_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_META_SEL',pic:'',nv:''},{av:'AV64TFContratoServicosIndicador_InstrumentoMedicao',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO',pic:'',nv:''},{av:'AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_SEL',pic:'',nv:''},{av:'AV76TFContratoServicosIndicador_Vigencia',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA',pic:'@!',nv:''},{av:'AV77TFContratoServicosIndicador_Vigencia_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA_SEL',pic:'@!',nv:''},{av:'AV80TFContratoServicosIndicador_QtdeFaixas',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS',pic:'ZZZ9',nv:0},{av:'AV81TFContratoServicosIndicador_QtdeFaixas_To',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS_TO',pic:'ZZZ9',nv:0},{av:'AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CNTSRVCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INDICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_FINALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_METATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_VIGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69TFContratoServicosIndicador_Tipo_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_TIPO_SELS',pic:'',nv:null},{av:'AV73TFContratoServicosIndicador_Periodicidade_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_PERIODICIDADE_SELS',pic:'',nv:null},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratoservicosindicador_indicador_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_INDICADOR',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosindicador_indicador_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_INDICADOR',prop:'FilteredText_get'},{av:'Ddo_contratoservicosindicador_indicador_Selectedvalue_get',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_INDICADOR',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosindicador_indicador_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_INDICADOR',prop:'SortedStatus'},{av:'AV52TFContratoServicosIndicador_Indicador',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR',pic:'',nv:''},{av:'AV53TFContratoServicosIndicador_Indicador_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR_SEL',pic:'',nv:''},{av:'Ddo_contratoservicosindicador_codigo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_cntsrvcod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_contratocod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_areatrabalhocod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_numero_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_finalidade_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_meta_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_META',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_instrumentomedicao_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_tipo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_TIPO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_periodicidade_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_vigencia_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE.ONOPTIONCLICKED","{handler:'E18JG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicador_Indicador1',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosIndicador_Indicador2',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosIndicador_Indicador3',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFContratoServicosIndicador_Codigo',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV33TFContratoServicosIndicador_Codigo_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratoServicosIndicador_CntSrvCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV37TFContratoServicosIndicador_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV40TFContratoServicosIndicador_ContratoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV41TFContratoServicosIndicador_ContratoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratoServicosIndicador_AreaTrabalhoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV45TFContratoServicosIndicador_AreaTrabalhoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratoServicosIndicador_Numero',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO',pic:'ZZZ9',nv:0},{av:'AV49TFContratoServicosIndicador_Numero_To',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO_TO',pic:'ZZZ9',nv:0},{av:'AV52TFContratoServicosIndicador_Indicador',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR',pic:'',nv:''},{av:'AV53TFContratoServicosIndicador_Indicador_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR_SEL',pic:'',nv:''},{av:'AV56TFContratoServicosIndicador_Finalidade',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE',pic:'',nv:''},{av:'AV57TFContratoServicosIndicador_Finalidade_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE_SEL',pic:'',nv:''},{av:'AV60TFContratoServicosIndicador_Meta',fld:'vTFCONTRATOSERVICOSINDICADOR_META',pic:'',nv:''},{av:'AV61TFContratoServicosIndicador_Meta_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_META_SEL',pic:'',nv:''},{av:'AV64TFContratoServicosIndicador_InstrumentoMedicao',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO',pic:'',nv:''},{av:'AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_SEL',pic:'',nv:''},{av:'AV76TFContratoServicosIndicador_Vigencia',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA',pic:'@!',nv:''},{av:'AV77TFContratoServicosIndicador_Vigencia_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA_SEL',pic:'@!',nv:''},{av:'AV80TFContratoServicosIndicador_QtdeFaixas',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS',pic:'ZZZ9',nv:0},{av:'AV81TFContratoServicosIndicador_QtdeFaixas_To',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS_TO',pic:'ZZZ9',nv:0},{av:'AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CNTSRVCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INDICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_FINALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_METATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_VIGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69TFContratoServicosIndicador_Tipo_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_TIPO_SELS',pic:'',nv:null},{av:'AV73TFContratoServicosIndicador_Periodicidade_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_PERIODICIDADE_SELS',pic:'',nv:null},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratoservicosindicador_finalidade_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosindicador_finalidade_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE',prop:'FilteredText_get'},{av:'Ddo_contratoservicosindicador_finalidade_Selectedvalue_get',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosindicador_finalidade_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE',prop:'SortedStatus'},{av:'AV56TFContratoServicosIndicador_Finalidade',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE',pic:'',nv:''},{av:'AV57TFContratoServicosIndicador_Finalidade_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE_SEL',pic:'',nv:''},{av:'Ddo_contratoservicosindicador_codigo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_cntsrvcod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_contratocod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_areatrabalhocod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_numero_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_indicador_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_INDICADOR',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_meta_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_META',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_instrumentomedicao_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_tipo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_TIPO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_periodicidade_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_vigencia_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSINDICADOR_META.ONOPTIONCLICKED","{handler:'E19JG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicador_Indicador1',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosIndicador_Indicador2',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosIndicador_Indicador3',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFContratoServicosIndicador_Codigo',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV33TFContratoServicosIndicador_Codigo_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratoServicosIndicador_CntSrvCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV37TFContratoServicosIndicador_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV40TFContratoServicosIndicador_ContratoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV41TFContratoServicosIndicador_ContratoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratoServicosIndicador_AreaTrabalhoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV45TFContratoServicosIndicador_AreaTrabalhoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratoServicosIndicador_Numero',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO',pic:'ZZZ9',nv:0},{av:'AV49TFContratoServicosIndicador_Numero_To',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO_TO',pic:'ZZZ9',nv:0},{av:'AV52TFContratoServicosIndicador_Indicador',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR',pic:'',nv:''},{av:'AV53TFContratoServicosIndicador_Indicador_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR_SEL',pic:'',nv:''},{av:'AV56TFContratoServicosIndicador_Finalidade',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE',pic:'',nv:''},{av:'AV57TFContratoServicosIndicador_Finalidade_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE_SEL',pic:'',nv:''},{av:'AV60TFContratoServicosIndicador_Meta',fld:'vTFCONTRATOSERVICOSINDICADOR_META',pic:'',nv:''},{av:'AV61TFContratoServicosIndicador_Meta_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_META_SEL',pic:'',nv:''},{av:'AV64TFContratoServicosIndicador_InstrumentoMedicao',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO',pic:'',nv:''},{av:'AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_SEL',pic:'',nv:''},{av:'AV76TFContratoServicosIndicador_Vigencia',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA',pic:'@!',nv:''},{av:'AV77TFContratoServicosIndicador_Vigencia_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA_SEL',pic:'@!',nv:''},{av:'AV80TFContratoServicosIndicador_QtdeFaixas',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS',pic:'ZZZ9',nv:0},{av:'AV81TFContratoServicosIndicador_QtdeFaixas_To',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS_TO',pic:'ZZZ9',nv:0},{av:'AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CNTSRVCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INDICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_FINALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_METATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_VIGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69TFContratoServicosIndicador_Tipo_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_TIPO_SELS',pic:'',nv:null},{av:'AV73TFContratoServicosIndicador_Periodicidade_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_PERIODICIDADE_SELS',pic:'',nv:null},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratoservicosindicador_meta_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_META',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosindicador_meta_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_META',prop:'FilteredText_get'},{av:'Ddo_contratoservicosindicador_meta_Selectedvalue_get',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_META',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosindicador_meta_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_META',prop:'SortedStatus'},{av:'AV60TFContratoServicosIndicador_Meta',fld:'vTFCONTRATOSERVICOSINDICADOR_META',pic:'',nv:''},{av:'AV61TFContratoServicosIndicador_Meta_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_META_SEL',pic:'',nv:''},{av:'Ddo_contratoservicosindicador_codigo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_cntsrvcod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_contratocod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_areatrabalhocod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_numero_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_indicador_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_INDICADOR',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_finalidade_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_instrumentomedicao_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_tipo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_TIPO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_periodicidade_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_vigencia_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO.ONOPTIONCLICKED","{handler:'E20JG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicador_Indicador1',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosIndicador_Indicador2',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosIndicador_Indicador3',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFContratoServicosIndicador_Codigo',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV33TFContratoServicosIndicador_Codigo_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratoServicosIndicador_CntSrvCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV37TFContratoServicosIndicador_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV40TFContratoServicosIndicador_ContratoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV41TFContratoServicosIndicador_ContratoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratoServicosIndicador_AreaTrabalhoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV45TFContratoServicosIndicador_AreaTrabalhoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratoServicosIndicador_Numero',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO',pic:'ZZZ9',nv:0},{av:'AV49TFContratoServicosIndicador_Numero_To',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO_TO',pic:'ZZZ9',nv:0},{av:'AV52TFContratoServicosIndicador_Indicador',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR',pic:'',nv:''},{av:'AV53TFContratoServicosIndicador_Indicador_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR_SEL',pic:'',nv:''},{av:'AV56TFContratoServicosIndicador_Finalidade',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE',pic:'',nv:''},{av:'AV57TFContratoServicosIndicador_Finalidade_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE_SEL',pic:'',nv:''},{av:'AV60TFContratoServicosIndicador_Meta',fld:'vTFCONTRATOSERVICOSINDICADOR_META',pic:'',nv:''},{av:'AV61TFContratoServicosIndicador_Meta_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_META_SEL',pic:'',nv:''},{av:'AV64TFContratoServicosIndicador_InstrumentoMedicao',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO',pic:'',nv:''},{av:'AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_SEL',pic:'',nv:''},{av:'AV76TFContratoServicosIndicador_Vigencia',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA',pic:'@!',nv:''},{av:'AV77TFContratoServicosIndicador_Vigencia_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA_SEL',pic:'@!',nv:''},{av:'AV80TFContratoServicosIndicador_QtdeFaixas',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS',pic:'ZZZ9',nv:0},{av:'AV81TFContratoServicosIndicador_QtdeFaixas_To',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS_TO',pic:'ZZZ9',nv:0},{av:'AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CNTSRVCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INDICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_FINALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_METATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_VIGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69TFContratoServicosIndicador_Tipo_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_TIPO_SELS',pic:'',nv:null},{av:'AV73TFContratoServicosIndicador_Periodicidade_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_PERIODICIDADE_SELS',pic:'',nv:null},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratoservicosindicador_instrumentomedicao_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosindicador_instrumentomedicao_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO',prop:'FilteredText_get'},{av:'Ddo_contratoservicosindicador_instrumentomedicao_Selectedvalue_get',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosindicador_instrumentomedicao_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO',prop:'SortedStatus'},{av:'AV64TFContratoServicosIndicador_InstrumentoMedicao',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO',pic:'',nv:''},{av:'AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_SEL',pic:'',nv:''},{av:'Ddo_contratoservicosindicador_codigo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_cntsrvcod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_contratocod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_areatrabalhocod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_numero_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_indicador_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_INDICADOR',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_finalidade_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_meta_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_META',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_tipo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_TIPO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_periodicidade_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_vigencia_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSINDICADOR_TIPO.ONOPTIONCLICKED","{handler:'E21JG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicador_Indicador1',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosIndicador_Indicador2',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosIndicador_Indicador3',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFContratoServicosIndicador_Codigo',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV33TFContratoServicosIndicador_Codigo_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratoServicosIndicador_CntSrvCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV37TFContratoServicosIndicador_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV40TFContratoServicosIndicador_ContratoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV41TFContratoServicosIndicador_ContratoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratoServicosIndicador_AreaTrabalhoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV45TFContratoServicosIndicador_AreaTrabalhoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratoServicosIndicador_Numero',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO',pic:'ZZZ9',nv:0},{av:'AV49TFContratoServicosIndicador_Numero_To',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO_TO',pic:'ZZZ9',nv:0},{av:'AV52TFContratoServicosIndicador_Indicador',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR',pic:'',nv:''},{av:'AV53TFContratoServicosIndicador_Indicador_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR_SEL',pic:'',nv:''},{av:'AV56TFContratoServicosIndicador_Finalidade',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE',pic:'',nv:''},{av:'AV57TFContratoServicosIndicador_Finalidade_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE_SEL',pic:'',nv:''},{av:'AV60TFContratoServicosIndicador_Meta',fld:'vTFCONTRATOSERVICOSINDICADOR_META',pic:'',nv:''},{av:'AV61TFContratoServicosIndicador_Meta_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_META_SEL',pic:'',nv:''},{av:'AV64TFContratoServicosIndicador_InstrumentoMedicao',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO',pic:'',nv:''},{av:'AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_SEL',pic:'',nv:''},{av:'AV76TFContratoServicosIndicador_Vigencia',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA',pic:'@!',nv:''},{av:'AV77TFContratoServicosIndicador_Vigencia_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA_SEL',pic:'@!',nv:''},{av:'AV80TFContratoServicosIndicador_QtdeFaixas',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS',pic:'ZZZ9',nv:0},{av:'AV81TFContratoServicosIndicador_QtdeFaixas_To',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS_TO',pic:'ZZZ9',nv:0},{av:'AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CNTSRVCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INDICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_FINALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_METATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_VIGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69TFContratoServicosIndicador_Tipo_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_TIPO_SELS',pic:'',nv:null},{av:'AV73TFContratoServicosIndicador_Periodicidade_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_PERIODICIDADE_SELS',pic:'',nv:null},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratoservicosindicador_tipo_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_TIPO',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosindicador_tipo_Selectedvalue_get',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_TIPO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosindicador_tipo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_TIPO',prop:'SortedStatus'},{av:'AV69TFContratoServicosIndicador_Tipo_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_TIPO_SELS',pic:'',nv:null},{av:'Ddo_contratoservicosindicador_codigo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_cntsrvcod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_contratocod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_areatrabalhocod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_numero_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_indicador_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_INDICADOR',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_finalidade_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_meta_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_META',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_instrumentomedicao_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_periodicidade_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_vigencia_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE.ONOPTIONCLICKED","{handler:'E22JG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicador_Indicador1',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosIndicador_Indicador2',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosIndicador_Indicador3',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFContratoServicosIndicador_Codigo',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV33TFContratoServicosIndicador_Codigo_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratoServicosIndicador_CntSrvCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV37TFContratoServicosIndicador_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV40TFContratoServicosIndicador_ContratoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV41TFContratoServicosIndicador_ContratoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratoServicosIndicador_AreaTrabalhoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV45TFContratoServicosIndicador_AreaTrabalhoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratoServicosIndicador_Numero',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO',pic:'ZZZ9',nv:0},{av:'AV49TFContratoServicosIndicador_Numero_To',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO_TO',pic:'ZZZ9',nv:0},{av:'AV52TFContratoServicosIndicador_Indicador',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR',pic:'',nv:''},{av:'AV53TFContratoServicosIndicador_Indicador_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR_SEL',pic:'',nv:''},{av:'AV56TFContratoServicosIndicador_Finalidade',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE',pic:'',nv:''},{av:'AV57TFContratoServicosIndicador_Finalidade_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE_SEL',pic:'',nv:''},{av:'AV60TFContratoServicosIndicador_Meta',fld:'vTFCONTRATOSERVICOSINDICADOR_META',pic:'',nv:''},{av:'AV61TFContratoServicosIndicador_Meta_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_META_SEL',pic:'',nv:''},{av:'AV64TFContratoServicosIndicador_InstrumentoMedicao',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO',pic:'',nv:''},{av:'AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_SEL',pic:'',nv:''},{av:'AV76TFContratoServicosIndicador_Vigencia',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA',pic:'@!',nv:''},{av:'AV77TFContratoServicosIndicador_Vigencia_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA_SEL',pic:'@!',nv:''},{av:'AV80TFContratoServicosIndicador_QtdeFaixas',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS',pic:'ZZZ9',nv:0},{av:'AV81TFContratoServicosIndicador_QtdeFaixas_To',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS_TO',pic:'ZZZ9',nv:0},{av:'AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CNTSRVCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INDICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_FINALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_METATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_VIGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69TFContratoServicosIndicador_Tipo_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_TIPO_SELS',pic:'',nv:null},{av:'AV73TFContratoServicosIndicador_Periodicidade_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_PERIODICIDADE_SELS',pic:'',nv:null},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratoservicosindicador_periodicidade_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosindicador_periodicidade_Selectedvalue_get',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosindicador_periodicidade_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE',prop:'SortedStatus'},{av:'AV73TFContratoServicosIndicador_Periodicidade_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_PERIODICIDADE_SELS',pic:'',nv:null},{av:'Ddo_contratoservicosindicador_codigo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_cntsrvcod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_contratocod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_areatrabalhocod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_numero_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_indicador_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_INDICADOR',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_finalidade_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_meta_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_META',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_instrumentomedicao_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_tipo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_TIPO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_vigencia_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA.ONOPTIONCLICKED","{handler:'E23JG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicador_Indicador1',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosIndicador_Indicador2',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosIndicador_Indicador3',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFContratoServicosIndicador_Codigo',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV33TFContratoServicosIndicador_Codigo_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratoServicosIndicador_CntSrvCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV37TFContratoServicosIndicador_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV40TFContratoServicosIndicador_ContratoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV41TFContratoServicosIndicador_ContratoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratoServicosIndicador_AreaTrabalhoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV45TFContratoServicosIndicador_AreaTrabalhoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratoServicosIndicador_Numero',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO',pic:'ZZZ9',nv:0},{av:'AV49TFContratoServicosIndicador_Numero_To',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO_TO',pic:'ZZZ9',nv:0},{av:'AV52TFContratoServicosIndicador_Indicador',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR',pic:'',nv:''},{av:'AV53TFContratoServicosIndicador_Indicador_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR_SEL',pic:'',nv:''},{av:'AV56TFContratoServicosIndicador_Finalidade',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE',pic:'',nv:''},{av:'AV57TFContratoServicosIndicador_Finalidade_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE_SEL',pic:'',nv:''},{av:'AV60TFContratoServicosIndicador_Meta',fld:'vTFCONTRATOSERVICOSINDICADOR_META',pic:'',nv:''},{av:'AV61TFContratoServicosIndicador_Meta_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_META_SEL',pic:'',nv:''},{av:'AV64TFContratoServicosIndicador_InstrumentoMedicao',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO',pic:'',nv:''},{av:'AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_SEL',pic:'',nv:''},{av:'AV76TFContratoServicosIndicador_Vigencia',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA',pic:'@!',nv:''},{av:'AV77TFContratoServicosIndicador_Vigencia_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA_SEL',pic:'@!',nv:''},{av:'AV80TFContratoServicosIndicador_QtdeFaixas',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS',pic:'ZZZ9',nv:0},{av:'AV81TFContratoServicosIndicador_QtdeFaixas_To',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS_TO',pic:'ZZZ9',nv:0},{av:'AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CNTSRVCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INDICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_FINALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_METATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_VIGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69TFContratoServicosIndicador_Tipo_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_TIPO_SELS',pic:'',nv:null},{av:'AV73TFContratoServicosIndicador_Periodicidade_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_PERIODICIDADE_SELS',pic:'',nv:null},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratoservicosindicador_vigencia_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosindicador_vigencia_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA',prop:'FilteredText_get'},{av:'Ddo_contratoservicosindicador_vigencia_Selectedvalue_get',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosindicador_vigencia_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA',prop:'SortedStatus'},{av:'AV76TFContratoServicosIndicador_Vigencia',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA',pic:'@!',nv:''},{av:'AV77TFContratoServicosIndicador_Vigencia_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA_SEL',pic:'@!',nv:''},{av:'Ddo_contratoservicosindicador_codigo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_cntsrvcod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_contratocod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_areatrabalhocod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_numero_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_indicador_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_INDICADOR',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_finalidade_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_meta_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_META',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_instrumentomedicao_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_tipo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_TIPO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicador_periodicidade_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS.ONOPTIONCLICKED","{handler:'E24JG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicador_Indicador1',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosIndicador_Indicador2',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosIndicador_Indicador3',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFContratoServicosIndicador_Codigo',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV33TFContratoServicosIndicador_Codigo_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratoServicosIndicador_CntSrvCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV37TFContratoServicosIndicador_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV40TFContratoServicosIndicador_ContratoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV41TFContratoServicosIndicador_ContratoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratoServicosIndicador_AreaTrabalhoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV45TFContratoServicosIndicador_AreaTrabalhoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratoServicosIndicador_Numero',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO',pic:'ZZZ9',nv:0},{av:'AV49TFContratoServicosIndicador_Numero_To',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO_TO',pic:'ZZZ9',nv:0},{av:'AV52TFContratoServicosIndicador_Indicador',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR',pic:'',nv:''},{av:'AV53TFContratoServicosIndicador_Indicador_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR_SEL',pic:'',nv:''},{av:'AV56TFContratoServicosIndicador_Finalidade',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE',pic:'',nv:''},{av:'AV57TFContratoServicosIndicador_Finalidade_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE_SEL',pic:'',nv:''},{av:'AV60TFContratoServicosIndicador_Meta',fld:'vTFCONTRATOSERVICOSINDICADOR_META',pic:'',nv:''},{av:'AV61TFContratoServicosIndicador_Meta_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_META_SEL',pic:'',nv:''},{av:'AV64TFContratoServicosIndicador_InstrumentoMedicao',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO',pic:'',nv:''},{av:'AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_SEL',pic:'',nv:''},{av:'AV76TFContratoServicosIndicador_Vigencia',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA',pic:'@!',nv:''},{av:'AV77TFContratoServicosIndicador_Vigencia_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA_SEL',pic:'@!',nv:''},{av:'AV80TFContratoServicosIndicador_QtdeFaixas',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS',pic:'ZZZ9',nv:0},{av:'AV81TFContratoServicosIndicador_QtdeFaixas_To',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS_TO',pic:'ZZZ9',nv:0},{av:'AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CNTSRVCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INDICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_FINALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_METATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_VIGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69TFContratoServicosIndicador_Tipo_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_TIPO_SELS',pic:'',nv:null},{av:'AV73TFContratoServicosIndicador_Periodicidade_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_PERIODICIDADE_SELS',pic:'',nv:null},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratoservicosindicador_qtdefaixas_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosindicador_qtdefaixas_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS',prop:'FilteredText_get'},{av:'Ddo_contratoservicosindicador_qtdefaixas_Filteredtextto_get',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS',prop:'FilteredTextTo_get'}],oparms:[{av:'AV80TFContratoServicosIndicador_QtdeFaixas',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS',pic:'ZZZ9',nv:0},{av:'AV81TFContratoServicosIndicador_QtdeFaixas_To',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS_TO',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("GRID.LOAD","{handler:'E37JG2',iparms:[],oparms:[{av:'AV28Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E38JG2',iparms:[{av:'A1269ContratoServicosIndicador_Codigo',fld:'CONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1274ContratoServicosIndicador_Indicador',fld:'CONTRATOSERVICOSINDICADOR_INDICADOR',pic:'',hsh:true,nv:''}],oparms:[{av:'AV7InOutContratoServicosIndicador_Codigo',fld:'vINOUTCONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8InOutContratoServicosIndicador_Indicador',fld:'vINOUTCONTRATOSERVICOSINDICADOR_INDICADOR',pic:'',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E25JG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicador_Indicador1',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosIndicador_Indicador2',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosIndicador_Indicador3',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFContratoServicosIndicador_Codigo',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV33TFContratoServicosIndicador_Codigo_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratoServicosIndicador_CntSrvCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV37TFContratoServicosIndicador_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV40TFContratoServicosIndicador_ContratoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV41TFContratoServicosIndicador_ContratoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratoServicosIndicador_AreaTrabalhoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV45TFContratoServicosIndicador_AreaTrabalhoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratoServicosIndicador_Numero',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO',pic:'ZZZ9',nv:0},{av:'AV49TFContratoServicosIndicador_Numero_To',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO_TO',pic:'ZZZ9',nv:0},{av:'AV52TFContratoServicosIndicador_Indicador',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR',pic:'',nv:''},{av:'AV53TFContratoServicosIndicador_Indicador_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR_SEL',pic:'',nv:''},{av:'AV56TFContratoServicosIndicador_Finalidade',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE',pic:'',nv:''},{av:'AV57TFContratoServicosIndicador_Finalidade_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE_SEL',pic:'',nv:''},{av:'AV60TFContratoServicosIndicador_Meta',fld:'vTFCONTRATOSERVICOSINDICADOR_META',pic:'',nv:''},{av:'AV61TFContratoServicosIndicador_Meta_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_META_SEL',pic:'',nv:''},{av:'AV64TFContratoServicosIndicador_InstrumentoMedicao',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO',pic:'',nv:''},{av:'AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_SEL',pic:'',nv:''},{av:'AV76TFContratoServicosIndicador_Vigencia',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA',pic:'@!',nv:''},{av:'AV77TFContratoServicosIndicador_Vigencia_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA_SEL',pic:'@!',nv:''},{av:'AV80TFContratoServicosIndicador_QtdeFaixas',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS',pic:'ZZZ9',nv:0},{av:'AV81TFContratoServicosIndicador_QtdeFaixas_To',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS_TO',pic:'ZZZ9',nv:0},{av:'AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CNTSRVCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INDICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_FINALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_METATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_VIGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69TFContratoServicosIndicador_Tipo_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_TIPO_SELS',pic:'',nv:null},{av:'AV73TFContratoServicosIndicador_Periodicidade_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_PERIODICIDADE_SELS',pic:'',nv:null},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E30JG2',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E26JG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicador_Indicador1',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosIndicador_Indicador2',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosIndicador_Indicador3',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFContratoServicosIndicador_Codigo',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV33TFContratoServicosIndicador_Codigo_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratoServicosIndicador_CntSrvCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV37TFContratoServicosIndicador_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV40TFContratoServicosIndicador_ContratoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV41TFContratoServicosIndicador_ContratoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratoServicosIndicador_AreaTrabalhoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV45TFContratoServicosIndicador_AreaTrabalhoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratoServicosIndicador_Numero',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO',pic:'ZZZ9',nv:0},{av:'AV49TFContratoServicosIndicador_Numero_To',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO_TO',pic:'ZZZ9',nv:0},{av:'AV52TFContratoServicosIndicador_Indicador',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR',pic:'',nv:''},{av:'AV53TFContratoServicosIndicador_Indicador_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR_SEL',pic:'',nv:''},{av:'AV56TFContratoServicosIndicador_Finalidade',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE',pic:'',nv:''},{av:'AV57TFContratoServicosIndicador_Finalidade_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE_SEL',pic:'',nv:''},{av:'AV60TFContratoServicosIndicador_Meta',fld:'vTFCONTRATOSERVICOSINDICADOR_META',pic:'',nv:''},{av:'AV61TFContratoServicosIndicador_Meta_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_META_SEL',pic:'',nv:''},{av:'AV64TFContratoServicosIndicador_InstrumentoMedicao',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO',pic:'',nv:''},{av:'AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_SEL',pic:'',nv:''},{av:'AV76TFContratoServicosIndicador_Vigencia',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA',pic:'@!',nv:''},{av:'AV77TFContratoServicosIndicador_Vigencia_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA_SEL',pic:'@!',nv:''},{av:'AV80TFContratoServicosIndicador_QtdeFaixas',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS',pic:'ZZZ9',nv:0},{av:'AV81TFContratoServicosIndicador_QtdeFaixas_To',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS_TO',pic:'ZZZ9',nv:0},{av:'AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CNTSRVCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INDICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_FINALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_METATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_VIGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69TFContratoServicosIndicador_Tipo_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_TIPO_SELS',pic:'',nv:null},{av:'AV73TFContratoServicosIndicador_Periodicidade_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_PERIODICIDADE_SELS',pic:'',nv:null},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosIndicador_Indicador2',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosIndicador_Indicador3',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicador_Indicador1',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavContratoservicosindicador_indicador2_Visible',ctrl:'vCONTRATOSERVICOSINDICADOR_INDICADOR2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContratoservicosindicador_indicador3_Visible',ctrl:'vCONTRATOSERVICOSINDICADOR_INDICADOR3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContratoservicosindicador_indicador1_Visible',ctrl:'vCONTRATOSERVICOSINDICADOR_INDICADOR1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E31JG2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavContratoservicosindicador_indicador1_Visible',ctrl:'vCONTRATOSERVICOSINDICADOR_INDICADOR1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E32JG2',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E27JG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicador_Indicador1',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosIndicador_Indicador2',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosIndicador_Indicador3',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFContratoServicosIndicador_Codigo',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV33TFContratoServicosIndicador_Codigo_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratoServicosIndicador_CntSrvCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV37TFContratoServicosIndicador_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV40TFContratoServicosIndicador_ContratoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV41TFContratoServicosIndicador_ContratoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratoServicosIndicador_AreaTrabalhoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV45TFContratoServicosIndicador_AreaTrabalhoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratoServicosIndicador_Numero',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO',pic:'ZZZ9',nv:0},{av:'AV49TFContratoServicosIndicador_Numero_To',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO_TO',pic:'ZZZ9',nv:0},{av:'AV52TFContratoServicosIndicador_Indicador',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR',pic:'',nv:''},{av:'AV53TFContratoServicosIndicador_Indicador_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR_SEL',pic:'',nv:''},{av:'AV56TFContratoServicosIndicador_Finalidade',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE',pic:'',nv:''},{av:'AV57TFContratoServicosIndicador_Finalidade_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE_SEL',pic:'',nv:''},{av:'AV60TFContratoServicosIndicador_Meta',fld:'vTFCONTRATOSERVICOSINDICADOR_META',pic:'',nv:''},{av:'AV61TFContratoServicosIndicador_Meta_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_META_SEL',pic:'',nv:''},{av:'AV64TFContratoServicosIndicador_InstrumentoMedicao',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO',pic:'',nv:''},{av:'AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_SEL',pic:'',nv:''},{av:'AV76TFContratoServicosIndicador_Vigencia',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA',pic:'@!',nv:''},{av:'AV77TFContratoServicosIndicador_Vigencia_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA_SEL',pic:'@!',nv:''},{av:'AV80TFContratoServicosIndicador_QtdeFaixas',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS',pic:'ZZZ9',nv:0},{av:'AV81TFContratoServicosIndicador_QtdeFaixas_To',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS_TO',pic:'ZZZ9',nv:0},{av:'AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CNTSRVCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INDICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_FINALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_METATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_VIGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69TFContratoServicosIndicador_Tipo_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_TIPO_SELS',pic:'',nv:null},{av:'AV73TFContratoServicosIndicador_Periodicidade_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_PERIODICIDADE_SELS',pic:'',nv:null},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosIndicador_Indicador2',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosIndicador_Indicador3',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicador_Indicador1',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavContratoservicosindicador_indicador2_Visible',ctrl:'vCONTRATOSERVICOSINDICADOR_INDICADOR2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContratoservicosindicador_indicador3_Visible',ctrl:'vCONTRATOSERVICOSINDICADOR_INDICADOR3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContratoservicosindicador_indicador1_Visible',ctrl:'vCONTRATOSERVICOSINDICADOR_INDICADOR1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E33JG2',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavContratoservicosindicador_indicador2_Visible',ctrl:'vCONTRATOSERVICOSINDICADOR_INDICADOR2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E28JG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicador_Indicador1',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosIndicador_Indicador2',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosIndicador_Indicador3',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFContratoServicosIndicador_Codigo',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV33TFContratoServicosIndicador_Codigo_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratoServicosIndicador_CntSrvCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV37TFContratoServicosIndicador_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV40TFContratoServicosIndicador_ContratoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV41TFContratoServicosIndicador_ContratoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratoServicosIndicador_AreaTrabalhoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV45TFContratoServicosIndicador_AreaTrabalhoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratoServicosIndicador_Numero',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO',pic:'ZZZ9',nv:0},{av:'AV49TFContratoServicosIndicador_Numero_To',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO_TO',pic:'ZZZ9',nv:0},{av:'AV52TFContratoServicosIndicador_Indicador',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR',pic:'',nv:''},{av:'AV53TFContratoServicosIndicador_Indicador_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR_SEL',pic:'',nv:''},{av:'AV56TFContratoServicosIndicador_Finalidade',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE',pic:'',nv:''},{av:'AV57TFContratoServicosIndicador_Finalidade_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE_SEL',pic:'',nv:''},{av:'AV60TFContratoServicosIndicador_Meta',fld:'vTFCONTRATOSERVICOSINDICADOR_META',pic:'',nv:''},{av:'AV61TFContratoServicosIndicador_Meta_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_META_SEL',pic:'',nv:''},{av:'AV64TFContratoServicosIndicador_InstrumentoMedicao',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO',pic:'',nv:''},{av:'AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_SEL',pic:'',nv:''},{av:'AV76TFContratoServicosIndicador_Vigencia',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA',pic:'@!',nv:''},{av:'AV77TFContratoServicosIndicador_Vigencia_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA_SEL',pic:'@!',nv:''},{av:'AV80TFContratoServicosIndicador_QtdeFaixas',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS',pic:'ZZZ9',nv:0},{av:'AV81TFContratoServicosIndicador_QtdeFaixas_To',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS_TO',pic:'ZZZ9',nv:0},{av:'AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CNTSRVCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INDICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_FINALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_METATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_VIGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69TFContratoServicosIndicador_Tipo_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_TIPO_SELS',pic:'',nv:null},{av:'AV73TFContratoServicosIndicador_Periodicidade_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_PERIODICIDADE_SELS',pic:'',nv:null},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosIndicador_Indicador2',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosIndicador_Indicador3',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicador_Indicador1',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavContratoservicosindicador_indicador2_Visible',ctrl:'vCONTRATOSERVICOSINDICADOR_INDICADOR2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContratoservicosindicador_indicador3_Visible',ctrl:'vCONTRATOSERVICOSINDICADOR_INDICADOR3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContratoservicosindicador_indicador1_Visible',ctrl:'vCONTRATOSERVICOSINDICADOR_INDICADOR1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E34JG2',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavContratoservicosindicador_indicador3_Visible',ctrl:'vCONTRATOSERVICOSINDICADOR_INDICADOR3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E29JG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicador_Indicador1',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosIndicador_Indicador2',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosIndicador_Indicador3',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFContratoServicosIndicador_Codigo',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV33TFContratoServicosIndicador_Codigo_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratoServicosIndicador_CntSrvCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV37TFContratoServicosIndicador_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV40TFContratoServicosIndicador_ContratoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV41TFContratoServicosIndicador_ContratoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratoServicosIndicador_AreaTrabalhoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV45TFContratoServicosIndicador_AreaTrabalhoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratoServicosIndicador_Numero',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO',pic:'ZZZ9',nv:0},{av:'AV49TFContratoServicosIndicador_Numero_To',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO_TO',pic:'ZZZ9',nv:0},{av:'AV52TFContratoServicosIndicador_Indicador',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR',pic:'',nv:''},{av:'AV53TFContratoServicosIndicador_Indicador_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR_SEL',pic:'',nv:''},{av:'AV56TFContratoServicosIndicador_Finalidade',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE',pic:'',nv:''},{av:'AV57TFContratoServicosIndicador_Finalidade_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE_SEL',pic:'',nv:''},{av:'AV60TFContratoServicosIndicador_Meta',fld:'vTFCONTRATOSERVICOSINDICADOR_META',pic:'',nv:''},{av:'AV61TFContratoServicosIndicador_Meta_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_META_SEL',pic:'',nv:''},{av:'AV64TFContratoServicosIndicador_InstrumentoMedicao',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO',pic:'',nv:''},{av:'AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_SEL',pic:'',nv:''},{av:'AV76TFContratoServicosIndicador_Vigencia',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA',pic:'@!',nv:''},{av:'AV77TFContratoServicosIndicador_Vigencia_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA_SEL',pic:'@!',nv:''},{av:'AV80TFContratoServicosIndicador_QtdeFaixas',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS',pic:'ZZZ9',nv:0},{av:'AV81TFContratoServicosIndicador_QtdeFaixas_To',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS_TO',pic:'ZZZ9',nv:0},{av:'AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CNTSRVCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_CONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INDICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_FINALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_METATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_VIGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69TFContratoServicosIndicador_Tipo_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_TIPO_SELS',pic:'',nv:null},{av:'AV73TFContratoServicosIndicador_Periodicidade_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_PERIODICIDADE_SELS',pic:'',nv:null},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV32TFContratoServicosIndicador_Codigo',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoservicosindicador_codigo_Filteredtext_set',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CODIGO',prop:'FilteredText_set'},{av:'AV33TFContratoServicosIndicador_Codigo_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoservicosindicador_codigo_Filteredtextto_set',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CODIGO',prop:'FilteredTextTo_set'},{av:'AV36TFContratoServicosIndicador_CntSrvCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoservicosindicador_cntsrvcod_Filteredtext_set',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD',prop:'FilteredText_set'},{av:'AV37TFContratoServicosIndicador_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoservicosindicador_cntsrvcod_Filteredtextto_set',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CNTSRVCOD',prop:'FilteredTextTo_set'},{av:'AV40TFContratoServicosIndicador_ContratoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoservicosindicador_contratocod_Filteredtext_set',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD',prop:'FilteredText_set'},{av:'AV41TFContratoServicosIndicador_ContratoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoservicosindicador_contratocod_Filteredtextto_set',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_CONTRATOCOD',prop:'FilteredTextTo_set'},{av:'AV44TFContratoServicosIndicador_AreaTrabalhoCod',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoservicosindicador_areatrabalhocod_Filteredtext_set',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',prop:'FilteredText_set'},{av:'AV45TFContratoServicosIndicador_AreaTrabalhoCod_To',fld:'vTFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoservicosindicador_areatrabalhocod_Filteredtextto_set',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',prop:'FilteredTextTo_set'},{av:'AV48TFContratoServicosIndicador_Numero',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO',pic:'ZZZ9',nv:0},{av:'Ddo_contratoservicosindicador_numero_Filteredtext_set',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_NUMERO',prop:'FilteredText_set'},{av:'AV49TFContratoServicosIndicador_Numero_To',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO_TO',pic:'ZZZ9',nv:0},{av:'Ddo_contratoservicosindicador_numero_Filteredtextto_set',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_NUMERO',prop:'FilteredTextTo_set'},{av:'AV52TFContratoServicosIndicador_Indicador',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR',pic:'',nv:''},{av:'Ddo_contratoservicosindicador_indicador_Filteredtext_set',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_INDICADOR',prop:'FilteredText_set'},{av:'AV53TFContratoServicosIndicador_Indicador_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR_SEL',pic:'',nv:''},{av:'Ddo_contratoservicosindicador_indicador_Selectedvalue_set',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_INDICADOR',prop:'SelectedValue_set'},{av:'AV56TFContratoServicosIndicador_Finalidade',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE',pic:'',nv:''},{av:'Ddo_contratoservicosindicador_finalidade_Filteredtext_set',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE',prop:'FilteredText_set'},{av:'AV57TFContratoServicosIndicador_Finalidade_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_FINALIDADE_SEL',pic:'',nv:''},{av:'Ddo_contratoservicosindicador_finalidade_Selectedvalue_set',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE',prop:'SelectedValue_set'},{av:'AV60TFContratoServicosIndicador_Meta',fld:'vTFCONTRATOSERVICOSINDICADOR_META',pic:'',nv:''},{av:'Ddo_contratoservicosindicador_meta_Filteredtext_set',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_META',prop:'FilteredText_set'},{av:'AV61TFContratoServicosIndicador_Meta_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_META_SEL',pic:'',nv:''},{av:'Ddo_contratoservicosindicador_meta_Selectedvalue_set',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_META',prop:'SelectedValue_set'},{av:'AV64TFContratoServicosIndicador_InstrumentoMedicao',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO',pic:'',nv:''},{av:'Ddo_contratoservicosindicador_instrumentomedicao_Filteredtext_set',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO',prop:'FilteredText_set'},{av:'AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_SEL',pic:'',nv:''},{av:'Ddo_contratoservicosindicador_instrumentomedicao_Selectedvalue_set',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO',prop:'SelectedValue_set'},{av:'AV69TFContratoServicosIndicador_Tipo_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_TIPO_SELS',pic:'',nv:null},{av:'Ddo_contratoservicosindicador_tipo_Selectedvalue_set',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_TIPO',prop:'SelectedValue_set'},{av:'AV73TFContratoServicosIndicador_Periodicidade_Sels',fld:'vTFCONTRATOSERVICOSINDICADOR_PERIODICIDADE_SELS',pic:'',nv:null},{av:'Ddo_contratoservicosindicador_periodicidade_Selectedvalue_set',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_PERIODICIDADE',prop:'SelectedValue_set'},{av:'AV76TFContratoServicosIndicador_Vigencia',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA',pic:'@!',nv:''},{av:'Ddo_contratoservicosindicador_vigencia_Filteredtext_set',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA',prop:'FilteredText_set'},{av:'AV77TFContratoServicosIndicador_Vigencia_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_VIGENCIA_SEL',pic:'@!',nv:''},{av:'Ddo_contratoservicosindicador_vigencia_Selectedvalue_set',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA',prop:'SelectedValue_set'},{av:'AV80TFContratoServicosIndicador_QtdeFaixas',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS',pic:'ZZZ9',nv:0},{av:'Ddo_contratoservicosindicador_qtdefaixas_Filteredtext_set',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS',prop:'FilteredText_set'},{av:'AV81TFContratoServicosIndicador_QtdeFaixas_To',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS_TO',pic:'ZZZ9',nv:0},{av:'Ddo_contratoservicosindicador_qtdefaixas_Filteredtextto_set',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS',prop:'FilteredTextTo_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicador_Indicador1',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR1',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavContratoservicosindicador_indicador1_Visible',ctrl:'vCONTRATOSERVICOSINDICADOR_INDICADOR1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosIndicador_Indicador2',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosIndicador_Indicador3',fld:'vCONTRATOSERVICOSINDICADOR_INDICADOR3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavContratoservicosindicador_indicador2_Visible',ctrl:'vCONTRATOSERVICOSINDICADOR_INDICADOR2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContratoservicosindicador_indicador3_Visible',ctrl:'vCONTRATOSERVICOSINDICADOR_INDICADOR3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV8InOutContratoServicosIndicador_Indicador = "";
         Gridpaginationbar_Selectedpage = "";
         Ddo_contratoservicosindicador_codigo_Activeeventkey = "";
         Ddo_contratoservicosindicador_codigo_Filteredtext_get = "";
         Ddo_contratoservicosindicador_codigo_Filteredtextto_get = "";
         Ddo_contratoservicosindicador_cntsrvcod_Activeeventkey = "";
         Ddo_contratoservicosindicador_cntsrvcod_Filteredtext_get = "";
         Ddo_contratoservicosindicador_cntsrvcod_Filteredtextto_get = "";
         Ddo_contratoservicosindicador_contratocod_Activeeventkey = "";
         Ddo_contratoservicosindicador_contratocod_Filteredtext_get = "";
         Ddo_contratoservicosindicador_contratocod_Filteredtextto_get = "";
         Ddo_contratoservicosindicador_areatrabalhocod_Activeeventkey = "";
         Ddo_contratoservicosindicador_areatrabalhocod_Filteredtext_get = "";
         Ddo_contratoservicosindicador_areatrabalhocod_Filteredtextto_get = "";
         Ddo_contratoservicosindicador_numero_Activeeventkey = "";
         Ddo_contratoservicosindicador_numero_Filteredtext_get = "";
         Ddo_contratoservicosindicador_numero_Filteredtextto_get = "";
         Ddo_contratoservicosindicador_indicador_Activeeventkey = "";
         Ddo_contratoservicosindicador_indicador_Filteredtext_get = "";
         Ddo_contratoservicosindicador_indicador_Selectedvalue_get = "";
         Ddo_contratoservicosindicador_finalidade_Activeeventkey = "";
         Ddo_contratoservicosindicador_finalidade_Filteredtext_get = "";
         Ddo_contratoservicosindicador_finalidade_Selectedvalue_get = "";
         Ddo_contratoservicosindicador_meta_Activeeventkey = "";
         Ddo_contratoservicosindicador_meta_Filteredtext_get = "";
         Ddo_contratoservicosindicador_meta_Selectedvalue_get = "";
         Ddo_contratoservicosindicador_instrumentomedicao_Activeeventkey = "";
         Ddo_contratoservicosindicador_instrumentomedicao_Filteredtext_get = "";
         Ddo_contratoservicosindicador_instrumentomedicao_Selectedvalue_get = "";
         Ddo_contratoservicosindicador_tipo_Activeeventkey = "";
         Ddo_contratoservicosindicador_tipo_Selectedvalue_get = "";
         Ddo_contratoservicosindicador_periodicidade_Activeeventkey = "";
         Ddo_contratoservicosindicador_periodicidade_Selectedvalue_get = "";
         Ddo_contratoservicosindicador_vigencia_Activeeventkey = "";
         Ddo_contratoservicosindicador_vigencia_Filteredtext_get = "";
         Ddo_contratoservicosindicador_vigencia_Selectedvalue_get = "";
         Ddo_contratoservicosindicador_qtdefaixas_Activeeventkey = "";
         Ddo_contratoservicosindicador_qtdefaixas_Filteredtext_get = "";
         Ddo_contratoservicosindicador_qtdefaixas_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17ContratoServicosIndicador_Indicador1 = "";
         AV19DynamicFiltersSelector2 = "";
         AV21ContratoServicosIndicador_Indicador2 = "";
         AV23DynamicFiltersSelector3 = "";
         AV25ContratoServicosIndicador_Indicador3 = "";
         AV52TFContratoServicosIndicador_Indicador = "";
         AV53TFContratoServicosIndicador_Indicador_Sel = "";
         AV56TFContratoServicosIndicador_Finalidade = "";
         AV57TFContratoServicosIndicador_Finalidade_Sel = "";
         AV60TFContratoServicosIndicador_Meta = "";
         AV61TFContratoServicosIndicador_Meta_Sel = "";
         AV64TFContratoServicosIndicador_InstrumentoMedicao = "";
         AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel = "";
         AV76TFContratoServicosIndicador_Vigencia = "";
         AV77TFContratoServicosIndicador_Vigencia_Sel = "";
         AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace = "";
         AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace = "";
         AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace = "";
         AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace = "";
         AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace = "";
         AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace = "";
         AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace = "";
         AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace = "";
         AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace = "";
         AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace = "";
         AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace = "";
         AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace = "";
         AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace = "";
         AV69TFContratoServicosIndicador_Tipo_Sels = new GxSimpleCollection();
         AV73TFContratoServicosIndicador_Periodicidade_Sels = new GxSimpleCollection();
         AV90Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV83DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV31ContratoServicosIndicador_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV35ContratoServicosIndicador_CntSrvCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV39ContratoServicosIndicador_ContratoCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV43ContratoServicosIndicador_AreaTrabalhoCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV47ContratoServicosIndicador_NumeroTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV51ContratoServicosIndicador_IndicadorTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV55ContratoServicosIndicador_FinalidadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV59ContratoServicosIndicador_MetaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV63ContratoServicosIndicador_InstrumentoMedicaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV67ContratoServicosIndicador_TipoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV71ContratoServicosIndicador_PeriodicidadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV75ContratoServicosIndicador_VigenciaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV79ContratoServicosIndicador_QtdeFaixasTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_contratoservicosindicador_codigo_Filteredtext_set = "";
         Ddo_contratoservicosindicador_codigo_Filteredtextto_set = "";
         Ddo_contratoservicosindicador_codigo_Sortedstatus = "";
         Ddo_contratoservicosindicador_cntsrvcod_Filteredtext_set = "";
         Ddo_contratoservicosindicador_cntsrvcod_Filteredtextto_set = "";
         Ddo_contratoservicosindicador_cntsrvcod_Sortedstatus = "";
         Ddo_contratoservicosindicador_contratocod_Filteredtext_set = "";
         Ddo_contratoservicosindicador_contratocod_Filteredtextto_set = "";
         Ddo_contratoservicosindicador_contratocod_Sortedstatus = "";
         Ddo_contratoservicosindicador_areatrabalhocod_Filteredtext_set = "";
         Ddo_contratoservicosindicador_areatrabalhocod_Filteredtextto_set = "";
         Ddo_contratoservicosindicador_areatrabalhocod_Sortedstatus = "";
         Ddo_contratoservicosindicador_numero_Filteredtext_set = "";
         Ddo_contratoservicosindicador_numero_Filteredtextto_set = "";
         Ddo_contratoservicosindicador_numero_Sortedstatus = "";
         Ddo_contratoservicosindicador_indicador_Filteredtext_set = "";
         Ddo_contratoservicosindicador_indicador_Selectedvalue_set = "";
         Ddo_contratoservicosindicador_indicador_Sortedstatus = "";
         Ddo_contratoservicosindicador_finalidade_Filteredtext_set = "";
         Ddo_contratoservicosindicador_finalidade_Selectedvalue_set = "";
         Ddo_contratoservicosindicador_finalidade_Sortedstatus = "";
         Ddo_contratoservicosindicador_meta_Filteredtext_set = "";
         Ddo_contratoservicosindicador_meta_Selectedvalue_set = "";
         Ddo_contratoservicosindicador_meta_Sortedstatus = "";
         Ddo_contratoservicosindicador_instrumentomedicao_Filteredtext_set = "";
         Ddo_contratoservicosindicador_instrumentomedicao_Selectedvalue_set = "";
         Ddo_contratoservicosindicador_instrumentomedicao_Sortedstatus = "";
         Ddo_contratoservicosindicador_tipo_Selectedvalue_set = "";
         Ddo_contratoservicosindicador_tipo_Sortedstatus = "";
         Ddo_contratoservicosindicador_periodicidade_Selectedvalue_set = "";
         Ddo_contratoservicosindicador_periodicidade_Sortedstatus = "";
         Ddo_contratoservicosindicador_vigencia_Filteredtext_set = "";
         Ddo_contratoservicosindicador_vigencia_Selectedvalue_set = "";
         Ddo_contratoservicosindicador_vigencia_Sortedstatus = "";
         Ddo_contratoservicosindicador_qtdefaixas_Filteredtext_set = "";
         Ddo_contratoservicosindicador_qtdefaixas_Filteredtextto_set = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Select = "";
         AV89Select_GXI = "";
         A1274ContratoServicosIndicador_Indicador = "";
         A1305ContratoServicosIndicador_Finalidade = "";
         A1306ContratoServicosIndicador_Meta = "";
         A1307ContratoServicosIndicador_InstrumentoMedicao = "";
         A1308ContratoServicosIndicador_Tipo = "";
         A1309ContratoServicosIndicador_Periodicidade = "";
         A1310ContratoServicosIndicador_Vigencia = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV17ContratoServicosIndicador_Indicador1 = "";
         lV21ContratoServicosIndicador_Indicador2 = "";
         lV25ContratoServicosIndicador_Indicador3 = "";
         lV52TFContratoServicosIndicador_Indicador = "";
         lV56TFContratoServicosIndicador_Finalidade = "";
         lV60TFContratoServicosIndicador_Meta = "";
         lV64TFContratoServicosIndicador_InstrumentoMedicao = "";
         lV76TFContratoServicosIndicador_Vigencia = "";
         H00JG3_A1310ContratoServicosIndicador_Vigencia = new String[] {""} ;
         H00JG3_n1310ContratoServicosIndicador_Vigencia = new bool[] {false} ;
         H00JG3_A1309ContratoServicosIndicador_Periodicidade = new String[] {""} ;
         H00JG3_n1309ContratoServicosIndicador_Periodicidade = new bool[] {false} ;
         H00JG3_A1308ContratoServicosIndicador_Tipo = new String[] {""} ;
         H00JG3_n1308ContratoServicosIndicador_Tipo = new bool[] {false} ;
         H00JG3_A1307ContratoServicosIndicador_InstrumentoMedicao = new String[] {""} ;
         H00JG3_n1307ContratoServicosIndicador_InstrumentoMedicao = new bool[] {false} ;
         H00JG3_A1306ContratoServicosIndicador_Meta = new String[] {""} ;
         H00JG3_n1306ContratoServicosIndicador_Meta = new bool[] {false} ;
         H00JG3_A1305ContratoServicosIndicador_Finalidade = new String[] {""} ;
         H00JG3_n1305ContratoServicosIndicador_Finalidade = new bool[] {false} ;
         H00JG3_A1274ContratoServicosIndicador_Indicador = new String[] {""} ;
         H00JG3_A1271ContratoServicosIndicador_Numero = new short[1] ;
         H00JG3_A1295ContratoServicosIndicador_AreaTrabalhoCod = new int[1] ;
         H00JG3_n1295ContratoServicosIndicador_AreaTrabalhoCod = new bool[] {false} ;
         H00JG3_A1296ContratoServicosIndicador_ContratoCod = new int[1] ;
         H00JG3_n1296ContratoServicosIndicador_ContratoCod = new bool[] {false} ;
         H00JG3_A1270ContratoServicosIndicador_CntSrvCod = new int[1] ;
         H00JG3_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         H00JG3_A1298ContratoServicosIndicador_QtdeFaixas = new short[1] ;
         H00JG3_n1298ContratoServicosIndicador_QtdeFaixas = new bool[] {false} ;
         H00JG5_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV68TFContratoServicosIndicador_Tipo_SelsJson = "";
         AV72TFContratoServicosIndicador_Periodicidade_SelsJson = "";
         GridRow = new GXWebRow();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptcontratoservicosindicador__default(),
            new Object[][] {
                new Object[] {
               H00JG3_A1310ContratoServicosIndicador_Vigencia, H00JG3_n1310ContratoServicosIndicador_Vigencia, H00JG3_A1309ContratoServicosIndicador_Periodicidade, H00JG3_n1309ContratoServicosIndicador_Periodicidade, H00JG3_A1308ContratoServicosIndicador_Tipo, H00JG3_n1308ContratoServicosIndicador_Tipo, H00JG3_A1307ContratoServicosIndicador_InstrumentoMedicao, H00JG3_n1307ContratoServicosIndicador_InstrumentoMedicao, H00JG3_A1306ContratoServicosIndicador_Meta, H00JG3_n1306ContratoServicosIndicador_Meta,
               H00JG3_A1305ContratoServicosIndicador_Finalidade, H00JG3_n1305ContratoServicosIndicador_Finalidade, H00JG3_A1274ContratoServicosIndicador_Indicador, H00JG3_A1271ContratoServicosIndicador_Numero, H00JG3_A1295ContratoServicosIndicador_AreaTrabalhoCod, H00JG3_n1295ContratoServicosIndicador_AreaTrabalhoCod, H00JG3_A1296ContratoServicosIndicador_ContratoCod, H00JG3_n1296ContratoServicosIndicador_ContratoCod, H00JG3_A1270ContratoServicosIndicador_CntSrvCod, H00JG3_A1269ContratoServicosIndicador_Codigo,
               H00JG3_A1298ContratoServicosIndicador_QtdeFaixas, H00JG3_n1298ContratoServicosIndicador_QtdeFaixas
               }
               , new Object[] {
               H00JG5_AGRID_nRecordCount
               }
            }
         );
         AV90Pgmname = "PromptContratoServicosIndicador";
         /* GeneXus formulas. */
         AV90Pgmname = "PromptContratoServicosIndicador";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_80 ;
      private short nGXsfl_80_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV20DynamicFiltersOperator2 ;
      private short AV24DynamicFiltersOperator3 ;
      private short AV48TFContratoServicosIndicador_Numero ;
      private short AV49TFContratoServicosIndicador_Numero_To ;
      private short AV80TFContratoServicosIndicador_QtdeFaixas ;
      private short AV81TFContratoServicosIndicador_QtdeFaixas_To ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A1271ContratoServicosIndicador_Numero ;
      private short A1298ContratoServicosIndicador_QtdeFaixas ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_80_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContratoServicosIndicador_Codigo_Titleformat ;
      private short edtContratoServicosIndicador_CntSrvCod_Titleformat ;
      private short edtContratoServicosIndicador_ContratoCod_Titleformat ;
      private short edtContratoServicosIndicador_AreaTrabalhoCod_Titleformat ;
      private short edtContratoServicosIndicador_Numero_Titleformat ;
      private short edtContratoServicosIndicador_Indicador_Titleformat ;
      private short edtContratoServicosIndicador_Finalidade_Titleformat ;
      private short edtContratoServicosIndicador_Meta_Titleformat ;
      private short edtContratoServicosIndicador_InstrumentoMedicao_Titleformat ;
      private short cmbContratoServicosIndicador_Tipo_Titleformat ;
      private short cmbContratoServicosIndicador_Periodicidade_Titleformat ;
      private short edtContratoServicosIndicador_Vigencia_Titleformat ;
      private short edtContratoServicosIndicador_QtdeFaixas_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutContratoServicosIndicador_Codigo ;
      private int wcpOAV7InOutContratoServicosIndicador_Codigo ;
      private int subGrid_Rows ;
      private int AV32TFContratoServicosIndicador_Codigo ;
      private int AV33TFContratoServicosIndicador_Codigo_To ;
      private int AV36TFContratoServicosIndicador_CntSrvCod ;
      private int AV37TFContratoServicosIndicador_CntSrvCod_To ;
      private int AV40TFContratoServicosIndicador_ContratoCod ;
      private int AV41TFContratoServicosIndicador_ContratoCod_To ;
      private int AV44TFContratoServicosIndicador_AreaTrabalhoCod ;
      private int AV45TFContratoServicosIndicador_AreaTrabalhoCod_To ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_contratoservicosindicador_indicador_Datalistupdateminimumcharacters ;
      private int Ddo_contratoservicosindicador_finalidade_Datalistupdateminimumcharacters ;
      private int Ddo_contratoservicosindicador_meta_Datalistupdateminimumcharacters ;
      private int Ddo_contratoservicosindicador_instrumentomedicao_Datalistupdateminimumcharacters ;
      private int Ddo_contratoservicosindicador_vigencia_Datalistupdateminimumcharacters ;
      private int edtavTfcontratoservicosindicador_codigo_Visible ;
      private int edtavTfcontratoservicosindicador_codigo_to_Visible ;
      private int edtavTfcontratoservicosindicador_cntsrvcod_Visible ;
      private int edtavTfcontratoservicosindicador_cntsrvcod_to_Visible ;
      private int edtavTfcontratoservicosindicador_contratocod_Visible ;
      private int edtavTfcontratoservicosindicador_contratocod_to_Visible ;
      private int edtavTfcontratoservicosindicador_areatrabalhocod_Visible ;
      private int edtavTfcontratoservicosindicador_areatrabalhocod_to_Visible ;
      private int edtavTfcontratoservicosindicador_numero_Visible ;
      private int edtavTfcontratoservicosindicador_numero_to_Visible ;
      private int edtavTfcontratoservicosindicador_indicador_Visible ;
      private int edtavTfcontratoservicosindicador_indicador_sel_Visible ;
      private int edtavTfcontratoservicosindicador_finalidade_Visible ;
      private int edtavTfcontratoservicosindicador_finalidade_sel_Visible ;
      private int edtavTfcontratoservicosindicador_meta_Visible ;
      private int edtavTfcontratoservicosindicador_meta_sel_Visible ;
      private int edtavTfcontratoservicosindicador_instrumentomedicao_Visible ;
      private int edtavTfcontratoservicosindicador_instrumentomedicao_sel_Visible ;
      private int edtavTfcontratoservicosindicador_vigencia_Visible ;
      private int edtavTfcontratoservicosindicador_vigencia_sel_Visible ;
      private int edtavTfcontratoservicosindicador_qtdefaixas_Visible ;
      private int edtavTfcontratoservicosindicador_qtdefaixas_to_Visible ;
      private int edtavDdo_contratoservicosindicador_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicosindicador_cntsrvcodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicosindicador_contratocodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicosindicador_areatrabalhocodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicosindicador_numerotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicosindicador_indicadortitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicosindicador_finalidadetitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicosindicador_metatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicosindicador_instrumentomedicaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicosindicador_tipotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicosindicador_periodicidadetitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicosindicador_vigenciatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicosindicador_qtdefaixastitlecontrolidtoreplace_Visible ;
      private int A1269ContratoServicosIndicador_Codigo ;
      private int A1270ContratoServicosIndicador_CntSrvCod ;
      private int A1296ContratoServicosIndicador_ContratoCod ;
      private int A1295ContratoServicosIndicador_AreaTrabalhoCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV69TFContratoServicosIndicador_Tipo_Sels_Count ;
      private int AV73TFContratoServicosIndicador_Periodicidade_Sels_Count ;
      private int edtavOrdereddsc_Visible ;
      private int AV84PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavContratoservicosindicador_indicador1_Visible ;
      private int edtavContratoservicosindicador_indicador2_Visible ;
      private int edtavContratoservicosindicador_indicador3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV85GridCurrentPage ;
      private long AV86GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contratoservicosindicador_codigo_Activeeventkey ;
      private String Ddo_contratoservicosindicador_codigo_Filteredtext_get ;
      private String Ddo_contratoservicosindicador_codigo_Filteredtextto_get ;
      private String Ddo_contratoservicosindicador_cntsrvcod_Activeeventkey ;
      private String Ddo_contratoservicosindicador_cntsrvcod_Filteredtext_get ;
      private String Ddo_contratoservicosindicador_cntsrvcod_Filteredtextto_get ;
      private String Ddo_contratoservicosindicador_contratocod_Activeeventkey ;
      private String Ddo_contratoservicosindicador_contratocod_Filteredtext_get ;
      private String Ddo_contratoservicosindicador_contratocod_Filteredtextto_get ;
      private String Ddo_contratoservicosindicador_areatrabalhocod_Activeeventkey ;
      private String Ddo_contratoservicosindicador_areatrabalhocod_Filteredtext_get ;
      private String Ddo_contratoservicosindicador_areatrabalhocod_Filteredtextto_get ;
      private String Ddo_contratoservicosindicador_numero_Activeeventkey ;
      private String Ddo_contratoservicosindicador_numero_Filteredtext_get ;
      private String Ddo_contratoservicosindicador_numero_Filteredtextto_get ;
      private String Ddo_contratoservicosindicador_indicador_Activeeventkey ;
      private String Ddo_contratoservicosindicador_indicador_Filteredtext_get ;
      private String Ddo_contratoservicosindicador_indicador_Selectedvalue_get ;
      private String Ddo_contratoservicosindicador_finalidade_Activeeventkey ;
      private String Ddo_contratoservicosindicador_finalidade_Filteredtext_get ;
      private String Ddo_contratoservicosindicador_finalidade_Selectedvalue_get ;
      private String Ddo_contratoservicosindicador_meta_Activeeventkey ;
      private String Ddo_contratoservicosindicador_meta_Filteredtext_get ;
      private String Ddo_contratoservicosindicador_meta_Selectedvalue_get ;
      private String Ddo_contratoservicosindicador_instrumentomedicao_Activeeventkey ;
      private String Ddo_contratoservicosindicador_instrumentomedicao_Filteredtext_get ;
      private String Ddo_contratoservicosindicador_instrumentomedicao_Selectedvalue_get ;
      private String Ddo_contratoservicosindicador_tipo_Activeeventkey ;
      private String Ddo_contratoservicosindicador_tipo_Selectedvalue_get ;
      private String Ddo_contratoservicosindicador_periodicidade_Activeeventkey ;
      private String Ddo_contratoservicosindicador_periodicidade_Selectedvalue_get ;
      private String Ddo_contratoservicosindicador_vigencia_Activeeventkey ;
      private String Ddo_contratoservicosindicador_vigencia_Filteredtext_get ;
      private String Ddo_contratoservicosindicador_vigencia_Selectedvalue_get ;
      private String Ddo_contratoservicosindicador_qtdefaixas_Activeeventkey ;
      private String Ddo_contratoservicosindicador_qtdefaixas_Filteredtext_get ;
      private String Ddo_contratoservicosindicador_qtdefaixas_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_80_idx="0001" ;
      private String AV90Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contratoservicosindicador_codigo_Caption ;
      private String Ddo_contratoservicosindicador_codigo_Tooltip ;
      private String Ddo_contratoservicosindicador_codigo_Cls ;
      private String Ddo_contratoservicosindicador_codigo_Filteredtext_set ;
      private String Ddo_contratoservicosindicador_codigo_Filteredtextto_set ;
      private String Ddo_contratoservicosindicador_codigo_Dropdownoptionstype ;
      private String Ddo_contratoservicosindicador_codigo_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosindicador_codigo_Sortedstatus ;
      private String Ddo_contratoservicosindicador_codigo_Filtertype ;
      private String Ddo_contratoservicosindicador_codigo_Sortasc ;
      private String Ddo_contratoservicosindicador_codigo_Sortdsc ;
      private String Ddo_contratoservicosindicador_codigo_Cleanfilter ;
      private String Ddo_contratoservicosindicador_codigo_Rangefilterfrom ;
      private String Ddo_contratoservicosindicador_codigo_Rangefilterto ;
      private String Ddo_contratoservicosindicador_codigo_Searchbuttontext ;
      private String Ddo_contratoservicosindicador_cntsrvcod_Caption ;
      private String Ddo_contratoservicosindicador_cntsrvcod_Tooltip ;
      private String Ddo_contratoservicosindicador_cntsrvcod_Cls ;
      private String Ddo_contratoservicosindicador_cntsrvcod_Filteredtext_set ;
      private String Ddo_contratoservicosindicador_cntsrvcod_Filteredtextto_set ;
      private String Ddo_contratoservicosindicador_cntsrvcod_Dropdownoptionstype ;
      private String Ddo_contratoservicosindicador_cntsrvcod_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosindicador_cntsrvcod_Sortedstatus ;
      private String Ddo_contratoservicosindicador_cntsrvcod_Filtertype ;
      private String Ddo_contratoservicosindicador_cntsrvcod_Sortasc ;
      private String Ddo_contratoservicosindicador_cntsrvcod_Sortdsc ;
      private String Ddo_contratoservicosindicador_cntsrvcod_Cleanfilter ;
      private String Ddo_contratoservicosindicador_cntsrvcod_Rangefilterfrom ;
      private String Ddo_contratoservicosindicador_cntsrvcod_Rangefilterto ;
      private String Ddo_contratoservicosindicador_cntsrvcod_Searchbuttontext ;
      private String Ddo_contratoservicosindicador_contratocod_Caption ;
      private String Ddo_contratoservicosindicador_contratocod_Tooltip ;
      private String Ddo_contratoservicosindicador_contratocod_Cls ;
      private String Ddo_contratoservicosindicador_contratocod_Filteredtext_set ;
      private String Ddo_contratoservicosindicador_contratocod_Filteredtextto_set ;
      private String Ddo_contratoservicosindicador_contratocod_Dropdownoptionstype ;
      private String Ddo_contratoservicosindicador_contratocod_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosindicador_contratocod_Sortedstatus ;
      private String Ddo_contratoservicosindicador_contratocod_Filtertype ;
      private String Ddo_contratoservicosindicador_contratocod_Sortasc ;
      private String Ddo_contratoservicosindicador_contratocod_Sortdsc ;
      private String Ddo_contratoservicosindicador_contratocod_Cleanfilter ;
      private String Ddo_contratoservicosindicador_contratocod_Rangefilterfrom ;
      private String Ddo_contratoservicosindicador_contratocod_Rangefilterto ;
      private String Ddo_contratoservicosindicador_contratocod_Searchbuttontext ;
      private String Ddo_contratoservicosindicador_areatrabalhocod_Caption ;
      private String Ddo_contratoservicosindicador_areatrabalhocod_Tooltip ;
      private String Ddo_contratoservicosindicador_areatrabalhocod_Cls ;
      private String Ddo_contratoservicosindicador_areatrabalhocod_Filteredtext_set ;
      private String Ddo_contratoservicosindicador_areatrabalhocod_Filteredtextto_set ;
      private String Ddo_contratoservicosindicador_areatrabalhocod_Dropdownoptionstype ;
      private String Ddo_contratoservicosindicador_areatrabalhocod_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosindicador_areatrabalhocod_Sortedstatus ;
      private String Ddo_contratoservicosindicador_areatrabalhocod_Filtertype ;
      private String Ddo_contratoservicosindicador_areatrabalhocod_Sortasc ;
      private String Ddo_contratoservicosindicador_areatrabalhocod_Sortdsc ;
      private String Ddo_contratoservicosindicador_areatrabalhocod_Cleanfilter ;
      private String Ddo_contratoservicosindicador_areatrabalhocod_Rangefilterfrom ;
      private String Ddo_contratoservicosindicador_areatrabalhocod_Rangefilterto ;
      private String Ddo_contratoservicosindicador_areatrabalhocod_Searchbuttontext ;
      private String Ddo_contratoservicosindicador_numero_Caption ;
      private String Ddo_contratoservicosindicador_numero_Tooltip ;
      private String Ddo_contratoservicosindicador_numero_Cls ;
      private String Ddo_contratoservicosindicador_numero_Filteredtext_set ;
      private String Ddo_contratoservicosindicador_numero_Filteredtextto_set ;
      private String Ddo_contratoservicosindicador_numero_Dropdownoptionstype ;
      private String Ddo_contratoservicosindicador_numero_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosindicador_numero_Sortedstatus ;
      private String Ddo_contratoservicosindicador_numero_Filtertype ;
      private String Ddo_contratoservicosindicador_numero_Sortasc ;
      private String Ddo_contratoservicosindicador_numero_Sortdsc ;
      private String Ddo_contratoservicosindicador_numero_Cleanfilter ;
      private String Ddo_contratoservicosindicador_numero_Rangefilterfrom ;
      private String Ddo_contratoservicosindicador_numero_Rangefilterto ;
      private String Ddo_contratoservicosindicador_numero_Searchbuttontext ;
      private String Ddo_contratoservicosindicador_indicador_Caption ;
      private String Ddo_contratoservicosindicador_indicador_Tooltip ;
      private String Ddo_contratoservicosindicador_indicador_Cls ;
      private String Ddo_contratoservicosindicador_indicador_Filteredtext_set ;
      private String Ddo_contratoservicosindicador_indicador_Selectedvalue_set ;
      private String Ddo_contratoservicosindicador_indicador_Dropdownoptionstype ;
      private String Ddo_contratoservicosindicador_indicador_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosindicador_indicador_Sortedstatus ;
      private String Ddo_contratoservicosindicador_indicador_Filtertype ;
      private String Ddo_contratoservicosindicador_indicador_Datalisttype ;
      private String Ddo_contratoservicosindicador_indicador_Datalistproc ;
      private String Ddo_contratoservicosindicador_indicador_Sortasc ;
      private String Ddo_contratoservicosindicador_indicador_Sortdsc ;
      private String Ddo_contratoservicosindicador_indicador_Loadingdata ;
      private String Ddo_contratoservicosindicador_indicador_Cleanfilter ;
      private String Ddo_contratoservicosindicador_indicador_Noresultsfound ;
      private String Ddo_contratoservicosindicador_indicador_Searchbuttontext ;
      private String Ddo_contratoservicosindicador_finalidade_Caption ;
      private String Ddo_contratoservicosindicador_finalidade_Tooltip ;
      private String Ddo_contratoservicosindicador_finalidade_Cls ;
      private String Ddo_contratoservicosindicador_finalidade_Filteredtext_set ;
      private String Ddo_contratoservicosindicador_finalidade_Selectedvalue_set ;
      private String Ddo_contratoservicosindicador_finalidade_Dropdownoptionstype ;
      private String Ddo_contratoservicosindicador_finalidade_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosindicador_finalidade_Sortedstatus ;
      private String Ddo_contratoservicosindicador_finalidade_Filtertype ;
      private String Ddo_contratoservicosindicador_finalidade_Datalisttype ;
      private String Ddo_contratoservicosindicador_finalidade_Datalistproc ;
      private String Ddo_contratoservicosindicador_finalidade_Sortasc ;
      private String Ddo_contratoservicosindicador_finalidade_Sortdsc ;
      private String Ddo_contratoservicosindicador_finalidade_Loadingdata ;
      private String Ddo_contratoservicosindicador_finalidade_Cleanfilter ;
      private String Ddo_contratoservicosindicador_finalidade_Noresultsfound ;
      private String Ddo_contratoservicosindicador_finalidade_Searchbuttontext ;
      private String Ddo_contratoservicosindicador_meta_Caption ;
      private String Ddo_contratoservicosindicador_meta_Tooltip ;
      private String Ddo_contratoservicosindicador_meta_Cls ;
      private String Ddo_contratoservicosindicador_meta_Filteredtext_set ;
      private String Ddo_contratoservicosindicador_meta_Selectedvalue_set ;
      private String Ddo_contratoservicosindicador_meta_Dropdownoptionstype ;
      private String Ddo_contratoservicosindicador_meta_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosindicador_meta_Sortedstatus ;
      private String Ddo_contratoservicosindicador_meta_Filtertype ;
      private String Ddo_contratoservicosindicador_meta_Datalisttype ;
      private String Ddo_contratoservicosindicador_meta_Datalistproc ;
      private String Ddo_contratoservicosindicador_meta_Sortasc ;
      private String Ddo_contratoservicosindicador_meta_Sortdsc ;
      private String Ddo_contratoservicosindicador_meta_Loadingdata ;
      private String Ddo_contratoservicosindicador_meta_Cleanfilter ;
      private String Ddo_contratoservicosindicador_meta_Noresultsfound ;
      private String Ddo_contratoservicosindicador_meta_Searchbuttontext ;
      private String Ddo_contratoservicosindicador_instrumentomedicao_Caption ;
      private String Ddo_contratoservicosindicador_instrumentomedicao_Tooltip ;
      private String Ddo_contratoservicosindicador_instrumentomedicao_Cls ;
      private String Ddo_contratoservicosindicador_instrumentomedicao_Filteredtext_set ;
      private String Ddo_contratoservicosindicador_instrumentomedicao_Selectedvalue_set ;
      private String Ddo_contratoservicosindicador_instrumentomedicao_Dropdownoptionstype ;
      private String Ddo_contratoservicosindicador_instrumentomedicao_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosindicador_instrumentomedicao_Sortedstatus ;
      private String Ddo_contratoservicosindicador_instrumentomedicao_Filtertype ;
      private String Ddo_contratoservicosindicador_instrumentomedicao_Datalisttype ;
      private String Ddo_contratoservicosindicador_instrumentomedicao_Datalistproc ;
      private String Ddo_contratoservicosindicador_instrumentomedicao_Sortasc ;
      private String Ddo_contratoservicosindicador_instrumentomedicao_Sortdsc ;
      private String Ddo_contratoservicosindicador_instrumentomedicao_Loadingdata ;
      private String Ddo_contratoservicosindicador_instrumentomedicao_Cleanfilter ;
      private String Ddo_contratoservicosindicador_instrumentomedicao_Noresultsfound ;
      private String Ddo_contratoservicosindicador_instrumentomedicao_Searchbuttontext ;
      private String Ddo_contratoservicosindicador_tipo_Caption ;
      private String Ddo_contratoservicosindicador_tipo_Tooltip ;
      private String Ddo_contratoservicosindicador_tipo_Cls ;
      private String Ddo_contratoservicosindicador_tipo_Selectedvalue_set ;
      private String Ddo_contratoservicosindicador_tipo_Dropdownoptionstype ;
      private String Ddo_contratoservicosindicador_tipo_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosindicador_tipo_Sortedstatus ;
      private String Ddo_contratoservicosindicador_tipo_Datalisttype ;
      private String Ddo_contratoservicosindicador_tipo_Datalistfixedvalues ;
      private String Ddo_contratoservicosindicador_tipo_Sortasc ;
      private String Ddo_contratoservicosindicador_tipo_Sortdsc ;
      private String Ddo_contratoservicosindicador_tipo_Cleanfilter ;
      private String Ddo_contratoservicosindicador_tipo_Searchbuttontext ;
      private String Ddo_contratoservicosindicador_periodicidade_Caption ;
      private String Ddo_contratoservicosindicador_periodicidade_Tooltip ;
      private String Ddo_contratoservicosindicador_periodicidade_Cls ;
      private String Ddo_contratoservicosindicador_periodicidade_Selectedvalue_set ;
      private String Ddo_contratoservicosindicador_periodicidade_Dropdownoptionstype ;
      private String Ddo_contratoservicosindicador_periodicidade_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosindicador_periodicidade_Sortedstatus ;
      private String Ddo_contratoservicosindicador_periodicidade_Datalisttype ;
      private String Ddo_contratoservicosindicador_periodicidade_Datalistfixedvalues ;
      private String Ddo_contratoservicosindicador_periodicidade_Sortasc ;
      private String Ddo_contratoservicosindicador_periodicidade_Sortdsc ;
      private String Ddo_contratoservicosindicador_periodicidade_Cleanfilter ;
      private String Ddo_contratoservicosindicador_periodicidade_Searchbuttontext ;
      private String Ddo_contratoservicosindicador_vigencia_Caption ;
      private String Ddo_contratoservicosindicador_vigencia_Tooltip ;
      private String Ddo_contratoservicosindicador_vigencia_Cls ;
      private String Ddo_contratoservicosindicador_vigencia_Filteredtext_set ;
      private String Ddo_contratoservicosindicador_vigencia_Selectedvalue_set ;
      private String Ddo_contratoservicosindicador_vigencia_Dropdownoptionstype ;
      private String Ddo_contratoservicosindicador_vigencia_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosindicador_vigencia_Sortedstatus ;
      private String Ddo_contratoservicosindicador_vigencia_Filtertype ;
      private String Ddo_contratoservicosindicador_vigencia_Datalisttype ;
      private String Ddo_contratoservicosindicador_vigencia_Datalistproc ;
      private String Ddo_contratoservicosindicador_vigencia_Sortasc ;
      private String Ddo_contratoservicosindicador_vigencia_Sortdsc ;
      private String Ddo_contratoservicosindicador_vigencia_Loadingdata ;
      private String Ddo_contratoservicosindicador_vigencia_Cleanfilter ;
      private String Ddo_contratoservicosindicador_vigencia_Noresultsfound ;
      private String Ddo_contratoservicosindicador_vigencia_Searchbuttontext ;
      private String Ddo_contratoservicosindicador_qtdefaixas_Caption ;
      private String Ddo_contratoservicosindicador_qtdefaixas_Tooltip ;
      private String Ddo_contratoservicosindicador_qtdefaixas_Cls ;
      private String Ddo_contratoservicosindicador_qtdefaixas_Filteredtext_set ;
      private String Ddo_contratoservicosindicador_qtdefaixas_Filteredtextto_set ;
      private String Ddo_contratoservicosindicador_qtdefaixas_Dropdownoptionstype ;
      private String Ddo_contratoservicosindicador_qtdefaixas_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosindicador_qtdefaixas_Filtertype ;
      private String Ddo_contratoservicosindicador_qtdefaixas_Cleanfilter ;
      private String Ddo_contratoservicosindicador_qtdefaixas_Rangefilterfrom ;
      private String Ddo_contratoservicosindicador_qtdefaixas_Rangefilterto ;
      private String Ddo_contratoservicosindicador_qtdefaixas_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfcontratoservicosindicador_codigo_Internalname ;
      private String edtavTfcontratoservicosindicador_codigo_Jsonclick ;
      private String edtavTfcontratoservicosindicador_codigo_to_Internalname ;
      private String edtavTfcontratoservicosindicador_codigo_to_Jsonclick ;
      private String edtavTfcontratoservicosindicador_cntsrvcod_Internalname ;
      private String edtavTfcontratoservicosindicador_cntsrvcod_Jsonclick ;
      private String edtavTfcontratoservicosindicador_cntsrvcod_to_Internalname ;
      private String edtavTfcontratoservicosindicador_cntsrvcod_to_Jsonclick ;
      private String edtavTfcontratoservicosindicador_contratocod_Internalname ;
      private String edtavTfcontratoservicosindicador_contratocod_Jsonclick ;
      private String edtavTfcontratoservicosindicador_contratocod_to_Internalname ;
      private String edtavTfcontratoservicosindicador_contratocod_to_Jsonclick ;
      private String edtavTfcontratoservicosindicador_areatrabalhocod_Internalname ;
      private String edtavTfcontratoservicosindicador_areatrabalhocod_Jsonclick ;
      private String edtavTfcontratoservicosindicador_areatrabalhocod_to_Internalname ;
      private String edtavTfcontratoservicosindicador_areatrabalhocod_to_Jsonclick ;
      private String edtavTfcontratoservicosindicador_numero_Internalname ;
      private String edtavTfcontratoservicosindicador_numero_Jsonclick ;
      private String edtavTfcontratoservicosindicador_numero_to_Internalname ;
      private String edtavTfcontratoservicosindicador_numero_to_Jsonclick ;
      private String edtavTfcontratoservicosindicador_indicador_Internalname ;
      private String edtavTfcontratoservicosindicador_indicador_sel_Internalname ;
      private String edtavTfcontratoservicosindicador_finalidade_Internalname ;
      private String edtavTfcontratoservicosindicador_finalidade_sel_Internalname ;
      private String edtavTfcontratoservicosindicador_meta_Internalname ;
      private String edtavTfcontratoservicosindicador_meta_sel_Internalname ;
      private String edtavTfcontratoservicosindicador_instrumentomedicao_Internalname ;
      private String edtavTfcontratoservicosindicador_instrumentomedicao_sel_Internalname ;
      private String edtavTfcontratoservicosindicador_vigencia_Internalname ;
      private String edtavTfcontratoservicosindicador_vigencia_Jsonclick ;
      private String edtavTfcontratoservicosindicador_vigencia_sel_Internalname ;
      private String edtavTfcontratoservicosindicador_vigencia_sel_Jsonclick ;
      private String edtavTfcontratoservicosindicador_qtdefaixas_Internalname ;
      private String edtavTfcontratoservicosindicador_qtdefaixas_Jsonclick ;
      private String edtavTfcontratoservicosindicador_qtdefaixas_to_Internalname ;
      private String edtavTfcontratoservicosindicador_qtdefaixas_to_Jsonclick ;
      private String edtavDdo_contratoservicosindicador_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicosindicador_cntsrvcodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicosindicador_contratocodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicosindicador_areatrabalhocodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicosindicador_numerotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicosindicador_indicadortitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicosindicador_finalidadetitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicosindicador_metatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicosindicador_instrumentomedicaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicosindicador_tipotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicosindicador_periodicidadetitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicosindicador_vigenciatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicosindicador_qtdefaixastitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtContratoServicosIndicador_Codigo_Internalname ;
      private String edtContratoServicosIndicador_CntSrvCod_Internalname ;
      private String edtContratoServicosIndicador_ContratoCod_Internalname ;
      private String edtContratoServicosIndicador_AreaTrabalhoCod_Internalname ;
      private String edtContratoServicosIndicador_Numero_Internalname ;
      private String edtContratoServicosIndicador_Indicador_Internalname ;
      private String edtContratoServicosIndicador_Finalidade_Internalname ;
      private String edtContratoServicosIndicador_Meta_Internalname ;
      private String edtContratoServicosIndicador_InstrumentoMedicao_Internalname ;
      private String cmbContratoServicosIndicador_Tipo_Internalname ;
      private String A1308ContratoServicosIndicador_Tipo ;
      private String cmbContratoServicosIndicador_Periodicidade_Internalname ;
      private String A1309ContratoServicosIndicador_Periodicidade ;
      private String edtContratoServicosIndicador_Vigencia_Internalname ;
      private String edtContratoServicosIndicador_QtdeFaixas_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavContratoservicosindicador_indicador1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavContratoservicosindicador_indicador2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavContratoservicosindicador_indicador3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_contratoservicosindicador_codigo_Internalname ;
      private String Ddo_contratoservicosindicador_cntsrvcod_Internalname ;
      private String Ddo_contratoservicosindicador_contratocod_Internalname ;
      private String Ddo_contratoservicosindicador_areatrabalhocod_Internalname ;
      private String Ddo_contratoservicosindicador_numero_Internalname ;
      private String Ddo_contratoservicosindicador_indicador_Internalname ;
      private String Ddo_contratoservicosindicador_finalidade_Internalname ;
      private String Ddo_contratoservicosindicador_meta_Internalname ;
      private String Ddo_contratoservicosindicador_instrumentomedicao_Internalname ;
      private String Ddo_contratoservicosindicador_tipo_Internalname ;
      private String Ddo_contratoservicosindicador_periodicidade_Internalname ;
      private String Ddo_contratoservicosindicador_vigencia_Internalname ;
      private String Ddo_contratoservicosindicador_qtdefaixas_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtContratoServicosIndicador_Codigo_Title ;
      private String edtContratoServicosIndicador_CntSrvCod_Title ;
      private String edtContratoServicosIndicador_ContratoCod_Title ;
      private String edtContratoServicosIndicador_AreaTrabalhoCod_Title ;
      private String edtContratoServicosIndicador_Numero_Title ;
      private String edtContratoServicosIndicador_Indicador_Title ;
      private String edtContratoServicosIndicador_Finalidade_Title ;
      private String edtContratoServicosIndicador_Meta_Title ;
      private String edtContratoServicosIndicador_InstrumentoMedicao_Title ;
      private String edtContratoServicosIndicador_Vigencia_Title ;
      private String edtContratoServicosIndicador_QtdeFaixas_Title ;
      private String edtavSelect_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String sGXsfl_80_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtContratoServicosIndicador_Codigo_Jsonclick ;
      private String edtContratoServicosIndicador_CntSrvCod_Jsonclick ;
      private String edtContratoServicosIndicador_ContratoCod_Jsonclick ;
      private String edtContratoServicosIndicador_AreaTrabalhoCod_Jsonclick ;
      private String edtContratoServicosIndicador_Numero_Jsonclick ;
      private String edtContratoServicosIndicador_Indicador_Jsonclick ;
      private String edtContratoServicosIndicador_Finalidade_Jsonclick ;
      private String edtContratoServicosIndicador_Meta_Jsonclick ;
      private String edtContratoServicosIndicador_InstrumentoMedicao_Jsonclick ;
      private String cmbContratoServicosIndicador_Tipo_Jsonclick ;
      private String cmbContratoServicosIndicador_Periodicidade_Jsonclick ;
      private String edtContratoServicosIndicador_Vigencia_Jsonclick ;
      private String edtContratoServicosIndicador_QtdeFaixas_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contratoservicosindicador_codigo_Includesortasc ;
      private bool Ddo_contratoservicosindicador_codigo_Includesortdsc ;
      private bool Ddo_contratoservicosindicador_codigo_Includefilter ;
      private bool Ddo_contratoservicosindicador_codigo_Filterisrange ;
      private bool Ddo_contratoservicosindicador_codigo_Includedatalist ;
      private bool Ddo_contratoservicosindicador_cntsrvcod_Includesortasc ;
      private bool Ddo_contratoservicosindicador_cntsrvcod_Includesortdsc ;
      private bool Ddo_contratoservicosindicador_cntsrvcod_Includefilter ;
      private bool Ddo_contratoservicosindicador_cntsrvcod_Filterisrange ;
      private bool Ddo_contratoservicosindicador_cntsrvcod_Includedatalist ;
      private bool Ddo_contratoservicosindicador_contratocod_Includesortasc ;
      private bool Ddo_contratoservicosindicador_contratocod_Includesortdsc ;
      private bool Ddo_contratoservicosindicador_contratocod_Includefilter ;
      private bool Ddo_contratoservicosindicador_contratocod_Filterisrange ;
      private bool Ddo_contratoservicosindicador_contratocod_Includedatalist ;
      private bool Ddo_contratoservicosindicador_areatrabalhocod_Includesortasc ;
      private bool Ddo_contratoservicosindicador_areatrabalhocod_Includesortdsc ;
      private bool Ddo_contratoservicosindicador_areatrabalhocod_Includefilter ;
      private bool Ddo_contratoservicosindicador_areatrabalhocod_Filterisrange ;
      private bool Ddo_contratoservicosindicador_areatrabalhocod_Includedatalist ;
      private bool Ddo_contratoservicosindicador_numero_Includesortasc ;
      private bool Ddo_contratoservicosindicador_numero_Includesortdsc ;
      private bool Ddo_contratoservicosindicador_numero_Includefilter ;
      private bool Ddo_contratoservicosindicador_numero_Filterisrange ;
      private bool Ddo_contratoservicosindicador_numero_Includedatalist ;
      private bool Ddo_contratoservicosindicador_indicador_Includesortasc ;
      private bool Ddo_contratoservicosindicador_indicador_Includesortdsc ;
      private bool Ddo_contratoservicosindicador_indicador_Includefilter ;
      private bool Ddo_contratoservicosindicador_indicador_Filterisrange ;
      private bool Ddo_contratoservicosindicador_indicador_Includedatalist ;
      private bool Ddo_contratoservicosindicador_finalidade_Includesortasc ;
      private bool Ddo_contratoservicosindicador_finalidade_Includesortdsc ;
      private bool Ddo_contratoservicosindicador_finalidade_Includefilter ;
      private bool Ddo_contratoservicosindicador_finalidade_Filterisrange ;
      private bool Ddo_contratoservicosindicador_finalidade_Includedatalist ;
      private bool Ddo_contratoservicosindicador_meta_Includesortasc ;
      private bool Ddo_contratoservicosindicador_meta_Includesortdsc ;
      private bool Ddo_contratoservicosindicador_meta_Includefilter ;
      private bool Ddo_contratoservicosindicador_meta_Filterisrange ;
      private bool Ddo_contratoservicosindicador_meta_Includedatalist ;
      private bool Ddo_contratoservicosindicador_instrumentomedicao_Includesortasc ;
      private bool Ddo_contratoservicosindicador_instrumentomedicao_Includesortdsc ;
      private bool Ddo_contratoservicosindicador_instrumentomedicao_Includefilter ;
      private bool Ddo_contratoservicosindicador_instrumentomedicao_Filterisrange ;
      private bool Ddo_contratoservicosindicador_instrumentomedicao_Includedatalist ;
      private bool Ddo_contratoservicosindicador_tipo_Includesortasc ;
      private bool Ddo_contratoservicosindicador_tipo_Includesortdsc ;
      private bool Ddo_contratoservicosindicador_tipo_Includefilter ;
      private bool Ddo_contratoservicosindicador_tipo_Includedatalist ;
      private bool Ddo_contratoservicosindicador_tipo_Allowmultipleselection ;
      private bool Ddo_contratoservicosindicador_periodicidade_Includesortasc ;
      private bool Ddo_contratoservicosindicador_periodicidade_Includesortdsc ;
      private bool Ddo_contratoservicosindicador_periodicidade_Includefilter ;
      private bool Ddo_contratoservicosindicador_periodicidade_Includedatalist ;
      private bool Ddo_contratoservicosindicador_periodicidade_Allowmultipleselection ;
      private bool Ddo_contratoservicosindicador_vigencia_Includesortasc ;
      private bool Ddo_contratoservicosindicador_vigencia_Includesortdsc ;
      private bool Ddo_contratoservicosindicador_vigencia_Includefilter ;
      private bool Ddo_contratoservicosindicador_vigencia_Filterisrange ;
      private bool Ddo_contratoservicosindicador_vigencia_Includedatalist ;
      private bool Ddo_contratoservicosindicador_qtdefaixas_Includesortasc ;
      private bool Ddo_contratoservicosindicador_qtdefaixas_Includesortdsc ;
      private bool Ddo_contratoservicosindicador_qtdefaixas_Includefilter ;
      private bool Ddo_contratoservicosindicador_qtdefaixas_Filterisrange ;
      private bool Ddo_contratoservicosindicador_qtdefaixas_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1296ContratoServicosIndicador_ContratoCod ;
      private bool n1295ContratoServicosIndicador_AreaTrabalhoCod ;
      private bool n1305ContratoServicosIndicador_Finalidade ;
      private bool n1306ContratoServicosIndicador_Meta ;
      private bool n1307ContratoServicosIndicador_InstrumentoMedicao ;
      private bool n1308ContratoServicosIndicador_Tipo ;
      private bool n1309ContratoServicosIndicador_Periodicidade ;
      private bool n1310ContratoServicosIndicador_Vigencia ;
      private bool n1298ContratoServicosIndicador_QtdeFaixas ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Select_IsBlob ;
      private String AV8InOutContratoServicosIndicador_Indicador ;
      private String wcpOAV8InOutContratoServicosIndicador_Indicador ;
      private String AV17ContratoServicosIndicador_Indicador1 ;
      private String AV21ContratoServicosIndicador_Indicador2 ;
      private String AV25ContratoServicosIndicador_Indicador3 ;
      private String A1274ContratoServicosIndicador_Indicador ;
      private String A1305ContratoServicosIndicador_Finalidade ;
      private String A1306ContratoServicosIndicador_Meta ;
      private String A1307ContratoServicosIndicador_InstrumentoMedicao ;
      private String lV17ContratoServicosIndicador_Indicador1 ;
      private String lV21ContratoServicosIndicador_Indicador2 ;
      private String lV25ContratoServicosIndicador_Indicador3 ;
      private String AV68TFContratoServicosIndicador_Tipo_SelsJson ;
      private String AV72TFContratoServicosIndicador_Periodicidade_SelsJson ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV52TFContratoServicosIndicador_Indicador ;
      private String AV53TFContratoServicosIndicador_Indicador_Sel ;
      private String AV56TFContratoServicosIndicador_Finalidade ;
      private String AV57TFContratoServicosIndicador_Finalidade_Sel ;
      private String AV60TFContratoServicosIndicador_Meta ;
      private String AV61TFContratoServicosIndicador_Meta_Sel ;
      private String AV64TFContratoServicosIndicador_InstrumentoMedicao ;
      private String AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel ;
      private String AV76TFContratoServicosIndicador_Vigencia ;
      private String AV77TFContratoServicosIndicador_Vigencia_Sel ;
      private String AV34ddo_ContratoServicosIndicador_CodigoTitleControlIdToReplace ;
      private String AV38ddo_ContratoServicosIndicador_CntSrvCodTitleControlIdToReplace ;
      private String AV42ddo_ContratoServicosIndicador_ContratoCodTitleControlIdToReplace ;
      private String AV46ddo_ContratoServicosIndicador_AreaTrabalhoCodTitleControlIdToReplace ;
      private String AV50ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace ;
      private String AV54ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace ;
      private String AV58ddo_ContratoServicosIndicador_FinalidadeTitleControlIdToReplace ;
      private String AV62ddo_ContratoServicosIndicador_MetaTitleControlIdToReplace ;
      private String AV66ddo_ContratoServicosIndicador_InstrumentoMedicaoTitleControlIdToReplace ;
      private String AV70ddo_ContratoServicosIndicador_TipoTitleControlIdToReplace ;
      private String AV74ddo_ContratoServicosIndicador_PeriodicidadeTitleControlIdToReplace ;
      private String AV78ddo_ContratoServicosIndicador_VigenciaTitleControlIdToReplace ;
      private String AV82ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace ;
      private String AV89Select_GXI ;
      private String A1310ContratoServicosIndicador_Vigencia ;
      private String lV52TFContratoServicosIndicador_Indicador ;
      private String lV56TFContratoServicosIndicador_Finalidade ;
      private String lV60TFContratoServicosIndicador_Meta ;
      private String lV64TFContratoServicosIndicador_InstrumentoMedicao ;
      private String lV76TFContratoServicosIndicador_Vigencia ;
      private String AV28Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutContratoServicosIndicador_Codigo ;
      private String aP1_InOutContratoServicosIndicador_Indicador ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCombobox cmbContratoServicosIndicador_Tipo ;
      private GXCombobox cmbContratoServicosIndicador_Periodicidade ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private String[] H00JG3_A1310ContratoServicosIndicador_Vigencia ;
      private bool[] H00JG3_n1310ContratoServicosIndicador_Vigencia ;
      private String[] H00JG3_A1309ContratoServicosIndicador_Periodicidade ;
      private bool[] H00JG3_n1309ContratoServicosIndicador_Periodicidade ;
      private String[] H00JG3_A1308ContratoServicosIndicador_Tipo ;
      private bool[] H00JG3_n1308ContratoServicosIndicador_Tipo ;
      private String[] H00JG3_A1307ContratoServicosIndicador_InstrumentoMedicao ;
      private bool[] H00JG3_n1307ContratoServicosIndicador_InstrumentoMedicao ;
      private String[] H00JG3_A1306ContratoServicosIndicador_Meta ;
      private bool[] H00JG3_n1306ContratoServicosIndicador_Meta ;
      private String[] H00JG3_A1305ContratoServicosIndicador_Finalidade ;
      private bool[] H00JG3_n1305ContratoServicosIndicador_Finalidade ;
      private String[] H00JG3_A1274ContratoServicosIndicador_Indicador ;
      private short[] H00JG3_A1271ContratoServicosIndicador_Numero ;
      private int[] H00JG3_A1295ContratoServicosIndicador_AreaTrabalhoCod ;
      private bool[] H00JG3_n1295ContratoServicosIndicador_AreaTrabalhoCod ;
      private int[] H00JG3_A1296ContratoServicosIndicador_ContratoCod ;
      private bool[] H00JG3_n1296ContratoServicosIndicador_ContratoCod ;
      private int[] H00JG3_A1270ContratoServicosIndicador_CntSrvCod ;
      private int[] H00JG3_A1269ContratoServicosIndicador_Codigo ;
      private short[] H00JG3_A1298ContratoServicosIndicador_QtdeFaixas ;
      private bool[] H00JG3_n1298ContratoServicosIndicador_QtdeFaixas ;
      private long[] H00JG5_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV69TFContratoServicosIndicador_Tipo_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV73TFContratoServicosIndicador_Periodicidade_Sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV31ContratoServicosIndicador_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV35ContratoServicosIndicador_CntSrvCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV39ContratoServicosIndicador_ContratoCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV43ContratoServicosIndicador_AreaTrabalhoCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV47ContratoServicosIndicador_NumeroTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV51ContratoServicosIndicador_IndicadorTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV55ContratoServicosIndicador_FinalidadeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV59ContratoServicosIndicador_MetaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV63ContratoServicosIndicador_InstrumentoMedicaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV67ContratoServicosIndicador_TipoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV71ContratoServicosIndicador_PeriodicidadeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV75ContratoServicosIndicador_VigenciaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV79ContratoServicosIndicador_QtdeFaixasTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV83DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class promptcontratoservicosindicador__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00JG3( IGxContext context ,
                                             String A1308ContratoServicosIndicador_Tipo ,
                                             IGxCollection AV69TFContratoServicosIndicador_Tipo_Sels ,
                                             String A1309ContratoServicosIndicador_Periodicidade ,
                                             IGxCollection AV73TFContratoServicosIndicador_Periodicidade_Sels ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             String AV17ContratoServicosIndicador_Indicador1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             short AV20DynamicFiltersOperator2 ,
                                             String AV21ContratoServicosIndicador_Indicador2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             short AV24DynamicFiltersOperator3 ,
                                             String AV25ContratoServicosIndicador_Indicador3 ,
                                             int AV32TFContratoServicosIndicador_Codigo ,
                                             int AV33TFContratoServicosIndicador_Codigo_To ,
                                             int AV36TFContratoServicosIndicador_CntSrvCod ,
                                             int AV37TFContratoServicosIndicador_CntSrvCod_To ,
                                             int AV40TFContratoServicosIndicador_ContratoCod ,
                                             int AV41TFContratoServicosIndicador_ContratoCod_To ,
                                             int AV44TFContratoServicosIndicador_AreaTrabalhoCod ,
                                             int AV45TFContratoServicosIndicador_AreaTrabalhoCod_To ,
                                             short AV48TFContratoServicosIndicador_Numero ,
                                             short AV49TFContratoServicosIndicador_Numero_To ,
                                             String AV53TFContratoServicosIndicador_Indicador_Sel ,
                                             String AV52TFContratoServicosIndicador_Indicador ,
                                             String AV57TFContratoServicosIndicador_Finalidade_Sel ,
                                             String AV56TFContratoServicosIndicador_Finalidade ,
                                             String AV61TFContratoServicosIndicador_Meta_Sel ,
                                             String AV60TFContratoServicosIndicador_Meta ,
                                             String AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel ,
                                             String AV64TFContratoServicosIndicador_InstrumentoMedicao ,
                                             int AV69TFContratoServicosIndicador_Tipo_Sels_Count ,
                                             int AV73TFContratoServicosIndicador_Periodicidade_Sels_Count ,
                                             String AV77TFContratoServicosIndicador_Vigencia_Sel ,
                                             String AV76TFContratoServicosIndicador_Vigencia ,
                                             String A1274ContratoServicosIndicador_Indicador ,
                                             int A1269ContratoServicosIndicador_Codigo ,
                                             int A1270ContratoServicosIndicador_CntSrvCod ,
                                             int A1296ContratoServicosIndicador_ContratoCod ,
                                             int A1295ContratoServicosIndicador_AreaTrabalhoCod ,
                                             short A1271ContratoServicosIndicador_Numero ,
                                             String A1305ContratoServicosIndicador_Finalidade ,
                                             String A1306ContratoServicosIndicador_Meta ,
                                             String A1307ContratoServicosIndicador_InstrumentoMedicao ,
                                             String A1310ContratoServicosIndicador_Vigencia ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             short AV80TFContratoServicosIndicador_QtdeFaixas ,
                                             short A1298ContratoServicosIndicador_QtdeFaixas ,
                                             short AV81TFContratoServicosIndicador_QtdeFaixas_To )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [35] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[ContratoServicosIndicador_Vigencia], T1.[ContratoServicosIndicador_Periodicidade], T1.[ContratoServicosIndicador_Tipo], T1.[ContratoServicosIndicador_InstrumentoMedicao], T1.[ContratoServicosIndicador_Meta], T1.[ContratoServicosIndicador_Finalidade], T1.[ContratoServicosIndicador_Indicador], T1.[ContratoServicosIndicador_Numero], T3.[Contrato_AreaTrabalhoCod] AS ContratoServicosIndicador_AreaTrabalhoCod, T2.[Contrato_Codigo] AS ContratoServicosIndicador_ContratoCod, T1.[ContratoServicosIndicador_CntSrvCod] AS ContratoServicosIndicador_CntSrvCod, T1.[ContratoServicosIndicador_Codigo], COALESCE( T4.[ContratoServicosIndicador_QtdeFaixas], 0) AS ContratoServicosIndicador_QtdeFaixas";
         sFromString = " FROM ((([ContratoServicosIndicador] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoServicosIndicador_CntSrvCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContratoServicosIndicador_QtdeFaixas, [ContratoServicosIndicador_Codigo] FROM [ContratoServicosIndicadorFaixas] WITH (NOLOCK) GROUP BY [ContratoServicosIndicador_Codigo] ) T4 ON T4.[ContratoServicosIndicador_Codigo] = T1.[ContratoServicosIndicador_Codigo])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE ((@AV80TFContratoServicosIndicador_QtdeFaixas = convert(int, 0)) or ( COALESCE( T4.[ContratoServicosIndicador_QtdeFaixas], 0) >= @AV80TFContratoServicosIndicador_QtdeFaixas))";
         sWhereString = sWhereString + " and ((@AV81TFContratoServicosIndicador_QtdeFaixas_To = convert(int, 0)) or ( COALESCE( T4.[ContratoServicosIndicador_QtdeFaixas], 0) <= @AV81TFContratoServicosIndicador_QtdeFaixas_To))";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17ContratoServicosIndicador_Indicador1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like @lV17ContratoServicosIndicador_Indicador1)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17ContratoServicosIndicador_Indicador1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like '%' + @lV17ContratoServicosIndicador_Indicador1)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ( AV20DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21ContratoServicosIndicador_Indicador2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like @lV21ContratoServicosIndicador_Indicador2)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ( AV20DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21ContratoServicosIndicador_Indicador2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like '%' + @lV21ContratoServicosIndicador_Indicador2)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ( AV24DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ContratoServicosIndicador_Indicador3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like @lV25ContratoServicosIndicador_Indicador3)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ( AV24DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ContratoServicosIndicador_Indicador3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like '%' + @lV25ContratoServicosIndicador_Indicador3)";
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! (0==AV32TFContratoServicosIndicador_Codigo) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Codigo] >= @AV32TFContratoServicosIndicador_Codigo)";
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! (0==AV33TFContratoServicosIndicador_Codigo_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Codigo] <= @AV33TFContratoServicosIndicador_Codigo_To)";
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ! (0==AV36TFContratoServicosIndicador_CntSrvCod) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_CntSrvCod] >= @AV36TFContratoServicosIndicador_CntSrvCod)";
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( ! (0==AV37TFContratoServicosIndicador_CntSrvCod_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_CntSrvCod] <= @AV37TFContratoServicosIndicador_CntSrvCod_To)";
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( ! (0==AV40TFContratoServicosIndicador_ContratoCod) )
         {
            sWhereString = sWhereString + " and (T2.[Contrato_Codigo] >= @AV40TFContratoServicosIndicador_ContratoCod)";
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( ! (0==AV41TFContratoServicosIndicador_ContratoCod_To) )
         {
            sWhereString = sWhereString + " and (T2.[Contrato_Codigo] <= @AV41TFContratoServicosIndicador_ContratoCod_To)";
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( ! (0==AV44TFContratoServicosIndicador_AreaTrabalhoCod) )
         {
            sWhereString = sWhereString + " and (T3.[Contrato_AreaTrabalhoCod] >= @AV44TFContratoServicosIndicador_AreaTrabalhoCod)";
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( ! (0==AV45TFContratoServicosIndicador_AreaTrabalhoCod_To) )
         {
            sWhereString = sWhereString + " and (T3.[Contrato_AreaTrabalhoCod] <= @AV45TFContratoServicosIndicador_AreaTrabalhoCod_To)";
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( ! (0==AV48TFContratoServicosIndicador_Numero) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Numero] >= @AV48TFContratoServicosIndicador_Numero)";
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( ! (0==AV49TFContratoServicosIndicador_Numero_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Numero] <= @AV49TFContratoServicosIndicador_Numero_To)";
         }
         else
         {
            GXv_int2[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV53TFContratoServicosIndicador_Indicador_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52TFContratoServicosIndicador_Indicador)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like @lV52TFContratoServicosIndicador_Indicador)";
         }
         else
         {
            GXv_int2[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53TFContratoServicosIndicador_Indicador_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] = @AV53TFContratoServicosIndicador_Indicador_Sel)";
         }
         else
         {
            GXv_int2[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV57TFContratoServicosIndicador_Finalidade_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56TFContratoServicosIndicador_Finalidade)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Finalidade] like @lV56TFContratoServicosIndicador_Finalidade)";
         }
         else
         {
            GXv_int2[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57TFContratoServicosIndicador_Finalidade_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Finalidade] = @AV57TFContratoServicosIndicador_Finalidade_Sel)";
         }
         else
         {
            GXv_int2[23] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV61TFContratoServicosIndicador_Meta_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFContratoServicosIndicador_Meta)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Meta] like @lV60TFContratoServicosIndicador_Meta)";
         }
         else
         {
            GXv_int2[24] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61TFContratoServicosIndicador_Meta_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Meta] = @AV61TFContratoServicosIndicador_Meta_Sel)";
         }
         else
         {
            GXv_int2[25] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64TFContratoServicosIndicador_InstrumentoMedicao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_InstrumentoMedicao] like @lV64TFContratoServicosIndicador_InstrumentoMedicao)";
         }
         else
         {
            GXv_int2[26] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_InstrumentoMedicao] = @AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel)";
         }
         else
         {
            GXv_int2[27] = 1;
         }
         if ( AV69TFContratoServicosIndicador_Tipo_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV69TFContratoServicosIndicador_Tipo_Sels, "T1.[ContratoServicosIndicador_Tipo] IN (", ")") + ")";
         }
         if ( AV73TFContratoServicosIndicador_Periodicidade_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV73TFContratoServicosIndicador_Periodicidade_Sels, "T1.[ContratoServicosIndicador_Periodicidade] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV77TFContratoServicosIndicador_Vigencia_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76TFContratoServicosIndicador_Vigencia)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Vigencia] like @lV76TFContratoServicosIndicador_Vigencia)";
         }
         else
         {
            GXv_int2[28] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77TFContratoServicosIndicador_Vigencia_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Vigencia] = @AV77TFContratoServicosIndicador_Vigencia_Sel)";
         }
         else
         {
            GXv_int2[29] = 1;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosIndicador_Codigo]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosIndicador_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosIndicador_CntSrvCod]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosIndicador_CntSrvCod] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contrato_Codigo]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contrato_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Contrato_AreaTrabalhoCod]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Contrato_AreaTrabalhoCod] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosIndicador_Numero]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosIndicador_Numero] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosIndicador_Indicador]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosIndicador_Indicador] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosIndicador_Finalidade]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosIndicador_Finalidade] DESC";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosIndicador_Meta]";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosIndicador_Meta] DESC";
         }
         else if ( ( AV13OrderedBy == 9 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosIndicador_InstrumentoMedicao]";
         }
         else if ( ( AV13OrderedBy == 9 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosIndicador_InstrumentoMedicao] DESC";
         }
         else if ( ( AV13OrderedBy == 10 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosIndicador_Tipo]";
         }
         else if ( ( AV13OrderedBy == 10 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosIndicador_Tipo] DESC";
         }
         else if ( ( AV13OrderedBy == 11 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosIndicador_Periodicidade]";
         }
         else if ( ( AV13OrderedBy == 11 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosIndicador_Periodicidade] DESC";
         }
         else if ( ( AV13OrderedBy == 12 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosIndicador_Vigencia]";
         }
         else if ( ( AV13OrderedBy == 12 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosIndicador_Vigencia] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosIndicador_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00JG5( IGxContext context ,
                                             String A1308ContratoServicosIndicador_Tipo ,
                                             IGxCollection AV69TFContratoServicosIndicador_Tipo_Sels ,
                                             String A1309ContratoServicosIndicador_Periodicidade ,
                                             IGxCollection AV73TFContratoServicosIndicador_Periodicidade_Sels ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             String AV17ContratoServicosIndicador_Indicador1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             short AV20DynamicFiltersOperator2 ,
                                             String AV21ContratoServicosIndicador_Indicador2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             short AV24DynamicFiltersOperator3 ,
                                             String AV25ContratoServicosIndicador_Indicador3 ,
                                             int AV32TFContratoServicosIndicador_Codigo ,
                                             int AV33TFContratoServicosIndicador_Codigo_To ,
                                             int AV36TFContratoServicosIndicador_CntSrvCod ,
                                             int AV37TFContratoServicosIndicador_CntSrvCod_To ,
                                             int AV40TFContratoServicosIndicador_ContratoCod ,
                                             int AV41TFContratoServicosIndicador_ContratoCod_To ,
                                             int AV44TFContratoServicosIndicador_AreaTrabalhoCod ,
                                             int AV45TFContratoServicosIndicador_AreaTrabalhoCod_To ,
                                             short AV48TFContratoServicosIndicador_Numero ,
                                             short AV49TFContratoServicosIndicador_Numero_To ,
                                             String AV53TFContratoServicosIndicador_Indicador_Sel ,
                                             String AV52TFContratoServicosIndicador_Indicador ,
                                             String AV57TFContratoServicosIndicador_Finalidade_Sel ,
                                             String AV56TFContratoServicosIndicador_Finalidade ,
                                             String AV61TFContratoServicosIndicador_Meta_Sel ,
                                             String AV60TFContratoServicosIndicador_Meta ,
                                             String AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel ,
                                             String AV64TFContratoServicosIndicador_InstrumentoMedicao ,
                                             int AV69TFContratoServicosIndicador_Tipo_Sels_Count ,
                                             int AV73TFContratoServicosIndicador_Periodicidade_Sels_Count ,
                                             String AV77TFContratoServicosIndicador_Vigencia_Sel ,
                                             String AV76TFContratoServicosIndicador_Vigencia ,
                                             String A1274ContratoServicosIndicador_Indicador ,
                                             int A1269ContratoServicosIndicador_Codigo ,
                                             int A1270ContratoServicosIndicador_CntSrvCod ,
                                             int A1296ContratoServicosIndicador_ContratoCod ,
                                             int A1295ContratoServicosIndicador_AreaTrabalhoCod ,
                                             short A1271ContratoServicosIndicador_Numero ,
                                             String A1305ContratoServicosIndicador_Finalidade ,
                                             String A1306ContratoServicosIndicador_Meta ,
                                             String A1307ContratoServicosIndicador_InstrumentoMedicao ,
                                             String A1310ContratoServicosIndicador_Vigencia ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             short AV80TFContratoServicosIndicador_QtdeFaixas ,
                                             short A1298ContratoServicosIndicador_QtdeFaixas ,
                                             short AV81TFContratoServicosIndicador_QtdeFaixas_To )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [30] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ((([ContratoServicosIndicador] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoServicosIndicador_CntSrvCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContratoServicosIndicador_QtdeFaixas, [ContratoServicosIndicador_Codigo] FROM [ContratoServicosIndicadorFaixas] WITH (NOLOCK) GROUP BY [ContratoServicosIndicador_Codigo] ) T4 ON T4.[ContratoServicosIndicador_Codigo] = T1.[ContratoServicosIndicador_Codigo])";
         scmdbuf = scmdbuf + " WHERE ((@AV80TFContratoServicosIndicador_QtdeFaixas = convert(int, 0)) or ( COALESCE( T4.[ContratoServicosIndicador_QtdeFaixas], 0) >= @AV80TFContratoServicosIndicador_QtdeFaixas))";
         scmdbuf = scmdbuf + " and ((@AV81TFContratoServicosIndicador_QtdeFaixas_To = convert(int, 0)) or ( COALESCE( T4.[ContratoServicosIndicador_QtdeFaixas], 0) <= @AV81TFContratoServicosIndicador_QtdeFaixas_To))";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17ContratoServicosIndicador_Indicador1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like @lV17ContratoServicosIndicador_Indicador1)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17ContratoServicosIndicador_Indicador1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like '%' + @lV17ContratoServicosIndicador_Indicador1)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ( AV20DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21ContratoServicosIndicador_Indicador2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like @lV21ContratoServicosIndicador_Indicador2)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ( AV20DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21ContratoServicosIndicador_Indicador2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like '%' + @lV21ContratoServicosIndicador_Indicador2)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ( AV24DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ContratoServicosIndicador_Indicador3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like @lV25ContratoServicosIndicador_Indicador3)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ( AV24DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ContratoServicosIndicador_Indicador3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like '%' + @lV25ContratoServicosIndicador_Indicador3)";
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! (0==AV32TFContratoServicosIndicador_Codigo) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Codigo] >= @AV32TFContratoServicosIndicador_Codigo)";
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! (0==AV33TFContratoServicosIndicador_Codigo_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Codigo] <= @AV33TFContratoServicosIndicador_Codigo_To)";
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ! (0==AV36TFContratoServicosIndicador_CntSrvCod) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_CntSrvCod] >= @AV36TFContratoServicosIndicador_CntSrvCod)";
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( ! (0==AV37TFContratoServicosIndicador_CntSrvCod_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_CntSrvCod] <= @AV37TFContratoServicosIndicador_CntSrvCod_To)";
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( ! (0==AV40TFContratoServicosIndicador_ContratoCod) )
         {
            sWhereString = sWhereString + " and (T2.[Contrato_Codigo] >= @AV40TFContratoServicosIndicador_ContratoCod)";
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( ! (0==AV41TFContratoServicosIndicador_ContratoCod_To) )
         {
            sWhereString = sWhereString + " and (T2.[Contrato_Codigo] <= @AV41TFContratoServicosIndicador_ContratoCod_To)";
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( ! (0==AV44TFContratoServicosIndicador_AreaTrabalhoCod) )
         {
            sWhereString = sWhereString + " and (T3.[Contrato_AreaTrabalhoCod] >= @AV44TFContratoServicosIndicador_AreaTrabalhoCod)";
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( ! (0==AV45TFContratoServicosIndicador_AreaTrabalhoCod_To) )
         {
            sWhereString = sWhereString + " and (T3.[Contrato_AreaTrabalhoCod] <= @AV45TFContratoServicosIndicador_AreaTrabalhoCod_To)";
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( ! (0==AV48TFContratoServicosIndicador_Numero) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Numero] >= @AV48TFContratoServicosIndicador_Numero)";
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( ! (0==AV49TFContratoServicosIndicador_Numero_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Numero] <= @AV49TFContratoServicosIndicador_Numero_To)";
         }
         else
         {
            GXv_int4[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV53TFContratoServicosIndicador_Indicador_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52TFContratoServicosIndicador_Indicador)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like @lV52TFContratoServicosIndicador_Indicador)";
         }
         else
         {
            GXv_int4[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53TFContratoServicosIndicador_Indicador_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] = @AV53TFContratoServicosIndicador_Indicador_Sel)";
         }
         else
         {
            GXv_int4[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV57TFContratoServicosIndicador_Finalidade_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56TFContratoServicosIndicador_Finalidade)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Finalidade] like @lV56TFContratoServicosIndicador_Finalidade)";
         }
         else
         {
            GXv_int4[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57TFContratoServicosIndicador_Finalidade_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Finalidade] = @AV57TFContratoServicosIndicador_Finalidade_Sel)";
         }
         else
         {
            GXv_int4[23] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV61TFContratoServicosIndicador_Meta_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFContratoServicosIndicador_Meta)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Meta] like @lV60TFContratoServicosIndicador_Meta)";
         }
         else
         {
            GXv_int4[24] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61TFContratoServicosIndicador_Meta_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Meta] = @AV61TFContratoServicosIndicador_Meta_Sel)";
         }
         else
         {
            GXv_int4[25] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64TFContratoServicosIndicador_InstrumentoMedicao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_InstrumentoMedicao] like @lV64TFContratoServicosIndicador_InstrumentoMedicao)";
         }
         else
         {
            GXv_int4[26] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_InstrumentoMedicao] = @AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel)";
         }
         else
         {
            GXv_int4[27] = 1;
         }
         if ( AV69TFContratoServicosIndicador_Tipo_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV69TFContratoServicosIndicador_Tipo_Sels, "T1.[ContratoServicosIndicador_Tipo] IN (", ")") + ")";
         }
         if ( AV73TFContratoServicosIndicador_Periodicidade_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV73TFContratoServicosIndicador_Periodicidade_Sels, "T1.[ContratoServicosIndicador_Periodicidade] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV77TFContratoServicosIndicador_Vigencia_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76TFContratoServicosIndicador_Vigencia)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Vigencia] like @lV76TFContratoServicosIndicador_Vigencia)";
         }
         else
         {
            GXv_int4[28] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77TFContratoServicosIndicador_Vigencia_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Vigencia] = @AV77TFContratoServicosIndicador_Vigencia_Sel)";
         }
         else
         {
            GXv_int4[29] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 9 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 9 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 10 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 10 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 11 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 11 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 12 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 12 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00JG3(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (short)dynConstraints[23] , (short)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (int)dynConstraints[33] , (int)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] , (int)dynConstraints[40] , (int)dynConstraints[41] , (short)dynConstraints[42] , (String)dynConstraints[43] , (String)dynConstraints[44] , (String)dynConstraints[45] , (String)dynConstraints[46] , (short)dynConstraints[47] , (bool)dynConstraints[48] , (short)dynConstraints[49] , (short)dynConstraints[50] , (short)dynConstraints[51] );
               case 1 :
                     return conditional_H00JG5(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (short)dynConstraints[23] , (short)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (int)dynConstraints[33] , (int)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] , (int)dynConstraints[40] , (int)dynConstraints[41] , (short)dynConstraints[42] , (String)dynConstraints[43] , (String)dynConstraints[44] , (String)dynConstraints[45] , (String)dynConstraints[46] , (short)dynConstraints[47] , (bool)dynConstraints[48] , (short)dynConstraints[49] , (short)dynConstraints[50] , (short)dynConstraints[51] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00JG3 ;
          prmH00JG3 = new Object[] {
          new Object[] {"@AV80TFContratoServicosIndicador_QtdeFaixas",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV80TFContratoServicosIndicador_QtdeFaixas",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV81TFContratoServicosIndicador_QtdeFaixas_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV81TFContratoServicosIndicador_QtdeFaixas_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV17ContratoServicosIndicador_Indicador1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV17ContratoServicosIndicador_Indicador1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV21ContratoServicosIndicador_Indicador2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV21ContratoServicosIndicador_Indicador2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV25ContratoServicosIndicador_Indicador3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV25ContratoServicosIndicador_Indicador3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV32TFContratoServicosIndicador_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV33TFContratoServicosIndicador_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV36TFContratoServicosIndicador_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV37TFContratoServicosIndicador_CntSrvCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV40TFContratoServicosIndicador_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV41TFContratoServicosIndicador_ContratoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV44TFContratoServicosIndicador_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV45TFContratoServicosIndicador_AreaTrabalhoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV48TFContratoServicosIndicador_Numero",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV49TFContratoServicosIndicador_Numero_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV52TFContratoServicosIndicador_Indicador",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV53TFContratoServicosIndicador_Indicador_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV56TFContratoServicosIndicador_Finalidade",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV57TFContratoServicosIndicador_Finalidade_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV60TFContratoServicosIndicador_Meta",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV61TFContratoServicosIndicador_Meta_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV64TFContratoServicosIndicador_InstrumentoMedicao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV76TFContratoServicosIndicador_Vigencia",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV77TFContratoServicosIndicador_Vigencia_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00JG5 ;
          prmH00JG5 = new Object[] {
          new Object[] {"@AV80TFContratoServicosIndicador_QtdeFaixas",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV80TFContratoServicosIndicador_QtdeFaixas",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV81TFContratoServicosIndicador_QtdeFaixas_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV81TFContratoServicosIndicador_QtdeFaixas_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV17ContratoServicosIndicador_Indicador1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV17ContratoServicosIndicador_Indicador1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV21ContratoServicosIndicador_Indicador2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV21ContratoServicosIndicador_Indicador2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV25ContratoServicosIndicador_Indicador3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV25ContratoServicosIndicador_Indicador3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV32TFContratoServicosIndicador_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV33TFContratoServicosIndicador_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV36TFContratoServicosIndicador_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV37TFContratoServicosIndicador_CntSrvCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV40TFContratoServicosIndicador_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV41TFContratoServicosIndicador_ContratoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV44TFContratoServicosIndicador_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV45TFContratoServicosIndicador_AreaTrabalhoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV48TFContratoServicosIndicador_Numero",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV49TFContratoServicosIndicador_Numero_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV52TFContratoServicosIndicador_Indicador",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV53TFContratoServicosIndicador_Indicador_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV56TFContratoServicosIndicador_Finalidade",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV57TFContratoServicosIndicador_Finalidade_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV60TFContratoServicosIndicador_Meta",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV61TFContratoServicosIndicador_Meta_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV64TFContratoServicosIndicador_InstrumentoMedicao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV65TFContratoServicosIndicador_InstrumentoMedicao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV76TFContratoServicosIndicador_Vigencia",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV77TFContratoServicosIndicador_Vigencia_Sel",SqlDbType.VarChar,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00JG3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JG3,11,0,true,false )
             ,new CursorDef("H00JG5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JG5,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 2) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getLongVarchar(7) ;
                ((short[]) buf[13])[0] = rslt.getShort(8) ;
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((int[]) buf[18])[0] = rslt.getInt(11) ;
                ((int[]) buf[19])[0] = rslt.getInt(12) ;
                ((short[]) buf[20])[0] = rslt.getShort(13) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[35]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[36]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[37]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[38]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[48]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[50]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[51]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[52]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[53]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[54]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[58]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[65]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[66]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[67]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[68]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[69]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[30]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[31]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[32]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[33]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[42]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[48]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[49]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[58]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                return;
       }
    }

 }

}
