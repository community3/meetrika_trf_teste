/*
               File: PRC_ImportarPFFS
        Description: Importar PFFS
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/18/2020 13:42:11.91
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Office;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class aprc_importarpffs : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            if ( ! entryPointCalled )
            {
               AV13Arquivo = gxfirstwebparm;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV10Aba = GetNextPar( );
                  AV22ColDmnn = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV76PraLinha = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV27ColPFBFSn = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV29ColPFLFSn = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV26ColPFBFMn = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV28ColPFLFMn = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV54Final = (bool)(BooleanUtil.Val(GetNextPar( )));
                  AV95ContratadaFS_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV97ContadorFS_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV99DataCnt = context.localUtil.ParseDateParm( GetNextPar( ));
                  AV19ColDataCntn = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV104RegraDivergencia = GetNextPar( );
                  AV114FileName = GetNextPar( );
                  AV115DataDmn = context.localUtil.ParseDateParm( GetNextPar( ));
                  AV116DataEntrega = context.localUtil.ParseDateParm( GetNextPar( ));
                  AV117DemandaFM = GetNextPar( );
                  AV135ColSistemaN = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV134ContratoservicosFS_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               }
            }
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public aprc_importarpffs( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public aprc_importarpffs( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Arquivo ,
                           String aP1_Aba ,
                           short aP2_ColDmnn ,
                           short aP3_PraLinha ,
                           short aP4_ColPFBFSn ,
                           short aP5_ColPFLFSn ,
                           short aP6_ColPFBFMn ,
                           short aP7_ColPFLFMn ,
                           bool aP8_Final ,
                           int aP9_ContratadaFS_Codigo ,
                           int aP10_ContadorFS_Codigo ,
                           DateTime aP11_DataCnt ,
                           short aP12_ColDataCntn ,
                           String aP13_RegraDivergencia ,
                           String aP14_FileName ,
                           DateTime aP15_DataDmn ,
                           DateTime aP16_DataEntrega ,
                           String aP17_DemandaFM ,
                           short aP18_ColSistemaN ,
                           ref int aP19_ContratoservicosFS_Codigo )
      {
         this.AV13Arquivo = aP0_Arquivo;
         this.AV10Aba = aP1_Aba;
         this.AV22ColDmnn = aP2_ColDmnn;
         this.AV76PraLinha = aP3_PraLinha;
         this.AV27ColPFBFSn = aP4_ColPFBFSn;
         this.AV29ColPFLFSn = aP5_ColPFLFSn;
         this.AV26ColPFBFMn = aP6_ColPFBFMn;
         this.AV28ColPFLFMn = aP7_ColPFLFMn;
         this.AV54Final = aP8_Final;
         this.AV95ContratadaFS_Codigo = aP9_ContratadaFS_Codigo;
         this.AV97ContadorFS_Codigo = aP10_ContadorFS_Codigo;
         this.AV99DataCnt = aP11_DataCnt;
         this.AV19ColDataCntn = aP12_ColDataCntn;
         this.AV104RegraDivergencia = aP13_RegraDivergencia;
         this.AV114FileName = aP14_FileName;
         this.AV115DataDmn = aP15_DataDmn;
         this.AV116DataEntrega = aP16_DataEntrega;
         this.AV117DemandaFM = aP17_DemandaFM;
         this.AV135ColSistemaN = aP18_ColSistemaN;
         this.AV134ContratoservicosFS_Codigo = aP19_ContratoservicosFS_Codigo;
         initialize();
         executePrivate();
         aP19_ContratoservicosFS_Codigo=this.AV134ContratoservicosFS_Codigo;
      }

      public int executeUdp( String aP0_Arquivo ,
                             String aP1_Aba ,
                             short aP2_ColDmnn ,
                             short aP3_PraLinha ,
                             short aP4_ColPFBFSn ,
                             short aP5_ColPFLFSn ,
                             short aP6_ColPFBFMn ,
                             short aP7_ColPFLFMn ,
                             bool aP8_Final ,
                             int aP9_ContratadaFS_Codigo ,
                             int aP10_ContadorFS_Codigo ,
                             DateTime aP11_DataCnt ,
                             short aP12_ColDataCntn ,
                             String aP13_RegraDivergencia ,
                             String aP14_FileName ,
                             DateTime aP15_DataDmn ,
                             DateTime aP16_DataEntrega ,
                             String aP17_DemandaFM ,
                             short aP18_ColSistemaN )
      {
         this.AV13Arquivo = aP0_Arquivo;
         this.AV10Aba = aP1_Aba;
         this.AV22ColDmnn = aP2_ColDmnn;
         this.AV76PraLinha = aP3_PraLinha;
         this.AV27ColPFBFSn = aP4_ColPFBFSn;
         this.AV29ColPFLFSn = aP5_ColPFLFSn;
         this.AV26ColPFBFMn = aP6_ColPFBFMn;
         this.AV28ColPFLFMn = aP7_ColPFLFMn;
         this.AV54Final = aP8_Final;
         this.AV95ContratadaFS_Codigo = aP9_ContratadaFS_Codigo;
         this.AV97ContadorFS_Codigo = aP10_ContadorFS_Codigo;
         this.AV99DataCnt = aP11_DataCnt;
         this.AV19ColDataCntn = aP12_ColDataCntn;
         this.AV104RegraDivergencia = aP13_RegraDivergencia;
         this.AV114FileName = aP14_FileName;
         this.AV115DataDmn = aP15_DataDmn;
         this.AV116DataEntrega = aP16_DataEntrega;
         this.AV117DemandaFM = aP17_DemandaFM;
         this.AV135ColSistemaN = aP18_ColSistemaN;
         this.AV134ContratoservicosFS_Codigo = aP19_ContratoservicosFS_Codigo;
         initialize();
         executePrivate();
         aP19_ContratoservicosFS_Codigo=this.AV134ContratoservicosFS_Codigo;
         return AV134ContratoservicosFS_Codigo ;
      }

      public void executeSubmit( String aP0_Arquivo ,
                                 String aP1_Aba ,
                                 short aP2_ColDmnn ,
                                 short aP3_PraLinha ,
                                 short aP4_ColPFBFSn ,
                                 short aP5_ColPFLFSn ,
                                 short aP6_ColPFBFMn ,
                                 short aP7_ColPFLFMn ,
                                 bool aP8_Final ,
                                 int aP9_ContratadaFS_Codigo ,
                                 int aP10_ContadorFS_Codigo ,
                                 DateTime aP11_DataCnt ,
                                 short aP12_ColDataCntn ,
                                 String aP13_RegraDivergencia ,
                                 String aP14_FileName ,
                                 DateTime aP15_DataDmn ,
                                 DateTime aP16_DataEntrega ,
                                 String aP17_DemandaFM ,
                                 short aP18_ColSistemaN ,
                                 ref int aP19_ContratoservicosFS_Codigo )
      {
         aprc_importarpffs objaprc_importarpffs;
         objaprc_importarpffs = new aprc_importarpffs();
         objaprc_importarpffs.AV13Arquivo = aP0_Arquivo;
         objaprc_importarpffs.AV10Aba = aP1_Aba;
         objaprc_importarpffs.AV22ColDmnn = aP2_ColDmnn;
         objaprc_importarpffs.AV76PraLinha = aP3_PraLinha;
         objaprc_importarpffs.AV27ColPFBFSn = aP4_ColPFBFSn;
         objaprc_importarpffs.AV29ColPFLFSn = aP5_ColPFLFSn;
         objaprc_importarpffs.AV26ColPFBFMn = aP6_ColPFBFMn;
         objaprc_importarpffs.AV28ColPFLFMn = aP7_ColPFLFMn;
         objaprc_importarpffs.AV54Final = aP8_Final;
         objaprc_importarpffs.AV95ContratadaFS_Codigo = aP9_ContratadaFS_Codigo;
         objaprc_importarpffs.AV97ContadorFS_Codigo = aP10_ContadorFS_Codigo;
         objaprc_importarpffs.AV99DataCnt = aP11_DataCnt;
         objaprc_importarpffs.AV19ColDataCntn = aP12_ColDataCntn;
         objaprc_importarpffs.AV104RegraDivergencia = aP13_RegraDivergencia;
         objaprc_importarpffs.AV114FileName = aP14_FileName;
         objaprc_importarpffs.AV115DataDmn = aP15_DataDmn;
         objaprc_importarpffs.AV116DataEntrega = aP16_DataEntrega;
         objaprc_importarpffs.AV117DemandaFM = aP17_DemandaFM;
         objaprc_importarpffs.AV135ColSistemaN = aP18_ColSistemaN;
         objaprc_importarpffs.AV134ContratoservicosFS_Codigo = aP19_ContratoservicosFS_Codigo;
         objaprc_importarpffs.context.SetSubmitInitialConfig(context);
         objaprc_importarpffs.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objaprc_importarpffs);
         aP19_ContratoservicosFS_Codigo=this.AV134ContratoservicosFS_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((aprc_importarpffs)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 6;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName("") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 1, 1, 15840, 12240, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*6));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV83WWPContext) ;
            /* Execute user subroutine: 'OPENFILE' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            AV12ArqTitulo = "Arquivo: " + AV114FileName;
            AV64Ln = AV76PraLinha;
            while ( (0==AV43Contratada_Codigo) && ! String.IsNullOrEmpty(StringUtil.RTrim( StringUtil.Trim( AV53ExcelDocument.get_Cells(AV64Ln, AV22ColDmnn, 1, 1).Text))) )
            {
               /* Execute user subroutine: 'LERDEMANDA' */
               S111 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
               /* Using cursor P00702 */
               pr_default.execute(0, new Object[] {AV46Demanda, AV83WWPContext.gxTpr_Areatrabalho_codigo});
               while ( (pr_default.getStatus(0) != 101) )
               {
                  A1553ContagemResultado_CntSrvCod = P00702_A1553ContagemResultado_CntSrvCod[0];
                  n1553ContagemResultado_CntSrvCod = P00702_n1553ContagemResultado_CntSrvCod[0];
                  A52Contratada_AreaTrabalhoCod = P00702_A52Contratada_AreaTrabalhoCod[0];
                  n52Contratada_AreaTrabalhoCod = P00702_n52Contratada_AreaTrabalhoCod[0];
                  A457ContagemResultado_Demanda = P00702_A457ContagemResultado_Demanda[0];
                  n457ContagemResultado_Demanda = P00702_n457ContagemResultado_Demanda[0];
                  A490ContagemResultado_ContratadaCod = P00702_A490ContagemResultado_ContratadaCod[0];
                  n490ContagemResultado_ContratadaCod = P00702_n490ContagemResultado_ContratadaCod[0];
                  A601ContagemResultado_Servico = P00702_A601ContagemResultado_Servico[0];
                  n601ContagemResultado_Servico = P00702_n601ContagemResultado_Servico[0];
                  A1046ContagemResultado_Agrupador = P00702_A1046ContagemResultado_Agrupador[0];
                  n1046ContagemResultado_Agrupador = P00702_n1046ContagemResultado_Agrupador[0];
                  A456ContagemResultado_Codigo = P00702_A456ContagemResultado_Codigo[0];
                  A601ContagemResultado_Servico = P00702_A601ContagemResultado_Servico[0];
                  n601ContagemResultado_Servico = P00702_n601ContagemResultado_Servico[0];
                  A52Contratada_AreaTrabalhoCod = P00702_A52Contratada_AreaTrabalhoCod[0];
                  n52Contratada_AreaTrabalhoCod = P00702_n52Contratada_AreaTrabalhoCod[0];
                  AV43Contratada_Codigo = A490ContagemResultado_ContratadaCod;
                  AV100Servico_Codigo = A601ContagemResultado_Servico;
                  AV90Agrupador = A1046ContagemResultado_Agrupador;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  pr_default.readNext(0);
               }
               pr_default.close(0);
               AV64Ln = (short)(AV64Ln+1);
            }
            AV101ContagemResltado_Deflator = (decimal)(1);
            /* Using cursor P00703 */
            pr_default.execute(1, new Object[] {AV134ContratoservicosFS_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A74Contrato_Codigo = P00703_A74Contrato_Codigo[0];
               A39Contratada_Codigo = P00703_A39Contratada_Codigo[0];
               A40Contratada_PessoaCod = P00703_A40Contratada_PessoaCod[0];
               A52Contratada_AreaTrabalhoCod = P00703_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = P00703_n52Contratada_AreaTrabalhoCod[0];
               A160ContratoServicos_Codigo = P00703_A160ContratoServicos_Codigo[0];
               A41Contratada_PessoaNom = P00703_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = P00703_n41Contratada_PessoaNom[0];
               A558Servico_Percentual = P00703_A558Servico_Percentual[0];
               n558Servico_Percentual = P00703_n558Servico_Percentual[0];
               A116Contrato_ValorUnidadeContratacao = P00703_A116Contrato_ValorUnidadeContratacao[0];
               A1224ContratoServicos_PrazoCorrecao = P00703_A1224ContratoServicos_PrazoCorrecao[0];
               n1224ContratoServicos_PrazoCorrecao = P00703_n1224ContratoServicos_PrazoCorrecao[0];
               A1225ContratoServicos_PrazoCorrecaoTipo = P00703_A1225ContratoServicos_PrazoCorrecaoTipo[0];
               n1225ContratoServicos_PrazoCorrecaoTipo = P00703_n1225ContratoServicos_PrazoCorrecaoTipo[0];
               A1649ContratoServicos_PrazoInicio = P00703_A1649ContratoServicos_PrazoInicio[0];
               n1649ContratoServicos_PrazoInicio = P00703_n1649ContratoServicos_PrazoInicio[0];
               A1152ContratoServicos_PrazoAnalise = P00703_A1152ContratoServicos_PrazoAnalise[0];
               n1152ContratoServicos_PrazoAnalise = P00703_n1152ContratoServicos_PrazoAnalise[0];
               A39Contratada_Codigo = P00703_A39Contratada_Codigo[0];
               A116Contrato_ValorUnidadeContratacao = P00703_A116Contrato_ValorUnidadeContratacao[0];
               A40Contratada_PessoaCod = P00703_A40Contratada_PessoaCod[0];
               A52Contratada_AreaTrabalhoCod = P00703_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = P00703_n52Contratada_AreaTrabalhoCod[0];
               A41Contratada_PessoaNom = P00703_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = P00703_n41Contratada_PessoaNom[0];
               OV130PrazoInicio = AV130PrazoInicio;
               AV44ContratadaFS_Nome = A41Contratada_PessoaNom;
               AV101ContagemResltado_Deflator = A558Servico_Percentual;
               AV106ValorPFFS = A116Contrato_ValorUnidadeContratacao;
               AV119PrazoCoreecao = A1224ContratoServicos_PrazoCorrecao;
               AV120PrazoCorrecaoTipo = A1225ContratoServicos_PrazoCorrecaoTipo;
               AV130PrazoInicio = A1649ContratoServicos_PrazoInicio;
               AV124DiasParaAnalise = A1152ContratoServicos_PrazoAnalise;
               /* Using cursor P00704 */
               pr_default.execute(2, new Object[] {n52Contratada_AreaTrabalhoCod, A52Contratada_AreaTrabalhoCod});
               while ( (pr_default.getStatus(2) != 101) )
               {
                  A5AreaTrabalho_Codigo = P00704_A5AreaTrabalho_Codigo[0];
                  A29Contratante_Codigo = P00704_A29Contratante_Codigo[0];
                  n29Contratante_Codigo = P00704_n29Contratante_Codigo[0];
                  A1192Contratante_FimDoExpediente = P00704_A1192Contratante_FimDoExpediente[0];
                  n1192Contratante_FimDoExpediente = P00704_n1192Contratante_FimDoExpediente[0];
                  A987AreaTrabalho_ContratadaUpdBslCod = P00704_A987AreaTrabalho_ContratadaUpdBslCod[0];
                  n987AreaTrabalho_ContratadaUpdBslCod = P00704_n987AreaTrabalho_ContratadaUpdBslCod[0];
                  A1192Contratante_FimDoExpediente = P00704_A1192Contratante_FimDoExpediente[0];
                  n1192Contratante_FimDoExpediente = P00704_n1192Contratante_FimDoExpediente[0];
                  /* Using cursor P00705 */
                  pr_default.execute(3, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo});
                  while ( (pr_default.getStatus(3) != 101) )
                  {
                     AV125FimDoExpediente = A1192Contratante_FimDoExpediente;
                     AV113AreaTrabalho_ContratadaUpdBslCod = A987AreaTrabalho_ContratadaUpdBslCod;
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                     /* Exiting from a For First loop. */
                     if (true) break;
                  }
                  pr_default.close(3);
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(2);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
            AV86EmailTextD = "Importa��o de Pontos de Fun��o da FS" + StringUtil.NewLine( ) + StringUtil.NewLine( );
            AV88EmailTextC = AV86EmailTextD;
            AV93StatusFinal = "H";
            AV94TxtStatusFinal = " homologadas";
            AV126ServerNow = DateTimeUtil.ServerNow( context, "DEFAULT");
            AV102ContagemResultado_DataCnt = AV99DataCnt;
            AV64Ln = AV76PraLinha;
            while ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ExcelDocument.get_Cells(AV64Ln, AV22ColDmnn, 1, 1).Text)) )
            {
               AV62Linha = StringUtil.Trim( StringUtil.Str( (decimal)(AV64Ln), 4, 0)) + " - ";
               AV51ErrCod = 0;
               if ( AV26ColPFBFMn > 0 )
               {
                  AV70PFBFM = (decimal)(AV53ExcelDocument.get_Cells(AV64Ln, AV26ColPFBFMn, 1, 1).Number);
                  if ( ( AV70PFBFM == Convert.ToDecimal( 0 )) )
                  {
                     AV70PFBFM = NumberUtil.Val( AV53ExcelDocument.get_Cells(AV64Ln, AV26ColPFBFMn, 1, 1).Value, ".");
                  }
                  AV74PFLFM = (decimal)(AV53ExcelDocument.get_Cells(AV64Ln, AV28ColPFLFMn, 1, 1).Number);
                  if ( ( AV74PFLFM == Convert.ToDecimal( 0 )) )
                  {
                     AV74PFLFM = NumberUtil.Val( AV53ExcelDocument.get_Cells(AV64Ln, AV28ColPFLFMn, 1, 1).Value, ".");
                  }
               }
               if ( AV27ColPFBFSn > 0 )
               {
                  AV71PFBFS = (decimal)(AV53ExcelDocument.get_Cells(AV64Ln, AV27ColPFBFSn, 1, 1).Number);
                  if ( ( AV71PFBFS == Convert.ToDecimal( 0 )) )
                  {
                     AV71PFBFS = NumberUtil.Val( AV53ExcelDocument.get_Cells(AV64Ln, AV27ColPFBFSn, 1, 1).Value, ".");
                  }
                  AV75PFLFS = (decimal)(AV53ExcelDocument.get_Cells(AV64Ln, AV29ColPFLFSn, 1, 1).Number);
                  if ( ( AV75PFLFS == Convert.ToDecimal( 0 )) )
                  {
                     AV75PFLFS = NumberUtil.Val( AV53ExcelDocument.get_Cells(AV64Ln, AV29ColPFLFSn, 1, 1).Value, ".");
                  }
               }
               if ( (DateTime.MinValue==AV99DataCnt) )
               {
                  AV102ContagemResultado_DataCnt = DateTimeUtil.ResetTime(AV53ExcelDocument.get_Cells(AV64Ln, AV19ColDataCntn, 1, 1).Date);
               }
               if ( ! (0==AV135ColSistemaN) )
               {
                  AV78Sistema_Sigla = StringUtil.Trim( AV53ExcelDocument.get_Cells(AV64Ln, AV135ColSistemaN, 1, 1).Value);
                  /* Execute user subroutine: 'BUSCA.CODIGO.SISTEMA' */
                  S221 ();
                  if ( returnInSub )
                  {
                     this.cleanup();
                     if (true) return;
                  }
               }
               if ( (Convert.ToDecimal(0)==AV71PFBFS) || (Convert.ToDecimal(0)==AV75PFLFS) )
               {
                  AV62Linha = AV62Linha + "PF da FS lidos contem valor 0!";
                  AV86EmailTextD = AV86EmailTextD + AV62Linha + StringUtil.NewLine( );
                  AV51ErrCod = 1;
               }
               else if ( (DateTime.MinValue==AV99DataCnt) && ( AV102ContagemResultado_DataCnt > AV126ServerNow ) )
               {
                  AV62Linha = AV62Linha + "Data da contagem lida maior � data de hoje!";
                  AV86EmailTextD = AV86EmailTextD + AV62Linha + StringUtil.NewLine( );
                  AV51ErrCod = 1;
               }
               else if ( (DateTime.MinValue==AV99DataCnt) && ( DateTimeUtil.DDiff( DateTimeUtil.ResetTime( AV126ServerNow) , AV102ContagemResultado_DataCnt ) > 364 ) )
               {
                  AV62Linha = AV62Linha + "Data da contagem lida n�o reconhecida ou muito antiga!";
                  AV86EmailTextD = AV86EmailTextD + AV62Linha + StringUtil.NewLine( );
                  AV51ErrCod = 1;
               }
               if ( AV51ErrCod == 0 )
               {
                  /* Execute user subroutine: 'LERDEMANDA' */
                  S111 ();
                  if ( returnInSub )
                  {
                     this.cleanup();
                     if (true) return;
                  }
                  AV35ContagemResultado_Codigo = 0;
                  AV127ContagemResultado_Codigos.Clear();
                  AV148GXLvl125 = 0;
                  pr_default.dynParam(4, new Object[]{ new Object[]{
                                                       AV117DemandaFM ,
                                                       A493ContagemResultado_DemandaFM ,
                                                       A490ContagemResultado_ContratadaCod ,
                                                       AV43Contratada_Codigo ,
                                                       A805ContagemResultado_ContratadaOrigemCod ,
                                                       AV95ContratadaFS_Codigo ,
                                                       A489ContagemResultado_SistemaCod ,
                                                       AV136Sistema_Codigo ,
                                                       AV46Demanda ,
                                                       A457ContagemResultado_Demanda },
                                                       new int[] {
                                                       TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                                       TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN
                                                       }
                  });
                  /* Using cursor P00706 */
                  pr_default.execute(4, new Object[] {AV46Demanda, AV43Contratada_Codigo, AV95ContratadaFS_Codigo, AV136Sistema_Codigo, AV117DemandaFM});
                  while ( (pr_default.getStatus(4) != 101) )
                  {
                     A457ContagemResultado_Demanda = P00706_A457ContagemResultado_Demanda[0];
                     n457ContagemResultado_Demanda = P00706_n457ContagemResultado_Demanda[0];
                     A490ContagemResultado_ContratadaCod = P00706_A490ContagemResultado_ContratadaCod[0];
                     n490ContagemResultado_ContratadaCod = P00706_n490ContagemResultado_ContratadaCod[0];
                     A805ContagemResultado_ContratadaOrigemCod = P00706_A805ContagemResultado_ContratadaOrigemCod[0];
                     n805ContagemResultado_ContratadaOrigemCod = P00706_n805ContagemResultado_ContratadaOrigemCod[0];
                     A489ContagemResultado_SistemaCod = P00706_A489ContagemResultado_SistemaCod[0];
                     n489ContagemResultado_SistemaCod = P00706_n489ContagemResultado_SistemaCod[0];
                     A493ContagemResultado_DemandaFM = P00706_A493ContagemResultado_DemandaFM[0];
                     n493ContagemResultado_DemandaFM = P00706_n493ContagemResultado_DemandaFM[0];
                     A456ContagemResultado_Codigo = P00706_A456ContagemResultado_Codigo[0];
                     A471ContagemResultado_DataDmn = P00706_A471ContagemResultado_DataDmn[0];
                     A472ContagemResultado_DataEntrega = P00706_A472ContagemResultado_DataEntrega[0];
                     n472ContagemResultado_DataEntrega = P00706_n472ContagemResultado_DataEntrega[0];
                     A912ContagemResultado_HoraEntrega = P00706_A912ContagemResultado_HoraEntrega[0];
                     n912ContagemResultado_HoraEntrega = P00706_n912ContagemResultado_HoraEntrega[0];
                     A1351ContagemResultado_DataPrevista = P00706_A1351ContagemResultado_DataPrevista[0];
                     n1351ContagemResultado_DataPrevista = P00706_n1351ContagemResultado_DataPrevista[0];
                     A484ContagemResultado_StatusDmn = P00706_A484ContagemResultado_StatusDmn[0];
                     n484ContagemResultado_StatusDmn = P00706_n484ContagemResultado_StatusDmn[0];
                     AV148GXLvl125 = 1;
                     AV51ErrCod = 0;
                     AV35ContagemResultado_Codigo = A456ContagemResultado_Codigo;
                     AV127ContagemResultado_Codigos.Add(A456ContagemResultado_Codigo, 0);
                     AV109ContagemResultado_StatusDmn = A484ContagemResultado_StatusDmn;
                     if ( StringUtil.StrCmp(AV104RegraDivergencia, "N") == 0 )
                     {
                        if ( ! (DateTime.MinValue==AV115DataDmn) )
                        {
                           A471ContagemResultado_DataDmn = AV115DataDmn;
                        }
                        if ( ! (DateTime.MinValue==AV116DataEntrega) )
                        {
                           A472ContagemResultado_DataEntrega = AV116DataEntrega;
                           n472ContagemResultado_DataEntrega = false;
                           A912ContagemResultado_HoraEntrega = AV118HoraEntrega;
                           n912ContagemResultado_HoraEntrega = false;
                           A1351ContagemResultado_DataPrevista = DateTimeUtil.ResetTime( AV116DataEntrega ) ;
                           n1351ContagemResultado_DataPrevista = false;
                        }
                     }
                     else
                     {
                        if ( AV54Final )
                        {
                           if ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "A") != 0 )
                           {
                              AV51ErrCod = 1;
                              AV62Linha = AV62Linha + AV46Demanda + " - com status " + A484ContagemResultado_StatusDmn + ", PF FS n�o importados e demanda n�o finalizada";
                              AV86EmailTextD = AV86EmailTextD + AV62Linha + StringUtil.NewLine( );
                           }
                        }
                        else
                        {
                           if ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "O") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "P") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "L") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "X") == 0 ) )
                           {
                              AV62Linha = AV62Linha + AV46Demanda + " - com status " + A484ContagemResultado_StatusDmn + ", PF FS n�o importados";
                              AV86EmailTextD = AV86EmailTextD + AV62Linha + StringUtil.NewLine( );
                              AV51ErrCod = 1;
                           }
                        }
                     }
                     /* Using cursor P00707 */
                     pr_default.execute(5, new Object[] {A471ContagemResultado_DataDmn, n472ContagemResultado_DataEntrega, A472ContagemResultado_DataEntrega, n912ContagemResultado_HoraEntrega, A912ContagemResultado_HoraEntrega, n1351ContagemResultado_DataPrevista, A1351ContagemResultado_DataPrevista, A456ContagemResultado_Codigo});
                     pr_default.close(5);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
                     pr_default.readNext(4);
                  }
                  pr_default.close(4);
                  if ( AV148GXLvl125 == 0 )
                  {
                     AV62Linha = AV62Linha + AV46Demanda + " - Demanda n�o achada.";
                     AV86EmailTextD = AV86EmailTextD + AV62Linha + StringUtil.NewLine( );
                     AV51ErrCod = 1;
                  }
                  if ( AV51ErrCod == 0 )
                  {
                     if ( AV54Final )
                     {
                        AV149GXLvl199 = 0;
                        /* Using cursor P00708 */
                        pr_default.execute(6, new Object[] {AV35ContagemResultado_Codigo});
                        while ( (pr_default.getStatus(6) != 101) )
                        {
                           GXT709 = 0;
                           A833ContagemResultado_CstUntPrd = P00708_A833ContagemResultado_CstUntPrd[0];
                           n833ContagemResultado_CstUntPrd = P00708_n833ContagemResultado_CstUntPrd[0];
                           A517ContagemResultado_Ultima = P00708_A517ContagemResultado_Ultima[0];
                           A482ContagemResultadoContagens_Esforco = P00708_A482ContagemResultadoContagens_Esforco[0];
                           A483ContagemResultado_StatusCnt = P00708_A483ContagemResultado_StatusCnt[0];
                           A469ContagemResultado_NaoCnfCntCod = P00708_A469ContagemResultado_NaoCnfCntCod[0];
                           n469ContagemResultado_NaoCnfCntCod = P00708_n469ContagemResultado_NaoCnfCntCod[0];
                           A470ContagemResultado_ContadorFMCod = P00708_A470ContagemResultado_ContadorFMCod[0];
                           A462ContagemResultado_Divergencia = P00708_A462ContagemResultado_Divergencia[0];
                           A461ContagemResultado_PFLFM = P00708_A461ContagemResultado_PFLFM[0];
                           n461ContagemResultado_PFLFM = P00708_n461ContagemResultado_PFLFM[0];
                           A460ContagemResultado_PFBFM = P00708_A460ContagemResultado_PFBFM[0];
                           n460ContagemResultado_PFBFM = P00708_n460ContagemResultado_PFBFM[0];
                           A459ContagemResultado_PFLFS = P00708_A459ContagemResultado_PFLFS[0];
                           n459ContagemResultado_PFLFS = P00708_n459ContagemResultado_PFLFS[0];
                           A458ContagemResultado_PFBFS = P00708_A458ContagemResultado_PFBFS[0];
                           n458ContagemResultado_PFBFS = P00708_n458ContagemResultado_PFBFS[0];
                           A511ContagemResultado_HoraCnt = P00708_A511ContagemResultado_HoraCnt[0];
                           A473ContagemResultado_DataCnt = P00708_A473ContagemResultado_DataCnt[0];
                           A456ContagemResultado_Codigo = P00708_A456ContagemResultado_Codigo[0];
                           A1756ContagemResultado_NvlCnt = P00708_A1756ContagemResultado_NvlCnt[0];
                           n1756ContagemResultado_NvlCnt = P00708_n1756ContagemResultado_NvlCnt[0];
                           A901ContagemResultadoContagens_Prazo = P00708_A901ContagemResultadoContagens_Prazo[0];
                           n901ContagemResultadoContagens_Prazo = P00708_n901ContagemResultadoContagens_Prazo[0];
                           A854ContagemResultado_TipoPla = P00708_A854ContagemResultado_TipoPla[0];
                           n854ContagemResultado_TipoPla = P00708_n854ContagemResultado_TipoPla[0];
                           A852ContagemResultado_Planilha_Filetype = A854ContagemResultado_TipoPla;
                           A853ContagemResultado_NomePla = P00708_A853ContagemResultado_NomePla[0];
                           n853ContagemResultado_NomePla = P00708_n853ContagemResultado_NomePla[0];
                           A852ContagemResultado_Planilha_Filename = A853ContagemResultado_NomePla;
                           A800ContagemResultado_Deflator = P00708_A800ContagemResultado_Deflator[0];
                           n800ContagemResultado_Deflator = P00708_n800ContagemResultado_Deflator[0];
                           A463ContagemResultado_ParecerTcn = P00708_A463ContagemResultado_ParecerTcn[0];
                           n463ContagemResultado_ParecerTcn = P00708_n463ContagemResultado_ParecerTcn[0];
                           A481ContagemResultado_TimeCnt = P00708_A481ContagemResultado_TimeCnt[0];
                           n481ContagemResultado_TimeCnt = P00708_n481ContagemResultado_TimeCnt[0];
                           A484ContagemResultado_StatusDmn = P00708_A484ContagemResultado_StatusDmn[0];
                           n484ContagemResultado_StatusDmn = P00708_n484ContagemResultado_StatusDmn[0];
                           A1348ContagemResultado_DataHomologacao = P00708_A1348ContagemResultado_DataHomologacao[0];
                           n1348ContagemResultado_DataHomologacao = P00708_n1348ContagemResultado_DataHomologacao[0];
                           A852ContagemResultado_Planilha = P00708_A852ContagemResultado_Planilha[0];
                           n852ContagemResultado_Planilha = P00708_n852ContagemResultado_Planilha[0];
                           A484ContagemResultado_StatusDmn = P00708_A484ContagemResultado_StatusDmn[0];
                           n484ContagemResultado_StatusDmn = P00708_n484ContagemResultado_StatusDmn[0];
                           A1348ContagemResultado_DataHomologacao = P00708_A1348ContagemResultado_DataHomologacao[0];
                           n1348ContagemResultado_DataHomologacao = P00708_n1348ContagemResultado_DataHomologacao[0];
                           W456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
                           AV149GXLvl199 = 1;
                           if ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "A") == 0 ) && ( A460ContagemResultado_PFBFM == AV71PFBFS ) && ( A461ContagemResultado_PFLFM == AV75PFLFS ) )
                           {
                              A458ContagemResultado_PFBFS = AV71PFBFS;
                              n458ContagemResultado_PFBFS = false;
                              A459ContagemResultado_PFLFS = AV75PFLFS;
                              n459ContagemResultado_PFLFS = false;
                              A462ContagemResultado_Divergencia = 0;
                              A483ContagemResultado_StatusCnt = 5;
                              A484ContagemResultado_StatusDmn = AV93StatusFinal;
                              n484ContagemResultado_StatusDmn = false;
                           }
                           else
                           {
                              A484ContagemResultado_StatusDmn = AV93StatusFinal;
                              n484ContagemResultado_StatusDmn = false;
                              if ( StringUtil.StrCmp(AV93StatusFinal, "H") == 0 )
                              {
                                 A1348ContagemResultado_DataHomologacao = AV126ServerNow;
                                 n1348ContagemResultado_DataHomologacao = false;
                              }
                              A517ContagemResultado_Ultima = false;
                              AV31ContadorFMCod = A470ContagemResultado_ContadorFMCod;
                              AV38ContagemResultado_CstUntPrd = A833ContagemResultado_CstUntPrd;
                              /*
                                 INSERT RECORD ON TABLE ContagemResultadoContagens

                              */
                              W456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
                              W470ContagemResultado_ContadorFMCod = A470ContagemResultado_ContadorFMCod;
                              W833ContagemResultado_CstUntPrd = A833ContagemResultado_CstUntPrd;
                              n833ContagemResultado_CstUntPrd = false;
                              W473ContagemResultado_DataCnt = A473ContagemResultado_DataCnt;
                              W511ContagemResultado_HoraCnt = A511ContagemResultado_HoraCnt;
                              W458ContagemResultado_PFBFS = A458ContagemResultado_PFBFS;
                              n458ContagemResultado_PFBFS = false;
                              W459ContagemResultado_PFLFS = A459ContagemResultado_PFLFS;
                              n459ContagemResultado_PFLFS = false;
                              W460ContagemResultado_PFBFM = A460ContagemResultado_PFBFM;
                              n460ContagemResultado_PFBFM = false;
                              W461ContagemResultado_PFLFM = A461ContagemResultado_PFLFM;
                              n461ContagemResultado_PFLFM = false;
                              W482ContagemResultadoContagens_Esforco = A482ContagemResultadoContagens_Esforco;
                              W462ContagemResultado_Divergencia = A462ContagemResultado_Divergencia;
                              W483ContagemResultado_StatusCnt = A483ContagemResultado_StatusCnt;
                              W469ContagemResultado_NaoCnfCntCod = A469ContagemResultado_NaoCnfCntCod;
                              n469ContagemResultado_NaoCnfCntCod = false;
                              W517ContagemResultado_Ultima = A517ContagemResultado_Ultima;
                              A456ContagemResultado_Codigo = AV35ContagemResultado_Codigo;
                              A470ContagemResultado_ContadorFMCod = AV31ContadorFMCod;
                              A833ContagemResultado_CstUntPrd = AV38ContagemResultado_CstUntPrd;
                              n833ContagemResultado_CstUntPrd = false;
                              A473ContagemResultado_DataCnt = DateTimeUtil.ResetTime(AV126ServerNow);
                              A511ContagemResultado_HoraCnt = context.localUtil.ServerTime( context, "DEFAULT");
                              A458ContagemResultado_PFBFS = AV71PFBFS;
                              n458ContagemResultado_PFBFS = false;
                              A459ContagemResultado_PFLFS = AV75PFLFS;
                              n459ContagemResultado_PFLFS = false;
                              A460ContagemResultado_PFBFM = AV70PFBFM;
                              n460ContagemResultado_PFBFM = false;
                              A461ContagemResultado_PFLFM = AV74PFLFM;
                              n461ContagemResultado_PFLFM = false;
                              A482ContagemResultadoContagens_Esforco = 0;
                              A462ContagemResultado_Divergencia = 0;
                              A483ContagemResultado_StatusCnt = 5;
                              A469ContagemResultado_NaoCnfCntCod = 0;
                              n469ContagemResultado_NaoCnfCntCod = false;
                              n469ContagemResultado_NaoCnfCntCod = true;
                              A517ContagemResultado_Ultima = true;
                              AV62Linha = AV62Linha + "New";
                              /* Using cursor P00709 */
                              A853ContagemResultado_NomePla = FileUtil.GetFileName( A852ContagemResultado_Planilha);
                              n853ContagemResultado_NomePla = false;
                              A854ContagemResultado_TipoPla = FileUtil.GetFileType( A852ContagemResultado_Planilha);
                              n854ContagemResultado_TipoPla = false;
                              pr_default.execute(7, new Object[] {A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt, n481ContagemResultado_TimeCnt, A481ContagemResultado_TimeCnt, n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, n460ContagemResultado_PFBFM, A460ContagemResultado_PFBFM, n461ContagemResultado_PFLFM, A461ContagemResultado_PFLFM, A462ContagemResultado_Divergencia, n463ContagemResultado_ParecerTcn, A463ContagemResultado_ParecerTcn, A470ContagemResultado_ContadorFMCod, n469ContagemResultado_NaoCnfCntCod, A469ContagemResultado_NaoCnfCntCod, A483ContagemResultado_StatusCnt, A482ContagemResultadoContagens_Esforco, A517ContagemResultado_Ultima, n800ContagemResultado_Deflator, A800ContagemResultado_Deflator, n833ContagemResultado_CstUntPrd, A833ContagemResultado_CstUntPrd, n852ContagemResultado_Planilha, A852ContagemResultado_Planilha, n853ContagemResultado_NomePla, A853ContagemResultado_NomePla, n854ContagemResultado_TipoPla, A854ContagemResultado_TipoPla, n901ContagemResultadoContagens_Prazo, A901ContagemResultadoContagens_Prazo, n1756ContagemResultado_NvlCnt, A1756ContagemResultado_NvlCnt});
                              pr_default.close(7);
                              dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
                              if ( (pr_default.getStatus(7) == 1) )
                              {
                                 context.Gx_err = 1;
                                 Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
                              }
                              else
                              {
                                 context.Gx_err = 0;
                                 Gx_emsg = "";
                              }
                              A456ContagemResultado_Codigo = W456ContagemResultado_Codigo;
                              A470ContagemResultado_ContadorFMCod = W470ContagemResultado_ContadorFMCod;
                              A833ContagemResultado_CstUntPrd = W833ContagemResultado_CstUntPrd;
                              n833ContagemResultado_CstUntPrd = false;
                              A473ContagemResultado_DataCnt = W473ContagemResultado_DataCnt;
                              A511ContagemResultado_HoraCnt = W511ContagemResultado_HoraCnt;
                              A458ContagemResultado_PFBFS = W458ContagemResultado_PFBFS;
                              n458ContagemResultado_PFBFS = false;
                              A459ContagemResultado_PFLFS = W459ContagemResultado_PFLFS;
                              n459ContagemResultado_PFLFS = false;
                              A460ContagemResultado_PFBFM = W460ContagemResultado_PFBFM;
                              n460ContagemResultado_PFBFM = false;
                              A461ContagemResultado_PFLFM = W461ContagemResultado_PFLFM;
                              n461ContagemResultado_PFLFM = false;
                              A482ContagemResultadoContagens_Esforco = W482ContagemResultadoContagens_Esforco;
                              A462ContagemResultado_Divergencia = W462ContagemResultado_Divergencia;
                              A483ContagemResultado_StatusCnt = W483ContagemResultado_StatusCnt;
                              A469ContagemResultado_NaoCnfCntCod = W469ContagemResultado_NaoCnfCntCod;
                              n469ContagemResultado_NaoCnfCntCod = false;
                              A517ContagemResultado_Ultima = W517ContagemResultado_Ultima;
                              /* End Insert */
                           }
                           AV11Aprovadas = (short)(AV11Aprovadas+1);
                           /* Execute user subroutine: 'NEWATUALIZABASELINE' */
                           S201 ();
                           if ( returnInSub )
                           {
                              pr_default.close(6);
                              this.cleanup();
                              if (true) return;
                           }
                           GXT709 = 1;
                           new prc_disparoservicovinculado(context ).execute(  A456ContagemResultado_Codigo,  AV83WWPContext.gxTpr_Userid) ;
                           AV62Linha = AV62Linha + AV46Demanda + " - Contagem inserida (";
                           AV62Linha = AV62Linha + StringUtil.Trim( StringUtil.Str( AV71PFBFS, 14, 5)) + ", " + StringUtil.Trim( StringUtil.Str( AV75PFLFS, 14, 5)) + " - " + StringUtil.Trim( StringUtil.Str( AV70PFBFM, 14, 5)) + ", " + StringUtil.Trim( StringUtil.Str( AV74PFLFM, 14, 5)) + ") " + context.localUtil.TToC( AV126ServerNow, 8, 5, 0, 3, "/", ":", " ");
                           /* Exit For each command. Update data (if necessary), close cursors & exit. */
                           /* Using cursor P007010 */
                           pr_default.execute(8, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n1348ContagemResultado_DataHomologacao, A1348ContagemResultado_DataHomologacao, A456ContagemResultado_Codigo});
                           pr_default.close(8);
                           dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
                           /* Using cursor P007011 */
                           pr_default.execute(9, new Object[] {A517ContagemResultado_Ultima, A483ContagemResultado_StatusCnt, A462ContagemResultado_Divergencia, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
                           pr_default.close(9);
                           dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
                           if ( GXT709 == 1 )
                           {
                              context.CommitDataStores( "PRC_ImportarPFFS");
                           }
                           if (true) break;
                           /* Using cursor P007012 */
                           pr_default.execute(10, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n1348ContagemResultado_DataHomologacao, A1348ContagemResultado_DataHomologacao, A456ContagemResultado_Codigo});
                           pr_default.close(10);
                           dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
                           /* Using cursor P007013 */
                           pr_default.execute(11, new Object[] {A517ContagemResultado_Ultima, A483ContagemResultado_StatusCnt, A462ContagemResultado_Divergencia, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
                           pr_default.close(11);
                           dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
                           A456ContagemResultado_Codigo = W456ContagemResultado_Codigo;
                           if ( GXT709 == 1 )
                           {
                              context.CommitDataStores( "PRC_ImportarPFFS");
                           }
                           pr_default.readNext(6);
                        }
                        pr_default.close(6);
                        if ( AV149GXLvl199 == 0 )
                        {
                           AV62Linha = AV62Linha + AV46Demanda + " - Contagem FM n�o achada.";
                           AV86EmailTextD = AV86EmailTextD + AV62Linha + StringUtil.NewLine( );
                           AV51ErrCod = 1;
                        }
                     }
                     else
                     {
                        AV150GXLvl260 = 0;
                        /* Using cursor P007014 */
                        pr_default.execute(12, new Object[] {AV35ContagemResultado_Codigo});
                        while ( (pr_default.getStatus(12) != 101) )
                        {
                           A517ContagemResultado_Ultima = P007014_A517ContagemResultado_Ultima[0];
                           A456ContagemResultado_Codigo = P007014_A456ContagemResultado_Codigo[0];
                           A484ContagemResultado_StatusDmn = P007014_A484ContagemResultado_StatusDmn[0];
                           n484ContagemResultado_StatusDmn = P007014_n484ContagemResultado_StatusDmn[0];
                           A799ContagemResultado_PFLFSImp = P007014_A799ContagemResultado_PFLFSImp[0];
                           n799ContagemResultado_PFLFSImp = P007014_n799ContagemResultado_PFLFSImp[0];
                           A798ContagemResultado_PFBFSImp = P007014_A798ContagemResultado_PFBFSImp[0];
                           n798ContagemResultado_PFBFSImp = P007014_n798ContagemResultado_PFBFSImp[0];
                           A459ContagemResultado_PFLFS = P007014_A459ContagemResultado_PFLFS[0];
                           n459ContagemResultado_PFLFS = P007014_n459ContagemResultado_PFLFS[0];
                           A458ContagemResultado_PFBFS = P007014_A458ContagemResultado_PFBFS[0];
                           n458ContagemResultado_PFBFS = P007014_n458ContagemResultado_PFBFS[0];
                           A460ContagemResultado_PFBFM = P007014_A460ContagemResultado_PFBFM[0];
                           n460ContagemResultado_PFBFM = P007014_n460ContagemResultado_PFBFM[0];
                           A461ContagemResultado_PFLFM = P007014_A461ContagemResultado_PFLFM[0];
                           n461ContagemResultado_PFLFM = P007014_n461ContagemResultado_PFLFM[0];
                           A1553ContagemResultado_CntSrvCod = P007014_A1553ContagemResultado_CntSrvCod[0];
                           n1553ContagemResultado_CntSrvCod = P007014_n1553ContagemResultado_CntSrvCod[0];
                           A483ContagemResultado_StatusCnt = P007014_A483ContagemResultado_StatusCnt[0];
                           A462ContagemResultado_Divergencia = P007014_A462ContagemResultado_Divergencia[0];
                           A1348ContagemResultado_DataHomologacao = P007014_A1348ContagemResultado_DataHomologacao[0];
                           n1348ContagemResultado_DataHomologacao = P007014_n1348ContagemResultado_DataHomologacao[0];
                           A890ContagemResultado_Responsavel = P007014_A890ContagemResultado_Responsavel[0];
                           n890ContagemResultado_Responsavel = P007014_n890ContagemResultado_Responsavel[0];
                           A1351ContagemResultado_DataPrevista = P007014_A1351ContagemResultado_DataPrevista[0];
                           n1351ContagemResultado_DataPrevista = P007014_n1351ContagemResultado_DataPrevista[0];
                           A473ContagemResultado_DataCnt = P007014_A473ContagemResultado_DataCnt[0];
                           A511ContagemResultado_HoraCnt = P007014_A511ContagemResultado_HoraCnt[0];
                           A484ContagemResultado_StatusDmn = P007014_A484ContagemResultado_StatusDmn[0];
                           n484ContagemResultado_StatusDmn = P007014_n484ContagemResultado_StatusDmn[0];
                           A799ContagemResultado_PFLFSImp = P007014_A799ContagemResultado_PFLFSImp[0];
                           n799ContagemResultado_PFLFSImp = P007014_n799ContagemResultado_PFLFSImp[0];
                           A798ContagemResultado_PFBFSImp = P007014_A798ContagemResultado_PFBFSImp[0];
                           n798ContagemResultado_PFBFSImp = P007014_n798ContagemResultado_PFBFSImp[0];
                           A1553ContagemResultado_CntSrvCod = P007014_A1553ContagemResultado_CntSrvCod[0];
                           n1553ContagemResultado_CntSrvCod = P007014_n1553ContagemResultado_CntSrvCod[0];
                           A1348ContagemResultado_DataHomologacao = P007014_A1348ContagemResultado_DataHomologacao[0];
                           n1348ContagemResultado_DataHomologacao = P007014_n1348ContagemResultado_DataHomologacao[0];
                           A890ContagemResultado_Responsavel = P007014_A890ContagemResultado_Responsavel[0];
                           n890ContagemResultado_Responsavel = P007014_n890ContagemResultado_Responsavel[0];
                           A1351ContagemResultado_DataPrevista = P007014_A1351ContagemResultado_DataPrevista[0];
                           n1351ContagemResultado_DataPrevista = P007014_n1351ContagemResultado_DataPrevista[0];
                           AV150GXLvl260 = 1;
                           AV132StatusAnt = A484ContagemResultado_StatusDmn;
                           if ( StringUtil.StrCmp(AV104RegraDivergencia, "N") == 0 )
                           {
                              AV62Linha = AV62Linha + AV46Demanda;
                              if ( ( A798ContagemResultado_PFBFSImp + A799ContagemResultado_PFLFSImp == Convert.ToDecimal( 0 )) )
                              {
                                 A798ContagemResultado_PFBFSImp = AV71PFBFS;
                                 n798ContagemResultado_PFBFSImp = false;
                                 A799ContagemResultado_PFLFSImp = AV75PFLFS;
                                 n799ContagemResultado_PFLFSImp = false;
                                 AV62Linha = AV62Linha + ", PF FS Imp importados";
                              }
                              else
                              {
                                 AV62Linha = AV62Linha + ", PF FS Imp j� preenchidos";
                              }
                              if ( ( A458ContagemResultado_PFBFS + A459ContagemResultado_PFLFS == Convert.ToDecimal( 0 )) )
                              {
                                 A458ContagemResultado_PFBFS = AV71PFBFS;
                                 n458ContagemResultado_PFBFS = false;
                                 A459ContagemResultado_PFLFS = AV75PFLFS;
                                 n459ContagemResultado_PFLFS = false;
                                 AV62Linha = AV62Linha + ", PF FS da contagem importados";
                              }
                              else
                              {
                                 AV62Linha = AV62Linha + ", PF FS da contagem j� preenchidos";
                              }
                           }
                           else
                           {
                              AV72PFFM = A460ContagemResultado_PFBFM;
                              AV73PFFS = AV71PFBFS;
                              AV8ContagemResultado_PFLFM = A461ContagemResultado_PFLFM;
                              AV9ContagemResultado_PFLFS = AV75PFLFS;
                              AV129ContratoServicos_Codigo = A1553ContagemResultado_CntSrvCod;
                              /* Execute user subroutine: 'CALCULADIVERGENCIA' */
                              S141 ();
                              if ( returnInSub )
                              {
                                 pr_default.close(12);
                                 this.cleanup();
                                 if (true) return;
                              }
                              A458ContagemResultado_PFBFS = AV71PFBFS;
                              n458ContagemResultado_PFBFS = false;
                              A459ContagemResultado_PFLFS = AV75PFLFS;
                              n459ContagemResultado_PFLFS = false;
                              A483ContagemResultado_StatusCnt = AV40ContagemResultado_StatusCnt;
                              A462ContagemResultado_Divergencia = AV39ContagemResultado_Divergencia;
                              if ( AV40ContagemResultado_StatusCnt == 5 )
                              {
                                 A484ContagemResultado_StatusDmn = AV93StatusFinal;
                                 n484ContagemResultado_StatusDmn = false;
                                 if ( StringUtil.StrCmp(AV93StatusFinal, "H") == 0 )
                                 {
                                    A1348ContagemResultado_DataHomologacao = AV126ServerNow;
                                    n1348ContagemResultado_DataHomologacao = false;
                                 }
                                 AV11Aprovadas = (short)(AV11Aprovadas+1);
                                 /* Execute user subroutine: 'NEWATUALIZABASELINE' */
                                 S201 ();
                                 if ( returnInSub )
                                 {
                                    pr_default.close(12);
                                    this.cleanup();
                                    if (true) return;
                                 }
                              }
                              else
                              {
                                 A484ContagemResultado_StatusDmn = "B";
                                 n484ContagemResultado_StatusDmn = false;
                                 /* Execute user subroutine: 'RESPONSAVEL' */
                                 S211 ();
                                 if ( returnInSub )
                                 {
                                    pr_default.close(12);
                                    this.cleanup();
                                    if (true) return;
                                 }
                                 A890ContagemResultado_Responsavel = AV131Responsavel;
                                 n890ContagemResultado_Responsavel = false;
                                 AV50Divergencias = (short)(AV50Divergencias+1);
                              }
                              A798ContagemResultado_PFBFSImp = AV71PFBFS;
                              n798ContagemResultado_PFBFSImp = false;
                              A799ContagemResultado_PFLFSImp = AV75PFLFS;
                              n799ContagemResultado_PFLFSImp = false;
                              new prc_inslogresponsavel(context ).execute( ref  AV35ContagemResultado_Codigo,  AV131Responsavel,  "I",  "D",  AV83WWPContext.gxTpr_Userid,  0,  AV132StatusAnt,  A484ContagemResultado_StatusDmn,  "PF da FS.",  A1351ContagemResultado_DataPrevista,  false) ;
                              new prc_disparoservicovinculado(context ).execute(  A456ContagemResultado_Codigo,  AV83WWPContext.gxTpr_Userid) ;
                              AV62Linha = AV62Linha + AV46Demanda + " - Contagem inserida (" + StringUtil.Trim( StringUtil.Str( AV71PFBFS, 14, 5)) + " - " + StringUtil.Trim( StringUtil.Str( AV75PFLFS, 14, 5)) + ") Status: " + gxdomainstatuscontagem.getDescription(context,AV40ContagemResultado_StatusCnt);
                           }
                           /* Exit For each command. Update data (if necessary), close cursors & exit. */
                           /* Using cursor P007015 */
                           pr_default.execute(13, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n799ContagemResultado_PFLFSImp, A799ContagemResultado_PFLFSImp, n798ContagemResultado_PFBFSImp, A798ContagemResultado_PFBFSImp, n1348ContagemResultado_DataHomologacao, A1348ContagemResultado_DataHomologacao, n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, A456ContagemResultado_Codigo});
                           pr_default.close(13);
                           dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
                           /* Using cursor P007016 */
                           pr_default.execute(14, new Object[] {n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, A483ContagemResultado_StatusCnt, A462ContagemResultado_Divergencia, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
                           pr_default.close(14);
                           dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
                           if (true) break;
                           /* Using cursor P007017 */
                           pr_default.execute(15, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n799ContagemResultado_PFLFSImp, A799ContagemResultado_PFLFSImp, n798ContagemResultado_PFBFSImp, A798ContagemResultado_PFBFSImp, n1348ContagemResultado_DataHomologacao, A1348ContagemResultado_DataHomologacao, n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, A456ContagemResultado_Codigo});
                           pr_default.close(15);
                           dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
                           /* Using cursor P007018 */
                           pr_default.execute(16, new Object[] {n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, A483ContagemResultado_StatusCnt, A462ContagemResultado_Divergencia, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
                           pr_default.close(16);
                           dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
                           pr_default.readNext(12);
                        }
                        pr_default.close(12);
                        if ( AV150GXLvl260 == 0 )
                        {
                           AV62Linha = AV62Linha + AV46Demanda + " - Contagem da FM n�o achada.";
                           AV86EmailTextD = AV86EmailTextD + AV62Linha + StringUtil.NewLine( );
                           AV51ErrCod = 1;
                        }
                        if ( ( AV51ErrCod == 0 ) && ! ( StringUtil.StrCmp(AV104RegraDivergencia, "N") == 0 ) )
                        {
                           /* Execute user subroutine: 'NEWCONTAGEMDAFS' */
                           S151 ();
                           if ( returnInSub )
                           {
                              this.cleanup();
                              if (true) return;
                           }
                        }
                     }
                  }
               }
               if ( AV51ErrCod == 1 )
               {
                  AV52Erros = (short)(AV52Erros+1);
               }
               H700( false, 20) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV62Linha, "")), 17, Gx_line+0, 799, Gx_line+15, 0+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+20);
               AV64Ln = (short)(AV64Ln+1);
               AV61Lidas = (short)(AV61Lidas+1);
            }
            AV79Totais = StringUtil.Trim( StringUtil.Str( (decimal)(AV61Lidas), 4, 0)) + " linhas processadas  -  " + StringUtil.Trim( StringUtil.Str( (decimal)(AV52Erros), 4, 0)) + " PF FS n�o importados" + ((AV27ColPFBFSn==0) ? "" : " - "+StringUtil.Trim( StringUtil.Str( (decimal)(AV50Divergencias), 4, 0))+" divergentes - "+StringUtil.Trim( StringUtil.Str( (decimal)(AV11Aprovadas), 4, 0))+AV94TxtStatusFinal) + " - " + StringUtil.Trim( StringUtil.Str( (decimal)(AV108Tratadas), 4, 0)) + " tratadas pela FS";
            H700( false, 35) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV79Totais, "")), 17, Gx_line+17, 747, Gx_line+35, 0+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+35);
            context.CommitDataStores( "PRC_ImportarPFFS");
            AV53ExcelDocument.Close();
            /* Execute user subroutine: 'ENVIAREMAILS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            context.nUserReturn = 1;
            this.cleanup();
            if (true) return;
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            H700( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LERDEMANDA' Routine */
         AV46Demanda = AV53ExcelDocument.get_Cells(AV64Ln, AV22ColDmnn, 1, 1).Text;
         AV46Demanda = StringUtil.Trim( AV46Demanda);
         AV46Demanda = StringUtil.StringReplace( AV46Demanda, "'", "");
         AV46Demanda = StringUtil.StringReplace( AV46Demanda, "\"", "");
         AV46Demanda = StringUtil.StringReplace( AV46Demanda, "�", "");
      }

      protected void S121( )
      {
         /* 'ENVIAREMAILS' Routine */
         if ( ! ( StringUtil.StrCmp(AV104RegraDivergencia, "N") == 0 ) )
         {
            if ( AV11Aprovadas > 0 )
            {
               AV84Usuarios.Clear();
               /* Using cursor P007019 */
               pr_default.execute(17, new Object[] {AV43Contratada_Codigo});
               while ( (pr_default.getStatus(17) != 101) )
               {
                  A293Usuario_EhFinanceiro = P007019_A293Usuario_EhFinanceiro[0];
                  n293Usuario_EhFinanceiro = P007019_n293Usuario_EhFinanceiro[0];
                  A66ContratadaUsuario_ContratadaCod = P007019_A66ContratadaUsuario_ContratadaCod[0];
                  A69ContratadaUsuario_UsuarioCod = P007019_A69ContratadaUsuario_UsuarioCod[0];
                  A293Usuario_EhFinanceiro = P007019_A293Usuario_EhFinanceiro[0];
                  n293Usuario_EhFinanceiro = P007019_n293Usuario_EhFinanceiro[0];
                  AV84Usuarios.Add(A69ContratadaUsuario_UsuarioCod, 0);
                  pr_default.readNext(17);
               }
               pr_default.close(17);
               if ( AV84Usuarios.Count > 0 )
               {
                  AV88EmailTextC = AV88EmailTextC + StringUtil.NewLine( ) + AV79Totais + StringUtil.NewLine( );
                  AV88EmailTextC = AV88EmailTextC + StringUtil.Trim( AV83WWPContext.gxTpr_Areatrabalho_descricao) + ", usu�rio " + StringUtil.Trim( AV83WWPContext.gxTpr_Username) + StringUtil.NewLine( );
                  AV87Subject = AV83WWPContext.gxTpr_Areatrabalho_descricao + " Demandas" + AV94TxtStatusFinal + " do lote " + StringUtil.Trim( AV90Agrupador) + " (No reply)";
                  AV87Subject = StringUtil.StringReplace( AV87Subject, "  ", " ");
                  AV82WebSession.Set("DemandaCodigo", AV127ContagemResultado_Codigos.ToXml(false, true, "Collection", ""));
                  AV91Resultado = "Env�o de" + AV94TxtStatusFinal + " para ";
                  new prc_enviaremail(context ).execute(  AV83WWPContext.gxTpr_Areatrabalho_codigo,  AV84Usuarios,  AV87Subject,  AV88EmailTextC,  AV85Attachments, ref  AV91Resultado) ;
                  AV82WebSession.Remove("DemandaCodigo");
               }
               else
               {
                  AV91Resultado = "N�o foram achados usu�rio de Financeiro para envio das demandas em diverg�ncia!";
               }
               H700( false, 32) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV91Resultado, "")), 17, Gx_line+17, 799, Gx_line+32, 0+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+32);
            }
            if ( AV50Divergencias > 0 )
            {
               AV84Usuarios.Clear();
               /* Using cursor P007020 */
               pr_default.execute(18, new Object[] {AV100Servico_Codigo, AV43Contratada_Codigo});
               while ( (pr_default.getStatus(18) != 101) )
               {
                  A291Usuario_EhContratada = P007020_A291Usuario_EhContratada[0];
                  n291Usuario_EhContratada = P007020_n291Usuario_EhContratada[0];
                  A74Contrato_Codigo = P007020_A74Contrato_Codigo[0];
                  A155Servico_Codigo = P007020_A155Servico_Codigo[0];
                  A39Contratada_Codigo = P007020_A39Contratada_Codigo[0];
                  A1013Contrato_PrepostoCod = P007020_A1013Contrato_PrepostoCod[0];
                  n1013Contrato_PrepostoCod = P007020_n1013Contrato_PrepostoCod[0];
                  A160ContratoServicos_Codigo = P007020_A160ContratoServicos_Codigo[0];
                  A39Contratada_Codigo = P007020_A39Contratada_Codigo[0];
                  A1013Contrato_PrepostoCod = P007020_A1013Contrato_PrepostoCod[0];
                  n1013Contrato_PrepostoCod = P007020_n1013Contrato_PrepostoCod[0];
                  A291Usuario_EhContratada = P007020_A291Usuario_EhContratada[0];
                  n291Usuario_EhContratada = P007020_n291Usuario_EhContratada[0];
                  AV84Usuarios.Add(A1013Contrato_PrepostoCod, 0);
                  /* Using cursor P007021 */
                  pr_default.execute(19, new Object[] {A74Contrato_Codigo, n291Usuario_EhContratada, A291Usuario_EhContratada});
                  while ( (pr_default.getStatus(19) != 101) )
                  {
                     A1078ContratoGestor_ContratoCod = P007021_A1078ContratoGestor_ContratoCod[0];
                     A1079ContratoGestor_UsuarioCod = P007021_A1079ContratoGestor_UsuarioCod[0];
                     AV84Usuarios.Add(A1079ContratoGestor_UsuarioCod, 0);
                     pr_default.readNext(19);
                  }
                  pr_default.close(19);
                  pr_default.readNext(18);
               }
               pr_default.close(18);
               if ( AV84Usuarios.Count > 0 )
               {
                  AV86EmailTextD = AV86EmailTextD + StringUtil.NewLine( ) + AV79Totais + StringUtil.NewLine( );
                  AV86EmailTextD = AV86EmailTextD + StringUtil.Trim( AV83WWPContext.gxTpr_Areatrabalho_descricao) + ", usu�rio " + StringUtil.Trim( AV83WWPContext.gxTpr_Username) + StringUtil.NewLine( );
                  AV87Subject = AV83WWPContext.gxTpr_Areatrabalho_descricao + " Demandas divergentes do lote " + StringUtil.Trim( AV90Agrupador) + " (No reply)";
                  AV87Subject = StringUtil.StringReplace( AV87Subject, "  ", " ");
                  AV82WebSession.Set("DemandaCodigo", AV127ContagemResultado_Codigos.ToXml(false, true, "Collection", ""));
                  AV91Resultado = "Env�o de diverg�ncias ";
                  new prc_enviaremail(context ).execute(  AV83WWPContext.gxTpr_Areatrabalho_codigo,  AV84Usuarios,  AV87Subject,  AV86EmailTextD,  AV85Attachments, ref  AV91Resultado) ;
                  AV82WebSession.Remove("DemandaCodigo");
               }
               else
               {
                  AV91Resultado = "N�o foi achado o usu�rio preposto no contrato para envio das demandas" + AV94TxtStatusFinal + "!";
               }
               H700( false, 32) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV91Resultado, "")), 17, Gx_line+17, 799, Gx_line+32, 0+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+32);
            }
         }
      }

      protected void S131( )
      {
         /* 'OPENFILE' Routine */
         AV51ErrCod = AV53ExcelDocument.Open(AV13Arquivo);
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10Aba)) )
         {
            AV51ErrCod = AV53ExcelDocument.SelectSheet(AV10Aba);
         }
      }

      protected void S141( )
      {
         /* 'CALCULADIVERGENCIA' Routine */
         AV105Calcular = true;
         if ( StringUtil.StrCmp(AV104RegraDivergencia, "M") == 0 )
         {
            if ( AV9ContagemResultado_PFLFS < AV8ContagemResultado_PFLFM )
            {
               AV39ContagemResultado_Divergencia = 0;
               AV40ContagemResultado_StatusCnt = 5;
               AV105Calcular = false;
            }
         }
         if ( AV105Calcular )
         {
            new prc_getparmindicedivergencia(context ).execute( ref  AV129ContratoServicos_Codigo, out  AV59IndiceDivergencia, out  AV15CalculoDivergencia, out  AV42ContagemResultado_ValorPF) ;
            GXt_decimal1 = AV39ContagemResultado_Divergencia;
            new prc_calculardivergencia(context ).execute(  AV15CalculoDivergencia,  AV73PFFS,  AV72PFFM,  AV9ContagemResultado_PFLFS,  AV8ContagemResultado_PFLFM, out  GXt_decimal1) ;
            AV39ContagemResultado_Divergencia = GXt_decimal1;
            if ( AV39ContagemResultado_Divergencia > AV59IndiceDivergencia )
            {
               AV40ContagemResultado_StatusCnt = 7;
            }
            else
            {
               AV40ContagemResultado_StatusCnt = 5;
            }
         }
      }

      protected void S151( )
      {
         /* 'NEWCONTAGEMDAFS' Routine */
         AV154GXLvl474 = 0;
         /* Using cursor P007022 */
         pr_default.execute(20, new Object[] {AV136Sistema_Codigo, AV46Demanda, AV95ContratadaFS_Codigo});
         while ( (pr_default.getStatus(20) != 101) )
         {
            A146Modulo_Codigo = P007022_A146Modulo_Codigo[0];
            n146Modulo_Codigo = P007022_n146Modulo_Codigo[0];
            A127Sistema_Codigo = P007022_A127Sistema_Codigo[0];
            A1399Sistema_ImpUserCod = P007022_A1399Sistema_ImpUserCod[0];
            n1399Sistema_ImpUserCod = P007022_n1399Sistema_ImpUserCod[0];
            A1073Usuario_CargoCod = P007022_A1073Usuario_CargoCod[0];
            n1073Usuario_CargoCod = P007022_n1073Usuario_CargoCod[0];
            A1075Usuario_CargoUOCod = P007022_A1075Usuario_CargoUOCod[0];
            n1075Usuario_CargoUOCod = P007022_n1075Usuario_CargoUOCod[0];
            A137Metodologia_Codigo = P007022_A137Metodologia_Codigo[0];
            n137Metodologia_Codigo = P007022_n137Metodologia_Codigo[0];
            A609TpUo_Codigo = P007022_A609TpUo_Codigo[0];
            n609TpUo_Codigo = P007022_n609TpUo_Codigo[0];
            A456ContagemResultado_Codigo = P007022_A456ContagemResultado_Codigo[0];
            A489ContagemResultado_SistemaCod = P007022_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P007022_n489ContagemResultado_SistemaCod[0];
            A490ContagemResultado_ContratadaCod = P007022_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P007022_n490ContagemResultado_ContratadaCod[0];
            A457ContagemResultado_Demanda = P007022_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P007022_n457ContagemResultado_Demanda[0];
            A602ContagemResultado_OSVinculada = P007022_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P007022_n602ContagemResultado_OSVinculada[0];
            A484ContagemResultado_StatusDmn = P007022_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P007022_n484ContagemResultado_StatusDmn[0];
            A1348ContagemResultado_DataHomologacao = P007022_A1348ContagemResultado_DataHomologacao[0];
            n1348ContagemResultado_DataHomologacao = P007022_n1348ContagemResultado_DataHomologacao[0];
            A912ContagemResultado_HoraEntrega = P007022_A912ContagemResultado_HoraEntrega[0];
            n912ContagemResultado_HoraEntrega = P007022_n912ContagemResultado_HoraEntrega[0];
            A1237ContagemResultado_PrazoMaisDias = P007022_A1237ContagemResultado_PrazoMaisDias[0];
            n1237ContagemResultado_PrazoMaisDias = P007022_n1237ContagemResultado_PrazoMaisDias[0];
            A1227ContagemResultado_PrazoInicialDias = P007022_A1227ContagemResultado_PrazoInicialDias[0];
            n1227ContagemResultado_PrazoInicialDias = P007022_n1227ContagemResultado_PrazoInicialDias[0];
            A472ContagemResultado_DataEntrega = P007022_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = P007022_n472ContagemResultado_DataEntrega[0];
            A512ContagemResultado_ValorPF = P007022_A512ContagemResultado_ValorPF[0];
            n512ContagemResultado_ValorPF = P007022_n512ContagemResultado_ValorPF[0];
            A127Sistema_Codigo = P007022_A127Sistema_Codigo[0];
            A1399Sistema_ImpUserCod = P007022_A1399Sistema_ImpUserCod[0];
            n1399Sistema_ImpUserCod = P007022_n1399Sistema_ImpUserCod[0];
            A137Metodologia_Codigo = P007022_A137Metodologia_Codigo[0];
            n137Metodologia_Codigo = P007022_n137Metodologia_Codigo[0];
            A1073Usuario_CargoCod = P007022_A1073Usuario_CargoCod[0];
            n1073Usuario_CargoCod = P007022_n1073Usuario_CargoCod[0];
            A1075Usuario_CargoUOCod = P007022_A1075Usuario_CargoUOCod[0];
            n1075Usuario_CargoUOCod = P007022_n1075Usuario_CargoUOCod[0];
            A609TpUo_Codigo = P007022_A609TpUo_Codigo[0];
            n609TpUo_Codigo = P007022_n609TpUo_Codigo[0];
            AV154GXLvl474 = 1;
            AV128OSVinculada = A602ContagemResultado_OSVinculada;
            if ( AV40ContagemResultado_StatusCnt == 5 )
            {
               A484ContagemResultado_StatusDmn = AV93StatusFinal;
               n484ContagemResultado_StatusDmn = false;
               if ( StringUtil.StrCmp(AV93StatusFinal, "H") == 0 )
               {
                  A1348ContagemResultado_DataHomologacao = AV126ServerNow;
                  n1348ContagemResultado_DataHomologacao = false;
               }
            }
            else
            {
               AV121PrazoEntrega = AV126ServerNow;
               if ( ! ( AV130PrazoInicio == 3 ) )
               {
                  AV121PrazoEntrega = DateTimeUtil.ResetTime( DateTimeUtil.ResetTime( AV121PrazoEntrega) ) ;
                  AV121PrazoEntrega = DateTimeUtil.TAdd( AV121PrazoEntrega, 3600*(DateTimeUtil.Hour( A912ContagemResultado_HoraEntrega)));
                  AV121PrazoEntrega = DateTimeUtil.TAdd( AV121PrazoEntrega, 60*(DateTimeUtil.Minute( A912ContagemResultado_HoraEntrega)));
               }
               AV122PrazoInicial = (short)(A1227ContagemResultado_PrazoInicialDias+A1237ContagemResultado_PrazoMaisDias);
               /* Execute user subroutine: 'PRAZOCORRECAO' */
               S1615 ();
               if ( returnInSub )
               {
                  pr_default.close(20);
                  returnInSub = true;
                  if (true) return;
               }
               A472ContagemResultado_DataEntrega = DateTimeUtil.ResetTime(AV121PrazoEntrega);
               n472ContagemResultado_DataEntrega = false;
               A912ContagemResultado_HoraEntrega = DateTimeUtil.ResetDate(AV121PrazoEntrega);
               n912ContagemResultado_HoraEntrega = false;
               A484ContagemResultado_StatusDmn = "A";
               n484ContagemResultado_StatusDmn = false;
            }
            A512ContagemResultado_ValorPF = AV106ValorPFFS;
            n512ContagemResultado_ValorPF = false;
            /* Using cursor P007023 */
            pr_default.execute(21, new Object[] {A456ContagemResultado_Codigo});
            while ( (pr_default.getStatus(21) != 101) )
            {
               A517ContagemResultado_Ultima = P007023_A517ContagemResultado_Ultima[0];
               A473ContagemResultado_DataCnt = P007023_A473ContagemResultado_DataCnt[0];
               A458ContagemResultado_PFBFS = P007023_A458ContagemResultado_PFBFS[0];
               n458ContagemResultado_PFBFS = P007023_n458ContagemResultado_PFBFS[0];
               A459ContagemResultado_PFLFS = P007023_A459ContagemResultado_PFLFS[0];
               n459ContagemResultado_PFLFS = P007023_n459ContagemResultado_PFLFS[0];
               A462ContagemResultado_Divergencia = P007023_A462ContagemResultado_Divergencia[0];
               A483ContagemResultado_StatusCnt = P007023_A483ContagemResultado_StatusCnt[0];
               A511ContagemResultado_HoraCnt = P007023_A511ContagemResultado_HoraCnt[0];
               if ( A473ContagemResultado_DataCnt == AV102ContagemResultado_DataCnt )
               {
                  AV55Flag = false;
                  A458ContagemResultado_PFBFS = AV71PFBFS;
                  n458ContagemResultado_PFBFS = false;
                  A459ContagemResultado_PFLFS = AV75PFLFS;
                  n459ContagemResultado_PFLFS = false;
                  A462ContagemResultado_Divergencia = AV39ContagemResultado_Divergencia;
                  A483ContagemResultado_StatusCnt = AV40ContagemResultado_StatusCnt;
               }
               else
               {
                  AV55Flag = true;
                  /* Using cursor P007024 */
                  pr_default.execute(22, new Object[] {A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
                  pr_default.close(22);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
               }
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               /* Using cursor P007025 */
               pr_default.execute(23, new Object[] {n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, A462ContagemResultado_Divergencia, A483ContagemResultado_StatusCnt, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
               pr_default.close(23);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
               if (true) break;
               /* Using cursor P007026 */
               pr_default.execute(24, new Object[] {n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, A462ContagemResultado_Divergencia, A483ContagemResultado_StatusCnt, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
               pr_default.close(24);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
               pr_default.readNext(21);
            }
            pr_default.close(21);
            if ( AV55Flag )
            {
               AV107ContagemResultado_CodigoFS = A456ContagemResultado_Codigo;
               /* Execute user subroutine: 'NEWCONTAGEMDATAALTERADA' */
               S1715 ();
               if ( returnInSub )
               {
                  pr_default.close(20);
                  returnInSub = true;
                  if (true) return;
               }
            }
            /* Using cursor P007027 */
            pr_default.execute(25, new Object[] {AV46Demanda, n609TpUo_Codigo, A609TpUo_Codigo, n137Metodologia_Codigo, A137Metodologia_Codigo, AV43Contratada_Codigo, AV95ContratadaFS_Codigo});
            while ( (pr_default.getStatus(25) != 101) )
            {
               A2183Contagem_TipoUnidade = P007027_A2183Contagem_TipoUnidade[0];
               n2183Contagem_TipoUnidade = P007027_n2183Contagem_TipoUnidade[0];
               A2185Contagem_Metodologia = P007027_A2185Contagem_Metodologia[0];
               n2185Contagem_Metodologia = P007027_n2185Contagem_Metodologia[0];
               A1118Contagem_ContratadaCod = P007027_A1118Contagem_ContratadaCod[0];
               n1118Contagem_ContratadaCod = P007027_n1118Contagem_ContratadaCod[0];
               A945Contagem_Demanda = P007027_A945Contagem_Demanda[0];
               n945Contagem_Demanda = P007027_n945Contagem_Demanda[0];
               A943Contagem_PFB = P007027_A943Contagem_PFB[0];
               n943Contagem_PFB = P007027_n943Contagem_PFB[0];
               A944Contagem_PFL = P007027_A944Contagem_PFL[0];
               n944Contagem_PFL = P007027_n944Contagem_PFL[0];
               A947Contagem_Fator = P007027_A947Contagem_Fator[0];
               n947Contagem_Fator = P007027_n947Contagem_Fator[0];
               A1113Contagem_PFBA = P007027_A1113Contagem_PFBA[0];
               n1113Contagem_PFBA = P007027_n1113Contagem_PFBA[0];
               A1114Contagem_PFLA = P007027_A1114Contagem_PFLA[0];
               n1114Contagem_PFLA = P007027_n1114Contagem_PFLA[0];
               A1117Contagem_Deflator = P007027_A1117Contagem_Deflator[0];
               n1117Contagem_Deflator = P007027_n1117Contagem_Deflator[0];
               A1115Contagem_PFBD = P007027_A1115Contagem_PFBD[0];
               n1115Contagem_PFBD = P007027_n1115Contagem_PFBD[0];
               A1116Contagem_PFLD = P007027_A1116Contagem_PFLD[0];
               n1116Contagem_PFLD = P007027_n1116Contagem_PFLD[0];
               A2200Contagem_DataAtualizacao = P007027_A2200Contagem_DataAtualizacao[0];
               n2200Contagem_DataAtualizacao = P007027_n2200Contagem_DataAtualizacao[0];
               A1119Contagem_Divergencia = P007027_A1119Contagem_Divergencia[0];
               n1119Contagem_Divergencia = P007027_n1119Contagem_Divergencia[0];
               A262Contagem_Status = P007027_A262Contagem_Status[0];
               n262Contagem_Status = P007027_n262Contagem_Status[0];
               A192Contagem_Codigo = P007027_A192Contagem_Codigo[0];
               if ( A1118Contagem_ContratadaCod == AV95ContratadaFS_Codigo )
               {
                  A943Contagem_PFB = AV71PFBFS;
                  n943Contagem_PFB = false;
                  A944Contagem_PFL = AV75PFLFS;
                  n944Contagem_PFL = false;
                  A1113Contagem_PFBA = (decimal)(A943Contagem_PFB*A947Contagem_Fator);
                  n1113Contagem_PFBA = false;
                  A1114Contagem_PFLA = (decimal)(A944Contagem_PFL*A947Contagem_Fator);
                  n1114Contagem_PFLA = false;
                  A1115Contagem_PFBD = (decimal)(A943Contagem_PFB*A1117Contagem_Deflator);
                  n1115Contagem_PFBD = false;
                  A1116Contagem_PFLD = (decimal)(A944Contagem_PFL*A1117Contagem_Deflator);
                  n1116Contagem_PFLD = false;
               }
               A2200Contagem_DataAtualizacao = AV126ServerNow;
               n2200Contagem_DataAtualizacao = false;
               A1119Contagem_Divergencia = AV39ContagemResultado_Divergencia;
               n1119Contagem_Divergencia = false;
               if ( AV40ContagemResultado_StatusCnt == 5 )
               {
                  A262Contagem_Status = "A";
                  n262Contagem_Status = false;
               }
               else
               {
                  A262Contagem_Status = "D";
                  n262Contagem_Status = false;
               }
               /* Using cursor P007028 */
               pr_default.execute(26, new Object[] {n943Contagem_PFB, A943Contagem_PFB, n944Contagem_PFL, A944Contagem_PFL, n1113Contagem_PFBA, A1113Contagem_PFBA, n1114Contagem_PFLA, A1114Contagem_PFLA, n1115Contagem_PFBD, A1115Contagem_PFBD, n1116Contagem_PFLD, A1116Contagem_PFLD, n2200Contagem_DataAtualizacao, A2200Contagem_DataAtualizacao, n1119Contagem_Divergencia, A1119Contagem_Divergencia, n262Contagem_Status, A262Contagem_Status, A192Contagem_Codigo});
               pr_default.close(26);
               dsDefault.SmartCacheProvider.SetUpdated("Contagem") ;
               pr_default.readNext(25);
            }
            pr_default.close(25);
            /* Using cursor P007029 */
            pr_default.execute(27, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n1348ContagemResultado_DataHomologacao, A1348ContagemResultado_DataHomologacao, n912ContagemResultado_HoraEntrega, A912ContagemResultado_HoraEntrega, n472ContagemResultado_DataEntrega, A472ContagemResultado_DataEntrega, n512ContagemResultado_ValorPF, A512ContagemResultado_ValorPF, A456ContagemResultado_Codigo});
            pr_default.close(27);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            pr_default.readNext(20);
         }
         pr_default.close(20);
         if ( AV154GXLvl474 == 0 )
         {
            /* Using cursor P007030 */
            pr_default.execute(28, new Object[] {AV35ContagemResultado_Codigo});
            while ( (pr_default.getStatus(28) != 101) )
            {
               A1553ContagemResultado_CntSrvCod = P007030_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P007030_n1553ContagemResultado_CntSrvCod[0];
               A1452ContagemResultado_SS = P007030_A1452ContagemResultado_SS[0];
               n1452ContagemResultado_SS = P007030_n1452ContagemResultado_SS[0];
               A1348ContagemResultado_DataHomologacao = P007030_A1348ContagemResultado_DataHomologacao[0];
               n1348ContagemResultado_DataHomologacao = P007030_n1348ContagemResultado_DataHomologacao[0];
               A1046ContagemResultado_Agrupador = P007030_A1046ContagemResultado_Agrupador[0];
               n1046ContagemResultado_Agrupador = P007030_n1046ContagemResultado_Agrupador[0];
               A912ContagemResultado_HoraEntrega = P007030_A912ContagemResultado_HoraEntrega[0];
               n912ContagemResultado_HoraEntrega = P007030_n912ContagemResultado_HoraEntrega[0];
               A890ContagemResultado_Responsavel = P007030_A890ContagemResultado_Responsavel[0];
               n890ContagemResultado_Responsavel = P007030_n890ContagemResultado_Responsavel[0];
               A602ContagemResultado_OSVinculada = P007030_A602ContagemResultado_OSVinculada[0];
               n602ContagemResultado_OSVinculada = P007030_n602ContagemResultado_OSVinculada[0];
               A512ContagemResultado_ValorPF = P007030_A512ContagemResultado_ValorPF[0];
               n512ContagemResultado_ValorPF = P007030_n512ContagemResultado_ValorPF[0];
               A490ContagemResultado_ContratadaCod = P007030_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = P007030_n490ContagemResultado_ContratadaCod[0];
               A484ContagemResultado_StatusDmn = P007030_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = P007030_n484ContagemResultado_StatusDmn[0];
               A457ContagemResultado_Demanda = P007030_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = P007030_n457ContagemResultado_Demanda[0];
               A472ContagemResultado_DataEntrega = P007030_A472ContagemResultado_DataEntrega[0];
               n472ContagemResultado_DataEntrega = P007030_n472ContagemResultado_DataEntrega[0];
               A2133ContagemResultado_QuantidadeSolicitada = P007030_A2133ContagemResultado_QuantidadeSolicitada[0];
               n2133ContagemResultado_QuantidadeSolicitada = P007030_n2133ContagemResultado_QuantidadeSolicitada[0];
               A2017ContagemResultado_DataEntregaReal = P007030_A2017ContagemResultado_DataEntregaReal[0];
               n2017ContagemResultado_DataEntregaReal = P007030_n2017ContagemResultado_DataEntregaReal[0];
               A1903ContagemResultado_DataPrvPgm = P007030_A1903ContagemResultado_DataPrvPgm[0];
               n1903ContagemResultado_DataPrvPgm = P007030_n1903ContagemResultado_DataPrvPgm[0];
               A1855ContagemResultado_PFCnc = P007030_A1855ContagemResultado_PFCnc[0];
               n1855ContagemResultado_PFCnc = P007030_n1855ContagemResultado_PFCnc[0];
               A1854ContagemResultado_VlrCnc = P007030_A1854ContagemResultado_VlrCnc[0];
               n1854ContagemResultado_VlrCnc = P007030_n1854ContagemResultado_VlrCnc[0];
               A1791ContagemResultado_SemCusto = P007030_A1791ContagemResultado_SemCusto[0];
               n1791ContagemResultado_SemCusto = P007030_n1791ContagemResultado_SemCusto[0];
               A1790ContagemResultado_DataInicio = P007030_A1790ContagemResultado_DataInicio[0];
               n1790ContagemResultado_DataInicio = P007030_n1790ContagemResultado_DataInicio[0];
               A1762ContagemResultado_Entrega = P007030_A1762ContagemResultado_Entrega[0];
               n1762ContagemResultado_Entrega = P007030_n1762ContagemResultado_Entrega[0];
               A1714ContagemResultado_Combinada = P007030_A1714ContagemResultado_Combinada[0];
               n1714ContagemResultado_Combinada = P007030_n1714ContagemResultado_Combinada[0];
               A1636ContagemResultado_ServicoSS = P007030_A1636ContagemResultado_ServicoSS[0];
               n1636ContagemResultado_ServicoSS = P007030_n1636ContagemResultado_ServicoSS[0];
               A1587ContagemResultado_PrioridadePrevista = P007030_A1587ContagemResultado_PrioridadePrevista[0];
               n1587ContagemResultado_PrioridadePrevista = P007030_n1587ContagemResultado_PrioridadePrevista[0];
               A1586ContagemResultado_Restricoes = P007030_A1586ContagemResultado_Restricoes[0];
               n1586ContagemResultado_Restricoes = P007030_n1586ContagemResultado_Restricoes[0];
               A1585ContagemResultado_Referencia = P007030_A1585ContagemResultado_Referencia[0];
               n1585ContagemResultado_Referencia = P007030_n1585ContagemResultado_Referencia[0];
               A1584ContagemResultado_UOOwner = P007030_A1584ContagemResultado_UOOwner[0];
               n1584ContagemResultado_UOOwner = P007030_n1584ContagemResultado_UOOwner[0];
               A1583ContagemResultado_TipoRegistro = P007030_A1583ContagemResultado_TipoRegistro[0];
               A1559ContagemResultado_VlrAceite = P007030_A1559ContagemResultado_VlrAceite[0];
               n1559ContagemResultado_VlrAceite = P007030_n1559ContagemResultado_VlrAceite[0];
               A1544ContagemResultado_ProjetoCod = P007030_A1544ContagemResultado_ProjetoCod[0];
               n1544ContagemResultado_ProjetoCod = P007030_n1544ContagemResultado_ProjetoCod[0];
               A1521ContagemResultado_FimAnl = P007030_A1521ContagemResultado_FimAnl[0];
               n1521ContagemResultado_FimAnl = P007030_n1521ContagemResultado_FimAnl[0];
               A1520ContagemResultado_InicioAnl = P007030_A1520ContagemResultado_InicioAnl[0];
               n1520ContagemResultado_InicioAnl = P007030_n1520ContagemResultado_InicioAnl[0];
               A1519ContagemResultado_TmpEstAnl = P007030_A1519ContagemResultado_TmpEstAnl[0];
               n1519ContagemResultado_TmpEstAnl = P007030_n1519ContagemResultado_TmpEstAnl[0];
               A1515ContagemResultado_Evento = P007030_A1515ContagemResultado_Evento[0];
               n1515ContagemResultado_Evento = P007030_n1515ContagemResultado_Evento[0];
               A1512ContagemResultado_FimCrr = P007030_A1512ContagemResultado_FimCrr[0];
               n1512ContagemResultado_FimCrr = P007030_n1512ContagemResultado_FimCrr[0];
               A1511ContagemResultado_InicioCrr = P007030_A1511ContagemResultado_InicioCrr[0];
               n1511ContagemResultado_InicioCrr = P007030_n1511ContagemResultado_InicioCrr[0];
               A1510ContagemResultado_FimExc = P007030_A1510ContagemResultado_FimExc[0];
               n1510ContagemResultado_FimExc = P007030_n1510ContagemResultado_FimExc[0];
               A1509ContagemResultado_InicioExc = P007030_A1509ContagemResultado_InicioExc[0];
               n1509ContagemResultado_InicioExc = P007030_n1509ContagemResultado_InicioExc[0];
               A1506ContagemResultado_TmpEstCrr = P007030_A1506ContagemResultado_TmpEstCrr[0];
               n1506ContagemResultado_TmpEstCrr = P007030_n1506ContagemResultado_TmpEstCrr[0];
               A1505ContagemResultado_TmpEstExc = P007030_A1505ContagemResultado_TmpEstExc[0];
               n1505ContagemResultado_TmpEstExc = P007030_n1505ContagemResultado_TmpEstExc[0];
               A1457ContagemResultado_TemDpnHmlg = P007030_A1457ContagemResultado_TemDpnHmlg[0];
               n1457ContagemResultado_TemDpnHmlg = P007030_n1457ContagemResultado_TemDpnHmlg[0];
               A1445ContagemResultado_CntSrvPrrCst = P007030_A1445ContagemResultado_CntSrvPrrCst[0];
               n1445ContagemResultado_CntSrvPrrCst = P007030_n1445ContagemResultado_CntSrvPrrCst[0];
               A1444ContagemResultado_CntSrvPrrPrz = P007030_A1444ContagemResultado_CntSrvPrrPrz[0];
               n1444ContagemResultado_CntSrvPrrPrz = P007030_n1444ContagemResultado_CntSrvPrrPrz[0];
               A1443ContagemResultado_CntSrvPrrCod = P007030_A1443ContagemResultado_CntSrvPrrCod[0];
               n1443ContagemResultado_CntSrvPrrCod = P007030_n1443ContagemResultado_CntSrvPrrCod[0];
               A1392ContagemResultado_RdmnUpdated = P007030_A1392ContagemResultado_RdmnUpdated[0];
               n1392ContagemResultado_RdmnUpdated = P007030_n1392ContagemResultado_RdmnUpdated[0];
               A1390ContagemResultado_RdmnProjectId = P007030_A1390ContagemResultado_RdmnProjectId[0];
               n1390ContagemResultado_RdmnProjectId = P007030_n1390ContagemResultado_RdmnProjectId[0];
               A1389ContagemResultado_RdmnIssueId = P007030_A1389ContagemResultado_RdmnIssueId[0];
               n1389ContagemResultado_RdmnIssueId = P007030_n1389ContagemResultado_RdmnIssueId[0];
               A1351ContagemResultado_DataPrevista = P007030_A1351ContagemResultado_DataPrevista[0];
               n1351ContagemResultado_DataPrevista = P007030_n1351ContagemResultado_DataPrevista[0];
               A1350ContagemResultado_DataCadastro = P007030_A1350ContagemResultado_DataCadastro[0];
               n1350ContagemResultado_DataCadastro = P007030_n1350ContagemResultado_DataCadastro[0];
               A1349ContagemResultado_DataExecucao = P007030_A1349ContagemResultado_DataExecucao[0];
               n1349ContagemResultado_DataExecucao = P007030_n1349ContagemResultado_DataExecucao[0];
               A1237ContagemResultado_PrazoMaisDias = P007030_A1237ContagemResultado_PrazoMaisDias[0];
               n1237ContagemResultado_PrazoMaisDias = P007030_n1237ContagemResultado_PrazoMaisDias[0];
               A1227ContagemResultado_PrazoInicialDias = P007030_A1227ContagemResultado_PrazoInicialDias[0];
               n1227ContagemResultado_PrazoInicialDias = P007030_n1227ContagemResultado_PrazoInicialDias[0];
               A1180ContagemResultado_Custo = P007030_A1180ContagemResultado_Custo[0];
               n1180ContagemResultado_Custo = P007030_n1180ContagemResultado_Custo[0];
               A1179ContagemResultado_PLFinal = P007030_A1179ContagemResultado_PLFinal[0];
               n1179ContagemResultado_PLFinal = P007030_n1179ContagemResultado_PLFinal[0];
               A1178ContagemResultado_PBFinal = P007030_A1178ContagemResultado_PBFinal[0];
               n1178ContagemResultado_PBFinal = P007030_n1178ContagemResultado_PBFinal[0];
               A1173ContagemResultado_OSManual = P007030_A1173ContagemResultado_OSManual[0];
               n1173ContagemResultado_OSManual = P007030_n1173ContagemResultado_OSManual[0];
               A1052ContagemResultado_GlsUser = P007030_A1052ContagemResultado_GlsUser[0];
               n1052ContagemResultado_GlsUser = P007030_n1052ContagemResultado_GlsUser[0];
               A1051ContagemResultado_GlsValor = P007030_A1051ContagemResultado_GlsValor[0];
               n1051ContagemResultado_GlsValor = P007030_n1051ContagemResultado_GlsValor[0];
               A1050ContagemResultado_GlsDescricao = P007030_A1050ContagemResultado_GlsDescricao[0];
               n1050ContagemResultado_GlsDescricao = P007030_n1050ContagemResultado_GlsDescricao[0];
               A1049ContagemResultado_GlsData = P007030_A1049ContagemResultado_GlsData[0];
               n1049ContagemResultado_GlsData = P007030_n1049ContagemResultado_GlsData[0];
               A1044ContagemResultado_FncUsrCod = P007030_A1044ContagemResultado_FncUsrCod[0];
               n1044ContagemResultado_FncUsrCod = P007030_n1044ContagemResultado_FncUsrCod[0];
               A1043ContagemResultado_LiqLogCod = P007030_A1043ContagemResultado_LiqLogCod[0];
               n1043ContagemResultado_LiqLogCod = P007030_n1043ContagemResultado_LiqLogCod[0];
               A805ContagemResultado_ContratadaOrigemCod = P007030_A805ContagemResultado_ContratadaOrigemCod[0];
               n805ContagemResultado_ContratadaOrigemCod = P007030_n805ContagemResultado_ContratadaOrigemCod[0];
               A799ContagemResultado_PFLFSImp = P007030_A799ContagemResultado_PFLFSImp[0];
               n799ContagemResultado_PFLFSImp = P007030_n799ContagemResultado_PFLFSImp[0];
               A798ContagemResultado_PFBFSImp = P007030_A798ContagemResultado_PFBFSImp[0];
               n798ContagemResultado_PFBFSImp = P007030_n798ContagemResultado_PFBFSImp[0];
               A598ContagemResultado_Baseline = P007030_A598ContagemResultado_Baseline[0];
               n598ContagemResultado_Baseline = P007030_n598ContagemResultado_Baseline[0];
               A597ContagemResultado_LoteAceiteCod = P007030_A597ContagemResultado_LoteAceiteCod[0];
               n597ContagemResultado_LoteAceiteCod = P007030_n597ContagemResultado_LoteAceiteCod[0];
               A592ContagemResultado_Evidencia = P007030_A592ContagemResultado_Evidencia[0];
               n592ContagemResultado_Evidencia = P007030_n592ContagemResultado_Evidencia[0];
               A508ContagemResultado_Owner = P007030_A508ContagemResultado_Owner[0];
               A514ContagemResultado_Observacao = P007030_A514ContagemResultado_Observacao[0];
               n514ContagemResultado_Observacao = P007030_n514ContagemResultado_Observacao[0];
               A146Modulo_Codigo = P007030_A146Modulo_Codigo[0];
               n146Modulo_Codigo = P007030_n146Modulo_Codigo[0];
               A489ContagemResultado_SistemaCod = P007030_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = P007030_n489ContagemResultado_SistemaCod[0];
               A494ContagemResultado_Descricao = P007030_A494ContagemResultado_Descricao[0];
               n494ContagemResultado_Descricao = P007030_n494ContagemResultado_Descricao[0];
               A493ContagemResultado_DemandaFM = P007030_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = P007030_n493ContagemResultado_DemandaFM[0];
               A485ContagemResultado_EhValidacao = P007030_A485ContagemResultado_EhValidacao[0];
               n485ContagemResultado_EhValidacao = P007030_n485ContagemResultado_EhValidacao[0];
               A468ContagemResultado_NaoCnfDmnCod = P007030_A468ContagemResultado_NaoCnfDmnCod[0];
               n468ContagemResultado_NaoCnfDmnCod = P007030_n468ContagemResultado_NaoCnfDmnCod[0];
               A465ContagemResultado_Link = P007030_A465ContagemResultado_Link[0];
               n465ContagemResultado_Link = P007030_n465ContagemResultado_Link[0];
               A454ContagemResultado_ContadorFSCod = P007030_A454ContagemResultado_ContadorFSCod[0];
               n454ContagemResultado_ContadorFSCod = P007030_n454ContagemResultado_ContadorFSCod[0];
               A471ContagemResultado_DataDmn = P007030_A471ContagemResultado_DataDmn[0];
               A456ContagemResultado_Codigo = P007030_A456ContagemResultado_Codigo[0];
               W456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
               AV128OSVinculada = AV35ContagemResultado_Codigo;
               /*
                  INSERT RECORD ON TABLE ContagemResultado

               */
               W490ContagemResultado_ContratadaCod = A490ContagemResultado_ContratadaCod;
               n490ContagemResultado_ContratadaCod = false;
               W457ContagemResultado_Demanda = A457ContagemResultado_Demanda;
               n457ContagemResultado_Demanda = false;
               W1553ContagemResultado_CntSrvCod = A1553ContagemResultado_CntSrvCod;
               n1553ContagemResultado_CntSrvCod = false;
               W484ContagemResultado_StatusDmn = A484ContagemResultado_StatusDmn;
               n484ContagemResultado_StatusDmn = false;
               W1348ContagemResultado_DataHomologacao = A1348ContagemResultado_DataHomologacao;
               n1348ContagemResultado_DataHomologacao = false;
               W484ContagemResultado_StatusDmn = A484ContagemResultado_StatusDmn;
               n484ContagemResultado_StatusDmn = false;
               W472ContagemResultado_DataEntrega = A472ContagemResultado_DataEntrega;
               n472ContagemResultado_DataEntrega = false;
               W912ContagemResultado_HoraEntrega = A912ContagemResultado_HoraEntrega;
               n912ContagemResultado_HoraEntrega = false;
               W912ContagemResultado_HoraEntrega = A912ContagemResultado_HoraEntrega;
               n912ContagemResultado_HoraEntrega = false;
               W512ContagemResultado_ValorPF = A512ContagemResultado_ValorPF;
               n512ContagemResultado_ValorPF = false;
               W602ContagemResultado_OSVinculada = A602ContagemResultado_OSVinculada;
               n602ContagemResultado_OSVinculada = false;
               W890ContagemResultado_Responsavel = A890ContagemResultado_Responsavel;
               n890ContagemResultado_Responsavel = false;
               W1452ContagemResultado_SS = A1452ContagemResultado_SS;
               n1452ContagemResultado_SS = false;
               W1046ContagemResultado_Agrupador = A1046ContagemResultado_Agrupador;
               n1046ContagemResultado_Agrupador = false;
               A490ContagemResultado_ContratadaCod = AV95ContratadaFS_Codigo;
               n490ContagemResultado_ContratadaCod = false;
               A457ContagemResultado_Demanda = AV46Demanda;
               n457ContagemResultado_Demanda = false;
               A1553ContagemResultado_CntSrvCod = AV134ContratoservicosFS_Codigo;
               n1553ContagemResultado_CntSrvCod = false;
               if ( AV40ContagemResultado_StatusCnt == 5 )
               {
                  A484ContagemResultado_StatusDmn = AV93StatusFinal;
                  n484ContagemResultado_StatusDmn = false;
                  if ( StringUtil.StrCmp(AV93StatusFinal, "H") == 0 )
                  {
                     A1348ContagemResultado_DataHomologacao = AV126ServerNow;
                     n1348ContagemResultado_DataHomologacao = false;
                  }
               }
               else
               {
                  AV121PrazoEntrega = AV126ServerNow;
                  if ( ! ( AV130PrazoInicio == 3 ) )
                  {
                     AV121PrazoEntrega = DateTimeUtil.ResetTime( DateTimeUtil.ResetTime( AV121PrazoEntrega) ) ;
                     AV121PrazoEntrega = DateTimeUtil.TAdd( AV121PrazoEntrega, 3600*(DateTimeUtil.Hour( A912ContagemResultado_HoraEntrega)));
                     AV121PrazoEntrega = DateTimeUtil.TAdd( AV121PrazoEntrega, 60*(DateTimeUtil.Minute( A912ContagemResultado_HoraEntrega)));
                  }
                  AV122PrazoInicial = (short)(A1227ContagemResultado_PrazoInicialDias+A1237ContagemResultado_PrazoMaisDias);
                  /* Execute user subroutine: 'PRAZOCORRECAO' */
                  S1615 ();
                  if ( returnInSub )
                  {
                     pr_default.close(28);
                     returnInSub = true;
                     if (true) return;
                  }
                  A484ContagemResultado_StatusDmn = "A";
                  n484ContagemResultado_StatusDmn = false;
                  A472ContagemResultado_DataEntrega = DateTimeUtil.ResetTime(AV121PrazoEntrega);
                  n472ContagemResultado_DataEntrega = false;
                  A912ContagemResultado_HoraEntrega = DateTimeUtil.ResetDate(AV121PrazoEntrega);
                  n912ContagemResultado_HoraEntrega = false;
                  A912ContagemResultado_HoraEntrega = DateTimeUtil.ResetDate(AV121PrazoEntrega);
                  n912ContagemResultado_HoraEntrega = false;
               }
               AV133Status = A484ContagemResultado_StatusDmn;
               A512ContagemResultado_ValorPF = AV106ValorPFFS;
               n512ContagemResultado_ValorPF = false;
               A602ContagemResultado_OSVinculada = AV35ContagemResultado_Codigo;
               n602ContagemResultado_OSVinculada = false;
               A890ContagemResultado_Responsavel = AV97ContadorFS_Codigo;
               n890ContagemResultado_Responsavel = false;
               A1452ContagemResultado_SS = 0;
               n1452ContagemResultado_SS = false;
               A1046ContagemResultado_Agrupador = AV90Agrupador;
               n1046ContagemResultado_Agrupador = false;
               /* Using cursor P007031 */
               pr_default.execute(29, new Object[] {A471ContagemResultado_DataDmn, n472ContagemResultado_DataEntrega, A472ContagemResultado_DataEntrega, n454ContagemResultado_ContadorFSCod, A454ContagemResultado_ContadorFSCod, n457ContagemResultado_Demanda, A457ContagemResultado_Demanda, n465ContagemResultado_Link, A465ContagemResultado_Link, n468ContagemResultado_NaoCnfDmnCod, A468ContagemResultado_NaoCnfDmnCod, n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n485ContagemResultado_EhValidacao, A485ContagemResultado_EhValidacao, n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod, n493ContagemResultado_DemandaFM, A493ContagemResultado_DemandaFM, n494ContagemResultado_Descricao, A494ContagemResultado_Descricao, n489ContagemResultado_SistemaCod, A489ContagemResultado_SistemaCod, n146Modulo_Codigo, A146Modulo_Codigo, n514ContagemResultado_Observacao, A514ContagemResultado_Observacao, A508ContagemResultado_Owner, n512ContagemResultado_ValorPF, A512ContagemResultado_ValorPF, n592ContagemResultado_Evidencia, A592ContagemResultado_Evidencia, n597ContagemResultado_LoteAceiteCod, A597ContagemResultado_LoteAceiteCod, n598ContagemResultado_Baseline, A598ContagemResultado_Baseline, n602ContagemResultado_OSVinculada, A602ContagemResultado_OSVinculada, n798ContagemResultado_PFBFSImp, A798ContagemResultado_PFBFSImp, n799ContagemResultado_PFLFSImp, A799ContagemResultado_PFLFSImp, n805ContagemResultado_ContratadaOrigemCod, A805ContagemResultado_ContratadaOrigemCod, n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, n912ContagemResultado_HoraEntrega, A912ContagemResultado_HoraEntrega, n1043ContagemResultado_LiqLogCod, A1043ContagemResultado_LiqLogCod, n1044ContagemResultado_FncUsrCod, A1044ContagemResultado_FncUsrCod, n1046ContagemResultado_Agrupador, A1046ContagemResultado_Agrupador, n1049ContagemResultado_GlsData, A1049ContagemResultado_GlsData, n1050ContagemResultado_GlsDescricao, A1050ContagemResultado_GlsDescricao, n1051ContagemResultado_GlsValor, A1051ContagemResultado_GlsValor, n1052ContagemResultado_GlsUser, A1052ContagemResultado_GlsUser, n1173ContagemResultado_OSManual, A1173ContagemResultado_OSManual, n1178ContagemResultado_PBFinal, A1178ContagemResultado_PBFinal, n1179ContagemResultado_PLFinal, A1179ContagemResultado_PLFinal, n1180ContagemResultado_Custo, A1180ContagemResultado_Custo, n1227ContagemResultado_PrazoInicialDias, A1227ContagemResultado_PrazoInicialDias, n1237ContagemResultado_PrazoMaisDias, A1237ContagemResultado_PrazoMaisDias, n1348ContagemResultado_DataHomologacao, A1348ContagemResultado_DataHomologacao, n1349ContagemResultado_DataExecucao, A1349ContagemResultado_DataExecucao, n1350ContagemResultado_DataCadastro, A1350ContagemResultado_DataCadastro, n1351ContagemResultado_DataPrevista, A1351ContagemResultado_DataPrevista, n1389ContagemResultado_RdmnIssueId, A1389ContagemResultado_RdmnIssueId, n1390ContagemResultado_RdmnProjectId, A1390ContagemResultado_RdmnProjectId, n1392ContagemResultado_RdmnUpdated, A1392ContagemResultado_RdmnUpdated, n1443ContagemResultado_CntSrvPrrCod, A1443ContagemResultado_CntSrvPrrCod, n1444ContagemResultado_CntSrvPrrPrz, A1444ContagemResultado_CntSrvPrrPrz, n1445ContagemResultado_CntSrvPrrCst, A1445ContagemResultado_CntSrvPrrCst, n1452ContagemResultado_SS, A1452ContagemResultado_SS, n1457ContagemResultado_TemDpnHmlg, A1457ContagemResultado_TemDpnHmlg, n1505ContagemResultado_TmpEstExc, A1505ContagemResultado_TmpEstExc, n1506ContagemResultado_TmpEstCrr, A1506ContagemResultado_TmpEstCrr, n1509ContagemResultado_InicioExc, A1509ContagemResultado_InicioExc, n1510ContagemResultado_FimExc, A1510ContagemResultado_FimExc, n1511ContagemResultado_InicioCrr, A1511ContagemResultado_InicioCrr, n1512ContagemResultado_FimCrr, A1512ContagemResultado_FimCrr, n1515ContagemResultado_Evento, A1515ContagemResultado_Evento, n1519ContagemResultado_TmpEstAnl, A1519ContagemResultado_TmpEstAnl, n1520ContagemResultado_InicioAnl, A1520ContagemResultado_InicioAnl, n1521ContagemResultado_FimAnl, A1521ContagemResultado_FimAnl, n1544ContagemResultado_ProjetoCod, A1544ContagemResultado_ProjetoCod, n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod,
               n1559ContagemResultado_VlrAceite, A1559ContagemResultado_VlrAceite, A1583ContagemResultado_TipoRegistro, n1584ContagemResultado_UOOwner, A1584ContagemResultado_UOOwner, n1585ContagemResultado_Referencia, A1585ContagemResultado_Referencia, n1586ContagemResultado_Restricoes, A1586ContagemResultado_Restricoes, n1587ContagemResultado_PrioridadePrevista, A1587ContagemResultado_PrioridadePrevista, n1636ContagemResultado_ServicoSS, A1636ContagemResultado_ServicoSS, n1714ContagemResultado_Combinada, A1714ContagemResultado_Combinada, n1762ContagemResultado_Entrega, A1762ContagemResultado_Entrega, n1790ContagemResultado_DataInicio, A1790ContagemResultado_DataInicio, n1791ContagemResultado_SemCusto, A1791ContagemResultado_SemCusto, n1854ContagemResultado_VlrCnc, A1854ContagemResultado_VlrCnc, n1855ContagemResultado_PFCnc, A1855ContagemResultado_PFCnc, n1903ContagemResultado_DataPrvPgm, A1903ContagemResultado_DataPrvPgm, n2017ContagemResultado_DataEntregaReal, A2017ContagemResultado_DataEntregaReal, n2133ContagemResultado_QuantidadeSolicitada, A2133ContagemResultado_QuantidadeSolicitada});
               A456ContagemResultado_Codigo = P007031_A456ContagemResultado_Codigo[0];
               pr_default.close(29);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               if ( (pr_default.getStatus(29) == 1) )
               {
                  context.Gx_err = 1;
                  Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
               }
               else
               {
                  context.Gx_err = 0;
                  Gx_emsg = "";
               }
               A490ContagemResultado_ContratadaCod = W490ContagemResultado_ContratadaCod;
               n490ContagemResultado_ContratadaCod = false;
               A457ContagemResultado_Demanda = W457ContagemResultado_Demanda;
               n457ContagemResultado_Demanda = false;
               A1553ContagemResultado_CntSrvCod = W1553ContagemResultado_CntSrvCod;
               n1553ContagemResultado_CntSrvCod = false;
               A484ContagemResultado_StatusDmn = W484ContagemResultado_StatusDmn;
               n484ContagemResultado_StatusDmn = false;
               A1348ContagemResultado_DataHomologacao = W1348ContagemResultado_DataHomologacao;
               n1348ContagemResultado_DataHomologacao = false;
               A484ContagemResultado_StatusDmn = W484ContagemResultado_StatusDmn;
               n484ContagemResultado_StatusDmn = false;
               A472ContagemResultado_DataEntrega = W472ContagemResultado_DataEntrega;
               n472ContagemResultado_DataEntrega = false;
               A912ContagemResultado_HoraEntrega = W912ContagemResultado_HoraEntrega;
               n912ContagemResultado_HoraEntrega = false;
               A912ContagemResultado_HoraEntrega = W912ContagemResultado_HoraEntrega;
               n912ContagemResultado_HoraEntrega = false;
               A512ContagemResultado_ValorPF = W512ContagemResultado_ValorPF;
               n512ContagemResultado_ValorPF = false;
               A602ContagemResultado_OSVinculada = W602ContagemResultado_OSVinculada;
               n602ContagemResultado_OSVinculada = false;
               A890ContagemResultado_Responsavel = W890ContagemResultado_Responsavel;
               n890ContagemResultado_Responsavel = false;
               A1452ContagemResultado_SS = W1452ContagemResultado_SS;
               n1452ContagemResultado_SS = false;
               A1046ContagemResultado_Agrupador = W1046ContagemResultado_Agrupador;
               n1046ContagemResultado_Agrupador = false;
               /* End Insert */
               AV96ContagemFS_Codigo = A456ContagemResultado_Codigo;
               /*
                  INSERT RECORD ON TABLE ContagemResultadoContagens

               */
               W456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
               A456ContagemResultado_Codigo = AV96ContagemFS_Codigo;
               A473ContagemResultado_DataCnt = AV102ContagemResultado_DataCnt;
               A511ContagemResultado_HoraCnt = "--:--";
               A460ContagemResultado_PFBFM = AV72PFFM;
               n460ContagemResultado_PFBFM = false;
               A461ContagemResultado_PFLFM = AV8ContagemResultado_PFLFM;
               n461ContagemResultado_PFLFM = false;
               A458ContagemResultado_PFBFS = AV71PFBFS;
               n458ContagemResultado_PFBFS = false;
               A459ContagemResultado_PFLFS = AV75PFLFS;
               n459ContagemResultado_PFLFS = false;
               A800ContagemResultado_Deflator = AV101ContagemResltado_Deflator;
               n800ContagemResultado_Deflator = false;
               A462ContagemResultado_Divergencia = AV39ContagemResultado_Divergencia;
               A482ContagemResultadoContagens_Esforco = 0;
               A470ContagemResultado_ContadorFMCod = AV97ContadorFS_Codigo;
               A483ContagemResultado_StatusCnt = AV40ContagemResultado_StatusCnt;
               A517ContagemResultado_Ultima = true;
               A469ContagemResultado_NaoCnfCntCod = 0;
               n469ContagemResultado_NaoCnfCntCod = false;
               n469ContagemResultado_NaoCnfCntCod = true;
               /* Using cursor P007032 */
               pr_default.execute(30, new Object[] {A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt, n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, n460ContagemResultado_PFBFM, A460ContagemResultado_PFBFM, n461ContagemResultado_PFLFM, A461ContagemResultado_PFLFM, A462ContagemResultado_Divergencia, A470ContagemResultado_ContadorFMCod, n469ContagemResultado_NaoCnfCntCod, A469ContagemResultado_NaoCnfCntCod, A483ContagemResultado_StatusCnt, A482ContagemResultadoContagens_Esforco, A517ContagemResultado_Ultima, n800ContagemResultado_Deflator, A800ContagemResultado_Deflator});
               pr_default.close(30);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
               if ( (pr_default.getStatus(30) == 1) )
               {
                  context.Gx_err = 1;
                  Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
               }
               else
               {
                  context.Gx_err = 0;
                  Gx_emsg = "";
               }
               A456ContagemResultado_Codigo = W456ContagemResultado_Codigo;
               /* End Insert */
               /*
                  INSERT RECORD ON TABLE Contagem

               */
               A193Contagem_AreaTrabalhoCod = AV83WWPContext.gxTpr_Areatrabalho_codigo;
               A197Contagem_DataCriacao = DateTimeUtil.ResetTime(AV126ServerNow);
               A195Contagem_Tecnica = "3";
               n195Contagem_Tecnica = false;
               A196Contagem_Tipo = "M";
               n196Contagem_Tipo = false;
               A2182Contagem_Pendencia = "0";
               n2182Contagem_Pendencia = false;
               A943Contagem_PFB = AV71PFBFS;
               n943Contagem_PFB = false;
               A944Contagem_PFL = AV75PFLFS;
               n944Contagem_PFL = false;
               A1114Contagem_PFLA = AV75PFLFS;
               n1114Contagem_PFLA = false;
               A945Contagem_Demanda = AV46Demanda;
               n945Contagem_Demanda = false;
               A940Contagem_SistemaCod = AV136Sistema_Codigo;
               n940Contagem_SistemaCod = false;
               A1118Contagem_ContratadaCod = AV95ContratadaFS_Codigo;
               n1118Contagem_ContratadaCod = false;
               A213Contagem_UsuarioContadorCod = AV97ContadorFS_Codigo;
               n213Contagem_UsuarioContadorCod = false;
               A2181Contagem_CntSrvCod = AV134ContratoservicosFS_Codigo;
               n2181Contagem_CntSrvCod = false;
               A2199Contagem_DemandaCod = AV96ContagemFS_Codigo;
               n2199Contagem_DemandaCod = false;
               A2200Contagem_DataAtualizacao = AV126ServerNow;
               n2200Contagem_DataAtualizacao = false;
               A1119Contagem_Divergencia = AV39ContagemResultado_Divergencia;
               n1119Contagem_Divergencia = false;
               A2187Contagem_Agrupador = AV90Agrupador;
               n2187Contagem_Agrupador = false;
               A2183Contagem_TipoUnidade = 0;
               n2183Contagem_TipoUnidade = false;
               n2183Contagem_TipoUnidade = true;
               A2184Contagem_UnidadeOrganizacional = 0;
               n2184Contagem_UnidadeOrganizacional = false;
               n2184Contagem_UnidadeOrganizacional = true;
               A2185Contagem_Metodologia = 0;
               n2185Contagem_Metodologia = false;
               n2185Contagem_Metodologia = true;
               A2186Contagem_Fases = 0;
               n2186Contagem_Fases = false;
               n2186Contagem_Fases = true;
               A939Contagem_ProjetoCod = 0;
               n939Contagem_ProjetoCod = false;
               n939Contagem_ProjetoCod = true;
               if ( AV40ContagemResultado_StatusCnt == 5 )
               {
                  A262Contagem_Status = "A";
                  n262Contagem_Status = false;
               }
               else
               {
                  A262Contagem_Status = "D";
                  n262Contagem_Status = false;
               }
               /* Using cursor P007033 */
               pr_default.execute(31, new Object[] {A193Contagem_AreaTrabalhoCod, n195Contagem_Tecnica, A195Contagem_Tecnica, n196Contagem_Tipo, A196Contagem_Tipo, A197Contagem_DataCriacao, n213Contagem_UsuarioContadorCod, A213Contagem_UsuarioContadorCod, n262Contagem_Status, A262Contagem_Status, n943Contagem_PFB, A943Contagem_PFB, n944Contagem_PFL, A944Contagem_PFL, n945Contagem_Demanda, A945Contagem_Demanda, n940Contagem_SistemaCod, A940Contagem_SistemaCod, n939Contagem_ProjetoCod, A939Contagem_ProjetoCod, n1114Contagem_PFLA, A1114Contagem_PFLA, n1118Contagem_ContratadaCod, A1118Contagem_ContratadaCod, n1119Contagem_Divergencia, A1119Contagem_Divergencia, n2181Contagem_CntSrvCod, A2181Contagem_CntSrvCod, n2182Contagem_Pendencia, A2182Contagem_Pendencia, n2183Contagem_TipoUnidade, A2183Contagem_TipoUnidade, n2184Contagem_UnidadeOrganizacional, A2184Contagem_UnidadeOrganizacional, n2185Contagem_Metodologia, A2185Contagem_Metodologia, n2186Contagem_Fases, A2186Contagem_Fases, n2187Contagem_Agrupador, A2187Contagem_Agrupador, n2199Contagem_DemandaCod, A2199Contagem_DemandaCod, n2200Contagem_DataAtualizacao, A2200Contagem_DataAtualizacao});
               A192Contagem_Codigo = P007033_A192Contagem_Codigo[0];
               pr_default.close(31);
               dsDefault.SmartCacheProvider.SetUpdated("Contagem") ;
               if ( (pr_default.getStatus(31) == 1) )
               {
                  context.Gx_err = 1;
                  Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
               }
               else
               {
                  context.Gx_err = 0;
                  Gx_emsg = "";
               }
               /* End Insert */
               /* Execute user subroutine: 'ATUALIZA.VINCULO.DEMANDA.PRINCIPAL' */
               S1818 ();
               if ( returnInSub )
               {
                  pr_default.close(28);
                  returnInSub = true;
                  if (true) return;
               }
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               A456ContagemResultado_Codigo = W456ContagemResultado_Codigo;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(28);
            new prc_inslogresponsavel(context ).execute( ref  AV96ContagemFS_Codigo,  AV97ContadorFS_Codigo,  "I",  "D",  AV83WWPContext.gxTpr_Userid,  0,  "",  AV133Status,  "PF da FS.",  AV121PrazoEntrega,  false) ;
            /* Using cursor P007034 */
            pr_default.execute(32, new Object[] {AV35ContagemResultado_Codigo});
            while ( (pr_default.getStatus(32) != 101) )
            {
               A2199Contagem_DemandaCod = P007034_A2199Contagem_DemandaCod[0];
               n2199Contagem_DemandaCod = P007034_n2199Contagem_DemandaCod[0];
               A1119Contagem_Divergencia = P007034_A1119Contagem_Divergencia[0];
               n1119Contagem_Divergencia = P007034_n1119Contagem_Divergencia[0];
               A262Contagem_Status = P007034_A262Contagem_Status[0];
               n262Contagem_Status = P007034_n262Contagem_Status[0];
               A192Contagem_Codigo = P007034_A192Contagem_Codigo[0];
               A1119Contagem_Divergencia = AV39ContagemResultado_Divergencia;
               n1119Contagem_Divergencia = false;
               if ( AV40ContagemResultado_StatusCnt == 5 )
               {
                  A262Contagem_Status = "A";
                  n262Contagem_Status = false;
               }
               else
               {
                  A262Contagem_Status = "D";
                  n262Contagem_Status = false;
               }
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               /* Using cursor P007035 */
               pr_default.execute(33, new Object[] {n1119Contagem_Divergencia, A1119Contagem_Divergencia, n262Contagem_Status, A262Contagem_Status, A192Contagem_Codigo});
               pr_default.close(33);
               dsDefault.SmartCacheProvider.SetUpdated("Contagem") ;
               if (true) break;
               /* Using cursor P007036 */
               pr_default.execute(34, new Object[] {n1119Contagem_Divergencia, A1119Contagem_Divergencia, n262Contagem_Status, A262Contagem_Status, A192Contagem_Codigo});
               pr_default.close(34);
               dsDefault.SmartCacheProvider.SetUpdated("Contagem") ;
               pr_default.readNext(32);
            }
            pr_default.close(32);
         }
         context.CommitDataStores( "PRC_ImportarPFFS");
         new prc_upddpnhmlg(context ).execute( ref  AV128OSVinculada) ;
         /* Execute user subroutine: 'ATUALIZARECDAFM' */
         S191 ();
         if (returnInSub) return;
      }

      protected void S1715( )
      {
         /* 'NEWCONTAGEMDATAALTERADA' Routine */
         /*
            INSERT RECORD ON TABLE ContagemResultadoContagens

         */
         A456ContagemResultado_Codigo = AV107ContagemResultado_CodigoFS;
         A473ContagemResultado_DataCnt = AV102ContagemResultado_DataCnt;
         A511ContagemResultado_HoraCnt = "--:--";
         A458ContagemResultado_PFBFS = AV71PFBFS;
         n458ContagemResultado_PFBFS = false;
         A459ContagemResultado_PFLFS = AV75PFLFS;
         n459ContagemResultado_PFLFS = false;
         A800ContagemResultado_Deflator = AV101ContagemResltado_Deflator;
         n800ContagemResultado_Deflator = false;
         A462ContagemResultado_Divergencia = AV39ContagemResultado_Divergencia;
         A482ContagemResultadoContagens_Esforco = 0;
         A470ContagemResultado_ContadorFMCod = AV97ContadorFS_Codigo;
         A483ContagemResultado_StatusCnt = AV40ContagemResultado_StatusCnt;
         A517ContagemResultado_Ultima = true;
         A469ContagemResultado_NaoCnfCntCod = 0;
         n469ContagemResultado_NaoCnfCntCod = false;
         n469ContagemResultado_NaoCnfCntCod = true;
         /* Using cursor P007037 */
         pr_default.execute(35, new Object[] {A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt, n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, A462ContagemResultado_Divergencia, A470ContagemResultado_ContadorFMCod, n469ContagemResultado_NaoCnfCntCod, A469ContagemResultado_NaoCnfCntCod, A483ContagemResultado_StatusCnt, A482ContagemResultadoContagens_Esforco, A517ContagemResultado_Ultima, n800ContagemResultado_Deflator, A800ContagemResultado_Deflator});
         pr_default.close(35);
         dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
         if ( (pr_default.getStatus(35) == 1) )
         {
            context.Gx_err = 1;
            Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
         }
         else
         {
            context.Gx_err = 0;
            Gx_emsg = "";
         }
         /* End Insert */
      }

      protected void S191( )
      {
         /* 'ATUALIZARECDAFM' Routine */
         /* Using cursor P007038 */
         pr_default.execute(36, new Object[] {AV35ContagemResultado_Codigo});
         while ( (pr_default.getStatus(36) != 101) )
         {
            A456ContagemResultado_Codigo = P007038_A456ContagemResultado_Codigo[0];
            A484ContagemResultado_StatusDmn = P007038_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P007038_n484ContagemResultado_StatusDmn[0];
            A1348ContagemResultado_DataHomologacao = P007038_A1348ContagemResultado_DataHomologacao[0];
            n1348ContagemResultado_DataHomologacao = P007038_n1348ContagemResultado_DataHomologacao[0];
            if ( AV40ContagemResultado_StatusCnt == 5 )
            {
               A484ContagemResultado_StatusDmn = AV93StatusFinal;
               n484ContagemResultado_StatusDmn = false;
               if ( StringUtil.StrCmp(AV93StatusFinal, "H") == 0 )
               {
                  A1348ContagemResultado_DataHomologacao = AV126ServerNow;
                  n1348ContagemResultado_DataHomologacao = false;
               }
            }
            else
            {
               A484ContagemResultado_StatusDmn = "B";
               n484ContagemResultado_StatusDmn = false;
            }
            /* Using cursor P007039 */
            pr_default.execute(37, new Object[] {A456ContagemResultado_Codigo});
            while ( (pr_default.getStatus(37) != 101) )
            {
               A517ContagemResultado_Ultima = P007039_A517ContagemResultado_Ultima[0];
               A458ContagemResultado_PFBFS = P007039_A458ContagemResultado_PFBFS[0];
               n458ContagemResultado_PFBFS = P007039_n458ContagemResultado_PFBFS[0];
               A459ContagemResultado_PFLFS = P007039_A459ContagemResultado_PFLFS[0];
               n459ContagemResultado_PFLFS = P007039_n459ContagemResultado_PFLFS[0];
               A462ContagemResultado_Divergencia = P007039_A462ContagemResultado_Divergencia[0];
               A483ContagemResultado_StatusCnt = P007039_A483ContagemResultado_StatusCnt[0];
               A473ContagemResultado_DataCnt = P007039_A473ContagemResultado_DataCnt[0];
               A511ContagemResultado_HoraCnt = P007039_A511ContagemResultado_HoraCnt[0];
               A458ContagemResultado_PFBFS = AV71PFBFS;
               n458ContagemResultado_PFBFS = false;
               A459ContagemResultado_PFLFS = AV75PFLFS;
               n459ContagemResultado_PFLFS = false;
               A462ContagemResultado_Divergencia = AV39ContagemResultado_Divergencia;
               A483ContagemResultado_StatusCnt = AV40ContagemResultado_StatusCnt;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               /* Using cursor P007040 */
               pr_default.execute(38, new Object[] {n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, A462ContagemResultado_Divergencia, A483ContagemResultado_StatusCnt, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
               pr_default.close(38);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
               if (true) break;
               /* Using cursor P007041 */
               pr_default.execute(39, new Object[] {n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, A462ContagemResultado_Divergencia, A483ContagemResultado_StatusCnt, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
               pr_default.close(39);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
               pr_default.readNext(37);
            }
            pr_default.close(37);
            /* Using cursor P007042 */
            pr_default.execute(40, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n1348ContagemResultado_DataHomologacao, A1348ContagemResultado_DataHomologacao, A456ContagemResultado_Codigo});
            pr_default.close(40);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(36);
      }

      protected void S201( )
      {
         /* 'NEWATUALIZABASELINE' Routine */
         /* Using cursor P007043 */
         pr_default.execute(41, new Object[] {AV46Demanda, AV113AreaTrabalho_ContratadaUpdBslCod});
         while ( (pr_default.getStatus(41) != 101) )
         {
            A192Contagem_Codigo = P007043_A192Contagem_Codigo[0];
            A1118Contagem_ContratadaCod = P007043_A1118Contagem_ContratadaCod[0];
            n1118Contagem_ContratadaCod = P007043_n1118Contagem_ContratadaCod[0];
            A945Contagem_Demanda = P007043_A945Contagem_Demanda[0];
            n945Contagem_Demanda = P007043_n945Contagem_Demanda[0];
            /*
               INSERT RECORD ON TABLE Baseline

            */
            A1164Baseline_DataHomologacao = AV126ServerNow;
            A721Baseline_UserCod = AV83WWPContext.gxTpr_Userid;
            A735Baseline_ProjetoMelCod = 0;
            n735Baseline_ProjetoMelCod = false;
            n735Baseline_ProjetoMelCod = true;
            /* Using cursor P007044 */
            pr_default.execute(42, new Object[] {A721Baseline_UserCod, n735Baseline_ProjetoMelCod, A735Baseline_ProjetoMelCod, A192Contagem_Codigo, A1164Baseline_DataHomologacao});
            A722Baseline_Codigo = P007044_A722Baseline_Codigo[0];
            pr_default.close(42);
            dsDefault.SmartCacheProvider.SetUpdated("Baseline") ;
            if ( (pr_default.getStatus(42) == 1) )
            {
               context.Gx_err = 1;
               Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
            }
            else
            {
               context.Gx_err = 0;
               Gx_emsg = "";
            }
            /* End Insert */
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(41);
      }

      protected void S1615( )
      {
         /* 'PRAZOCORRECAO' Routine */
         new prc_prazodecorrecaodata(context ).execute(  0,  AV120PrazoCorrecaoTipo,  AV119PrazoCoreecao, ref  AV124DiasParaAnalise,  AV122PrazoInicial,  AV130PrazoInicio,  AV125FimDoExpediente, ref  AV121PrazoEntrega) ;
      }

      protected void S211( )
      {
         /* 'RESPONSAVEL' Routine */
         /* Using cursor P007045 */
         pr_default.execute(43, new Object[] {AV35ContagemResultado_Codigo});
         while ( (pr_default.getStatus(43) != 101) )
         {
            A1797LogResponsavel_Codigo = P007045_A1797LogResponsavel_Codigo[0];
            A896LogResponsavel_Owner = P007045_A896LogResponsavel_Owner[0];
            A892LogResponsavel_DemandaCod = P007045_A892LogResponsavel_DemandaCod[0];
            n892LogResponsavel_DemandaCod = P007045_n892LogResponsavel_DemandaCod[0];
            GXt_boolean2 = A1149LogResponsavel_OwnerEhContratante;
            new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A896LogResponsavel_Owner, out  GXt_boolean2) ;
            A1149LogResponsavel_OwnerEhContratante = GXt_boolean2;
            if ( ! A1149LogResponsavel_OwnerEhContratante )
            {
               AV131Responsavel = A896LogResponsavel_Owner;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            pr_default.readNext(43);
         }
         pr_default.close(43);
      }

      protected void S221( )
      {
         /* 'BUSCA.CODIGO.SISTEMA' Routine */
         AV136Sistema_Codigo = 0;
         /* Using cursor P007046 */
         pr_default.execute(44, new Object[] {AV83WWPContext.gxTpr_Areatrabalho_codigo, AV78Sistema_Sigla});
         while ( (pr_default.getStatus(44) != 101) )
         {
            A135Sistema_AreaTrabalhoCod = P007046_A135Sistema_AreaTrabalhoCod[0];
            A129Sistema_Sigla = P007046_A129Sistema_Sigla[0];
            A127Sistema_Codigo = P007046_A127Sistema_Codigo[0];
            AV136Sistema_Codigo = A127Sistema_Codigo;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(44);
         }
         pr_default.close(44);
      }

      protected void S1818( )
      {
         /* 'ATUALIZA.VINCULO.DEMANDA.PRINCIPAL' Routine */
         /* Optimized UPDATE. */
         /* Using cursor P007047 */
         pr_default.execute(45, new Object[] {n602ContagemResultado_OSVinculada, AV96ContagemFS_Codigo, AV35ContagemResultado_Codigo});
         pr_default.close(45);
         dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
         dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
         /* End optimized UPDATE. */
      }

      protected void H700( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("P�gina", 675, Gx_line+0, 710, Gx_line+14, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("de", 750, Gx_line+0, 764, Gx_line+14, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("{{Pages}}", 767, Gx_line+0, 816, Gx_line+14, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(Gx_page), "ZZZZZ9")), 708, Gx_line+0, 747, Gx_line+15, 1+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+15);
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( Gx_time, "")), 725, Gx_line+9, 818, Gx_line+24, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(context.localUtil.Format( Gx_date, "99/99/99"), 667, Gx_line+9, 716, Gx_line+24, 2+256, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV12ArqTitulo, "")), 8, Gx_line+50, 808, Gx_line+68, 1, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV44ContratadaFS_Nome, "@!")), 243, Gx_line+33, 557, Gx_line+51, 1+256, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 14, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Importa��o de Pontos de Fun��o da FS", 225, Gx_line+0, 575, Gx_line+25, 0+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+96);
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
         add_metrics1( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics1( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_ImportarPFFS");
         CloseOpenCursors();
         if (IsMain)	waitPrinterEnd();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         AV83WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV12ArqTitulo = "";
         AV53ExcelDocument = new ExcelDocumentI();
         scmdbuf = "";
         AV46Demanda = "";
         P00702_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00702_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00702_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00702_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00702_A457ContagemResultado_Demanda = new String[] {""} ;
         P00702_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00702_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00702_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00702_A601ContagemResultado_Servico = new int[1] ;
         P00702_n601ContagemResultado_Servico = new bool[] {false} ;
         P00702_A1046ContagemResultado_Agrupador = new String[] {""} ;
         P00702_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         P00702_A456ContagemResultado_Codigo = new int[1] ;
         A457ContagemResultado_Demanda = "";
         A1046ContagemResultado_Agrupador = "";
         AV90Agrupador = "";
         P00703_A74Contrato_Codigo = new int[1] ;
         P00703_A39Contratada_Codigo = new int[1] ;
         P00703_A40Contratada_PessoaCod = new int[1] ;
         P00703_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00703_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00703_A160ContratoServicos_Codigo = new int[1] ;
         P00703_A41Contratada_PessoaNom = new String[] {""} ;
         P00703_n41Contratada_PessoaNom = new bool[] {false} ;
         P00703_A558Servico_Percentual = new decimal[1] ;
         P00703_n558Servico_Percentual = new bool[] {false} ;
         P00703_A116Contrato_ValorUnidadeContratacao = new decimal[1] ;
         P00703_A1224ContratoServicos_PrazoCorrecao = new short[1] ;
         P00703_n1224ContratoServicos_PrazoCorrecao = new bool[] {false} ;
         P00703_A1225ContratoServicos_PrazoCorrecaoTipo = new String[] {""} ;
         P00703_n1225ContratoServicos_PrazoCorrecaoTipo = new bool[] {false} ;
         P00703_A1649ContratoServicos_PrazoInicio = new short[1] ;
         P00703_n1649ContratoServicos_PrazoInicio = new bool[] {false} ;
         P00703_A1152ContratoServicos_PrazoAnalise = new short[1] ;
         P00703_n1152ContratoServicos_PrazoAnalise = new bool[] {false} ;
         A41Contratada_PessoaNom = "";
         A1225ContratoServicos_PrazoCorrecaoTipo = "";
         AV130PrazoInicio = 1;
         AV44ContratadaFS_Nome = "";
         AV120PrazoCorrecaoTipo = "";
         P00704_A5AreaTrabalho_Codigo = new int[1] ;
         P00704_A29Contratante_Codigo = new int[1] ;
         P00704_n29Contratante_Codigo = new bool[] {false} ;
         P00704_A1192Contratante_FimDoExpediente = new DateTime[] {DateTime.MinValue} ;
         P00704_n1192Contratante_FimDoExpediente = new bool[] {false} ;
         P00704_A987AreaTrabalho_ContratadaUpdBslCod = new int[1] ;
         P00704_n987AreaTrabalho_ContratadaUpdBslCod = new bool[] {false} ;
         A1192Contratante_FimDoExpediente = (DateTime)(DateTime.MinValue);
         P00705_A29Contratante_Codigo = new int[1] ;
         P00705_n29Contratante_Codigo = new bool[] {false} ;
         AV125FimDoExpediente = (DateTime)(DateTime.MinValue);
         AV86EmailTextD = "";
         AV88EmailTextC = "";
         AV93StatusFinal = "";
         AV94TxtStatusFinal = "";
         AV126ServerNow = (DateTime)(DateTime.MinValue);
         AV102ContagemResultado_DataCnt = DateTime.MinValue;
         AV62Linha = "";
         AV78Sistema_Sigla = "";
         AV127ContagemResultado_Codigos = new GxSimpleCollection();
         A493ContagemResultado_DemandaFM = "";
         P00706_A457ContagemResultado_Demanda = new String[] {""} ;
         P00706_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00706_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00706_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00706_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         P00706_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         P00706_A489ContagemResultado_SistemaCod = new int[1] ;
         P00706_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00706_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00706_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00706_A456ContagemResultado_Codigo = new int[1] ;
         P00706_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00706_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P00706_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P00706_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         P00706_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         P00706_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P00706_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P00706_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00706_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         A912ContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         A1351ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         A484ContagemResultado_StatusDmn = "";
         AV109ContagemResultado_StatusDmn = "";
         AV118HoraEntrega = (DateTime)(DateTime.MinValue);
         P00708_A833ContagemResultado_CstUntPrd = new decimal[1] ;
         P00708_n833ContagemResultado_CstUntPrd = new bool[] {false} ;
         P00708_A517ContagemResultado_Ultima = new bool[] {false} ;
         P00708_A482ContagemResultadoContagens_Esforco = new short[1] ;
         P00708_A483ContagemResultado_StatusCnt = new short[1] ;
         P00708_A469ContagemResultado_NaoCnfCntCod = new int[1] ;
         P00708_n469ContagemResultado_NaoCnfCntCod = new bool[] {false} ;
         P00708_A470ContagemResultado_ContadorFMCod = new int[1] ;
         P00708_A462ContagemResultado_Divergencia = new decimal[1] ;
         P00708_A461ContagemResultado_PFLFM = new decimal[1] ;
         P00708_n461ContagemResultado_PFLFM = new bool[] {false} ;
         P00708_A460ContagemResultado_PFBFM = new decimal[1] ;
         P00708_n460ContagemResultado_PFBFM = new bool[] {false} ;
         P00708_A459ContagemResultado_PFLFS = new decimal[1] ;
         P00708_n459ContagemResultado_PFLFS = new bool[] {false} ;
         P00708_A458ContagemResultado_PFBFS = new decimal[1] ;
         P00708_n458ContagemResultado_PFBFS = new bool[] {false} ;
         P00708_A511ContagemResultado_HoraCnt = new String[] {""} ;
         P00708_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P00708_A456ContagemResultado_Codigo = new int[1] ;
         P00708_A1756ContagemResultado_NvlCnt = new short[1] ;
         P00708_n1756ContagemResultado_NvlCnt = new bool[] {false} ;
         P00708_A901ContagemResultadoContagens_Prazo = new DateTime[] {DateTime.MinValue} ;
         P00708_n901ContagemResultadoContagens_Prazo = new bool[] {false} ;
         P00708_A854ContagemResultado_TipoPla = new String[] {""} ;
         P00708_n854ContagemResultado_TipoPla = new bool[] {false} ;
         P00708_A853ContagemResultado_NomePla = new String[] {""} ;
         P00708_n853ContagemResultado_NomePla = new bool[] {false} ;
         P00708_A800ContagemResultado_Deflator = new decimal[1] ;
         P00708_n800ContagemResultado_Deflator = new bool[] {false} ;
         P00708_A463ContagemResultado_ParecerTcn = new String[] {""} ;
         P00708_n463ContagemResultado_ParecerTcn = new bool[] {false} ;
         P00708_A481ContagemResultado_TimeCnt = new DateTime[] {DateTime.MinValue} ;
         P00708_n481ContagemResultado_TimeCnt = new bool[] {false} ;
         P00708_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00708_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00708_A1348ContagemResultado_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         P00708_n1348ContagemResultado_DataHomologacao = new bool[] {false} ;
         P00708_A852ContagemResultado_Planilha = new String[] {""} ;
         P00708_n852ContagemResultado_Planilha = new bool[] {false} ;
         A511ContagemResultado_HoraCnt = "";
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A901ContagemResultadoContagens_Prazo = (DateTime)(DateTime.MinValue);
         A854ContagemResultado_TipoPla = "";
         A852ContagemResultado_Planilha_Filetype = "";
         A853ContagemResultado_NomePla = "";
         A852ContagemResultado_Planilha_Filename = "";
         A463ContagemResultado_ParecerTcn = "";
         A481ContagemResultado_TimeCnt = (DateTime)(DateTime.MinValue);
         A1348ContagemResultado_DataHomologacao = (DateTime)(DateTime.MinValue);
         A852ContagemResultado_Planilha = "";
         W473ContagemResultado_DataCnt = DateTime.MinValue;
         W511ContagemResultado_HoraCnt = "";
         Gx_emsg = "";
         P007014_A517ContagemResultado_Ultima = new bool[] {false} ;
         P007014_A456ContagemResultado_Codigo = new int[1] ;
         P007014_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P007014_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P007014_A799ContagemResultado_PFLFSImp = new decimal[1] ;
         P007014_n799ContagemResultado_PFLFSImp = new bool[] {false} ;
         P007014_A798ContagemResultado_PFBFSImp = new decimal[1] ;
         P007014_n798ContagemResultado_PFBFSImp = new bool[] {false} ;
         P007014_A459ContagemResultado_PFLFS = new decimal[1] ;
         P007014_n459ContagemResultado_PFLFS = new bool[] {false} ;
         P007014_A458ContagemResultado_PFBFS = new decimal[1] ;
         P007014_n458ContagemResultado_PFBFS = new bool[] {false} ;
         P007014_A460ContagemResultado_PFBFM = new decimal[1] ;
         P007014_n460ContagemResultado_PFBFM = new bool[] {false} ;
         P007014_A461ContagemResultado_PFLFM = new decimal[1] ;
         P007014_n461ContagemResultado_PFLFM = new bool[] {false} ;
         P007014_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P007014_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P007014_A483ContagemResultado_StatusCnt = new short[1] ;
         P007014_A462ContagemResultado_Divergencia = new decimal[1] ;
         P007014_A1348ContagemResultado_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         P007014_n1348ContagemResultado_DataHomologacao = new bool[] {false} ;
         P007014_A890ContagemResultado_Responsavel = new int[1] ;
         P007014_n890ContagemResultado_Responsavel = new bool[] {false} ;
         P007014_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P007014_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P007014_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P007014_A511ContagemResultado_HoraCnt = new String[] {""} ;
         AV132StatusAnt = "";
         AV79Totais = "";
         AV84Usuarios = new GxSimpleCollection();
         P007019_A293Usuario_EhFinanceiro = new bool[] {false} ;
         P007019_n293Usuario_EhFinanceiro = new bool[] {false} ;
         P007019_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P007019_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         AV87Subject = "";
         AV82WebSession = context.GetSession();
         AV91Resultado = "";
         AV85Attachments = new GxSimpleCollection();
         P007020_A291Usuario_EhContratada = new bool[] {false} ;
         P007020_n291Usuario_EhContratada = new bool[] {false} ;
         P007020_A74Contrato_Codigo = new int[1] ;
         P007020_A155Servico_Codigo = new int[1] ;
         P007020_A39Contratada_Codigo = new int[1] ;
         P007020_A1013Contrato_PrepostoCod = new int[1] ;
         P007020_n1013Contrato_PrepostoCod = new bool[] {false} ;
         P007020_A160ContratoServicos_Codigo = new int[1] ;
         P007021_A1078ContratoGestor_ContratoCod = new int[1] ;
         P007021_A1079ContratoGestor_UsuarioCod = new int[1] ;
         AV15CalculoDivergencia = "";
         P007022_A146Modulo_Codigo = new int[1] ;
         P007022_n146Modulo_Codigo = new bool[] {false} ;
         P007022_A127Sistema_Codigo = new int[1] ;
         P007022_A1399Sistema_ImpUserCod = new int[1] ;
         P007022_n1399Sistema_ImpUserCod = new bool[] {false} ;
         P007022_A1073Usuario_CargoCod = new int[1] ;
         P007022_n1073Usuario_CargoCod = new bool[] {false} ;
         P007022_A1075Usuario_CargoUOCod = new int[1] ;
         P007022_n1075Usuario_CargoUOCod = new bool[] {false} ;
         P007022_A137Metodologia_Codigo = new int[1] ;
         P007022_n137Metodologia_Codigo = new bool[] {false} ;
         P007022_A609TpUo_Codigo = new int[1] ;
         P007022_n609TpUo_Codigo = new bool[] {false} ;
         P007022_A456ContagemResultado_Codigo = new int[1] ;
         P007022_A489ContagemResultado_SistemaCod = new int[1] ;
         P007022_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P007022_A490ContagemResultado_ContratadaCod = new int[1] ;
         P007022_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P007022_A457ContagemResultado_Demanda = new String[] {""} ;
         P007022_n457ContagemResultado_Demanda = new bool[] {false} ;
         P007022_A602ContagemResultado_OSVinculada = new int[1] ;
         P007022_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P007022_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P007022_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P007022_A1348ContagemResultado_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         P007022_n1348ContagemResultado_DataHomologacao = new bool[] {false} ;
         P007022_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         P007022_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         P007022_A1237ContagemResultado_PrazoMaisDias = new short[1] ;
         P007022_n1237ContagemResultado_PrazoMaisDias = new bool[] {false} ;
         P007022_A1227ContagemResultado_PrazoInicialDias = new short[1] ;
         P007022_n1227ContagemResultado_PrazoInicialDias = new bool[] {false} ;
         P007022_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P007022_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P007022_A512ContagemResultado_ValorPF = new decimal[1] ;
         P007022_n512ContagemResultado_ValorPF = new bool[] {false} ;
         AV121PrazoEntrega = (DateTime)(DateTime.MinValue);
         P007023_A456ContagemResultado_Codigo = new int[1] ;
         P007023_A517ContagemResultado_Ultima = new bool[] {false} ;
         P007023_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P007023_A458ContagemResultado_PFBFS = new decimal[1] ;
         P007023_n458ContagemResultado_PFBFS = new bool[] {false} ;
         P007023_A459ContagemResultado_PFLFS = new decimal[1] ;
         P007023_n459ContagemResultado_PFLFS = new bool[] {false} ;
         P007023_A462ContagemResultado_Divergencia = new decimal[1] ;
         P007023_A483ContagemResultado_StatusCnt = new short[1] ;
         P007023_A511ContagemResultado_HoraCnt = new String[] {""} ;
         P007027_A2183Contagem_TipoUnidade = new int[1] ;
         P007027_n2183Contagem_TipoUnidade = new bool[] {false} ;
         P007027_A2185Contagem_Metodologia = new int[1] ;
         P007027_n2185Contagem_Metodologia = new bool[] {false} ;
         P007027_A1118Contagem_ContratadaCod = new int[1] ;
         P007027_n1118Contagem_ContratadaCod = new bool[] {false} ;
         P007027_A945Contagem_Demanda = new String[] {""} ;
         P007027_n945Contagem_Demanda = new bool[] {false} ;
         P007027_A943Contagem_PFB = new decimal[1] ;
         P007027_n943Contagem_PFB = new bool[] {false} ;
         P007027_A944Contagem_PFL = new decimal[1] ;
         P007027_n944Contagem_PFL = new bool[] {false} ;
         P007027_A947Contagem_Fator = new decimal[1] ;
         P007027_n947Contagem_Fator = new bool[] {false} ;
         P007027_A1113Contagem_PFBA = new decimal[1] ;
         P007027_n1113Contagem_PFBA = new bool[] {false} ;
         P007027_A1114Contagem_PFLA = new decimal[1] ;
         P007027_n1114Contagem_PFLA = new bool[] {false} ;
         P007027_A1117Contagem_Deflator = new decimal[1] ;
         P007027_n1117Contagem_Deflator = new bool[] {false} ;
         P007027_A1115Contagem_PFBD = new decimal[1] ;
         P007027_n1115Contagem_PFBD = new bool[] {false} ;
         P007027_A1116Contagem_PFLD = new decimal[1] ;
         P007027_n1116Contagem_PFLD = new bool[] {false} ;
         P007027_A2200Contagem_DataAtualizacao = new DateTime[] {DateTime.MinValue} ;
         P007027_n2200Contagem_DataAtualizacao = new bool[] {false} ;
         P007027_A1119Contagem_Divergencia = new decimal[1] ;
         P007027_n1119Contagem_Divergencia = new bool[] {false} ;
         P007027_A262Contagem_Status = new String[] {""} ;
         P007027_n262Contagem_Status = new bool[] {false} ;
         P007027_A192Contagem_Codigo = new int[1] ;
         A945Contagem_Demanda = "";
         A2200Contagem_DataAtualizacao = (DateTime)(DateTime.MinValue);
         A262Contagem_Status = "";
         P007030_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P007030_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P007030_A1452ContagemResultado_SS = new int[1] ;
         P007030_n1452ContagemResultado_SS = new bool[] {false} ;
         P007030_A1348ContagemResultado_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         P007030_n1348ContagemResultado_DataHomologacao = new bool[] {false} ;
         P007030_A1046ContagemResultado_Agrupador = new String[] {""} ;
         P007030_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         P007030_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         P007030_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         P007030_A890ContagemResultado_Responsavel = new int[1] ;
         P007030_n890ContagemResultado_Responsavel = new bool[] {false} ;
         P007030_A602ContagemResultado_OSVinculada = new int[1] ;
         P007030_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P007030_A512ContagemResultado_ValorPF = new decimal[1] ;
         P007030_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P007030_A490ContagemResultado_ContratadaCod = new int[1] ;
         P007030_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P007030_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P007030_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P007030_A457ContagemResultado_Demanda = new String[] {""} ;
         P007030_n457ContagemResultado_Demanda = new bool[] {false} ;
         P007030_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P007030_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P007030_A2133ContagemResultado_QuantidadeSolicitada = new decimal[1] ;
         P007030_n2133ContagemResultado_QuantidadeSolicitada = new bool[] {false} ;
         P007030_A2017ContagemResultado_DataEntregaReal = new DateTime[] {DateTime.MinValue} ;
         P007030_n2017ContagemResultado_DataEntregaReal = new bool[] {false} ;
         P007030_A1903ContagemResultado_DataPrvPgm = new DateTime[] {DateTime.MinValue} ;
         P007030_n1903ContagemResultado_DataPrvPgm = new bool[] {false} ;
         P007030_A1855ContagemResultado_PFCnc = new decimal[1] ;
         P007030_n1855ContagemResultado_PFCnc = new bool[] {false} ;
         P007030_A1854ContagemResultado_VlrCnc = new decimal[1] ;
         P007030_n1854ContagemResultado_VlrCnc = new bool[] {false} ;
         P007030_A1791ContagemResultado_SemCusto = new bool[] {false} ;
         P007030_n1791ContagemResultado_SemCusto = new bool[] {false} ;
         P007030_A1790ContagemResultado_DataInicio = new DateTime[] {DateTime.MinValue} ;
         P007030_n1790ContagemResultado_DataInicio = new bool[] {false} ;
         P007030_A1762ContagemResultado_Entrega = new short[1] ;
         P007030_n1762ContagemResultado_Entrega = new bool[] {false} ;
         P007030_A1714ContagemResultado_Combinada = new bool[] {false} ;
         P007030_n1714ContagemResultado_Combinada = new bool[] {false} ;
         P007030_A1636ContagemResultado_ServicoSS = new int[1] ;
         P007030_n1636ContagemResultado_ServicoSS = new bool[] {false} ;
         P007030_A1587ContagemResultado_PrioridadePrevista = new String[] {""} ;
         P007030_n1587ContagemResultado_PrioridadePrevista = new bool[] {false} ;
         P007030_A1586ContagemResultado_Restricoes = new String[] {""} ;
         P007030_n1586ContagemResultado_Restricoes = new bool[] {false} ;
         P007030_A1585ContagemResultado_Referencia = new String[] {""} ;
         P007030_n1585ContagemResultado_Referencia = new bool[] {false} ;
         P007030_A1584ContagemResultado_UOOwner = new int[1] ;
         P007030_n1584ContagemResultado_UOOwner = new bool[] {false} ;
         P007030_A1583ContagemResultado_TipoRegistro = new short[1] ;
         P007030_A1559ContagemResultado_VlrAceite = new decimal[1] ;
         P007030_n1559ContagemResultado_VlrAceite = new bool[] {false} ;
         P007030_A1544ContagemResultado_ProjetoCod = new int[1] ;
         P007030_n1544ContagemResultado_ProjetoCod = new bool[] {false} ;
         P007030_A1521ContagemResultado_FimAnl = new DateTime[] {DateTime.MinValue} ;
         P007030_n1521ContagemResultado_FimAnl = new bool[] {false} ;
         P007030_A1520ContagemResultado_InicioAnl = new DateTime[] {DateTime.MinValue} ;
         P007030_n1520ContagemResultado_InicioAnl = new bool[] {false} ;
         P007030_A1519ContagemResultado_TmpEstAnl = new int[1] ;
         P007030_n1519ContagemResultado_TmpEstAnl = new bool[] {false} ;
         P007030_A1515ContagemResultado_Evento = new short[1] ;
         P007030_n1515ContagemResultado_Evento = new bool[] {false} ;
         P007030_A1512ContagemResultado_FimCrr = new DateTime[] {DateTime.MinValue} ;
         P007030_n1512ContagemResultado_FimCrr = new bool[] {false} ;
         P007030_A1511ContagemResultado_InicioCrr = new DateTime[] {DateTime.MinValue} ;
         P007030_n1511ContagemResultado_InicioCrr = new bool[] {false} ;
         P007030_A1510ContagemResultado_FimExc = new DateTime[] {DateTime.MinValue} ;
         P007030_n1510ContagemResultado_FimExc = new bool[] {false} ;
         P007030_A1509ContagemResultado_InicioExc = new DateTime[] {DateTime.MinValue} ;
         P007030_n1509ContagemResultado_InicioExc = new bool[] {false} ;
         P007030_A1506ContagemResultado_TmpEstCrr = new int[1] ;
         P007030_n1506ContagemResultado_TmpEstCrr = new bool[] {false} ;
         P007030_A1505ContagemResultado_TmpEstExc = new int[1] ;
         P007030_n1505ContagemResultado_TmpEstExc = new bool[] {false} ;
         P007030_A1457ContagemResultado_TemDpnHmlg = new bool[] {false} ;
         P007030_n1457ContagemResultado_TemDpnHmlg = new bool[] {false} ;
         P007030_A1445ContagemResultado_CntSrvPrrCst = new decimal[1] ;
         P007030_n1445ContagemResultado_CntSrvPrrCst = new bool[] {false} ;
         P007030_A1444ContagemResultado_CntSrvPrrPrz = new decimal[1] ;
         P007030_n1444ContagemResultado_CntSrvPrrPrz = new bool[] {false} ;
         P007030_A1443ContagemResultado_CntSrvPrrCod = new int[1] ;
         P007030_n1443ContagemResultado_CntSrvPrrCod = new bool[] {false} ;
         P007030_A1392ContagemResultado_RdmnUpdated = new DateTime[] {DateTime.MinValue} ;
         P007030_n1392ContagemResultado_RdmnUpdated = new bool[] {false} ;
         P007030_A1390ContagemResultado_RdmnProjectId = new int[1] ;
         P007030_n1390ContagemResultado_RdmnProjectId = new bool[] {false} ;
         P007030_A1389ContagemResultado_RdmnIssueId = new int[1] ;
         P007030_n1389ContagemResultado_RdmnIssueId = new bool[] {false} ;
         P007030_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P007030_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P007030_A1350ContagemResultado_DataCadastro = new DateTime[] {DateTime.MinValue} ;
         P007030_n1350ContagemResultado_DataCadastro = new bool[] {false} ;
         P007030_A1349ContagemResultado_DataExecucao = new DateTime[] {DateTime.MinValue} ;
         P007030_n1349ContagemResultado_DataExecucao = new bool[] {false} ;
         P007030_A1237ContagemResultado_PrazoMaisDias = new short[1] ;
         P007030_n1237ContagemResultado_PrazoMaisDias = new bool[] {false} ;
         P007030_A1227ContagemResultado_PrazoInicialDias = new short[1] ;
         P007030_n1227ContagemResultado_PrazoInicialDias = new bool[] {false} ;
         P007030_A1180ContagemResultado_Custo = new decimal[1] ;
         P007030_n1180ContagemResultado_Custo = new bool[] {false} ;
         P007030_A1179ContagemResultado_PLFinal = new decimal[1] ;
         P007030_n1179ContagemResultado_PLFinal = new bool[] {false} ;
         P007030_A1178ContagemResultado_PBFinal = new decimal[1] ;
         P007030_n1178ContagemResultado_PBFinal = new bool[] {false} ;
         P007030_A1173ContagemResultado_OSManual = new bool[] {false} ;
         P007030_n1173ContagemResultado_OSManual = new bool[] {false} ;
         P007030_A1052ContagemResultado_GlsUser = new int[1] ;
         P007030_n1052ContagemResultado_GlsUser = new bool[] {false} ;
         P007030_A1051ContagemResultado_GlsValor = new decimal[1] ;
         P007030_n1051ContagemResultado_GlsValor = new bool[] {false} ;
         P007030_A1050ContagemResultado_GlsDescricao = new String[] {""} ;
         P007030_n1050ContagemResultado_GlsDescricao = new bool[] {false} ;
         P007030_A1049ContagemResultado_GlsData = new DateTime[] {DateTime.MinValue} ;
         P007030_n1049ContagemResultado_GlsData = new bool[] {false} ;
         P007030_A1044ContagemResultado_FncUsrCod = new int[1] ;
         P007030_n1044ContagemResultado_FncUsrCod = new bool[] {false} ;
         P007030_A1043ContagemResultado_LiqLogCod = new int[1] ;
         P007030_n1043ContagemResultado_LiqLogCod = new bool[] {false} ;
         P007030_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         P007030_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         P007030_A799ContagemResultado_PFLFSImp = new decimal[1] ;
         P007030_n799ContagemResultado_PFLFSImp = new bool[] {false} ;
         P007030_A798ContagemResultado_PFBFSImp = new decimal[1] ;
         P007030_n798ContagemResultado_PFBFSImp = new bool[] {false} ;
         P007030_A598ContagemResultado_Baseline = new bool[] {false} ;
         P007030_n598ContagemResultado_Baseline = new bool[] {false} ;
         P007030_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P007030_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P007030_A592ContagemResultado_Evidencia = new String[] {""} ;
         P007030_n592ContagemResultado_Evidencia = new bool[] {false} ;
         P007030_A508ContagemResultado_Owner = new int[1] ;
         P007030_A514ContagemResultado_Observacao = new String[] {""} ;
         P007030_n514ContagemResultado_Observacao = new bool[] {false} ;
         P007030_A146Modulo_Codigo = new int[1] ;
         P007030_n146Modulo_Codigo = new bool[] {false} ;
         P007030_A489ContagemResultado_SistemaCod = new int[1] ;
         P007030_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P007030_A494ContagemResultado_Descricao = new String[] {""} ;
         P007030_n494ContagemResultado_Descricao = new bool[] {false} ;
         P007030_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P007030_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P007030_A485ContagemResultado_EhValidacao = new bool[] {false} ;
         P007030_n485ContagemResultado_EhValidacao = new bool[] {false} ;
         P007030_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         P007030_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         P007030_A465ContagemResultado_Link = new String[] {""} ;
         P007030_n465ContagemResultado_Link = new bool[] {false} ;
         P007030_A454ContagemResultado_ContadorFSCod = new int[1] ;
         P007030_n454ContagemResultado_ContadorFSCod = new bool[] {false} ;
         P007030_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P007030_A456ContagemResultado_Codigo = new int[1] ;
         A2017ContagemResultado_DataEntregaReal = (DateTime)(DateTime.MinValue);
         A1903ContagemResultado_DataPrvPgm = DateTime.MinValue;
         A1790ContagemResultado_DataInicio = DateTime.MinValue;
         A1587ContagemResultado_PrioridadePrevista = "";
         A1586ContagemResultado_Restricoes = "";
         A1585ContagemResultado_Referencia = "";
         A1521ContagemResultado_FimAnl = (DateTime)(DateTime.MinValue);
         A1520ContagemResultado_InicioAnl = (DateTime)(DateTime.MinValue);
         A1512ContagemResultado_FimCrr = (DateTime)(DateTime.MinValue);
         A1511ContagemResultado_InicioCrr = (DateTime)(DateTime.MinValue);
         A1510ContagemResultado_FimExc = (DateTime)(DateTime.MinValue);
         A1509ContagemResultado_InicioExc = (DateTime)(DateTime.MinValue);
         A1392ContagemResultado_RdmnUpdated = (DateTime)(DateTime.MinValue);
         A1350ContagemResultado_DataCadastro = (DateTime)(DateTime.MinValue);
         A1349ContagemResultado_DataExecucao = (DateTime)(DateTime.MinValue);
         A1050ContagemResultado_GlsDescricao = "";
         A1049ContagemResultado_GlsData = DateTime.MinValue;
         A592ContagemResultado_Evidencia = "";
         A514ContagemResultado_Observacao = "";
         A494ContagemResultado_Descricao = "";
         A465ContagemResultado_Link = "";
         W457ContagemResultado_Demanda = "";
         W484ContagemResultado_StatusDmn = "";
         W1348ContagemResultado_DataHomologacao = (DateTime)(DateTime.MinValue);
         W472ContagemResultado_DataEntrega = DateTime.MinValue;
         W912ContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         W1046ContagemResultado_Agrupador = "";
         AV133Status = "";
         P007031_A456ContagemResultado_Codigo = new int[1] ;
         A197Contagem_DataCriacao = DateTime.MinValue;
         A195Contagem_Tecnica = "";
         A196Contagem_Tipo = "";
         A2182Contagem_Pendencia = "";
         A2187Contagem_Agrupador = "";
         P007033_A192Contagem_Codigo = new int[1] ;
         P007034_A2199Contagem_DemandaCod = new int[1] ;
         P007034_n2199Contagem_DemandaCod = new bool[] {false} ;
         P007034_A1119Contagem_Divergencia = new decimal[1] ;
         P007034_n1119Contagem_Divergencia = new bool[] {false} ;
         P007034_A262Contagem_Status = new String[] {""} ;
         P007034_n262Contagem_Status = new bool[] {false} ;
         P007034_A192Contagem_Codigo = new int[1] ;
         P007038_A456ContagemResultado_Codigo = new int[1] ;
         P007038_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P007038_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P007038_A1348ContagemResultado_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         P007038_n1348ContagemResultado_DataHomologacao = new bool[] {false} ;
         P007039_A456ContagemResultado_Codigo = new int[1] ;
         P007039_A517ContagemResultado_Ultima = new bool[] {false} ;
         P007039_A458ContagemResultado_PFBFS = new decimal[1] ;
         P007039_n458ContagemResultado_PFBFS = new bool[] {false} ;
         P007039_A459ContagemResultado_PFLFS = new decimal[1] ;
         P007039_n459ContagemResultado_PFLFS = new bool[] {false} ;
         P007039_A462ContagemResultado_Divergencia = new decimal[1] ;
         P007039_A483ContagemResultado_StatusCnt = new short[1] ;
         P007039_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P007039_A511ContagemResultado_HoraCnt = new String[] {""} ;
         P007043_A192Contagem_Codigo = new int[1] ;
         P007043_A1118Contagem_ContratadaCod = new int[1] ;
         P007043_n1118Contagem_ContratadaCod = new bool[] {false} ;
         P007043_A945Contagem_Demanda = new String[] {""} ;
         P007043_n945Contagem_Demanda = new bool[] {false} ;
         A1164Baseline_DataHomologacao = (DateTime)(DateTime.MinValue);
         P007044_A722Baseline_Codigo = new int[1] ;
         P007045_A1797LogResponsavel_Codigo = new long[1] ;
         P007045_A896LogResponsavel_Owner = new int[1] ;
         P007045_A892LogResponsavel_DemandaCod = new int[1] ;
         P007045_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         P007046_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P007046_A129Sistema_Sigla = new String[] {""} ;
         P007046_A127Sistema_Codigo = new int[1] ;
         A129Sistema_Sigla = "";
         Gx_time = "";
         Gx_date = DateTime.MinValue;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.aprc_importarpffs__default(),
            new Object[][] {
                new Object[] {
               P00702_A1553ContagemResultado_CntSrvCod, P00702_n1553ContagemResultado_CntSrvCod, P00702_A52Contratada_AreaTrabalhoCod, P00702_n52Contratada_AreaTrabalhoCod, P00702_A457ContagemResultado_Demanda, P00702_n457ContagemResultado_Demanda, P00702_A490ContagemResultado_ContratadaCod, P00702_n490ContagemResultado_ContratadaCod, P00702_A601ContagemResultado_Servico, P00702_n601ContagemResultado_Servico,
               P00702_A1046ContagemResultado_Agrupador, P00702_n1046ContagemResultado_Agrupador, P00702_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P00703_A74Contrato_Codigo, P00703_A39Contratada_Codigo, P00703_A40Contratada_PessoaCod, P00703_A52Contratada_AreaTrabalhoCod, P00703_A160ContratoServicos_Codigo, P00703_A41Contratada_PessoaNom, P00703_n41Contratada_PessoaNom, P00703_A558Servico_Percentual, P00703_n558Servico_Percentual, P00703_A116Contrato_ValorUnidadeContratacao,
               P00703_A1224ContratoServicos_PrazoCorrecao, P00703_n1224ContratoServicos_PrazoCorrecao, P00703_A1225ContratoServicos_PrazoCorrecaoTipo, P00703_n1225ContratoServicos_PrazoCorrecaoTipo, P00703_A1649ContratoServicos_PrazoInicio, P00703_n1649ContratoServicos_PrazoInicio, P00703_A1152ContratoServicos_PrazoAnalise, P00703_n1152ContratoServicos_PrazoAnalise
               }
               , new Object[] {
               P00704_A5AreaTrabalho_Codigo, P00704_A29Contratante_Codigo, P00704_n29Contratante_Codigo, P00704_A1192Contratante_FimDoExpediente, P00704_n1192Contratante_FimDoExpediente, P00704_A987AreaTrabalho_ContratadaUpdBslCod, P00704_n987AreaTrabalho_ContratadaUpdBslCod
               }
               , new Object[] {
               P00705_A29Contratante_Codigo
               }
               , new Object[] {
               P00706_A457ContagemResultado_Demanda, P00706_n457ContagemResultado_Demanda, P00706_A490ContagemResultado_ContratadaCod, P00706_n490ContagemResultado_ContratadaCod, P00706_A805ContagemResultado_ContratadaOrigemCod, P00706_n805ContagemResultado_ContratadaOrigemCod, P00706_A489ContagemResultado_SistemaCod, P00706_n489ContagemResultado_SistemaCod, P00706_A493ContagemResultado_DemandaFM, P00706_n493ContagemResultado_DemandaFM,
               P00706_A456ContagemResultado_Codigo, P00706_A471ContagemResultado_DataDmn, P00706_A472ContagemResultado_DataEntrega, P00706_n472ContagemResultado_DataEntrega, P00706_A912ContagemResultado_HoraEntrega, P00706_n912ContagemResultado_HoraEntrega, P00706_A1351ContagemResultado_DataPrevista, P00706_n1351ContagemResultado_DataPrevista, P00706_A484ContagemResultado_StatusDmn, P00706_n484ContagemResultado_StatusDmn
               }
               , new Object[] {
               }
               , new Object[] {
               P00708_A833ContagemResultado_CstUntPrd, P00708_n833ContagemResultado_CstUntPrd, P00708_A517ContagemResultado_Ultima, P00708_A482ContagemResultadoContagens_Esforco, P00708_A483ContagemResultado_StatusCnt, P00708_A469ContagemResultado_NaoCnfCntCod, P00708_n469ContagemResultado_NaoCnfCntCod, P00708_A470ContagemResultado_ContadorFMCod, P00708_A462ContagemResultado_Divergencia, P00708_A461ContagemResultado_PFLFM,
               P00708_n461ContagemResultado_PFLFM, P00708_A460ContagemResultado_PFBFM, P00708_n460ContagemResultado_PFBFM, P00708_A459ContagemResultado_PFLFS, P00708_n459ContagemResultado_PFLFS, P00708_A458ContagemResultado_PFBFS, P00708_n458ContagemResultado_PFBFS, P00708_A511ContagemResultado_HoraCnt, P00708_A473ContagemResultado_DataCnt, P00708_A456ContagemResultado_Codigo,
               P00708_A1756ContagemResultado_NvlCnt, P00708_n1756ContagemResultado_NvlCnt, P00708_A901ContagemResultadoContagens_Prazo, P00708_n901ContagemResultadoContagens_Prazo, P00708_A854ContagemResultado_TipoPla, P00708_n854ContagemResultado_TipoPla, P00708_A853ContagemResultado_NomePla, P00708_n853ContagemResultado_NomePla, P00708_A800ContagemResultado_Deflator, P00708_n800ContagemResultado_Deflator,
               P00708_A463ContagemResultado_ParecerTcn, P00708_n463ContagemResultado_ParecerTcn, P00708_A481ContagemResultado_TimeCnt, P00708_n481ContagemResultado_TimeCnt, P00708_A484ContagemResultado_StatusDmn, P00708_n484ContagemResultado_StatusDmn, P00708_A1348ContagemResultado_DataHomologacao, P00708_n1348ContagemResultado_DataHomologacao, P00708_A852ContagemResultado_Planilha, P00708_n852ContagemResultado_Planilha
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P007014_A517ContagemResultado_Ultima, P007014_A456ContagemResultado_Codigo, P007014_A484ContagemResultado_StatusDmn, P007014_n484ContagemResultado_StatusDmn, P007014_A799ContagemResultado_PFLFSImp, P007014_n799ContagemResultado_PFLFSImp, P007014_A798ContagemResultado_PFBFSImp, P007014_n798ContagemResultado_PFBFSImp, P007014_A459ContagemResultado_PFLFS, P007014_n459ContagemResultado_PFLFS,
               P007014_A458ContagemResultado_PFBFS, P007014_n458ContagemResultado_PFBFS, P007014_A460ContagemResultado_PFBFM, P007014_n460ContagemResultado_PFBFM, P007014_A461ContagemResultado_PFLFM, P007014_n461ContagemResultado_PFLFM, P007014_A1553ContagemResultado_CntSrvCod, P007014_n1553ContagemResultado_CntSrvCod, P007014_A483ContagemResultado_StatusCnt, P007014_A462ContagemResultado_Divergencia,
               P007014_A1348ContagemResultado_DataHomologacao, P007014_n1348ContagemResultado_DataHomologacao, P007014_A890ContagemResultado_Responsavel, P007014_n890ContagemResultado_Responsavel, P007014_A1351ContagemResultado_DataPrevista, P007014_n1351ContagemResultado_DataPrevista, P007014_A473ContagemResultado_DataCnt, P007014_A511ContagemResultado_HoraCnt
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P007019_A293Usuario_EhFinanceiro, P007019_n293Usuario_EhFinanceiro, P007019_A66ContratadaUsuario_ContratadaCod, P007019_A69ContratadaUsuario_UsuarioCod
               }
               , new Object[] {
               P007020_A291Usuario_EhContratada, P007020_n291Usuario_EhContratada, P007020_A74Contrato_Codigo, P007020_A155Servico_Codigo, P007020_A39Contratada_Codigo, P007020_A1013Contrato_PrepostoCod, P007020_n1013Contrato_PrepostoCod, P007020_A160ContratoServicos_Codigo
               }
               , new Object[] {
               P007021_A1078ContratoGestor_ContratoCod, P007021_A1079ContratoGestor_UsuarioCod
               }
               , new Object[] {
               P007022_A146Modulo_Codigo, P007022_n146Modulo_Codigo, P007022_A127Sistema_Codigo, P007022_A1399Sistema_ImpUserCod, P007022_n1399Sistema_ImpUserCod, P007022_A1073Usuario_CargoCod, P007022_n1073Usuario_CargoCod, P007022_A1075Usuario_CargoUOCod, P007022_n1075Usuario_CargoUOCod, P007022_A137Metodologia_Codigo,
               P007022_n137Metodologia_Codigo, P007022_A609TpUo_Codigo, P007022_n609TpUo_Codigo, P007022_A456ContagemResultado_Codigo, P007022_A489ContagemResultado_SistemaCod, P007022_n489ContagemResultado_SistemaCod, P007022_A490ContagemResultado_ContratadaCod, P007022_n490ContagemResultado_ContratadaCod, P007022_A457ContagemResultado_Demanda, P007022_n457ContagemResultado_Demanda,
               P007022_A602ContagemResultado_OSVinculada, P007022_n602ContagemResultado_OSVinculada, P007022_A484ContagemResultado_StatusDmn, P007022_n484ContagemResultado_StatusDmn, P007022_A1348ContagemResultado_DataHomologacao, P007022_n1348ContagemResultado_DataHomologacao, P007022_A912ContagemResultado_HoraEntrega, P007022_n912ContagemResultado_HoraEntrega, P007022_A1237ContagemResultado_PrazoMaisDias, P007022_n1237ContagemResultado_PrazoMaisDias,
               P007022_A1227ContagemResultado_PrazoInicialDias, P007022_n1227ContagemResultado_PrazoInicialDias, P007022_A472ContagemResultado_DataEntrega, P007022_n472ContagemResultado_DataEntrega, P007022_A512ContagemResultado_ValorPF, P007022_n512ContagemResultado_ValorPF
               }
               , new Object[] {
               P007023_A456ContagemResultado_Codigo, P007023_A517ContagemResultado_Ultima, P007023_A473ContagemResultado_DataCnt, P007023_A458ContagemResultado_PFBFS, P007023_n458ContagemResultado_PFBFS, P007023_A459ContagemResultado_PFLFS, P007023_n459ContagemResultado_PFLFS, P007023_A462ContagemResultado_Divergencia, P007023_A483ContagemResultado_StatusCnt, P007023_A511ContagemResultado_HoraCnt
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P007027_A2183Contagem_TipoUnidade, P007027_n2183Contagem_TipoUnidade, P007027_A2185Contagem_Metodologia, P007027_n2185Contagem_Metodologia, P007027_A1118Contagem_ContratadaCod, P007027_n1118Contagem_ContratadaCod, P007027_A945Contagem_Demanda, P007027_n945Contagem_Demanda, P007027_A943Contagem_PFB, P007027_n943Contagem_PFB,
               P007027_A944Contagem_PFL, P007027_n944Contagem_PFL, P007027_A947Contagem_Fator, P007027_n947Contagem_Fator, P007027_A1113Contagem_PFBA, P007027_n1113Contagem_PFBA, P007027_A1114Contagem_PFLA, P007027_n1114Contagem_PFLA, P007027_A1117Contagem_Deflator, P007027_n1117Contagem_Deflator,
               P007027_A1115Contagem_PFBD, P007027_n1115Contagem_PFBD, P007027_A1116Contagem_PFLD, P007027_n1116Contagem_PFLD, P007027_A2200Contagem_DataAtualizacao, P007027_n2200Contagem_DataAtualizacao, P007027_A1119Contagem_Divergencia, P007027_n1119Contagem_Divergencia, P007027_A262Contagem_Status, P007027_n262Contagem_Status,
               P007027_A192Contagem_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P007030_A1553ContagemResultado_CntSrvCod, P007030_n1553ContagemResultado_CntSrvCod, P007030_A1452ContagemResultado_SS, P007030_n1452ContagemResultado_SS, P007030_A1348ContagemResultado_DataHomologacao, P007030_n1348ContagemResultado_DataHomologacao, P007030_A1046ContagemResultado_Agrupador, P007030_n1046ContagemResultado_Agrupador, P007030_A912ContagemResultado_HoraEntrega, P007030_n912ContagemResultado_HoraEntrega,
               P007030_A890ContagemResultado_Responsavel, P007030_n890ContagemResultado_Responsavel, P007030_A602ContagemResultado_OSVinculada, P007030_n602ContagemResultado_OSVinculada, P007030_A512ContagemResultado_ValorPF, P007030_n512ContagemResultado_ValorPF, P007030_A490ContagemResultado_ContratadaCod, P007030_n490ContagemResultado_ContratadaCod, P007030_A484ContagemResultado_StatusDmn, P007030_n484ContagemResultado_StatusDmn,
               P007030_A457ContagemResultado_Demanda, P007030_n457ContagemResultado_Demanda, P007030_A472ContagemResultado_DataEntrega, P007030_n472ContagemResultado_DataEntrega, P007030_A2133ContagemResultado_QuantidadeSolicitada, P007030_n2133ContagemResultado_QuantidadeSolicitada, P007030_A2017ContagemResultado_DataEntregaReal, P007030_n2017ContagemResultado_DataEntregaReal, P007030_A1903ContagemResultado_DataPrvPgm, P007030_n1903ContagemResultado_DataPrvPgm,
               P007030_A1855ContagemResultado_PFCnc, P007030_n1855ContagemResultado_PFCnc, P007030_A1854ContagemResultado_VlrCnc, P007030_n1854ContagemResultado_VlrCnc, P007030_A1791ContagemResultado_SemCusto, P007030_n1791ContagemResultado_SemCusto, P007030_A1790ContagemResultado_DataInicio, P007030_n1790ContagemResultado_DataInicio, P007030_A1762ContagemResultado_Entrega, P007030_n1762ContagemResultado_Entrega,
               P007030_A1714ContagemResultado_Combinada, P007030_n1714ContagemResultado_Combinada, P007030_A1636ContagemResultado_ServicoSS, P007030_n1636ContagemResultado_ServicoSS, P007030_A1587ContagemResultado_PrioridadePrevista, P007030_n1587ContagemResultado_PrioridadePrevista, P007030_A1586ContagemResultado_Restricoes, P007030_n1586ContagemResultado_Restricoes, P007030_A1585ContagemResultado_Referencia, P007030_n1585ContagemResultado_Referencia,
               P007030_A1584ContagemResultado_UOOwner, P007030_n1584ContagemResultado_UOOwner, P007030_A1583ContagemResultado_TipoRegistro, P007030_A1559ContagemResultado_VlrAceite, P007030_n1559ContagemResultado_VlrAceite, P007030_A1544ContagemResultado_ProjetoCod, P007030_n1544ContagemResultado_ProjetoCod, P007030_A1521ContagemResultado_FimAnl, P007030_n1521ContagemResultado_FimAnl, P007030_A1520ContagemResultado_InicioAnl,
               P007030_n1520ContagemResultado_InicioAnl, P007030_A1519ContagemResultado_TmpEstAnl, P007030_n1519ContagemResultado_TmpEstAnl, P007030_A1515ContagemResultado_Evento, P007030_n1515ContagemResultado_Evento, P007030_A1512ContagemResultado_FimCrr, P007030_n1512ContagemResultado_FimCrr, P007030_A1511ContagemResultado_InicioCrr, P007030_n1511ContagemResultado_InicioCrr, P007030_A1510ContagemResultado_FimExc,
               P007030_n1510ContagemResultado_FimExc, P007030_A1509ContagemResultado_InicioExc, P007030_n1509ContagemResultado_InicioExc, P007030_A1506ContagemResultado_TmpEstCrr, P007030_n1506ContagemResultado_TmpEstCrr, P007030_A1505ContagemResultado_TmpEstExc, P007030_n1505ContagemResultado_TmpEstExc, P007030_A1457ContagemResultado_TemDpnHmlg, P007030_n1457ContagemResultado_TemDpnHmlg, P007030_A1445ContagemResultado_CntSrvPrrCst,
               P007030_n1445ContagemResultado_CntSrvPrrCst, P007030_A1444ContagemResultado_CntSrvPrrPrz, P007030_n1444ContagemResultado_CntSrvPrrPrz, P007030_A1443ContagemResultado_CntSrvPrrCod, P007030_n1443ContagemResultado_CntSrvPrrCod, P007030_A1392ContagemResultado_RdmnUpdated, P007030_n1392ContagemResultado_RdmnUpdated, P007030_A1390ContagemResultado_RdmnProjectId, P007030_n1390ContagemResultado_RdmnProjectId, P007030_A1389ContagemResultado_RdmnIssueId,
               P007030_n1389ContagemResultado_RdmnIssueId, P007030_A1351ContagemResultado_DataPrevista, P007030_n1351ContagemResultado_DataPrevista, P007030_A1350ContagemResultado_DataCadastro, P007030_n1350ContagemResultado_DataCadastro, P007030_A1349ContagemResultado_DataExecucao, P007030_n1349ContagemResultado_DataExecucao, P007030_A1237ContagemResultado_PrazoMaisDias, P007030_n1237ContagemResultado_PrazoMaisDias, P007030_A1227ContagemResultado_PrazoInicialDias,
               P007030_n1227ContagemResultado_PrazoInicialDias, P007030_A1180ContagemResultado_Custo, P007030_n1180ContagemResultado_Custo, P007030_A1179ContagemResultado_PLFinal, P007030_n1179ContagemResultado_PLFinal, P007030_A1178ContagemResultado_PBFinal, P007030_n1178ContagemResultado_PBFinal, P007030_A1173ContagemResultado_OSManual, P007030_n1173ContagemResultado_OSManual, P007030_A1052ContagemResultado_GlsUser,
               P007030_n1052ContagemResultado_GlsUser, P007030_A1051ContagemResultado_GlsValor, P007030_n1051ContagemResultado_GlsValor, P007030_A1050ContagemResultado_GlsDescricao, P007030_n1050ContagemResultado_GlsDescricao, P007030_A1049ContagemResultado_GlsData, P007030_n1049ContagemResultado_GlsData, P007030_A1044ContagemResultado_FncUsrCod, P007030_n1044ContagemResultado_FncUsrCod, P007030_A1043ContagemResultado_LiqLogCod,
               P007030_n1043ContagemResultado_LiqLogCod, P007030_A805ContagemResultado_ContratadaOrigemCod, P007030_n805ContagemResultado_ContratadaOrigemCod, P007030_A799ContagemResultado_PFLFSImp, P007030_n799ContagemResultado_PFLFSImp, P007030_A798ContagemResultado_PFBFSImp, P007030_n798ContagemResultado_PFBFSImp, P007030_A598ContagemResultado_Baseline, P007030_n598ContagemResultado_Baseline, P007030_A597ContagemResultado_LoteAceiteCod,
               P007030_n597ContagemResultado_LoteAceiteCod, P007030_A592ContagemResultado_Evidencia, P007030_n592ContagemResultado_Evidencia, P007030_A508ContagemResultado_Owner, P007030_A514ContagemResultado_Observacao, P007030_n514ContagemResultado_Observacao, P007030_A146Modulo_Codigo, P007030_n146Modulo_Codigo, P007030_A489ContagemResultado_SistemaCod, P007030_n489ContagemResultado_SistemaCod,
               P007030_A494ContagemResultado_Descricao, P007030_n494ContagemResultado_Descricao, P007030_A493ContagemResultado_DemandaFM, P007030_n493ContagemResultado_DemandaFM, P007030_A485ContagemResultado_EhValidacao, P007030_n485ContagemResultado_EhValidacao, P007030_A468ContagemResultado_NaoCnfDmnCod, P007030_n468ContagemResultado_NaoCnfDmnCod, P007030_A465ContagemResultado_Link, P007030_n465ContagemResultado_Link,
               P007030_A454ContagemResultado_ContadorFSCod, P007030_n454ContagemResultado_ContadorFSCod, P007030_A471ContagemResultado_DataDmn, P007030_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P007031_A456ContagemResultado_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               P007033_A192Contagem_Codigo
               }
               , new Object[] {
               P007034_A2199Contagem_DemandaCod, P007034_n2199Contagem_DemandaCod, P007034_A1119Contagem_Divergencia, P007034_n1119Contagem_Divergencia, P007034_A262Contagem_Status, P007034_n262Contagem_Status, P007034_A192Contagem_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P007038_A456ContagemResultado_Codigo, P007038_A484ContagemResultado_StatusDmn, P007038_n484ContagemResultado_StatusDmn, P007038_A1348ContagemResultado_DataHomologacao, P007038_n1348ContagemResultado_DataHomologacao
               }
               , new Object[] {
               P007039_A456ContagemResultado_Codigo, P007039_A517ContagemResultado_Ultima, P007039_A458ContagemResultado_PFBFS, P007039_n458ContagemResultado_PFBFS, P007039_A459ContagemResultado_PFLFS, P007039_n459ContagemResultado_PFLFS, P007039_A462ContagemResultado_Divergencia, P007039_A483ContagemResultado_StatusCnt, P007039_A473ContagemResultado_DataCnt, P007039_A511ContagemResultado_HoraCnt
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P007043_A192Contagem_Codigo, P007043_A1118Contagem_ContratadaCod, P007043_n1118Contagem_ContratadaCod, P007043_A945Contagem_Demanda, P007043_n945Contagem_Demanda
               }
               , new Object[] {
               P007044_A722Baseline_Codigo
               }
               , new Object[] {
               P007045_A1797LogResponsavel_Codigo, P007045_A896LogResponsavel_Owner, P007045_A892LogResponsavel_DemandaCod, P007045_n892LogResponsavel_DemandaCod
               }
               , new Object[] {
               P007046_A135Sistema_AreaTrabalhoCod, P007046_A129Sistema_Sigla, P007046_A127Sistema_Codigo
               }
               , new Object[] {
               }
            }
         );
         Gx_date = DateTimeUtil.Today( context);
         Gx_time = context.localUtil.Time( );
         /* GeneXus formulas. */
         Gx_line = 0;
         Gx_date = DateTimeUtil.Today( context);
         Gx_time = context.localUtil.Time( );
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short AV22ColDmnn ;
      private short AV76PraLinha ;
      private short AV27ColPFBFSn ;
      private short AV29ColPFLFSn ;
      private short AV26ColPFBFMn ;
      private short AV28ColPFLFMn ;
      private short AV19ColDataCntn ;
      private short AV135ColSistemaN ;
      private short GxWebError ;
      private short AV64Ln ;
      private short A1224ContratoServicos_PrazoCorrecao ;
      private short A1649ContratoServicos_PrazoInicio ;
      private short A1152ContratoServicos_PrazoAnalise ;
      private short OV130PrazoInicio ;
      private short AV130PrazoInicio ;
      private short AV119PrazoCoreecao ;
      private short AV124DiasParaAnalise ;
      private short AV51ErrCod ;
      private short AV148GXLvl125 ;
      private short AV149GXLvl199 ;
      private short GXT709 ;
      private short A482ContagemResultadoContagens_Esforco ;
      private short A483ContagemResultado_StatusCnt ;
      private short A1756ContagemResultado_NvlCnt ;
      private short W482ContagemResultadoContagens_Esforco ;
      private short W483ContagemResultado_StatusCnt ;
      private short AV11Aprovadas ;
      private short AV150GXLvl260 ;
      private short AV40ContagemResultado_StatusCnt ;
      private short AV50Divergencias ;
      private short AV52Erros ;
      private short AV61Lidas ;
      private short AV108Tratadas ;
      private short AV154GXLvl474 ;
      private short A1237ContagemResultado_PrazoMaisDias ;
      private short A1227ContagemResultado_PrazoInicialDias ;
      private short AV122PrazoInicial ;
      private short A1762ContagemResultado_Entrega ;
      private short A1583ContagemResultado_TipoRegistro ;
      private short A1515ContagemResultado_Evento ;
      private int AV95ContratadaFS_Codigo ;
      private int AV97ContadorFS_Codigo ;
      private int AV134ContratoservicosFS_Codigo ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int AV43Contratada_Codigo ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A601ContagemResultado_Servico ;
      private int A456ContagemResultado_Codigo ;
      private int AV100Servico_Codigo ;
      private int A74Contrato_Codigo ;
      private int A39Contratada_Codigo ;
      private int A40Contratada_PessoaCod ;
      private int A160ContratoServicos_Codigo ;
      private int A5AreaTrabalho_Codigo ;
      private int A29Contratante_Codigo ;
      private int A987AreaTrabalho_ContratadaUpdBslCod ;
      private int AV113AreaTrabalho_ContratadaUpdBslCod ;
      private int AV35ContagemResultado_Codigo ;
      private int A805ContagemResultado_ContratadaOrigemCod ;
      private int A489ContagemResultado_SistemaCod ;
      private int AV136Sistema_Codigo ;
      private int A469ContagemResultado_NaoCnfCntCod ;
      private int A470ContagemResultado_ContadorFMCod ;
      private int W456ContagemResultado_Codigo ;
      private int AV31ContadorFMCod ;
      private int GX_INS72 ;
      private int W470ContagemResultado_ContadorFMCod ;
      private int W469ContagemResultado_NaoCnfCntCod ;
      private int A890ContagemResultado_Responsavel ;
      private int AV129ContratoServicos_Codigo ;
      private int AV131Responsavel ;
      private int Gx_OldLine ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A155Servico_Codigo ;
      private int A1013Contrato_PrepostoCod ;
      private int A1078ContratoGestor_ContratoCod ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int A146Modulo_Codigo ;
      private int A127Sistema_Codigo ;
      private int A1399Sistema_ImpUserCod ;
      private int A1073Usuario_CargoCod ;
      private int A1075Usuario_CargoUOCod ;
      private int A137Metodologia_Codigo ;
      private int A609TpUo_Codigo ;
      private int A602ContagemResultado_OSVinculada ;
      private int AV128OSVinculada ;
      private int AV107ContagemResultado_CodigoFS ;
      private int A2183Contagem_TipoUnidade ;
      private int A2185Contagem_Metodologia ;
      private int A1118Contagem_ContratadaCod ;
      private int A192Contagem_Codigo ;
      private int A1452ContagemResultado_SS ;
      private int A1636ContagemResultado_ServicoSS ;
      private int A1584ContagemResultado_UOOwner ;
      private int A1544ContagemResultado_ProjetoCod ;
      private int A1519ContagemResultado_TmpEstAnl ;
      private int A1506ContagemResultado_TmpEstCrr ;
      private int A1505ContagemResultado_TmpEstExc ;
      private int A1443ContagemResultado_CntSrvPrrCod ;
      private int A1390ContagemResultado_RdmnProjectId ;
      private int A1389ContagemResultado_RdmnIssueId ;
      private int A1052ContagemResultado_GlsUser ;
      private int A1044ContagemResultado_FncUsrCod ;
      private int A1043ContagemResultado_LiqLogCod ;
      private int A597ContagemResultado_LoteAceiteCod ;
      private int A508ContagemResultado_Owner ;
      private int A468ContagemResultado_NaoCnfDmnCod ;
      private int A454ContagemResultado_ContadorFSCod ;
      private int GX_INS69 ;
      private int W490ContagemResultado_ContratadaCod ;
      private int W1553ContagemResultado_CntSrvCod ;
      private int W602ContagemResultado_OSVinculada ;
      private int W890ContagemResultado_Responsavel ;
      private int W1452ContagemResultado_SS ;
      private int AV96ContagemFS_Codigo ;
      private int GX_INS43 ;
      private int A193Contagem_AreaTrabalhoCod ;
      private int A940Contagem_SistemaCod ;
      private int A213Contagem_UsuarioContadorCod ;
      private int A2181Contagem_CntSrvCod ;
      private int A2199Contagem_DemandaCod ;
      private int A2184Contagem_UnidadeOrganizacional ;
      private int A2186Contagem_Fases ;
      private int A939Contagem_ProjetoCod ;
      private int GX_INS92 ;
      private int A721Baseline_UserCod ;
      private int A735Baseline_ProjetoMelCod ;
      private int A722Baseline_Codigo ;
      private int A896LogResponsavel_Owner ;
      private int A892LogResponsavel_DemandaCod ;
      private int A135Sistema_AreaTrabalhoCod ;
      private long A1797LogResponsavel_Codigo ;
      private decimal AV101ContagemResltado_Deflator ;
      private decimal A558Servico_Percentual ;
      private decimal A116Contrato_ValorUnidadeContratacao ;
      private decimal AV106ValorPFFS ;
      private decimal AV70PFBFM ;
      private decimal AV74PFLFM ;
      private decimal AV71PFBFS ;
      private decimal AV75PFLFS ;
      private decimal A833ContagemResultado_CstUntPrd ;
      private decimal A462ContagemResultado_Divergencia ;
      private decimal A461ContagemResultado_PFLFM ;
      private decimal A460ContagemResultado_PFBFM ;
      private decimal A459ContagemResultado_PFLFS ;
      private decimal A458ContagemResultado_PFBFS ;
      private decimal A800ContagemResultado_Deflator ;
      private decimal AV38ContagemResultado_CstUntPrd ;
      private decimal W833ContagemResultado_CstUntPrd ;
      private decimal W458ContagemResultado_PFBFS ;
      private decimal W459ContagemResultado_PFLFS ;
      private decimal W460ContagemResultado_PFBFM ;
      private decimal W461ContagemResultado_PFLFM ;
      private decimal W462ContagemResultado_Divergencia ;
      private decimal A799ContagemResultado_PFLFSImp ;
      private decimal A798ContagemResultado_PFBFSImp ;
      private decimal AV72PFFM ;
      private decimal AV73PFFS ;
      private decimal AV8ContagemResultado_PFLFM ;
      private decimal AV9ContagemResultado_PFLFS ;
      private decimal AV39ContagemResultado_Divergencia ;
      private decimal AV59IndiceDivergencia ;
      private decimal AV42ContagemResultado_ValorPF ;
      private decimal GXt_decimal1 ;
      private decimal A512ContagemResultado_ValorPF ;
      private decimal A943Contagem_PFB ;
      private decimal A944Contagem_PFL ;
      private decimal A947Contagem_Fator ;
      private decimal A1113Contagem_PFBA ;
      private decimal A1114Contagem_PFLA ;
      private decimal A1117Contagem_Deflator ;
      private decimal A1115Contagem_PFBD ;
      private decimal A1116Contagem_PFLD ;
      private decimal A1119Contagem_Divergencia ;
      private decimal A2133ContagemResultado_QuantidadeSolicitada ;
      private decimal A1855ContagemResultado_PFCnc ;
      private decimal A1854ContagemResultado_VlrCnc ;
      private decimal A1559ContagemResultado_VlrAceite ;
      private decimal A1445ContagemResultado_CntSrvPrrCst ;
      private decimal A1444ContagemResultado_CntSrvPrrPrz ;
      private decimal A1180ContagemResultado_Custo ;
      private decimal A1179ContagemResultado_PLFinal ;
      private decimal A1178ContagemResultado_PBFinal ;
      private decimal A1051ContagemResultado_GlsValor ;
      private decimal W512ContagemResultado_ValorPF ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String AV13Arquivo ;
      private String AV10Aba ;
      private String AV104RegraDivergencia ;
      private String AV114FileName ;
      private String AV12ArqTitulo ;
      private String scmdbuf ;
      private String A1046ContagemResultado_Agrupador ;
      private String AV90Agrupador ;
      private String A41Contratada_PessoaNom ;
      private String A1225ContratoServicos_PrazoCorrecaoTipo ;
      private String AV44ContratadaFS_Nome ;
      private String AV120PrazoCorrecaoTipo ;
      private String AV86EmailTextD ;
      private String AV88EmailTextC ;
      private String AV93StatusFinal ;
      private String AV94TxtStatusFinal ;
      private String AV62Linha ;
      private String AV78Sistema_Sigla ;
      private String A484ContagemResultado_StatusDmn ;
      private String AV109ContagemResultado_StatusDmn ;
      private String A511ContagemResultado_HoraCnt ;
      private String A854ContagemResultado_TipoPla ;
      private String A852ContagemResultado_Planilha_Filetype ;
      private String A853ContagemResultado_NomePla ;
      private String A852ContagemResultado_Planilha_Filename ;
      private String W511ContagemResultado_HoraCnt ;
      private String Gx_emsg ;
      private String AV132StatusAnt ;
      private String AV79Totais ;
      private String AV87Subject ;
      private String AV91Resultado ;
      private String AV15CalculoDivergencia ;
      private String A262Contagem_Status ;
      private String W484ContagemResultado_StatusDmn ;
      private String W1046ContagemResultado_Agrupador ;
      private String AV133Status ;
      private String A195Contagem_Tecnica ;
      private String A196Contagem_Tipo ;
      private String A129Sistema_Sigla ;
      private String Gx_time ;
      private DateTime A1192Contratante_FimDoExpediente ;
      private DateTime AV125FimDoExpediente ;
      private DateTime AV126ServerNow ;
      private DateTime A912ContagemResultado_HoraEntrega ;
      private DateTime A1351ContagemResultado_DataPrevista ;
      private DateTime AV118HoraEntrega ;
      private DateTime A901ContagemResultadoContagens_Prazo ;
      private DateTime A481ContagemResultado_TimeCnt ;
      private DateTime A1348ContagemResultado_DataHomologacao ;
      private DateTime AV121PrazoEntrega ;
      private DateTime A2200Contagem_DataAtualizacao ;
      private DateTime A2017ContagemResultado_DataEntregaReal ;
      private DateTime A1521ContagemResultado_FimAnl ;
      private DateTime A1520ContagemResultado_InicioAnl ;
      private DateTime A1512ContagemResultado_FimCrr ;
      private DateTime A1511ContagemResultado_InicioCrr ;
      private DateTime A1510ContagemResultado_FimExc ;
      private DateTime A1509ContagemResultado_InicioExc ;
      private DateTime A1392ContagemResultado_RdmnUpdated ;
      private DateTime A1350ContagemResultado_DataCadastro ;
      private DateTime A1349ContagemResultado_DataExecucao ;
      private DateTime W1348ContagemResultado_DataHomologacao ;
      private DateTime W912ContagemResultado_HoraEntrega ;
      private DateTime A1164Baseline_DataHomologacao ;
      private DateTime AV99DataCnt ;
      private DateTime AV115DataDmn ;
      private DateTime AV116DataEntrega ;
      private DateTime AV102ContagemResultado_DataCnt ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private DateTime A473ContagemResultado_DataCnt ;
      private DateTime W473ContagemResultado_DataCnt ;
      private DateTime A1903ContagemResultado_DataPrvPgm ;
      private DateTime A1790ContagemResultado_DataInicio ;
      private DateTime A1049ContagemResultado_GlsData ;
      private DateTime W472ContagemResultado_DataEntrega ;
      private DateTime A197Contagem_DataCriacao ;
      private DateTime Gx_date ;
      private bool entryPointCalled ;
      private bool AV54Final ;
      private bool returnInSub ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n457ContagemResultado_Demanda ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n1046ContagemResultado_Agrupador ;
      private bool n41Contratada_PessoaNom ;
      private bool n558Servico_Percentual ;
      private bool n1224ContratoServicos_PrazoCorrecao ;
      private bool n1225ContratoServicos_PrazoCorrecaoTipo ;
      private bool n1649ContratoServicos_PrazoInicio ;
      private bool n1152ContratoServicos_PrazoAnalise ;
      private bool n29Contratante_Codigo ;
      private bool n1192Contratante_FimDoExpediente ;
      private bool n987AreaTrabalho_ContratadaUpdBslCod ;
      private bool n805ContagemResultado_ContratadaOrigemCod ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool n912ContagemResultado_HoraEntrega ;
      private bool n1351ContagemResultado_DataPrevista ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n833ContagemResultado_CstUntPrd ;
      private bool A517ContagemResultado_Ultima ;
      private bool n469ContagemResultado_NaoCnfCntCod ;
      private bool n461ContagemResultado_PFLFM ;
      private bool n460ContagemResultado_PFBFM ;
      private bool n459ContagemResultado_PFLFS ;
      private bool n458ContagemResultado_PFBFS ;
      private bool n1756ContagemResultado_NvlCnt ;
      private bool n901ContagemResultadoContagens_Prazo ;
      private bool n854ContagemResultado_TipoPla ;
      private bool n853ContagemResultado_NomePla ;
      private bool n800ContagemResultado_Deflator ;
      private bool n463ContagemResultado_ParecerTcn ;
      private bool n481ContagemResultado_TimeCnt ;
      private bool n1348ContagemResultado_DataHomologacao ;
      private bool n852ContagemResultado_Planilha ;
      private bool W517ContagemResultado_Ultima ;
      private bool n799ContagemResultado_PFLFSImp ;
      private bool n798ContagemResultado_PFBFSImp ;
      private bool n890ContagemResultado_Responsavel ;
      private bool A293Usuario_EhFinanceiro ;
      private bool n293Usuario_EhFinanceiro ;
      private bool A291Usuario_EhContratada ;
      private bool n291Usuario_EhContratada ;
      private bool n1013Contrato_PrepostoCod ;
      private bool AV105Calcular ;
      private bool n146Modulo_Codigo ;
      private bool n1399Sistema_ImpUserCod ;
      private bool n1073Usuario_CargoCod ;
      private bool n1075Usuario_CargoUOCod ;
      private bool n137Metodologia_Codigo ;
      private bool n609TpUo_Codigo ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n1237ContagemResultado_PrazoMaisDias ;
      private bool n1227ContagemResultado_PrazoInicialDias ;
      private bool n512ContagemResultado_ValorPF ;
      private bool AV55Flag ;
      private bool n2183Contagem_TipoUnidade ;
      private bool n2185Contagem_Metodologia ;
      private bool n1118Contagem_ContratadaCod ;
      private bool n945Contagem_Demanda ;
      private bool n943Contagem_PFB ;
      private bool n944Contagem_PFL ;
      private bool n947Contagem_Fator ;
      private bool n1113Contagem_PFBA ;
      private bool n1114Contagem_PFLA ;
      private bool n1117Contagem_Deflator ;
      private bool n1115Contagem_PFBD ;
      private bool n1116Contagem_PFLD ;
      private bool n2200Contagem_DataAtualizacao ;
      private bool n1119Contagem_Divergencia ;
      private bool n262Contagem_Status ;
      private bool n1452ContagemResultado_SS ;
      private bool n2133ContagemResultado_QuantidadeSolicitada ;
      private bool n2017ContagemResultado_DataEntregaReal ;
      private bool n1903ContagemResultado_DataPrvPgm ;
      private bool n1855ContagemResultado_PFCnc ;
      private bool n1854ContagemResultado_VlrCnc ;
      private bool A1791ContagemResultado_SemCusto ;
      private bool n1791ContagemResultado_SemCusto ;
      private bool n1790ContagemResultado_DataInicio ;
      private bool n1762ContagemResultado_Entrega ;
      private bool A1714ContagemResultado_Combinada ;
      private bool n1714ContagemResultado_Combinada ;
      private bool n1636ContagemResultado_ServicoSS ;
      private bool n1587ContagemResultado_PrioridadePrevista ;
      private bool n1586ContagemResultado_Restricoes ;
      private bool n1585ContagemResultado_Referencia ;
      private bool n1584ContagemResultado_UOOwner ;
      private bool n1559ContagemResultado_VlrAceite ;
      private bool n1544ContagemResultado_ProjetoCod ;
      private bool n1521ContagemResultado_FimAnl ;
      private bool n1520ContagemResultado_InicioAnl ;
      private bool n1519ContagemResultado_TmpEstAnl ;
      private bool n1515ContagemResultado_Evento ;
      private bool n1512ContagemResultado_FimCrr ;
      private bool n1511ContagemResultado_InicioCrr ;
      private bool n1510ContagemResultado_FimExc ;
      private bool n1509ContagemResultado_InicioExc ;
      private bool n1506ContagemResultado_TmpEstCrr ;
      private bool n1505ContagemResultado_TmpEstExc ;
      private bool A1457ContagemResultado_TemDpnHmlg ;
      private bool n1457ContagemResultado_TemDpnHmlg ;
      private bool n1445ContagemResultado_CntSrvPrrCst ;
      private bool n1444ContagemResultado_CntSrvPrrPrz ;
      private bool n1443ContagemResultado_CntSrvPrrCod ;
      private bool n1392ContagemResultado_RdmnUpdated ;
      private bool n1390ContagemResultado_RdmnProjectId ;
      private bool n1389ContagemResultado_RdmnIssueId ;
      private bool n1350ContagemResultado_DataCadastro ;
      private bool n1349ContagemResultado_DataExecucao ;
      private bool n1180ContagemResultado_Custo ;
      private bool n1179ContagemResultado_PLFinal ;
      private bool n1178ContagemResultado_PBFinal ;
      private bool A1173ContagemResultado_OSManual ;
      private bool n1173ContagemResultado_OSManual ;
      private bool n1052ContagemResultado_GlsUser ;
      private bool n1051ContagemResultado_GlsValor ;
      private bool n1050ContagemResultado_GlsDescricao ;
      private bool n1049ContagemResultado_GlsData ;
      private bool n1044ContagemResultado_FncUsrCod ;
      private bool n1043ContagemResultado_LiqLogCod ;
      private bool A598ContagemResultado_Baseline ;
      private bool n598ContagemResultado_Baseline ;
      private bool n597ContagemResultado_LoteAceiteCod ;
      private bool n592ContagemResultado_Evidencia ;
      private bool n514ContagemResultado_Observacao ;
      private bool n494ContagemResultado_Descricao ;
      private bool A485ContagemResultado_EhValidacao ;
      private bool n485ContagemResultado_EhValidacao ;
      private bool n468ContagemResultado_NaoCnfDmnCod ;
      private bool n465ContagemResultado_Link ;
      private bool n454ContagemResultado_ContadorFSCod ;
      private bool n195Contagem_Tecnica ;
      private bool n196Contagem_Tipo ;
      private bool n2182Contagem_Pendencia ;
      private bool n940Contagem_SistemaCod ;
      private bool n213Contagem_UsuarioContadorCod ;
      private bool n2181Contagem_CntSrvCod ;
      private bool n2199Contagem_DemandaCod ;
      private bool n2187Contagem_Agrupador ;
      private bool n2184Contagem_UnidadeOrganizacional ;
      private bool n2186Contagem_Fases ;
      private bool n939Contagem_ProjetoCod ;
      private bool n735Baseline_ProjetoMelCod ;
      private bool n892LogResponsavel_DemandaCod ;
      private bool A1149LogResponsavel_OwnerEhContratante ;
      private bool GXt_boolean2 ;
      private String A463ContagemResultado_ParecerTcn ;
      private String A1050ContagemResultado_GlsDescricao ;
      private String A592ContagemResultado_Evidencia ;
      private String A514ContagemResultado_Observacao ;
      private String A465ContagemResultado_Link ;
      private String AV117DemandaFM ;
      private String AV46Demanda ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String A945Contagem_Demanda ;
      private String A1587ContagemResultado_PrioridadePrevista ;
      private String A1586ContagemResultado_Restricoes ;
      private String A1585ContagemResultado_Referencia ;
      private String A494ContagemResultado_Descricao ;
      private String W457ContagemResultado_Demanda ;
      private String A2182Contagem_Pendencia ;
      private String A2187Contagem_Agrupador ;
      private String A852ContagemResultado_Planilha ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP19_ContratoservicosFS_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00702_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00702_n1553ContagemResultado_CntSrvCod ;
      private int[] P00702_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00702_n52Contratada_AreaTrabalhoCod ;
      private String[] P00702_A457ContagemResultado_Demanda ;
      private bool[] P00702_n457ContagemResultado_Demanda ;
      private int[] P00702_A490ContagemResultado_ContratadaCod ;
      private bool[] P00702_n490ContagemResultado_ContratadaCod ;
      private int[] P00702_A601ContagemResultado_Servico ;
      private bool[] P00702_n601ContagemResultado_Servico ;
      private String[] P00702_A1046ContagemResultado_Agrupador ;
      private bool[] P00702_n1046ContagemResultado_Agrupador ;
      private int[] P00702_A456ContagemResultado_Codigo ;
      private int[] P00703_A74Contrato_Codigo ;
      private int[] P00703_A39Contratada_Codigo ;
      private int[] P00703_A40Contratada_PessoaCod ;
      private int[] P00703_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00703_n52Contratada_AreaTrabalhoCod ;
      private int[] P00703_A160ContratoServicos_Codigo ;
      private String[] P00703_A41Contratada_PessoaNom ;
      private bool[] P00703_n41Contratada_PessoaNom ;
      private decimal[] P00703_A558Servico_Percentual ;
      private bool[] P00703_n558Servico_Percentual ;
      private decimal[] P00703_A116Contrato_ValorUnidadeContratacao ;
      private short[] P00703_A1224ContratoServicos_PrazoCorrecao ;
      private bool[] P00703_n1224ContratoServicos_PrazoCorrecao ;
      private String[] P00703_A1225ContratoServicos_PrazoCorrecaoTipo ;
      private bool[] P00703_n1225ContratoServicos_PrazoCorrecaoTipo ;
      private short[] P00703_A1649ContratoServicos_PrazoInicio ;
      private bool[] P00703_n1649ContratoServicos_PrazoInicio ;
      private short[] P00703_A1152ContratoServicos_PrazoAnalise ;
      private bool[] P00703_n1152ContratoServicos_PrazoAnalise ;
      private int[] P00704_A5AreaTrabalho_Codigo ;
      private int[] P00704_A29Contratante_Codigo ;
      private bool[] P00704_n29Contratante_Codigo ;
      private DateTime[] P00704_A1192Contratante_FimDoExpediente ;
      private bool[] P00704_n1192Contratante_FimDoExpediente ;
      private int[] P00704_A987AreaTrabalho_ContratadaUpdBslCod ;
      private bool[] P00704_n987AreaTrabalho_ContratadaUpdBslCod ;
      private int[] P00705_A29Contratante_Codigo ;
      private bool[] P00705_n29Contratante_Codigo ;
      private String[] P00706_A457ContagemResultado_Demanda ;
      private bool[] P00706_n457ContagemResultado_Demanda ;
      private int[] P00706_A490ContagemResultado_ContratadaCod ;
      private bool[] P00706_n490ContagemResultado_ContratadaCod ;
      private int[] P00706_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] P00706_n805ContagemResultado_ContratadaOrigemCod ;
      private int[] P00706_A489ContagemResultado_SistemaCod ;
      private bool[] P00706_n489ContagemResultado_SistemaCod ;
      private String[] P00706_A493ContagemResultado_DemandaFM ;
      private bool[] P00706_n493ContagemResultado_DemandaFM ;
      private int[] P00706_A456ContagemResultado_Codigo ;
      private DateTime[] P00706_A471ContagemResultado_DataDmn ;
      private DateTime[] P00706_A472ContagemResultado_DataEntrega ;
      private bool[] P00706_n472ContagemResultado_DataEntrega ;
      private DateTime[] P00706_A912ContagemResultado_HoraEntrega ;
      private bool[] P00706_n912ContagemResultado_HoraEntrega ;
      private DateTime[] P00706_A1351ContagemResultado_DataPrevista ;
      private bool[] P00706_n1351ContagemResultado_DataPrevista ;
      private String[] P00706_A484ContagemResultado_StatusDmn ;
      private bool[] P00706_n484ContagemResultado_StatusDmn ;
      private decimal[] P00708_A833ContagemResultado_CstUntPrd ;
      private bool[] P00708_n833ContagemResultado_CstUntPrd ;
      private bool[] P00708_A517ContagemResultado_Ultima ;
      private short[] P00708_A482ContagemResultadoContagens_Esforco ;
      private short[] P00708_A483ContagemResultado_StatusCnt ;
      private int[] P00708_A469ContagemResultado_NaoCnfCntCod ;
      private bool[] P00708_n469ContagemResultado_NaoCnfCntCod ;
      private int[] P00708_A470ContagemResultado_ContadorFMCod ;
      private decimal[] P00708_A462ContagemResultado_Divergencia ;
      private decimal[] P00708_A461ContagemResultado_PFLFM ;
      private bool[] P00708_n461ContagemResultado_PFLFM ;
      private decimal[] P00708_A460ContagemResultado_PFBFM ;
      private bool[] P00708_n460ContagemResultado_PFBFM ;
      private decimal[] P00708_A459ContagemResultado_PFLFS ;
      private bool[] P00708_n459ContagemResultado_PFLFS ;
      private decimal[] P00708_A458ContagemResultado_PFBFS ;
      private bool[] P00708_n458ContagemResultado_PFBFS ;
      private String[] P00708_A511ContagemResultado_HoraCnt ;
      private DateTime[] P00708_A473ContagemResultado_DataCnt ;
      private int[] P00708_A456ContagemResultado_Codigo ;
      private short[] P00708_A1756ContagemResultado_NvlCnt ;
      private bool[] P00708_n1756ContagemResultado_NvlCnt ;
      private DateTime[] P00708_A901ContagemResultadoContagens_Prazo ;
      private bool[] P00708_n901ContagemResultadoContagens_Prazo ;
      private String[] P00708_A854ContagemResultado_TipoPla ;
      private bool[] P00708_n854ContagemResultado_TipoPla ;
      private String[] P00708_A853ContagemResultado_NomePla ;
      private bool[] P00708_n853ContagemResultado_NomePla ;
      private decimal[] P00708_A800ContagemResultado_Deflator ;
      private bool[] P00708_n800ContagemResultado_Deflator ;
      private String[] P00708_A463ContagemResultado_ParecerTcn ;
      private bool[] P00708_n463ContagemResultado_ParecerTcn ;
      private DateTime[] P00708_A481ContagemResultado_TimeCnt ;
      private bool[] P00708_n481ContagemResultado_TimeCnt ;
      private String[] P00708_A484ContagemResultado_StatusDmn ;
      private bool[] P00708_n484ContagemResultado_StatusDmn ;
      private DateTime[] P00708_A1348ContagemResultado_DataHomologacao ;
      private bool[] P00708_n1348ContagemResultado_DataHomologacao ;
      private String[] P00708_A852ContagemResultado_Planilha ;
      private bool[] P00708_n852ContagemResultado_Planilha ;
      private bool[] P007014_A517ContagemResultado_Ultima ;
      private int[] P007014_A456ContagemResultado_Codigo ;
      private String[] P007014_A484ContagemResultado_StatusDmn ;
      private bool[] P007014_n484ContagemResultado_StatusDmn ;
      private decimal[] P007014_A799ContagemResultado_PFLFSImp ;
      private bool[] P007014_n799ContagemResultado_PFLFSImp ;
      private decimal[] P007014_A798ContagemResultado_PFBFSImp ;
      private bool[] P007014_n798ContagemResultado_PFBFSImp ;
      private decimal[] P007014_A459ContagemResultado_PFLFS ;
      private bool[] P007014_n459ContagemResultado_PFLFS ;
      private decimal[] P007014_A458ContagemResultado_PFBFS ;
      private bool[] P007014_n458ContagemResultado_PFBFS ;
      private decimal[] P007014_A460ContagemResultado_PFBFM ;
      private bool[] P007014_n460ContagemResultado_PFBFM ;
      private decimal[] P007014_A461ContagemResultado_PFLFM ;
      private bool[] P007014_n461ContagemResultado_PFLFM ;
      private int[] P007014_A1553ContagemResultado_CntSrvCod ;
      private bool[] P007014_n1553ContagemResultado_CntSrvCod ;
      private short[] P007014_A483ContagemResultado_StatusCnt ;
      private decimal[] P007014_A462ContagemResultado_Divergencia ;
      private DateTime[] P007014_A1348ContagemResultado_DataHomologacao ;
      private bool[] P007014_n1348ContagemResultado_DataHomologacao ;
      private int[] P007014_A890ContagemResultado_Responsavel ;
      private bool[] P007014_n890ContagemResultado_Responsavel ;
      private DateTime[] P007014_A1351ContagemResultado_DataPrevista ;
      private bool[] P007014_n1351ContagemResultado_DataPrevista ;
      private DateTime[] P007014_A473ContagemResultado_DataCnt ;
      private String[] P007014_A511ContagemResultado_HoraCnt ;
      private bool[] P007019_A293Usuario_EhFinanceiro ;
      private bool[] P007019_n293Usuario_EhFinanceiro ;
      private int[] P007019_A66ContratadaUsuario_ContratadaCod ;
      private int[] P007019_A69ContratadaUsuario_UsuarioCod ;
      private bool[] P007020_A291Usuario_EhContratada ;
      private bool[] P007020_n291Usuario_EhContratada ;
      private int[] P007020_A74Contrato_Codigo ;
      private int[] P007020_A155Servico_Codigo ;
      private int[] P007020_A39Contratada_Codigo ;
      private int[] P007020_A1013Contrato_PrepostoCod ;
      private bool[] P007020_n1013Contrato_PrepostoCod ;
      private int[] P007020_A160ContratoServicos_Codigo ;
      private int[] P007021_A1078ContratoGestor_ContratoCod ;
      private int[] P007021_A1079ContratoGestor_UsuarioCod ;
      private int[] P007022_A146Modulo_Codigo ;
      private bool[] P007022_n146Modulo_Codigo ;
      private int[] P007022_A127Sistema_Codigo ;
      private int[] P007022_A1399Sistema_ImpUserCod ;
      private bool[] P007022_n1399Sistema_ImpUserCod ;
      private int[] P007022_A1073Usuario_CargoCod ;
      private bool[] P007022_n1073Usuario_CargoCod ;
      private int[] P007022_A1075Usuario_CargoUOCod ;
      private bool[] P007022_n1075Usuario_CargoUOCod ;
      private int[] P007022_A137Metodologia_Codigo ;
      private bool[] P007022_n137Metodologia_Codigo ;
      private int[] P007022_A609TpUo_Codigo ;
      private bool[] P007022_n609TpUo_Codigo ;
      private int[] P007022_A456ContagemResultado_Codigo ;
      private int[] P007022_A489ContagemResultado_SistemaCod ;
      private bool[] P007022_n489ContagemResultado_SistemaCod ;
      private int[] P007022_A490ContagemResultado_ContratadaCod ;
      private bool[] P007022_n490ContagemResultado_ContratadaCod ;
      private String[] P007022_A457ContagemResultado_Demanda ;
      private bool[] P007022_n457ContagemResultado_Demanda ;
      private int[] P007022_A602ContagemResultado_OSVinculada ;
      private bool[] P007022_n602ContagemResultado_OSVinculada ;
      private String[] P007022_A484ContagemResultado_StatusDmn ;
      private bool[] P007022_n484ContagemResultado_StatusDmn ;
      private DateTime[] P007022_A1348ContagemResultado_DataHomologacao ;
      private bool[] P007022_n1348ContagemResultado_DataHomologacao ;
      private DateTime[] P007022_A912ContagemResultado_HoraEntrega ;
      private bool[] P007022_n912ContagemResultado_HoraEntrega ;
      private short[] P007022_A1237ContagemResultado_PrazoMaisDias ;
      private bool[] P007022_n1237ContagemResultado_PrazoMaisDias ;
      private short[] P007022_A1227ContagemResultado_PrazoInicialDias ;
      private bool[] P007022_n1227ContagemResultado_PrazoInicialDias ;
      private DateTime[] P007022_A472ContagemResultado_DataEntrega ;
      private bool[] P007022_n472ContagemResultado_DataEntrega ;
      private decimal[] P007022_A512ContagemResultado_ValorPF ;
      private bool[] P007022_n512ContagemResultado_ValorPF ;
      private int[] P007023_A456ContagemResultado_Codigo ;
      private bool[] P007023_A517ContagemResultado_Ultima ;
      private DateTime[] P007023_A473ContagemResultado_DataCnt ;
      private decimal[] P007023_A458ContagemResultado_PFBFS ;
      private bool[] P007023_n458ContagemResultado_PFBFS ;
      private decimal[] P007023_A459ContagemResultado_PFLFS ;
      private bool[] P007023_n459ContagemResultado_PFLFS ;
      private decimal[] P007023_A462ContagemResultado_Divergencia ;
      private short[] P007023_A483ContagemResultado_StatusCnt ;
      private String[] P007023_A511ContagemResultado_HoraCnt ;
      private int[] P007027_A2183Contagem_TipoUnidade ;
      private bool[] P007027_n2183Contagem_TipoUnidade ;
      private int[] P007027_A2185Contagem_Metodologia ;
      private bool[] P007027_n2185Contagem_Metodologia ;
      private int[] P007027_A1118Contagem_ContratadaCod ;
      private bool[] P007027_n1118Contagem_ContratadaCod ;
      private String[] P007027_A945Contagem_Demanda ;
      private bool[] P007027_n945Contagem_Demanda ;
      private decimal[] P007027_A943Contagem_PFB ;
      private bool[] P007027_n943Contagem_PFB ;
      private decimal[] P007027_A944Contagem_PFL ;
      private bool[] P007027_n944Contagem_PFL ;
      private decimal[] P007027_A947Contagem_Fator ;
      private bool[] P007027_n947Contagem_Fator ;
      private decimal[] P007027_A1113Contagem_PFBA ;
      private bool[] P007027_n1113Contagem_PFBA ;
      private decimal[] P007027_A1114Contagem_PFLA ;
      private bool[] P007027_n1114Contagem_PFLA ;
      private decimal[] P007027_A1117Contagem_Deflator ;
      private bool[] P007027_n1117Contagem_Deflator ;
      private decimal[] P007027_A1115Contagem_PFBD ;
      private bool[] P007027_n1115Contagem_PFBD ;
      private decimal[] P007027_A1116Contagem_PFLD ;
      private bool[] P007027_n1116Contagem_PFLD ;
      private DateTime[] P007027_A2200Contagem_DataAtualizacao ;
      private bool[] P007027_n2200Contagem_DataAtualizacao ;
      private decimal[] P007027_A1119Contagem_Divergencia ;
      private bool[] P007027_n1119Contagem_Divergencia ;
      private String[] P007027_A262Contagem_Status ;
      private bool[] P007027_n262Contagem_Status ;
      private int[] P007027_A192Contagem_Codigo ;
      private int[] P007030_A1553ContagemResultado_CntSrvCod ;
      private bool[] P007030_n1553ContagemResultado_CntSrvCod ;
      private int[] P007030_A1452ContagemResultado_SS ;
      private bool[] P007030_n1452ContagemResultado_SS ;
      private DateTime[] P007030_A1348ContagemResultado_DataHomologacao ;
      private bool[] P007030_n1348ContagemResultado_DataHomologacao ;
      private String[] P007030_A1046ContagemResultado_Agrupador ;
      private bool[] P007030_n1046ContagemResultado_Agrupador ;
      private DateTime[] P007030_A912ContagemResultado_HoraEntrega ;
      private bool[] P007030_n912ContagemResultado_HoraEntrega ;
      private int[] P007030_A890ContagemResultado_Responsavel ;
      private bool[] P007030_n890ContagemResultado_Responsavel ;
      private int[] P007030_A602ContagemResultado_OSVinculada ;
      private bool[] P007030_n602ContagemResultado_OSVinculada ;
      private decimal[] P007030_A512ContagemResultado_ValorPF ;
      private bool[] P007030_n512ContagemResultado_ValorPF ;
      private int[] P007030_A490ContagemResultado_ContratadaCod ;
      private bool[] P007030_n490ContagemResultado_ContratadaCod ;
      private String[] P007030_A484ContagemResultado_StatusDmn ;
      private bool[] P007030_n484ContagemResultado_StatusDmn ;
      private String[] P007030_A457ContagemResultado_Demanda ;
      private bool[] P007030_n457ContagemResultado_Demanda ;
      private DateTime[] P007030_A472ContagemResultado_DataEntrega ;
      private bool[] P007030_n472ContagemResultado_DataEntrega ;
      private decimal[] P007030_A2133ContagemResultado_QuantidadeSolicitada ;
      private bool[] P007030_n2133ContagemResultado_QuantidadeSolicitada ;
      private DateTime[] P007030_A2017ContagemResultado_DataEntregaReal ;
      private bool[] P007030_n2017ContagemResultado_DataEntregaReal ;
      private DateTime[] P007030_A1903ContagemResultado_DataPrvPgm ;
      private bool[] P007030_n1903ContagemResultado_DataPrvPgm ;
      private decimal[] P007030_A1855ContagemResultado_PFCnc ;
      private bool[] P007030_n1855ContagemResultado_PFCnc ;
      private decimal[] P007030_A1854ContagemResultado_VlrCnc ;
      private bool[] P007030_n1854ContagemResultado_VlrCnc ;
      private bool[] P007030_A1791ContagemResultado_SemCusto ;
      private bool[] P007030_n1791ContagemResultado_SemCusto ;
      private DateTime[] P007030_A1790ContagemResultado_DataInicio ;
      private bool[] P007030_n1790ContagemResultado_DataInicio ;
      private short[] P007030_A1762ContagemResultado_Entrega ;
      private bool[] P007030_n1762ContagemResultado_Entrega ;
      private bool[] P007030_A1714ContagemResultado_Combinada ;
      private bool[] P007030_n1714ContagemResultado_Combinada ;
      private int[] P007030_A1636ContagemResultado_ServicoSS ;
      private bool[] P007030_n1636ContagemResultado_ServicoSS ;
      private String[] P007030_A1587ContagemResultado_PrioridadePrevista ;
      private bool[] P007030_n1587ContagemResultado_PrioridadePrevista ;
      private String[] P007030_A1586ContagemResultado_Restricoes ;
      private bool[] P007030_n1586ContagemResultado_Restricoes ;
      private String[] P007030_A1585ContagemResultado_Referencia ;
      private bool[] P007030_n1585ContagemResultado_Referencia ;
      private int[] P007030_A1584ContagemResultado_UOOwner ;
      private bool[] P007030_n1584ContagemResultado_UOOwner ;
      private short[] P007030_A1583ContagemResultado_TipoRegistro ;
      private decimal[] P007030_A1559ContagemResultado_VlrAceite ;
      private bool[] P007030_n1559ContagemResultado_VlrAceite ;
      private int[] P007030_A1544ContagemResultado_ProjetoCod ;
      private bool[] P007030_n1544ContagemResultado_ProjetoCod ;
      private DateTime[] P007030_A1521ContagemResultado_FimAnl ;
      private bool[] P007030_n1521ContagemResultado_FimAnl ;
      private DateTime[] P007030_A1520ContagemResultado_InicioAnl ;
      private bool[] P007030_n1520ContagemResultado_InicioAnl ;
      private int[] P007030_A1519ContagemResultado_TmpEstAnl ;
      private bool[] P007030_n1519ContagemResultado_TmpEstAnl ;
      private short[] P007030_A1515ContagemResultado_Evento ;
      private bool[] P007030_n1515ContagemResultado_Evento ;
      private DateTime[] P007030_A1512ContagemResultado_FimCrr ;
      private bool[] P007030_n1512ContagemResultado_FimCrr ;
      private DateTime[] P007030_A1511ContagemResultado_InicioCrr ;
      private bool[] P007030_n1511ContagemResultado_InicioCrr ;
      private DateTime[] P007030_A1510ContagemResultado_FimExc ;
      private bool[] P007030_n1510ContagemResultado_FimExc ;
      private DateTime[] P007030_A1509ContagemResultado_InicioExc ;
      private bool[] P007030_n1509ContagemResultado_InicioExc ;
      private int[] P007030_A1506ContagemResultado_TmpEstCrr ;
      private bool[] P007030_n1506ContagemResultado_TmpEstCrr ;
      private int[] P007030_A1505ContagemResultado_TmpEstExc ;
      private bool[] P007030_n1505ContagemResultado_TmpEstExc ;
      private bool[] P007030_A1457ContagemResultado_TemDpnHmlg ;
      private bool[] P007030_n1457ContagemResultado_TemDpnHmlg ;
      private decimal[] P007030_A1445ContagemResultado_CntSrvPrrCst ;
      private bool[] P007030_n1445ContagemResultado_CntSrvPrrCst ;
      private decimal[] P007030_A1444ContagemResultado_CntSrvPrrPrz ;
      private bool[] P007030_n1444ContagemResultado_CntSrvPrrPrz ;
      private int[] P007030_A1443ContagemResultado_CntSrvPrrCod ;
      private bool[] P007030_n1443ContagemResultado_CntSrvPrrCod ;
      private DateTime[] P007030_A1392ContagemResultado_RdmnUpdated ;
      private bool[] P007030_n1392ContagemResultado_RdmnUpdated ;
      private int[] P007030_A1390ContagemResultado_RdmnProjectId ;
      private bool[] P007030_n1390ContagemResultado_RdmnProjectId ;
      private int[] P007030_A1389ContagemResultado_RdmnIssueId ;
      private bool[] P007030_n1389ContagemResultado_RdmnIssueId ;
      private DateTime[] P007030_A1351ContagemResultado_DataPrevista ;
      private bool[] P007030_n1351ContagemResultado_DataPrevista ;
      private DateTime[] P007030_A1350ContagemResultado_DataCadastro ;
      private bool[] P007030_n1350ContagemResultado_DataCadastro ;
      private DateTime[] P007030_A1349ContagemResultado_DataExecucao ;
      private bool[] P007030_n1349ContagemResultado_DataExecucao ;
      private short[] P007030_A1237ContagemResultado_PrazoMaisDias ;
      private bool[] P007030_n1237ContagemResultado_PrazoMaisDias ;
      private short[] P007030_A1227ContagemResultado_PrazoInicialDias ;
      private bool[] P007030_n1227ContagemResultado_PrazoInicialDias ;
      private decimal[] P007030_A1180ContagemResultado_Custo ;
      private bool[] P007030_n1180ContagemResultado_Custo ;
      private decimal[] P007030_A1179ContagemResultado_PLFinal ;
      private bool[] P007030_n1179ContagemResultado_PLFinal ;
      private decimal[] P007030_A1178ContagemResultado_PBFinal ;
      private bool[] P007030_n1178ContagemResultado_PBFinal ;
      private bool[] P007030_A1173ContagemResultado_OSManual ;
      private bool[] P007030_n1173ContagemResultado_OSManual ;
      private int[] P007030_A1052ContagemResultado_GlsUser ;
      private bool[] P007030_n1052ContagemResultado_GlsUser ;
      private decimal[] P007030_A1051ContagemResultado_GlsValor ;
      private bool[] P007030_n1051ContagemResultado_GlsValor ;
      private String[] P007030_A1050ContagemResultado_GlsDescricao ;
      private bool[] P007030_n1050ContagemResultado_GlsDescricao ;
      private DateTime[] P007030_A1049ContagemResultado_GlsData ;
      private bool[] P007030_n1049ContagemResultado_GlsData ;
      private int[] P007030_A1044ContagemResultado_FncUsrCod ;
      private bool[] P007030_n1044ContagemResultado_FncUsrCod ;
      private int[] P007030_A1043ContagemResultado_LiqLogCod ;
      private bool[] P007030_n1043ContagemResultado_LiqLogCod ;
      private int[] P007030_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] P007030_n805ContagemResultado_ContratadaOrigemCod ;
      private decimal[] P007030_A799ContagemResultado_PFLFSImp ;
      private bool[] P007030_n799ContagemResultado_PFLFSImp ;
      private decimal[] P007030_A798ContagemResultado_PFBFSImp ;
      private bool[] P007030_n798ContagemResultado_PFBFSImp ;
      private bool[] P007030_A598ContagemResultado_Baseline ;
      private bool[] P007030_n598ContagemResultado_Baseline ;
      private int[] P007030_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P007030_n597ContagemResultado_LoteAceiteCod ;
      private String[] P007030_A592ContagemResultado_Evidencia ;
      private bool[] P007030_n592ContagemResultado_Evidencia ;
      private int[] P007030_A508ContagemResultado_Owner ;
      private String[] P007030_A514ContagemResultado_Observacao ;
      private bool[] P007030_n514ContagemResultado_Observacao ;
      private int[] P007030_A146Modulo_Codigo ;
      private bool[] P007030_n146Modulo_Codigo ;
      private int[] P007030_A489ContagemResultado_SistemaCod ;
      private bool[] P007030_n489ContagemResultado_SistemaCod ;
      private String[] P007030_A494ContagemResultado_Descricao ;
      private bool[] P007030_n494ContagemResultado_Descricao ;
      private String[] P007030_A493ContagemResultado_DemandaFM ;
      private bool[] P007030_n493ContagemResultado_DemandaFM ;
      private bool[] P007030_A485ContagemResultado_EhValidacao ;
      private bool[] P007030_n485ContagemResultado_EhValidacao ;
      private int[] P007030_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] P007030_n468ContagemResultado_NaoCnfDmnCod ;
      private String[] P007030_A465ContagemResultado_Link ;
      private bool[] P007030_n465ContagemResultado_Link ;
      private int[] P007030_A454ContagemResultado_ContadorFSCod ;
      private bool[] P007030_n454ContagemResultado_ContadorFSCod ;
      private DateTime[] P007030_A471ContagemResultado_DataDmn ;
      private int[] P007030_A456ContagemResultado_Codigo ;
      private int[] P007031_A456ContagemResultado_Codigo ;
      private int[] P007033_A192Contagem_Codigo ;
      private int[] P007034_A2199Contagem_DemandaCod ;
      private bool[] P007034_n2199Contagem_DemandaCod ;
      private decimal[] P007034_A1119Contagem_Divergencia ;
      private bool[] P007034_n1119Contagem_Divergencia ;
      private String[] P007034_A262Contagem_Status ;
      private bool[] P007034_n262Contagem_Status ;
      private int[] P007034_A192Contagem_Codigo ;
      private int[] P007038_A456ContagemResultado_Codigo ;
      private String[] P007038_A484ContagemResultado_StatusDmn ;
      private bool[] P007038_n484ContagemResultado_StatusDmn ;
      private DateTime[] P007038_A1348ContagemResultado_DataHomologacao ;
      private bool[] P007038_n1348ContagemResultado_DataHomologacao ;
      private int[] P007039_A456ContagemResultado_Codigo ;
      private bool[] P007039_A517ContagemResultado_Ultima ;
      private decimal[] P007039_A458ContagemResultado_PFBFS ;
      private bool[] P007039_n458ContagemResultado_PFBFS ;
      private decimal[] P007039_A459ContagemResultado_PFLFS ;
      private bool[] P007039_n459ContagemResultado_PFLFS ;
      private decimal[] P007039_A462ContagemResultado_Divergencia ;
      private short[] P007039_A483ContagemResultado_StatusCnt ;
      private DateTime[] P007039_A473ContagemResultado_DataCnt ;
      private String[] P007039_A511ContagemResultado_HoraCnt ;
      private int[] P007043_A192Contagem_Codigo ;
      private int[] P007043_A1118Contagem_ContratadaCod ;
      private bool[] P007043_n1118Contagem_ContratadaCod ;
      private String[] P007043_A945Contagem_Demanda ;
      private bool[] P007043_n945Contagem_Demanda ;
      private int[] P007044_A722Baseline_Codigo ;
      private long[] P007045_A1797LogResponsavel_Codigo ;
      private int[] P007045_A896LogResponsavel_Owner ;
      private int[] P007045_A892LogResponsavel_DemandaCod ;
      private bool[] P007045_n892LogResponsavel_DemandaCod ;
      private int[] P007046_A135Sistema_AreaTrabalhoCod ;
      private String[] P007046_A129Sistema_Sigla ;
      private int[] P007046_A127Sistema_Codigo ;
      private IGxSession AV82WebSession ;
      private ExcelDocumentI AV53ExcelDocument ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV127ContagemResultado_Codigos ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV84Usuarios ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV85Attachments ;
      private wwpbaseobjects.SdtWWPContext AV83WWPContext ;
   }

   public class aprc_importarpffs__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00706( IGxContext context ,
                                             String AV117DemandaFM ,
                                             String A493ContagemResultado_DemandaFM ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             int AV43Contratada_Codigo ,
                                             int A805ContagemResultado_ContratadaOrigemCod ,
                                             int AV95ContratadaFS_Codigo ,
                                             int A489ContagemResultado_SistemaCod ,
                                             int AV136Sistema_Codigo ,
                                             String AV46Demanda ,
                                             String A457ContagemResultado_Demanda )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [5] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [ContagemResultado_Demanda], [ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, [ContagemResultado_ContratadaOrigemCod], [ContagemResultado_SistemaCod], [ContagemResultado_DemandaFM], [ContagemResultado_Codigo], [ContagemResultado_DataDmn], [ContagemResultado_DataEntrega], [ContagemResultado_HoraEntrega], [ContagemResultado_DataPrevista], [ContagemResultado_StatusDmn] FROM [ContagemResultado] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([ContagemResultado_Demanda] = @AV46Demanda)";
         scmdbuf = scmdbuf + " and ([ContagemResultado_ContratadaCod] = @AV43Contratada_Codigo)";
         scmdbuf = scmdbuf + " and ([ContagemResultado_ContratadaOrigemCod] = @AV95ContratadaFS_Codigo)";
         scmdbuf = scmdbuf + " and ([ContagemResultado_SistemaCod] = @AV136Sistema_Codigo)";
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117DemandaFM)) )
         {
            sWhereString = sWhereString + " and ([ContagemResultado_DemandaFM] = @AV117DemandaFM)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [ContagemResultado_Demanda], [ContagemResultado_StatusDmn] DESC";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 4 :
                     return conditional_P00706(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] , (int)dynConstraints[6] , (int)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new UpdateCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new UpdateCursor(def[13])
         ,new UpdateCursor(def[14])
         ,new UpdateCursor(def[15])
         ,new UpdateCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new UpdateCursor(def[22])
         ,new UpdateCursor(def[23])
         ,new UpdateCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new UpdateCursor(def[26])
         ,new UpdateCursor(def[27])
         ,new ForEachCursor(def[28])
         ,new ForEachCursor(def[29])
         ,new UpdateCursor(def[30])
         ,new ForEachCursor(def[31])
         ,new ForEachCursor(def[32])
         ,new UpdateCursor(def[33])
         ,new UpdateCursor(def[34])
         ,new UpdateCursor(def[35])
         ,new ForEachCursor(def[36])
         ,new ForEachCursor(def[37])
         ,new UpdateCursor(def[38])
         ,new UpdateCursor(def[39])
         ,new UpdateCursor(def[40])
         ,new ForEachCursor(def[41])
         ,new ForEachCursor(def[42])
         ,new ForEachCursor(def[43])
         ,new ForEachCursor(def[44])
         ,new UpdateCursor(def[45])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00702 ;
          prmP00702 = new Object[] {
          new Object[] {"@AV46Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV83WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00703 ;
          prmP00703 = new Object[] {
          new Object[] {"@AV134ContratoservicosFS_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00704 ;
          prmP00704 = new Object[] {
          new Object[] {"@Contratada_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00705 ;
          prmP00705 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00707 ;
          prmP00707 = new Object[] {
          new Object[] {"@ContagemResultado_DataDmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_DataEntrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraEntrega",SqlDbType.DateTime,0,5} ,
          new Object[] {"@ContagemResultado_DataPrevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00708 ;
          prmP00708 = new Object[] {
          new Object[] {"@AV35ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00709 ;
          prmP00709 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0} ,
          new Object[] {"@ContagemResultado_TimeCnt",SqlDbType.DateTime,0,5} ,
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFBFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_ParecerTcn",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_NaoCnfCntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultadoContagens_Esforco",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Deflator",SqlDbType.Decimal,6,3} ,
          new Object[] {"@ContagemResultado_CstUntPrd",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultado_Planilha",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@ContagemResultado_NomePla",SqlDbType.Char,50,0} ,
          new Object[] {"@ContagemResultado_TipoPla",SqlDbType.Char,10,0} ,
          new Object[] {"@ContagemResultadoContagens_Prazo",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_NvlCnt",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmP007010 ;
          prmP007010 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_DataHomologacao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP007011 ;
          prmP007011 = new Object[] {
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP007012 ;
          prmP007012 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_DataHomologacao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP007013 ;
          prmP007013 = new Object[] {
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP007014 ;
          prmP007014 = new Object[] {
          new Object[] {"@AV35ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP007015 ;
          prmP007015 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_PFLFSImp",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFBFSImp",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_DataHomologacao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP007016 ;
          prmP007016 = new Object[] {
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP007017 ;
          prmP007017 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_PFLFSImp",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFBFSImp",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_DataHomologacao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP007018 ;
          prmP007018 = new Object[] {
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP007019 ;
          prmP007019 = new Object[] {
          new Object[] {"@AV43Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP007020 ;
          prmP007020 = new Object[] {
          new Object[] {"@AV100Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV43Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP007021 ;
          prmP007021 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Usuario_EhContratada",SqlDbType.Bit,4,0}
          } ;
          Object[] prmP007022 ;
          prmP007022 = new Object[] {
          new Object[] {"@AV136Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV46Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV95ContratadaFS_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP007023 ;
          prmP007023 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP007024 ;
          prmP007024 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP007025 ;
          prmP007025 = new Object[] {
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP007026 ;
          prmP007026 = new Object[] {
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP007027 ;
          prmP007027 = new Object[] {
          new Object[] {"@AV46Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@TpUo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Metodologia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV43Contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV95ContratadaFS_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP007028 ;
          prmP007028 = new Object[] {
          new Object[] {"@Contagem_PFB",SqlDbType.Decimal,13,5} ,
          new Object[] {"@Contagem_PFL",SqlDbType.Decimal,13,5} ,
          new Object[] {"@Contagem_PFBA",SqlDbType.Decimal,14,5} ,
          new Object[] {"@Contagem_PFLA",SqlDbType.Decimal,14,5} ,
          new Object[] {"@Contagem_PFBD",SqlDbType.Decimal,14,5} ,
          new Object[] {"@Contagem_PFLD",SqlDbType.Decimal,14,5} ,
          new Object[] {"@Contagem_DataAtualizacao",SqlDbType.DateTime,10,8} ,
          new Object[] {"@Contagem_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Contagem_Status",SqlDbType.Char,1,0} ,
          new Object[] {"@Contagem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP007029 ;
          prmP007029 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_DataHomologacao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_HoraEntrega",SqlDbType.DateTime,0,5} ,
          new Object[] {"@ContagemResultado_DataEntrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_ValorPF",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP007030 ;
          prmP007030 = new Object[] {
          new Object[] {"@AV35ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferP007030 ;
          cmdBufferP007030=" SELECT TOP 1 [ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, [ContagemResultado_SS], [ContagemResultado_DataHomologacao], [ContagemResultado_Agrupador], [ContagemResultado_HoraEntrega], [ContagemResultado_Responsavel], [ContagemResultado_OSVinculada], [ContagemResultado_ValorPF], [ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, [ContagemResultado_StatusDmn], [ContagemResultado_Demanda], [ContagemResultado_DataEntrega], [ContagemResultado_QuantidadeSolicitada], [ContagemResultado_DataEntregaReal], [ContagemResultado_DataPrvPgm], [ContagemResultado_PFCnc], [ContagemResultado_VlrCnc], [ContagemResultado_SemCusto], [ContagemResultado_DataInicio], [ContagemResultado_Entrega], [ContagemResultado_Combinada], [ContagemResultado_ServicoSS], [ContagemResultado_PrioridadePrevista], [ContagemResultado_Restricoes], [ContagemResultado_Referencia], [ContagemResultado_UOOwner], [ContagemResultado_TipoRegistro], [ContagemResultado_VlrAceite], [ContagemResultado_ProjetoCod], [ContagemResultado_FimAnl], [ContagemResultado_InicioAnl], [ContagemResultado_TmpEstAnl], [ContagemResultado_Evento], [ContagemResultado_FimCrr], [ContagemResultado_InicioCrr], [ContagemResultado_FimExc], [ContagemResultado_InicioExc], [ContagemResultado_TmpEstCrr], [ContagemResultado_TmpEstExc], [ContagemResultado_TemDpnHmlg], [ContagemResultado_CntSrvPrrCst], [ContagemResultado_CntSrvPrrPrz], [ContagemResultado_CntSrvPrrCod], [ContagemResultado_RdmnUpdated], [ContagemResultado_RdmnProjectId], [ContagemResultado_RdmnIssueId], [ContagemResultado_DataPrevista], [ContagemResultado_DataCadastro], [ContagemResultado_DataExecucao], [ContagemResultado_PrazoMaisDias], [ContagemResultado_PrazoInicialDias], [ContagemResultado_Custo], [ContagemResultado_PLFinal], [ContagemResultado_PBFinal], [ContagemResultado_OSManual], "
          + " [ContagemResultado_GlsUser], [ContagemResultado_GlsValor], [ContagemResultado_GlsDescricao], [ContagemResultado_GlsData], [ContagemResultado_FncUsrCod], [ContagemResultado_LiqLogCod], [ContagemResultado_ContratadaOrigemCod], [ContagemResultado_PFLFSImp], [ContagemResultado_PFBFSImp], [ContagemResultado_Baseline], [ContagemResultado_LoteAceiteCod], [ContagemResultado_Evidencia], [ContagemResultado_Owner], [ContagemResultado_Observacao], [Modulo_Codigo], [ContagemResultado_SistemaCod], [ContagemResultado_Descricao], [ContagemResultado_DemandaFM], [ContagemResultado_EhValidacao], [ContagemResultado_NaoCnfDmnCod], [ContagemResultado_Link], [ContagemResultado_ContadorFSCod], [ContagemResultado_DataDmn], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @AV35ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo]" ;
          Object[] prmP007031 ;
          prmP007031 = new Object[] {
          new Object[] {"@ContagemResultado_DataDmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_DataEntrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_ContadorFSCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@ContagemResultado_Link",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@ContagemResultado_NaoCnfDmnCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_EhValidacao",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DemandaFM",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ContagemResultado_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultado_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Observacao",SqlDbType.VarChar,20000,0} ,
          new Object[] {"@ContagemResultado_Owner",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_ValorPF",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultado_Evidencia",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultado_LoteAceiteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Baseline",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_OSVinculada",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_PFBFSImp",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFSImp",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_ContratadaOrigemCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_HoraEntrega",SqlDbType.DateTime,0,5} ,
          new Object[] {"@ContagemResultado_LiqLogCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_FncUsrCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Agrupador",SqlDbType.Char,15,0} ,
          new Object[] {"@ContagemResultado_GlsData",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_GlsDescricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultado_GlsValor",SqlDbType.Decimal,12,2} ,
          new Object[] {"@ContagemResultado_GlsUser",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_OSManual",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_PBFinal",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PLFinal",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_Custo",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultado_PrazoInicialDias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_PrazoMaisDias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_DataHomologacao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_DataExecucao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_DataCadastro",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_DataPrevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_RdmnIssueId",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_RdmnProjectId",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_RdmnUpdated",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_CntSrvPrrCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_CntSrvPrrPrz",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_CntSrvPrrCst",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_SS",SqlDbType.Int,8,0} ,
          new Object[] {"@ContagemResultado_TemDpnHmlg",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_TmpEstExc",SqlDbType.Int,8,0} ,
          new Object[] {"@ContagemResultado_TmpEstCrr",SqlDbType.Int,8,0} ,
          new Object[] {"@ContagemResultado_InicioExc",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_FimExc",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_InicioCrr",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_FimCrr",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Evento",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_TmpEstAnl",SqlDbType.Int,8,0} ,
          new Object[] {"@ContagemResultado_InicioAnl",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_FimAnl",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_ProjetoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_VlrAceite",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultado_TipoRegistro",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_UOOwner",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Referencia",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultado_Restricoes",SqlDbType.VarChar,250,0} ,
          new Object[] {"@ContagemResultado_PrioridadePrevista",SqlDbType.VarChar,250,0} ,
          new Object[] {"@ContagemResultado_ServicoSS",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Combinada",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Entrega",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_DataInicio",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_SemCusto",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_VlrCnc",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultado_PFCnc",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_DataPrvPgm",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_DataEntregaReal",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_QuantidadeSolicitada",SqlDbType.Decimal,9,4}
          } ;
          String cmdBufferP007031 ;
          cmdBufferP007031=" INSERT INTO [ContagemResultado]([ContagemResultado_DataDmn], [ContagemResultado_DataEntrega], [ContagemResultado_ContadorFSCod], [ContagemResultado_Demanda], [ContagemResultado_Link], [ContagemResultado_NaoCnfDmnCod], [ContagemResultado_StatusDmn], [ContagemResultado_EhValidacao], [ContagemResultado_ContratadaCod], [ContagemResultado_DemandaFM], [ContagemResultado_Descricao], [ContagemResultado_SistemaCod], [Modulo_Codigo], [ContagemResultado_Observacao], [ContagemResultado_Owner], [ContagemResultado_ValorPF], [ContagemResultado_Evidencia], [ContagemResultado_LoteAceiteCod], [ContagemResultado_Baseline], [ContagemResultado_OSVinculada], [ContagemResultado_PFBFSImp], [ContagemResultado_PFLFSImp], [ContagemResultado_ContratadaOrigemCod], [ContagemResultado_Responsavel], [ContagemResultado_HoraEntrega], [ContagemResultado_LiqLogCod], [ContagemResultado_FncUsrCod], [ContagemResultado_Agrupador], [ContagemResultado_GlsData], [ContagemResultado_GlsDescricao], [ContagemResultado_GlsValor], [ContagemResultado_GlsUser], [ContagemResultado_OSManual], [ContagemResultado_PBFinal], [ContagemResultado_PLFinal], [ContagemResultado_Custo], [ContagemResultado_PrazoInicialDias], [ContagemResultado_PrazoMaisDias], [ContagemResultado_DataHomologacao], [ContagemResultado_DataExecucao], [ContagemResultado_DataCadastro], [ContagemResultado_DataPrevista], [ContagemResultado_RdmnIssueId], [ContagemResultado_RdmnProjectId], [ContagemResultado_RdmnUpdated], [ContagemResultado_CntSrvPrrCod], [ContagemResultado_CntSrvPrrPrz], [ContagemResultado_CntSrvPrrCst], [ContagemResultado_SS], [ContagemResultado_TemDpnHmlg], [ContagemResultado_TmpEstExc], [ContagemResultado_TmpEstCrr], [ContagemResultado_InicioExc], [ContagemResultado_FimExc], [ContagemResultado_InicioCrr], [ContagemResultado_FimCrr], [ContagemResultado_Evento], "
          + " [ContagemResultado_TmpEstAnl], [ContagemResultado_InicioAnl], [ContagemResultado_FimAnl], [ContagemResultado_ProjetoCod], [ContagemResultado_CntSrvCod], [ContagemResultado_VlrAceite], [ContagemResultado_TipoRegistro], [ContagemResultado_UOOwner], [ContagemResultado_Referencia], [ContagemResultado_Restricoes], [ContagemResultado_PrioridadePrevista], [ContagemResultado_ServicoSS], [ContagemResultado_Combinada], [ContagemResultado_Entrega], [ContagemResultado_DataInicio], [ContagemResultado_SemCusto], [ContagemResultado_VlrCnc], [ContagemResultado_PFCnc], [ContagemResultado_DataPrvPgm], [ContagemResultado_DataEntregaReal], [ContagemResultado_QuantidadeSolicitada]) VALUES(@ContagemResultado_DataDmn, @ContagemResultado_DataEntrega, @ContagemResultado_ContadorFSCod, @ContagemResultado_Demanda, @ContagemResultado_Link, @ContagemResultado_NaoCnfDmnCod, @ContagemResultado_StatusDmn, @ContagemResultado_EhValidacao, @ContagemResultado_ContratadaCod, @ContagemResultado_DemandaFM, @ContagemResultado_Descricao, @ContagemResultado_SistemaCod, @Modulo_Codigo, @ContagemResultado_Observacao, @ContagemResultado_Owner, @ContagemResultado_ValorPF, @ContagemResultado_Evidencia, @ContagemResultado_LoteAceiteCod, @ContagemResultado_Baseline, @ContagemResultado_OSVinculada, @ContagemResultado_PFBFSImp, @ContagemResultado_PFLFSImp, @ContagemResultado_ContratadaOrigemCod, @ContagemResultado_Responsavel, @ContagemResultado_HoraEntrega, @ContagemResultado_LiqLogCod, @ContagemResultado_FncUsrCod, @ContagemResultado_Agrupador, @ContagemResultado_GlsData, @ContagemResultado_GlsDescricao, @ContagemResultado_GlsValor, @ContagemResultado_GlsUser, @ContagemResultado_OSManual, @ContagemResultado_PBFinal, @ContagemResultado_PLFinal, @ContagemResultado_Custo, @ContagemResultado_PrazoInicialDias, @ContagemResultado_PrazoMaisDias,"
          + " @ContagemResultado_DataHomologacao, @ContagemResultado_DataExecucao, @ContagemResultado_DataCadastro, @ContagemResultado_DataPrevista, @ContagemResultado_RdmnIssueId, @ContagemResultado_RdmnProjectId, @ContagemResultado_RdmnUpdated, @ContagemResultado_CntSrvPrrCod, @ContagemResultado_CntSrvPrrPrz, @ContagemResultado_CntSrvPrrCst, @ContagemResultado_SS, @ContagemResultado_TemDpnHmlg, @ContagemResultado_TmpEstExc, @ContagemResultado_TmpEstCrr, @ContagemResultado_InicioExc, @ContagemResultado_FimExc, @ContagemResultado_InicioCrr, @ContagemResultado_FimCrr, @ContagemResultado_Evento, @ContagemResultado_TmpEstAnl, @ContagemResultado_InicioAnl, @ContagemResultado_FimAnl, @ContagemResultado_ProjetoCod, @ContagemResultado_CntSrvCod, @ContagemResultado_VlrAceite, @ContagemResultado_TipoRegistro, @ContagemResultado_UOOwner, @ContagemResultado_Referencia, @ContagemResultado_Restricoes, @ContagemResultado_PrioridadePrevista, @ContagemResultado_ServicoSS, @ContagemResultado_Combinada, @ContagemResultado_Entrega, @ContagemResultado_DataInicio, @ContagemResultado_SemCusto, @ContagemResultado_VlrCnc, @ContagemResultado_PFCnc, @ContagemResultado_DataPrvPgm, @ContagemResultado_DataEntregaReal, @ContagemResultado_QuantidadeSolicitada); SELECT SCOPE_IDENTITY()" ;
          Object[] prmP007032 ;
          prmP007032 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0} ,
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFBFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_NaoCnfCntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultadoContagens_Esforco",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Deflator",SqlDbType.Decimal,6,3}
          } ;
          Object[] prmP007033 ;
          prmP007033 = new Object[] {
          new Object[] {"@Contagem_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contagem_Tecnica",SqlDbType.Char,1,0} ,
          new Object[] {"@Contagem_Tipo",SqlDbType.Char,1,0} ,
          new Object[] {"@Contagem_DataCriacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Contagem_UsuarioContadorCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contagem_Status",SqlDbType.Char,1,0} ,
          new Object[] {"@Contagem_PFB",SqlDbType.Decimal,13,5} ,
          new Object[] {"@Contagem_PFL",SqlDbType.Decimal,13,5} ,
          new Object[] {"@Contagem_Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@Contagem_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contagem_ProjetoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contagem_PFLA",SqlDbType.Decimal,14,5} ,
          new Object[] {"@Contagem_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contagem_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Contagem_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contagem_Pendencia",SqlDbType.VarChar,1,0} ,
          new Object[] {"@Contagem_TipoUnidade",SqlDbType.Int,6,0} ,
          new Object[] {"@Contagem_UnidadeOrganizacional",SqlDbType.Int,6,0} ,
          new Object[] {"@Contagem_Metodologia",SqlDbType.Int,6,0} ,
          new Object[] {"@Contagem_Fases",SqlDbType.Int,6,0} ,
          new Object[] {"@Contagem_Agrupador",SqlDbType.VarChar,15,0} ,
          new Object[] {"@Contagem_DemandaCod",SqlDbType.Int,9,0} ,
          new Object[] {"@Contagem_DataAtualizacao",SqlDbType.DateTime,10,8}
          } ;
          String cmdBufferP007033 ;
          cmdBufferP007033=" INSERT INTO [Contagem]([Contagem_AreaTrabalhoCod], [Contagem_Tecnica], [Contagem_Tipo], [Contagem_DataCriacao], [Contagem_UsuarioContadorCod], [Contagem_Status], [Contagem_PFB], [Contagem_PFL], [Contagem_Demanda], [Contagem_SistemaCod], [Contagem_ProjetoCod], [Contagem_PFLA], [Contagem_ContratadaCod], [Contagem_Divergencia], [Contagem_CntSrvCod], [Contagem_Pendencia], [Contagem_TipoUnidade], [Contagem_UnidadeOrganizacional], [Contagem_Metodologia], [Contagem_Fases], [Contagem_Agrupador], [Contagem_DemandaCod], [Contagem_DataAtualizacao], [Contagem_Proposito], [Contagem_Escopo], [Contagem_Fronteira], [Contagem_Observacao], [Contagem_Link], [Contagem_Fator], [Contagem_Lock], [Contagem_Notas], [Contagem_Consideracoes], [Contagem_PFBA], [Contagem_PFBD], [Contagem_PFLD], [Contagem_Deflator], [Contagem_DataHomologacao], [Contagem_Descricao], [Contagem_Aplicabilidade], [Contagem_Versao], [Contagem_ServicoCod], [Contagem_ArquivoImp], [Contagem_ReferenciaINM], [Contagem_AmbienteTecnologico], [Contagem_Excluidos], [Contagem_PFBRetrabalho], [Contagem_PFLImp], [Contagem_fatorImp], [Contagem_PFLAImp], [Contagem_TaxaEntregaDsnv], [Contagem_TaxaEntregaMlhr], [Contagem_LocPF], [Contagem_ValorTaxaEntregaDsnv], [Contagem_ValorTaxaEntregaMlhr], [Contagem_valorLocPF]) VALUES(@Contagem_AreaTrabalhoCod, @Contagem_Tecnica, @Contagem_Tipo, @Contagem_DataCriacao, @Contagem_UsuarioContadorCod, @Contagem_Status, @Contagem_PFB, @Contagem_PFL, @Contagem_Demanda, @Contagem_SistemaCod, @Contagem_ProjetoCod, @Contagem_PFLA, @Contagem_ContratadaCod, @Contagem_Divergencia, @Contagem_CntSrvCod, @Contagem_Pendencia, @Contagem_TipoUnidade, @Contagem_UnidadeOrganizacional, @Contagem_Metodologia, @Contagem_Fases, @Contagem_Agrupador, @Contagem_DemandaCod, @Contagem_DataAtualizacao, '', '', '', '', '', convert(int, "
          + " 0), convert(bit, 0), '', '', convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), '', '', '', convert(int, 0), '', convert(int, 0), convert(int, 0), '', convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0)); SELECT SCOPE_IDENTITY()" ;
          Object[] prmP007034 ;
          prmP007034 = new Object[] {
          new Object[] {"@AV35ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP007035 ;
          prmP007035 = new Object[] {
          new Object[] {"@Contagem_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Contagem_Status",SqlDbType.Char,1,0} ,
          new Object[] {"@Contagem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP007036 ;
          prmP007036 = new Object[] {
          new Object[] {"@Contagem_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Contagem_Status",SqlDbType.Char,1,0} ,
          new Object[] {"@Contagem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP007037 ;
          prmP007037 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0} ,
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_NaoCnfCntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultadoContagens_Esforco",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Deflator",SqlDbType.Decimal,6,3}
          } ;
          Object[] prmP007038 ;
          prmP007038 = new Object[] {
          new Object[] {"@AV35ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP007039 ;
          prmP007039 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP007040 ;
          prmP007040 = new Object[] {
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP007041 ;
          prmP007041 = new Object[] {
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP007042 ;
          prmP007042 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_DataHomologacao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP007043 ;
          prmP007043 = new Object[] {
          new Object[] {"@AV46Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV113AreaTrabalho_ContratadaUpdBslCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP007044 ;
          prmP007044 = new Object[] {
          new Object[] {"@Baseline_UserCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Baseline_ProjetoMelCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contagem_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Baseline_DataHomologacao",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmP007045 ;
          prmP007045 = new Object[] {
          new Object[] {"@AV35ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP007046 ;
          prmP007046 = new Object[] {
          new Object[] {"@AV83WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV78Sistema_Sigla",SqlDbType.Char,25,0}
          } ;
          Object[] prmP007047 ;
          prmP007047 = new Object[] {
          new Object[] {"@ContagemResultado_OSVinculada",SqlDbType.Int,6,0} ,
          new Object[] {"@AV35ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00706 ;
          prmP00706 = new Object[] {
          new Object[] {"@AV46Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV43Contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV95ContratadaFS_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV136Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV117DemandaFM",SqlDbType.VarChar,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00702", "SELECT TOP 1 T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T3.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_Agrupador], T1.[ContagemResultado_Codigo] FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) WHERE (T1.[ContagemResultado_Demanda] = @AV46Demanda) AND (T3.[Contratada_AreaTrabalhoCod] = @AV83WWPC_1Areatrabalho_codigo) ORDER BY T1.[ContagemResultado_Demanda] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00702,1,0,false,true )
             ,new CursorDef("P00703", "SELECT T1.[Contrato_Codigo], T2.[Contratada_Codigo], T3.[Contratada_PessoaCod] AS Contratada_PessoaCod, T3.[Contratada_AreaTrabalhoCod], T1.[ContratoServicos_Codigo], T4.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Servico_Percentual], T2.[Contrato_ValorUnidadeContratacao], T1.[ContratoServicos_PrazoCorrecao], T1.[ContratoServicos_PrazoCorrecaoTipo], T1.[ContratoServicos_PrazoInicio], T1.[ContratoServicos_PrazoAnalise] FROM ((([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod]) WHERE T1.[ContratoServicos_Codigo] = @AV134ContratoservicosFS_Codigo ORDER BY T1.[ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00703,1,0,true,true )
             ,new CursorDef("P00704", "SELECT TOP 1 T1.[AreaTrabalho_Codigo], T1.[Contratante_Codigo], T2.[Contratante_FimDoExpediente], T1.[AreaTrabalho_ContratadaUpdBslCod] FROM ([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) WHERE T1.[AreaTrabalho_Codigo] = @Contratada_AreaTrabalhoCod ORDER BY T1.[AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00704,1,0,true,true )
             ,new CursorDef("P00705", "SELECT TOP 1 [Contratante_Codigo] FROM [Contratante] WITH (NOLOCK) WHERE [Contratante_Codigo] = @Contratante_Codigo ORDER BY [Contratante_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00705,1,0,false,true )
             ,new CursorDef("P00706", "scmdbuf",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00706,1,0,true,false )
             ,new CursorDef("P00707", "UPDATE [ContagemResultado] SET [ContagemResultado_DataDmn]=@ContagemResultado_DataDmn, [ContagemResultado_DataEntrega]=@ContagemResultado_DataEntrega, [ContagemResultado_HoraEntrega]=@ContagemResultado_HoraEntrega, [ContagemResultado_DataPrevista]=@ContagemResultado_DataPrevista  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00707)
             ,new CursorDef("P00708", "SELECT TOP 1 T1.[ContagemResultado_CstUntPrd], T1.[ContagemResultado_Ultima], T1.[ContagemResultadoContagens_Esforco], T1.[ContagemResultado_StatusCnt], T1.[ContagemResultado_NaoCnfCntCod], T1.[ContagemResultado_ContadorFMCod], T1.[ContagemResultado_Divergencia], T1.[ContagemResultado_PFLFM], T1.[ContagemResultado_PFBFM], T1.[ContagemResultado_PFLFS], T1.[ContagemResultado_PFBFS], T1.[ContagemResultado_HoraCnt], T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_Codigo], T1.[ContagemResultado_NvlCnt], T1.[ContagemResultadoContagens_Prazo], T1.[ContagemResultado_TipoPla], T1.[ContagemResultado_NomePla], T1.[ContagemResultado_Deflator], T1.[ContagemResultado_ParecerTcn], T1.[ContagemResultado_TimeCnt], T2.[ContagemResultado_StatusDmn], T2.[ContagemResultado_DataHomologacao], T1.[ContagemResultado_Planilha] FROM ([ContagemResultadoContagens] T1 WITH (UPDLOCK) INNER JOIN [ContagemResultado] T2 WITH (UPDLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE (T1.[ContagemResultado_Codigo] = @AV35ContagemResultado_Codigo) AND (T1.[ContagemResultado_Ultima] = 1) ORDER BY T1.[ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00708,1,0,true,true )
             ,new CursorDef("P00709", "INSERT INTO [ContagemResultadoContagens]([ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt], [ContagemResultado_TimeCnt], [ContagemResultado_PFBFS], [ContagemResultado_PFLFS], [ContagemResultado_PFBFM], [ContagemResultado_PFLFM], [ContagemResultado_Divergencia], [ContagemResultado_ParecerTcn], [ContagemResultado_ContadorFMCod], [ContagemResultado_NaoCnfCntCod], [ContagemResultado_StatusCnt], [ContagemResultadoContagens_Esforco], [ContagemResultado_Ultima], [ContagemResultado_Deflator], [ContagemResultado_CstUntPrd], [ContagemResultado_Planilha], [ContagemResultado_NomePla], [ContagemResultado_TipoPla], [ContagemResultadoContagens_Prazo], [ContagemResultado_NvlCnt]) VALUES(@ContagemResultado_Codigo, @ContagemResultado_DataCnt, @ContagemResultado_HoraCnt, @ContagemResultado_TimeCnt, @ContagemResultado_PFBFS, @ContagemResultado_PFLFS, @ContagemResultado_PFBFM, @ContagemResultado_PFLFM, @ContagemResultado_Divergencia, @ContagemResultado_ParecerTcn, @ContagemResultado_ContadorFMCod, @ContagemResultado_NaoCnfCntCod, @ContagemResultado_StatusCnt, @ContagemResultadoContagens_Esforco, @ContagemResultado_Ultima, @ContagemResultado_Deflator, @ContagemResultado_CstUntPrd, @ContagemResultado_Planilha, @ContagemResultado_NomePla, @ContagemResultado_TipoPla, @ContagemResultadoContagens_Prazo, @ContagemResultado_NvlCnt)", GxErrorMask.GX_NOMASK,prmP00709)
             ,new CursorDef("P007010", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_DataHomologacao]=@ContagemResultado_DataHomologacao  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP007010)
             ,new CursorDef("P007011", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_Ultima]=@ContagemResultado_Ultima, [ContagemResultado_StatusCnt]=@ContagemResultado_StatusCnt, [ContagemResultado_Divergencia]=@ContagemResultado_Divergencia, [ContagemResultado_PFLFS]=@ContagemResultado_PFLFS, [ContagemResultado_PFBFS]=@ContagemResultado_PFBFS  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP007011)
             ,new CursorDef("P007012", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_DataHomologacao]=@ContagemResultado_DataHomologacao  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP007012)
             ,new CursorDef("P007013", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_Ultima]=@ContagemResultado_Ultima, [ContagemResultado_StatusCnt]=@ContagemResultado_StatusCnt, [ContagemResultado_Divergencia]=@ContagemResultado_Divergencia, [ContagemResultado_PFLFS]=@ContagemResultado_PFLFS, [ContagemResultado_PFBFS]=@ContagemResultado_PFBFS  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP007013)
             ,new CursorDef("P007014", "SELECT TOP 1 T1.[ContagemResultado_Ultima], T1.[ContagemResultado_Codigo], T2.[ContagemResultado_StatusDmn], T2.[ContagemResultado_PFLFSImp], T2.[ContagemResultado_PFBFSImp], T1.[ContagemResultado_PFLFS], T1.[ContagemResultado_PFBFS], T1.[ContagemResultado_PFBFM], T1.[ContagemResultado_PFLFM], T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_StatusCnt], T1.[ContagemResultado_Divergencia], T2.[ContagemResultado_DataHomologacao], T2.[ContagemResultado_Responsavel], T2.[ContagemResultado_DataPrevista], T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_HoraCnt] FROM ([ContagemResultadoContagens] T1 WITH (UPDLOCK) INNER JOIN [ContagemResultado] T2 WITH (UPDLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE (T1.[ContagemResultado_Codigo] = @AV35ContagemResultado_Codigo) AND (T1.[ContagemResultado_Ultima] = 1) ORDER BY T1.[ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP007014,1,0,true,true )
             ,new CursorDef("P007015", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_PFLFSImp]=@ContagemResultado_PFLFSImp, [ContagemResultado_PFBFSImp]=@ContagemResultado_PFBFSImp, [ContagemResultado_DataHomologacao]=@ContagemResultado_DataHomologacao, [ContagemResultado_Responsavel]=@ContagemResultado_Responsavel  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP007015)
             ,new CursorDef("P007016", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_PFLFS]=@ContagemResultado_PFLFS, [ContagemResultado_PFBFS]=@ContagemResultado_PFBFS, [ContagemResultado_StatusCnt]=@ContagemResultado_StatusCnt, [ContagemResultado_Divergencia]=@ContagemResultado_Divergencia  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP007016)
             ,new CursorDef("P007017", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_PFLFSImp]=@ContagemResultado_PFLFSImp, [ContagemResultado_PFBFSImp]=@ContagemResultado_PFBFSImp, [ContagemResultado_DataHomologacao]=@ContagemResultado_DataHomologacao, [ContagemResultado_Responsavel]=@ContagemResultado_Responsavel  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP007017)
             ,new CursorDef("P007018", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_PFLFS]=@ContagemResultado_PFLFS, [ContagemResultado_PFBFS]=@ContagemResultado_PFBFS, [ContagemResultado_StatusCnt]=@ContagemResultado_StatusCnt, [ContagemResultado_Divergencia]=@ContagemResultado_Divergencia  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP007018)
             ,new CursorDef("P007019", "SELECT T2.[Usuario_EhFinanceiro], T1.[ContratadaUsuario_ContratadaCod], T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod FROM ([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) WHERE (T1.[ContratadaUsuario_ContratadaCod] = @AV43Contratada_Codigo) AND (T2.[Usuario_EhFinanceiro] = 1) ORDER BY T1.[ContratadaUsuario_ContratadaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP007019,100,0,false,false )
             ,new CursorDef("P007020", "SELECT T3.[Usuario_EhContratada], T1.[Contrato_Codigo], T1.[Servico_Codigo], T2.[Contratada_Codigo], T2.[Contrato_PrepostoCod] AS Contrato_PrepostoCod, T1.[ContratoServicos_Codigo] FROM (([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) LEFT JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T2.[Contrato_PrepostoCod]) WHERE (T1.[Servico_Codigo] = @AV100Servico_Codigo) AND (T2.[Contratada_Codigo] = @AV43Contratada_Codigo) ORDER BY T1.[Servico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP007020,100,0,true,false )
             ,new CursorDef("P007021", "SELECT [ContratoGestor_ContratoCod], [ContratoGestor_UsuarioCod] FROM [ContratoGestor] WITH (NOLOCK) WHERE ([ContratoGestor_ContratoCod] = @Contrato_Codigo) AND (@Usuario_EhContratada = 1) ORDER BY [ContratoGestor_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP007021,100,0,false,false )
             ,new CursorDef("P007022", "SELECT T1.[Modulo_Codigo], T2.[Sistema_Codigo], T3.[Sistema_ImpUserCod] AS Sistema_ImpUserCod, T4.[Usuario_CargoCod] AS Usuario_CargoCod, T5.[Cargo_UOCod] AS Usuario_CargoUOCod, T3.[Metodologia_Codigo], T6.[TpUo_Codigo], T1.[ContagemResultado_Codigo], T1.[ContagemResultado_SistemaCod], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_Demanda], T1.[ContagemResultado_OSVinculada], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DataHomologacao], T1.[ContagemResultado_HoraEntrega], T1.[ContagemResultado_PrazoMaisDias], T1.[ContagemResultado_PrazoInicialDias], T1.[ContagemResultado_DataEntrega], T1.[ContagemResultado_ValorPF] FROM ((((([ContagemResultado] T1 WITH (UPDLOCK) LEFT JOIN [Modulo] T2 WITH (NOLOCK) ON T2.[Modulo_Codigo] = T1.[Modulo_Codigo]) LEFT JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T2.[Sistema_Codigo]) LEFT JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = T3.[Sistema_ImpUserCod]) LEFT JOIN [Geral_Cargo] T5 WITH (NOLOCK) ON T5.[Cargo_Codigo] = T4.[Usuario_CargoCod]) LEFT JOIN [Geral_UnidadeOrganizacional] T6 WITH (NOLOCK) ON T6.[UnidadeOrganizacional_Codigo] = T5.[Cargo_UOCod]) WHERE (T1.[ContagemResultado_SistemaCod] = @AV136Sistema_Codigo) AND (T1.[ContagemResultado_Demanda] = @AV46Demanda) AND (T1.[ContagemResultado_ContratadaCod] = @AV95ContratadaFS_Codigo) ORDER BY T1.[ContagemResultado_SistemaCod] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP007022,1,0,true,false )
             ,new CursorDef("P007023", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_Ultima], [ContagemResultado_DataCnt], [ContagemResultado_PFBFS], [ContagemResultado_PFLFS], [ContagemResultado_Divergencia], [ContagemResultado_StatusCnt], [ContagemResultado_HoraCnt] FROM [ContagemResultadoContagens] WITH (UPDLOCK) WHERE ([ContagemResultado_Codigo] = @ContagemResultado_Codigo) AND ([ContagemResultado_Ultima] = 1) ORDER BY [ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP007023,1,0,true,true )
             ,new CursorDef("P007024", "DELETE FROM [ContagemResultadoContagens]  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP007024)
             ,new CursorDef("P007025", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_PFBFS]=@ContagemResultado_PFBFS, [ContagemResultado_PFLFS]=@ContagemResultado_PFLFS, [ContagemResultado_Divergencia]=@ContagemResultado_Divergencia, [ContagemResultado_StatusCnt]=@ContagemResultado_StatusCnt  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP007025)
             ,new CursorDef("P007026", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_PFBFS]=@ContagemResultado_PFBFS, [ContagemResultado_PFLFS]=@ContagemResultado_PFLFS, [ContagemResultado_Divergencia]=@ContagemResultado_Divergencia, [ContagemResultado_StatusCnt]=@ContagemResultado_StatusCnt  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP007026)
             ,new CursorDef("P007027", "SELECT [Contagem_TipoUnidade], [Contagem_Metodologia], [Contagem_ContratadaCod], [Contagem_Demanda], [Contagem_PFB], [Contagem_PFL], [Contagem_Fator], [Contagem_PFBA], [Contagem_PFLA], [Contagem_Deflator], [Contagem_PFBD], [Contagem_PFLD], [Contagem_DataAtualizacao], [Contagem_Divergencia], [Contagem_Status], [Contagem_Codigo] FROM [Contagem] WITH (UPDLOCK) WHERE ([Contagem_Demanda] = @AV46Demanda) AND ([Contagem_TipoUnidade] = @TpUo_Codigo) AND ([Contagem_Metodologia] = @Metodologia_Codigo) AND ([Contagem_ContratadaCod] = @AV43Contratada_Codigo or [Contagem_ContratadaCod] = @AV95ContratadaFS_Codigo) ORDER BY [Contagem_Demanda] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP007027,1,0,true,false )
             ,new CursorDef("P007028", "UPDATE [Contagem] SET [Contagem_PFB]=@Contagem_PFB, [Contagem_PFL]=@Contagem_PFL, [Contagem_PFBA]=@Contagem_PFBA, [Contagem_PFLA]=@Contagem_PFLA, [Contagem_PFBD]=@Contagem_PFBD, [Contagem_PFLD]=@Contagem_PFLD, [Contagem_DataAtualizacao]=@Contagem_DataAtualizacao, [Contagem_Divergencia]=@Contagem_Divergencia, [Contagem_Status]=@Contagem_Status  WHERE [Contagem_Codigo] = @Contagem_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP007028)
             ,new CursorDef("P007029", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_DataHomologacao]=@ContagemResultado_DataHomologacao, [ContagemResultado_HoraEntrega]=@ContagemResultado_HoraEntrega, [ContagemResultado_DataEntrega]=@ContagemResultado_DataEntrega, [ContagemResultado_ValorPF]=@ContagemResultado_ValorPF  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP007029)
             ,new CursorDef("P007030", cmdBufferP007030,false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP007030,1,0,true,true )
             ,new CursorDef("P007031", cmdBufferP007031, GxErrorMask.GX_NOMASK,prmP007031)
             ,new CursorDef("P007032", "INSERT INTO [ContagemResultadoContagens]([ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt], [ContagemResultado_PFBFS], [ContagemResultado_PFLFS], [ContagemResultado_PFBFM], [ContagemResultado_PFLFM], [ContagemResultado_Divergencia], [ContagemResultado_ContadorFMCod], [ContagemResultado_NaoCnfCntCod], [ContagemResultado_StatusCnt], [ContagemResultadoContagens_Esforco], [ContagemResultado_Ultima], [ContagemResultado_Deflator], [ContagemResultado_TimeCnt], [ContagemResultado_ParecerTcn], [ContagemResultado_CstUntPrd], [ContagemResultado_Planilha], [ContagemResultado_NomePla], [ContagemResultado_TipoPla], [ContagemResultadoContagens_Prazo], [ContagemResultado_NvlCnt]) VALUES(@ContagemResultado_Codigo, @ContagemResultado_DataCnt, @ContagemResultado_HoraCnt, @ContagemResultado_PFBFS, @ContagemResultado_PFLFS, @ContagemResultado_PFBFM, @ContagemResultado_PFLFM, @ContagemResultado_Divergencia, @ContagemResultado_ContadorFMCod, @ContagemResultado_NaoCnfCntCod, @ContagemResultado_StatusCnt, @ContagemResultadoContagens_Esforco, @ContagemResultado_Ultima, @ContagemResultado_Deflator, convert( DATETIME, '17530101', 112 ), '', convert(int, 0), CONVERT(varbinary(1), ''), '', '', convert( DATETIME, '17530101', 112 ), convert(int, 0))", GxErrorMask.GX_NOMASK,prmP007032)
             ,new CursorDef("P007033", cmdBufferP007033, GxErrorMask.GX_NOMASK,prmP007033)
             ,new CursorDef("P007034", "SELECT TOP 1 [Contagem_DemandaCod], [Contagem_Divergencia], [Contagem_Status], [Contagem_Codigo] FROM [Contagem] WITH (UPDLOCK) WHERE [Contagem_DemandaCod] = @AV35ContagemResultado_Codigo ORDER BY [Contagem_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP007034,1,0,true,true )
             ,new CursorDef("P007035", "UPDATE [Contagem] SET [Contagem_Divergencia]=@Contagem_Divergencia, [Contagem_Status]=@Contagem_Status  WHERE [Contagem_Codigo] = @Contagem_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP007035)
             ,new CursorDef("P007036", "UPDATE [Contagem] SET [Contagem_Divergencia]=@Contagem_Divergencia, [Contagem_Status]=@Contagem_Status  WHERE [Contagem_Codigo] = @Contagem_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP007036)
             ,new CursorDef("P007037", "INSERT INTO [ContagemResultadoContagens]([ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt], [ContagemResultado_PFBFS], [ContagemResultado_PFLFS], [ContagemResultado_Divergencia], [ContagemResultado_ContadorFMCod], [ContagemResultado_NaoCnfCntCod], [ContagemResultado_StatusCnt], [ContagemResultadoContagens_Esforco], [ContagemResultado_Ultima], [ContagemResultado_Deflator], [ContagemResultado_TimeCnt], [ContagemResultado_PFBFM], [ContagemResultado_PFLFM], [ContagemResultado_ParecerTcn], [ContagemResultado_CstUntPrd], [ContagemResultado_Planilha], [ContagemResultado_NomePla], [ContagemResultado_TipoPla], [ContagemResultadoContagens_Prazo], [ContagemResultado_NvlCnt]) VALUES(@ContagemResultado_Codigo, @ContagemResultado_DataCnt, @ContagemResultado_HoraCnt, @ContagemResultado_PFBFS, @ContagemResultado_PFLFS, @ContagemResultado_Divergencia, @ContagemResultado_ContadorFMCod, @ContagemResultado_NaoCnfCntCod, @ContagemResultado_StatusCnt, @ContagemResultadoContagens_Esforco, @ContagemResultado_Ultima, @ContagemResultado_Deflator, convert( DATETIME, '17530101', 112 ), convert(int, 0), convert(int, 0), '', convert(int, 0), CONVERT(varbinary(1), ''), '', '', convert( DATETIME, '17530101', 112 ), convert(int, 0))", GxErrorMask.GX_NOMASK,prmP007037)
             ,new CursorDef("P007038", "SELECT [ContagemResultado_Codigo], [ContagemResultado_StatusDmn], [ContagemResultado_DataHomologacao] FROM [ContagemResultado] WITH (UPDLOCK) WHERE [ContagemResultado_Codigo] = @AV35ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP007038,1,0,true,true )
             ,new CursorDef("P007039", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_Ultima], [ContagemResultado_PFBFS], [ContagemResultado_PFLFS], [ContagemResultado_Divergencia], [ContagemResultado_StatusCnt], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt] FROM [ContagemResultadoContagens] WITH (UPDLOCK) WHERE ([ContagemResultado_Codigo] = @ContagemResultado_Codigo) AND ([ContagemResultado_Ultima] = 1) ORDER BY [ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP007039,1,0,true,true )
             ,new CursorDef("P007040", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_PFBFS]=@ContagemResultado_PFBFS, [ContagemResultado_PFLFS]=@ContagemResultado_PFLFS, [ContagemResultado_Divergencia]=@ContagemResultado_Divergencia, [ContagemResultado_StatusCnt]=@ContagemResultado_StatusCnt  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP007040)
             ,new CursorDef("P007041", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_PFBFS]=@ContagemResultado_PFBFS, [ContagemResultado_PFLFS]=@ContagemResultado_PFLFS, [ContagemResultado_Divergencia]=@ContagemResultado_Divergencia, [ContagemResultado_StatusCnt]=@ContagemResultado_StatusCnt  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP007041)
             ,new CursorDef("P007042", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_DataHomologacao]=@ContagemResultado_DataHomologacao  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP007042)
             ,new CursorDef("P007043", "SELECT TOP 1 [Contagem_Codigo], [Contagem_ContratadaCod], [Contagem_Demanda] FROM [Contagem] WITH (NOLOCK) WHERE [Contagem_Demanda] = @AV46Demanda and [Contagem_ContratadaCod] = @AV113AreaTrabalho_ContratadaUpdBslCod ORDER BY [Contagem_Demanda], [Contagem_ContratadaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP007043,1,0,true,true )
             ,new CursorDef("P007044", "INSERT INTO [Baseline]([Baseline_UserCod], [Baseline_ProjetoMelCod], [Contagem_Codigo], [Baseline_DataHomologacao], [Baseline_PFBAntes], [Baseline_PFBDepois], [Baseline_Antes], [Baseline_Depois], [Baseline_DataAtualizacao]) VALUES(@Baseline_UserCod, @Baseline_ProjetoMelCod, @Contagem_Codigo, @Baseline_DataHomologacao, convert(int, 0), convert(int, 0), '', '', convert( DATETIME, '17530101', 112 )); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP007044)
             ,new CursorDef("P007045", "SELECT [LogResponsavel_Codigo], [LogResponsavel_Owner], [LogResponsavel_DemandaCod] FROM [LogResponsavel] WITH (NOLOCK) WHERE [LogResponsavel_DemandaCod] = @AV35ContagemResultado_Codigo ORDER BY [LogResponsavel_DemandaCod], [LogResponsavel_Codigo] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP007045,100,0,true,false )
             ,new CursorDef("P007046", "SELECT TOP 1 [Sistema_AreaTrabalhoCod], [Sistema_Sigla], [Sistema_Codigo] FROM [Sistema] WITH (NOLOCK) WHERE ([Sistema_AreaTrabalhoCod] = @AV83WWPC_1Areatrabalho_codigo) AND ([Sistema_Sigla] = @AV78Sistema_Sigla) ORDER BY [Sistema_AreaTrabalhoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP007046,1,0,false,true )
             ,new CursorDef("P007047", "UPDATE [ContagemResultado] SET [ContagemResultado_OSVinculada]=@ContagemResultado_OSVinculada  WHERE [ContagemResultado_Codigo] = @AV35ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP007047)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((decimal[]) buf[7])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((decimal[]) buf[9])[0] = rslt.getDecimal(8) ;
                ((short[]) buf[10])[0] = rslt.getShort(9) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(9);
                ((String[]) buf[12])[0] = rslt.getString(10, 1) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(10);
                ((short[]) buf[14])[0] = rslt.getShort(11) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(11);
                ((short[]) buf[16])[0] = rslt.getShort(12) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(12);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((DateTime[]) buf[11])[0] = rslt.getGXDate(7) ;
                ((DateTime[]) buf[12])[0] = rslt.getGXDate(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[14])[0] = rslt.getGXDateTime(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[16])[0] = rslt.getGXDateTime(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                return;
             case 6 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((decimal[]) buf[8])[0] = rslt.getDecimal(7) ;
                ((decimal[]) buf[9])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((decimal[]) buf[11])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((decimal[]) buf[13])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((decimal[]) buf[15])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((String[]) buf[17])[0] = rslt.getString(12, 5) ;
                ((DateTime[]) buf[18])[0] = rslt.getGXDate(13) ;
                ((int[]) buf[19])[0] = rslt.getInt(14) ;
                ((short[]) buf[20])[0] = rslt.getShort(15) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(15);
                ((DateTime[]) buf[22])[0] = rslt.getGXDateTime(16) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(16);
                ((String[]) buf[24])[0] = rslt.getString(17, 10) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(17);
                ((String[]) buf[26])[0] = rslt.getString(18, 50) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(18);
                ((decimal[]) buf[28])[0] = rslt.getDecimal(19) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(19);
                ((String[]) buf[30])[0] = rslt.getLongVarchar(20) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(20);
                ((DateTime[]) buf[32])[0] = rslt.getGXDateTime(21) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(21);
                ((String[]) buf[34])[0] = rslt.getString(22, 1) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(22);
                ((DateTime[]) buf[36])[0] = rslt.getGXDateTime(23) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(23);
                ((String[]) buf[38])[0] = rslt.getBLOBFile(24, rslt.getString(17, 10), rslt.getString(18, 50)) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(24);
                return;
             case 12 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((decimal[]) buf[10])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((decimal[]) buf[14])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((short[]) buf[18])[0] = rslt.getShort(11) ;
                ((decimal[]) buf[19])[0] = rslt.getDecimal(12) ;
                ((DateTime[]) buf[20])[0] = rslt.getGXDateTime(13) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                ((int[]) buf[22])[0] = rslt.getInt(14) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(14);
                ((DateTime[]) buf[24])[0] = rslt.getGXDateTime(15) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(15);
                ((DateTime[]) buf[26])[0] = rslt.getGXDate(16) ;
                ((String[]) buf[27])[0] = rslt.getString(17, 5) ;
                return;
             case 17 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 18 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getVarchar(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((int[]) buf[20])[0] = rslt.getInt(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((String[]) buf[22])[0] = rslt.getString(13, 1) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((DateTime[]) buf[24])[0] = rslt.getGXDateTime(14) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((DateTime[]) buf[26])[0] = rslt.getGXDateTime(15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((short[]) buf[28])[0] = rslt.getShort(16) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((short[]) buf[30])[0] = rslt.getShort(17) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(17);
                ((DateTime[]) buf[32])[0] = rslt.getGXDate(18) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(18);
                ((decimal[]) buf[34])[0] = rslt.getDecimal(19) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(19);
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((decimal[]) buf[7])[0] = rslt.getDecimal(6) ;
                ((short[]) buf[8])[0] = rslt.getShort(7) ;
                ((String[]) buf[9])[0] = rslt.getString(8, 5) ;
                return;
             case 25 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((decimal[]) buf[10])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((decimal[]) buf[14])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((decimal[]) buf[16])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((decimal[]) buf[18])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((decimal[]) buf[20])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((decimal[]) buf[22])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(12);
                ((DateTime[]) buf[24])[0] = rslt.getGXDateTime(13) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(13);
                ((decimal[]) buf[26])[0] = rslt.getDecimal(14) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(14);
                ((String[]) buf[28])[0] = rslt.getString(15, 1) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(15);
                ((int[]) buf[30])[0] = rslt.getInt(16) ;
                return;
             case 28 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[4])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[8])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((decimal[]) buf[14])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((int[]) buf[16])[0] = rslt.getInt(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((String[]) buf[18])[0] = rslt.getString(10, 1) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((String[]) buf[20])[0] = rslt.getVarchar(11) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[22])[0] = rslt.getGXDate(12) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(12);
                ((decimal[]) buf[24])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(13);
                ((DateTime[]) buf[26])[0] = rslt.getGXDateTime(14) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(14);
                ((DateTime[]) buf[28])[0] = rslt.getGXDate(15) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(15);
                ((decimal[]) buf[30])[0] = rslt.getDecimal(16) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(16);
                ((decimal[]) buf[32])[0] = rslt.getDecimal(17) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(17);
                ((bool[]) buf[34])[0] = rslt.getBool(18) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(18);
                ((DateTime[]) buf[36])[0] = rslt.getGXDate(19) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(19);
                ((short[]) buf[38])[0] = rslt.getShort(20) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(20);
                ((bool[]) buf[40])[0] = rslt.getBool(21) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(21);
                ((int[]) buf[42])[0] = rslt.getInt(22) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(22);
                ((String[]) buf[44])[0] = rslt.getVarchar(23) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(23);
                ((String[]) buf[46])[0] = rslt.getVarchar(24) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(24);
                ((String[]) buf[48])[0] = rslt.getVarchar(25) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(25);
                ((int[]) buf[50])[0] = rslt.getInt(26) ;
                ((bool[]) buf[51])[0] = rslt.wasNull(26);
                ((short[]) buf[52])[0] = rslt.getShort(27) ;
                ((decimal[]) buf[53])[0] = rslt.getDecimal(28) ;
                ((bool[]) buf[54])[0] = rslt.wasNull(28);
                ((int[]) buf[55])[0] = rslt.getInt(29) ;
                ((bool[]) buf[56])[0] = rslt.wasNull(29);
                ((DateTime[]) buf[57])[0] = rslt.getGXDateTime(30) ;
                ((bool[]) buf[58])[0] = rslt.wasNull(30);
                ((DateTime[]) buf[59])[0] = rslt.getGXDateTime(31) ;
                ((bool[]) buf[60])[0] = rslt.wasNull(31);
                ((int[]) buf[61])[0] = rslt.getInt(32) ;
                ((bool[]) buf[62])[0] = rslt.wasNull(32);
                ((short[]) buf[63])[0] = rslt.getShort(33) ;
                ((bool[]) buf[64])[0] = rslt.wasNull(33);
                ((DateTime[]) buf[65])[0] = rslt.getGXDateTime(34) ;
                ((bool[]) buf[66])[0] = rslt.wasNull(34);
                ((DateTime[]) buf[67])[0] = rslt.getGXDateTime(35) ;
                ((bool[]) buf[68])[0] = rslt.wasNull(35);
                ((DateTime[]) buf[69])[0] = rslt.getGXDateTime(36) ;
                ((bool[]) buf[70])[0] = rslt.wasNull(36);
                ((DateTime[]) buf[71])[0] = rslt.getGXDateTime(37) ;
                ((bool[]) buf[72])[0] = rslt.wasNull(37);
                ((int[]) buf[73])[0] = rslt.getInt(38) ;
                ((bool[]) buf[74])[0] = rslt.wasNull(38);
                ((int[]) buf[75])[0] = rslt.getInt(39) ;
                ((bool[]) buf[76])[0] = rslt.wasNull(39);
                ((bool[]) buf[77])[0] = rslt.getBool(40) ;
                ((bool[]) buf[78])[0] = rslt.wasNull(40);
                ((decimal[]) buf[79])[0] = rslt.getDecimal(41) ;
                ((bool[]) buf[80])[0] = rslt.wasNull(41);
                ((decimal[]) buf[81])[0] = rslt.getDecimal(42) ;
                ((bool[]) buf[82])[0] = rslt.wasNull(42);
                ((int[]) buf[83])[0] = rslt.getInt(43) ;
                ((bool[]) buf[84])[0] = rslt.wasNull(43);
                ((DateTime[]) buf[85])[0] = rslt.getGXDateTime(44) ;
                ((bool[]) buf[86])[0] = rslt.wasNull(44);
                ((int[]) buf[87])[0] = rslt.getInt(45) ;
                ((bool[]) buf[88])[0] = rslt.wasNull(45);
                ((int[]) buf[89])[0] = rslt.getInt(46) ;
                ((bool[]) buf[90])[0] = rslt.wasNull(46);
                ((DateTime[]) buf[91])[0] = rslt.getGXDateTime(47) ;
                ((bool[]) buf[92])[0] = rslt.wasNull(47);
                ((DateTime[]) buf[93])[0] = rslt.getGXDateTime(48) ;
                ((bool[]) buf[94])[0] = rslt.wasNull(48);
                ((DateTime[]) buf[95])[0] = rslt.getGXDateTime(49) ;
                ((bool[]) buf[96])[0] = rslt.wasNull(49);
                ((short[]) buf[97])[0] = rslt.getShort(50) ;
                ((bool[]) buf[98])[0] = rslt.wasNull(50);
                ((short[]) buf[99])[0] = rslt.getShort(51) ;
                ((bool[]) buf[100])[0] = rslt.wasNull(51);
                ((decimal[]) buf[101])[0] = rslt.getDecimal(52) ;
                ((bool[]) buf[102])[0] = rslt.wasNull(52);
                ((decimal[]) buf[103])[0] = rslt.getDecimal(53) ;
                ((bool[]) buf[104])[0] = rslt.wasNull(53);
                ((decimal[]) buf[105])[0] = rslt.getDecimal(54) ;
                ((bool[]) buf[106])[0] = rslt.wasNull(54);
                ((bool[]) buf[107])[0] = rslt.getBool(55) ;
                ((bool[]) buf[108])[0] = rslt.wasNull(55);
                ((int[]) buf[109])[0] = rslt.getInt(56) ;
                ((bool[]) buf[110])[0] = rslt.wasNull(56);
                ((decimal[]) buf[111])[0] = rslt.getDecimal(57) ;
                ((bool[]) buf[112])[0] = rslt.wasNull(57);
                ((String[]) buf[113])[0] = rslt.getLongVarchar(58) ;
                ((bool[]) buf[114])[0] = rslt.wasNull(58);
                ((DateTime[]) buf[115])[0] = rslt.getGXDate(59) ;
                ((bool[]) buf[116])[0] = rslt.wasNull(59);
                ((int[]) buf[117])[0] = rslt.getInt(60) ;
                ((bool[]) buf[118])[0] = rslt.wasNull(60);
                ((int[]) buf[119])[0] = rslt.getInt(61) ;
                ((bool[]) buf[120])[0] = rslt.wasNull(61);
                ((int[]) buf[121])[0] = rslt.getInt(62) ;
                ((bool[]) buf[122])[0] = rslt.wasNull(62);
                ((decimal[]) buf[123])[0] = rslt.getDecimal(63) ;
                ((bool[]) buf[124])[0] = rslt.wasNull(63);
                ((decimal[]) buf[125])[0] = rslt.getDecimal(64) ;
                ((bool[]) buf[126])[0] = rslt.wasNull(64);
                ((bool[]) buf[127])[0] = rslt.getBool(65) ;
                ((bool[]) buf[128])[0] = rslt.wasNull(65);
                ((int[]) buf[129])[0] = rslt.getInt(66) ;
                ((bool[]) buf[130])[0] = rslt.wasNull(66);
                ((String[]) buf[131])[0] = rslt.getLongVarchar(67) ;
                ((bool[]) buf[132])[0] = rslt.wasNull(67);
                ((int[]) buf[133])[0] = rslt.getInt(68) ;
                ((String[]) buf[134])[0] = rslt.getLongVarchar(69) ;
                ((bool[]) buf[135])[0] = rslt.wasNull(69);
                ((int[]) buf[136])[0] = rslt.getInt(70) ;
                ((bool[]) buf[137])[0] = rslt.wasNull(70);
                ((int[]) buf[138])[0] = rslt.getInt(71) ;
                ((bool[]) buf[139])[0] = rslt.wasNull(71);
                ((String[]) buf[140])[0] = rslt.getVarchar(72) ;
                ((bool[]) buf[141])[0] = rslt.wasNull(72);
                ((String[]) buf[142])[0] = rslt.getVarchar(73) ;
                ((bool[]) buf[143])[0] = rslt.wasNull(73);
                ((bool[]) buf[144])[0] = rslt.getBool(74) ;
                ((bool[]) buf[145])[0] = rslt.wasNull(74);
                ((int[]) buf[146])[0] = rslt.getInt(75) ;
                ((bool[]) buf[147])[0] = rslt.wasNull(75);
                ((String[]) buf[148])[0] = rslt.getLongVarchar(76) ;
                ((bool[]) buf[149])[0] = rslt.wasNull(76);
                ((int[]) buf[150])[0] = rslt.getInt(77) ;
                ((bool[]) buf[151])[0] = rslt.wasNull(77);
                ((DateTime[]) buf[152])[0] = rslt.getGXDate(78) ;
                ((int[]) buf[153])[0] = rslt.getInt(79) ;
                return;
             case 29 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 31 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 32 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((decimal[]) buf[2])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                return;
             case 36 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 37 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((short[]) buf[7])[0] = rslt.getShort(6) ;
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(7) ;
                ((String[]) buf[9])[0] = rslt.getString(8, 5) ;
                return;
             case 41 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 42 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 43 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 44 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 25) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[5]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(2, (DateTime)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(3, (DateTime)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(4, (DateTime)parms[6]);
                }
                stmt.SetParameter(5, (int)parms[7]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(4, (DateTime)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(6, (decimal)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(7, (decimal)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 8 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(8, (decimal)parms[12]);
                }
                stmt.SetParameter(9, (decimal)parms[13]);
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 10 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(10, (String)parms[15]);
                }
                stmt.SetParameter(11, (int)parms[16]);
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 12 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(12, (int)parms[18]);
                }
                stmt.SetParameter(13, (short)parms[19]);
                stmt.SetParameter(14, (short)parms[20]);
                stmt.SetParameter(15, (bool)parms[21]);
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 16 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(16, (decimal)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 17 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(17, (decimal)parms[25]);
                }
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 18 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(18, (String)parms[27]);
                }
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 19 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(19, (String)parms[29]);
                }
                if ( (bool)parms[30] )
                {
                   stmt.setNull( 20 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(20, (String)parms[31]);
                }
                if ( (bool)parms[32] )
                {
                   stmt.setNull( 21 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(21, (DateTime)parms[33]);
                }
                if ( (bool)parms[34] )
                {
                   stmt.setNull( 22 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(22, (short)parms[35]);
                }
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(2, (DateTime)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 9 :
                stmt.SetParameter(1, (bool)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                stmt.SetParameter(3, (decimal)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[6]);
                }
                stmt.SetParameter(6, (int)parms[7]);
                stmt.SetParameter(7, (DateTime)parms[8]);
                stmt.SetParameter(8, (String)parms[9]);
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(2, (DateTime)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 11 :
                stmt.SetParameter(1, (bool)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                stmt.SetParameter(3, (decimal)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[6]);
                }
                stmt.SetParameter(6, (int)parms[7]);
                stmt.SetParameter(7, (DateTime)parms[8]);
                stmt.SetParameter(8, (String)parms[9]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (decimal)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(3, (decimal)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(4, (DateTime)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[9]);
                }
                stmt.SetParameter(6, (int)parms[10]);
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (decimal)parms[3]);
                }
                stmt.SetParameter(3, (short)parms[4]);
                stmt.SetParameter(4, (decimal)parms[5]);
                stmt.SetParameter(5, (int)parms[6]);
                stmt.SetParameter(6, (DateTime)parms[7]);
                stmt.SetParameter(7, (String)parms[8]);
                return;
             case 15 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (decimal)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(3, (decimal)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(4, (DateTime)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[9]);
                }
                stmt.SetParameter(6, (int)parms[10]);
                return;
             case 16 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (decimal)parms[3]);
                }
                stmt.SetParameter(3, (short)parms[4]);
                stmt.SetParameter(4, (decimal)parms[5]);
                stmt.SetParameter(5, (int)parms[6]);
                stmt.SetParameter(6, (DateTime)parms[7]);
                stmt.SetParameter(7, (String)parms[8]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(2, (bool)parms[2]);
                }
                return;
             case 20 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 21 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 22 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                return;
             case 23 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (decimal)parms[3]);
                }
                stmt.SetParameter(3, (decimal)parms[4]);
                stmt.SetParameter(4, (short)parms[5]);
                stmt.SetParameter(5, (int)parms[6]);
                stmt.SetParameter(6, (DateTime)parms[7]);
                stmt.SetParameter(7, (String)parms[8]);
                return;
             case 24 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (decimal)parms[3]);
                }
                stmt.SetParameter(3, (decimal)parms[4]);
                stmt.SetParameter(4, (short)parms[5]);
                stmt.SetParameter(5, (int)parms[6]);
                stmt.SetParameter(6, (DateTime)parms[7]);
                stmt.SetParameter(7, (String)parms[8]);
                return;
             case 25 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[4]);
                }
                stmt.SetParameter(4, (int)parms[5]);
                stmt.SetParameter(5, (int)parms[6]);
                return;
             case 26 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (decimal)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(3, (decimal)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 6 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(6, (decimal)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 7 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(7, (DateTime)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 8 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(8, (decimal)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 9 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[17]);
                }
                stmt.SetParameter(10, (int)parms[18]);
                return;
             case 27 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(2, (DateTime)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(3, (DateTime)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(4, (DateTime)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[9]);
                }
                stmt.SetParameter(6, (int)parms[10]);
                return;
             case 28 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 29 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(2, (DateTime)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 8 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(8, (bool)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 10 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(10, (String)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 11 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(11, (String)parms[20]);
                }
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 12 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(12, (int)parms[22]);
                }
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 13 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(13, (int)parms[24]);
                }
                if ( (bool)parms[25] )
                {
                   stmt.setNull( 14 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(14, (String)parms[26]);
                }
                stmt.SetParameter(15, (int)parms[27]);
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 16 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(16, (decimal)parms[29]);
                }
                if ( (bool)parms[30] )
                {
                   stmt.setNull( 17 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(17, (String)parms[31]);
                }
                if ( (bool)parms[32] )
                {
                   stmt.setNull( 18 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(18, (int)parms[33]);
                }
                if ( (bool)parms[34] )
                {
                   stmt.setNull( 19 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(19, (bool)parms[35]);
                }
                if ( (bool)parms[36] )
                {
                   stmt.setNull( 20 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(20, (int)parms[37]);
                }
                if ( (bool)parms[38] )
                {
                   stmt.setNull( 21 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(21, (decimal)parms[39]);
                }
                if ( (bool)parms[40] )
                {
                   stmt.setNull( 22 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(22, (decimal)parms[41]);
                }
                if ( (bool)parms[42] )
                {
                   stmt.setNull( 23 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(23, (int)parms[43]);
                }
                if ( (bool)parms[44] )
                {
                   stmt.setNull( 24 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(24, (int)parms[45]);
                }
                if ( (bool)parms[46] )
                {
                   stmt.setNull( 25 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(25, (DateTime)parms[47]);
                }
                if ( (bool)parms[48] )
                {
                   stmt.setNull( 26 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(26, (int)parms[49]);
                }
                if ( (bool)parms[50] )
                {
                   stmt.setNull( 27 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(27, (int)parms[51]);
                }
                if ( (bool)parms[52] )
                {
                   stmt.setNull( 28 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(28, (String)parms[53]);
                }
                if ( (bool)parms[54] )
                {
                   stmt.setNull( 29 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(29, (DateTime)parms[55]);
                }
                if ( (bool)parms[56] )
                {
                   stmt.setNull( 30 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(30, (String)parms[57]);
                }
                if ( (bool)parms[58] )
                {
                   stmt.setNull( 31 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(31, (decimal)parms[59]);
                }
                if ( (bool)parms[60] )
                {
                   stmt.setNull( 32 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(32, (int)parms[61]);
                }
                if ( (bool)parms[62] )
                {
                   stmt.setNull( 33 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(33, (bool)parms[63]);
                }
                if ( (bool)parms[64] )
                {
                   stmt.setNull( 34 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(34, (decimal)parms[65]);
                }
                if ( (bool)parms[66] )
                {
                   stmt.setNull( 35 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(35, (decimal)parms[67]);
                }
                if ( (bool)parms[68] )
                {
                   stmt.setNull( 36 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(36, (decimal)parms[69]);
                }
                if ( (bool)parms[70] )
                {
                   stmt.setNull( 37 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(37, (short)parms[71]);
                }
                if ( (bool)parms[72] )
                {
                   stmt.setNull( 38 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(38, (short)parms[73]);
                }
                if ( (bool)parms[74] )
                {
                   stmt.setNull( 39 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(39, (DateTime)parms[75]);
                }
                if ( (bool)parms[76] )
                {
                   stmt.setNull( 40 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(40, (DateTime)parms[77]);
                }
                if ( (bool)parms[78] )
                {
                   stmt.setNull( 41 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(41, (DateTime)parms[79]);
                }
                if ( (bool)parms[80] )
                {
                   stmt.setNull( 42 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(42, (DateTime)parms[81]);
                }
                if ( (bool)parms[82] )
                {
                   stmt.setNull( 43 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(43, (int)parms[83]);
                }
                if ( (bool)parms[84] )
                {
                   stmt.setNull( 44 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(44, (int)parms[85]);
                }
                if ( (bool)parms[86] )
                {
                   stmt.setNull( 45 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(45, (DateTime)parms[87]);
                }
                if ( (bool)parms[88] )
                {
                   stmt.setNull( 46 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(46, (int)parms[89]);
                }
                if ( (bool)parms[90] )
                {
                   stmt.setNull( 47 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(47, (decimal)parms[91]);
                }
                if ( (bool)parms[92] )
                {
                   stmt.setNull( 48 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(48, (decimal)parms[93]);
                }
                if ( (bool)parms[94] )
                {
                   stmt.setNull( 49 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(49, (int)parms[95]);
                }
                if ( (bool)parms[96] )
                {
                   stmt.setNull( 50 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(50, (bool)parms[97]);
                }
                if ( (bool)parms[98] )
                {
                   stmt.setNull( 51 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(51, (int)parms[99]);
                }
                if ( (bool)parms[100] )
                {
                   stmt.setNull( 52 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(52, (int)parms[101]);
                }
                if ( (bool)parms[102] )
                {
                   stmt.setNull( 53 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(53, (DateTime)parms[103]);
                }
                if ( (bool)parms[104] )
                {
                   stmt.setNull( 54 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(54, (DateTime)parms[105]);
                }
                if ( (bool)parms[106] )
                {
                   stmt.setNull( 55 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(55, (DateTime)parms[107]);
                }
                if ( (bool)parms[108] )
                {
                   stmt.setNull( 56 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(56, (DateTime)parms[109]);
                }
                if ( (bool)parms[110] )
                {
                   stmt.setNull( 57 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(57, (short)parms[111]);
                }
                if ( (bool)parms[112] )
                {
                   stmt.setNull( 58 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(58, (int)parms[113]);
                }
                if ( (bool)parms[114] )
                {
                   stmt.setNull( 59 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(59, (DateTime)parms[115]);
                }
                if ( (bool)parms[116] )
                {
                   stmt.setNull( 60 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(60, (DateTime)parms[117]);
                }
                if ( (bool)parms[118] )
                {
                   stmt.setNull( 61 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(61, (int)parms[119]);
                }
                if ( (bool)parms[120] )
                {
                   stmt.setNull( 62 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(62, (int)parms[121]);
                }
                if ( (bool)parms[122] )
                {
                   stmt.setNull( 63 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(63, (decimal)parms[123]);
                }
                stmt.SetParameter(64, (short)parms[124]);
                if ( (bool)parms[125] )
                {
                   stmt.setNull( 65 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(65, (int)parms[126]);
                }
                if ( (bool)parms[127] )
                {
                   stmt.setNull( 66 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(66, (String)parms[128]);
                }
                if ( (bool)parms[129] )
                {
                   stmt.setNull( 67 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(67, (String)parms[130]);
                }
                if ( (bool)parms[131] )
                {
                   stmt.setNull( 68 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(68, (String)parms[132]);
                }
                if ( (bool)parms[133] )
                {
                   stmt.setNull( 69 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(69, (int)parms[134]);
                }
                if ( (bool)parms[135] )
                {
                   stmt.setNull( 70 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(70, (bool)parms[136]);
                }
                if ( (bool)parms[137] )
                {
                   stmt.setNull( 71 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(71, (short)parms[138]);
                }
                if ( (bool)parms[139] )
                {
                   stmt.setNull( 72 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(72, (DateTime)parms[140]);
                }
                if ( (bool)parms[141] )
                {
                   stmt.setNull( 73 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(73, (bool)parms[142]);
                }
                if ( (bool)parms[143] )
                {
                   stmt.setNull( 74 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(74, (decimal)parms[144]);
                }
                if ( (bool)parms[145] )
                {
                   stmt.setNull( 75 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(75, (decimal)parms[146]);
                }
                if ( (bool)parms[147] )
                {
                   stmt.setNull( 76 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(76, (DateTime)parms[148]);
                }
                if ( (bool)parms[149] )
                {
                   stmt.setNull( 77 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(77, (DateTime)parms[150]);
                }
                if ( (bool)parms[151] )
                {
                   stmt.setNull( 78 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(78, (decimal)parms[152]);
                }
                return;
       }
       setparameters30( cursor, stmt, parms) ;
    }

    public void setparameters30( int cursor ,
                                 IFieldSetter stmt ,
                                 Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 30 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(6, (decimal)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(7, (decimal)parms[10]);
                }
                stmt.SetParameter(8, (decimal)parms[11]);
                stmt.SetParameter(9, (int)parms[12]);
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 10 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(10, (int)parms[14]);
                }
                stmt.SetParameter(11, (short)parms[15]);
                stmt.SetParameter(12, (short)parms[16]);
                stmt.SetParameter(13, (bool)parms[17]);
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 14 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(14, (decimal)parms[19]);
                }
                return;
             case 31 :
                stmt.SetParameter(1, (int)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                stmt.SetParameter(4, (DateTime)parms[5]);
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 7 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(7, (decimal)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 8 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(8, (decimal)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 9 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 10 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(10, (int)parms[17]);
                }
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 11 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(11, (int)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 12 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(12, (decimal)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 13 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(13, (int)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 14 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(14, (decimal)parms[25]);
                }
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 15 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(15, (int)parms[27]);
                }
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 16 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(16, (String)parms[29]);
                }
                if ( (bool)parms[30] )
                {
                   stmt.setNull( 17 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(17, (int)parms[31]);
                }
                if ( (bool)parms[32] )
                {
                   stmt.setNull( 18 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(18, (int)parms[33]);
                }
                if ( (bool)parms[34] )
                {
                   stmt.setNull( 19 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(19, (int)parms[35]);
                }
                if ( (bool)parms[36] )
                {
                   stmt.setNull( 20 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(20, (int)parms[37]);
                }
                if ( (bool)parms[38] )
                {
                   stmt.setNull( 21 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(21, (String)parms[39]);
                }
                if ( (bool)parms[40] )
                {
                   stmt.setNull( 22 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(22, (int)parms[41]);
                }
                if ( (bool)parms[42] )
                {
                   stmt.setNull( 23 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(23, (DateTime)parms[43]);
                }
                return;
             case 32 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 33 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 34 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 35 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[6]);
                }
                stmt.SetParameter(6, (decimal)parms[7]);
                stmt.SetParameter(7, (int)parms[8]);
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 8 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(8, (int)parms[10]);
                }
                stmt.SetParameter(9, (short)parms[11]);
                stmt.SetParameter(10, (short)parms[12]);
                stmt.SetParameter(11, (bool)parms[13]);
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 12 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(12, (decimal)parms[15]);
                }
                return;
             case 36 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 37 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 38 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (decimal)parms[3]);
                }
                stmt.SetParameter(3, (decimal)parms[4]);
                stmt.SetParameter(4, (short)parms[5]);
                stmt.SetParameter(5, (int)parms[6]);
                stmt.SetParameter(6, (DateTime)parms[7]);
                stmt.SetParameter(7, (String)parms[8]);
                return;
             case 39 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (decimal)parms[3]);
                }
                stmt.SetParameter(3, (decimal)parms[4]);
                stmt.SetParameter(4, (short)parms[5]);
                stmt.SetParameter(5, (int)parms[6]);
                stmt.SetParameter(6, (DateTime)parms[7]);
                stmt.SetParameter(7, (String)parms[8]);
                return;
             case 40 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(2, (DateTime)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 41 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 42 :
                stmt.SetParameter(1, (int)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[2]);
                }
                stmt.SetParameter(3, (int)parms[3]);
                stmt.SetParameterDatetime(4, (DateTime)parms[4]);
                return;
             case 43 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 44 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 45 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
