/*
               File: type_SdtUsuarioPerfil
        Description: Usuario x Perfil
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:7:0.17
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "UsuarioPerfil" )]
   [XmlType(TypeName =  "UsuarioPerfil" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtUsuarioPerfil : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtUsuarioPerfil( )
      {
         /* Constructor for serialization */
         gxTv_SdtUsuarioPerfil_Usuario_pessoanom = "";
         gxTv_SdtUsuarioPerfil_Usuario_nome = "";
         gxTv_SdtUsuarioPerfil_Usuario_usergamguid = "";
         gxTv_SdtUsuarioPerfil_Perfil_nome = "";
         gxTv_SdtUsuarioPerfil_Mode = "";
         gxTv_SdtUsuarioPerfil_Usuario_pessoanom_Z = "";
         gxTv_SdtUsuarioPerfil_Usuario_nome_Z = "";
         gxTv_SdtUsuarioPerfil_Usuario_usergamguid_Z = "";
         gxTv_SdtUsuarioPerfil_Perfil_nome_Z = "";
      }

      public SdtUsuarioPerfil( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV1Usuario_Codigo ,
                        int AV3Perfil_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV1Usuario_Codigo,(int)AV3Perfil_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"Usuario_Codigo", typeof(int)}, new Object[]{"Perfil_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "UsuarioPerfil");
         metadata.Set("BT", "UsuarioPerfil");
         metadata.Set("PK", "[ \"Usuario_Codigo\",\"Perfil_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"Perfil_Codigo\" ],\"FKMap\":[  ] },{ \"FK\":[ \"Usuario_Codigo\" ],\"FKMap\":[  ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_pessoacod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_pessoanom_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_nome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_usergamguid_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Perfil_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Perfil_areatrabalhocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Perfil_nome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Perfil_tipo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Perfil_gamid_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuarioperfil_display_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuarioperfil_insert_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuarioperfil_update_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuarioperfil_delete_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Perfil_ativo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_pessoanom_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_nome_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuarioperfil_display_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuarioperfil_insert_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuarioperfil_update_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuarioperfil_delete_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtUsuarioPerfil deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtUsuarioPerfil)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtUsuarioPerfil obj ;
         obj = this;
         obj.gxTpr_Usuario_codigo = deserialized.gxTpr_Usuario_codigo;
         obj.gxTpr_Usuario_pessoacod = deserialized.gxTpr_Usuario_pessoacod;
         obj.gxTpr_Usuario_pessoanom = deserialized.gxTpr_Usuario_pessoanom;
         obj.gxTpr_Usuario_nome = deserialized.gxTpr_Usuario_nome;
         obj.gxTpr_Usuario_usergamguid = deserialized.gxTpr_Usuario_usergamguid;
         obj.gxTpr_Perfil_codigo = deserialized.gxTpr_Perfil_codigo;
         obj.gxTpr_Perfil_areatrabalhocod = deserialized.gxTpr_Perfil_areatrabalhocod;
         obj.gxTpr_Perfil_nome = deserialized.gxTpr_Perfil_nome;
         obj.gxTpr_Perfil_tipo = deserialized.gxTpr_Perfil_tipo;
         obj.gxTpr_Perfil_gamid = deserialized.gxTpr_Perfil_gamid;
         obj.gxTpr_Usuarioperfil_display = deserialized.gxTpr_Usuarioperfil_display;
         obj.gxTpr_Usuarioperfil_insert = deserialized.gxTpr_Usuarioperfil_insert;
         obj.gxTpr_Usuarioperfil_update = deserialized.gxTpr_Usuarioperfil_update;
         obj.gxTpr_Usuarioperfil_delete = deserialized.gxTpr_Usuarioperfil_delete;
         obj.gxTpr_Perfil_ativo = deserialized.gxTpr_Perfil_ativo;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Usuario_codigo_Z = deserialized.gxTpr_Usuario_codigo_Z;
         obj.gxTpr_Usuario_pessoacod_Z = deserialized.gxTpr_Usuario_pessoacod_Z;
         obj.gxTpr_Usuario_pessoanom_Z = deserialized.gxTpr_Usuario_pessoanom_Z;
         obj.gxTpr_Usuario_nome_Z = deserialized.gxTpr_Usuario_nome_Z;
         obj.gxTpr_Usuario_usergamguid_Z = deserialized.gxTpr_Usuario_usergamguid_Z;
         obj.gxTpr_Perfil_codigo_Z = deserialized.gxTpr_Perfil_codigo_Z;
         obj.gxTpr_Perfil_areatrabalhocod_Z = deserialized.gxTpr_Perfil_areatrabalhocod_Z;
         obj.gxTpr_Perfil_nome_Z = deserialized.gxTpr_Perfil_nome_Z;
         obj.gxTpr_Perfil_tipo_Z = deserialized.gxTpr_Perfil_tipo_Z;
         obj.gxTpr_Perfil_gamid_Z = deserialized.gxTpr_Perfil_gamid_Z;
         obj.gxTpr_Usuarioperfil_display_Z = deserialized.gxTpr_Usuarioperfil_display_Z;
         obj.gxTpr_Usuarioperfil_insert_Z = deserialized.gxTpr_Usuarioperfil_insert_Z;
         obj.gxTpr_Usuarioperfil_update_Z = deserialized.gxTpr_Usuarioperfil_update_Z;
         obj.gxTpr_Usuarioperfil_delete_Z = deserialized.gxTpr_Usuarioperfil_delete_Z;
         obj.gxTpr_Perfil_ativo_Z = deserialized.gxTpr_Perfil_ativo_Z;
         obj.gxTpr_Usuario_pessoanom_N = deserialized.gxTpr_Usuario_pessoanom_N;
         obj.gxTpr_Usuario_nome_N = deserialized.gxTpr_Usuario_nome_N;
         obj.gxTpr_Usuarioperfil_display_N = deserialized.gxTpr_Usuarioperfil_display_N;
         obj.gxTpr_Usuarioperfil_insert_N = deserialized.gxTpr_Usuarioperfil_insert_N;
         obj.gxTpr_Usuarioperfil_update_N = deserialized.gxTpr_Usuarioperfil_update_N;
         obj.gxTpr_Usuarioperfil_delete_N = deserialized.gxTpr_Usuarioperfil_delete_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Codigo") )
               {
                  gxTv_SdtUsuarioPerfil_Usuario_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_PessoaCod") )
               {
                  gxTv_SdtUsuarioPerfil_Usuario_pessoacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_PessoaNom") )
               {
                  gxTv_SdtUsuarioPerfil_Usuario_pessoanom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Nome") )
               {
                  gxTv_SdtUsuarioPerfil_Usuario_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_UserGamGuid") )
               {
                  gxTv_SdtUsuarioPerfil_Usuario_usergamguid = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfil_Codigo") )
               {
                  gxTv_SdtUsuarioPerfil_Perfil_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfil_AreaTrabalhoCod") )
               {
                  gxTv_SdtUsuarioPerfil_Perfil_areatrabalhocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfil_Nome") )
               {
                  gxTv_SdtUsuarioPerfil_Perfil_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfil_Tipo") )
               {
                  gxTv_SdtUsuarioPerfil_Perfil_tipo = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfil_GamId") )
               {
                  gxTv_SdtUsuarioPerfil_Perfil_gamid = (long)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UsuarioPerfil_Display") )
               {
                  gxTv_SdtUsuarioPerfil_Usuarioperfil_display = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UsuarioPerfil_Insert") )
               {
                  gxTv_SdtUsuarioPerfil_Usuarioperfil_insert = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UsuarioPerfil_Update") )
               {
                  gxTv_SdtUsuarioPerfil_Usuarioperfil_update = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UsuarioPerfil_Delete") )
               {
                  gxTv_SdtUsuarioPerfil_Usuarioperfil_delete = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfil_Ativo") )
               {
                  gxTv_SdtUsuarioPerfil_Perfil_ativo = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtUsuarioPerfil_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtUsuarioPerfil_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Codigo_Z") )
               {
                  gxTv_SdtUsuarioPerfil_Usuario_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_PessoaCod_Z") )
               {
                  gxTv_SdtUsuarioPerfil_Usuario_pessoacod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_PessoaNom_Z") )
               {
                  gxTv_SdtUsuarioPerfil_Usuario_pessoanom_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Nome_Z") )
               {
                  gxTv_SdtUsuarioPerfil_Usuario_nome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_UserGamGuid_Z") )
               {
                  gxTv_SdtUsuarioPerfil_Usuario_usergamguid_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfil_Codigo_Z") )
               {
                  gxTv_SdtUsuarioPerfil_Perfil_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfil_AreaTrabalhoCod_Z") )
               {
                  gxTv_SdtUsuarioPerfil_Perfil_areatrabalhocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfil_Nome_Z") )
               {
                  gxTv_SdtUsuarioPerfil_Perfil_nome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfil_Tipo_Z") )
               {
                  gxTv_SdtUsuarioPerfil_Perfil_tipo_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfil_GamId_Z") )
               {
                  gxTv_SdtUsuarioPerfil_Perfil_gamid_Z = (long)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UsuarioPerfil_Display_Z") )
               {
                  gxTv_SdtUsuarioPerfil_Usuarioperfil_display_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UsuarioPerfil_Insert_Z") )
               {
                  gxTv_SdtUsuarioPerfil_Usuarioperfil_insert_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UsuarioPerfil_Update_Z") )
               {
                  gxTv_SdtUsuarioPerfil_Usuarioperfil_update_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UsuarioPerfil_Delete_Z") )
               {
                  gxTv_SdtUsuarioPerfil_Usuarioperfil_delete_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfil_Ativo_Z") )
               {
                  gxTv_SdtUsuarioPerfil_Perfil_ativo_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_PessoaNom_N") )
               {
                  gxTv_SdtUsuarioPerfil_Usuario_pessoanom_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Nome_N") )
               {
                  gxTv_SdtUsuarioPerfil_Usuario_nome_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UsuarioPerfil_Display_N") )
               {
                  gxTv_SdtUsuarioPerfil_Usuarioperfil_display_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UsuarioPerfil_Insert_N") )
               {
                  gxTv_SdtUsuarioPerfil_Usuarioperfil_insert_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UsuarioPerfil_Update_N") )
               {
                  gxTv_SdtUsuarioPerfil_Usuarioperfil_update_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UsuarioPerfil_Delete_N") )
               {
                  gxTv_SdtUsuarioPerfil_Usuarioperfil_delete_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "UsuarioPerfil";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Usuario_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuarioPerfil_Usuario_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Usuario_PessoaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuarioPerfil_Usuario_pessoacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Usuario_PessoaNom", StringUtil.RTrim( gxTv_SdtUsuarioPerfil_Usuario_pessoanom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Usuario_Nome", StringUtil.RTrim( gxTv_SdtUsuarioPerfil_Usuario_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Usuario_UserGamGuid", StringUtil.RTrim( gxTv_SdtUsuarioPerfil_Usuario_usergamguid));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Perfil_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuarioPerfil_Perfil_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Perfil_AreaTrabalhoCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuarioPerfil_Perfil_areatrabalhocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Perfil_Nome", StringUtil.RTrim( gxTv_SdtUsuarioPerfil_Perfil_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Perfil_Tipo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuarioPerfil_Perfil_tipo), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Perfil_GamId", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuarioPerfil_Perfil_gamid), 12, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("UsuarioPerfil_Display", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtUsuarioPerfil_Usuarioperfil_display)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("UsuarioPerfil_Insert", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtUsuarioPerfil_Usuarioperfil_insert)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("UsuarioPerfil_Update", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtUsuarioPerfil_Usuarioperfil_update)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("UsuarioPerfil_Delete", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtUsuarioPerfil_Usuarioperfil_delete)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Perfil_Ativo", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtUsuarioPerfil_Perfil_ativo)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtUsuarioPerfil_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuarioPerfil_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuarioPerfil_Usuario_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_PessoaCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuarioPerfil_Usuario_pessoacod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_PessoaNom_Z", StringUtil.RTrim( gxTv_SdtUsuarioPerfil_Usuario_pessoanom_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_Nome_Z", StringUtil.RTrim( gxTv_SdtUsuarioPerfil_Usuario_nome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_UserGamGuid_Z", StringUtil.RTrim( gxTv_SdtUsuarioPerfil_Usuario_usergamguid_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Perfil_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuarioPerfil_Perfil_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Perfil_AreaTrabalhoCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuarioPerfil_Perfil_areatrabalhocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Perfil_Nome_Z", StringUtil.RTrim( gxTv_SdtUsuarioPerfil_Perfil_nome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Perfil_Tipo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuarioPerfil_Perfil_tipo_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Perfil_GamId_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuarioPerfil_Perfil_gamid_Z), 12, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("UsuarioPerfil_Display_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtUsuarioPerfil_Usuarioperfil_display_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("UsuarioPerfil_Insert_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtUsuarioPerfil_Usuarioperfil_insert_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("UsuarioPerfil_Update_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtUsuarioPerfil_Usuarioperfil_update_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("UsuarioPerfil_Delete_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtUsuarioPerfil_Usuarioperfil_delete_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Perfil_Ativo_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtUsuarioPerfil_Perfil_ativo_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_PessoaNom_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuarioPerfil_Usuario_pessoanom_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_Nome_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuarioPerfil_Usuario_nome_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("UsuarioPerfil_Display_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuarioPerfil_Usuarioperfil_display_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("UsuarioPerfil_Insert_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuarioPerfil_Usuarioperfil_insert_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("UsuarioPerfil_Update_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuarioPerfil_Usuarioperfil_update_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("UsuarioPerfil_Delete_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuarioPerfil_Usuarioperfil_delete_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Usuario_Codigo", gxTv_SdtUsuarioPerfil_Usuario_codigo, false);
         AddObjectProperty("Usuario_PessoaCod", gxTv_SdtUsuarioPerfil_Usuario_pessoacod, false);
         AddObjectProperty("Usuario_PessoaNom", gxTv_SdtUsuarioPerfil_Usuario_pessoanom, false);
         AddObjectProperty("Usuario_Nome", gxTv_SdtUsuarioPerfil_Usuario_nome, false);
         AddObjectProperty("Usuario_UserGamGuid", gxTv_SdtUsuarioPerfil_Usuario_usergamguid, false);
         AddObjectProperty("Perfil_Codigo", gxTv_SdtUsuarioPerfil_Perfil_codigo, false);
         AddObjectProperty("Perfil_AreaTrabalhoCod", gxTv_SdtUsuarioPerfil_Perfil_areatrabalhocod, false);
         AddObjectProperty("Perfil_Nome", gxTv_SdtUsuarioPerfil_Perfil_nome, false);
         AddObjectProperty("Perfil_Tipo", gxTv_SdtUsuarioPerfil_Perfil_tipo, false);
         AddObjectProperty("Perfil_GamId", gxTv_SdtUsuarioPerfil_Perfil_gamid, false);
         AddObjectProperty("UsuarioPerfil_Display", gxTv_SdtUsuarioPerfil_Usuarioperfil_display, false);
         AddObjectProperty("UsuarioPerfil_Insert", gxTv_SdtUsuarioPerfil_Usuarioperfil_insert, false);
         AddObjectProperty("UsuarioPerfil_Update", gxTv_SdtUsuarioPerfil_Usuarioperfil_update, false);
         AddObjectProperty("UsuarioPerfil_Delete", gxTv_SdtUsuarioPerfil_Usuarioperfil_delete, false);
         AddObjectProperty("Perfil_Ativo", gxTv_SdtUsuarioPerfil_Perfil_ativo, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtUsuarioPerfil_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtUsuarioPerfil_Initialized, false);
            AddObjectProperty("Usuario_Codigo_Z", gxTv_SdtUsuarioPerfil_Usuario_codigo_Z, false);
            AddObjectProperty("Usuario_PessoaCod_Z", gxTv_SdtUsuarioPerfil_Usuario_pessoacod_Z, false);
            AddObjectProperty("Usuario_PessoaNom_Z", gxTv_SdtUsuarioPerfil_Usuario_pessoanom_Z, false);
            AddObjectProperty("Usuario_Nome_Z", gxTv_SdtUsuarioPerfil_Usuario_nome_Z, false);
            AddObjectProperty("Usuario_UserGamGuid_Z", gxTv_SdtUsuarioPerfil_Usuario_usergamguid_Z, false);
            AddObjectProperty("Perfil_Codigo_Z", gxTv_SdtUsuarioPerfil_Perfil_codigo_Z, false);
            AddObjectProperty("Perfil_AreaTrabalhoCod_Z", gxTv_SdtUsuarioPerfil_Perfil_areatrabalhocod_Z, false);
            AddObjectProperty("Perfil_Nome_Z", gxTv_SdtUsuarioPerfil_Perfil_nome_Z, false);
            AddObjectProperty("Perfil_Tipo_Z", gxTv_SdtUsuarioPerfil_Perfil_tipo_Z, false);
            AddObjectProperty("Perfil_GamId_Z", gxTv_SdtUsuarioPerfil_Perfil_gamid_Z, false);
            AddObjectProperty("UsuarioPerfil_Display_Z", gxTv_SdtUsuarioPerfil_Usuarioperfil_display_Z, false);
            AddObjectProperty("UsuarioPerfil_Insert_Z", gxTv_SdtUsuarioPerfil_Usuarioperfil_insert_Z, false);
            AddObjectProperty("UsuarioPerfil_Update_Z", gxTv_SdtUsuarioPerfil_Usuarioperfil_update_Z, false);
            AddObjectProperty("UsuarioPerfil_Delete_Z", gxTv_SdtUsuarioPerfil_Usuarioperfil_delete_Z, false);
            AddObjectProperty("Perfil_Ativo_Z", gxTv_SdtUsuarioPerfil_Perfil_ativo_Z, false);
            AddObjectProperty("Usuario_PessoaNom_N", gxTv_SdtUsuarioPerfil_Usuario_pessoanom_N, false);
            AddObjectProperty("Usuario_Nome_N", gxTv_SdtUsuarioPerfil_Usuario_nome_N, false);
            AddObjectProperty("UsuarioPerfil_Display_N", gxTv_SdtUsuarioPerfil_Usuarioperfil_display_N, false);
            AddObjectProperty("UsuarioPerfil_Insert_N", gxTv_SdtUsuarioPerfil_Usuarioperfil_insert_N, false);
            AddObjectProperty("UsuarioPerfil_Update_N", gxTv_SdtUsuarioPerfil_Usuarioperfil_update_N, false);
            AddObjectProperty("UsuarioPerfil_Delete_N", gxTv_SdtUsuarioPerfil_Usuarioperfil_delete_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Usuario_Codigo" )]
      [  XmlElement( ElementName = "Usuario_Codigo"   )]
      public int gxTpr_Usuario_codigo
      {
         get {
            return gxTv_SdtUsuarioPerfil_Usuario_codigo ;
         }

         set {
            if ( gxTv_SdtUsuarioPerfil_Usuario_codigo != value )
            {
               gxTv_SdtUsuarioPerfil_Mode = "INS";
               this.gxTv_SdtUsuarioPerfil_Usuario_codigo_Z_SetNull( );
               this.gxTv_SdtUsuarioPerfil_Usuario_pessoacod_Z_SetNull( );
               this.gxTv_SdtUsuarioPerfil_Usuario_pessoanom_Z_SetNull( );
               this.gxTv_SdtUsuarioPerfil_Usuario_nome_Z_SetNull( );
               this.gxTv_SdtUsuarioPerfil_Usuario_usergamguid_Z_SetNull( );
               this.gxTv_SdtUsuarioPerfil_Perfil_codigo_Z_SetNull( );
               this.gxTv_SdtUsuarioPerfil_Perfil_areatrabalhocod_Z_SetNull( );
               this.gxTv_SdtUsuarioPerfil_Perfil_nome_Z_SetNull( );
               this.gxTv_SdtUsuarioPerfil_Perfil_tipo_Z_SetNull( );
               this.gxTv_SdtUsuarioPerfil_Perfil_gamid_Z_SetNull( );
               this.gxTv_SdtUsuarioPerfil_Usuarioperfil_display_Z_SetNull( );
               this.gxTv_SdtUsuarioPerfil_Usuarioperfil_insert_Z_SetNull( );
               this.gxTv_SdtUsuarioPerfil_Usuarioperfil_update_Z_SetNull( );
               this.gxTv_SdtUsuarioPerfil_Usuarioperfil_delete_Z_SetNull( );
               this.gxTv_SdtUsuarioPerfil_Perfil_ativo_Z_SetNull( );
            }
            gxTv_SdtUsuarioPerfil_Usuario_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Usuario_PessoaCod" )]
      [  XmlElement( ElementName = "Usuario_PessoaCod"   )]
      public int gxTpr_Usuario_pessoacod
      {
         get {
            return gxTv_SdtUsuarioPerfil_Usuario_pessoacod ;
         }

         set {
            gxTv_SdtUsuarioPerfil_Usuario_pessoacod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Usuario_PessoaNom" )]
      [  XmlElement( ElementName = "Usuario_PessoaNom"   )]
      public String gxTpr_Usuario_pessoanom
      {
         get {
            return gxTv_SdtUsuarioPerfil_Usuario_pessoanom ;
         }

         set {
            gxTv_SdtUsuarioPerfil_Usuario_pessoanom_N = 0;
            gxTv_SdtUsuarioPerfil_Usuario_pessoanom = (String)(value);
         }

      }

      public void gxTv_SdtUsuarioPerfil_Usuario_pessoanom_SetNull( )
      {
         gxTv_SdtUsuarioPerfil_Usuario_pessoanom_N = 1;
         gxTv_SdtUsuarioPerfil_Usuario_pessoanom = "";
         return  ;
      }

      public bool gxTv_SdtUsuarioPerfil_Usuario_pessoanom_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_Nome" )]
      [  XmlElement( ElementName = "Usuario_Nome"   )]
      public String gxTpr_Usuario_nome
      {
         get {
            return gxTv_SdtUsuarioPerfil_Usuario_nome ;
         }

         set {
            gxTv_SdtUsuarioPerfil_Usuario_nome_N = 0;
            gxTv_SdtUsuarioPerfil_Usuario_nome = (String)(value);
         }

      }

      public void gxTv_SdtUsuarioPerfil_Usuario_nome_SetNull( )
      {
         gxTv_SdtUsuarioPerfil_Usuario_nome_N = 1;
         gxTv_SdtUsuarioPerfil_Usuario_nome = "";
         return  ;
      }

      public bool gxTv_SdtUsuarioPerfil_Usuario_nome_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_UserGamGuid" )]
      [  XmlElement( ElementName = "Usuario_UserGamGuid"   )]
      public String gxTpr_Usuario_usergamguid
      {
         get {
            return gxTv_SdtUsuarioPerfil_Usuario_usergamguid ;
         }

         set {
            gxTv_SdtUsuarioPerfil_Usuario_usergamguid = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Perfil_Codigo" )]
      [  XmlElement( ElementName = "Perfil_Codigo"   )]
      public int gxTpr_Perfil_codigo
      {
         get {
            return gxTv_SdtUsuarioPerfil_Perfil_codigo ;
         }

         set {
            if ( gxTv_SdtUsuarioPerfil_Perfil_codigo != value )
            {
               gxTv_SdtUsuarioPerfil_Mode = "INS";
               this.gxTv_SdtUsuarioPerfil_Usuario_codigo_Z_SetNull( );
               this.gxTv_SdtUsuarioPerfil_Usuario_pessoacod_Z_SetNull( );
               this.gxTv_SdtUsuarioPerfil_Usuario_pessoanom_Z_SetNull( );
               this.gxTv_SdtUsuarioPerfil_Usuario_nome_Z_SetNull( );
               this.gxTv_SdtUsuarioPerfil_Usuario_usergamguid_Z_SetNull( );
               this.gxTv_SdtUsuarioPerfil_Perfil_codigo_Z_SetNull( );
               this.gxTv_SdtUsuarioPerfil_Perfil_areatrabalhocod_Z_SetNull( );
               this.gxTv_SdtUsuarioPerfil_Perfil_nome_Z_SetNull( );
               this.gxTv_SdtUsuarioPerfil_Perfil_tipo_Z_SetNull( );
               this.gxTv_SdtUsuarioPerfil_Perfil_gamid_Z_SetNull( );
               this.gxTv_SdtUsuarioPerfil_Usuarioperfil_display_Z_SetNull( );
               this.gxTv_SdtUsuarioPerfil_Usuarioperfil_insert_Z_SetNull( );
               this.gxTv_SdtUsuarioPerfil_Usuarioperfil_update_Z_SetNull( );
               this.gxTv_SdtUsuarioPerfil_Usuarioperfil_delete_Z_SetNull( );
               this.gxTv_SdtUsuarioPerfil_Perfil_ativo_Z_SetNull( );
            }
            gxTv_SdtUsuarioPerfil_Perfil_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Perfil_AreaTrabalhoCod" )]
      [  XmlElement( ElementName = "Perfil_AreaTrabalhoCod"   )]
      public int gxTpr_Perfil_areatrabalhocod
      {
         get {
            return gxTv_SdtUsuarioPerfil_Perfil_areatrabalhocod ;
         }

         set {
            gxTv_SdtUsuarioPerfil_Perfil_areatrabalhocod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Perfil_Nome" )]
      [  XmlElement( ElementName = "Perfil_Nome"   )]
      public String gxTpr_Perfil_nome
      {
         get {
            return gxTv_SdtUsuarioPerfil_Perfil_nome ;
         }

         set {
            gxTv_SdtUsuarioPerfil_Perfil_nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Perfil_Tipo" )]
      [  XmlElement( ElementName = "Perfil_Tipo"   )]
      public short gxTpr_Perfil_tipo
      {
         get {
            return gxTv_SdtUsuarioPerfil_Perfil_tipo ;
         }

         set {
            gxTv_SdtUsuarioPerfil_Perfil_tipo = (short)(value);
         }

      }

      [  SoapElement( ElementName = "Perfil_GamId" )]
      [  XmlElement( ElementName = "Perfil_GamId"   )]
      public long gxTpr_Perfil_gamid
      {
         get {
            return gxTv_SdtUsuarioPerfil_Perfil_gamid ;
         }

         set {
            gxTv_SdtUsuarioPerfil_Perfil_gamid = (long)(value);
         }

      }

      [  SoapElement( ElementName = "UsuarioPerfil_Display" )]
      [  XmlElement( ElementName = "UsuarioPerfil_Display"   )]
      public bool gxTpr_Usuarioperfil_display
      {
         get {
            return gxTv_SdtUsuarioPerfil_Usuarioperfil_display ;
         }

         set {
            gxTv_SdtUsuarioPerfil_Usuarioperfil_display_N = 0;
            gxTv_SdtUsuarioPerfil_Usuarioperfil_display = value;
         }

      }

      public void gxTv_SdtUsuarioPerfil_Usuarioperfil_display_SetNull( )
      {
         gxTv_SdtUsuarioPerfil_Usuarioperfil_display_N = 1;
         gxTv_SdtUsuarioPerfil_Usuarioperfil_display = false;
         return  ;
      }

      public bool gxTv_SdtUsuarioPerfil_Usuarioperfil_display_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "UsuarioPerfil_Insert" )]
      [  XmlElement( ElementName = "UsuarioPerfil_Insert"   )]
      public bool gxTpr_Usuarioperfil_insert
      {
         get {
            return gxTv_SdtUsuarioPerfil_Usuarioperfil_insert ;
         }

         set {
            gxTv_SdtUsuarioPerfil_Usuarioperfil_insert_N = 0;
            gxTv_SdtUsuarioPerfil_Usuarioperfil_insert = value;
         }

      }

      public void gxTv_SdtUsuarioPerfil_Usuarioperfil_insert_SetNull( )
      {
         gxTv_SdtUsuarioPerfil_Usuarioperfil_insert_N = 1;
         gxTv_SdtUsuarioPerfil_Usuarioperfil_insert = false;
         return  ;
      }

      public bool gxTv_SdtUsuarioPerfil_Usuarioperfil_insert_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "UsuarioPerfil_Update" )]
      [  XmlElement( ElementName = "UsuarioPerfil_Update"   )]
      public bool gxTpr_Usuarioperfil_update
      {
         get {
            return gxTv_SdtUsuarioPerfil_Usuarioperfil_update ;
         }

         set {
            gxTv_SdtUsuarioPerfil_Usuarioperfil_update_N = 0;
            gxTv_SdtUsuarioPerfil_Usuarioperfil_update = value;
         }

      }

      public void gxTv_SdtUsuarioPerfil_Usuarioperfil_update_SetNull( )
      {
         gxTv_SdtUsuarioPerfil_Usuarioperfil_update_N = 1;
         gxTv_SdtUsuarioPerfil_Usuarioperfil_update = false;
         return  ;
      }

      public bool gxTv_SdtUsuarioPerfil_Usuarioperfil_update_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "UsuarioPerfil_Delete" )]
      [  XmlElement( ElementName = "UsuarioPerfil_Delete"   )]
      public bool gxTpr_Usuarioperfil_delete
      {
         get {
            return gxTv_SdtUsuarioPerfil_Usuarioperfil_delete ;
         }

         set {
            gxTv_SdtUsuarioPerfil_Usuarioperfil_delete_N = 0;
            gxTv_SdtUsuarioPerfil_Usuarioperfil_delete = value;
         }

      }

      public void gxTv_SdtUsuarioPerfil_Usuarioperfil_delete_SetNull( )
      {
         gxTv_SdtUsuarioPerfil_Usuarioperfil_delete_N = 1;
         gxTv_SdtUsuarioPerfil_Usuarioperfil_delete = false;
         return  ;
      }

      public bool gxTv_SdtUsuarioPerfil_Usuarioperfil_delete_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Perfil_Ativo" )]
      [  XmlElement( ElementName = "Perfil_Ativo"   )]
      public bool gxTpr_Perfil_ativo
      {
         get {
            return gxTv_SdtUsuarioPerfil_Perfil_ativo ;
         }

         set {
            gxTv_SdtUsuarioPerfil_Perfil_ativo = value;
         }

      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtUsuarioPerfil_Mode ;
         }

         set {
            gxTv_SdtUsuarioPerfil_Mode = (String)(value);
         }

      }

      public void gxTv_SdtUsuarioPerfil_Mode_SetNull( )
      {
         gxTv_SdtUsuarioPerfil_Mode = "";
         return  ;
      }

      public bool gxTv_SdtUsuarioPerfil_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtUsuarioPerfil_Initialized ;
         }

         set {
            gxTv_SdtUsuarioPerfil_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtUsuarioPerfil_Initialized_SetNull( )
      {
         gxTv_SdtUsuarioPerfil_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtUsuarioPerfil_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_Codigo_Z" )]
      [  XmlElement( ElementName = "Usuario_Codigo_Z"   )]
      public int gxTpr_Usuario_codigo_Z
      {
         get {
            return gxTv_SdtUsuarioPerfil_Usuario_codigo_Z ;
         }

         set {
            gxTv_SdtUsuarioPerfil_Usuario_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtUsuarioPerfil_Usuario_codigo_Z_SetNull( )
      {
         gxTv_SdtUsuarioPerfil_Usuario_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtUsuarioPerfil_Usuario_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_PessoaCod_Z" )]
      [  XmlElement( ElementName = "Usuario_PessoaCod_Z"   )]
      public int gxTpr_Usuario_pessoacod_Z
      {
         get {
            return gxTv_SdtUsuarioPerfil_Usuario_pessoacod_Z ;
         }

         set {
            gxTv_SdtUsuarioPerfil_Usuario_pessoacod_Z = (int)(value);
         }

      }

      public void gxTv_SdtUsuarioPerfil_Usuario_pessoacod_Z_SetNull( )
      {
         gxTv_SdtUsuarioPerfil_Usuario_pessoacod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtUsuarioPerfil_Usuario_pessoacod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_PessoaNom_Z" )]
      [  XmlElement( ElementName = "Usuario_PessoaNom_Z"   )]
      public String gxTpr_Usuario_pessoanom_Z
      {
         get {
            return gxTv_SdtUsuarioPerfil_Usuario_pessoanom_Z ;
         }

         set {
            gxTv_SdtUsuarioPerfil_Usuario_pessoanom_Z = (String)(value);
         }

      }

      public void gxTv_SdtUsuarioPerfil_Usuario_pessoanom_Z_SetNull( )
      {
         gxTv_SdtUsuarioPerfil_Usuario_pessoanom_Z = "";
         return  ;
      }

      public bool gxTv_SdtUsuarioPerfil_Usuario_pessoanom_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_Nome_Z" )]
      [  XmlElement( ElementName = "Usuario_Nome_Z"   )]
      public String gxTpr_Usuario_nome_Z
      {
         get {
            return gxTv_SdtUsuarioPerfil_Usuario_nome_Z ;
         }

         set {
            gxTv_SdtUsuarioPerfil_Usuario_nome_Z = (String)(value);
         }

      }

      public void gxTv_SdtUsuarioPerfil_Usuario_nome_Z_SetNull( )
      {
         gxTv_SdtUsuarioPerfil_Usuario_nome_Z = "";
         return  ;
      }

      public bool gxTv_SdtUsuarioPerfil_Usuario_nome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_UserGamGuid_Z" )]
      [  XmlElement( ElementName = "Usuario_UserGamGuid_Z"   )]
      public String gxTpr_Usuario_usergamguid_Z
      {
         get {
            return gxTv_SdtUsuarioPerfil_Usuario_usergamguid_Z ;
         }

         set {
            gxTv_SdtUsuarioPerfil_Usuario_usergamguid_Z = (String)(value);
         }

      }

      public void gxTv_SdtUsuarioPerfil_Usuario_usergamguid_Z_SetNull( )
      {
         gxTv_SdtUsuarioPerfil_Usuario_usergamguid_Z = "";
         return  ;
      }

      public bool gxTv_SdtUsuarioPerfil_Usuario_usergamguid_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Perfil_Codigo_Z" )]
      [  XmlElement( ElementName = "Perfil_Codigo_Z"   )]
      public int gxTpr_Perfil_codigo_Z
      {
         get {
            return gxTv_SdtUsuarioPerfil_Perfil_codigo_Z ;
         }

         set {
            gxTv_SdtUsuarioPerfil_Perfil_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtUsuarioPerfil_Perfil_codigo_Z_SetNull( )
      {
         gxTv_SdtUsuarioPerfil_Perfil_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtUsuarioPerfil_Perfil_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Perfil_AreaTrabalhoCod_Z" )]
      [  XmlElement( ElementName = "Perfil_AreaTrabalhoCod_Z"   )]
      public int gxTpr_Perfil_areatrabalhocod_Z
      {
         get {
            return gxTv_SdtUsuarioPerfil_Perfil_areatrabalhocod_Z ;
         }

         set {
            gxTv_SdtUsuarioPerfil_Perfil_areatrabalhocod_Z = (int)(value);
         }

      }

      public void gxTv_SdtUsuarioPerfil_Perfil_areatrabalhocod_Z_SetNull( )
      {
         gxTv_SdtUsuarioPerfil_Perfil_areatrabalhocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtUsuarioPerfil_Perfil_areatrabalhocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Perfil_Nome_Z" )]
      [  XmlElement( ElementName = "Perfil_Nome_Z"   )]
      public String gxTpr_Perfil_nome_Z
      {
         get {
            return gxTv_SdtUsuarioPerfil_Perfil_nome_Z ;
         }

         set {
            gxTv_SdtUsuarioPerfil_Perfil_nome_Z = (String)(value);
         }

      }

      public void gxTv_SdtUsuarioPerfil_Perfil_nome_Z_SetNull( )
      {
         gxTv_SdtUsuarioPerfil_Perfil_nome_Z = "";
         return  ;
      }

      public bool gxTv_SdtUsuarioPerfil_Perfil_nome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Perfil_Tipo_Z" )]
      [  XmlElement( ElementName = "Perfil_Tipo_Z"   )]
      public short gxTpr_Perfil_tipo_Z
      {
         get {
            return gxTv_SdtUsuarioPerfil_Perfil_tipo_Z ;
         }

         set {
            gxTv_SdtUsuarioPerfil_Perfil_tipo_Z = (short)(value);
         }

      }

      public void gxTv_SdtUsuarioPerfil_Perfil_tipo_Z_SetNull( )
      {
         gxTv_SdtUsuarioPerfil_Perfil_tipo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtUsuarioPerfil_Perfil_tipo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Perfil_GamId_Z" )]
      [  XmlElement( ElementName = "Perfil_GamId_Z"   )]
      public long gxTpr_Perfil_gamid_Z
      {
         get {
            return gxTv_SdtUsuarioPerfil_Perfil_gamid_Z ;
         }

         set {
            gxTv_SdtUsuarioPerfil_Perfil_gamid_Z = (long)(value);
         }

      }

      public void gxTv_SdtUsuarioPerfil_Perfil_gamid_Z_SetNull( )
      {
         gxTv_SdtUsuarioPerfil_Perfil_gamid_Z = 0;
         return  ;
      }

      public bool gxTv_SdtUsuarioPerfil_Perfil_gamid_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "UsuarioPerfil_Display_Z" )]
      [  XmlElement( ElementName = "UsuarioPerfil_Display_Z"   )]
      public bool gxTpr_Usuarioperfil_display_Z
      {
         get {
            return gxTv_SdtUsuarioPerfil_Usuarioperfil_display_Z ;
         }

         set {
            gxTv_SdtUsuarioPerfil_Usuarioperfil_display_Z = value;
         }

      }

      public void gxTv_SdtUsuarioPerfil_Usuarioperfil_display_Z_SetNull( )
      {
         gxTv_SdtUsuarioPerfil_Usuarioperfil_display_Z = false;
         return  ;
      }

      public bool gxTv_SdtUsuarioPerfil_Usuarioperfil_display_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "UsuarioPerfil_Insert_Z" )]
      [  XmlElement( ElementName = "UsuarioPerfil_Insert_Z"   )]
      public bool gxTpr_Usuarioperfil_insert_Z
      {
         get {
            return gxTv_SdtUsuarioPerfil_Usuarioperfil_insert_Z ;
         }

         set {
            gxTv_SdtUsuarioPerfil_Usuarioperfil_insert_Z = value;
         }

      }

      public void gxTv_SdtUsuarioPerfil_Usuarioperfil_insert_Z_SetNull( )
      {
         gxTv_SdtUsuarioPerfil_Usuarioperfil_insert_Z = false;
         return  ;
      }

      public bool gxTv_SdtUsuarioPerfil_Usuarioperfil_insert_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "UsuarioPerfil_Update_Z" )]
      [  XmlElement( ElementName = "UsuarioPerfil_Update_Z"   )]
      public bool gxTpr_Usuarioperfil_update_Z
      {
         get {
            return gxTv_SdtUsuarioPerfil_Usuarioperfil_update_Z ;
         }

         set {
            gxTv_SdtUsuarioPerfil_Usuarioperfil_update_Z = value;
         }

      }

      public void gxTv_SdtUsuarioPerfil_Usuarioperfil_update_Z_SetNull( )
      {
         gxTv_SdtUsuarioPerfil_Usuarioperfil_update_Z = false;
         return  ;
      }

      public bool gxTv_SdtUsuarioPerfil_Usuarioperfil_update_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "UsuarioPerfil_Delete_Z" )]
      [  XmlElement( ElementName = "UsuarioPerfil_Delete_Z"   )]
      public bool gxTpr_Usuarioperfil_delete_Z
      {
         get {
            return gxTv_SdtUsuarioPerfil_Usuarioperfil_delete_Z ;
         }

         set {
            gxTv_SdtUsuarioPerfil_Usuarioperfil_delete_Z = value;
         }

      }

      public void gxTv_SdtUsuarioPerfil_Usuarioperfil_delete_Z_SetNull( )
      {
         gxTv_SdtUsuarioPerfil_Usuarioperfil_delete_Z = false;
         return  ;
      }

      public bool gxTv_SdtUsuarioPerfil_Usuarioperfil_delete_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Perfil_Ativo_Z" )]
      [  XmlElement( ElementName = "Perfil_Ativo_Z"   )]
      public bool gxTpr_Perfil_ativo_Z
      {
         get {
            return gxTv_SdtUsuarioPerfil_Perfil_ativo_Z ;
         }

         set {
            gxTv_SdtUsuarioPerfil_Perfil_ativo_Z = value;
         }

      }

      public void gxTv_SdtUsuarioPerfil_Perfil_ativo_Z_SetNull( )
      {
         gxTv_SdtUsuarioPerfil_Perfil_ativo_Z = false;
         return  ;
      }

      public bool gxTv_SdtUsuarioPerfil_Perfil_ativo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_PessoaNom_N" )]
      [  XmlElement( ElementName = "Usuario_PessoaNom_N"   )]
      public short gxTpr_Usuario_pessoanom_N
      {
         get {
            return gxTv_SdtUsuarioPerfil_Usuario_pessoanom_N ;
         }

         set {
            gxTv_SdtUsuarioPerfil_Usuario_pessoanom_N = (short)(value);
         }

      }

      public void gxTv_SdtUsuarioPerfil_Usuario_pessoanom_N_SetNull( )
      {
         gxTv_SdtUsuarioPerfil_Usuario_pessoanom_N = 0;
         return  ;
      }

      public bool gxTv_SdtUsuarioPerfil_Usuario_pessoanom_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_Nome_N" )]
      [  XmlElement( ElementName = "Usuario_Nome_N"   )]
      public short gxTpr_Usuario_nome_N
      {
         get {
            return gxTv_SdtUsuarioPerfil_Usuario_nome_N ;
         }

         set {
            gxTv_SdtUsuarioPerfil_Usuario_nome_N = (short)(value);
         }

      }

      public void gxTv_SdtUsuarioPerfil_Usuario_nome_N_SetNull( )
      {
         gxTv_SdtUsuarioPerfil_Usuario_nome_N = 0;
         return  ;
      }

      public bool gxTv_SdtUsuarioPerfil_Usuario_nome_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "UsuarioPerfil_Display_N" )]
      [  XmlElement( ElementName = "UsuarioPerfil_Display_N"   )]
      public short gxTpr_Usuarioperfil_display_N
      {
         get {
            return gxTv_SdtUsuarioPerfil_Usuarioperfil_display_N ;
         }

         set {
            gxTv_SdtUsuarioPerfil_Usuarioperfil_display_N = (short)(value);
         }

      }

      public void gxTv_SdtUsuarioPerfil_Usuarioperfil_display_N_SetNull( )
      {
         gxTv_SdtUsuarioPerfil_Usuarioperfil_display_N = 0;
         return  ;
      }

      public bool gxTv_SdtUsuarioPerfil_Usuarioperfil_display_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "UsuarioPerfil_Insert_N" )]
      [  XmlElement( ElementName = "UsuarioPerfil_Insert_N"   )]
      public short gxTpr_Usuarioperfil_insert_N
      {
         get {
            return gxTv_SdtUsuarioPerfil_Usuarioperfil_insert_N ;
         }

         set {
            gxTv_SdtUsuarioPerfil_Usuarioperfil_insert_N = (short)(value);
         }

      }

      public void gxTv_SdtUsuarioPerfil_Usuarioperfil_insert_N_SetNull( )
      {
         gxTv_SdtUsuarioPerfil_Usuarioperfil_insert_N = 0;
         return  ;
      }

      public bool gxTv_SdtUsuarioPerfil_Usuarioperfil_insert_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "UsuarioPerfil_Update_N" )]
      [  XmlElement( ElementName = "UsuarioPerfil_Update_N"   )]
      public short gxTpr_Usuarioperfil_update_N
      {
         get {
            return gxTv_SdtUsuarioPerfil_Usuarioperfil_update_N ;
         }

         set {
            gxTv_SdtUsuarioPerfil_Usuarioperfil_update_N = (short)(value);
         }

      }

      public void gxTv_SdtUsuarioPerfil_Usuarioperfil_update_N_SetNull( )
      {
         gxTv_SdtUsuarioPerfil_Usuarioperfil_update_N = 0;
         return  ;
      }

      public bool gxTv_SdtUsuarioPerfil_Usuarioperfil_update_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "UsuarioPerfil_Delete_N" )]
      [  XmlElement( ElementName = "UsuarioPerfil_Delete_N"   )]
      public short gxTpr_Usuarioperfil_delete_N
      {
         get {
            return gxTv_SdtUsuarioPerfil_Usuarioperfil_delete_N ;
         }

         set {
            gxTv_SdtUsuarioPerfil_Usuarioperfil_delete_N = (short)(value);
         }

      }

      public void gxTv_SdtUsuarioPerfil_Usuarioperfil_delete_N_SetNull( )
      {
         gxTv_SdtUsuarioPerfil_Usuarioperfil_delete_N = 0;
         return  ;
      }

      public bool gxTv_SdtUsuarioPerfil_Usuarioperfil_delete_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtUsuarioPerfil_Usuario_pessoanom = "";
         gxTv_SdtUsuarioPerfil_Usuario_nome = "";
         gxTv_SdtUsuarioPerfil_Usuario_usergamguid = "";
         gxTv_SdtUsuarioPerfil_Perfil_nome = "";
         gxTv_SdtUsuarioPerfil_Mode = "";
         gxTv_SdtUsuarioPerfil_Usuario_pessoanom_Z = "";
         gxTv_SdtUsuarioPerfil_Usuario_nome_Z = "";
         gxTv_SdtUsuarioPerfil_Usuario_usergamguid_Z = "";
         gxTv_SdtUsuarioPerfil_Perfil_nome_Z = "";
         gxTv_SdtUsuarioPerfil_Usuarioperfil_display = true;
         gxTv_SdtUsuarioPerfil_Perfil_ativo = true;
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "usuarioperfil", "GeneXus.Programs.usuarioperfil_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtUsuarioPerfil_Perfil_tipo ;
      private short gxTv_SdtUsuarioPerfil_Initialized ;
      private short gxTv_SdtUsuarioPerfil_Perfil_tipo_Z ;
      private short gxTv_SdtUsuarioPerfil_Usuario_pessoanom_N ;
      private short gxTv_SdtUsuarioPerfil_Usuario_nome_N ;
      private short gxTv_SdtUsuarioPerfil_Usuarioperfil_display_N ;
      private short gxTv_SdtUsuarioPerfil_Usuarioperfil_insert_N ;
      private short gxTv_SdtUsuarioPerfil_Usuarioperfil_update_N ;
      private short gxTv_SdtUsuarioPerfil_Usuarioperfil_delete_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtUsuarioPerfil_Usuario_codigo ;
      private int gxTv_SdtUsuarioPerfil_Usuario_pessoacod ;
      private int gxTv_SdtUsuarioPerfil_Perfil_codigo ;
      private int gxTv_SdtUsuarioPerfil_Perfil_areatrabalhocod ;
      private int gxTv_SdtUsuarioPerfil_Usuario_codigo_Z ;
      private int gxTv_SdtUsuarioPerfil_Usuario_pessoacod_Z ;
      private int gxTv_SdtUsuarioPerfil_Perfil_codigo_Z ;
      private int gxTv_SdtUsuarioPerfil_Perfil_areatrabalhocod_Z ;
      private long gxTv_SdtUsuarioPerfil_Perfil_gamid ;
      private long gxTv_SdtUsuarioPerfil_Perfil_gamid_Z ;
      private String gxTv_SdtUsuarioPerfil_Usuario_pessoanom ;
      private String gxTv_SdtUsuarioPerfil_Usuario_nome ;
      private String gxTv_SdtUsuarioPerfil_Usuario_usergamguid ;
      private String gxTv_SdtUsuarioPerfil_Perfil_nome ;
      private String gxTv_SdtUsuarioPerfil_Mode ;
      private String gxTv_SdtUsuarioPerfil_Usuario_pessoanom_Z ;
      private String gxTv_SdtUsuarioPerfil_Usuario_nome_Z ;
      private String gxTv_SdtUsuarioPerfil_Usuario_usergamguid_Z ;
      private String gxTv_SdtUsuarioPerfil_Perfil_nome_Z ;
      private String sTagName ;
      private bool gxTv_SdtUsuarioPerfil_Usuarioperfil_display ;
      private bool gxTv_SdtUsuarioPerfil_Usuarioperfil_insert ;
      private bool gxTv_SdtUsuarioPerfil_Usuarioperfil_update ;
      private bool gxTv_SdtUsuarioPerfil_Usuarioperfil_delete ;
      private bool gxTv_SdtUsuarioPerfil_Perfil_ativo ;
      private bool gxTv_SdtUsuarioPerfil_Usuarioperfil_display_Z ;
      private bool gxTv_SdtUsuarioPerfil_Usuarioperfil_insert_Z ;
      private bool gxTv_SdtUsuarioPerfil_Usuarioperfil_update_Z ;
      private bool gxTv_SdtUsuarioPerfil_Usuarioperfil_delete_Z ;
      private bool gxTv_SdtUsuarioPerfil_Perfil_ativo_Z ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"UsuarioPerfil", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtUsuarioPerfil_RESTInterface : GxGenericCollectionItem<SdtUsuarioPerfil>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtUsuarioPerfil_RESTInterface( ) : base()
      {
      }

      public SdtUsuarioPerfil_RESTInterface( SdtUsuarioPerfil psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Usuario_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Usuario_codigo
      {
         get {
            return sdt.gxTpr_Usuario_codigo ;
         }

         set {
            sdt.gxTpr_Usuario_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Usuario_PessoaCod" , Order = 1 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Usuario_pessoacod
      {
         get {
            return sdt.gxTpr_Usuario_pessoacod ;
         }

         set {
            sdt.gxTpr_Usuario_pessoacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Usuario_PessoaNom" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Usuario_pessoanom
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Usuario_pessoanom) ;
         }

         set {
            sdt.gxTpr_Usuario_pessoanom = (String)(value);
         }

      }

      [DataMember( Name = "Usuario_Nome" , Order = 3 )]
      [GxSeudo()]
      public String gxTpr_Usuario_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Usuario_nome) ;
         }

         set {
            sdt.gxTpr_Usuario_nome = (String)(value);
         }

      }

      [DataMember( Name = "Usuario_UserGamGuid" , Order = 4 )]
      [GxSeudo()]
      public String gxTpr_Usuario_usergamguid
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Usuario_usergamguid) ;
         }

         set {
            sdt.gxTpr_Usuario_usergamguid = (String)(value);
         }

      }

      [DataMember( Name = "Perfil_Codigo" , Order = 5 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Perfil_codigo
      {
         get {
            return sdt.gxTpr_Perfil_codigo ;
         }

         set {
            sdt.gxTpr_Perfil_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Perfil_AreaTrabalhoCod" , Order = 6 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Perfil_areatrabalhocod
      {
         get {
            return sdt.gxTpr_Perfil_areatrabalhocod ;
         }

         set {
            sdt.gxTpr_Perfil_areatrabalhocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Perfil_Nome" , Order = 7 )]
      [GxSeudo()]
      public String gxTpr_Perfil_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Perfil_nome) ;
         }

         set {
            sdt.gxTpr_Perfil_nome = (String)(value);
         }

      }

      [DataMember( Name = "Perfil_Tipo" , Order = 8 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Perfil_tipo
      {
         get {
            return sdt.gxTpr_Perfil_tipo ;
         }

         set {
            sdt.gxTpr_Perfil_tipo = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Perfil_GamId" , Order = 9 )]
      [GxSeudo()]
      public String gxTpr_Perfil_gamid
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Perfil_gamid), 12, 0)) ;
         }

         set {
            sdt.gxTpr_Perfil_gamid = (long)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "UsuarioPerfil_Display" , Order = 10 )]
      [GxSeudo()]
      public bool gxTpr_Usuarioperfil_display
      {
         get {
            return sdt.gxTpr_Usuarioperfil_display ;
         }

         set {
            sdt.gxTpr_Usuarioperfil_display = value;
         }

      }

      [DataMember( Name = "UsuarioPerfil_Insert" , Order = 11 )]
      [GxSeudo()]
      public bool gxTpr_Usuarioperfil_insert
      {
         get {
            return sdt.gxTpr_Usuarioperfil_insert ;
         }

         set {
            sdt.gxTpr_Usuarioperfil_insert = value;
         }

      }

      [DataMember( Name = "UsuarioPerfil_Update" , Order = 12 )]
      [GxSeudo()]
      public bool gxTpr_Usuarioperfil_update
      {
         get {
            return sdt.gxTpr_Usuarioperfil_update ;
         }

         set {
            sdt.gxTpr_Usuarioperfil_update = value;
         }

      }

      [DataMember( Name = "UsuarioPerfil_Delete" , Order = 13 )]
      [GxSeudo()]
      public bool gxTpr_Usuarioperfil_delete
      {
         get {
            return sdt.gxTpr_Usuarioperfil_delete ;
         }

         set {
            sdt.gxTpr_Usuarioperfil_delete = value;
         }

      }

      [DataMember( Name = "Perfil_Ativo" , Order = 14 )]
      [GxSeudo()]
      public bool gxTpr_Perfil_ativo
      {
         get {
            return sdt.gxTpr_Perfil_ativo ;
         }

         set {
            sdt.gxTpr_Perfil_ativo = value;
         }

      }

      public SdtUsuarioPerfil sdt
      {
         get {
            return (SdtUsuarioPerfil)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtUsuarioPerfil() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 38 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
