/*
               File: PromptContratoUnidades
        Description: Prompt Contrato Unidades
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:43:45.22
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptcontratounidades : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptcontratounidades( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptcontratounidades( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutContratoUnidades_ContratoCod ,
                           ref int aP1_InOutContratoUnidades_UndMedCod ,
                           ref String aP2_InOutContratoUnidades_UndMedNom )
      {
         this.AV7InOutContratoUnidades_ContratoCod = aP0_InOutContratoUnidades_ContratoCod;
         this.AV12InOutContratoUnidades_UndMedCod = aP1_InOutContratoUnidades_UndMedCod;
         this.AV8InOutContratoUnidades_UndMedNom = aP2_InOutContratoUnidades_UndMedNom;
         executePrivate();
         aP0_InOutContratoUnidades_ContratoCod=this.AV7InOutContratoUnidades_ContratoCod;
         aP1_InOutContratoUnidades_UndMedCod=this.AV12InOutContratoUnidades_UndMedCod;
         aP2_InOutContratoUnidades_UndMedNom=this.AV8InOutContratoUnidades_UndMedNom;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_26 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_26_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_26_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV9OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9OrderedBy), 4, 0)));
               AV10OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10OrderedDsc", AV10OrderedDsc);
               AV16TFContratoUnidades_ContratoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16TFContratoUnidades_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16TFContratoUnidades_ContratoCod), 6, 0)));
               AV17TFContratoUnidades_ContratoCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17TFContratoUnidades_ContratoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17TFContratoUnidades_ContratoCod_To), 6, 0)));
               AV20TFContratoUnidades_UndMedCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20TFContratoUnidades_UndMedCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20TFContratoUnidades_UndMedCod), 6, 0)));
               AV21TFContratoUnidades_UndMedCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TFContratoUnidades_UndMedCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21TFContratoUnidades_UndMedCod_To), 6, 0)));
               AV24TFContratoUnidades_UndMedNom = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24TFContratoUnidades_UndMedNom", AV24TFContratoUnidades_UndMedNom);
               AV25TFContratoUnidades_UndMedNom_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TFContratoUnidades_UndMedNom_Sel", AV25TFContratoUnidades_UndMedNom_Sel);
               AV28TFContratoUnidades_UndMedSigla = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28TFContratoUnidades_UndMedSigla", AV28TFContratoUnidades_UndMedSigla);
               AV29TFContratoUnidades_UndMedSigla_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29TFContratoUnidades_UndMedSigla_Sel", AV29TFContratoUnidades_UndMedSigla_Sel);
               AV32TFContratoUnidades_Produtividade = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFContratoUnidades_Produtividade", StringUtil.LTrim( StringUtil.Str( AV32TFContratoUnidades_Produtividade, 14, 5)));
               AV33TFContratoUnidades_Produtividade_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TFContratoUnidades_Produtividade_To", StringUtil.LTrim( StringUtil.Str( AV33TFContratoUnidades_Produtividade_To, 14, 5)));
               AV18ddo_ContratoUnidades_ContratoCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ddo_ContratoUnidades_ContratoCodTitleControlIdToReplace", AV18ddo_ContratoUnidades_ContratoCodTitleControlIdToReplace);
               AV22ddo_ContratoUnidades_UndMedCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ddo_ContratoUnidades_UndMedCodTitleControlIdToReplace", AV22ddo_ContratoUnidades_UndMedCodTitleControlIdToReplace);
               AV26ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace", AV26ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace);
               AV30ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace", AV30ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace);
               AV34ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace", AV34ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace);
               AV42Pgmname = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV9OrderedBy, AV10OrderedDsc, AV16TFContratoUnidades_ContratoCod, AV17TFContratoUnidades_ContratoCod_To, AV20TFContratoUnidades_UndMedCod, AV21TFContratoUnidades_UndMedCod_To, AV24TFContratoUnidades_UndMedNom, AV25TFContratoUnidades_UndMedNom_Sel, AV28TFContratoUnidades_UndMedSigla, AV29TFContratoUnidades_UndMedSigla_Sel, AV32TFContratoUnidades_Produtividade, AV33TFContratoUnidades_Produtividade_To, AV18ddo_ContratoUnidades_ContratoCodTitleControlIdToReplace, AV22ddo_ContratoUnidades_UndMedCodTitleControlIdToReplace, AV26ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace, AV30ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace, AV34ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace, AV42Pgmname) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutContratoUnidades_ContratoCod = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContratoUnidades_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContratoUnidades_ContratoCod), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV12InOutContratoUnidades_UndMedCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12InOutContratoUnidades_UndMedCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12InOutContratoUnidades_UndMedCod), 6, 0)));
                  AV8InOutContratoUnidades_UndMedNom = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContratoUnidades_UndMedNom", AV8InOutContratoUnidades_UndMedNom);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAIM2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV42Pgmname = "PromptContratoUnidades";
               context.Gx_err = 0;
               WSIM2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEIM2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042823434540");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptcontratounidades.aspx") + "?" + UrlEncode("" +AV7InOutContratoUnidades_ContratoCod) + "," + UrlEncode("" +AV12InOutContratoUnidades_UndMedCod) + "," + UrlEncode(StringUtil.RTrim(AV8InOutContratoUnidades_UndMedNom))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV10OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOUNIDADES_CONTRATOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16TFContratoUnidades_ContratoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOUNIDADES_CONTRATOCOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17TFContratoUnidades_ContratoCod_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOUNIDADES_UNDMEDCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20TFContratoUnidades_UndMedCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOUNIDADES_UNDMEDCOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21TFContratoUnidades_UndMedCod_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOUNIDADES_UNDMEDNOM", StringUtil.RTrim( AV24TFContratoUnidades_UndMedNom));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOUNIDADES_UNDMEDNOM_SEL", StringUtil.RTrim( AV25TFContratoUnidades_UndMedNom_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOUNIDADES_UNDMEDSIGLA", StringUtil.RTrim( AV28TFContratoUnidades_UndMedSigla));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOUNIDADES_UNDMEDSIGLA_SEL", StringUtil.RTrim( AV29TFContratoUnidades_UndMedSigla_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOUNIDADES_PRODUTIVIDADE", StringUtil.LTrim( StringUtil.NToC( AV32TFContratoUnidades_Produtividade, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOUNIDADES_PRODUTIVIDADE_TO", StringUtil.LTrim( StringUtil.NToC( AV33TFContratoUnidades_Produtividade_To, 14, 5, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_26", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_26), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV37GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV35DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV35DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOUNIDADES_CONTRATOCODTITLEFILTERDATA", AV15ContratoUnidades_ContratoCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOUNIDADES_CONTRATOCODTITLEFILTERDATA", AV15ContratoUnidades_ContratoCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOUNIDADES_UNDMEDCODTITLEFILTERDATA", AV19ContratoUnidades_UndMedCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOUNIDADES_UNDMEDCODTITLEFILTERDATA", AV19ContratoUnidades_UndMedCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOUNIDADES_UNDMEDNOMTITLEFILTERDATA", AV23ContratoUnidades_UndMedNomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOUNIDADES_UNDMEDNOMTITLEFILTERDATA", AV23ContratoUnidades_UndMedNomTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOUNIDADES_UNDMEDSIGLATITLEFILTERDATA", AV27ContratoUnidades_UndMedSiglaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOUNIDADES_UNDMEDSIGLATITLEFILTERDATA", AV27ContratoUnidades_UndMedSiglaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOUNIDADES_PRODUTIVIDADETITLEFILTERDATA", AV31ContratoUnidades_ProdutividadeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOUNIDADES_PRODUTIVIDADETITLEFILTERDATA", AV31ContratoUnidades_ProdutividadeTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV42Pgmname));
         GxWebStd.gx_hidden_field( context, "vINOUTCONTRATOUNIDADES_CONTRATOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutContratoUnidades_ContratoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTCONTRATOUNIDADES_UNDMEDCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12InOutContratoUnidades_UndMedCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTCONTRATOUNIDADES_UNDMEDNOM", StringUtil.RTrim( AV8InOutContratoUnidades_UndMedNom));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_CONTRATOCOD_Caption", StringUtil.RTrim( Ddo_contratounidades_contratocod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_CONTRATOCOD_Tooltip", StringUtil.RTrim( Ddo_contratounidades_contratocod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_CONTRATOCOD_Cls", StringUtil.RTrim( Ddo_contratounidades_contratocod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_CONTRATOCOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratounidades_contratocod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_CONTRATOCOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratounidades_contratocod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_CONTRATOCOD_Includesortasc", StringUtil.BoolToStr( Ddo_contratounidades_contratocod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_CONTRATOCOD_Includesortdsc", StringUtil.BoolToStr( Ddo_contratounidades_contratocod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_CONTRATOCOD_Sortedstatus", StringUtil.RTrim( Ddo_contratounidades_contratocod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_CONTRATOCOD_Includefilter", StringUtil.BoolToStr( Ddo_contratounidades_contratocod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_CONTRATOCOD_Filtertype", StringUtil.RTrim( Ddo_contratounidades_contratocod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_CONTRATOCOD_Filterisrange", StringUtil.BoolToStr( Ddo_contratounidades_contratocod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_CONTRATOCOD_Includedatalist", StringUtil.BoolToStr( Ddo_contratounidades_contratocod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_CONTRATOCOD_Datalistfixedvalues", StringUtil.RTrim( Ddo_contratounidades_contratocod_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_CONTRATOCOD_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratounidades_contratocod_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_CONTRATOCOD_Sortasc", StringUtil.RTrim( Ddo_contratounidades_contratocod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_CONTRATOCOD_Sortdsc", StringUtil.RTrim( Ddo_contratounidades_contratocod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_CONTRATOCOD_Loadingdata", StringUtil.RTrim( Ddo_contratounidades_contratocod_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_CONTRATOCOD_Cleanfilter", StringUtil.RTrim( Ddo_contratounidades_contratocod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_CONTRATOCOD_Rangefilterfrom", StringUtil.RTrim( Ddo_contratounidades_contratocod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_CONTRATOCOD_Rangefilterto", StringUtil.RTrim( Ddo_contratounidades_contratocod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_CONTRATOCOD_Noresultsfound", StringUtil.RTrim( Ddo_contratounidades_contratocod_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_CONTRATOCOD_Searchbuttontext", StringUtil.RTrim( Ddo_contratounidades_contratocod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDCOD_Caption", StringUtil.RTrim( Ddo_contratounidades_undmedcod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDCOD_Tooltip", StringUtil.RTrim( Ddo_contratounidades_undmedcod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDCOD_Cls", StringUtil.RTrim( Ddo_contratounidades_undmedcod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDCOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratounidades_undmedcod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDCOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratounidades_undmedcod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDCOD_Includesortasc", StringUtil.BoolToStr( Ddo_contratounidades_undmedcod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDCOD_Includesortdsc", StringUtil.BoolToStr( Ddo_contratounidades_undmedcod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDCOD_Sortedstatus", StringUtil.RTrim( Ddo_contratounidades_undmedcod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDCOD_Includefilter", StringUtil.BoolToStr( Ddo_contratounidades_undmedcod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDCOD_Filtertype", StringUtil.RTrim( Ddo_contratounidades_undmedcod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDCOD_Filterisrange", StringUtil.BoolToStr( Ddo_contratounidades_undmedcod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDCOD_Includedatalist", StringUtil.BoolToStr( Ddo_contratounidades_undmedcod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDCOD_Datalistfixedvalues", StringUtil.RTrim( Ddo_contratounidades_undmedcod_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDCOD_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratounidades_undmedcod_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDCOD_Sortasc", StringUtil.RTrim( Ddo_contratounidades_undmedcod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDCOD_Sortdsc", StringUtil.RTrim( Ddo_contratounidades_undmedcod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDCOD_Loadingdata", StringUtil.RTrim( Ddo_contratounidades_undmedcod_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDCOD_Cleanfilter", StringUtil.RTrim( Ddo_contratounidades_undmedcod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDCOD_Rangefilterfrom", StringUtil.RTrim( Ddo_contratounidades_undmedcod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDCOD_Rangefilterto", StringUtil.RTrim( Ddo_contratounidades_undmedcod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDCOD_Noresultsfound", StringUtil.RTrim( Ddo_contratounidades_undmedcod_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDCOD_Searchbuttontext", StringUtil.RTrim( Ddo_contratounidades_undmedcod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Caption", StringUtil.RTrim( Ddo_contratounidades_undmednom_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Tooltip", StringUtil.RTrim( Ddo_contratounidades_undmednom_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Cls", StringUtil.RTrim( Ddo_contratounidades_undmednom_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratounidades_undmednom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratounidades_undmednom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Includesortasc", StringUtil.BoolToStr( Ddo_contratounidades_undmednom_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Includesortdsc", StringUtil.BoolToStr( Ddo_contratounidades_undmednom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Sortedstatus", StringUtil.RTrim( Ddo_contratounidades_undmednom_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Includefilter", StringUtil.BoolToStr( Ddo_contratounidades_undmednom_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Filtertype", StringUtil.RTrim( Ddo_contratounidades_undmednom_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Filterisrange", StringUtil.BoolToStr( Ddo_contratounidades_undmednom_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Includedatalist", StringUtil.BoolToStr( Ddo_contratounidades_undmednom_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Datalisttype", StringUtil.RTrim( Ddo_contratounidades_undmednom_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Datalistfixedvalues", StringUtil.RTrim( Ddo_contratounidades_undmednom_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Datalistproc", StringUtil.RTrim( Ddo_contratounidades_undmednom_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratounidades_undmednom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Sortasc", StringUtil.RTrim( Ddo_contratounidades_undmednom_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Sortdsc", StringUtil.RTrim( Ddo_contratounidades_undmednom_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Loadingdata", StringUtil.RTrim( Ddo_contratounidades_undmednom_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Cleanfilter", StringUtil.RTrim( Ddo_contratounidades_undmednom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Rangefilterfrom", StringUtil.RTrim( Ddo_contratounidades_undmednom_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Rangefilterto", StringUtil.RTrim( Ddo_contratounidades_undmednom_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Noresultsfound", StringUtil.RTrim( Ddo_contratounidades_undmednom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Searchbuttontext", StringUtil.RTrim( Ddo_contratounidades_undmednom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Caption", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Tooltip", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Cls", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Includesortasc", StringUtil.BoolToStr( Ddo_contratounidades_undmedsigla_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Includesortdsc", StringUtil.BoolToStr( Ddo_contratounidades_undmedsigla_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Sortedstatus", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Includefilter", StringUtil.BoolToStr( Ddo_contratounidades_undmedsigla_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Filtertype", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Filterisrange", StringUtil.BoolToStr( Ddo_contratounidades_undmedsigla_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Includedatalist", StringUtil.BoolToStr( Ddo_contratounidades_undmedsigla_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Datalisttype", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Datalistfixedvalues", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Datalistproc", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratounidades_undmedsigla_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Sortasc", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Sortdsc", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Loadingdata", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Cleanfilter", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Rangefilterfrom", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Rangefilterto", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Noresultsfound", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Searchbuttontext", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Caption", StringUtil.RTrim( Ddo_contratounidades_produtividade_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Tooltip", StringUtil.RTrim( Ddo_contratounidades_produtividade_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Cls", StringUtil.RTrim( Ddo_contratounidades_produtividade_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratounidades_produtividade_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratounidades_produtividade_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Includesortasc", StringUtil.BoolToStr( Ddo_contratounidades_produtividade_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Includesortdsc", StringUtil.BoolToStr( Ddo_contratounidades_produtividade_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Sortedstatus", StringUtil.RTrim( Ddo_contratounidades_produtividade_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Includefilter", StringUtil.BoolToStr( Ddo_contratounidades_produtividade_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Filtertype", StringUtil.RTrim( Ddo_contratounidades_produtividade_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Filterisrange", StringUtil.BoolToStr( Ddo_contratounidades_produtividade_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Includedatalist", StringUtil.BoolToStr( Ddo_contratounidades_produtividade_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Datalistfixedvalues", StringUtil.RTrim( Ddo_contratounidades_produtividade_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratounidades_produtividade_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Sortasc", StringUtil.RTrim( Ddo_contratounidades_produtividade_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Sortdsc", StringUtil.RTrim( Ddo_contratounidades_produtividade_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Loadingdata", StringUtil.RTrim( Ddo_contratounidades_produtividade_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Cleanfilter", StringUtil.RTrim( Ddo_contratounidades_produtividade_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Rangefilterfrom", StringUtil.RTrim( Ddo_contratounidades_produtividade_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Rangefilterto", StringUtil.RTrim( Ddo_contratounidades_produtividade_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Noresultsfound", StringUtil.RTrim( Ddo_contratounidades_produtividade_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Searchbuttontext", StringUtil.RTrim( Ddo_contratounidades_produtividade_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_CONTRATOCOD_Activeeventkey", StringUtil.RTrim( Ddo_contratounidades_contratocod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_CONTRATOCOD_Filteredtext_get", StringUtil.RTrim( Ddo_contratounidades_contratocod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_CONTRATOCOD_Filteredtextto_get", StringUtil.RTrim( Ddo_contratounidades_contratocod_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDCOD_Activeeventkey", StringUtil.RTrim( Ddo_contratounidades_undmedcod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDCOD_Filteredtext_get", StringUtil.RTrim( Ddo_contratounidades_undmedcod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDCOD_Filteredtextto_get", StringUtil.RTrim( Ddo_contratounidades_undmedcod_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Activeeventkey", StringUtil.RTrim( Ddo_contratounidades_undmednom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Filteredtext_get", StringUtil.RTrim( Ddo_contratounidades_undmednom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Selectedvalue_get", StringUtil.RTrim( Ddo_contratounidades_undmednom_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Activeeventkey", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Filteredtext_get", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Selectedvalue_get", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Activeeventkey", StringUtil.RTrim( Ddo_contratounidades_produtividade_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Filteredtext_get", StringUtil.RTrim( Ddo_contratounidades_produtividade_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Filteredtextto_get", StringUtil.RTrim( Ddo_contratounidades_produtividade_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormIM2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptContratoUnidades" ;
      }

      public override String GetPgmdesc( )
      {
         return "Prompt Contrato Unidades" ;
      }

      protected void WBIM0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_IM2( true) ;
         }
         else
         {
            wb_table1_2_IM2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_IM2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_26_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratounidades_contratocod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16TFContratoUnidades_ContratoCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV16TFContratoUnidades_ContratoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,37);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratounidades_contratocod_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratounidades_contratocod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoUnidades.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'" + sGXsfl_26_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratounidades_contratocod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17TFContratoUnidades_ContratoCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV17TFContratoUnidades_ContratoCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,38);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratounidades_contratocod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratounidades_contratocod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoUnidades.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'" + sGXsfl_26_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratounidades_undmedcod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20TFContratoUnidades_UndMedCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV20TFContratoUnidades_UndMedCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratounidades_undmedcod_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratounidades_undmedcod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoUnidades.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_26_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratounidades_undmedcod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21TFContratoUnidades_UndMedCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV21TFContratoUnidades_UndMedCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,40);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratounidades_undmedcod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratounidades_undmedcod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoUnidades.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'" + sGXsfl_26_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratounidades_undmednom_Internalname, StringUtil.RTrim( AV24TFContratoUnidades_UndMedNom), StringUtil.RTrim( context.localUtil.Format( AV24TFContratoUnidades_UndMedNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,41);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratounidades_undmednom_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratounidades_undmednom_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoUnidades.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_26_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratounidades_undmednom_sel_Internalname, StringUtil.RTrim( AV25TFContratoUnidades_UndMedNom_Sel), StringUtil.RTrim( context.localUtil.Format( AV25TFContratoUnidades_UndMedNom_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,42);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratounidades_undmednom_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratounidades_undmednom_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoUnidades.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'" + sGXsfl_26_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratounidades_undmedsigla_Internalname, StringUtil.RTrim( AV28TFContratoUnidades_UndMedSigla), StringUtil.RTrim( context.localUtil.Format( AV28TFContratoUnidades_UndMedSigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,43);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratounidades_undmedsigla_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratounidades_undmedsigla_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoUnidades.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'" + sGXsfl_26_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratounidades_undmedsigla_sel_Internalname, StringUtil.RTrim( AV29TFContratoUnidades_UndMedSigla_Sel), StringUtil.RTrim( context.localUtil.Format( AV29TFContratoUnidades_UndMedSigla_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratounidades_undmedsigla_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratounidades_undmedsigla_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoUnidades.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'" + sGXsfl_26_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratounidades_produtividade_Internalname, StringUtil.LTrim( StringUtil.NToC( AV32TFContratoUnidades_Produtividade, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV32TFContratoUnidades_Produtividade, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,45);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratounidades_produtividade_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratounidades_produtividade_Visible, 1, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoUnidades.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'" + sGXsfl_26_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratounidades_produtividade_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV33TFContratoUnidades_Produtividade_To, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV33TFContratoUnidades_Produtividade_To, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,46);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratounidades_produtividade_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratounidades_produtividade_to_Visible, 1, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoUnidades.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOUNIDADES_CONTRATOCODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'" + sGXsfl_26_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratounidades_contratocodtitlecontrolidtoreplace_Internalname, AV18ddo_ContratoUnidades_ContratoCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,48);\"", 0, edtavDdo_contratounidades_contratocodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoUnidades.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOUNIDADES_UNDMEDCODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_26_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratounidades_undmedcodtitlecontrolidtoreplace_Internalname, AV22ddo_ContratoUnidades_UndMedCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", 0, edtavDdo_contratounidades_undmedcodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoUnidades.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOUNIDADES_UNDMEDNOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_26_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratounidades_undmednomtitlecontrolidtoreplace_Internalname, AV26ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,52);\"", 0, edtavDdo_contratounidades_undmednomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoUnidades.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOUNIDADES_UNDMEDSIGLAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'" + sGXsfl_26_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratounidades_undmedsiglatitlecontrolidtoreplace_Internalname, AV30ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", 0, edtavDdo_contratounidades_undmedsiglatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoUnidades.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOUNIDADES_PRODUTIVIDADEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'" + sGXsfl_26_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratounidades_produtividadetitlecontrolidtoreplace_Internalname, AV34ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", 0, edtavDdo_contratounidades_produtividadetitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoUnidades.htm");
         }
         wbLoad = true;
      }

      protected void STARTIM2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Prompt Contrato Unidades", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPIM0( ) ;
      }

      protected void WSIM2( )
      {
         STARTIM2( ) ;
         EVTIM2( ) ;
      }

      protected void EVTIM2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11IM2 */
                           E11IM2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOUNIDADES_CONTRATOCOD.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12IM2 */
                           E12IM2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOUNIDADES_UNDMEDCOD.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13IM2 */
                           E13IM2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOUNIDADES_UNDMEDNOM.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14IM2 */
                           E14IM2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15IM2 */
                           E15IM2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16IM2 */
                           E16IM2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17IM2 */
                           E17IM2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_26_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_26_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_26_idx), 4, 0)), 4, "0");
                           SubsflControlProps_262( ) ;
                           AV11Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV11Select)) ? AV41Select_GXI : context.convertURL( context.PathToRelativeUrl( AV11Select))));
                           A1207ContratoUnidades_ContratoCod = (int)(context.localUtil.CToN( cgiGet( edtContratoUnidades_ContratoCod_Internalname), ",", "."));
                           A1204ContratoUnidades_UndMedCod = (int)(context.localUtil.CToN( cgiGet( edtContratoUnidades_UndMedCod_Internalname), ",", "."));
                           A1205ContratoUnidades_UndMedNom = StringUtil.Upper( cgiGet( edtContratoUnidades_UndMedNom_Internalname));
                           n1205ContratoUnidades_UndMedNom = false;
                           A1206ContratoUnidades_UndMedSigla = StringUtil.Upper( cgiGet( edtContratoUnidades_UndMedSigla_Internalname));
                           n1206ContratoUnidades_UndMedSigla = false;
                           A1208ContratoUnidades_Produtividade = context.localUtil.CToN( cgiGet( edtContratoUnidades_Produtividade_Internalname), ",", ".");
                           n1208ContratoUnidades_Produtividade = false;
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E18IM2 */
                                 E18IM2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E19IM2 */
                                 E19IM2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E20IM2 */
                                 E20IM2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV9OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV10OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratounidades_contratocod Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOUNIDADES_CONTRATOCOD"), ",", ".") != Convert.ToDecimal( AV16TFContratoUnidades_ContratoCod )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratounidades_contratocod_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOUNIDADES_CONTRATOCOD_TO"), ",", ".") != Convert.ToDecimal( AV17TFContratoUnidades_ContratoCod_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratounidades_undmedcod Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOUNIDADES_UNDMEDCOD"), ",", ".") != Convert.ToDecimal( AV20TFContratoUnidades_UndMedCod )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratounidades_undmedcod_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOUNIDADES_UNDMEDCOD_TO"), ",", ".") != Convert.ToDecimal( AV21TFContratoUnidades_UndMedCod_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratounidades_undmednom Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOUNIDADES_UNDMEDNOM"), AV24TFContratoUnidades_UndMedNom) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratounidades_undmednom_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOUNIDADES_UNDMEDNOM_SEL"), AV25TFContratoUnidades_UndMedNom_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratounidades_undmedsigla Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOUNIDADES_UNDMEDSIGLA"), AV28TFContratoUnidades_UndMedSigla) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratounidades_undmedsigla_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOUNIDADES_UNDMEDSIGLA_SEL"), AV29TFContratoUnidades_UndMedSigla_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratounidades_produtividade Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOUNIDADES_PRODUTIVIDADE"), ",", ".") != AV32TFContratoUnidades_Produtividade )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratounidades_produtividade_to Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOUNIDADES_PRODUTIVIDADE_TO"), ",", ".") != AV33TFContratoUnidades_Produtividade_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E21IM2 */
                                       E21IM2 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEIM2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormIM2( ) ;
            }
         }
      }

      protected void PAIM2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV9OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9OrderedBy), 4, 0)));
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_262( ) ;
         while ( nGXsfl_26_idx <= nRC_GXsfl_26 )
         {
            sendrow_262( ) ;
            nGXsfl_26_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_26_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_26_idx+1));
            sGXsfl_26_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_26_idx), 4, 0)), 4, "0");
            SubsflControlProps_262( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV9OrderedBy ,
                                       bool AV10OrderedDsc ,
                                       int AV16TFContratoUnidades_ContratoCod ,
                                       int AV17TFContratoUnidades_ContratoCod_To ,
                                       int AV20TFContratoUnidades_UndMedCod ,
                                       int AV21TFContratoUnidades_UndMedCod_To ,
                                       String AV24TFContratoUnidades_UndMedNom ,
                                       String AV25TFContratoUnidades_UndMedNom_Sel ,
                                       String AV28TFContratoUnidades_UndMedSigla ,
                                       String AV29TFContratoUnidades_UndMedSigla_Sel ,
                                       decimal AV32TFContratoUnidades_Produtividade ,
                                       decimal AV33TFContratoUnidades_Produtividade_To ,
                                       String AV18ddo_ContratoUnidades_ContratoCodTitleControlIdToReplace ,
                                       String AV22ddo_ContratoUnidades_UndMedCodTitleControlIdToReplace ,
                                       String AV26ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace ,
                                       String AV30ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace ,
                                       String AV34ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace ,
                                       String AV42Pgmname )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFIM2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOUNIDADES_CONTRATOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1207ContratoUnidades_ContratoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATOUNIDADES_CONTRATOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1207ContratoUnidades_ContratoCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOUNIDADES_UNDMEDCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1204ContratoUnidades_UndMedCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATOUNIDADES_UNDMEDCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1204ContratoUnidades_UndMedCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOUNIDADES_PRODUTIVIDADE", GetSecureSignedToken( "", context.localUtil.Format( A1208ContratoUnidades_Produtividade, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "CONTRATOUNIDADES_PRODUTIVIDADE", StringUtil.LTrim( StringUtil.NToC( A1208ContratoUnidades_Produtividade, 14, 5, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV9OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9OrderedBy), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFIM2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV42Pgmname = "PromptContratoUnidades";
         context.Gx_err = 0;
      }

      protected void RFIM2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 26;
         /* Execute user event: E19IM2 */
         E19IM2 ();
         nGXsfl_26_idx = 1;
         sGXsfl_26_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_26_idx), 4, 0)), 4, "0");
         SubsflControlProps_262( ) ;
         nGXsfl_26_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_262( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV16TFContratoUnidades_ContratoCod ,
                                                 AV17TFContratoUnidades_ContratoCod_To ,
                                                 AV20TFContratoUnidades_UndMedCod ,
                                                 AV21TFContratoUnidades_UndMedCod_To ,
                                                 AV25TFContratoUnidades_UndMedNom_Sel ,
                                                 AV24TFContratoUnidades_UndMedNom ,
                                                 AV29TFContratoUnidades_UndMedSigla_Sel ,
                                                 AV28TFContratoUnidades_UndMedSigla ,
                                                 AV32TFContratoUnidades_Produtividade ,
                                                 AV33TFContratoUnidades_Produtividade_To ,
                                                 A1207ContratoUnidades_ContratoCod ,
                                                 A1204ContratoUnidades_UndMedCod ,
                                                 A1205ContratoUnidades_UndMedNom ,
                                                 A1206ContratoUnidades_UndMedSigla ,
                                                 A1208ContratoUnidades_Produtividade ,
                                                 AV9OrderedBy ,
                                                 AV10OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV24TFContratoUnidades_UndMedNom = StringUtil.PadR( StringUtil.RTrim( AV24TFContratoUnidades_UndMedNom), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24TFContratoUnidades_UndMedNom", AV24TFContratoUnidades_UndMedNom);
            lV28TFContratoUnidades_UndMedSigla = StringUtil.PadR( StringUtil.RTrim( AV28TFContratoUnidades_UndMedSigla), 15, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28TFContratoUnidades_UndMedSigla", AV28TFContratoUnidades_UndMedSigla);
            /* Using cursor H00IM2 */
            pr_default.execute(0, new Object[] {AV16TFContratoUnidades_ContratoCod, AV17TFContratoUnidades_ContratoCod_To, AV20TFContratoUnidades_UndMedCod, AV21TFContratoUnidades_UndMedCod_To, lV24TFContratoUnidades_UndMedNom, AV25TFContratoUnidades_UndMedNom_Sel, lV28TFContratoUnidades_UndMedSigla, AV29TFContratoUnidades_UndMedSigla_Sel, AV32TFContratoUnidades_Produtividade, AV33TFContratoUnidades_Produtividade_To, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_26_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1208ContratoUnidades_Produtividade = H00IM2_A1208ContratoUnidades_Produtividade[0];
               n1208ContratoUnidades_Produtividade = H00IM2_n1208ContratoUnidades_Produtividade[0];
               A1206ContratoUnidades_UndMedSigla = H00IM2_A1206ContratoUnidades_UndMedSigla[0];
               n1206ContratoUnidades_UndMedSigla = H00IM2_n1206ContratoUnidades_UndMedSigla[0];
               A1205ContratoUnidades_UndMedNom = H00IM2_A1205ContratoUnidades_UndMedNom[0];
               n1205ContratoUnidades_UndMedNom = H00IM2_n1205ContratoUnidades_UndMedNom[0];
               A1204ContratoUnidades_UndMedCod = H00IM2_A1204ContratoUnidades_UndMedCod[0];
               A1207ContratoUnidades_ContratoCod = H00IM2_A1207ContratoUnidades_ContratoCod[0];
               A1206ContratoUnidades_UndMedSigla = H00IM2_A1206ContratoUnidades_UndMedSigla[0];
               n1206ContratoUnidades_UndMedSigla = H00IM2_n1206ContratoUnidades_UndMedSigla[0];
               A1205ContratoUnidades_UndMedNom = H00IM2_A1205ContratoUnidades_UndMedNom[0];
               n1205ContratoUnidades_UndMedNom = H00IM2_n1205ContratoUnidades_UndMedNom[0];
               /* Execute user event: E20IM2 */
               E20IM2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 26;
            WBIM0( ) ;
         }
         nGXsfl_26_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV16TFContratoUnidades_ContratoCod ,
                                              AV17TFContratoUnidades_ContratoCod_To ,
                                              AV20TFContratoUnidades_UndMedCod ,
                                              AV21TFContratoUnidades_UndMedCod_To ,
                                              AV25TFContratoUnidades_UndMedNom_Sel ,
                                              AV24TFContratoUnidades_UndMedNom ,
                                              AV29TFContratoUnidades_UndMedSigla_Sel ,
                                              AV28TFContratoUnidades_UndMedSigla ,
                                              AV32TFContratoUnidades_Produtividade ,
                                              AV33TFContratoUnidades_Produtividade_To ,
                                              A1207ContratoUnidades_ContratoCod ,
                                              A1204ContratoUnidades_UndMedCod ,
                                              A1205ContratoUnidades_UndMedNom ,
                                              A1206ContratoUnidades_UndMedSigla ,
                                              A1208ContratoUnidades_Produtividade ,
                                              AV9OrderedBy ,
                                              AV10OrderedDsc },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV24TFContratoUnidades_UndMedNom = StringUtil.PadR( StringUtil.RTrim( AV24TFContratoUnidades_UndMedNom), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24TFContratoUnidades_UndMedNom", AV24TFContratoUnidades_UndMedNom);
         lV28TFContratoUnidades_UndMedSigla = StringUtil.PadR( StringUtil.RTrim( AV28TFContratoUnidades_UndMedSigla), 15, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28TFContratoUnidades_UndMedSigla", AV28TFContratoUnidades_UndMedSigla);
         /* Using cursor H00IM3 */
         pr_default.execute(1, new Object[] {AV16TFContratoUnidades_ContratoCod, AV17TFContratoUnidades_ContratoCod_To, AV20TFContratoUnidades_UndMedCod, AV21TFContratoUnidades_UndMedCod_To, lV24TFContratoUnidades_UndMedNom, AV25TFContratoUnidades_UndMedNom_Sel, lV28TFContratoUnidades_UndMedSigla, AV29TFContratoUnidades_UndMedSigla_Sel, AV32TFContratoUnidades_Produtividade, AV33TFContratoUnidades_Produtividade_To});
         GRID_nRecordCount = H00IM3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV9OrderedBy, AV10OrderedDsc, AV16TFContratoUnidades_ContratoCod, AV17TFContratoUnidades_ContratoCod_To, AV20TFContratoUnidades_UndMedCod, AV21TFContratoUnidades_UndMedCod_To, AV24TFContratoUnidades_UndMedNom, AV25TFContratoUnidades_UndMedNom_Sel, AV28TFContratoUnidades_UndMedSigla, AV29TFContratoUnidades_UndMedSigla_Sel, AV32TFContratoUnidades_Produtividade, AV33TFContratoUnidades_Produtividade_To, AV18ddo_ContratoUnidades_ContratoCodTitleControlIdToReplace, AV22ddo_ContratoUnidades_UndMedCodTitleControlIdToReplace, AV26ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace, AV30ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace, AV34ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace, AV42Pgmname) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV9OrderedBy, AV10OrderedDsc, AV16TFContratoUnidades_ContratoCod, AV17TFContratoUnidades_ContratoCod_To, AV20TFContratoUnidades_UndMedCod, AV21TFContratoUnidades_UndMedCod_To, AV24TFContratoUnidades_UndMedNom, AV25TFContratoUnidades_UndMedNom_Sel, AV28TFContratoUnidades_UndMedSigla, AV29TFContratoUnidades_UndMedSigla_Sel, AV32TFContratoUnidades_Produtividade, AV33TFContratoUnidades_Produtividade_To, AV18ddo_ContratoUnidades_ContratoCodTitleControlIdToReplace, AV22ddo_ContratoUnidades_UndMedCodTitleControlIdToReplace, AV26ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace, AV30ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace, AV34ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace, AV42Pgmname) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV9OrderedBy, AV10OrderedDsc, AV16TFContratoUnidades_ContratoCod, AV17TFContratoUnidades_ContratoCod_To, AV20TFContratoUnidades_UndMedCod, AV21TFContratoUnidades_UndMedCod_To, AV24TFContratoUnidades_UndMedNom, AV25TFContratoUnidades_UndMedNom_Sel, AV28TFContratoUnidades_UndMedSigla, AV29TFContratoUnidades_UndMedSigla_Sel, AV32TFContratoUnidades_Produtividade, AV33TFContratoUnidades_Produtividade_To, AV18ddo_ContratoUnidades_ContratoCodTitleControlIdToReplace, AV22ddo_ContratoUnidades_UndMedCodTitleControlIdToReplace, AV26ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace, AV30ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace, AV34ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace, AV42Pgmname) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV9OrderedBy, AV10OrderedDsc, AV16TFContratoUnidades_ContratoCod, AV17TFContratoUnidades_ContratoCod_To, AV20TFContratoUnidades_UndMedCod, AV21TFContratoUnidades_UndMedCod_To, AV24TFContratoUnidades_UndMedNom, AV25TFContratoUnidades_UndMedNom_Sel, AV28TFContratoUnidades_UndMedSigla, AV29TFContratoUnidades_UndMedSigla_Sel, AV32TFContratoUnidades_Produtividade, AV33TFContratoUnidades_Produtividade_To, AV18ddo_ContratoUnidades_ContratoCodTitleControlIdToReplace, AV22ddo_ContratoUnidades_UndMedCodTitleControlIdToReplace, AV26ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace, AV30ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace, AV34ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace, AV42Pgmname) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV9OrderedBy, AV10OrderedDsc, AV16TFContratoUnidades_ContratoCod, AV17TFContratoUnidades_ContratoCod_To, AV20TFContratoUnidades_UndMedCod, AV21TFContratoUnidades_UndMedCod_To, AV24TFContratoUnidades_UndMedNom, AV25TFContratoUnidades_UndMedNom_Sel, AV28TFContratoUnidades_UndMedSigla, AV29TFContratoUnidades_UndMedSigla_Sel, AV32TFContratoUnidades_Produtividade, AV33TFContratoUnidades_Produtividade_To, AV18ddo_ContratoUnidades_ContratoCodTitleControlIdToReplace, AV22ddo_ContratoUnidades_UndMedCodTitleControlIdToReplace, AV26ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace, AV30ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace, AV34ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace, AV42Pgmname) ;
         }
         return (int)(0) ;
      }

      protected void STRUPIM0( )
      {
         /* Before Start, stand alone formulas. */
         AV42Pgmname = "PromptContratoUnidades";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E18IM2 */
         E18IM2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV35DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOUNIDADES_CONTRATOCODTITLEFILTERDATA"), AV15ContratoUnidades_ContratoCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOUNIDADES_UNDMEDCODTITLEFILTERDATA"), AV19ContratoUnidades_UndMedCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOUNIDADES_UNDMEDNOMTITLEFILTERDATA"), AV23ContratoUnidades_UndMedNomTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOUNIDADES_UNDMEDSIGLATITLEFILTERDATA"), AV27ContratoUnidades_UndMedSiglaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOUNIDADES_PRODUTIVIDADETITLEFILTERDATA"), AV31ContratoUnidades_ProdutividadeTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV9OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9OrderedBy), 4, 0)));
            AV10OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10OrderedDsc", AV10OrderedDsc);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratounidades_contratocod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratounidades_contratocod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOUNIDADES_CONTRATOCOD");
               GX_FocusControl = edtavTfcontratounidades_contratocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV16TFContratoUnidades_ContratoCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16TFContratoUnidades_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16TFContratoUnidades_ContratoCod), 6, 0)));
            }
            else
            {
               AV16TFContratoUnidades_ContratoCod = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratounidades_contratocod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16TFContratoUnidades_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16TFContratoUnidades_ContratoCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratounidades_contratocod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratounidades_contratocod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOUNIDADES_CONTRATOCOD_TO");
               GX_FocusControl = edtavTfcontratounidades_contratocod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV17TFContratoUnidades_ContratoCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17TFContratoUnidades_ContratoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17TFContratoUnidades_ContratoCod_To), 6, 0)));
            }
            else
            {
               AV17TFContratoUnidades_ContratoCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratounidades_contratocod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17TFContratoUnidades_ContratoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17TFContratoUnidades_ContratoCod_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratounidades_undmedcod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratounidades_undmedcod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOUNIDADES_UNDMEDCOD");
               GX_FocusControl = edtavTfcontratounidades_undmedcod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV20TFContratoUnidades_UndMedCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20TFContratoUnidades_UndMedCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20TFContratoUnidades_UndMedCod), 6, 0)));
            }
            else
            {
               AV20TFContratoUnidades_UndMedCod = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratounidades_undmedcod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20TFContratoUnidades_UndMedCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20TFContratoUnidades_UndMedCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratounidades_undmedcod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratounidades_undmedcod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOUNIDADES_UNDMEDCOD_TO");
               GX_FocusControl = edtavTfcontratounidades_undmedcod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV21TFContratoUnidades_UndMedCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TFContratoUnidades_UndMedCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21TFContratoUnidades_UndMedCod_To), 6, 0)));
            }
            else
            {
               AV21TFContratoUnidades_UndMedCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratounidades_undmedcod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TFContratoUnidades_UndMedCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21TFContratoUnidades_UndMedCod_To), 6, 0)));
            }
            AV24TFContratoUnidades_UndMedNom = StringUtil.Upper( cgiGet( edtavTfcontratounidades_undmednom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24TFContratoUnidades_UndMedNom", AV24TFContratoUnidades_UndMedNom);
            AV25TFContratoUnidades_UndMedNom_Sel = StringUtil.Upper( cgiGet( edtavTfcontratounidades_undmednom_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TFContratoUnidades_UndMedNom_Sel", AV25TFContratoUnidades_UndMedNom_Sel);
            AV28TFContratoUnidades_UndMedSigla = StringUtil.Upper( cgiGet( edtavTfcontratounidades_undmedsigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28TFContratoUnidades_UndMedSigla", AV28TFContratoUnidades_UndMedSigla);
            AV29TFContratoUnidades_UndMedSigla_Sel = StringUtil.Upper( cgiGet( edtavTfcontratounidades_undmedsigla_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29TFContratoUnidades_UndMedSigla_Sel", AV29TFContratoUnidades_UndMedSigla_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratounidades_produtividade_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratounidades_produtividade_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOUNIDADES_PRODUTIVIDADE");
               GX_FocusControl = edtavTfcontratounidades_produtividade_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV32TFContratoUnidades_Produtividade = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFContratoUnidades_Produtividade", StringUtil.LTrim( StringUtil.Str( AV32TFContratoUnidades_Produtividade, 14, 5)));
            }
            else
            {
               AV32TFContratoUnidades_Produtividade = context.localUtil.CToN( cgiGet( edtavTfcontratounidades_produtividade_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFContratoUnidades_Produtividade", StringUtil.LTrim( StringUtil.Str( AV32TFContratoUnidades_Produtividade, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratounidades_produtividade_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratounidades_produtividade_to_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOUNIDADES_PRODUTIVIDADE_TO");
               GX_FocusControl = edtavTfcontratounidades_produtividade_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV33TFContratoUnidades_Produtividade_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TFContratoUnidades_Produtividade_To", StringUtil.LTrim( StringUtil.Str( AV33TFContratoUnidades_Produtividade_To, 14, 5)));
            }
            else
            {
               AV33TFContratoUnidades_Produtividade_To = context.localUtil.CToN( cgiGet( edtavTfcontratounidades_produtividade_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TFContratoUnidades_Produtividade_To", StringUtil.LTrim( StringUtil.Str( AV33TFContratoUnidades_Produtividade_To, 14, 5)));
            }
            AV18ddo_ContratoUnidades_ContratoCodTitleControlIdToReplace = cgiGet( edtavDdo_contratounidades_contratocodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ddo_ContratoUnidades_ContratoCodTitleControlIdToReplace", AV18ddo_ContratoUnidades_ContratoCodTitleControlIdToReplace);
            AV22ddo_ContratoUnidades_UndMedCodTitleControlIdToReplace = cgiGet( edtavDdo_contratounidades_undmedcodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ddo_ContratoUnidades_UndMedCodTitleControlIdToReplace", AV22ddo_ContratoUnidades_UndMedCodTitleControlIdToReplace);
            AV26ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace = cgiGet( edtavDdo_contratounidades_undmednomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace", AV26ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace);
            AV30ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace = cgiGet( edtavDdo_contratounidades_undmedsiglatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace", AV30ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace);
            AV34ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace = cgiGet( edtavDdo_contratounidades_produtividadetitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace", AV34ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_26 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_26"), ",", "."));
            AV37GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV38GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contratounidades_contratocod_Caption = cgiGet( "DDO_CONTRATOUNIDADES_CONTRATOCOD_Caption");
            Ddo_contratounidades_contratocod_Tooltip = cgiGet( "DDO_CONTRATOUNIDADES_CONTRATOCOD_Tooltip");
            Ddo_contratounidades_contratocod_Cls = cgiGet( "DDO_CONTRATOUNIDADES_CONTRATOCOD_Cls");
            Ddo_contratounidades_contratocod_Dropdownoptionstype = cgiGet( "DDO_CONTRATOUNIDADES_CONTRATOCOD_Dropdownoptionstype");
            Ddo_contratounidades_contratocod_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOUNIDADES_CONTRATOCOD_Titlecontrolidtoreplace");
            Ddo_contratounidades_contratocod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOUNIDADES_CONTRATOCOD_Includesortasc"));
            Ddo_contratounidades_contratocod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOUNIDADES_CONTRATOCOD_Includesortdsc"));
            Ddo_contratounidades_contratocod_Sortedstatus = cgiGet( "DDO_CONTRATOUNIDADES_CONTRATOCOD_Sortedstatus");
            Ddo_contratounidades_contratocod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOUNIDADES_CONTRATOCOD_Includefilter"));
            Ddo_contratounidades_contratocod_Filtertype = cgiGet( "DDO_CONTRATOUNIDADES_CONTRATOCOD_Filtertype");
            Ddo_contratounidades_contratocod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOUNIDADES_CONTRATOCOD_Filterisrange"));
            Ddo_contratounidades_contratocod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOUNIDADES_CONTRATOCOD_Includedatalist"));
            Ddo_contratounidades_contratocod_Datalistfixedvalues = cgiGet( "DDO_CONTRATOUNIDADES_CONTRATOCOD_Datalistfixedvalues");
            Ddo_contratounidades_contratocod_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOUNIDADES_CONTRATOCOD_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratounidades_contratocod_Sortasc = cgiGet( "DDO_CONTRATOUNIDADES_CONTRATOCOD_Sortasc");
            Ddo_contratounidades_contratocod_Sortdsc = cgiGet( "DDO_CONTRATOUNIDADES_CONTRATOCOD_Sortdsc");
            Ddo_contratounidades_contratocod_Loadingdata = cgiGet( "DDO_CONTRATOUNIDADES_CONTRATOCOD_Loadingdata");
            Ddo_contratounidades_contratocod_Cleanfilter = cgiGet( "DDO_CONTRATOUNIDADES_CONTRATOCOD_Cleanfilter");
            Ddo_contratounidades_contratocod_Rangefilterfrom = cgiGet( "DDO_CONTRATOUNIDADES_CONTRATOCOD_Rangefilterfrom");
            Ddo_contratounidades_contratocod_Rangefilterto = cgiGet( "DDO_CONTRATOUNIDADES_CONTRATOCOD_Rangefilterto");
            Ddo_contratounidades_contratocod_Noresultsfound = cgiGet( "DDO_CONTRATOUNIDADES_CONTRATOCOD_Noresultsfound");
            Ddo_contratounidades_contratocod_Searchbuttontext = cgiGet( "DDO_CONTRATOUNIDADES_CONTRATOCOD_Searchbuttontext");
            Ddo_contratounidades_undmedcod_Caption = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDCOD_Caption");
            Ddo_contratounidades_undmedcod_Tooltip = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDCOD_Tooltip");
            Ddo_contratounidades_undmedcod_Cls = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDCOD_Cls");
            Ddo_contratounidades_undmedcod_Dropdownoptionstype = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDCOD_Dropdownoptionstype");
            Ddo_contratounidades_undmedcod_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDCOD_Titlecontrolidtoreplace");
            Ddo_contratounidades_undmedcod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDCOD_Includesortasc"));
            Ddo_contratounidades_undmedcod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDCOD_Includesortdsc"));
            Ddo_contratounidades_undmedcod_Sortedstatus = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDCOD_Sortedstatus");
            Ddo_contratounidades_undmedcod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDCOD_Includefilter"));
            Ddo_contratounidades_undmedcod_Filtertype = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDCOD_Filtertype");
            Ddo_contratounidades_undmedcod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDCOD_Filterisrange"));
            Ddo_contratounidades_undmedcod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDCOD_Includedatalist"));
            Ddo_contratounidades_undmedcod_Datalistfixedvalues = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDCOD_Datalistfixedvalues");
            Ddo_contratounidades_undmedcod_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDCOD_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratounidades_undmedcod_Sortasc = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDCOD_Sortasc");
            Ddo_contratounidades_undmedcod_Sortdsc = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDCOD_Sortdsc");
            Ddo_contratounidades_undmedcod_Loadingdata = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDCOD_Loadingdata");
            Ddo_contratounidades_undmedcod_Cleanfilter = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDCOD_Cleanfilter");
            Ddo_contratounidades_undmedcod_Rangefilterfrom = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDCOD_Rangefilterfrom");
            Ddo_contratounidades_undmedcod_Rangefilterto = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDCOD_Rangefilterto");
            Ddo_contratounidades_undmedcod_Noresultsfound = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDCOD_Noresultsfound");
            Ddo_contratounidades_undmedcod_Searchbuttontext = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDCOD_Searchbuttontext");
            Ddo_contratounidades_undmednom_Caption = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Caption");
            Ddo_contratounidades_undmednom_Tooltip = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Tooltip");
            Ddo_contratounidades_undmednom_Cls = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Cls");
            Ddo_contratounidades_undmednom_Dropdownoptionstype = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Dropdownoptionstype");
            Ddo_contratounidades_undmednom_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Titlecontrolidtoreplace");
            Ddo_contratounidades_undmednom_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Includesortasc"));
            Ddo_contratounidades_undmednom_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Includesortdsc"));
            Ddo_contratounidades_undmednom_Sortedstatus = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Sortedstatus");
            Ddo_contratounidades_undmednom_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Includefilter"));
            Ddo_contratounidades_undmednom_Filtertype = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Filtertype");
            Ddo_contratounidades_undmednom_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Filterisrange"));
            Ddo_contratounidades_undmednom_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Includedatalist"));
            Ddo_contratounidades_undmednom_Datalisttype = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Datalisttype");
            Ddo_contratounidades_undmednom_Datalistfixedvalues = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Datalistfixedvalues");
            Ddo_contratounidades_undmednom_Datalistproc = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Datalistproc");
            Ddo_contratounidades_undmednom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratounidades_undmednom_Sortasc = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Sortasc");
            Ddo_contratounidades_undmednom_Sortdsc = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Sortdsc");
            Ddo_contratounidades_undmednom_Loadingdata = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Loadingdata");
            Ddo_contratounidades_undmednom_Cleanfilter = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Cleanfilter");
            Ddo_contratounidades_undmednom_Rangefilterfrom = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Rangefilterfrom");
            Ddo_contratounidades_undmednom_Rangefilterto = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Rangefilterto");
            Ddo_contratounidades_undmednom_Noresultsfound = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Noresultsfound");
            Ddo_contratounidades_undmednom_Searchbuttontext = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Searchbuttontext");
            Ddo_contratounidades_undmedsigla_Caption = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Caption");
            Ddo_contratounidades_undmedsigla_Tooltip = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Tooltip");
            Ddo_contratounidades_undmedsigla_Cls = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Cls");
            Ddo_contratounidades_undmedsigla_Dropdownoptionstype = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Dropdownoptionstype");
            Ddo_contratounidades_undmedsigla_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Titlecontrolidtoreplace");
            Ddo_contratounidades_undmedsigla_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Includesortasc"));
            Ddo_contratounidades_undmedsigla_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Includesortdsc"));
            Ddo_contratounidades_undmedsigla_Sortedstatus = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Sortedstatus");
            Ddo_contratounidades_undmedsigla_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Includefilter"));
            Ddo_contratounidades_undmedsigla_Filtertype = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Filtertype");
            Ddo_contratounidades_undmedsigla_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Filterisrange"));
            Ddo_contratounidades_undmedsigla_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Includedatalist"));
            Ddo_contratounidades_undmedsigla_Datalisttype = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Datalisttype");
            Ddo_contratounidades_undmedsigla_Datalistfixedvalues = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Datalistfixedvalues");
            Ddo_contratounidades_undmedsigla_Datalistproc = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Datalistproc");
            Ddo_contratounidades_undmedsigla_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratounidades_undmedsigla_Sortasc = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Sortasc");
            Ddo_contratounidades_undmedsigla_Sortdsc = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Sortdsc");
            Ddo_contratounidades_undmedsigla_Loadingdata = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Loadingdata");
            Ddo_contratounidades_undmedsigla_Cleanfilter = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Cleanfilter");
            Ddo_contratounidades_undmedsigla_Rangefilterfrom = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Rangefilterfrom");
            Ddo_contratounidades_undmedsigla_Rangefilterto = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Rangefilterto");
            Ddo_contratounidades_undmedsigla_Noresultsfound = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Noresultsfound");
            Ddo_contratounidades_undmedsigla_Searchbuttontext = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Searchbuttontext");
            Ddo_contratounidades_produtividade_Caption = cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Caption");
            Ddo_contratounidades_produtividade_Tooltip = cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Tooltip");
            Ddo_contratounidades_produtividade_Cls = cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Cls");
            Ddo_contratounidades_produtividade_Dropdownoptionstype = cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Dropdownoptionstype");
            Ddo_contratounidades_produtividade_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Titlecontrolidtoreplace");
            Ddo_contratounidades_produtividade_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Includesortasc"));
            Ddo_contratounidades_produtividade_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Includesortdsc"));
            Ddo_contratounidades_produtividade_Sortedstatus = cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Sortedstatus");
            Ddo_contratounidades_produtividade_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Includefilter"));
            Ddo_contratounidades_produtividade_Filtertype = cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Filtertype");
            Ddo_contratounidades_produtividade_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Filterisrange"));
            Ddo_contratounidades_produtividade_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Includedatalist"));
            Ddo_contratounidades_produtividade_Datalistfixedvalues = cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Datalistfixedvalues");
            Ddo_contratounidades_produtividade_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratounidades_produtividade_Sortasc = cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Sortasc");
            Ddo_contratounidades_produtividade_Sortdsc = cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Sortdsc");
            Ddo_contratounidades_produtividade_Loadingdata = cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Loadingdata");
            Ddo_contratounidades_produtividade_Cleanfilter = cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Cleanfilter");
            Ddo_contratounidades_produtividade_Rangefilterfrom = cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Rangefilterfrom");
            Ddo_contratounidades_produtividade_Rangefilterto = cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Rangefilterto");
            Ddo_contratounidades_produtividade_Noresultsfound = cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Noresultsfound");
            Ddo_contratounidades_produtividade_Searchbuttontext = cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contratounidades_contratocod_Activeeventkey = cgiGet( "DDO_CONTRATOUNIDADES_CONTRATOCOD_Activeeventkey");
            Ddo_contratounidades_contratocod_Filteredtext_get = cgiGet( "DDO_CONTRATOUNIDADES_CONTRATOCOD_Filteredtext_get");
            Ddo_contratounidades_contratocod_Filteredtextto_get = cgiGet( "DDO_CONTRATOUNIDADES_CONTRATOCOD_Filteredtextto_get");
            Ddo_contratounidades_undmedcod_Activeeventkey = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDCOD_Activeeventkey");
            Ddo_contratounidades_undmedcod_Filteredtext_get = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDCOD_Filteredtext_get");
            Ddo_contratounidades_undmedcod_Filteredtextto_get = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDCOD_Filteredtextto_get");
            Ddo_contratounidades_undmednom_Activeeventkey = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Activeeventkey");
            Ddo_contratounidades_undmednom_Filteredtext_get = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Filteredtext_get");
            Ddo_contratounidades_undmednom_Selectedvalue_get = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Selectedvalue_get");
            Ddo_contratounidades_undmedsigla_Activeeventkey = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Activeeventkey");
            Ddo_contratounidades_undmedsigla_Filteredtext_get = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Filteredtext_get");
            Ddo_contratounidades_undmedsigla_Selectedvalue_get = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Selectedvalue_get");
            Ddo_contratounidades_produtividade_Activeeventkey = cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Activeeventkey");
            Ddo_contratounidades_produtividade_Filteredtext_get = cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Filteredtext_get");
            Ddo_contratounidades_produtividade_Filteredtextto_get = cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV9OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV10OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOUNIDADES_CONTRATOCOD"), ",", ".") != Convert.ToDecimal( AV16TFContratoUnidades_ContratoCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOUNIDADES_CONTRATOCOD_TO"), ",", ".") != Convert.ToDecimal( AV17TFContratoUnidades_ContratoCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOUNIDADES_UNDMEDCOD"), ",", ".") != Convert.ToDecimal( AV20TFContratoUnidades_UndMedCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOUNIDADES_UNDMEDCOD_TO"), ",", ".") != Convert.ToDecimal( AV21TFContratoUnidades_UndMedCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOUNIDADES_UNDMEDNOM"), AV24TFContratoUnidades_UndMedNom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOUNIDADES_UNDMEDNOM_SEL"), AV25TFContratoUnidades_UndMedNom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOUNIDADES_UNDMEDSIGLA"), AV28TFContratoUnidades_UndMedSigla) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOUNIDADES_UNDMEDSIGLA_SEL"), AV29TFContratoUnidades_UndMedSigla_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOUNIDADES_PRODUTIVIDADE"), ",", ".") != AV32TFContratoUnidades_Produtividade )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOUNIDADES_PRODUTIVIDADE_TO"), ",", ".") != AV33TFContratoUnidades_Produtividade_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E18IM2 */
         E18IM2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E18IM2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTfcontratounidades_contratocod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratounidades_contratocod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratounidades_contratocod_Visible), 5, 0)));
         edtavTfcontratounidades_contratocod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratounidades_contratocod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratounidades_contratocod_to_Visible), 5, 0)));
         edtavTfcontratounidades_undmedcod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratounidades_undmedcod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratounidades_undmedcod_Visible), 5, 0)));
         edtavTfcontratounidades_undmedcod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratounidades_undmedcod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratounidades_undmedcod_to_Visible), 5, 0)));
         edtavTfcontratounidades_undmednom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratounidades_undmednom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratounidades_undmednom_Visible), 5, 0)));
         edtavTfcontratounidades_undmednom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratounidades_undmednom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratounidades_undmednom_sel_Visible), 5, 0)));
         edtavTfcontratounidades_undmedsigla_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratounidades_undmedsigla_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratounidades_undmedsigla_Visible), 5, 0)));
         edtavTfcontratounidades_undmedsigla_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratounidades_undmedsigla_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratounidades_undmedsigla_sel_Visible), 5, 0)));
         edtavTfcontratounidades_produtividade_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratounidades_produtividade_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratounidades_produtividade_Visible), 5, 0)));
         edtavTfcontratounidades_produtividade_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratounidades_produtividade_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratounidades_produtividade_to_Visible), 5, 0)));
         Ddo_contratounidades_contratocod_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoUnidades_ContratoCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_contratocod_Internalname, "TitleControlIdToReplace", Ddo_contratounidades_contratocod_Titlecontrolidtoreplace);
         AV18ddo_ContratoUnidades_ContratoCodTitleControlIdToReplace = Ddo_contratounidades_contratocod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ddo_ContratoUnidades_ContratoCodTitleControlIdToReplace", AV18ddo_ContratoUnidades_ContratoCodTitleControlIdToReplace);
         edtavDdo_contratounidades_contratocodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratounidades_contratocodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratounidades_contratocodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratounidades_undmedcod_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoUnidades_UndMedCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_undmedcod_Internalname, "TitleControlIdToReplace", Ddo_contratounidades_undmedcod_Titlecontrolidtoreplace);
         AV22ddo_ContratoUnidades_UndMedCodTitleControlIdToReplace = Ddo_contratounidades_undmedcod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ddo_ContratoUnidades_UndMedCodTitleControlIdToReplace", AV22ddo_ContratoUnidades_UndMedCodTitleControlIdToReplace);
         edtavDdo_contratounidades_undmedcodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratounidades_undmedcodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratounidades_undmedcodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratounidades_undmednom_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoUnidades_UndMedNom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_undmednom_Internalname, "TitleControlIdToReplace", Ddo_contratounidades_undmednom_Titlecontrolidtoreplace);
         AV26ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace = Ddo_contratounidades_undmednom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace", AV26ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace);
         edtavDdo_contratounidades_undmednomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratounidades_undmednomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratounidades_undmednomtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratounidades_undmedsigla_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoUnidades_UndMedSigla";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_undmedsigla_Internalname, "TitleControlIdToReplace", Ddo_contratounidades_undmedsigla_Titlecontrolidtoreplace);
         AV30ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace = Ddo_contratounidades_undmedsigla_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace", AV30ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace);
         edtavDdo_contratounidades_undmedsiglatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratounidades_undmedsiglatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratounidades_undmedsiglatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratounidades_produtividade_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoUnidades_Produtividade";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_produtividade_Internalname, "TitleControlIdToReplace", Ddo_contratounidades_produtividade_Titlecontrolidtoreplace);
         AV34ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace = Ddo_contratounidades_produtividade_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace", AV34ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace);
         edtavDdo_contratounidades_produtividadetitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratounidades_produtividadetitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratounidades_produtividadetitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione Unidades Contratadas";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "de Contrata��o", 0);
         cmbavOrderedby.addItem("2", "Unidades_Contrato Cod", 0);
         cmbavOrderedby.addItem("3", "de Contrata��o", 0);
         cmbavOrderedby.addItem("4", "Sigla", 0);
         cmbavOrderedby.addItem("5", "diaria", 0);
         if ( AV9OrderedBy < 1 )
         {
            AV9OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV35DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV35DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E19IM2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV15ContratoUnidades_ContratoCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV19ContratoUnidades_UndMedCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV23ContratoUnidades_UndMedNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV27ContratoUnidades_UndMedSiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV31ContratoUnidades_ProdutividadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContratoUnidades_ContratoCod_Titleformat = 2;
         edtContratoUnidades_ContratoCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Unidades_Contrato Cod", AV18ddo_ContratoUnidades_ContratoCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoUnidades_ContratoCod_Internalname, "Title", edtContratoUnidades_ContratoCod_Title);
         edtContratoUnidades_UndMedCod_Titleformat = 2;
         edtContratoUnidades_UndMedCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "de Contrata��o", AV22ddo_ContratoUnidades_UndMedCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoUnidades_UndMedCod_Internalname, "Title", edtContratoUnidades_UndMedCod_Title);
         edtContratoUnidades_UndMedNom_Titleformat = 2;
         edtContratoUnidades_UndMedNom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "de Contrata��o", AV26ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoUnidades_UndMedNom_Internalname, "Title", edtContratoUnidades_UndMedNom_Title);
         edtContratoUnidades_UndMedSigla_Titleformat = 2;
         edtContratoUnidades_UndMedSigla_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Sigla", AV30ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoUnidades_UndMedSigla_Internalname, "Title", edtContratoUnidades_UndMedSigla_Title);
         edtContratoUnidades_Produtividade_Titleformat = 2;
         edtContratoUnidades_Produtividade_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "diaria", AV34ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoUnidades_Produtividade_Internalname, "Title", edtContratoUnidades_Produtividade_Title);
         AV37GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37GridCurrentPage), 10, 0)));
         AV38GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV15ContratoUnidades_ContratoCodTitleFilterData", AV15ContratoUnidades_ContratoCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV19ContratoUnidades_UndMedCodTitleFilterData", AV19ContratoUnidades_UndMedCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV23ContratoUnidades_UndMedNomTitleFilterData", AV23ContratoUnidades_UndMedNomTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV27ContratoUnidades_UndMedSiglaTitleFilterData", AV27ContratoUnidades_UndMedSiglaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV31ContratoUnidades_ProdutividadeTitleFilterData", AV31ContratoUnidades_ProdutividadeTitleFilterData);
      }

      protected void E11IM2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV36PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV36PageToGo) ;
         }
      }

      protected void E12IM2( )
      {
         /* Ddo_contratounidades_contratocod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratounidades_contratocod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV9OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9OrderedBy), 4, 0)));
            AV10OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10OrderedDsc", AV10OrderedDsc);
            Ddo_contratounidades_contratocod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_contratocod_Internalname, "SortedStatus", Ddo_contratounidades_contratocod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratounidades_contratocod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV9OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9OrderedBy), 4, 0)));
            AV10OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10OrderedDsc", AV10OrderedDsc);
            Ddo_contratounidades_contratocod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_contratocod_Internalname, "SortedStatus", Ddo_contratounidades_contratocod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratounidades_contratocod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV16TFContratoUnidades_ContratoCod = (int)(NumberUtil.Val( Ddo_contratounidades_contratocod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16TFContratoUnidades_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16TFContratoUnidades_ContratoCod), 6, 0)));
            AV17TFContratoUnidades_ContratoCod_To = (int)(NumberUtil.Val( Ddo_contratounidades_contratocod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17TFContratoUnidades_ContratoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17TFContratoUnidades_ContratoCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13IM2( )
      {
         /* Ddo_contratounidades_undmedcod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratounidades_undmedcod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV9OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9OrderedBy), 4, 0)));
            AV10OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10OrderedDsc", AV10OrderedDsc);
            Ddo_contratounidades_undmedcod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_undmedcod_Internalname, "SortedStatus", Ddo_contratounidades_undmedcod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratounidades_undmedcod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV9OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9OrderedBy), 4, 0)));
            AV10OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10OrderedDsc", AV10OrderedDsc);
            Ddo_contratounidades_undmedcod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_undmedcod_Internalname, "SortedStatus", Ddo_contratounidades_undmedcod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratounidades_undmedcod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV20TFContratoUnidades_UndMedCod = (int)(NumberUtil.Val( Ddo_contratounidades_undmedcod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20TFContratoUnidades_UndMedCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20TFContratoUnidades_UndMedCod), 6, 0)));
            AV21TFContratoUnidades_UndMedCod_To = (int)(NumberUtil.Val( Ddo_contratounidades_undmedcod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TFContratoUnidades_UndMedCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21TFContratoUnidades_UndMedCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14IM2( )
      {
         /* Ddo_contratounidades_undmednom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratounidades_undmednom_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV9OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9OrderedBy), 4, 0)));
            AV10OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10OrderedDsc", AV10OrderedDsc);
            Ddo_contratounidades_undmednom_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_undmednom_Internalname, "SortedStatus", Ddo_contratounidades_undmednom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratounidades_undmednom_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV9OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9OrderedBy), 4, 0)));
            AV10OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10OrderedDsc", AV10OrderedDsc);
            Ddo_contratounidades_undmednom_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_undmednom_Internalname, "SortedStatus", Ddo_contratounidades_undmednom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratounidades_undmednom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV24TFContratoUnidades_UndMedNom = Ddo_contratounidades_undmednom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24TFContratoUnidades_UndMedNom", AV24TFContratoUnidades_UndMedNom);
            AV25TFContratoUnidades_UndMedNom_Sel = Ddo_contratounidades_undmednom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TFContratoUnidades_UndMedNom_Sel", AV25TFContratoUnidades_UndMedNom_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E15IM2( )
      {
         /* Ddo_contratounidades_undmedsigla_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratounidades_undmedsigla_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV9OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9OrderedBy), 4, 0)));
            AV10OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10OrderedDsc", AV10OrderedDsc);
            Ddo_contratounidades_undmedsigla_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_undmedsigla_Internalname, "SortedStatus", Ddo_contratounidades_undmedsigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratounidades_undmedsigla_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV9OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9OrderedBy), 4, 0)));
            AV10OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10OrderedDsc", AV10OrderedDsc);
            Ddo_contratounidades_undmedsigla_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_undmedsigla_Internalname, "SortedStatus", Ddo_contratounidades_undmedsigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratounidades_undmedsigla_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV28TFContratoUnidades_UndMedSigla = Ddo_contratounidades_undmedsigla_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28TFContratoUnidades_UndMedSigla", AV28TFContratoUnidades_UndMedSigla);
            AV29TFContratoUnidades_UndMedSigla_Sel = Ddo_contratounidades_undmedsigla_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29TFContratoUnidades_UndMedSigla_Sel", AV29TFContratoUnidades_UndMedSigla_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E16IM2( )
      {
         /* Ddo_contratounidades_produtividade_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratounidades_produtividade_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV9OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9OrderedBy), 4, 0)));
            AV10OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10OrderedDsc", AV10OrderedDsc);
            Ddo_contratounidades_produtividade_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_produtividade_Internalname, "SortedStatus", Ddo_contratounidades_produtividade_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratounidades_produtividade_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV9OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9OrderedBy), 4, 0)));
            AV10OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10OrderedDsc", AV10OrderedDsc);
            Ddo_contratounidades_produtividade_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_produtividade_Internalname, "SortedStatus", Ddo_contratounidades_produtividade_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratounidades_produtividade_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV32TFContratoUnidades_Produtividade = NumberUtil.Val( Ddo_contratounidades_produtividade_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFContratoUnidades_Produtividade", StringUtil.LTrim( StringUtil.Str( AV32TFContratoUnidades_Produtividade, 14, 5)));
            AV33TFContratoUnidades_Produtividade_To = NumberUtil.Val( Ddo_contratounidades_produtividade_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TFContratoUnidades_Produtividade_To", StringUtil.LTrim( StringUtil.Str( AV33TFContratoUnidades_Produtividade_To, 14, 5)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E20IM2( )
      {
         /* Grid_Load Routine */
         AV11Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV11Select);
         AV41Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 26;
         }
         sendrow_262( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_26_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(26, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E21IM2 */
         E21IM2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E21IM2( )
      {
         /* Enter Routine */
         AV7InOutContratoUnidades_ContratoCod = A1207ContratoUnidades_ContratoCod;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContratoUnidades_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContratoUnidades_ContratoCod), 6, 0)));
         AV12InOutContratoUnidades_UndMedCod = A1204ContratoUnidades_UndMedCod;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12InOutContratoUnidades_UndMedCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12InOutContratoUnidades_UndMedCod), 6, 0)));
         AV8InOutContratoUnidades_UndMedNom = A1205ContratoUnidades_UndMedNom;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContratoUnidades_UndMedNom", AV8InOutContratoUnidades_UndMedNom);
         context.setWebReturnParms(new Object[] {(int)AV7InOutContratoUnidades_ContratoCod,(int)AV12InOutContratoUnidades_UndMedCod,(String)AV8InOutContratoUnidades_UndMedNom});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E17IM2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void S132( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contratounidades_contratocod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_contratocod_Internalname, "SortedStatus", Ddo_contratounidades_contratocod_Sortedstatus);
         Ddo_contratounidades_undmedcod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_undmedcod_Internalname, "SortedStatus", Ddo_contratounidades_undmedcod_Sortedstatus);
         Ddo_contratounidades_undmednom_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_undmednom_Internalname, "SortedStatus", Ddo_contratounidades_undmednom_Sortedstatus);
         Ddo_contratounidades_undmedsigla_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_undmedsigla_Internalname, "SortedStatus", Ddo_contratounidades_undmedsigla_Sortedstatus);
         Ddo_contratounidades_produtividade_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_produtividade_Internalname, "SortedStatus", Ddo_contratounidades_produtividade_Sortedstatus);
      }

      protected void S112( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV9OrderedBy == 2 )
         {
            Ddo_contratounidades_contratocod_Sortedstatus = (AV10OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_contratocod_Internalname, "SortedStatus", Ddo_contratounidades_contratocod_Sortedstatus);
         }
         else if ( AV9OrderedBy == 3 )
         {
            Ddo_contratounidades_undmedcod_Sortedstatus = (AV10OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_undmedcod_Internalname, "SortedStatus", Ddo_contratounidades_undmedcod_Sortedstatus);
         }
         else if ( AV9OrderedBy == 1 )
         {
            Ddo_contratounidades_undmednom_Sortedstatus = (AV10OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_undmednom_Internalname, "SortedStatus", Ddo_contratounidades_undmednom_Sortedstatus);
         }
         else if ( AV9OrderedBy == 4 )
         {
            Ddo_contratounidades_undmedsigla_Sortedstatus = (AV10OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_undmedsigla_Internalname, "SortedStatus", Ddo_contratounidades_undmedsigla_Sortedstatus);
         }
         else if ( AV9OrderedBy == 5 )
         {
            Ddo_contratounidades_produtividade_Sortedstatus = (AV10OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_produtividade_Internalname, "SortedStatus", Ddo_contratounidades_produtividade_Sortedstatus);
         }
      }

      protected void S122( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV13GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV13GridState.gxTpr_Orderedby = AV9OrderedBy;
         AV13GridState.gxTpr_Ordereddsc = AV10OrderedDsc;
         AV13GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (0==AV16TFContratoUnidades_ContratoCod) && (0==AV17TFContratoUnidades_ContratoCod_To) ) )
         {
            AV14GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV14GridStateFilterValue.gxTpr_Name = "TFCONTRATOUNIDADES_CONTRATOCOD";
            AV14GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV16TFContratoUnidades_ContratoCod), 6, 0);
            AV14GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV17TFContratoUnidades_ContratoCod_To), 6, 0);
            AV13GridState.gxTpr_Filtervalues.Add(AV14GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV20TFContratoUnidades_UndMedCod) && (0==AV21TFContratoUnidades_UndMedCod_To) ) )
         {
            AV14GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV14GridStateFilterValue.gxTpr_Name = "TFCONTRATOUNIDADES_UNDMEDCOD";
            AV14GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV20TFContratoUnidades_UndMedCod), 6, 0);
            AV14GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV21TFContratoUnidades_UndMedCod_To), 6, 0);
            AV13GridState.gxTpr_Filtervalues.Add(AV14GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24TFContratoUnidades_UndMedNom)) )
         {
            AV14GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV14GridStateFilterValue.gxTpr_Name = "TFCONTRATOUNIDADES_UNDMEDNOM";
            AV14GridStateFilterValue.gxTpr_Value = AV24TFContratoUnidades_UndMedNom;
            AV13GridState.gxTpr_Filtervalues.Add(AV14GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TFContratoUnidades_UndMedNom_Sel)) )
         {
            AV14GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV14GridStateFilterValue.gxTpr_Name = "TFCONTRATOUNIDADES_UNDMEDNOM_SEL";
            AV14GridStateFilterValue.gxTpr_Value = AV25TFContratoUnidades_UndMedNom_Sel;
            AV13GridState.gxTpr_Filtervalues.Add(AV14GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28TFContratoUnidades_UndMedSigla)) )
         {
            AV14GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV14GridStateFilterValue.gxTpr_Name = "TFCONTRATOUNIDADES_UNDMEDSIGLA";
            AV14GridStateFilterValue.gxTpr_Value = AV28TFContratoUnidades_UndMedSigla;
            AV13GridState.gxTpr_Filtervalues.Add(AV14GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29TFContratoUnidades_UndMedSigla_Sel)) )
         {
            AV14GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV14GridStateFilterValue.gxTpr_Name = "TFCONTRATOUNIDADES_UNDMEDSIGLA_SEL";
            AV14GridStateFilterValue.gxTpr_Value = AV29TFContratoUnidades_UndMedSigla_Sel;
            AV13GridState.gxTpr_Filtervalues.Add(AV14GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV32TFContratoUnidades_Produtividade) && (Convert.ToDecimal(0)==AV33TFContratoUnidades_Produtividade_To) ) )
         {
            AV14GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV14GridStateFilterValue.gxTpr_Name = "TFCONTRATOUNIDADES_PRODUTIVIDADE";
            AV14GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV32TFContratoUnidades_Produtividade, 14, 5);
            AV14GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV33TFContratoUnidades_Produtividade_To, 14, 5);
            AV13GridState.gxTpr_Filtervalues.Add(AV14GridStateFilterValue, 0);
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV42Pgmname+"GridState",  AV13GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void wb_table1_2_IM2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_IM2( true) ;
         }
         else
         {
            wb_table2_5_IM2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_IM2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_20_IM2( true) ;
         }
         else
         {
            wb_table3_20_IM2( false) ;
         }
         return  ;
      }

      protected void wb_table3_20_IM2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_IM2e( true) ;
         }
         else
         {
            wb_table1_2_IM2e( false) ;
         }
      }

      protected void wb_table3_20_IM2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_23_IM2( true) ;
         }
         else
         {
            wb_table4_23_IM2( false) ;
         }
         return  ;
      }

      protected void wb_table4_23_IM2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_20_IM2e( true) ;
         }
         else
         {
            wb_table3_20_IM2e( false) ;
         }
      }

      protected void wb_table4_23_IM2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"26\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoUnidades_ContratoCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoUnidades_ContratoCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoUnidades_ContratoCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoUnidades_UndMedCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoUnidades_UndMedCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoUnidades_UndMedCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoUnidades_UndMedNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoUnidades_UndMedNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoUnidades_UndMedNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoUnidades_UndMedSigla_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoUnidades_UndMedSigla_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoUnidades_UndMedSigla_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(80), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoUnidades_Produtividade_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoUnidades_Produtividade_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoUnidades_Produtividade_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV11Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1207ContratoUnidades_ContratoCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoUnidades_ContratoCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoUnidades_ContratoCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1204ContratoUnidades_UndMedCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoUnidades_UndMedCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoUnidades_UndMedCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1205ContratoUnidades_UndMedNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoUnidades_UndMedNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoUnidades_UndMedNom_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1206ContratoUnidades_UndMedSigla));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoUnidades_UndMedSigla_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoUnidades_UndMedSigla_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1208ContratoUnidades_Produtividade, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoUnidades_Produtividade_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoUnidades_Produtividade_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 26 )
         {
            wbEnd = 0;
            nRC_GXsfl_26 = (short)(nGXsfl_26_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_23_IM2e( true) ;
         }
         else
         {
            wb_table4_23_IM2e( false) ;
         }
      }

      protected void wb_table2_5_IM2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptContratoUnidades.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_26_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV9OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptContratoUnidades.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_26_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV10OrderedDsc), StringUtil.BoolToStr( AV10OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptContratoUnidades.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_14_IM2( true) ;
         }
         else
         {
            wb_table5_14_IM2( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_IM2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_IM2e( true) ;
         }
         else
         {
            wb_table2_5_IM2e( false) ;
         }
      }

      protected void wb_table5_14_IM2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_IM2e( true) ;
         }
         else
         {
            wb_table5_14_IM2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutContratoUnidades_ContratoCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContratoUnidades_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContratoUnidades_ContratoCod), 6, 0)));
         AV12InOutContratoUnidades_UndMedCod = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12InOutContratoUnidades_UndMedCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12InOutContratoUnidades_UndMedCod), 6, 0)));
         AV8InOutContratoUnidades_UndMedNom = (String)getParm(obj,2);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContratoUnidades_UndMedNom", AV8InOutContratoUnidades_UndMedNom);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAIM2( ) ;
         WSIM2( ) ;
         WEIM2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202042823435031");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptcontratounidades.js", "?202042823435031");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_262( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_26_idx;
         edtContratoUnidades_ContratoCod_Internalname = "CONTRATOUNIDADES_CONTRATOCOD_"+sGXsfl_26_idx;
         edtContratoUnidades_UndMedCod_Internalname = "CONTRATOUNIDADES_UNDMEDCOD_"+sGXsfl_26_idx;
         edtContratoUnidades_UndMedNom_Internalname = "CONTRATOUNIDADES_UNDMEDNOM_"+sGXsfl_26_idx;
         edtContratoUnidades_UndMedSigla_Internalname = "CONTRATOUNIDADES_UNDMEDSIGLA_"+sGXsfl_26_idx;
         edtContratoUnidades_Produtividade_Internalname = "CONTRATOUNIDADES_PRODUTIVIDADE_"+sGXsfl_26_idx;
      }

      protected void SubsflControlProps_fel_262( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_26_fel_idx;
         edtContratoUnidades_ContratoCod_Internalname = "CONTRATOUNIDADES_CONTRATOCOD_"+sGXsfl_26_fel_idx;
         edtContratoUnidades_UndMedCod_Internalname = "CONTRATOUNIDADES_UNDMEDCOD_"+sGXsfl_26_fel_idx;
         edtContratoUnidades_UndMedNom_Internalname = "CONTRATOUNIDADES_UNDMEDNOM_"+sGXsfl_26_fel_idx;
         edtContratoUnidades_UndMedSigla_Internalname = "CONTRATOUNIDADES_UNDMEDSIGLA_"+sGXsfl_26_fel_idx;
         edtContratoUnidades_Produtividade_Internalname = "CONTRATOUNIDADES_PRODUTIVIDADE_"+sGXsfl_26_fel_idx;
      }

      protected void sendrow_262( )
      {
         SubsflControlProps_262( ) ;
         WBIM0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_26_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_26_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_26_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 27,'',false,'',26)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV11Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV11Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV41Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV11Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV11Select)) ? AV41Select_GXI : context.PathToRelativeUrl( AV11Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_26_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV11Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoUnidades_ContratoCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1207ContratoUnidades_ContratoCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1207ContratoUnidades_ContratoCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoUnidades_ContratoCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)26,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoUnidades_UndMedCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1204ContratoUnidades_UndMedCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1204ContratoUnidades_UndMedCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoUnidades_UndMedCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)26,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoUnidades_UndMedNom_Internalname,StringUtil.RTrim( A1205ContratoUnidades_UndMedNom),StringUtil.RTrim( context.localUtil.Format( A1205ContratoUnidades_UndMedNom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoUnidades_UndMedNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)26,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoUnidades_UndMedSigla_Internalname,StringUtil.RTrim( A1206ContratoUnidades_UndMedSigla),StringUtil.RTrim( context.localUtil.Format( A1206ContratoUnidades_UndMedSigla, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoUnidades_UndMedSigla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)26,(short)1,(short)-1,(short)-1,(bool)true,(String)"Sigla",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoUnidades_Produtividade_Internalname,StringUtil.LTrim( StringUtil.NToC( A1208ContratoUnidades_Produtividade, 14, 5, ",", "")),context.localUtil.Format( A1208ContratoUnidades_Produtividade, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoUnidades_Produtividade_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)80,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)26,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOUNIDADES_CONTRATOCOD"+"_"+sGXsfl_26_idx, GetSecureSignedToken( sGXsfl_26_idx, context.localUtil.Format( (decimal)(A1207ContratoUnidades_ContratoCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOUNIDADES_UNDMEDCOD"+"_"+sGXsfl_26_idx, GetSecureSignedToken( sGXsfl_26_idx, context.localUtil.Format( (decimal)(A1204ContratoUnidades_UndMedCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOUNIDADES_PRODUTIVIDADE"+"_"+sGXsfl_26_idx, GetSecureSignedToken( sGXsfl_26_idx, context.localUtil.Format( A1208ContratoUnidades_Produtividade, "ZZ,ZZZ,ZZ9.999")));
            GridContainer.AddRow(GridRow);
            nGXsfl_26_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_26_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_26_idx+1));
            sGXsfl_26_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_26_idx), 4, 0)), 4, "0");
            SubsflControlProps_262( ) ;
         }
         /* End function sendrow_262 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtContratoUnidades_ContratoCod_Internalname = "CONTRATOUNIDADES_CONTRATOCOD";
         edtContratoUnidades_UndMedCod_Internalname = "CONTRATOUNIDADES_UNDMEDCOD";
         edtContratoUnidades_UndMedNom_Internalname = "CONTRATOUNIDADES_UNDMEDNOM";
         edtContratoUnidades_UndMedSigla_Internalname = "CONTRATOUNIDADES_UNDMEDSIGLA";
         edtContratoUnidades_Produtividade_Internalname = "CONTRATOUNIDADES_PRODUTIVIDADE";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         edtavTfcontratounidades_contratocod_Internalname = "vTFCONTRATOUNIDADES_CONTRATOCOD";
         edtavTfcontratounidades_contratocod_to_Internalname = "vTFCONTRATOUNIDADES_CONTRATOCOD_TO";
         edtavTfcontratounidades_undmedcod_Internalname = "vTFCONTRATOUNIDADES_UNDMEDCOD";
         edtavTfcontratounidades_undmedcod_to_Internalname = "vTFCONTRATOUNIDADES_UNDMEDCOD_TO";
         edtavTfcontratounidades_undmednom_Internalname = "vTFCONTRATOUNIDADES_UNDMEDNOM";
         edtavTfcontratounidades_undmednom_sel_Internalname = "vTFCONTRATOUNIDADES_UNDMEDNOM_SEL";
         edtavTfcontratounidades_undmedsigla_Internalname = "vTFCONTRATOUNIDADES_UNDMEDSIGLA";
         edtavTfcontratounidades_undmedsigla_sel_Internalname = "vTFCONTRATOUNIDADES_UNDMEDSIGLA_SEL";
         edtavTfcontratounidades_produtividade_Internalname = "vTFCONTRATOUNIDADES_PRODUTIVIDADE";
         edtavTfcontratounidades_produtividade_to_Internalname = "vTFCONTRATOUNIDADES_PRODUTIVIDADE_TO";
         Ddo_contratounidades_contratocod_Internalname = "DDO_CONTRATOUNIDADES_CONTRATOCOD";
         edtavDdo_contratounidades_contratocodtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOUNIDADES_CONTRATOCODTITLECONTROLIDTOREPLACE";
         Ddo_contratounidades_undmedcod_Internalname = "DDO_CONTRATOUNIDADES_UNDMEDCOD";
         edtavDdo_contratounidades_undmedcodtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOUNIDADES_UNDMEDCODTITLECONTROLIDTOREPLACE";
         Ddo_contratounidades_undmednom_Internalname = "DDO_CONTRATOUNIDADES_UNDMEDNOM";
         edtavDdo_contratounidades_undmednomtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOUNIDADES_UNDMEDNOMTITLECONTROLIDTOREPLACE";
         Ddo_contratounidades_undmedsigla_Internalname = "DDO_CONTRATOUNIDADES_UNDMEDSIGLA";
         edtavDdo_contratounidades_undmedsiglatitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOUNIDADES_UNDMEDSIGLATITLECONTROLIDTOREPLACE";
         Ddo_contratounidades_produtividade_Internalname = "DDO_CONTRATOUNIDADES_PRODUTIVIDADE";
         edtavDdo_contratounidades_produtividadetitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOUNIDADES_PRODUTIVIDADETITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtContratoUnidades_Produtividade_Jsonclick = "";
         edtContratoUnidades_UndMedSigla_Jsonclick = "";
         edtContratoUnidades_UndMedNom_Jsonclick = "";
         edtContratoUnidades_UndMedCod_Jsonclick = "";
         edtContratoUnidades_ContratoCod_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         edtContratoUnidades_Produtividade_Titleformat = 0;
         edtContratoUnidades_UndMedSigla_Titleformat = 0;
         edtContratoUnidades_UndMedNom_Titleformat = 0;
         edtContratoUnidades_UndMedCod_Titleformat = 0;
         edtContratoUnidades_ContratoCod_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtContratoUnidades_Produtividade_Title = "diaria";
         edtContratoUnidades_UndMedSigla_Title = "Sigla";
         edtContratoUnidades_UndMedNom_Title = "de Contrata��o";
         edtContratoUnidades_UndMedCod_Title = "de Contrata��o";
         edtContratoUnidades_ContratoCod_Title = "Unidades_Contrato Cod";
         edtavOrdereddsc_Visible = 1;
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_contratounidades_produtividadetitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratounidades_undmedsiglatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratounidades_undmednomtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratounidades_undmedcodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratounidades_contratocodtitlecontrolidtoreplace_Visible = 1;
         edtavTfcontratounidades_produtividade_to_Jsonclick = "";
         edtavTfcontratounidades_produtividade_to_Visible = 1;
         edtavTfcontratounidades_produtividade_Jsonclick = "";
         edtavTfcontratounidades_produtividade_Visible = 1;
         edtavTfcontratounidades_undmedsigla_sel_Jsonclick = "";
         edtavTfcontratounidades_undmedsigla_sel_Visible = 1;
         edtavTfcontratounidades_undmedsigla_Jsonclick = "";
         edtavTfcontratounidades_undmedsigla_Visible = 1;
         edtavTfcontratounidades_undmednom_sel_Jsonclick = "";
         edtavTfcontratounidades_undmednom_sel_Visible = 1;
         edtavTfcontratounidades_undmednom_Jsonclick = "";
         edtavTfcontratounidades_undmednom_Visible = 1;
         edtavTfcontratounidades_undmedcod_to_Jsonclick = "";
         edtavTfcontratounidades_undmedcod_to_Visible = 1;
         edtavTfcontratounidades_undmedcod_Jsonclick = "";
         edtavTfcontratounidades_undmedcod_Visible = 1;
         edtavTfcontratounidades_contratocod_to_Jsonclick = "";
         edtavTfcontratounidades_contratocod_to_Visible = 1;
         edtavTfcontratounidades_contratocod_Jsonclick = "";
         edtavTfcontratounidades_contratocod_Visible = 1;
         Ddo_contratounidades_produtividade_Searchbuttontext = "Pesquisar";
         Ddo_contratounidades_produtividade_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratounidades_produtividade_Rangefilterto = "At�";
         Ddo_contratounidades_produtividade_Rangefilterfrom = "Desde";
         Ddo_contratounidades_produtividade_Cleanfilter = "Limpar pesquisa";
         Ddo_contratounidades_produtividade_Loadingdata = "Carregando dados...";
         Ddo_contratounidades_produtividade_Sortdsc = "Ordenar de Z � A";
         Ddo_contratounidades_produtividade_Sortasc = "Ordenar de A � Z";
         Ddo_contratounidades_produtividade_Datalistupdateminimumcharacters = 0;
         Ddo_contratounidades_produtividade_Datalistfixedvalues = "";
         Ddo_contratounidades_produtividade_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratounidades_produtividade_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratounidades_produtividade_Filtertype = "Numeric";
         Ddo_contratounidades_produtividade_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratounidades_produtividade_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratounidades_produtividade_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratounidades_produtividade_Titlecontrolidtoreplace = "";
         Ddo_contratounidades_produtividade_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratounidades_produtividade_Cls = "ColumnSettings";
         Ddo_contratounidades_produtividade_Tooltip = "Op��es";
         Ddo_contratounidades_produtividade_Caption = "";
         Ddo_contratounidades_undmedsigla_Searchbuttontext = "Pesquisar";
         Ddo_contratounidades_undmedsigla_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratounidades_undmedsigla_Rangefilterto = "At�";
         Ddo_contratounidades_undmedsigla_Rangefilterfrom = "Desde";
         Ddo_contratounidades_undmedsigla_Cleanfilter = "Limpar pesquisa";
         Ddo_contratounidades_undmedsigla_Loadingdata = "Carregando dados...";
         Ddo_contratounidades_undmedsigla_Sortdsc = "Ordenar de Z � A";
         Ddo_contratounidades_undmedsigla_Sortasc = "Ordenar de A � Z";
         Ddo_contratounidades_undmedsigla_Datalistupdateminimumcharacters = 0;
         Ddo_contratounidades_undmedsigla_Datalistproc = "GetPromptContratoUnidadesFilterData";
         Ddo_contratounidades_undmedsigla_Datalistfixedvalues = "";
         Ddo_contratounidades_undmedsigla_Datalisttype = "Dynamic";
         Ddo_contratounidades_undmedsigla_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratounidades_undmedsigla_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratounidades_undmedsigla_Filtertype = "Character";
         Ddo_contratounidades_undmedsigla_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratounidades_undmedsigla_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratounidades_undmedsigla_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratounidades_undmedsigla_Titlecontrolidtoreplace = "";
         Ddo_contratounidades_undmedsigla_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratounidades_undmedsigla_Cls = "ColumnSettings";
         Ddo_contratounidades_undmedsigla_Tooltip = "Op��es";
         Ddo_contratounidades_undmedsigla_Caption = "";
         Ddo_contratounidades_undmednom_Searchbuttontext = "Pesquisar";
         Ddo_contratounidades_undmednom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratounidades_undmednom_Rangefilterto = "At�";
         Ddo_contratounidades_undmednom_Rangefilterfrom = "Desde";
         Ddo_contratounidades_undmednom_Cleanfilter = "Limpar pesquisa";
         Ddo_contratounidades_undmednom_Loadingdata = "Carregando dados...";
         Ddo_contratounidades_undmednom_Sortdsc = "Ordenar de Z � A";
         Ddo_contratounidades_undmednom_Sortasc = "Ordenar de A � Z";
         Ddo_contratounidades_undmednom_Datalistupdateminimumcharacters = 0;
         Ddo_contratounidades_undmednom_Datalistproc = "GetPromptContratoUnidadesFilterData";
         Ddo_contratounidades_undmednom_Datalistfixedvalues = "";
         Ddo_contratounidades_undmednom_Datalisttype = "Dynamic";
         Ddo_contratounidades_undmednom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratounidades_undmednom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratounidades_undmednom_Filtertype = "Character";
         Ddo_contratounidades_undmednom_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratounidades_undmednom_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratounidades_undmednom_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratounidades_undmednom_Titlecontrolidtoreplace = "";
         Ddo_contratounidades_undmednom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratounidades_undmednom_Cls = "ColumnSettings";
         Ddo_contratounidades_undmednom_Tooltip = "Op��es";
         Ddo_contratounidades_undmednom_Caption = "";
         Ddo_contratounidades_undmedcod_Searchbuttontext = "Pesquisar";
         Ddo_contratounidades_undmedcod_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratounidades_undmedcod_Rangefilterto = "At�";
         Ddo_contratounidades_undmedcod_Rangefilterfrom = "Desde";
         Ddo_contratounidades_undmedcod_Cleanfilter = "Limpar pesquisa";
         Ddo_contratounidades_undmedcod_Loadingdata = "Carregando dados...";
         Ddo_contratounidades_undmedcod_Sortdsc = "Ordenar de Z � A";
         Ddo_contratounidades_undmedcod_Sortasc = "Ordenar de A � Z";
         Ddo_contratounidades_undmedcod_Datalistupdateminimumcharacters = 0;
         Ddo_contratounidades_undmedcod_Datalistfixedvalues = "";
         Ddo_contratounidades_undmedcod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratounidades_undmedcod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratounidades_undmedcod_Filtertype = "Numeric";
         Ddo_contratounidades_undmedcod_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratounidades_undmedcod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratounidades_undmedcod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratounidades_undmedcod_Titlecontrolidtoreplace = "";
         Ddo_contratounidades_undmedcod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratounidades_undmedcod_Cls = "ColumnSettings";
         Ddo_contratounidades_undmedcod_Tooltip = "Op��es";
         Ddo_contratounidades_undmedcod_Caption = "";
         Ddo_contratounidades_contratocod_Searchbuttontext = "Pesquisar";
         Ddo_contratounidades_contratocod_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratounidades_contratocod_Rangefilterto = "At�";
         Ddo_contratounidades_contratocod_Rangefilterfrom = "Desde";
         Ddo_contratounidades_contratocod_Cleanfilter = "Limpar pesquisa";
         Ddo_contratounidades_contratocod_Loadingdata = "Carregando dados...";
         Ddo_contratounidades_contratocod_Sortdsc = "Ordenar de Z � A";
         Ddo_contratounidades_contratocod_Sortasc = "Ordenar de A � Z";
         Ddo_contratounidades_contratocod_Datalistupdateminimumcharacters = 0;
         Ddo_contratounidades_contratocod_Datalistfixedvalues = "";
         Ddo_contratounidades_contratocod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratounidades_contratocod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratounidades_contratocod_Filtertype = "Numeric";
         Ddo_contratounidades_contratocod_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratounidades_contratocod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratounidades_contratocod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratounidades_contratocod_Titlecontrolidtoreplace = "";
         Ddo_contratounidades_contratocod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratounidades_contratocod_Cls = "ColumnSettings";
         Ddo_contratounidades_contratocod_Tooltip = "Op��es";
         Ddo_contratounidades_contratocod_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Prompt Contrato Unidades";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV18ddo_ContratoUnidades_ContratoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_CONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV22ddo_ContratoUnidades_UndMedCodTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV26ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV30ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_PRODUTIVIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV9OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV10OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16TFContratoUnidades_ContratoCod',fld:'vTFCONTRATOUNIDADES_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17TFContratoUnidades_ContratoCod_To',fld:'vTFCONTRATOUNIDADES_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV20TFContratoUnidades_UndMedCod',fld:'vTFCONTRATOUNIDADES_UNDMEDCOD',pic:'ZZZZZ9',nv:0},{av:'AV21TFContratoUnidades_UndMedCod_To',fld:'vTFCONTRATOUNIDADES_UNDMEDCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV24TFContratoUnidades_UndMedNom',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM',pic:'@!',nv:''},{av:'AV25TFContratoUnidades_UndMedNom_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM_SEL',pic:'@!',nv:''},{av:'AV28TFContratoUnidades_UndMedSigla',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA',pic:'@!',nv:''},{av:'AV29TFContratoUnidades_UndMedSigla_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA_SEL',pic:'@!',nv:''},{av:'AV32TFContratoUnidades_Produtividade',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV33TFContratoUnidades_Produtividade_To',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV42Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'AV15ContratoUnidades_ContratoCodTitleFilterData',fld:'vCONTRATOUNIDADES_CONTRATOCODTITLEFILTERDATA',pic:'',nv:null},{av:'AV19ContratoUnidades_UndMedCodTitleFilterData',fld:'vCONTRATOUNIDADES_UNDMEDCODTITLEFILTERDATA',pic:'',nv:null},{av:'AV23ContratoUnidades_UndMedNomTitleFilterData',fld:'vCONTRATOUNIDADES_UNDMEDNOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV27ContratoUnidades_UndMedSiglaTitleFilterData',fld:'vCONTRATOUNIDADES_UNDMEDSIGLATITLEFILTERDATA',pic:'',nv:null},{av:'AV31ContratoUnidades_ProdutividadeTitleFilterData',fld:'vCONTRATOUNIDADES_PRODUTIVIDADETITLEFILTERDATA',pic:'',nv:null},{av:'edtContratoUnidades_ContratoCod_Titleformat',ctrl:'CONTRATOUNIDADES_CONTRATOCOD',prop:'Titleformat'},{av:'edtContratoUnidades_ContratoCod_Title',ctrl:'CONTRATOUNIDADES_CONTRATOCOD',prop:'Title'},{av:'edtContratoUnidades_UndMedCod_Titleformat',ctrl:'CONTRATOUNIDADES_UNDMEDCOD',prop:'Titleformat'},{av:'edtContratoUnidades_UndMedCod_Title',ctrl:'CONTRATOUNIDADES_UNDMEDCOD',prop:'Title'},{av:'edtContratoUnidades_UndMedNom_Titleformat',ctrl:'CONTRATOUNIDADES_UNDMEDNOM',prop:'Titleformat'},{av:'edtContratoUnidades_UndMedNom_Title',ctrl:'CONTRATOUNIDADES_UNDMEDNOM',prop:'Title'},{av:'edtContratoUnidades_UndMedSigla_Titleformat',ctrl:'CONTRATOUNIDADES_UNDMEDSIGLA',prop:'Titleformat'},{av:'edtContratoUnidades_UndMedSigla_Title',ctrl:'CONTRATOUNIDADES_UNDMEDSIGLA',prop:'Title'},{av:'edtContratoUnidades_Produtividade_Titleformat',ctrl:'CONTRATOUNIDADES_PRODUTIVIDADE',prop:'Titleformat'},{av:'edtContratoUnidades_Produtividade_Title',ctrl:'CONTRATOUNIDADES_PRODUTIVIDADE',prop:'Title'},{av:'AV37GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV38GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11IM2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV9OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV10OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16TFContratoUnidades_ContratoCod',fld:'vTFCONTRATOUNIDADES_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17TFContratoUnidades_ContratoCod_To',fld:'vTFCONTRATOUNIDADES_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV20TFContratoUnidades_UndMedCod',fld:'vTFCONTRATOUNIDADES_UNDMEDCOD',pic:'ZZZZZ9',nv:0},{av:'AV21TFContratoUnidades_UndMedCod_To',fld:'vTFCONTRATOUNIDADES_UNDMEDCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV24TFContratoUnidades_UndMedNom',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM',pic:'@!',nv:''},{av:'AV25TFContratoUnidades_UndMedNom_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM_SEL',pic:'@!',nv:''},{av:'AV28TFContratoUnidades_UndMedSigla',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA',pic:'@!',nv:''},{av:'AV29TFContratoUnidades_UndMedSigla_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA_SEL',pic:'@!',nv:''},{av:'AV32TFContratoUnidades_Produtividade',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV33TFContratoUnidades_Produtividade_To',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV18ddo_ContratoUnidades_ContratoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_CONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV22ddo_ContratoUnidades_UndMedCodTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV26ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV30ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_PRODUTIVIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTRATOUNIDADES_CONTRATOCOD.ONOPTIONCLICKED","{handler:'E12IM2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV9OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV10OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16TFContratoUnidades_ContratoCod',fld:'vTFCONTRATOUNIDADES_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17TFContratoUnidades_ContratoCod_To',fld:'vTFCONTRATOUNIDADES_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV20TFContratoUnidades_UndMedCod',fld:'vTFCONTRATOUNIDADES_UNDMEDCOD',pic:'ZZZZZ9',nv:0},{av:'AV21TFContratoUnidades_UndMedCod_To',fld:'vTFCONTRATOUNIDADES_UNDMEDCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV24TFContratoUnidades_UndMedNom',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM',pic:'@!',nv:''},{av:'AV25TFContratoUnidades_UndMedNom_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM_SEL',pic:'@!',nv:''},{av:'AV28TFContratoUnidades_UndMedSigla',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA',pic:'@!',nv:''},{av:'AV29TFContratoUnidades_UndMedSigla_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA_SEL',pic:'@!',nv:''},{av:'AV32TFContratoUnidades_Produtividade',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV33TFContratoUnidades_Produtividade_To',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV18ddo_ContratoUnidades_ContratoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_CONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV22ddo_ContratoUnidades_UndMedCodTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV26ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV30ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_PRODUTIVIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'Ddo_contratounidades_contratocod_Activeeventkey',ctrl:'DDO_CONTRATOUNIDADES_CONTRATOCOD',prop:'ActiveEventKey'},{av:'Ddo_contratounidades_contratocod_Filteredtext_get',ctrl:'DDO_CONTRATOUNIDADES_CONTRATOCOD',prop:'FilteredText_get'},{av:'Ddo_contratounidades_contratocod_Filteredtextto_get',ctrl:'DDO_CONTRATOUNIDADES_CONTRATOCOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV9OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV10OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratounidades_contratocod_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_CONTRATOCOD',prop:'SortedStatus'},{av:'AV16TFContratoUnidades_ContratoCod',fld:'vTFCONTRATOUNIDADES_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17TFContratoUnidades_ContratoCod_To',fld:'vTFCONTRATOUNIDADES_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratounidades_undmedcod_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDCOD',prop:'SortedStatus'},{av:'Ddo_contratounidades_undmednom_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDNOM',prop:'SortedStatus'},{av:'Ddo_contratounidades_undmedsigla_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDSIGLA',prop:'SortedStatus'},{av:'Ddo_contratounidades_produtividade_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_PRODUTIVIDADE',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOUNIDADES_UNDMEDCOD.ONOPTIONCLICKED","{handler:'E13IM2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV9OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV10OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16TFContratoUnidades_ContratoCod',fld:'vTFCONTRATOUNIDADES_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17TFContratoUnidades_ContratoCod_To',fld:'vTFCONTRATOUNIDADES_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV20TFContratoUnidades_UndMedCod',fld:'vTFCONTRATOUNIDADES_UNDMEDCOD',pic:'ZZZZZ9',nv:0},{av:'AV21TFContratoUnidades_UndMedCod_To',fld:'vTFCONTRATOUNIDADES_UNDMEDCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV24TFContratoUnidades_UndMedNom',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM',pic:'@!',nv:''},{av:'AV25TFContratoUnidades_UndMedNom_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM_SEL',pic:'@!',nv:''},{av:'AV28TFContratoUnidades_UndMedSigla',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA',pic:'@!',nv:''},{av:'AV29TFContratoUnidades_UndMedSigla_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA_SEL',pic:'@!',nv:''},{av:'AV32TFContratoUnidades_Produtividade',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV33TFContratoUnidades_Produtividade_To',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV18ddo_ContratoUnidades_ContratoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_CONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV22ddo_ContratoUnidades_UndMedCodTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV26ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV30ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_PRODUTIVIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'Ddo_contratounidades_undmedcod_Activeeventkey',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDCOD',prop:'ActiveEventKey'},{av:'Ddo_contratounidades_undmedcod_Filteredtext_get',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDCOD',prop:'FilteredText_get'},{av:'Ddo_contratounidades_undmedcod_Filteredtextto_get',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDCOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV9OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV10OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratounidades_undmedcod_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDCOD',prop:'SortedStatus'},{av:'AV20TFContratoUnidades_UndMedCod',fld:'vTFCONTRATOUNIDADES_UNDMEDCOD',pic:'ZZZZZ9',nv:0},{av:'AV21TFContratoUnidades_UndMedCod_To',fld:'vTFCONTRATOUNIDADES_UNDMEDCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratounidades_contratocod_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_CONTRATOCOD',prop:'SortedStatus'},{av:'Ddo_contratounidades_undmednom_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDNOM',prop:'SortedStatus'},{av:'Ddo_contratounidades_undmedsigla_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDSIGLA',prop:'SortedStatus'},{av:'Ddo_contratounidades_produtividade_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_PRODUTIVIDADE',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOUNIDADES_UNDMEDNOM.ONOPTIONCLICKED","{handler:'E14IM2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV9OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV10OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16TFContratoUnidades_ContratoCod',fld:'vTFCONTRATOUNIDADES_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17TFContratoUnidades_ContratoCod_To',fld:'vTFCONTRATOUNIDADES_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV20TFContratoUnidades_UndMedCod',fld:'vTFCONTRATOUNIDADES_UNDMEDCOD',pic:'ZZZZZ9',nv:0},{av:'AV21TFContratoUnidades_UndMedCod_To',fld:'vTFCONTRATOUNIDADES_UNDMEDCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV24TFContratoUnidades_UndMedNom',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM',pic:'@!',nv:''},{av:'AV25TFContratoUnidades_UndMedNom_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM_SEL',pic:'@!',nv:''},{av:'AV28TFContratoUnidades_UndMedSigla',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA',pic:'@!',nv:''},{av:'AV29TFContratoUnidades_UndMedSigla_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA_SEL',pic:'@!',nv:''},{av:'AV32TFContratoUnidades_Produtividade',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV33TFContratoUnidades_Produtividade_To',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV18ddo_ContratoUnidades_ContratoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_CONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV22ddo_ContratoUnidades_UndMedCodTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV26ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV30ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_PRODUTIVIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'Ddo_contratounidades_undmednom_Activeeventkey',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDNOM',prop:'ActiveEventKey'},{av:'Ddo_contratounidades_undmednom_Filteredtext_get',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDNOM',prop:'FilteredText_get'},{av:'Ddo_contratounidades_undmednom_Selectedvalue_get',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDNOM',prop:'SelectedValue_get'}],oparms:[{av:'AV9OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV10OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratounidades_undmednom_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDNOM',prop:'SortedStatus'},{av:'AV24TFContratoUnidades_UndMedNom',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM',pic:'@!',nv:''},{av:'AV25TFContratoUnidades_UndMedNom_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM_SEL',pic:'@!',nv:''},{av:'Ddo_contratounidades_contratocod_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_CONTRATOCOD',prop:'SortedStatus'},{av:'Ddo_contratounidades_undmedcod_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDCOD',prop:'SortedStatus'},{av:'Ddo_contratounidades_undmedsigla_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDSIGLA',prop:'SortedStatus'},{av:'Ddo_contratounidades_produtividade_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_PRODUTIVIDADE',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOUNIDADES_UNDMEDSIGLA.ONOPTIONCLICKED","{handler:'E15IM2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV9OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV10OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16TFContratoUnidades_ContratoCod',fld:'vTFCONTRATOUNIDADES_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17TFContratoUnidades_ContratoCod_To',fld:'vTFCONTRATOUNIDADES_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV20TFContratoUnidades_UndMedCod',fld:'vTFCONTRATOUNIDADES_UNDMEDCOD',pic:'ZZZZZ9',nv:0},{av:'AV21TFContratoUnidades_UndMedCod_To',fld:'vTFCONTRATOUNIDADES_UNDMEDCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV24TFContratoUnidades_UndMedNom',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM',pic:'@!',nv:''},{av:'AV25TFContratoUnidades_UndMedNom_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM_SEL',pic:'@!',nv:''},{av:'AV28TFContratoUnidades_UndMedSigla',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA',pic:'@!',nv:''},{av:'AV29TFContratoUnidades_UndMedSigla_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA_SEL',pic:'@!',nv:''},{av:'AV32TFContratoUnidades_Produtividade',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV33TFContratoUnidades_Produtividade_To',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV18ddo_ContratoUnidades_ContratoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_CONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV22ddo_ContratoUnidades_UndMedCodTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV26ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV30ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_PRODUTIVIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'Ddo_contratounidades_undmedsigla_Activeeventkey',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDSIGLA',prop:'ActiveEventKey'},{av:'Ddo_contratounidades_undmedsigla_Filteredtext_get',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDSIGLA',prop:'FilteredText_get'},{av:'Ddo_contratounidades_undmedsigla_Selectedvalue_get',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDSIGLA',prop:'SelectedValue_get'}],oparms:[{av:'AV9OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV10OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratounidades_undmedsigla_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDSIGLA',prop:'SortedStatus'},{av:'AV28TFContratoUnidades_UndMedSigla',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA',pic:'@!',nv:''},{av:'AV29TFContratoUnidades_UndMedSigla_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_contratounidades_contratocod_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_CONTRATOCOD',prop:'SortedStatus'},{av:'Ddo_contratounidades_undmedcod_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDCOD',prop:'SortedStatus'},{av:'Ddo_contratounidades_undmednom_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDNOM',prop:'SortedStatus'},{av:'Ddo_contratounidades_produtividade_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_PRODUTIVIDADE',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOUNIDADES_PRODUTIVIDADE.ONOPTIONCLICKED","{handler:'E16IM2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV9OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV10OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16TFContratoUnidades_ContratoCod',fld:'vTFCONTRATOUNIDADES_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17TFContratoUnidades_ContratoCod_To',fld:'vTFCONTRATOUNIDADES_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV20TFContratoUnidades_UndMedCod',fld:'vTFCONTRATOUNIDADES_UNDMEDCOD',pic:'ZZZZZ9',nv:0},{av:'AV21TFContratoUnidades_UndMedCod_To',fld:'vTFCONTRATOUNIDADES_UNDMEDCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV24TFContratoUnidades_UndMedNom',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM',pic:'@!',nv:''},{av:'AV25TFContratoUnidades_UndMedNom_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM_SEL',pic:'@!',nv:''},{av:'AV28TFContratoUnidades_UndMedSigla',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA',pic:'@!',nv:''},{av:'AV29TFContratoUnidades_UndMedSigla_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA_SEL',pic:'@!',nv:''},{av:'AV32TFContratoUnidades_Produtividade',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV33TFContratoUnidades_Produtividade_To',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV18ddo_ContratoUnidades_ContratoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_CONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV22ddo_ContratoUnidades_UndMedCodTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV26ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV30ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_PRODUTIVIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'Ddo_contratounidades_produtividade_Activeeventkey',ctrl:'DDO_CONTRATOUNIDADES_PRODUTIVIDADE',prop:'ActiveEventKey'},{av:'Ddo_contratounidades_produtividade_Filteredtext_get',ctrl:'DDO_CONTRATOUNIDADES_PRODUTIVIDADE',prop:'FilteredText_get'},{av:'Ddo_contratounidades_produtividade_Filteredtextto_get',ctrl:'DDO_CONTRATOUNIDADES_PRODUTIVIDADE',prop:'FilteredTextTo_get'}],oparms:[{av:'AV9OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV10OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratounidades_produtividade_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_PRODUTIVIDADE',prop:'SortedStatus'},{av:'AV32TFContratoUnidades_Produtividade',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV33TFContratoUnidades_Produtividade_To',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_contratounidades_contratocod_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_CONTRATOCOD',prop:'SortedStatus'},{av:'Ddo_contratounidades_undmedcod_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDCOD',prop:'SortedStatus'},{av:'Ddo_contratounidades_undmednom_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDNOM',prop:'SortedStatus'},{av:'Ddo_contratounidades_undmedsigla_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDSIGLA',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E20IM2',iparms:[],oparms:[{av:'AV11Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E21IM2',iparms:[{av:'A1207ContratoUnidades_ContratoCod',fld:'CONTRATOUNIDADES_CONTRATOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1204ContratoUnidades_UndMedCod',fld:'CONTRATOUNIDADES_UNDMEDCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1205ContratoUnidades_UndMedNom',fld:'CONTRATOUNIDADES_UNDMEDNOM',pic:'@!',nv:''}],oparms:[{av:'AV7InOutContratoUnidades_ContratoCod',fld:'vINOUTCONTRATOUNIDADES_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV12InOutContratoUnidades_UndMedCod',fld:'vINOUTCONTRATOUNIDADES_UNDMEDCOD',pic:'ZZZZZ9',nv:0},{av:'AV8InOutContratoUnidades_UndMedNom',fld:'vINOUTCONTRATOUNIDADES_UNDMEDNOM',pic:'@!',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E17IM2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV9OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV10OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16TFContratoUnidades_ContratoCod',fld:'vTFCONTRATOUNIDADES_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17TFContratoUnidades_ContratoCod_To',fld:'vTFCONTRATOUNIDADES_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV20TFContratoUnidades_UndMedCod',fld:'vTFCONTRATOUNIDADES_UNDMEDCOD',pic:'ZZZZZ9',nv:0},{av:'AV21TFContratoUnidades_UndMedCod_To',fld:'vTFCONTRATOUNIDADES_UNDMEDCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV24TFContratoUnidades_UndMedNom',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM',pic:'@!',nv:''},{av:'AV25TFContratoUnidades_UndMedNom_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM_SEL',pic:'@!',nv:''},{av:'AV28TFContratoUnidades_UndMedSigla',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA',pic:'@!',nv:''},{av:'AV29TFContratoUnidades_UndMedSigla_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA_SEL',pic:'@!',nv:''},{av:'AV32TFContratoUnidades_Produtividade',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV33TFContratoUnidades_Produtividade_To',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV18ddo_ContratoUnidades_ContratoCodTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_CONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV22ddo_ContratoUnidades_UndMedCodTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV26ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV30ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_PRODUTIVIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV8InOutContratoUnidades_UndMedNom = "";
         Gridpaginationbar_Selectedpage = "";
         Ddo_contratounidades_contratocod_Activeeventkey = "";
         Ddo_contratounidades_contratocod_Filteredtext_get = "";
         Ddo_contratounidades_contratocod_Filteredtextto_get = "";
         Ddo_contratounidades_undmedcod_Activeeventkey = "";
         Ddo_contratounidades_undmedcod_Filteredtext_get = "";
         Ddo_contratounidades_undmedcod_Filteredtextto_get = "";
         Ddo_contratounidades_undmednom_Activeeventkey = "";
         Ddo_contratounidades_undmednom_Filteredtext_get = "";
         Ddo_contratounidades_undmednom_Selectedvalue_get = "";
         Ddo_contratounidades_undmedsigla_Activeeventkey = "";
         Ddo_contratounidades_undmedsigla_Filteredtext_get = "";
         Ddo_contratounidades_undmedsigla_Selectedvalue_get = "";
         Ddo_contratounidades_produtividade_Activeeventkey = "";
         Ddo_contratounidades_produtividade_Filteredtext_get = "";
         Ddo_contratounidades_produtividade_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV24TFContratoUnidades_UndMedNom = "";
         AV25TFContratoUnidades_UndMedNom_Sel = "";
         AV28TFContratoUnidades_UndMedSigla = "";
         AV29TFContratoUnidades_UndMedSigla_Sel = "";
         AV18ddo_ContratoUnidades_ContratoCodTitleControlIdToReplace = "";
         AV22ddo_ContratoUnidades_UndMedCodTitleControlIdToReplace = "";
         AV26ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace = "";
         AV30ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace = "";
         AV34ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace = "";
         AV42Pgmname = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV35DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV15ContratoUnidades_ContratoCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV19ContratoUnidades_UndMedCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV23ContratoUnidades_UndMedNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV27ContratoUnidades_UndMedSiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV31ContratoUnidades_ProdutividadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_contratounidades_contratocod_Sortedstatus = "";
         Ddo_contratounidades_undmedcod_Sortedstatus = "";
         Ddo_contratounidades_undmednom_Sortedstatus = "";
         Ddo_contratounidades_undmedsigla_Sortedstatus = "";
         Ddo_contratounidades_produtividade_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV11Select = "";
         AV41Select_GXI = "";
         A1205ContratoUnidades_UndMedNom = "";
         A1206ContratoUnidades_UndMedSigla = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV24TFContratoUnidades_UndMedNom = "";
         lV28TFContratoUnidades_UndMedSigla = "";
         H00IM2_A1208ContratoUnidades_Produtividade = new decimal[1] ;
         H00IM2_n1208ContratoUnidades_Produtividade = new bool[] {false} ;
         H00IM2_A1206ContratoUnidades_UndMedSigla = new String[] {""} ;
         H00IM2_n1206ContratoUnidades_UndMedSigla = new bool[] {false} ;
         H00IM2_A1205ContratoUnidades_UndMedNom = new String[] {""} ;
         H00IM2_n1205ContratoUnidades_UndMedNom = new bool[] {false} ;
         H00IM2_A1204ContratoUnidades_UndMedCod = new int[1] ;
         H00IM2_A1207ContratoUnidades_ContratoCod = new int[1] ;
         H00IM3_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV13GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV14GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptcontratounidades__default(),
            new Object[][] {
                new Object[] {
               H00IM2_A1208ContratoUnidades_Produtividade, H00IM2_n1208ContratoUnidades_Produtividade, H00IM2_A1206ContratoUnidades_UndMedSigla, H00IM2_n1206ContratoUnidades_UndMedSigla, H00IM2_A1205ContratoUnidades_UndMedNom, H00IM2_n1205ContratoUnidades_UndMedNom, H00IM2_A1204ContratoUnidades_UndMedCod, H00IM2_A1207ContratoUnidades_ContratoCod
               }
               , new Object[] {
               H00IM3_AGRID_nRecordCount
               }
            }
         );
         AV42Pgmname = "PromptContratoUnidades";
         /* GeneXus formulas. */
         AV42Pgmname = "PromptContratoUnidades";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_26 ;
      private short nGXsfl_26_idx=1 ;
      private short AV9OrderedBy ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_26_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContratoUnidades_ContratoCod_Titleformat ;
      private short edtContratoUnidades_UndMedCod_Titleformat ;
      private short edtContratoUnidades_UndMedNom_Titleformat ;
      private short edtContratoUnidades_UndMedSigla_Titleformat ;
      private short edtContratoUnidades_Produtividade_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutContratoUnidades_ContratoCod ;
      private int AV12InOutContratoUnidades_UndMedCod ;
      private int wcpOAV7InOutContratoUnidades_ContratoCod ;
      private int wcpOAV12InOutContratoUnidades_UndMedCod ;
      private int subGrid_Rows ;
      private int AV16TFContratoUnidades_ContratoCod ;
      private int AV17TFContratoUnidades_ContratoCod_To ;
      private int AV20TFContratoUnidades_UndMedCod ;
      private int AV21TFContratoUnidades_UndMedCod_To ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_contratounidades_contratocod_Datalistupdateminimumcharacters ;
      private int Ddo_contratounidades_undmedcod_Datalistupdateminimumcharacters ;
      private int Ddo_contratounidades_undmednom_Datalistupdateminimumcharacters ;
      private int Ddo_contratounidades_undmedsigla_Datalistupdateminimumcharacters ;
      private int Ddo_contratounidades_produtividade_Datalistupdateminimumcharacters ;
      private int edtavTfcontratounidades_contratocod_Visible ;
      private int edtavTfcontratounidades_contratocod_to_Visible ;
      private int edtavTfcontratounidades_undmedcod_Visible ;
      private int edtavTfcontratounidades_undmedcod_to_Visible ;
      private int edtavTfcontratounidades_undmednom_Visible ;
      private int edtavTfcontratounidades_undmednom_sel_Visible ;
      private int edtavTfcontratounidades_undmedsigla_Visible ;
      private int edtavTfcontratounidades_undmedsigla_sel_Visible ;
      private int edtavTfcontratounidades_produtividade_Visible ;
      private int edtavTfcontratounidades_produtividade_to_Visible ;
      private int edtavDdo_contratounidades_contratocodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratounidades_undmedcodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratounidades_undmednomtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratounidades_undmedsiglatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratounidades_produtividadetitlecontrolidtoreplace_Visible ;
      private int A1207ContratoUnidades_ContratoCod ;
      private int A1204ContratoUnidades_UndMedCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV36PageToGo ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV37GridCurrentPage ;
      private long AV38GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV32TFContratoUnidades_Produtividade ;
      private decimal AV33TFContratoUnidades_Produtividade_To ;
      private decimal A1208ContratoUnidades_Produtividade ;
      private String AV8InOutContratoUnidades_UndMedNom ;
      private String wcpOAV8InOutContratoUnidades_UndMedNom ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contratounidades_contratocod_Activeeventkey ;
      private String Ddo_contratounidades_contratocod_Filteredtext_get ;
      private String Ddo_contratounidades_contratocod_Filteredtextto_get ;
      private String Ddo_contratounidades_undmedcod_Activeeventkey ;
      private String Ddo_contratounidades_undmedcod_Filteredtext_get ;
      private String Ddo_contratounidades_undmedcod_Filteredtextto_get ;
      private String Ddo_contratounidades_undmednom_Activeeventkey ;
      private String Ddo_contratounidades_undmednom_Filteredtext_get ;
      private String Ddo_contratounidades_undmednom_Selectedvalue_get ;
      private String Ddo_contratounidades_undmedsigla_Activeeventkey ;
      private String Ddo_contratounidades_undmedsigla_Filteredtext_get ;
      private String Ddo_contratounidades_undmedsigla_Selectedvalue_get ;
      private String Ddo_contratounidades_produtividade_Activeeventkey ;
      private String Ddo_contratounidades_produtividade_Filteredtext_get ;
      private String Ddo_contratounidades_produtividade_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_26_idx="0001" ;
      private String AV24TFContratoUnidades_UndMedNom ;
      private String AV25TFContratoUnidades_UndMedNom_Sel ;
      private String AV28TFContratoUnidades_UndMedSigla ;
      private String AV29TFContratoUnidades_UndMedSigla_Sel ;
      private String AV42Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contratounidades_contratocod_Caption ;
      private String Ddo_contratounidades_contratocod_Tooltip ;
      private String Ddo_contratounidades_contratocod_Cls ;
      private String Ddo_contratounidades_contratocod_Dropdownoptionstype ;
      private String Ddo_contratounidades_contratocod_Titlecontrolidtoreplace ;
      private String Ddo_contratounidades_contratocod_Sortedstatus ;
      private String Ddo_contratounidades_contratocod_Filtertype ;
      private String Ddo_contratounidades_contratocod_Datalistfixedvalues ;
      private String Ddo_contratounidades_contratocod_Sortasc ;
      private String Ddo_contratounidades_contratocod_Sortdsc ;
      private String Ddo_contratounidades_contratocod_Loadingdata ;
      private String Ddo_contratounidades_contratocod_Cleanfilter ;
      private String Ddo_contratounidades_contratocod_Rangefilterfrom ;
      private String Ddo_contratounidades_contratocod_Rangefilterto ;
      private String Ddo_contratounidades_contratocod_Noresultsfound ;
      private String Ddo_contratounidades_contratocod_Searchbuttontext ;
      private String Ddo_contratounidades_undmedcod_Caption ;
      private String Ddo_contratounidades_undmedcod_Tooltip ;
      private String Ddo_contratounidades_undmedcod_Cls ;
      private String Ddo_contratounidades_undmedcod_Dropdownoptionstype ;
      private String Ddo_contratounidades_undmedcod_Titlecontrolidtoreplace ;
      private String Ddo_contratounidades_undmedcod_Sortedstatus ;
      private String Ddo_contratounidades_undmedcod_Filtertype ;
      private String Ddo_contratounidades_undmedcod_Datalistfixedvalues ;
      private String Ddo_contratounidades_undmedcod_Sortasc ;
      private String Ddo_contratounidades_undmedcod_Sortdsc ;
      private String Ddo_contratounidades_undmedcod_Loadingdata ;
      private String Ddo_contratounidades_undmedcod_Cleanfilter ;
      private String Ddo_contratounidades_undmedcod_Rangefilterfrom ;
      private String Ddo_contratounidades_undmedcod_Rangefilterto ;
      private String Ddo_contratounidades_undmedcod_Noresultsfound ;
      private String Ddo_contratounidades_undmedcod_Searchbuttontext ;
      private String Ddo_contratounidades_undmednom_Caption ;
      private String Ddo_contratounidades_undmednom_Tooltip ;
      private String Ddo_contratounidades_undmednom_Cls ;
      private String Ddo_contratounidades_undmednom_Dropdownoptionstype ;
      private String Ddo_contratounidades_undmednom_Titlecontrolidtoreplace ;
      private String Ddo_contratounidades_undmednom_Sortedstatus ;
      private String Ddo_contratounidades_undmednom_Filtertype ;
      private String Ddo_contratounidades_undmednom_Datalisttype ;
      private String Ddo_contratounidades_undmednom_Datalistfixedvalues ;
      private String Ddo_contratounidades_undmednom_Datalistproc ;
      private String Ddo_contratounidades_undmednom_Sortasc ;
      private String Ddo_contratounidades_undmednom_Sortdsc ;
      private String Ddo_contratounidades_undmednom_Loadingdata ;
      private String Ddo_contratounidades_undmednom_Cleanfilter ;
      private String Ddo_contratounidades_undmednom_Rangefilterfrom ;
      private String Ddo_contratounidades_undmednom_Rangefilterto ;
      private String Ddo_contratounidades_undmednom_Noresultsfound ;
      private String Ddo_contratounidades_undmednom_Searchbuttontext ;
      private String Ddo_contratounidades_undmedsigla_Caption ;
      private String Ddo_contratounidades_undmedsigla_Tooltip ;
      private String Ddo_contratounidades_undmedsigla_Cls ;
      private String Ddo_contratounidades_undmedsigla_Dropdownoptionstype ;
      private String Ddo_contratounidades_undmedsigla_Titlecontrolidtoreplace ;
      private String Ddo_contratounidades_undmedsigla_Sortedstatus ;
      private String Ddo_contratounidades_undmedsigla_Filtertype ;
      private String Ddo_contratounidades_undmedsigla_Datalisttype ;
      private String Ddo_contratounidades_undmedsigla_Datalistfixedvalues ;
      private String Ddo_contratounidades_undmedsigla_Datalistproc ;
      private String Ddo_contratounidades_undmedsigla_Sortasc ;
      private String Ddo_contratounidades_undmedsigla_Sortdsc ;
      private String Ddo_contratounidades_undmedsigla_Loadingdata ;
      private String Ddo_contratounidades_undmedsigla_Cleanfilter ;
      private String Ddo_contratounidades_undmedsigla_Rangefilterfrom ;
      private String Ddo_contratounidades_undmedsigla_Rangefilterto ;
      private String Ddo_contratounidades_undmedsigla_Noresultsfound ;
      private String Ddo_contratounidades_undmedsigla_Searchbuttontext ;
      private String Ddo_contratounidades_produtividade_Caption ;
      private String Ddo_contratounidades_produtividade_Tooltip ;
      private String Ddo_contratounidades_produtividade_Cls ;
      private String Ddo_contratounidades_produtividade_Dropdownoptionstype ;
      private String Ddo_contratounidades_produtividade_Titlecontrolidtoreplace ;
      private String Ddo_contratounidades_produtividade_Sortedstatus ;
      private String Ddo_contratounidades_produtividade_Filtertype ;
      private String Ddo_contratounidades_produtividade_Datalistfixedvalues ;
      private String Ddo_contratounidades_produtividade_Sortasc ;
      private String Ddo_contratounidades_produtividade_Sortdsc ;
      private String Ddo_contratounidades_produtividade_Loadingdata ;
      private String Ddo_contratounidades_produtividade_Cleanfilter ;
      private String Ddo_contratounidades_produtividade_Rangefilterfrom ;
      private String Ddo_contratounidades_produtividade_Rangefilterto ;
      private String Ddo_contratounidades_produtividade_Noresultsfound ;
      private String Ddo_contratounidades_produtividade_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String edtavTfcontratounidades_contratocod_Internalname ;
      private String edtavTfcontratounidades_contratocod_Jsonclick ;
      private String edtavTfcontratounidades_contratocod_to_Internalname ;
      private String edtavTfcontratounidades_contratocod_to_Jsonclick ;
      private String edtavTfcontratounidades_undmedcod_Internalname ;
      private String edtavTfcontratounidades_undmedcod_Jsonclick ;
      private String edtavTfcontratounidades_undmedcod_to_Internalname ;
      private String edtavTfcontratounidades_undmedcod_to_Jsonclick ;
      private String edtavTfcontratounidades_undmednom_Internalname ;
      private String edtavTfcontratounidades_undmednom_Jsonclick ;
      private String edtavTfcontratounidades_undmednom_sel_Internalname ;
      private String edtavTfcontratounidades_undmednom_sel_Jsonclick ;
      private String edtavTfcontratounidades_undmedsigla_Internalname ;
      private String edtavTfcontratounidades_undmedsigla_Jsonclick ;
      private String edtavTfcontratounidades_undmedsigla_sel_Internalname ;
      private String edtavTfcontratounidades_undmedsigla_sel_Jsonclick ;
      private String edtavTfcontratounidades_produtividade_Internalname ;
      private String edtavTfcontratounidades_produtividade_Jsonclick ;
      private String edtavTfcontratounidades_produtividade_to_Internalname ;
      private String edtavTfcontratounidades_produtividade_to_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_contratounidades_contratocodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratounidades_undmedcodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratounidades_undmednomtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratounidades_undmedsiglatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratounidades_produtividadetitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtContratoUnidades_ContratoCod_Internalname ;
      private String edtContratoUnidades_UndMedCod_Internalname ;
      private String A1205ContratoUnidades_UndMedNom ;
      private String edtContratoUnidades_UndMedNom_Internalname ;
      private String A1206ContratoUnidades_UndMedSigla ;
      private String edtContratoUnidades_UndMedSigla_Internalname ;
      private String edtContratoUnidades_Produtividade_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV24TFContratoUnidades_UndMedNom ;
      private String lV28TFContratoUnidades_UndMedSigla ;
      private String edtavOrdereddsc_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_contratounidades_contratocod_Internalname ;
      private String Ddo_contratounidades_undmedcod_Internalname ;
      private String Ddo_contratounidades_undmednom_Internalname ;
      private String Ddo_contratounidades_undmedsigla_Internalname ;
      private String Ddo_contratounidades_produtividade_Internalname ;
      private String edtContratoUnidades_ContratoCod_Title ;
      private String edtContratoUnidades_UndMedCod_Title ;
      private String edtContratoUnidades_UndMedNom_Title ;
      private String edtContratoUnidades_UndMedSigla_Title ;
      private String edtContratoUnidades_Produtividade_Title ;
      private String edtavSelect_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String sGXsfl_26_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtContratoUnidades_ContratoCod_Jsonclick ;
      private String edtContratoUnidades_UndMedCod_Jsonclick ;
      private String edtContratoUnidades_UndMedNom_Jsonclick ;
      private String edtContratoUnidades_UndMedSigla_Jsonclick ;
      private String edtContratoUnidades_Produtividade_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV10OrderedDsc ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contratounidades_contratocod_Includesortasc ;
      private bool Ddo_contratounidades_contratocod_Includesortdsc ;
      private bool Ddo_contratounidades_contratocod_Includefilter ;
      private bool Ddo_contratounidades_contratocod_Filterisrange ;
      private bool Ddo_contratounidades_contratocod_Includedatalist ;
      private bool Ddo_contratounidades_undmedcod_Includesortasc ;
      private bool Ddo_contratounidades_undmedcod_Includesortdsc ;
      private bool Ddo_contratounidades_undmedcod_Includefilter ;
      private bool Ddo_contratounidades_undmedcod_Filterisrange ;
      private bool Ddo_contratounidades_undmedcod_Includedatalist ;
      private bool Ddo_contratounidades_undmednom_Includesortasc ;
      private bool Ddo_contratounidades_undmednom_Includesortdsc ;
      private bool Ddo_contratounidades_undmednom_Includefilter ;
      private bool Ddo_contratounidades_undmednom_Filterisrange ;
      private bool Ddo_contratounidades_undmednom_Includedatalist ;
      private bool Ddo_contratounidades_undmedsigla_Includesortasc ;
      private bool Ddo_contratounidades_undmedsigla_Includesortdsc ;
      private bool Ddo_contratounidades_undmedsigla_Includefilter ;
      private bool Ddo_contratounidades_undmedsigla_Filterisrange ;
      private bool Ddo_contratounidades_undmedsigla_Includedatalist ;
      private bool Ddo_contratounidades_produtividade_Includesortasc ;
      private bool Ddo_contratounidades_produtividade_Includesortdsc ;
      private bool Ddo_contratounidades_produtividade_Includefilter ;
      private bool Ddo_contratounidades_produtividade_Filterisrange ;
      private bool Ddo_contratounidades_produtividade_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1205ContratoUnidades_UndMedNom ;
      private bool n1206ContratoUnidades_UndMedSigla ;
      private bool n1208ContratoUnidades_Produtividade ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV11Select_IsBlob ;
      private String AV18ddo_ContratoUnidades_ContratoCodTitleControlIdToReplace ;
      private String AV22ddo_ContratoUnidades_UndMedCodTitleControlIdToReplace ;
      private String AV26ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace ;
      private String AV30ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace ;
      private String AV34ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace ;
      private String AV41Select_GXI ;
      private String AV11Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutContratoUnidades_ContratoCod ;
      private int aP1_InOutContratoUnidades_UndMedCod ;
      private String aP2_InOutContratoUnidades_UndMedNom ;
      private GXCombobox cmbavOrderedby ;
      private IDataStoreProvider pr_default ;
      private decimal[] H00IM2_A1208ContratoUnidades_Produtividade ;
      private bool[] H00IM2_n1208ContratoUnidades_Produtividade ;
      private String[] H00IM2_A1206ContratoUnidades_UndMedSigla ;
      private bool[] H00IM2_n1206ContratoUnidades_UndMedSigla ;
      private String[] H00IM2_A1205ContratoUnidades_UndMedNom ;
      private bool[] H00IM2_n1205ContratoUnidades_UndMedNom ;
      private int[] H00IM2_A1204ContratoUnidades_UndMedCod ;
      private int[] H00IM2_A1207ContratoUnidades_ContratoCod ;
      private long[] H00IM3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV15ContratoUnidades_ContratoCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV19ContratoUnidades_UndMedCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV23ContratoUnidades_UndMedNomTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV27ContratoUnidades_UndMedSiglaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV31ContratoUnidades_ProdutividadeTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV13GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV14GridStateFilterValue ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV35DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class promptcontratounidades__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00IM2( IGxContext context ,
                                             int AV16TFContratoUnidades_ContratoCod ,
                                             int AV17TFContratoUnidades_ContratoCod_To ,
                                             int AV20TFContratoUnidades_UndMedCod ,
                                             int AV21TFContratoUnidades_UndMedCod_To ,
                                             String AV25TFContratoUnidades_UndMedNom_Sel ,
                                             String AV24TFContratoUnidades_UndMedNom ,
                                             String AV29TFContratoUnidades_UndMedSigla_Sel ,
                                             String AV28TFContratoUnidades_UndMedSigla ,
                                             decimal AV32TFContratoUnidades_Produtividade ,
                                             decimal AV33TFContratoUnidades_Produtividade_To ,
                                             int A1207ContratoUnidades_ContratoCod ,
                                             int A1204ContratoUnidades_UndMedCod ,
                                             String A1205ContratoUnidades_UndMedNom ,
                                             String A1206ContratoUnidades_UndMedSigla ,
                                             decimal A1208ContratoUnidades_Produtividade ,
                                             short AV9OrderedBy ,
                                             bool AV10OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [15] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[ContratoUnidades_Produtividade], T2.[UnidadeMedicao_Sigla] AS ContratoUnidades_UndMedSigla, T2.[UnidadeMedicao_Nome] AS ContratoUnidades_UndMedNom, T1.[ContratoUnidades_UndMedCod] AS ContratoUnidades_UndMedCod, T1.[ContratoUnidades_ContratoCod]";
         sFromString = " FROM ([ContratoUnidades] T1 WITH (NOLOCK) INNER JOIN [UnidadeMedicao] T2 WITH (NOLOCK) ON T2.[UnidadeMedicao_Codigo] = T1.[ContratoUnidades_UndMedCod])";
         sOrderString = "";
         if ( ! (0==AV16TFContratoUnidades_ContratoCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoUnidades_ContratoCod] >= @AV16TFContratoUnidades_ContratoCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoUnidades_ContratoCod] >= @AV16TFContratoUnidades_ContratoCod)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ! (0==AV17TFContratoUnidades_ContratoCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoUnidades_ContratoCod] <= @AV17TFContratoUnidades_ContratoCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoUnidades_ContratoCod] <= @AV17TFContratoUnidades_ContratoCod_To)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ! (0==AV20TFContratoUnidades_UndMedCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoUnidades_UndMedCod] >= @AV20TFContratoUnidades_UndMedCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoUnidades_UndMedCod] >= @AV20TFContratoUnidades_UndMedCod)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! (0==AV21TFContratoUnidades_UndMedCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoUnidades_UndMedCod] <= @AV21TFContratoUnidades_UndMedCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoUnidades_UndMedCod] <= @AV21TFContratoUnidades_UndMedCod_To)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV25TFContratoUnidades_UndMedNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24TFContratoUnidades_UndMedNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] like @lV24TFContratoUnidades_UndMedNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeMedicao_Nome] like @lV24TFContratoUnidades_UndMedNom)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TFContratoUnidades_UndMedNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] = @AV25TFContratoUnidades_UndMedNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeMedicao_Nome] = @AV25TFContratoUnidades_UndMedNom_Sel)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV29TFContratoUnidades_UndMedSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28TFContratoUnidades_UndMedSigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Sigla] like @lV28TFContratoUnidades_UndMedSigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeMedicao_Sigla] like @lV28TFContratoUnidades_UndMedSigla)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29TFContratoUnidades_UndMedSigla_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Sigla] = @AV29TFContratoUnidades_UndMedSigla_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeMedicao_Sigla] = @AV29TFContratoUnidades_UndMedSigla_Sel)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV32TFContratoUnidades_Produtividade) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoUnidades_Produtividade] >= @AV32TFContratoUnidades_Produtividade)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoUnidades_Produtividade] >= @AV32TFContratoUnidades_Produtividade)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV33TFContratoUnidades_Produtividade_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoUnidades_Produtividade] <= @AV33TFContratoUnidades_Produtividade_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoUnidades_Produtividade] <= @AV33TFContratoUnidades_Produtividade_To)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV9OrderedBy == 1 ) && ! AV10OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[UnidadeMedicao_Nome]";
         }
         else if ( ( AV9OrderedBy == 1 ) && ( AV10OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[UnidadeMedicao_Nome] DESC";
         }
         else if ( ( AV9OrderedBy == 2 ) && ! AV10OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoUnidades_ContratoCod]";
         }
         else if ( ( AV9OrderedBy == 2 ) && ( AV10OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoUnidades_ContratoCod] DESC";
         }
         else if ( ( AV9OrderedBy == 3 ) && ! AV10OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoUnidades_UndMedCod]";
         }
         else if ( ( AV9OrderedBy == 3 ) && ( AV10OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoUnidades_UndMedCod] DESC";
         }
         else if ( ( AV9OrderedBy == 4 ) && ! AV10OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[UnidadeMedicao_Sigla]";
         }
         else if ( ( AV9OrderedBy == 4 ) && ( AV10OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[UnidadeMedicao_Sigla] DESC";
         }
         else if ( ( AV9OrderedBy == 5 ) && ! AV10OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoUnidades_Produtividade]";
         }
         else if ( ( AV9OrderedBy == 5 ) && ( AV10OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoUnidades_Produtividade] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoUnidades_ContratoCod], T1.[ContratoUnidades_UndMedCod]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00IM3( IGxContext context ,
                                             int AV16TFContratoUnidades_ContratoCod ,
                                             int AV17TFContratoUnidades_ContratoCod_To ,
                                             int AV20TFContratoUnidades_UndMedCod ,
                                             int AV21TFContratoUnidades_UndMedCod_To ,
                                             String AV25TFContratoUnidades_UndMedNom_Sel ,
                                             String AV24TFContratoUnidades_UndMedNom ,
                                             String AV29TFContratoUnidades_UndMedSigla_Sel ,
                                             String AV28TFContratoUnidades_UndMedSigla ,
                                             decimal AV32TFContratoUnidades_Produtividade ,
                                             decimal AV33TFContratoUnidades_Produtividade_To ,
                                             int A1207ContratoUnidades_ContratoCod ,
                                             int A1204ContratoUnidades_UndMedCod ,
                                             String A1205ContratoUnidades_UndMedNom ,
                                             String A1206ContratoUnidades_UndMedSigla ,
                                             decimal A1208ContratoUnidades_Produtividade ,
                                             short AV9OrderedBy ,
                                             bool AV10OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [10] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([ContratoUnidades] T1 WITH (NOLOCK) INNER JOIN [UnidadeMedicao] T2 WITH (NOLOCK) ON T2.[UnidadeMedicao_Codigo] = T1.[ContratoUnidades_UndMedCod])";
         if ( ! (0==AV16TFContratoUnidades_ContratoCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoUnidades_ContratoCod] >= @AV16TFContratoUnidades_ContratoCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoUnidades_ContratoCod] >= @AV16TFContratoUnidades_ContratoCod)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ! (0==AV17TFContratoUnidades_ContratoCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoUnidades_ContratoCod] <= @AV17TFContratoUnidades_ContratoCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoUnidades_ContratoCod] <= @AV17TFContratoUnidades_ContratoCod_To)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ! (0==AV20TFContratoUnidades_UndMedCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoUnidades_UndMedCod] >= @AV20TFContratoUnidades_UndMedCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoUnidades_UndMedCod] >= @AV20TFContratoUnidades_UndMedCod)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ! (0==AV21TFContratoUnidades_UndMedCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoUnidades_UndMedCod] <= @AV21TFContratoUnidades_UndMedCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoUnidades_UndMedCod] <= @AV21TFContratoUnidades_UndMedCod_To)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV25TFContratoUnidades_UndMedNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24TFContratoUnidades_UndMedNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] like @lV24TFContratoUnidades_UndMedNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeMedicao_Nome] like @lV24TFContratoUnidades_UndMedNom)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TFContratoUnidades_UndMedNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] = @AV25TFContratoUnidades_UndMedNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeMedicao_Nome] = @AV25TFContratoUnidades_UndMedNom_Sel)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV29TFContratoUnidades_UndMedSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28TFContratoUnidades_UndMedSigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Sigla] like @lV28TFContratoUnidades_UndMedSigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeMedicao_Sigla] like @lV28TFContratoUnidades_UndMedSigla)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29TFContratoUnidades_UndMedSigla_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Sigla] = @AV29TFContratoUnidades_UndMedSigla_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeMedicao_Sigla] = @AV29TFContratoUnidades_UndMedSigla_Sel)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV32TFContratoUnidades_Produtividade) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoUnidades_Produtividade] >= @AV32TFContratoUnidades_Produtividade)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoUnidades_Produtividade] >= @AV32TFContratoUnidades_Produtividade)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV33TFContratoUnidades_Produtividade_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoUnidades_Produtividade] <= @AV33TFContratoUnidades_Produtividade_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoUnidades_Produtividade] <= @AV33TFContratoUnidades_Produtividade_To)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV9OrderedBy == 1 ) && ! AV10OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV9OrderedBy == 1 ) && ( AV10OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV9OrderedBy == 2 ) && ! AV10OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV9OrderedBy == 2 ) && ( AV10OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV9OrderedBy == 3 ) && ! AV10OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV9OrderedBy == 3 ) && ( AV10OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV9OrderedBy == 4 ) && ! AV10OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV9OrderedBy == 4 ) && ( AV10OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV9OrderedBy == 5 ) && ! AV10OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV9OrderedBy == 5 ) && ( AV10OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00IM2(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (decimal)dynConstraints[8] , (decimal)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (decimal)dynConstraints[14] , (short)dynConstraints[15] , (bool)dynConstraints[16] );
               case 1 :
                     return conditional_H00IM3(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (decimal)dynConstraints[8] , (decimal)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (decimal)dynConstraints[14] , (short)dynConstraints[15] , (bool)dynConstraints[16] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00IM2 ;
          prmH00IM2 = new Object[] {
          new Object[] {"@AV16TFContratoUnidades_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFContratoUnidades_ContratoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV20TFContratoUnidades_UndMedCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV21TFContratoUnidades_UndMedCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV24TFContratoUnidades_UndMedNom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV25TFContratoUnidades_UndMedNom_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV28TFContratoUnidades_UndMedSigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV29TFContratoUnidades_UndMedSigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV32TFContratoUnidades_Produtividade",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV33TFContratoUnidades_Produtividade_To",SqlDbType.Decimal,14,5} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00IM3 ;
          prmH00IM3 = new Object[] {
          new Object[] {"@AV16TFContratoUnidades_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFContratoUnidades_ContratoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV20TFContratoUnidades_UndMedCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV21TFContratoUnidades_UndMedCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV24TFContratoUnidades_UndMedNom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV25TFContratoUnidades_UndMedNom_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV28TFContratoUnidades_UndMedSigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV29TFContratoUnidades_UndMedSigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV32TFContratoUnidades_Produtividade",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV33TFContratoUnidades_Produtividade_To",SqlDbType.Decimal,14,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00IM2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IM2,11,0,true,false )
             ,new CursorDef("H00IM3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IM3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[23]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[24]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[12]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[18]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[19]);
                }
                return;
       }
    }

 }

}
