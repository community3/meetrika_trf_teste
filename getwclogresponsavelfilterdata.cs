/*
               File: GetWCLogResponsavelFilterData
        Description: Get WCLog Responsavel Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 12:44:26.16
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwclogresponsavelfilterdata : GXProcedure
   {
      public getwclogresponsavelfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwclogresponsavelfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV24DDOName = aP0_DDOName;
         this.AV22SearchTxt = aP1_SearchTxt;
         this.AV23SearchTxtTo = aP2_SearchTxtTo;
         this.AV28OptionsJson = "" ;
         this.AV31OptionsDescJson = "" ;
         this.AV33OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV28OptionsJson;
         aP4_OptionsDescJson=this.AV31OptionsDescJson;
         aP5_OptionIndexesJson=this.AV33OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV24DDOName = aP0_DDOName;
         this.AV22SearchTxt = aP1_SearchTxt;
         this.AV23SearchTxtTo = aP2_SearchTxtTo;
         this.AV28OptionsJson = "" ;
         this.AV31OptionsDescJson = "" ;
         this.AV33OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV28OptionsJson;
         aP4_OptionsDescJson=this.AV31OptionsDescJson;
         aP5_OptionIndexesJson=this.AV33OptionIndexesJson;
         return AV33OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwclogresponsavelfilterdata objgetwclogresponsavelfilterdata;
         objgetwclogresponsavelfilterdata = new getwclogresponsavelfilterdata();
         objgetwclogresponsavelfilterdata.AV24DDOName = aP0_DDOName;
         objgetwclogresponsavelfilterdata.AV22SearchTxt = aP1_SearchTxt;
         objgetwclogresponsavelfilterdata.AV23SearchTxtTo = aP2_SearchTxtTo;
         objgetwclogresponsavelfilterdata.AV28OptionsJson = "" ;
         objgetwclogresponsavelfilterdata.AV31OptionsDescJson = "" ;
         objgetwclogresponsavelfilterdata.AV33OptionIndexesJson = "" ;
         objgetwclogresponsavelfilterdata.context.SetSubmitInitialConfig(context);
         objgetwclogresponsavelfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwclogresponsavelfilterdata);
         aP3_OptionsJson=this.AV28OptionsJson;
         aP4_OptionsDescJson=this.AV31OptionsDescJson;
         aP5_OptionIndexesJson=this.AV33OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwclogresponsavelfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV27Options = (IGxCollection)(new GxSimpleCollection());
         AV30OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV32OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV24DDOName), "DDO_LOGRESPONSAVEL_OBSERVACAO") == 0 )
         {
            /* Execute user subroutine: 'LOADLOGRESPONSAVEL_OBSERVACAOOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV28OptionsJson = AV27Options.ToJSonString(false);
         AV31OptionsDescJson = AV30OptionsDesc.ToJSonString(false);
         AV33OptionIndexesJson = AV32OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV35Session.Get("WCLogResponsavelGridState"), "") == 0 )
         {
            AV37GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WCLogResponsavelGridState"), "");
         }
         else
         {
            AV37GridState.FromXml(AV35Session.Get("WCLogResponsavelGridState"), "");
         }
         AV43GXV1 = 1;
         while ( AV43GXV1 <= AV37GridState.gxTpr_Filtervalues.Count )
         {
            AV38GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV37GridState.gxTpr_Filtervalues.Item(AV43GXV1));
            if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFLOGRESPONSAVEL_DATAHORA") == 0 )
            {
               AV10TFLogResponsavel_DataHora = context.localUtil.CToT( AV38GridStateFilterValue.gxTpr_Value, 2);
               AV11TFLogResponsavel_DataHora_To = context.localUtil.CToT( AV38GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFLOGRESPONSAVEL_ACAO_SEL") == 0 )
            {
               AV12TFLogResponsavel_Acao_SelsJson = AV38GridStateFilterValue.gxTpr_Value;
               AV13TFLogResponsavel_Acao_Sels.FromJSonString(AV12TFLogResponsavel_Acao_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFLOGRESPONSAVEL_STATUS_SEL") == 0 )
            {
               AV14TFLogResponsavel_Status_SelsJson = AV38GridStateFilterValue.gxTpr_Value;
               AV15TFLogResponsavel_Status_Sels.FromJSonString(AV14TFLogResponsavel_Status_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFLOGRESPONSAVEL_NOVOSTATUS_SEL") == 0 )
            {
               AV16TFLogResponsavel_NovoStatus_SelsJson = AV38GridStateFilterValue.gxTpr_Value;
               AV17TFLogResponsavel_NovoStatus_Sels.FromJSonString(AV16TFLogResponsavel_NovoStatus_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFLOGRESPONSAVEL_PRAZO") == 0 )
            {
               AV18TFLogResponsavel_Prazo = context.localUtil.CToT( AV38GridStateFilterValue.gxTpr_Value, 2);
               AV19TFLogResponsavel_Prazo_To = context.localUtil.CToT( AV38GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFLOGRESPONSAVEL_OBSERVACAO") == 0 )
            {
               AV20TFLogResponsavel_Observacao = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFLOGRESPONSAVEL_OBSERVACAO_SEL") == 0 )
            {
               AV21TFLogResponsavel_Observacao_Sel = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "PARM_&LOGRESPONSAVEL_DEMANDACOD") == 0 )
            {
               AV40LogResponsavel_DemandaCod = (int)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Value, "."));
            }
            AV43GXV1 = (int)(AV43GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'LOADLOGRESPONSAVEL_OBSERVACAOOPTIONS' Routine */
         AV20TFLogResponsavel_Observacao = AV22SearchTxt;
         AV21TFLogResponsavel_Observacao_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A894LogResponsavel_Acao ,
                                              AV13TFLogResponsavel_Acao_Sels ,
                                              A1130LogResponsavel_Status ,
                                              AV15TFLogResponsavel_Status_Sels ,
                                              A1234LogResponsavel_NovoStatus ,
                                              AV17TFLogResponsavel_NovoStatus_Sels ,
                                              AV10TFLogResponsavel_DataHora ,
                                              AV11TFLogResponsavel_DataHora_To ,
                                              AV13TFLogResponsavel_Acao_Sels.Count ,
                                              AV15TFLogResponsavel_Status_Sels.Count ,
                                              AV17TFLogResponsavel_NovoStatus_Sels.Count ,
                                              AV18TFLogResponsavel_Prazo ,
                                              AV19TFLogResponsavel_Prazo_To ,
                                              AV21TFLogResponsavel_Observacao_Sel ,
                                              AV20TFLogResponsavel_Observacao ,
                                              A893LogResponsavel_DataHora ,
                                              A1177LogResponsavel_Prazo ,
                                              A1131LogResponsavel_Observacao ,
                                              AV40LogResponsavel_DemandaCod ,
                                              A892LogResponsavel_DemandaCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         lV20TFLogResponsavel_Observacao = StringUtil.Concat( StringUtil.RTrim( AV20TFLogResponsavel_Observacao), "%", "");
         /* Using cursor P00S72 */
         pr_default.execute(0, new Object[] {AV40LogResponsavel_DemandaCod, AV10TFLogResponsavel_DataHora, AV11TFLogResponsavel_DataHora_To, AV18TFLogResponsavel_Prazo, AV19TFLogResponsavel_Prazo_To, lV20TFLogResponsavel_Observacao, AV21TFLogResponsavel_Observacao_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKS72 = false;
            A892LogResponsavel_DemandaCod = P00S72_A892LogResponsavel_DemandaCod[0];
            n892LogResponsavel_DemandaCod = P00S72_n892LogResponsavel_DemandaCod[0];
            A1131LogResponsavel_Observacao = P00S72_A1131LogResponsavel_Observacao[0];
            n1131LogResponsavel_Observacao = P00S72_n1131LogResponsavel_Observacao[0];
            A1177LogResponsavel_Prazo = P00S72_A1177LogResponsavel_Prazo[0];
            n1177LogResponsavel_Prazo = P00S72_n1177LogResponsavel_Prazo[0];
            A1234LogResponsavel_NovoStatus = P00S72_A1234LogResponsavel_NovoStatus[0];
            n1234LogResponsavel_NovoStatus = P00S72_n1234LogResponsavel_NovoStatus[0];
            A1130LogResponsavel_Status = P00S72_A1130LogResponsavel_Status[0];
            n1130LogResponsavel_Status = P00S72_n1130LogResponsavel_Status[0];
            A894LogResponsavel_Acao = P00S72_A894LogResponsavel_Acao[0];
            A893LogResponsavel_DataHora = P00S72_A893LogResponsavel_DataHora[0];
            A1797LogResponsavel_Codigo = P00S72_A1797LogResponsavel_Codigo[0];
            AV34count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00S72_A892LogResponsavel_DemandaCod[0] == A892LogResponsavel_DemandaCod ) && ( StringUtil.StrCmp(P00S72_A1131LogResponsavel_Observacao[0], A1131LogResponsavel_Observacao) == 0 ) )
            {
               BRKS72 = false;
               A1797LogResponsavel_Codigo = P00S72_A1797LogResponsavel_Codigo[0];
               AV34count = (long)(AV34count+1);
               BRKS72 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1131LogResponsavel_Observacao)) )
            {
               AV26Option = A1131LogResponsavel_Observacao;
               AV27Options.Add(AV26Option, 0);
               AV32OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV34count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV27Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKS72 )
            {
               BRKS72 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV27Options = new GxSimpleCollection();
         AV30OptionsDesc = new GxSimpleCollection();
         AV32OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV35Session = context.GetSession();
         AV37GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV38GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFLogResponsavel_DataHora = (DateTime)(DateTime.MinValue);
         AV11TFLogResponsavel_DataHora_To = (DateTime)(DateTime.MinValue);
         AV12TFLogResponsavel_Acao_SelsJson = "";
         AV13TFLogResponsavel_Acao_Sels = new GxSimpleCollection();
         AV14TFLogResponsavel_Status_SelsJson = "";
         AV15TFLogResponsavel_Status_Sels = new GxSimpleCollection();
         AV16TFLogResponsavel_NovoStatus_SelsJson = "";
         AV17TFLogResponsavel_NovoStatus_Sels = new GxSimpleCollection();
         AV18TFLogResponsavel_Prazo = (DateTime)(DateTime.MinValue);
         AV19TFLogResponsavel_Prazo_To = (DateTime)(DateTime.MinValue);
         AV20TFLogResponsavel_Observacao = "";
         AV21TFLogResponsavel_Observacao_Sel = "";
         scmdbuf = "";
         lV20TFLogResponsavel_Observacao = "";
         A894LogResponsavel_Acao = "";
         A1130LogResponsavel_Status = "";
         A1234LogResponsavel_NovoStatus = "";
         A893LogResponsavel_DataHora = (DateTime)(DateTime.MinValue);
         A1177LogResponsavel_Prazo = (DateTime)(DateTime.MinValue);
         A1131LogResponsavel_Observacao = "";
         P00S72_A892LogResponsavel_DemandaCod = new int[1] ;
         P00S72_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         P00S72_A1131LogResponsavel_Observacao = new String[] {""} ;
         P00S72_n1131LogResponsavel_Observacao = new bool[] {false} ;
         P00S72_A1177LogResponsavel_Prazo = new DateTime[] {DateTime.MinValue} ;
         P00S72_n1177LogResponsavel_Prazo = new bool[] {false} ;
         P00S72_A1234LogResponsavel_NovoStatus = new String[] {""} ;
         P00S72_n1234LogResponsavel_NovoStatus = new bool[] {false} ;
         P00S72_A1130LogResponsavel_Status = new String[] {""} ;
         P00S72_n1130LogResponsavel_Status = new bool[] {false} ;
         P00S72_A894LogResponsavel_Acao = new String[] {""} ;
         P00S72_A893LogResponsavel_DataHora = new DateTime[] {DateTime.MinValue} ;
         P00S72_A1797LogResponsavel_Codigo = new long[1] ;
         AV26Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwclogresponsavelfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00S72_A892LogResponsavel_DemandaCod, P00S72_n892LogResponsavel_DemandaCod, P00S72_A1131LogResponsavel_Observacao, P00S72_n1131LogResponsavel_Observacao, P00S72_A1177LogResponsavel_Prazo, P00S72_n1177LogResponsavel_Prazo, P00S72_A1234LogResponsavel_NovoStatus, P00S72_n1234LogResponsavel_NovoStatus, P00S72_A1130LogResponsavel_Status, P00S72_n1130LogResponsavel_Status,
               P00S72_A894LogResponsavel_Acao, P00S72_A893LogResponsavel_DataHora, P00S72_A1797LogResponsavel_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV43GXV1 ;
      private int AV40LogResponsavel_DemandaCod ;
      private int AV13TFLogResponsavel_Acao_Sels_Count ;
      private int AV15TFLogResponsavel_Status_Sels_Count ;
      private int AV17TFLogResponsavel_NovoStatus_Sels_Count ;
      private int A892LogResponsavel_DemandaCod ;
      private long A1797LogResponsavel_Codigo ;
      private long AV34count ;
      private String scmdbuf ;
      private String A894LogResponsavel_Acao ;
      private String A1130LogResponsavel_Status ;
      private String A1234LogResponsavel_NovoStatus ;
      private DateTime AV10TFLogResponsavel_DataHora ;
      private DateTime AV11TFLogResponsavel_DataHora_To ;
      private DateTime AV18TFLogResponsavel_Prazo ;
      private DateTime AV19TFLogResponsavel_Prazo_To ;
      private DateTime A893LogResponsavel_DataHora ;
      private DateTime A1177LogResponsavel_Prazo ;
      private bool returnInSub ;
      private bool BRKS72 ;
      private bool n892LogResponsavel_DemandaCod ;
      private bool n1131LogResponsavel_Observacao ;
      private bool n1177LogResponsavel_Prazo ;
      private bool n1234LogResponsavel_NovoStatus ;
      private bool n1130LogResponsavel_Status ;
      private String AV33OptionIndexesJson ;
      private String AV28OptionsJson ;
      private String AV31OptionsDescJson ;
      private String AV12TFLogResponsavel_Acao_SelsJson ;
      private String AV14TFLogResponsavel_Status_SelsJson ;
      private String AV16TFLogResponsavel_NovoStatus_SelsJson ;
      private String A1131LogResponsavel_Observacao ;
      private String AV24DDOName ;
      private String AV22SearchTxt ;
      private String AV23SearchTxtTo ;
      private String AV20TFLogResponsavel_Observacao ;
      private String AV21TFLogResponsavel_Observacao_Sel ;
      private String lV20TFLogResponsavel_Observacao ;
      private String AV26Option ;
      private IGxSession AV35Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00S72_A892LogResponsavel_DemandaCod ;
      private bool[] P00S72_n892LogResponsavel_DemandaCod ;
      private String[] P00S72_A1131LogResponsavel_Observacao ;
      private bool[] P00S72_n1131LogResponsavel_Observacao ;
      private DateTime[] P00S72_A1177LogResponsavel_Prazo ;
      private bool[] P00S72_n1177LogResponsavel_Prazo ;
      private String[] P00S72_A1234LogResponsavel_NovoStatus ;
      private bool[] P00S72_n1234LogResponsavel_NovoStatus ;
      private String[] P00S72_A1130LogResponsavel_Status ;
      private bool[] P00S72_n1130LogResponsavel_Status ;
      private String[] P00S72_A894LogResponsavel_Acao ;
      private DateTime[] P00S72_A893LogResponsavel_DataHora ;
      private long[] P00S72_A1797LogResponsavel_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV13TFLogResponsavel_Acao_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV15TFLogResponsavel_Status_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV17TFLogResponsavel_NovoStatus_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV27Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV30OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV32OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV37GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV38GridStateFilterValue ;
   }

   public class getwclogresponsavelfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00S72( IGxContext context ,
                                             String A894LogResponsavel_Acao ,
                                             IGxCollection AV13TFLogResponsavel_Acao_Sels ,
                                             String A1130LogResponsavel_Status ,
                                             IGxCollection AV15TFLogResponsavel_Status_Sels ,
                                             String A1234LogResponsavel_NovoStatus ,
                                             IGxCollection AV17TFLogResponsavel_NovoStatus_Sels ,
                                             DateTime AV10TFLogResponsavel_DataHora ,
                                             DateTime AV11TFLogResponsavel_DataHora_To ,
                                             int AV13TFLogResponsavel_Acao_Sels_Count ,
                                             int AV15TFLogResponsavel_Status_Sels_Count ,
                                             int AV17TFLogResponsavel_NovoStatus_Sels_Count ,
                                             DateTime AV18TFLogResponsavel_Prazo ,
                                             DateTime AV19TFLogResponsavel_Prazo_To ,
                                             String AV21TFLogResponsavel_Observacao_Sel ,
                                             String AV20TFLogResponsavel_Observacao ,
                                             DateTime A893LogResponsavel_DataHora ,
                                             DateTime A1177LogResponsavel_Prazo ,
                                             String A1131LogResponsavel_Observacao ,
                                             int AV40LogResponsavel_DemandaCod ,
                                             int A892LogResponsavel_DemandaCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [7] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [LogResponsavel_DemandaCod], [LogResponsavel_Observacao], [LogResponsavel_Prazo], [LogResponsavel_NovoStatus], [LogResponsavel_Status], [LogResponsavel_Acao], [LogResponsavel_DataHora], [LogResponsavel_Codigo] FROM [LogResponsavel] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([LogResponsavel_DemandaCod] = @AV40LogResponsavel_DemandaCod)";
         if ( ! (DateTime.MinValue==AV10TFLogResponsavel_DataHora) )
         {
            sWhereString = sWhereString + " and ([LogResponsavel_DataHora] >= @AV10TFLogResponsavel_DataHora)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ! (DateTime.MinValue==AV11TFLogResponsavel_DataHora_To) )
         {
            sWhereString = sWhereString + " and ([LogResponsavel_DataHora] <= @AV11TFLogResponsavel_DataHora_To)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV13TFLogResponsavel_Acao_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV13TFLogResponsavel_Acao_Sels, "[LogResponsavel_Acao] IN (", ")") + ")";
         }
         if ( AV15TFLogResponsavel_Status_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV15TFLogResponsavel_Status_Sels, "[LogResponsavel_Status] IN (", ")") + ")";
         }
         if ( AV17TFLogResponsavel_NovoStatus_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV17TFLogResponsavel_NovoStatus_Sels, "[LogResponsavel_NovoStatus] IN (", ")") + ")";
         }
         if ( ! (DateTime.MinValue==AV18TFLogResponsavel_Prazo) )
         {
            sWhereString = sWhereString + " and ([LogResponsavel_Prazo] >= @AV18TFLogResponsavel_Prazo)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! (DateTime.MinValue==AV19TFLogResponsavel_Prazo_To) )
         {
            sWhereString = sWhereString + " and ([LogResponsavel_Prazo] <= @AV19TFLogResponsavel_Prazo_To)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21TFLogResponsavel_Observacao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFLogResponsavel_Observacao)) ) )
         {
            sWhereString = sWhereString + " and ([LogResponsavel_Observacao] like @lV20TFLogResponsavel_Observacao)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFLogResponsavel_Observacao_Sel)) )
         {
            sWhereString = sWhereString + " and ([LogResponsavel_Observacao] = @AV21TFLogResponsavel_Observacao_Sel)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [LogResponsavel_DemandaCod], [LogResponsavel_Observacao]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00S72(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (DateTime)dynConstraints[6] , (DateTime)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] , (DateTime)dynConstraints[11] , (DateTime)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00S72 ;
          prmP00S72 = new Object[] {
          new Object[] {"@AV40LogResponsavel_DemandaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV10TFLogResponsavel_DataHora",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV11TFLogResponsavel_DataHora_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV18TFLogResponsavel_Prazo",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV19TFLogResponsavel_Prazo_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV20TFLogResponsavel_Observacao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV21TFLogResponsavel_Observacao_Sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00S72", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00S72,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getLongVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[4])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getString(6, 20) ;
                ((DateTime[]) buf[11])[0] = rslt.getGXDateTime(7) ;
                ((long[]) buf[12])[0] = rslt.getLong(8) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwclogresponsavelfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwclogresponsavelfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwclogresponsavelfilterdata") )
          {
             return  ;
          }
          getwclogresponsavelfilterdata worker = new getwclogresponsavelfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
