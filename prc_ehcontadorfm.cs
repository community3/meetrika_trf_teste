/*
               File: PRC_EhContadorFM
        Description: O Usu�rio Eh Contador FM
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:51:11.8
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_ehcontadorfm : GXProcedure
   {
      public prc_ehcontadorfm( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_ehcontadorfm( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratadaUsuario_UsuarioCod ,
                           int aP1_Contratada_AreaTrabalhoCod ,
                           out bool aP2_Flag )
      {
         this.A69ContratadaUsuario_UsuarioCod = aP0_ContratadaUsuario_UsuarioCod;
         this.A52Contratada_AreaTrabalhoCod = aP1_Contratada_AreaTrabalhoCod;
         this.AV8Flag = false ;
         initialize();
         executePrivate();
         aP2_Flag=this.AV8Flag;
      }

      public bool executeUdp( int aP0_ContratadaUsuario_UsuarioCod ,
                              int aP1_Contratada_AreaTrabalhoCod )
      {
         this.A69ContratadaUsuario_UsuarioCod = aP0_ContratadaUsuario_UsuarioCod;
         this.A52Contratada_AreaTrabalhoCod = aP1_Contratada_AreaTrabalhoCod;
         this.AV8Flag = false ;
         initialize();
         executePrivate();
         aP2_Flag=this.AV8Flag;
         return AV8Flag ;
      }

      public void executeSubmit( int aP0_ContratadaUsuario_UsuarioCod ,
                                 int aP1_Contratada_AreaTrabalhoCod ,
                                 out bool aP2_Flag )
      {
         prc_ehcontadorfm objprc_ehcontadorfm;
         objprc_ehcontadorfm = new prc_ehcontadorfm();
         objprc_ehcontadorfm.A69ContratadaUsuario_UsuarioCod = aP0_ContratadaUsuario_UsuarioCod;
         objprc_ehcontadorfm.A52Contratada_AreaTrabalhoCod = aP1_Contratada_AreaTrabalhoCod;
         objprc_ehcontadorfm.AV8Flag = false ;
         objprc_ehcontadorfm.context.SetSubmitInitialConfig(context);
         objprc_ehcontadorfm.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_ehcontadorfm);
         aP2_Flag=this.AV8Flag;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_ehcontadorfm)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00462 */
         pr_default.execute(0, new Object[] {A69ContratadaUsuario_UsuarioCod, A52Contratada_AreaTrabalhoCod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A66ContratadaUsuario_ContratadaCod = P00462_A66ContratadaUsuario_ContratadaCod[0];
            A516Contratada_TipoFabrica = P00462_A516Contratada_TipoFabrica[0];
            n516Contratada_TipoFabrica = P00462_n516Contratada_TipoFabrica[0];
            A516Contratada_TipoFabrica = P00462_A516Contratada_TipoFabrica[0];
            n516Contratada_TipoFabrica = P00462_n516Contratada_TipoFabrica[0];
            AV8Flag = true;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00462_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P00462_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         P00462_A516Contratada_TipoFabrica = new String[] {""} ;
         P00462_n516Contratada_TipoFabrica = new bool[] {false} ;
         A516Contratada_TipoFabrica = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_ehcontadorfm__default(),
            new Object[][] {
                new Object[] {
               P00462_A66ContratadaUsuario_ContratadaCod, P00462_A69ContratadaUsuario_UsuarioCod, P00462_A516Contratada_TipoFabrica, P00462_n516Contratada_TipoFabrica
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A69ContratadaUsuario_UsuarioCod ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private String scmdbuf ;
      private String A516Contratada_TipoFabrica ;
      private bool AV8Flag ;
      private bool n516Contratada_TipoFabrica ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00462_A66ContratadaUsuario_ContratadaCod ;
      private int[] P00462_A69ContratadaUsuario_UsuarioCod ;
      private String[] P00462_A516Contratada_TipoFabrica ;
      private bool[] P00462_n516Contratada_TipoFabrica ;
      private bool aP2_Flag ;
   }

   public class prc_ehcontadorfm__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00462 ;
          prmP00462 = new Object[] {
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contratada_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00462", "SELECT TOP 1 T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T1.[ContratadaUsuario_UsuarioCod], T2.[Contratada_TipoFabrica] FROM ([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) WHERE (T1.[ContratadaUsuario_UsuarioCod] = @ContratadaUsuario_UsuarioCod) AND (@Contratada_AreaTrabalhoCod = @Contratada_AreaTrabalhoCod) AND (T2.[Contratada_TipoFabrica] = 'M') ORDER BY T1.[ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00462,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
