/*
               File: PRC_ContarArquivoAnexo
        Description: PRC_Contar Arquivo Anexo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:51:36.23
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_contararquivoanexo : GXProcedure
   {
      public prc_contararquivoanexo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_contararquivoanexo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_LoteArquivoAnexo_Verificador ,
                           out short aP1_vrQtd )
      {
         this.AV10LoteArquivoAnexo_Verificador = aP0_LoteArquivoAnexo_Verificador;
         this.AV9vrQtd = 0 ;
         initialize();
         executePrivate();
         aP1_vrQtd=this.AV9vrQtd;
      }

      public short executeUdp( String aP0_LoteArquivoAnexo_Verificador )
      {
         this.AV10LoteArquivoAnexo_Verificador = aP0_LoteArquivoAnexo_Verificador;
         this.AV9vrQtd = 0 ;
         initialize();
         executePrivate();
         aP1_vrQtd=this.AV9vrQtd;
         return AV9vrQtd ;
      }

      public void executeSubmit( String aP0_LoteArquivoAnexo_Verificador ,
                                 out short aP1_vrQtd )
      {
         prc_contararquivoanexo objprc_contararquivoanexo;
         objprc_contararquivoanexo = new prc_contararquivoanexo();
         objprc_contararquivoanexo.AV10LoteArquivoAnexo_Verificador = aP0_LoteArquivoAnexo_Verificador;
         objprc_contararquivoanexo.AV9vrQtd = 0 ;
         objprc_contararquivoanexo.context.SetSubmitInitialConfig(context);
         objprc_contararquivoanexo.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_contararquivoanexo);
         aP1_vrQtd=this.AV9vrQtd;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_contararquivoanexo)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Optimized group. */
         /* Using cursor P006P2 */
         pr_default.execute(0, new Object[] {AV10LoteArquivoAnexo_Verificador});
         cV9vrQtd = P006P2_AV9vrQtd[0];
         pr_default.close(0);
         AV9vrQtd = (short)(AV9vrQtd+cV9vrQtd*1);
         /* End optimized group. */
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P006P2_AV9vrQtd = new short[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_contararquivoanexo__default(),
            new Object[][] {
                new Object[] {
               P006P2_AV9vrQtd
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV9vrQtd ;
      private short cV9vrQtd ;
      private String scmdbuf ;
      private String AV10LoteArquivoAnexo_Verificador ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private short[] P006P2_AV9vrQtd ;
      private short aP1_vrQtd ;
   }

   public class prc_contararquivoanexo__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP006P2 ;
          prmP006P2 = new Object[] {
          new Object[] {"@AV10LoteArquivoAnexo_Verificador",SqlDbType.VarChar,40,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P006P2", "SELECT COUNT(*) FROM [LoteArquivoAnexo] WITH (NOLOCK) WHERE [LoteArquivoAnexo_Verificador] = @AV10LoteArquivoAnexo_Verificador ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006P2,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
       }
    }

 }

}
