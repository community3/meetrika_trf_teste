/*
               File: LoadAuditContrato
        Description: Load Audit Contrato
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:6:44.17
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class loadauditcontrato : GXProcedure
   {
      public loadauditcontrato( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public loadauditcontrato( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_SaveOldValues ,
                           ref wwpbaseobjects.SdtAuditingObject aP1_AuditingObject ,
                           int aP2_Contrato_Codigo ,
                           String aP3_ActualMode )
      {
         this.AV13SaveOldValues = aP0_SaveOldValues;
         this.AV10AuditingObject = aP1_AuditingObject;
         this.AV16Contrato_Codigo = aP2_Contrato_Codigo;
         this.AV14ActualMode = aP3_ActualMode;
         initialize();
         executePrivate();
         aP1_AuditingObject=this.AV10AuditingObject;
      }

      public void executeSubmit( String aP0_SaveOldValues ,
                                 ref wwpbaseobjects.SdtAuditingObject aP1_AuditingObject ,
                                 int aP2_Contrato_Codigo ,
                                 String aP3_ActualMode )
      {
         loadauditcontrato objloadauditcontrato;
         objloadauditcontrato = new loadauditcontrato();
         objloadauditcontrato.AV13SaveOldValues = aP0_SaveOldValues;
         objloadauditcontrato.AV10AuditingObject = aP1_AuditingObject;
         objloadauditcontrato.AV16Contrato_Codigo = aP2_Contrato_Codigo;
         objloadauditcontrato.AV14ActualMode = aP3_ActualMode;
         objloadauditcontrato.context.SetSubmitInitialConfig(context);
         objloadauditcontrato.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objloadauditcontrato);
         aP1_AuditingObject=this.AV10AuditingObject;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((loadauditcontrato)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( StringUtil.StrCmp(AV13SaveOldValues, "Y") == 0 )
         {
            if ( ( StringUtil.StrCmp(AV14ActualMode, "DLT") == 0 ) || ( StringUtil.StrCmp(AV14ActualMode, "UPD") == 0 ) )
            {
               /* Execute user subroutine: 'LOADOLDVALUES' */
               S111 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
            }
         }
         else
         {
            /* Execute user subroutine: 'LOADNEWVALUES' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADOLDVALUES' Routine */
         /* Using cursor P00WD9 */
         pr_default.execute(0, new Object[] {AV16Contrato_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A74Contrato_Codigo = P00WD9_A74Contrato_Codigo[0];
            n74Contrato_Codigo = P00WD9_n74Contrato_Codigo[0];
            A75Contrato_AreaTrabalhoCod = P00WD9_A75Contrato_AreaTrabalhoCod[0];
            A76Contrato_AreaTrabalhoDes = P00WD9_A76Contrato_AreaTrabalhoDes[0];
            n76Contrato_AreaTrabalhoDes = P00WD9_n76Contrato_AreaTrabalhoDes[0];
            A39Contratada_Codigo = P00WD9_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00WD9_A40Contratada_PessoaCod[0];
            A42Contratada_PessoaCNPJ = P00WD9_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00WD9_n42Contratada_PessoaCNPJ[0];
            A438Contratada_Sigla = P00WD9_A438Contratada_Sigla[0];
            A516Contratada_TipoFabrica = P00WD9_A516Contratada_TipoFabrica[0];
            A78Contrato_NumeroAta = P00WD9_A78Contrato_NumeroAta[0];
            n78Contrato_NumeroAta = P00WD9_n78Contrato_NumeroAta[0];
            A79Contrato_Ano = P00WD9_A79Contrato_Ano[0];
            A80Contrato_Objeto = P00WD9_A80Contrato_Objeto[0];
            A115Contrato_UnidadeContratacao = P00WD9_A115Contrato_UnidadeContratacao[0];
            A81Contrato_Quantidade = P00WD9_A81Contrato_Quantidade[0];
            A82Contrato_DataVigenciaInicio = P00WD9_A82Contrato_DataVigenciaInicio[0];
            A84Contrato_DataPublicacaoDOU = P00WD9_A84Contrato_DataPublicacaoDOU[0];
            A85Contrato_DataAssinatura = P00WD9_A85Contrato_DataAssinatura[0];
            A86Contrato_DataPedidoReajuste = P00WD9_A86Contrato_DataPedidoReajuste[0];
            A87Contrato_DataTerminoAta = P00WD9_A87Contrato_DataTerminoAta[0];
            A88Contrato_DataFimAdaptacao = P00WD9_A88Contrato_DataFimAdaptacao[0];
            A89Contrato_Valor = P00WD9_A89Contrato_Valor[0];
            A116Contrato_ValorUnidadeContratacao = P00WD9_A116Contrato_ValorUnidadeContratacao[0];
            A90Contrato_RegrasPagto = P00WD9_A90Contrato_RegrasPagto[0];
            A91Contrato_DiasPagto = P00WD9_A91Contrato_DiasPagto[0];
            A452Contrato_CalculoDivergencia = P00WD9_A452Contrato_CalculoDivergencia[0];
            A453Contrato_IndiceDivergencia = P00WD9_A453Contrato_IndiceDivergencia[0];
            A1150Contrato_AceitaPFFS = P00WD9_A1150Contrato_AceitaPFFS[0];
            n1150Contrato_AceitaPFFS = P00WD9_n1150Contrato_AceitaPFFS[0];
            A1013Contrato_PrepostoCod = P00WD9_A1013Contrato_PrepostoCod[0];
            n1013Contrato_PrepostoCod = P00WD9_n1013Contrato_PrepostoCod[0];
            A1016Contrato_PrepostoPesCod = P00WD9_A1016Contrato_PrepostoPesCod[0];
            n1016Contrato_PrepostoPesCod = P00WD9_n1016Contrato_PrepostoPesCod[0];
            A1015Contrato_PrepostoNom = P00WD9_A1015Contrato_PrepostoNom[0];
            n1015Contrato_PrepostoNom = P00WD9_n1015Contrato_PrepostoNom[0];
            A1354Contrato_PrdFtrCada = P00WD9_A1354Contrato_PrdFtrCada[0];
            n1354Contrato_PrdFtrCada = P00WD9_n1354Contrato_PrdFtrCada[0];
            A2086Contrato_LmtFtr = P00WD9_A2086Contrato_LmtFtr[0];
            n2086Contrato_LmtFtr = P00WD9_n2086Contrato_LmtFtr[0];
            A1357Contrato_PrdFtrIni = P00WD9_A1357Contrato_PrdFtrIni[0];
            n1357Contrato_PrdFtrIni = P00WD9_n1357Contrato_PrdFtrIni[0];
            A1358Contrato_PrdFtrFim = P00WD9_A1358Contrato_PrdFtrFim[0];
            n1358Contrato_PrdFtrFim = P00WD9_n1358Contrato_PrdFtrFim[0];
            A842Contrato_DataInicioTA = P00WD9_A842Contrato_DataInicioTA[0];
            n842Contrato_DataInicioTA = P00WD9_n842Contrato_DataInicioTA[0];
            A1870Contrato_ValorUndCntAtual = P00WD9_A1870Contrato_ValorUndCntAtual[0];
            n1870Contrato_ValorUndCntAtual = P00WD9_n1870Contrato_ValorUndCntAtual[0];
            A41Contratada_PessoaNom = P00WD9_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00WD9_n41Contratada_PessoaNom[0];
            A92Contrato_Ativo = P00WD9_A92Contrato_Ativo[0];
            A77Contrato_Numero = P00WD9_A77Contrato_Numero[0];
            A843Contrato_DataFimTA = P00WD9_A843Contrato_DataFimTA[0];
            n843Contrato_DataFimTA = P00WD9_n843Contrato_DataFimTA[0];
            A83Contrato_DataVigenciaTermino = P00WD9_A83Contrato_DataVigenciaTermino[0];
            A842Contrato_DataInicioTA = P00WD9_A842Contrato_DataInicioTA[0];
            n842Contrato_DataInicioTA = P00WD9_n842Contrato_DataInicioTA[0];
            A1870Contrato_ValorUndCntAtual = P00WD9_A1870Contrato_ValorUndCntAtual[0];
            n1870Contrato_ValorUndCntAtual = P00WD9_n1870Contrato_ValorUndCntAtual[0];
            A843Contrato_DataFimTA = P00WD9_A843Contrato_DataFimTA[0];
            n843Contrato_DataFimTA = P00WD9_n843Contrato_DataFimTA[0];
            A76Contrato_AreaTrabalhoDes = P00WD9_A76Contrato_AreaTrabalhoDes[0];
            n76Contrato_AreaTrabalhoDes = P00WD9_n76Contrato_AreaTrabalhoDes[0];
            A40Contratada_PessoaCod = P00WD9_A40Contratada_PessoaCod[0];
            A438Contratada_Sigla = P00WD9_A438Contratada_Sigla[0];
            A516Contratada_TipoFabrica = P00WD9_A516Contratada_TipoFabrica[0];
            A42Contratada_PessoaCNPJ = P00WD9_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00WD9_n42Contratada_PessoaCNPJ[0];
            A41Contratada_PessoaNom = P00WD9_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00WD9_n41Contratada_PessoaNom[0];
            A1016Contrato_PrepostoPesCod = P00WD9_A1016Contrato_PrepostoPesCod[0];
            n1016Contrato_PrepostoPesCod = P00WD9_n1016Contrato_PrepostoPesCod[0];
            A1015Contrato_PrepostoNom = P00WD9_A1015Contrato_PrepostoNom[0];
            n1015Contrato_PrepostoNom = P00WD9_n1015Contrato_PrepostoNom[0];
            A1869Contrato_DataTermino = ((A83Contrato_DataVigenciaTermino>A843Contrato_DataFimTA) ? A83Contrato_DataVigenciaTermino : A843Contrato_DataFimTA);
            A2096Contrato_Identificacao = StringUtil.Format( "%1 - %2 %3", A77Contrato_Numero, A41Contratada_PessoaNom, (A92Contrato_Ativo ? "(Vigente)" : "(Encerrado)"), "", "", "", "", "", "");
            AV10AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
            AV10AuditingObject.gxTpr_Mode = AV14ActualMode;
            AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
            AV11AuditingObjectRecordItem.gxTpr_Tablename = "Contrato";
            AV11AuditingObjectRecordItem.gxTpr_Mode = AV14ActualMode;
            AV10AuditingObject.gxTpr_Record.Add(AV11AuditingObjectRecordItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_Codigo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "C�digo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = true;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_AreaTrabalhoCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Area de Trabalho";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A75Contrato_AreaTrabalhoCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_AreaTrabalhoDes";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "�rea de Trabalho";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A76Contrato_AreaTrabalhoDes;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_Codigo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contratada";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_PessoaCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "C�d. da Pessoa";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A40Contratada_PessoaCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_PessoaNom";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contratada";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A41Contratada_PessoaNom;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_PessoaCNPJ";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "CNPJ";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A42Contratada_PessoaCNPJ;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_Sigla";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Sigla";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A438Contratada_Sigla;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_TipoFabrica";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tipo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A516Contratada_TipoFabrica;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_Numero";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "N�mero do Contrato";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A77Contrato_Numero;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_NumeroAta";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "N�mero da Ata";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = true;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A78Contrato_NumeroAta;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_Ano";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Ano";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_Objeto";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Objeto do Contrato";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A80Contrato_Objeto;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_UnidadeContratacao";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Unidade";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A115Contrato_UnidadeContratacao), 2, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_Quantidade";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Quantidade";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A81Contrato_Quantidade), 9, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_DataVigenciaInicio";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "D. Vig�ncia Inicio";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.DToC( A82Contrato_DataVigenciaInicio, 2, "/");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_DataVigenciaTermino";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "D. Vig�ncia T�rmino";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.DToC( A83Contrato_DataVigenciaTermino, 2, "/");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_DataInicioTA";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Termo Aditivo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.DToC( A842Contrato_DataInicioTA, 2, "/");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_DataFimTA";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Termo Aditivo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.DToC( A843Contrato_DataFimTA, 2, "/");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_DataPublicacaoDOU";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "D. Publica��o DOU";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.DToC( A84Contrato_DataPublicacaoDOU, 2, "/");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_DataTermino";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Vig�ncia";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.DToC( A1869Contrato_DataTermino, 2, "/");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_DataAssinatura";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "D. da Assinatura";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.DToC( A85Contrato_DataAssinatura, 2, "/");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_DataPedidoReajuste";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "D. Pedido Reajuste";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.DToC( A86Contrato_DataPedidoReajuste, 2, "/");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_DataTerminoAta";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "D. T�rmino da Ata";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.DToC( A87Contrato_DataTerminoAta, 2, "/");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_DataFimAdaptacao";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Fim Adapta��o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.DToC( A88Contrato_DataFimAdaptacao, 2, "/");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_Valor";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Valor do Contrato";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A89Contrato_Valor, 18, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_ValorUnidadeContratacao";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Valor Unidade Contrata��o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A116Contrato_ValorUnidadeContratacao, 18, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_ValorUndCntAtual";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Unidade Contrata��o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A1870Contrato_ValorUndCntAtual, 18, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_RegrasPagto";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Regras para pagamento";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A90Contrato_RegrasPagto;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_DiasPagto";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Dias para Pagto";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A91Contrato_DiasPagto), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_CalculoDivergencia";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "C�lculo da Diverg�ncia:";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A452Contrato_CalculoDivergencia;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_IndiceDivergencia";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Indice de Aceita��o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A453Contrato_IndiceDivergencia, 6, 2);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_AceitaPFFS";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Aceita menor PF da FS";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A1150Contrato_AceitaPFFS);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_Ativo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Ativo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A92Contrato_Ativo);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_PrepostoCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Preposto";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1013Contrato_PrepostoCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_PrepostoPesCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Preposto";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1016Contrato_PrepostoPesCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_PrepostoNom";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Preposto";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1015Contrato_PrepostoNom;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_PrdFtrCada";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Faturamento";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1354Contrato_PrdFtrCada;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_LmtFtr";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Limite de Faturamento";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A2086Contrato_LmtFtr, 14, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_PrdFtrIni";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Inicio";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1357Contrato_PrdFtrIni), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_PrdFtrFim";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Fim";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1358Contrato_PrdFtrFim), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_Identificacao";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contrato ";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A2096Contrato_Identificacao;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
      }

      protected void S121( )
      {
         /* 'LOADNEWVALUES' Routine */
         /* Using cursor P00WD17 */
         pr_default.execute(1, new Object[] {AV16Contrato_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A74Contrato_Codigo = P00WD17_A74Contrato_Codigo[0];
            n74Contrato_Codigo = P00WD17_n74Contrato_Codigo[0];
            A75Contrato_AreaTrabalhoCod = P00WD17_A75Contrato_AreaTrabalhoCod[0];
            A76Contrato_AreaTrabalhoDes = P00WD17_A76Contrato_AreaTrabalhoDes[0];
            n76Contrato_AreaTrabalhoDes = P00WD17_n76Contrato_AreaTrabalhoDes[0];
            A39Contratada_Codigo = P00WD17_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00WD17_A40Contratada_PessoaCod[0];
            A42Contratada_PessoaCNPJ = P00WD17_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00WD17_n42Contratada_PessoaCNPJ[0];
            A438Contratada_Sigla = P00WD17_A438Contratada_Sigla[0];
            A516Contratada_TipoFabrica = P00WD17_A516Contratada_TipoFabrica[0];
            A78Contrato_NumeroAta = P00WD17_A78Contrato_NumeroAta[0];
            n78Contrato_NumeroAta = P00WD17_n78Contrato_NumeroAta[0];
            A79Contrato_Ano = P00WD17_A79Contrato_Ano[0];
            A80Contrato_Objeto = P00WD17_A80Contrato_Objeto[0];
            A115Contrato_UnidadeContratacao = P00WD17_A115Contrato_UnidadeContratacao[0];
            A81Contrato_Quantidade = P00WD17_A81Contrato_Quantidade[0];
            A82Contrato_DataVigenciaInicio = P00WD17_A82Contrato_DataVigenciaInicio[0];
            A84Contrato_DataPublicacaoDOU = P00WD17_A84Contrato_DataPublicacaoDOU[0];
            A85Contrato_DataAssinatura = P00WD17_A85Contrato_DataAssinatura[0];
            A86Contrato_DataPedidoReajuste = P00WD17_A86Contrato_DataPedidoReajuste[0];
            A87Contrato_DataTerminoAta = P00WD17_A87Contrato_DataTerminoAta[0];
            A88Contrato_DataFimAdaptacao = P00WD17_A88Contrato_DataFimAdaptacao[0];
            A89Contrato_Valor = P00WD17_A89Contrato_Valor[0];
            A116Contrato_ValorUnidadeContratacao = P00WD17_A116Contrato_ValorUnidadeContratacao[0];
            A90Contrato_RegrasPagto = P00WD17_A90Contrato_RegrasPagto[0];
            A91Contrato_DiasPagto = P00WD17_A91Contrato_DiasPagto[0];
            A452Contrato_CalculoDivergencia = P00WD17_A452Contrato_CalculoDivergencia[0];
            A453Contrato_IndiceDivergencia = P00WD17_A453Contrato_IndiceDivergencia[0];
            A1150Contrato_AceitaPFFS = P00WD17_A1150Contrato_AceitaPFFS[0];
            n1150Contrato_AceitaPFFS = P00WD17_n1150Contrato_AceitaPFFS[0];
            A1013Contrato_PrepostoCod = P00WD17_A1013Contrato_PrepostoCod[0];
            n1013Contrato_PrepostoCod = P00WD17_n1013Contrato_PrepostoCod[0];
            A1016Contrato_PrepostoPesCod = P00WD17_A1016Contrato_PrepostoPesCod[0];
            n1016Contrato_PrepostoPesCod = P00WD17_n1016Contrato_PrepostoPesCod[0];
            A1015Contrato_PrepostoNom = P00WD17_A1015Contrato_PrepostoNom[0];
            n1015Contrato_PrepostoNom = P00WD17_n1015Contrato_PrepostoNom[0];
            A1354Contrato_PrdFtrCada = P00WD17_A1354Contrato_PrdFtrCada[0];
            n1354Contrato_PrdFtrCada = P00WD17_n1354Contrato_PrdFtrCada[0];
            A2086Contrato_LmtFtr = P00WD17_A2086Contrato_LmtFtr[0];
            n2086Contrato_LmtFtr = P00WD17_n2086Contrato_LmtFtr[0];
            A1357Contrato_PrdFtrIni = P00WD17_A1357Contrato_PrdFtrIni[0];
            n1357Contrato_PrdFtrIni = P00WD17_n1357Contrato_PrdFtrIni[0];
            A1358Contrato_PrdFtrFim = P00WD17_A1358Contrato_PrdFtrFim[0];
            n1358Contrato_PrdFtrFim = P00WD17_n1358Contrato_PrdFtrFim[0];
            A842Contrato_DataInicioTA = P00WD17_A842Contrato_DataInicioTA[0];
            n842Contrato_DataInicioTA = P00WD17_n842Contrato_DataInicioTA[0];
            A1870Contrato_ValorUndCntAtual = P00WD17_A1870Contrato_ValorUndCntAtual[0];
            n1870Contrato_ValorUndCntAtual = P00WD17_n1870Contrato_ValorUndCntAtual[0];
            A41Contratada_PessoaNom = P00WD17_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00WD17_n41Contratada_PessoaNom[0];
            A92Contrato_Ativo = P00WD17_A92Contrato_Ativo[0];
            A77Contrato_Numero = P00WD17_A77Contrato_Numero[0];
            A843Contrato_DataFimTA = P00WD17_A843Contrato_DataFimTA[0];
            n843Contrato_DataFimTA = P00WD17_n843Contrato_DataFimTA[0];
            A83Contrato_DataVigenciaTermino = P00WD17_A83Contrato_DataVigenciaTermino[0];
            A842Contrato_DataInicioTA = P00WD17_A842Contrato_DataInicioTA[0];
            n842Contrato_DataInicioTA = P00WD17_n842Contrato_DataInicioTA[0];
            A1870Contrato_ValorUndCntAtual = P00WD17_A1870Contrato_ValorUndCntAtual[0];
            n1870Contrato_ValorUndCntAtual = P00WD17_n1870Contrato_ValorUndCntAtual[0];
            A843Contrato_DataFimTA = P00WD17_A843Contrato_DataFimTA[0];
            n843Contrato_DataFimTA = P00WD17_n843Contrato_DataFimTA[0];
            A76Contrato_AreaTrabalhoDes = P00WD17_A76Contrato_AreaTrabalhoDes[0];
            n76Contrato_AreaTrabalhoDes = P00WD17_n76Contrato_AreaTrabalhoDes[0];
            A40Contratada_PessoaCod = P00WD17_A40Contratada_PessoaCod[0];
            A438Contratada_Sigla = P00WD17_A438Contratada_Sigla[0];
            A516Contratada_TipoFabrica = P00WD17_A516Contratada_TipoFabrica[0];
            A42Contratada_PessoaCNPJ = P00WD17_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00WD17_n42Contratada_PessoaCNPJ[0];
            A41Contratada_PessoaNom = P00WD17_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00WD17_n41Contratada_PessoaNom[0];
            A1016Contrato_PrepostoPesCod = P00WD17_A1016Contrato_PrepostoPesCod[0];
            n1016Contrato_PrepostoPesCod = P00WD17_n1016Contrato_PrepostoPesCod[0];
            A1015Contrato_PrepostoNom = P00WD17_A1015Contrato_PrepostoNom[0];
            n1015Contrato_PrepostoNom = P00WD17_n1015Contrato_PrepostoNom[0];
            A1869Contrato_DataTermino = ((A83Contrato_DataVigenciaTermino>A843Contrato_DataFimTA) ? A83Contrato_DataVigenciaTermino : A843Contrato_DataFimTA);
            A2096Contrato_Identificacao = StringUtil.Format( "%1 - %2 %3", A77Contrato_Numero, A41Contratada_PessoaNom, (A92Contrato_Ativo ? "(Vigente)" : "(Encerrado)"), "", "", "", "", "", "");
            if ( StringUtil.StrCmp(AV14ActualMode, "INS") == 0 )
            {
               AV10AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
               AV10AuditingObject.gxTpr_Mode = AV14ActualMode;
               AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
               AV11AuditingObjectRecordItem.gxTpr_Tablename = "Contrato";
               AV10AuditingObject.gxTpr_Record.Add(AV11AuditingObjectRecordItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_Codigo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "C�digo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = true;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_AreaTrabalhoCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Area de Trabalho";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A75Contrato_AreaTrabalhoCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_AreaTrabalhoDes";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "�rea de Trabalho";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A76Contrato_AreaTrabalhoDes;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_Codigo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contratada";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_PessoaCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "C�d. da Pessoa";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A40Contratada_PessoaCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_PessoaNom";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contratada";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A41Contratada_PessoaNom;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_PessoaCNPJ";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "CNPJ";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A42Contratada_PessoaCNPJ;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_Sigla";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Sigla";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A438Contratada_Sigla;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_TipoFabrica";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tipo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A516Contratada_TipoFabrica;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_Numero";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "N�mero do Contrato";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A77Contrato_Numero;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_NumeroAta";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "N�mero da Ata";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = true;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A78Contrato_NumeroAta;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_Ano";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Ano";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_Objeto";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Objeto do Contrato";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A80Contrato_Objeto;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_UnidadeContratacao";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Unidade";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A115Contrato_UnidadeContratacao), 2, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_Quantidade";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Quantidade";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A81Contrato_Quantidade), 9, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_DataVigenciaInicio";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "D. Vig�ncia Inicio";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A82Contrato_DataVigenciaInicio, 2, "/");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_DataVigenciaTermino";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "D. Vig�ncia T�rmino";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A83Contrato_DataVigenciaTermino, 2, "/");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_DataInicioTA";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Termo Aditivo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A842Contrato_DataInicioTA, 2, "/");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_DataFimTA";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Termo Aditivo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A843Contrato_DataFimTA, 2, "/");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_DataPublicacaoDOU";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "D. Publica��o DOU";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A84Contrato_DataPublicacaoDOU, 2, "/");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_DataTermino";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Vig�ncia";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A1869Contrato_DataTermino, 2, "/");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_DataAssinatura";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "D. da Assinatura";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A85Contrato_DataAssinatura, 2, "/");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_DataPedidoReajuste";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "D. Pedido Reajuste";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A86Contrato_DataPedidoReajuste, 2, "/");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_DataTerminoAta";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "D. T�rmino da Ata";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A87Contrato_DataTerminoAta, 2, "/");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_DataFimAdaptacao";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Fim Adapta��o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A88Contrato_DataFimAdaptacao, 2, "/");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_Valor";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Valor do Contrato";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A89Contrato_Valor, 18, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_ValorUnidadeContratacao";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Valor Unidade Contrata��o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A116Contrato_ValorUnidadeContratacao, 18, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_ValorUndCntAtual";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Unidade Contrata��o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1870Contrato_ValorUndCntAtual, 18, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_RegrasPagto";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Regras para pagamento";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A90Contrato_RegrasPagto;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_DiasPagto";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Dias para Pagto";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A91Contrato_DiasPagto), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_CalculoDivergencia";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "C�lculo da Diverg�ncia:";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A452Contrato_CalculoDivergencia;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_IndiceDivergencia";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Indice de Aceita��o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A453Contrato_IndiceDivergencia, 6, 2);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_AceitaPFFS";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Aceita menor PF da FS";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1150Contrato_AceitaPFFS);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_Ativo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Ativo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A92Contrato_Ativo);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_PrepostoCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Preposto";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1013Contrato_PrepostoCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_PrepostoPesCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Preposto";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1016Contrato_PrepostoPesCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_PrepostoNom";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Preposto";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1015Contrato_PrepostoNom;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_PrdFtrCada";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Faturamento";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1354Contrato_PrdFtrCada;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_LmtFtr";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Limite de Faturamento";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A2086Contrato_LmtFtr, 14, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_PrdFtrIni";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Inicio";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1357Contrato_PrdFtrIni), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_PrdFtrFim";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Fim";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1358Contrato_PrdFtrFim), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contrato_Identificacao";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contrato ";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A2096Contrato_Identificacao;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            }
            if ( StringUtil.StrCmp(AV14ActualMode, "UPD") == 0 )
            {
               AV21GXV1 = 1;
               while ( AV21GXV1 <= AV10AuditingObject.gxTpr_Record.Count )
               {
                  AV11AuditingObjectRecordItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem)AV10AuditingObject.gxTpr_Record.Item(AV21GXV1));
                  while ( (pr_default.getStatus(1) != 101) && ( P00WD17_A74Contrato_Codigo[0] == A74Contrato_Codigo ) )
                  {
                     A75Contrato_AreaTrabalhoCod = P00WD17_A75Contrato_AreaTrabalhoCod[0];
                     A76Contrato_AreaTrabalhoDes = P00WD17_A76Contrato_AreaTrabalhoDes[0];
                     n76Contrato_AreaTrabalhoDes = P00WD17_n76Contrato_AreaTrabalhoDes[0];
                     A39Contratada_Codigo = P00WD17_A39Contratada_Codigo[0];
                     A40Contratada_PessoaCod = P00WD17_A40Contratada_PessoaCod[0];
                     A42Contratada_PessoaCNPJ = P00WD17_A42Contratada_PessoaCNPJ[0];
                     n42Contratada_PessoaCNPJ = P00WD17_n42Contratada_PessoaCNPJ[0];
                     A438Contratada_Sigla = P00WD17_A438Contratada_Sigla[0];
                     A516Contratada_TipoFabrica = P00WD17_A516Contratada_TipoFabrica[0];
                     A78Contrato_NumeroAta = P00WD17_A78Contrato_NumeroAta[0];
                     n78Contrato_NumeroAta = P00WD17_n78Contrato_NumeroAta[0];
                     A79Contrato_Ano = P00WD17_A79Contrato_Ano[0];
                     A80Contrato_Objeto = P00WD17_A80Contrato_Objeto[0];
                     A115Contrato_UnidadeContratacao = P00WD17_A115Contrato_UnidadeContratacao[0];
                     A81Contrato_Quantidade = P00WD17_A81Contrato_Quantidade[0];
                     A82Contrato_DataVigenciaInicio = P00WD17_A82Contrato_DataVigenciaInicio[0];
                     A84Contrato_DataPublicacaoDOU = P00WD17_A84Contrato_DataPublicacaoDOU[0];
                     A85Contrato_DataAssinatura = P00WD17_A85Contrato_DataAssinatura[0];
                     A86Contrato_DataPedidoReajuste = P00WD17_A86Contrato_DataPedidoReajuste[0];
                     A87Contrato_DataTerminoAta = P00WD17_A87Contrato_DataTerminoAta[0];
                     A88Contrato_DataFimAdaptacao = P00WD17_A88Contrato_DataFimAdaptacao[0];
                     A89Contrato_Valor = P00WD17_A89Contrato_Valor[0];
                     A116Contrato_ValorUnidadeContratacao = P00WD17_A116Contrato_ValorUnidadeContratacao[0];
                     A90Contrato_RegrasPagto = P00WD17_A90Contrato_RegrasPagto[0];
                     A91Contrato_DiasPagto = P00WD17_A91Contrato_DiasPagto[0];
                     A452Contrato_CalculoDivergencia = P00WD17_A452Contrato_CalculoDivergencia[0];
                     A453Contrato_IndiceDivergencia = P00WD17_A453Contrato_IndiceDivergencia[0];
                     A1150Contrato_AceitaPFFS = P00WD17_A1150Contrato_AceitaPFFS[0];
                     n1150Contrato_AceitaPFFS = P00WD17_n1150Contrato_AceitaPFFS[0];
                     A1013Contrato_PrepostoCod = P00WD17_A1013Contrato_PrepostoCod[0];
                     n1013Contrato_PrepostoCod = P00WD17_n1013Contrato_PrepostoCod[0];
                     A1016Contrato_PrepostoPesCod = P00WD17_A1016Contrato_PrepostoPesCod[0];
                     n1016Contrato_PrepostoPesCod = P00WD17_n1016Contrato_PrepostoPesCod[0];
                     A1015Contrato_PrepostoNom = P00WD17_A1015Contrato_PrepostoNom[0];
                     n1015Contrato_PrepostoNom = P00WD17_n1015Contrato_PrepostoNom[0];
                     A1354Contrato_PrdFtrCada = P00WD17_A1354Contrato_PrdFtrCada[0];
                     n1354Contrato_PrdFtrCada = P00WD17_n1354Contrato_PrdFtrCada[0];
                     A2086Contrato_LmtFtr = P00WD17_A2086Contrato_LmtFtr[0];
                     n2086Contrato_LmtFtr = P00WD17_n2086Contrato_LmtFtr[0];
                     A1357Contrato_PrdFtrIni = P00WD17_A1357Contrato_PrdFtrIni[0];
                     n1357Contrato_PrdFtrIni = P00WD17_n1357Contrato_PrdFtrIni[0];
                     A1358Contrato_PrdFtrFim = P00WD17_A1358Contrato_PrdFtrFim[0];
                     n1358Contrato_PrdFtrFim = P00WD17_n1358Contrato_PrdFtrFim[0];
                     A1870Contrato_ValorUndCntAtual = P00WD17_A1870Contrato_ValorUndCntAtual[0];
                     n1870Contrato_ValorUndCntAtual = P00WD17_n1870Contrato_ValorUndCntAtual[0];
                     A41Contratada_PessoaNom = P00WD17_A41Contratada_PessoaNom[0];
                     n41Contratada_PessoaNom = P00WD17_n41Contratada_PessoaNom[0];
                     A92Contrato_Ativo = P00WD17_A92Contrato_Ativo[0];
                     A77Contrato_Numero = P00WD17_A77Contrato_Numero[0];
                     A83Contrato_DataVigenciaTermino = P00WD17_A83Contrato_DataVigenciaTermino[0];
                     A1870Contrato_ValorUndCntAtual = P00WD17_A1870Contrato_ValorUndCntAtual[0];
                     n1870Contrato_ValorUndCntAtual = P00WD17_n1870Contrato_ValorUndCntAtual[0];
                     A76Contrato_AreaTrabalhoDes = P00WD17_A76Contrato_AreaTrabalhoDes[0];
                     n76Contrato_AreaTrabalhoDes = P00WD17_n76Contrato_AreaTrabalhoDes[0];
                     A40Contratada_PessoaCod = P00WD17_A40Contratada_PessoaCod[0];
                     A438Contratada_Sigla = P00WD17_A438Contratada_Sigla[0];
                     A516Contratada_TipoFabrica = P00WD17_A516Contratada_TipoFabrica[0];
                     A42Contratada_PessoaCNPJ = P00WD17_A42Contratada_PessoaCNPJ[0];
                     n42Contratada_PessoaCNPJ = P00WD17_n42Contratada_PessoaCNPJ[0];
                     A41Contratada_PessoaNom = P00WD17_A41Contratada_PessoaNom[0];
                     n41Contratada_PessoaNom = P00WD17_n41Contratada_PessoaNom[0];
                     A1016Contrato_PrepostoPesCod = P00WD17_A1016Contrato_PrepostoPesCod[0];
                     n1016Contrato_PrepostoPesCod = P00WD17_n1016Contrato_PrepostoPesCod[0];
                     A1015Contrato_PrepostoNom = P00WD17_A1015Contrato_PrepostoNom[0];
                     n1015Contrato_PrepostoNom = P00WD17_n1015Contrato_PrepostoNom[0];
                     /* Using cursor P00WD20 */
                     pr_default.execute(2, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
                     if ( (pr_default.getStatus(2) != 101) )
                     {
                        A842Contrato_DataInicioTA = P00WD20_A842Contrato_DataInicioTA[0];
                        n842Contrato_DataInicioTA = P00WD20_n842Contrato_DataInicioTA[0];
                     }
                     else
                     {
                        A842Contrato_DataInicioTA = DateTime.MinValue;
                        n842Contrato_DataInicioTA = false;
                     }
                     pr_default.close(2);
                     /* Using cursor P00WD23 */
                     pr_default.execute(3, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
                     if ( (pr_default.getStatus(3) != 101) )
                     {
                        A843Contrato_DataFimTA = P00WD23_A843Contrato_DataFimTA[0];
                        n843Contrato_DataFimTA = P00WD23_n843Contrato_DataFimTA[0];
                     }
                     else
                     {
                        A843Contrato_DataFimTA = DateTime.MinValue;
                        n843Contrato_DataFimTA = false;
                     }
                     pr_default.close(3);
                     A1869Contrato_DataTermino = ((A83Contrato_DataVigenciaTermino>A843Contrato_DataFimTA) ? A83Contrato_DataVigenciaTermino : A843Contrato_DataFimTA);
                     A2096Contrato_Identificacao = StringUtil.Format( "%1 - %2 %3", A77Contrato_Numero, A41Contratada_PessoaNom, (A92Contrato_Ativo ? "(Vigente)" : "(Encerrado)"), "", "", "", "", "", "");
                     AV23GXV2 = 1;
                     while ( AV23GXV2 <= AV11AuditingObjectRecordItem.gxTpr_Attribute.Count )
                     {
                        AV12AuditingObjectRecordItemAttributeItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem)AV11AuditingObjectRecordItem.gxTpr_Attribute.Item(AV23GXV2));
                        if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_Codigo") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_AreaTrabalhoCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A75Contrato_AreaTrabalhoCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_AreaTrabalhoDes") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A76Contrato_AreaTrabalhoDes;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratada_Codigo") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratada_PessoaCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A40Contratada_PessoaCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratada_PessoaNom") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A41Contratada_PessoaNom;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratada_PessoaCNPJ") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A42Contratada_PessoaCNPJ;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratada_Sigla") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A438Contratada_Sigla;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratada_TipoFabrica") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A516Contratada_TipoFabrica;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_Numero") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A77Contrato_Numero;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_NumeroAta") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A78Contrato_NumeroAta;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_Ano") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_Objeto") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A80Contrato_Objeto;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_UnidadeContratacao") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A115Contrato_UnidadeContratacao), 2, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_Quantidade") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A81Contrato_Quantidade), 9, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_DataVigenciaInicio") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A82Contrato_DataVigenciaInicio, 2, "/");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_DataVigenciaTermino") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A83Contrato_DataVigenciaTermino, 2, "/");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_DataInicioTA") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A842Contrato_DataInicioTA, 2, "/");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_DataFimTA") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A843Contrato_DataFimTA, 2, "/");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_DataPublicacaoDOU") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A84Contrato_DataPublicacaoDOU, 2, "/");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_DataTermino") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A1869Contrato_DataTermino, 2, "/");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_DataAssinatura") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A85Contrato_DataAssinatura, 2, "/");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_DataPedidoReajuste") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A86Contrato_DataPedidoReajuste, 2, "/");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_DataTerminoAta") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A87Contrato_DataTerminoAta, 2, "/");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_DataFimAdaptacao") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A88Contrato_DataFimAdaptacao, 2, "/");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_Valor") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A89Contrato_Valor, 18, 5);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_ValorUnidadeContratacao") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A116Contrato_ValorUnidadeContratacao, 18, 5);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_ValorUndCntAtual") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1870Contrato_ValorUndCntAtual, 18, 5);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_RegrasPagto") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A90Contrato_RegrasPagto;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_DiasPagto") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A91Contrato_DiasPagto), 4, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_CalculoDivergencia") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A452Contrato_CalculoDivergencia;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_IndiceDivergencia") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A453Contrato_IndiceDivergencia, 6, 2);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_AceitaPFFS") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1150Contrato_AceitaPFFS);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_Ativo") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A92Contrato_Ativo);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_PrepostoCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1013Contrato_PrepostoCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_PrepostoPesCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1016Contrato_PrepostoPesCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_PrepostoNom") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1015Contrato_PrepostoNom;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_PrdFtrCada") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1354Contrato_PrdFtrCada;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_LmtFtr") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A2086Contrato_LmtFtr, 14, 5);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_PrdFtrIni") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1357Contrato_PrdFtrIni), 4, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_PrdFtrFim") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1358Contrato_PrdFtrFim), 4, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contrato_Identificacao") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A2096Contrato_Identificacao;
                        }
                        AV23GXV2 = (int)(AV23GXV2+1);
                     }
                     /* Exiting from a For First loop. */
                     if (true) break;
                  }
                  AV21GXV1 = (int)(AV21GXV1+1);
               }
            }
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00WD9_A74Contrato_Codigo = new int[1] ;
         P00WD9_n74Contrato_Codigo = new bool[] {false} ;
         P00WD9_A75Contrato_AreaTrabalhoCod = new int[1] ;
         P00WD9_A76Contrato_AreaTrabalhoDes = new String[] {""} ;
         P00WD9_n76Contrato_AreaTrabalhoDes = new bool[] {false} ;
         P00WD9_A39Contratada_Codigo = new int[1] ;
         P00WD9_A40Contratada_PessoaCod = new int[1] ;
         P00WD9_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P00WD9_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P00WD9_A438Contratada_Sigla = new String[] {""} ;
         P00WD9_A516Contratada_TipoFabrica = new String[] {""} ;
         P00WD9_A78Contrato_NumeroAta = new String[] {""} ;
         P00WD9_n78Contrato_NumeroAta = new bool[] {false} ;
         P00WD9_A79Contrato_Ano = new short[1] ;
         P00WD9_A80Contrato_Objeto = new String[] {""} ;
         P00WD9_A115Contrato_UnidadeContratacao = new short[1] ;
         P00WD9_A81Contrato_Quantidade = new int[1] ;
         P00WD9_A82Contrato_DataVigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         P00WD9_A84Contrato_DataPublicacaoDOU = new DateTime[] {DateTime.MinValue} ;
         P00WD9_A85Contrato_DataAssinatura = new DateTime[] {DateTime.MinValue} ;
         P00WD9_A86Contrato_DataPedidoReajuste = new DateTime[] {DateTime.MinValue} ;
         P00WD9_A87Contrato_DataTerminoAta = new DateTime[] {DateTime.MinValue} ;
         P00WD9_A88Contrato_DataFimAdaptacao = new DateTime[] {DateTime.MinValue} ;
         P00WD9_A89Contrato_Valor = new decimal[1] ;
         P00WD9_A116Contrato_ValorUnidadeContratacao = new decimal[1] ;
         P00WD9_A90Contrato_RegrasPagto = new String[] {""} ;
         P00WD9_A91Contrato_DiasPagto = new short[1] ;
         P00WD9_A452Contrato_CalculoDivergencia = new String[] {""} ;
         P00WD9_A453Contrato_IndiceDivergencia = new decimal[1] ;
         P00WD9_A1150Contrato_AceitaPFFS = new bool[] {false} ;
         P00WD9_n1150Contrato_AceitaPFFS = new bool[] {false} ;
         P00WD9_A1013Contrato_PrepostoCod = new int[1] ;
         P00WD9_n1013Contrato_PrepostoCod = new bool[] {false} ;
         P00WD9_A1016Contrato_PrepostoPesCod = new int[1] ;
         P00WD9_n1016Contrato_PrepostoPesCod = new bool[] {false} ;
         P00WD9_A1015Contrato_PrepostoNom = new String[] {""} ;
         P00WD9_n1015Contrato_PrepostoNom = new bool[] {false} ;
         P00WD9_A1354Contrato_PrdFtrCada = new String[] {""} ;
         P00WD9_n1354Contrato_PrdFtrCada = new bool[] {false} ;
         P00WD9_A2086Contrato_LmtFtr = new decimal[1] ;
         P00WD9_n2086Contrato_LmtFtr = new bool[] {false} ;
         P00WD9_A1357Contrato_PrdFtrIni = new short[1] ;
         P00WD9_n1357Contrato_PrdFtrIni = new bool[] {false} ;
         P00WD9_A1358Contrato_PrdFtrFim = new short[1] ;
         P00WD9_n1358Contrato_PrdFtrFim = new bool[] {false} ;
         P00WD9_A842Contrato_DataInicioTA = new DateTime[] {DateTime.MinValue} ;
         P00WD9_n842Contrato_DataInicioTA = new bool[] {false} ;
         P00WD9_A1870Contrato_ValorUndCntAtual = new decimal[1] ;
         P00WD9_n1870Contrato_ValorUndCntAtual = new bool[] {false} ;
         P00WD9_A41Contratada_PessoaNom = new String[] {""} ;
         P00WD9_n41Contratada_PessoaNom = new bool[] {false} ;
         P00WD9_A92Contrato_Ativo = new bool[] {false} ;
         P00WD9_A77Contrato_Numero = new String[] {""} ;
         P00WD9_A843Contrato_DataFimTA = new DateTime[] {DateTime.MinValue} ;
         P00WD9_n843Contrato_DataFimTA = new bool[] {false} ;
         P00WD9_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         A76Contrato_AreaTrabalhoDes = "";
         A42Contratada_PessoaCNPJ = "";
         A438Contratada_Sigla = "";
         A516Contratada_TipoFabrica = "";
         A78Contrato_NumeroAta = "";
         A80Contrato_Objeto = "";
         A82Contrato_DataVigenciaInicio = DateTime.MinValue;
         A84Contrato_DataPublicacaoDOU = DateTime.MinValue;
         A85Contrato_DataAssinatura = DateTime.MinValue;
         A86Contrato_DataPedidoReajuste = DateTime.MinValue;
         A87Contrato_DataTerminoAta = DateTime.MinValue;
         A88Contrato_DataFimAdaptacao = DateTime.MinValue;
         A90Contrato_RegrasPagto = "";
         A452Contrato_CalculoDivergencia = "";
         A1015Contrato_PrepostoNom = "";
         A1354Contrato_PrdFtrCada = "";
         A842Contrato_DataInicioTA = DateTime.MinValue;
         A41Contratada_PessoaNom = "";
         A77Contrato_Numero = "";
         A843Contrato_DataFimTA = DateTime.MinValue;
         A83Contrato_DataVigenciaTermino = DateTime.MinValue;
         A1869Contrato_DataTermino = DateTime.MinValue;
         A2096Contrato_Identificacao = "";
         AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
         AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
         P00WD17_A74Contrato_Codigo = new int[1] ;
         P00WD17_n74Contrato_Codigo = new bool[] {false} ;
         P00WD17_A75Contrato_AreaTrabalhoCod = new int[1] ;
         P00WD17_A76Contrato_AreaTrabalhoDes = new String[] {""} ;
         P00WD17_n76Contrato_AreaTrabalhoDes = new bool[] {false} ;
         P00WD17_A39Contratada_Codigo = new int[1] ;
         P00WD17_A40Contratada_PessoaCod = new int[1] ;
         P00WD17_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P00WD17_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P00WD17_A438Contratada_Sigla = new String[] {""} ;
         P00WD17_A516Contratada_TipoFabrica = new String[] {""} ;
         P00WD17_A78Contrato_NumeroAta = new String[] {""} ;
         P00WD17_n78Contrato_NumeroAta = new bool[] {false} ;
         P00WD17_A79Contrato_Ano = new short[1] ;
         P00WD17_A80Contrato_Objeto = new String[] {""} ;
         P00WD17_A115Contrato_UnidadeContratacao = new short[1] ;
         P00WD17_A81Contrato_Quantidade = new int[1] ;
         P00WD17_A82Contrato_DataVigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         P00WD17_A84Contrato_DataPublicacaoDOU = new DateTime[] {DateTime.MinValue} ;
         P00WD17_A85Contrato_DataAssinatura = new DateTime[] {DateTime.MinValue} ;
         P00WD17_A86Contrato_DataPedidoReajuste = new DateTime[] {DateTime.MinValue} ;
         P00WD17_A87Contrato_DataTerminoAta = new DateTime[] {DateTime.MinValue} ;
         P00WD17_A88Contrato_DataFimAdaptacao = new DateTime[] {DateTime.MinValue} ;
         P00WD17_A89Contrato_Valor = new decimal[1] ;
         P00WD17_A116Contrato_ValorUnidadeContratacao = new decimal[1] ;
         P00WD17_A90Contrato_RegrasPagto = new String[] {""} ;
         P00WD17_A91Contrato_DiasPagto = new short[1] ;
         P00WD17_A452Contrato_CalculoDivergencia = new String[] {""} ;
         P00WD17_A453Contrato_IndiceDivergencia = new decimal[1] ;
         P00WD17_A1150Contrato_AceitaPFFS = new bool[] {false} ;
         P00WD17_n1150Contrato_AceitaPFFS = new bool[] {false} ;
         P00WD17_A1013Contrato_PrepostoCod = new int[1] ;
         P00WD17_n1013Contrato_PrepostoCod = new bool[] {false} ;
         P00WD17_A1016Contrato_PrepostoPesCod = new int[1] ;
         P00WD17_n1016Contrato_PrepostoPesCod = new bool[] {false} ;
         P00WD17_A1015Contrato_PrepostoNom = new String[] {""} ;
         P00WD17_n1015Contrato_PrepostoNom = new bool[] {false} ;
         P00WD17_A1354Contrato_PrdFtrCada = new String[] {""} ;
         P00WD17_n1354Contrato_PrdFtrCada = new bool[] {false} ;
         P00WD17_A2086Contrato_LmtFtr = new decimal[1] ;
         P00WD17_n2086Contrato_LmtFtr = new bool[] {false} ;
         P00WD17_A1357Contrato_PrdFtrIni = new short[1] ;
         P00WD17_n1357Contrato_PrdFtrIni = new bool[] {false} ;
         P00WD17_A1358Contrato_PrdFtrFim = new short[1] ;
         P00WD17_n1358Contrato_PrdFtrFim = new bool[] {false} ;
         P00WD17_A842Contrato_DataInicioTA = new DateTime[] {DateTime.MinValue} ;
         P00WD17_n842Contrato_DataInicioTA = new bool[] {false} ;
         P00WD17_A1870Contrato_ValorUndCntAtual = new decimal[1] ;
         P00WD17_n1870Contrato_ValorUndCntAtual = new bool[] {false} ;
         P00WD17_A41Contratada_PessoaNom = new String[] {""} ;
         P00WD17_n41Contratada_PessoaNom = new bool[] {false} ;
         P00WD17_A92Contrato_Ativo = new bool[] {false} ;
         P00WD17_A77Contrato_Numero = new String[] {""} ;
         P00WD17_A843Contrato_DataFimTA = new DateTime[] {DateTime.MinValue} ;
         P00WD17_n843Contrato_DataFimTA = new bool[] {false} ;
         P00WD17_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         P00WD20_A842Contrato_DataInicioTA = new DateTime[] {DateTime.MinValue} ;
         P00WD20_n842Contrato_DataInicioTA = new bool[] {false} ;
         P00WD23_A843Contrato_DataFimTA = new DateTime[] {DateTime.MinValue} ;
         P00WD23_n843Contrato_DataFimTA = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.loadauditcontrato__default(),
            new Object[][] {
                new Object[] {
               P00WD9_A74Contrato_Codigo, P00WD9_A75Contrato_AreaTrabalhoCod, P00WD9_A76Contrato_AreaTrabalhoDes, P00WD9_n76Contrato_AreaTrabalhoDes, P00WD9_A39Contratada_Codigo, P00WD9_A40Contratada_PessoaCod, P00WD9_A42Contratada_PessoaCNPJ, P00WD9_n42Contratada_PessoaCNPJ, P00WD9_A438Contratada_Sigla, P00WD9_A516Contratada_TipoFabrica,
               P00WD9_A78Contrato_NumeroAta, P00WD9_n78Contrato_NumeroAta, P00WD9_A79Contrato_Ano, P00WD9_A80Contrato_Objeto, P00WD9_A115Contrato_UnidadeContratacao, P00WD9_A81Contrato_Quantidade, P00WD9_A82Contrato_DataVigenciaInicio, P00WD9_A84Contrato_DataPublicacaoDOU, P00WD9_A85Contrato_DataAssinatura, P00WD9_A86Contrato_DataPedidoReajuste,
               P00WD9_A87Contrato_DataTerminoAta, P00WD9_A88Contrato_DataFimAdaptacao, P00WD9_A89Contrato_Valor, P00WD9_A116Contrato_ValorUnidadeContratacao, P00WD9_A90Contrato_RegrasPagto, P00WD9_A91Contrato_DiasPagto, P00WD9_A452Contrato_CalculoDivergencia, P00WD9_A453Contrato_IndiceDivergencia, P00WD9_A1150Contrato_AceitaPFFS, P00WD9_n1150Contrato_AceitaPFFS,
               P00WD9_A1013Contrato_PrepostoCod, P00WD9_n1013Contrato_PrepostoCod, P00WD9_A1016Contrato_PrepostoPesCod, P00WD9_n1016Contrato_PrepostoPesCod, P00WD9_A1015Contrato_PrepostoNom, P00WD9_n1015Contrato_PrepostoNom, P00WD9_A1354Contrato_PrdFtrCada, P00WD9_n1354Contrato_PrdFtrCada, P00WD9_A2086Contrato_LmtFtr, P00WD9_n2086Contrato_LmtFtr,
               P00WD9_A1357Contrato_PrdFtrIni, P00WD9_n1357Contrato_PrdFtrIni, P00WD9_A1358Contrato_PrdFtrFim, P00WD9_n1358Contrato_PrdFtrFim, P00WD9_A842Contrato_DataInicioTA, P00WD9_n842Contrato_DataInicioTA, P00WD9_A1870Contrato_ValorUndCntAtual, P00WD9_n1870Contrato_ValorUndCntAtual, P00WD9_A41Contratada_PessoaNom, P00WD9_n41Contratada_PessoaNom,
               P00WD9_A92Contrato_Ativo, P00WD9_A77Contrato_Numero, P00WD9_A843Contrato_DataFimTA, P00WD9_n843Contrato_DataFimTA, P00WD9_A83Contrato_DataVigenciaTermino
               }
               , new Object[] {
               P00WD17_A74Contrato_Codigo, P00WD17_A75Contrato_AreaTrabalhoCod, P00WD17_A76Contrato_AreaTrabalhoDes, P00WD17_n76Contrato_AreaTrabalhoDes, P00WD17_A39Contratada_Codigo, P00WD17_A40Contratada_PessoaCod, P00WD17_A42Contratada_PessoaCNPJ, P00WD17_n42Contratada_PessoaCNPJ, P00WD17_A438Contratada_Sigla, P00WD17_A516Contratada_TipoFabrica,
               P00WD17_A78Contrato_NumeroAta, P00WD17_n78Contrato_NumeroAta, P00WD17_A79Contrato_Ano, P00WD17_A80Contrato_Objeto, P00WD17_A115Contrato_UnidadeContratacao, P00WD17_A81Contrato_Quantidade, P00WD17_A82Contrato_DataVigenciaInicio, P00WD17_A84Contrato_DataPublicacaoDOU, P00WD17_A85Contrato_DataAssinatura, P00WD17_A86Contrato_DataPedidoReajuste,
               P00WD17_A87Contrato_DataTerminoAta, P00WD17_A88Contrato_DataFimAdaptacao, P00WD17_A89Contrato_Valor, P00WD17_A116Contrato_ValorUnidadeContratacao, P00WD17_A90Contrato_RegrasPagto, P00WD17_A91Contrato_DiasPagto, P00WD17_A452Contrato_CalculoDivergencia, P00WD17_A453Contrato_IndiceDivergencia, P00WD17_A1150Contrato_AceitaPFFS, P00WD17_n1150Contrato_AceitaPFFS,
               P00WD17_A1013Contrato_PrepostoCod, P00WD17_n1013Contrato_PrepostoCod, P00WD17_A1016Contrato_PrepostoPesCod, P00WD17_n1016Contrato_PrepostoPesCod, P00WD17_A1015Contrato_PrepostoNom, P00WD17_n1015Contrato_PrepostoNom, P00WD17_A1354Contrato_PrdFtrCada, P00WD17_n1354Contrato_PrdFtrCada, P00WD17_A2086Contrato_LmtFtr, P00WD17_n2086Contrato_LmtFtr,
               P00WD17_A1357Contrato_PrdFtrIni, P00WD17_n1357Contrato_PrdFtrIni, P00WD17_A1358Contrato_PrdFtrFim, P00WD17_n1358Contrato_PrdFtrFim, P00WD17_A842Contrato_DataInicioTA, P00WD17_n842Contrato_DataInicioTA, P00WD17_A1870Contrato_ValorUndCntAtual, P00WD17_n1870Contrato_ValorUndCntAtual, P00WD17_A41Contratada_PessoaNom, P00WD17_n41Contratada_PessoaNom,
               P00WD17_A92Contrato_Ativo, P00WD17_A77Contrato_Numero, P00WD17_A843Contrato_DataFimTA, P00WD17_n843Contrato_DataFimTA, P00WD17_A83Contrato_DataVigenciaTermino
               }
               , new Object[] {
               P00WD20_A842Contrato_DataInicioTA, P00WD20_n842Contrato_DataInicioTA
               }
               , new Object[] {
               P00WD23_A843Contrato_DataFimTA, P00WD23_n843Contrato_DataFimTA
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A79Contrato_Ano ;
      private short A115Contrato_UnidadeContratacao ;
      private short A91Contrato_DiasPagto ;
      private short A1357Contrato_PrdFtrIni ;
      private short A1358Contrato_PrdFtrFim ;
      private int AV16Contrato_Codigo ;
      private int A74Contrato_Codigo ;
      private int A75Contrato_AreaTrabalhoCod ;
      private int A39Contratada_Codigo ;
      private int A40Contratada_PessoaCod ;
      private int A81Contrato_Quantidade ;
      private int A1013Contrato_PrepostoCod ;
      private int A1016Contrato_PrepostoPesCod ;
      private int AV21GXV1 ;
      private int AV23GXV2 ;
      private decimal A89Contrato_Valor ;
      private decimal A116Contrato_ValorUnidadeContratacao ;
      private decimal A453Contrato_IndiceDivergencia ;
      private decimal A2086Contrato_LmtFtr ;
      private decimal A1870Contrato_ValorUndCntAtual ;
      private String AV13SaveOldValues ;
      private String AV14ActualMode ;
      private String scmdbuf ;
      private String A438Contratada_Sigla ;
      private String A516Contratada_TipoFabrica ;
      private String A78Contrato_NumeroAta ;
      private String A452Contrato_CalculoDivergencia ;
      private String A1015Contrato_PrepostoNom ;
      private String A1354Contrato_PrdFtrCada ;
      private String A41Contratada_PessoaNom ;
      private String A77Contrato_Numero ;
      private DateTime A82Contrato_DataVigenciaInicio ;
      private DateTime A84Contrato_DataPublicacaoDOU ;
      private DateTime A85Contrato_DataAssinatura ;
      private DateTime A86Contrato_DataPedidoReajuste ;
      private DateTime A87Contrato_DataTerminoAta ;
      private DateTime A88Contrato_DataFimAdaptacao ;
      private DateTime A842Contrato_DataInicioTA ;
      private DateTime A843Contrato_DataFimTA ;
      private DateTime A83Contrato_DataVigenciaTermino ;
      private DateTime A1869Contrato_DataTermino ;
      private bool returnInSub ;
      private bool n74Contrato_Codigo ;
      private bool n76Contrato_AreaTrabalhoDes ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool n78Contrato_NumeroAta ;
      private bool A1150Contrato_AceitaPFFS ;
      private bool n1150Contrato_AceitaPFFS ;
      private bool n1013Contrato_PrepostoCod ;
      private bool n1016Contrato_PrepostoPesCod ;
      private bool n1015Contrato_PrepostoNom ;
      private bool n1354Contrato_PrdFtrCada ;
      private bool n2086Contrato_LmtFtr ;
      private bool n1357Contrato_PrdFtrIni ;
      private bool n1358Contrato_PrdFtrFim ;
      private bool n842Contrato_DataInicioTA ;
      private bool n1870Contrato_ValorUndCntAtual ;
      private bool n41Contratada_PessoaNom ;
      private bool A92Contrato_Ativo ;
      private bool n843Contrato_DataFimTA ;
      private String A80Contrato_Objeto ;
      private String A90Contrato_RegrasPagto ;
      private String A76Contrato_AreaTrabalhoDes ;
      private String A42Contratada_PessoaCNPJ ;
      private String A2096Contrato_Identificacao ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private wwpbaseobjects.SdtAuditingObject aP1_AuditingObject ;
      private IDataStoreProvider pr_default ;
      private int[] P00WD9_A74Contrato_Codigo ;
      private bool[] P00WD9_n74Contrato_Codigo ;
      private int[] P00WD9_A75Contrato_AreaTrabalhoCod ;
      private String[] P00WD9_A76Contrato_AreaTrabalhoDes ;
      private bool[] P00WD9_n76Contrato_AreaTrabalhoDes ;
      private int[] P00WD9_A39Contratada_Codigo ;
      private int[] P00WD9_A40Contratada_PessoaCod ;
      private String[] P00WD9_A42Contratada_PessoaCNPJ ;
      private bool[] P00WD9_n42Contratada_PessoaCNPJ ;
      private String[] P00WD9_A438Contratada_Sigla ;
      private String[] P00WD9_A516Contratada_TipoFabrica ;
      private String[] P00WD9_A78Contrato_NumeroAta ;
      private bool[] P00WD9_n78Contrato_NumeroAta ;
      private short[] P00WD9_A79Contrato_Ano ;
      private String[] P00WD9_A80Contrato_Objeto ;
      private short[] P00WD9_A115Contrato_UnidadeContratacao ;
      private int[] P00WD9_A81Contrato_Quantidade ;
      private DateTime[] P00WD9_A82Contrato_DataVigenciaInicio ;
      private DateTime[] P00WD9_A84Contrato_DataPublicacaoDOU ;
      private DateTime[] P00WD9_A85Contrato_DataAssinatura ;
      private DateTime[] P00WD9_A86Contrato_DataPedidoReajuste ;
      private DateTime[] P00WD9_A87Contrato_DataTerminoAta ;
      private DateTime[] P00WD9_A88Contrato_DataFimAdaptacao ;
      private decimal[] P00WD9_A89Contrato_Valor ;
      private decimal[] P00WD9_A116Contrato_ValorUnidadeContratacao ;
      private String[] P00WD9_A90Contrato_RegrasPagto ;
      private short[] P00WD9_A91Contrato_DiasPagto ;
      private String[] P00WD9_A452Contrato_CalculoDivergencia ;
      private decimal[] P00WD9_A453Contrato_IndiceDivergencia ;
      private bool[] P00WD9_A1150Contrato_AceitaPFFS ;
      private bool[] P00WD9_n1150Contrato_AceitaPFFS ;
      private int[] P00WD9_A1013Contrato_PrepostoCod ;
      private bool[] P00WD9_n1013Contrato_PrepostoCod ;
      private int[] P00WD9_A1016Contrato_PrepostoPesCod ;
      private bool[] P00WD9_n1016Contrato_PrepostoPesCod ;
      private String[] P00WD9_A1015Contrato_PrepostoNom ;
      private bool[] P00WD9_n1015Contrato_PrepostoNom ;
      private String[] P00WD9_A1354Contrato_PrdFtrCada ;
      private bool[] P00WD9_n1354Contrato_PrdFtrCada ;
      private decimal[] P00WD9_A2086Contrato_LmtFtr ;
      private bool[] P00WD9_n2086Contrato_LmtFtr ;
      private short[] P00WD9_A1357Contrato_PrdFtrIni ;
      private bool[] P00WD9_n1357Contrato_PrdFtrIni ;
      private short[] P00WD9_A1358Contrato_PrdFtrFim ;
      private bool[] P00WD9_n1358Contrato_PrdFtrFim ;
      private DateTime[] P00WD9_A842Contrato_DataInicioTA ;
      private bool[] P00WD9_n842Contrato_DataInicioTA ;
      private decimal[] P00WD9_A1870Contrato_ValorUndCntAtual ;
      private bool[] P00WD9_n1870Contrato_ValorUndCntAtual ;
      private String[] P00WD9_A41Contratada_PessoaNom ;
      private bool[] P00WD9_n41Contratada_PessoaNom ;
      private bool[] P00WD9_A92Contrato_Ativo ;
      private String[] P00WD9_A77Contrato_Numero ;
      private DateTime[] P00WD9_A843Contrato_DataFimTA ;
      private bool[] P00WD9_n843Contrato_DataFimTA ;
      private DateTime[] P00WD9_A83Contrato_DataVigenciaTermino ;
      private int[] P00WD17_A74Contrato_Codigo ;
      private bool[] P00WD17_n74Contrato_Codigo ;
      private int[] P00WD17_A75Contrato_AreaTrabalhoCod ;
      private String[] P00WD17_A76Contrato_AreaTrabalhoDes ;
      private bool[] P00WD17_n76Contrato_AreaTrabalhoDes ;
      private int[] P00WD17_A39Contratada_Codigo ;
      private int[] P00WD17_A40Contratada_PessoaCod ;
      private String[] P00WD17_A42Contratada_PessoaCNPJ ;
      private bool[] P00WD17_n42Contratada_PessoaCNPJ ;
      private String[] P00WD17_A438Contratada_Sigla ;
      private String[] P00WD17_A516Contratada_TipoFabrica ;
      private String[] P00WD17_A78Contrato_NumeroAta ;
      private bool[] P00WD17_n78Contrato_NumeroAta ;
      private short[] P00WD17_A79Contrato_Ano ;
      private String[] P00WD17_A80Contrato_Objeto ;
      private short[] P00WD17_A115Contrato_UnidadeContratacao ;
      private int[] P00WD17_A81Contrato_Quantidade ;
      private DateTime[] P00WD17_A82Contrato_DataVigenciaInicio ;
      private DateTime[] P00WD17_A84Contrato_DataPublicacaoDOU ;
      private DateTime[] P00WD17_A85Contrato_DataAssinatura ;
      private DateTime[] P00WD17_A86Contrato_DataPedidoReajuste ;
      private DateTime[] P00WD17_A87Contrato_DataTerminoAta ;
      private DateTime[] P00WD17_A88Contrato_DataFimAdaptacao ;
      private decimal[] P00WD17_A89Contrato_Valor ;
      private decimal[] P00WD17_A116Contrato_ValorUnidadeContratacao ;
      private String[] P00WD17_A90Contrato_RegrasPagto ;
      private short[] P00WD17_A91Contrato_DiasPagto ;
      private String[] P00WD17_A452Contrato_CalculoDivergencia ;
      private decimal[] P00WD17_A453Contrato_IndiceDivergencia ;
      private bool[] P00WD17_A1150Contrato_AceitaPFFS ;
      private bool[] P00WD17_n1150Contrato_AceitaPFFS ;
      private int[] P00WD17_A1013Contrato_PrepostoCod ;
      private bool[] P00WD17_n1013Contrato_PrepostoCod ;
      private int[] P00WD17_A1016Contrato_PrepostoPesCod ;
      private bool[] P00WD17_n1016Contrato_PrepostoPesCod ;
      private String[] P00WD17_A1015Contrato_PrepostoNom ;
      private bool[] P00WD17_n1015Contrato_PrepostoNom ;
      private String[] P00WD17_A1354Contrato_PrdFtrCada ;
      private bool[] P00WD17_n1354Contrato_PrdFtrCada ;
      private decimal[] P00WD17_A2086Contrato_LmtFtr ;
      private bool[] P00WD17_n2086Contrato_LmtFtr ;
      private short[] P00WD17_A1357Contrato_PrdFtrIni ;
      private bool[] P00WD17_n1357Contrato_PrdFtrIni ;
      private short[] P00WD17_A1358Contrato_PrdFtrFim ;
      private bool[] P00WD17_n1358Contrato_PrdFtrFim ;
      private DateTime[] P00WD17_A842Contrato_DataInicioTA ;
      private bool[] P00WD17_n842Contrato_DataInicioTA ;
      private decimal[] P00WD17_A1870Contrato_ValorUndCntAtual ;
      private bool[] P00WD17_n1870Contrato_ValorUndCntAtual ;
      private String[] P00WD17_A41Contratada_PessoaNom ;
      private bool[] P00WD17_n41Contratada_PessoaNom ;
      private bool[] P00WD17_A92Contrato_Ativo ;
      private String[] P00WD17_A77Contrato_Numero ;
      private DateTime[] P00WD17_A843Contrato_DataFimTA ;
      private bool[] P00WD17_n843Contrato_DataFimTA ;
      private DateTime[] P00WD17_A83Contrato_DataVigenciaTermino ;
      private DateTime[] P00WD20_A842Contrato_DataInicioTA ;
      private bool[] P00WD20_n842Contrato_DataInicioTA ;
      private DateTime[] P00WD23_A843Contrato_DataFimTA ;
      private bool[] P00WD23_n843Contrato_DataFimTA ;
      private wwpbaseobjects.SdtAuditingObject AV10AuditingObject ;
      private wwpbaseobjects.SdtAuditingObject_RecordItem AV11AuditingObjectRecordItem ;
      private wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem AV12AuditingObjectRecordItemAttributeItem ;
   }

   public class loadauditcontrato__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00WD9 ;
          prmP00WD9 = new Object[] {
          new Object[] {"@AV16Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferP00WD9 ;
          cmdBufferP00WD9=" SELECT T1.[Contrato_Codigo], T1.[Contrato_AreaTrabalhoCod] AS Contrato_AreaTrabalhoCod, T5.[AreaTrabalho_Descricao] AS Contrato_AreaTrabalhoDes, T1.[Contratada_Codigo], T6.[Contratada_PessoaCod] AS Contratada_PessoaCod, T7.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T6.[Contratada_Sigla], T6.[Contratada_TipoFabrica], T1.[Contrato_NumeroAta], T1.[Contrato_Ano], T1.[Contrato_Objeto], T1.[Contrato_UnidadeContratacao], T1.[Contrato_Quantidade], T1.[Contrato_DataVigenciaInicio], T1.[Contrato_DataPublicacaoDOU], T1.[Contrato_DataAssinatura], T1.[Contrato_DataPedidoReajuste], T1.[Contrato_DataTerminoAta], T1.[Contrato_DataFimAdaptacao], T1.[Contrato_Valor], T1.[Contrato_ValorUnidadeContratacao], T1.[Contrato_RegrasPagto], T1.[Contrato_DiasPagto], T1.[Contrato_CalculoDivergencia], T1.[Contrato_IndiceDivergencia], T1.[Contrato_AceitaPFFS], T1.[Contrato_PrepostoCod] AS Contrato_PrepostoCod, T8.[Usuario_PessoaCod] AS Contrato_PrepostoPesCod, T9.[Pessoa_Nome] AS Contrato_PrepostoNom, T1.[Contrato_PrdFtrCada], T1.[Contrato_LmtFtr], T1.[Contrato_PrdFtrIni], T1.[Contrato_PrdFtrFim], COALESCE( T2.[ContratoTermoAditivo_DataInicio], convert( DATETIME, '17530101', 112 )) AS Contrato_DataInicioTA, COALESCE( T3.[Contrato_ValorUndCntAtual], 0) AS Contrato_ValorUndCntAtual, T7.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contrato_Ativo], T1.[Contrato_Numero], COALESCE( T4.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) AS Contrato_DataFimTA, T1.[Contrato_DataVigenciaTermino] FROM (((((((([Contrato] T1 WITH (NOLOCK) LEFT JOIN (SELECT T10.[ContratoTermoAditivo_DataInicio], T10.[Contrato_Codigo], T10.[ContratoTermoAditivo_Codigo], T11.[GXC3] AS GXC3 FROM ([ContratoTermoAditivo] T10 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC3, [Contrato_Codigo] FROM "
          + " [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T11 ON T11.[Contrato_Codigo] = T10.[Contrato_Codigo]) WHERE T10.[ContratoTermoAditivo_Codigo] = T11.[GXC3] ) T2 ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) LEFT JOIN (SELECT CASE  WHEN COALESCE( T11.[GXC4], 0) > 0 THEN COALESCE( T11.[GXC4], 0) ELSE T10.[Contrato_ValorUnidadeContratacao] END AS Contrato_ValorUndCntAtual, T10.[Contrato_Codigo] FROM ([Contrato] T10 WITH (NOLOCK) LEFT JOIN (SELECT T12.[ContratoTermoAditivo_VlrUntUndCnt] AS GXC4, T12.[Contrato_Codigo], T12.[ContratoTermoAditivo_Codigo], T13.[GXC6] AS GXC6 FROM ([ContratoTermoAditivo] T12 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC6, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T13 ON T13.[Contrato_Codigo] = T12.[Contrato_Codigo]) WHERE T12.[ContratoTermoAditivo_Codigo] = T13.[GXC6] ) T11 ON T11.[Contrato_Codigo] = T10.[Contrato_Codigo]) ) T3 ON T3.[Contrato_Codigo] = T1.[Contrato_Codigo]) LEFT JOIN (SELECT T10.[ContratoTermoAditivo_DataFim], T10.[Contrato_Codigo], T10.[ContratoTermoAditivo_Codigo], T11.[GXC3] AS GXC3 FROM ([ContratoTermoAditivo] T10 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC3, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T11 ON T11.[Contrato_Codigo] = T10.[Contrato_Codigo]) WHERE T10.[ContratoTermoAditivo_Codigo] = T11.[GXC3] ) T4 ON T4.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [AreaTrabalho] T5 WITH (NOLOCK) ON T5.[AreaTrabalho_Codigo] = T1.[Contrato_AreaTrabalhoCod]) INNER JOIN [Contratada] T6 WITH (NOLOCK) ON T6.[Contratada_Codigo] = T1.[Contratada_Codigo]) INNER JOIN [Pessoa] T7 WITH (NOLOCK) ON T7.[Pessoa_Codigo] = T6.[Contratada_PessoaCod]) LEFT JOIN [Usuario]"
          + " T8 WITH (NOLOCK) ON T8.[Usuario_Codigo] = T1.[Contrato_PrepostoCod]) LEFT JOIN [Pessoa] T9 WITH (NOLOCK) ON T9.[Pessoa_Codigo] = T8.[Usuario_PessoaCod]) WHERE T1.[Contrato_Codigo] = @AV16Contrato_Codigo ORDER BY T1.[Contrato_Codigo]" ;
          Object[] prmP00WD17 ;
          prmP00WD17 = new Object[] {
          new Object[] {"@AV16Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferP00WD17 ;
          cmdBufferP00WD17=" SELECT T1.[Contrato_Codigo], T1.[Contrato_AreaTrabalhoCod] AS Contrato_AreaTrabalhoCod, T5.[AreaTrabalho_Descricao] AS Contrato_AreaTrabalhoDes, T1.[Contratada_Codigo], T6.[Contratada_PessoaCod] AS Contratada_PessoaCod, T7.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T6.[Contratada_Sigla], T6.[Contratada_TipoFabrica], T1.[Contrato_NumeroAta], T1.[Contrato_Ano], T1.[Contrato_Objeto], T1.[Contrato_UnidadeContratacao], T1.[Contrato_Quantidade], T1.[Contrato_DataVigenciaInicio], T1.[Contrato_DataPublicacaoDOU], T1.[Contrato_DataAssinatura], T1.[Contrato_DataPedidoReajuste], T1.[Contrato_DataTerminoAta], T1.[Contrato_DataFimAdaptacao], T1.[Contrato_Valor], T1.[Contrato_ValorUnidadeContratacao], T1.[Contrato_RegrasPagto], T1.[Contrato_DiasPagto], T1.[Contrato_CalculoDivergencia], T1.[Contrato_IndiceDivergencia], T1.[Contrato_AceitaPFFS], T1.[Contrato_PrepostoCod] AS Contrato_PrepostoCod, T8.[Usuario_PessoaCod] AS Contrato_PrepostoPesCod, T9.[Pessoa_Nome] AS Contrato_PrepostoNom, T1.[Contrato_PrdFtrCada], T1.[Contrato_LmtFtr], T1.[Contrato_PrdFtrIni], T1.[Contrato_PrdFtrFim], COALESCE( T2.[ContratoTermoAditivo_DataInicio], convert( DATETIME, '17530101', 112 )) AS Contrato_DataInicioTA, COALESCE( T3.[Contrato_ValorUndCntAtual], 0) AS Contrato_ValorUndCntAtual, T7.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contrato_Ativo], T1.[Contrato_Numero], COALESCE( T4.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) AS Contrato_DataFimTA, T1.[Contrato_DataVigenciaTermino] FROM (((((((([Contrato] T1 WITH (NOLOCK) LEFT JOIN (SELECT T10.[ContratoTermoAditivo_DataInicio], T10.[Contrato_Codigo], T10.[ContratoTermoAditivo_Codigo], T11.[GXC3] AS GXC3 FROM ([ContratoTermoAditivo] T10 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC3, [Contrato_Codigo] FROM "
          + " [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T11 ON T11.[Contrato_Codigo] = T10.[Contrato_Codigo]) WHERE T10.[ContratoTermoAditivo_Codigo] = T11.[GXC3] ) T2 ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) LEFT JOIN (SELECT CASE  WHEN COALESCE( T11.[GXC4], 0) > 0 THEN COALESCE( T11.[GXC4], 0) ELSE T10.[Contrato_ValorUnidadeContratacao] END AS Contrato_ValorUndCntAtual, T10.[Contrato_Codigo] FROM ([Contrato] T10 WITH (NOLOCK) LEFT JOIN (SELECT T12.[ContratoTermoAditivo_VlrUntUndCnt] AS GXC4, T12.[Contrato_Codigo], T12.[ContratoTermoAditivo_Codigo], T13.[GXC6] AS GXC6 FROM ([ContratoTermoAditivo] T12 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC6, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T13 ON T13.[Contrato_Codigo] = T12.[Contrato_Codigo]) WHERE T12.[ContratoTermoAditivo_Codigo] = T13.[GXC6] ) T11 ON T11.[Contrato_Codigo] = T10.[Contrato_Codigo]) ) T3 ON T3.[Contrato_Codigo] = T1.[Contrato_Codigo]) LEFT JOIN (SELECT T10.[ContratoTermoAditivo_DataFim], T10.[Contrato_Codigo], T10.[ContratoTermoAditivo_Codigo], T11.[GXC3] AS GXC3 FROM ([ContratoTermoAditivo] T10 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC3, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T11 ON T11.[Contrato_Codigo] = T10.[Contrato_Codigo]) WHERE T10.[ContratoTermoAditivo_Codigo] = T11.[GXC3] ) T4 ON T4.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [AreaTrabalho] T5 WITH (NOLOCK) ON T5.[AreaTrabalho_Codigo] = T1.[Contrato_AreaTrabalhoCod]) INNER JOIN [Contratada] T6 WITH (NOLOCK) ON T6.[Contratada_Codigo] = T1.[Contratada_Codigo]) INNER JOIN [Pessoa] T7 WITH (NOLOCK) ON T7.[Pessoa_Codigo] = T6.[Contratada_PessoaCod]) LEFT JOIN [Usuario]"
          + " T8 WITH (NOLOCK) ON T8.[Usuario_Codigo] = T1.[Contrato_PrepostoCod]) LEFT JOIN [Pessoa] T9 WITH (NOLOCK) ON T9.[Pessoa_Codigo] = T8.[Usuario_PessoaCod]) WHERE T1.[Contrato_Codigo] = @AV16Contrato_Codigo ORDER BY T1.[Contrato_Codigo]" ;
          Object[] prmP00WD20 ;
          prmP00WD20 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00WD23 ;
          prmP00WD23 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00WD9", cmdBufferP00WD9,false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WD9,1,0,false,true )
             ,new CursorDef("P00WD17", cmdBufferP00WD17,false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WD17,1,0,true,true )
             ,new CursorDef("P00WD20", "SELECT COALESCE( T1.[ContratoTermoAditivo_DataInicio], convert( DATETIME, '17530101', 112 )) AS Contrato_DataInicioTA FROM (SELECT T2.[ContratoTermoAditivo_DataInicio], T2.[Contrato_Codigo], T2.[ContratoTermoAditivo_Codigo], T3.[GXC3] AS GXC3 FROM ([ContratoTermoAditivo] T2 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC3, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T3 ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) WHERE T2.[ContratoTermoAditivo_Codigo] = T3.[GXC3] ) T1 WHERE T1.[Contrato_Codigo] = @Contrato_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WD20,1,0,true,true )
             ,new CursorDef("P00WD23", "SELECT COALESCE( T1.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) AS Contrato_DataFimTA FROM (SELECT T2.[ContratoTermoAditivo_DataFim], T2.[Contrato_Codigo], T2.[ContratoTermoAditivo_Codigo], T3.[GXC3] AS GXC3 FROM ([ContratoTermoAditivo] T2 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC3, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T3 ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) WHERE T2.[ContratoTermoAditivo_Codigo] = T3.[GXC3] ) T1 WHERE T1.[Contrato_Codigo] = @Contrato_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WD23,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getString(7, 15) ;
                ((String[]) buf[9])[0] = rslt.getString(8, 1) ;
                ((String[]) buf[10])[0] = rslt.getString(9, 10) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(9);
                ((short[]) buf[12])[0] = rslt.getShort(10) ;
                ((String[]) buf[13])[0] = rslt.getLongVarchar(11) ;
                ((short[]) buf[14])[0] = rslt.getShort(12) ;
                ((int[]) buf[15])[0] = rslt.getInt(13) ;
                ((DateTime[]) buf[16])[0] = rslt.getGXDate(14) ;
                ((DateTime[]) buf[17])[0] = rslt.getGXDate(15) ;
                ((DateTime[]) buf[18])[0] = rslt.getGXDate(16) ;
                ((DateTime[]) buf[19])[0] = rslt.getGXDate(17) ;
                ((DateTime[]) buf[20])[0] = rslt.getGXDate(18) ;
                ((DateTime[]) buf[21])[0] = rslt.getGXDate(19) ;
                ((decimal[]) buf[22])[0] = rslt.getDecimal(20) ;
                ((decimal[]) buf[23])[0] = rslt.getDecimal(21) ;
                ((String[]) buf[24])[0] = rslt.getLongVarchar(22) ;
                ((short[]) buf[25])[0] = rslt.getShort(23) ;
                ((String[]) buf[26])[0] = rslt.getString(24, 1) ;
                ((decimal[]) buf[27])[0] = rslt.getDecimal(25) ;
                ((bool[]) buf[28])[0] = rslt.getBool(26) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(26);
                ((int[]) buf[30])[0] = rslt.getInt(27) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(27);
                ((int[]) buf[32])[0] = rslt.getInt(28) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(28);
                ((String[]) buf[34])[0] = rslt.getString(29, 100) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(29);
                ((String[]) buf[36])[0] = rslt.getString(30, 1) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(30);
                ((decimal[]) buf[38])[0] = rslt.getDecimal(31) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(31);
                ((short[]) buf[40])[0] = rslt.getShort(32) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(32);
                ((short[]) buf[42])[0] = rslt.getShort(33) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(33);
                ((DateTime[]) buf[44])[0] = rslt.getGXDate(34) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(34);
                ((decimal[]) buf[46])[0] = rslt.getDecimal(35) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(35);
                ((String[]) buf[48])[0] = rslt.getString(36, 100) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(36);
                ((bool[]) buf[50])[0] = rslt.getBool(37) ;
                ((String[]) buf[51])[0] = rslt.getString(38, 20) ;
                ((DateTime[]) buf[52])[0] = rslt.getGXDate(39) ;
                ((bool[]) buf[53])[0] = rslt.wasNull(39);
                ((DateTime[]) buf[54])[0] = rslt.getGXDate(40) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getString(7, 15) ;
                ((String[]) buf[9])[0] = rslt.getString(8, 1) ;
                ((String[]) buf[10])[0] = rslt.getString(9, 10) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(9);
                ((short[]) buf[12])[0] = rslt.getShort(10) ;
                ((String[]) buf[13])[0] = rslt.getLongVarchar(11) ;
                ((short[]) buf[14])[0] = rslt.getShort(12) ;
                ((int[]) buf[15])[0] = rslt.getInt(13) ;
                ((DateTime[]) buf[16])[0] = rslt.getGXDate(14) ;
                ((DateTime[]) buf[17])[0] = rslt.getGXDate(15) ;
                ((DateTime[]) buf[18])[0] = rslt.getGXDate(16) ;
                ((DateTime[]) buf[19])[0] = rslt.getGXDate(17) ;
                ((DateTime[]) buf[20])[0] = rslt.getGXDate(18) ;
                ((DateTime[]) buf[21])[0] = rslt.getGXDate(19) ;
                ((decimal[]) buf[22])[0] = rslt.getDecimal(20) ;
                ((decimal[]) buf[23])[0] = rslt.getDecimal(21) ;
                ((String[]) buf[24])[0] = rslt.getLongVarchar(22) ;
                ((short[]) buf[25])[0] = rslt.getShort(23) ;
                ((String[]) buf[26])[0] = rslt.getString(24, 1) ;
                ((decimal[]) buf[27])[0] = rslt.getDecimal(25) ;
                ((bool[]) buf[28])[0] = rslt.getBool(26) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(26);
                ((int[]) buf[30])[0] = rslt.getInt(27) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(27);
                ((int[]) buf[32])[0] = rslt.getInt(28) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(28);
                ((String[]) buf[34])[0] = rslt.getString(29, 100) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(29);
                ((String[]) buf[36])[0] = rslt.getString(30, 1) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(30);
                ((decimal[]) buf[38])[0] = rslt.getDecimal(31) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(31);
                ((short[]) buf[40])[0] = rslt.getShort(32) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(32);
                ((short[]) buf[42])[0] = rslt.getShort(33) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(33);
                ((DateTime[]) buf[44])[0] = rslt.getGXDate(34) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(34);
                ((decimal[]) buf[46])[0] = rslt.getDecimal(35) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(35);
                ((String[]) buf[48])[0] = rslt.getString(36, 100) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(36);
                ((bool[]) buf[50])[0] = rslt.getBool(37) ;
                ((String[]) buf[51])[0] = rslt.getString(38, 20) ;
                ((DateTime[]) buf[52])[0] = rslt.getGXDate(39) ;
                ((bool[]) buf[53])[0] = rslt.wasNull(39);
                ((DateTime[]) buf[54])[0] = rslt.getGXDate(40) ;
                return;
             case 2 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
