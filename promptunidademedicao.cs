/*
               File: PromptUnidadeMedicao
        Description: Selecione Unidades de Medi��o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:43:58.9
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptunidademedicao : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptunidademedicao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptunidademedicao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutUnidadeMedicao_Codigo ,
                           ref String aP1_InOutUnidadeMedicao_Nome )
      {
         this.AV7InOutUnidadeMedicao_Codigo = aP0_InOutUnidadeMedicao_Codigo;
         this.AV8InOutUnidadeMedicao_Nome = aP1_InOutUnidadeMedicao_Nome;
         executePrivate();
         aP0_InOutUnidadeMedicao_Codigo=this.AV7InOutUnidadeMedicao_Codigo;
         aP1_InOutUnidadeMedicao_Nome=this.AV8InOutUnidadeMedicao_Nome;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         chkUnidadeMedicao_Ativo = new GXCheckbox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_65 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_65_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_65_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16UnidadeMedicao_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16UnidadeMedicao_Nome1", AV16UnidadeMedicao_Nome1);
               AV18DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
               AV19UnidadeMedicao_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19UnidadeMedicao_Nome2", AV19UnidadeMedicao_Nome2);
               AV21DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
               AV22UnidadeMedicao_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22UnidadeMedicao_Nome3", AV22UnidadeMedicao_Nome3);
               AV17DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
               AV20DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
               AV28TFUnidadeMedicao_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28TFUnidadeMedicao_Nome", AV28TFUnidadeMedicao_Nome);
               AV29TFUnidadeMedicao_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29TFUnidadeMedicao_Nome_Sel", AV29TFUnidadeMedicao_Nome_Sel);
               AV32TFUnidadeMedicao_Sigla = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFUnidadeMedicao_Sigla", AV32TFUnidadeMedicao_Sigla);
               AV33TFUnidadeMedicao_Sigla_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TFUnidadeMedicao_Sigla_Sel", AV33TFUnidadeMedicao_Sigla_Sel);
               AV36TFUnidadeMedicao_Ativo_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFUnidadeMedicao_Ativo_Sel", StringUtil.Str( (decimal)(AV36TFUnidadeMedicao_Ativo_Sel), 1, 0));
               AV30ddo_UnidadeMedicao_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ddo_UnidadeMedicao_NomeTitleControlIdToReplace", AV30ddo_UnidadeMedicao_NomeTitleControlIdToReplace);
               AV34ddo_UnidadeMedicao_SiglaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ddo_UnidadeMedicao_SiglaTitleControlIdToReplace", AV34ddo_UnidadeMedicao_SiglaTitleControlIdToReplace);
               AV37ddo_UnidadeMedicao_AtivoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_UnidadeMedicao_AtivoTitleControlIdToReplace", AV37ddo_UnidadeMedicao_AtivoTitleControlIdToReplace);
               AV45Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV24DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersIgnoreFirst", AV24DynamicFiltersIgnoreFirst);
               AV23DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16UnidadeMedicao_Nome1, AV18DynamicFiltersSelector2, AV19UnidadeMedicao_Nome2, AV21DynamicFiltersSelector3, AV22UnidadeMedicao_Nome3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV28TFUnidadeMedicao_Nome, AV29TFUnidadeMedicao_Nome_Sel, AV32TFUnidadeMedicao_Sigla, AV33TFUnidadeMedicao_Sigla_Sel, AV36TFUnidadeMedicao_Ativo_Sel, AV30ddo_UnidadeMedicao_NomeTitleControlIdToReplace, AV34ddo_UnidadeMedicao_SiglaTitleControlIdToReplace, AV37ddo_UnidadeMedicao_AtivoTitleControlIdToReplace, AV45Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutUnidadeMedicao_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutUnidadeMedicao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutUnidadeMedicao_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutUnidadeMedicao_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutUnidadeMedicao_Nome", AV8InOutUnidadeMedicao_Nome);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAIP2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV45Pgmname = "PromptUnidadeMedicao";
               context.Gx_err = 0;
               WSIP2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEIP2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042823435829");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptunidademedicao.aspx") + "?" + UrlEncode("" +AV7InOutUnidadeMedicao_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV8InOutUnidadeMedicao_Nome))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vUNIDADEMEDICAO_NOME1", StringUtil.RTrim( AV16UnidadeMedicao_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV18DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vUNIDADEMEDICAO_NOME2", StringUtil.RTrim( AV19UnidadeMedicao_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV21DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vUNIDADEMEDICAO_NOME3", StringUtil.RTrim( AV22UnidadeMedicao_Nome3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV17DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV20DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFUNIDADEMEDICAO_NOME", StringUtil.RTrim( AV28TFUnidadeMedicao_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFUNIDADEMEDICAO_NOME_SEL", StringUtil.RTrim( AV29TFUnidadeMedicao_Nome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFUNIDADEMEDICAO_SIGLA", StringUtil.RTrim( AV32TFUnidadeMedicao_Sigla));
         GxWebStd.gx_hidden_field( context, "GXH_vTFUNIDADEMEDICAO_SIGLA_SEL", StringUtil.RTrim( AV33TFUnidadeMedicao_Sigla_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFUNIDADEMEDICAO_ATIVO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36TFUnidadeMedicao_Ativo_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_65", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_65), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV40GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV41GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV38DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV38DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vUNIDADEMEDICAO_NOMETITLEFILTERDATA", AV27UnidadeMedicao_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vUNIDADEMEDICAO_NOMETITLEFILTERDATA", AV27UnidadeMedicao_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vUNIDADEMEDICAO_SIGLATITLEFILTERDATA", AV31UnidadeMedicao_SiglaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vUNIDADEMEDICAO_SIGLATITLEFILTERDATA", AV31UnidadeMedicao_SiglaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vUNIDADEMEDICAO_ATIVOTITLEFILTERDATA", AV35UnidadeMedicao_AtivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vUNIDADEMEDICAO_ATIVOTITLEFILTERDATA", AV35UnidadeMedicao_AtivoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV45Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV24DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV23DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTUNIDADEMEDICAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutUnidadeMedicao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTUNIDADEMEDICAO_NOME", StringUtil.RTrim( AV8InOutUnidadeMedicao_Nome));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_NOME_Caption", StringUtil.RTrim( Ddo_unidademedicao_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_NOME_Tooltip", StringUtil.RTrim( Ddo_unidademedicao_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_NOME_Cls", StringUtil.RTrim( Ddo_unidademedicao_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_unidademedicao_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_unidademedicao_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_unidademedicao_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_unidademedicao_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_unidademedicao_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_unidademedicao_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_NOME_Sortedstatus", StringUtil.RTrim( Ddo_unidademedicao_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_unidademedicao_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_NOME_Filtertype", StringUtil.RTrim( Ddo_unidademedicao_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_unidademedicao_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_unidademedicao_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_NOME_Datalisttype", StringUtil.RTrim( Ddo_unidademedicao_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_NOME_Datalistfixedvalues", StringUtil.RTrim( Ddo_unidademedicao_nome_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_NOME_Datalistproc", StringUtil.RTrim( Ddo_unidademedicao_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_unidademedicao_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_NOME_Sortasc", StringUtil.RTrim( Ddo_unidademedicao_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_NOME_Sortdsc", StringUtil.RTrim( Ddo_unidademedicao_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_NOME_Loadingdata", StringUtil.RTrim( Ddo_unidademedicao_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_unidademedicao_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_NOME_Rangefilterfrom", StringUtil.RTrim( Ddo_unidademedicao_nome_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_NOME_Rangefilterto", StringUtil.RTrim( Ddo_unidademedicao_nome_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_unidademedicao_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_unidademedicao_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_SIGLA_Caption", StringUtil.RTrim( Ddo_unidademedicao_sigla_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_SIGLA_Tooltip", StringUtil.RTrim( Ddo_unidademedicao_sigla_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_SIGLA_Cls", StringUtil.RTrim( Ddo_unidademedicao_sigla_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_SIGLA_Filteredtext_set", StringUtil.RTrim( Ddo_unidademedicao_sigla_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_SIGLA_Selectedvalue_set", StringUtil.RTrim( Ddo_unidademedicao_sigla_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_SIGLA_Dropdownoptionstype", StringUtil.RTrim( Ddo_unidademedicao_sigla_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_SIGLA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_unidademedicao_sigla_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_SIGLA_Includesortasc", StringUtil.BoolToStr( Ddo_unidademedicao_sigla_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_SIGLA_Includesortdsc", StringUtil.BoolToStr( Ddo_unidademedicao_sigla_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_SIGLA_Sortedstatus", StringUtil.RTrim( Ddo_unidademedicao_sigla_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_SIGLA_Includefilter", StringUtil.BoolToStr( Ddo_unidademedicao_sigla_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_SIGLA_Filtertype", StringUtil.RTrim( Ddo_unidademedicao_sigla_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_SIGLA_Filterisrange", StringUtil.BoolToStr( Ddo_unidademedicao_sigla_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_SIGLA_Includedatalist", StringUtil.BoolToStr( Ddo_unidademedicao_sigla_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_SIGLA_Datalisttype", StringUtil.RTrim( Ddo_unidademedicao_sigla_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_SIGLA_Datalistfixedvalues", StringUtil.RTrim( Ddo_unidademedicao_sigla_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_SIGLA_Datalistproc", StringUtil.RTrim( Ddo_unidademedicao_sigla_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_SIGLA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_unidademedicao_sigla_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_SIGLA_Sortasc", StringUtil.RTrim( Ddo_unidademedicao_sigla_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_SIGLA_Sortdsc", StringUtil.RTrim( Ddo_unidademedicao_sigla_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_SIGLA_Loadingdata", StringUtil.RTrim( Ddo_unidademedicao_sigla_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_SIGLA_Cleanfilter", StringUtil.RTrim( Ddo_unidademedicao_sigla_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_SIGLA_Rangefilterfrom", StringUtil.RTrim( Ddo_unidademedicao_sigla_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_SIGLA_Rangefilterto", StringUtil.RTrim( Ddo_unidademedicao_sigla_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_SIGLA_Noresultsfound", StringUtil.RTrim( Ddo_unidademedicao_sigla_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_SIGLA_Searchbuttontext", StringUtil.RTrim( Ddo_unidademedicao_sigla_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_ATIVO_Caption", StringUtil.RTrim( Ddo_unidademedicao_ativo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_ATIVO_Tooltip", StringUtil.RTrim( Ddo_unidademedicao_ativo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_ATIVO_Cls", StringUtil.RTrim( Ddo_unidademedicao_ativo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_ATIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_unidademedicao_ativo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_ATIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_unidademedicao_ativo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_ATIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_unidademedicao_ativo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_ATIVO_Includesortasc", StringUtil.BoolToStr( Ddo_unidademedicao_ativo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_ATIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_unidademedicao_ativo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_ATIVO_Sortedstatus", StringUtil.RTrim( Ddo_unidademedicao_ativo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_ATIVO_Includefilter", StringUtil.BoolToStr( Ddo_unidademedicao_ativo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_ATIVO_Filterisrange", StringUtil.BoolToStr( Ddo_unidademedicao_ativo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_ATIVO_Includedatalist", StringUtil.BoolToStr( Ddo_unidademedicao_ativo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_ATIVO_Datalisttype", StringUtil.RTrim( Ddo_unidademedicao_ativo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_ATIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_unidademedicao_ativo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_ATIVO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_unidademedicao_ativo_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_ATIVO_Sortasc", StringUtil.RTrim( Ddo_unidademedicao_ativo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_ATIVO_Sortdsc", StringUtil.RTrim( Ddo_unidademedicao_ativo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_ATIVO_Loadingdata", StringUtil.RTrim( Ddo_unidademedicao_ativo_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_ATIVO_Cleanfilter", StringUtil.RTrim( Ddo_unidademedicao_ativo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_ATIVO_Rangefilterfrom", StringUtil.RTrim( Ddo_unidademedicao_ativo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_ATIVO_Rangefilterto", StringUtil.RTrim( Ddo_unidademedicao_ativo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_ATIVO_Noresultsfound", StringUtil.RTrim( Ddo_unidademedicao_ativo_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_ATIVO_Searchbuttontext", StringUtil.RTrim( Ddo_unidademedicao_ativo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_unidademedicao_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_unidademedicao_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_unidademedicao_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_SIGLA_Activeeventkey", StringUtil.RTrim( Ddo_unidademedicao_sigla_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_SIGLA_Filteredtext_get", StringUtil.RTrim( Ddo_unidademedicao_sigla_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_SIGLA_Selectedvalue_get", StringUtil.RTrim( Ddo_unidademedicao_sigla_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_ATIVO_Activeeventkey", StringUtil.RTrim( Ddo_unidademedicao_ativo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEMEDICAO_ATIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_unidademedicao_ativo_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormIP2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptUnidadeMedicao" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Unidades de Medi��o" ;
      }

      protected void WBIP0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_IP2( true) ;
         }
         else
         {
            wb_table1_2_IP2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_IP2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'" + sGXsfl_65_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV17DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(75, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,75);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'" + sGXsfl_65_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV20DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(76, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,76);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'" + sGXsfl_65_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfunidademedicao_nome_Internalname, StringUtil.RTrim( AV28TFUnidadeMedicao_Nome), StringUtil.RTrim( context.localUtil.Format( AV28TFUnidadeMedicao_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,77);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfunidademedicao_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfunidademedicao_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptUnidadeMedicao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'" + sGXsfl_65_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfunidademedicao_nome_sel_Internalname, StringUtil.RTrim( AV29TFUnidadeMedicao_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV29TFUnidadeMedicao_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,78);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfunidademedicao_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfunidademedicao_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptUnidadeMedicao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'" + sGXsfl_65_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfunidademedicao_sigla_Internalname, StringUtil.RTrim( AV32TFUnidadeMedicao_Sigla), StringUtil.RTrim( context.localUtil.Format( AV32TFUnidadeMedicao_Sigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,79);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfunidademedicao_sigla_Jsonclick, 0, "Attribute", "", "", "", edtavTfunidademedicao_sigla_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptUnidadeMedicao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'',false,'" + sGXsfl_65_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfunidademedicao_sigla_sel_Internalname, StringUtil.RTrim( AV33TFUnidadeMedicao_Sigla_Sel), StringUtil.RTrim( context.localUtil.Format( AV33TFUnidadeMedicao_Sigla_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,80);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfunidademedicao_sigla_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfunidademedicao_sigla_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptUnidadeMedicao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'" + sGXsfl_65_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfunidademedicao_ativo_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36TFUnidadeMedicao_Ativo_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV36TFUnidadeMedicao_Ativo_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,81);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfunidademedicao_ativo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfunidademedicao_ativo_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptUnidadeMedicao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_UNIDADEMEDICAO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'" + sGXsfl_65_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_unidademedicao_nometitlecontrolidtoreplace_Internalname, AV30ddo_UnidadeMedicao_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,83);\"", 0, edtavDdo_unidademedicao_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptUnidadeMedicao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_UNIDADEMEDICAO_SIGLAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 85,'',false,'" + sGXsfl_65_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_unidademedicao_siglatitlecontrolidtoreplace_Internalname, AV34ddo_UnidadeMedicao_SiglaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,85);\"", 0, edtavDdo_unidademedicao_siglatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptUnidadeMedicao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_UNIDADEMEDICAO_ATIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 87,'',false,'" + sGXsfl_65_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_unidademedicao_ativotitlecontrolidtoreplace_Internalname, AV37ddo_UnidadeMedicao_AtivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,87);\"", 0, edtavDdo_unidademedicao_ativotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptUnidadeMedicao.htm");
         }
         wbLoad = true;
      }

      protected void STARTIP2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Unidades de Medi��o", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPIP0( ) ;
      }

      protected void WSIP2( )
      {
         STARTIP2( ) ;
         EVTIP2( ) ;
      }

      protected void EVTIP2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11IP2 */
                           E11IP2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_UNIDADEMEDICAO_NOME.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12IP2 */
                           E12IP2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_UNIDADEMEDICAO_SIGLA.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13IP2 */
                           E13IP2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_UNIDADEMEDICAO_ATIVO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14IP2 */
                           E14IP2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15IP2 */
                           E15IP2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16IP2 */
                           E16IP2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17IP2 */
                           E17IP2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18IP2 */
                           E18IP2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19IP2 */
                           E19IP2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20IP2 */
                           E20IP2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E21IP2 */
                           E21IP2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E22IP2 */
                           E22IP2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E23IP2 */
                           E23IP2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E24IP2 */
                           E24IP2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_65_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_65_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_65_idx), 4, 0)), 4, "0");
                           SubsflControlProps_652( ) ;
                           AV25Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV25Select)) ? AV44Select_GXI : context.convertURL( context.PathToRelativeUrl( AV25Select))));
                           A1197UnidadeMedicao_Codigo = (int)(context.localUtil.CToN( cgiGet( edtUnidadeMedicao_Codigo_Internalname), ",", "."));
                           A1198UnidadeMedicao_Nome = StringUtil.Upper( cgiGet( edtUnidadeMedicao_Nome_Internalname));
                           A1200UnidadeMedicao_Sigla = StringUtil.Upper( cgiGet( edtUnidadeMedicao_Sigla_Internalname));
                           A1199UnidadeMedicao_Ativo = StringUtil.StrToBool( cgiGet( chkUnidadeMedicao_Ativo_Internalname));
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E25IP2 */
                                 E25IP2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E26IP2 */
                                 E26IP2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E27IP2 */
                                 E27IP2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Unidademedicao_nome1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vUNIDADEMEDICAO_NOME1"), AV16UnidadeMedicao_Nome1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV18DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Unidademedicao_nome2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vUNIDADEMEDICAO_NOME2"), AV19UnidadeMedicao_Nome2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV21DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Unidademedicao_nome3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vUNIDADEMEDICAO_NOME3"), AV22UnidadeMedicao_Nome3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV17DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV20DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfunidademedicao_nome Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUNIDADEMEDICAO_NOME"), AV28TFUnidadeMedicao_Nome) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfunidademedicao_nome_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUNIDADEMEDICAO_NOME_SEL"), AV29TFUnidadeMedicao_Nome_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfunidademedicao_sigla Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUNIDADEMEDICAO_SIGLA"), AV32TFUnidadeMedicao_Sigla) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfunidademedicao_sigla_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUNIDADEMEDICAO_SIGLA_SEL"), AV33TFUnidadeMedicao_Sigla_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfunidademedicao_ativo_sel Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFUNIDADEMEDICAO_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV36TFUnidadeMedicao_Ativo_Sel )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E28IP2 */
                                       E28IP2 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEIP2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormIP2( ) ;
            }
         }
      }

      protected void PAIP2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("UNIDADEMEDICAO_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("UNIDADEMEDICAO_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV18DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV18DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("UNIDADEMEDICAO_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV21DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV21DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
            }
            GXCCtl = "UNIDADEMEDICAO_ATIVO_" + sGXsfl_65_idx;
            chkUnidadeMedicao_Ativo.Name = GXCCtl;
            chkUnidadeMedicao_Ativo.WebTags = "";
            chkUnidadeMedicao_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUnidadeMedicao_Ativo_Internalname, "TitleCaption", chkUnidadeMedicao_Ativo.Caption);
            chkUnidadeMedicao_Ativo.CheckedValue = "false";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_652( ) ;
         while ( nGXsfl_65_idx <= nRC_GXsfl_65 )
         {
            sendrow_652( ) ;
            nGXsfl_65_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_65_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_65_idx+1));
            sGXsfl_65_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_65_idx), 4, 0)), 4, "0");
            SubsflControlProps_652( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       String AV16UnidadeMedicao_Nome1 ,
                                       String AV18DynamicFiltersSelector2 ,
                                       String AV19UnidadeMedicao_Nome2 ,
                                       String AV21DynamicFiltersSelector3 ,
                                       String AV22UnidadeMedicao_Nome3 ,
                                       bool AV17DynamicFiltersEnabled2 ,
                                       bool AV20DynamicFiltersEnabled3 ,
                                       String AV28TFUnidadeMedicao_Nome ,
                                       String AV29TFUnidadeMedicao_Nome_Sel ,
                                       String AV32TFUnidadeMedicao_Sigla ,
                                       String AV33TFUnidadeMedicao_Sigla_Sel ,
                                       short AV36TFUnidadeMedicao_Ativo_Sel ,
                                       String AV30ddo_UnidadeMedicao_NomeTitleControlIdToReplace ,
                                       String AV34ddo_UnidadeMedicao_SiglaTitleControlIdToReplace ,
                                       String AV37ddo_UnidadeMedicao_AtivoTitleControlIdToReplace ,
                                       String AV45Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV24DynamicFiltersIgnoreFirst ,
                                       bool AV23DynamicFiltersRemoving )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFIP2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_UNIDADEMEDICAO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1197UnidadeMedicao_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "UNIDADEMEDICAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1197UnidadeMedicao_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_UNIDADEMEDICAO_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1198UnidadeMedicao_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "UNIDADEMEDICAO_NOME", StringUtil.RTrim( A1198UnidadeMedicao_Nome));
         GxWebStd.gx_hidden_field( context, "gxhash_UNIDADEMEDICAO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1200UnidadeMedicao_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, "UNIDADEMEDICAO_SIGLA", StringUtil.RTrim( A1200UnidadeMedicao_Sigla));
         GxWebStd.gx_hidden_field( context, "gxhash_UNIDADEMEDICAO_ATIVO", GetSecureSignedToken( "", A1199UnidadeMedicao_Ativo));
         GxWebStd.gx_hidden_field( context, "UNIDADEMEDICAO_ATIVO", StringUtil.BoolToStr( A1199UnidadeMedicao_Ativo));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV18DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV18DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV21DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV21DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFIP2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV45Pgmname = "PromptUnidadeMedicao";
         context.Gx_err = 0;
      }

      protected void RFIP2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 65;
         /* Execute user event: E26IP2 */
         E26IP2 ();
         nGXsfl_65_idx = 1;
         sGXsfl_65_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_65_idx), 4, 0)), 4, "0");
         SubsflControlProps_652( ) ;
         nGXsfl_65_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_652( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV16UnidadeMedicao_Nome1 ,
                                                 AV17DynamicFiltersEnabled2 ,
                                                 AV18DynamicFiltersSelector2 ,
                                                 AV19UnidadeMedicao_Nome2 ,
                                                 AV20DynamicFiltersEnabled3 ,
                                                 AV21DynamicFiltersSelector3 ,
                                                 AV22UnidadeMedicao_Nome3 ,
                                                 AV29TFUnidadeMedicao_Nome_Sel ,
                                                 AV28TFUnidadeMedicao_Nome ,
                                                 AV33TFUnidadeMedicao_Sigla_Sel ,
                                                 AV32TFUnidadeMedicao_Sigla ,
                                                 AV36TFUnidadeMedicao_Ativo_Sel ,
                                                 A1198UnidadeMedicao_Nome ,
                                                 A1200UnidadeMedicao_Sigla ,
                                                 A1199UnidadeMedicao_Ativo ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV16UnidadeMedicao_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV16UnidadeMedicao_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16UnidadeMedicao_Nome1", AV16UnidadeMedicao_Nome1);
            lV19UnidadeMedicao_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV19UnidadeMedicao_Nome2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19UnidadeMedicao_Nome2", AV19UnidadeMedicao_Nome2);
            lV22UnidadeMedicao_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV22UnidadeMedicao_Nome3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22UnidadeMedicao_Nome3", AV22UnidadeMedicao_Nome3);
            lV28TFUnidadeMedicao_Nome = StringUtil.PadR( StringUtil.RTrim( AV28TFUnidadeMedicao_Nome), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28TFUnidadeMedicao_Nome", AV28TFUnidadeMedicao_Nome);
            lV32TFUnidadeMedicao_Sigla = StringUtil.PadR( StringUtil.RTrim( AV32TFUnidadeMedicao_Sigla), 15, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFUnidadeMedicao_Sigla", AV32TFUnidadeMedicao_Sigla);
            /* Using cursor H00IP2 */
            pr_default.execute(0, new Object[] {lV16UnidadeMedicao_Nome1, lV19UnidadeMedicao_Nome2, lV22UnidadeMedicao_Nome3, lV28TFUnidadeMedicao_Nome, AV29TFUnidadeMedicao_Nome_Sel, lV32TFUnidadeMedicao_Sigla, AV33TFUnidadeMedicao_Sigla_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_65_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1199UnidadeMedicao_Ativo = H00IP2_A1199UnidadeMedicao_Ativo[0];
               A1200UnidadeMedicao_Sigla = H00IP2_A1200UnidadeMedicao_Sigla[0];
               A1198UnidadeMedicao_Nome = H00IP2_A1198UnidadeMedicao_Nome[0];
               A1197UnidadeMedicao_Codigo = H00IP2_A1197UnidadeMedicao_Codigo[0];
               /* Execute user event: E27IP2 */
               E27IP2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 65;
            WBIP0( ) ;
         }
         nGXsfl_65_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV15DynamicFiltersSelector1 ,
                                              AV16UnidadeMedicao_Nome1 ,
                                              AV17DynamicFiltersEnabled2 ,
                                              AV18DynamicFiltersSelector2 ,
                                              AV19UnidadeMedicao_Nome2 ,
                                              AV20DynamicFiltersEnabled3 ,
                                              AV21DynamicFiltersSelector3 ,
                                              AV22UnidadeMedicao_Nome3 ,
                                              AV29TFUnidadeMedicao_Nome_Sel ,
                                              AV28TFUnidadeMedicao_Nome ,
                                              AV33TFUnidadeMedicao_Sigla_Sel ,
                                              AV32TFUnidadeMedicao_Sigla ,
                                              AV36TFUnidadeMedicao_Ativo_Sel ,
                                              A1198UnidadeMedicao_Nome ,
                                              A1200UnidadeMedicao_Sigla ,
                                              A1199UnidadeMedicao_Ativo ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV16UnidadeMedicao_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV16UnidadeMedicao_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16UnidadeMedicao_Nome1", AV16UnidadeMedicao_Nome1);
         lV19UnidadeMedicao_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV19UnidadeMedicao_Nome2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19UnidadeMedicao_Nome2", AV19UnidadeMedicao_Nome2);
         lV22UnidadeMedicao_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV22UnidadeMedicao_Nome3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22UnidadeMedicao_Nome3", AV22UnidadeMedicao_Nome3);
         lV28TFUnidadeMedicao_Nome = StringUtil.PadR( StringUtil.RTrim( AV28TFUnidadeMedicao_Nome), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28TFUnidadeMedicao_Nome", AV28TFUnidadeMedicao_Nome);
         lV32TFUnidadeMedicao_Sigla = StringUtil.PadR( StringUtil.RTrim( AV32TFUnidadeMedicao_Sigla), 15, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFUnidadeMedicao_Sigla", AV32TFUnidadeMedicao_Sigla);
         /* Using cursor H00IP3 */
         pr_default.execute(1, new Object[] {lV16UnidadeMedicao_Nome1, lV19UnidadeMedicao_Nome2, lV22UnidadeMedicao_Nome3, lV28TFUnidadeMedicao_Nome, AV29TFUnidadeMedicao_Nome_Sel, lV32TFUnidadeMedicao_Sigla, AV33TFUnidadeMedicao_Sigla_Sel});
         GRID_nRecordCount = H00IP3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16UnidadeMedicao_Nome1, AV18DynamicFiltersSelector2, AV19UnidadeMedicao_Nome2, AV21DynamicFiltersSelector3, AV22UnidadeMedicao_Nome3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV28TFUnidadeMedicao_Nome, AV29TFUnidadeMedicao_Nome_Sel, AV32TFUnidadeMedicao_Sigla, AV33TFUnidadeMedicao_Sigla_Sel, AV36TFUnidadeMedicao_Ativo_Sel, AV30ddo_UnidadeMedicao_NomeTitleControlIdToReplace, AV34ddo_UnidadeMedicao_SiglaTitleControlIdToReplace, AV37ddo_UnidadeMedicao_AtivoTitleControlIdToReplace, AV45Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16UnidadeMedicao_Nome1, AV18DynamicFiltersSelector2, AV19UnidadeMedicao_Nome2, AV21DynamicFiltersSelector3, AV22UnidadeMedicao_Nome3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV28TFUnidadeMedicao_Nome, AV29TFUnidadeMedicao_Nome_Sel, AV32TFUnidadeMedicao_Sigla, AV33TFUnidadeMedicao_Sigla_Sel, AV36TFUnidadeMedicao_Ativo_Sel, AV30ddo_UnidadeMedicao_NomeTitleControlIdToReplace, AV34ddo_UnidadeMedicao_SiglaTitleControlIdToReplace, AV37ddo_UnidadeMedicao_AtivoTitleControlIdToReplace, AV45Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16UnidadeMedicao_Nome1, AV18DynamicFiltersSelector2, AV19UnidadeMedicao_Nome2, AV21DynamicFiltersSelector3, AV22UnidadeMedicao_Nome3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV28TFUnidadeMedicao_Nome, AV29TFUnidadeMedicao_Nome_Sel, AV32TFUnidadeMedicao_Sigla, AV33TFUnidadeMedicao_Sigla_Sel, AV36TFUnidadeMedicao_Ativo_Sel, AV30ddo_UnidadeMedicao_NomeTitleControlIdToReplace, AV34ddo_UnidadeMedicao_SiglaTitleControlIdToReplace, AV37ddo_UnidadeMedicao_AtivoTitleControlIdToReplace, AV45Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16UnidadeMedicao_Nome1, AV18DynamicFiltersSelector2, AV19UnidadeMedicao_Nome2, AV21DynamicFiltersSelector3, AV22UnidadeMedicao_Nome3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV28TFUnidadeMedicao_Nome, AV29TFUnidadeMedicao_Nome_Sel, AV32TFUnidadeMedicao_Sigla, AV33TFUnidadeMedicao_Sigla_Sel, AV36TFUnidadeMedicao_Ativo_Sel, AV30ddo_UnidadeMedicao_NomeTitleControlIdToReplace, AV34ddo_UnidadeMedicao_SiglaTitleControlIdToReplace, AV37ddo_UnidadeMedicao_AtivoTitleControlIdToReplace, AV45Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16UnidadeMedicao_Nome1, AV18DynamicFiltersSelector2, AV19UnidadeMedicao_Nome2, AV21DynamicFiltersSelector3, AV22UnidadeMedicao_Nome3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV28TFUnidadeMedicao_Nome, AV29TFUnidadeMedicao_Nome_Sel, AV32TFUnidadeMedicao_Sigla, AV33TFUnidadeMedicao_Sigla_Sel, AV36TFUnidadeMedicao_Ativo_Sel, AV30ddo_UnidadeMedicao_NomeTitleControlIdToReplace, AV34ddo_UnidadeMedicao_SiglaTitleControlIdToReplace, AV37ddo_UnidadeMedicao_AtivoTitleControlIdToReplace, AV45Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving) ;
         }
         return (int)(0) ;
      }

      protected void STRUPIP0( )
      {
         /* Before Start, stand alone formulas. */
         AV45Pgmname = "PromptUnidadeMedicao";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E25IP2 */
         E25IP2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV38DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vUNIDADEMEDICAO_NOMETITLEFILTERDATA"), AV27UnidadeMedicao_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vUNIDADEMEDICAO_SIGLATITLEFILTERDATA"), AV31UnidadeMedicao_SiglaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vUNIDADEMEDICAO_ATIVOTITLEFILTERDATA"), AV35UnidadeMedicao_AtivoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            AV16UnidadeMedicao_Nome1 = StringUtil.Upper( cgiGet( edtavUnidademedicao_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16UnidadeMedicao_Nome1", AV16UnidadeMedicao_Nome1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV18DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
            AV19UnidadeMedicao_Nome2 = StringUtil.Upper( cgiGet( edtavUnidademedicao_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19UnidadeMedicao_Nome2", AV19UnidadeMedicao_Nome2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV21DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
            AV22UnidadeMedicao_Nome3 = StringUtil.Upper( cgiGet( edtavUnidademedicao_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22UnidadeMedicao_Nome3", AV22UnidadeMedicao_Nome3);
            AV17DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
            AV20DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
            AV28TFUnidadeMedicao_Nome = StringUtil.Upper( cgiGet( edtavTfunidademedicao_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28TFUnidadeMedicao_Nome", AV28TFUnidadeMedicao_Nome);
            AV29TFUnidadeMedicao_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfunidademedicao_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29TFUnidadeMedicao_Nome_Sel", AV29TFUnidadeMedicao_Nome_Sel);
            AV32TFUnidadeMedicao_Sigla = StringUtil.Upper( cgiGet( edtavTfunidademedicao_sigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFUnidadeMedicao_Sigla", AV32TFUnidadeMedicao_Sigla);
            AV33TFUnidadeMedicao_Sigla_Sel = StringUtil.Upper( cgiGet( edtavTfunidademedicao_sigla_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TFUnidadeMedicao_Sigla_Sel", AV33TFUnidadeMedicao_Sigla_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfunidademedicao_ativo_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfunidademedicao_ativo_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFUNIDADEMEDICAO_ATIVO_SEL");
               GX_FocusControl = edtavTfunidademedicao_ativo_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV36TFUnidadeMedicao_Ativo_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFUnidadeMedicao_Ativo_Sel", StringUtil.Str( (decimal)(AV36TFUnidadeMedicao_Ativo_Sel), 1, 0));
            }
            else
            {
               AV36TFUnidadeMedicao_Ativo_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfunidademedicao_ativo_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFUnidadeMedicao_Ativo_Sel", StringUtil.Str( (decimal)(AV36TFUnidadeMedicao_Ativo_Sel), 1, 0));
            }
            AV30ddo_UnidadeMedicao_NomeTitleControlIdToReplace = cgiGet( edtavDdo_unidademedicao_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ddo_UnidadeMedicao_NomeTitleControlIdToReplace", AV30ddo_UnidadeMedicao_NomeTitleControlIdToReplace);
            AV34ddo_UnidadeMedicao_SiglaTitleControlIdToReplace = cgiGet( edtavDdo_unidademedicao_siglatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ddo_UnidadeMedicao_SiglaTitleControlIdToReplace", AV34ddo_UnidadeMedicao_SiglaTitleControlIdToReplace);
            AV37ddo_UnidadeMedicao_AtivoTitleControlIdToReplace = cgiGet( edtavDdo_unidademedicao_ativotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_UnidadeMedicao_AtivoTitleControlIdToReplace", AV37ddo_UnidadeMedicao_AtivoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_65 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_65"), ",", "."));
            AV40GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV41GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_unidademedicao_nome_Caption = cgiGet( "DDO_UNIDADEMEDICAO_NOME_Caption");
            Ddo_unidademedicao_nome_Tooltip = cgiGet( "DDO_UNIDADEMEDICAO_NOME_Tooltip");
            Ddo_unidademedicao_nome_Cls = cgiGet( "DDO_UNIDADEMEDICAO_NOME_Cls");
            Ddo_unidademedicao_nome_Filteredtext_set = cgiGet( "DDO_UNIDADEMEDICAO_NOME_Filteredtext_set");
            Ddo_unidademedicao_nome_Selectedvalue_set = cgiGet( "DDO_UNIDADEMEDICAO_NOME_Selectedvalue_set");
            Ddo_unidademedicao_nome_Dropdownoptionstype = cgiGet( "DDO_UNIDADEMEDICAO_NOME_Dropdownoptionstype");
            Ddo_unidademedicao_nome_Titlecontrolidtoreplace = cgiGet( "DDO_UNIDADEMEDICAO_NOME_Titlecontrolidtoreplace");
            Ddo_unidademedicao_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_UNIDADEMEDICAO_NOME_Includesortasc"));
            Ddo_unidademedicao_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_UNIDADEMEDICAO_NOME_Includesortdsc"));
            Ddo_unidademedicao_nome_Sortedstatus = cgiGet( "DDO_UNIDADEMEDICAO_NOME_Sortedstatus");
            Ddo_unidademedicao_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_UNIDADEMEDICAO_NOME_Includefilter"));
            Ddo_unidademedicao_nome_Filtertype = cgiGet( "DDO_UNIDADEMEDICAO_NOME_Filtertype");
            Ddo_unidademedicao_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_UNIDADEMEDICAO_NOME_Filterisrange"));
            Ddo_unidademedicao_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_UNIDADEMEDICAO_NOME_Includedatalist"));
            Ddo_unidademedicao_nome_Datalisttype = cgiGet( "DDO_UNIDADEMEDICAO_NOME_Datalisttype");
            Ddo_unidademedicao_nome_Datalistfixedvalues = cgiGet( "DDO_UNIDADEMEDICAO_NOME_Datalistfixedvalues");
            Ddo_unidademedicao_nome_Datalistproc = cgiGet( "DDO_UNIDADEMEDICAO_NOME_Datalistproc");
            Ddo_unidademedicao_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_UNIDADEMEDICAO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_unidademedicao_nome_Sortasc = cgiGet( "DDO_UNIDADEMEDICAO_NOME_Sortasc");
            Ddo_unidademedicao_nome_Sortdsc = cgiGet( "DDO_UNIDADEMEDICAO_NOME_Sortdsc");
            Ddo_unidademedicao_nome_Loadingdata = cgiGet( "DDO_UNIDADEMEDICAO_NOME_Loadingdata");
            Ddo_unidademedicao_nome_Cleanfilter = cgiGet( "DDO_UNIDADEMEDICAO_NOME_Cleanfilter");
            Ddo_unidademedicao_nome_Rangefilterfrom = cgiGet( "DDO_UNIDADEMEDICAO_NOME_Rangefilterfrom");
            Ddo_unidademedicao_nome_Rangefilterto = cgiGet( "DDO_UNIDADEMEDICAO_NOME_Rangefilterto");
            Ddo_unidademedicao_nome_Noresultsfound = cgiGet( "DDO_UNIDADEMEDICAO_NOME_Noresultsfound");
            Ddo_unidademedicao_nome_Searchbuttontext = cgiGet( "DDO_UNIDADEMEDICAO_NOME_Searchbuttontext");
            Ddo_unidademedicao_sigla_Caption = cgiGet( "DDO_UNIDADEMEDICAO_SIGLA_Caption");
            Ddo_unidademedicao_sigla_Tooltip = cgiGet( "DDO_UNIDADEMEDICAO_SIGLA_Tooltip");
            Ddo_unidademedicao_sigla_Cls = cgiGet( "DDO_UNIDADEMEDICAO_SIGLA_Cls");
            Ddo_unidademedicao_sigla_Filteredtext_set = cgiGet( "DDO_UNIDADEMEDICAO_SIGLA_Filteredtext_set");
            Ddo_unidademedicao_sigla_Selectedvalue_set = cgiGet( "DDO_UNIDADEMEDICAO_SIGLA_Selectedvalue_set");
            Ddo_unidademedicao_sigla_Dropdownoptionstype = cgiGet( "DDO_UNIDADEMEDICAO_SIGLA_Dropdownoptionstype");
            Ddo_unidademedicao_sigla_Titlecontrolidtoreplace = cgiGet( "DDO_UNIDADEMEDICAO_SIGLA_Titlecontrolidtoreplace");
            Ddo_unidademedicao_sigla_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_UNIDADEMEDICAO_SIGLA_Includesortasc"));
            Ddo_unidademedicao_sigla_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_UNIDADEMEDICAO_SIGLA_Includesortdsc"));
            Ddo_unidademedicao_sigla_Sortedstatus = cgiGet( "DDO_UNIDADEMEDICAO_SIGLA_Sortedstatus");
            Ddo_unidademedicao_sigla_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_UNIDADEMEDICAO_SIGLA_Includefilter"));
            Ddo_unidademedicao_sigla_Filtertype = cgiGet( "DDO_UNIDADEMEDICAO_SIGLA_Filtertype");
            Ddo_unidademedicao_sigla_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_UNIDADEMEDICAO_SIGLA_Filterisrange"));
            Ddo_unidademedicao_sigla_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_UNIDADEMEDICAO_SIGLA_Includedatalist"));
            Ddo_unidademedicao_sigla_Datalisttype = cgiGet( "DDO_UNIDADEMEDICAO_SIGLA_Datalisttype");
            Ddo_unidademedicao_sigla_Datalistfixedvalues = cgiGet( "DDO_UNIDADEMEDICAO_SIGLA_Datalistfixedvalues");
            Ddo_unidademedicao_sigla_Datalistproc = cgiGet( "DDO_UNIDADEMEDICAO_SIGLA_Datalistproc");
            Ddo_unidademedicao_sigla_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_UNIDADEMEDICAO_SIGLA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_unidademedicao_sigla_Sortasc = cgiGet( "DDO_UNIDADEMEDICAO_SIGLA_Sortasc");
            Ddo_unidademedicao_sigla_Sortdsc = cgiGet( "DDO_UNIDADEMEDICAO_SIGLA_Sortdsc");
            Ddo_unidademedicao_sigla_Loadingdata = cgiGet( "DDO_UNIDADEMEDICAO_SIGLA_Loadingdata");
            Ddo_unidademedicao_sigla_Cleanfilter = cgiGet( "DDO_UNIDADEMEDICAO_SIGLA_Cleanfilter");
            Ddo_unidademedicao_sigla_Rangefilterfrom = cgiGet( "DDO_UNIDADEMEDICAO_SIGLA_Rangefilterfrom");
            Ddo_unidademedicao_sigla_Rangefilterto = cgiGet( "DDO_UNIDADEMEDICAO_SIGLA_Rangefilterto");
            Ddo_unidademedicao_sigla_Noresultsfound = cgiGet( "DDO_UNIDADEMEDICAO_SIGLA_Noresultsfound");
            Ddo_unidademedicao_sigla_Searchbuttontext = cgiGet( "DDO_UNIDADEMEDICAO_SIGLA_Searchbuttontext");
            Ddo_unidademedicao_ativo_Caption = cgiGet( "DDO_UNIDADEMEDICAO_ATIVO_Caption");
            Ddo_unidademedicao_ativo_Tooltip = cgiGet( "DDO_UNIDADEMEDICAO_ATIVO_Tooltip");
            Ddo_unidademedicao_ativo_Cls = cgiGet( "DDO_UNIDADEMEDICAO_ATIVO_Cls");
            Ddo_unidademedicao_ativo_Selectedvalue_set = cgiGet( "DDO_UNIDADEMEDICAO_ATIVO_Selectedvalue_set");
            Ddo_unidademedicao_ativo_Dropdownoptionstype = cgiGet( "DDO_UNIDADEMEDICAO_ATIVO_Dropdownoptionstype");
            Ddo_unidademedicao_ativo_Titlecontrolidtoreplace = cgiGet( "DDO_UNIDADEMEDICAO_ATIVO_Titlecontrolidtoreplace");
            Ddo_unidademedicao_ativo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_UNIDADEMEDICAO_ATIVO_Includesortasc"));
            Ddo_unidademedicao_ativo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_UNIDADEMEDICAO_ATIVO_Includesortdsc"));
            Ddo_unidademedicao_ativo_Sortedstatus = cgiGet( "DDO_UNIDADEMEDICAO_ATIVO_Sortedstatus");
            Ddo_unidademedicao_ativo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_UNIDADEMEDICAO_ATIVO_Includefilter"));
            Ddo_unidademedicao_ativo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_UNIDADEMEDICAO_ATIVO_Filterisrange"));
            Ddo_unidademedicao_ativo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_UNIDADEMEDICAO_ATIVO_Includedatalist"));
            Ddo_unidademedicao_ativo_Datalisttype = cgiGet( "DDO_UNIDADEMEDICAO_ATIVO_Datalisttype");
            Ddo_unidademedicao_ativo_Datalistfixedvalues = cgiGet( "DDO_UNIDADEMEDICAO_ATIVO_Datalistfixedvalues");
            Ddo_unidademedicao_ativo_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_UNIDADEMEDICAO_ATIVO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_unidademedicao_ativo_Sortasc = cgiGet( "DDO_UNIDADEMEDICAO_ATIVO_Sortasc");
            Ddo_unidademedicao_ativo_Sortdsc = cgiGet( "DDO_UNIDADEMEDICAO_ATIVO_Sortdsc");
            Ddo_unidademedicao_ativo_Loadingdata = cgiGet( "DDO_UNIDADEMEDICAO_ATIVO_Loadingdata");
            Ddo_unidademedicao_ativo_Cleanfilter = cgiGet( "DDO_UNIDADEMEDICAO_ATIVO_Cleanfilter");
            Ddo_unidademedicao_ativo_Rangefilterfrom = cgiGet( "DDO_UNIDADEMEDICAO_ATIVO_Rangefilterfrom");
            Ddo_unidademedicao_ativo_Rangefilterto = cgiGet( "DDO_UNIDADEMEDICAO_ATIVO_Rangefilterto");
            Ddo_unidademedicao_ativo_Noresultsfound = cgiGet( "DDO_UNIDADEMEDICAO_ATIVO_Noresultsfound");
            Ddo_unidademedicao_ativo_Searchbuttontext = cgiGet( "DDO_UNIDADEMEDICAO_ATIVO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_unidademedicao_nome_Activeeventkey = cgiGet( "DDO_UNIDADEMEDICAO_NOME_Activeeventkey");
            Ddo_unidademedicao_nome_Filteredtext_get = cgiGet( "DDO_UNIDADEMEDICAO_NOME_Filteredtext_get");
            Ddo_unidademedicao_nome_Selectedvalue_get = cgiGet( "DDO_UNIDADEMEDICAO_NOME_Selectedvalue_get");
            Ddo_unidademedicao_sigla_Activeeventkey = cgiGet( "DDO_UNIDADEMEDICAO_SIGLA_Activeeventkey");
            Ddo_unidademedicao_sigla_Filteredtext_get = cgiGet( "DDO_UNIDADEMEDICAO_SIGLA_Filteredtext_get");
            Ddo_unidademedicao_sigla_Selectedvalue_get = cgiGet( "DDO_UNIDADEMEDICAO_SIGLA_Selectedvalue_get");
            Ddo_unidademedicao_ativo_Activeeventkey = cgiGet( "DDO_UNIDADEMEDICAO_ATIVO_Activeeventkey");
            Ddo_unidademedicao_ativo_Selectedvalue_get = cgiGet( "DDO_UNIDADEMEDICAO_ATIVO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vUNIDADEMEDICAO_NOME1"), AV16UnidadeMedicao_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV18DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vUNIDADEMEDICAO_NOME2"), AV19UnidadeMedicao_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV21DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vUNIDADEMEDICAO_NOME3"), AV22UnidadeMedicao_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV17DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV20DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUNIDADEMEDICAO_NOME"), AV28TFUnidadeMedicao_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUNIDADEMEDICAO_NOME_SEL"), AV29TFUnidadeMedicao_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUNIDADEMEDICAO_SIGLA"), AV32TFUnidadeMedicao_Sigla) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUNIDADEMEDICAO_SIGLA_SEL"), AV33TFUnidadeMedicao_Sigla_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFUNIDADEMEDICAO_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV36TFUnidadeMedicao_Ativo_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E25IP2 */
         E25IP2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E25IP2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "UNIDADEMEDICAO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV18DynamicFiltersSelector2 = "UNIDADEMEDICAO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV21DynamicFiltersSelector3 = "UNIDADEMEDICAO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfunidademedicao_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfunidademedicao_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfunidademedicao_nome_Visible), 5, 0)));
         edtavTfunidademedicao_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfunidademedicao_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfunidademedicao_nome_sel_Visible), 5, 0)));
         edtavTfunidademedicao_sigla_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfunidademedicao_sigla_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfunidademedicao_sigla_Visible), 5, 0)));
         edtavTfunidademedicao_sigla_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfunidademedicao_sigla_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfunidademedicao_sigla_sel_Visible), 5, 0)));
         edtavTfunidademedicao_ativo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfunidademedicao_ativo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfunidademedicao_ativo_sel_Visible), 5, 0)));
         Ddo_unidademedicao_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_UnidadeMedicao_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_unidademedicao_nome_Internalname, "TitleControlIdToReplace", Ddo_unidademedicao_nome_Titlecontrolidtoreplace);
         AV30ddo_UnidadeMedicao_NomeTitleControlIdToReplace = Ddo_unidademedicao_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ddo_UnidadeMedicao_NomeTitleControlIdToReplace", AV30ddo_UnidadeMedicao_NomeTitleControlIdToReplace);
         edtavDdo_unidademedicao_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_unidademedicao_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_unidademedicao_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_unidademedicao_sigla_Titlecontrolidtoreplace = subGrid_Internalname+"_UnidadeMedicao_Sigla";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_unidademedicao_sigla_Internalname, "TitleControlIdToReplace", Ddo_unidademedicao_sigla_Titlecontrolidtoreplace);
         AV34ddo_UnidadeMedicao_SiglaTitleControlIdToReplace = Ddo_unidademedicao_sigla_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ddo_UnidadeMedicao_SiglaTitleControlIdToReplace", AV34ddo_UnidadeMedicao_SiglaTitleControlIdToReplace);
         edtavDdo_unidademedicao_siglatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_unidademedicao_siglatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_unidademedicao_siglatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_unidademedicao_ativo_Titlecontrolidtoreplace = subGrid_Internalname+"_UnidadeMedicao_Ativo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_unidademedicao_ativo_Internalname, "TitleControlIdToReplace", Ddo_unidademedicao_ativo_Titlecontrolidtoreplace);
         AV37ddo_UnidadeMedicao_AtivoTitleControlIdToReplace = Ddo_unidademedicao_ativo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_UnidadeMedicao_AtivoTitleControlIdToReplace", AV37ddo_UnidadeMedicao_AtivoTitleControlIdToReplace);
         edtavDdo_unidademedicao_ativotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_unidademedicao_ativotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_unidademedicao_ativotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione Unidades de Medi��o";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Nome", 0);
         cmbavOrderedby.addItem("2", "Sigla", 0);
         cmbavOrderedby.addItem("3", "Ativo?", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV38DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV38DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E26IP2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV27UnidadeMedicao_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV31UnidadeMedicao_SiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV35UnidadeMedicao_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtUnidadeMedicao_Nome_Titleformat = 2;
         edtUnidadeMedicao_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV30ddo_UnidadeMedicao_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUnidadeMedicao_Nome_Internalname, "Title", edtUnidadeMedicao_Nome_Title);
         edtUnidadeMedicao_Sigla_Titleformat = 2;
         edtUnidadeMedicao_Sigla_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Sigla", AV34ddo_UnidadeMedicao_SiglaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUnidadeMedicao_Sigla_Internalname, "Title", edtUnidadeMedicao_Sigla_Title);
         chkUnidadeMedicao_Ativo_Titleformat = 2;
         chkUnidadeMedicao_Ativo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ativo?", AV37ddo_UnidadeMedicao_AtivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUnidadeMedicao_Ativo_Internalname, "Title", chkUnidadeMedicao_Ativo.Title.Text);
         AV40GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40GridCurrentPage), 10, 0)));
         AV41GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV27UnidadeMedicao_NomeTitleFilterData", AV27UnidadeMedicao_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV31UnidadeMedicao_SiglaTitleFilterData", AV31UnidadeMedicao_SiglaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35UnidadeMedicao_AtivoTitleFilterData", AV35UnidadeMedicao_AtivoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11IP2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV39PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV39PageToGo) ;
         }
      }

      protected void E12IP2( )
      {
         /* Ddo_unidademedicao_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_unidademedicao_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_unidademedicao_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_unidademedicao_nome_Internalname, "SortedStatus", Ddo_unidademedicao_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_unidademedicao_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_unidademedicao_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_unidademedicao_nome_Internalname, "SortedStatus", Ddo_unidademedicao_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_unidademedicao_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV28TFUnidadeMedicao_Nome = Ddo_unidademedicao_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28TFUnidadeMedicao_Nome", AV28TFUnidadeMedicao_Nome);
            AV29TFUnidadeMedicao_Nome_Sel = Ddo_unidademedicao_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29TFUnidadeMedicao_Nome_Sel", AV29TFUnidadeMedicao_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13IP2( )
      {
         /* Ddo_unidademedicao_sigla_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_unidademedicao_sigla_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_unidademedicao_sigla_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_unidademedicao_sigla_Internalname, "SortedStatus", Ddo_unidademedicao_sigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_unidademedicao_sigla_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_unidademedicao_sigla_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_unidademedicao_sigla_Internalname, "SortedStatus", Ddo_unidademedicao_sigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_unidademedicao_sigla_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV32TFUnidadeMedicao_Sigla = Ddo_unidademedicao_sigla_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFUnidadeMedicao_Sigla", AV32TFUnidadeMedicao_Sigla);
            AV33TFUnidadeMedicao_Sigla_Sel = Ddo_unidademedicao_sigla_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TFUnidadeMedicao_Sigla_Sel", AV33TFUnidadeMedicao_Sigla_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14IP2( )
      {
         /* Ddo_unidademedicao_ativo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_unidademedicao_ativo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_unidademedicao_ativo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_unidademedicao_ativo_Internalname, "SortedStatus", Ddo_unidademedicao_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_unidademedicao_ativo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_unidademedicao_ativo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_unidademedicao_ativo_Internalname, "SortedStatus", Ddo_unidademedicao_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_unidademedicao_ativo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV36TFUnidadeMedicao_Ativo_Sel = (short)(NumberUtil.Val( Ddo_unidademedicao_ativo_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFUnidadeMedicao_Ativo_Sel", StringUtil.Str( (decimal)(AV36TFUnidadeMedicao_Ativo_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E27IP2( )
      {
         /* Grid_Load Routine */
         AV25Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV25Select);
         AV44Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 65;
         }
         sendrow_652( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_65_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(65, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E28IP2 */
         E28IP2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E28IP2( )
      {
         /* Enter Routine */
         AV7InOutUnidadeMedicao_Codigo = A1197UnidadeMedicao_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutUnidadeMedicao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutUnidadeMedicao_Codigo), 6, 0)));
         AV8InOutUnidadeMedicao_Nome = A1198UnidadeMedicao_Nome;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutUnidadeMedicao_Nome", AV8InOutUnidadeMedicao_Nome);
         context.setWebReturnParms(new Object[] {(int)AV7InOutUnidadeMedicao_Codigo,(String)AV8InOutUnidadeMedicao_Nome});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E15IP2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E20IP2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV17DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E16IP2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV23DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV24DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersIgnoreFirst", AV24DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV24DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersIgnoreFirst", AV24DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16UnidadeMedicao_Nome1, AV18DynamicFiltersSelector2, AV19UnidadeMedicao_Nome2, AV21DynamicFiltersSelector3, AV22UnidadeMedicao_Nome3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV28TFUnidadeMedicao_Nome, AV29TFUnidadeMedicao_Nome_Sel, AV32TFUnidadeMedicao_Sigla, AV33TFUnidadeMedicao_Sigla_Sel, AV36TFUnidadeMedicao_Ativo_Sel, AV30ddo_UnidadeMedicao_NomeTitleControlIdToReplace, AV34ddo_UnidadeMedicao_SiglaTitleControlIdToReplace, AV37ddo_UnidadeMedicao_AtivoTitleControlIdToReplace, AV45Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E21IP2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E22IP2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV20DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E17IP2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV23DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV17DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16UnidadeMedicao_Nome1, AV18DynamicFiltersSelector2, AV19UnidadeMedicao_Nome2, AV21DynamicFiltersSelector3, AV22UnidadeMedicao_Nome3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV28TFUnidadeMedicao_Nome, AV29TFUnidadeMedicao_Nome_Sel, AV32TFUnidadeMedicao_Sigla, AV33TFUnidadeMedicao_Sigla_Sel, AV36TFUnidadeMedicao_Ativo_Sel, AV30ddo_UnidadeMedicao_NomeTitleControlIdToReplace, AV34ddo_UnidadeMedicao_SiglaTitleControlIdToReplace, AV37ddo_UnidadeMedicao_AtivoTitleControlIdToReplace, AV45Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E23IP2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E18IP2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV23DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV20DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16UnidadeMedicao_Nome1, AV18DynamicFiltersSelector2, AV19UnidadeMedicao_Nome2, AV21DynamicFiltersSelector3, AV22UnidadeMedicao_Nome3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV28TFUnidadeMedicao_Nome, AV29TFUnidadeMedicao_Nome_Sel, AV32TFUnidadeMedicao_Sigla, AV33TFUnidadeMedicao_Sigla_Sel, AV36TFUnidadeMedicao_Ativo_Sel, AV30ddo_UnidadeMedicao_NomeTitleControlIdToReplace, AV34ddo_UnidadeMedicao_SiglaTitleControlIdToReplace, AV37ddo_UnidadeMedicao_AtivoTitleControlIdToReplace, AV45Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E24IP2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E19IP2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_unidademedicao_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_unidademedicao_nome_Internalname, "SortedStatus", Ddo_unidademedicao_nome_Sortedstatus);
         Ddo_unidademedicao_sigla_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_unidademedicao_sigla_Internalname, "SortedStatus", Ddo_unidademedicao_sigla_Sortedstatus);
         Ddo_unidademedicao_ativo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_unidademedicao_ativo_Internalname, "SortedStatus", Ddo_unidademedicao_ativo_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_unidademedicao_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_unidademedicao_nome_Internalname, "SortedStatus", Ddo_unidademedicao_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_unidademedicao_sigla_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_unidademedicao_sigla_Internalname, "SortedStatus", Ddo_unidademedicao_sigla_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_unidademedicao_ativo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_unidademedicao_ativo_Internalname, "SortedStatus", Ddo_unidademedicao_ativo_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavUnidademedicao_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUnidademedicao_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUnidademedicao_nome1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "UNIDADEMEDICAO_NOME") == 0 )
         {
            edtavUnidademedicao_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUnidademedicao_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUnidademedicao_nome1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavUnidademedicao_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUnidademedicao_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUnidademedicao_nome2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "UNIDADEMEDICAO_NOME") == 0 )
         {
            edtavUnidademedicao_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUnidademedicao_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUnidademedicao_nome2_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavUnidademedicao_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUnidademedicao_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUnidademedicao_nome3_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "UNIDADEMEDICAO_NOME") == 0 )
         {
            edtavUnidademedicao_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUnidademedicao_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUnidademedicao_nome3_Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV17DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
         AV18DynamicFiltersSelector2 = "UNIDADEMEDICAO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
         AV19UnidadeMedicao_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19UnidadeMedicao_Nome2", AV19UnidadeMedicao_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV20DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
         AV21DynamicFiltersSelector3 = "UNIDADEMEDICAO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
         AV22UnidadeMedicao_Nome3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22UnidadeMedicao_Nome3", AV22UnidadeMedicao_Nome3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'CLEANFILTERS' Routine */
         AV28TFUnidadeMedicao_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28TFUnidadeMedicao_Nome", AV28TFUnidadeMedicao_Nome);
         Ddo_unidademedicao_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_unidademedicao_nome_Internalname, "FilteredText_set", Ddo_unidademedicao_nome_Filteredtext_set);
         AV29TFUnidadeMedicao_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29TFUnidadeMedicao_Nome_Sel", AV29TFUnidadeMedicao_Nome_Sel);
         Ddo_unidademedicao_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_unidademedicao_nome_Internalname, "SelectedValue_set", Ddo_unidademedicao_nome_Selectedvalue_set);
         AV32TFUnidadeMedicao_Sigla = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFUnidadeMedicao_Sigla", AV32TFUnidadeMedicao_Sigla);
         Ddo_unidademedicao_sigla_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_unidademedicao_sigla_Internalname, "FilteredText_set", Ddo_unidademedicao_sigla_Filteredtext_set);
         AV33TFUnidadeMedicao_Sigla_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TFUnidadeMedicao_Sigla_Sel", AV33TFUnidadeMedicao_Sigla_Sel);
         Ddo_unidademedicao_sigla_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_unidademedicao_sigla_Internalname, "SelectedValue_set", Ddo_unidademedicao_sigla_Selectedvalue_set);
         AV36TFUnidadeMedicao_Ativo_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFUnidadeMedicao_Ativo_Sel", StringUtil.Str( (decimal)(AV36TFUnidadeMedicao_Ativo_Sel), 1, 0));
         Ddo_unidademedicao_ativo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_unidademedicao_ativo_Internalname, "SelectedValue_set", Ddo_unidademedicao_ativo_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "UNIDADEMEDICAO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16UnidadeMedicao_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16UnidadeMedicao_Nome1", AV16UnidadeMedicao_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "UNIDADEMEDICAO_NOME") == 0 )
            {
               AV16UnidadeMedicao_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16UnidadeMedicao_Nome1", AV16UnidadeMedicao_Nome1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV17DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV18DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "UNIDADEMEDICAO_NOME") == 0 )
               {
                  AV19UnidadeMedicao_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19UnidadeMedicao_Nome2", AV19UnidadeMedicao_Nome2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV20DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV21DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "UNIDADEMEDICAO_NOME") == 0 )
                  {
                     AV22UnidadeMedicao_Nome3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22UnidadeMedicao_Nome3", AV22UnidadeMedicao_Nome3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV23DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28TFUnidadeMedicao_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFUNIDADEMEDICAO_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV28TFUnidadeMedicao_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29TFUnidadeMedicao_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFUNIDADEMEDICAO_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV29TFUnidadeMedicao_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32TFUnidadeMedicao_Sigla)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFUNIDADEMEDICAO_SIGLA";
            AV11GridStateFilterValue.gxTpr_Value = AV32TFUnidadeMedicao_Sigla;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33TFUnidadeMedicao_Sigla_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFUNIDADEMEDICAO_SIGLA_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV33TFUnidadeMedicao_Sigla_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV36TFUnidadeMedicao_Ativo_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFUNIDADEMEDICAO_ATIVO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV36TFUnidadeMedicao_Ativo_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV45Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV24DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "UNIDADEMEDICAO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV16UnidadeMedicao_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV16UnidadeMedicao_Nome1;
            }
            if ( AV23DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV17DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV18DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "UNIDADEMEDICAO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV19UnidadeMedicao_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV19UnidadeMedicao_Nome2;
            }
            if ( AV23DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV20DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV21DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "UNIDADEMEDICAO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV22UnidadeMedicao_Nome3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV22UnidadeMedicao_Nome3;
            }
            if ( AV23DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_IP2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_IP2( true) ;
         }
         else
         {
            wb_table2_5_IP2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_IP2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_59_IP2( true) ;
         }
         else
         {
            wb_table3_59_IP2( false) ;
         }
         return  ;
      }

      protected void wb_table3_59_IP2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_IP2e( true) ;
         }
         else
         {
            wb_table1_2_IP2e( false) ;
         }
      }

      protected void wb_table3_59_IP2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_62_IP2( true) ;
         }
         else
         {
            wb_table4_62_IP2( false) ;
         }
         return  ;
      }

      protected void wb_table4_62_IP2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_59_IP2e( true) ;
         }
         else
         {
            wb_table3_59_IP2e( false) ;
         }
      }

      protected void wb_table4_62_IP2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"65\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtUnidadeMedicao_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtUnidadeMedicao_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtUnidadeMedicao_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtUnidadeMedicao_Sigla_Titleformat == 0 )
               {
                  context.SendWebValue( edtUnidadeMedicao_Sigla_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtUnidadeMedicao_Sigla_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkUnidadeMedicao_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( chkUnidadeMedicao_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkUnidadeMedicao_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV25Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1197UnidadeMedicao_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1198UnidadeMedicao_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtUnidadeMedicao_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUnidadeMedicao_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1200UnidadeMedicao_Sigla));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtUnidadeMedicao_Sigla_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUnidadeMedicao_Sigla_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A1199UnidadeMedicao_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkUnidadeMedicao_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkUnidadeMedicao_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 65 )
         {
            wbEnd = 0;
            nRC_GXsfl_65 = (short)(nGXsfl_65_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_62_IP2e( true) ;
         }
         else
         {
            wb_table4_62_IP2e( false) ;
         }
      }

      protected void wb_table2_5_IP2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptUnidadeMedicao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_65_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptUnidadeMedicao.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_65_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptUnidadeMedicao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_14_IP2( true) ;
         }
         else
         {
            wb_table5_14_IP2( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_IP2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_IP2e( true) ;
         }
         else
         {
            wb_table2_5_IP2e( false) ;
         }
      }

      protected void wb_table5_14_IP2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptUnidadeMedicao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_19_IP2( true) ;
         }
         else
         {
            wb_table6_19_IP2( false) ;
         }
         return  ;
      }

      protected void wb_table6_19_IP2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptUnidadeMedicao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_IP2e( true) ;
         }
         else
         {
            wb_table5_14_IP2e( false) ;
         }
      }

      protected void wb_table6_19_IP2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptUnidadeMedicao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_65_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_PromptUnidadeMedicao.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptUnidadeMedicao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'" + sGXsfl_65_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUnidademedicao_nome1_Internalname, StringUtil.RTrim( AV16UnidadeMedicao_Nome1), StringUtil.RTrim( context.localUtil.Format( AV16UnidadeMedicao_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,28);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUnidademedicao_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUnidademedicao_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptUnidadeMedicao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptUnidadeMedicao.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptUnidadeMedicao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptUnidadeMedicao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'" + sGXsfl_65_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV18DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,36);\"", "", true, "HLP_PromptUnidadeMedicao.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptUnidadeMedicao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_65_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUnidademedicao_nome2_Internalname, StringUtil.RTrim( AV19UnidadeMedicao_Nome2), StringUtil.RTrim( context.localUtil.Format( AV19UnidadeMedicao_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,40);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUnidademedicao_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUnidademedicao_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptUnidadeMedicao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptUnidadeMedicao.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptUnidadeMedicao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptUnidadeMedicao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'" + sGXsfl_65_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV21DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,48);\"", "", true, "HLP_PromptUnidadeMedicao.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptUnidadeMedicao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_65_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUnidademedicao_nome3_Internalname, StringUtil.RTrim( AV22UnidadeMedicao_Nome3), StringUtil.RTrim( context.localUtil.Format( AV22UnidadeMedicao_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,52);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUnidademedicao_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUnidademedicao_nome3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptUnidadeMedicao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptUnidadeMedicao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_19_IP2e( true) ;
         }
         else
         {
            wb_table6_19_IP2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutUnidadeMedicao_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutUnidadeMedicao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutUnidadeMedicao_Codigo), 6, 0)));
         AV8InOutUnidadeMedicao_Nome = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutUnidadeMedicao_Nome", AV8InOutUnidadeMedicao_Nome);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAIP2( ) ;
         WSIP2( ) ;
         WEIP2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20204282344269");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptunidademedicao.js", "?20204282344269");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_652( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_65_idx;
         edtUnidadeMedicao_Codigo_Internalname = "UNIDADEMEDICAO_CODIGO_"+sGXsfl_65_idx;
         edtUnidadeMedicao_Nome_Internalname = "UNIDADEMEDICAO_NOME_"+sGXsfl_65_idx;
         edtUnidadeMedicao_Sigla_Internalname = "UNIDADEMEDICAO_SIGLA_"+sGXsfl_65_idx;
         chkUnidadeMedicao_Ativo_Internalname = "UNIDADEMEDICAO_ATIVO_"+sGXsfl_65_idx;
      }

      protected void SubsflControlProps_fel_652( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_65_fel_idx;
         edtUnidadeMedicao_Codigo_Internalname = "UNIDADEMEDICAO_CODIGO_"+sGXsfl_65_fel_idx;
         edtUnidadeMedicao_Nome_Internalname = "UNIDADEMEDICAO_NOME_"+sGXsfl_65_fel_idx;
         edtUnidadeMedicao_Sigla_Internalname = "UNIDADEMEDICAO_SIGLA_"+sGXsfl_65_fel_idx;
         chkUnidadeMedicao_Ativo_Internalname = "UNIDADEMEDICAO_ATIVO_"+sGXsfl_65_fel_idx;
      }

      protected void sendrow_652( )
      {
         SubsflControlProps_652( ) ;
         WBIP0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_65_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_65_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_65_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 66,'',false,'',65)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV25Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV25Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV44Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV25Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV25Select)) ? AV44Select_GXI : context.PathToRelativeUrl( AV25Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_65_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV25Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUnidadeMedicao_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1197UnidadeMedicao_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1197UnidadeMedicao_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUnidadeMedicao_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)65,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUnidadeMedicao_Nome_Internalname,StringUtil.RTrim( A1198UnidadeMedicao_Nome),StringUtil.RTrim( context.localUtil.Format( A1198UnidadeMedicao_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUnidadeMedicao_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)65,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUnidadeMedicao_Sigla_Internalname,StringUtil.RTrim( A1200UnidadeMedicao_Sigla),StringUtil.RTrim( context.localUtil.Format( A1200UnidadeMedicao_Sigla, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUnidadeMedicao_Sigla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)65,(short)1,(short)-1,(short)-1,(bool)true,(String)"Sigla",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkUnidadeMedicao_Ativo_Internalname,StringUtil.BoolToStr( A1199UnidadeMedicao_Ativo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            GxWebStd.gx_hidden_field( context, "gxhash_UNIDADEMEDICAO_CODIGO"+"_"+sGXsfl_65_idx, GetSecureSignedToken( sGXsfl_65_idx, context.localUtil.Format( (decimal)(A1197UnidadeMedicao_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_UNIDADEMEDICAO_NOME"+"_"+sGXsfl_65_idx, GetSecureSignedToken( sGXsfl_65_idx, StringUtil.RTrim( context.localUtil.Format( A1198UnidadeMedicao_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_UNIDADEMEDICAO_SIGLA"+"_"+sGXsfl_65_idx, GetSecureSignedToken( sGXsfl_65_idx, StringUtil.RTrim( context.localUtil.Format( A1200UnidadeMedicao_Sigla, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_UNIDADEMEDICAO_ATIVO"+"_"+sGXsfl_65_idx, GetSecureSignedToken( sGXsfl_65_idx, A1199UnidadeMedicao_Ativo));
            GridContainer.AddRow(GridRow);
            nGXsfl_65_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_65_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_65_idx+1));
            sGXsfl_65_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_65_idx), 4, 0)), 4, "0");
            SubsflControlProps_652( ) ;
         }
         /* End function sendrow_652 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavUnidademedicao_nome1_Internalname = "vUNIDADEMEDICAO_NOME1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavUnidademedicao_nome2_Internalname = "vUNIDADEMEDICAO_NOME2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavUnidademedicao_nome3_Internalname = "vUNIDADEMEDICAO_NOME3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtUnidadeMedicao_Codigo_Internalname = "UNIDADEMEDICAO_CODIGO";
         edtUnidadeMedicao_Nome_Internalname = "UNIDADEMEDICAO_NOME";
         edtUnidadeMedicao_Sigla_Internalname = "UNIDADEMEDICAO_SIGLA";
         chkUnidadeMedicao_Ativo_Internalname = "UNIDADEMEDICAO_ATIVO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfunidademedicao_nome_Internalname = "vTFUNIDADEMEDICAO_NOME";
         edtavTfunidademedicao_nome_sel_Internalname = "vTFUNIDADEMEDICAO_NOME_SEL";
         edtavTfunidademedicao_sigla_Internalname = "vTFUNIDADEMEDICAO_SIGLA";
         edtavTfunidademedicao_sigla_sel_Internalname = "vTFUNIDADEMEDICAO_SIGLA_SEL";
         edtavTfunidademedicao_ativo_sel_Internalname = "vTFUNIDADEMEDICAO_ATIVO_SEL";
         Ddo_unidademedicao_nome_Internalname = "DDO_UNIDADEMEDICAO_NOME";
         edtavDdo_unidademedicao_nometitlecontrolidtoreplace_Internalname = "vDDO_UNIDADEMEDICAO_NOMETITLECONTROLIDTOREPLACE";
         Ddo_unidademedicao_sigla_Internalname = "DDO_UNIDADEMEDICAO_SIGLA";
         edtavDdo_unidademedicao_siglatitlecontrolidtoreplace_Internalname = "vDDO_UNIDADEMEDICAO_SIGLATITLECONTROLIDTOREPLACE";
         Ddo_unidademedicao_ativo_Internalname = "DDO_UNIDADEMEDICAO_ATIVO";
         edtavDdo_unidademedicao_ativotitlecontrolidtoreplace_Internalname = "vDDO_UNIDADEMEDICAO_ATIVOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtUnidadeMedicao_Sigla_Jsonclick = "";
         edtUnidadeMedicao_Nome_Jsonclick = "";
         edtUnidadeMedicao_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavUnidademedicao_nome3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         edtavUnidademedicao_nome2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         edtavUnidademedicao_nome1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         chkUnidadeMedicao_Ativo_Titleformat = 0;
         edtUnidadeMedicao_Sigla_Titleformat = 0;
         edtUnidadeMedicao_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavUnidademedicao_nome3_Visible = 1;
         edtavUnidademedicao_nome2_Visible = 1;
         edtavUnidademedicao_nome1_Visible = 1;
         chkUnidadeMedicao_Ativo.Title.Text = "Ativo?";
         edtUnidadeMedicao_Sigla_Title = "Sigla";
         edtUnidadeMedicao_Nome_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkUnidadeMedicao_Ativo.Caption = "";
         edtavDdo_unidademedicao_ativotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_unidademedicao_siglatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_unidademedicao_nometitlecontrolidtoreplace_Visible = 1;
         edtavTfunidademedicao_ativo_sel_Jsonclick = "";
         edtavTfunidademedicao_ativo_sel_Visible = 1;
         edtavTfunidademedicao_sigla_sel_Jsonclick = "";
         edtavTfunidademedicao_sigla_sel_Visible = 1;
         edtavTfunidademedicao_sigla_Jsonclick = "";
         edtavTfunidademedicao_sigla_Visible = 1;
         edtavTfunidademedicao_nome_sel_Jsonclick = "";
         edtavTfunidademedicao_nome_sel_Visible = 1;
         edtavTfunidademedicao_nome_Jsonclick = "";
         edtavTfunidademedicao_nome_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_unidademedicao_ativo_Searchbuttontext = "Pesquisar";
         Ddo_unidademedicao_ativo_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_unidademedicao_ativo_Rangefilterto = "At�";
         Ddo_unidademedicao_ativo_Rangefilterfrom = "Desde";
         Ddo_unidademedicao_ativo_Cleanfilter = "Limpar pesquisa";
         Ddo_unidademedicao_ativo_Loadingdata = "Carregando dados...";
         Ddo_unidademedicao_ativo_Sortdsc = "Ordenar de Z � A";
         Ddo_unidademedicao_ativo_Sortasc = "Ordenar de A � Z";
         Ddo_unidademedicao_ativo_Datalistupdateminimumcharacters = 0;
         Ddo_unidademedicao_ativo_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_unidademedicao_ativo_Datalisttype = "FixedValues";
         Ddo_unidademedicao_ativo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_unidademedicao_ativo_Filterisrange = Convert.ToBoolean( 0);
         Ddo_unidademedicao_ativo_Includefilter = Convert.ToBoolean( 0);
         Ddo_unidademedicao_ativo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_unidademedicao_ativo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_unidademedicao_ativo_Titlecontrolidtoreplace = "";
         Ddo_unidademedicao_ativo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_unidademedicao_ativo_Cls = "ColumnSettings";
         Ddo_unidademedicao_ativo_Tooltip = "Op��es";
         Ddo_unidademedicao_ativo_Caption = "";
         Ddo_unidademedicao_sigla_Searchbuttontext = "Pesquisar";
         Ddo_unidademedicao_sigla_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_unidademedicao_sigla_Rangefilterto = "At�";
         Ddo_unidademedicao_sigla_Rangefilterfrom = "Desde";
         Ddo_unidademedicao_sigla_Cleanfilter = "Limpar pesquisa";
         Ddo_unidademedicao_sigla_Loadingdata = "Carregando dados...";
         Ddo_unidademedicao_sigla_Sortdsc = "Ordenar de Z � A";
         Ddo_unidademedicao_sigla_Sortasc = "Ordenar de A � Z";
         Ddo_unidademedicao_sigla_Datalistupdateminimumcharacters = 0;
         Ddo_unidademedicao_sigla_Datalistproc = "GetPromptUnidadeMedicaoFilterData";
         Ddo_unidademedicao_sigla_Datalistfixedvalues = "";
         Ddo_unidademedicao_sigla_Datalisttype = "Dynamic";
         Ddo_unidademedicao_sigla_Includedatalist = Convert.ToBoolean( -1);
         Ddo_unidademedicao_sigla_Filterisrange = Convert.ToBoolean( 0);
         Ddo_unidademedicao_sigla_Filtertype = "Character";
         Ddo_unidademedicao_sigla_Includefilter = Convert.ToBoolean( -1);
         Ddo_unidademedicao_sigla_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_unidademedicao_sigla_Includesortasc = Convert.ToBoolean( -1);
         Ddo_unidademedicao_sigla_Titlecontrolidtoreplace = "";
         Ddo_unidademedicao_sigla_Dropdownoptionstype = "GridTitleSettings";
         Ddo_unidademedicao_sigla_Cls = "ColumnSettings";
         Ddo_unidademedicao_sigla_Tooltip = "Op��es";
         Ddo_unidademedicao_sigla_Caption = "";
         Ddo_unidademedicao_nome_Searchbuttontext = "Pesquisar";
         Ddo_unidademedicao_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_unidademedicao_nome_Rangefilterto = "At�";
         Ddo_unidademedicao_nome_Rangefilterfrom = "Desde";
         Ddo_unidademedicao_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_unidademedicao_nome_Loadingdata = "Carregando dados...";
         Ddo_unidademedicao_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_unidademedicao_nome_Sortasc = "Ordenar de A � Z";
         Ddo_unidademedicao_nome_Datalistupdateminimumcharacters = 0;
         Ddo_unidademedicao_nome_Datalistproc = "GetPromptUnidadeMedicaoFilterData";
         Ddo_unidademedicao_nome_Datalistfixedvalues = "";
         Ddo_unidademedicao_nome_Datalisttype = "Dynamic";
         Ddo_unidademedicao_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_unidademedicao_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_unidademedicao_nome_Filtertype = "Character";
         Ddo_unidademedicao_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_unidademedicao_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_unidademedicao_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_unidademedicao_nome_Titlecontrolidtoreplace = "";
         Ddo_unidademedicao_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_unidademedicao_nome_Cls = "ColumnSettings";
         Ddo_unidademedicao_nome_Tooltip = "Op��es";
         Ddo_unidademedicao_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Unidades de Medi��o";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV30ddo_UnidadeMedicao_NomeTitleControlIdToReplace',fld:'vDDO_UNIDADEMEDICAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34ddo_UnidadeMedicao_SiglaTitleControlIdToReplace',fld:'vDDO_UNIDADEMEDICAO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_UnidadeMedicao_AtivoTitleControlIdToReplace',fld:'vDDO_UNIDADEMEDICAO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV28TFUnidadeMedicao_Nome',fld:'vTFUNIDADEMEDICAO_NOME',pic:'@!',nv:''},{av:'AV29TFUnidadeMedicao_Nome_Sel',fld:'vTFUNIDADEMEDICAO_NOME_SEL',pic:'@!',nv:''},{av:'AV32TFUnidadeMedicao_Sigla',fld:'vTFUNIDADEMEDICAO_SIGLA',pic:'@!',nv:''},{av:'AV33TFUnidadeMedicao_Sigla_Sel',fld:'vTFUNIDADEMEDICAO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV36TFUnidadeMedicao_Ativo_Sel',fld:'vTFUNIDADEMEDICAO_ATIVO_SEL',pic:'9',nv:0},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16UnidadeMedicao_Nome1',fld:'vUNIDADEMEDICAO_NOME1',pic:'@!',nv:''},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19UnidadeMedicao_Nome2',fld:'vUNIDADEMEDICAO_NOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22UnidadeMedicao_Nome3',fld:'vUNIDADEMEDICAO_NOME3',pic:'@!',nv:''}],oparms:[{av:'AV27UnidadeMedicao_NomeTitleFilterData',fld:'vUNIDADEMEDICAO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV31UnidadeMedicao_SiglaTitleFilterData',fld:'vUNIDADEMEDICAO_SIGLATITLEFILTERDATA',pic:'',nv:null},{av:'AV35UnidadeMedicao_AtivoTitleFilterData',fld:'vUNIDADEMEDICAO_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'edtUnidadeMedicao_Nome_Titleformat',ctrl:'UNIDADEMEDICAO_NOME',prop:'Titleformat'},{av:'edtUnidadeMedicao_Nome_Title',ctrl:'UNIDADEMEDICAO_NOME',prop:'Title'},{av:'edtUnidadeMedicao_Sigla_Titleformat',ctrl:'UNIDADEMEDICAO_SIGLA',prop:'Titleformat'},{av:'edtUnidadeMedicao_Sigla_Title',ctrl:'UNIDADEMEDICAO_SIGLA',prop:'Title'},{av:'chkUnidadeMedicao_Ativo_Titleformat',ctrl:'UNIDADEMEDICAO_ATIVO',prop:'Titleformat'},{av:'chkUnidadeMedicao_Ativo.Title.Text',ctrl:'UNIDADEMEDICAO_ATIVO',prop:'Title'},{av:'AV40GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV41GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11IP2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16UnidadeMedicao_Nome1',fld:'vUNIDADEMEDICAO_NOME1',pic:'@!',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19UnidadeMedicao_Nome2',fld:'vUNIDADEMEDICAO_NOME2',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22UnidadeMedicao_Nome3',fld:'vUNIDADEMEDICAO_NOME3',pic:'@!',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV28TFUnidadeMedicao_Nome',fld:'vTFUNIDADEMEDICAO_NOME',pic:'@!',nv:''},{av:'AV29TFUnidadeMedicao_Nome_Sel',fld:'vTFUNIDADEMEDICAO_NOME_SEL',pic:'@!',nv:''},{av:'AV32TFUnidadeMedicao_Sigla',fld:'vTFUNIDADEMEDICAO_SIGLA',pic:'@!',nv:''},{av:'AV33TFUnidadeMedicao_Sigla_Sel',fld:'vTFUNIDADEMEDICAO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV36TFUnidadeMedicao_Ativo_Sel',fld:'vTFUNIDADEMEDICAO_ATIVO_SEL',pic:'9',nv:0},{av:'AV30ddo_UnidadeMedicao_NomeTitleControlIdToReplace',fld:'vDDO_UNIDADEMEDICAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34ddo_UnidadeMedicao_SiglaTitleControlIdToReplace',fld:'vDDO_UNIDADEMEDICAO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_UnidadeMedicao_AtivoTitleControlIdToReplace',fld:'vDDO_UNIDADEMEDICAO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_UNIDADEMEDICAO_NOME.ONOPTIONCLICKED","{handler:'E12IP2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16UnidadeMedicao_Nome1',fld:'vUNIDADEMEDICAO_NOME1',pic:'@!',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19UnidadeMedicao_Nome2',fld:'vUNIDADEMEDICAO_NOME2',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22UnidadeMedicao_Nome3',fld:'vUNIDADEMEDICAO_NOME3',pic:'@!',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV28TFUnidadeMedicao_Nome',fld:'vTFUNIDADEMEDICAO_NOME',pic:'@!',nv:''},{av:'AV29TFUnidadeMedicao_Nome_Sel',fld:'vTFUNIDADEMEDICAO_NOME_SEL',pic:'@!',nv:''},{av:'AV32TFUnidadeMedicao_Sigla',fld:'vTFUNIDADEMEDICAO_SIGLA',pic:'@!',nv:''},{av:'AV33TFUnidadeMedicao_Sigla_Sel',fld:'vTFUNIDADEMEDICAO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV36TFUnidadeMedicao_Ativo_Sel',fld:'vTFUNIDADEMEDICAO_ATIVO_SEL',pic:'9',nv:0},{av:'AV30ddo_UnidadeMedicao_NomeTitleControlIdToReplace',fld:'vDDO_UNIDADEMEDICAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34ddo_UnidadeMedicao_SiglaTitleControlIdToReplace',fld:'vDDO_UNIDADEMEDICAO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_UnidadeMedicao_AtivoTitleControlIdToReplace',fld:'vDDO_UNIDADEMEDICAO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_unidademedicao_nome_Activeeventkey',ctrl:'DDO_UNIDADEMEDICAO_NOME',prop:'ActiveEventKey'},{av:'Ddo_unidademedicao_nome_Filteredtext_get',ctrl:'DDO_UNIDADEMEDICAO_NOME',prop:'FilteredText_get'},{av:'Ddo_unidademedicao_nome_Selectedvalue_get',ctrl:'DDO_UNIDADEMEDICAO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_unidademedicao_nome_Sortedstatus',ctrl:'DDO_UNIDADEMEDICAO_NOME',prop:'SortedStatus'},{av:'AV28TFUnidadeMedicao_Nome',fld:'vTFUNIDADEMEDICAO_NOME',pic:'@!',nv:''},{av:'AV29TFUnidadeMedicao_Nome_Sel',fld:'vTFUNIDADEMEDICAO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_unidademedicao_sigla_Sortedstatus',ctrl:'DDO_UNIDADEMEDICAO_SIGLA',prop:'SortedStatus'},{av:'Ddo_unidademedicao_ativo_Sortedstatus',ctrl:'DDO_UNIDADEMEDICAO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_UNIDADEMEDICAO_SIGLA.ONOPTIONCLICKED","{handler:'E13IP2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16UnidadeMedicao_Nome1',fld:'vUNIDADEMEDICAO_NOME1',pic:'@!',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19UnidadeMedicao_Nome2',fld:'vUNIDADEMEDICAO_NOME2',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22UnidadeMedicao_Nome3',fld:'vUNIDADEMEDICAO_NOME3',pic:'@!',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV28TFUnidadeMedicao_Nome',fld:'vTFUNIDADEMEDICAO_NOME',pic:'@!',nv:''},{av:'AV29TFUnidadeMedicao_Nome_Sel',fld:'vTFUNIDADEMEDICAO_NOME_SEL',pic:'@!',nv:''},{av:'AV32TFUnidadeMedicao_Sigla',fld:'vTFUNIDADEMEDICAO_SIGLA',pic:'@!',nv:''},{av:'AV33TFUnidadeMedicao_Sigla_Sel',fld:'vTFUNIDADEMEDICAO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV36TFUnidadeMedicao_Ativo_Sel',fld:'vTFUNIDADEMEDICAO_ATIVO_SEL',pic:'9',nv:0},{av:'AV30ddo_UnidadeMedicao_NomeTitleControlIdToReplace',fld:'vDDO_UNIDADEMEDICAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34ddo_UnidadeMedicao_SiglaTitleControlIdToReplace',fld:'vDDO_UNIDADEMEDICAO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_UnidadeMedicao_AtivoTitleControlIdToReplace',fld:'vDDO_UNIDADEMEDICAO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_unidademedicao_sigla_Activeeventkey',ctrl:'DDO_UNIDADEMEDICAO_SIGLA',prop:'ActiveEventKey'},{av:'Ddo_unidademedicao_sigla_Filteredtext_get',ctrl:'DDO_UNIDADEMEDICAO_SIGLA',prop:'FilteredText_get'},{av:'Ddo_unidademedicao_sigla_Selectedvalue_get',ctrl:'DDO_UNIDADEMEDICAO_SIGLA',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_unidademedicao_sigla_Sortedstatus',ctrl:'DDO_UNIDADEMEDICAO_SIGLA',prop:'SortedStatus'},{av:'AV32TFUnidadeMedicao_Sigla',fld:'vTFUNIDADEMEDICAO_SIGLA',pic:'@!',nv:''},{av:'AV33TFUnidadeMedicao_Sigla_Sel',fld:'vTFUNIDADEMEDICAO_SIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_unidademedicao_nome_Sortedstatus',ctrl:'DDO_UNIDADEMEDICAO_NOME',prop:'SortedStatus'},{av:'Ddo_unidademedicao_ativo_Sortedstatus',ctrl:'DDO_UNIDADEMEDICAO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_UNIDADEMEDICAO_ATIVO.ONOPTIONCLICKED","{handler:'E14IP2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16UnidadeMedicao_Nome1',fld:'vUNIDADEMEDICAO_NOME1',pic:'@!',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19UnidadeMedicao_Nome2',fld:'vUNIDADEMEDICAO_NOME2',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22UnidadeMedicao_Nome3',fld:'vUNIDADEMEDICAO_NOME3',pic:'@!',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV28TFUnidadeMedicao_Nome',fld:'vTFUNIDADEMEDICAO_NOME',pic:'@!',nv:''},{av:'AV29TFUnidadeMedicao_Nome_Sel',fld:'vTFUNIDADEMEDICAO_NOME_SEL',pic:'@!',nv:''},{av:'AV32TFUnidadeMedicao_Sigla',fld:'vTFUNIDADEMEDICAO_SIGLA',pic:'@!',nv:''},{av:'AV33TFUnidadeMedicao_Sigla_Sel',fld:'vTFUNIDADEMEDICAO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV36TFUnidadeMedicao_Ativo_Sel',fld:'vTFUNIDADEMEDICAO_ATIVO_SEL',pic:'9',nv:0},{av:'AV30ddo_UnidadeMedicao_NomeTitleControlIdToReplace',fld:'vDDO_UNIDADEMEDICAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34ddo_UnidadeMedicao_SiglaTitleControlIdToReplace',fld:'vDDO_UNIDADEMEDICAO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_UnidadeMedicao_AtivoTitleControlIdToReplace',fld:'vDDO_UNIDADEMEDICAO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_unidademedicao_ativo_Activeeventkey',ctrl:'DDO_UNIDADEMEDICAO_ATIVO',prop:'ActiveEventKey'},{av:'Ddo_unidademedicao_ativo_Selectedvalue_get',ctrl:'DDO_UNIDADEMEDICAO_ATIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_unidademedicao_ativo_Sortedstatus',ctrl:'DDO_UNIDADEMEDICAO_ATIVO',prop:'SortedStatus'},{av:'AV36TFUnidadeMedicao_Ativo_Sel',fld:'vTFUNIDADEMEDICAO_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_unidademedicao_nome_Sortedstatus',ctrl:'DDO_UNIDADEMEDICAO_NOME',prop:'SortedStatus'},{av:'Ddo_unidademedicao_sigla_Sortedstatus',ctrl:'DDO_UNIDADEMEDICAO_SIGLA',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E27IP2',iparms:[],oparms:[{av:'AV25Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E28IP2',iparms:[{av:'A1197UnidadeMedicao_Codigo',fld:'UNIDADEMEDICAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1198UnidadeMedicao_Nome',fld:'UNIDADEMEDICAO_NOME',pic:'@!',hsh:true,nv:''}],oparms:[{av:'AV7InOutUnidadeMedicao_Codigo',fld:'vINOUTUNIDADEMEDICAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8InOutUnidadeMedicao_Nome',fld:'vINOUTUNIDADEMEDICAO_NOME',pic:'@!',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E15IP2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16UnidadeMedicao_Nome1',fld:'vUNIDADEMEDICAO_NOME1',pic:'@!',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19UnidadeMedicao_Nome2',fld:'vUNIDADEMEDICAO_NOME2',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22UnidadeMedicao_Nome3',fld:'vUNIDADEMEDICAO_NOME3',pic:'@!',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV28TFUnidadeMedicao_Nome',fld:'vTFUNIDADEMEDICAO_NOME',pic:'@!',nv:''},{av:'AV29TFUnidadeMedicao_Nome_Sel',fld:'vTFUNIDADEMEDICAO_NOME_SEL',pic:'@!',nv:''},{av:'AV32TFUnidadeMedicao_Sigla',fld:'vTFUNIDADEMEDICAO_SIGLA',pic:'@!',nv:''},{av:'AV33TFUnidadeMedicao_Sigla_Sel',fld:'vTFUNIDADEMEDICAO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV36TFUnidadeMedicao_Ativo_Sel',fld:'vTFUNIDADEMEDICAO_ATIVO_SEL',pic:'9',nv:0},{av:'AV30ddo_UnidadeMedicao_NomeTitleControlIdToReplace',fld:'vDDO_UNIDADEMEDICAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34ddo_UnidadeMedicao_SiglaTitleControlIdToReplace',fld:'vDDO_UNIDADEMEDICAO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_UnidadeMedicao_AtivoTitleControlIdToReplace',fld:'vDDO_UNIDADEMEDICAO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E20IP2',iparms:[],oparms:[{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E16IP2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16UnidadeMedicao_Nome1',fld:'vUNIDADEMEDICAO_NOME1',pic:'@!',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19UnidadeMedicao_Nome2',fld:'vUNIDADEMEDICAO_NOME2',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22UnidadeMedicao_Nome3',fld:'vUNIDADEMEDICAO_NOME3',pic:'@!',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV28TFUnidadeMedicao_Nome',fld:'vTFUNIDADEMEDICAO_NOME',pic:'@!',nv:''},{av:'AV29TFUnidadeMedicao_Nome_Sel',fld:'vTFUNIDADEMEDICAO_NOME_SEL',pic:'@!',nv:''},{av:'AV32TFUnidadeMedicao_Sigla',fld:'vTFUNIDADEMEDICAO_SIGLA',pic:'@!',nv:''},{av:'AV33TFUnidadeMedicao_Sigla_Sel',fld:'vTFUNIDADEMEDICAO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV36TFUnidadeMedicao_Ativo_Sel',fld:'vTFUNIDADEMEDICAO_ATIVO_SEL',pic:'9',nv:0},{av:'AV30ddo_UnidadeMedicao_NomeTitleControlIdToReplace',fld:'vDDO_UNIDADEMEDICAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34ddo_UnidadeMedicao_SiglaTitleControlIdToReplace',fld:'vDDO_UNIDADEMEDICAO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_UnidadeMedicao_AtivoTitleControlIdToReplace',fld:'vDDO_UNIDADEMEDICAO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19UnidadeMedicao_Nome2',fld:'vUNIDADEMEDICAO_NOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22UnidadeMedicao_Nome3',fld:'vUNIDADEMEDICAO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16UnidadeMedicao_Nome1',fld:'vUNIDADEMEDICAO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavUnidademedicao_nome2_Visible',ctrl:'vUNIDADEMEDICAO_NOME2',prop:'Visible'},{av:'edtavUnidademedicao_nome3_Visible',ctrl:'vUNIDADEMEDICAO_NOME3',prop:'Visible'},{av:'edtavUnidademedicao_nome1_Visible',ctrl:'vUNIDADEMEDICAO_NOME1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E21IP2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavUnidademedicao_nome1_Visible',ctrl:'vUNIDADEMEDICAO_NOME1',prop:'Visible'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E22IP2',iparms:[],oparms:[{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E17IP2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16UnidadeMedicao_Nome1',fld:'vUNIDADEMEDICAO_NOME1',pic:'@!',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19UnidadeMedicao_Nome2',fld:'vUNIDADEMEDICAO_NOME2',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22UnidadeMedicao_Nome3',fld:'vUNIDADEMEDICAO_NOME3',pic:'@!',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV28TFUnidadeMedicao_Nome',fld:'vTFUNIDADEMEDICAO_NOME',pic:'@!',nv:''},{av:'AV29TFUnidadeMedicao_Nome_Sel',fld:'vTFUNIDADEMEDICAO_NOME_SEL',pic:'@!',nv:''},{av:'AV32TFUnidadeMedicao_Sigla',fld:'vTFUNIDADEMEDICAO_SIGLA',pic:'@!',nv:''},{av:'AV33TFUnidadeMedicao_Sigla_Sel',fld:'vTFUNIDADEMEDICAO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV36TFUnidadeMedicao_Ativo_Sel',fld:'vTFUNIDADEMEDICAO_ATIVO_SEL',pic:'9',nv:0},{av:'AV30ddo_UnidadeMedicao_NomeTitleControlIdToReplace',fld:'vDDO_UNIDADEMEDICAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34ddo_UnidadeMedicao_SiglaTitleControlIdToReplace',fld:'vDDO_UNIDADEMEDICAO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_UnidadeMedicao_AtivoTitleControlIdToReplace',fld:'vDDO_UNIDADEMEDICAO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19UnidadeMedicao_Nome2',fld:'vUNIDADEMEDICAO_NOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22UnidadeMedicao_Nome3',fld:'vUNIDADEMEDICAO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16UnidadeMedicao_Nome1',fld:'vUNIDADEMEDICAO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavUnidademedicao_nome2_Visible',ctrl:'vUNIDADEMEDICAO_NOME2',prop:'Visible'},{av:'edtavUnidademedicao_nome3_Visible',ctrl:'vUNIDADEMEDICAO_NOME3',prop:'Visible'},{av:'edtavUnidademedicao_nome1_Visible',ctrl:'vUNIDADEMEDICAO_NOME1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E23IP2',iparms:[{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavUnidademedicao_nome2_Visible',ctrl:'vUNIDADEMEDICAO_NOME2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E18IP2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16UnidadeMedicao_Nome1',fld:'vUNIDADEMEDICAO_NOME1',pic:'@!',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19UnidadeMedicao_Nome2',fld:'vUNIDADEMEDICAO_NOME2',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22UnidadeMedicao_Nome3',fld:'vUNIDADEMEDICAO_NOME3',pic:'@!',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV28TFUnidadeMedicao_Nome',fld:'vTFUNIDADEMEDICAO_NOME',pic:'@!',nv:''},{av:'AV29TFUnidadeMedicao_Nome_Sel',fld:'vTFUNIDADEMEDICAO_NOME_SEL',pic:'@!',nv:''},{av:'AV32TFUnidadeMedicao_Sigla',fld:'vTFUNIDADEMEDICAO_SIGLA',pic:'@!',nv:''},{av:'AV33TFUnidadeMedicao_Sigla_Sel',fld:'vTFUNIDADEMEDICAO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV36TFUnidadeMedicao_Ativo_Sel',fld:'vTFUNIDADEMEDICAO_ATIVO_SEL',pic:'9',nv:0},{av:'AV30ddo_UnidadeMedicao_NomeTitleControlIdToReplace',fld:'vDDO_UNIDADEMEDICAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34ddo_UnidadeMedicao_SiglaTitleControlIdToReplace',fld:'vDDO_UNIDADEMEDICAO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_UnidadeMedicao_AtivoTitleControlIdToReplace',fld:'vDDO_UNIDADEMEDICAO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19UnidadeMedicao_Nome2',fld:'vUNIDADEMEDICAO_NOME2',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22UnidadeMedicao_Nome3',fld:'vUNIDADEMEDICAO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16UnidadeMedicao_Nome1',fld:'vUNIDADEMEDICAO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavUnidademedicao_nome2_Visible',ctrl:'vUNIDADEMEDICAO_NOME2',prop:'Visible'},{av:'edtavUnidademedicao_nome3_Visible',ctrl:'vUNIDADEMEDICAO_NOME3',prop:'Visible'},{av:'edtavUnidademedicao_nome1_Visible',ctrl:'vUNIDADEMEDICAO_NOME1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E24IP2',iparms:[{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavUnidademedicao_nome3_Visible',ctrl:'vUNIDADEMEDICAO_NOME3',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E19IP2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16UnidadeMedicao_Nome1',fld:'vUNIDADEMEDICAO_NOME1',pic:'@!',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19UnidadeMedicao_Nome2',fld:'vUNIDADEMEDICAO_NOME2',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22UnidadeMedicao_Nome3',fld:'vUNIDADEMEDICAO_NOME3',pic:'@!',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV28TFUnidadeMedicao_Nome',fld:'vTFUNIDADEMEDICAO_NOME',pic:'@!',nv:''},{av:'AV29TFUnidadeMedicao_Nome_Sel',fld:'vTFUNIDADEMEDICAO_NOME_SEL',pic:'@!',nv:''},{av:'AV32TFUnidadeMedicao_Sigla',fld:'vTFUNIDADEMEDICAO_SIGLA',pic:'@!',nv:''},{av:'AV33TFUnidadeMedicao_Sigla_Sel',fld:'vTFUNIDADEMEDICAO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV36TFUnidadeMedicao_Ativo_Sel',fld:'vTFUNIDADEMEDICAO_ATIVO_SEL',pic:'9',nv:0},{av:'AV30ddo_UnidadeMedicao_NomeTitleControlIdToReplace',fld:'vDDO_UNIDADEMEDICAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34ddo_UnidadeMedicao_SiglaTitleControlIdToReplace',fld:'vDDO_UNIDADEMEDICAO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_UnidadeMedicao_AtivoTitleControlIdToReplace',fld:'vDDO_UNIDADEMEDICAO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV28TFUnidadeMedicao_Nome',fld:'vTFUNIDADEMEDICAO_NOME',pic:'@!',nv:''},{av:'Ddo_unidademedicao_nome_Filteredtext_set',ctrl:'DDO_UNIDADEMEDICAO_NOME',prop:'FilteredText_set'},{av:'AV29TFUnidadeMedicao_Nome_Sel',fld:'vTFUNIDADEMEDICAO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_unidademedicao_nome_Selectedvalue_set',ctrl:'DDO_UNIDADEMEDICAO_NOME',prop:'SelectedValue_set'},{av:'AV32TFUnidadeMedicao_Sigla',fld:'vTFUNIDADEMEDICAO_SIGLA',pic:'@!',nv:''},{av:'Ddo_unidademedicao_sigla_Filteredtext_set',ctrl:'DDO_UNIDADEMEDICAO_SIGLA',prop:'FilteredText_set'},{av:'AV33TFUnidadeMedicao_Sigla_Sel',fld:'vTFUNIDADEMEDICAO_SIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_unidademedicao_sigla_Selectedvalue_set',ctrl:'DDO_UNIDADEMEDICAO_SIGLA',prop:'SelectedValue_set'},{av:'AV36TFUnidadeMedicao_Ativo_Sel',fld:'vTFUNIDADEMEDICAO_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_unidademedicao_ativo_Selectedvalue_set',ctrl:'DDO_UNIDADEMEDICAO_ATIVO',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16UnidadeMedicao_Nome1',fld:'vUNIDADEMEDICAO_NOME1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavUnidademedicao_nome1_Visible',ctrl:'vUNIDADEMEDICAO_NOME1',prop:'Visible'},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19UnidadeMedicao_Nome2',fld:'vUNIDADEMEDICAO_NOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22UnidadeMedicao_Nome3',fld:'vUNIDADEMEDICAO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavUnidademedicao_nome2_Visible',ctrl:'vUNIDADEMEDICAO_NOME2',prop:'Visible'},{av:'edtavUnidademedicao_nome3_Visible',ctrl:'vUNIDADEMEDICAO_NOME3',prop:'Visible'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV8InOutUnidadeMedicao_Nome = "";
         Gridpaginationbar_Selectedpage = "";
         Ddo_unidademedicao_nome_Activeeventkey = "";
         Ddo_unidademedicao_nome_Filteredtext_get = "";
         Ddo_unidademedicao_nome_Selectedvalue_get = "";
         Ddo_unidademedicao_sigla_Activeeventkey = "";
         Ddo_unidademedicao_sigla_Filteredtext_get = "";
         Ddo_unidademedicao_sigla_Selectedvalue_get = "";
         Ddo_unidademedicao_ativo_Activeeventkey = "";
         Ddo_unidademedicao_ativo_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV16UnidadeMedicao_Nome1 = "";
         AV18DynamicFiltersSelector2 = "";
         AV19UnidadeMedicao_Nome2 = "";
         AV21DynamicFiltersSelector3 = "";
         AV22UnidadeMedicao_Nome3 = "";
         AV28TFUnidadeMedicao_Nome = "";
         AV29TFUnidadeMedicao_Nome_Sel = "";
         AV32TFUnidadeMedicao_Sigla = "";
         AV33TFUnidadeMedicao_Sigla_Sel = "";
         AV30ddo_UnidadeMedicao_NomeTitleControlIdToReplace = "";
         AV34ddo_UnidadeMedicao_SiglaTitleControlIdToReplace = "";
         AV37ddo_UnidadeMedicao_AtivoTitleControlIdToReplace = "";
         AV45Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV38DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV27UnidadeMedicao_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV31UnidadeMedicao_SiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV35UnidadeMedicao_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_unidademedicao_nome_Filteredtext_set = "";
         Ddo_unidademedicao_nome_Selectedvalue_set = "";
         Ddo_unidademedicao_nome_Sortedstatus = "";
         Ddo_unidademedicao_sigla_Filteredtext_set = "";
         Ddo_unidademedicao_sigla_Selectedvalue_set = "";
         Ddo_unidademedicao_sigla_Sortedstatus = "";
         Ddo_unidademedicao_ativo_Selectedvalue_set = "";
         Ddo_unidademedicao_ativo_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV25Select = "";
         AV44Select_GXI = "";
         A1198UnidadeMedicao_Nome = "";
         A1200UnidadeMedicao_Sigla = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV16UnidadeMedicao_Nome1 = "";
         lV19UnidadeMedicao_Nome2 = "";
         lV22UnidadeMedicao_Nome3 = "";
         lV28TFUnidadeMedicao_Nome = "";
         lV32TFUnidadeMedicao_Sigla = "";
         H00IP2_A1199UnidadeMedicao_Ativo = new bool[] {false} ;
         H00IP2_A1200UnidadeMedicao_Sigla = new String[] {""} ;
         H00IP2_A1198UnidadeMedicao_Nome = new String[] {""} ;
         H00IP2_A1197UnidadeMedicao_Codigo = new int[1] ;
         H00IP3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptunidademedicao__default(),
            new Object[][] {
                new Object[] {
               H00IP2_A1199UnidadeMedicao_Ativo, H00IP2_A1200UnidadeMedicao_Sigla, H00IP2_A1198UnidadeMedicao_Nome, H00IP2_A1197UnidadeMedicao_Codigo
               }
               , new Object[] {
               H00IP3_AGRID_nRecordCount
               }
            }
         );
         AV45Pgmname = "PromptUnidadeMedicao";
         /* GeneXus formulas. */
         AV45Pgmname = "PromptUnidadeMedicao";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_65 ;
      private short nGXsfl_65_idx=1 ;
      private short AV13OrderedBy ;
      private short AV36TFUnidadeMedicao_Ativo_Sel ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_65_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtUnidadeMedicao_Nome_Titleformat ;
      private short edtUnidadeMedicao_Sigla_Titleformat ;
      private short chkUnidadeMedicao_Ativo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutUnidadeMedicao_Codigo ;
      private int wcpOAV7InOutUnidadeMedicao_Codigo ;
      private int subGrid_Rows ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_unidademedicao_nome_Datalistupdateminimumcharacters ;
      private int Ddo_unidademedicao_sigla_Datalistupdateminimumcharacters ;
      private int Ddo_unidademedicao_ativo_Datalistupdateminimumcharacters ;
      private int edtavTfunidademedicao_nome_Visible ;
      private int edtavTfunidademedicao_nome_sel_Visible ;
      private int edtavTfunidademedicao_sigla_Visible ;
      private int edtavTfunidademedicao_sigla_sel_Visible ;
      private int edtavTfunidademedicao_ativo_sel_Visible ;
      private int edtavDdo_unidademedicao_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_unidademedicao_siglatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_unidademedicao_ativotitlecontrolidtoreplace_Visible ;
      private int A1197UnidadeMedicao_Codigo ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV39PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavUnidademedicao_nome1_Visible ;
      private int edtavUnidademedicao_nome2_Visible ;
      private int edtavUnidademedicao_nome3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV40GridCurrentPage ;
      private long AV41GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String AV8InOutUnidadeMedicao_Nome ;
      private String wcpOAV8InOutUnidadeMedicao_Nome ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_unidademedicao_nome_Activeeventkey ;
      private String Ddo_unidademedicao_nome_Filteredtext_get ;
      private String Ddo_unidademedicao_nome_Selectedvalue_get ;
      private String Ddo_unidademedicao_sigla_Activeeventkey ;
      private String Ddo_unidademedicao_sigla_Filteredtext_get ;
      private String Ddo_unidademedicao_sigla_Selectedvalue_get ;
      private String Ddo_unidademedicao_ativo_Activeeventkey ;
      private String Ddo_unidademedicao_ativo_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_65_idx="0001" ;
      private String AV16UnidadeMedicao_Nome1 ;
      private String AV19UnidadeMedicao_Nome2 ;
      private String AV22UnidadeMedicao_Nome3 ;
      private String AV28TFUnidadeMedicao_Nome ;
      private String AV29TFUnidadeMedicao_Nome_Sel ;
      private String AV32TFUnidadeMedicao_Sigla ;
      private String AV33TFUnidadeMedicao_Sigla_Sel ;
      private String AV45Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_unidademedicao_nome_Caption ;
      private String Ddo_unidademedicao_nome_Tooltip ;
      private String Ddo_unidademedicao_nome_Cls ;
      private String Ddo_unidademedicao_nome_Filteredtext_set ;
      private String Ddo_unidademedicao_nome_Selectedvalue_set ;
      private String Ddo_unidademedicao_nome_Dropdownoptionstype ;
      private String Ddo_unidademedicao_nome_Titlecontrolidtoreplace ;
      private String Ddo_unidademedicao_nome_Sortedstatus ;
      private String Ddo_unidademedicao_nome_Filtertype ;
      private String Ddo_unidademedicao_nome_Datalisttype ;
      private String Ddo_unidademedicao_nome_Datalistfixedvalues ;
      private String Ddo_unidademedicao_nome_Datalistproc ;
      private String Ddo_unidademedicao_nome_Sortasc ;
      private String Ddo_unidademedicao_nome_Sortdsc ;
      private String Ddo_unidademedicao_nome_Loadingdata ;
      private String Ddo_unidademedicao_nome_Cleanfilter ;
      private String Ddo_unidademedicao_nome_Rangefilterfrom ;
      private String Ddo_unidademedicao_nome_Rangefilterto ;
      private String Ddo_unidademedicao_nome_Noresultsfound ;
      private String Ddo_unidademedicao_nome_Searchbuttontext ;
      private String Ddo_unidademedicao_sigla_Caption ;
      private String Ddo_unidademedicao_sigla_Tooltip ;
      private String Ddo_unidademedicao_sigla_Cls ;
      private String Ddo_unidademedicao_sigla_Filteredtext_set ;
      private String Ddo_unidademedicao_sigla_Selectedvalue_set ;
      private String Ddo_unidademedicao_sigla_Dropdownoptionstype ;
      private String Ddo_unidademedicao_sigla_Titlecontrolidtoreplace ;
      private String Ddo_unidademedicao_sigla_Sortedstatus ;
      private String Ddo_unidademedicao_sigla_Filtertype ;
      private String Ddo_unidademedicao_sigla_Datalisttype ;
      private String Ddo_unidademedicao_sigla_Datalistfixedvalues ;
      private String Ddo_unidademedicao_sigla_Datalistproc ;
      private String Ddo_unidademedicao_sigla_Sortasc ;
      private String Ddo_unidademedicao_sigla_Sortdsc ;
      private String Ddo_unidademedicao_sigla_Loadingdata ;
      private String Ddo_unidademedicao_sigla_Cleanfilter ;
      private String Ddo_unidademedicao_sigla_Rangefilterfrom ;
      private String Ddo_unidademedicao_sigla_Rangefilterto ;
      private String Ddo_unidademedicao_sigla_Noresultsfound ;
      private String Ddo_unidademedicao_sigla_Searchbuttontext ;
      private String Ddo_unidademedicao_ativo_Caption ;
      private String Ddo_unidademedicao_ativo_Tooltip ;
      private String Ddo_unidademedicao_ativo_Cls ;
      private String Ddo_unidademedicao_ativo_Selectedvalue_set ;
      private String Ddo_unidademedicao_ativo_Dropdownoptionstype ;
      private String Ddo_unidademedicao_ativo_Titlecontrolidtoreplace ;
      private String Ddo_unidademedicao_ativo_Sortedstatus ;
      private String Ddo_unidademedicao_ativo_Datalisttype ;
      private String Ddo_unidademedicao_ativo_Datalistfixedvalues ;
      private String Ddo_unidademedicao_ativo_Sortasc ;
      private String Ddo_unidademedicao_ativo_Sortdsc ;
      private String Ddo_unidademedicao_ativo_Loadingdata ;
      private String Ddo_unidademedicao_ativo_Cleanfilter ;
      private String Ddo_unidademedicao_ativo_Rangefilterfrom ;
      private String Ddo_unidademedicao_ativo_Rangefilterto ;
      private String Ddo_unidademedicao_ativo_Noresultsfound ;
      private String Ddo_unidademedicao_ativo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfunidademedicao_nome_Internalname ;
      private String edtavTfunidademedicao_nome_Jsonclick ;
      private String edtavTfunidademedicao_nome_sel_Internalname ;
      private String edtavTfunidademedicao_nome_sel_Jsonclick ;
      private String edtavTfunidademedicao_sigla_Internalname ;
      private String edtavTfunidademedicao_sigla_Jsonclick ;
      private String edtavTfunidademedicao_sigla_sel_Internalname ;
      private String edtavTfunidademedicao_sigla_sel_Jsonclick ;
      private String edtavTfunidademedicao_ativo_sel_Internalname ;
      private String edtavTfunidademedicao_ativo_sel_Jsonclick ;
      private String edtavDdo_unidademedicao_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_unidademedicao_siglatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_unidademedicao_ativotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtUnidadeMedicao_Codigo_Internalname ;
      private String A1198UnidadeMedicao_Nome ;
      private String edtUnidadeMedicao_Nome_Internalname ;
      private String A1200UnidadeMedicao_Sigla ;
      private String edtUnidadeMedicao_Sigla_Internalname ;
      private String chkUnidadeMedicao_Ativo_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV16UnidadeMedicao_Nome1 ;
      private String lV19UnidadeMedicao_Nome2 ;
      private String lV22UnidadeMedicao_Nome3 ;
      private String lV28TFUnidadeMedicao_Nome ;
      private String lV32TFUnidadeMedicao_Sigla ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavUnidademedicao_nome1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavUnidademedicao_nome2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavUnidademedicao_nome3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_unidademedicao_nome_Internalname ;
      private String Ddo_unidademedicao_sigla_Internalname ;
      private String Ddo_unidademedicao_ativo_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtUnidadeMedicao_Nome_Title ;
      private String edtUnidadeMedicao_Sigla_Title ;
      private String edtavSelect_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavUnidademedicao_nome1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavUnidademedicao_nome2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavUnidademedicao_nome3_Jsonclick ;
      private String sGXsfl_65_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtUnidadeMedicao_Codigo_Jsonclick ;
      private String edtUnidadeMedicao_Nome_Jsonclick ;
      private String edtUnidadeMedicao_Sigla_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV17DynamicFiltersEnabled2 ;
      private bool AV20DynamicFiltersEnabled3 ;
      private bool AV24DynamicFiltersIgnoreFirst ;
      private bool AV23DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_unidademedicao_nome_Includesortasc ;
      private bool Ddo_unidademedicao_nome_Includesortdsc ;
      private bool Ddo_unidademedicao_nome_Includefilter ;
      private bool Ddo_unidademedicao_nome_Filterisrange ;
      private bool Ddo_unidademedicao_nome_Includedatalist ;
      private bool Ddo_unidademedicao_sigla_Includesortasc ;
      private bool Ddo_unidademedicao_sigla_Includesortdsc ;
      private bool Ddo_unidademedicao_sigla_Includefilter ;
      private bool Ddo_unidademedicao_sigla_Filterisrange ;
      private bool Ddo_unidademedicao_sigla_Includedatalist ;
      private bool Ddo_unidademedicao_ativo_Includesortasc ;
      private bool Ddo_unidademedicao_ativo_Includesortdsc ;
      private bool Ddo_unidademedicao_ativo_Includefilter ;
      private bool Ddo_unidademedicao_ativo_Filterisrange ;
      private bool Ddo_unidademedicao_ativo_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool A1199UnidadeMedicao_Ativo ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV25Select_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV18DynamicFiltersSelector2 ;
      private String AV21DynamicFiltersSelector3 ;
      private String AV30ddo_UnidadeMedicao_NomeTitleControlIdToReplace ;
      private String AV34ddo_UnidadeMedicao_SiglaTitleControlIdToReplace ;
      private String AV37ddo_UnidadeMedicao_AtivoTitleControlIdToReplace ;
      private String AV44Select_GXI ;
      private String AV25Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutUnidadeMedicao_Codigo ;
      private String aP1_InOutUnidadeMedicao_Nome ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCheckbox chkUnidadeMedicao_Ativo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private bool[] H00IP2_A1199UnidadeMedicao_Ativo ;
      private String[] H00IP2_A1200UnidadeMedicao_Sigla ;
      private String[] H00IP2_A1198UnidadeMedicao_Nome ;
      private int[] H00IP2_A1197UnidadeMedicao_Codigo ;
      private long[] H00IP3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV27UnidadeMedicao_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV31UnidadeMedicao_SiglaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV35UnidadeMedicao_AtivoTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV38DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class promptunidademedicao__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00IP2( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             String AV16UnidadeMedicao_Nome1 ,
                                             bool AV17DynamicFiltersEnabled2 ,
                                             String AV18DynamicFiltersSelector2 ,
                                             String AV19UnidadeMedicao_Nome2 ,
                                             bool AV20DynamicFiltersEnabled3 ,
                                             String AV21DynamicFiltersSelector3 ,
                                             String AV22UnidadeMedicao_Nome3 ,
                                             String AV29TFUnidadeMedicao_Nome_Sel ,
                                             String AV28TFUnidadeMedicao_Nome ,
                                             String AV33TFUnidadeMedicao_Sigla_Sel ,
                                             String AV32TFUnidadeMedicao_Sigla ,
                                             short AV36TFUnidadeMedicao_Ativo_Sel ,
                                             String A1198UnidadeMedicao_Nome ,
                                             String A1200UnidadeMedicao_Sigla ,
                                             bool A1199UnidadeMedicao_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [12] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [UnidadeMedicao_Ativo], [UnidadeMedicao_Sigla], [UnidadeMedicao_Nome], [UnidadeMedicao_Codigo]";
         sFromString = " FROM [UnidadeMedicao] WITH (NOLOCK)";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "UNIDADEMEDICAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16UnidadeMedicao_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Nome] like '%' + @lV16UnidadeMedicao_Nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Nome] like '%' + @lV16UnidadeMedicao_Nome1 + '%')";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( AV17DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "UNIDADEMEDICAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19UnidadeMedicao_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Nome] like '%' + @lV19UnidadeMedicao_Nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Nome] like '%' + @lV19UnidadeMedicao_Nome2 + '%')";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( AV20DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "UNIDADEMEDICAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22UnidadeMedicao_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Nome] like '%' + @lV22UnidadeMedicao_Nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Nome] like '%' + @lV22UnidadeMedicao_Nome3 + '%')";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV29TFUnidadeMedicao_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28TFUnidadeMedicao_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Nome] like @lV28TFUnidadeMedicao_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Nome] like @lV28TFUnidadeMedicao_Nome)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29TFUnidadeMedicao_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Nome] = @AV29TFUnidadeMedicao_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Nome] = @AV29TFUnidadeMedicao_Nome_Sel)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV33TFUnidadeMedicao_Sigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32TFUnidadeMedicao_Sigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Sigla] like @lV32TFUnidadeMedicao_Sigla)";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Sigla] like @lV32TFUnidadeMedicao_Sigla)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33TFUnidadeMedicao_Sigla_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Sigla] = @AV33TFUnidadeMedicao_Sigla_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Sigla] = @AV33TFUnidadeMedicao_Sigla_Sel)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV36TFUnidadeMedicao_Ativo_Sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Ativo] = 1)";
            }
         }
         if ( AV36TFUnidadeMedicao_Ativo_Sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [UnidadeMedicao_Nome]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [UnidadeMedicao_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [UnidadeMedicao_Sigla]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [UnidadeMedicao_Sigla] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [UnidadeMedicao_Ativo]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [UnidadeMedicao_Ativo] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [UnidadeMedicao_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00IP3( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             String AV16UnidadeMedicao_Nome1 ,
                                             bool AV17DynamicFiltersEnabled2 ,
                                             String AV18DynamicFiltersSelector2 ,
                                             String AV19UnidadeMedicao_Nome2 ,
                                             bool AV20DynamicFiltersEnabled3 ,
                                             String AV21DynamicFiltersSelector3 ,
                                             String AV22UnidadeMedicao_Nome3 ,
                                             String AV29TFUnidadeMedicao_Nome_Sel ,
                                             String AV28TFUnidadeMedicao_Nome ,
                                             String AV33TFUnidadeMedicao_Sigla_Sel ,
                                             String AV32TFUnidadeMedicao_Sigla ,
                                             short AV36TFUnidadeMedicao_Ativo_Sel ,
                                             String A1198UnidadeMedicao_Nome ,
                                             String A1200UnidadeMedicao_Sigla ,
                                             bool A1199UnidadeMedicao_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [7] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [UnidadeMedicao] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "UNIDADEMEDICAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16UnidadeMedicao_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Nome] like '%' + @lV16UnidadeMedicao_Nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Nome] like '%' + @lV16UnidadeMedicao_Nome1 + '%')";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( AV17DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "UNIDADEMEDICAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19UnidadeMedicao_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Nome] like '%' + @lV19UnidadeMedicao_Nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Nome] like '%' + @lV19UnidadeMedicao_Nome2 + '%')";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( AV20DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "UNIDADEMEDICAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22UnidadeMedicao_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Nome] like '%' + @lV22UnidadeMedicao_Nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Nome] like '%' + @lV22UnidadeMedicao_Nome3 + '%')";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV29TFUnidadeMedicao_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28TFUnidadeMedicao_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Nome] like @lV28TFUnidadeMedicao_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Nome] like @lV28TFUnidadeMedicao_Nome)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29TFUnidadeMedicao_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Nome] = @AV29TFUnidadeMedicao_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Nome] = @AV29TFUnidadeMedicao_Nome_Sel)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV33TFUnidadeMedicao_Sigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32TFUnidadeMedicao_Sigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Sigla] like @lV32TFUnidadeMedicao_Sigla)";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Sigla] like @lV32TFUnidadeMedicao_Sigla)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33TFUnidadeMedicao_Sigla_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Sigla] = @AV33TFUnidadeMedicao_Sigla_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Sigla] = @AV33TFUnidadeMedicao_Sigla_Sel)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV36TFUnidadeMedicao_Ativo_Sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Ativo] = 1)";
            }
         }
         if ( AV36TFUnidadeMedicao_Ativo_Sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00IP2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (short)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (bool)dynConstraints[15] , (short)dynConstraints[16] , (bool)dynConstraints[17] );
               case 1 :
                     return conditional_H00IP3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (short)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (bool)dynConstraints[15] , (short)dynConstraints[16] , (bool)dynConstraints[17] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00IP2 ;
          prmH00IP2 = new Object[] {
          new Object[] {"@lV16UnidadeMedicao_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV19UnidadeMedicao_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV22UnidadeMedicao_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV28TFUnidadeMedicao_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV29TFUnidadeMedicao_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV32TFUnidadeMedicao_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV33TFUnidadeMedicao_Sigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00IP3 ;
          prmH00IP3 = new Object[] {
          new Object[] {"@lV16UnidadeMedicao_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV19UnidadeMedicao_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV22UnidadeMedicao_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV28TFUnidadeMedicao_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV29TFUnidadeMedicao_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV32TFUnidadeMedicao_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV33TFUnidadeMedicao_Sigla_Sel",SqlDbType.Char,15,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00IP2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IP2,11,0,true,false )
             ,new CursorDef("H00IP3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IP3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                return;
       }
    }

 }

}
