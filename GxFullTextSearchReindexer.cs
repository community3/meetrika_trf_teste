/*
               File: GxFullTextSearchReindexer
        Description: No description for object
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/17/2020 2:57:31.96
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
namespace GeneXus.Programs {
   public class GxFullTextSearchReindexer
   {
      public static int Reindex( IGxContext context )
      {
         GxSilentTrnSdt obj ;
         IGxSilentTrn trn ;
         bool result ;
         obj = new SdtContagemResultadoNewOS(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtModuloFuncoes(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtSolicitacoesItens(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtContratoServicosDePara(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtServicoResponsavel(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtEmail_Instancia_Destinatarios(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtEmail_Instancia(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtHelpSistema(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtLogErroBC(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtCatalogoSutentacaoArtefatos(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtGpoObjCtrlResponsavel(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtReqNegReqTec(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtFuncoesUsuarioFuncoesAPF(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtContagemItemAtributosFMetrica(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtContagemItemAtributosFSoftware(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtAreaTrabalho_Guias(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtMenuPerfil(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtAmbienteTecnologicoTecnologias(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtFuncaoDadosTabela(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtRegrasContagemAnexos(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtSistemaDePara(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtContratoSistemas(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtContratanteUsuarioSistema(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtFuncoesAPFAtributos(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtAtributos(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtTabela(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtMenu(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtParametrosSistema(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtReferenciaINM(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtLoteArquivoAnexo(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtFeriados(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtContratoServicosArtefatos(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtTmp_File(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtUsuarioPerfil(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtContratadaUsuario(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtContrato(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtSolicitacoes(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtUsuarioServicos(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtProjeto(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtContratoServicosSistemas(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtContratoGestor(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtContratoServicosPrioridade(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtAudit(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtServicoFluxo(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtServicoArtefatos(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtContratoAuxiliar(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtServicoCheck(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtRequisito(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtAreaTrabalho(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtPessoa(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtContratante(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtContratanteUsuario(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtContratada(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtModulo(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtUsuario(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtServico(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtSistema(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtContratoServicos(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtContagemResultadoContagens(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtContagemResultadoEvidencia(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtContagemResultadoChckLstLog(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtContagemResultadoNotas(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtAnexos(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtContagemResultadoArtefato(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtContagemResultadoNotificacao(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtSolicitacaoServicoReqNegocio(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtContagemResultadoQA(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtContagemResultadoRequisito(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtProposta(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtSolicitacaoServico(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtPerfil(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         obj = new SdtContagemResultado(context);
         trn = obj.getTransaction();
         result = trn.Reindex();
         return 1 ;
      }

   }

}
