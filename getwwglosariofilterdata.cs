/*
               File: GetWWGlosarioFilterData
        Description: Get WWGlosario Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:54:10.99
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwglosariofilterdata : GXProcedure
   {
      public getwwglosariofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwglosariofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
         return AV25OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwglosariofilterdata objgetwwglosariofilterdata;
         objgetwwglosariofilterdata = new getwwglosariofilterdata();
         objgetwwglosariofilterdata.AV16DDOName = aP0_DDOName;
         objgetwwglosariofilterdata.AV14SearchTxt = aP1_SearchTxt;
         objgetwwglosariofilterdata.AV15SearchTxtTo = aP2_SearchTxtTo;
         objgetwwglosariofilterdata.AV20OptionsJson = "" ;
         objgetwwglosariofilterdata.AV23OptionsDescJson = "" ;
         objgetwwglosariofilterdata.AV25OptionIndexesJson = "" ;
         objgetwwglosariofilterdata.context.SetSubmitInitialConfig(context);
         objgetwwglosariofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwglosariofilterdata);
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwglosariofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV19Options = (IGxCollection)(new GxSimpleCollection());
         AV22OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV24OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV16DDOName), "DDO_GLOSARIO_TERMO") == 0 )
         {
            /* Execute user subroutine: 'LOADGLOSARIO_TERMOOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV16DDOName), "DDO_GLOSARIO_NOMEARQ") == 0 )
         {
            /* Execute user subroutine: 'LOADGLOSARIO_NOMEARQOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV20OptionsJson = AV19Options.ToJSonString(false);
         AV23OptionsDescJson = AV22OptionsDesc.ToJSonString(false);
         AV25OptionIndexesJson = AV24OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV27Session.Get("WWGlosarioGridState"), "") == 0 )
         {
            AV29GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWGlosarioGridState"), "");
         }
         else
         {
            AV29GridState.FromXml(AV27Session.Get("WWGlosarioGridState"), "");
         }
         AV46GXV1 = 1;
         while ( AV46GXV1 <= AV29GridState.gxTpr_Filtervalues.Count )
         {
            AV30GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV29GridState.gxTpr_Filtervalues.Item(AV46GXV1));
            if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "GLOSARIO_AREATRABALHOCOD") == 0 )
            {
               AV32Glosario_AreaTrabalhoCod = (int)(NumberUtil.Val( AV30GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFGLOSARIO_TERMO") == 0 )
            {
               AV10TFGlosario_Termo = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFGLOSARIO_TERMO_SEL") == 0 )
            {
               AV11TFGlosario_Termo_Sel = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFGLOSARIO_NOMEARQ") == 0 )
            {
               AV12TFGlosario_NomeArq = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFGLOSARIO_NOMEARQ_SEL") == 0 )
            {
               AV13TFGlosario_NomeArq_Sel = AV30GridStateFilterValue.gxTpr_Value;
            }
            AV46GXV1 = (int)(AV46GXV1+1);
         }
         if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(1));
            AV33DynamicFiltersSelector1 = AV31GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV33DynamicFiltersSelector1, "GLOSARIO_TERMO") == 0 )
            {
               AV34Glosario_Termo1 = AV31GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33DynamicFiltersSelector1, "DESCRICAO") == 0 )
            {
               AV35Descricao1 = AV31GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV36DynamicFiltersEnabled2 = true;
               AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(2));
               AV37DynamicFiltersSelector2 = AV31GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "GLOSARIO_TERMO") == 0 )
               {
                  AV38Glosario_Termo2 = AV31GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "DESCRICAO") == 0 )
               {
                  AV39Descricao2 = AV31GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV40DynamicFiltersEnabled3 = true;
                  AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(3));
                  AV41DynamicFiltersSelector3 = AV31GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV41DynamicFiltersSelector3, "GLOSARIO_TERMO") == 0 )
                  {
                     AV42Glosario_Termo3 = AV31GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV41DynamicFiltersSelector3, "DESCRICAO") == 0 )
                  {
                     AV43Descricao3 = AV31GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADGLOSARIO_TERMOOPTIONS' Routine */
         AV10TFGlosario_Termo = AV14SearchTxt;
         AV11TFGlosario_Termo_Sel = "";
         AV48WWGlosarioDS_1_Glosario_areatrabalhocod = AV32Glosario_AreaTrabalhoCod;
         AV49WWGlosarioDS_2_Dynamicfiltersselector1 = AV33DynamicFiltersSelector1;
         AV50WWGlosarioDS_3_Glosario_termo1 = AV34Glosario_Termo1;
         AV51WWGlosarioDS_4_Descricao1 = AV35Descricao1;
         AV52WWGlosarioDS_5_Dynamicfiltersenabled2 = AV36DynamicFiltersEnabled2;
         AV53WWGlosarioDS_6_Dynamicfiltersselector2 = AV37DynamicFiltersSelector2;
         AV54WWGlosarioDS_7_Glosario_termo2 = AV38Glosario_Termo2;
         AV55WWGlosarioDS_8_Descricao2 = AV39Descricao2;
         AV56WWGlosarioDS_9_Dynamicfiltersenabled3 = AV40DynamicFiltersEnabled3;
         AV57WWGlosarioDS_10_Dynamicfiltersselector3 = AV41DynamicFiltersSelector3;
         AV58WWGlosarioDS_11_Glosario_termo3 = AV42Glosario_Termo3;
         AV59WWGlosarioDS_12_Descricao3 = AV43Descricao3;
         AV60WWGlosarioDS_13_Tfglosario_termo = AV10TFGlosario_Termo;
         AV61WWGlosarioDS_14_Tfglosario_termo_sel = AV11TFGlosario_Termo_Sel;
         AV62WWGlosarioDS_15_Tfglosario_nomearq = AV12TFGlosario_NomeArq;
         AV63WWGlosarioDS_16_Tfglosario_nomearq_sel = AV13TFGlosario_NomeArq_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV49WWGlosarioDS_2_Dynamicfiltersselector1 ,
                                              AV50WWGlosarioDS_3_Glosario_termo1 ,
                                              AV51WWGlosarioDS_4_Descricao1 ,
                                              AV52WWGlosarioDS_5_Dynamicfiltersenabled2 ,
                                              AV53WWGlosarioDS_6_Dynamicfiltersselector2 ,
                                              AV54WWGlosarioDS_7_Glosario_termo2 ,
                                              AV55WWGlosarioDS_8_Descricao2 ,
                                              AV56WWGlosarioDS_9_Dynamicfiltersenabled3 ,
                                              AV57WWGlosarioDS_10_Dynamicfiltersselector3 ,
                                              AV58WWGlosarioDS_11_Glosario_termo3 ,
                                              AV59WWGlosarioDS_12_Descricao3 ,
                                              AV61WWGlosarioDS_14_Tfglosario_termo_sel ,
                                              AV60WWGlosarioDS_13_Tfglosario_termo ,
                                              AV63WWGlosarioDS_16_Tfglosario_nomearq_sel ,
                                              AV62WWGlosarioDS_15_Tfglosario_nomearq ,
                                              A858Glosario_Termo ,
                                              A859Glosario_Descricao ,
                                              A1343Glosario_NomeArq ,
                                              A1346Glosario_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.INT
                                              }
         });
         lV50WWGlosarioDS_3_Glosario_termo1 = StringUtil.Concat( StringUtil.RTrim( AV50WWGlosarioDS_3_Glosario_termo1), "%", "");
         lV51WWGlosarioDS_4_Descricao1 = StringUtil.PadR( StringUtil.RTrim( AV51WWGlosarioDS_4_Descricao1), 40, "%");
         lV54WWGlosarioDS_7_Glosario_termo2 = StringUtil.Concat( StringUtil.RTrim( AV54WWGlosarioDS_7_Glosario_termo2), "%", "");
         lV55WWGlosarioDS_8_Descricao2 = StringUtil.PadR( StringUtil.RTrim( AV55WWGlosarioDS_8_Descricao2), 40, "%");
         lV58WWGlosarioDS_11_Glosario_termo3 = StringUtil.Concat( StringUtil.RTrim( AV58WWGlosarioDS_11_Glosario_termo3), "%", "");
         lV59WWGlosarioDS_12_Descricao3 = StringUtil.PadR( StringUtil.RTrim( AV59WWGlosarioDS_12_Descricao3), 40, "%");
         lV60WWGlosarioDS_13_Tfglosario_termo = StringUtil.Concat( StringUtil.RTrim( AV60WWGlosarioDS_13_Tfglosario_termo), "%", "");
         lV62WWGlosarioDS_15_Tfglosario_nomearq = StringUtil.PadR( StringUtil.RTrim( AV62WWGlosarioDS_15_Tfglosario_nomearq), 50, "%");
         /* Using cursor P00OR2 */
         pr_default.execute(0, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, lV50WWGlosarioDS_3_Glosario_termo1, lV51WWGlosarioDS_4_Descricao1, lV54WWGlosarioDS_7_Glosario_termo2, lV55WWGlosarioDS_8_Descricao2, lV58WWGlosarioDS_11_Glosario_termo3, lV59WWGlosarioDS_12_Descricao3, lV60WWGlosarioDS_13_Tfglosario_termo, AV61WWGlosarioDS_14_Tfglosario_termo_sel, lV62WWGlosarioDS_15_Tfglosario_nomearq, AV63WWGlosarioDS_16_Tfglosario_nomearq_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKOR2 = false;
            A1346Glosario_AreaTrabalhoCod = P00OR2_A1346Glosario_AreaTrabalhoCod[0];
            A858Glosario_Termo = P00OR2_A858Glosario_Termo[0];
            A1343Glosario_NomeArq = P00OR2_A1343Glosario_NomeArq[0];
            n1343Glosario_NomeArq = P00OR2_n1343Glosario_NomeArq[0];
            A859Glosario_Descricao = P00OR2_A859Glosario_Descricao[0];
            A1347Glosario_Codigo = P00OR2_A1347Glosario_Codigo[0];
            AV26count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00OR2_A858Glosario_Termo[0], A858Glosario_Termo) == 0 ) )
            {
               BRKOR2 = false;
               A1347Glosario_Codigo = P00OR2_A1347Glosario_Codigo[0];
               AV26count = (long)(AV26count+1);
               BRKOR2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A858Glosario_Termo)) )
            {
               AV18Option = A858Glosario_Termo;
               AV19Options.Add(AV18Option, 0);
               AV24OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV26count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV19Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKOR2 )
            {
               BRKOR2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADGLOSARIO_NOMEARQOPTIONS' Routine */
         AV12TFGlosario_NomeArq = AV14SearchTxt;
         AV13TFGlosario_NomeArq_Sel = "";
         AV48WWGlosarioDS_1_Glosario_areatrabalhocod = AV32Glosario_AreaTrabalhoCod;
         AV49WWGlosarioDS_2_Dynamicfiltersselector1 = AV33DynamicFiltersSelector1;
         AV50WWGlosarioDS_3_Glosario_termo1 = AV34Glosario_Termo1;
         AV51WWGlosarioDS_4_Descricao1 = AV35Descricao1;
         AV52WWGlosarioDS_5_Dynamicfiltersenabled2 = AV36DynamicFiltersEnabled2;
         AV53WWGlosarioDS_6_Dynamicfiltersselector2 = AV37DynamicFiltersSelector2;
         AV54WWGlosarioDS_7_Glosario_termo2 = AV38Glosario_Termo2;
         AV55WWGlosarioDS_8_Descricao2 = AV39Descricao2;
         AV56WWGlosarioDS_9_Dynamicfiltersenabled3 = AV40DynamicFiltersEnabled3;
         AV57WWGlosarioDS_10_Dynamicfiltersselector3 = AV41DynamicFiltersSelector3;
         AV58WWGlosarioDS_11_Glosario_termo3 = AV42Glosario_Termo3;
         AV59WWGlosarioDS_12_Descricao3 = AV43Descricao3;
         AV60WWGlosarioDS_13_Tfglosario_termo = AV10TFGlosario_Termo;
         AV61WWGlosarioDS_14_Tfglosario_termo_sel = AV11TFGlosario_Termo_Sel;
         AV62WWGlosarioDS_15_Tfglosario_nomearq = AV12TFGlosario_NomeArq;
         AV63WWGlosarioDS_16_Tfglosario_nomearq_sel = AV13TFGlosario_NomeArq_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV49WWGlosarioDS_2_Dynamicfiltersselector1 ,
                                              AV50WWGlosarioDS_3_Glosario_termo1 ,
                                              AV51WWGlosarioDS_4_Descricao1 ,
                                              AV52WWGlosarioDS_5_Dynamicfiltersenabled2 ,
                                              AV53WWGlosarioDS_6_Dynamicfiltersselector2 ,
                                              AV54WWGlosarioDS_7_Glosario_termo2 ,
                                              AV55WWGlosarioDS_8_Descricao2 ,
                                              AV56WWGlosarioDS_9_Dynamicfiltersenabled3 ,
                                              AV57WWGlosarioDS_10_Dynamicfiltersselector3 ,
                                              AV58WWGlosarioDS_11_Glosario_termo3 ,
                                              AV59WWGlosarioDS_12_Descricao3 ,
                                              AV61WWGlosarioDS_14_Tfglosario_termo_sel ,
                                              AV60WWGlosarioDS_13_Tfglosario_termo ,
                                              AV63WWGlosarioDS_16_Tfglosario_nomearq_sel ,
                                              AV62WWGlosarioDS_15_Tfglosario_nomearq ,
                                              A858Glosario_Termo ,
                                              A859Glosario_Descricao ,
                                              A1343Glosario_NomeArq ,
                                              A1346Glosario_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.INT
                                              }
         });
         lV50WWGlosarioDS_3_Glosario_termo1 = StringUtil.Concat( StringUtil.RTrim( AV50WWGlosarioDS_3_Glosario_termo1), "%", "");
         lV51WWGlosarioDS_4_Descricao1 = StringUtil.PadR( StringUtil.RTrim( AV51WWGlosarioDS_4_Descricao1), 40, "%");
         lV54WWGlosarioDS_7_Glosario_termo2 = StringUtil.Concat( StringUtil.RTrim( AV54WWGlosarioDS_7_Glosario_termo2), "%", "");
         lV55WWGlosarioDS_8_Descricao2 = StringUtil.PadR( StringUtil.RTrim( AV55WWGlosarioDS_8_Descricao2), 40, "%");
         lV58WWGlosarioDS_11_Glosario_termo3 = StringUtil.Concat( StringUtil.RTrim( AV58WWGlosarioDS_11_Glosario_termo3), "%", "");
         lV59WWGlosarioDS_12_Descricao3 = StringUtil.PadR( StringUtil.RTrim( AV59WWGlosarioDS_12_Descricao3), 40, "%");
         lV60WWGlosarioDS_13_Tfglosario_termo = StringUtil.Concat( StringUtil.RTrim( AV60WWGlosarioDS_13_Tfglosario_termo), "%", "");
         lV62WWGlosarioDS_15_Tfglosario_nomearq = StringUtil.PadR( StringUtil.RTrim( AV62WWGlosarioDS_15_Tfglosario_nomearq), 50, "%");
         /* Using cursor P00OR3 */
         pr_default.execute(1, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, lV50WWGlosarioDS_3_Glosario_termo1, lV51WWGlosarioDS_4_Descricao1, lV54WWGlosarioDS_7_Glosario_termo2, lV55WWGlosarioDS_8_Descricao2, lV58WWGlosarioDS_11_Glosario_termo3, lV59WWGlosarioDS_12_Descricao3, lV60WWGlosarioDS_13_Tfglosario_termo, AV61WWGlosarioDS_14_Tfglosario_termo_sel, lV62WWGlosarioDS_15_Tfglosario_nomearq, AV63WWGlosarioDS_16_Tfglosario_nomearq_sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKOR4 = false;
            A1346Glosario_AreaTrabalhoCod = P00OR3_A1346Glosario_AreaTrabalhoCod[0];
            A1343Glosario_NomeArq = P00OR3_A1343Glosario_NomeArq[0];
            n1343Glosario_NomeArq = P00OR3_n1343Glosario_NomeArq[0];
            A859Glosario_Descricao = P00OR3_A859Glosario_Descricao[0];
            A858Glosario_Termo = P00OR3_A858Glosario_Termo[0];
            A1347Glosario_Codigo = P00OR3_A1347Glosario_Codigo[0];
            AV26count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00OR3_A1343Glosario_NomeArq[0], A1343Glosario_NomeArq) == 0 ) )
            {
               BRKOR4 = false;
               A1347Glosario_Codigo = P00OR3_A1347Glosario_Codigo[0];
               AV26count = (long)(AV26count+1);
               BRKOR4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1343Glosario_NomeArq)) )
            {
               AV18Option = A1343Glosario_NomeArq;
               AV19Options.Add(AV18Option, 0);
               AV24OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV26count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV19Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKOR4 )
            {
               BRKOR4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV19Options = new GxSimpleCollection();
         AV22OptionsDesc = new GxSimpleCollection();
         AV24OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV27Session = context.GetSession();
         AV29GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV30GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFGlosario_Termo = "";
         AV11TFGlosario_Termo_Sel = "";
         AV12TFGlosario_NomeArq = "";
         AV13TFGlosario_NomeArq_Sel = "";
         AV31GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV33DynamicFiltersSelector1 = "";
         AV34Glosario_Termo1 = "";
         AV35Descricao1 = "";
         AV37DynamicFiltersSelector2 = "";
         AV38Glosario_Termo2 = "";
         AV39Descricao2 = "";
         AV41DynamicFiltersSelector3 = "";
         AV42Glosario_Termo3 = "";
         AV43Descricao3 = "";
         AV49WWGlosarioDS_2_Dynamicfiltersselector1 = "";
         AV50WWGlosarioDS_3_Glosario_termo1 = "";
         AV51WWGlosarioDS_4_Descricao1 = "";
         AV53WWGlosarioDS_6_Dynamicfiltersselector2 = "";
         AV54WWGlosarioDS_7_Glosario_termo2 = "";
         AV55WWGlosarioDS_8_Descricao2 = "";
         AV57WWGlosarioDS_10_Dynamicfiltersselector3 = "";
         AV58WWGlosarioDS_11_Glosario_termo3 = "";
         AV59WWGlosarioDS_12_Descricao3 = "";
         AV60WWGlosarioDS_13_Tfglosario_termo = "";
         AV61WWGlosarioDS_14_Tfglosario_termo_sel = "";
         AV62WWGlosarioDS_15_Tfglosario_nomearq = "";
         AV63WWGlosarioDS_16_Tfglosario_nomearq_sel = "";
         scmdbuf = "";
         lV50WWGlosarioDS_3_Glosario_termo1 = "";
         lV51WWGlosarioDS_4_Descricao1 = "";
         lV54WWGlosarioDS_7_Glosario_termo2 = "";
         lV55WWGlosarioDS_8_Descricao2 = "";
         lV58WWGlosarioDS_11_Glosario_termo3 = "";
         lV59WWGlosarioDS_12_Descricao3 = "";
         lV60WWGlosarioDS_13_Tfglosario_termo = "";
         lV62WWGlosarioDS_15_Tfglosario_nomearq = "";
         A858Glosario_Termo = "";
         A859Glosario_Descricao = "";
         A1343Glosario_NomeArq = "";
         P00OR2_A1346Glosario_AreaTrabalhoCod = new int[1] ;
         P00OR2_A858Glosario_Termo = new String[] {""} ;
         P00OR2_A1343Glosario_NomeArq = new String[] {""} ;
         P00OR2_n1343Glosario_NomeArq = new bool[] {false} ;
         P00OR2_A859Glosario_Descricao = new String[] {""} ;
         P00OR2_A1347Glosario_Codigo = new int[1] ;
         AV18Option = "";
         P00OR3_A1346Glosario_AreaTrabalhoCod = new int[1] ;
         P00OR3_A1343Glosario_NomeArq = new String[] {""} ;
         P00OR3_n1343Glosario_NomeArq = new bool[] {false} ;
         P00OR3_A859Glosario_Descricao = new String[] {""} ;
         P00OR3_A858Glosario_Termo = new String[] {""} ;
         P00OR3_A1347Glosario_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwglosariofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00OR2_A1346Glosario_AreaTrabalhoCod, P00OR2_A858Glosario_Termo, P00OR2_A1343Glosario_NomeArq, P00OR2_n1343Glosario_NomeArq, P00OR2_A859Glosario_Descricao, P00OR2_A1347Glosario_Codigo
               }
               , new Object[] {
               P00OR3_A1346Glosario_AreaTrabalhoCod, P00OR3_A1343Glosario_NomeArq, P00OR3_n1343Glosario_NomeArq, P00OR3_A859Glosario_Descricao, P00OR3_A858Glosario_Termo, P00OR3_A1347Glosario_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV46GXV1 ;
      private int AV32Glosario_AreaTrabalhoCod ;
      private int AV48WWGlosarioDS_1_Glosario_areatrabalhocod ;
      private int AV9WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A1346Glosario_AreaTrabalhoCod ;
      private int A1347Glosario_Codigo ;
      private long AV26count ;
      private String AV12TFGlosario_NomeArq ;
      private String AV13TFGlosario_NomeArq_Sel ;
      private String AV35Descricao1 ;
      private String AV39Descricao2 ;
      private String AV43Descricao3 ;
      private String AV51WWGlosarioDS_4_Descricao1 ;
      private String AV55WWGlosarioDS_8_Descricao2 ;
      private String AV59WWGlosarioDS_12_Descricao3 ;
      private String AV62WWGlosarioDS_15_Tfglosario_nomearq ;
      private String AV63WWGlosarioDS_16_Tfglosario_nomearq_sel ;
      private String scmdbuf ;
      private String lV51WWGlosarioDS_4_Descricao1 ;
      private String lV55WWGlosarioDS_8_Descricao2 ;
      private String lV59WWGlosarioDS_12_Descricao3 ;
      private String lV62WWGlosarioDS_15_Tfglosario_nomearq ;
      private String A1343Glosario_NomeArq ;
      private bool returnInSub ;
      private bool AV36DynamicFiltersEnabled2 ;
      private bool AV40DynamicFiltersEnabled3 ;
      private bool AV52WWGlosarioDS_5_Dynamicfiltersenabled2 ;
      private bool AV56WWGlosarioDS_9_Dynamicfiltersenabled3 ;
      private bool BRKOR2 ;
      private bool n1343Glosario_NomeArq ;
      private bool BRKOR4 ;
      private String AV25OptionIndexesJson ;
      private String AV20OptionsJson ;
      private String AV23OptionsDescJson ;
      private String A859Glosario_Descricao ;
      private String AV16DDOName ;
      private String AV14SearchTxt ;
      private String AV15SearchTxtTo ;
      private String AV10TFGlosario_Termo ;
      private String AV11TFGlosario_Termo_Sel ;
      private String AV33DynamicFiltersSelector1 ;
      private String AV34Glosario_Termo1 ;
      private String AV37DynamicFiltersSelector2 ;
      private String AV38Glosario_Termo2 ;
      private String AV41DynamicFiltersSelector3 ;
      private String AV42Glosario_Termo3 ;
      private String AV49WWGlosarioDS_2_Dynamicfiltersselector1 ;
      private String AV50WWGlosarioDS_3_Glosario_termo1 ;
      private String AV53WWGlosarioDS_6_Dynamicfiltersselector2 ;
      private String AV54WWGlosarioDS_7_Glosario_termo2 ;
      private String AV57WWGlosarioDS_10_Dynamicfiltersselector3 ;
      private String AV58WWGlosarioDS_11_Glosario_termo3 ;
      private String AV60WWGlosarioDS_13_Tfglosario_termo ;
      private String AV61WWGlosarioDS_14_Tfglosario_termo_sel ;
      private String lV50WWGlosarioDS_3_Glosario_termo1 ;
      private String lV54WWGlosarioDS_7_Glosario_termo2 ;
      private String lV58WWGlosarioDS_11_Glosario_termo3 ;
      private String lV60WWGlosarioDS_13_Tfglosario_termo ;
      private String A858Glosario_Termo ;
      private String AV18Option ;
      private IGxSession AV27Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00OR2_A1346Glosario_AreaTrabalhoCod ;
      private String[] P00OR2_A858Glosario_Termo ;
      private String[] P00OR2_A1343Glosario_NomeArq ;
      private bool[] P00OR2_n1343Glosario_NomeArq ;
      private String[] P00OR2_A859Glosario_Descricao ;
      private int[] P00OR2_A1347Glosario_Codigo ;
      private int[] P00OR3_A1346Glosario_AreaTrabalhoCod ;
      private String[] P00OR3_A1343Glosario_NomeArq ;
      private bool[] P00OR3_n1343Glosario_NomeArq ;
      private String[] P00OR3_A859Glosario_Descricao ;
      private String[] P00OR3_A858Glosario_Termo ;
      private int[] P00OR3_A1347Glosario_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV19Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV29GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV30GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV31GridStateDynamicFilter ;
   }

   public class getwwglosariofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00OR2( IGxContext context ,
                                             String AV49WWGlosarioDS_2_Dynamicfiltersselector1 ,
                                             String AV50WWGlosarioDS_3_Glosario_termo1 ,
                                             String AV51WWGlosarioDS_4_Descricao1 ,
                                             bool AV52WWGlosarioDS_5_Dynamicfiltersenabled2 ,
                                             String AV53WWGlosarioDS_6_Dynamicfiltersselector2 ,
                                             String AV54WWGlosarioDS_7_Glosario_termo2 ,
                                             String AV55WWGlosarioDS_8_Descricao2 ,
                                             bool AV56WWGlosarioDS_9_Dynamicfiltersenabled3 ,
                                             String AV57WWGlosarioDS_10_Dynamicfiltersselector3 ,
                                             String AV58WWGlosarioDS_11_Glosario_termo3 ,
                                             String AV59WWGlosarioDS_12_Descricao3 ,
                                             String AV61WWGlosarioDS_14_Tfglosario_termo_sel ,
                                             String AV60WWGlosarioDS_13_Tfglosario_termo ,
                                             String AV63WWGlosarioDS_16_Tfglosario_nomearq_sel ,
                                             String AV62WWGlosarioDS_15_Tfglosario_nomearq ,
                                             String A858Glosario_Termo ,
                                             String A859Glosario_Descricao ,
                                             String A1343Glosario_NomeArq ,
                                             int A1346Glosario_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [11] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [Glosario_AreaTrabalhoCod], [Glosario_Termo], [Glosario_NomeArq], [Glosario_Descricao], [Glosario_Codigo] FROM [Glosario] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Glosario_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV49WWGlosarioDS_2_Dynamicfiltersselector1, "GLOSARIO_TERMO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WWGlosarioDS_3_Glosario_termo1)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_Termo] like '%' + @lV50WWGlosarioDS_3_Glosario_termo1)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV49WWGlosarioDS_2_Dynamicfiltersselector1, "DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WWGlosarioDS_4_Descricao1)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_Descricao] like '%' + @lV51WWGlosarioDS_4_Descricao1 + '%')";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV52WWGlosarioDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV53WWGlosarioDS_6_Dynamicfiltersselector2, "GLOSARIO_TERMO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWGlosarioDS_7_Glosario_termo2)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_Termo] like '%' + @lV54WWGlosarioDS_7_Glosario_termo2)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV52WWGlosarioDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV53WWGlosarioDS_6_Dynamicfiltersselector2, "DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWGlosarioDS_8_Descricao2)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_Descricao] like '%' + @lV55WWGlosarioDS_8_Descricao2 + '%')";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV56WWGlosarioDS_9_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV57WWGlosarioDS_10_Dynamicfiltersselector3, "GLOSARIO_TERMO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWGlosarioDS_11_Glosario_termo3)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_Termo] like '%' + @lV58WWGlosarioDS_11_Glosario_termo3)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV56WWGlosarioDS_9_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV57WWGlosarioDS_10_Dynamicfiltersselector3, "DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWGlosarioDS_12_Descricao3)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_Descricao] like '%' + @lV59WWGlosarioDS_12_Descricao3 + '%')";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV61WWGlosarioDS_14_Tfglosario_termo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWGlosarioDS_13_Tfglosario_termo)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_Termo] like @lV60WWGlosarioDS_13_Tfglosario_termo)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWGlosarioDS_14_Tfglosario_termo_sel)) )
         {
            sWhereString = sWhereString + " and ([Glosario_Termo] = @AV61WWGlosarioDS_14_Tfglosario_termo_sel)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV63WWGlosarioDS_16_Tfglosario_nomearq_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWGlosarioDS_15_Tfglosario_nomearq)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_NomeArq] like @lV62WWGlosarioDS_15_Tfglosario_nomearq)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWGlosarioDS_16_Tfglosario_nomearq_sel)) )
         {
            sWhereString = sWhereString + " and ([Glosario_NomeArq] = @AV63WWGlosarioDS_16_Tfglosario_nomearq_sel)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [Glosario_Termo]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00OR3( IGxContext context ,
                                             String AV49WWGlosarioDS_2_Dynamicfiltersselector1 ,
                                             String AV50WWGlosarioDS_3_Glosario_termo1 ,
                                             String AV51WWGlosarioDS_4_Descricao1 ,
                                             bool AV52WWGlosarioDS_5_Dynamicfiltersenabled2 ,
                                             String AV53WWGlosarioDS_6_Dynamicfiltersselector2 ,
                                             String AV54WWGlosarioDS_7_Glosario_termo2 ,
                                             String AV55WWGlosarioDS_8_Descricao2 ,
                                             bool AV56WWGlosarioDS_9_Dynamicfiltersenabled3 ,
                                             String AV57WWGlosarioDS_10_Dynamicfiltersselector3 ,
                                             String AV58WWGlosarioDS_11_Glosario_termo3 ,
                                             String AV59WWGlosarioDS_12_Descricao3 ,
                                             String AV61WWGlosarioDS_14_Tfglosario_termo_sel ,
                                             String AV60WWGlosarioDS_13_Tfglosario_termo ,
                                             String AV63WWGlosarioDS_16_Tfglosario_nomearq_sel ,
                                             String AV62WWGlosarioDS_15_Tfglosario_nomearq ,
                                             String A858Glosario_Termo ,
                                             String A859Glosario_Descricao ,
                                             String A1343Glosario_NomeArq ,
                                             int A1346Glosario_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [11] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [Glosario_AreaTrabalhoCod], [Glosario_NomeArq], [Glosario_Descricao], [Glosario_Termo], [Glosario_Codigo] FROM [Glosario] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Glosario_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV49WWGlosarioDS_2_Dynamicfiltersselector1, "GLOSARIO_TERMO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WWGlosarioDS_3_Glosario_termo1)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_Termo] like '%' + @lV50WWGlosarioDS_3_Glosario_termo1)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV49WWGlosarioDS_2_Dynamicfiltersselector1, "DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WWGlosarioDS_4_Descricao1)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_Descricao] like '%' + @lV51WWGlosarioDS_4_Descricao1 + '%')";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV52WWGlosarioDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV53WWGlosarioDS_6_Dynamicfiltersselector2, "GLOSARIO_TERMO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWGlosarioDS_7_Glosario_termo2)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_Termo] like '%' + @lV54WWGlosarioDS_7_Glosario_termo2)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV52WWGlosarioDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV53WWGlosarioDS_6_Dynamicfiltersselector2, "DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWGlosarioDS_8_Descricao2)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_Descricao] like '%' + @lV55WWGlosarioDS_8_Descricao2 + '%')";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV56WWGlosarioDS_9_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV57WWGlosarioDS_10_Dynamicfiltersselector3, "GLOSARIO_TERMO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWGlosarioDS_11_Glosario_termo3)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_Termo] like '%' + @lV58WWGlosarioDS_11_Glosario_termo3)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV56WWGlosarioDS_9_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV57WWGlosarioDS_10_Dynamicfiltersselector3, "DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWGlosarioDS_12_Descricao3)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_Descricao] like '%' + @lV59WWGlosarioDS_12_Descricao3 + '%')";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV61WWGlosarioDS_14_Tfglosario_termo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWGlosarioDS_13_Tfglosario_termo)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_Termo] like @lV60WWGlosarioDS_13_Tfglosario_termo)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWGlosarioDS_14_Tfglosario_termo_sel)) )
         {
            sWhereString = sWhereString + " and ([Glosario_Termo] = @AV61WWGlosarioDS_14_Tfglosario_termo_sel)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV63WWGlosarioDS_16_Tfglosario_nomearq_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWGlosarioDS_15_Tfglosario_nomearq)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_NomeArq] like @lV62WWGlosarioDS_15_Tfglosario_nomearq)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWGlosarioDS_16_Tfglosario_nomearq_sel)) )
         {
            sWhereString = sWhereString + " and ([Glosario_NomeArq] = @AV63WWGlosarioDS_16_Tfglosario_nomearq_sel)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [Glosario_NomeArq]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00OR2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] );
               case 1 :
                     return conditional_P00OR3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00OR2 ;
          prmP00OR2 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV50WWGlosarioDS_3_Glosario_termo1",SqlDbType.VarChar,255,0} ,
          new Object[] {"@lV51WWGlosarioDS_4_Descricao1",SqlDbType.Char,40,0} ,
          new Object[] {"@lV54WWGlosarioDS_7_Glosario_termo2",SqlDbType.VarChar,255,0} ,
          new Object[] {"@lV55WWGlosarioDS_8_Descricao2",SqlDbType.Char,40,0} ,
          new Object[] {"@lV58WWGlosarioDS_11_Glosario_termo3",SqlDbType.VarChar,255,0} ,
          new Object[] {"@lV59WWGlosarioDS_12_Descricao3",SqlDbType.Char,40,0} ,
          new Object[] {"@lV60WWGlosarioDS_13_Tfglosario_termo",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV61WWGlosarioDS_14_Tfglosario_termo_sel",SqlDbType.VarChar,255,0} ,
          new Object[] {"@lV62WWGlosarioDS_15_Tfglosario_nomearq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV63WWGlosarioDS_16_Tfglosario_nomearq_sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmP00OR3 ;
          prmP00OR3 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV50WWGlosarioDS_3_Glosario_termo1",SqlDbType.VarChar,255,0} ,
          new Object[] {"@lV51WWGlosarioDS_4_Descricao1",SqlDbType.Char,40,0} ,
          new Object[] {"@lV54WWGlosarioDS_7_Glosario_termo2",SqlDbType.VarChar,255,0} ,
          new Object[] {"@lV55WWGlosarioDS_8_Descricao2",SqlDbType.Char,40,0} ,
          new Object[] {"@lV58WWGlosarioDS_11_Glosario_termo3",SqlDbType.VarChar,255,0} ,
          new Object[] {"@lV59WWGlosarioDS_12_Descricao3",SqlDbType.Char,40,0} ,
          new Object[] {"@lV60WWGlosarioDS_13_Tfglosario_termo",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV61WWGlosarioDS_14_Tfglosario_termo_sel",SqlDbType.VarChar,255,0} ,
          new Object[] {"@lV62WWGlosarioDS_15_Tfglosario_nomearq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV63WWGlosarioDS_16_Tfglosario_nomearq_sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00OR2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00OR2,100,0,true,false )
             ,new CursorDef("P00OR3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00OR3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwglosariofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwglosariofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwglosariofilterdata") )
          {
             return  ;
          }
          getwwglosariofilterdata worker = new getwwglosariofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
