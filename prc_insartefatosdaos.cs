/*
               File: PRC_INSArtefatosDaOs
        Description: Inserir Artefatos Da Os
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:52:54.40
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_insartefatosdaos : GXProcedure
   {
      public prc_insartefatosdaos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_insartefatosdaos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultadoArtefato_OSCod )
      {
         this.A1772ContagemResultadoArtefato_OSCod = aP0_ContagemResultadoArtefato_OSCod;
         initialize();
         executePrivate();
         aP0_ContagemResultadoArtefato_OSCod=this.A1772ContagemResultadoArtefato_OSCod;
      }

      public int executeUdp( )
      {
         this.A1772ContagemResultadoArtefato_OSCod = aP0_ContagemResultadoArtefato_OSCod;
         initialize();
         executePrivate();
         aP0_ContagemResultadoArtefato_OSCod=this.A1772ContagemResultadoArtefato_OSCod;
         return A1772ContagemResultadoArtefato_OSCod ;
      }

      public void executeSubmit( ref int aP0_ContagemResultadoArtefato_OSCod )
      {
         prc_insartefatosdaos objprc_insartefatosdaos;
         objprc_insartefatosdaos = new prc_insartefatosdaos();
         objprc_insartefatosdaos.A1772ContagemResultadoArtefato_OSCod = aP0_ContagemResultadoArtefato_OSCod;
         objprc_insartefatosdaos.context.SetSubmitInitialConfig(context);
         objprc_insartefatosdaos.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_insartefatosdaos);
         aP0_ContagemResultadoArtefato_OSCod=this.A1772ContagemResultadoArtefato_OSCod;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_insartefatosdaos)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV8Artefatos.FromXml(AV9WebSession.Get("Artefatos"), "SDT_ArtefatosCollection");
         AV13GXV1 = 1;
         while ( AV13GXV1 <= AV8Artefatos.Count )
         {
            AV10Artefato = ((SdtSDT_Artefatos)AV8Artefatos.Item(AV13GXV1));
            /*
               INSERT RECORD ON TABLE ContagemResultadoArtefato

            */
            A1771ContagemResultadoArtefato_ArtefatoCod = AV10Artefato.gxTpr_Artefatos_codigo;
            A1770ContagemResultadoArtefato_EvdCod = 0;
            n1770ContagemResultadoArtefato_EvdCod = false;
            n1770ContagemResultadoArtefato_EvdCod = true;
            A1773ContagemResultadoArtefato_AnxCod = 0;
            n1773ContagemResultadoArtefato_AnxCod = false;
            n1773ContagemResultadoArtefato_AnxCod = true;
            /* Using cursor P00CY2 */
            pr_default.execute(0, new Object[] {A1772ContagemResultadoArtefato_OSCod, A1771ContagemResultadoArtefato_ArtefatoCod, n1770ContagemResultadoArtefato_EvdCod, A1770ContagemResultadoArtefato_EvdCod, n1773ContagemResultadoArtefato_AnxCod, A1773ContagemResultadoArtefato_AnxCod});
            A1769ContagemResultadoArtefato_Codigo = P00CY2_A1769ContagemResultadoArtefato_Codigo[0];
            pr_default.close(0);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoArtefato") ;
            if ( (pr_default.getStatus(0) == 1) )
            {
               context.Gx_err = 1;
               Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
            }
            else
            {
               context.Gx_err = 0;
               Gx_emsg = "";
            }
            /* End Insert */
            AV13GXV1 = (int)(AV13GXV1+1);
         }
         AV9WebSession.Remove("Artefatos");
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV8Artefatos = new GxObjectCollection( context, "SDT_Artefatos", "GxEv3Up14_MeetrikaVs3", "SdtSDT_Artefatos", "GeneXus.Programs");
         AV9WebSession = context.GetSession();
         AV10Artefato = new SdtSDT_Artefatos(context);
         P00CY2_A1769ContagemResultadoArtefato_Codigo = new int[1] ;
         Gx_emsg = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_insartefatosdaos__default(),
            new Object[][] {
                new Object[] {
               P00CY2_A1769ContagemResultadoArtefato_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A1772ContagemResultadoArtefato_OSCod ;
      private int AV13GXV1 ;
      private int GX_INS196 ;
      private int A1771ContagemResultadoArtefato_ArtefatoCod ;
      private int A1770ContagemResultadoArtefato_EvdCod ;
      private int A1773ContagemResultadoArtefato_AnxCod ;
      private int A1769ContagemResultadoArtefato_Codigo ;
      private String Gx_emsg ;
      private bool n1770ContagemResultadoArtefato_EvdCod ;
      private bool n1773ContagemResultadoArtefato_AnxCod ;
      private IGxSession AV9WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultadoArtefato_OSCod ;
      private IDataStoreProvider pr_default ;
      private int[] P00CY2_A1769ContagemResultadoArtefato_Codigo ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Artefatos ))]
      private IGxCollection AV8Artefatos ;
      private SdtSDT_Artefatos AV10Artefato ;
   }

   public class prc_insartefatosdaos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00CY2 ;
          prmP00CY2 = new Object[] {
          new Object[] {"@ContagemResultadoArtefato_OSCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoArtefato_ArtefatoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoArtefato_EvdCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoArtefato_AnxCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00CY2", "INSERT INTO [ContagemResultadoArtefato]([ContagemResultadoArtefato_OSCod], [ContagemResultadoArtefato_ArtefatoCod], [ContagemResultadoArtefato_EvdCod], [ContagemResultadoArtefato_AnxCod]) VALUES(@ContagemResultadoArtefato_OSCod, @ContagemResultadoArtefato_ArtefatoCod, @ContagemResultadoArtefato_EvdCod, @ContagemResultadoArtefato_AnxCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP00CY2)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[5]);
                }
                return;
       }
    }

 }

}
