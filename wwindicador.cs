/*
               File: WWIndicador
        Description:  Indicador
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 13:45:17.84
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwindicador : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwindicador( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwindicador( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         cmbIndicador_Refer = new GXCombobox();
         cmbIndicador_Aplica = new GXCombobox();
         dltIndicador_NaoCnfCod = new GXListbox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"INDICADOR_NAOCNFCOD") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLAINDICADOR_NAOCNFCODRV2( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_92 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_92_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_92_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV44OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44OrderedBy), 4, 0)));
               AV13OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
               AV14DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14DynamicFiltersSelector1", AV14DynamicFiltersSelector1);
               AV15DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15DynamicFiltersOperator1), 4, 0)));
               AV16Indicador_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Indicador_Nome1", AV16Indicador_Nome1);
               AV18DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
               AV19DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19DynamicFiltersOperator2), 4, 0)));
               AV20Indicador_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Indicador_Nome2", AV20Indicador_Nome2);
               AV22DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersSelector3", AV22DynamicFiltersSelector3);
               AV23DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator3), 4, 0)));
               AV24Indicador_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Indicador_Nome3", AV24Indicador_Nome3);
               AV17DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
               AV21DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersEnabled3", AV21DynamicFiltersEnabled3);
               AV35TFIndicador_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFIndicador_Nome", AV35TFIndicador_Nome);
               AV36TFIndicador_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFIndicador_Nome_Sel", AV36TFIndicador_Nome_Sel);
               AV55TFIndicador_NaoCnfCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFIndicador_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFIndicador_NaoCnfCod), 6, 0)));
               AV56TFIndicador_NaoCnfCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFIndicador_NaoCnfCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFIndicador_NaoCnfCod_To), 6, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV2WWPContext);
               AV28ManageFiltersExecutionStep = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV28ManageFiltersExecutionStep), 1, 0));
               AV37ddo_Indicador_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_Indicador_NomeTitleControlIdToReplace", AV37ddo_Indicador_NomeTitleControlIdToReplace);
               AV49ddo_Indicador_ReferTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_Indicador_ReferTitleControlIdToReplace", AV49ddo_Indicador_ReferTitleControlIdToReplace);
               AV53ddo_Indicador_AplicaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_Indicador_AplicaTitleControlIdToReplace", AV53ddo_Indicador_AplicaTitleControlIdToReplace);
               AV57ddo_Indicador_NaoCnfCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_Indicador_NaoCnfCodTitleControlIdToReplace", AV57ddo_Indicador_NaoCnfCodTitleControlIdToReplace);
               AV45Indicador_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45Indicador_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45Indicador_AreaTrabalhoCod), 6, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV48TFIndicador_Refer_Sels);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV52TFIndicador_Aplica_Sels);
               AV80Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV26DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersIgnoreFirst", AV26DynamicFiltersIgnoreFirst);
               AV25DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersRemoving", AV25DynamicFiltersRemoving);
               A2058Indicador_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV44OrderedBy, AV13OrderedDsc, AV14DynamicFiltersSelector1, AV15DynamicFiltersOperator1, AV16Indicador_Nome1, AV18DynamicFiltersSelector2, AV19DynamicFiltersOperator2, AV20Indicador_Nome2, AV22DynamicFiltersSelector3, AV23DynamicFiltersOperator3, AV24Indicador_Nome3, AV17DynamicFiltersEnabled2, AV21DynamicFiltersEnabled3, AV35TFIndicador_Nome, AV36TFIndicador_Nome_Sel, AV55TFIndicador_NaoCnfCod, AV56TFIndicador_NaoCnfCod_To, AV2WWPContext, AV28ManageFiltersExecutionStep, AV37ddo_Indicador_NomeTitleControlIdToReplace, AV49ddo_Indicador_ReferTitleControlIdToReplace, AV53ddo_Indicador_AplicaTitleControlIdToReplace, AV57ddo_Indicador_NaoCnfCodTitleControlIdToReplace, AV45Indicador_AreaTrabalhoCod, AV48TFIndicador_Refer_Sels, AV52TFIndicador_Aplica_Sels, AV80Pgmname, AV10GridState, AV26DynamicFiltersIgnoreFirst, AV25DynamicFiltersRemoving, A2058Indicador_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PARV2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTRV2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202051813451840");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwindicador.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV13OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV14DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vINDICADOR_NOME1", StringUtil.RTrim( AV16Indicador_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV18DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV19DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vINDICADOR_NOME2", StringUtil.RTrim( AV20Indicador_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV22DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV23DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vINDICADOR_NOME3", StringUtil.RTrim( AV24Indicador_Nome3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV17DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV21DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFINDICADOR_NOME", StringUtil.RTrim( AV35TFIndicador_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFINDICADOR_NOME_SEL", StringUtil.RTrim( AV36TFIndicador_Nome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFINDICADOR_NAOCNFCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV55TFIndicador_NaoCnfCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFINDICADOR_NAOCNFCOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV56TFIndicador_NaoCnfCod_To), 6, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_92", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_92), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMANAGEFILTERSDATA", AV32ManageFiltersData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMANAGEFILTERSDATA", AV32ManageFiltersData);
         }
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV40GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV41GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV38DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV38DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vINDICADOR_NOMETITLEFILTERDATA", AV34Indicador_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vINDICADOR_NOMETITLEFILTERDATA", AV34Indicador_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vINDICADOR_REFERTITLEFILTERDATA", AV46Indicador_ReferTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vINDICADOR_REFERTITLEFILTERDATA", AV46Indicador_ReferTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vINDICADOR_APLICATITLEFILTERDATA", AV50Indicador_AplicaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vINDICADOR_APLICATITLEFILTERDATA", AV50Indicador_AplicaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vINDICADOR_NAOCNFCODTITLEFILTERDATA", AV54Indicador_NaoCnfCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vINDICADOR_NAOCNFCODTITLEFILTERDATA", AV54Indicador_NaoCnfCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV2WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV2WWPContext);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFINDICADOR_REFER_SELS", AV48TFIndicador_Refer_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFINDICADOR_REFER_SELS", AV48TFIndicador_Refer_Sels);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFINDICADOR_APLICA_SELS", AV52TFIndicador_Aplica_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFINDICADOR_APLICA_SELS", AV52TFIndicador_Aplica_Sels);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV80Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV26DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV25DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Icon", StringUtil.RTrim( Ddo_managefilters_Icon));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Caption", StringUtil.RTrim( Ddo_managefilters_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Tooltip", StringUtil.RTrim( Ddo_managefilters_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Cls", StringUtil.RTrim( Ddo_managefilters_Cls));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NOME_Caption", StringUtil.RTrim( Ddo_indicador_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NOME_Tooltip", StringUtil.RTrim( Ddo_indicador_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NOME_Cls", StringUtil.RTrim( Ddo_indicador_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_indicador_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_indicador_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_indicador_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_indicador_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_indicador_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_indicador_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NOME_Sortedstatus", StringUtil.RTrim( Ddo_indicador_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NOME_Includefilter", StringUtil.BoolToStr( Ddo_indicador_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NOME_Filtertype", StringUtil.RTrim( Ddo_indicador_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_indicador_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_indicador_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NOME_Datalisttype", StringUtil.RTrim( Ddo_indicador_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NOME_Datalistproc", StringUtil.RTrim( Ddo_indicador_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_indicador_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NOME_Sortasc", StringUtil.RTrim( Ddo_indicador_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NOME_Sortdsc", StringUtil.RTrim( Ddo_indicador_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NOME_Loadingdata", StringUtil.RTrim( Ddo_indicador_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NOME_Cleanfilter", StringUtil.RTrim( Ddo_indicador_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NOME_Noresultsfound", StringUtil.RTrim( Ddo_indicador_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_indicador_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_REFER_Caption", StringUtil.RTrim( Ddo_indicador_refer_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_REFER_Tooltip", StringUtil.RTrim( Ddo_indicador_refer_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_REFER_Cls", StringUtil.RTrim( Ddo_indicador_refer_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_REFER_Selectedvalue_set", StringUtil.RTrim( Ddo_indicador_refer_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_REFER_Dropdownoptionstype", StringUtil.RTrim( Ddo_indicador_refer_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_REFER_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_indicador_refer_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_REFER_Includesortasc", StringUtil.BoolToStr( Ddo_indicador_refer_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_REFER_Includesortdsc", StringUtil.BoolToStr( Ddo_indicador_refer_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_REFER_Sortedstatus", StringUtil.RTrim( Ddo_indicador_refer_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_REFER_Includefilter", StringUtil.BoolToStr( Ddo_indicador_refer_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_REFER_Includedatalist", StringUtil.BoolToStr( Ddo_indicador_refer_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_REFER_Datalisttype", StringUtil.RTrim( Ddo_indicador_refer_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_REFER_Allowmultipleselection", StringUtil.BoolToStr( Ddo_indicador_refer_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_REFER_Datalistfixedvalues", StringUtil.RTrim( Ddo_indicador_refer_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_REFER_Sortasc", StringUtil.RTrim( Ddo_indicador_refer_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_REFER_Sortdsc", StringUtil.RTrim( Ddo_indicador_refer_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_REFER_Cleanfilter", StringUtil.RTrim( Ddo_indicador_refer_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_REFER_Searchbuttontext", StringUtil.RTrim( Ddo_indicador_refer_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_APLICA_Caption", StringUtil.RTrim( Ddo_indicador_aplica_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_APLICA_Tooltip", StringUtil.RTrim( Ddo_indicador_aplica_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_APLICA_Cls", StringUtil.RTrim( Ddo_indicador_aplica_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_APLICA_Selectedvalue_set", StringUtil.RTrim( Ddo_indicador_aplica_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_APLICA_Dropdownoptionstype", StringUtil.RTrim( Ddo_indicador_aplica_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_APLICA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_indicador_aplica_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_APLICA_Includesortasc", StringUtil.BoolToStr( Ddo_indicador_aplica_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_APLICA_Includesortdsc", StringUtil.BoolToStr( Ddo_indicador_aplica_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_APLICA_Sortedstatus", StringUtil.RTrim( Ddo_indicador_aplica_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_APLICA_Includefilter", StringUtil.BoolToStr( Ddo_indicador_aplica_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_APLICA_Includedatalist", StringUtil.BoolToStr( Ddo_indicador_aplica_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_APLICA_Datalisttype", StringUtil.RTrim( Ddo_indicador_aplica_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_APLICA_Allowmultipleselection", StringUtil.BoolToStr( Ddo_indicador_aplica_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_APLICA_Datalistfixedvalues", StringUtil.RTrim( Ddo_indicador_aplica_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_APLICA_Sortasc", StringUtil.RTrim( Ddo_indicador_aplica_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_APLICA_Sortdsc", StringUtil.RTrim( Ddo_indicador_aplica_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_APLICA_Cleanfilter", StringUtil.RTrim( Ddo_indicador_aplica_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_APLICA_Searchbuttontext", StringUtil.RTrim( Ddo_indicador_aplica_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NAOCNFCOD_Caption", StringUtil.RTrim( Ddo_indicador_naocnfcod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NAOCNFCOD_Tooltip", StringUtil.RTrim( Ddo_indicador_naocnfcod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NAOCNFCOD_Cls", StringUtil.RTrim( Ddo_indicador_naocnfcod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NAOCNFCOD_Filteredtext_set", StringUtil.RTrim( Ddo_indicador_naocnfcod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NAOCNFCOD_Filteredtextto_set", StringUtil.RTrim( Ddo_indicador_naocnfcod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NAOCNFCOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_indicador_naocnfcod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NAOCNFCOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_indicador_naocnfcod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NAOCNFCOD_Includesortasc", StringUtil.BoolToStr( Ddo_indicador_naocnfcod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NAOCNFCOD_Includesortdsc", StringUtil.BoolToStr( Ddo_indicador_naocnfcod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NAOCNFCOD_Sortedstatus", StringUtil.RTrim( Ddo_indicador_naocnfcod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NAOCNFCOD_Includefilter", StringUtil.BoolToStr( Ddo_indicador_naocnfcod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NAOCNFCOD_Filtertype", StringUtil.RTrim( Ddo_indicador_naocnfcod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NAOCNFCOD_Filterisrange", StringUtil.BoolToStr( Ddo_indicador_naocnfcod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NAOCNFCOD_Includedatalist", StringUtil.BoolToStr( Ddo_indicador_naocnfcod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NAOCNFCOD_Sortasc", StringUtil.RTrim( Ddo_indicador_naocnfcod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NAOCNFCOD_Sortdsc", StringUtil.RTrim( Ddo_indicador_naocnfcod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NAOCNFCOD_Cleanfilter", StringUtil.RTrim( Ddo_indicador_naocnfcod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NAOCNFCOD_Rangefilterfrom", StringUtil.RTrim( Ddo_indicador_naocnfcod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NAOCNFCOD_Rangefilterto", StringUtil.RTrim( Ddo_indicador_naocnfcod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NAOCNFCOD_Searchbuttontext", StringUtil.RTrim( Ddo_indicador_naocnfcod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NOME_Activeeventkey", StringUtil.RTrim( Ddo_indicador_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_indicador_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_indicador_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_REFER_Activeeventkey", StringUtil.RTrim( Ddo_indicador_refer_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_REFER_Selectedvalue_get", StringUtil.RTrim( Ddo_indicador_refer_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_APLICA_Activeeventkey", StringUtil.RTrim( Ddo_indicador_aplica_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_APLICA_Selectedvalue_get", StringUtil.RTrim( Ddo_indicador_aplica_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NAOCNFCOD_Activeeventkey", StringUtil.RTrim( Ddo_indicador_naocnfcod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NAOCNFCOD_Filteredtext_get", StringUtil.RTrim( Ddo_indicador_naocnfcod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_INDICADOR_NAOCNFCOD_Filteredtextto_get", StringUtil.RTrim( Ddo_indicador_naocnfcod_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Activeeventkey", StringUtil.RTrim( Ddo_managefilters_Activeeventkey));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WERV2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTRV2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwindicador.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWIndicador" ;
      }

      public override String GetPgmdesc( )
      {
         return " Indicador" ;
      }

      protected void WBRV0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_RV2( true) ;
         }
         else
         {
            wb_table1_2_RV2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_RV2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_92_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV17DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(104, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,104);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_92_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV21DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(105, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,105);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavManagefiltersexecutionstep_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV28ManageFiltersExecutionStep), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV28ManageFiltersExecutionStep), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,106);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavManagefiltersexecutionstep_Jsonclick, 0, "Attribute", "", "", "", edtavManagefiltersexecutionstep_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWIndicador.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfindicador_nome_Internalname, StringUtil.RTrim( AV35TFIndicador_Nome), StringUtil.RTrim( context.localUtil.Format( AV35TFIndicador_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,107);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfindicador_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfindicador_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWIndicador.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfindicador_nome_sel_Internalname, StringUtil.RTrim( AV36TFIndicador_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV36TFIndicador_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,108);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfindicador_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfindicador_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWIndicador.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfindicador_naocnfcod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV55TFIndicador_NaoCnfCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV55TFIndicador_NaoCnfCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,109);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfindicador_naocnfcod_Jsonclick, 0, "Attribute", "", "", "", edtavTfindicador_naocnfcod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWIndicador.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfindicador_naocnfcod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV56TFIndicador_NaoCnfCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV56TFIndicador_NaoCnfCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,110);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfindicador_naocnfcod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfindicador_naocnfcod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWIndicador.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_INDICADOR_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_92_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_indicador_nometitlecontrolidtoreplace_Internalname, AV37ddo_Indicador_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,112);\"", 0, edtavDdo_indicador_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWIndicador.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_INDICADOR_REFERContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_92_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_indicador_refertitlecontrolidtoreplace_Internalname, AV49ddo_Indicador_ReferTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,114);\"", 0, edtavDdo_indicador_refertitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWIndicador.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_INDICADOR_APLICAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'" + sGXsfl_92_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_indicador_aplicatitlecontrolidtoreplace_Internalname, AV53ddo_Indicador_AplicaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,116);\"", 0, edtavDdo_indicador_aplicatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWIndicador.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_INDICADOR_NAOCNFCODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'" + sGXsfl_92_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_indicador_naocnfcodtitlecontrolidtoreplace_Internalname, AV57ddo_Indicador_NaoCnfCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,118);\"", 0, edtavDdo_indicador_naocnfcodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWIndicador.htm");
         }
         wbLoad = true;
      }

      protected void STARTRV2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Indicador", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPRV0( ) ;
      }

      protected void WSRV2( )
      {
         STARTRV2( ) ;
         EVTRV2( ) ;
      }

      protected void EVTRV2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_MANAGEFILTERS.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11RV2 */
                              E11RV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12RV2 */
                              E12RV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_INDICADOR_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13RV2 */
                              E13RV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_INDICADOR_REFER.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14RV2 */
                              E14RV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_INDICADOR_APLICA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15RV2 */
                              E15RV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_INDICADOR_NAOCNFCOD.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16RV2 */
                              E16RV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17RV2 */
                              E17RV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18RV2 */
                              E18RV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19RV2 */
                              E19RV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20RV2 */
                              E20RV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21RV2 */
                              E21RV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22RV2 */
                              E22RV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23RV2 */
                              E23RV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E24RV2 */
                              E24RV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E25RV2 */
                              E25RV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E26RV2 */
                              E26RV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_92_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_92_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_92_idx), 4, 0)), 4, "0");
                              SubsflControlProps_922( ) ;
                              AV42Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV42Update)) ? AV78Update_GXI : context.convertURL( context.PathToRelativeUrl( AV42Update))));
                              AV43Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV43Delete)) ? AV79Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV43Delete))));
                              A2058Indicador_Codigo = (int)(context.localUtil.CToN( cgiGet( edtIndicador_Codigo_Internalname), ",", "."));
                              A2059Indicador_Nome = StringUtil.Upper( cgiGet( edtIndicador_Nome_Internalname));
                              cmbIndicador_Refer.Name = cmbIndicador_Refer_Internalname;
                              cmbIndicador_Refer.CurrentValue = cgiGet( cmbIndicador_Refer_Internalname);
                              A2063Indicador_Refer = (short)(NumberUtil.Val( cgiGet( cmbIndicador_Refer_Internalname), "."));
                              n2063Indicador_Refer = false;
                              cmbIndicador_Aplica.Name = cmbIndicador_Aplica_Internalname;
                              cmbIndicador_Aplica.CurrentValue = cgiGet( cmbIndicador_Aplica_Internalname);
                              A2064Indicador_Aplica = (short)(NumberUtil.Val( cgiGet( cmbIndicador_Aplica_Internalname), "."));
                              n2064Indicador_Aplica = false;
                              dltIndicador_NaoCnfCod.Name = dltIndicador_NaoCnfCod_Internalname;
                              dltIndicador_NaoCnfCod.CurrentValue = cgiGet( dltIndicador_NaoCnfCod_Internalname);
                              A2062Indicador_NaoCnfCod = (int)(NumberUtil.Val( cgiGet( dltIndicador_NaoCnfCod_Internalname), "."));
                              n2062Indicador_NaoCnfCod = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E27RV2 */
                                    E27RV2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E28RV2 */
                                    E28RV2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E29RV2 */
                                    E29RV2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV44OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV13OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV14DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV15DynamicFiltersOperator1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Indicador_nome1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vINDICADOR_NOME1"), AV16Indicador_Nome1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV18DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV19DynamicFiltersOperator2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Indicador_nome2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vINDICADOR_NOME2"), AV20Indicador_Nome2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV22DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV23DynamicFiltersOperator3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Indicador_nome3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vINDICADOR_NOME3"), AV24Indicador_Nome3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV17DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV21DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfindicador_nome Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFINDICADOR_NOME"), AV35TFIndicador_Nome) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfindicador_nome_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFINDICADOR_NOME_SEL"), AV36TFIndicador_Nome_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfindicador_naocnfcod Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFINDICADOR_NAOCNFCOD"), ",", ".") != Convert.ToDecimal( AV55TFIndicador_NaoCnfCod )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfindicador_naocnfcod_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFINDICADOR_NAOCNFCOD_TO"), ",", ".") != Convert.ToDecimal( AV56TFIndicador_NaoCnfCod_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WERV2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PARV2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV44OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV44OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("INDICADOR_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV14DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV14DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14DynamicFiltersSelector1", AV14DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV15DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV15DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("INDICADOR_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV18DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV18DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV19DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV19DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("INDICADOR_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV22DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV22DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersSelector3", AV22DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV23DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator3), 4, 0)));
            }
            GXCCtl = "INDICADOR_REFER_" + sGXsfl_92_idx;
            cmbIndicador_Refer.Name = GXCCtl;
            cmbIndicador_Refer.WebTags = "";
            cmbIndicador_Refer.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "(Nenhum)", 0);
            cmbIndicador_Refer.addItem("1", "Prazo", 0);
            cmbIndicador_Refer.addItem("2", "Frequ�ncia", 0);
            cmbIndicador_Refer.addItem("3", "Valores", 0);
            if ( cmbIndicador_Refer.ItemCount > 0 )
            {
               A2063Indicador_Refer = (short)(NumberUtil.Val( cmbIndicador_Refer.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2063Indicador_Refer), 4, 0))), "."));
               n2063Indicador_Refer = false;
            }
            GXCCtl = "INDICADOR_APLICA_" + sGXsfl_92_idx;
            cmbIndicador_Aplica.Name = GXCCtl;
            cmbIndicador_Aplica.WebTags = "";
            cmbIndicador_Aplica.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "(Nenhum)", 0);
            cmbIndicador_Aplica.addItem("1", "Demandas", 0);
            cmbIndicador_Aplica.addItem("2", "Lotes", 0);
            if ( cmbIndicador_Aplica.ItemCount > 0 )
            {
               A2064Indicador_Aplica = (short)(NumberUtil.Val( cmbIndicador_Aplica.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2064Indicador_Aplica), 4, 0))), "."));
               n2064Indicador_Aplica = false;
            }
            GXCCtl = "INDICADOR_NAOCNFCOD_" + sGXsfl_92_idx;
            dltIndicador_NaoCnfCod.Name = GXCCtl;
            dltIndicador_NaoCnfCod.WebTags = "";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLAINDICADOR_NAOCNFCODRV2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAINDICADOR_NAOCNFCOD_dataRV2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAINDICADOR_NAOCNFCOD_htmlRV2( )
      {
         int gxdynajaxvalue ;
         GXDLAINDICADOR_NAOCNFCOD_dataRV2( ) ;
         gxdynajaxindex = 1;
         dltIndicador_NaoCnfCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dltIndicador_NaoCnfCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAINDICADOR_NAOCNFCOD_dataRV2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhuma)");
         /* Using cursor H00RV2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00RV2_A2062Indicador_NaoCnfCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00RV2_A427NaoConformidade_Nome[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_922( ) ;
         while ( nGXsfl_92_idx <= nRC_GXsfl_92 )
         {
            sendrow_922( ) ;
            nGXsfl_92_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_92_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_92_idx+1));
            sGXsfl_92_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_92_idx), 4, 0)), 4, "0");
            SubsflControlProps_922( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV44OrderedBy ,
                                       bool AV13OrderedDsc ,
                                       String AV14DynamicFiltersSelector1 ,
                                       short AV15DynamicFiltersOperator1 ,
                                       String AV16Indicador_Nome1 ,
                                       String AV18DynamicFiltersSelector2 ,
                                       short AV19DynamicFiltersOperator2 ,
                                       String AV20Indicador_Nome2 ,
                                       String AV22DynamicFiltersSelector3 ,
                                       short AV23DynamicFiltersOperator3 ,
                                       String AV24Indicador_Nome3 ,
                                       bool AV17DynamicFiltersEnabled2 ,
                                       bool AV21DynamicFiltersEnabled3 ,
                                       String AV35TFIndicador_Nome ,
                                       String AV36TFIndicador_Nome_Sel ,
                                       int AV55TFIndicador_NaoCnfCod ,
                                       int AV56TFIndicador_NaoCnfCod_To ,
                                       wwpbaseobjects.SdtWWPContext AV2WWPContext ,
                                       short AV28ManageFiltersExecutionStep ,
                                       String AV37ddo_Indicador_NomeTitleControlIdToReplace ,
                                       String AV49ddo_Indicador_ReferTitleControlIdToReplace ,
                                       String AV53ddo_Indicador_AplicaTitleControlIdToReplace ,
                                       String AV57ddo_Indicador_NaoCnfCodTitleControlIdToReplace ,
                                       int AV45Indicador_AreaTrabalhoCod ,
                                       IGxCollection AV48TFIndicador_Refer_Sels ,
                                       IGxCollection AV52TFIndicador_Aplica_Sels ,
                                       String AV80Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV26DynamicFiltersIgnoreFirst ,
                                       bool AV25DynamicFiltersRemoving ,
                                       int A2058Indicador_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFRV2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_INDICADOR_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A2058Indicador_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "INDICADOR_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2058Indicador_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_INDICADOR_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A2059Indicador_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "INDICADOR_NOME", StringUtil.RTrim( A2059Indicador_Nome));
         GxWebStd.gx_hidden_field( context, "gxhash_INDICADOR_REFER", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A2063Indicador_Refer), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "INDICADOR_REFER", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2063Indicador_Refer), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_INDICADOR_APLICA", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A2064Indicador_Aplica), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "INDICADOR_APLICA", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2064Indicador_Aplica), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_INDICADOR_NAOCNFCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A2062Indicador_NaoCnfCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "INDICADOR_NAOCNFCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2062Indicador_NaoCnfCod), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV44OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV44OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV14DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV14DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14DynamicFiltersSelector1", AV14DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV15DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV15DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV18DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV18DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV19DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV19DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV22DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV22DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersSelector3", AV22DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV23DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFRV2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV80Pgmname = "WWIndicador";
         context.Gx_err = 0;
      }

      protected void RFRV2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 92;
         /* Execute user event: E28RV2 */
         E28RV2 ();
         nGXsfl_92_idx = 1;
         sGXsfl_92_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_92_idx), 4, 0)), 4, "0");
         SubsflControlProps_922( ) ;
         nGXsfl_92_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_922( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(1, new Object[]{ new Object[]{
                                                 A2063Indicador_Refer ,
                                                 AV74WWIndicadorDS_15_Tfindicador_refer_sels ,
                                                 A2064Indicador_Aplica ,
                                                 AV75WWIndicadorDS_16_Tfindicador_aplica_sels ,
                                                 AV61WWIndicadorDS_2_Dynamicfiltersselector1 ,
                                                 AV62WWIndicadorDS_3_Dynamicfiltersoperator1 ,
                                                 AV63WWIndicadorDS_4_Indicador_nome1 ,
                                                 AV64WWIndicadorDS_5_Dynamicfiltersenabled2 ,
                                                 AV65WWIndicadorDS_6_Dynamicfiltersselector2 ,
                                                 AV66WWIndicadorDS_7_Dynamicfiltersoperator2 ,
                                                 AV67WWIndicadorDS_8_Indicador_nome2 ,
                                                 AV68WWIndicadorDS_9_Dynamicfiltersenabled3 ,
                                                 AV69WWIndicadorDS_10_Dynamicfiltersselector3 ,
                                                 AV70WWIndicadorDS_11_Dynamicfiltersoperator3 ,
                                                 AV71WWIndicadorDS_12_Indicador_nome3 ,
                                                 AV73WWIndicadorDS_14_Tfindicador_nome_sel ,
                                                 AV72WWIndicadorDS_13_Tfindicador_nome ,
                                                 AV74WWIndicadorDS_15_Tfindicador_refer_sels.Count ,
                                                 AV75WWIndicadorDS_16_Tfindicador_aplica_sels.Count ,
                                                 AV76WWIndicadorDS_17_Tfindicador_naocnfcod ,
                                                 AV77WWIndicadorDS_18_Tfindicador_naocnfcod_to ,
                                                 A2059Indicador_Nome ,
                                                 A2062Indicador_NaoCnfCod ,
                                                 AV44OrderedBy ,
                                                 AV13OrderedDsc ,
                                                 A2061Indicador_AreaTrabalhoCod ,
                                                 AV2WWPContext.gxTpr_Areatrabalho_codigo },
                                                 new int[] {
                                                 TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                                 TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                                 }
            });
            lV63WWIndicadorDS_4_Indicador_nome1 = StringUtil.PadR( StringUtil.RTrim( AV63WWIndicadorDS_4_Indicador_nome1), 50, "%");
            lV63WWIndicadorDS_4_Indicador_nome1 = StringUtil.PadR( StringUtil.RTrim( AV63WWIndicadorDS_4_Indicador_nome1), 50, "%");
            lV67WWIndicadorDS_8_Indicador_nome2 = StringUtil.PadR( StringUtil.RTrim( AV67WWIndicadorDS_8_Indicador_nome2), 50, "%");
            lV67WWIndicadorDS_8_Indicador_nome2 = StringUtil.PadR( StringUtil.RTrim( AV67WWIndicadorDS_8_Indicador_nome2), 50, "%");
            lV71WWIndicadorDS_12_Indicador_nome3 = StringUtil.PadR( StringUtil.RTrim( AV71WWIndicadorDS_12_Indicador_nome3), 50, "%");
            lV71WWIndicadorDS_12_Indicador_nome3 = StringUtil.PadR( StringUtil.RTrim( AV71WWIndicadorDS_12_Indicador_nome3), 50, "%");
            lV72WWIndicadorDS_13_Tfindicador_nome = StringUtil.PadR( StringUtil.RTrim( AV72WWIndicadorDS_13_Tfindicador_nome), 50, "%");
            /* Using cursor H00RV3 */
            pr_default.execute(1, new Object[] {AV2WWPContext.gxTpr_Areatrabalho_codigo, lV63WWIndicadorDS_4_Indicador_nome1, lV63WWIndicadorDS_4_Indicador_nome1, lV67WWIndicadorDS_8_Indicador_nome2, lV67WWIndicadorDS_8_Indicador_nome2, lV71WWIndicadorDS_12_Indicador_nome3, lV71WWIndicadorDS_12_Indicador_nome3, lV72WWIndicadorDS_13_Tfindicador_nome, AV73WWIndicadorDS_14_Tfindicador_nome_sel, AV76WWIndicadorDS_17_Tfindicador_naocnfcod, AV77WWIndicadorDS_18_Tfindicador_naocnfcod_to, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_92_idx = 1;
            while ( ( (pr_default.getStatus(1) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A2061Indicador_AreaTrabalhoCod = H00RV3_A2061Indicador_AreaTrabalhoCod[0];
               n2061Indicador_AreaTrabalhoCod = H00RV3_n2061Indicador_AreaTrabalhoCod[0];
               A2062Indicador_NaoCnfCod = H00RV3_A2062Indicador_NaoCnfCod[0];
               n2062Indicador_NaoCnfCod = H00RV3_n2062Indicador_NaoCnfCod[0];
               A2064Indicador_Aplica = H00RV3_A2064Indicador_Aplica[0];
               n2064Indicador_Aplica = H00RV3_n2064Indicador_Aplica[0];
               A2063Indicador_Refer = H00RV3_A2063Indicador_Refer[0];
               n2063Indicador_Refer = H00RV3_n2063Indicador_Refer[0];
               A2059Indicador_Nome = H00RV3_A2059Indicador_Nome[0];
               A2058Indicador_Codigo = H00RV3_A2058Indicador_Codigo[0];
               /* Execute user event: E29RV2 */
               E29RV2 ();
               pr_default.readNext(1);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(1) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(1);
            wbEnd = 92;
            WBRV0( ) ;
         }
         nGXsfl_92_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV60WWIndicadorDS_1_Indicador_areatrabalhocod = AV45Indicador_AreaTrabalhoCod;
         AV61WWIndicadorDS_2_Dynamicfiltersselector1 = AV14DynamicFiltersSelector1;
         AV62WWIndicadorDS_3_Dynamicfiltersoperator1 = AV15DynamicFiltersOperator1;
         AV63WWIndicadorDS_4_Indicador_nome1 = AV16Indicador_Nome1;
         AV64WWIndicadorDS_5_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV65WWIndicadorDS_6_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV66WWIndicadorDS_7_Dynamicfiltersoperator2 = AV19DynamicFiltersOperator2;
         AV67WWIndicadorDS_8_Indicador_nome2 = AV20Indicador_Nome2;
         AV68WWIndicadorDS_9_Dynamicfiltersenabled3 = AV21DynamicFiltersEnabled3;
         AV69WWIndicadorDS_10_Dynamicfiltersselector3 = AV22DynamicFiltersSelector3;
         AV70WWIndicadorDS_11_Dynamicfiltersoperator3 = AV23DynamicFiltersOperator3;
         AV71WWIndicadorDS_12_Indicador_nome3 = AV24Indicador_Nome3;
         AV72WWIndicadorDS_13_Tfindicador_nome = AV35TFIndicador_Nome;
         AV73WWIndicadorDS_14_Tfindicador_nome_sel = AV36TFIndicador_Nome_Sel;
         AV74WWIndicadorDS_15_Tfindicador_refer_sels = AV48TFIndicador_Refer_Sels;
         AV75WWIndicadorDS_16_Tfindicador_aplica_sels = AV52TFIndicador_Aplica_Sels;
         AV76WWIndicadorDS_17_Tfindicador_naocnfcod = AV55TFIndicador_NaoCnfCod;
         AV77WWIndicadorDS_18_Tfindicador_naocnfcod_to = AV56TFIndicador_NaoCnfCod_To;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A2063Indicador_Refer ,
                                              AV74WWIndicadorDS_15_Tfindicador_refer_sels ,
                                              A2064Indicador_Aplica ,
                                              AV75WWIndicadorDS_16_Tfindicador_aplica_sels ,
                                              AV61WWIndicadorDS_2_Dynamicfiltersselector1 ,
                                              AV62WWIndicadorDS_3_Dynamicfiltersoperator1 ,
                                              AV63WWIndicadorDS_4_Indicador_nome1 ,
                                              AV64WWIndicadorDS_5_Dynamicfiltersenabled2 ,
                                              AV65WWIndicadorDS_6_Dynamicfiltersselector2 ,
                                              AV66WWIndicadorDS_7_Dynamicfiltersoperator2 ,
                                              AV67WWIndicadorDS_8_Indicador_nome2 ,
                                              AV68WWIndicadorDS_9_Dynamicfiltersenabled3 ,
                                              AV69WWIndicadorDS_10_Dynamicfiltersselector3 ,
                                              AV70WWIndicadorDS_11_Dynamicfiltersoperator3 ,
                                              AV71WWIndicadorDS_12_Indicador_nome3 ,
                                              AV73WWIndicadorDS_14_Tfindicador_nome_sel ,
                                              AV72WWIndicadorDS_13_Tfindicador_nome ,
                                              AV74WWIndicadorDS_15_Tfindicador_refer_sels.Count ,
                                              AV75WWIndicadorDS_16_Tfindicador_aplica_sels.Count ,
                                              AV76WWIndicadorDS_17_Tfindicador_naocnfcod ,
                                              AV77WWIndicadorDS_18_Tfindicador_naocnfcod_to ,
                                              A2059Indicador_Nome ,
                                              A2062Indicador_NaoCnfCod ,
                                              AV44OrderedBy ,
                                              AV13OrderedDsc ,
                                              A2061Indicador_AreaTrabalhoCod ,
                                              AV2WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV63WWIndicadorDS_4_Indicador_nome1 = StringUtil.PadR( StringUtil.RTrim( AV63WWIndicadorDS_4_Indicador_nome1), 50, "%");
         lV63WWIndicadorDS_4_Indicador_nome1 = StringUtil.PadR( StringUtil.RTrim( AV63WWIndicadorDS_4_Indicador_nome1), 50, "%");
         lV67WWIndicadorDS_8_Indicador_nome2 = StringUtil.PadR( StringUtil.RTrim( AV67WWIndicadorDS_8_Indicador_nome2), 50, "%");
         lV67WWIndicadorDS_8_Indicador_nome2 = StringUtil.PadR( StringUtil.RTrim( AV67WWIndicadorDS_8_Indicador_nome2), 50, "%");
         lV71WWIndicadorDS_12_Indicador_nome3 = StringUtil.PadR( StringUtil.RTrim( AV71WWIndicadorDS_12_Indicador_nome3), 50, "%");
         lV71WWIndicadorDS_12_Indicador_nome3 = StringUtil.PadR( StringUtil.RTrim( AV71WWIndicadorDS_12_Indicador_nome3), 50, "%");
         lV72WWIndicadorDS_13_Tfindicador_nome = StringUtil.PadR( StringUtil.RTrim( AV72WWIndicadorDS_13_Tfindicador_nome), 50, "%");
         /* Using cursor H00RV4 */
         pr_default.execute(2, new Object[] {AV2WWPContext.gxTpr_Areatrabalho_codigo, lV63WWIndicadorDS_4_Indicador_nome1, lV63WWIndicadorDS_4_Indicador_nome1, lV67WWIndicadorDS_8_Indicador_nome2, lV67WWIndicadorDS_8_Indicador_nome2, lV71WWIndicadorDS_12_Indicador_nome3, lV71WWIndicadorDS_12_Indicador_nome3, lV72WWIndicadorDS_13_Tfindicador_nome, AV73WWIndicadorDS_14_Tfindicador_nome_sel, AV76WWIndicadorDS_17_Tfindicador_naocnfcod, AV77WWIndicadorDS_18_Tfindicador_naocnfcod_to});
         GRID_nRecordCount = H00RV4_AGRID_nRecordCount[0];
         pr_default.close(2);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV60WWIndicadorDS_1_Indicador_areatrabalhocod = AV45Indicador_AreaTrabalhoCod;
         AV61WWIndicadorDS_2_Dynamicfiltersselector1 = AV14DynamicFiltersSelector1;
         AV62WWIndicadorDS_3_Dynamicfiltersoperator1 = AV15DynamicFiltersOperator1;
         AV63WWIndicadorDS_4_Indicador_nome1 = AV16Indicador_Nome1;
         AV64WWIndicadorDS_5_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV65WWIndicadorDS_6_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV66WWIndicadorDS_7_Dynamicfiltersoperator2 = AV19DynamicFiltersOperator2;
         AV67WWIndicadorDS_8_Indicador_nome2 = AV20Indicador_Nome2;
         AV68WWIndicadorDS_9_Dynamicfiltersenabled3 = AV21DynamicFiltersEnabled3;
         AV69WWIndicadorDS_10_Dynamicfiltersselector3 = AV22DynamicFiltersSelector3;
         AV70WWIndicadorDS_11_Dynamicfiltersoperator3 = AV23DynamicFiltersOperator3;
         AV71WWIndicadorDS_12_Indicador_nome3 = AV24Indicador_Nome3;
         AV72WWIndicadorDS_13_Tfindicador_nome = AV35TFIndicador_Nome;
         AV73WWIndicadorDS_14_Tfindicador_nome_sel = AV36TFIndicador_Nome_Sel;
         AV74WWIndicadorDS_15_Tfindicador_refer_sels = AV48TFIndicador_Refer_Sels;
         AV75WWIndicadorDS_16_Tfindicador_aplica_sels = AV52TFIndicador_Aplica_Sels;
         AV76WWIndicadorDS_17_Tfindicador_naocnfcod = AV55TFIndicador_NaoCnfCod;
         AV77WWIndicadorDS_18_Tfindicador_naocnfcod_to = AV56TFIndicador_NaoCnfCod_To;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV44OrderedBy, AV13OrderedDsc, AV14DynamicFiltersSelector1, AV15DynamicFiltersOperator1, AV16Indicador_Nome1, AV18DynamicFiltersSelector2, AV19DynamicFiltersOperator2, AV20Indicador_Nome2, AV22DynamicFiltersSelector3, AV23DynamicFiltersOperator3, AV24Indicador_Nome3, AV17DynamicFiltersEnabled2, AV21DynamicFiltersEnabled3, AV35TFIndicador_Nome, AV36TFIndicador_Nome_Sel, AV55TFIndicador_NaoCnfCod, AV56TFIndicador_NaoCnfCod_To, AV2WWPContext, AV28ManageFiltersExecutionStep, AV37ddo_Indicador_NomeTitleControlIdToReplace, AV49ddo_Indicador_ReferTitleControlIdToReplace, AV53ddo_Indicador_AplicaTitleControlIdToReplace, AV57ddo_Indicador_NaoCnfCodTitleControlIdToReplace, AV45Indicador_AreaTrabalhoCod, AV48TFIndicador_Refer_Sels, AV52TFIndicador_Aplica_Sels, AV80Pgmname, AV10GridState, AV26DynamicFiltersIgnoreFirst, AV25DynamicFiltersRemoving, A2058Indicador_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV60WWIndicadorDS_1_Indicador_areatrabalhocod = AV45Indicador_AreaTrabalhoCod;
         AV61WWIndicadorDS_2_Dynamicfiltersselector1 = AV14DynamicFiltersSelector1;
         AV62WWIndicadorDS_3_Dynamicfiltersoperator1 = AV15DynamicFiltersOperator1;
         AV63WWIndicadorDS_4_Indicador_nome1 = AV16Indicador_Nome1;
         AV64WWIndicadorDS_5_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV65WWIndicadorDS_6_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV66WWIndicadorDS_7_Dynamicfiltersoperator2 = AV19DynamicFiltersOperator2;
         AV67WWIndicadorDS_8_Indicador_nome2 = AV20Indicador_Nome2;
         AV68WWIndicadorDS_9_Dynamicfiltersenabled3 = AV21DynamicFiltersEnabled3;
         AV69WWIndicadorDS_10_Dynamicfiltersselector3 = AV22DynamicFiltersSelector3;
         AV70WWIndicadorDS_11_Dynamicfiltersoperator3 = AV23DynamicFiltersOperator3;
         AV71WWIndicadorDS_12_Indicador_nome3 = AV24Indicador_Nome3;
         AV72WWIndicadorDS_13_Tfindicador_nome = AV35TFIndicador_Nome;
         AV73WWIndicadorDS_14_Tfindicador_nome_sel = AV36TFIndicador_Nome_Sel;
         AV74WWIndicadorDS_15_Tfindicador_refer_sels = AV48TFIndicador_Refer_Sels;
         AV75WWIndicadorDS_16_Tfindicador_aplica_sels = AV52TFIndicador_Aplica_Sels;
         AV76WWIndicadorDS_17_Tfindicador_naocnfcod = AV55TFIndicador_NaoCnfCod;
         AV77WWIndicadorDS_18_Tfindicador_naocnfcod_to = AV56TFIndicador_NaoCnfCod_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV44OrderedBy, AV13OrderedDsc, AV14DynamicFiltersSelector1, AV15DynamicFiltersOperator1, AV16Indicador_Nome1, AV18DynamicFiltersSelector2, AV19DynamicFiltersOperator2, AV20Indicador_Nome2, AV22DynamicFiltersSelector3, AV23DynamicFiltersOperator3, AV24Indicador_Nome3, AV17DynamicFiltersEnabled2, AV21DynamicFiltersEnabled3, AV35TFIndicador_Nome, AV36TFIndicador_Nome_Sel, AV55TFIndicador_NaoCnfCod, AV56TFIndicador_NaoCnfCod_To, AV2WWPContext, AV28ManageFiltersExecutionStep, AV37ddo_Indicador_NomeTitleControlIdToReplace, AV49ddo_Indicador_ReferTitleControlIdToReplace, AV53ddo_Indicador_AplicaTitleControlIdToReplace, AV57ddo_Indicador_NaoCnfCodTitleControlIdToReplace, AV45Indicador_AreaTrabalhoCod, AV48TFIndicador_Refer_Sels, AV52TFIndicador_Aplica_Sels, AV80Pgmname, AV10GridState, AV26DynamicFiltersIgnoreFirst, AV25DynamicFiltersRemoving, A2058Indicador_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV60WWIndicadorDS_1_Indicador_areatrabalhocod = AV45Indicador_AreaTrabalhoCod;
         AV61WWIndicadorDS_2_Dynamicfiltersselector1 = AV14DynamicFiltersSelector1;
         AV62WWIndicadorDS_3_Dynamicfiltersoperator1 = AV15DynamicFiltersOperator1;
         AV63WWIndicadorDS_4_Indicador_nome1 = AV16Indicador_Nome1;
         AV64WWIndicadorDS_5_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV65WWIndicadorDS_6_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV66WWIndicadorDS_7_Dynamicfiltersoperator2 = AV19DynamicFiltersOperator2;
         AV67WWIndicadorDS_8_Indicador_nome2 = AV20Indicador_Nome2;
         AV68WWIndicadorDS_9_Dynamicfiltersenabled3 = AV21DynamicFiltersEnabled3;
         AV69WWIndicadorDS_10_Dynamicfiltersselector3 = AV22DynamicFiltersSelector3;
         AV70WWIndicadorDS_11_Dynamicfiltersoperator3 = AV23DynamicFiltersOperator3;
         AV71WWIndicadorDS_12_Indicador_nome3 = AV24Indicador_Nome3;
         AV72WWIndicadorDS_13_Tfindicador_nome = AV35TFIndicador_Nome;
         AV73WWIndicadorDS_14_Tfindicador_nome_sel = AV36TFIndicador_Nome_Sel;
         AV74WWIndicadorDS_15_Tfindicador_refer_sels = AV48TFIndicador_Refer_Sels;
         AV75WWIndicadorDS_16_Tfindicador_aplica_sels = AV52TFIndicador_Aplica_Sels;
         AV76WWIndicadorDS_17_Tfindicador_naocnfcod = AV55TFIndicador_NaoCnfCod;
         AV77WWIndicadorDS_18_Tfindicador_naocnfcod_to = AV56TFIndicador_NaoCnfCod_To;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV44OrderedBy, AV13OrderedDsc, AV14DynamicFiltersSelector1, AV15DynamicFiltersOperator1, AV16Indicador_Nome1, AV18DynamicFiltersSelector2, AV19DynamicFiltersOperator2, AV20Indicador_Nome2, AV22DynamicFiltersSelector3, AV23DynamicFiltersOperator3, AV24Indicador_Nome3, AV17DynamicFiltersEnabled2, AV21DynamicFiltersEnabled3, AV35TFIndicador_Nome, AV36TFIndicador_Nome_Sel, AV55TFIndicador_NaoCnfCod, AV56TFIndicador_NaoCnfCod_To, AV2WWPContext, AV28ManageFiltersExecutionStep, AV37ddo_Indicador_NomeTitleControlIdToReplace, AV49ddo_Indicador_ReferTitleControlIdToReplace, AV53ddo_Indicador_AplicaTitleControlIdToReplace, AV57ddo_Indicador_NaoCnfCodTitleControlIdToReplace, AV45Indicador_AreaTrabalhoCod, AV48TFIndicador_Refer_Sels, AV52TFIndicador_Aplica_Sels, AV80Pgmname, AV10GridState, AV26DynamicFiltersIgnoreFirst, AV25DynamicFiltersRemoving, A2058Indicador_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV60WWIndicadorDS_1_Indicador_areatrabalhocod = AV45Indicador_AreaTrabalhoCod;
         AV61WWIndicadorDS_2_Dynamicfiltersselector1 = AV14DynamicFiltersSelector1;
         AV62WWIndicadorDS_3_Dynamicfiltersoperator1 = AV15DynamicFiltersOperator1;
         AV63WWIndicadorDS_4_Indicador_nome1 = AV16Indicador_Nome1;
         AV64WWIndicadorDS_5_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV65WWIndicadorDS_6_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV66WWIndicadorDS_7_Dynamicfiltersoperator2 = AV19DynamicFiltersOperator2;
         AV67WWIndicadorDS_8_Indicador_nome2 = AV20Indicador_Nome2;
         AV68WWIndicadorDS_9_Dynamicfiltersenabled3 = AV21DynamicFiltersEnabled3;
         AV69WWIndicadorDS_10_Dynamicfiltersselector3 = AV22DynamicFiltersSelector3;
         AV70WWIndicadorDS_11_Dynamicfiltersoperator3 = AV23DynamicFiltersOperator3;
         AV71WWIndicadorDS_12_Indicador_nome3 = AV24Indicador_Nome3;
         AV72WWIndicadorDS_13_Tfindicador_nome = AV35TFIndicador_Nome;
         AV73WWIndicadorDS_14_Tfindicador_nome_sel = AV36TFIndicador_Nome_Sel;
         AV74WWIndicadorDS_15_Tfindicador_refer_sels = AV48TFIndicador_Refer_Sels;
         AV75WWIndicadorDS_16_Tfindicador_aplica_sels = AV52TFIndicador_Aplica_Sels;
         AV76WWIndicadorDS_17_Tfindicador_naocnfcod = AV55TFIndicador_NaoCnfCod;
         AV77WWIndicadorDS_18_Tfindicador_naocnfcod_to = AV56TFIndicador_NaoCnfCod_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV44OrderedBy, AV13OrderedDsc, AV14DynamicFiltersSelector1, AV15DynamicFiltersOperator1, AV16Indicador_Nome1, AV18DynamicFiltersSelector2, AV19DynamicFiltersOperator2, AV20Indicador_Nome2, AV22DynamicFiltersSelector3, AV23DynamicFiltersOperator3, AV24Indicador_Nome3, AV17DynamicFiltersEnabled2, AV21DynamicFiltersEnabled3, AV35TFIndicador_Nome, AV36TFIndicador_Nome_Sel, AV55TFIndicador_NaoCnfCod, AV56TFIndicador_NaoCnfCod_To, AV2WWPContext, AV28ManageFiltersExecutionStep, AV37ddo_Indicador_NomeTitleControlIdToReplace, AV49ddo_Indicador_ReferTitleControlIdToReplace, AV53ddo_Indicador_AplicaTitleControlIdToReplace, AV57ddo_Indicador_NaoCnfCodTitleControlIdToReplace, AV45Indicador_AreaTrabalhoCod, AV48TFIndicador_Refer_Sels, AV52TFIndicador_Aplica_Sels, AV80Pgmname, AV10GridState, AV26DynamicFiltersIgnoreFirst, AV25DynamicFiltersRemoving, A2058Indicador_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV60WWIndicadorDS_1_Indicador_areatrabalhocod = AV45Indicador_AreaTrabalhoCod;
         AV61WWIndicadorDS_2_Dynamicfiltersselector1 = AV14DynamicFiltersSelector1;
         AV62WWIndicadorDS_3_Dynamicfiltersoperator1 = AV15DynamicFiltersOperator1;
         AV63WWIndicadorDS_4_Indicador_nome1 = AV16Indicador_Nome1;
         AV64WWIndicadorDS_5_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV65WWIndicadorDS_6_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV66WWIndicadorDS_7_Dynamicfiltersoperator2 = AV19DynamicFiltersOperator2;
         AV67WWIndicadorDS_8_Indicador_nome2 = AV20Indicador_Nome2;
         AV68WWIndicadorDS_9_Dynamicfiltersenabled3 = AV21DynamicFiltersEnabled3;
         AV69WWIndicadorDS_10_Dynamicfiltersselector3 = AV22DynamicFiltersSelector3;
         AV70WWIndicadorDS_11_Dynamicfiltersoperator3 = AV23DynamicFiltersOperator3;
         AV71WWIndicadorDS_12_Indicador_nome3 = AV24Indicador_Nome3;
         AV72WWIndicadorDS_13_Tfindicador_nome = AV35TFIndicador_Nome;
         AV73WWIndicadorDS_14_Tfindicador_nome_sel = AV36TFIndicador_Nome_Sel;
         AV74WWIndicadorDS_15_Tfindicador_refer_sels = AV48TFIndicador_Refer_Sels;
         AV75WWIndicadorDS_16_Tfindicador_aplica_sels = AV52TFIndicador_Aplica_Sels;
         AV76WWIndicadorDS_17_Tfindicador_naocnfcod = AV55TFIndicador_NaoCnfCod;
         AV77WWIndicadorDS_18_Tfindicador_naocnfcod_to = AV56TFIndicador_NaoCnfCod_To;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV44OrderedBy, AV13OrderedDsc, AV14DynamicFiltersSelector1, AV15DynamicFiltersOperator1, AV16Indicador_Nome1, AV18DynamicFiltersSelector2, AV19DynamicFiltersOperator2, AV20Indicador_Nome2, AV22DynamicFiltersSelector3, AV23DynamicFiltersOperator3, AV24Indicador_Nome3, AV17DynamicFiltersEnabled2, AV21DynamicFiltersEnabled3, AV35TFIndicador_Nome, AV36TFIndicador_Nome_Sel, AV55TFIndicador_NaoCnfCod, AV56TFIndicador_NaoCnfCod_To, AV2WWPContext, AV28ManageFiltersExecutionStep, AV37ddo_Indicador_NomeTitleControlIdToReplace, AV49ddo_Indicador_ReferTitleControlIdToReplace, AV53ddo_Indicador_AplicaTitleControlIdToReplace, AV57ddo_Indicador_NaoCnfCodTitleControlIdToReplace, AV45Indicador_AreaTrabalhoCod, AV48TFIndicador_Refer_Sels, AV52TFIndicador_Aplica_Sels, AV80Pgmname, AV10GridState, AV26DynamicFiltersIgnoreFirst, AV25DynamicFiltersRemoving, A2058Indicador_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUPRV0( )
      {
         /* Before Start, stand alone formulas. */
         AV80Pgmname = "WWIndicador";
         context.Gx_err = 0;
         GXAINDICADOR_NAOCNFCOD_htmlRV2( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E27RV2 */
         E27RV2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vMANAGEFILTERSDATA"), AV32ManageFiltersData);
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV38DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vINDICADOR_NOMETITLEFILTERDATA"), AV34Indicador_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vINDICADOR_REFERTITLEFILTERDATA"), AV46Indicador_ReferTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vINDICADOR_APLICATITLEFILTERDATA"), AV50Indicador_AplicaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vINDICADOR_NAOCNFCODTITLEFILTERDATA"), AV54Indicador_NaoCnfCodTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV44OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44OrderedBy), 4, 0)));
            AV13OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavIndicador_areatrabalhocod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavIndicador_areatrabalhocod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vINDICADOR_AREATRABALHOCOD");
               GX_FocusControl = edtavIndicador_areatrabalhocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV45Indicador_AreaTrabalhoCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45Indicador_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45Indicador_AreaTrabalhoCod), 6, 0)));
            }
            else
            {
               AV45Indicador_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtavIndicador_areatrabalhocod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45Indicador_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45Indicador_AreaTrabalhoCod), 6, 0)));
            }
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV14DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14DynamicFiltersSelector1", AV14DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV15DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15DynamicFiltersOperator1), 4, 0)));
            AV16Indicador_Nome1 = StringUtil.Upper( cgiGet( edtavIndicador_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Indicador_Nome1", AV16Indicador_Nome1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV18DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV19DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19DynamicFiltersOperator2), 4, 0)));
            AV20Indicador_Nome2 = StringUtil.Upper( cgiGet( edtavIndicador_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Indicador_Nome2", AV20Indicador_Nome2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV22DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersSelector3", AV22DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV23DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator3), 4, 0)));
            AV24Indicador_Nome3 = StringUtil.Upper( cgiGet( edtavIndicador_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Indicador_Nome3", AV24Indicador_Nome3);
            AV17DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
            AV21DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersEnabled3", AV21DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vMANAGEFILTERSEXECUTIONSTEP");
               GX_FocusControl = edtavManagefiltersexecutionstep_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV28ManageFiltersExecutionStep = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV28ManageFiltersExecutionStep), 1, 0));
            }
            else
            {
               AV28ManageFiltersExecutionStep = (short)(context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV28ManageFiltersExecutionStep), 1, 0));
            }
            AV35TFIndicador_Nome = StringUtil.Upper( cgiGet( edtavTfindicador_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFIndicador_Nome", AV35TFIndicador_Nome);
            AV36TFIndicador_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfindicador_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFIndicador_Nome_Sel", AV36TFIndicador_Nome_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfindicador_naocnfcod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfindicador_naocnfcod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFINDICADOR_NAOCNFCOD");
               GX_FocusControl = edtavTfindicador_naocnfcod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV55TFIndicador_NaoCnfCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFIndicador_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFIndicador_NaoCnfCod), 6, 0)));
            }
            else
            {
               AV55TFIndicador_NaoCnfCod = (int)(context.localUtil.CToN( cgiGet( edtavTfindicador_naocnfcod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFIndicador_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFIndicador_NaoCnfCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfindicador_naocnfcod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfindicador_naocnfcod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFINDICADOR_NAOCNFCOD_TO");
               GX_FocusControl = edtavTfindicador_naocnfcod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV56TFIndicador_NaoCnfCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFIndicador_NaoCnfCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFIndicador_NaoCnfCod_To), 6, 0)));
            }
            else
            {
               AV56TFIndicador_NaoCnfCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTfindicador_naocnfcod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFIndicador_NaoCnfCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFIndicador_NaoCnfCod_To), 6, 0)));
            }
            AV37ddo_Indicador_NomeTitleControlIdToReplace = cgiGet( edtavDdo_indicador_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_Indicador_NomeTitleControlIdToReplace", AV37ddo_Indicador_NomeTitleControlIdToReplace);
            AV49ddo_Indicador_ReferTitleControlIdToReplace = cgiGet( edtavDdo_indicador_refertitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_Indicador_ReferTitleControlIdToReplace", AV49ddo_Indicador_ReferTitleControlIdToReplace);
            AV53ddo_Indicador_AplicaTitleControlIdToReplace = cgiGet( edtavDdo_indicador_aplicatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_Indicador_AplicaTitleControlIdToReplace", AV53ddo_Indicador_AplicaTitleControlIdToReplace);
            AV57ddo_Indicador_NaoCnfCodTitleControlIdToReplace = cgiGet( edtavDdo_indicador_naocnfcodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_Indicador_NaoCnfCodTitleControlIdToReplace", AV57ddo_Indicador_NaoCnfCodTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_92 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_92"), ",", "."));
            AV40GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV41GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Ddo_managefilters_Icon = cgiGet( "DDO_MANAGEFILTERS_Icon");
            Ddo_managefilters_Caption = cgiGet( "DDO_MANAGEFILTERS_Caption");
            Ddo_managefilters_Tooltip = cgiGet( "DDO_MANAGEFILTERS_Tooltip");
            Ddo_managefilters_Cls = cgiGet( "DDO_MANAGEFILTERS_Cls");
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_indicador_nome_Caption = cgiGet( "DDO_INDICADOR_NOME_Caption");
            Ddo_indicador_nome_Tooltip = cgiGet( "DDO_INDICADOR_NOME_Tooltip");
            Ddo_indicador_nome_Cls = cgiGet( "DDO_INDICADOR_NOME_Cls");
            Ddo_indicador_nome_Filteredtext_set = cgiGet( "DDO_INDICADOR_NOME_Filteredtext_set");
            Ddo_indicador_nome_Selectedvalue_set = cgiGet( "DDO_INDICADOR_NOME_Selectedvalue_set");
            Ddo_indicador_nome_Dropdownoptionstype = cgiGet( "DDO_INDICADOR_NOME_Dropdownoptionstype");
            Ddo_indicador_nome_Titlecontrolidtoreplace = cgiGet( "DDO_INDICADOR_NOME_Titlecontrolidtoreplace");
            Ddo_indicador_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_INDICADOR_NOME_Includesortasc"));
            Ddo_indicador_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_INDICADOR_NOME_Includesortdsc"));
            Ddo_indicador_nome_Sortedstatus = cgiGet( "DDO_INDICADOR_NOME_Sortedstatus");
            Ddo_indicador_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_INDICADOR_NOME_Includefilter"));
            Ddo_indicador_nome_Filtertype = cgiGet( "DDO_INDICADOR_NOME_Filtertype");
            Ddo_indicador_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_INDICADOR_NOME_Filterisrange"));
            Ddo_indicador_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_INDICADOR_NOME_Includedatalist"));
            Ddo_indicador_nome_Datalisttype = cgiGet( "DDO_INDICADOR_NOME_Datalisttype");
            Ddo_indicador_nome_Datalistproc = cgiGet( "DDO_INDICADOR_NOME_Datalistproc");
            Ddo_indicador_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_INDICADOR_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_indicador_nome_Sortasc = cgiGet( "DDO_INDICADOR_NOME_Sortasc");
            Ddo_indicador_nome_Sortdsc = cgiGet( "DDO_INDICADOR_NOME_Sortdsc");
            Ddo_indicador_nome_Loadingdata = cgiGet( "DDO_INDICADOR_NOME_Loadingdata");
            Ddo_indicador_nome_Cleanfilter = cgiGet( "DDO_INDICADOR_NOME_Cleanfilter");
            Ddo_indicador_nome_Noresultsfound = cgiGet( "DDO_INDICADOR_NOME_Noresultsfound");
            Ddo_indicador_nome_Searchbuttontext = cgiGet( "DDO_INDICADOR_NOME_Searchbuttontext");
            Ddo_indicador_refer_Caption = cgiGet( "DDO_INDICADOR_REFER_Caption");
            Ddo_indicador_refer_Tooltip = cgiGet( "DDO_INDICADOR_REFER_Tooltip");
            Ddo_indicador_refer_Cls = cgiGet( "DDO_INDICADOR_REFER_Cls");
            Ddo_indicador_refer_Selectedvalue_set = cgiGet( "DDO_INDICADOR_REFER_Selectedvalue_set");
            Ddo_indicador_refer_Dropdownoptionstype = cgiGet( "DDO_INDICADOR_REFER_Dropdownoptionstype");
            Ddo_indicador_refer_Titlecontrolidtoreplace = cgiGet( "DDO_INDICADOR_REFER_Titlecontrolidtoreplace");
            Ddo_indicador_refer_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_INDICADOR_REFER_Includesortasc"));
            Ddo_indicador_refer_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_INDICADOR_REFER_Includesortdsc"));
            Ddo_indicador_refer_Sortedstatus = cgiGet( "DDO_INDICADOR_REFER_Sortedstatus");
            Ddo_indicador_refer_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_INDICADOR_REFER_Includefilter"));
            Ddo_indicador_refer_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_INDICADOR_REFER_Includedatalist"));
            Ddo_indicador_refer_Datalisttype = cgiGet( "DDO_INDICADOR_REFER_Datalisttype");
            Ddo_indicador_refer_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_INDICADOR_REFER_Allowmultipleselection"));
            Ddo_indicador_refer_Datalistfixedvalues = cgiGet( "DDO_INDICADOR_REFER_Datalistfixedvalues");
            Ddo_indicador_refer_Sortasc = cgiGet( "DDO_INDICADOR_REFER_Sortasc");
            Ddo_indicador_refer_Sortdsc = cgiGet( "DDO_INDICADOR_REFER_Sortdsc");
            Ddo_indicador_refer_Cleanfilter = cgiGet( "DDO_INDICADOR_REFER_Cleanfilter");
            Ddo_indicador_refer_Searchbuttontext = cgiGet( "DDO_INDICADOR_REFER_Searchbuttontext");
            Ddo_indicador_aplica_Caption = cgiGet( "DDO_INDICADOR_APLICA_Caption");
            Ddo_indicador_aplica_Tooltip = cgiGet( "DDO_INDICADOR_APLICA_Tooltip");
            Ddo_indicador_aplica_Cls = cgiGet( "DDO_INDICADOR_APLICA_Cls");
            Ddo_indicador_aplica_Selectedvalue_set = cgiGet( "DDO_INDICADOR_APLICA_Selectedvalue_set");
            Ddo_indicador_aplica_Dropdownoptionstype = cgiGet( "DDO_INDICADOR_APLICA_Dropdownoptionstype");
            Ddo_indicador_aplica_Titlecontrolidtoreplace = cgiGet( "DDO_INDICADOR_APLICA_Titlecontrolidtoreplace");
            Ddo_indicador_aplica_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_INDICADOR_APLICA_Includesortasc"));
            Ddo_indicador_aplica_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_INDICADOR_APLICA_Includesortdsc"));
            Ddo_indicador_aplica_Sortedstatus = cgiGet( "DDO_INDICADOR_APLICA_Sortedstatus");
            Ddo_indicador_aplica_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_INDICADOR_APLICA_Includefilter"));
            Ddo_indicador_aplica_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_INDICADOR_APLICA_Includedatalist"));
            Ddo_indicador_aplica_Datalisttype = cgiGet( "DDO_INDICADOR_APLICA_Datalisttype");
            Ddo_indicador_aplica_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_INDICADOR_APLICA_Allowmultipleselection"));
            Ddo_indicador_aplica_Datalistfixedvalues = cgiGet( "DDO_INDICADOR_APLICA_Datalistfixedvalues");
            Ddo_indicador_aplica_Sortasc = cgiGet( "DDO_INDICADOR_APLICA_Sortasc");
            Ddo_indicador_aplica_Sortdsc = cgiGet( "DDO_INDICADOR_APLICA_Sortdsc");
            Ddo_indicador_aplica_Cleanfilter = cgiGet( "DDO_INDICADOR_APLICA_Cleanfilter");
            Ddo_indicador_aplica_Searchbuttontext = cgiGet( "DDO_INDICADOR_APLICA_Searchbuttontext");
            Ddo_indicador_naocnfcod_Caption = cgiGet( "DDO_INDICADOR_NAOCNFCOD_Caption");
            Ddo_indicador_naocnfcod_Tooltip = cgiGet( "DDO_INDICADOR_NAOCNFCOD_Tooltip");
            Ddo_indicador_naocnfcod_Cls = cgiGet( "DDO_INDICADOR_NAOCNFCOD_Cls");
            Ddo_indicador_naocnfcod_Filteredtext_set = cgiGet( "DDO_INDICADOR_NAOCNFCOD_Filteredtext_set");
            Ddo_indicador_naocnfcod_Filteredtextto_set = cgiGet( "DDO_INDICADOR_NAOCNFCOD_Filteredtextto_set");
            Ddo_indicador_naocnfcod_Dropdownoptionstype = cgiGet( "DDO_INDICADOR_NAOCNFCOD_Dropdownoptionstype");
            Ddo_indicador_naocnfcod_Titlecontrolidtoreplace = cgiGet( "DDO_INDICADOR_NAOCNFCOD_Titlecontrolidtoreplace");
            Ddo_indicador_naocnfcod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_INDICADOR_NAOCNFCOD_Includesortasc"));
            Ddo_indicador_naocnfcod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_INDICADOR_NAOCNFCOD_Includesortdsc"));
            Ddo_indicador_naocnfcod_Sortedstatus = cgiGet( "DDO_INDICADOR_NAOCNFCOD_Sortedstatus");
            Ddo_indicador_naocnfcod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_INDICADOR_NAOCNFCOD_Includefilter"));
            Ddo_indicador_naocnfcod_Filtertype = cgiGet( "DDO_INDICADOR_NAOCNFCOD_Filtertype");
            Ddo_indicador_naocnfcod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_INDICADOR_NAOCNFCOD_Filterisrange"));
            Ddo_indicador_naocnfcod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_INDICADOR_NAOCNFCOD_Includedatalist"));
            Ddo_indicador_naocnfcod_Sortasc = cgiGet( "DDO_INDICADOR_NAOCNFCOD_Sortasc");
            Ddo_indicador_naocnfcod_Sortdsc = cgiGet( "DDO_INDICADOR_NAOCNFCOD_Sortdsc");
            Ddo_indicador_naocnfcod_Cleanfilter = cgiGet( "DDO_INDICADOR_NAOCNFCOD_Cleanfilter");
            Ddo_indicador_naocnfcod_Rangefilterfrom = cgiGet( "DDO_INDICADOR_NAOCNFCOD_Rangefilterfrom");
            Ddo_indicador_naocnfcod_Rangefilterto = cgiGet( "DDO_INDICADOR_NAOCNFCOD_Rangefilterto");
            Ddo_indicador_naocnfcod_Searchbuttontext = cgiGet( "DDO_INDICADOR_NAOCNFCOD_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_indicador_nome_Activeeventkey = cgiGet( "DDO_INDICADOR_NOME_Activeeventkey");
            Ddo_indicador_nome_Filteredtext_get = cgiGet( "DDO_INDICADOR_NOME_Filteredtext_get");
            Ddo_indicador_nome_Selectedvalue_get = cgiGet( "DDO_INDICADOR_NOME_Selectedvalue_get");
            Ddo_indicador_refer_Activeeventkey = cgiGet( "DDO_INDICADOR_REFER_Activeeventkey");
            Ddo_indicador_refer_Selectedvalue_get = cgiGet( "DDO_INDICADOR_REFER_Selectedvalue_get");
            Ddo_indicador_aplica_Activeeventkey = cgiGet( "DDO_INDICADOR_APLICA_Activeeventkey");
            Ddo_indicador_aplica_Selectedvalue_get = cgiGet( "DDO_INDICADOR_APLICA_Selectedvalue_get");
            Ddo_indicador_naocnfcod_Activeeventkey = cgiGet( "DDO_INDICADOR_NAOCNFCOD_Activeeventkey");
            Ddo_indicador_naocnfcod_Filteredtext_get = cgiGet( "DDO_INDICADOR_NAOCNFCOD_Filteredtext_get");
            Ddo_indicador_naocnfcod_Filteredtextto_get = cgiGet( "DDO_INDICADOR_NAOCNFCOD_Filteredtextto_get");
            Ddo_managefilters_Activeeventkey = cgiGet( "DDO_MANAGEFILTERS_Activeeventkey");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV44OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV13OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV14DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV15DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vINDICADOR_NOME1"), AV16Indicador_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV18DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV19DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vINDICADOR_NOME2"), AV20Indicador_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV22DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV23DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vINDICADOR_NOME3"), AV24Indicador_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV17DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV21DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFINDICADOR_NOME"), AV35TFIndicador_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFINDICADOR_NOME_SEL"), AV36TFIndicador_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFINDICADOR_NAOCNFCOD"), ",", ".") != Convert.ToDecimal( AV55TFIndicador_NaoCnfCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFINDICADOR_NAOCNFCOD_TO"), ",", ".") != Convert.ToDecimal( AV56TFIndicador_NaoCnfCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E27RV2 */
         E27RV2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E27RV2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV2WWPContext) ;
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         if ( StringUtil.StrCmp(AV7HTTPRequest.Method, "GET") == 0 )
         {
            /* Execute user subroutine: 'LOADSAVEDFILTERS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            edtavManagefiltersexecutionstep_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavManagefiltersexecutionstep_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavManagefiltersexecutionstep_Visible), 5, 0)));
         }
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV14DynamicFiltersSelector1 = "INDICADOR_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14DynamicFiltersSelector1", AV14DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV18DynamicFiltersSelector2 = "INDICADOR_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersSelector3 = "INDICADOR_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersSelector3", AV22DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfindicador_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfindicador_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfindicador_nome_Visible), 5, 0)));
         edtavTfindicador_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfindicador_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfindicador_nome_sel_Visible), 5, 0)));
         edtavTfindicador_naocnfcod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfindicador_naocnfcod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfindicador_naocnfcod_Visible), 5, 0)));
         edtavTfindicador_naocnfcod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfindicador_naocnfcod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfindicador_naocnfcod_to_Visible), 5, 0)));
         Ddo_indicador_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Indicador_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_indicador_nome_Internalname, "TitleControlIdToReplace", Ddo_indicador_nome_Titlecontrolidtoreplace);
         AV37ddo_Indicador_NomeTitleControlIdToReplace = Ddo_indicador_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_Indicador_NomeTitleControlIdToReplace", AV37ddo_Indicador_NomeTitleControlIdToReplace);
         edtavDdo_indicador_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_indicador_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_indicador_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_indicador_refer_Titlecontrolidtoreplace = subGrid_Internalname+"_Indicador_Refer";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_indicador_refer_Internalname, "TitleControlIdToReplace", Ddo_indicador_refer_Titlecontrolidtoreplace);
         AV49ddo_Indicador_ReferTitleControlIdToReplace = Ddo_indicador_refer_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_Indicador_ReferTitleControlIdToReplace", AV49ddo_Indicador_ReferTitleControlIdToReplace);
         edtavDdo_indicador_refertitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_indicador_refertitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_indicador_refertitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_indicador_aplica_Titlecontrolidtoreplace = subGrid_Internalname+"_Indicador_Aplica";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_indicador_aplica_Internalname, "TitleControlIdToReplace", Ddo_indicador_aplica_Titlecontrolidtoreplace);
         AV53ddo_Indicador_AplicaTitleControlIdToReplace = Ddo_indicador_aplica_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_Indicador_AplicaTitleControlIdToReplace", AV53ddo_Indicador_AplicaTitleControlIdToReplace);
         edtavDdo_indicador_aplicatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_indicador_aplicatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_indicador_aplicatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_indicador_naocnfcod_Titlecontrolidtoreplace = subGrid_Internalname+"_Indicador_NaoCnfCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_indicador_naocnfcod_Internalname, "TitleControlIdToReplace", Ddo_indicador_naocnfcod_Titlecontrolidtoreplace);
         AV57ddo_Indicador_NaoCnfCodTitleControlIdToReplace = Ddo_indicador_naocnfcod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_Indicador_NaoCnfCodTitleControlIdToReplace", AV57ddo_Indicador_NaoCnfCodTitleControlIdToReplace);
         edtavDdo_indicador_naocnfcodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_indicador_naocnfcodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_indicador_naocnfcodtitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Indicador";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Nome", 0);
         cmbavOrderedby.addItem("2", "Refere-se a", 0);
         cmbavOrderedby.addItem("3", "Aplica-se a", 0);
         cmbavOrderedby.addItem("4", "N�o Conformidade", 0);
         if ( AV44OrderedBy < 1 )
         {
            AV44OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         Ddo_managefilters_Icon = context.convertURL( (String)(context.GetImagePath( "5efb9e2b-46db-43f4-8f74-dd3f5818d30e", "", context.GetTheme( ))));
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_managefilters_Internalname, "Icon", Ddo_managefilters_Icon);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV38DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV38DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E28RV2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV34Indicador_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46Indicador_ReferTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV50Indicador_AplicaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV54Indicador_NaoCnfCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV2WWPContext) ;
         /* Execute user subroutine: 'CHECKSECURITYFORACTIONS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV28ManageFiltersExecutionStep == 1 )
         {
            AV28ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV28ManageFiltersExecutionStep), 1, 0));
         }
         else if ( AV28ManageFiltersExecutionStep == 2 )
         {
            AV28ManageFiltersExecutionStep = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV28ManageFiltersExecutionStep), 1, 0));
            /* Execute user subroutine: 'LOADSAVEDFILTERS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtIndicador_Nome_Titleformat = 2;
         edtIndicador_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV37ddo_Indicador_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtIndicador_Nome_Internalname, "Title", edtIndicador_Nome_Title);
         cmbIndicador_Refer_Titleformat = 2;
         cmbIndicador_Refer.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Refere-se a", AV49ddo_Indicador_ReferTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbIndicador_Refer_Internalname, "Title", cmbIndicador_Refer.Title.Text);
         cmbIndicador_Aplica_Titleformat = 2;
         cmbIndicador_Aplica.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Aplica-se a", AV53ddo_Indicador_AplicaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbIndicador_Aplica_Internalname, "Title", cmbIndicador_Aplica.Title.Text);
         dltIndicador_NaoCnfCod_Titleformat = 2;
         dltIndicador_NaoCnfCod.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "N�o Conformidade", AV57ddo_Indicador_NaoCnfCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dltIndicador_NaoCnfCod_Internalname, "Title", dltIndicador_NaoCnfCod.Title.Text);
         AV40GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40GridCurrentPage), 10, 0)));
         AV41GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41GridPageCount), 10, 0)));
         imgInsert_Visible = (AV2WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Visible), 5, 0)));
         edtavUpdate_Visible = (AV2WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUpdate_Visible), 5, 0)));
         edtavDelete_Visible = (AV2WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDelete_Visible), 5, 0)));
         AV60WWIndicadorDS_1_Indicador_areatrabalhocod = AV45Indicador_AreaTrabalhoCod;
         AV61WWIndicadorDS_2_Dynamicfiltersselector1 = AV14DynamicFiltersSelector1;
         AV62WWIndicadorDS_3_Dynamicfiltersoperator1 = AV15DynamicFiltersOperator1;
         AV63WWIndicadorDS_4_Indicador_nome1 = AV16Indicador_Nome1;
         AV64WWIndicadorDS_5_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV65WWIndicadorDS_6_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV66WWIndicadorDS_7_Dynamicfiltersoperator2 = AV19DynamicFiltersOperator2;
         AV67WWIndicadorDS_8_Indicador_nome2 = AV20Indicador_Nome2;
         AV68WWIndicadorDS_9_Dynamicfiltersenabled3 = AV21DynamicFiltersEnabled3;
         AV69WWIndicadorDS_10_Dynamicfiltersselector3 = AV22DynamicFiltersSelector3;
         AV70WWIndicadorDS_11_Dynamicfiltersoperator3 = AV23DynamicFiltersOperator3;
         AV71WWIndicadorDS_12_Indicador_nome3 = AV24Indicador_Nome3;
         AV72WWIndicadorDS_13_Tfindicador_nome = AV35TFIndicador_Nome;
         AV73WWIndicadorDS_14_Tfindicador_nome_sel = AV36TFIndicador_Nome_Sel;
         AV74WWIndicadorDS_15_Tfindicador_refer_sels = AV48TFIndicador_Refer_Sels;
         AV75WWIndicadorDS_16_Tfindicador_aplica_sels = AV52TFIndicador_Aplica_Sels;
         AV76WWIndicadorDS_17_Tfindicador_naocnfcod = AV55TFIndicador_NaoCnfCod;
         AV77WWIndicadorDS_18_Tfindicador_naocnfcod_to = AV56TFIndicador_NaoCnfCod_To;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV34Indicador_NomeTitleFilterData", AV34Indicador_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV46Indicador_ReferTitleFilterData", AV46Indicador_ReferTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV50Indicador_AplicaTitleFilterData", AV50Indicador_AplicaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV54Indicador_NaoCnfCodTitleFilterData", AV54Indicador_NaoCnfCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV2WWPContext", AV2WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV32ManageFiltersData", AV32ManageFiltersData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E12RV2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV39PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV39PageToGo) ;
         }
      }

      protected void E13RV2( )
      {
         /* Ddo_indicador_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_indicador_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV44OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44OrderedBy), 4, 0)));
            AV13OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_indicador_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_indicador_nome_Internalname, "SortedStatus", Ddo_indicador_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_indicador_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV44OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44OrderedBy), 4, 0)));
            AV13OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_indicador_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_indicador_nome_Internalname, "SortedStatus", Ddo_indicador_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_indicador_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV35TFIndicador_Nome = Ddo_indicador_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFIndicador_Nome", AV35TFIndicador_Nome);
            AV36TFIndicador_Nome_Sel = Ddo_indicador_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFIndicador_Nome_Sel", AV36TFIndicador_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV44OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14RV2( )
      {
         /* Ddo_indicador_refer_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_indicador_refer_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV44OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44OrderedBy), 4, 0)));
            AV13OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_indicador_refer_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_indicador_refer_Internalname, "SortedStatus", Ddo_indicador_refer_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_indicador_refer_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV44OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44OrderedBy), 4, 0)));
            AV13OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_indicador_refer_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_indicador_refer_Internalname, "SortedStatus", Ddo_indicador_refer_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_indicador_refer_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV47TFIndicador_Refer_SelsJson = Ddo_indicador_refer_Selectedvalue_get;
            AV48TFIndicador_Refer_Sels.FromJSonString(StringUtil.StringReplace( AV47TFIndicador_Refer_SelsJson, "\"", ""));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV44OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV48TFIndicador_Refer_Sels", AV48TFIndicador_Refer_Sels);
      }

      protected void E15RV2( )
      {
         /* Ddo_indicador_aplica_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_indicador_aplica_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV44OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44OrderedBy), 4, 0)));
            AV13OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_indicador_aplica_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_indicador_aplica_Internalname, "SortedStatus", Ddo_indicador_aplica_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_indicador_aplica_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV44OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44OrderedBy), 4, 0)));
            AV13OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_indicador_aplica_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_indicador_aplica_Internalname, "SortedStatus", Ddo_indicador_aplica_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_indicador_aplica_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV51TFIndicador_Aplica_SelsJson = Ddo_indicador_aplica_Selectedvalue_get;
            AV52TFIndicador_Aplica_Sels.FromJSonString(StringUtil.StringReplace( AV51TFIndicador_Aplica_SelsJson, "\"", ""));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV44OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV52TFIndicador_Aplica_Sels", AV52TFIndicador_Aplica_Sels);
      }

      protected void E16RV2( )
      {
         /* Ddo_indicador_naocnfcod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_indicador_naocnfcod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV44OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44OrderedBy), 4, 0)));
            AV13OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_indicador_naocnfcod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_indicador_naocnfcod_Internalname, "SortedStatus", Ddo_indicador_naocnfcod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_indicador_naocnfcod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV44OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44OrderedBy), 4, 0)));
            AV13OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_indicador_naocnfcod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_indicador_naocnfcod_Internalname, "SortedStatus", Ddo_indicador_naocnfcod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_indicador_naocnfcod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV55TFIndicador_NaoCnfCod = (int)(NumberUtil.Val( Ddo_indicador_naocnfcod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFIndicador_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFIndicador_NaoCnfCod), 6, 0)));
            AV56TFIndicador_NaoCnfCod_To = (int)(NumberUtil.Val( Ddo_indicador_naocnfcod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFIndicador_NaoCnfCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFIndicador_NaoCnfCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV44OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E29RV2( )
      {
         /* Grid_Load Routine */
         edtavUpdate_Tooltiptext = "Modifica";
         if ( AV2WWPContext.gxTpr_Update )
         {
            edtavUpdate_Link = formatLink("indicador.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A2058Indicador_Codigo);
            AV42Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV42Update);
            AV78Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            edtavUpdate_Enabled = 1;
         }
         else
         {
            edtavUpdate_Link = "";
            AV42Update = context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV42Update);
            AV78Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( )));
            edtavUpdate_Enabled = 0;
         }
         edtavDelete_Tooltiptext = "Eliminar";
         if ( AV2WWPContext.gxTpr_Delete )
         {
            edtavDelete_Link = formatLink("indicador.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A2058Indicador_Codigo);
            AV43Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV43Delete);
            AV79Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavDelete_Enabled = 1;
         }
         else
         {
            edtavDelete_Link = "";
            AV43Delete = context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV43Delete);
            AV79Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( )));
            edtavDelete_Enabled = 0;
         }
         edtIndicador_Nome_Link = formatLink("viewindicador.aspx") + "?" + UrlEncode("" +A2058Indicador_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 92;
         }
         sendrow_922( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_92_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(92, GridRow);
         }
      }

      protected void E17RV2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E22RV2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV17DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E18RV2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV25DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersRemoving", AV25DynamicFiltersRemoving);
         AV26DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersIgnoreFirst", AV26DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV25DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersRemoving", AV25DynamicFiltersRemoving);
         AV26DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersIgnoreFirst", AV26DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV44OrderedBy, AV13OrderedDsc, AV14DynamicFiltersSelector1, AV15DynamicFiltersOperator1, AV16Indicador_Nome1, AV18DynamicFiltersSelector2, AV19DynamicFiltersOperator2, AV20Indicador_Nome2, AV22DynamicFiltersSelector3, AV23DynamicFiltersOperator3, AV24Indicador_Nome3, AV17DynamicFiltersEnabled2, AV21DynamicFiltersEnabled3, AV35TFIndicador_Nome, AV36TFIndicador_Nome_Sel, AV55TFIndicador_NaoCnfCod, AV56TFIndicador_NaoCnfCod_To, AV2WWPContext, AV28ManageFiltersExecutionStep, AV37ddo_Indicador_NomeTitleControlIdToReplace, AV49ddo_Indicador_ReferTitleControlIdToReplace, AV53ddo_Indicador_AplicaTitleControlIdToReplace, AV57ddo_Indicador_NaoCnfCodTitleControlIdToReplace, AV45Indicador_AreaTrabalhoCod, AV48TFIndicador_Refer_Sels, AV52TFIndicador_Aplica_Sels, AV80Pgmname, AV10GridState, AV26DynamicFiltersIgnoreFirst, AV25DynamicFiltersRemoving, A2058Indicador_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV19DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV22DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV14DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E23RV2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E24RV2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV21DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersEnabled3", AV21DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E19RV2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV25DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersRemoving", AV25DynamicFiltersRemoving);
         AV17DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV25DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersRemoving", AV25DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV44OrderedBy, AV13OrderedDsc, AV14DynamicFiltersSelector1, AV15DynamicFiltersOperator1, AV16Indicador_Nome1, AV18DynamicFiltersSelector2, AV19DynamicFiltersOperator2, AV20Indicador_Nome2, AV22DynamicFiltersSelector3, AV23DynamicFiltersOperator3, AV24Indicador_Nome3, AV17DynamicFiltersEnabled2, AV21DynamicFiltersEnabled3, AV35TFIndicador_Nome, AV36TFIndicador_Nome_Sel, AV55TFIndicador_NaoCnfCod, AV56TFIndicador_NaoCnfCod_To, AV2WWPContext, AV28ManageFiltersExecutionStep, AV37ddo_Indicador_NomeTitleControlIdToReplace, AV49ddo_Indicador_ReferTitleControlIdToReplace, AV53ddo_Indicador_AplicaTitleControlIdToReplace, AV57ddo_Indicador_NaoCnfCodTitleControlIdToReplace, AV45Indicador_AreaTrabalhoCod, AV48TFIndicador_Refer_Sels, AV52TFIndicador_Aplica_Sels, AV80Pgmname, AV10GridState, AV26DynamicFiltersIgnoreFirst, AV25DynamicFiltersRemoving, A2058Indicador_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV19DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV22DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV14DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E25RV2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E20RV2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV25DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersRemoving", AV25DynamicFiltersRemoving);
         AV21DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersEnabled3", AV21DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV25DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersRemoving", AV25DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV44OrderedBy, AV13OrderedDsc, AV14DynamicFiltersSelector1, AV15DynamicFiltersOperator1, AV16Indicador_Nome1, AV18DynamicFiltersSelector2, AV19DynamicFiltersOperator2, AV20Indicador_Nome2, AV22DynamicFiltersSelector3, AV23DynamicFiltersOperator3, AV24Indicador_Nome3, AV17DynamicFiltersEnabled2, AV21DynamicFiltersEnabled3, AV35TFIndicador_Nome, AV36TFIndicador_Nome_Sel, AV55TFIndicador_NaoCnfCod, AV56TFIndicador_NaoCnfCod_To, AV2WWPContext, AV28ManageFiltersExecutionStep, AV37ddo_Indicador_NomeTitleControlIdToReplace, AV49ddo_Indicador_ReferTitleControlIdToReplace, AV53ddo_Indicador_AplicaTitleControlIdToReplace, AV57ddo_Indicador_NaoCnfCodTitleControlIdToReplace, AV45Indicador_AreaTrabalhoCod, AV48TFIndicador_Refer_Sels, AV52TFIndicador_Aplica_Sels, AV80Pgmname, AV10GridState, AV26DynamicFiltersIgnoreFirst, AV25DynamicFiltersRemoving, A2058Indicador_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV19DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV22DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV14DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E26RV2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11RV2( )
      {
         /* Ddo_managefilters_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Clean#>") == 0 )
         {
            /* Execute user subroutine: 'CLEANFILTERS' */
            S242 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Save#>") == 0 )
         {
            /* Execute user subroutine: 'SAVEGRIDSTATE' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            context.PopUp(formatLink("wwpbaseobjects.savefilteras.aspx") + "?" + UrlEncode(StringUtil.RTrim("WWIndicadorFilters")) + "," + UrlEncode(StringUtil.RTrim(AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3"))), new Object[] {});
            AV28ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV28ManageFiltersExecutionStep), 1, 0));
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Manage#>") == 0 )
         {
            context.PopUp(formatLink("wwpbaseobjects.managefilters.aspx") + "?" + UrlEncode(StringUtil.RTrim("WWIndicadorFilters")), new Object[] {});
            AV28ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV28ManageFiltersExecutionStep), 1, 0));
            context.DoAjaxRefresh();
         }
         else
         {
            GXt_char2 = AV29ManageFiltersXml;
            new wwpbaseobjects.getfilterbyname(context ).execute(  "WWIndicadorFilters",  Ddo_managefilters_Activeeventkey, out  GXt_char2) ;
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_managefilters_Internalname, "ActiveEventKey", Ddo_managefilters_Activeeventkey);
            AV29ManageFiltersXml = GXt_char2;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV29ManageFiltersXml)) )
            {
               GX_msglist.addItem("O filtro selecionado n�o existe mais.");
            }
            else
            {
               /* Execute user subroutine: 'CLEANFILTERS' */
               S242 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               new wwpbaseobjects.savegridstate(context ).execute(  AV80Pgmname+"GridState",  AV29ManageFiltersXml) ;
               AV10GridState.FromXml(AV29ManageFiltersXml, "");
               AV44OrderedBy = AV10GridState.gxTpr_Orderedby;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44OrderedBy), 4, 0)));
               AV13OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
               /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
               S172 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
               S252 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
               S232 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               subgrid_firstpage( ) ;
               context.DoAjaxRefresh();
            }
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV44OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV48TFIndicador_Refer_Sels", AV48TFIndicador_Refer_Sels);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV52TFIndicador_Aplica_Sels", AV52TFIndicador_Aplica_Sels);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV14DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV19DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV22DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E21RV2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("indicador.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S202( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_indicador_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_indicador_nome_Internalname, "SortedStatus", Ddo_indicador_nome_Sortedstatus);
         Ddo_indicador_refer_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_indicador_refer_Internalname, "SortedStatus", Ddo_indicador_refer_Sortedstatus);
         Ddo_indicador_aplica_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_indicador_aplica_Internalname, "SortedStatus", Ddo_indicador_aplica_Sortedstatus);
         Ddo_indicador_naocnfcod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_indicador_naocnfcod_Internalname, "SortedStatus", Ddo_indicador_naocnfcod_Sortedstatus);
      }

      protected void S172( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV44OrderedBy == 1 )
         {
            Ddo_indicador_nome_Sortedstatus = (AV13OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_indicador_nome_Internalname, "SortedStatus", Ddo_indicador_nome_Sortedstatus);
         }
         else if ( AV44OrderedBy == 2 )
         {
            Ddo_indicador_refer_Sortedstatus = (AV13OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_indicador_refer_Internalname, "SortedStatus", Ddo_indicador_refer_Sortedstatus);
         }
         else if ( AV44OrderedBy == 3 )
         {
            Ddo_indicador_aplica_Sortedstatus = (AV13OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_indicador_aplica_Internalname, "SortedStatus", Ddo_indicador_aplica_Sortedstatus);
         }
         else if ( AV44OrderedBy == 4 )
         {
            Ddo_indicador_naocnfcod_Sortedstatus = (AV13OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_indicador_naocnfcod_Internalname, "SortedStatus", Ddo_indicador_naocnfcod_Sortedstatus);
         }
      }

      protected void S182( )
      {
         /* 'CHECKSECURITYFORACTIONS' Routine */
         if ( ! ( AV2WWPContext.gxTpr_Insert ) )
         {
            imgInsert_Link = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Link", imgInsert_Link);
            imgInsert_Bitmap = context.GetImagePath( "13d28f37-c579-4dd9-a404-ba51ab71d1e3", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Bitmap", context.convertURL( context.PathToRelativeUrl( imgInsert_Bitmap)));
            imgInsert_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Enabled), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavIndicador_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavIndicador_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavIndicador_nome1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "INDICADOR_NOME") == 0 )
         {
            edtavIndicador_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavIndicador_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavIndicador_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavIndicador_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavIndicador_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavIndicador_nome2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "INDICADOR_NOME") == 0 )
         {
            edtavIndicador_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavIndicador_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavIndicador_nome2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S142( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavIndicador_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavIndicador_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavIndicador_nome3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV22DynamicFiltersSelector3, "INDICADOR_NOME") == 0 )
         {
            edtavIndicador_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavIndicador_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavIndicador_nome3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S222( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV17DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
         AV18DynamicFiltersSelector2 = "INDICADOR_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
         AV19DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19DynamicFiltersOperator2), 4, 0)));
         AV20Indicador_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Indicador_Nome2", AV20Indicador_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV21DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersEnabled3", AV21DynamicFiltersEnabled3);
         AV22DynamicFiltersSelector3 = "INDICADOR_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersSelector3", AV22DynamicFiltersSelector3);
         AV23DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator3), 4, 0)));
         AV24Indicador_Nome3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Indicador_Nome3", AV24Indicador_Nome3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S112( )
      {
         /* 'LOADSAVEDFILTERS' Routine */
         AV32ManageFiltersData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV33ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV33ManageFiltersDataItem.gxTpr_Title = "Limpar filtros";
         AV33ManageFiltersDataItem.gxTpr_Eventkey = "<#Clean#>";
         AV33ManageFiltersDataItem.gxTpr_Isdivider = false;
         AV33ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "63d2ae92-4e43-4a70-af61-0943e39ea422", "", context.GetTheme( ))));
         AV33ManageFiltersDataItem.gxTpr_Jsonclickevent = "WWPDynFilterHideAll(3)";
         AV32ManageFiltersData.Add(AV33ManageFiltersDataItem, 0);
         AV33ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV33ManageFiltersDataItem.gxTpr_Title = "Salvar filtro como...";
         AV33ManageFiltersDataItem.gxTpr_Eventkey = "<#Save#>";
         AV33ManageFiltersDataItem.gxTpr_Isdivider = false;
         AV33ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "6eee63e8-73c7-4738-beee-f98e3a8d2841", "", context.GetTheme( ))));
         AV32ManageFiltersData.Add(AV33ManageFiltersDataItem, 0);
         AV33ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV33ManageFiltersDataItem.gxTpr_Isdivider = true;
         AV32ManageFiltersData.Add(AV33ManageFiltersDataItem, 0);
         AV30ManageFiltersItems.FromXml(new wwpbaseobjects.loadmanagefiltersstate(context).executeUdp(  "WWIndicadorFilters"), "");
         AV81GXV1 = 1;
         while ( AV81GXV1 <= AV30ManageFiltersItems.Count )
         {
            AV31ManageFiltersItem = ((wwpbaseobjects.SdtGridStateCollection_Item)AV30ManageFiltersItems.Item(AV81GXV1));
            AV33ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV33ManageFiltersDataItem.gxTpr_Title = AV31ManageFiltersItem.gxTpr_Title;
            AV33ManageFiltersDataItem.gxTpr_Eventkey = AV31ManageFiltersItem.gxTpr_Title;
            AV33ManageFiltersDataItem.gxTpr_Isdivider = false;
            AV33ManageFiltersDataItem.gxTpr_Jsonclickevent = "WWPDynFilterHideAll(3)";
            AV32ManageFiltersData.Add(AV33ManageFiltersDataItem, 0);
            if ( AV32ManageFiltersData.Count == 13 )
            {
               if (true) break;
            }
            AV81GXV1 = (int)(AV81GXV1+1);
         }
         if ( AV32ManageFiltersData.Count > 3 )
         {
            AV33ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV33ManageFiltersDataItem.gxTpr_Isdivider = true;
            AV32ManageFiltersData.Add(AV33ManageFiltersDataItem, 0);
            AV33ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV33ManageFiltersDataItem.gxTpr_Title = "Gerenciar filtros";
            AV33ManageFiltersDataItem.gxTpr_Eventkey = "<#Manage#>";
            AV33ManageFiltersDataItem.gxTpr_Isdivider = false;
            AV33ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "653f6166-5d82-407a-af84-19e0dde65efd", "", context.GetTheme( ))));
            AV33ManageFiltersDataItem.gxTpr_Jsonclickevent = "";
            AV32ManageFiltersData.Add(AV33ManageFiltersDataItem, 0);
         }
      }

      protected void S242( )
      {
         /* 'CLEANFILTERS' Routine */
         AV45Indicador_AreaTrabalhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45Indicador_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45Indicador_AreaTrabalhoCod), 6, 0)));
         AV35TFIndicador_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFIndicador_Nome", AV35TFIndicador_Nome);
         Ddo_indicador_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_indicador_nome_Internalname, "FilteredText_set", Ddo_indicador_nome_Filteredtext_set);
         AV36TFIndicador_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFIndicador_Nome_Sel", AV36TFIndicador_Nome_Sel);
         Ddo_indicador_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_indicador_nome_Internalname, "SelectedValue_set", Ddo_indicador_nome_Selectedvalue_set);
         AV48TFIndicador_Refer_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_indicador_refer_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_indicador_refer_Internalname, "SelectedValue_set", Ddo_indicador_refer_Selectedvalue_set);
         AV52TFIndicador_Aplica_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_indicador_aplica_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_indicador_aplica_Internalname, "SelectedValue_set", Ddo_indicador_aplica_Selectedvalue_set);
         AV55TFIndicador_NaoCnfCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFIndicador_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFIndicador_NaoCnfCod), 6, 0)));
         Ddo_indicador_naocnfcod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_indicador_naocnfcod_Internalname, "FilteredText_set", Ddo_indicador_naocnfcod_Filteredtext_set);
         AV56TFIndicador_NaoCnfCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFIndicador_NaoCnfCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFIndicador_NaoCnfCod_To), 6, 0)));
         Ddo_indicador_naocnfcod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_indicador_naocnfcod_Internalname, "FilteredTextTo_set", Ddo_indicador_naocnfcod_Filteredtextto_set);
         AV14DynamicFiltersSelector1 = "INDICADOR_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14DynamicFiltersSelector1", AV14DynamicFiltersSelector1);
         AV15DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15DynamicFiltersOperator1), 4, 0)));
         AV16Indicador_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Indicador_Nome1", AV16Indicador_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S162( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV27Session.Get(AV80Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV80Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV27Session.Get(AV80Pgmname+"GridState"), "");
         }
         AV44OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44OrderedBy), 4, 0)));
         AV13OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S252 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S252( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV82GXV2 = 1;
         while ( AV82GXV2 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV82GXV2));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "INDICADOR_AREATRABALHOCOD") == 0 )
            {
               AV45Indicador_AreaTrabalhoCod = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45Indicador_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45Indicador_AreaTrabalhoCod), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFINDICADOR_NOME") == 0 )
            {
               AV35TFIndicador_Nome = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFIndicador_Nome", AV35TFIndicador_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TFIndicador_Nome)) )
               {
                  Ddo_indicador_nome_Filteredtext_set = AV35TFIndicador_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_indicador_nome_Internalname, "FilteredText_set", Ddo_indicador_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFINDICADOR_NOME_SEL") == 0 )
            {
               AV36TFIndicador_Nome_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFIndicador_Nome_Sel", AV36TFIndicador_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TFIndicador_Nome_Sel)) )
               {
                  Ddo_indicador_nome_Selectedvalue_set = AV36TFIndicador_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_indicador_nome_Internalname, "SelectedValue_set", Ddo_indicador_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFINDICADOR_REFER_SEL") == 0 )
            {
               AV47TFIndicador_Refer_SelsJson = AV11GridStateFilterValue.gxTpr_Value;
               AV48TFIndicador_Refer_Sels.FromJSonString(AV47TFIndicador_Refer_SelsJson);
               if ( ! ( AV48TFIndicador_Refer_Sels.Count == 0 ) )
               {
                  Ddo_indicador_refer_Selectedvalue_set = AV47TFIndicador_Refer_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_indicador_refer_Internalname, "SelectedValue_set", Ddo_indicador_refer_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFINDICADOR_APLICA_SEL") == 0 )
            {
               AV51TFIndicador_Aplica_SelsJson = AV11GridStateFilterValue.gxTpr_Value;
               AV52TFIndicador_Aplica_Sels.FromJSonString(AV51TFIndicador_Aplica_SelsJson);
               if ( ! ( AV52TFIndicador_Aplica_Sels.Count == 0 ) )
               {
                  Ddo_indicador_aplica_Selectedvalue_set = AV51TFIndicador_Aplica_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_indicador_aplica_Internalname, "SelectedValue_set", Ddo_indicador_aplica_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFINDICADOR_NAOCNFCOD") == 0 )
            {
               AV55TFIndicador_NaoCnfCod = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFIndicador_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFIndicador_NaoCnfCod), 6, 0)));
               AV56TFIndicador_NaoCnfCod_To = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFIndicador_NaoCnfCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFIndicador_NaoCnfCod_To), 6, 0)));
               if ( ! (0==AV55TFIndicador_NaoCnfCod) )
               {
                  Ddo_indicador_naocnfcod_Filteredtext_set = StringUtil.Str( (decimal)(AV55TFIndicador_NaoCnfCod), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_indicador_naocnfcod_Internalname, "FilteredText_set", Ddo_indicador_naocnfcod_Filteredtext_set);
               }
               if ( ! (0==AV56TFIndicador_NaoCnfCod_To) )
               {
                  Ddo_indicador_naocnfcod_Filteredtextto_set = StringUtil.Str( (decimal)(AV56TFIndicador_NaoCnfCod_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_indicador_naocnfcod_Internalname, "FilteredTextTo_set", Ddo_indicador_naocnfcod_Filteredtextto_set);
               }
            }
            AV82GXV2 = (int)(AV82GXV2+1);
         }
      }

      protected void S232( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV14DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14DynamicFiltersSelector1", AV14DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "INDICADOR_NOME") == 0 )
            {
               AV15DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15DynamicFiltersOperator1), 4, 0)));
               AV16Indicador_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Indicador_Nome1", AV16Indicador_Nome1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV17DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV18DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "INDICADOR_NOME") == 0 )
               {
                  AV19DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19DynamicFiltersOperator2), 4, 0)));
                  AV20Indicador_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Indicador_Nome2", AV20Indicador_Nome2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S132 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV21DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersEnabled3", AV21DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV22DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersSelector3", AV22DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV22DynamicFiltersSelector3, "INDICADOR_NOME") == 0 )
                  {
                     AV23DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator3), 4, 0)));
                     AV24Indicador_Nome3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Indicador_Nome3", AV24Indicador_Nome3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S142 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV25DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S192( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV27Session.Get(AV80Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV44OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV13OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! (0==AV45Indicador_AreaTrabalhoCod) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "INDICADOR_AREATRABALHOCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV45Indicador_AreaTrabalhoCod), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TFIndicador_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFINDICADOR_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV35TFIndicador_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TFIndicador_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFINDICADOR_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV36TFIndicador_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV48TFIndicador_Refer_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFINDICADOR_REFER_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV48TFIndicador_Refer_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV52TFIndicador_Aplica_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFINDICADOR_APLICA_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV52TFIndicador_Aplica_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV55TFIndicador_NaoCnfCod) && (0==AV56TFIndicador_NaoCnfCod_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFINDICADOR_NAOCNFCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV55TFIndicador_NaoCnfCod), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV56TFIndicador_NaoCnfCod_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV80Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S212( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV26DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV14DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "INDICADOR_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV16Indicador_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV16Indicador_Nome1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV15DynamicFiltersOperator1;
            }
            if ( AV25DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV17DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV18DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "INDICADOR_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV20Indicador_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV20Indicador_Nome2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV19DynamicFiltersOperator2;
            }
            if ( AV25DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV21DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV22DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV22DynamicFiltersSelector3, "INDICADOR_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Indicador_Nome3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV24Indicador_Nome3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV23DynamicFiltersOperator3;
            }
            if ( AV25DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S152( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV80Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Indicador";
         AV27Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_RV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_RV2( true) ;
         }
         else
         {
            wb_table2_8_RV2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_RV2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_86_RV2( true) ;
         }
         else
         {
            wb_table3_86_RV2( false) ;
         }
         return  ;
      }

      protected void wb_table3_86_RV2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_RV2e( true) ;
         }
         else
         {
            wb_table1_2_RV2e( false) ;
         }
      }

      protected void wb_table3_86_RV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_89_RV2( true) ;
         }
         else
         {
            wb_table4_89_RV2( false) ;
         }
         return  ;
      }

      protected void wb_table4_89_RV2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_86_RV2e( true) ;
         }
         else
         {
            wb_table3_86_RV2e( false) ;
         }
      }

      protected void wb_table4_89_RV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"92\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Id") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtIndicador_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtIndicador_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtIndicador_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbIndicador_Refer_Titleformat == 0 )
               {
                  context.SendWebValue( cmbIndicador_Refer.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbIndicador_Refer.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbIndicador_Aplica_Titleformat == 0 )
               {
                  context.SendWebValue( cmbIndicador_Aplica.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbIndicador_Aplica.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( dltIndicador_NaoCnfCod_Titleformat == 0 )
               {
                  context.SendWebValue( dltIndicador_NaoCnfCod.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( dltIndicador_NaoCnfCod.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV42Update));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV43Delete));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2058Indicador_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A2059Indicador_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtIndicador_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtIndicador_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtIndicador_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2063Indicador_Refer), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbIndicador_Refer.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbIndicador_Refer_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2064Indicador_Aplica), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbIndicador_Aplica.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbIndicador_Aplica_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2062Indicador_NaoCnfCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( dltIndicador_NaoCnfCod.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(dltIndicador_NaoCnfCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 92 )
         {
            wbEnd = 0;
            nRC_GXsfl_92 = (short)(nGXsfl_92_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_89_RV2e( true) ;
         }
         else
         {
            wb_table4_89_RV2e( false) ;
         }
      }

      protected void wb_table2_8_RV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table5_11_RV2( true) ;
         }
         else
         {
            wb_table5_11_RV2( false) ;
         }
         return  ;
      }

      protected void wb_table5_11_RV2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_23_RV2( true) ;
         }
         else
         {
            wb_table6_23_RV2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_RV2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_RV2e( true) ;
         }
         else
         {
            wb_table2_8_RV2e( false) ;
         }
      }

      protected void wb_table6_23_RV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "TableFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_MANAGEFILTERSContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextindicador_areatrabalhocod_Internalname, "�rea de Trabalho", "", "", lblFiltertextindicador_areatrabalhocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavIndicador_areatrabalhocod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV45Indicador_AreaTrabalhoCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV45Indicador_AreaTrabalhoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,30);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavIndicador_areatrabalhocod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_32_RV2( true) ;
         }
         else
         {
            wb_table7_32_RV2( false) ;
         }
         return  ;
      }

      protected void wb_table7_32_RV2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_RV2e( true) ;
         }
         else
         {
            wb_table6_23_RV2e( false) ;
         }
      }

      protected void wb_table7_32_RV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_92_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV14DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,37);\"", "", true, "HLP_WWIndicador.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV14DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_41_RV2( true) ;
         }
         else
         {
            wb_table8_41_RV2( false) ;
         }
         return  ;
      }

      protected void wb_table8_41_RV2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWIndicador.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'" + sGXsfl_92_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV18DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", "", true, "HLP_WWIndicador.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_58_RV2( true) ;
         }
         else
         {
            wb_table9_58_RV2( false) ;
         }
         return  ;
      }

      protected void wb_table9_58_RV2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWIndicador.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'" + sGXsfl_92_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV22DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,71);\"", "", true, "HLP_WWIndicador.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV22DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_75_RV2( true) ;
         }
         else
         {
            wb_table10_75_RV2( false) ;
         }
         return  ;
      }

      protected void wb_table10_75_RV2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_32_RV2e( true) ;
         }
         else
         {
            wb_table7_32_RV2e( false) ;
         }
      }

      protected void wb_table10_75_RV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'" + sGXsfl_92_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,78);\"", "", true, "HLP_WWIndicador.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavIndicador_nome3_Internalname, StringUtil.RTrim( AV24Indicador_Nome3), StringUtil.RTrim( context.localUtil.Format( AV24Indicador_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,80);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavIndicador_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavIndicador_nome3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_75_RV2e( true) ;
         }
         else
         {
            wb_table10_75_RV2e( false) ;
         }
      }

      protected void wb_table9_58_RV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'" + sGXsfl_92_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV19DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,61);\"", "", true, "HLP_WWIndicador.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV19DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavIndicador_nome2_Internalname, StringUtil.RTrim( AV20Indicador_Nome2), StringUtil.RTrim( context.localUtil.Format( AV20Indicador_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,63);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavIndicador_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavIndicador_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_58_RV2e( true) ;
         }
         else
         {
            wb_table9_58_RV2e( false) ;
         }
      }

      protected void wb_table8_41_RV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'" + sGXsfl_92_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV15DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,44);\"", "", true, "HLP_WWIndicador.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavIndicador_nome1_Internalname, StringUtil.RTrim( AV16Indicador_Nome1), StringUtil.RTrim( context.localUtil.Format( AV16Indicador_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,46);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavIndicador_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavIndicador_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_41_RV2e( true) ;
         }
         else
         {
            wb_table8_41_RV2e( false) ;
         }
      }

      protected void wb_table5_11_RV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblIndicadortitle_Internalname, "Indicadores", "", "", lblIndicadortitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, imgInsert_Bitmap, "", "", "", context.GetTheme( ), imgInsert_Visible, imgInsert_Enabled, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_92_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV44OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWIndicador.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV44OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV13OrderedDsc), StringUtil.BoolToStr( AV13OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_11_RV2e( true) ;
         }
         else
         {
            wb_table5_11_RV2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PARV2( ) ;
         WSRV2( ) ;
         WERV2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205181345277");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwindicador.js", "?20205181345277");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_922( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_92_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_92_idx;
         edtIndicador_Codigo_Internalname = "INDICADOR_CODIGO_"+sGXsfl_92_idx;
         edtIndicador_Nome_Internalname = "INDICADOR_NOME_"+sGXsfl_92_idx;
         cmbIndicador_Refer_Internalname = "INDICADOR_REFER_"+sGXsfl_92_idx;
         cmbIndicador_Aplica_Internalname = "INDICADOR_APLICA_"+sGXsfl_92_idx;
         dltIndicador_NaoCnfCod_Internalname = "INDICADOR_NAOCNFCOD_"+sGXsfl_92_idx;
      }

      protected void SubsflControlProps_fel_922( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_92_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_92_fel_idx;
         edtIndicador_Codigo_Internalname = "INDICADOR_CODIGO_"+sGXsfl_92_fel_idx;
         edtIndicador_Nome_Internalname = "INDICADOR_NOME_"+sGXsfl_92_fel_idx;
         cmbIndicador_Refer_Internalname = "INDICADOR_REFER_"+sGXsfl_92_fel_idx;
         cmbIndicador_Aplica_Internalname = "INDICADOR_APLICA_"+sGXsfl_92_fel_idx;
         dltIndicador_NaoCnfCod_Internalname = "INDICADOR_NAOCNFCOD_"+sGXsfl_92_fel_idx;
      }

      protected void sendrow_922( )
      {
         SubsflControlProps_922( ) ;
         WBRV0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_92_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_92_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_92_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV42Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV42Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV78Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV42Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV42Update)) ? AV78Update_GXI : context.PathToRelativeUrl( AV42Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavUpdate_Visible,(int)edtavUpdate_Enabled,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV42Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV43Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV43Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV79Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV43Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV43Delete)) ? AV79Delete_GXI : context.PathToRelativeUrl( AV43Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDelete_Visible,(int)edtavDelete_Enabled,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV43Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtIndicador_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A2058Indicador_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A2058Indicador_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtIndicador_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)92,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtIndicador_Nome_Internalname,StringUtil.RTrim( A2059Indicador_Nome),StringUtil.RTrim( context.localUtil.Format( A2059Indicador_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtIndicador_Nome_Link,(String)"",(String)"",(String)"",(String)edtIndicador_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)92,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_92_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "INDICADOR_REFER_" + sGXsfl_92_idx;
               cmbIndicador_Refer.Name = GXCCtl;
               cmbIndicador_Refer.WebTags = "";
               cmbIndicador_Refer.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "(Nenhum)", 0);
               cmbIndicador_Refer.addItem("1", "Prazo", 0);
               cmbIndicador_Refer.addItem("2", "Frequ�ncia", 0);
               cmbIndicador_Refer.addItem("3", "Valores", 0);
               if ( cmbIndicador_Refer.ItemCount > 0 )
               {
                  A2063Indicador_Refer = (short)(NumberUtil.Val( cmbIndicador_Refer.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2063Indicador_Refer), 4, 0))), "."));
                  n2063Indicador_Refer = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbIndicador_Refer,(String)cmbIndicador_Refer_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A2063Indicador_Refer), 4, 0)),(short)1,(String)cmbIndicador_Refer_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbIndicador_Refer.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A2063Indicador_Refer), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbIndicador_Refer_Internalname, "Values", (String)(cmbIndicador_Refer.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_92_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "INDICADOR_APLICA_" + sGXsfl_92_idx;
               cmbIndicador_Aplica.Name = GXCCtl;
               cmbIndicador_Aplica.WebTags = "";
               cmbIndicador_Aplica.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "(Nenhum)", 0);
               cmbIndicador_Aplica.addItem("1", "Demandas", 0);
               cmbIndicador_Aplica.addItem("2", "Lotes", 0);
               if ( cmbIndicador_Aplica.ItemCount > 0 )
               {
                  A2064Indicador_Aplica = (short)(NumberUtil.Val( cmbIndicador_Aplica.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2064Indicador_Aplica), 4, 0))), "."));
                  n2064Indicador_Aplica = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbIndicador_Aplica,(String)cmbIndicador_Aplica_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A2064Indicador_Aplica), 4, 0)),(short)1,(String)cmbIndicador_Aplica_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbIndicador_Aplica.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A2064Indicador_Aplica), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbIndicador_Aplica_Internalname, "Values", (String)(cmbIndicador_Aplica.ToJavascriptSource()));
            GXAINDICADOR_NAOCNFCOD_htmlRV2( ) ;
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_92_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "INDICADOR_NAOCNFCOD_" + sGXsfl_92_idx;
               dltIndicador_NaoCnfCod.Name = GXCCtl;
               dltIndicador_NaoCnfCod.WebTags = "";
            }
            /* ListBox */
            GridRow.AddColumnProperties("listbox", 2, isAjaxCallMode( ), new Object[] {(GXListbox)dltIndicador_NaoCnfCod,(String)dltIndicador_NaoCnfCod_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A2062Indicador_NaoCnfCod), 6, 0)),(short)2,(String)dltIndicador_NaoCnfCod_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)17,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            dltIndicador_NaoCnfCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A2062Indicador_NaoCnfCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dltIndicador_NaoCnfCod_Internalname, "Values", (String)(dltIndicador_NaoCnfCod.ToJavascriptSource()));
            GxWebStd.gx_hidden_field( context, "gxhash_INDICADOR_CODIGO"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sGXsfl_92_idx, context.localUtil.Format( (decimal)(A2058Indicador_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_INDICADOR_NOME"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sGXsfl_92_idx, StringUtil.RTrim( context.localUtil.Format( A2059Indicador_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_INDICADOR_REFER"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sGXsfl_92_idx, context.localUtil.Format( (decimal)(A2063Indicador_Refer), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_INDICADOR_APLICA"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sGXsfl_92_idx, context.localUtil.Format( (decimal)(A2064Indicador_Aplica), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_INDICADOR_NAOCNFCOD"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sGXsfl_92_idx, context.localUtil.Format( (decimal)(A2062Indicador_NaoCnfCod), "ZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_92_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_92_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_92_idx+1));
            sGXsfl_92_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_92_idx), 4, 0)), 4, "0");
            SubsflControlProps_922( ) ;
         }
         /* End function sendrow_922 */
      }

      protected void init_default_properties( )
      {
         lblIndicadortitle_Internalname = "INDICADORTITLE";
         imgInsert_Internalname = "INSERT";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         tblTableactions_Internalname = "TABLEACTIONS";
         Ddo_managefilters_Internalname = "DDO_MANAGEFILTERS";
         lblFiltertextindicador_areatrabalhocod_Internalname = "FILTERTEXTINDICADOR_AREATRABALHOCOD";
         edtavIndicador_areatrabalhocod_Internalname = "vINDICADOR_AREATRABALHOCOD";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavIndicador_nome1_Internalname = "vINDICADOR_NOME1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavIndicador_nome2_Internalname = "vINDICADOR_NOME2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavIndicador_nome3_Internalname = "vINDICADOR_NOME3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtIndicador_Codigo_Internalname = "INDICADOR_CODIGO";
         edtIndicador_Nome_Internalname = "INDICADOR_NOME";
         cmbIndicador_Refer_Internalname = "INDICADOR_REFER";
         cmbIndicador_Aplica_Internalname = "INDICADOR_APLICA";
         dltIndicador_NaoCnfCod_Internalname = "INDICADOR_NAOCNFCOD";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavManagefiltersexecutionstep_Internalname = "vMANAGEFILTERSEXECUTIONSTEP";
         edtavTfindicador_nome_Internalname = "vTFINDICADOR_NOME";
         edtavTfindicador_nome_sel_Internalname = "vTFINDICADOR_NOME_SEL";
         edtavTfindicador_naocnfcod_Internalname = "vTFINDICADOR_NAOCNFCOD";
         edtavTfindicador_naocnfcod_to_Internalname = "vTFINDICADOR_NAOCNFCOD_TO";
         Ddo_indicador_nome_Internalname = "DDO_INDICADOR_NOME";
         edtavDdo_indicador_nometitlecontrolidtoreplace_Internalname = "vDDO_INDICADOR_NOMETITLECONTROLIDTOREPLACE";
         Ddo_indicador_refer_Internalname = "DDO_INDICADOR_REFER";
         edtavDdo_indicador_refertitlecontrolidtoreplace_Internalname = "vDDO_INDICADOR_REFERTITLECONTROLIDTOREPLACE";
         Ddo_indicador_aplica_Internalname = "DDO_INDICADOR_APLICA";
         edtavDdo_indicador_aplicatitlecontrolidtoreplace_Internalname = "vDDO_INDICADOR_APLICATITLECONTROLIDTOREPLACE";
         Ddo_indicador_naocnfcod_Internalname = "DDO_INDICADOR_NAOCNFCOD";
         edtavDdo_indicador_naocnfcodtitlecontrolidtoreplace_Internalname = "vDDO_INDICADOR_NAOCNFCODTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         dltIndicador_NaoCnfCod_Jsonclick = "";
         cmbIndicador_Aplica_Jsonclick = "";
         cmbIndicador_Refer_Jsonclick = "";
         edtIndicador_Nome_Jsonclick = "";
         edtIndicador_Codigo_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         imgInsert_Enabled = 1;
         imgInsert_Visible = 1;
         imgInsert_Bitmap = (String)(context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )));
         edtavIndicador_nome1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavIndicador_nome2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavIndicador_nome3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavIndicador_areatrabalhocod_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtIndicador_Nome_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavDelete_Enabled = 1;
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtavUpdate_Enabled = 1;
         dltIndicador_NaoCnfCod_Titleformat = 0;
         cmbIndicador_Aplica_Titleformat = 0;
         cmbIndicador_Refer_Titleformat = 0;
         edtIndicador_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavIndicador_nome3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavIndicador_nome2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavIndicador_nome1_Visible = 1;
         edtavDelete_Visible = -1;
         edtavUpdate_Visible = -1;
         dltIndicador_NaoCnfCod.Title.Text = "N�o Conformidade";
         cmbIndicador_Aplica.Title.Text = "Aplica-se a";
         cmbIndicador_Refer.Title.Text = "Refere-se a";
         edtIndicador_Nome_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_indicador_naocnfcodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_indicador_aplicatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_indicador_refertitlecontrolidtoreplace_Visible = 1;
         edtavDdo_indicador_nometitlecontrolidtoreplace_Visible = 1;
         edtavTfindicador_naocnfcod_to_Jsonclick = "";
         edtavTfindicador_naocnfcod_to_Visible = 1;
         edtavTfindicador_naocnfcod_Jsonclick = "";
         edtavTfindicador_naocnfcod_Visible = 1;
         edtavTfindicador_nome_sel_Jsonclick = "";
         edtavTfindicador_nome_sel_Visible = 1;
         edtavTfindicador_nome_Jsonclick = "";
         edtavTfindicador_nome_Visible = 1;
         edtavManagefiltersexecutionstep_Jsonclick = "";
         edtavManagefiltersexecutionstep_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_indicador_naocnfcod_Searchbuttontext = "Pesquisar";
         Ddo_indicador_naocnfcod_Rangefilterto = "At�";
         Ddo_indicador_naocnfcod_Rangefilterfrom = "Desde";
         Ddo_indicador_naocnfcod_Cleanfilter = "Limpar pesquisa";
         Ddo_indicador_naocnfcod_Sortdsc = "Ordenar de Z � A";
         Ddo_indicador_naocnfcod_Sortasc = "Ordenar de A � Z";
         Ddo_indicador_naocnfcod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_indicador_naocnfcod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_indicador_naocnfcod_Filtertype = "Numeric";
         Ddo_indicador_naocnfcod_Includefilter = Convert.ToBoolean( -1);
         Ddo_indicador_naocnfcod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_indicador_naocnfcod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_indicador_naocnfcod_Titlecontrolidtoreplace = "";
         Ddo_indicador_naocnfcod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_indicador_naocnfcod_Cls = "ColumnSettings";
         Ddo_indicador_naocnfcod_Tooltip = "Op��es";
         Ddo_indicador_naocnfcod_Caption = "";
         Ddo_indicador_aplica_Searchbuttontext = "Filtrar Selecionados";
         Ddo_indicador_aplica_Cleanfilter = "Limpar pesquisa";
         Ddo_indicador_aplica_Sortdsc = "Ordenar de Z � A";
         Ddo_indicador_aplica_Sortasc = "Ordenar de A � Z";
         Ddo_indicador_aplica_Datalistfixedvalues = "1:Demandas,2:Lotes";
         Ddo_indicador_aplica_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_indicador_aplica_Datalisttype = "FixedValues";
         Ddo_indicador_aplica_Includedatalist = Convert.ToBoolean( -1);
         Ddo_indicador_aplica_Includefilter = Convert.ToBoolean( 0);
         Ddo_indicador_aplica_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_indicador_aplica_Includesortasc = Convert.ToBoolean( -1);
         Ddo_indicador_aplica_Titlecontrolidtoreplace = "";
         Ddo_indicador_aplica_Dropdownoptionstype = "GridTitleSettings";
         Ddo_indicador_aplica_Cls = "ColumnSettings";
         Ddo_indicador_aplica_Tooltip = "Op��es";
         Ddo_indicador_aplica_Caption = "";
         Ddo_indicador_refer_Searchbuttontext = "Filtrar Selecionados";
         Ddo_indicador_refer_Cleanfilter = "Limpar pesquisa";
         Ddo_indicador_refer_Sortdsc = "Ordenar de Z � A";
         Ddo_indicador_refer_Sortasc = "Ordenar de A � Z";
         Ddo_indicador_refer_Datalistfixedvalues = "1:Prazo,2:Frequ�ncia,3:Valores";
         Ddo_indicador_refer_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_indicador_refer_Datalisttype = "FixedValues";
         Ddo_indicador_refer_Includedatalist = Convert.ToBoolean( -1);
         Ddo_indicador_refer_Includefilter = Convert.ToBoolean( 0);
         Ddo_indicador_refer_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_indicador_refer_Includesortasc = Convert.ToBoolean( -1);
         Ddo_indicador_refer_Titlecontrolidtoreplace = "";
         Ddo_indicador_refer_Dropdownoptionstype = "GridTitleSettings";
         Ddo_indicador_refer_Cls = "ColumnSettings";
         Ddo_indicador_refer_Tooltip = "Op��es";
         Ddo_indicador_refer_Caption = "";
         Ddo_indicador_nome_Searchbuttontext = "Pesquisar";
         Ddo_indicador_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_indicador_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_indicador_nome_Loadingdata = "Carregando dados...";
         Ddo_indicador_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_indicador_nome_Sortasc = "Ordenar de A � Z";
         Ddo_indicador_nome_Datalistupdateminimumcharacters = 0;
         Ddo_indicador_nome_Datalistproc = "GetWWIndicadorFilterData";
         Ddo_indicador_nome_Datalisttype = "Dynamic";
         Ddo_indicador_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_indicador_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_indicador_nome_Filtertype = "Character";
         Ddo_indicador_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_indicador_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_indicador_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_indicador_nome_Titlecontrolidtoreplace = "";
         Ddo_indicador_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_indicador_nome_Cls = "ColumnSettings";
         Ddo_indicador_nome_Tooltip = "Op��es";
         Ddo_indicador_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Ddo_managefilters_Cls = "ManageFilters";
         Ddo_managefilters_Tooltip = "Gerenciar filtros";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Indicador";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A2058Indicador_Codigo',fld:'INDICADOR_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV28ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV37ddo_Indicador_NomeTitleControlIdToReplace',fld:'vDDO_INDICADOR_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Indicador_ReferTitleControlIdToReplace',fld:'vDDO_INDICADOR_REFERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Indicador_AplicaTitleControlIdToReplace',fld:'vDDO_INDICADOR_APLICATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Indicador_NaoCnfCodTitleControlIdToReplace',fld:'vDDO_INDICADOR_NAOCNFCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45Indicador_AreaTrabalhoCod',fld:'vINDICADOR_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV15DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16Indicador_Nome1',fld:'vINDICADOR_NOME1',pic:'@!',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20Indicador_Nome2',fld:'vINDICADOR_NOME2',pic:'@!',nv:''},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV24Indicador_Nome3',fld:'vINDICADOR_NOME3',pic:'@!',nv:''},{av:'AV35TFIndicador_Nome',fld:'vTFINDICADOR_NOME',pic:'@!',nv:''},{av:'AV36TFIndicador_Nome_Sel',fld:'vTFINDICADOR_NOME_SEL',pic:'@!',nv:''},{av:'AV48TFIndicador_Refer_Sels',fld:'vTFINDICADOR_REFER_SELS',pic:'',nv:null},{av:'AV52TFIndicador_Aplica_Sels',fld:'vTFINDICADOR_APLICA_SELS',pic:'',nv:null},{av:'AV55TFIndicador_NaoCnfCod',fld:'vTFINDICADOR_NAOCNFCOD',pic:'ZZZZZ9',nv:0},{av:'AV56TFIndicador_NaoCnfCod_To',fld:'vTFINDICADOR_NAOCNFCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV2WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV80Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV44OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV34Indicador_NomeTitleFilterData',fld:'vINDICADOR_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV46Indicador_ReferTitleFilterData',fld:'vINDICADOR_REFERTITLEFILTERDATA',pic:'',nv:null},{av:'AV50Indicador_AplicaTitleFilterData',fld:'vINDICADOR_APLICATITLEFILTERDATA',pic:'',nv:null},{av:'AV54Indicador_NaoCnfCodTitleFilterData',fld:'vINDICADOR_NAOCNFCODTITLEFILTERDATA',pic:'',nv:null},{av:'AV2WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV28ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'edtIndicador_Nome_Titleformat',ctrl:'INDICADOR_NOME',prop:'Titleformat'},{av:'edtIndicador_Nome_Title',ctrl:'INDICADOR_NOME',prop:'Title'},{av:'cmbIndicador_Refer'},{av:'cmbIndicador_Aplica'},{av:'dltIndicador_NaoCnfCod'},{av:'AV40GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV41GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'imgInsert_Visible',ctrl:'INSERT',prop:'Visible'},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'imgInsert_Link',ctrl:'INSERT',prop:'Link'},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'},{av:'AV32ManageFiltersData',fld:'vMANAGEFILTERSDATA',pic:'',nv:null},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E12RV2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV44OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV15DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16Indicador_Nome1',fld:'vINDICADOR_NOME1',pic:'@!',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20Indicador_Nome2',fld:'vINDICADOR_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV24Indicador_Nome3',fld:'vINDICADOR_NOME3',pic:'@!',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFIndicador_Nome',fld:'vTFINDICADOR_NOME',pic:'@!',nv:''},{av:'AV36TFIndicador_Nome_Sel',fld:'vTFINDICADOR_NOME_SEL',pic:'@!',nv:''},{av:'AV55TFIndicador_NaoCnfCod',fld:'vTFINDICADOR_NAOCNFCOD',pic:'ZZZZZ9',nv:0},{av:'AV56TFIndicador_NaoCnfCod_To',fld:'vTFINDICADOR_NAOCNFCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV2WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV28ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV37ddo_Indicador_NomeTitleControlIdToReplace',fld:'vDDO_INDICADOR_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Indicador_ReferTitleControlIdToReplace',fld:'vDDO_INDICADOR_REFERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Indicador_AplicaTitleControlIdToReplace',fld:'vDDO_INDICADOR_APLICATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Indicador_NaoCnfCodTitleControlIdToReplace',fld:'vDDO_INDICADOR_NAOCNFCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45Indicador_AreaTrabalhoCod',fld:'vINDICADOR_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFIndicador_Refer_Sels',fld:'vTFINDICADOR_REFER_SELS',pic:'',nv:null},{av:'AV52TFIndicador_Aplica_Sels',fld:'vTFINDICADOR_APLICA_SELS',pic:'',nv:null},{av:'AV80Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A2058Indicador_Codigo',fld:'INDICADOR_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_INDICADOR_NOME.ONOPTIONCLICKED","{handler:'E13RV2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV44OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV15DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16Indicador_Nome1',fld:'vINDICADOR_NOME1',pic:'@!',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20Indicador_Nome2',fld:'vINDICADOR_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV24Indicador_Nome3',fld:'vINDICADOR_NOME3',pic:'@!',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFIndicador_Nome',fld:'vTFINDICADOR_NOME',pic:'@!',nv:''},{av:'AV36TFIndicador_Nome_Sel',fld:'vTFINDICADOR_NOME_SEL',pic:'@!',nv:''},{av:'AV55TFIndicador_NaoCnfCod',fld:'vTFINDICADOR_NAOCNFCOD',pic:'ZZZZZ9',nv:0},{av:'AV56TFIndicador_NaoCnfCod_To',fld:'vTFINDICADOR_NAOCNFCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV2WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV28ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV37ddo_Indicador_NomeTitleControlIdToReplace',fld:'vDDO_INDICADOR_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Indicador_ReferTitleControlIdToReplace',fld:'vDDO_INDICADOR_REFERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Indicador_AplicaTitleControlIdToReplace',fld:'vDDO_INDICADOR_APLICATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Indicador_NaoCnfCodTitleControlIdToReplace',fld:'vDDO_INDICADOR_NAOCNFCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45Indicador_AreaTrabalhoCod',fld:'vINDICADOR_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFIndicador_Refer_Sels',fld:'vTFINDICADOR_REFER_SELS',pic:'',nv:null},{av:'AV52TFIndicador_Aplica_Sels',fld:'vTFINDICADOR_APLICA_SELS',pic:'',nv:null},{av:'AV80Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A2058Indicador_Codigo',fld:'INDICADOR_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_indicador_nome_Activeeventkey',ctrl:'DDO_INDICADOR_NOME',prop:'ActiveEventKey'},{av:'Ddo_indicador_nome_Filteredtext_get',ctrl:'DDO_INDICADOR_NOME',prop:'FilteredText_get'},{av:'Ddo_indicador_nome_Selectedvalue_get',ctrl:'DDO_INDICADOR_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV44OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_indicador_nome_Sortedstatus',ctrl:'DDO_INDICADOR_NOME',prop:'SortedStatus'},{av:'AV35TFIndicador_Nome',fld:'vTFINDICADOR_NOME',pic:'@!',nv:''},{av:'AV36TFIndicador_Nome_Sel',fld:'vTFINDICADOR_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_indicador_refer_Sortedstatus',ctrl:'DDO_INDICADOR_REFER',prop:'SortedStatus'},{av:'Ddo_indicador_aplica_Sortedstatus',ctrl:'DDO_INDICADOR_APLICA',prop:'SortedStatus'},{av:'Ddo_indicador_naocnfcod_Sortedstatus',ctrl:'DDO_INDICADOR_NAOCNFCOD',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_INDICADOR_REFER.ONOPTIONCLICKED","{handler:'E14RV2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV44OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV15DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16Indicador_Nome1',fld:'vINDICADOR_NOME1',pic:'@!',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20Indicador_Nome2',fld:'vINDICADOR_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV24Indicador_Nome3',fld:'vINDICADOR_NOME3',pic:'@!',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFIndicador_Nome',fld:'vTFINDICADOR_NOME',pic:'@!',nv:''},{av:'AV36TFIndicador_Nome_Sel',fld:'vTFINDICADOR_NOME_SEL',pic:'@!',nv:''},{av:'AV55TFIndicador_NaoCnfCod',fld:'vTFINDICADOR_NAOCNFCOD',pic:'ZZZZZ9',nv:0},{av:'AV56TFIndicador_NaoCnfCod_To',fld:'vTFINDICADOR_NAOCNFCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV2WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV28ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV37ddo_Indicador_NomeTitleControlIdToReplace',fld:'vDDO_INDICADOR_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Indicador_ReferTitleControlIdToReplace',fld:'vDDO_INDICADOR_REFERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Indicador_AplicaTitleControlIdToReplace',fld:'vDDO_INDICADOR_APLICATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Indicador_NaoCnfCodTitleControlIdToReplace',fld:'vDDO_INDICADOR_NAOCNFCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45Indicador_AreaTrabalhoCod',fld:'vINDICADOR_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFIndicador_Refer_Sels',fld:'vTFINDICADOR_REFER_SELS',pic:'',nv:null},{av:'AV52TFIndicador_Aplica_Sels',fld:'vTFINDICADOR_APLICA_SELS',pic:'',nv:null},{av:'AV80Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A2058Indicador_Codigo',fld:'INDICADOR_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_indicador_refer_Activeeventkey',ctrl:'DDO_INDICADOR_REFER',prop:'ActiveEventKey'},{av:'Ddo_indicador_refer_Selectedvalue_get',ctrl:'DDO_INDICADOR_REFER',prop:'SelectedValue_get'}],oparms:[{av:'AV44OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_indicador_refer_Sortedstatus',ctrl:'DDO_INDICADOR_REFER',prop:'SortedStatus'},{av:'AV48TFIndicador_Refer_Sels',fld:'vTFINDICADOR_REFER_SELS',pic:'',nv:null},{av:'Ddo_indicador_nome_Sortedstatus',ctrl:'DDO_INDICADOR_NOME',prop:'SortedStatus'},{av:'Ddo_indicador_aplica_Sortedstatus',ctrl:'DDO_INDICADOR_APLICA',prop:'SortedStatus'},{av:'Ddo_indicador_naocnfcod_Sortedstatus',ctrl:'DDO_INDICADOR_NAOCNFCOD',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_INDICADOR_APLICA.ONOPTIONCLICKED","{handler:'E15RV2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV44OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV15DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16Indicador_Nome1',fld:'vINDICADOR_NOME1',pic:'@!',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20Indicador_Nome2',fld:'vINDICADOR_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV24Indicador_Nome3',fld:'vINDICADOR_NOME3',pic:'@!',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFIndicador_Nome',fld:'vTFINDICADOR_NOME',pic:'@!',nv:''},{av:'AV36TFIndicador_Nome_Sel',fld:'vTFINDICADOR_NOME_SEL',pic:'@!',nv:''},{av:'AV55TFIndicador_NaoCnfCod',fld:'vTFINDICADOR_NAOCNFCOD',pic:'ZZZZZ9',nv:0},{av:'AV56TFIndicador_NaoCnfCod_To',fld:'vTFINDICADOR_NAOCNFCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV2WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV28ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV37ddo_Indicador_NomeTitleControlIdToReplace',fld:'vDDO_INDICADOR_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Indicador_ReferTitleControlIdToReplace',fld:'vDDO_INDICADOR_REFERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Indicador_AplicaTitleControlIdToReplace',fld:'vDDO_INDICADOR_APLICATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Indicador_NaoCnfCodTitleControlIdToReplace',fld:'vDDO_INDICADOR_NAOCNFCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45Indicador_AreaTrabalhoCod',fld:'vINDICADOR_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFIndicador_Refer_Sels',fld:'vTFINDICADOR_REFER_SELS',pic:'',nv:null},{av:'AV52TFIndicador_Aplica_Sels',fld:'vTFINDICADOR_APLICA_SELS',pic:'',nv:null},{av:'AV80Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A2058Indicador_Codigo',fld:'INDICADOR_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_indicador_aplica_Activeeventkey',ctrl:'DDO_INDICADOR_APLICA',prop:'ActiveEventKey'},{av:'Ddo_indicador_aplica_Selectedvalue_get',ctrl:'DDO_INDICADOR_APLICA',prop:'SelectedValue_get'}],oparms:[{av:'AV44OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_indicador_aplica_Sortedstatus',ctrl:'DDO_INDICADOR_APLICA',prop:'SortedStatus'},{av:'AV52TFIndicador_Aplica_Sels',fld:'vTFINDICADOR_APLICA_SELS',pic:'',nv:null},{av:'Ddo_indicador_nome_Sortedstatus',ctrl:'DDO_INDICADOR_NOME',prop:'SortedStatus'},{av:'Ddo_indicador_refer_Sortedstatus',ctrl:'DDO_INDICADOR_REFER',prop:'SortedStatus'},{av:'Ddo_indicador_naocnfcod_Sortedstatus',ctrl:'DDO_INDICADOR_NAOCNFCOD',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_INDICADOR_NAOCNFCOD.ONOPTIONCLICKED","{handler:'E16RV2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV44OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV15DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16Indicador_Nome1',fld:'vINDICADOR_NOME1',pic:'@!',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20Indicador_Nome2',fld:'vINDICADOR_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV24Indicador_Nome3',fld:'vINDICADOR_NOME3',pic:'@!',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFIndicador_Nome',fld:'vTFINDICADOR_NOME',pic:'@!',nv:''},{av:'AV36TFIndicador_Nome_Sel',fld:'vTFINDICADOR_NOME_SEL',pic:'@!',nv:''},{av:'AV55TFIndicador_NaoCnfCod',fld:'vTFINDICADOR_NAOCNFCOD',pic:'ZZZZZ9',nv:0},{av:'AV56TFIndicador_NaoCnfCod_To',fld:'vTFINDICADOR_NAOCNFCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV2WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV28ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV37ddo_Indicador_NomeTitleControlIdToReplace',fld:'vDDO_INDICADOR_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Indicador_ReferTitleControlIdToReplace',fld:'vDDO_INDICADOR_REFERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Indicador_AplicaTitleControlIdToReplace',fld:'vDDO_INDICADOR_APLICATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Indicador_NaoCnfCodTitleControlIdToReplace',fld:'vDDO_INDICADOR_NAOCNFCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45Indicador_AreaTrabalhoCod',fld:'vINDICADOR_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFIndicador_Refer_Sels',fld:'vTFINDICADOR_REFER_SELS',pic:'',nv:null},{av:'AV52TFIndicador_Aplica_Sels',fld:'vTFINDICADOR_APLICA_SELS',pic:'',nv:null},{av:'AV80Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A2058Indicador_Codigo',fld:'INDICADOR_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_indicador_naocnfcod_Activeeventkey',ctrl:'DDO_INDICADOR_NAOCNFCOD',prop:'ActiveEventKey'},{av:'Ddo_indicador_naocnfcod_Filteredtext_get',ctrl:'DDO_INDICADOR_NAOCNFCOD',prop:'FilteredText_get'},{av:'Ddo_indicador_naocnfcod_Filteredtextto_get',ctrl:'DDO_INDICADOR_NAOCNFCOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV44OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_indicador_naocnfcod_Sortedstatus',ctrl:'DDO_INDICADOR_NAOCNFCOD',prop:'SortedStatus'},{av:'AV55TFIndicador_NaoCnfCod',fld:'vTFINDICADOR_NAOCNFCOD',pic:'ZZZZZ9',nv:0},{av:'AV56TFIndicador_NaoCnfCod_To',fld:'vTFINDICADOR_NAOCNFCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_indicador_nome_Sortedstatus',ctrl:'DDO_INDICADOR_NOME',prop:'SortedStatus'},{av:'Ddo_indicador_refer_Sortedstatus',ctrl:'DDO_INDICADOR_REFER',prop:'SortedStatus'},{av:'Ddo_indicador_aplica_Sortedstatus',ctrl:'DDO_INDICADOR_APLICA',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E29RV2',iparms:[{av:'AV2WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A2058Indicador_Codigo',fld:'INDICADOR_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV42Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Enabled',ctrl:'vUPDATE',prop:'Enabled'},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV43Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Enabled',ctrl:'vDELETE',prop:'Enabled'},{av:'edtIndicador_Nome_Link',ctrl:'INDICADOR_NOME',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E17RV2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV44OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV15DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16Indicador_Nome1',fld:'vINDICADOR_NOME1',pic:'@!',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20Indicador_Nome2',fld:'vINDICADOR_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV24Indicador_Nome3',fld:'vINDICADOR_NOME3',pic:'@!',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFIndicador_Nome',fld:'vTFINDICADOR_NOME',pic:'@!',nv:''},{av:'AV36TFIndicador_Nome_Sel',fld:'vTFINDICADOR_NOME_SEL',pic:'@!',nv:''},{av:'AV55TFIndicador_NaoCnfCod',fld:'vTFINDICADOR_NAOCNFCOD',pic:'ZZZZZ9',nv:0},{av:'AV56TFIndicador_NaoCnfCod_To',fld:'vTFINDICADOR_NAOCNFCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV2WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV28ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV37ddo_Indicador_NomeTitleControlIdToReplace',fld:'vDDO_INDICADOR_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Indicador_ReferTitleControlIdToReplace',fld:'vDDO_INDICADOR_REFERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Indicador_AplicaTitleControlIdToReplace',fld:'vDDO_INDICADOR_APLICATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Indicador_NaoCnfCodTitleControlIdToReplace',fld:'vDDO_INDICADOR_NAOCNFCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45Indicador_AreaTrabalhoCod',fld:'vINDICADOR_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFIndicador_Refer_Sels',fld:'vTFINDICADOR_REFER_SELS',pic:'',nv:null},{av:'AV52TFIndicador_Aplica_Sels',fld:'vTFINDICADOR_APLICA_SELS',pic:'',nv:null},{av:'AV80Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A2058Indicador_Codigo',fld:'INDICADOR_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E22RV2',iparms:[],oparms:[{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E18RV2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV44OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV15DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16Indicador_Nome1',fld:'vINDICADOR_NOME1',pic:'@!',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20Indicador_Nome2',fld:'vINDICADOR_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV24Indicador_Nome3',fld:'vINDICADOR_NOME3',pic:'@!',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFIndicador_Nome',fld:'vTFINDICADOR_NOME',pic:'@!',nv:''},{av:'AV36TFIndicador_Nome_Sel',fld:'vTFINDICADOR_NOME_SEL',pic:'@!',nv:''},{av:'AV55TFIndicador_NaoCnfCod',fld:'vTFINDICADOR_NAOCNFCOD',pic:'ZZZZZ9',nv:0},{av:'AV56TFIndicador_NaoCnfCod_To',fld:'vTFINDICADOR_NAOCNFCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV2WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV28ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV37ddo_Indicador_NomeTitleControlIdToReplace',fld:'vDDO_INDICADOR_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Indicador_ReferTitleControlIdToReplace',fld:'vDDO_INDICADOR_REFERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Indicador_AplicaTitleControlIdToReplace',fld:'vDDO_INDICADOR_APLICATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Indicador_NaoCnfCodTitleControlIdToReplace',fld:'vDDO_INDICADOR_NAOCNFCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45Indicador_AreaTrabalhoCod',fld:'vINDICADOR_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFIndicador_Refer_Sels',fld:'vTFINDICADOR_REFER_SELS',pic:'',nv:null},{av:'AV52TFIndicador_Aplica_Sels',fld:'vTFINDICADOR_APLICA_SELS',pic:'',nv:null},{av:'AV80Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A2058Indicador_Codigo',fld:'INDICADOR_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20Indicador_Nome2',fld:'vINDICADOR_NOME2',pic:'@!',nv:''},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV24Indicador_Nome3',fld:'vINDICADOR_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV15DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16Indicador_Nome1',fld:'vINDICADOR_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavIndicador_nome2_Visible',ctrl:'vINDICADOR_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavIndicador_nome3_Visible',ctrl:'vINDICADOR_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavIndicador_nome1_Visible',ctrl:'vINDICADOR_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E23RV2',iparms:[{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavIndicador_nome1_Visible',ctrl:'vINDICADOR_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E24RV2',iparms:[],oparms:[{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E19RV2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV44OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV15DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16Indicador_Nome1',fld:'vINDICADOR_NOME1',pic:'@!',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20Indicador_Nome2',fld:'vINDICADOR_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV24Indicador_Nome3',fld:'vINDICADOR_NOME3',pic:'@!',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFIndicador_Nome',fld:'vTFINDICADOR_NOME',pic:'@!',nv:''},{av:'AV36TFIndicador_Nome_Sel',fld:'vTFINDICADOR_NOME_SEL',pic:'@!',nv:''},{av:'AV55TFIndicador_NaoCnfCod',fld:'vTFINDICADOR_NAOCNFCOD',pic:'ZZZZZ9',nv:0},{av:'AV56TFIndicador_NaoCnfCod_To',fld:'vTFINDICADOR_NAOCNFCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV2WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV28ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV37ddo_Indicador_NomeTitleControlIdToReplace',fld:'vDDO_INDICADOR_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Indicador_ReferTitleControlIdToReplace',fld:'vDDO_INDICADOR_REFERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Indicador_AplicaTitleControlIdToReplace',fld:'vDDO_INDICADOR_APLICATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Indicador_NaoCnfCodTitleControlIdToReplace',fld:'vDDO_INDICADOR_NAOCNFCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45Indicador_AreaTrabalhoCod',fld:'vINDICADOR_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFIndicador_Refer_Sels',fld:'vTFINDICADOR_REFER_SELS',pic:'',nv:null},{av:'AV52TFIndicador_Aplica_Sels',fld:'vTFINDICADOR_APLICA_SELS',pic:'',nv:null},{av:'AV80Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A2058Indicador_Codigo',fld:'INDICADOR_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20Indicador_Nome2',fld:'vINDICADOR_NOME2',pic:'@!',nv:''},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV24Indicador_Nome3',fld:'vINDICADOR_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV15DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16Indicador_Nome1',fld:'vINDICADOR_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavIndicador_nome2_Visible',ctrl:'vINDICADOR_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavIndicador_nome3_Visible',ctrl:'vINDICADOR_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavIndicador_nome1_Visible',ctrl:'vINDICADOR_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E25RV2',iparms:[{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavIndicador_nome2_Visible',ctrl:'vINDICADOR_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E20RV2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV44OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV15DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16Indicador_Nome1',fld:'vINDICADOR_NOME1',pic:'@!',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20Indicador_Nome2',fld:'vINDICADOR_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV24Indicador_Nome3',fld:'vINDICADOR_NOME3',pic:'@!',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFIndicador_Nome',fld:'vTFINDICADOR_NOME',pic:'@!',nv:''},{av:'AV36TFIndicador_Nome_Sel',fld:'vTFINDICADOR_NOME_SEL',pic:'@!',nv:''},{av:'AV55TFIndicador_NaoCnfCod',fld:'vTFINDICADOR_NAOCNFCOD',pic:'ZZZZZ9',nv:0},{av:'AV56TFIndicador_NaoCnfCod_To',fld:'vTFINDICADOR_NAOCNFCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV2WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV28ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV37ddo_Indicador_NomeTitleControlIdToReplace',fld:'vDDO_INDICADOR_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Indicador_ReferTitleControlIdToReplace',fld:'vDDO_INDICADOR_REFERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Indicador_AplicaTitleControlIdToReplace',fld:'vDDO_INDICADOR_APLICATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Indicador_NaoCnfCodTitleControlIdToReplace',fld:'vDDO_INDICADOR_NAOCNFCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45Indicador_AreaTrabalhoCod',fld:'vINDICADOR_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFIndicador_Refer_Sels',fld:'vTFINDICADOR_REFER_SELS',pic:'',nv:null},{av:'AV52TFIndicador_Aplica_Sels',fld:'vTFINDICADOR_APLICA_SELS',pic:'',nv:null},{av:'AV80Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A2058Indicador_Codigo',fld:'INDICADOR_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20Indicador_Nome2',fld:'vINDICADOR_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV24Indicador_Nome3',fld:'vINDICADOR_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV15DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16Indicador_Nome1',fld:'vINDICADOR_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavIndicador_nome2_Visible',ctrl:'vINDICADOR_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavIndicador_nome3_Visible',ctrl:'vINDICADOR_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavIndicador_nome1_Visible',ctrl:'vINDICADOR_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E26RV2',iparms:[{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavIndicador_nome3_Visible',ctrl:'vINDICADOR_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("DDO_MANAGEFILTERS.ONOPTIONCLICKED","{handler:'E11RV2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV44OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV15DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16Indicador_Nome1',fld:'vINDICADOR_NOME1',pic:'@!',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20Indicador_Nome2',fld:'vINDICADOR_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV24Indicador_Nome3',fld:'vINDICADOR_NOME3',pic:'@!',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFIndicador_Nome',fld:'vTFINDICADOR_NOME',pic:'@!',nv:''},{av:'AV36TFIndicador_Nome_Sel',fld:'vTFINDICADOR_NOME_SEL',pic:'@!',nv:''},{av:'AV55TFIndicador_NaoCnfCod',fld:'vTFINDICADOR_NAOCNFCOD',pic:'ZZZZZ9',nv:0},{av:'AV56TFIndicador_NaoCnfCod_To',fld:'vTFINDICADOR_NAOCNFCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV2WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV28ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV37ddo_Indicador_NomeTitleControlIdToReplace',fld:'vDDO_INDICADOR_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Indicador_ReferTitleControlIdToReplace',fld:'vDDO_INDICADOR_REFERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Indicador_AplicaTitleControlIdToReplace',fld:'vDDO_INDICADOR_APLICATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Indicador_NaoCnfCodTitleControlIdToReplace',fld:'vDDO_INDICADOR_NAOCNFCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45Indicador_AreaTrabalhoCod',fld:'vINDICADOR_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFIndicador_Refer_Sels',fld:'vTFINDICADOR_REFER_SELS',pic:'',nv:null},{av:'AV52TFIndicador_Aplica_Sels',fld:'vTFINDICADOR_APLICA_SELS',pic:'',nv:null},{av:'AV80Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A2058Indicador_Codigo',fld:'INDICADOR_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_managefilters_Activeeventkey',ctrl:'DDO_MANAGEFILTERS',prop:'ActiveEventKey'}],oparms:[{av:'AV28ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV44OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV45Indicador_AreaTrabalhoCod',fld:'vINDICADOR_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV35TFIndicador_Nome',fld:'vTFINDICADOR_NOME',pic:'@!',nv:''},{av:'Ddo_indicador_nome_Filteredtext_set',ctrl:'DDO_INDICADOR_NOME',prop:'FilteredText_set'},{av:'AV36TFIndicador_Nome_Sel',fld:'vTFINDICADOR_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_indicador_nome_Selectedvalue_set',ctrl:'DDO_INDICADOR_NOME',prop:'SelectedValue_set'},{av:'AV48TFIndicador_Refer_Sels',fld:'vTFINDICADOR_REFER_SELS',pic:'',nv:null},{av:'Ddo_indicador_refer_Selectedvalue_set',ctrl:'DDO_INDICADOR_REFER',prop:'SelectedValue_set'},{av:'AV52TFIndicador_Aplica_Sels',fld:'vTFINDICADOR_APLICA_SELS',pic:'',nv:null},{av:'Ddo_indicador_aplica_Selectedvalue_set',ctrl:'DDO_INDICADOR_APLICA',prop:'SelectedValue_set'},{av:'AV55TFIndicador_NaoCnfCod',fld:'vTFINDICADOR_NAOCNFCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_indicador_naocnfcod_Filteredtext_set',ctrl:'DDO_INDICADOR_NAOCNFCOD',prop:'FilteredText_set'},{av:'AV56TFIndicador_NaoCnfCod_To',fld:'vTFINDICADOR_NAOCNFCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_indicador_naocnfcod_Filteredtextto_set',ctrl:'DDO_INDICADOR_NAOCNFCOD',prop:'FilteredTextTo_set'},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV15DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16Indicador_Nome1',fld:'vINDICADOR_NOME1',pic:'@!',nv:''},{av:'Ddo_indicador_nome_Sortedstatus',ctrl:'DDO_INDICADOR_NOME',prop:'SortedStatus'},{av:'Ddo_indicador_refer_Sortedstatus',ctrl:'DDO_INDICADOR_REFER',prop:'SortedStatus'},{av:'Ddo_indicador_aplica_Sortedstatus',ctrl:'DDO_INDICADOR_APLICA',prop:'SortedStatus'},{av:'Ddo_indicador_naocnfcod_Sortedstatus',ctrl:'DDO_INDICADOR_NAOCNFCOD',prop:'SortedStatus'},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20Indicador_Nome2',fld:'vINDICADOR_NOME2',pic:'@!',nv:''},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV24Indicador_Nome3',fld:'vINDICADOR_NOME3',pic:'@!',nv:''},{av:'edtavIndicador_nome1_Visible',ctrl:'vINDICADOR_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'edtavIndicador_nome2_Visible',ctrl:'vINDICADOR_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavIndicador_nome3_Visible',ctrl:'vINDICADOR_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E21RV2',iparms:[{av:'A2058Indicador_Codigo',fld:'INDICADOR_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_indicador_nome_Activeeventkey = "";
         Ddo_indicador_nome_Filteredtext_get = "";
         Ddo_indicador_nome_Selectedvalue_get = "";
         Ddo_indicador_refer_Activeeventkey = "";
         Ddo_indicador_refer_Selectedvalue_get = "";
         Ddo_indicador_aplica_Activeeventkey = "";
         Ddo_indicador_aplica_Selectedvalue_get = "";
         Ddo_indicador_naocnfcod_Activeeventkey = "";
         Ddo_indicador_naocnfcod_Filteredtext_get = "";
         Ddo_indicador_naocnfcod_Filteredtextto_get = "";
         Ddo_managefilters_Activeeventkey = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV14DynamicFiltersSelector1 = "";
         AV16Indicador_Nome1 = "";
         AV18DynamicFiltersSelector2 = "";
         AV20Indicador_Nome2 = "";
         AV22DynamicFiltersSelector3 = "";
         AV24Indicador_Nome3 = "";
         AV35TFIndicador_Nome = "";
         AV36TFIndicador_Nome_Sel = "";
         AV2WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV37ddo_Indicador_NomeTitleControlIdToReplace = "";
         AV49ddo_Indicador_ReferTitleControlIdToReplace = "";
         AV53ddo_Indicador_AplicaTitleControlIdToReplace = "";
         AV57ddo_Indicador_NaoCnfCodTitleControlIdToReplace = "";
         AV45Indicador_AreaTrabalhoCod = AV2WWPContext.gxTpr_Areatrabalho_codigo;
         AV48TFIndicador_Refer_Sels = new GxSimpleCollection();
         AV52TFIndicador_Aplica_Sels = new GxSimpleCollection();
         AV80Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV32ManageFiltersData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV34Indicador_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46Indicador_ReferTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV50Indicador_AplicaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV54Indicador_NaoCnfCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_managefilters_Icon = "";
         Ddo_managefilters_Caption = "";
         Ddo_indicador_nome_Filteredtext_set = "";
         Ddo_indicador_nome_Selectedvalue_set = "";
         Ddo_indicador_nome_Sortedstatus = "";
         Ddo_indicador_refer_Selectedvalue_set = "";
         Ddo_indicador_refer_Sortedstatus = "";
         Ddo_indicador_aplica_Selectedvalue_set = "";
         Ddo_indicador_aplica_Sortedstatus = "";
         Ddo_indicador_naocnfcod_Filteredtext_set = "";
         Ddo_indicador_naocnfcod_Filteredtextto_set = "";
         Ddo_indicador_naocnfcod_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV42Update = "";
         AV78Update_GXI = "";
         AV43Delete = "";
         AV79Delete_GXI = "";
         A2059Indicador_Nome = "";
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00RV2_A2062Indicador_NaoCnfCod = new int[1] ;
         H00RV2_n2062Indicador_NaoCnfCod = new bool[] {false} ;
         H00RV2_A427NaoConformidade_Nome = new String[] {""} ;
         H00RV2_n427NaoConformidade_Nome = new bool[] {false} ;
         GridContainer = new GXWebGrid( context);
         AV74WWIndicadorDS_15_Tfindicador_refer_sels = new GxSimpleCollection();
         AV75WWIndicadorDS_16_Tfindicador_aplica_sels = new GxSimpleCollection();
         lV63WWIndicadorDS_4_Indicador_nome1 = "";
         lV67WWIndicadorDS_8_Indicador_nome2 = "";
         lV71WWIndicadorDS_12_Indicador_nome3 = "";
         lV72WWIndicadorDS_13_Tfindicador_nome = "";
         AV61WWIndicadorDS_2_Dynamicfiltersselector1 = "";
         AV63WWIndicadorDS_4_Indicador_nome1 = "";
         AV65WWIndicadorDS_6_Dynamicfiltersselector2 = "";
         AV67WWIndicadorDS_8_Indicador_nome2 = "";
         AV69WWIndicadorDS_10_Dynamicfiltersselector3 = "";
         AV71WWIndicadorDS_12_Indicador_nome3 = "";
         AV73WWIndicadorDS_14_Tfindicador_nome_sel = "";
         AV72WWIndicadorDS_13_Tfindicador_nome = "";
         H00RV3_A2061Indicador_AreaTrabalhoCod = new int[1] ;
         H00RV3_n2061Indicador_AreaTrabalhoCod = new bool[] {false} ;
         H00RV3_A2062Indicador_NaoCnfCod = new int[1] ;
         H00RV3_n2062Indicador_NaoCnfCod = new bool[] {false} ;
         H00RV3_A2064Indicador_Aplica = new short[1] ;
         H00RV3_n2064Indicador_Aplica = new bool[] {false} ;
         H00RV3_A2063Indicador_Refer = new short[1] ;
         H00RV3_n2063Indicador_Refer = new bool[] {false} ;
         H00RV3_A2059Indicador_Nome = new String[] {""} ;
         H00RV3_A2058Indicador_Codigo = new int[1] ;
         H00RV4_AGRID_nRecordCount = new long[1] ;
         AV7HTTPRequest = new GxHttpRequest( context);
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV47TFIndicador_Refer_SelsJson = "";
         AV51TFIndicador_Aplica_SelsJson = "";
         GridRow = new GXWebRow();
         AV29ManageFiltersXml = "";
         GXt_char2 = "";
         imgInsert_Link = "";
         AV33ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV30ManageFiltersItems = new GxObjectCollection( context, "GridStateCollection.Item", "", "wwpbaseobjects.SdtGridStateCollection_Item", "GeneXus.Programs");
         AV31ManageFiltersItem = new wwpbaseobjects.SdtGridStateCollection_Item(context);
         AV27Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblFiltertextindicador_areatrabalhocod_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblIndicadortitle_Jsonclick = "";
         imgInsert_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwindicador__default(),
            new Object[][] {
                new Object[] {
               H00RV2_A2062Indicador_NaoCnfCod, H00RV2_A427NaoConformidade_Nome, H00RV2_n427NaoConformidade_Nome
               }
               , new Object[] {
               H00RV3_A2061Indicador_AreaTrabalhoCod, H00RV3_n2061Indicador_AreaTrabalhoCod, H00RV3_A2062Indicador_NaoCnfCod, H00RV3_n2062Indicador_NaoCnfCod, H00RV3_A2064Indicador_Aplica, H00RV3_n2064Indicador_Aplica, H00RV3_A2063Indicador_Refer, H00RV3_n2063Indicador_Refer, H00RV3_A2059Indicador_Nome, H00RV3_A2058Indicador_Codigo
               }
               , new Object[] {
               H00RV4_AGRID_nRecordCount
               }
            }
         );
         AV80Pgmname = "WWIndicador";
         /* GeneXus formulas. */
         AV80Pgmname = "WWIndicador";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_92 ;
      private short nGXsfl_92_idx=1 ;
      private short AV44OrderedBy ;
      private short AV15DynamicFiltersOperator1 ;
      private short AV19DynamicFiltersOperator2 ;
      private short AV23DynamicFiltersOperator3 ;
      private short AV28ManageFiltersExecutionStep ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A2063Indicador_Refer ;
      private short A2064Indicador_Aplica ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_92_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV62WWIndicadorDS_3_Dynamicfiltersoperator1 ;
      private short AV66WWIndicadorDS_7_Dynamicfiltersoperator2 ;
      private short AV70WWIndicadorDS_11_Dynamicfiltersoperator3 ;
      private short edtIndicador_Nome_Titleformat ;
      private short cmbIndicador_Refer_Titleformat ;
      private short cmbIndicador_Aplica_Titleformat ;
      private short dltIndicador_NaoCnfCod_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV55TFIndicador_NaoCnfCod ;
      private int AV56TFIndicador_NaoCnfCod_To ;
      private int AV45Indicador_AreaTrabalhoCod ;
      private int A2058Indicador_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_indicador_nome_Datalistupdateminimumcharacters ;
      private int edtavManagefiltersexecutionstep_Visible ;
      private int edtavTfindicador_nome_Visible ;
      private int edtavTfindicador_nome_sel_Visible ;
      private int edtavTfindicador_naocnfcod_Visible ;
      private int edtavTfindicador_naocnfcod_to_Visible ;
      private int edtavDdo_indicador_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_indicador_refertitlecontrolidtoreplace_Visible ;
      private int edtavDdo_indicador_aplicatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_indicador_naocnfcodtitlecontrolidtoreplace_Visible ;
      private int A2062Indicador_NaoCnfCod ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV74WWIndicadorDS_15_Tfindicador_refer_sels_Count ;
      private int AV75WWIndicadorDS_16_Tfindicador_aplica_sels_Count ;
      private int AV2WWPContext_gxTpr_Areatrabalho_codigo ;
      private int AV76WWIndicadorDS_17_Tfindicador_naocnfcod ;
      private int AV77WWIndicadorDS_18_Tfindicador_naocnfcod_to ;
      private int A2061Indicador_AreaTrabalhoCod ;
      private int AV60WWIndicadorDS_1_Indicador_areatrabalhocod ;
      private int edtavOrdereddsc_Visible ;
      private int imgInsert_Visible ;
      private int edtavUpdate_Visible ;
      private int edtavDelete_Visible ;
      private int AV39PageToGo ;
      private int edtavUpdate_Enabled ;
      private int edtavDelete_Enabled ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int imgInsert_Enabled ;
      private int edtavIndicador_nome1_Visible ;
      private int edtavIndicador_nome2_Visible ;
      private int edtavIndicador_nome3_Visible ;
      private int AV81GXV1 ;
      private int AV82GXV2 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV40GridCurrentPage ;
      private long AV41GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_indicador_nome_Activeeventkey ;
      private String Ddo_indicador_nome_Filteredtext_get ;
      private String Ddo_indicador_nome_Selectedvalue_get ;
      private String Ddo_indicador_refer_Activeeventkey ;
      private String Ddo_indicador_refer_Selectedvalue_get ;
      private String Ddo_indicador_aplica_Activeeventkey ;
      private String Ddo_indicador_aplica_Selectedvalue_get ;
      private String Ddo_indicador_naocnfcod_Activeeventkey ;
      private String Ddo_indicador_naocnfcod_Filteredtext_get ;
      private String Ddo_indicador_naocnfcod_Filteredtextto_get ;
      private String Ddo_managefilters_Activeeventkey ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_92_idx="0001" ;
      private String AV16Indicador_Nome1 ;
      private String AV20Indicador_Nome2 ;
      private String AV24Indicador_Nome3 ;
      private String AV35TFIndicador_Nome ;
      private String AV36TFIndicador_Nome_Sel ;
      private String AV80Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Ddo_managefilters_Icon ;
      private String Ddo_managefilters_Caption ;
      private String Ddo_managefilters_Tooltip ;
      private String Ddo_managefilters_Cls ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_indicador_nome_Caption ;
      private String Ddo_indicador_nome_Tooltip ;
      private String Ddo_indicador_nome_Cls ;
      private String Ddo_indicador_nome_Filteredtext_set ;
      private String Ddo_indicador_nome_Selectedvalue_set ;
      private String Ddo_indicador_nome_Dropdownoptionstype ;
      private String Ddo_indicador_nome_Titlecontrolidtoreplace ;
      private String Ddo_indicador_nome_Sortedstatus ;
      private String Ddo_indicador_nome_Filtertype ;
      private String Ddo_indicador_nome_Datalisttype ;
      private String Ddo_indicador_nome_Datalistproc ;
      private String Ddo_indicador_nome_Sortasc ;
      private String Ddo_indicador_nome_Sortdsc ;
      private String Ddo_indicador_nome_Loadingdata ;
      private String Ddo_indicador_nome_Cleanfilter ;
      private String Ddo_indicador_nome_Noresultsfound ;
      private String Ddo_indicador_nome_Searchbuttontext ;
      private String Ddo_indicador_refer_Caption ;
      private String Ddo_indicador_refer_Tooltip ;
      private String Ddo_indicador_refer_Cls ;
      private String Ddo_indicador_refer_Selectedvalue_set ;
      private String Ddo_indicador_refer_Dropdownoptionstype ;
      private String Ddo_indicador_refer_Titlecontrolidtoreplace ;
      private String Ddo_indicador_refer_Sortedstatus ;
      private String Ddo_indicador_refer_Datalisttype ;
      private String Ddo_indicador_refer_Datalistfixedvalues ;
      private String Ddo_indicador_refer_Sortasc ;
      private String Ddo_indicador_refer_Sortdsc ;
      private String Ddo_indicador_refer_Cleanfilter ;
      private String Ddo_indicador_refer_Searchbuttontext ;
      private String Ddo_indicador_aplica_Caption ;
      private String Ddo_indicador_aplica_Tooltip ;
      private String Ddo_indicador_aplica_Cls ;
      private String Ddo_indicador_aplica_Selectedvalue_set ;
      private String Ddo_indicador_aplica_Dropdownoptionstype ;
      private String Ddo_indicador_aplica_Titlecontrolidtoreplace ;
      private String Ddo_indicador_aplica_Sortedstatus ;
      private String Ddo_indicador_aplica_Datalisttype ;
      private String Ddo_indicador_aplica_Datalistfixedvalues ;
      private String Ddo_indicador_aplica_Sortasc ;
      private String Ddo_indicador_aplica_Sortdsc ;
      private String Ddo_indicador_aplica_Cleanfilter ;
      private String Ddo_indicador_aplica_Searchbuttontext ;
      private String Ddo_indicador_naocnfcod_Caption ;
      private String Ddo_indicador_naocnfcod_Tooltip ;
      private String Ddo_indicador_naocnfcod_Cls ;
      private String Ddo_indicador_naocnfcod_Filteredtext_set ;
      private String Ddo_indicador_naocnfcod_Filteredtextto_set ;
      private String Ddo_indicador_naocnfcod_Dropdownoptionstype ;
      private String Ddo_indicador_naocnfcod_Titlecontrolidtoreplace ;
      private String Ddo_indicador_naocnfcod_Sortedstatus ;
      private String Ddo_indicador_naocnfcod_Filtertype ;
      private String Ddo_indicador_naocnfcod_Sortasc ;
      private String Ddo_indicador_naocnfcod_Sortdsc ;
      private String Ddo_indicador_naocnfcod_Cleanfilter ;
      private String Ddo_indicador_naocnfcod_Rangefilterfrom ;
      private String Ddo_indicador_naocnfcod_Rangefilterto ;
      private String Ddo_indicador_naocnfcod_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavManagefiltersexecutionstep_Internalname ;
      private String edtavManagefiltersexecutionstep_Jsonclick ;
      private String edtavTfindicador_nome_Internalname ;
      private String edtavTfindicador_nome_Jsonclick ;
      private String edtavTfindicador_nome_sel_Internalname ;
      private String edtavTfindicador_nome_sel_Jsonclick ;
      private String edtavTfindicador_naocnfcod_Internalname ;
      private String edtavTfindicador_naocnfcod_Jsonclick ;
      private String edtavTfindicador_naocnfcod_to_Internalname ;
      private String edtavTfindicador_naocnfcod_to_Jsonclick ;
      private String edtavDdo_indicador_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_indicador_refertitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_indicador_aplicatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_indicador_naocnfcodtitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtIndicador_Codigo_Internalname ;
      private String A2059Indicador_Nome ;
      private String edtIndicador_Nome_Internalname ;
      private String cmbIndicador_Refer_Internalname ;
      private String cmbIndicador_Aplica_Internalname ;
      private String dltIndicador_NaoCnfCod_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String lV63WWIndicadorDS_4_Indicador_nome1 ;
      private String lV67WWIndicadorDS_8_Indicador_nome2 ;
      private String lV71WWIndicadorDS_12_Indicador_nome3 ;
      private String lV72WWIndicadorDS_13_Tfindicador_nome ;
      private String AV63WWIndicadorDS_4_Indicador_nome1 ;
      private String AV67WWIndicadorDS_8_Indicador_nome2 ;
      private String AV71WWIndicadorDS_12_Indicador_nome3 ;
      private String AV73WWIndicadorDS_14_Tfindicador_nome_sel ;
      private String AV72WWIndicadorDS_13_Tfindicador_nome ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavIndicador_areatrabalhocod_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavIndicador_nome1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavIndicador_nome2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavIndicador_nome3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_indicador_nome_Internalname ;
      private String Ddo_indicador_refer_Internalname ;
      private String Ddo_indicador_aplica_Internalname ;
      private String Ddo_indicador_naocnfcod_Internalname ;
      private String Ddo_managefilters_Internalname ;
      private String edtIndicador_Nome_Title ;
      private String imgInsert_Internalname ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtIndicador_Nome_Link ;
      private String GXt_char2 ;
      private String imgInsert_Link ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String tblTablefilters_Internalname ;
      private String lblFiltertextindicador_areatrabalhocod_Internalname ;
      private String lblFiltertextindicador_areatrabalhocod_Jsonclick ;
      private String edtavIndicador_areatrabalhocod_Jsonclick ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavIndicador_nome3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavIndicador_nome2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavIndicador_nome1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String lblIndicadortitle_Internalname ;
      private String lblIndicadortitle_Jsonclick ;
      private String imgInsert_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String sGXsfl_92_fel_idx="0001" ;
      private String ROClassString ;
      private String edtIndicador_Codigo_Jsonclick ;
      private String edtIndicador_Nome_Jsonclick ;
      private String cmbIndicador_Refer_Jsonclick ;
      private String cmbIndicador_Aplica_Jsonclick ;
      private String dltIndicador_NaoCnfCod_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV13OrderedDsc ;
      private bool AV17DynamicFiltersEnabled2 ;
      private bool AV21DynamicFiltersEnabled3 ;
      private bool AV26DynamicFiltersIgnoreFirst ;
      private bool AV25DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_indicador_nome_Includesortasc ;
      private bool Ddo_indicador_nome_Includesortdsc ;
      private bool Ddo_indicador_nome_Includefilter ;
      private bool Ddo_indicador_nome_Filterisrange ;
      private bool Ddo_indicador_nome_Includedatalist ;
      private bool Ddo_indicador_refer_Includesortasc ;
      private bool Ddo_indicador_refer_Includesortdsc ;
      private bool Ddo_indicador_refer_Includefilter ;
      private bool Ddo_indicador_refer_Includedatalist ;
      private bool Ddo_indicador_refer_Allowmultipleselection ;
      private bool Ddo_indicador_aplica_Includesortasc ;
      private bool Ddo_indicador_aplica_Includesortdsc ;
      private bool Ddo_indicador_aplica_Includefilter ;
      private bool Ddo_indicador_aplica_Includedatalist ;
      private bool Ddo_indicador_aplica_Allowmultipleselection ;
      private bool Ddo_indicador_naocnfcod_Includesortasc ;
      private bool Ddo_indicador_naocnfcod_Includesortdsc ;
      private bool Ddo_indicador_naocnfcod_Includefilter ;
      private bool Ddo_indicador_naocnfcod_Filterisrange ;
      private bool Ddo_indicador_naocnfcod_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n2063Indicador_Refer ;
      private bool n2064Indicador_Aplica ;
      private bool n2062Indicador_NaoCnfCod ;
      private bool AV64WWIndicadorDS_5_Dynamicfiltersenabled2 ;
      private bool AV68WWIndicadorDS_9_Dynamicfiltersenabled3 ;
      private bool n2061Indicador_AreaTrabalhoCod ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV42Update_IsBlob ;
      private bool AV43Delete_IsBlob ;
      private String AV47TFIndicador_Refer_SelsJson ;
      private String AV51TFIndicador_Aplica_SelsJson ;
      private String AV29ManageFiltersXml ;
      private String AV14DynamicFiltersSelector1 ;
      private String AV18DynamicFiltersSelector2 ;
      private String AV22DynamicFiltersSelector3 ;
      private String AV37ddo_Indicador_NomeTitleControlIdToReplace ;
      private String AV49ddo_Indicador_ReferTitleControlIdToReplace ;
      private String AV53ddo_Indicador_AplicaTitleControlIdToReplace ;
      private String AV57ddo_Indicador_NaoCnfCodTitleControlIdToReplace ;
      private String AV78Update_GXI ;
      private String AV79Delete_GXI ;
      private String AV61WWIndicadorDS_2_Dynamicfiltersselector1 ;
      private String AV65WWIndicadorDS_6_Dynamicfiltersselector2 ;
      private String AV69WWIndicadorDS_10_Dynamicfiltersselector3 ;
      private String AV42Update ;
      private String AV43Delete ;
      private String imgInsert_Bitmap ;
      private IGxSession AV27Session ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCombobox cmbIndicador_Refer ;
      private GXCombobox cmbIndicador_Aplica ;
      private GXListbox dltIndicador_NaoCnfCod ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H00RV2_A2062Indicador_NaoCnfCod ;
      private bool[] H00RV2_n2062Indicador_NaoCnfCod ;
      private String[] H00RV2_A427NaoConformidade_Nome ;
      private bool[] H00RV2_n427NaoConformidade_Nome ;
      private int[] H00RV3_A2061Indicador_AreaTrabalhoCod ;
      private bool[] H00RV3_n2061Indicador_AreaTrabalhoCod ;
      private int[] H00RV3_A2062Indicador_NaoCnfCod ;
      private bool[] H00RV3_n2062Indicador_NaoCnfCod ;
      private short[] H00RV3_A2064Indicador_Aplica ;
      private bool[] H00RV3_n2064Indicador_Aplica ;
      private short[] H00RV3_A2063Indicador_Refer ;
      private bool[] H00RV3_n2063Indicador_Refer ;
      private String[] H00RV3_A2059Indicador_Nome ;
      private int[] H00RV3_A2058Indicador_Codigo ;
      private long[] H00RV4_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV48TFIndicador_Refer_Sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV52TFIndicador_Aplica_Sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV74WWIndicadorDS_15_Tfindicador_refer_sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV75WWIndicadorDS_16_Tfindicador_aplica_sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtGridStateCollection_Item ))]
      private IGxCollection AV30ManageFiltersItems ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV32ManageFiltersData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV34Indicador_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV46Indicador_ReferTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV50Indicador_AplicaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV54Indicador_NaoCnfCodTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV2WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtGridStateCollection_Item AV31ManageFiltersItem ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item AV33ManageFiltersDataItem ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV38DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwindicador__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00RV3( IGxContext context ,
                                             short A2063Indicador_Refer ,
                                             IGxCollection AV74WWIndicadorDS_15_Tfindicador_refer_sels ,
                                             short A2064Indicador_Aplica ,
                                             IGxCollection AV75WWIndicadorDS_16_Tfindicador_aplica_sels ,
                                             String AV61WWIndicadorDS_2_Dynamicfiltersselector1 ,
                                             short AV62WWIndicadorDS_3_Dynamicfiltersoperator1 ,
                                             String AV63WWIndicadorDS_4_Indicador_nome1 ,
                                             bool AV64WWIndicadorDS_5_Dynamicfiltersenabled2 ,
                                             String AV65WWIndicadorDS_6_Dynamicfiltersselector2 ,
                                             short AV66WWIndicadorDS_7_Dynamicfiltersoperator2 ,
                                             String AV67WWIndicadorDS_8_Indicador_nome2 ,
                                             bool AV68WWIndicadorDS_9_Dynamicfiltersenabled3 ,
                                             String AV69WWIndicadorDS_10_Dynamicfiltersselector3 ,
                                             short AV70WWIndicadorDS_11_Dynamicfiltersoperator3 ,
                                             String AV71WWIndicadorDS_12_Indicador_nome3 ,
                                             String AV73WWIndicadorDS_14_Tfindicador_nome_sel ,
                                             String AV72WWIndicadorDS_13_Tfindicador_nome ,
                                             int AV74WWIndicadorDS_15_Tfindicador_refer_sels_Count ,
                                             int AV75WWIndicadorDS_16_Tfindicador_aplica_sels_Count ,
                                             int AV76WWIndicadorDS_17_Tfindicador_naocnfcod ,
                                             int AV77WWIndicadorDS_18_Tfindicador_naocnfcod_to ,
                                             String A2059Indicador_Nome ,
                                             int A2062Indicador_NaoCnfCod ,
                                             short AV44OrderedBy ,
                                             bool AV13OrderedDsc ,
                                             int A2061Indicador_AreaTrabalhoCod ,
                                             int AV2WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [16] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [Indicador_AreaTrabalhoCod], [Indicador_NaoCnfCod] AS Indicador_NaoCnfCod, [Indicador_Aplica], [Indicador_Refer], [Indicador_Nome], [Indicador_Codigo]";
         sFromString = " FROM [Indicador] WITH (NOLOCK)";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE ([Indicador_AreaTrabalhoCod] = @AV2WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV61WWIndicadorDS_2_Dynamicfiltersselector1, "INDICADOR_NOME") == 0 ) && ( AV62WWIndicadorDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWIndicadorDS_4_Indicador_nome1)) ) )
         {
            sWhereString = sWhereString + " and ([Indicador_Nome] like @lV63WWIndicadorDS_4_Indicador_nome1)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV61WWIndicadorDS_2_Dynamicfiltersselector1, "INDICADOR_NOME") == 0 ) && ( AV62WWIndicadorDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWIndicadorDS_4_Indicador_nome1)) ) )
         {
            sWhereString = sWhereString + " and ([Indicador_Nome] like '%' + @lV63WWIndicadorDS_4_Indicador_nome1)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV64WWIndicadorDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWIndicadorDS_6_Dynamicfiltersselector2, "INDICADOR_NOME") == 0 ) && ( AV66WWIndicadorDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWIndicadorDS_8_Indicador_nome2)) ) )
         {
            sWhereString = sWhereString + " and ([Indicador_Nome] like @lV67WWIndicadorDS_8_Indicador_nome2)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV64WWIndicadorDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWIndicadorDS_6_Dynamicfiltersselector2, "INDICADOR_NOME") == 0 ) && ( AV66WWIndicadorDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWIndicadorDS_8_Indicador_nome2)) ) )
         {
            sWhereString = sWhereString + " and ([Indicador_Nome] like '%' + @lV67WWIndicadorDS_8_Indicador_nome2)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV68WWIndicadorDS_9_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV69WWIndicadorDS_10_Dynamicfiltersselector3, "INDICADOR_NOME") == 0 ) && ( AV70WWIndicadorDS_11_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWIndicadorDS_12_Indicador_nome3)) ) )
         {
            sWhereString = sWhereString + " and ([Indicador_Nome] like @lV71WWIndicadorDS_12_Indicador_nome3)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV68WWIndicadorDS_9_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV69WWIndicadorDS_10_Dynamicfiltersselector3, "INDICADOR_NOME") == 0 ) && ( AV70WWIndicadorDS_11_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWIndicadorDS_12_Indicador_nome3)) ) )
         {
            sWhereString = sWhereString + " and ([Indicador_Nome] like '%' + @lV71WWIndicadorDS_12_Indicador_nome3)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV73WWIndicadorDS_14_Tfindicador_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWIndicadorDS_13_Tfindicador_nome)) ) )
         {
            sWhereString = sWhereString + " and ([Indicador_Nome] like @lV72WWIndicadorDS_13_Tfindicador_nome)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWIndicadorDS_14_Tfindicador_nome_sel)) )
         {
            sWhereString = sWhereString + " and ([Indicador_Nome] = @AV73WWIndicadorDS_14_Tfindicador_nome_sel)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV74WWIndicadorDS_15_Tfindicador_refer_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV74WWIndicadorDS_15_Tfindicador_refer_sels, "[Indicador_Refer] IN (", ")") + ")";
         }
         if ( AV75WWIndicadorDS_16_Tfindicador_aplica_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV75WWIndicadorDS_16_Tfindicador_aplica_sels, "[Indicador_Aplica] IN (", ")") + ")";
         }
         if ( ! (0==AV76WWIndicadorDS_17_Tfindicador_naocnfcod) )
         {
            sWhereString = sWhereString + " and ([Indicador_NaoCnfCod] >= @AV76WWIndicadorDS_17_Tfindicador_naocnfcod)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! (0==AV77WWIndicadorDS_18_Tfindicador_naocnfcod_to) )
         {
            sWhereString = sWhereString + " and ([Indicador_NaoCnfCod] <= @AV77WWIndicadorDS_18_Tfindicador_naocnfcod_to)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ( AV44OrderedBy == 1 ) && ! AV13OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Indicador_Nome]";
         }
         else if ( ( AV44OrderedBy == 1 ) && ( AV13OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Indicador_Nome] DESC";
         }
         else if ( ( AV44OrderedBy == 2 ) && ! AV13OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Indicador_Refer]";
         }
         else if ( ( AV44OrderedBy == 2 ) && ( AV13OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Indicador_Refer] DESC";
         }
         else if ( ( AV44OrderedBy == 3 ) && ! AV13OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Indicador_Aplica]";
         }
         else if ( ( AV44OrderedBy == 3 ) && ( AV13OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Indicador_Aplica] DESC";
         }
         else if ( ( AV44OrderedBy == 4 ) && ! AV13OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Indicador_NaoCnfCod]";
         }
         else if ( ( AV44OrderedBy == 4 ) && ( AV13OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Indicador_NaoCnfCod] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [Indicador_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_H00RV4( IGxContext context ,
                                             short A2063Indicador_Refer ,
                                             IGxCollection AV74WWIndicadorDS_15_Tfindicador_refer_sels ,
                                             short A2064Indicador_Aplica ,
                                             IGxCollection AV75WWIndicadorDS_16_Tfindicador_aplica_sels ,
                                             String AV61WWIndicadorDS_2_Dynamicfiltersselector1 ,
                                             short AV62WWIndicadorDS_3_Dynamicfiltersoperator1 ,
                                             String AV63WWIndicadorDS_4_Indicador_nome1 ,
                                             bool AV64WWIndicadorDS_5_Dynamicfiltersenabled2 ,
                                             String AV65WWIndicadorDS_6_Dynamicfiltersselector2 ,
                                             short AV66WWIndicadorDS_7_Dynamicfiltersoperator2 ,
                                             String AV67WWIndicadorDS_8_Indicador_nome2 ,
                                             bool AV68WWIndicadorDS_9_Dynamicfiltersenabled3 ,
                                             String AV69WWIndicadorDS_10_Dynamicfiltersselector3 ,
                                             short AV70WWIndicadorDS_11_Dynamicfiltersoperator3 ,
                                             String AV71WWIndicadorDS_12_Indicador_nome3 ,
                                             String AV73WWIndicadorDS_14_Tfindicador_nome_sel ,
                                             String AV72WWIndicadorDS_13_Tfindicador_nome ,
                                             int AV74WWIndicadorDS_15_Tfindicador_refer_sels_Count ,
                                             int AV75WWIndicadorDS_16_Tfindicador_aplica_sels_Count ,
                                             int AV76WWIndicadorDS_17_Tfindicador_naocnfcod ,
                                             int AV77WWIndicadorDS_18_Tfindicador_naocnfcod_to ,
                                             String A2059Indicador_Nome ,
                                             int A2062Indicador_NaoCnfCod ,
                                             short AV44OrderedBy ,
                                             bool AV13OrderedDsc ,
                                             int A2061Indicador_AreaTrabalhoCod ,
                                             int AV2WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [11] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [Indicador] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Indicador_AreaTrabalhoCod] = @AV2WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV61WWIndicadorDS_2_Dynamicfiltersselector1, "INDICADOR_NOME") == 0 ) && ( AV62WWIndicadorDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWIndicadorDS_4_Indicador_nome1)) ) )
         {
            sWhereString = sWhereString + " and ([Indicador_Nome] like @lV63WWIndicadorDS_4_Indicador_nome1)";
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV61WWIndicadorDS_2_Dynamicfiltersselector1, "INDICADOR_NOME") == 0 ) && ( AV62WWIndicadorDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWIndicadorDS_4_Indicador_nome1)) ) )
         {
            sWhereString = sWhereString + " and ([Indicador_Nome] like '%' + @lV63WWIndicadorDS_4_Indicador_nome1)";
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( AV64WWIndicadorDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWIndicadorDS_6_Dynamicfiltersselector2, "INDICADOR_NOME") == 0 ) && ( AV66WWIndicadorDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWIndicadorDS_8_Indicador_nome2)) ) )
         {
            sWhereString = sWhereString + " and ([Indicador_Nome] like @lV67WWIndicadorDS_8_Indicador_nome2)";
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV64WWIndicadorDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWIndicadorDS_6_Dynamicfiltersselector2, "INDICADOR_NOME") == 0 ) && ( AV66WWIndicadorDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWIndicadorDS_8_Indicador_nome2)) ) )
         {
            sWhereString = sWhereString + " and ([Indicador_Nome] like '%' + @lV67WWIndicadorDS_8_Indicador_nome2)";
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV68WWIndicadorDS_9_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV69WWIndicadorDS_10_Dynamicfiltersselector3, "INDICADOR_NOME") == 0 ) && ( AV70WWIndicadorDS_11_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWIndicadorDS_12_Indicador_nome3)) ) )
         {
            sWhereString = sWhereString + " and ([Indicador_Nome] like @lV71WWIndicadorDS_12_Indicador_nome3)";
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV68WWIndicadorDS_9_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV69WWIndicadorDS_10_Dynamicfiltersselector3, "INDICADOR_NOME") == 0 ) && ( AV70WWIndicadorDS_11_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWIndicadorDS_12_Indicador_nome3)) ) )
         {
            sWhereString = sWhereString + " and ([Indicador_Nome] like '%' + @lV71WWIndicadorDS_12_Indicador_nome3)";
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV73WWIndicadorDS_14_Tfindicador_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWIndicadorDS_13_Tfindicador_nome)) ) )
         {
            sWhereString = sWhereString + " and ([Indicador_Nome] like @lV72WWIndicadorDS_13_Tfindicador_nome)";
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWIndicadorDS_14_Tfindicador_nome_sel)) )
         {
            sWhereString = sWhereString + " and ([Indicador_Nome] = @AV73WWIndicadorDS_14_Tfindicador_nome_sel)";
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( AV74WWIndicadorDS_15_Tfindicador_refer_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV74WWIndicadorDS_15_Tfindicador_refer_sels, "[Indicador_Refer] IN (", ")") + ")";
         }
         if ( AV75WWIndicadorDS_16_Tfindicador_aplica_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV75WWIndicadorDS_16_Tfindicador_aplica_sels, "[Indicador_Aplica] IN (", ")") + ")";
         }
         if ( ! (0==AV76WWIndicadorDS_17_Tfindicador_naocnfcod) )
         {
            sWhereString = sWhereString + " and ([Indicador_NaoCnfCod] >= @AV76WWIndicadorDS_17_Tfindicador_naocnfcod)";
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( ! (0==AV77WWIndicadorDS_18_Tfindicador_naocnfcod_to) )
         {
            sWhereString = sWhereString + " and ([Indicador_NaoCnfCod] <= @AV77WWIndicadorDS_18_Tfindicador_naocnfcod_to)";
         }
         else
         {
            GXv_int5[10] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV44OrderedBy == 1 ) && ! AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV44OrderedBy == 1 ) && ( AV13OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV44OrderedBy == 2 ) && ! AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV44OrderedBy == 2 ) && ( AV13OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV44OrderedBy == 3 ) && ! AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV44OrderedBy == 3 ) && ( AV13OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV44OrderedBy == 4 ) && ! AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV44OrderedBy == 4 ) && ( AV13OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 1 :
                     return conditional_H00RV3(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (short)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (short)dynConstraints[23] , (bool)dynConstraints[24] , (int)dynConstraints[25] , (int)dynConstraints[26] );
               case 2 :
                     return conditional_H00RV4(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (short)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (short)dynConstraints[23] , (bool)dynConstraints[24] , (int)dynConstraints[25] , (int)dynConstraints[26] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00RV2 ;
          prmH00RV2 = new Object[] {
          } ;
          Object[] prmH00RV3 ;
          prmH00RV3 = new Object[] {
          new Object[] {"@AV2WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV63WWIndicadorDS_4_Indicador_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV63WWIndicadorDS_4_Indicador_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV67WWIndicadorDS_8_Indicador_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV67WWIndicadorDS_8_Indicador_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV71WWIndicadorDS_12_Indicador_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV71WWIndicadorDS_12_Indicador_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV72WWIndicadorDS_13_Tfindicador_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV73WWIndicadorDS_14_Tfindicador_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV76WWIndicadorDS_17_Tfindicador_naocnfcod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV77WWIndicadorDS_18_Tfindicador_naocnfcod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00RV4 ;
          prmH00RV4 = new Object[] {
          new Object[] {"@AV2WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV63WWIndicadorDS_4_Indicador_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV63WWIndicadorDS_4_Indicador_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV67WWIndicadorDS_8_Indicador_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV67WWIndicadorDS_8_Indicador_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV71WWIndicadorDS_12_Indicador_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV71WWIndicadorDS_12_Indicador_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV72WWIndicadorDS_13_Tfindicador_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV73WWIndicadorDS_14_Tfindicador_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV76WWIndicadorDS_17_Tfindicador_naocnfcod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV77WWIndicadorDS_18_Tfindicador_naocnfcod_to",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00RV2", "SELECT [NaoConformidade_Codigo] AS Indicador_NaoCnfCod, [NaoConformidade_Nome] FROM [NaoConformidade] WITH (NOLOCK) ORDER BY [NaoConformidade_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00RV2,0,0,true,false )
             ,new CursorDef("H00RV3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00RV3,11,0,true,false )
             ,new CursorDef("H00RV4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00RV4,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((short[]) buf[4])[0] = rslt.getShort(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((short[]) buf[6])[0] = rslt.getShort(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 50) ;
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                return;
             case 2 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                return;
       }
    }

 }

}
