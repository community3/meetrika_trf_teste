/*
               File: type_SdtGAMUserFilter
        Description: GAMUserFilter
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/30/2020 1:32:19.5
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMUserFilter : GxUserType, IGxExternalObject
   {
      public SdtGAMUserFilter( )
      {
         initialize();
      }

      public SdtGAMUserFilter( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMUserFilter_externalReference == null )
         {
            GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
         }
         returntostring = "";
         returntostring = (String)(GAMUserFilter_externalReference.ToString());
         return returntostring ;
      }

      public String gxTpr_Guid
      {
         get {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            return GAMUserFilter_externalReference.GUID ;
         }

         set {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            GAMUserFilter_externalReference.GUID = value;
         }

      }

      public String gxTpr_Namespace
      {
         get {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            return GAMUserFilter_externalReference.NameSpace ;
         }

         set {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            GAMUserFilter_externalReference.NameSpace = value;
         }

      }

      public String gxTpr_Authenticationtypename
      {
         get {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            return GAMUserFilter_externalReference.AuthenticationTypeName ;
         }

         set {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            GAMUserFilter_externalReference.AuthenticationTypeName = value;
         }

      }

      public String gxTpr_Name
      {
         get {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            return GAMUserFilter_externalReference.Name ;
         }

         set {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            GAMUserFilter_externalReference.Name = value;
         }

      }

      public String gxTpr_Email
      {
         get {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            return GAMUserFilter_externalReference.EMail ;
         }

         set {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            GAMUserFilter_externalReference.EMail = value;
         }

      }

      public String gxTpr_Names
      {
         get {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            return GAMUserFilter_externalReference.Names ;
         }

         set {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            GAMUserFilter_externalReference.Names = value;
         }

      }

      public String gxTpr_Externalid
      {
         get {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            return GAMUserFilter_externalReference.ExternalId ;
         }

         set {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            GAMUserFilter_externalReference.ExternalId = value;
         }

      }

      public int gxTpr_Securitypolicyid
      {
         get {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            return GAMUserFilter_externalReference.SecurityPolicyId ;
         }

         set {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            GAMUserFilter_externalReference.SecurityPolicyId = value;
         }

      }

      public String gxTpr_Gender
      {
         get {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            return GAMUserFilter_externalReference.Gender ;
         }

         set {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            GAMUserFilter_externalReference.Gender = value;
         }

      }

      public String gxTpr_Isblocked
      {
         get {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            return GAMUserFilter_externalReference.IsBlocked ;
         }

         set {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            GAMUserFilter_externalReference.IsBlocked = value;
         }

      }

      public String gxTpr_Isactive
      {
         get {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            return GAMUserFilter_externalReference.IsActive ;
         }

         set {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            GAMUserFilter_externalReference.IsActive = value;
         }

      }

      public String gxTpr_Isdeleted
      {
         get {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            return GAMUserFilter_externalReference.IsDeleted ;
         }

         set {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            GAMUserFilter_externalReference.IsDeleted = value;
         }

      }

      public String gxTpr_Isenabledinrepository
      {
         get {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            return GAMUserFilter_externalReference.IsEnabledInRepository ;
         }

         set {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            GAMUserFilter_externalReference.IsEnabledInRepository = value;
         }

      }

      public bool gxTpr_Loadcustomattributes
      {
         get {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            return GAMUserFilter_externalReference.LoadCustomAttributes ;
         }

         set {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            GAMUserFilter_externalReference.LoadCustomAttributes = value;
         }

      }

      public long gxTpr_Roleid
      {
         get {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            return GAMUserFilter_externalReference.RoleId ;
         }

         set {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            GAMUserFilter_externalReference.RoleId = value;
         }

      }

      public bool gxTpr_Searchrolesinherited
      {
         get {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            return GAMUserFilter_externalReference.SearchRolesInherited ;
         }

         set {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            GAMUserFilter_externalReference.SearchRolesInherited = value;
         }

      }

      public String gxTpr_Searchable
      {
         get {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            return GAMUserFilter_externalReference.Searchable ;
         }

         set {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            GAMUserFilter_externalReference.Searchable = value;
         }

      }

      public bool gxTpr_Returnanonymoususer
      {
         get {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            return GAMUserFilter_externalReference.ReturnAnonymousUser ;
         }

         set {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            GAMUserFilter_externalReference.ReturnAnonymousUser = value;
         }

      }

      public int gxTpr_Start
      {
         get {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            return GAMUserFilter_externalReference.Start ;
         }

         set {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            GAMUserFilter_externalReference.Start = value;
         }

      }

      public int gxTpr_Limit
      {
         get {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            return GAMUserFilter_externalReference.Limit ;
         }

         set {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            GAMUserFilter_externalReference.Limit = value;
         }

      }

      public IGxCollection gxTpr_Roles
      {
         get {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            IGxCollection intValue ;
            intValue = new GxExternalCollection();
            System.Collections.Generic.List<System.Int64> externalParm0 ;
            externalParm0 = GAMUserFilter_externalReference.Roles;
            intValue.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<System.Int64>), externalParm0);
            return intValue ;
         }

         set {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            IGxCollection intValue ;
            System.Collections.Generic.List<System.Int64> externalParm1 ;
            intValue = value;
            externalParm1 = (System.Collections.Generic.List<System.Int64>)CollectionUtils.ConvertToExternal( typeof(System.Collections.Generic.List<System.Int64>), intValue.ExternalInstance);
            GAMUserFilter_externalReference.Roles = externalParm1;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMUserFilter_externalReference == null )
            {
               GAMUserFilter_externalReference = new Artech.Security.GAMUserFilter(context);
            }
            return GAMUserFilter_externalReference ;
         }

         set {
            GAMUserFilter_externalReference = (Artech.Security.GAMUserFilter)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMUserFilter GAMUserFilter_externalReference=null ;
   }

}
