/*
               File: WP_RelatorioComparacaoDemandas_OLD
        Description: Relat�rio de Compara��o entre Demandas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/19/2020 1:25:38.92
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_relatoriocomparacaodemandas_old : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_relatoriocomparacaodemandas_old( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_relatoriocomparacaodemandas_old( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavContagemresultado_contratadacod = new GXCombobox();
         dynavContagemresultado_servicogrupo = new GXCombobox();
         dynavContagemresultado_servico = new GXCombobox();
         dynavContagemresultado_cntadaosvinc = new GXCombobox();
         dynavContagemresultado_sergrupovinc = new GXCombobox();
         dynavContagemresultado_codsrvvnc = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavContagemresultado_statusdmn1 = new GXCombobox();
         cmbavContagemresultado_statuscnt1 = new GXCombobox();
         dynavContagemresultado_sistemacod1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavContagemresultado_statusdmn2 = new GXCombobox();
         cmbavContagemresultado_statuscnt2 = new GXCombobox();
         dynavContagemresultado_sistemacod2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         cmbavContagemresultado_statusdmn3 = new GXCombobox();
         cmbavContagemresultado_statuscnt3 = new GXCombobox();
         dynavContagemresultado_sistemacod3 = new GXCombobox();
         cmbavContagemresultado_contratadatipofab = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_CONTRATADACOD") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_CONTRATADACODTR2( AV6WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_SERVICOGRUPO") == 0 )
            {
               AV15ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15ContagemResultado_ContratadaCod), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_SERVICOGRUPOTR2( AV15ContagemResultado_ContratadaCod) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_SERVICO") == 0 )
            {
               AV15ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15ContagemResultado_ContratadaCod), 6, 0)));
               AV16ContagemResultado_ServicoGrupo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContagemResultado_ServicoGrupo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16ContagemResultado_ServicoGrupo), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_SERVICOTR2( AV15ContagemResultado_ContratadaCod, AV16ContagemResultado_ServicoGrupo) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_CNTADAOSVINC") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_CNTADAOSVINCTR2( AV6WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_SERGRUPOVINC") == 0 )
            {
               AV18ContagemResultado_CntadaOsVinc = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultado_CntadaOsVinc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ContagemResultado_CntadaOsVinc), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_SERGRUPOVINCTR2( AV18ContagemResultado_CntadaOsVinc) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_CODSRVVNC") == 0 )
            {
               AV18ContagemResultado_CntadaOsVinc = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultado_CntadaOsVinc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ContagemResultado_CntadaOsVinc), 6, 0)));
               AV19ContagemResultado_SerGrupoVinc = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultado_SerGrupoVinc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19ContagemResultado_SerGrupoVinc), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_CODSRVVNCTR2( AV18ContagemResultado_CntadaOsVinc, AV19ContagemResultado_SerGrupoVinc) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_SISTEMACOD1") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_SISTEMACOD1TR2( AV6WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_SISTEMACOD2") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_SISTEMACOD2TR2( AV6WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_SISTEMACOD3") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_SISTEMACOD3TR2( AV6WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_187 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_187_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_187_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV21DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector1", AV21DynamicFiltersSelector1);
               AV25DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersEnabled2", AV25DynamicFiltersEnabled2);
               AV26DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector2", AV26DynamicFiltersSelector2);
               AV30DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersEnabled3", AV30DynamicFiltersEnabled3);
               AV31DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31DynamicFiltersSelector3", AV31DynamicFiltersSelector3);
               AV192Pgmname = GetNextPar( );
               AV13Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Contratada_AreaTrabalhoCod), 6, 0)));
               AV15ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15ContagemResultado_ContratadaCod), 6, 0)));
               AV16ContagemResultado_ServicoGrupo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContagemResultado_ServicoGrupo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16ContagemResultado_ServicoGrupo), 6, 0)));
               AV17ContagemResultado_Servico = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContagemResultado_Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17ContagemResultado_Servico), 6, 0)));
               AV18ContagemResultado_CntadaOsVinc = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultado_CntadaOsVinc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ContagemResultado_CntadaOsVinc), 6, 0)));
               AV19ContagemResultado_SerGrupoVinc = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultado_SerGrupoVinc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19ContagemResultado_SerGrupoVinc), 6, 0)));
               AV20ContagemResultado_CodSrvVnc = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContagemResultado_CodSrvVnc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20ContagemResultado_CodSrvVnc), 6, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV36DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36DynamicFiltersIgnoreFirst", AV36DynamicFiltersIgnoreFirst);
               AV48ContagemResultado_DemandaFM1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ContagemResultado_DemandaFM1", AV48ContagemResultado_DemandaFM1);
               AV24ContagemResultado_DataDmn_To1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultado_DataDmn_To1", context.localUtil.Format(AV24ContagemResultado_DataDmn_To1, "99/99/99"));
               AV23ContagemResultado_DataDmn1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemResultado_DataDmn1", context.localUtil.Format(AV23ContagemResultado_DataDmn1, "99/99/99"));
               AV50ContagemResultado_DataEntregaReal_To1 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ContagemResultado_DataEntregaReal_To1", context.localUtil.TToC( AV50ContagemResultado_DataEntregaReal_To1, 8, 5, 0, 3, "/", ":", " "));
               AV49ContagemResultado_DataEntregaReal1 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ContagemResultado_DataEntregaReal1", context.localUtil.TToC( AV49ContagemResultado_DataEntregaReal1, 8, 5, 0, 3, "/", ":", " "));
               AV51ContagemResultado_StatusDmn1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ContagemResultado_StatusDmn1", AV51ContagemResultado_StatusDmn1);
               AV52ContagemResultado_StatusCnt1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ContagemResultado_StatusCnt1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52ContagemResultado_StatusCnt1), 2, 0)));
               AV53ContagemResultado_Descricao1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ContagemResultado_Descricao1", AV53ContagemResultado_Descricao1);
               AV54ContagemResultado_SistemaCod1 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ContagemResultado_SistemaCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54ContagemResultado_SistemaCod1), 6, 0)));
               AV22DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator1), 4, 0)));
               AV55ContagemResultado_Agrupador1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55ContagemResultado_Agrupador1", AV55ContagemResultado_Agrupador1);
               AV35DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35DynamicFiltersRemoving", AV35DynamicFiltersRemoving);
               AV56ContagemResultado_DemandaFM2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ContagemResultado_DemandaFM2", AV56ContagemResultado_DemandaFM2);
               AV29ContagemResultado_DataDmn_To2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContagemResultado_DataDmn_To2", context.localUtil.Format(AV29ContagemResultado_DataDmn_To2, "99/99/99"));
               AV28ContagemResultado_DataDmn2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ContagemResultado_DataDmn2", context.localUtil.Format(AV28ContagemResultado_DataDmn2, "99/99/99"));
               AV58ContagemResultado_DataEntregaReal_To2 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58ContagemResultado_DataEntregaReal_To2", context.localUtil.TToC( AV58ContagemResultado_DataEntregaReal_To2, 8, 5, 0, 3, "/", ":", " "));
               AV57ContagemResultado_DataEntregaReal2 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ContagemResultado_DataEntregaReal2", context.localUtil.TToC( AV57ContagemResultado_DataEntregaReal2, 8, 5, 0, 3, "/", ":", " "));
               AV59ContagemResultado_StatusDmn2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59ContagemResultado_StatusDmn2", AV59ContagemResultado_StatusDmn2);
               AV60ContagemResultado_StatusCnt2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60ContagemResultado_StatusCnt2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60ContagemResultado_StatusCnt2), 2, 0)));
               AV61ContagemResultado_Descricao2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ContagemResultado_Descricao2", AV61ContagemResultado_Descricao2);
               AV62ContagemResultado_SistemaCod2 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62ContagemResultado_SistemaCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62ContagemResultado_SistemaCod2), 6, 0)));
               AV27DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator2), 4, 0)));
               AV63ContagemResultado_Agrupador2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ContagemResultado_Agrupador2", AV63ContagemResultado_Agrupador2);
               AV64ContagemResultado_DemandaFM3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64ContagemResultado_DemandaFM3", AV64ContagemResultado_DemandaFM3);
               AV34ContagemResultado_DataDmn_To3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContagemResultado_DataDmn_To3", context.localUtil.Format(AV34ContagemResultado_DataDmn_To3, "99/99/99"));
               AV33ContagemResultado_DataDmn3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContagemResultado_DataDmn3", context.localUtil.Format(AV33ContagemResultado_DataDmn3, "99/99/99"));
               AV66ContagemResultado_DataEntregaReal_To3 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ContagemResultado_DataEntregaReal_To3", context.localUtil.TToC( AV66ContagemResultado_DataEntregaReal_To3, 8, 5, 0, 3, "/", ":", " "));
               AV65ContagemResultado_DataEntregaReal3 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ContagemResultado_DataEntregaReal3", context.localUtil.TToC( AV65ContagemResultado_DataEntregaReal3, 8, 5, 0, 3, "/", ":", " "));
               AV67ContagemResultado_StatusDmn3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ContagemResultado_StatusDmn3", AV67ContagemResultado_StatusDmn3);
               AV68ContagemResultado_StatusCnt3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ContagemResultado_StatusCnt3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68ContagemResultado_StatusCnt3), 2, 0)));
               AV69ContagemResultado_Descricao3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ContagemResultado_Descricao3", AV69ContagemResultado_Descricao3);
               AV70ContagemResultado_SistemaCod3 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70ContagemResultado_SistemaCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV70ContagemResultado_SistemaCod3), 6, 0)));
               AV32DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32DynamicFiltersOperator3), 4, 0)));
               AV71ContagemResultado_Agrupador3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ContagemResultado_Agrupador3", AV71ContagemResultado_Agrupador3);
               A457ContagemResultado_Demanda = GetNextPar( );
               n457ContagemResultado_Demanda = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A457ContagemResultado_Demanda", A457ContagemResultado_Demanda);
               A52Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n52Contratada_AreaTrabalhoCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               A490ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n490ContagemResultado_ContratadaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A490ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0)));
               A764ContagemResultado_ServicoGrupo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n764ContagemResultado_ServicoGrupo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A764ContagemResultado_ServicoGrupo", StringUtil.LTrim( StringUtil.Str( (decimal)(A764ContagemResultado_ServicoGrupo), 6, 0)));
               A601ContagemResultado_Servico = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n601ContagemResultado_Servico = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A601ContagemResultado_Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(A601ContagemResultado_Servico), 6, 0)));
               A602ContagemResultado_OSVinculada = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n602ContagemResultado_OSVinculada = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A602ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(A602ContagemResultado_OSVinculada), 6, 0)));
               A493ContagemResultado_DemandaFM = GetNextPar( );
               n493ContagemResultado_DemandaFM = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A493ContagemResultado_DemandaFM", A493ContagemResultado_DemandaFM);
               A471ContagemResultado_DataDmn = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A471ContagemResultado_DataDmn", context.localUtil.Format(A471ContagemResultado_DataDmn, "99/99/99"));
               A2017ContagemResultado_DataEntregaReal = context.localUtil.ParseDTimeParm( GetNextPar( ));
               n2017ContagemResultado_DataEntregaReal = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2017ContagemResultado_DataEntregaReal", context.localUtil.TToC( A2017ContagemResultado_DataEntregaReal, 8, 5, 0, 3, "/", ":", " "));
               A484ContagemResultado_StatusDmn = GetNextPar( );
               n484ContagemResultado_StatusDmn = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A484ContagemResultado_StatusDmn", A484ContagemResultado_StatusDmn);
               A494ContagemResultado_Descricao = GetNextPar( );
               n494ContagemResultado_Descricao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A494ContagemResultado_Descricao", A494ContagemResultado_Descricao);
               A489ContagemResultado_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n489ContagemResultado_SistemaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A489ContagemResultado_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A489ContagemResultado_SistemaCod), 6, 0)));
               A1046ContagemResultado_Agrupador = GetNextPar( );
               n1046ContagemResultado_Agrupador = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1046ContagemResultado_Agrupador", A1046ContagemResultado_Agrupador);
               A456ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
               A509ContagemrResultado_SistemaSigla = GetNextPar( );
               n509ContagemrResultado_SistemaSigla = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A509ContagemrResultado_SistemaSigla", A509ContagemrResultado_SistemaSigla);
               A1326ContagemResultado_ContratadaTipoFab = GetNextPar( );
               n1326ContagemResultado_ContratadaTipoFab = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1326ContagemResultado_ContratadaTipoFab", A1326ContagemResultado_ContratadaTipoFab);
               A682ContagemResultado_PFBFMUltima = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A682ContagemResultado_PFBFMUltima", StringUtil.LTrim( StringUtil.Str( A682ContagemResultado_PFBFMUltima, 14, 5)));
               A684ContagemResultado_PFBFSUltima = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A684ContagemResultado_PFBFSUltima", StringUtil.LTrim( StringUtil.Str( A684ContagemResultado_PFBFSUltima, 14, 5)));
               AV14ContagemResultado_OSVinculada = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_osvinculada_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV14ContagemResultado_OSVinculada), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV21DynamicFiltersSelector1, AV25DynamicFiltersEnabled2, AV26DynamicFiltersSelector2, AV30DynamicFiltersEnabled3, AV31DynamicFiltersSelector3, AV192Pgmname, AV13Contratada_AreaTrabalhoCod, AV15ContagemResultado_ContratadaCod, AV16ContagemResultado_ServicoGrupo, AV17ContagemResultado_Servico, AV18ContagemResultado_CntadaOsVinc, AV19ContagemResultado_SerGrupoVinc, AV20ContagemResultado_CodSrvVnc, AV10GridState, AV36DynamicFiltersIgnoreFirst, AV48ContagemResultado_DemandaFM1, AV24ContagemResultado_DataDmn_To1, AV23ContagemResultado_DataDmn1, AV50ContagemResultado_DataEntregaReal_To1, AV49ContagemResultado_DataEntregaReal1, AV51ContagemResultado_StatusDmn1, AV52ContagemResultado_StatusCnt1, AV53ContagemResultado_Descricao1, AV54ContagemResultado_SistemaCod1, AV22DynamicFiltersOperator1, AV55ContagemResultado_Agrupador1, AV35DynamicFiltersRemoving, AV56ContagemResultado_DemandaFM2, AV29ContagemResultado_DataDmn_To2, AV28ContagemResultado_DataDmn2, AV58ContagemResultado_DataEntregaReal_To2, AV57ContagemResultado_DataEntregaReal2, AV59ContagemResultado_StatusDmn2, AV60ContagemResultado_StatusCnt2, AV61ContagemResultado_Descricao2, AV62ContagemResultado_SistemaCod2, AV27DynamicFiltersOperator2, AV63ContagemResultado_Agrupador2, AV64ContagemResultado_DemandaFM3, AV34ContagemResultado_DataDmn_To3, AV33ContagemResultado_DataDmn3, AV66ContagemResultado_DataEntregaReal_To3, AV65ContagemResultado_DataEntregaReal3, AV67ContagemResultado_StatusDmn3, AV68ContagemResultado_StatusCnt3, AV69ContagemResultado_Descricao3, AV70ContagemResultado_SistemaCod3, AV32DynamicFiltersOperator3, AV71ContagemResultado_Agrupador3, A457ContagemResultado_Demanda, A52Contratada_AreaTrabalhoCod, AV6WWPContext, A490ContagemResultado_ContratadaCod, A764ContagemResultado_ServicoGrupo, A601ContagemResultado_Servico, A602ContagemResultado_OSVinculada, A493ContagemResultado_DemandaFM, A471ContagemResultado_DataDmn, A2017ContagemResultado_DataEntregaReal, A484ContagemResultado_StatusDmn, A494ContagemResultado_Descricao, A489ContagemResultado_SistemaCod, A1046ContagemResultado_Agrupador, A456ContagemResultado_Codigo, A509ContagemrResultado_SistemaSigla, A1326ContagemResultado_ContratadaTipoFab, A682ContagemResultado_PFBFMUltima, A684ContagemResultado_PFBFSUltima, AV14ContagemResultado_OSVinculada) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PATR2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTTR2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20204191253946");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_relatoriocomparacaodemandas_old.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_187", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_187), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV45GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV46GridPageCount), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV192Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV36DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV35DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DEMANDA", A457ContagemResultado_Demanda);
         GxWebStd.gx_hidden_field( context, "CONTRATADA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_SERVICOGRUPO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A764ContagemResultado_ServicoGrupo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_SERVICO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A601ContagemResultado_Servico), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_OSVINCULADA", StringUtil.LTrim( StringUtil.NToC( (decimal)(A602ContagemResultado_OSVinculada), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DEMANDAFM", A493ContagemResultado_DemandaFM);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DATADMN", context.localUtil.DToC( A471ContagemResultado_DataDmn, 0, "/"));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DATAENTREGAREAL", context.localUtil.TToC( A2017ContagemResultado_DataEntregaReal, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_STATUSDMN", StringUtil.RTrim( A484ContagemResultado_StatusDmn));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DESCRICAO", A494ContagemResultado_Descricao);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A489ContagemResultado_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_AGRUPADOR", StringUtil.RTrim( A1046ContagemResultado_Agrupador));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRRESULTADO_SISTEMASIGLA", StringUtil.RTrim( A509ContagemrResultado_SistemaSigla));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTRATADATIPOFAB", StringUtil.RTrim( A1326ContagemResultado_ContratadaTipoFab));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFBFMULTIMA", StringUtil.LTrim( StringUtil.NToC( A682ContagemResultado_PFBFMUltima, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFBFSULTIMA", StringUtil.LTrim( StringUtil.NToC( A684ContagemResultado_PFBFSUltima, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "vTODAY", context.localUtil.DToC( Gx_date, 0, "/"));
         GxWebStd.gx_hidden_field( context, "vEXCELFILENAME", AV98ExcelFilename);
         GxWebStd.gx_hidden_field( context, "vERRORMESSAGE", AV99ErrorMessage);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WETR2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTTR2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_relatoriocomparacaodemandas_old.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WP_RelatorioComparacaoDemandas_OLD" ;
      }

      public override String GetPgmdesc( )
      {
         return "Relat�rio de Compara��o entre Demandas" ;
      }

      protected void WBTR0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_TR2( true) ;
         }
         else
         {
            wb_table1_2_TR2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_TR2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 205,'',false,'" + sGXsfl_187_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV25DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(205, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,205);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 206,'',false,'" + sGXsfl_187_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV30DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(206, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,206);\"");
         }
         wbLoad = true;
      }

      protected void STARTTR2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Relat�rio de Compara��o entre Demandas", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPTR0( ) ;
      }

      protected void WSTR2( )
      {
         STARTTR2( ) ;
         EVTTR2( ) ;
      }

      protected void EVTTR2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11TR2 */
                              E11TR2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12TR2 */
                              E12TR2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOEXPORT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13TR2 */
                              E13TR2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14TR2 */
                              E14TR2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15TR2 */
                              E15TR2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16TR2 */
                              E16TR2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17TR2 */
                              E17TR2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSOPERATOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18TR2 */
                              E18TR2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19TR2 */
                              E19TR2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20TR2 */
                              E20TR2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSOPERATOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21TR2 */
                              E21TR2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22TR2 */
                              E22TR2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSOPERATOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23TR2 */
                              E23TR2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_187_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_187_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_187_idx), 4, 0)), 4, "0");
                              SubsflControlProps_1872( ) ;
                              AV47Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV47Display)) ? AV189Display_GXI : context.convertURL( context.PathToRelativeUrl( AV47Display))));
                              AV88ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavContagemresultado_codigo_Internalname), ",", "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV88ContagemResultado_Codigo), 6, 0)));
                              AV89ContagemResultado_DemandaFM = cgiGet( edtavContagemresultado_demandafm_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_demandafm_Internalname, AV89ContagemResultado_DemandaFM);
                              AV90ContagemResultado_Demanda = StringUtil.Upper( cgiGet( edtavContagemresultado_demanda_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_demanda_Internalname, AV90ContagemResultado_Demanda);
                              AV91ContagemResultado_DataDmn = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_datadmn_Internalname), 0));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_datadmn_Internalname, context.localUtil.Format(AV91ContagemResultado_DataDmn, "99/99/99"));
                              AV92ContagemResultado_DataEntregaReal = context.localUtil.CToT( cgiGet( edtavContagemresultado_dataentregareal_Internalname), 0);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_dataentregareal_Internalname, context.localUtil.TToC( AV92ContagemResultado_DataEntregaReal, 8, 5, 0, 3, "/", ":", " "));
                              AV93ContagemResultado_Descricao = cgiGet( edtavContagemresultado_descricao_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_descricao_Internalname, AV93ContagemResultado_Descricao);
                              AV94ContagemrResultado_SistemaSigla = StringUtil.Upper( cgiGet( edtavContagemrresultado_sistemasigla_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemrresultado_sistemasigla_Internalname, AV94ContagemrResultado_SistemaSigla);
                              AV14ContagemResultado_OSVinculada = (int)(context.localUtil.CToN( cgiGet( edtavContagemresultado_osvinculada_Internalname), ",", "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_osvinculada_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV14ContagemResultado_OSVinculada), 6, 0)));
                              cmbavContagemresultado_contratadatipofab.Name = cmbavContagemresultado_contratadatipofab_Internalname;
                              cmbavContagemresultado_contratadatipofab.CurrentValue = cgiGet( cmbavContagemresultado_contratadatipofab_Internalname);
                              AV95ContagemResultado_ContratadaTipoFab = cgiGet( cmbavContagemresultado_contratadatipofab_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavContagemresultado_contratadatipofab_Internalname, AV95ContagemResultado_ContratadaTipoFab);
                              AV96ContagemResultado_PFBFMUltima = context.localUtil.CToN( cgiGet( edtavContagemresultado_pfbfmultima_Internalname), ",", ".");
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_pfbfmultima_Internalname, StringUtil.LTrim( StringUtil.Str( AV96ContagemResultado_PFBFMUltima, 14, 5)));
                              AV97ContagemResultado_PFBFSUltima = context.localUtil.CToN( cgiGet( edtavContagemresultado_pfbfsultima_Internalname), ",", ".");
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_pfbfsultima_Internalname, StringUtil.LTrim( StringUtil.Str( AV97ContagemResultado_PFBFSUltima, 14, 5)));
                              AV87Esforco = context.localUtil.CToN( cgiGet( edtavEsforco_Internalname), ",", ".");
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavEsforco_Internalname, StringUtil.LTrim( StringUtil.Str( AV87Esforco, 14, 5)));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E24TR2 */
                                    E24TR2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E25TR2 */
                                    E25TR2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E26TR2 */
                                    E26TR2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WETR2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PATR2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavContagemresultado_contratadacod.Name = "vCONTAGEMRESULTADO_CONTRATADACOD";
            dynavContagemresultado_contratadacod.WebTags = "";
            dynavContagemresultado_servicogrupo.Name = "vCONTAGEMRESULTADO_SERVICOGRUPO";
            dynavContagemresultado_servicogrupo.WebTags = "";
            dynavContagemresultado_servico.Name = "vCONTAGEMRESULTADO_SERVICO";
            dynavContagemresultado_servico.WebTags = "";
            dynavContagemresultado_cntadaosvinc.Name = "vCONTAGEMRESULTADO_CNTADAOSVINC";
            dynavContagemresultado_cntadaosvinc.WebTags = "";
            dynavContagemresultado_sergrupovinc.Name = "vCONTAGEMRESULTADO_SERGRUPOVINC";
            dynavContagemresultado_sergrupovinc.WebTags = "";
            dynavContagemresultado_codsrvvnc.Name = "vCONTAGEMRESULTADO_CODSRVVNC";
            dynavContagemresultado_codsrvvnc.WebTags = "";
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTAGEMRESULTADO_DEMANDAFM", "OS / OS Refer�ncia", 0);
            cmbavDynamicfiltersselector1.addItem("CONTAGEMRESULTADO_DATADMN", "Data da Demanda", 0);
            cmbavDynamicfiltersselector1.addItem("CONTAGEMRESULTADO_DATAENTREGAREAL", "Data de Entrega", 0);
            cmbavDynamicfiltersselector1.addItem("CONTAGEMRESULTADO_STATUSDMN", "Status da Demanda", 0);
            cmbavDynamicfiltersselector1.addItem("CONTAGEMRESULTADO_STATUSCNT", "Status do Servi�o", 0);
            cmbavDynamicfiltersselector1.addItem("CONTAGEMRESULTADO_DESCRICAO", "T�tulo", 0);
            cmbavDynamicfiltersselector1.addItem("CONTAGEMRESULTADO_SISTEMACOD", "Sistema", 0);
            cmbavDynamicfiltersselector1.addItem("CONTAGEMRESULTADO_AGRUPADOR", "Agrupador", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV21DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV21DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector1", AV21DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Igual", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("2", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV22DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator1), 4, 0)));
            }
            cmbavContagemresultado_statusdmn1.Name = "vCONTAGEMRESULTADO_STATUSDMN1";
            cmbavContagemresultado_statusdmn1.WebTags = "";
            cmbavContagemresultado_statusdmn1.addItem("", "Todos", 0);
            cmbavContagemresultado_statusdmn1.addItem("B", "Stand by", 0);
            cmbavContagemresultado_statusdmn1.addItem("S", "Solicitada", 0);
            cmbavContagemresultado_statusdmn1.addItem("E", "Em An�lise", 0);
            cmbavContagemresultado_statusdmn1.addItem("A", "Em execu��o", 0);
            cmbavContagemresultado_statusdmn1.addItem("R", "Resolvida", 0);
            cmbavContagemresultado_statusdmn1.addItem("C", "Conferida", 0);
            cmbavContagemresultado_statusdmn1.addItem("D", "Rejeitada", 0);
            cmbavContagemresultado_statusdmn1.addItem("H", "Homologada", 0);
            cmbavContagemresultado_statusdmn1.addItem("O", "Aceite", 0);
            cmbavContagemresultado_statusdmn1.addItem("P", "A Pagar", 0);
            cmbavContagemresultado_statusdmn1.addItem("L", "Liquidada", 0);
            cmbavContagemresultado_statusdmn1.addItem("X", "Cancelada", 0);
            cmbavContagemresultado_statusdmn1.addItem("N", "N�o Faturada", 0);
            cmbavContagemresultado_statusdmn1.addItem("J", "Planejamento", 0);
            cmbavContagemresultado_statusdmn1.addItem("I", "An�lise Planejamento", 0);
            cmbavContagemresultado_statusdmn1.addItem("T", "Validacao T�cnica", 0);
            cmbavContagemresultado_statusdmn1.addItem("Q", "Validacao Qualidade", 0);
            cmbavContagemresultado_statusdmn1.addItem("G", "Em Homologa��o", 0);
            cmbavContagemresultado_statusdmn1.addItem("M", "Valida��o Mensura��o", 0);
            cmbavContagemresultado_statusdmn1.addItem("U", "Rascunho", 0);
            if ( cmbavContagemresultado_statusdmn1.ItemCount > 0 )
            {
               AV51ContagemResultado_StatusDmn1 = cmbavContagemresultado_statusdmn1.getValidValue(AV51ContagemResultado_StatusDmn1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ContagemResultado_StatusDmn1", AV51ContagemResultado_StatusDmn1);
            }
            cmbavContagemresultado_statuscnt1.Name = "vCONTAGEMRESULTADO_STATUSCNT1";
            cmbavContagemresultado_statuscnt1.WebTags = "";
            cmbavContagemresultado_statuscnt1.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 2, 0)), "Todos", 0);
            cmbavContagemresultado_statuscnt1.addItem("1", "Inicial", 0);
            cmbavContagemresultado_statuscnt1.addItem("2", "Auditoria", 0);
            cmbavContagemresultado_statuscnt1.addItem("3", "Negociac�o", 0);
            cmbavContagemresultado_statuscnt1.addItem("4", "N�o Aprovada", 0);
            cmbavContagemresultado_statuscnt1.addItem("5", "Aprovada", 0);
            cmbavContagemresultado_statuscnt1.addItem("6", "Pend�ncias", 0);
            cmbavContagemresultado_statuscnt1.addItem("7", "Diverg�ncia", 0);
            if ( cmbavContagemresultado_statuscnt1.ItemCount > 0 )
            {
               AV52ContagemResultado_StatusCnt1 = (short)(NumberUtil.Val( cmbavContagemresultado_statuscnt1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV52ContagemResultado_StatusCnt1), 2, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ContagemResultado_StatusCnt1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52ContagemResultado_StatusCnt1), 2, 0)));
            }
            dynavContagemresultado_sistemacod1.Name = "vCONTAGEMRESULTADO_SISTEMACOD1";
            dynavContagemresultado_sistemacod1.WebTags = "";
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTAGEMRESULTADO_DEMANDAFM", "OS / OS Refer�ncia", 0);
            cmbavDynamicfiltersselector2.addItem("CONTAGEMRESULTADO_DATADMN", "Data da Demanda", 0);
            cmbavDynamicfiltersselector2.addItem("CONTAGEMRESULTADO_DATAENTREGAREAL", "Data de Entrega", 0);
            cmbavDynamicfiltersselector2.addItem("CONTAGEMRESULTADO_STATUSDMN", "Status da Demanda", 0);
            cmbavDynamicfiltersselector2.addItem("CONTAGEMRESULTADO_STATUSCNT", "Status do Servi�o", 0);
            cmbavDynamicfiltersselector2.addItem("CONTAGEMRESULTADO_DESCRICAO", "T�tulo", 0);
            cmbavDynamicfiltersselector2.addItem("CONTAGEMRESULTADO_SISTEMACOD", "Sistema", 0);
            cmbavDynamicfiltersselector2.addItem("CONTAGEMRESULTADO_AGRUPADOR", "Agrupador", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV26DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV26DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector2", AV26DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Igual", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("2", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV27DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator2), 4, 0)));
            }
            cmbavContagemresultado_statusdmn2.Name = "vCONTAGEMRESULTADO_STATUSDMN2";
            cmbavContagemresultado_statusdmn2.WebTags = "";
            cmbavContagemresultado_statusdmn2.addItem("", "Todos", 0);
            cmbavContagemresultado_statusdmn2.addItem("B", "Stand by", 0);
            cmbavContagemresultado_statusdmn2.addItem("S", "Solicitada", 0);
            cmbavContagemresultado_statusdmn2.addItem("E", "Em An�lise", 0);
            cmbavContagemresultado_statusdmn2.addItem("A", "Em execu��o", 0);
            cmbavContagemresultado_statusdmn2.addItem("R", "Resolvida", 0);
            cmbavContagemresultado_statusdmn2.addItem("C", "Conferida", 0);
            cmbavContagemresultado_statusdmn2.addItem("D", "Rejeitada", 0);
            cmbavContagemresultado_statusdmn2.addItem("H", "Homologada", 0);
            cmbavContagemresultado_statusdmn2.addItem("O", "Aceite", 0);
            cmbavContagemresultado_statusdmn2.addItem("P", "A Pagar", 0);
            cmbavContagemresultado_statusdmn2.addItem("L", "Liquidada", 0);
            cmbavContagemresultado_statusdmn2.addItem("X", "Cancelada", 0);
            cmbavContagemresultado_statusdmn2.addItem("N", "N�o Faturada", 0);
            cmbavContagemresultado_statusdmn2.addItem("J", "Planejamento", 0);
            cmbavContagemresultado_statusdmn2.addItem("I", "An�lise Planejamento", 0);
            cmbavContagemresultado_statusdmn2.addItem("T", "Validacao T�cnica", 0);
            cmbavContagemresultado_statusdmn2.addItem("Q", "Validacao Qualidade", 0);
            cmbavContagemresultado_statusdmn2.addItem("G", "Em Homologa��o", 0);
            cmbavContagemresultado_statusdmn2.addItem("M", "Valida��o Mensura��o", 0);
            cmbavContagemresultado_statusdmn2.addItem("U", "Rascunho", 0);
            if ( cmbavContagemresultado_statusdmn2.ItemCount > 0 )
            {
               AV59ContagemResultado_StatusDmn2 = cmbavContagemresultado_statusdmn2.getValidValue(AV59ContagemResultado_StatusDmn2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59ContagemResultado_StatusDmn2", AV59ContagemResultado_StatusDmn2);
            }
            cmbavContagemresultado_statuscnt2.Name = "vCONTAGEMRESULTADO_STATUSCNT2";
            cmbavContagemresultado_statuscnt2.WebTags = "";
            cmbavContagemresultado_statuscnt2.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 2, 0)), "Todos", 0);
            cmbavContagemresultado_statuscnt2.addItem("1", "Inicial", 0);
            cmbavContagemresultado_statuscnt2.addItem("2", "Auditoria", 0);
            cmbavContagemresultado_statuscnt2.addItem("3", "Negociac�o", 0);
            cmbavContagemresultado_statuscnt2.addItem("4", "N�o Aprovada", 0);
            cmbavContagemresultado_statuscnt2.addItem("5", "Aprovada", 0);
            cmbavContagemresultado_statuscnt2.addItem("6", "Pend�ncias", 0);
            cmbavContagemresultado_statuscnt2.addItem("7", "Diverg�ncia", 0);
            if ( cmbavContagemresultado_statuscnt2.ItemCount > 0 )
            {
               AV60ContagemResultado_StatusCnt2 = (short)(NumberUtil.Val( cmbavContagemresultado_statuscnt2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV60ContagemResultado_StatusCnt2), 2, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60ContagemResultado_StatusCnt2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60ContagemResultado_StatusCnt2), 2, 0)));
            }
            dynavContagemresultado_sistemacod2.Name = "vCONTAGEMRESULTADO_SISTEMACOD2";
            dynavContagemresultado_sistemacod2.WebTags = "";
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CONTAGEMRESULTADO_DEMANDAFM", "OS / OS Refer�ncia", 0);
            cmbavDynamicfiltersselector3.addItem("CONTAGEMRESULTADO_DATADMN", "Data da Demanda", 0);
            cmbavDynamicfiltersselector3.addItem("CONTAGEMRESULTADO_DATAENTREGAREAL", "Data de Entrega", 0);
            cmbavDynamicfiltersselector3.addItem("CONTAGEMRESULTADO_STATUSDMN", "Status da Demanda", 0);
            cmbavDynamicfiltersselector3.addItem("CONTAGEMRESULTADO_STATUSCNT", "Status do Servi�o", 0);
            cmbavDynamicfiltersselector3.addItem("CONTAGEMRESULTADO_DESCRICAO", "T�tulo", 0);
            cmbavDynamicfiltersselector3.addItem("CONTAGEMRESULTADO_SISTEMACOD", "Sistema", 0);
            cmbavDynamicfiltersselector3.addItem("CONTAGEMRESULTADO_AGRUPADOR", "Agrupador", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV31DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV31DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31DynamicFiltersSelector3", AV31DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Igual", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("2", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV32DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV32DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32DynamicFiltersOperator3), 4, 0)));
            }
            cmbavContagemresultado_statusdmn3.Name = "vCONTAGEMRESULTADO_STATUSDMN3";
            cmbavContagemresultado_statusdmn3.WebTags = "";
            cmbavContagemresultado_statusdmn3.addItem("", "Todos", 0);
            cmbavContagemresultado_statusdmn3.addItem("B", "Stand by", 0);
            cmbavContagemresultado_statusdmn3.addItem("S", "Solicitada", 0);
            cmbavContagemresultado_statusdmn3.addItem("E", "Em An�lise", 0);
            cmbavContagemresultado_statusdmn3.addItem("A", "Em execu��o", 0);
            cmbavContagemresultado_statusdmn3.addItem("R", "Resolvida", 0);
            cmbavContagemresultado_statusdmn3.addItem("C", "Conferida", 0);
            cmbavContagemresultado_statusdmn3.addItem("D", "Rejeitada", 0);
            cmbavContagemresultado_statusdmn3.addItem("H", "Homologada", 0);
            cmbavContagemresultado_statusdmn3.addItem("O", "Aceite", 0);
            cmbavContagemresultado_statusdmn3.addItem("P", "A Pagar", 0);
            cmbavContagemresultado_statusdmn3.addItem("L", "Liquidada", 0);
            cmbavContagemresultado_statusdmn3.addItem("X", "Cancelada", 0);
            cmbavContagemresultado_statusdmn3.addItem("N", "N�o Faturada", 0);
            cmbavContagemresultado_statusdmn3.addItem("J", "Planejamento", 0);
            cmbavContagemresultado_statusdmn3.addItem("I", "An�lise Planejamento", 0);
            cmbavContagemresultado_statusdmn3.addItem("T", "Validacao T�cnica", 0);
            cmbavContagemresultado_statusdmn3.addItem("Q", "Validacao Qualidade", 0);
            cmbavContagemresultado_statusdmn3.addItem("G", "Em Homologa��o", 0);
            cmbavContagemresultado_statusdmn3.addItem("M", "Valida��o Mensura��o", 0);
            cmbavContagemresultado_statusdmn3.addItem("U", "Rascunho", 0);
            if ( cmbavContagemresultado_statusdmn3.ItemCount > 0 )
            {
               AV67ContagemResultado_StatusDmn3 = cmbavContagemresultado_statusdmn3.getValidValue(AV67ContagemResultado_StatusDmn3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ContagemResultado_StatusDmn3", AV67ContagemResultado_StatusDmn3);
            }
            cmbavContagemresultado_statuscnt3.Name = "vCONTAGEMRESULTADO_STATUSCNT3";
            cmbavContagemresultado_statuscnt3.WebTags = "";
            cmbavContagemresultado_statuscnt3.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 2, 0)), "Todos", 0);
            cmbavContagemresultado_statuscnt3.addItem("1", "Inicial", 0);
            cmbavContagemresultado_statuscnt3.addItem("2", "Auditoria", 0);
            cmbavContagemresultado_statuscnt3.addItem("3", "Negociac�o", 0);
            cmbavContagemresultado_statuscnt3.addItem("4", "N�o Aprovada", 0);
            cmbavContagemresultado_statuscnt3.addItem("5", "Aprovada", 0);
            cmbavContagemresultado_statuscnt3.addItem("6", "Pend�ncias", 0);
            cmbavContagemresultado_statuscnt3.addItem("7", "Diverg�ncia", 0);
            if ( cmbavContagemresultado_statuscnt3.ItemCount > 0 )
            {
               AV68ContagemResultado_StatusCnt3 = (short)(NumberUtil.Val( cmbavContagemresultado_statuscnt3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV68ContagemResultado_StatusCnt3), 2, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ContagemResultado_StatusCnt3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68ContagemResultado_StatusCnt3), 2, 0)));
            }
            dynavContagemresultado_sistemacod3.Name = "vCONTAGEMRESULTADO_SISTEMACOD3";
            dynavContagemresultado_sistemacod3.WebTags = "";
            GXCCtl = "vCONTAGEMRESULTADO_CONTRATADATIPOFAB_" + sGXsfl_187_idx;
            cmbavContagemresultado_contratadatipofab.Name = GXCCtl;
            cmbavContagemresultado_contratadatipofab.WebTags = "";
            cmbavContagemresultado_contratadatipofab.addItem("", "(Nenhuma)", 0);
            cmbavContagemresultado_contratadatipofab.addItem("S", "F�brica de Software", 0);
            cmbavContagemresultado_contratadatipofab.addItem("M", "F�brica de M�trica", 0);
            cmbavContagemresultado_contratadatipofab.addItem("I", "Departamento / Setor", 0);
            cmbavContagemresultado_contratadatipofab.addItem("O", "Outras", 0);
            if ( cmbavContagemresultado_contratadatipofab.ItemCount > 0 )
            {
               AV95ContagemResultado_ContratadaTipoFab = cmbavContagemresultado_contratadatipofab.getValidValue(AV95ContagemResultado_ContratadaTipoFab);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavContagemresultado_contratadatipofab_Internalname, AV95ContagemResultado_ContratadaTipoFab);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavContratada_areatrabalhocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         GXVvCONTAGEMRESULTADO_SERVICO_htmlTR2( AV15ContagemResultado_ContratadaCod, AV16ContagemResultado_ServicoGrupo) ;
         GXVvCONTAGEMRESULTADO_SERVICO_htmlTR2( AV15ContagemResultado_ContratadaCod, AV16ContagemResultado_ServicoGrupo) ;
         GXVvCONTAGEMRESULTADO_CODSRVVNC_htmlTR2( AV18ContagemResultado_CntadaOsVinc, AV19ContagemResultado_SerGrupoVinc) ;
         GXVvCONTAGEMRESULTADO_CODSRVVNC_htmlTR2( AV18ContagemResultado_CntadaOsVinc, AV19ContagemResultado_SerGrupoVinc) ;
         /* End function dynload_actions */
      }

      protected void GXDLVvCONTAGEMRESULTADO_CONTRATADACODTR2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_CONTRATADACOD_dataTR2( AV6WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_CONTRATADACOD_htmlTR2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_CONTRATADACOD_dataTR2( AV6WWPContext) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_contratadacod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_contratadacod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_contratadacod.ItemCount > 0 )
         {
            AV15ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( dynavContagemresultado_contratadacod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV15ContagemResultado_ContratadaCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15ContagemResultado_ContratadaCod), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_CONTRATADACOD_dataTR2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Selecione");
         /* Using cursor H00TR2 */
         pr_default.execute(0, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00TR2_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00TR2_A438Contratada_Sigla[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void GXDLVvCONTAGEMRESULTADO_SERVICOGRUPOTR2( int AV15ContagemResultado_ContratadaCod )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_SERVICOGRUPO_dataTR2( AV15ContagemResultado_ContratadaCod) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_SERVICOGRUPO_htmlTR2( int AV15ContagemResultado_ContratadaCod )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_SERVICOGRUPO_dataTR2( AV15ContagemResultado_ContratadaCod) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_servicogrupo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_servicogrupo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_servicogrupo.ItemCount > 0 )
         {
            AV16ContagemResultado_ServicoGrupo = (int)(NumberUtil.Val( dynavContagemresultado_servicogrupo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16ContagemResultado_ServicoGrupo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContagemResultado_ServicoGrupo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16ContagemResultado_ServicoGrupo), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_SERVICOGRUPO_dataTR2( int AV15ContagemResultado_ContratadaCod )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Selecione");
         /* Using cursor H00TR3 */
         pr_default.execute(1, new Object[] {AV15ContagemResultado_ContratadaCod});
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00TR3_A157ServicoGrupo_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00TR3_A158ServicoGrupo_Descricao[0]);
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void GXDLVvCONTAGEMRESULTADO_SERVICOTR2( int AV15ContagemResultado_ContratadaCod ,
                                                         int AV16ContagemResultado_ServicoGrupo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_SERVICO_dataTR2( AV15ContagemResultado_ContratadaCod, AV16ContagemResultado_ServicoGrupo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_SERVICO_htmlTR2( int AV15ContagemResultado_ContratadaCod ,
                                                            int AV16ContagemResultado_ServicoGrupo )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_SERVICO_dataTR2( AV15ContagemResultado_ContratadaCod, AV16ContagemResultado_ServicoGrupo) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_servico.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_servico.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_servico.ItemCount > 0 )
         {
            AV17ContagemResultado_Servico = (int)(NumberUtil.Val( dynavContagemresultado_servico.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17ContagemResultado_Servico), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContagemResultado_Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17ContagemResultado_Servico), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_SERVICO_dataTR2( int AV15ContagemResultado_ContratadaCod ,
                                                              int AV16ContagemResultado_ServicoGrupo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Selecione");
         /* Using cursor H00TR4 */
         pr_default.execute(2, new Object[] {AV15ContagemResultado_ContratadaCod, AV16ContagemResultado_ServicoGrupo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00TR4_A155Servico_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00TR4_A605Servico_Sigla[0]));
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void GXDLVvCONTAGEMRESULTADO_CNTADAOSVINCTR2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_CNTADAOSVINC_dataTR2( AV6WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_CNTADAOSVINC_htmlTR2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_CNTADAOSVINC_dataTR2( AV6WWPContext) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_cntadaosvinc.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_cntadaosvinc.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_cntadaosvinc.ItemCount > 0 )
         {
            AV18ContagemResultado_CntadaOsVinc = (int)(NumberUtil.Val( dynavContagemresultado_cntadaosvinc.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV18ContagemResultado_CntadaOsVinc), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultado_CntadaOsVinc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ContagemResultado_CntadaOsVinc), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_CNTADAOSVINC_dataTR2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Selecione");
         /* Using cursor H00TR5 */
         pr_default.execute(3, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(3) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00TR5_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00TR5_A438Contratada_Sigla[0]));
            pr_default.readNext(3);
         }
         pr_default.close(3);
      }

      protected void GXDLVvCONTAGEMRESULTADO_SERGRUPOVINCTR2( int AV18ContagemResultado_CntadaOsVinc )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_SERGRUPOVINC_dataTR2( AV18ContagemResultado_CntadaOsVinc) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_SERGRUPOVINC_htmlTR2( int AV18ContagemResultado_CntadaOsVinc )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_SERGRUPOVINC_dataTR2( AV18ContagemResultado_CntadaOsVinc) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_sergrupovinc.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_sergrupovinc.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_sergrupovinc.ItemCount > 0 )
         {
            AV19ContagemResultado_SerGrupoVinc = (int)(NumberUtil.Val( dynavContagemresultado_sergrupovinc.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV19ContagemResultado_SerGrupoVinc), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultado_SerGrupoVinc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19ContagemResultado_SerGrupoVinc), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_SERGRUPOVINC_dataTR2( int AV18ContagemResultado_CntadaOsVinc )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Selecione");
         /* Using cursor H00TR6 */
         pr_default.execute(4, new Object[] {AV18ContagemResultado_CntadaOsVinc});
         while ( (pr_default.getStatus(4) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00TR6_A157ServicoGrupo_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00TR6_A158ServicoGrupo_Descricao[0]);
            pr_default.readNext(4);
         }
         pr_default.close(4);
      }

      protected void GXDLVvCONTAGEMRESULTADO_CODSRVVNCTR2( int AV18ContagemResultado_CntadaOsVinc ,
                                                           int AV19ContagemResultado_SerGrupoVinc )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_CODSRVVNC_dataTR2( AV18ContagemResultado_CntadaOsVinc, AV19ContagemResultado_SerGrupoVinc) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_CODSRVVNC_htmlTR2( int AV18ContagemResultado_CntadaOsVinc ,
                                                              int AV19ContagemResultado_SerGrupoVinc )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_CODSRVVNC_dataTR2( AV18ContagemResultado_CntadaOsVinc, AV19ContagemResultado_SerGrupoVinc) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_codsrvvnc.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_codsrvvnc.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_codsrvvnc.ItemCount > 0 )
         {
            AV20ContagemResultado_CodSrvVnc = (int)(NumberUtil.Val( dynavContagemresultado_codsrvvnc.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20ContagemResultado_CodSrvVnc), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContagemResultado_CodSrvVnc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20ContagemResultado_CodSrvVnc), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_CODSRVVNC_dataTR2( int AV18ContagemResultado_CntadaOsVinc ,
                                                                int AV19ContagemResultado_SerGrupoVinc )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Selecione");
         /* Using cursor H00TR7 */
         pr_default.execute(5, new Object[] {AV18ContagemResultado_CntadaOsVinc, AV19ContagemResultado_SerGrupoVinc});
         while ( (pr_default.getStatus(5) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00TR7_A155Servico_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00TR7_A605Servico_Sigla[0]));
            pr_default.readNext(5);
         }
         pr_default.close(5);
      }

      protected void GXDLVvCONTAGEMRESULTADO_SISTEMACOD1TR2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_SISTEMACOD1_dataTR2( AV6WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_SISTEMACOD1_htmlTR2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_SISTEMACOD1_dataTR2( AV6WWPContext) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_sistemacod1.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_sistemacod1.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_sistemacod1.ItemCount > 0 )
         {
            AV54ContagemResultado_SistemaCod1 = (int)(NumberUtil.Val( dynavContagemresultado_sistemacod1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV54ContagemResultado_SistemaCod1), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ContagemResultado_SistemaCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54ContagemResultado_SistemaCod1), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_SISTEMACOD1_dataTR2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhum");
         /* Using cursor H00TR8 */
         pr_default.execute(6, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(6) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00TR8_A127Sistema_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00TR8_A129Sistema_Sigla[0]));
            pr_default.readNext(6);
         }
         pr_default.close(6);
      }

      protected void GXDLVvCONTAGEMRESULTADO_SISTEMACOD2TR2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_SISTEMACOD2_dataTR2( AV6WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_SISTEMACOD2_htmlTR2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_SISTEMACOD2_dataTR2( AV6WWPContext) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_sistemacod2.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_sistemacod2.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_sistemacod2.ItemCount > 0 )
         {
            AV62ContagemResultado_SistemaCod2 = (int)(NumberUtil.Val( dynavContagemresultado_sistemacod2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV62ContagemResultado_SistemaCod2), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62ContagemResultado_SistemaCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62ContagemResultado_SistemaCod2), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_SISTEMACOD2_dataTR2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhum");
         /* Using cursor H00TR9 */
         pr_default.execute(7, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(7) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00TR9_A127Sistema_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00TR9_A129Sistema_Sigla[0]));
            pr_default.readNext(7);
         }
         pr_default.close(7);
      }

      protected void GXDLVvCONTAGEMRESULTADO_SISTEMACOD3TR2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_SISTEMACOD3_dataTR2( AV6WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_SISTEMACOD3_htmlTR2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_SISTEMACOD3_dataTR2( AV6WWPContext) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_sistemacod3.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_sistemacod3.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_sistemacod3.ItemCount > 0 )
         {
            AV70ContagemResultado_SistemaCod3 = (int)(NumberUtil.Val( dynavContagemresultado_sistemacod3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV70ContagemResultado_SistemaCod3), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70ContagemResultado_SistemaCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV70ContagemResultado_SistemaCod3), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_SISTEMACOD3_dataTR2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhum");
         /* Using cursor H00TR10 */
         pr_default.execute(8, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(8) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00TR10_A127Sistema_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00TR10_A129Sistema_Sigla[0]));
            pr_default.readNext(8);
         }
         pr_default.close(8);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_1872( ) ;
         while ( nGXsfl_187_idx <= nRC_GXsfl_187 )
         {
            sendrow_1872( ) ;
            nGXsfl_187_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_187_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_187_idx+1));
            sGXsfl_187_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_187_idx), 4, 0)), 4, "0");
            SubsflControlProps_1872( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       String AV21DynamicFiltersSelector1 ,
                                       bool AV25DynamicFiltersEnabled2 ,
                                       String AV26DynamicFiltersSelector2 ,
                                       bool AV30DynamicFiltersEnabled3 ,
                                       String AV31DynamicFiltersSelector3 ,
                                       String AV192Pgmname ,
                                       int AV13Contratada_AreaTrabalhoCod ,
                                       int AV15ContagemResultado_ContratadaCod ,
                                       int AV16ContagemResultado_ServicoGrupo ,
                                       int AV17ContagemResultado_Servico ,
                                       int AV18ContagemResultado_CntadaOsVinc ,
                                       int AV19ContagemResultado_SerGrupoVinc ,
                                       int AV20ContagemResultado_CodSrvVnc ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV36DynamicFiltersIgnoreFirst ,
                                       String AV48ContagemResultado_DemandaFM1 ,
                                       DateTime AV24ContagemResultado_DataDmn_To1 ,
                                       DateTime AV23ContagemResultado_DataDmn1 ,
                                       DateTime AV50ContagemResultado_DataEntregaReal_To1 ,
                                       DateTime AV49ContagemResultado_DataEntregaReal1 ,
                                       String AV51ContagemResultado_StatusDmn1 ,
                                       short AV52ContagemResultado_StatusCnt1 ,
                                       String AV53ContagemResultado_Descricao1 ,
                                       int AV54ContagemResultado_SistemaCod1 ,
                                       short AV22DynamicFiltersOperator1 ,
                                       String AV55ContagemResultado_Agrupador1 ,
                                       bool AV35DynamicFiltersRemoving ,
                                       String AV56ContagemResultado_DemandaFM2 ,
                                       DateTime AV29ContagemResultado_DataDmn_To2 ,
                                       DateTime AV28ContagemResultado_DataDmn2 ,
                                       DateTime AV58ContagemResultado_DataEntregaReal_To2 ,
                                       DateTime AV57ContagemResultado_DataEntregaReal2 ,
                                       String AV59ContagemResultado_StatusDmn2 ,
                                       short AV60ContagemResultado_StatusCnt2 ,
                                       String AV61ContagemResultado_Descricao2 ,
                                       int AV62ContagemResultado_SistemaCod2 ,
                                       short AV27DynamicFiltersOperator2 ,
                                       String AV63ContagemResultado_Agrupador2 ,
                                       String AV64ContagemResultado_DemandaFM3 ,
                                       DateTime AV34ContagemResultado_DataDmn_To3 ,
                                       DateTime AV33ContagemResultado_DataDmn3 ,
                                       DateTime AV66ContagemResultado_DataEntregaReal_To3 ,
                                       DateTime AV65ContagemResultado_DataEntregaReal3 ,
                                       String AV67ContagemResultado_StatusDmn3 ,
                                       short AV68ContagemResultado_StatusCnt3 ,
                                       String AV69ContagemResultado_Descricao3 ,
                                       int AV70ContagemResultado_SistemaCod3 ,
                                       short AV32DynamicFiltersOperator3 ,
                                       String AV71ContagemResultado_Agrupador3 ,
                                       String A457ContagemResultado_Demanda ,
                                       int A52Contratada_AreaTrabalhoCod ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       int A490ContagemResultado_ContratadaCod ,
                                       int A764ContagemResultado_ServicoGrupo ,
                                       int A601ContagemResultado_Servico ,
                                       int A602ContagemResultado_OSVinculada ,
                                       String A493ContagemResultado_DemandaFM ,
                                       DateTime A471ContagemResultado_DataDmn ,
                                       DateTime A2017ContagemResultado_DataEntregaReal ,
                                       String A484ContagemResultado_StatusDmn ,
                                       String A494ContagemResultado_Descricao ,
                                       int A489ContagemResultado_SistemaCod ,
                                       String A1046ContagemResultado_Agrupador ,
                                       int A456ContagemResultado_Codigo ,
                                       String A509ContagemrResultado_SistemaSigla ,
                                       String A1326ContagemResultado_ContratadaTipoFab ,
                                       decimal A682ContagemResultado_PFBFMUltima ,
                                       decimal A684ContagemResultado_PFBFSUltima ,
                                       int AV14ContagemResultado_OSVinculada )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFTR2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavContagemresultado_contratadacod.ItemCount > 0 )
         {
            AV15ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( dynavContagemresultado_contratadacod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV15ContagemResultado_ContratadaCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15ContagemResultado_ContratadaCod), 6, 0)));
         }
         if ( dynavContagemresultado_servicogrupo.ItemCount > 0 )
         {
            AV16ContagemResultado_ServicoGrupo = (int)(NumberUtil.Val( dynavContagemresultado_servicogrupo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16ContagemResultado_ServicoGrupo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContagemResultado_ServicoGrupo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16ContagemResultado_ServicoGrupo), 6, 0)));
         }
         if ( dynavContagemresultado_servico.ItemCount > 0 )
         {
            AV17ContagemResultado_Servico = (int)(NumberUtil.Val( dynavContagemresultado_servico.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17ContagemResultado_Servico), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContagemResultado_Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17ContagemResultado_Servico), 6, 0)));
         }
         if ( dynavContagemresultado_cntadaosvinc.ItemCount > 0 )
         {
            AV18ContagemResultado_CntadaOsVinc = (int)(NumberUtil.Val( dynavContagemresultado_cntadaosvinc.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV18ContagemResultado_CntadaOsVinc), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultado_CntadaOsVinc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ContagemResultado_CntadaOsVinc), 6, 0)));
         }
         if ( dynavContagemresultado_sergrupovinc.ItemCount > 0 )
         {
            AV19ContagemResultado_SerGrupoVinc = (int)(NumberUtil.Val( dynavContagemresultado_sergrupovinc.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV19ContagemResultado_SerGrupoVinc), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultado_SerGrupoVinc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19ContagemResultado_SerGrupoVinc), 6, 0)));
         }
         if ( dynavContagemresultado_codsrvvnc.ItemCount > 0 )
         {
            AV20ContagemResultado_CodSrvVnc = (int)(NumberUtil.Val( dynavContagemresultado_codsrvvnc.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20ContagemResultado_CodSrvVnc), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContagemResultado_CodSrvVnc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20ContagemResultado_CodSrvVnc), 6, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV21DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV21DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector1", AV21DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV22DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavContagemresultado_statusdmn1.ItemCount > 0 )
         {
            AV51ContagemResultado_StatusDmn1 = cmbavContagemresultado_statusdmn1.getValidValue(AV51ContagemResultado_StatusDmn1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ContagemResultado_StatusDmn1", AV51ContagemResultado_StatusDmn1);
         }
         if ( cmbavContagemresultado_statuscnt1.ItemCount > 0 )
         {
            AV52ContagemResultado_StatusCnt1 = (short)(NumberUtil.Val( cmbavContagemresultado_statuscnt1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV52ContagemResultado_StatusCnt1), 2, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ContagemResultado_StatusCnt1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52ContagemResultado_StatusCnt1), 2, 0)));
         }
         if ( dynavContagemresultado_sistemacod1.ItemCount > 0 )
         {
            AV54ContagemResultado_SistemaCod1 = (int)(NumberUtil.Val( dynavContagemresultado_sistemacod1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV54ContagemResultado_SistemaCod1), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ContagemResultado_SistemaCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54ContagemResultado_SistemaCod1), 6, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV26DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV26DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector2", AV26DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV27DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavContagemresultado_statusdmn2.ItemCount > 0 )
         {
            AV59ContagemResultado_StatusDmn2 = cmbavContagemresultado_statusdmn2.getValidValue(AV59ContagemResultado_StatusDmn2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59ContagemResultado_StatusDmn2", AV59ContagemResultado_StatusDmn2);
         }
         if ( cmbavContagemresultado_statuscnt2.ItemCount > 0 )
         {
            AV60ContagemResultado_StatusCnt2 = (short)(NumberUtil.Val( cmbavContagemresultado_statuscnt2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV60ContagemResultado_StatusCnt2), 2, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60ContagemResultado_StatusCnt2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60ContagemResultado_StatusCnt2), 2, 0)));
         }
         if ( dynavContagemresultado_sistemacod2.ItemCount > 0 )
         {
            AV62ContagemResultado_SistemaCod2 = (int)(NumberUtil.Val( dynavContagemresultado_sistemacod2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV62ContagemResultado_SistemaCod2), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62ContagemResultado_SistemaCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62ContagemResultado_SistemaCod2), 6, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV31DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV31DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31DynamicFiltersSelector3", AV31DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV32DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV32DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32DynamicFiltersOperator3), 4, 0)));
         }
         if ( cmbavContagemresultado_statusdmn3.ItemCount > 0 )
         {
            AV67ContagemResultado_StatusDmn3 = cmbavContagemresultado_statusdmn3.getValidValue(AV67ContagemResultado_StatusDmn3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ContagemResultado_StatusDmn3", AV67ContagemResultado_StatusDmn3);
         }
         if ( cmbavContagemresultado_statuscnt3.ItemCount > 0 )
         {
            AV68ContagemResultado_StatusCnt3 = (short)(NumberUtil.Val( cmbavContagemresultado_statuscnt3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV68ContagemResultado_StatusCnt3), 2, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ContagemResultado_StatusCnt3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68ContagemResultado_StatusCnt3), 2, 0)));
         }
         if ( dynavContagemresultado_sistemacod3.ItemCount > 0 )
         {
            AV70ContagemResultado_SistemaCod3 = (int)(NumberUtil.Val( dynavContagemresultado_sistemacod3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV70ContagemResultado_SistemaCod3), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70ContagemResultado_SistemaCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV70ContagemResultado_SistemaCod3), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFTR2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV192Pgmname = "WP_RelatorioComparacaoDemandas_OLD";
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         edtavContagemresultado_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_codigo_Enabled), 5, 0)));
         edtavContagemresultado_osvinculada_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_osvinculada_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_osvinculada_Enabled), 5, 0)));
         cmbavContagemresultado_contratadatipofab.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_contratadatipofab_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContagemresultado_contratadatipofab.Enabled), 5, 0)));
         edtavContagemresultado_pfbfmultima_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_pfbfmultima_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_pfbfmultima_Enabled), 5, 0)));
         edtavContagemresultado_pfbfsultima_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_pfbfsultima_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_pfbfsultima_Enabled), 5, 0)));
         edtavEsforco_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEsforco_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEsforco_Enabled), 5, 0)));
      }

      protected void RFTR2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 187;
         /* Execute user event: E25TR2 */
         E25TR2 ();
         nGXsfl_187_idx = 1;
         sGXsfl_187_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_187_idx), 4, 0)), 4, "0");
         SubsflControlProps_1872( ) ;
         nGXsfl_187_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_1872( ) ;
            /* Execute user event: E26TR2 */
            E26TR2 ();
            if ( ( GRID_nCurrentRecord > 0 ) && ( GRID_nGridOutOfScope == 0 ) && ( nGXsfl_187_idx == 1 ) )
            {
               GRID_nCurrentRecord = 0;
               GRID_nGridOutOfScope = 1;
               subgrid_firstpage( ) ;
               /* Execute user event: E26TR2 */
               E26TR2 ();
            }
            wbEnd = 187;
            WBTR0( ) ;
         }
         nGXsfl_187_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV21DynamicFiltersSelector1, AV25DynamicFiltersEnabled2, AV26DynamicFiltersSelector2, AV30DynamicFiltersEnabled3, AV31DynamicFiltersSelector3, AV192Pgmname, AV13Contratada_AreaTrabalhoCod, AV15ContagemResultado_ContratadaCod, AV16ContagemResultado_ServicoGrupo, AV17ContagemResultado_Servico, AV18ContagemResultado_CntadaOsVinc, AV19ContagemResultado_SerGrupoVinc, AV20ContagemResultado_CodSrvVnc, AV10GridState, AV36DynamicFiltersIgnoreFirst, AV48ContagemResultado_DemandaFM1, AV24ContagemResultado_DataDmn_To1, AV23ContagemResultado_DataDmn1, AV50ContagemResultado_DataEntregaReal_To1, AV49ContagemResultado_DataEntregaReal1, AV51ContagemResultado_StatusDmn1, AV52ContagemResultado_StatusCnt1, AV53ContagemResultado_Descricao1, AV54ContagemResultado_SistemaCod1, AV22DynamicFiltersOperator1, AV55ContagemResultado_Agrupador1, AV35DynamicFiltersRemoving, AV56ContagemResultado_DemandaFM2, AV29ContagemResultado_DataDmn_To2, AV28ContagemResultado_DataDmn2, AV58ContagemResultado_DataEntregaReal_To2, AV57ContagemResultado_DataEntregaReal2, AV59ContagemResultado_StatusDmn2, AV60ContagemResultado_StatusCnt2, AV61ContagemResultado_Descricao2, AV62ContagemResultado_SistemaCod2, AV27DynamicFiltersOperator2, AV63ContagemResultado_Agrupador2, AV64ContagemResultado_DemandaFM3, AV34ContagemResultado_DataDmn_To3, AV33ContagemResultado_DataDmn3, AV66ContagemResultado_DataEntregaReal_To3, AV65ContagemResultado_DataEntregaReal3, AV67ContagemResultado_StatusDmn3, AV68ContagemResultado_StatusCnt3, AV69ContagemResultado_Descricao3, AV70ContagemResultado_SistemaCod3, AV32DynamicFiltersOperator3, AV71ContagemResultado_Agrupador3, A457ContagemResultado_Demanda, A52Contratada_AreaTrabalhoCod, AV6WWPContext, A490ContagemResultado_ContratadaCod, A764ContagemResultado_ServicoGrupo, A601ContagemResultado_Servico, A602ContagemResultado_OSVinculada, A493ContagemResultado_DemandaFM, A471ContagemResultado_DataDmn, A2017ContagemResultado_DataEntregaReal, A484ContagemResultado_StatusDmn, A494ContagemResultado_Descricao, A489ContagemResultado_SistemaCod, A1046ContagemResultado_Agrupador, A456ContagemResultado_Codigo, A509ContagemrResultado_SistemaSigla, A1326ContagemResultado_ContratadaTipoFab, A682ContagemResultado_PFBFMUltima, A684ContagemResultado_PFBFSUltima, AV14ContagemResultado_OSVinculada) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         if ( GRID_nEOF == 0 )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV21DynamicFiltersSelector1, AV25DynamicFiltersEnabled2, AV26DynamicFiltersSelector2, AV30DynamicFiltersEnabled3, AV31DynamicFiltersSelector3, AV192Pgmname, AV13Contratada_AreaTrabalhoCod, AV15ContagemResultado_ContratadaCod, AV16ContagemResultado_ServicoGrupo, AV17ContagemResultado_Servico, AV18ContagemResultado_CntadaOsVinc, AV19ContagemResultado_SerGrupoVinc, AV20ContagemResultado_CodSrvVnc, AV10GridState, AV36DynamicFiltersIgnoreFirst, AV48ContagemResultado_DemandaFM1, AV24ContagemResultado_DataDmn_To1, AV23ContagemResultado_DataDmn1, AV50ContagemResultado_DataEntregaReal_To1, AV49ContagemResultado_DataEntregaReal1, AV51ContagemResultado_StatusDmn1, AV52ContagemResultado_StatusCnt1, AV53ContagemResultado_Descricao1, AV54ContagemResultado_SistemaCod1, AV22DynamicFiltersOperator1, AV55ContagemResultado_Agrupador1, AV35DynamicFiltersRemoving, AV56ContagemResultado_DemandaFM2, AV29ContagemResultado_DataDmn_To2, AV28ContagemResultado_DataDmn2, AV58ContagemResultado_DataEntregaReal_To2, AV57ContagemResultado_DataEntregaReal2, AV59ContagemResultado_StatusDmn2, AV60ContagemResultado_StatusCnt2, AV61ContagemResultado_Descricao2, AV62ContagemResultado_SistemaCod2, AV27DynamicFiltersOperator2, AV63ContagemResultado_Agrupador2, AV64ContagemResultado_DemandaFM3, AV34ContagemResultado_DataDmn_To3, AV33ContagemResultado_DataDmn3, AV66ContagemResultado_DataEntregaReal_To3, AV65ContagemResultado_DataEntregaReal3, AV67ContagemResultado_StatusDmn3, AV68ContagemResultado_StatusCnt3, AV69ContagemResultado_Descricao3, AV70ContagemResultado_SistemaCod3, AV32DynamicFiltersOperator3, AV71ContagemResultado_Agrupador3, A457ContagemResultado_Demanda, A52Contratada_AreaTrabalhoCod, AV6WWPContext, A490ContagemResultado_ContratadaCod, A764ContagemResultado_ServicoGrupo, A601ContagemResultado_Servico, A602ContagemResultado_OSVinculada, A493ContagemResultado_DemandaFM, A471ContagemResultado_DataDmn, A2017ContagemResultado_DataEntregaReal, A484ContagemResultado_StatusDmn, A494ContagemResultado_Descricao, A489ContagemResultado_SistemaCod, A1046ContagemResultado_Agrupador, A456ContagemResultado_Codigo, A509ContagemrResultado_SistemaSigla, A1326ContagemResultado_ContratadaTipoFab, A682ContagemResultado_PFBFMUltima, A684ContagemResultado_PFBFSUltima, AV14ContagemResultado_OSVinculada) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV21DynamicFiltersSelector1, AV25DynamicFiltersEnabled2, AV26DynamicFiltersSelector2, AV30DynamicFiltersEnabled3, AV31DynamicFiltersSelector3, AV192Pgmname, AV13Contratada_AreaTrabalhoCod, AV15ContagemResultado_ContratadaCod, AV16ContagemResultado_ServicoGrupo, AV17ContagemResultado_Servico, AV18ContagemResultado_CntadaOsVinc, AV19ContagemResultado_SerGrupoVinc, AV20ContagemResultado_CodSrvVnc, AV10GridState, AV36DynamicFiltersIgnoreFirst, AV48ContagemResultado_DemandaFM1, AV24ContagemResultado_DataDmn_To1, AV23ContagemResultado_DataDmn1, AV50ContagemResultado_DataEntregaReal_To1, AV49ContagemResultado_DataEntregaReal1, AV51ContagemResultado_StatusDmn1, AV52ContagemResultado_StatusCnt1, AV53ContagemResultado_Descricao1, AV54ContagemResultado_SistemaCod1, AV22DynamicFiltersOperator1, AV55ContagemResultado_Agrupador1, AV35DynamicFiltersRemoving, AV56ContagemResultado_DemandaFM2, AV29ContagemResultado_DataDmn_To2, AV28ContagemResultado_DataDmn2, AV58ContagemResultado_DataEntregaReal_To2, AV57ContagemResultado_DataEntregaReal2, AV59ContagemResultado_StatusDmn2, AV60ContagemResultado_StatusCnt2, AV61ContagemResultado_Descricao2, AV62ContagemResultado_SistemaCod2, AV27DynamicFiltersOperator2, AV63ContagemResultado_Agrupador2, AV64ContagemResultado_DemandaFM3, AV34ContagemResultado_DataDmn_To3, AV33ContagemResultado_DataDmn3, AV66ContagemResultado_DataEntregaReal_To3, AV65ContagemResultado_DataEntregaReal3, AV67ContagemResultado_StatusDmn3, AV68ContagemResultado_StatusCnt3, AV69ContagemResultado_Descricao3, AV70ContagemResultado_SistemaCod3, AV32DynamicFiltersOperator3, AV71ContagemResultado_Agrupador3, A457ContagemResultado_Demanda, A52Contratada_AreaTrabalhoCod, AV6WWPContext, A490ContagemResultado_ContratadaCod, A764ContagemResultado_ServicoGrupo, A601ContagemResultado_Servico, A602ContagemResultado_OSVinculada, A493ContagemResultado_DemandaFM, A471ContagemResultado_DataDmn, A2017ContagemResultado_DataEntregaReal, A484ContagemResultado_StatusDmn, A494ContagemResultado_Descricao, A489ContagemResultado_SistemaCod, A1046ContagemResultado_Agrupador, A456ContagemResultado_Codigo, A509ContagemrResultado_SistemaSigla, A1326ContagemResultado_ContratadaTipoFab, A682ContagemResultado_PFBFMUltima, A684ContagemResultado_PFBFSUltima, AV14ContagemResultado_OSVinculada) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         subGrid_Islastpage = 1;
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV21DynamicFiltersSelector1, AV25DynamicFiltersEnabled2, AV26DynamicFiltersSelector2, AV30DynamicFiltersEnabled3, AV31DynamicFiltersSelector3, AV192Pgmname, AV13Contratada_AreaTrabalhoCod, AV15ContagemResultado_ContratadaCod, AV16ContagemResultado_ServicoGrupo, AV17ContagemResultado_Servico, AV18ContagemResultado_CntadaOsVinc, AV19ContagemResultado_SerGrupoVinc, AV20ContagemResultado_CodSrvVnc, AV10GridState, AV36DynamicFiltersIgnoreFirst, AV48ContagemResultado_DemandaFM1, AV24ContagemResultado_DataDmn_To1, AV23ContagemResultado_DataDmn1, AV50ContagemResultado_DataEntregaReal_To1, AV49ContagemResultado_DataEntregaReal1, AV51ContagemResultado_StatusDmn1, AV52ContagemResultado_StatusCnt1, AV53ContagemResultado_Descricao1, AV54ContagemResultado_SistemaCod1, AV22DynamicFiltersOperator1, AV55ContagemResultado_Agrupador1, AV35DynamicFiltersRemoving, AV56ContagemResultado_DemandaFM2, AV29ContagemResultado_DataDmn_To2, AV28ContagemResultado_DataDmn2, AV58ContagemResultado_DataEntregaReal_To2, AV57ContagemResultado_DataEntregaReal2, AV59ContagemResultado_StatusDmn2, AV60ContagemResultado_StatusCnt2, AV61ContagemResultado_Descricao2, AV62ContagemResultado_SistemaCod2, AV27DynamicFiltersOperator2, AV63ContagemResultado_Agrupador2, AV64ContagemResultado_DemandaFM3, AV34ContagemResultado_DataDmn_To3, AV33ContagemResultado_DataDmn3, AV66ContagemResultado_DataEntregaReal_To3, AV65ContagemResultado_DataEntregaReal3, AV67ContagemResultado_StatusDmn3, AV68ContagemResultado_StatusCnt3, AV69ContagemResultado_Descricao3, AV70ContagemResultado_SistemaCod3, AV32DynamicFiltersOperator3, AV71ContagemResultado_Agrupador3, A457ContagemResultado_Demanda, A52Contratada_AreaTrabalhoCod, AV6WWPContext, A490ContagemResultado_ContratadaCod, A764ContagemResultado_ServicoGrupo, A601ContagemResultado_Servico, A602ContagemResultado_OSVinculada, A493ContagemResultado_DemandaFM, A471ContagemResultado_DataDmn, A2017ContagemResultado_DataEntregaReal, A484ContagemResultado_StatusDmn, A494ContagemResultado_Descricao, A489ContagemResultado_SistemaCod, A1046ContagemResultado_Agrupador, A456ContagemResultado_Codigo, A509ContagemrResultado_SistemaSigla, A1326ContagemResultado_ContratadaTipoFab, A682ContagemResultado_PFBFMUltima, A684ContagemResultado_PFBFSUltima, AV14ContagemResultado_OSVinculada) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV21DynamicFiltersSelector1, AV25DynamicFiltersEnabled2, AV26DynamicFiltersSelector2, AV30DynamicFiltersEnabled3, AV31DynamicFiltersSelector3, AV192Pgmname, AV13Contratada_AreaTrabalhoCod, AV15ContagemResultado_ContratadaCod, AV16ContagemResultado_ServicoGrupo, AV17ContagemResultado_Servico, AV18ContagemResultado_CntadaOsVinc, AV19ContagemResultado_SerGrupoVinc, AV20ContagemResultado_CodSrvVnc, AV10GridState, AV36DynamicFiltersIgnoreFirst, AV48ContagemResultado_DemandaFM1, AV24ContagemResultado_DataDmn_To1, AV23ContagemResultado_DataDmn1, AV50ContagemResultado_DataEntregaReal_To1, AV49ContagemResultado_DataEntregaReal1, AV51ContagemResultado_StatusDmn1, AV52ContagemResultado_StatusCnt1, AV53ContagemResultado_Descricao1, AV54ContagemResultado_SistemaCod1, AV22DynamicFiltersOperator1, AV55ContagemResultado_Agrupador1, AV35DynamicFiltersRemoving, AV56ContagemResultado_DemandaFM2, AV29ContagemResultado_DataDmn_To2, AV28ContagemResultado_DataDmn2, AV58ContagemResultado_DataEntregaReal_To2, AV57ContagemResultado_DataEntregaReal2, AV59ContagemResultado_StatusDmn2, AV60ContagemResultado_StatusCnt2, AV61ContagemResultado_Descricao2, AV62ContagemResultado_SistemaCod2, AV27DynamicFiltersOperator2, AV63ContagemResultado_Agrupador2, AV64ContagemResultado_DemandaFM3, AV34ContagemResultado_DataDmn_To3, AV33ContagemResultado_DataDmn3, AV66ContagemResultado_DataEntregaReal_To3, AV65ContagemResultado_DataEntregaReal3, AV67ContagemResultado_StatusDmn3, AV68ContagemResultado_StatusCnt3, AV69ContagemResultado_Descricao3, AV70ContagemResultado_SistemaCod3, AV32DynamicFiltersOperator3, AV71ContagemResultado_Agrupador3, A457ContagemResultado_Demanda, A52Contratada_AreaTrabalhoCod, AV6WWPContext, A490ContagemResultado_ContratadaCod, A764ContagemResultado_ServicoGrupo, A601ContagemResultado_Servico, A602ContagemResultado_OSVinculada, A493ContagemResultado_DemandaFM, A471ContagemResultado_DataDmn, A2017ContagemResultado_DataEntregaReal, A484ContagemResultado_StatusDmn, A494ContagemResultado_Descricao, A489ContagemResultado_SistemaCod, A1046ContagemResultado_Agrupador, A456ContagemResultado_Codigo, A509ContagemrResultado_SistemaSigla, A1326ContagemResultado_ContratadaTipoFab, A682ContagemResultado_PFBFMUltima, A684ContagemResultado_PFBFSUltima, AV14ContagemResultado_OSVinculada) ;
         }
         return (int)(0) ;
      }

      protected void STRUPTR0( )
      {
         /* Before Start, stand alone formulas. */
         AV192Pgmname = "WP_RelatorioComparacaoDemandas_OLD";
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         edtavContagemresultado_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_codigo_Enabled), 5, 0)));
         edtavContagemresultado_osvinculada_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_osvinculada_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_osvinculada_Enabled), 5, 0)));
         cmbavContagemresultado_contratadatipofab.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_contratadatipofab_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContagemresultado_contratadatipofab.Enabled), 5, 0)));
         edtavContagemresultado_pfbfmultima_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_pfbfmultima_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_pfbfmultima_Enabled), 5, 0)));
         edtavContagemresultado_pfbfsultima_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_pfbfsultima_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_pfbfsultima_Enabled), 5, 0)));
         edtavEsforco_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEsforco_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEsforco_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E24TR2 */
         E24TR2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         GXVvCONTAGEMRESULTADO_CONTRATADACOD_htmlTR2( AV6WWPContext) ;
         GXVvCONTAGEMRESULTADO_CNTADAOSVINC_htmlTR2( AV6WWPContext) ;
         GXVvCONTAGEMRESULTADO_SISTEMACOD1_htmlTR2( AV6WWPContext) ;
         GXVvCONTAGEMRESULTADO_SISTEMACOD2_htmlTR2( AV6WWPContext) ;
         GXVvCONTAGEMRESULTADO_SISTEMACOD3_htmlTR2( AV6WWPContext) ;
         GXVvCONTAGEMRESULTADO_SERVICOGRUPO_htmlTR2( AV15ContagemResultado_ContratadaCod) ;
         GXVvCONTAGEMRESULTADO_SERVICO_htmlTR2( AV15ContagemResultado_ContratadaCod, AV16ContagemResultado_ServicoGrupo) ;
         GXVvCONTAGEMRESULTADO_SERGRUPOVINC_htmlTR2( AV18ContagemResultado_CntadaOsVinc) ;
         GXVvCONTAGEMRESULTADO_CODSRVVNC_htmlTR2( AV18ContagemResultado_CntadaOsVinc, AV19ContagemResultado_SerGrupoVinc) ;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContratada_areatrabalhocod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContratada_areatrabalhocod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATADA_AREATRABALHOCOD");
               GX_FocusControl = edtavContratada_areatrabalhocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV13Contratada_AreaTrabalhoCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Contratada_AreaTrabalhoCod), 6, 0)));
            }
            else
            {
               AV13Contratada_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtavContratada_areatrabalhocod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Contratada_AreaTrabalhoCod), 6, 0)));
            }
            dynavContagemresultado_contratadacod.Name = dynavContagemresultado_contratadacod_Internalname;
            dynavContagemresultado_contratadacod.CurrentValue = cgiGet( dynavContagemresultado_contratadacod_Internalname);
            AV15ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_contratadacod_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15ContagemResultado_ContratadaCod), 6, 0)));
            dynavContagemresultado_servicogrupo.Name = dynavContagemresultado_servicogrupo_Internalname;
            dynavContagemresultado_servicogrupo.CurrentValue = cgiGet( dynavContagemresultado_servicogrupo_Internalname);
            AV16ContagemResultado_ServicoGrupo = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_servicogrupo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContagemResultado_ServicoGrupo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16ContagemResultado_ServicoGrupo), 6, 0)));
            dynavContagemresultado_servico.Name = dynavContagemresultado_servico_Internalname;
            dynavContagemresultado_servico.CurrentValue = cgiGet( dynavContagemresultado_servico_Internalname);
            AV17ContagemResultado_Servico = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_servico_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContagemResultado_Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17ContagemResultado_Servico), 6, 0)));
            dynavContagemresultado_cntadaosvinc.Name = dynavContagemresultado_cntadaosvinc_Internalname;
            dynavContagemresultado_cntadaosvinc.CurrentValue = cgiGet( dynavContagemresultado_cntadaosvinc_Internalname);
            AV18ContagemResultado_CntadaOsVinc = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_cntadaosvinc_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultado_CntadaOsVinc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ContagemResultado_CntadaOsVinc), 6, 0)));
            dynavContagemresultado_sergrupovinc.Name = dynavContagemresultado_sergrupovinc_Internalname;
            dynavContagemresultado_sergrupovinc.CurrentValue = cgiGet( dynavContagemresultado_sergrupovinc_Internalname);
            AV19ContagemResultado_SerGrupoVinc = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_sergrupovinc_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultado_SerGrupoVinc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19ContagemResultado_SerGrupoVinc), 6, 0)));
            dynavContagemresultado_codsrvvnc.Name = dynavContagemresultado_codsrvvnc_Internalname;
            dynavContagemresultado_codsrvvnc.CurrentValue = cgiGet( dynavContagemresultado_codsrvvnc_Internalname);
            AV20ContagemResultado_CodSrvVnc = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_codsrvvnc_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContagemResultado_CodSrvVnc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20ContagemResultado_CodSrvVnc), 6, 0)));
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV21DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector1", AV21DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV22DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator1), 4, 0)));
            AV48ContagemResultado_DemandaFM1 = cgiGet( edtavContagemresultado_demandafm1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ContagemResultado_DemandaFM1", AV48ContagemResultado_DemandaFM1);
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_datadmn1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado_Data Dmn1"}), 1, "vCONTAGEMRESULTADO_DATADMN1");
               GX_FocusControl = edtavContagemresultado_datadmn1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV23ContagemResultado_DataDmn1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemResultado_DataDmn1", context.localUtil.Format(AV23ContagemResultado_DataDmn1, "99/99/99"));
            }
            else
            {
               AV23ContagemResultado_DataDmn1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_datadmn1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemResultado_DataDmn1", context.localUtil.Format(AV23ContagemResultado_DataDmn1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_datadmn_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado_Data Dmn_To1"}), 1, "vCONTAGEMRESULTADO_DATADMN_TO1");
               GX_FocusControl = edtavContagemresultado_datadmn_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV24ContagemResultado_DataDmn_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultado_DataDmn_To1", context.localUtil.Format(AV24ContagemResultado_DataDmn_To1, "99/99/99"));
            }
            else
            {
               AV24ContagemResultado_DataDmn_To1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_datadmn_to1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultado_DataDmn_To1", context.localUtil.Format(AV24ContagemResultado_DataDmn_To1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_dataentregareal1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado_Data Entrega Real1"}), 1, "vCONTAGEMRESULTADO_DATAENTREGAREAL1");
               GX_FocusControl = edtavContagemresultado_dataentregareal1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV49ContagemResultado_DataEntregaReal1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ContagemResultado_DataEntregaReal1", context.localUtil.TToC( AV49ContagemResultado_DataEntregaReal1, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV49ContagemResultado_DataEntregaReal1 = context.localUtil.CToT( cgiGet( edtavContagemresultado_dataentregareal1_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ContagemResultado_DataEntregaReal1", context.localUtil.TToC( AV49ContagemResultado_DataEntregaReal1, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_dataentregareal_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado_Data Entrega Real_To1"}), 1, "vCONTAGEMRESULTADO_DATAENTREGAREAL_TO1");
               GX_FocusControl = edtavContagemresultado_dataentregareal_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV50ContagemResultado_DataEntregaReal_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ContagemResultado_DataEntregaReal_To1", context.localUtil.TToC( AV50ContagemResultado_DataEntregaReal_To1, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV50ContagemResultado_DataEntregaReal_To1 = context.localUtil.CToT( cgiGet( edtavContagemresultado_dataentregareal_to1_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ContagemResultado_DataEntregaReal_To1", context.localUtil.TToC( AV50ContagemResultado_DataEntregaReal_To1, 8, 5, 0, 3, "/", ":", " "));
            }
            cmbavContagemresultado_statusdmn1.Name = cmbavContagemresultado_statusdmn1_Internalname;
            cmbavContagemresultado_statusdmn1.CurrentValue = cgiGet( cmbavContagemresultado_statusdmn1_Internalname);
            AV51ContagemResultado_StatusDmn1 = cgiGet( cmbavContagemresultado_statusdmn1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ContagemResultado_StatusDmn1", AV51ContagemResultado_StatusDmn1);
            cmbavContagemresultado_statuscnt1.Name = cmbavContagemresultado_statuscnt1_Internalname;
            cmbavContagemresultado_statuscnt1.CurrentValue = cgiGet( cmbavContagemresultado_statuscnt1_Internalname);
            AV52ContagemResultado_StatusCnt1 = (short)(NumberUtil.Val( cgiGet( cmbavContagemresultado_statuscnt1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ContagemResultado_StatusCnt1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52ContagemResultado_StatusCnt1), 2, 0)));
            AV53ContagemResultado_Descricao1 = cgiGet( edtavContagemresultado_descricao1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ContagemResultado_Descricao1", AV53ContagemResultado_Descricao1);
            dynavContagemresultado_sistemacod1.Name = dynavContagemresultado_sistemacod1_Internalname;
            dynavContagemresultado_sistemacod1.CurrentValue = cgiGet( dynavContagemresultado_sistemacod1_Internalname);
            AV54ContagemResultado_SistemaCod1 = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_sistemacod1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ContagemResultado_SistemaCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54ContagemResultado_SistemaCod1), 6, 0)));
            AV55ContagemResultado_Agrupador1 = StringUtil.Upper( cgiGet( edtavContagemresultado_agrupador1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55ContagemResultado_Agrupador1", AV55ContagemResultado_Agrupador1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV26DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector2", AV26DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV27DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator2), 4, 0)));
            AV56ContagemResultado_DemandaFM2 = cgiGet( edtavContagemresultado_demandafm2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ContagemResultado_DemandaFM2", AV56ContagemResultado_DemandaFM2);
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_datadmn2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado_Data Dmn2"}), 1, "vCONTAGEMRESULTADO_DATADMN2");
               GX_FocusControl = edtavContagemresultado_datadmn2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV28ContagemResultado_DataDmn2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ContagemResultado_DataDmn2", context.localUtil.Format(AV28ContagemResultado_DataDmn2, "99/99/99"));
            }
            else
            {
               AV28ContagemResultado_DataDmn2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_datadmn2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ContagemResultado_DataDmn2", context.localUtil.Format(AV28ContagemResultado_DataDmn2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_datadmn_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado_Data Dmn_To2"}), 1, "vCONTAGEMRESULTADO_DATADMN_TO2");
               GX_FocusControl = edtavContagemresultado_datadmn_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV29ContagemResultado_DataDmn_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContagemResultado_DataDmn_To2", context.localUtil.Format(AV29ContagemResultado_DataDmn_To2, "99/99/99"));
            }
            else
            {
               AV29ContagemResultado_DataDmn_To2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_datadmn_to2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContagemResultado_DataDmn_To2", context.localUtil.Format(AV29ContagemResultado_DataDmn_To2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_dataentregareal2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado_Data Entrega Real2"}), 1, "vCONTAGEMRESULTADO_DATAENTREGAREAL2");
               GX_FocusControl = edtavContagemresultado_dataentregareal2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV57ContagemResultado_DataEntregaReal2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ContagemResultado_DataEntregaReal2", context.localUtil.TToC( AV57ContagemResultado_DataEntregaReal2, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV57ContagemResultado_DataEntregaReal2 = context.localUtil.CToT( cgiGet( edtavContagemresultado_dataentregareal2_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ContagemResultado_DataEntregaReal2", context.localUtil.TToC( AV57ContagemResultado_DataEntregaReal2, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_dataentregareal_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado_Data Entrega Real_To2"}), 1, "vCONTAGEMRESULTADO_DATAENTREGAREAL_TO2");
               GX_FocusControl = edtavContagemresultado_dataentregareal_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV58ContagemResultado_DataEntregaReal_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58ContagemResultado_DataEntregaReal_To2", context.localUtil.TToC( AV58ContagemResultado_DataEntregaReal_To2, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV58ContagemResultado_DataEntregaReal_To2 = context.localUtil.CToT( cgiGet( edtavContagemresultado_dataentregareal_to2_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58ContagemResultado_DataEntregaReal_To2", context.localUtil.TToC( AV58ContagemResultado_DataEntregaReal_To2, 8, 5, 0, 3, "/", ":", " "));
            }
            cmbavContagemresultado_statusdmn2.Name = cmbavContagemresultado_statusdmn2_Internalname;
            cmbavContagemresultado_statusdmn2.CurrentValue = cgiGet( cmbavContagemresultado_statusdmn2_Internalname);
            AV59ContagemResultado_StatusDmn2 = cgiGet( cmbavContagemresultado_statusdmn2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59ContagemResultado_StatusDmn2", AV59ContagemResultado_StatusDmn2);
            cmbavContagemresultado_statuscnt2.Name = cmbavContagemresultado_statuscnt2_Internalname;
            cmbavContagemresultado_statuscnt2.CurrentValue = cgiGet( cmbavContagemresultado_statuscnt2_Internalname);
            AV60ContagemResultado_StatusCnt2 = (short)(NumberUtil.Val( cgiGet( cmbavContagemresultado_statuscnt2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60ContagemResultado_StatusCnt2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60ContagemResultado_StatusCnt2), 2, 0)));
            AV61ContagemResultado_Descricao2 = cgiGet( edtavContagemresultado_descricao2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ContagemResultado_Descricao2", AV61ContagemResultado_Descricao2);
            dynavContagemresultado_sistemacod2.Name = dynavContagemresultado_sistemacod2_Internalname;
            dynavContagemresultado_sistemacod2.CurrentValue = cgiGet( dynavContagemresultado_sistemacod2_Internalname);
            AV62ContagemResultado_SistemaCod2 = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_sistemacod2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62ContagemResultado_SistemaCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62ContagemResultado_SistemaCod2), 6, 0)));
            AV63ContagemResultado_Agrupador2 = StringUtil.Upper( cgiGet( edtavContagemresultado_agrupador2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ContagemResultado_Agrupador2", AV63ContagemResultado_Agrupador2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV31DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31DynamicFiltersSelector3", AV31DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV32DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32DynamicFiltersOperator3), 4, 0)));
            AV64ContagemResultado_DemandaFM3 = cgiGet( edtavContagemresultado_demandafm3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64ContagemResultado_DemandaFM3", AV64ContagemResultado_DemandaFM3);
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_datadmn3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado_Data Dmn3"}), 1, "vCONTAGEMRESULTADO_DATADMN3");
               GX_FocusControl = edtavContagemresultado_datadmn3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV33ContagemResultado_DataDmn3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContagemResultado_DataDmn3", context.localUtil.Format(AV33ContagemResultado_DataDmn3, "99/99/99"));
            }
            else
            {
               AV33ContagemResultado_DataDmn3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_datadmn3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContagemResultado_DataDmn3", context.localUtil.Format(AV33ContagemResultado_DataDmn3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_datadmn_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado_Data Dmn_To3"}), 1, "vCONTAGEMRESULTADO_DATADMN_TO3");
               GX_FocusControl = edtavContagemresultado_datadmn_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV34ContagemResultado_DataDmn_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContagemResultado_DataDmn_To3", context.localUtil.Format(AV34ContagemResultado_DataDmn_To3, "99/99/99"));
            }
            else
            {
               AV34ContagemResultado_DataDmn_To3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_datadmn_to3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContagemResultado_DataDmn_To3", context.localUtil.Format(AV34ContagemResultado_DataDmn_To3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_dataentregareal3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado_Data Entrega Real3"}), 1, "vCONTAGEMRESULTADO_DATAENTREGAREAL3");
               GX_FocusControl = edtavContagemresultado_dataentregareal3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV65ContagemResultado_DataEntregaReal3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ContagemResultado_DataEntregaReal3", context.localUtil.TToC( AV65ContagemResultado_DataEntregaReal3, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV65ContagemResultado_DataEntregaReal3 = context.localUtil.CToT( cgiGet( edtavContagemresultado_dataentregareal3_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ContagemResultado_DataEntregaReal3", context.localUtil.TToC( AV65ContagemResultado_DataEntregaReal3, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_dataentregareal_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado_Data Entrega Real_To3"}), 1, "vCONTAGEMRESULTADO_DATAENTREGAREAL_TO3");
               GX_FocusControl = edtavContagemresultado_dataentregareal_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV66ContagemResultado_DataEntregaReal_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ContagemResultado_DataEntregaReal_To3", context.localUtil.TToC( AV66ContagemResultado_DataEntregaReal_To3, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV66ContagemResultado_DataEntregaReal_To3 = context.localUtil.CToT( cgiGet( edtavContagemresultado_dataentregareal_to3_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ContagemResultado_DataEntregaReal_To3", context.localUtil.TToC( AV66ContagemResultado_DataEntregaReal_To3, 8, 5, 0, 3, "/", ":", " "));
            }
            cmbavContagemresultado_statusdmn3.Name = cmbavContagemresultado_statusdmn3_Internalname;
            cmbavContagemresultado_statusdmn3.CurrentValue = cgiGet( cmbavContagemresultado_statusdmn3_Internalname);
            AV67ContagemResultado_StatusDmn3 = cgiGet( cmbavContagemresultado_statusdmn3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ContagemResultado_StatusDmn3", AV67ContagemResultado_StatusDmn3);
            cmbavContagemresultado_statuscnt3.Name = cmbavContagemresultado_statuscnt3_Internalname;
            cmbavContagemresultado_statuscnt3.CurrentValue = cgiGet( cmbavContagemresultado_statuscnt3_Internalname);
            AV68ContagemResultado_StatusCnt3 = (short)(NumberUtil.Val( cgiGet( cmbavContagemresultado_statuscnt3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ContagemResultado_StatusCnt3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68ContagemResultado_StatusCnt3), 2, 0)));
            AV69ContagemResultado_Descricao3 = cgiGet( edtavContagemresultado_descricao3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ContagemResultado_Descricao3", AV69ContagemResultado_Descricao3);
            dynavContagemresultado_sistemacod3.Name = dynavContagemresultado_sistemacod3_Internalname;
            dynavContagemresultado_sistemacod3.CurrentValue = cgiGet( dynavContagemresultado_sistemacod3_Internalname);
            AV70ContagemResultado_SistemaCod3 = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_sistemacod3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70ContagemResultado_SistemaCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV70ContagemResultado_SistemaCod3), 6, 0)));
            AV71ContagemResultado_Agrupador3 = StringUtil.Upper( cgiGet( edtavContagemresultado_agrupador3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ContagemResultado_Agrupador3", AV71ContagemResultado_Agrupador3);
            AV25DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersEnabled2", AV25DynamicFiltersEnabled2);
            AV30DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersEnabled3", AV30DynamicFiltersEnabled3);
            /* Read saved values. */
            nRC_GXsfl_187 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_187"), ",", "."));
            AV45GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV46GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E24TR2 */
         E24TR2 ();
         if (returnInSub) return;
      }

      protected void E24TR2( )
      {
         /* Start Routine */
         if ( 1 == 2 )
         {
            context.wjLoc = formatLink("wp_relatoriocomparacaodemandasmodelo.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         Form.Meta.addItem("Versao", "1.1 - Data: 18/04/2020 23:34", 0) ;
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV51ContagemResultado_StatusDmn1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ContagemResultado_StatusDmn1", AV51ContagemResultado_StatusDmn1);
         AV52ContagemResultado_StatusCnt1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ContagemResultado_StatusCnt1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52ContagemResultado_StatusCnt1), 2, 0)));
         AV21DynamicFiltersSelector1 = "CONTAGEMRESULTADO_DEMANDAFM";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector1", AV21DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if (returnInSub) return;
         AV59ContagemResultado_StatusDmn2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59ContagemResultado_StatusDmn2", AV59ContagemResultado_StatusDmn2);
         AV60ContagemResultado_StatusCnt2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60ContagemResultado_StatusCnt2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60ContagemResultado_StatusCnt2), 2, 0)));
         AV26DynamicFiltersSelector2 = "CONTAGEMRESULTADO_DEMANDAFM";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector2", AV26DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if (returnInSub) return;
         AV67ContagemResultado_StatusDmn3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ContagemResultado_StatusDmn3", AV67ContagemResultado_StatusDmn3);
         AV68ContagemResultado_StatusCnt3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ContagemResultado_StatusCnt3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68ContagemResultado_StatusCnt3), 2, 0)));
         AV31DynamicFiltersSelector3 = "CONTAGEMRESULTADO_DEMANDAFM";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31DynamicFiltersSelector3", AV31DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if (returnInSub) return;
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         Form.Caption = " Resultado das Contagens";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if (returnInSub) return;
         if ( ! (0==AV6WWPContext.gxTpr_Contratada_codigo) )
         {
            AV15ContagemResultado_ContratadaCod = AV6WWPContext.gxTpr_Contratada_codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15ContagemResultado_ContratadaCod), 6, 0)));
            dynavContagemresultado_contratadacod.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contratadacod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_contratadacod.Enabled), 5, 0)));
            AV18ContagemResultado_CntadaOsVinc = AV6WWPContext.gxTpr_Contratada_codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultado_CntadaOsVinc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ContagemResultado_CntadaOsVinc), 6, 0)));
            dynavContagemresultado_cntadaosvinc.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_cntadaosvinc_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_cntadaosvinc.Enabled), 5, 0)));
         }
      }

      protected void E25TR2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         cmbavDynamicfiltersoperator1.removeAllItems();
         if ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Igual", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("2", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Per�odo", 0);
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Per�odo", 0);
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            cmbavDynamicfiltersoperator1.addItem("2", "Igual", 0);
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Igual", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("2", "Cont�m", 0);
         }
         if ( AV25DynamicFiltersEnabled2 )
         {
            cmbavDynamicfiltersoperator2.removeAllItems();
            if ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Igual", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("2", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Per�odo", 0);
            }
            else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Per�odo", 0);
            }
            else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
               cmbavDynamicfiltersoperator2.addItem("2", "Igual", 0);
            }
            else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Igual", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("2", "Cont�m", 0);
            }
            if ( AV30DynamicFiltersEnabled3 )
            {
               cmbavDynamicfiltersoperator3.removeAllItems();
               if ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Igual", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("2", "Cont�m", 0);
               }
               else if ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Per�odo", 0);
               }
               else if ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Per�odo", 0);
               }
               else if ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
                  cmbavDynamicfiltersoperator3.addItem("2", "Igual", 0);
               }
               else if ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Igual", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("2", "Cont�m", 0);
               }
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if (returnInSub) return;
         AV45GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45GridCurrentPage), 10, 0)));
         AV46GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV32DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11TR2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV44PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV44PageToGo) ;
         }
      }

      private void E26TR2( )
      {
         /* Grid_Load Routine */
         pr_default.dynParam(9, new Object[]{ new Object[]{
                                              AV16ContagemResultado_ServicoGrupo ,
                                              AV17ContagemResultado_Servico ,
                                              AV21DynamicFiltersSelector1 ,
                                              AV22DynamicFiltersOperator1 ,
                                              AV48ContagemResultado_DemandaFM1 ,
                                              AV23ContagemResultado_DataDmn1 ,
                                              AV24ContagemResultado_DataDmn_To1 ,
                                              AV49ContagemResultado_DataEntregaReal1 ,
                                              AV50ContagemResultado_DataEntregaReal_To1 ,
                                              AV51ContagemResultado_StatusDmn1 ,
                                              AV53ContagemResultado_Descricao1 ,
                                              AV54ContagemResultado_SistemaCod1 ,
                                              AV55ContagemResultado_Agrupador1 ,
                                              AV25DynamicFiltersEnabled2 ,
                                              AV26DynamicFiltersSelector2 ,
                                              AV27DynamicFiltersOperator2 ,
                                              AV56ContagemResultado_DemandaFM2 ,
                                              AV28ContagemResultado_DataDmn2 ,
                                              AV29ContagemResultado_DataDmn_To2 ,
                                              AV57ContagemResultado_DataEntregaReal2 ,
                                              AV58ContagemResultado_DataEntregaReal_To2 ,
                                              AV59ContagemResultado_StatusDmn2 ,
                                              AV61ContagemResultado_Descricao2 ,
                                              AV62ContagemResultado_SistemaCod2 ,
                                              AV63ContagemResultado_Agrupador2 ,
                                              AV30DynamicFiltersEnabled3 ,
                                              AV31DynamicFiltersSelector3 ,
                                              AV32DynamicFiltersOperator3 ,
                                              AV64ContagemResultado_DemandaFM3 ,
                                              AV33ContagemResultado_DataDmn3 ,
                                              AV34ContagemResultado_DataDmn_To3 ,
                                              AV65ContagemResultado_DataEntregaReal3 ,
                                              AV66ContagemResultado_DataEntregaReal_To3 ,
                                              AV67ContagemResultado_StatusDmn3 ,
                                              AV69ContagemResultado_Descricao3 ,
                                              AV70ContagemResultado_SistemaCod3 ,
                                              AV71ContagemResultado_Agrupador3 ,
                                              A764ContagemResultado_ServicoGrupo ,
                                              A601ContagemResultado_Servico ,
                                              A493ContagemResultado_DemandaFM ,
                                              A471ContagemResultado_DataDmn ,
                                              A2017ContagemResultado_DataEntregaReal ,
                                              A484ContagemResultado_StatusDmn ,
                                              A494ContagemResultado_Descricao ,
                                              A489ContagemResultado_SistemaCod ,
                                              A1046ContagemResultado_Agrupador ,
                                              A602ContagemResultado_OSVinculada ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV6WWPContext.gxTpr_Areatrabalho_codigo ,
                                              A490ContagemResultado_ContratadaCod ,
                                              AV15ContagemResultado_ContratadaCod },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV48ContagemResultado_DemandaFM1 = StringUtil.Concat( StringUtil.RTrim( AV48ContagemResultado_DemandaFM1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ContagemResultado_DemandaFM1", AV48ContagemResultado_DemandaFM1);
         lV48ContagemResultado_DemandaFM1 = StringUtil.Concat( StringUtil.RTrim( AV48ContagemResultado_DemandaFM1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ContagemResultado_DemandaFM1", AV48ContagemResultado_DemandaFM1);
         lV53ContagemResultado_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV53ContagemResultado_Descricao1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ContagemResultado_Descricao1", AV53ContagemResultado_Descricao1);
         lV53ContagemResultado_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV53ContagemResultado_Descricao1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ContagemResultado_Descricao1", AV53ContagemResultado_Descricao1);
         lV55ContagemResultado_Agrupador1 = StringUtil.PadR( StringUtil.RTrim( AV55ContagemResultado_Agrupador1), 15, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55ContagemResultado_Agrupador1", AV55ContagemResultado_Agrupador1);
         lV55ContagemResultado_Agrupador1 = StringUtil.PadR( StringUtil.RTrim( AV55ContagemResultado_Agrupador1), 15, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55ContagemResultado_Agrupador1", AV55ContagemResultado_Agrupador1);
         lV56ContagemResultado_DemandaFM2 = StringUtil.Concat( StringUtil.RTrim( AV56ContagemResultado_DemandaFM2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ContagemResultado_DemandaFM2", AV56ContagemResultado_DemandaFM2);
         lV56ContagemResultado_DemandaFM2 = StringUtil.Concat( StringUtil.RTrim( AV56ContagemResultado_DemandaFM2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ContagemResultado_DemandaFM2", AV56ContagemResultado_DemandaFM2);
         lV61ContagemResultado_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV61ContagemResultado_Descricao2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ContagemResultado_Descricao2", AV61ContagemResultado_Descricao2);
         lV61ContagemResultado_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV61ContagemResultado_Descricao2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ContagemResultado_Descricao2", AV61ContagemResultado_Descricao2);
         lV63ContagemResultado_Agrupador2 = StringUtil.PadR( StringUtil.RTrim( AV63ContagemResultado_Agrupador2), 15, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ContagemResultado_Agrupador2", AV63ContagemResultado_Agrupador2);
         lV63ContagemResultado_Agrupador2 = StringUtil.PadR( StringUtil.RTrim( AV63ContagemResultado_Agrupador2), 15, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ContagemResultado_Agrupador2", AV63ContagemResultado_Agrupador2);
         lV64ContagemResultado_DemandaFM3 = StringUtil.Concat( StringUtil.RTrim( AV64ContagemResultado_DemandaFM3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64ContagemResultado_DemandaFM3", AV64ContagemResultado_DemandaFM3);
         lV64ContagemResultado_DemandaFM3 = StringUtil.Concat( StringUtil.RTrim( AV64ContagemResultado_DemandaFM3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64ContagemResultado_DemandaFM3", AV64ContagemResultado_DemandaFM3);
         lV69ContagemResultado_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV69ContagemResultado_Descricao3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ContagemResultado_Descricao3", AV69ContagemResultado_Descricao3);
         lV69ContagemResultado_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV69ContagemResultado_Descricao3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ContagemResultado_Descricao3", AV69ContagemResultado_Descricao3);
         lV71ContagemResultado_Agrupador3 = StringUtil.PadR( StringUtil.RTrim( AV71ContagemResultado_Agrupador3), 15, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ContagemResultado_Agrupador3", AV71ContagemResultado_Agrupador3);
         lV71ContagemResultado_Agrupador3 = StringUtil.PadR( StringUtil.RTrim( AV71ContagemResultado_Agrupador3), 15, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ContagemResultado_Agrupador3", AV71ContagemResultado_Agrupador3);
         /* Using cursor H00TR12 */
         pr_default.execute(9, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, AV15ContagemResultado_ContratadaCod, AV16ContagemResultado_ServicoGrupo, AV17ContagemResultado_Servico, AV48ContagemResultado_DemandaFM1, lV48ContagemResultado_DemandaFM1, lV48ContagemResultado_DemandaFM1, AV23ContagemResultado_DataDmn1, AV24ContagemResultado_DataDmn_To1, AV49ContagemResultado_DataEntregaReal1, AV49ContagemResultado_DataEntregaReal1, AV50ContagemResultado_DataEntregaReal_To1, AV50ContagemResultado_DataEntregaReal_To1, AV51ContagemResultado_StatusDmn1, lV53ContagemResultado_Descricao1, lV53ContagemResultado_Descricao1, AV53ContagemResultado_Descricao1, AV54ContagemResultado_SistemaCod1, AV54ContagemResultado_SistemaCod1, AV54ContagemResultado_SistemaCod1, AV55ContagemResultado_Agrupador1, lV55ContagemResultado_Agrupador1, lV55ContagemResultado_Agrupador1, AV56ContagemResultado_DemandaFM2, lV56ContagemResultado_DemandaFM2, lV56ContagemResultado_DemandaFM2, AV28ContagemResultado_DataDmn2, AV29ContagemResultado_DataDmn_To2, AV57ContagemResultado_DataEntregaReal2, AV57ContagemResultado_DataEntregaReal2, AV58ContagemResultado_DataEntregaReal_To2, AV58ContagemResultado_DataEntregaReal_To2, AV59ContagemResultado_StatusDmn2, lV61ContagemResultado_Descricao2, lV61ContagemResultado_Descricao2, AV61ContagemResultado_Descricao2, AV62ContagemResultado_SistemaCod2, AV62ContagemResultado_SistemaCod2, AV62ContagemResultado_SistemaCod2, AV63ContagemResultado_Agrupador2, lV63ContagemResultado_Agrupador2, lV63ContagemResultado_Agrupador2, AV64ContagemResultado_DemandaFM3, lV64ContagemResultado_DemandaFM3, lV64ContagemResultado_DemandaFM3, AV33ContagemResultado_DataDmn3, AV34ContagemResultado_DataDmn_To3, AV65ContagemResultado_DataEntregaReal3, AV65ContagemResultado_DataEntregaReal3, AV66ContagemResultado_DataEntregaReal_To3, AV66ContagemResultado_DataEntregaReal_To3, AV67ContagemResultado_StatusDmn3, lV69ContagemResultado_Descricao3, lV69ContagemResultado_Descricao3, AV69ContagemResultado_Descricao3, AV70ContagemResultado_SistemaCod3, AV70ContagemResultado_SistemaCod3, AV70ContagemResultado_SistemaCod3, AV71ContagemResultado_Agrupador3, lV71ContagemResultado_Agrupador3, lV71ContagemResultado_Agrupador3});
         while ( (pr_default.getStatus(9) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = H00TR12_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = H00TR12_n1553ContagemResultado_CntSrvCod[0];
            A1046ContagemResultado_Agrupador = H00TR12_A1046ContagemResultado_Agrupador[0];
            n1046ContagemResultado_Agrupador = H00TR12_n1046ContagemResultado_Agrupador[0];
            A489ContagemResultado_SistemaCod = H00TR12_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = H00TR12_n489ContagemResultado_SistemaCod[0];
            A494ContagemResultado_Descricao = H00TR12_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = H00TR12_n494ContagemResultado_Descricao[0];
            A484ContagemResultado_StatusDmn = H00TR12_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00TR12_n484ContagemResultado_StatusDmn[0];
            A2017ContagemResultado_DataEntregaReal = H00TR12_A2017ContagemResultado_DataEntregaReal[0];
            n2017ContagemResultado_DataEntregaReal = H00TR12_n2017ContagemResultado_DataEntregaReal[0];
            A471ContagemResultado_DataDmn = H00TR12_A471ContagemResultado_DataDmn[0];
            A493ContagemResultado_DemandaFM = H00TR12_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = H00TR12_n493ContagemResultado_DemandaFM[0];
            A602ContagemResultado_OSVinculada = H00TR12_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = H00TR12_n602ContagemResultado_OSVinculada[0];
            A601ContagemResultado_Servico = H00TR12_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00TR12_n601ContagemResultado_Servico[0];
            A764ContagemResultado_ServicoGrupo = H00TR12_A764ContagemResultado_ServicoGrupo[0];
            n764ContagemResultado_ServicoGrupo = H00TR12_n764ContagemResultado_ServicoGrupo[0];
            A490ContagemResultado_ContratadaCod = H00TR12_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00TR12_n490ContagemResultado_ContratadaCod[0];
            A52Contratada_AreaTrabalhoCod = H00TR12_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00TR12_n52Contratada_AreaTrabalhoCod[0];
            A456ContagemResultado_Codigo = H00TR12_A456ContagemResultado_Codigo[0];
            A509ContagemrResultado_SistemaSigla = H00TR12_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = H00TR12_n509ContagemrResultado_SistemaSigla[0];
            A1326ContagemResultado_ContratadaTipoFab = H00TR12_A1326ContagemResultado_ContratadaTipoFab[0];
            n1326ContagemResultado_ContratadaTipoFab = H00TR12_n1326ContagemResultado_ContratadaTipoFab[0];
            A457ContagemResultado_Demanda = H00TR12_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = H00TR12_n457ContagemResultado_Demanda[0];
            A682ContagemResultado_PFBFMUltima = H00TR12_A682ContagemResultado_PFBFMUltima[0];
            A684ContagemResultado_PFBFSUltima = H00TR12_A684ContagemResultado_PFBFSUltima[0];
            A601ContagemResultado_Servico = H00TR12_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00TR12_n601ContagemResultado_Servico[0];
            A764ContagemResultado_ServicoGrupo = H00TR12_A764ContagemResultado_ServicoGrupo[0];
            n764ContagemResultado_ServicoGrupo = H00TR12_n764ContagemResultado_ServicoGrupo[0];
            A509ContagemrResultado_SistemaSigla = H00TR12_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = H00TR12_n509ContagemrResultado_SistemaSigla[0];
            A52Contratada_AreaTrabalhoCod = H00TR12_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00TR12_n52Contratada_AreaTrabalhoCod[0];
            A1326ContagemResultado_ContratadaTipoFab = H00TR12_A1326ContagemResultado_ContratadaTipoFab[0];
            n1326ContagemResultado_ContratadaTipoFab = H00TR12_n1326ContagemResultado_ContratadaTipoFab[0];
            A682ContagemResultado_PFBFMUltima = H00TR12_A682ContagemResultado_PFBFMUltima[0];
            A684ContagemResultado_PFBFSUltima = H00TR12_A684ContagemResultado_PFBFSUltima[0];
            edtavDisplay_Tooltiptext = "Mostrar";
            edtavDisplay_Link = formatLink("viewcontagemresultado.aspx") + "?" + UrlEncode("" +A456ContagemResultado_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
            AV47Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV47Display);
            AV189Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
            AV88ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV88ContagemResultado_Codigo), 6, 0)));
            AV89ContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_demandafm_Internalname, AV89ContagemResultado_DemandaFM);
            AV90ContagemResultado_Demanda = A457ContagemResultado_Demanda;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_demanda_Internalname, AV90ContagemResultado_Demanda);
            AV91ContagemResultado_DataDmn = A471ContagemResultado_DataDmn;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_datadmn_Internalname, context.localUtil.Format(AV91ContagemResultado_DataDmn, "99/99/99"));
            AV92ContagemResultado_DataEntregaReal = A2017ContagemResultado_DataEntregaReal;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_dataentregareal_Internalname, context.localUtil.TToC( AV92ContagemResultado_DataEntregaReal, 8, 5, 0, 3, "/", ":", " "));
            AV93ContagemResultado_Descricao = A494ContagemResultado_Descricao;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_descricao_Internalname, AV93ContagemResultado_Descricao);
            AV94ContagemrResultado_SistemaSigla = A509ContagemrResultado_SistemaSigla;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemrresultado_sistemasigla_Internalname, AV94ContagemrResultado_SistemaSigla);
            AV14ContagemResultado_OSVinculada = A602ContagemResultado_OSVinculada;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_osvinculada_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV14ContagemResultado_OSVinculada), 6, 0)));
            AV95ContagemResultado_ContratadaTipoFab = A1326ContagemResultado_ContratadaTipoFab;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavContagemresultado_contratadatipofab_Internalname, AV95ContagemResultado_ContratadaTipoFab);
            AV96ContagemResultado_PFBFMUltima = A682ContagemResultado_PFBFMUltima;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_pfbfmultima_Internalname, StringUtil.LTrim( StringUtil.Str( AV96ContagemResultado_PFBFMUltima, 14, 5)));
            AV97ContagemResultado_PFBFSUltima = A684ContagemResultado_PFBFSUltima;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_pfbfsultima_Internalname, StringUtil.LTrim( StringUtil.Str( AV97ContagemResultado_PFBFSUltima, 14, 5)));
            AV87Esforco = ((StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "M")==0) ? A682ContagemResultado_PFBFMUltima : ((StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "S")==0) ? A684ContagemResultado_PFBFSUltima : (decimal)(0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavEsforco_Internalname, StringUtil.LTrim( StringUtil.Str( AV87Esforco, 14, 5)));
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 187;
            }
            if ( ( subGrid_Islastpage == 1 ) || ( subGrid_Rows == 0 ) || ( ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage ) && ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) )
            {
               sendrow_1872( ) ;
               GRID_nEOF = 1;
               GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
               if ( ( subGrid_Islastpage == 1 ) && ( ((int)((GRID_nCurrentRecord) % (subGrid_Recordsperpage( )))) == 0 ) )
               {
                  GRID_nFirstRecordOnPage = GRID_nCurrentRecord;
               }
            }
            if ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) )
            {
               GRID_nEOF = 0;
               GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            }
            GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
            if ( isFullAjaxMode( ) && ( nGXsfl_187_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(187, GridRow);
            }
            /* Execute user subroutine: 'CARREGA.DEMANDA.VINCULADA' */
            S173 ();
            if ( returnInSub )
            {
               pr_default.close(9);
               returnInSub = true;
               if (true) return;
            }
            pr_default.readNext(9);
         }
         pr_default.close(9);
         cmbavContagemresultado_contratadatipofab.CurrentValue = StringUtil.RTrim( AV95ContagemResultado_ContratadaTipoFab);
      }

      protected void S173( )
      {
         /* 'CARREGA.DEMANDA.VINCULADA' Routine */
         pr_default.dynParam(10, new Object[]{ new Object[]{
                                              AV18ContagemResultado_CntadaOsVinc ,
                                              AV19ContagemResultado_SerGrupoVinc ,
                                              AV20ContagemResultado_CodSrvVnc ,
                                              AV21DynamicFiltersSelector1 ,
                                              AV22DynamicFiltersOperator1 ,
                                              AV48ContagemResultado_DemandaFM1 ,
                                              AV23ContagemResultado_DataDmn1 ,
                                              AV24ContagemResultado_DataDmn_To1 ,
                                              AV49ContagemResultado_DataEntregaReal1 ,
                                              AV50ContagemResultado_DataEntregaReal_To1 ,
                                              AV51ContagemResultado_StatusDmn1 ,
                                              AV53ContagemResultado_Descricao1 ,
                                              AV54ContagemResultado_SistemaCod1 ,
                                              AV55ContagemResultado_Agrupador1 ,
                                              AV25DynamicFiltersEnabled2 ,
                                              AV26DynamicFiltersSelector2 ,
                                              AV27DynamicFiltersOperator2 ,
                                              AV56ContagemResultado_DemandaFM2 ,
                                              AV28ContagemResultado_DataDmn2 ,
                                              AV29ContagemResultado_DataDmn_To2 ,
                                              AV57ContagemResultado_DataEntregaReal2 ,
                                              AV58ContagemResultado_DataEntregaReal_To2 ,
                                              AV59ContagemResultado_StatusDmn2 ,
                                              AV61ContagemResultado_Descricao2 ,
                                              AV62ContagemResultado_SistemaCod2 ,
                                              AV63ContagemResultado_Agrupador2 ,
                                              AV30DynamicFiltersEnabled3 ,
                                              AV31DynamicFiltersSelector3 ,
                                              AV32DynamicFiltersOperator3 ,
                                              AV64ContagemResultado_DemandaFM3 ,
                                              AV33ContagemResultado_DataDmn3 ,
                                              AV34ContagemResultado_DataDmn_To3 ,
                                              AV65ContagemResultado_DataEntregaReal3 ,
                                              AV66ContagemResultado_DataEntregaReal_To3 ,
                                              AV67ContagemResultado_StatusDmn3 ,
                                              AV69ContagemResultado_Descricao3 ,
                                              AV70ContagemResultado_SistemaCod3 ,
                                              AV71ContagemResultado_Agrupador3 ,
                                              A490ContagemResultado_ContratadaCod ,
                                              A764ContagemResultado_ServicoGrupo ,
                                              AV16ContagemResultado_ServicoGrupo ,
                                              A601ContagemResultado_Servico ,
                                              AV17ContagemResultado_Servico ,
                                              A493ContagemResultado_DemandaFM ,
                                              A471ContagemResultado_DataDmn ,
                                              A2017ContagemResultado_DataEntregaReal ,
                                              A484ContagemResultado_StatusDmn ,
                                              A494ContagemResultado_Descricao ,
                                              A489ContagemResultado_SistemaCod ,
                                              A1046ContagemResultado_Agrupador ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV6WWPContext.gxTpr_Areatrabalho_codigo ,
                                              A456ContagemResultado_Codigo ,
                                              AV14ContagemResultado_OSVinculada },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV48ContagemResultado_DemandaFM1 = StringUtil.Concat( StringUtil.RTrim( AV48ContagemResultado_DemandaFM1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ContagemResultado_DemandaFM1", AV48ContagemResultado_DemandaFM1);
         lV48ContagemResultado_DemandaFM1 = StringUtil.Concat( StringUtil.RTrim( AV48ContagemResultado_DemandaFM1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ContagemResultado_DemandaFM1", AV48ContagemResultado_DemandaFM1);
         lV53ContagemResultado_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV53ContagemResultado_Descricao1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ContagemResultado_Descricao1", AV53ContagemResultado_Descricao1);
         lV53ContagemResultado_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV53ContagemResultado_Descricao1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ContagemResultado_Descricao1", AV53ContagemResultado_Descricao1);
         lV55ContagemResultado_Agrupador1 = StringUtil.PadR( StringUtil.RTrim( AV55ContagemResultado_Agrupador1), 15, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55ContagemResultado_Agrupador1", AV55ContagemResultado_Agrupador1);
         lV55ContagemResultado_Agrupador1 = StringUtil.PadR( StringUtil.RTrim( AV55ContagemResultado_Agrupador1), 15, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55ContagemResultado_Agrupador1", AV55ContagemResultado_Agrupador1);
         lV56ContagemResultado_DemandaFM2 = StringUtil.Concat( StringUtil.RTrim( AV56ContagemResultado_DemandaFM2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ContagemResultado_DemandaFM2", AV56ContagemResultado_DemandaFM2);
         lV56ContagemResultado_DemandaFM2 = StringUtil.Concat( StringUtil.RTrim( AV56ContagemResultado_DemandaFM2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ContagemResultado_DemandaFM2", AV56ContagemResultado_DemandaFM2);
         lV61ContagemResultado_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV61ContagemResultado_Descricao2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ContagemResultado_Descricao2", AV61ContagemResultado_Descricao2);
         lV61ContagemResultado_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV61ContagemResultado_Descricao2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ContagemResultado_Descricao2", AV61ContagemResultado_Descricao2);
         lV63ContagemResultado_Agrupador2 = StringUtil.PadR( StringUtil.RTrim( AV63ContagemResultado_Agrupador2), 15, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ContagemResultado_Agrupador2", AV63ContagemResultado_Agrupador2);
         lV63ContagemResultado_Agrupador2 = StringUtil.PadR( StringUtil.RTrim( AV63ContagemResultado_Agrupador2), 15, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ContagemResultado_Agrupador2", AV63ContagemResultado_Agrupador2);
         lV64ContagemResultado_DemandaFM3 = StringUtil.Concat( StringUtil.RTrim( AV64ContagemResultado_DemandaFM3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64ContagemResultado_DemandaFM3", AV64ContagemResultado_DemandaFM3);
         lV64ContagemResultado_DemandaFM3 = StringUtil.Concat( StringUtil.RTrim( AV64ContagemResultado_DemandaFM3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64ContagemResultado_DemandaFM3", AV64ContagemResultado_DemandaFM3);
         lV69ContagemResultado_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV69ContagemResultado_Descricao3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ContagemResultado_Descricao3", AV69ContagemResultado_Descricao3);
         lV69ContagemResultado_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV69ContagemResultado_Descricao3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ContagemResultado_Descricao3", AV69ContagemResultado_Descricao3);
         lV71ContagemResultado_Agrupador3 = StringUtil.PadR( StringUtil.RTrim( AV71ContagemResultado_Agrupador3), 15, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ContagemResultado_Agrupador3", AV71ContagemResultado_Agrupador3);
         lV71ContagemResultado_Agrupador3 = StringUtil.PadR( StringUtil.RTrim( AV71ContagemResultado_Agrupador3), 15, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ContagemResultado_Agrupador3", AV71ContagemResultado_Agrupador3);
         /* Using cursor H00TR14 */
         pr_default.execute(10, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, AV14ContagemResultado_OSVinculada, AV18ContagemResultado_CntadaOsVinc, AV16ContagemResultado_ServicoGrupo, AV17ContagemResultado_Servico, AV48ContagemResultado_DemandaFM1, lV48ContagemResultado_DemandaFM1, lV48ContagemResultado_DemandaFM1, AV23ContagemResultado_DataDmn1, AV24ContagemResultado_DataDmn_To1, AV49ContagemResultado_DataEntregaReal1, AV49ContagemResultado_DataEntregaReal1, AV50ContagemResultado_DataEntregaReal_To1, AV50ContagemResultado_DataEntregaReal_To1, AV51ContagemResultado_StatusDmn1, lV53ContagemResultado_Descricao1, lV53ContagemResultado_Descricao1, AV53ContagemResultado_Descricao1, AV54ContagemResultado_SistemaCod1, AV54ContagemResultado_SistemaCod1, AV54ContagemResultado_SistemaCod1, AV55ContagemResultado_Agrupador1, lV55ContagemResultado_Agrupador1, lV55ContagemResultado_Agrupador1, AV56ContagemResultado_DemandaFM2, lV56ContagemResultado_DemandaFM2, lV56ContagemResultado_DemandaFM2, AV28ContagemResultado_DataDmn2, AV29ContagemResultado_DataDmn_To2, AV57ContagemResultado_DataEntregaReal2, AV57ContagemResultado_DataEntregaReal2, AV58ContagemResultado_DataEntregaReal_To2, AV58ContagemResultado_DataEntregaReal_To2, AV59ContagemResultado_StatusDmn2, lV61ContagemResultado_Descricao2, lV61ContagemResultado_Descricao2, AV61ContagemResultado_Descricao2, AV62ContagemResultado_SistemaCod2, AV62ContagemResultado_SistemaCod2, AV62ContagemResultado_SistemaCod2, AV63ContagemResultado_Agrupador2, lV63ContagemResultado_Agrupador2, lV63ContagemResultado_Agrupador2, AV64ContagemResultado_DemandaFM3, lV64ContagemResultado_DemandaFM3, lV64ContagemResultado_DemandaFM3, AV33ContagemResultado_DataDmn3, AV34ContagemResultado_DataDmn_To3, AV65ContagemResultado_DataEntregaReal3, AV65ContagemResultado_DataEntregaReal3, AV66ContagemResultado_DataEntregaReal_To3, AV66ContagemResultado_DataEntregaReal_To3, AV67ContagemResultado_StatusDmn3, lV69ContagemResultado_Descricao3, lV69ContagemResultado_Descricao3, AV69ContagemResultado_Descricao3, AV70ContagemResultado_SistemaCod3, AV70ContagemResultado_SistemaCod3, AV70ContagemResultado_SistemaCod3, AV71ContagemResultado_Agrupador3, lV71ContagemResultado_Agrupador3, lV71ContagemResultado_Agrupador3});
         while ( (pr_default.getStatus(10) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = H00TR14_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = H00TR14_n1553ContagemResultado_CntSrvCod[0];
            A1046ContagemResultado_Agrupador = H00TR14_A1046ContagemResultado_Agrupador[0];
            n1046ContagemResultado_Agrupador = H00TR14_n1046ContagemResultado_Agrupador[0];
            A489ContagemResultado_SistemaCod = H00TR14_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = H00TR14_n489ContagemResultado_SistemaCod[0];
            A494ContagemResultado_Descricao = H00TR14_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = H00TR14_n494ContagemResultado_Descricao[0];
            A484ContagemResultado_StatusDmn = H00TR14_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00TR14_n484ContagemResultado_StatusDmn[0];
            A2017ContagemResultado_DataEntregaReal = H00TR14_A2017ContagemResultado_DataEntregaReal[0];
            n2017ContagemResultado_DataEntregaReal = H00TR14_n2017ContagemResultado_DataEntregaReal[0];
            A471ContagemResultado_DataDmn = H00TR14_A471ContagemResultado_DataDmn[0];
            A493ContagemResultado_DemandaFM = H00TR14_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = H00TR14_n493ContagemResultado_DemandaFM[0];
            A456ContagemResultado_Codigo = H00TR14_A456ContagemResultado_Codigo[0];
            A601ContagemResultado_Servico = H00TR14_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00TR14_n601ContagemResultado_Servico[0];
            A764ContagemResultado_ServicoGrupo = H00TR14_A764ContagemResultado_ServicoGrupo[0];
            n764ContagemResultado_ServicoGrupo = H00TR14_n764ContagemResultado_ServicoGrupo[0];
            A490ContagemResultado_ContratadaCod = H00TR14_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00TR14_n490ContagemResultado_ContratadaCod[0];
            A52Contratada_AreaTrabalhoCod = H00TR14_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00TR14_n52Contratada_AreaTrabalhoCod[0];
            A509ContagemrResultado_SistemaSigla = H00TR14_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = H00TR14_n509ContagemrResultado_SistemaSigla[0];
            A602ContagemResultado_OSVinculada = H00TR14_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = H00TR14_n602ContagemResultado_OSVinculada[0];
            A1326ContagemResultado_ContratadaTipoFab = H00TR14_A1326ContagemResultado_ContratadaTipoFab[0];
            n1326ContagemResultado_ContratadaTipoFab = H00TR14_n1326ContagemResultado_ContratadaTipoFab[0];
            A457ContagemResultado_Demanda = H00TR14_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = H00TR14_n457ContagemResultado_Demanda[0];
            A682ContagemResultado_PFBFMUltima = H00TR14_A682ContagemResultado_PFBFMUltima[0];
            A684ContagemResultado_PFBFSUltima = H00TR14_A684ContagemResultado_PFBFSUltima[0];
            A601ContagemResultado_Servico = H00TR14_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00TR14_n601ContagemResultado_Servico[0];
            A764ContagemResultado_ServicoGrupo = H00TR14_A764ContagemResultado_ServicoGrupo[0];
            n764ContagemResultado_ServicoGrupo = H00TR14_n764ContagemResultado_ServicoGrupo[0];
            A509ContagemrResultado_SistemaSigla = H00TR14_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = H00TR14_n509ContagemrResultado_SistemaSigla[0];
            A682ContagemResultado_PFBFMUltima = H00TR14_A682ContagemResultado_PFBFMUltima[0];
            A684ContagemResultado_PFBFSUltima = H00TR14_A684ContagemResultado_PFBFSUltima[0];
            A52Contratada_AreaTrabalhoCod = H00TR14_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00TR14_n52Contratada_AreaTrabalhoCod[0];
            A1326ContagemResultado_ContratadaTipoFab = H00TR14_A1326ContagemResultado_ContratadaTipoFab[0];
            n1326ContagemResultado_ContratadaTipoFab = H00TR14_n1326ContagemResultado_ContratadaTipoFab[0];
            edtavDisplay_Tooltiptext = "Mostrar";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Tooltiptext", edtavDisplay_Tooltiptext);
            edtavDisplay_Link = formatLink("viewcontagemresultado.aspx") + "?" + UrlEncode("" +A456ContagemResultado_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Link", edtavDisplay_Link);
            AV47Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV47Display)) ? AV189Display_GXI : context.convertURL( context.PathToRelativeUrl( AV47Display))));
            AV189Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV47Display)) ? AV189Display_GXI : context.convertURL( context.PathToRelativeUrl( AV47Display))));
            AV88ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV88ContagemResultado_Codigo), 6, 0)));
            AV89ContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_demandafm_Internalname, AV89ContagemResultado_DemandaFM);
            AV90ContagemResultado_Demanda = A457ContagemResultado_Demanda;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_demanda_Internalname, AV90ContagemResultado_Demanda);
            AV91ContagemResultado_DataDmn = A471ContagemResultado_DataDmn;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_datadmn_Internalname, context.localUtil.Format(AV91ContagemResultado_DataDmn, "99/99/99"));
            AV92ContagemResultado_DataEntregaReal = A2017ContagemResultado_DataEntregaReal;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_dataentregareal_Internalname, context.localUtil.TToC( AV92ContagemResultado_DataEntregaReal, 8, 5, 0, 3, "/", ":", " "));
            AV93ContagemResultado_Descricao = A494ContagemResultado_Descricao;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_descricao_Internalname, AV93ContagemResultado_Descricao);
            AV94ContagemrResultado_SistemaSigla = A509ContagemrResultado_SistemaSigla;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemrresultado_sistemasigla_Internalname, AV94ContagemrResultado_SistemaSigla);
            AV14ContagemResultado_OSVinculada = A602ContagemResultado_OSVinculada;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_osvinculada_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV14ContagemResultado_OSVinculada), 6, 0)));
            AV95ContagemResultado_ContratadaTipoFab = A1326ContagemResultado_ContratadaTipoFab;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavContagemresultado_contratadatipofab_Internalname, AV95ContagemResultado_ContratadaTipoFab);
            AV96ContagemResultado_PFBFMUltima = A682ContagemResultado_PFBFMUltima;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_pfbfmultima_Internalname, StringUtil.LTrim( StringUtil.Str( AV96ContagemResultado_PFBFMUltima, 14, 5)));
            AV97ContagemResultado_PFBFSUltima = A684ContagemResultado_PFBFSUltima;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_pfbfsultima_Internalname, StringUtil.LTrim( StringUtil.Str( AV97ContagemResultado_PFBFSUltima, 14, 5)));
            AV87Esforco = ((StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "M")==0) ? A682ContagemResultado_PFBFMUltima : ((StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "S")==0) ? A684ContagemResultado_PFBFSUltima : (decimal)(0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavEsforco_Internalname, StringUtil.LTrim( StringUtil.Str( AV87Esforco, 14, 5)));
            if ( ( subGrid_Islastpage == 1 ) || ( subGrid_Rows == 0 ) || ( ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage ) && ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) )
            {
               sendrow_1872( ) ;
               GRID_nEOF = 1;
               GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
               if ( ( subGrid_Islastpage == 1 ) && ( ((int)((GRID_nCurrentRecord) % (subGrid_Recordsperpage( )))) == 0 ) )
               {
                  GRID_nFirstRecordOnPage = GRID_nCurrentRecord;
               }
            }
            if ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) )
            {
               GRID_nEOF = 0;
               GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            }
            GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
            if ( isFullAjaxMode( ) && ( nGXsfl_187_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(187, GridRow);
            }
            pr_default.readNext(10);
         }
         pr_default.close(10);
      }

      protected void E16TR2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV25DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersEnabled2", AV25DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV21DynamicFiltersSelector1, AV25DynamicFiltersEnabled2, AV26DynamicFiltersSelector2, AV30DynamicFiltersEnabled3, AV31DynamicFiltersSelector3, AV192Pgmname, AV13Contratada_AreaTrabalhoCod, AV15ContagemResultado_ContratadaCod, AV16ContagemResultado_ServicoGrupo, AV17ContagemResultado_Servico, AV18ContagemResultado_CntadaOsVinc, AV19ContagemResultado_SerGrupoVinc, AV20ContagemResultado_CodSrvVnc, AV10GridState, AV36DynamicFiltersIgnoreFirst, AV48ContagemResultado_DemandaFM1, AV24ContagemResultado_DataDmn_To1, AV23ContagemResultado_DataDmn1, AV50ContagemResultado_DataEntregaReal_To1, AV49ContagemResultado_DataEntregaReal1, AV51ContagemResultado_StatusDmn1, AV52ContagemResultado_StatusCnt1, AV53ContagemResultado_Descricao1, AV54ContagemResultado_SistemaCod1, AV22DynamicFiltersOperator1, AV55ContagemResultado_Agrupador1, AV35DynamicFiltersRemoving, AV56ContagemResultado_DemandaFM2, AV29ContagemResultado_DataDmn_To2, AV28ContagemResultado_DataDmn2, AV58ContagemResultado_DataEntregaReal_To2, AV57ContagemResultado_DataEntregaReal2, AV59ContagemResultado_StatusDmn2, AV60ContagemResultado_StatusCnt2, AV61ContagemResultado_Descricao2, AV62ContagemResultado_SistemaCod2, AV27DynamicFiltersOperator2, AV63ContagemResultado_Agrupador2, AV64ContagemResultado_DemandaFM3, AV34ContagemResultado_DataDmn_To3, AV33ContagemResultado_DataDmn3, AV66ContagemResultado_DataEntregaReal_To3, AV65ContagemResultado_DataEntregaReal3, AV67ContagemResultado_StatusDmn3, AV68ContagemResultado_StatusCnt3, AV69ContagemResultado_Descricao3, AV70ContagemResultado_SistemaCod3, AV32DynamicFiltersOperator3, AV71ContagemResultado_Agrupador3, A457ContagemResultado_Demanda, A52Contratada_AreaTrabalhoCod, AV6WWPContext, A490ContagemResultado_ContratadaCod, A764ContagemResultado_ServicoGrupo, A601ContagemResultado_Servico, A602ContagemResultado_OSVinculada, A493ContagemResultado_DemandaFM, A471ContagemResultado_DataDmn, A2017ContagemResultado_DataEntregaReal, A484ContagemResultado_StatusDmn, A494ContagemResultado_Descricao, A489ContagemResultado_SistemaCod, A1046ContagemResultado_Agrupador, A456ContagemResultado_Codigo, A509ContagemrResultado_SistemaSigla, A1326ContagemResultado_ContratadaTipoFab, A682ContagemResultado_PFBFMUltima, A684ContagemResultado_PFBFSUltima, AV14ContagemResultado_OSVinculada) ;
      }

      protected void E12TR2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV35DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35DynamicFiltersRemoving", AV35DynamicFiltersRemoving);
         AV36DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36DynamicFiltersIgnoreFirst", AV36DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if (returnInSub) return;
         AV35DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35DynamicFiltersRemoving", AV35DynamicFiltersRemoving);
         AV36DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36DynamicFiltersIgnoreFirst", AV36DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV21DynamicFiltersSelector1, AV25DynamicFiltersEnabled2, AV26DynamicFiltersSelector2, AV30DynamicFiltersEnabled3, AV31DynamicFiltersSelector3, AV192Pgmname, AV13Contratada_AreaTrabalhoCod, AV15ContagemResultado_ContratadaCod, AV16ContagemResultado_ServicoGrupo, AV17ContagemResultado_Servico, AV18ContagemResultado_CntadaOsVinc, AV19ContagemResultado_SerGrupoVinc, AV20ContagemResultado_CodSrvVnc, AV10GridState, AV36DynamicFiltersIgnoreFirst, AV48ContagemResultado_DemandaFM1, AV24ContagemResultado_DataDmn_To1, AV23ContagemResultado_DataDmn1, AV50ContagemResultado_DataEntregaReal_To1, AV49ContagemResultado_DataEntregaReal1, AV51ContagemResultado_StatusDmn1, AV52ContagemResultado_StatusCnt1, AV53ContagemResultado_Descricao1, AV54ContagemResultado_SistemaCod1, AV22DynamicFiltersOperator1, AV55ContagemResultado_Agrupador1, AV35DynamicFiltersRemoving, AV56ContagemResultado_DemandaFM2, AV29ContagemResultado_DataDmn_To2, AV28ContagemResultado_DataDmn2, AV58ContagemResultado_DataEntregaReal_To2, AV57ContagemResultado_DataEntregaReal2, AV59ContagemResultado_StatusDmn2, AV60ContagemResultado_StatusCnt2, AV61ContagemResultado_Descricao2, AV62ContagemResultado_SistemaCod2, AV27DynamicFiltersOperator2, AV63ContagemResultado_Agrupador2, AV64ContagemResultado_DemandaFM3, AV34ContagemResultado_DataDmn_To3, AV33ContagemResultado_DataDmn3, AV66ContagemResultado_DataEntregaReal_To3, AV65ContagemResultado_DataEntregaReal3, AV67ContagemResultado_StatusDmn3, AV68ContagemResultado_StatusCnt3, AV69ContagemResultado_Descricao3, AV70ContagemResultado_SistemaCod3, AV32DynamicFiltersOperator3, AV71ContagemResultado_Agrupador3, A457ContagemResultado_Demanda, A52Contratada_AreaTrabalhoCod, AV6WWPContext, A490ContagemResultado_ContratadaCod, A764ContagemResultado_ServicoGrupo, A601ContagemResultado_Servico, A602ContagemResultado_OSVinculada, A493ContagemResultado_DemandaFM, A471ContagemResultado_DataDmn, A2017ContagemResultado_DataEntregaReal, A484ContagemResultado_StatusDmn, A494ContagemResultado_Descricao, A489ContagemResultado_SistemaCod, A1046ContagemResultado_Agrupador, A456ContagemResultado_Codigo, A509ContagemrResultado_SistemaSigla, A1326ContagemResultado_ContratadaTipoFab, A682ContagemResultado_PFBFMUltima, A684ContagemResultado_PFBFSUltima, AV14ContagemResultado_OSVinculada) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV31DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV32DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         dynavContagemresultado_sistemacod1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV54ContagemResultado_SistemaCod1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_sistemacod1_Internalname, "Values", dynavContagemresultado_sistemacod1.ToJavascriptSource());
         cmbavContagemresultado_statuscnt1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV52ContagemResultado_StatusCnt1), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statuscnt1_Internalname, "Values", cmbavContagemresultado_statuscnt1.ToJavascriptSource());
         cmbavContagemresultado_statusdmn1.CurrentValue = StringUtil.RTrim( AV51ContagemResultado_StatusDmn1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn1_Internalname, "Values", cmbavContagemresultado_statusdmn1.ToJavascriptSource());
         dynavContagemresultado_sistemacod2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV62ContagemResultado_SistemaCod2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_sistemacod2_Internalname, "Values", dynavContagemresultado_sistemacod2.ToJavascriptSource());
         cmbavContagemresultado_statuscnt2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV60ContagemResultado_StatusCnt2), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statuscnt2_Internalname, "Values", cmbavContagemresultado_statuscnt2.ToJavascriptSource());
         cmbavContagemresultado_statusdmn2.CurrentValue = StringUtil.RTrim( AV59ContagemResultado_StatusDmn2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn2_Internalname, "Values", cmbavContagemresultado_statusdmn2.ToJavascriptSource());
         dynavContagemresultado_sistemacod3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV70ContagemResultado_SistemaCod3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_sistemacod3_Internalname, "Values", dynavContagemresultado_sistemacod3.ToJavascriptSource());
         cmbavContagemresultado_statuscnt3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV68ContagemResultado_StatusCnt3), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statuscnt3_Internalname, "Values", cmbavContagemresultado_statuscnt3.ToJavascriptSource());
         cmbavContagemresultado_statusdmn3.CurrentValue = StringUtil.RTrim( AV67ContagemResultado_StatusDmn3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn3_Internalname, "Values", cmbavContagemresultado_statusdmn3.ToJavascriptSource());
      }

      protected void E13TR2( )
      {
         /* 'DoExport' Routine */
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if (returnInSub) return;
         if ( StringUtil.StrCmp(AV98ExcelFilename, "") != 0 )
         {
            context.wjLoc = formatLink(AV98ExcelFilename) ;
            context.wjLocDisableFrm = 0;
         }
         else
         {
            GX_msglist.addItem(AV99ErrorMessage);
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E17TR2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         AV22DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if (returnInSub) return;
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E18TR2( )
      {
         /* Dynamicfiltersoperator1_Click Routine */
         if ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 )
         {
            AV49ContagemResultado_DataEntregaReal1 = (DateTime)(DateTime.MinValue);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ContagemResultado_DataEntregaReal1", context.localUtil.TToC( AV49ContagemResultado_DataEntregaReal1, 8, 5, 0, 3, "/", ":", " "));
            AV50ContagemResultado_DataEntregaReal_To1 = (DateTime)(DateTime.MinValue);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ContagemResultado_DataEntregaReal_To1", context.localUtil.TToC( AV50ContagemResultado_DataEntregaReal_To1, 8, 5, 0, 3, "/", ":", " "));
            /* Execute user subroutine: 'UPDATECONTAGEMRESULTADO_DATAENTREGAREAL1OPERATORVALUES' */
            S212 ();
            if (returnInSub) return;
            gxgrGrid_refresh( subGrid_Rows, AV21DynamicFiltersSelector1, AV25DynamicFiltersEnabled2, AV26DynamicFiltersSelector2, AV30DynamicFiltersEnabled3, AV31DynamicFiltersSelector3, AV192Pgmname, AV13Contratada_AreaTrabalhoCod, AV15ContagemResultado_ContratadaCod, AV16ContagemResultado_ServicoGrupo, AV17ContagemResultado_Servico, AV18ContagemResultado_CntadaOsVinc, AV19ContagemResultado_SerGrupoVinc, AV20ContagemResultado_CodSrvVnc, AV10GridState, AV36DynamicFiltersIgnoreFirst, AV48ContagemResultado_DemandaFM1, AV24ContagemResultado_DataDmn_To1, AV23ContagemResultado_DataDmn1, AV50ContagemResultado_DataEntregaReal_To1, AV49ContagemResultado_DataEntregaReal1, AV51ContagemResultado_StatusDmn1, AV52ContagemResultado_StatusCnt1, AV53ContagemResultado_Descricao1, AV54ContagemResultado_SistemaCod1, AV22DynamicFiltersOperator1, AV55ContagemResultado_Agrupador1, AV35DynamicFiltersRemoving, AV56ContagemResultado_DemandaFM2, AV29ContagemResultado_DataDmn_To2, AV28ContagemResultado_DataDmn2, AV58ContagemResultado_DataEntregaReal_To2, AV57ContagemResultado_DataEntregaReal2, AV59ContagemResultado_StatusDmn2, AV60ContagemResultado_StatusCnt2, AV61ContagemResultado_Descricao2, AV62ContagemResultado_SistemaCod2, AV27DynamicFiltersOperator2, AV63ContagemResultado_Agrupador2, AV64ContagemResultado_DemandaFM3, AV34ContagemResultado_DataDmn_To3, AV33ContagemResultado_DataDmn3, AV66ContagemResultado_DataEntregaReal_To3, AV65ContagemResultado_DataEntregaReal3, AV67ContagemResultado_StatusDmn3, AV68ContagemResultado_StatusCnt3, AV69ContagemResultado_Descricao3, AV70ContagemResultado_SistemaCod3, AV32DynamicFiltersOperator3, AV71ContagemResultado_Agrupador3, A457ContagemResultado_Demanda, A52Contratada_AreaTrabalhoCod, AV6WWPContext, A490ContagemResultado_ContratadaCod, A764ContagemResultado_ServicoGrupo, A601ContagemResultado_Servico, A602ContagemResultado_OSVinculada, A493ContagemResultado_DemandaFM, A471ContagemResultado_DataDmn, A2017ContagemResultado_DataEntregaReal, A484ContagemResultado_StatusDmn, A494ContagemResultado_Descricao, A489ContagemResultado_SistemaCod, A1046ContagemResultado_Agrupador, A456ContagemResultado_Codigo, A509ContagemrResultado_SistemaSigla, A1326ContagemResultado_ContratadaTipoFab, A682ContagemResultado_PFBFMUltima, A684ContagemResultado_PFBFSUltima, AV14ContagemResultado_OSVinculada) ;
         }
      }

      protected void E19TR2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV30DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersEnabled3", AV30DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV21DynamicFiltersSelector1, AV25DynamicFiltersEnabled2, AV26DynamicFiltersSelector2, AV30DynamicFiltersEnabled3, AV31DynamicFiltersSelector3, AV192Pgmname, AV13Contratada_AreaTrabalhoCod, AV15ContagemResultado_ContratadaCod, AV16ContagemResultado_ServicoGrupo, AV17ContagemResultado_Servico, AV18ContagemResultado_CntadaOsVinc, AV19ContagemResultado_SerGrupoVinc, AV20ContagemResultado_CodSrvVnc, AV10GridState, AV36DynamicFiltersIgnoreFirst, AV48ContagemResultado_DemandaFM1, AV24ContagemResultado_DataDmn_To1, AV23ContagemResultado_DataDmn1, AV50ContagemResultado_DataEntregaReal_To1, AV49ContagemResultado_DataEntregaReal1, AV51ContagemResultado_StatusDmn1, AV52ContagemResultado_StatusCnt1, AV53ContagemResultado_Descricao1, AV54ContagemResultado_SistemaCod1, AV22DynamicFiltersOperator1, AV55ContagemResultado_Agrupador1, AV35DynamicFiltersRemoving, AV56ContagemResultado_DemandaFM2, AV29ContagemResultado_DataDmn_To2, AV28ContagemResultado_DataDmn2, AV58ContagemResultado_DataEntregaReal_To2, AV57ContagemResultado_DataEntregaReal2, AV59ContagemResultado_StatusDmn2, AV60ContagemResultado_StatusCnt2, AV61ContagemResultado_Descricao2, AV62ContagemResultado_SistemaCod2, AV27DynamicFiltersOperator2, AV63ContagemResultado_Agrupador2, AV64ContagemResultado_DemandaFM3, AV34ContagemResultado_DataDmn_To3, AV33ContagemResultado_DataDmn3, AV66ContagemResultado_DataEntregaReal_To3, AV65ContagemResultado_DataEntregaReal3, AV67ContagemResultado_StatusDmn3, AV68ContagemResultado_StatusCnt3, AV69ContagemResultado_Descricao3, AV70ContagemResultado_SistemaCod3, AV32DynamicFiltersOperator3, AV71ContagemResultado_Agrupador3, A457ContagemResultado_Demanda, A52Contratada_AreaTrabalhoCod, AV6WWPContext, A490ContagemResultado_ContratadaCod, A764ContagemResultado_ServicoGrupo, A601ContagemResultado_Servico, A602ContagemResultado_OSVinculada, A493ContagemResultado_DemandaFM, A471ContagemResultado_DataDmn, A2017ContagemResultado_DataEntregaReal, A484ContagemResultado_StatusDmn, A494ContagemResultado_Descricao, A489ContagemResultado_SistemaCod, A1046ContagemResultado_Agrupador, A456ContagemResultado_Codigo, A509ContagemrResultado_SistemaSigla, A1326ContagemResultado_ContratadaTipoFab, A682ContagemResultado_PFBFMUltima, A684ContagemResultado_PFBFSUltima, AV14ContagemResultado_OSVinculada) ;
      }

      protected void E14TR2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV35DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35DynamicFiltersRemoving", AV35DynamicFiltersRemoving);
         AV25DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersEnabled2", AV25DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if (returnInSub) return;
         AV35DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35DynamicFiltersRemoving", AV35DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV21DynamicFiltersSelector1, AV25DynamicFiltersEnabled2, AV26DynamicFiltersSelector2, AV30DynamicFiltersEnabled3, AV31DynamicFiltersSelector3, AV192Pgmname, AV13Contratada_AreaTrabalhoCod, AV15ContagemResultado_ContratadaCod, AV16ContagemResultado_ServicoGrupo, AV17ContagemResultado_Servico, AV18ContagemResultado_CntadaOsVinc, AV19ContagemResultado_SerGrupoVinc, AV20ContagemResultado_CodSrvVnc, AV10GridState, AV36DynamicFiltersIgnoreFirst, AV48ContagemResultado_DemandaFM1, AV24ContagemResultado_DataDmn_To1, AV23ContagemResultado_DataDmn1, AV50ContagemResultado_DataEntregaReal_To1, AV49ContagemResultado_DataEntregaReal1, AV51ContagemResultado_StatusDmn1, AV52ContagemResultado_StatusCnt1, AV53ContagemResultado_Descricao1, AV54ContagemResultado_SistemaCod1, AV22DynamicFiltersOperator1, AV55ContagemResultado_Agrupador1, AV35DynamicFiltersRemoving, AV56ContagemResultado_DemandaFM2, AV29ContagemResultado_DataDmn_To2, AV28ContagemResultado_DataDmn2, AV58ContagemResultado_DataEntregaReal_To2, AV57ContagemResultado_DataEntregaReal2, AV59ContagemResultado_StatusDmn2, AV60ContagemResultado_StatusCnt2, AV61ContagemResultado_Descricao2, AV62ContagemResultado_SistemaCod2, AV27DynamicFiltersOperator2, AV63ContagemResultado_Agrupador2, AV64ContagemResultado_DemandaFM3, AV34ContagemResultado_DataDmn_To3, AV33ContagemResultado_DataDmn3, AV66ContagemResultado_DataEntregaReal_To3, AV65ContagemResultado_DataEntregaReal3, AV67ContagemResultado_StatusDmn3, AV68ContagemResultado_StatusCnt3, AV69ContagemResultado_Descricao3, AV70ContagemResultado_SistemaCod3, AV32DynamicFiltersOperator3, AV71ContagemResultado_Agrupador3, A457ContagemResultado_Demanda, A52Contratada_AreaTrabalhoCod, AV6WWPContext, A490ContagemResultado_ContratadaCod, A764ContagemResultado_ServicoGrupo, A601ContagemResultado_Servico, A602ContagemResultado_OSVinculada, A493ContagemResultado_DemandaFM, A471ContagemResultado_DataDmn, A2017ContagemResultado_DataEntregaReal, A484ContagemResultado_StatusDmn, A494ContagemResultado_Descricao, A489ContagemResultado_SistemaCod, A1046ContagemResultado_Agrupador, A456ContagemResultado_Codigo, A509ContagemrResultado_SistemaSigla, A1326ContagemResultado_ContratadaTipoFab, A682ContagemResultado_PFBFMUltima, A684ContagemResultado_PFBFSUltima, AV14ContagemResultado_OSVinculada) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV31DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV32DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         dynavContagemresultado_sistemacod1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV54ContagemResultado_SistemaCod1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_sistemacod1_Internalname, "Values", dynavContagemresultado_sistemacod1.ToJavascriptSource());
         cmbavContagemresultado_statuscnt1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV52ContagemResultado_StatusCnt1), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statuscnt1_Internalname, "Values", cmbavContagemresultado_statuscnt1.ToJavascriptSource());
         cmbavContagemresultado_statusdmn1.CurrentValue = StringUtil.RTrim( AV51ContagemResultado_StatusDmn1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn1_Internalname, "Values", cmbavContagemresultado_statusdmn1.ToJavascriptSource());
         dynavContagemresultado_sistemacod2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV62ContagemResultado_SistemaCod2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_sistemacod2_Internalname, "Values", dynavContagemresultado_sistemacod2.ToJavascriptSource());
         cmbavContagemresultado_statuscnt2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV60ContagemResultado_StatusCnt2), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statuscnt2_Internalname, "Values", cmbavContagemresultado_statuscnt2.ToJavascriptSource());
         cmbavContagemresultado_statusdmn2.CurrentValue = StringUtil.RTrim( AV59ContagemResultado_StatusDmn2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn2_Internalname, "Values", cmbavContagemresultado_statusdmn2.ToJavascriptSource());
         dynavContagemresultado_sistemacod3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV70ContagemResultado_SistemaCod3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_sistemacod3_Internalname, "Values", dynavContagemresultado_sistemacod3.ToJavascriptSource());
         cmbavContagemresultado_statuscnt3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV68ContagemResultado_StatusCnt3), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statuscnt3_Internalname, "Values", cmbavContagemresultado_statuscnt3.ToJavascriptSource());
         cmbavContagemresultado_statusdmn3.CurrentValue = StringUtil.RTrim( AV67ContagemResultado_StatusDmn3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn3_Internalname, "Values", cmbavContagemresultado_statusdmn3.ToJavascriptSource());
      }

      protected void E20TR2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         AV27DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator2), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if (returnInSub) return;
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E21TR2( )
      {
         /* Dynamicfiltersoperator2_Click Routine */
         if ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 )
         {
            AV57ContagemResultado_DataEntregaReal2 = (DateTime)(DateTime.MinValue);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ContagemResultado_DataEntregaReal2", context.localUtil.TToC( AV57ContagemResultado_DataEntregaReal2, 8, 5, 0, 3, "/", ":", " "));
            AV58ContagemResultado_DataEntregaReal_To2 = (DateTime)(DateTime.MinValue);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58ContagemResultado_DataEntregaReal_To2", context.localUtil.TToC( AV58ContagemResultado_DataEntregaReal_To2, 8, 5, 0, 3, "/", ":", " "));
            /* Execute user subroutine: 'UPDATECONTAGEMRESULTADO_DATAENTREGAREAL2OPERATORVALUES' */
            S222 ();
            if (returnInSub) return;
            gxgrGrid_refresh( subGrid_Rows, AV21DynamicFiltersSelector1, AV25DynamicFiltersEnabled2, AV26DynamicFiltersSelector2, AV30DynamicFiltersEnabled3, AV31DynamicFiltersSelector3, AV192Pgmname, AV13Contratada_AreaTrabalhoCod, AV15ContagemResultado_ContratadaCod, AV16ContagemResultado_ServicoGrupo, AV17ContagemResultado_Servico, AV18ContagemResultado_CntadaOsVinc, AV19ContagemResultado_SerGrupoVinc, AV20ContagemResultado_CodSrvVnc, AV10GridState, AV36DynamicFiltersIgnoreFirst, AV48ContagemResultado_DemandaFM1, AV24ContagemResultado_DataDmn_To1, AV23ContagemResultado_DataDmn1, AV50ContagemResultado_DataEntregaReal_To1, AV49ContagemResultado_DataEntregaReal1, AV51ContagemResultado_StatusDmn1, AV52ContagemResultado_StatusCnt1, AV53ContagemResultado_Descricao1, AV54ContagemResultado_SistemaCod1, AV22DynamicFiltersOperator1, AV55ContagemResultado_Agrupador1, AV35DynamicFiltersRemoving, AV56ContagemResultado_DemandaFM2, AV29ContagemResultado_DataDmn_To2, AV28ContagemResultado_DataDmn2, AV58ContagemResultado_DataEntregaReal_To2, AV57ContagemResultado_DataEntregaReal2, AV59ContagemResultado_StatusDmn2, AV60ContagemResultado_StatusCnt2, AV61ContagemResultado_Descricao2, AV62ContagemResultado_SistemaCod2, AV27DynamicFiltersOperator2, AV63ContagemResultado_Agrupador2, AV64ContagemResultado_DemandaFM3, AV34ContagemResultado_DataDmn_To3, AV33ContagemResultado_DataDmn3, AV66ContagemResultado_DataEntregaReal_To3, AV65ContagemResultado_DataEntregaReal3, AV67ContagemResultado_StatusDmn3, AV68ContagemResultado_StatusCnt3, AV69ContagemResultado_Descricao3, AV70ContagemResultado_SistemaCod3, AV32DynamicFiltersOperator3, AV71ContagemResultado_Agrupador3, A457ContagemResultado_Demanda, A52Contratada_AreaTrabalhoCod, AV6WWPContext, A490ContagemResultado_ContratadaCod, A764ContagemResultado_ServicoGrupo, A601ContagemResultado_Servico, A602ContagemResultado_OSVinculada, A493ContagemResultado_DemandaFM, A471ContagemResultado_DataDmn, A2017ContagemResultado_DataEntregaReal, A484ContagemResultado_StatusDmn, A494ContagemResultado_Descricao, A489ContagemResultado_SistemaCod, A1046ContagemResultado_Agrupador, A456ContagemResultado_Codigo, A509ContagemrResultado_SistemaSigla, A1326ContagemResultado_ContratadaTipoFab, A682ContagemResultado_PFBFMUltima, A684ContagemResultado_PFBFSUltima, AV14ContagemResultado_OSVinculada) ;
         }
      }

      protected void E15TR2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV35DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35DynamicFiltersRemoving", AV35DynamicFiltersRemoving);
         AV30DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersEnabled3", AV30DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if (returnInSub) return;
         AV35DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35DynamicFiltersRemoving", AV35DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV21DynamicFiltersSelector1, AV25DynamicFiltersEnabled2, AV26DynamicFiltersSelector2, AV30DynamicFiltersEnabled3, AV31DynamicFiltersSelector3, AV192Pgmname, AV13Contratada_AreaTrabalhoCod, AV15ContagemResultado_ContratadaCod, AV16ContagemResultado_ServicoGrupo, AV17ContagemResultado_Servico, AV18ContagemResultado_CntadaOsVinc, AV19ContagemResultado_SerGrupoVinc, AV20ContagemResultado_CodSrvVnc, AV10GridState, AV36DynamicFiltersIgnoreFirst, AV48ContagemResultado_DemandaFM1, AV24ContagemResultado_DataDmn_To1, AV23ContagemResultado_DataDmn1, AV50ContagemResultado_DataEntregaReal_To1, AV49ContagemResultado_DataEntregaReal1, AV51ContagemResultado_StatusDmn1, AV52ContagemResultado_StatusCnt1, AV53ContagemResultado_Descricao1, AV54ContagemResultado_SistemaCod1, AV22DynamicFiltersOperator1, AV55ContagemResultado_Agrupador1, AV35DynamicFiltersRemoving, AV56ContagemResultado_DemandaFM2, AV29ContagemResultado_DataDmn_To2, AV28ContagemResultado_DataDmn2, AV58ContagemResultado_DataEntregaReal_To2, AV57ContagemResultado_DataEntregaReal2, AV59ContagemResultado_StatusDmn2, AV60ContagemResultado_StatusCnt2, AV61ContagemResultado_Descricao2, AV62ContagemResultado_SistemaCod2, AV27DynamicFiltersOperator2, AV63ContagemResultado_Agrupador2, AV64ContagemResultado_DemandaFM3, AV34ContagemResultado_DataDmn_To3, AV33ContagemResultado_DataDmn3, AV66ContagemResultado_DataEntregaReal_To3, AV65ContagemResultado_DataEntregaReal3, AV67ContagemResultado_StatusDmn3, AV68ContagemResultado_StatusCnt3, AV69ContagemResultado_Descricao3, AV70ContagemResultado_SistemaCod3, AV32DynamicFiltersOperator3, AV71ContagemResultado_Agrupador3, A457ContagemResultado_Demanda, A52Contratada_AreaTrabalhoCod, AV6WWPContext, A490ContagemResultado_ContratadaCod, A764ContagemResultado_ServicoGrupo, A601ContagemResultado_Servico, A602ContagemResultado_OSVinculada, A493ContagemResultado_DemandaFM, A471ContagemResultado_DataDmn, A2017ContagemResultado_DataEntregaReal, A484ContagemResultado_StatusDmn, A494ContagemResultado_Descricao, A489ContagemResultado_SistemaCod, A1046ContagemResultado_Agrupador, A456ContagemResultado_Codigo, A509ContagemrResultado_SistemaSigla, A1326ContagemResultado_ContratadaTipoFab, A682ContagemResultado_PFBFMUltima, A684ContagemResultado_PFBFSUltima, AV14ContagemResultado_OSVinculada) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV31DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV32DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         dynavContagemresultado_sistemacod1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV54ContagemResultado_SistemaCod1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_sistemacod1_Internalname, "Values", dynavContagemresultado_sistemacod1.ToJavascriptSource());
         cmbavContagemresultado_statuscnt1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV52ContagemResultado_StatusCnt1), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statuscnt1_Internalname, "Values", cmbavContagemresultado_statuscnt1.ToJavascriptSource());
         cmbavContagemresultado_statusdmn1.CurrentValue = StringUtil.RTrim( AV51ContagemResultado_StatusDmn1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn1_Internalname, "Values", cmbavContagemresultado_statusdmn1.ToJavascriptSource());
         dynavContagemresultado_sistemacod2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV62ContagemResultado_SistemaCod2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_sistemacod2_Internalname, "Values", dynavContagemresultado_sistemacod2.ToJavascriptSource());
         cmbavContagemresultado_statuscnt2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV60ContagemResultado_StatusCnt2), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statuscnt2_Internalname, "Values", cmbavContagemresultado_statuscnt2.ToJavascriptSource());
         cmbavContagemresultado_statusdmn2.CurrentValue = StringUtil.RTrim( AV59ContagemResultado_StatusDmn2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn2_Internalname, "Values", cmbavContagemresultado_statusdmn2.ToJavascriptSource());
         dynavContagemresultado_sistemacod3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV70ContagemResultado_SistemaCod3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_sistemacod3_Internalname, "Values", dynavContagemresultado_sistemacod3.ToJavascriptSource());
         cmbavContagemresultado_statuscnt3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV68ContagemResultado_StatusCnt3), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statuscnt3_Internalname, "Values", cmbavContagemresultado_statuscnt3.ToJavascriptSource());
         cmbavContagemresultado_statusdmn3.CurrentValue = StringUtil.RTrim( AV67ContagemResultado_StatusDmn3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn3_Internalname, "Values", cmbavContagemresultado_statusdmn3.ToJavascriptSource());
      }

      protected void E22TR2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         AV32DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32DynamicFiltersOperator3), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if (returnInSub) return;
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV32DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E23TR2( )
      {
         /* Dynamicfiltersoperator3_Click Routine */
         if ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 )
         {
            AV65ContagemResultado_DataEntregaReal3 = (DateTime)(DateTime.MinValue);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ContagemResultado_DataEntregaReal3", context.localUtil.TToC( AV65ContagemResultado_DataEntregaReal3, 8, 5, 0, 3, "/", ":", " "));
            AV66ContagemResultado_DataEntregaReal_To3 = (DateTime)(DateTime.MinValue);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ContagemResultado_DataEntregaReal_To3", context.localUtil.TToC( AV66ContagemResultado_DataEntregaReal_To3, 8, 5, 0, 3, "/", ":", " "));
            /* Execute user subroutine: 'UPDATECONTAGEMRESULTADO_DATAENTREGAREAL3OPERATORVALUES' */
            S232 ();
            if (returnInSub) return;
            gxgrGrid_refresh( subGrid_Rows, AV21DynamicFiltersSelector1, AV25DynamicFiltersEnabled2, AV26DynamicFiltersSelector2, AV30DynamicFiltersEnabled3, AV31DynamicFiltersSelector3, AV192Pgmname, AV13Contratada_AreaTrabalhoCod, AV15ContagemResultado_ContratadaCod, AV16ContagemResultado_ServicoGrupo, AV17ContagemResultado_Servico, AV18ContagemResultado_CntadaOsVinc, AV19ContagemResultado_SerGrupoVinc, AV20ContagemResultado_CodSrvVnc, AV10GridState, AV36DynamicFiltersIgnoreFirst, AV48ContagemResultado_DemandaFM1, AV24ContagemResultado_DataDmn_To1, AV23ContagemResultado_DataDmn1, AV50ContagemResultado_DataEntregaReal_To1, AV49ContagemResultado_DataEntregaReal1, AV51ContagemResultado_StatusDmn1, AV52ContagemResultado_StatusCnt1, AV53ContagemResultado_Descricao1, AV54ContagemResultado_SistemaCod1, AV22DynamicFiltersOperator1, AV55ContagemResultado_Agrupador1, AV35DynamicFiltersRemoving, AV56ContagemResultado_DemandaFM2, AV29ContagemResultado_DataDmn_To2, AV28ContagemResultado_DataDmn2, AV58ContagemResultado_DataEntregaReal_To2, AV57ContagemResultado_DataEntregaReal2, AV59ContagemResultado_StatusDmn2, AV60ContagemResultado_StatusCnt2, AV61ContagemResultado_Descricao2, AV62ContagemResultado_SistemaCod2, AV27DynamicFiltersOperator2, AV63ContagemResultado_Agrupador2, AV64ContagemResultado_DemandaFM3, AV34ContagemResultado_DataDmn_To3, AV33ContagemResultado_DataDmn3, AV66ContagemResultado_DataEntregaReal_To3, AV65ContagemResultado_DataEntregaReal3, AV67ContagemResultado_StatusDmn3, AV68ContagemResultado_StatusCnt3, AV69ContagemResultado_Descricao3, AV70ContagemResultado_SistemaCod3, AV32DynamicFiltersOperator3, AV71ContagemResultado_Agrupador3, A457ContagemResultado_Demanda, A52Contratada_AreaTrabalhoCod, AV6WWPContext, A490ContagemResultado_ContratadaCod, A764ContagemResultado_ServicoGrupo, A601ContagemResultado_Servico, A602ContagemResultado_OSVinculada, A493ContagemResultado_DemandaFM, A471ContagemResultado_DataDmn, A2017ContagemResultado_DataEntregaReal, A484ContagemResultado_StatusDmn, A494ContagemResultado_Descricao, A489ContagemResultado_SistemaCod, A1046ContagemResultado_Agrupador, A456ContagemResultado_Codigo, A509ContagemrResultado_SistemaSigla, A1326ContagemResultado_ContratadaTipoFab, A682ContagemResultado_PFBFMUltima, A684ContagemResultado_PFBFSUltima, AV14ContagemResultado_OSVinculada) ;
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavContagemresultado_demandafm1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demandafm1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demandafm1_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontagemresultado_datadmn1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultado_datadmn1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultado_datadmn1_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontagemresultado_dataentregareal1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultado_dataentregareal1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultado_dataentregareal1_Visible), 5, 0)));
         cmbavContagemresultado_statusdmn1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContagemresultado_statusdmn1.Visible), 5, 0)));
         cmbavContagemresultado_statuscnt1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statuscnt1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContagemresultado_statuscnt1.Visible), 5, 0)));
         edtavContagemresultado_descricao1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_descricao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_descricao1_Visible), 5, 0)));
         dynavContagemresultado_sistemacod1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_sistemacod1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_sistemacod1.Visible), 5, 0)));
         edtavContagemresultado_agrupador1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_agrupador1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_agrupador1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 )
         {
            edtavContagemresultado_demandafm1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demandafm1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demandafm1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 )
         {
            tblTablemergeddynamicfilterscontagemresultado_datadmn1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultado_datadmn1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultado_datadmn1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 )
         {
            tblTablemergeddynamicfilterscontagemresultado_dataentregareal1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultado_dataentregareal1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultado_dataentregareal1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
            /* Execute user subroutine: 'UPDATECONTAGEMRESULTADO_DATAENTREGAREAL1OPERATORVALUES' */
            S212 ();
            if (returnInSub) return;
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
         {
            cmbavContagemresultado_statusdmn1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContagemresultado_statusdmn1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSCNT") == 0 )
         {
            cmbavContagemresultado_statuscnt1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statuscnt1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContagemresultado_statuscnt1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
         {
            edtavContagemresultado_descricao1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_descricao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_descricao1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
         {
            dynavContagemresultado_sistemacod1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_sistemacod1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_sistemacod1.Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
         {
            edtavContagemresultado_agrupador1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_agrupador1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_agrupador1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S212( )
      {
         /* 'UPDATECONTAGEMRESULTADO_DATAENTREGAREAL1OPERATORVALUES' Routine */
         cellContagemresultado_dataentregareal_cell1_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultado_dataentregareal_cell1_Internalname, "Class", cellContagemresultado_dataentregareal_cell1_Class);
         cellContagemresultado_dataentregareal_to_cell1_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultado_dataentregareal_to_cell1_Internalname, "Class", cellContagemresultado_dataentregareal_to_cell1_Class);
         lblContagemresultado_dataentregareal_rangemiddletext_11_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblContagemresultado_dataentregareal_rangemiddletext_11_Internalname, "Class", lblContagemresultado_dataentregareal_rangemiddletext_11_Class);
         if ( AV22DynamicFiltersOperator1 == 0 )
         {
            AV49ContagemResultado_DataEntregaReal1 = DateTimeUtil.ResetTime( Gx_date ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ContagemResultado_DataEntregaReal1", context.localUtil.TToC( AV49ContagemResultado_DataEntregaReal1, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( AV22DynamicFiltersOperator1 == 1 )
         {
            AV49ContagemResultado_DataEntregaReal1 = DateTimeUtil.ResetTime( Gx_date ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ContagemResultado_DataEntregaReal1", context.localUtil.TToC( AV49ContagemResultado_DataEntregaReal1, 8, 5, 0, 3, "/", ":", " "));
            AV50ContagemResultado_DataEntregaReal_To1 = DateTimeUtil.ResetTime( DateTimeUtil.DAdd( Gx_date , + ( (int)(1) )) ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ContagemResultado_DataEntregaReal_To1", context.localUtil.TToC( AV50ContagemResultado_DataEntregaReal_To1, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( AV22DynamicFiltersOperator1 == 2 )
         {
            AV49ContagemResultado_DataEntregaReal1 = DateTimeUtil.ResetTime( DateTimeUtil.DAdd( Gx_date , + ( (int)(1) )) ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ContagemResultado_DataEntregaReal1", context.localUtil.TToC( AV49ContagemResultado_DataEntregaReal1, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( AV22DynamicFiltersOperator1 == 3 )
         {
            cellContagemresultado_dataentregareal_cell1_Class = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultado_dataentregareal_cell1_Internalname, "Class", cellContagemresultado_dataentregareal_cell1_Class);
            cellContagemresultado_dataentregareal_to_cell1_Class = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultado_dataentregareal_to_cell1_Internalname, "Class", cellContagemresultado_dataentregareal_to_cell1_Class);
            lblContagemresultado_dataentregareal_rangemiddletext_11_Class = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblContagemresultado_dataentregareal_rangemiddletext_11_Internalname, "Class", lblContagemresultado_dataentregareal_rangemiddletext_11_Class);
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavContagemresultado_demandafm2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demandafm2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demandafm2_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontagemresultado_datadmn2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultado_datadmn2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultado_datadmn2_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontagemresultado_dataentregareal2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultado_dataentregareal2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultado_dataentregareal2_Visible), 5, 0)));
         cmbavContagemresultado_statusdmn2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContagemresultado_statusdmn2.Visible), 5, 0)));
         cmbavContagemresultado_statuscnt2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statuscnt2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContagemresultado_statuscnt2.Visible), 5, 0)));
         edtavContagemresultado_descricao2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_descricao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_descricao2_Visible), 5, 0)));
         dynavContagemresultado_sistemacod2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_sistemacod2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_sistemacod2.Visible), 5, 0)));
         edtavContagemresultado_agrupador2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_agrupador2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_agrupador2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 )
         {
            edtavContagemresultado_demandafm2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demandafm2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demandafm2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 )
         {
            tblTablemergeddynamicfilterscontagemresultado_datadmn2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultado_datadmn2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultado_datadmn2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 )
         {
            tblTablemergeddynamicfilterscontagemresultado_dataentregareal2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultado_dataentregareal2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultado_dataentregareal2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
            /* Execute user subroutine: 'UPDATECONTAGEMRESULTADO_DATAENTREGAREAL2OPERATORVALUES' */
            S222 ();
            if (returnInSub) return;
         }
         else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
         {
            cmbavContagemresultado_statusdmn2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContagemresultado_statusdmn2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSCNT") == 0 )
         {
            cmbavContagemresultado_statuscnt2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statuscnt2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContagemresultado_statuscnt2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
         {
            edtavContagemresultado_descricao2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_descricao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_descricao2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
         {
            dynavContagemresultado_sistemacod2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_sistemacod2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_sistemacod2.Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
         {
            edtavContagemresultado_agrupador2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_agrupador2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_agrupador2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S222( )
      {
         /* 'UPDATECONTAGEMRESULTADO_DATAENTREGAREAL2OPERATORVALUES' Routine */
         cellContagemresultado_dataentregareal_cell2_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultado_dataentregareal_cell2_Internalname, "Class", cellContagemresultado_dataentregareal_cell2_Class);
         cellContagemresultado_dataentregareal_to_cell2_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultado_dataentregareal_to_cell2_Internalname, "Class", cellContagemresultado_dataentregareal_to_cell2_Class);
         lblContagemresultado_dataentregareal_rangemiddletext_12_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblContagemresultado_dataentregareal_rangemiddletext_12_Internalname, "Class", lblContagemresultado_dataentregareal_rangemiddletext_12_Class);
         if ( AV27DynamicFiltersOperator2 == 0 )
         {
            AV57ContagemResultado_DataEntregaReal2 = DateTimeUtil.ResetTime( Gx_date ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ContagemResultado_DataEntregaReal2", context.localUtil.TToC( AV57ContagemResultado_DataEntregaReal2, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( AV27DynamicFiltersOperator2 == 1 )
         {
            AV57ContagemResultado_DataEntregaReal2 = DateTimeUtil.ResetTime( Gx_date ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ContagemResultado_DataEntregaReal2", context.localUtil.TToC( AV57ContagemResultado_DataEntregaReal2, 8, 5, 0, 3, "/", ":", " "));
            AV58ContagemResultado_DataEntregaReal_To2 = DateTimeUtil.ResetTime( DateTimeUtil.DAdd( Gx_date , + ( (int)(1) )) ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58ContagemResultado_DataEntregaReal_To2", context.localUtil.TToC( AV58ContagemResultado_DataEntregaReal_To2, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( AV27DynamicFiltersOperator2 == 2 )
         {
            AV57ContagemResultado_DataEntregaReal2 = DateTimeUtil.ResetTime( DateTimeUtil.DAdd( Gx_date , + ( (int)(1) )) ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ContagemResultado_DataEntregaReal2", context.localUtil.TToC( AV57ContagemResultado_DataEntregaReal2, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( AV27DynamicFiltersOperator2 == 3 )
         {
            cellContagemresultado_dataentregareal_cell2_Class = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultado_dataentregareal_cell2_Internalname, "Class", cellContagemresultado_dataentregareal_cell2_Class);
            cellContagemresultado_dataentregareal_to_cell2_Class = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultado_dataentregareal_to_cell2_Internalname, "Class", cellContagemresultado_dataentregareal_to_cell2_Class);
            lblContagemresultado_dataentregareal_rangemiddletext_12_Class = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblContagemresultado_dataentregareal_rangemiddletext_12_Internalname, "Class", lblContagemresultado_dataentregareal_rangemiddletext_12_Class);
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavContagemresultado_demandafm3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demandafm3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demandafm3_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontagemresultado_datadmn3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultado_datadmn3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultado_datadmn3_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontagemresultado_dataentregareal3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultado_dataentregareal3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultado_dataentregareal3_Visible), 5, 0)));
         cmbavContagemresultado_statusdmn3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContagemresultado_statusdmn3.Visible), 5, 0)));
         cmbavContagemresultado_statuscnt3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statuscnt3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContagemresultado_statuscnt3.Visible), 5, 0)));
         edtavContagemresultado_descricao3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_descricao3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_descricao3_Visible), 5, 0)));
         dynavContagemresultado_sistemacod3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_sistemacod3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_sistemacod3.Visible), 5, 0)));
         edtavContagemresultado_agrupador3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_agrupador3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_agrupador3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 )
         {
            edtavContagemresultado_demandafm3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demandafm3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demandafm3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 )
         {
            tblTablemergeddynamicfilterscontagemresultado_datadmn3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultado_datadmn3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultado_datadmn3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 )
         {
            tblTablemergeddynamicfilterscontagemresultado_dataentregareal3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultado_dataentregareal3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultado_dataentregareal3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
            /* Execute user subroutine: 'UPDATECONTAGEMRESULTADO_DATAENTREGAREAL3OPERATORVALUES' */
            S232 ();
            if (returnInSub) return;
         }
         else if ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
         {
            cmbavContagemresultado_statusdmn3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContagemresultado_statusdmn3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSCNT") == 0 )
         {
            cmbavContagemresultado_statuscnt3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statuscnt3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContagemresultado_statuscnt3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
         {
            edtavContagemresultado_descricao3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_descricao3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_descricao3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
         {
            dynavContagemresultado_sistemacod3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_sistemacod3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_sistemacod3.Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
         {
            edtavContagemresultado_agrupador3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_agrupador3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_agrupador3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S232( )
      {
         /* 'UPDATECONTAGEMRESULTADO_DATAENTREGAREAL3OPERATORVALUES' Routine */
         cellContagemresultado_dataentregareal_cell3_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultado_dataentregareal_cell3_Internalname, "Class", cellContagemresultado_dataentregareal_cell3_Class);
         cellContagemresultado_dataentregareal_to_cell3_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultado_dataentregareal_to_cell3_Internalname, "Class", cellContagemresultado_dataentregareal_to_cell3_Class);
         lblContagemresultado_dataentregareal_rangemiddletext_13_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblContagemresultado_dataentregareal_rangemiddletext_13_Internalname, "Class", lblContagemresultado_dataentregareal_rangemiddletext_13_Class);
         if ( AV32DynamicFiltersOperator3 == 0 )
         {
            AV65ContagemResultado_DataEntregaReal3 = DateTimeUtil.ResetTime( Gx_date ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ContagemResultado_DataEntregaReal3", context.localUtil.TToC( AV65ContagemResultado_DataEntregaReal3, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( AV32DynamicFiltersOperator3 == 1 )
         {
            AV65ContagemResultado_DataEntregaReal3 = DateTimeUtil.ResetTime( Gx_date ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ContagemResultado_DataEntregaReal3", context.localUtil.TToC( AV65ContagemResultado_DataEntregaReal3, 8, 5, 0, 3, "/", ":", " "));
            AV66ContagemResultado_DataEntregaReal_To3 = DateTimeUtil.ResetTime( DateTimeUtil.DAdd( Gx_date , + ( (int)(1) )) ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ContagemResultado_DataEntregaReal_To3", context.localUtil.TToC( AV66ContagemResultado_DataEntregaReal_To3, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( AV32DynamicFiltersOperator3 == 2 )
         {
            AV65ContagemResultado_DataEntregaReal3 = DateTimeUtil.ResetTime( DateTimeUtil.DAdd( Gx_date , + ( (int)(1) )) ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ContagemResultado_DataEntregaReal3", context.localUtil.TToC( AV65ContagemResultado_DataEntregaReal3, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( AV32DynamicFiltersOperator3 == 3 )
         {
            cellContagemresultado_dataentregareal_cell3_Class = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultado_dataentregareal_cell3_Internalname, "Class", cellContagemresultado_dataentregareal_cell3_Class);
            cellContagemresultado_dataentregareal_to_cell3_Class = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultado_dataentregareal_to_cell3_Internalname, "Class", cellContagemresultado_dataentregareal_to_cell3_Class);
            lblContagemresultado_dataentregareal_rangemiddletext_13_Class = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblContagemresultado_dataentregareal_rangemiddletext_13_Internalname, "Class", lblContagemresultado_dataentregareal_rangemiddletext_13_Class);
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV25DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersEnabled2", AV25DynamicFiltersEnabled2);
         AV26DynamicFiltersSelector2 = "CONTAGEMRESULTADO_DEMANDAFM";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector2", AV26DynamicFiltersSelector2);
         AV27DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator2), 4, 0)));
         AV56ContagemResultado_DemandaFM2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ContagemResultado_DemandaFM2", AV56ContagemResultado_DemandaFM2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if (returnInSub) return;
         AV30DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersEnabled3", AV30DynamicFiltersEnabled3);
         AV31DynamicFiltersSelector3 = "CONTAGEMRESULTADO_DEMANDAFM";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31DynamicFiltersSelector3", AV31DynamicFiltersSelector3);
         AV32DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32DynamicFiltersOperator3), 4, 0)));
         AV64ContagemResultado_DemandaFM3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64ContagemResultado_DemandaFM3", AV64ContagemResultado_DemandaFM3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if (returnInSub) return;
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV37Session.Get(AV192Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV192Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV37Session.Get(AV192Pgmname+"GridState"), "");
         }
         AV193GXV1 = 1;
         while ( AV193GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV193GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "CONTRATADA_AREATRABALHOCOD") == 0 )
            {
               AV13Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Contratada_AreaTrabalhoCod), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
            {
               AV15ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15ContagemResultado_ContratadaCod), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 )
            {
               AV16ContagemResultado_ServicoGrupo = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContagemResultado_ServicoGrupo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16ContagemResultado_ServicoGrupo), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "CONTAGEMRESULTADO_SERVICO") == 0 )
            {
               AV17ContagemResultado_Servico = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContagemResultado_Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17ContagemResultado_Servico), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "CONTAGEMRESULTADO_CNTADAOSVINC") == 0 )
            {
               AV18ContagemResultado_CntadaOsVinc = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultado_CntadaOsVinc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ContagemResultado_CntadaOsVinc), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "CONTAGEMRESULTADO_SERGRUPOVINC") == 0 )
            {
               AV19ContagemResultado_SerGrupoVinc = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultado_SerGrupoVinc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19ContagemResultado_SerGrupoVinc), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "CONTAGEMRESULTADO_CODSRVVNC") == 0 )
            {
               AV20ContagemResultado_CodSrvVnc = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContagemResultado_CodSrvVnc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20ContagemResultado_CodSrvVnc), 6, 0)));
            }
            AV193GXV1 = (int)(AV193GXV1+1);
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if (returnInSub) return;
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S202( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV21DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector1", AV21DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 )
            {
               AV22DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator1), 4, 0)));
               AV48ContagemResultado_DemandaFM1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ContagemResultado_DemandaFM1", AV48ContagemResultado_DemandaFM1);
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 )
            {
               AV22DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator1), 4, 0)));
               AV23ContagemResultado_DataDmn1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemResultado_DataDmn1", context.localUtil.Format(AV23ContagemResultado_DataDmn1, "99/99/99"));
               AV24ContagemResultado_DataDmn_To1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultado_DataDmn_To1", context.localUtil.Format(AV24ContagemResultado_DataDmn_To1, "99/99/99"));
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 )
            {
               AV22DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator1), 4, 0)));
               AV49ContagemResultado_DataEntregaReal1 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ContagemResultado_DataEntregaReal1", context.localUtil.TToC( AV49ContagemResultado_DataEntregaReal1, 8, 5, 0, 3, "/", ":", " "));
               AV50ContagemResultado_DataEntregaReal_To1 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ContagemResultado_DataEntregaReal_To1", context.localUtil.TToC( AV50ContagemResultado_DataEntregaReal_To1, 8, 5, 0, 3, "/", ":", " "));
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
            {
               AV51ContagemResultado_StatusDmn1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ContagemResultado_StatusDmn1", AV51ContagemResultado_StatusDmn1);
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSCNT") == 0 )
            {
               AV52ContagemResultado_StatusCnt1 = (short)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ContagemResultado_StatusCnt1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52ContagemResultado_StatusCnt1), 2, 0)));
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
            {
               AV22DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator1), 4, 0)));
               AV53ContagemResultado_Descricao1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ContagemResultado_Descricao1", AV53ContagemResultado_Descricao1);
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
            {
               AV22DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator1), 4, 0)));
               AV54ContagemResultado_SistemaCod1 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ContagemResultado_SistemaCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54ContagemResultado_SistemaCod1), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
            {
               AV22DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator1), 4, 0)));
               AV55ContagemResultado_Agrupador1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55ContagemResultado_Agrupador1", AV55ContagemResultado_Agrupador1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if (returnInSub) return;
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV25DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersEnabled2", AV25DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV26DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector2", AV26DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 )
               {
                  AV27DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator2), 4, 0)));
                  AV56ContagemResultado_DemandaFM2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ContagemResultado_DemandaFM2", AV56ContagemResultado_DemandaFM2);
               }
               else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 )
               {
                  AV27DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator2), 4, 0)));
                  AV28ContagemResultado_DataDmn2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ContagemResultado_DataDmn2", context.localUtil.Format(AV28ContagemResultado_DataDmn2, "99/99/99"));
                  AV29ContagemResultado_DataDmn_To2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContagemResultado_DataDmn_To2", context.localUtil.Format(AV29ContagemResultado_DataDmn_To2, "99/99/99"));
               }
               else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 )
               {
                  AV27DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator2), 4, 0)));
                  AV57ContagemResultado_DataEntregaReal2 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ContagemResultado_DataEntregaReal2", context.localUtil.TToC( AV57ContagemResultado_DataEntregaReal2, 8, 5, 0, 3, "/", ":", " "));
                  AV58ContagemResultado_DataEntregaReal_To2 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58ContagemResultado_DataEntregaReal_To2", context.localUtil.TToC( AV58ContagemResultado_DataEntregaReal_To2, 8, 5, 0, 3, "/", ":", " "));
               }
               else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
               {
                  AV59ContagemResultado_StatusDmn2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59ContagemResultado_StatusDmn2", AV59ContagemResultado_StatusDmn2);
               }
               else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSCNT") == 0 )
               {
                  AV60ContagemResultado_StatusCnt2 = (short)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60ContagemResultado_StatusCnt2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60ContagemResultado_StatusCnt2), 2, 0)));
               }
               else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
               {
                  AV27DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator2), 4, 0)));
                  AV61ContagemResultado_Descricao2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ContagemResultado_Descricao2", AV61ContagemResultado_Descricao2);
               }
               else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
               {
                  AV27DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator2), 4, 0)));
                  AV62ContagemResultado_SistemaCod2 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62ContagemResultado_SistemaCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62ContagemResultado_SistemaCod2), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
               {
                  AV27DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator2), 4, 0)));
                  AV63ContagemResultado_Agrupador2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ContagemResultado_Agrupador2", AV63ContagemResultado_Agrupador2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if (returnInSub) return;
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV30DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersEnabled3", AV30DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV31DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31DynamicFiltersSelector3", AV31DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 )
                  {
                     AV32DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32DynamicFiltersOperator3), 4, 0)));
                     AV64ContagemResultado_DemandaFM3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64ContagemResultado_DemandaFM3", AV64ContagemResultado_DemandaFM3);
                  }
                  else if ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 )
                  {
                     AV32DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32DynamicFiltersOperator3), 4, 0)));
                     AV33ContagemResultado_DataDmn3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContagemResultado_DataDmn3", context.localUtil.Format(AV33ContagemResultado_DataDmn3, "99/99/99"));
                     AV34ContagemResultado_DataDmn_To3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContagemResultado_DataDmn_To3", context.localUtil.Format(AV34ContagemResultado_DataDmn_To3, "99/99/99"));
                  }
                  else if ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 )
                  {
                     AV32DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32DynamicFiltersOperator3), 4, 0)));
                     AV65ContagemResultado_DataEntregaReal3 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ContagemResultado_DataEntregaReal3", context.localUtil.TToC( AV65ContagemResultado_DataEntregaReal3, 8, 5, 0, 3, "/", ":", " "));
                     AV66ContagemResultado_DataEntregaReal_To3 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ContagemResultado_DataEntregaReal_To3", context.localUtil.TToC( AV66ContagemResultado_DataEntregaReal_To3, 8, 5, 0, 3, "/", ":", " "));
                  }
                  else if ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                  {
                     AV67ContagemResultado_StatusDmn3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ContagemResultado_StatusDmn3", AV67ContagemResultado_StatusDmn3);
                  }
                  else if ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSCNT") == 0 )
                  {
                     AV68ContagemResultado_StatusCnt3 = (short)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ContagemResultado_StatusCnt3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68ContagemResultado_StatusCnt3), 2, 0)));
                  }
                  else if ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
                  {
                     AV32DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32DynamicFiltersOperator3), 4, 0)));
                     AV69ContagemResultado_Descricao3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ContagemResultado_Descricao3", AV69ContagemResultado_Descricao3);
                  }
                  else if ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                  {
                     AV32DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32DynamicFiltersOperator3), 4, 0)));
                     AV70ContagemResultado_SistemaCod3 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70ContagemResultado_SistemaCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV70ContagemResultado_SistemaCod3), 6, 0)));
                  }
                  else if ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
                  {
                     AV32DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32DynamicFiltersOperator3), 4, 0)));
                     AV71ContagemResultado_Agrupador3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ContagemResultado_Agrupador3", AV71ContagemResultado_Agrupador3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if (returnInSub) return;
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV35DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV37Session.Get(AV192Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! (0==AV13Contratada_AreaTrabalhoCod) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "CONTRATADA_AREATRABALHOCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV13Contratada_AreaTrabalhoCod), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV15ContagemResultado_ContratadaCod) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "CONTAGEMRESULTADO_CONTRATADACOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV15ContagemResultado_ContratadaCod), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV16ContagemResultado_ServicoGrupo) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "CONTAGEMRESULTADO_SERVICOGRUPO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV16ContagemResultado_ServicoGrupo), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV17ContagemResultado_Servico) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "CONTAGEMRESULTADO_SERVICO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV17ContagemResultado_Servico), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV18ContagemResultado_CntadaOsVinc) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "CONTAGEMRESULTADO_CNTADAOSVINC";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV18ContagemResultado_CntadaOsVinc), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV19ContagemResultado_SerGrupoVinc) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "CONTAGEMRESULTADO_SERGRUPOVINC";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV19ContagemResultado_SerGrupoVinc), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV20ContagemResultado_CodSrvVnc) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "CONTAGEMRESULTADO_CODSRVVNC";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV20ContagemResultado_CodSrvVnc), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if (returnInSub) return;
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV192Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV36DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV21DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ContagemResultado_DemandaFM1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV48ContagemResultado_DemandaFM1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ! ( (DateTime.MinValue==AV23ContagemResultado_DataDmn1) && (DateTime.MinValue==AV24ContagemResultado_DataDmn_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV23ContagemResultado_DataDmn1, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator1;
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV24ContagemResultado_DataDmn_To1, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ! ( (DateTime.MinValue==AV49ContagemResultado_DataEntregaReal1) && (DateTime.MinValue==AV50ContagemResultado_DataEntregaReal_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.TToC( AV49ContagemResultado_DataEntregaReal1, 8, 5, 0, 3, "/", ":", " ");
               AV12GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator1;
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.TToC( AV50ContagemResultado_DataEntregaReal_To1, 8, 5, 0, 3, "/", ":", " ");
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV51ContagemResultado_StatusDmn1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV51ContagemResultado_StatusDmn1;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSCNT") == 0 ) && ! (0==AV52ContagemResultado_StatusCnt1) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV52ContagemResultado_StatusCnt1), 2, 0);
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ContagemResultado_Descricao1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV53ContagemResultado_Descricao1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ! (0==AV54ContagemResultado_SistemaCod1) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV54ContagemResultado_SistemaCod1), 6, 0);
               AV12GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV55ContagemResultado_Agrupador1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV55ContagemResultado_Agrupador1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator1;
            }
            if ( AV35DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV25DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV26DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContagemResultado_DemandaFM2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV56ContagemResultado_DemandaFM2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV27DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ! ( (DateTime.MinValue==AV28ContagemResultado_DataDmn2) && (DateTime.MinValue==AV29ContagemResultado_DataDmn_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV28ContagemResultado_DataDmn2, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Operator = AV27DynamicFiltersOperator2;
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV29ContagemResultado_DataDmn_To2, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ! ( (DateTime.MinValue==AV57ContagemResultado_DataEntregaReal2) && (DateTime.MinValue==AV58ContagemResultado_DataEntregaReal_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.TToC( AV57ContagemResultado_DataEntregaReal2, 8, 5, 0, 3, "/", ":", " ");
               AV12GridStateDynamicFilter.gxTpr_Operator = AV27DynamicFiltersOperator2;
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.TToC( AV58ContagemResultado_DataEntregaReal_To2, 8, 5, 0, 3, "/", ":", " ");
            }
            else if ( ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV59ContagemResultado_StatusDmn2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV59ContagemResultado_StatusDmn2;
            }
            else if ( ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSCNT") == 0 ) && ! (0==AV60ContagemResultado_StatusCnt2) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV60ContagemResultado_StatusCnt2), 2, 0);
            }
            else if ( ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV61ContagemResultado_Descricao2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV61ContagemResultado_Descricao2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV27DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ! (0==AV62ContagemResultado_SistemaCod2) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV62ContagemResultado_SistemaCod2), 6, 0);
               AV12GridStateDynamicFilter.gxTpr_Operator = AV27DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV63ContagemResultado_Agrupador2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV63ContagemResultado_Agrupador2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV27DynamicFiltersOperator2;
            }
            if ( AV35DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV30DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV31DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV64ContagemResultado_DemandaFM3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV64ContagemResultado_DemandaFM3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV32DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ! ( (DateTime.MinValue==AV33ContagemResultado_DataDmn3) && (DateTime.MinValue==AV34ContagemResultado_DataDmn_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV33ContagemResultado_DataDmn3, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Operator = AV32DynamicFiltersOperator3;
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV34ContagemResultado_DataDmn_To3, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ! ( (DateTime.MinValue==AV65ContagemResultado_DataEntregaReal3) && (DateTime.MinValue==AV66ContagemResultado_DataEntregaReal_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.TToC( AV65ContagemResultado_DataEntregaReal3, 8, 5, 0, 3, "/", ":", " ");
               AV12GridStateDynamicFilter.gxTpr_Operator = AV32DynamicFiltersOperator3;
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.TToC( AV66ContagemResultado_DataEntregaReal_To3, 8, 5, 0, 3, "/", ":", " ");
            }
            else if ( ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV67ContagemResultado_StatusDmn3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV67ContagemResultado_StatusDmn3;
            }
            else if ( ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSCNT") == 0 ) && ! (0==AV68ContagemResultado_StatusCnt3) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV68ContagemResultado_StatusCnt3), 2, 0);
            }
            else if ( ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV69ContagemResultado_Descricao3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV69ContagemResultado_Descricao3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV32DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ! (0==AV70ContagemResultado_SistemaCod3) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV70ContagemResultado_SistemaCod3), 6, 0);
               AV12GridStateDynamicFilter.gxTpr_Operator = AV32DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV71ContagemResultado_Agrupador3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV71ContagemResultado_Agrupador3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV32DynamicFiltersOperator3;
            }
            if ( AV35DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV192Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ContagemResultado";
         AV37Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_TR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_TR2( true) ;
         }
         else
         {
            wb_table2_8_TR2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_TR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_181_TR2( true) ;
         }
         else
         {
            wb_table3_181_TR2( false) ;
         }
         return  ;
      }

      protected void wb_table3_181_TR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_TR2e( true) ;
         }
         else
         {
            wb_table1_2_TR2e( false) ;
         }
      }

      protected void wb_table3_181_TR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "TableGridHeader", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_184_TR2( true) ;
         }
         else
         {
            wb_table4_184_TR2( false) ;
         }
         return  ;
      }

      protected void wb_table4_184_TR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_181_TR2e( true) ;
         }
         else
         {
            wb_table3_181_TR2e( false) ;
         }
      }

      protected void wb_table4_184_TR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"187\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo Demanda") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(80), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "OS") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "OS Ref.") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(108), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Data da Demanda") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Data de Entrega") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(410), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "T�tulo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Sigla do Sistema") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "OS Vinculada") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Tipo de F�brica") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "PFBFM") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "PFBFS") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Esfor�o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV47Display));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV88ContagemResultado_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContagemresultado_codigo_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", AV89ContagemResultado_DemandaFM);
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", AV90ContagemResultado_Demanda);
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(AV91ContagemResultado_DataDmn, "99/99/99"));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( AV92ContagemResultado_DataEntregaReal, 10, 8, 0, 3, "/", ":", " "));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", AV93ContagemResultado_Descricao);
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV94ContagemrResultado_SistemaSigla));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14ContagemResultado_OSVinculada), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContagemresultado_osvinculada_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV95ContagemResultado_ContratadaTipoFab));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbavContagemresultado_contratadatipofab.Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV96ContagemResultado_PFBFMUltima, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContagemresultado_pfbfmultima_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV97ContagemResultado_PFBFSUltima, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContagemresultado_pfbfsultima_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV87Esforco, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavEsforco_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 187 )
         {
            wbEnd = 0;
            nRC_GXsfl_187 = (short)(nGXsfl_187_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_184_TR2e( true) ;
         }
         else
         {
            wb_table4_184_TR2e( false) ;
         }
      }

      protected void wb_table2_8_TR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table5_11_TR2( true) ;
         }
         else
         {
            wb_table5_11_TR2( false) ;
         }
         return  ;
      }

      protected void wb_table5_11_TR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_18_TR2( true) ;
         }
         else
         {
            wb_table6_18_TR2( false) ;
         }
         return  ;
      }

      protected void wb_table6_18_TR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_TR2e( true) ;
         }
         else
         {
            wb_table2_8_TR2e( false) ;
         }
      }

      protected void wb_table6_18_TR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "TableFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextcontratada_areatrabalhocod_Internalname, "�rea de Trabalho", "", "", lblFiltertextcontratada_areatrabalhocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'" + sGXsfl_187_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratada_areatrabalhocod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13Contratada_AreaTrabalhoCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV13Contratada_AreaTrabalhoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,23);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratada_areatrabalhocod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_26_TR2( true) ;
         }
         else
         {
            wb_table7_26_TR2( false) ;
         }
         return  ;
      }

      protected void wb_table7_26_TR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_43_TR2( true) ;
         }
         else
         {
            wb_table8_43_TR2( false) ;
         }
         return  ;
      }

      protected void wb_table8_43_TR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            wb_table9_61_TR2( true) ;
         }
         else
         {
            wb_table9_61_TR2( false) ;
         }
         return  ;
      }

      protected void wb_table9_61_TR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_18_TR2e( true) ;
         }
         else
         {
            wb_table6_18_TR2e( false) ;
         }
      }

      protected void wb_table9_61_TR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefiltersextras_Internalname, tblTablefiltersextras_Internalname, "", "TableFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_64_TR2( true) ;
         }
         else
         {
            wb_table10_64_TR2( false) ;
         }
         return  ;
      }

      protected void wb_table10_64_TR2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_61_TR2e( true) ;
         }
         else
         {
            wb_table9_61_TR2e( false) ;
         }
      }

      protected void wb_table10_64_TR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'" + sGXsfl_187_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV21DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,69);\"", "", true, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table11_73_TR2( true) ;
         }
         else
         {
            wb_table11_73_TR2( false) ;
         }
         return  ;
      }

      protected void wb_table11_73_TR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'" + sGXsfl_187_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV26DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,107);\"", "", true, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table12_111_TR2( true) ;
         }
         else
         {
            wb_table12_111_TR2( false) ;
         }
         return  ;
      }

      protected void wb_table12_111_TR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 139,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 140,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 145,'',false,'" + sGXsfl_187_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV31DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,145);\"", "", true, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV31DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table13_149_TR2( true) ;
         }
         else
         {
            wb_table13_149_TR2( false) ;
         }
         return  ;
      }

      protected void wb_table13_149_TR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 177,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_64_TR2e( true) ;
         }
         else
         {
            wb_table10_64_TR2e( false) ;
         }
      }

      protected void wb_table13_149_TR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 152,'',false,'" + sGXsfl_187_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV32DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSOPERATOR3.CLICK."+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,152);\"", "", true, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV32DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 154,'',false,'" + sGXsfl_187_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_demandafm3_Internalname, AV64ContagemResultado_DemandaFM3, StringUtil.RTrim( context.localUtil.Format( AV64ContagemResultado_DemandaFM3, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,154);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_demandafm3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_demandafm3_Visible, 1, 0, "text", "", 80, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            wb_table14_155_TR2( true) ;
         }
         else
         {
            wb_table14_155_TR2( false) ;
         }
         return  ;
      }

      protected void wb_table14_155_TR2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table15_163_TR2( true) ;
         }
         else
         {
            wb_table15_163_TR2( false) ;
         }
         return  ;
      }

      protected void wb_table15_163_TR2e( bool wbgen )
      {
         if ( wbgen )
         {
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 171,'',false,'" + sGXsfl_187_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContagemresultado_statusdmn3, cmbavContagemresultado_statusdmn3_Internalname, StringUtil.RTrim( AV67ContagemResultado_StatusDmn3), 1, cmbavContagemresultado_statusdmn3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavContagemresultado_statusdmn3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,171);\"", "", true, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            cmbavContagemresultado_statusdmn3.CurrentValue = StringUtil.RTrim( AV67ContagemResultado_StatusDmn3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn3_Internalname, "Values", (String)(cmbavContagemresultado_statusdmn3.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 172,'',false,'" + sGXsfl_187_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContagemresultado_statuscnt3, cmbavContagemresultado_statuscnt3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV68ContagemResultado_StatusCnt3), 2, 0)), 1, cmbavContagemresultado_statuscnt3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavContagemresultado_statuscnt3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,172);\"", "", true, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            cmbavContagemresultado_statuscnt3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV68ContagemResultado_StatusCnt3), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statuscnt3_Internalname, "Values", (String)(cmbavContagemresultado_statuscnt3.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 173,'',false,'" + sGXsfl_187_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_descricao3_Internalname, AV69ContagemResultado_Descricao3, StringUtil.RTrim( context.localUtil.Format( AV69ContagemResultado_Descricao3, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,173);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_descricao3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_descricao3_Visible, 1, 0, "text", "", 100, "%", 1, "row", 500, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 174,'',false,'" + sGXsfl_187_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_sistemacod3, dynavContagemresultado_sistemacod3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV70ContagemResultado_SistemaCod3), 6, 0)), 1, dynavContagemresultado_sistemacod3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynavContagemresultado_sistemacod3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,174);\"", "", true, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            dynavContagemresultado_sistemacod3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV70ContagemResultado_SistemaCod3), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_sistemacod3_Internalname, "Values", (String)(dynavContagemresultado_sistemacod3.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 175,'',false,'" + sGXsfl_187_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_agrupador3_Internalname, StringUtil.RTrim( AV71ContagemResultado_Agrupador3), StringUtil.RTrim( context.localUtil.Format( AV71ContagemResultado_Agrupador3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,175);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_agrupador3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_agrupador3_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, 0, true, "", "left", true, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table13_149_TR2e( true) ;
         }
         else
         {
            wb_table13_149_TR2e( false) ;
         }
      }

      protected void wb_table15_163_TR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagemresultado_dataentregareal3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagemresultado_dataentregareal3_Internalname, tblTablemergeddynamicfilterscontagemresultado_dataentregareal3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultado_dataentregareal_cell3_Internalname+"\"  class='"+cellContagemresultado_dataentregareal_cell3_Class+"'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 166,'',false,'" + sGXsfl_187_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_dataentregareal3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_dataentregareal3_Internalname, context.localUtil.TToC( AV65ContagemResultado_DataEntregaReal3, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV65ContagemResultado_DataEntregaReal3, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,166);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_dataentregareal3_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_dataentregareal3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultado_dataentregareal_rangemiddletext_1_cell3_Internalname+"\"  class=''>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemresultado_dataentregareal_rangemiddletext_13_Internalname, "to", "", "", lblContagemresultado_dataentregareal_rangemiddletext_13_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", lblContagemresultado_dataentregareal_rangemiddletext_13_Class, 0, "", 1, 1, 0, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultado_dataentregareal_to_cell3_Internalname+"\"  class='"+cellContagemresultado_dataentregareal_to_cell3_Class+"'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 170,'',false,'" + sGXsfl_187_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_dataentregareal_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_dataentregareal_to3_Internalname, context.localUtil.TToC( AV66ContagemResultado_DataEntregaReal_To3, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV66ContagemResultado_DataEntregaReal_To3, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,170);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_dataentregareal_to3_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_dataentregareal_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table15_163_TR2e( true) ;
         }
         else
         {
            wb_table15_163_TR2e( false) ;
         }
      }

      protected void wb_table14_155_TR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagemresultado_datadmn3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagemresultado_datadmn3_Internalname, tblTablemergeddynamicfilterscontagemresultado_datadmn3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 158,'',false,'" + sGXsfl_187_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_datadmn3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_datadmn3_Internalname, context.localUtil.Format(AV33ContagemResultado_DataDmn3, "99/99/99"), context.localUtil.Format( AV33ContagemResultado_DataDmn3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,158);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_datadmn3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 70, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_datadmn3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultado_datadmn_rangemiddletext_1_cell3_Internalname+"\"  class=''>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemresultado_datadmn_rangemiddletext_13_Internalname, "to", "", "", lblContagemresultado_datadmn_rangemiddletext_13_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultado_datadmn_to_cell3_Internalname+"\"  class=''>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 162,'',false,'" + sGXsfl_187_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_datadmn_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_datadmn_to3_Internalname, context.localUtil.Format(AV34ContagemResultado_DataDmn_To3, "99/99/99"), context.localUtil.Format( AV34ContagemResultado_DataDmn_To3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,162);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_datadmn_to3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 70, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_datadmn_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table14_155_TR2e( true) ;
         }
         else
         {
            wb_table14_155_TR2e( false) ;
         }
      }

      protected void wb_table12_111_TR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_187_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSOPERATOR2.CLICK."+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,114);\"", "", true, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'" + sGXsfl_187_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_demandafm2_Internalname, AV56ContagemResultado_DemandaFM2, StringUtil.RTrim( context.localUtil.Format( AV56ContagemResultado_DemandaFM2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,116);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_demandafm2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_demandafm2_Visible, 1, 0, "text", "", 80, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            wb_table16_117_TR2( true) ;
         }
         else
         {
            wb_table16_117_TR2( false) ;
         }
         return  ;
      }

      protected void wb_table16_117_TR2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table17_125_TR2( true) ;
         }
         else
         {
            wb_table17_125_TR2( false) ;
         }
         return  ;
      }

      protected void wb_table17_125_TR2e( bool wbgen )
      {
         if ( wbgen )
         {
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 133,'',false,'" + sGXsfl_187_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContagemresultado_statusdmn2, cmbavContagemresultado_statusdmn2_Internalname, StringUtil.RTrim( AV59ContagemResultado_StatusDmn2), 1, cmbavContagemresultado_statusdmn2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavContagemresultado_statusdmn2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,133);\"", "", true, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            cmbavContagemresultado_statusdmn2.CurrentValue = StringUtil.RTrim( AV59ContagemResultado_StatusDmn2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn2_Internalname, "Values", (String)(cmbavContagemresultado_statusdmn2.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 134,'',false,'" + sGXsfl_187_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContagemresultado_statuscnt2, cmbavContagemresultado_statuscnt2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV60ContagemResultado_StatusCnt2), 2, 0)), 1, cmbavContagemresultado_statuscnt2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavContagemresultado_statuscnt2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,134);\"", "", true, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            cmbavContagemresultado_statuscnt2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV60ContagemResultado_StatusCnt2), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statuscnt2_Internalname, "Values", (String)(cmbavContagemresultado_statuscnt2.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 135,'',false,'" + sGXsfl_187_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_descricao2_Internalname, AV61ContagemResultado_Descricao2, StringUtil.RTrim( context.localUtil.Format( AV61ContagemResultado_Descricao2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,135);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_descricao2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_descricao2_Visible, 1, 0, "text", "", 100, "%", 1, "row", 500, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 136,'',false,'" + sGXsfl_187_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_sistemacod2, dynavContagemresultado_sistemacod2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV62ContagemResultado_SistemaCod2), 6, 0)), 1, dynavContagemresultado_sistemacod2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynavContagemresultado_sistemacod2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,136);\"", "", true, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            dynavContagemresultado_sistemacod2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV62ContagemResultado_SistemaCod2), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_sistemacod2_Internalname, "Values", (String)(dynavContagemresultado_sistemacod2.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 137,'',false,'" + sGXsfl_187_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_agrupador2_Internalname, StringUtil.RTrim( AV63ContagemResultado_Agrupador2), StringUtil.RTrim( context.localUtil.Format( AV63ContagemResultado_Agrupador2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,137);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_agrupador2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_agrupador2_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, 0, true, "", "left", true, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table12_111_TR2e( true) ;
         }
         else
         {
            wb_table12_111_TR2e( false) ;
         }
      }

      protected void wb_table17_125_TR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagemresultado_dataentregareal2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagemresultado_dataentregareal2_Internalname, tblTablemergeddynamicfilterscontagemresultado_dataentregareal2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultado_dataentregareal_cell2_Internalname+"\"  class='"+cellContagemresultado_dataentregareal_cell2_Class+"'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 128,'',false,'" + sGXsfl_187_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_dataentregareal2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_dataentregareal2_Internalname, context.localUtil.TToC( AV57ContagemResultado_DataEntregaReal2, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV57ContagemResultado_DataEntregaReal2, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,128);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_dataentregareal2_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_dataentregareal2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultado_dataentregareal_rangemiddletext_1_cell2_Internalname+"\"  class=''>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemresultado_dataentregareal_rangemiddletext_12_Internalname, "to", "", "", lblContagemresultado_dataentregareal_rangemiddletext_12_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", lblContagemresultado_dataentregareal_rangemiddletext_12_Class, 0, "", 1, 1, 0, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultado_dataentregareal_to_cell2_Internalname+"\"  class='"+cellContagemresultado_dataentregareal_to_cell2_Class+"'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 132,'',false,'" + sGXsfl_187_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_dataentregareal_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_dataentregareal_to2_Internalname, context.localUtil.TToC( AV58ContagemResultado_DataEntregaReal_To2, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV58ContagemResultado_DataEntregaReal_To2, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,132);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_dataentregareal_to2_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_dataentregareal_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table17_125_TR2e( true) ;
         }
         else
         {
            wb_table17_125_TR2e( false) ;
         }
      }

      protected void wb_table16_117_TR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagemresultado_datadmn2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagemresultado_datadmn2_Internalname, tblTablemergeddynamicfilterscontagemresultado_datadmn2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 120,'',false,'" + sGXsfl_187_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_datadmn2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_datadmn2_Internalname, context.localUtil.Format(AV28ContagemResultado_DataDmn2, "99/99/99"), context.localUtil.Format( AV28ContagemResultado_DataDmn2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,120);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_datadmn2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 70, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_datadmn2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultado_datadmn_rangemiddletext_1_cell2_Internalname+"\"  class=''>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemresultado_datadmn_rangemiddletext_12_Internalname, "to", "", "", lblContagemresultado_datadmn_rangemiddletext_12_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultado_datadmn_to_cell2_Internalname+"\"  class=''>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 124,'',false,'" + sGXsfl_187_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_datadmn_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_datadmn_to2_Internalname, context.localUtil.Format(AV29ContagemResultado_DataDmn_To2, "99/99/99"), context.localUtil.Format( AV29ContagemResultado_DataDmn_To2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,124);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_datadmn_to2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 70, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_datadmn_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table16_117_TR2e( true) ;
         }
         else
         {
            wb_table16_117_TR2e( false) ;
         }
      }

      protected void wb_table11_73_TR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'" + sGXsfl_187_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSOPERATOR1.CLICK."+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,76);\"", "", true, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'" + sGXsfl_187_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_demandafm1_Internalname, AV48ContagemResultado_DemandaFM1, StringUtil.RTrim( context.localUtil.Format( AV48ContagemResultado_DemandaFM1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,78);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_demandafm1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_demandafm1_Visible, 1, 0, "text", "", 80, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            wb_table18_79_TR2( true) ;
         }
         else
         {
            wb_table18_79_TR2( false) ;
         }
         return  ;
      }

      protected void wb_table18_79_TR2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table19_87_TR2( true) ;
         }
         else
         {
            wb_table19_87_TR2( false) ;
         }
         return  ;
      }

      protected void wb_table19_87_TR2e( bool wbgen )
      {
         if ( wbgen )
         {
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'',false,'" + sGXsfl_187_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContagemresultado_statusdmn1, cmbavContagemresultado_statusdmn1_Internalname, StringUtil.RTrim( AV51ContagemResultado_StatusDmn1), 1, cmbavContagemresultado_statusdmn1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavContagemresultado_statusdmn1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,95);\"", "", true, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            cmbavContagemresultado_statusdmn1.CurrentValue = StringUtil.RTrim( AV51ContagemResultado_StatusDmn1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn1_Internalname, "Values", (String)(cmbavContagemresultado_statusdmn1.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'" + sGXsfl_187_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContagemresultado_statuscnt1, cmbavContagemresultado_statuscnt1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV52ContagemResultado_StatusCnt1), 2, 0)), 1, cmbavContagemresultado_statuscnt1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavContagemresultado_statuscnt1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,96);\"", "", true, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            cmbavContagemresultado_statuscnt1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV52ContagemResultado_StatusCnt1), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statuscnt1_Internalname, "Values", (String)(cmbavContagemresultado_statuscnt1.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'" + sGXsfl_187_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_descricao1_Internalname, AV53ContagemResultado_Descricao1, StringUtil.RTrim( context.localUtil.Format( AV53ContagemResultado_Descricao1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,97);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_descricao1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_descricao1_Visible, 1, 0, "text", "", 100, "%", 1, "row", 500, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'" + sGXsfl_187_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_sistemacod1, dynavContagemresultado_sistemacod1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV54ContagemResultado_SistemaCod1), 6, 0)), 1, dynavContagemresultado_sistemacod1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynavContagemresultado_sistemacod1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,98);\"", "", true, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            dynavContagemresultado_sistemacod1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV54ContagemResultado_SistemaCod1), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_sistemacod1_Internalname, "Values", (String)(dynavContagemresultado_sistemacod1.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'" + sGXsfl_187_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_agrupador1_Internalname, StringUtil.RTrim( AV55ContagemResultado_Agrupador1), StringUtil.RTrim( context.localUtil.Format( AV55ContagemResultado_Agrupador1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,99);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_agrupador1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_agrupador1_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, 0, true, "", "left", true, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table11_73_TR2e( true) ;
         }
         else
         {
            wb_table11_73_TR2e( false) ;
         }
      }

      protected void wb_table19_87_TR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagemresultado_dataentregareal1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagemresultado_dataentregareal1_Internalname, tblTablemergeddynamicfilterscontagemresultado_dataentregareal1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultado_dataentregareal_cell1_Internalname+"\"  class='"+cellContagemresultado_dataentregareal_cell1_Class+"'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 90,'',false,'" + sGXsfl_187_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_dataentregareal1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_dataentregareal1_Internalname, context.localUtil.TToC( AV49ContagemResultado_DataEntregaReal1, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV49ContagemResultado_DataEntregaReal1, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,90);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_dataentregareal1_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_dataentregareal1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultado_dataentregareal_rangemiddletext_1_cell1_Internalname+"\"  class=''>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemresultado_dataentregareal_rangemiddletext_11_Internalname, "to", "", "", lblContagemresultado_dataentregareal_rangemiddletext_11_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", lblContagemresultado_dataentregareal_rangemiddletext_11_Class, 0, "", 1, 1, 0, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultado_dataentregareal_to_cell1_Internalname+"\"  class='"+cellContagemresultado_dataentregareal_to_cell1_Class+"'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'" + sGXsfl_187_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_dataentregareal_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_dataentregareal_to1_Internalname, context.localUtil.TToC( AV50ContagemResultado_DataEntregaReal_To1, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV50ContagemResultado_DataEntregaReal_To1, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,94);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_dataentregareal_to1_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_dataentregareal_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table19_87_TR2e( true) ;
         }
         else
         {
            wb_table19_87_TR2e( false) ;
         }
      }

      protected void wb_table18_79_TR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagemresultado_datadmn1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagemresultado_datadmn1_Internalname, tblTablemergeddynamicfilterscontagemresultado_datadmn1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'" + sGXsfl_187_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_datadmn1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_datadmn1_Internalname, context.localUtil.Format(AV23ContagemResultado_DataDmn1, "99/99/99"), context.localUtil.Format( AV23ContagemResultado_DataDmn1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,82);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_datadmn1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 70, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_datadmn1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultado_datadmn_rangemiddletext_1_cell1_Internalname+"\"  class=''>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemresultado_datadmn_rangemiddletext_11_Internalname, "to", "", "", lblContagemresultado_datadmn_rangemiddletext_11_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultado_datadmn_to_cell1_Internalname+"\"  class=''>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'',false,'" + sGXsfl_187_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_datadmn_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_datadmn_to1_Internalname, context.localUtil.Format(AV24ContagemResultado_DataDmn_To1, "99/99/99"), context.localUtil.Format( AV24ContagemResultado_DataDmn_To1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,86);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_datadmn_to1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 70, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_datadmn_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table18_79_TR2e( true) ;
         }
         else
         {
            wb_table18_79_TR2e( false) ;
         }
      }

      protected void wb_table8_43_TR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefiltersb_Internalname, tblTablefiltersb_Internalname, "", "TableFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextcontagemresultado_cntadaosvinc_Internalname, "Contratada", "", "", lblFiltertextcontagemresultado_cntadaosvinc_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'" + sGXsfl_187_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_cntadaosvinc, dynavContagemresultado_cntadaosvinc_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV18ContagemResultado_CntadaOsVinc), 6, 0)), 1, dynavContagemresultado_cntadaosvinc_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynavContagemresultado_cntadaosvinc.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,48);\"", "", true, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            dynavContagemresultado_cntadaosvinc.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV18ContagemResultado_CntadaOsVinc), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_cntadaosvinc_Internalname, "Values", (String)(dynavContagemresultado_cntadaosvinc.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextcontagemresultado_sergrupovinc_Internalname, "Grupo de Servi�os", "", "", lblFiltertextcontagemresultado_sergrupovinc_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'" + sGXsfl_187_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_sergrupovinc, dynavContagemresultado_sergrupovinc_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV19ContagemResultado_SerGrupoVinc), 6, 0)), 1, dynavContagemresultado_sergrupovinc_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,53);\"", "", true, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            dynavContagemresultado_sergrupovinc.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV19ContagemResultado_SerGrupoVinc), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_sergrupovinc_Internalname, "Values", (String)(dynavContagemresultado_sergrupovinc.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextcontagemresultado_codsrvvnc_Internalname, "Servi�o", "", "", lblFiltertextcontagemresultado_codsrvvnc_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'" + sGXsfl_187_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_codsrvvnc, dynavContagemresultado_codsrvvnc_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV20ContagemResultado_CodSrvVnc), 6, 0)), 1, dynavContagemresultado_codsrvvnc_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,58);\"", "", true, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            dynavContagemresultado_codsrvvnc.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20ContagemResultado_CodSrvVnc), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_codsrvvnc_Internalname, "Values", (String)(dynavContagemresultado_codsrvvnc.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_43_TR2e( true) ;
         }
         else
         {
            wb_table8_43_TR2e( false) ;
         }
      }

      protected void wb_table7_26_TR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefiltersa_Internalname, tblTablefiltersa_Internalname, "", "TableFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextcontagemresultado_contratadacod_Internalname, "Contratada", "", "", lblFiltertextcontagemresultado_contratadacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_187_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_contratadacod, dynavContagemresultado_contratadacod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV15ContagemResultado_ContratadaCod), 6, 0)), 1, dynavContagemresultado_contratadacod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynavContagemresultado_contratadacod.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            dynavContagemresultado_contratadacod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15ContagemResultado_ContratadaCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contratadacod_Internalname, "Values", (String)(dynavContagemresultado_contratadacod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextcontagemresultado_servicogrupo_Internalname, "Grupo de Servi�os", "", "", lblFiltertextcontagemresultado_servicogrupo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'" + sGXsfl_187_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_servicogrupo, dynavContagemresultado_servicogrupo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16ContagemResultado_ServicoGrupo), 6, 0)), 1, dynavContagemresultado_servicogrupo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,36);\"", "", true, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            dynavContagemresultado_servicogrupo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16ContagemResultado_ServicoGrupo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_servicogrupo_Internalname, "Values", (String)(dynavContagemresultado_servicogrupo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextcontagemresultado_servico_Internalname, "Servi�o", "", "", lblFiltertextcontagemresultado_servico_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'" + sGXsfl_187_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_servico, dynavContagemresultado_servico_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV17ContagemResultado_Servico), 6, 0)), 1, dynavContagemresultado_servico_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,41);\"", "", true, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            dynavContagemresultado_servico.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17ContagemResultado_Servico), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_servico_Internalname, "Values", (String)(dynavContagemresultado_servico.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_26_TR2e( true) ;
         }
         else
         {
            wb_table7_26_TR2e( false) ;
         }
      }

      protected void wb_table5_11_TR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemresultadotitle_Internalname, "Relat�rio de Compara��o entre Demandas", "", "", lblContagemresultadotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgExport_Internalname, context.GetImagePath( "da69a816-fd11-445b-8aaf-1a2f7f1acc93", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Exportar p/ Excel!", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgExport_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOEXPORT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_RelatorioComparacaoDemandas_OLD.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_11_TR2e( true) ;
         }
         else
         {
            wb_table5_11_TR2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PATR2( ) ;
         WSTR2( ) ;
         WETR2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?5113033");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20204191254648");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_relatoriocomparacaodemandas_old.js", "?20204191254649");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_1872( )
      {
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_187_idx;
         edtavContagemresultado_codigo_Internalname = "vCONTAGEMRESULTADO_CODIGO_"+sGXsfl_187_idx;
         edtavContagemresultado_demandafm_Internalname = "vCONTAGEMRESULTADO_DEMANDAFM_"+sGXsfl_187_idx;
         edtavContagemresultado_demanda_Internalname = "vCONTAGEMRESULTADO_DEMANDA_"+sGXsfl_187_idx;
         edtavContagemresultado_datadmn_Internalname = "vCONTAGEMRESULTADO_DATADMN_"+sGXsfl_187_idx;
         edtavContagemresultado_dataentregareal_Internalname = "vCONTAGEMRESULTADO_DATAENTREGAREAL_"+sGXsfl_187_idx;
         edtavContagemresultado_descricao_Internalname = "vCONTAGEMRESULTADO_DESCRICAO_"+sGXsfl_187_idx;
         edtavContagemrresultado_sistemasigla_Internalname = "vCONTAGEMRRESULTADO_SISTEMASIGLA_"+sGXsfl_187_idx;
         edtavContagemresultado_osvinculada_Internalname = "vCONTAGEMRESULTADO_OSVINCULADA_"+sGXsfl_187_idx;
         cmbavContagemresultado_contratadatipofab_Internalname = "vCONTAGEMRESULTADO_CONTRATADATIPOFAB_"+sGXsfl_187_idx;
         edtavContagemresultado_pfbfmultima_Internalname = "vCONTAGEMRESULTADO_PFBFMULTIMA_"+sGXsfl_187_idx;
         edtavContagemresultado_pfbfsultima_Internalname = "vCONTAGEMRESULTADO_PFBFSULTIMA_"+sGXsfl_187_idx;
         edtavEsforco_Internalname = "vESFORCO_"+sGXsfl_187_idx;
      }

      protected void SubsflControlProps_fel_1872( )
      {
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_187_fel_idx;
         edtavContagemresultado_codigo_Internalname = "vCONTAGEMRESULTADO_CODIGO_"+sGXsfl_187_fel_idx;
         edtavContagemresultado_demandafm_Internalname = "vCONTAGEMRESULTADO_DEMANDAFM_"+sGXsfl_187_fel_idx;
         edtavContagemresultado_demanda_Internalname = "vCONTAGEMRESULTADO_DEMANDA_"+sGXsfl_187_fel_idx;
         edtavContagemresultado_datadmn_Internalname = "vCONTAGEMRESULTADO_DATADMN_"+sGXsfl_187_fel_idx;
         edtavContagemresultado_dataentregareal_Internalname = "vCONTAGEMRESULTADO_DATAENTREGAREAL_"+sGXsfl_187_fel_idx;
         edtavContagemresultado_descricao_Internalname = "vCONTAGEMRESULTADO_DESCRICAO_"+sGXsfl_187_fel_idx;
         edtavContagemrresultado_sistemasigla_Internalname = "vCONTAGEMRRESULTADO_SISTEMASIGLA_"+sGXsfl_187_fel_idx;
         edtavContagemresultado_osvinculada_Internalname = "vCONTAGEMRESULTADO_OSVINCULADA_"+sGXsfl_187_fel_idx;
         cmbavContagemresultado_contratadatipofab_Internalname = "vCONTAGEMRESULTADO_CONTRATADATIPOFAB_"+sGXsfl_187_fel_idx;
         edtavContagemresultado_pfbfmultima_Internalname = "vCONTAGEMRESULTADO_PFBFMULTIMA_"+sGXsfl_187_fel_idx;
         edtavContagemresultado_pfbfsultima_Internalname = "vCONTAGEMRESULTADO_PFBFSULTIMA_"+sGXsfl_187_fel_idx;
         edtavEsforco_Internalname = "vESFORCO_"+sGXsfl_187_fel_idx;
      }

      protected void sendrow_1872( )
      {
         SubsflControlProps_1872( ) ;
         WBTR0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_187_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_187_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_187_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV47Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV47Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV189Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV47Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV47Display)) ? AV189Display_GXI : context.PathToRelativeUrl( AV47Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV47Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavContagemresultado_codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV88ContagemResultado_Codigo), 6, 0, ",", "")),((edtavContagemresultado_codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV88ContagemResultado_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV88ContagemResultado_Codigo), "ZZZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavContagemresultado_codigo_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavContagemresultado_codigo_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)187,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavContagemresultado_demandafm_Internalname,(String)AV89ContagemResultado_DemandaFM,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavContagemresultado_demandafm_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)80,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)187,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavContagemresultado_demanda_Internalname,(String)AV90ContagemResultado_Demanda,StringUtil.RTrim( context.localUtil.Format( AV90ContagemResultado_Demanda, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavContagemresultado_demanda_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)30,(short)0,(short)0,(short)187,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavContagemresultado_datadmn_Internalname,context.localUtil.Format(AV91ContagemResultado_DataDmn, "99/99/99"),context.localUtil.Format( AV91ContagemResultado_DataDmn, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavContagemresultado_datadmn_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)108,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)187,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavContagemresultado_dataentregareal_Internalname,context.localUtil.TToC( AV92ContagemResultado_DataEntregaReal, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( AV92ContagemResultado_DataEntregaReal, "99/99/99 99:99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavContagemresultado_dataentregareal_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)187,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavContagemresultado_descricao_Internalname,(String)AV93ContagemResultado_Descricao,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavContagemresultado_descricao_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)410,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)0,(short)187,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavContagemrresultado_sistemasigla_Internalname,StringUtil.RTrim( AV94ContagemrResultado_SistemaSigla),StringUtil.RTrim( context.localUtil.Format( AV94ContagemrResultado_SistemaSigla, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavContagemrresultado_sistemasigla_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)25,(short)0,(short)0,(short)187,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavContagemresultado_osvinculada_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14ContagemResultado_OSVinculada), 6, 0, ",", "")),((edtavContagemresultado_osvinculada_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV14ContagemResultado_OSVinculada), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV14ContagemResultado_OSVinculada), "ZZZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavContagemresultado_osvinculada_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavContagemresultado_osvinculada_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)187,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            if ( ( nGXsfl_187_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "vCONTAGEMRESULTADO_CONTRATADATIPOFAB_" + sGXsfl_187_idx;
               cmbavContagemresultado_contratadatipofab.Name = GXCCtl;
               cmbavContagemresultado_contratadatipofab.WebTags = "";
               cmbavContagemresultado_contratadatipofab.addItem("", "(Nenhuma)", 0);
               cmbavContagemresultado_contratadatipofab.addItem("S", "F�brica de Software", 0);
               cmbavContagemresultado_contratadatipofab.addItem("M", "F�brica de M�trica", 0);
               cmbavContagemresultado_contratadatipofab.addItem("I", "Departamento / Setor", 0);
               cmbavContagemresultado_contratadatipofab.addItem("O", "Outras", 0);
               if ( cmbavContagemresultado_contratadatipofab.ItemCount > 0 )
               {
                  AV95ContagemResultado_ContratadaTipoFab = cmbavContagemresultado_contratadatipofab.getValidValue(AV95ContagemResultado_ContratadaTipoFab);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavContagemresultado_contratadatipofab_Internalname, AV95ContagemResultado_ContratadaTipoFab);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavContagemresultado_contratadatipofab,(String)cmbavContagemresultado_contratadatipofab_Internalname,StringUtil.RTrim( AV95ContagemResultado_ContratadaTipoFab),(short)1,(String)cmbavContagemresultado_contratadatipofab_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)0,cmbavContagemresultado_contratadatipofab.Enabled,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute",(String)"",(String)"",(String)"",(bool)true});
            cmbavContagemresultado_contratadatipofab.CurrentValue = StringUtil.RTrim( AV95ContagemResultado_ContratadaTipoFab);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_contratadatipofab_Internalname, "Values", (String)(cmbavContagemresultado_contratadatipofab.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavContagemresultado_pfbfmultima_Internalname,StringUtil.LTrim( StringUtil.NToC( AV96ContagemResultado_PFBFMUltima, 14, 5, ",", "")),((edtavContagemresultado_pfbfmultima_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV96ContagemResultado_PFBFMUltima, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV96ContagemResultado_PFBFMUltima, "ZZ,ZZZ,ZZ9.999")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavContagemresultado_pfbfmultima_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavContagemresultado_pfbfmultima_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)187,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavContagemresultado_pfbfsultima_Internalname,StringUtil.LTrim( StringUtil.NToC( AV97ContagemResultado_PFBFSUltima, 14, 5, ",", "")),((edtavContagemresultado_pfbfsultima_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV97ContagemResultado_PFBFSUltima, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV97ContagemResultado_PFBFSUltima, "ZZ,ZZZ,ZZ9.999")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavContagemresultado_pfbfsultima_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavContagemresultado_pfbfsultima_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)187,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavEsforco_Internalname,StringUtil.LTrim( StringUtil.NToC( AV87Esforco, 14, 5, ",", "")),((edtavEsforco_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV87Esforco, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV87Esforco, "ZZ,ZZZ,ZZ9.999")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavEsforco_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavEsforco_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)187,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            GridContainer.AddRow(GridRow);
            nGXsfl_187_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_187_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_187_idx+1));
            sGXsfl_187_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_187_idx), 4, 0)), 4, "0");
            SubsflControlProps_1872( ) ;
         }
         /* End function sendrow_1872 */
      }

      protected void init_default_properties( )
      {
         lblContagemresultadotitle_Internalname = "CONTAGEMRESULTADOTITLE";
         imgExport_Internalname = "EXPORT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblFiltertextcontratada_areatrabalhocod_Internalname = "FILTERTEXTCONTRATADA_AREATRABALHOCOD";
         edtavContratada_areatrabalhocod_Internalname = "vCONTRATADA_AREATRABALHOCOD";
         lblFiltertextcontagemresultado_contratadacod_Internalname = "FILTERTEXTCONTAGEMRESULTADO_CONTRATADACOD";
         dynavContagemresultado_contratadacod_Internalname = "vCONTAGEMRESULTADO_CONTRATADACOD";
         lblFiltertextcontagemresultado_servicogrupo_Internalname = "FILTERTEXTCONTAGEMRESULTADO_SERVICOGRUPO";
         dynavContagemresultado_servicogrupo_Internalname = "vCONTAGEMRESULTADO_SERVICOGRUPO";
         lblFiltertextcontagemresultado_servico_Internalname = "FILTERTEXTCONTAGEMRESULTADO_SERVICO";
         dynavContagemresultado_servico_Internalname = "vCONTAGEMRESULTADO_SERVICO";
         tblTablefiltersa_Internalname = "TABLEFILTERSA";
         lblFiltertextcontagemresultado_cntadaosvinc_Internalname = "FILTERTEXTCONTAGEMRESULTADO_CNTADAOSVINC";
         dynavContagemresultado_cntadaosvinc_Internalname = "vCONTAGEMRESULTADO_CNTADAOSVINC";
         lblFiltertextcontagemresultado_sergrupovinc_Internalname = "FILTERTEXTCONTAGEMRESULTADO_SERGRUPOVINC";
         dynavContagemresultado_sergrupovinc_Internalname = "vCONTAGEMRESULTADO_SERGRUPOVINC";
         lblFiltertextcontagemresultado_codsrvvnc_Internalname = "FILTERTEXTCONTAGEMRESULTADO_CODSRVVNC";
         dynavContagemresultado_codsrvvnc_Internalname = "vCONTAGEMRESULTADO_CODSRVVNC";
         tblTablefiltersb_Internalname = "TABLEFILTERSB";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavContagemresultado_demandafm1_Internalname = "vCONTAGEMRESULTADO_DEMANDAFM1";
         edtavContagemresultado_datadmn1_Internalname = "vCONTAGEMRESULTADO_DATADMN1";
         lblContagemresultado_datadmn_rangemiddletext_11_Internalname = "CONTAGEMRESULTADO_DATADMN_RANGEMIDDLETEXT_11";
         cellContagemresultado_datadmn_rangemiddletext_1_cell1_Internalname = "CONTAGEMRESULTADO_DATADMN_RANGEMIDDLETEXT_1_CELL1";
         edtavContagemresultado_datadmn_to1_Internalname = "vCONTAGEMRESULTADO_DATADMN_TO1";
         cellContagemresultado_datadmn_to_cell1_Internalname = "CONTAGEMRESULTADO_DATADMN_TO_CELL1";
         tblTablemergeddynamicfilterscontagemresultado_datadmn1_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN1";
         edtavContagemresultado_dataentregareal1_Internalname = "vCONTAGEMRESULTADO_DATAENTREGAREAL1";
         cellContagemresultado_dataentregareal_cell1_Internalname = "CONTAGEMRESULTADO_DATAENTREGAREAL_CELL1";
         lblContagemresultado_dataentregareal_rangemiddletext_11_Internalname = "CONTAGEMRESULTADO_DATAENTREGAREAL_RANGEMIDDLETEXT_11";
         cellContagemresultado_dataentregareal_rangemiddletext_1_cell1_Internalname = "CONTAGEMRESULTADO_DATAENTREGAREAL_RANGEMIDDLETEXT_1_CELL1";
         edtavContagemresultado_dataentregareal_to1_Internalname = "vCONTAGEMRESULTADO_DATAENTREGAREAL_TO1";
         cellContagemresultado_dataentregareal_to_cell1_Internalname = "CONTAGEMRESULTADO_DATAENTREGAREAL_TO_CELL1";
         tblTablemergeddynamicfilterscontagemresultado_dataentregareal1_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAENTREGAREAL1";
         cmbavContagemresultado_statusdmn1_Internalname = "vCONTAGEMRESULTADO_STATUSDMN1";
         cmbavContagemresultado_statuscnt1_Internalname = "vCONTAGEMRESULTADO_STATUSCNT1";
         edtavContagemresultado_descricao1_Internalname = "vCONTAGEMRESULTADO_DESCRICAO1";
         dynavContagemresultado_sistemacod1_Internalname = "vCONTAGEMRESULTADO_SISTEMACOD1";
         edtavContagemresultado_agrupador1_Internalname = "vCONTAGEMRESULTADO_AGRUPADOR1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavContagemresultado_demandafm2_Internalname = "vCONTAGEMRESULTADO_DEMANDAFM2";
         edtavContagemresultado_datadmn2_Internalname = "vCONTAGEMRESULTADO_DATADMN2";
         lblContagemresultado_datadmn_rangemiddletext_12_Internalname = "CONTAGEMRESULTADO_DATADMN_RANGEMIDDLETEXT_12";
         cellContagemresultado_datadmn_rangemiddletext_1_cell2_Internalname = "CONTAGEMRESULTADO_DATADMN_RANGEMIDDLETEXT_1_CELL2";
         edtavContagemresultado_datadmn_to2_Internalname = "vCONTAGEMRESULTADO_DATADMN_TO2";
         cellContagemresultado_datadmn_to_cell2_Internalname = "CONTAGEMRESULTADO_DATADMN_TO_CELL2";
         tblTablemergeddynamicfilterscontagemresultado_datadmn2_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN2";
         edtavContagemresultado_dataentregareal2_Internalname = "vCONTAGEMRESULTADO_DATAENTREGAREAL2";
         cellContagemresultado_dataentregareal_cell2_Internalname = "CONTAGEMRESULTADO_DATAENTREGAREAL_CELL2";
         lblContagemresultado_dataentregareal_rangemiddletext_12_Internalname = "CONTAGEMRESULTADO_DATAENTREGAREAL_RANGEMIDDLETEXT_12";
         cellContagemresultado_dataentregareal_rangemiddletext_1_cell2_Internalname = "CONTAGEMRESULTADO_DATAENTREGAREAL_RANGEMIDDLETEXT_1_CELL2";
         edtavContagemresultado_dataentregareal_to2_Internalname = "vCONTAGEMRESULTADO_DATAENTREGAREAL_TO2";
         cellContagemresultado_dataentregareal_to_cell2_Internalname = "CONTAGEMRESULTADO_DATAENTREGAREAL_TO_CELL2";
         tblTablemergeddynamicfilterscontagemresultado_dataentregareal2_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAENTREGAREAL2";
         cmbavContagemresultado_statusdmn2_Internalname = "vCONTAGEMRESULTADO_STATUSDMN2";
         cmbavContagemresultado_statuscnt2_Internalname = "vCONTAGEMRESULTADO_STATUSCNT2";
         edtavContagemresultado_descricao2_Internalname = "vCONTAGEMRESULTADO_DESCRICAO2";
         dynavContagemresultado_sistemacod2_Internalname = "vCONTAGEMRESULTADO_SISTEMACOD2";
         edtavContagemresultado_agrupador2_Internalname = "vCONTAGEMRESULTADO_AGRUPADOR2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavContagemresultado_demandafm3_Internalname = "vCONTAGEMRESULTADO_DEMANDAFM3";
         edtavContagemresultado_datadmn3_Internalname = "vCONTAGEMRESULTADO_DATADMN3";
         lblContagemresultado_datadmn_rangemiddletext_13_Internalname = "CONTAGEMRESULTADO_DATADMN_RANGEMIDDLETEXT_13";
         cellContagemresultado_datadmn_rangemiddletext_1_cell3_Internalname = "CONTAGEMRESULTADO_DATADMN_RANGEMIDDLETEXT_1_CELL3";
         edtavContagemresultado_datadmn_to3_Internalname = "vCONTAGEMRESULTADO_DATADMN_TO3";
         cellContagemresultado_datadmn_to_cell3_Internalname = "CONTAGEMRESULTADO_DATADMN_TO_CELL3";
         tblTablemergeddynamicfilterscontagemresultado_datadmn3_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN3";
         edtavContagemresultado_dataentregareal3_Internalname = "vCONTAGEMRESULTADO_DATAENTREGAREAL3";
         cellContagemresultado_dataentregareal_cell3_Internalname = "CONTAGEMRESULTADO_DATAENTREGAREAL_CELL3";
         lblContagemresultado_dataentregareal_rangemiddletext_13_Internalname = "CONTAGEMRESULTADO_DATAENTREGAREAL_RANGEMIDDLETEXT_13";
         cellContagemresultado_dataentregareal_rangemiddletext_1_cell3_Internalname = "CONTAGEMRESULTADO_DATAENTREGAREAL_RANGEMIDDLETEXT_1_CELL3";
         edtavContagemresultado_dataentregareal_to3_Internalname = "vCONTAGEMRESULTADO_DATAENTREGAREAL_TO3";
         cellContagemresultado_dataentregareal_to_cell3_Internalname = "CONTAGEMRESULTADO_DATAENTREGAREAL_TO_CELL3";
         tblTablemergeddynamicfilterscontagemresultado_dataentregareal3_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAENTREGAREAL3";
         cmbavContagemresultado_statusdmn3_Internalname = "vCONTAGEMRESULTADO_STATUSDMN3";
         cmbavContagemresultado_statuscnt3_Internalname = "vCONTAGEMRESULTADO_STATUSCNT3";
         edtavContagemresultado_descricao3_Internalname = "vCONTAGEMRESULTADO_DESCRICAO3";
         dynavContagemresultado_sistemacod3_Internalname = "vCONTAGEMRESULTADO_SISTEMACOD3";
         edtavContagemresultado_agrupador3_Internalname = "vCONTAGEMRESULTADO_AGRUPADOR3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefiltersextras_Internalname = "TABLEFILTERSEXTRAS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavDisplay_Internalname = "vDISPLAY";
         edtavContagemresultado_codigo_Internalname = "vCONTAGEMRESULTADO_CODIGO";
         edtavContagemresultado_demandafm_Internalname = "vCONTAGEMRESULTADO_DEMANDAFM";
         edtavContagemresultado_demanda_Internalname = "vCONTAGEMRESULTADO_DEMANDA";
         edtavContagemresultado_datadmn_Internalname = "vCONTAGEMRESULTADO_DATADMN";
         edtavContagemresultado_dataentregareal_Internalname = "vCONTAGEMRESULTADO_DATAENTREGAREAL";
         edtavContagemresultado_descricao_Internalname = "vCONTAGEMRESULTADO_DESCRICAO";
         edtavContagemrresultado_sistemasigla_Internalname = "vCONTAGEMRRESULTADO_SISTEMASIGLA";
         edtavContagemresultado_osvinculada_Internalname = "vCONTAGEMRESULTADO_OSVINCULADA";
         cmbavContagemresultado_contratadatipofab_Internalname = "vCONTAGEMRESULTADO_CONTRATADATIPOFAB";
         edtavContagemresultado_pfbfmultima_Internalname = "vCONTAGEMRESULTADO_PFBFMULTIMA";
         edtavContagemresultado_pfbfsultima_Internalname = "vCONTAGEMRESULTADO_PFBFSULTIMA";
         edtavEsforco_Internalname = "vESFORCO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavEsforco_Jsonclick = "";
         edtavContagemresultado_pfbfsultima_Jsonclick = "";
         edtavContagemresultado_pfbfmultima_Jsonclick = "";
         cmbavContagemresultado_contratadatipofab_Jsonclick = "";
         edtavContagemresultado_osvinculada_Jsonclick = "";
         edtavContagemrresultado_sistemasigla_Jsonclick = "";
         edtavContagemresultado_descricao_Jsonclick = "";
         edtavContagemresultado_dataentregareal_Jsonclick = "";
         edtavContagemresultado_datadmn_Jsonclick = "";
         edtavContagemresultado_demanda_Jsonclick = "";
         edtavContagemresultado_demandafm_Jsonclick = "";
         edtavContagemresultado_codigo_Jsonclick = "";
         dynavContagemresultado_servico_Jsonclick = "";
         dynavContagemresultado_servicogrupo_Jsonclick = "";
         dynavContagemresultado_contratadacod_Jsonclick = "";
         dynavContagemresultado_codsrvvnc_Jsonclick = "";
         dynavContagemresultado_sergrupovinc_Jsonclick = "";
         dynavContagemresultado_cntadaosvinc_Jsonclick = "";
         edtavContagemresultado_datadmn_to1_Jsonclick = "";
         edtavContagemresultado_datadmn1_Jsonclick = "";
         edtavContagemresultado_dataentregareal_to1_Jsonclick = "";
         cellContagemresultado_dataentregareal_to_cell1_Class = "";
         edtavContagemresultado_dataentregareal1_Jsonclick = "";
         cellContagemresultado_dataentregareal_cell1_Class = "";
         edtavContagemresultado_agrupador1_Jsonclick = "";
         dynavContagemresultado_sistemacod1_Jsonclick = "";
         edtavContagemresultado_descricao1_Jsonclick = "";
         cmbavContagemresultado_statuscnt1_Jsonclick = "";
         cmbavContagemresultado_statusdmn1_Jsonclick = "";
         edtavContagemresultado_demandafm1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavContagemresultado_datadmn_to2_Jsonclick = "";
         edtavContagemresultado_datadmn2_Jsonclick = "";
         edtavContagemresultado_dataentregareal_to2_Jsonclick = "";
         cellContagemresultado_dataentregareal_to_cell2_Class = "";
         edtavContagemresultado_dataentregareal2_Jsonclick = "";
         cellContagemresultado_dataentregareal_cell2_Class = "";
         edtavContagemresultado_agrupador2_Jsonclick = "";
         dynavContagemresultado_sistemacod2_Jsonclick = "";
         edtavContagemresultado_descricao2_Jsonclick = "";
         cmbavContagemresultado_statuscnt2_Jsonclick = "";
         cmbavContagemresultado_statusdmn2_Jsonclick = "";
         edtavContagemresultado_demandafm2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavContagemresultado_datadmn_to3_Jsonclick = "";
         edtavContagemresultado_datadmn3_Jsonclick = "";
         edtavContagemresultado_dataentregareal_to3_Jsonclick = "";
         cellContagemresultado_dataentregareal_to_cell3_Class = "";
         edtavContagemresultado_dataentregareal3_Jsonclick = "";
         cellContagemresultado_dataentregareal_cell3_Class = "";
         edtavContagemresultado_agrupador3_Jsonclick = "";
         dynavContagemresultado_sistemacod3_Jsonclick = "";
         edtavContagemresultado_descricao3_Jsonclick = "";
         cmbavContagemresultado_statuscnt3_Jsonclick = "";
         cmbavContagemresultado_statusdmn3_Jsonclick = "";
         edtavContagemresultado_demandafm3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavContratada_areatrabalhocod_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavEsforco_Enabled = 0;
         edtavContagemresultado_pfbfsultima_Enabled = 0;
         edtavContagemresultado_pfbfmultima_Enabled = 0;
         cmbavContagemresultado_contratadatipofab.Enabled = 0;
         edtavContagemresultado_osvinculada_Enabled = 0;
         edtavContagemresultado_codigo_Enabled = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         lblContagemresultado_dataentregareal_rangemiddletext_13_Class = "DataFilterDescription";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavContagemresultado_agrupador3_Visible = 1;
         dynavContagemresultado_sistemacod3.Visible = 1;
         edtavContagemresultado_descricao3_Visible = 1;
         cmbavContagemresultado_statuscnt3.Visible = 1;
         cmbavContagemresultado_statusdmn3.Visible = 1;
         tblTablemergeddynamicfilterscontagemresultado_dataentregareal3_Visible = 1;
         tblTablemergeddynamicfilterscontagemresultado_datadmn3_Visible = 1;
         edtavContagemresultado_demandafm3_Visible = 1;
         lblContagemresultado_dataentregareal_rangemiddletext_12_Class = "DataFilterDescription";
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavContagemresultado_agrupador2_Visible = 1;
         dynavContagemresultado_sistemacod2.Visible = 1;
         edtavContagemresultado_descricao2_Visible = 1;
         cmbavContagemresultado_statuscnt2.Visible = 1;
         cmbavContagemresultado_statusdmn2.Visible = 1;
         tblTablemergeddynamicfilterscontagemresultado_dataentregareal2_Visible = 1;
         tblTablemergeddynamicfilterscontagemresultado_datadmn2_Visible = 1;
         edtavContagemresultado_demandafm2_Visible = 1;
         lblContagemresultado_dataentregareal_rangemiddletext_11_Class = "DataFilterDescription";
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavContagemresultado_agrupador1_Visible = 1;
         dynavContagemresultado_sistemacod1.Visible = 1;
         edtavContagemresultado_descricao1_Visible = 1;
         cmbavContagemresultado_statuscnt1.Visible = 1;
         cmbavContagemresultado_statusdmn1.Visible = 1;
         tblTablemergeddynamicfilterscontagemresultado_dataentregareal1_Visible = 1;
         tblTablemergeddynamicfilterscontagemresultado_datadmn1_Visible = 1;
         edtavContagemresultado_demandafm1_Visible = 1;
         edtavDisplay_Link = "";
         edtavDisplay_Tooltiptext = "Mostrar";
         dynavContagemresultado_cntadaosvinc.Enabled = 1;
         dynavContagemresultado_contratadacod.Enabled = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Relat�rio de Compara��o entre Demandas";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public void Validv_Contagemresultado_contratadacod( GXCombobox dynGX_Parm1 ,
                                                          GXCombobox dynGX_Parm2 ,
                                                          GXCombobox dynGX_Parm3 )
      {
         dynavContagemresultado_contratadacod = dynGX_Parm1;
         AV15ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( dynavContagemresultado_contratadacod.CurrentValue, "."));
         dynavContagemresultado_servicogrupo = dynGX_Parm2;
         AV16ContagemResultado_ServicoGrupo = (int)(NumberUtil.Val( dynavContagemresultado_servicogrupo.CurrentValue, "."));
         dynavContagemresultado_servico = dynGX_Parm3;
         AV17ContagemResultado_Servico = (int)(NumberUtil.Val( dynavContagemresultado_servico.CurrentValue, "."));
         GXVvCONTAGEMRESULTADO_SERVICOGRUPO_htmlTR2( AV15ContagemResultado_ContratadaCod) ;
         GXVvCONTAGEMRESULTADO_SERVICO_htmlTR2( AV15ContagemResultado_ContratadaCod, AV16ContagemResultado_ServicoGrupo) ;
         dynload_actions( ) ;
         if ( dynavContagemresultado_servicogrupo.ItemCount > 0 )
         {
            AV16ContagemResultado_ServicoGrupo = (int)(NumberUtil.Val( dynavContagemresultado_servicogrupo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16ContagemResultado_ServicoGrupo), 6, 0))), "."));
         }
         dynavContagemresultado_servicogrupo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16ContagemResultado_ServicoGrupo), 6, 0));
         isValidOutput.Add(dynavContagemresultado_servicogrupo);
         if ( dynavContagemresultado_servico.ItemCount > 0 )
         {
            AV17ContagemResultado_Servico = (int)(NumberUtil.Val( dynavContagemresultado_servico.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17ContagemResultado_Servico), 6, 0))), "."));
         }
         dynavContagemresultado_servico.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17ContagemResultado_Servico), 6, 0));
         isValidOutput.Add(dynavContagemresultado_servico);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Validv_Contagemresultado_servicogrupo( GXCombobox dynGX_Parm1 ,
                                                         GXCombobox dynGX_Parm2 ,
                                                         GXCombobox dynGX_Parm3 )
      {
         dynavContagemresultado_contratadacod = dynGX_Parm1;
         AV15ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( dynavContagemresultado_contratadacod.CurrentValue, "."));
         dynavContagemresultado_servicogrupo = dynGX_Parm2;
         AV16ContagemResultado_ServicoGrupo = (int)(NumberUtil.Val( dynavContagemresultado_servicogrupo.CurrentValue, "."));
         dynavContagemresultado_servico = dynGX_Parm3;
         AV17ContagemResultado_Servico = (int)(NumberUtil.Val( dynavContagemresultado_servico.CurrentValue, "."));
         GXVvCONTAGEMRESULTADO_SERVICO_htmlTR2( AV15ContagemResultado_ContratadaCod, AV16ContagemResultado_ServicoGrupo) ;
         dynload_actions( ) ;
         if ( dynavContagemresultado_servico.ItemCount > 0 )
         {
            AV17ContagemResultado_Servico = (int)(NumberUtil.Val( dynavContagemresultado_servico.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17ContagemResultado_Servico), 6, 0))), "."));
         }
         dynavContagemresultado_servico.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17ContagemResultado_Servico), 6, 0));
         isValidOutput.Add(dynavContagemresultado_servico);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Validv_Contagemresultado_cntadaosvinc( GXCombobox dynGX_Parm1 ,
                                                         GXCombobox dynGX_Parm2 ,
                                                         GXCombobox dynGX_Parm3 )
      {
         dynavContagemresultado_cntadaosvinc = dynGX_Parm1;
         AV18ContagemResultado_CntadaOsVinc = (int)(NumberUtil.Val( dynavContagemresultado_cntadaosvinc.CurrentValue, "."));
         dynavContagemresultado_sergrupovinc = dynGX_Parm2;
         AV19ContagemResultado_SerGrupoVinc = (int)(NumberUtil.Val( dynavContagemresultado_sergrupovinc.CurrentValue, "."));
         dynavContagemresultado_codsrvvnc = dynGX_Parm3;
         AV20ContagemResultado_CodSrvVnc = (int)(NumberUtil.Val( dynavContagemresultado_codsrvvnc.CurrentValue, "."));
         GXVvCONTAGEMRESULTADO_SERGRUPOVINC_htmlTR2( AV18ContagemResultado_CntadaOsVinc) ;
         GXVvCONTAGEMRESULTADO_CODSRVVNC_htmlTR2( AV18ContagemResultado_CntadaOsVinc, AV19ContagemResultado_SerGrupoVinc) ;
         dynload_actions( ) ;
         if ( dynavContagemresultado_sergrupovinc.ItemCount > 0 )
         {
            AV19ContagemResultado_SerGrupoVinc = (int)(NumberUtil.Val( dynavContagemresultado_sergrupovinc.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV19ContagemResultado_SerGrupoVinc), 6, 0))), "."));
         }
         dynavContagemresultado_sergrupovinc.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV19ContagemResultado_SerGrupoVinc), 6, 0));
         isValidOutput.Add(dynavContagemresultado_sergrupovinc);
         if ( dynavContagemresultado_codsrvvnc.ItemCount > 0 )
         {
            AV20ContagemResultado_CodSrvVnc = (int)(NumberUtil.Val( dynavContagemresultado_codsrvvnc.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20ContagemResultado_CodSrvVnc), 6, 0))), "."));
         }
         dynavContagemresultado_codsrvvnc.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20ContagemResultado_CodSrvVnc), 6, 0));
         isValidOutput.Add(dynavContagemresultado_codsrvvnc);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Validv_Contagemresultado_sergrupovinc( GXCombobox dynGX_Parm1 ,
                                                         GXCombobox dynGX_Parm2 ,
                                                         GXCombobox dynGX_Parm3 )
      {
         dynavContagemresultado_cntadaosvinc = dynGX_Parm1;
         AV18ContagemResultado_CntadaOsVinc = (int)(NumberUtil.Val( dynavContagemresultado_cntadaosvinc.CurrentValue, "."));
         dynavContagemresultado_sergrupovinc = dynGX_Parm2;
         AV19ContagemResultado_SerGrupoVinc = (int)(NumberUtil.Val( dynavContagemresultado_sergrupovinc.CurrentValue, "."));
         dynavContagemresultado_codsrvvnc = dynGX_Parm3;
         AV20ContagemResultado_CodSrvVnc = (int)(NumberUtil.Val( dynavContagemresultado_codsrvvnc.CurrentValue, "."));
         GXVvCONTAGEMRESULTADO_CODSRVVNC_htmlTR2( AV18ContagemResultado_CntadaOsVinc, AV19ContagemResultado_SerGrupoVinc) ;
         dynload_actions( ) ;
         if ( dynavContagemresultado_codsrvvnc.ItemCount > 0 )
         {
            AV20ContagemResultado_CodSrvVnc = (int)(NumberUtil.Val( dynavContagemresultado_codsrvvnc.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20ContagemResultado_CodSrvVnc), 6, 0))), "."));
         }
         dynavContagemresultado_codsrvvnc.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20ContagemResultado_CodSrvVnc), 6, 0));
         isValidOutput.Add(dynavContagemresultado_codsrvvnc);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A457ContagemResultado_Demanda',fld:'CONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A764ContagemResultado_ServicoGrupo',fld:'CONTAGEMRESULTADO_SERVICOGRUPO',pic:'ZZZZZ9',nv:0},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'A2017ContagemResultado_DataEntregaReal',fld:'CONTAGEMRESULTADO_DATAENTREGAREAL',pic:'99/99/99 99:99',nv:''},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'A494ContagemResultado_Descricao',fld:'CONTAGEMRESULTADO_DESCRICAO',pic:'',nv:''},{av:'A489ContagemResultado_SistemaCod',fld:'CONTAGEMRESULTADO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A1046ContagemResultado_Agrupador',fld:'CONTAGEMRESULTADO_AGRUPADOR',pic:'@!',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A509ContagemrResultado_SistemaSigla',fld:'CONTAGEMRRESULTADO_SISTEMASIGLA',pic:'@!',nv:''},{av:'A1326ContagemResultado_ContratadaTipoFab',fld:'CONTAGEMRESULTADO_CONTRATADATIPOFAB',pic:'',nv:''},{av:'A682ContagemResultado_PFBFMUltima',fld:'CONTAGEMRESULTADO_PFBFMULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A684ContagemResultado_PFBFSUltima',fld:'CONTAGEMRESULTADO_PFBFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV14ContagemResultado_OSVinculada',fld:'vCONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'AV21DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV25DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV192Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV16ContagemResultado_ServicoGrupo',fld:'vCONTAGEMRESULTADO_SERVICOGRUPO',pic:'ZZZZZ9',nv:0},{av:'AV17ContagemResultado_Servico',fld:'vCONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV18ContagemResultado_CntadaOsVinc',fld:'vCONTAGEMRESULTADO_CNTADAOSVINC',pic:'ZZZZZ9',nv:0},{av:'AV19ContagemResultado_SerGrupoVinc',fld:'vCONTAGEMRESULTADO_SERGRUPOVINC',pic:'ZZZZZ9',nv:0},{av:'AV20ContagemResultado_CodSrvVnc',fld:'vCONTAGEMRESULTADO_CODSRVVNC',pic:'ZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV36DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV48ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'AV24ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV23ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV50ContagemResultado_DataEntregaReal_To1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV49ContagemResultado_DataEntregaReal1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL1',pic:'99/99/99 99:99',nv:''},{av:'AV51ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'AV52ContagemResultado_StatusCnt1',fld:'vCONTAGEMRESULTADO_STATUSCNT1',pic:'Z9',nv:0},{av:'AV53ContagemResultado_Descricao1',fld:'vCONTAGEMRESULTADO_DESCRICAO1',pic:'',nv:''},{av:'AV54ContagemResultado_SistemaCod1',fld:'vCONTAGEMRESULTADO_SISTEMACOD1',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV55ContagemResultado_Agrupador1',fld:'vCONTAGEMRESULTADO_AGRUPADOR1',pic:'@!',nv:''},{av:'AV35DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV56ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV29ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV28ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV58ContagemResultado_DataEntregaReal_To2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV57ContagemResultado_DataEntregaReal2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL2',pic:'99/99/99 99:99',nv:''},{av:'AV59ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV60ContagemResultado_StatusCnt2',fld:'vCONTAGEMRESULTADO_STATUSCNT2',pic:'Z9',nv:0},{av:'AV61ContagemResultado_Descricao2',fld:'vCONTAGEMRESULTADO_DESCRICAO2',pic:'',nv:''},{av:'AV62ContagemResultado_SistemaCod2',fld:'vCONTAGEMRESULTADO_SISTEMACOD2',pic:'ZZZZZ9',nv:0},{av:'AV27DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV63ContagemResultado_Agrupador2',fld:'vCONTAGEMRESULTADO_AGRUPADOR2',pic:'@!',nv:''},{av:'AV64ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'AV34ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'AV33ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV66ContagemResultado_DataEntregaReal_To3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV65ContagemResultado_DataEntregaReal3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL3',pic:'99/99/99 99:99',nv:''},{av:'AV67ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'AV68ContagemResultado_StatusCnt3',fld:'vCONTAGEMRESULTADO_STATUSCNT3',pic:'Z9',nv:0},{av:'AV69ContagemResultado_Descricao3',fld:'vCONTAGEMRESULTADO_DESCRICAO3',pic:'',nv:''},{av:'AV70ContagemResultado_SistemaCod3',fld:'vCONTAGEMRESULTADO_SISTEMACOD3',pic:'ZZZZZ9',nv:0},{av:'AV32DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV71ContagemResultado_Agrupador3',fld:'vCONTAGEMRESULTADO_AGRUPADOR3',pic:'@!',nv:''}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV22DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV27DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV32DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV45GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV46GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11TR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV21DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV25DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV192Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV16ContagemResultado_ServicoGrupo',fld:'vCONTAGEMRESULTADO_SERVICOGRUPO',pic:'ZZZZZ9',nv:0},{av:'AV17ContagemResultado_Servico',fld:'vCONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV18ContagemResultado_CntadaOsVinc',fld:'vCONTAGEMRESULTADO_CNTADAOSVINC',pic:'ZZZZZ9',nv:0},{av:'AV19ContagemResultado_SerGrupoVinc',fld:'vCONTAGEMRESULTADO_SERGRUPOVINC',pic:'ZZZZZ9',nv:0},{av:'AV20ContagemResultado_CodSrvVnc',fld:'vCONTAGEMRESULTADO_CODSRVVNC',pic:'ZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV36DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV48ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'AV24ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV23ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV50ContagemResultado_DataEntregaReal_To1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV49ContagemResultado_DataEntregaReal1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL1',pic:'99/99/99 99:99',nv:''},{av:'AV51ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'AV52ContagemResultado_StatusCnt1',fld:'vCONTAGEMRESULTADO_STATUSCNT1',pic:'Z9',nv:0},{av:'AV53ContagemResultado_Descricao1',fld:'vCONTAGEMRESULTADO_DESCRICAO1',pic:'',nv:''},{av:'AV54ContagemResultado_SistemaCod1',fld:'vCONTAGEMRESULTADO_SISTEMACOD1',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV55ContagemResultado_Agrupador1',fld:'vCONTAGEMRESULTADO_AGRUPADOR1',pic:'@!',nv:''},{av:'AV35DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV56ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV29ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV28ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV58ContagemResultado_DataEntregaReal_To2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV57ContagemResultado_DataEntregaReal2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL2',pic:'99/99/99 99:99',nv:''},{av:'AV59ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV60ContagemResultado_StatusCnt2',fld:'vCONTAGEMRESULTADO_STATUSCNT2',pic:'Z9',nv:0},{av:'AV61ContagemResultado_Descricao2',fld:'vCONTAGEMRESULTADO_DESCRICAO2',pic:'',nv:''},{av:'AV62ContagemResultado_SistemaCod2',fld:'vCONTAGEMRESULTADO_SISTEMACOD2',pic:'ZZZZZ9',nv:0},{av:'AV27DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV63ContagemResultado_Agrupador2',fld:'vCONTAGEMRESULTADO_AGRUPADOR2',pic:'@!',nv:''},{av:'AV64ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'AV34ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'AV33ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV66ContagemResultado_DataEntregaReal_To3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV65ContagemResultado_DataEntregaReal3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL3',pic:'99/99/99 99:99',nv:''},{av:'AV67ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'AV68ContagemResultado_StatusCnt3',fld:'vCONTAGEMRESULTADO_STATUSCNT3',pic:'Z9',nv:0},{av:'AV69ContagemResultado_Descricao3',fld:'vCONTAGEMRESULTADO_DESCRICAO3',pic:'',nv:''},{av:'AV70ContagemResultado_SistemaCod3',fld:'vCONTAGEMRESULTADO_SISTEMACOD3',pic:'ZZZZZ9',nv:0},{av:'AV32DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV71ContagemResultado_Agrupador3',fld:'vCONTAGEMRESULTADO_AGRUPADOR3',pic:'@!',nv:''},{av:'A457ContagemResultado_Demanda',fld:'CONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A764ContagemResultado_ServicoGrupo',fld:'CONTAGEMRESULTADO_SERVICOGRUPO',pic:'ZZZZZ9',nv:0},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'A2017ContagemResultado_DataEntregaReal',fld:'CONTAGEMRESULTADO_DATAENTREGAREAL',pic:'99/99/99 99:99',nv:''},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'A494ContagemResultado_Descricao',fld:'CONTAGEMRESULTADO_DESCRICAO',pic:'',nv:''},{av:'A489ContagemResultado_SistemaCod',fld:'CONTAGEMRESULTADO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A1046ContagemResultado_Agrupador',fld:'CONTAGEMRESULTADO_AGRUPADOR',pic:'@!',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A509ContagemrResultado_SistemaSigla',fld:'CONTAGEMRRESULTADO_SISTEMASIGLA',pic:'@!',nv:''},{av:'A1326ContagemResultado_ContratadaTipoFab',fld:'CONTAGEMRESULTADO_CONTRATADATIPOFAB',pic:'',nv:''},{av:'A682ContagemResultado_PFBFMUltima',fld:'CONTAGEMRESULTADO_PFBFMULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A684ContagemResultado_PFBFSUltima',fld:'CONTAGEMRESULTADO_PFBFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV14ContagemResultado_OSVinculada',fld:'vCONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("GRID.LOAD","{handler:'E26TR2',iparms:[{av:'A457ContagemResultado_Demanda',fld:'CONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV15ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A764ContagemResultado_ServicoGrupo',fld:'CONTAGEMRESULTADO_SERVICOGRUPO',pic:'ZZZZZ9',nv:0},{av:'AV16ContagemResultado_ServicoGrupo',fld:'vCONTAGEMRESULTADO_SERVICOGRUPO',pic:'ZZZZZ9',nv:0},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV17ContagemResultado_Servico',fld:'vCONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'AV21DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV22DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV48ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV23ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV24ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'A2017ContagemResultado_DataEntregaReal',fld:'CONTAGEMRESULTADO_DATAENTREGAREAL',pic:'99/99/99 99:99',nv:''},{av:'AV49ContagemResultado_DataEntregaReal1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL1',pic:'99/99/99 99:99',nv:''},{av:'AV50ContagemResultado_DataEntregaReal_To1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO1',pic:'99/99/99 99:99',nv:''},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'AV51ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'A494ContagemResultado_Descricao',fld:'CONTAGEMRESULTADO_DESCRICAO',pic:'',nv:''},{av:'AV53ContagemResultado_Descricao1',fld:'vCONTAGEMRESULTADO_DESCRICAO1',pic:'',nv:''},{av:'A489ContagemResultado_SistemaCod',fld:'CONTAGEMRESULTADO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV54ContagemResultado_SistemaCod1',fld:'vCONTAGEMRESULTADO_SISTEMACOD1',pic:'ZZZZZ9',nv:0},{av:'A1046ContagemResultado_Agrupador',fld:'CONTAGEMRESULTADO_AGRUPADOR',pic:'@!',nv:''},{av:'AV55ContagemResultado_Agrupador1',fld:'vCONTAGEMRESULTADO_AGRUPADOR1',pic:'@!',nv:''},{av:'AV25DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV27DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV56ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV28ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV29ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV57ContagemResultado_DataEntregaReal2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL2',pic:'99/99/99 99:99',nv:''},{av:'AV58ContagemResultado_DataEntregaReal_To2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV59ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV61ContagemResultado_Descricao2',fld:'vCONTAGEMRESULTADO_DESCRICAO2',pic:'',nv:''},{av:'AV62ContagemResultado_SistemaCod2',fld:'vCONTAGEMRESULTADO_SISTEMACOD2',pic:'ZZZZZ9',nv:0},{av:'AV63ContagemResultado_Agrupador2',fld:'vCONTAGEMRESULTADO_AGRUPADOR2',pic:'@!',nv:''},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV64ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'AV33ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV34ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'AV65ContagemResultado_DataEntregaReal3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL3',pic:'99/99/99 99:99',nv:''},{av:'AV66ContagemResultado_DataEntregaReal_To3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV67ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'AV69ContagemResultado_Descricao3',fld:'vCONTAGEMRESULTADO_DESCRICAO3',pic:'',nv:''},{av:'AV70ContagemResultado_SistemaCod3',fld:'vCONTAGEMRESULTADO_SISTEMACOD3',pic:'ZZZZZ9',nv:0},{av:'AV71ContagemResultado_Agrupador3',fld:'vCONTAGEMRESULTADO_AGRUPADOR3',pic:'@!',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A509ContagemrResultado_SistemaSigla',fld:'CONTAGEMRRESULTADO_SISTEMASIGLA',pic:'@!',nv:''},{av:'A1326ContagemResultado_ContratadaTipoFab',fld:'CONTAGEMRESULTADO_CONTRATADATIPOFAB',pic:'',nv:''},{av:'A682ContagemResultado_PFBFMUltima',fld:'CONTAGEMRESULTADO_PFBFMULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A684ContagemResultado_PFBFSUltima',fld:'CONTAGEMRESULTADO_PFBFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV18ContagemResultado_CntadaOsVinc',fld:'vCONTAGEMRESULTADO_CNTADAOSVINC',pic:'ZZZZZ9',nv:0},{av:'AV19ContagemResultado_SerGrupoVinc',fld:'vCONTAGEMRESULTADO_SERGRUPOVINC',pic:'ZZZZZ9',nv:0},{av:'AV20ContagemResultado_CodSrvVnc',fld:'vCONTAGEMRESULTADO_CODSRVVNC',pic:'ZZZZZ9',nv:0},{av:'AV14ContagemResultado_OSVinculada',fld:'vCONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0}],oparms:[{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'AV47Display',fld:'vDISPLAY',pic:'',nv:''},{av:'AV88ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV89ContagemResultado_DemandaFM',fld:'vCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV90ContagemResultado_Demanda',fld:'vCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV91ContagemResultado_DataDmn',fld:'vCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV92ContagemResultado_DataEntregaReal',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL',pic:'99/99/99 99:99',nv:''},{av:'AV93ContagemResultado_Descricao',fld:'vCONTAGEMRESULTADO_DESCRICAO',pic:'',nv:''},{av:'AV94ContagemrResultado_SistemaSigla',fld:'vCONTAGEMRRESULTADO_SISTEMASIGLA',pic:'@!',nv:''},{av:'AV14ContagemResultado_OSVinculada',fld:'vCONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'AV95ContagemResultado_ContratadaTipoFab',fld:'vCONTAGEMRESULTADO_CONTRATADATIPOFAB',pic:'',nv:''},{av:'AV96ContagemResultado_PFBFMUltima',fld:'vCONTAGEMRESULTADO_PFBFMULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV97ContagemResultado_PFBFSUltima',fld:'vCONTAGEMRESULTADO_PFBFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV87Esforco',fld:'vESFORCO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E16TR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV21DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV25DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV192Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV16ContagemResultado_ServicoGrupo',fld:'vCONTAGEMRESULTADO_SERVICOGRUPO',pic:'ZZZZZ9',nv:0},{av:'AV17ContagemResultado_Servico',fld:'vCONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV18ContagemResultado_CntadaOsVinc',fld:'vCONTAGEMRESULTADO_CNTADAOSVINC',pic:'ZZZZZ9',nv:0},{av:'AV19ContagemResultado_SerGrupoVinc',fld:'vCONTAGEMRESULTADO_SERGRUPOVINC',pic:'ZZZZZ9',nv:0},{av:'AV20ContagemResultado_CodSrvVnc',fld:'vCONTAGEMRESULTADO_CODSRVVNC',pic:'ZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV36DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV48ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'AV24ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV23ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV50ContagemResultado_DataEntregaReal_To1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV49ContagemResultado_DataEntregaReal1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL1',pic:'99/99/99 99:99',nv:''},{av:'AV51ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'AV52ContagemResultado_StatusCnt1',fld:'vCONTAGEMRESULTADO_STATUSCNT1',pic:'Z9',nv:0},{av:'AV53ContagemResultado_Descricao1',fld:'vCONTAGEMRESULTADO_DESCRICAO1',pic:'',nv:''},{av:'AV54ContagemResultado_SistemaCod1',fld:'vCONTAGEMRESULTADO_SISTEMACOD1',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV55ContagemResultado_Agrupador1',fld:'vCONTAGEMRESULTADO_AGRUPADOR1',pic:'@!',nv:''},{av:'AV35DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV56ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV29ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV28ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV58ContagemResultado_DataEntregaReal_To2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV57ContagemResultado_DataEntregaReal2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL2',pic:'99/99/99 99:99',nv:''},{av:'AV59ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV60ContagemResultado_StatusCnt2',fld:'vCONTAGEMRESULTADO_STATUSCNT2',pic:'Z9',nv:0},{av:'AV61ContagemResultado_Descricao2',fld:'vCONTAGEMRESULTADO_DESCRICAO2',pic:'',nv:''},{av:'AV62ContagemResultado_SistemaCod2',fld:'vCONTAGEMRESULTADO_SISTEMACOD2',pic:'ZZZZZ9',nv:0},{av:'AV27DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV63ContagemResultado_Agrupador2',fld:'vCONTAGEMRESULTADO_AGRUPADOR2',pic:'@!',nv:''},{av:'AV64ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'AV34ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'AV33ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV66ContagemResultado_DataEntregaReal_To3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV65ContagemResultado_DataEntregaReal3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL3',pic:'99/99/99 99:99',nv:''},{av:'AV67ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'AV68ContagemResultado_StatusCnt3',fld:'vCONTAGEMRESULTADO_STATUSCNT3',pic:'Z9',nv:0},{av:'AV69ContagemResultado_Descricao3',fld:'vCONTAGEMRESULTADO_DESCRICAO3',pic:'',nv:''},{av:'AV70ContagemResultado_SistemaCod3',fld:'vCONTAGEMRESULTADO_SISTEMACOD3',pic:'ZZZZZ9',nv:0},{av:'AV32DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV71ContagemResultado_Agrupador3',fld:'vCONTAGEMRESULTADO_AGRUPADOR3',pic:'@!',nv:''},{av:'A457ContagemResultado_Demanda',fld:'CONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A764ContagemResultado_ServicoGrupo',fld:'CONTAGEMRESULTADO_SERVICOGRUPO',pic:'ZZZZZ9',nv:0},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'A2017ContagemResultado_DataEntregaReal',fld:'CONTAGEMRESULTADO_DATAENTREGAREAL',pic:'99/99/99 99:99',nv:''},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'A494ContagemResultado_Descricao',fld:'CONTAGEMRESULTADO_DESCRICAO',pic:'',nv:''},{av:'A489ContagemResultado_SistemaCod',fld:'CONTAGEMRESULTADO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A1046ContagemResultado_Agrupador',fld:'CONTAGEMRESULTADO_AGRUPADOR',pic:'@!',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A509ContagemrResultado_SistemaSigla',fld:'CONTAGEMRRESULTADO_SISTEMASIGLA',pic:'@!',nv:''},{av:'A1326ContagemResultado_ContratadaTipoFab',fld:'CONTAGEMRESULTADO_CONTRATADATIPOFAB',pic:'',nv:''},{av:'A682ContagemResultado_PFBFMUltima',fld:'CONTAGEMRESULTADO_PFBFMULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A684ContagemResultado_PFBFSUltima',fld:'CONTAGEMRESULTADO_PFBFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV14ContagemResultado_OSVinculada',fld:'vCONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV25DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E12TR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV21DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV25DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV192Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV16ContagemResultado_ServicoGrupo',fld:'vCONTAGEMRESULTADO_SERVICOGRUPO',pic:'ZZZZZ9',nv:0},{av:'AV17ContagemResultado_Servico',fld:'vCONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV18ContagemResultado_CntadaOsVinc',fld:'vCONTAGEMRESULTADO_CNTADAOSVINC',pic:'ZZZZZ9',nv:0},{av:'AV19ContagemResultado_SerGrupoVinc',fld:'vCONTAGEMRESULTADO_SERGRUPOVINC',pic:'ZZZZZ9',nv:0},{av:'AV20ContagemResultado_CodSrvVnc',fld:'vCONTAGEMRESULTADO_CODSRVVNC',pic:'ZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV36DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV48ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'AV24ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV23ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV50ContagemResultado_DataEntregaReal_To1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV49ContagemResultado_DataEntregaReal1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL1',pic:'99/99/99 99:99',nv:''},{av:'AV51ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'AV52ContagemResultado_StatusCnt1',fld:'vCONTAGEMRESULTADO_STATUSCNT1',pic:'Z9',nv:0},{av:'AV53ContagemResultado_Descricao1',fld:'vCONTAGEMRESULTADO_DESCRICAO1',pic:'',nv:''},{av:'AV54ContagemResultado_SistemaCod1',fld:'vCONTAGEMRESULTADO_SISTEMACOD1',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV55ContagemResultado_Agrupador1',fld:'vCONTAGEMRESULTADO_AGRUPADOR1',pic:'@!',nv:''},{av:'AV35DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV56ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV29ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV28ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV58ContagemResultado_DataEntregaReal_To2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV57ContagemResultado_DataEntregaReal2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL2',pic:'99/99/99 99:99',nv:''},{av:'AV59ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV60ContagemResultado_StatusCnt2',fld:'vCONTAGEMRESULTADO_STATUSCNT2',pic:'Z9',nv:0},{av:'AV61ContagemResultado_Descricao2',fld:'vCONTAGEMRESULTADO_DESCRICAO2',pic:'',nv:''},{av:'AV62ContagemResultado_SistemaCod2',fld:'vCONTAGEMRESULTADO_SISTEMACOD2',pic:'ZZZZZ9',nv:0},{av:'AV27DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV63ContagemResultado_Agrupador2',fld:'vCONTAGEMRESULTADO_AGRUPADOR2',pic:'@!',nv:''},{av:'AV64ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'AV34ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'AV33ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV66ContagemResultado_DataEntregaReal_To3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV65ContagemResultado_DataEntregaReal3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL3',pic:'99/99/99 99:99',nv:''},{av:'AV67ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'AV68ContagemResultado_StatusCnt3',fld:'vCONTAGEMRESULTADO_STATUSCNT3',pic:'Z9',nv:0},{av:'AV69ContagemResultado_Descricao3',fld:'vCONTAGEMRESULTADO_DESCRICAO3',pic:'',nv:''},{av:'AV70ContagemResultado_SistemaCod3',fld:'vCONTAGEMRESULTADO_SISTEMACOD3',pic:'ZZZZZ9',nv:0},{av:'AV32DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV71ContagemResultado_Agrupador3',fld:'vCONTAGEMRESULTADO_AGRUPADOR3',pic:'@!',nv:''},{av:'A457ContagemResultado_Demanda',fld:'CONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A764ContagemResultado_ServicoGrupo',fld:'CONTAGEMRESULTADO_SERVICOGRUPO',pic:'ZZZZZ9',nv:0},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'A2017ContagemResultado_DataEntregaReal',fld:'CONTAGEMRESULTADO_DATAENTREGAREAL',pic:'99/99/99 99:99',nv:''},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'A494ContagemResultado_Descricao',fld:'CONTAGEMRESULTADO_DESCRICAO',pic:'',nv:''},{av:'A489ContagemResultado_SistemaCod',fld:'CONTAGEMRESULTADO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A1046ContagemResultado_Agrupador',fld:'CONTAGEMRESULTADO_AGRUPADOR',pic:'@!',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A509ContagemrResultado_SistemaSigla',fld:'CONTAGEMRRESULTADO_SISTEMASIGLA',pic:'@!',nv:''},{av:'A1326ContagemResultado_ContratadaTipoFab',fld:'CONTAGEMRESULTADO_CONTRATADATIPOFAB',pic:'',nv:''},{av:'A682ContagemResultado_PFBFMUltima',fld:'CONTAGEMRESULTADO_PFBFMULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A684ContagemResultado_PFBFSUltima',fld:'CONTAGEMRESULTADO_PFBFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV14ContagemResultado_OSVinculada',fld:'vCONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'Gx_date',fld:'vTODAY',pic:'',nv:''}],oparms:[{av:'AV35DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV36DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV25DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV27DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV56ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV64ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV21DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV22DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV55ContagemResultado_Agrupador1',fld:'vCONTAGEMRESULTADO_AGRUPADOR1',pic:'@!',nv:''},{av:'AV54ContagemResultado_SistemaCod1',fld:'vCONTAGEMRESULTADO_SISTEMACOD1',pic:'ZZZZZ9',nv:0},{av:'AV53ContagemResultado_Descricao1',fld:'vCONTAGEMRESULTADO_DESCRICAO1',pic:'',nv:''},{av:'AV52ContagemResultado_StatusCnt1',fld:'vCONTAGEMRESULTADO_STATUSCNT1',pic:'Z9',nv:0},{av:'AV51ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'AV49ContagemResultado_DataEntregaReal1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL1',pic:'99/99/99 99:99',nv:''},{av:'AV50ContagemResultado_DataEntregaReal_To1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV23ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV24ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV48ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV63ContagemResultado_Agrupador2',fld:'vCONTAGEMRESULTADO_AGRUPADOR2',pic:'@!',nv:''},{av:'AV62ContagemResultado_SistemaCod2',fld:'vCONTAGEMRESULTADO_SISTEMACOD2',pic:'ZZZZZ9',nv:0},{av:'AV61ContagemResultado_Descricao2',fld:'vCONTAGEMRESULTADO_DESCRICAO2',pic:'',nv:''},{av:'AV60ContagemResultado_StatusCnt2',fld:'vCONTAGEMRESULTADO_STATUSCNT2',pic:'Z9',nv:0},{av:'AV59ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV57ContagemResultado_DataEntregaReal2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL2',pic:'99/99/99 99:99',nv:''},{av:'AV58ContagemResultado_DataEntregaReal_To2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV28ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV29ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV71ContagemResultado_Agrupador3',fld:'vCONTAGEMRESULTADO_AGRUPADOR3',pic:'@!',nv:''},{av:'AV70ContagemResultado_SistemaCod3',fld:'vCONTAGEMRESULTADO_SISTEMACOD3',pic:'ZZZZZ9',nv:0},{av:'AV69ContagemResultado_Descricao3',fld:'vCONTAGEMRESULTADO_DESCRICAO3',pic:'',nv:''},{av:'AV68ContagemResultado_StatusCnt3',fld:'vCONTAGEMRESULTADO_STATUSCNT3',pic:'Z9',nv:0},{av:'AV67ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'AV65ContagemResultado_DataEntregaReal3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL3',pic:'99/99/99 99:99',nv:''},{av:'AV66ContagemResultado_DataEntregaReal_To3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV34ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'edtavContagemresultado_demandafm2_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDAFM2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datadmn2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_dataentregareal2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAENTREGAREAL2',prop:'Visible'},{av:'cmbavContagemresultado_statusdmn2'},{av:'cmbavContagemresultado_statuscnt2'},{av:'edtavContagemresultado_descricao2_Visible',ctrl:'vCONTAGEMRESULTADO_DESCRICAO2',prop:'Visible'},{av:'dynavContagemresultado_sistemacod2'},{av:'edtavContagemresultado_agrupador2_Visible',ctrl:'vCONTAGEMRESULTADO_AGRUPADOR2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContagemresultado_demandafm3_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDAFM3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datadmn3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_dataentregareal3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAENTREGAREAL3',prop:'Visible'},{av:'cmbavContagemresultado_statusdmn3'},{av:'cmbavContagemresultado_statuscnt3'},{av:'edtavContagemresultado_descricao3_Visible',ctrl:'vCONTAGEMRESULTADO_DESCRICAO3',prop:'Visible'},{av:'dynavContagemresultado_sistemacod3'},{av:'edtavContagemresultado_agrupador3_Visible',ctrl:'vCONTAGEMRESULTADO_AGRUPADOR3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContagemresultado_demandafm1_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDAFM1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datadmn1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_dataentregareal1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAENTREGAREAL1',prop:'Visible'},{av:'cmbavContagemresultado_statusdmn1'},{av:'cmbavContagemresultado_statuscnt1'},{av:'edtavContagemresultado_descricao1_Visible',ctrl:'vCONTAGEMRESULTADO_DESCRICAO1',prop:'Visible'},{av:'dynavContagemresultado_sistemacod1'},{av:'edtavContagemresultado_agrupador1_Visible',ctrl:'vCONTAGEMRESULTADO_AGRUPADOR1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'cellContagemresultado_dataentregareal_cell2_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_CELL2',prop:'Class'},{av:'cellContagemresultado_dataentregareal_to_cell2_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_TO_CELL2',prop:'Class'},{av:'lblContagemresultado_dataentregareal_rangemiddletext_12_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_RANGEMIDDLETEXT_12',prop:'Class'},{av:'cellContagemresultado_dataentregareal_cell3_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_CELL3',prop:'Class'},{av:'cellContagemresultado_dataentregareal_to_cell3_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_TO_CELL3',prop:'Class'},{av:'lblContagemresultado_dataentregareal_rangemiddletext_13_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_RANGEMIDDLETEXT_13',prop:'Class'},{av:'cellContagemresultado_dataentregareal_cell1_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_CELL1',prop:'Class'},{av:'cellContagemresultado_dataentregareal_to_cell1_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_TO_CELL1',prop:'Class'},{av:'lblContagemresultado_dataentregareal_rangemiddletext_11_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_RANGEMIDDLETEXT_11',prop:'Class'}]}");
         setEventMetadata("'DOEXPORT'","{handler:'E13TR2',iparms:[{av:'AV98ExcelFilename',fld:'vEXCELFILENAME',pic:'',nv:''},{av:'AV99ErrorMessage',fld:'vERRORMESSAGE',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV36DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV21DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV48ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'AV24ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV23ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV50ContagemResultado_DataEntregaReal_To1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV49ContagemResultado_DataEntregaReal1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL1',pic:'99/99/99 99:99',nv:''},{av:'AV51ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'AV52ContagemResultado_StatusCnt1',fld:'vCONTAGEMRESULTADO_STATUSCNT1',pic:'Z9',nv:0},{av:'AV53ContagemResultado_Descricao1',fld:'vCONTAGEMRESULTADO_DESCRICAO1',pic:'',nv:''},{av:'AV54ContagemResultado_SistemaCod1',fld:'vCONTAGEMRESULTADO_SISTEMACOD1',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV55ContagemResultado_Agrupador1',fld:'vCONTAGEMRESULTADO_AGRUPADOR1',pic:'@!',nv:''},{av:'AV35DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV56ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV29ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV28ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV58ContagemResultado_DataEntregaReal_To2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV57ContagemResultado_DataEntregaReal2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL2',pic:'99/99/99 99:99',nv:''},{av:'AV59ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV60ContagemResultado_StatusCnt2',fld:'vCONTAGEMRESULTADO_STATUSCNT2',pic:'Z9',nv:0},{av:'AV61ContagemResultado_Descricao2',fld:'vCONTAGEMRESULTADO_DESCRICAO2',pic:'',nv:''},{av:'AV62ContagemResultado_SistemaCod2',fld:'vCONTAGEMRESULTADO_SISTEMACOD2',pic:'ZZZZZ9',nv:0},{av:'AV27DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV63ContagemResultado_Agrupador2',fld:'vCONTAGEMRESULTADO_AGRUPADOR2',pic:'@!',nv:''},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV64ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'AV34ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'AV33ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV66ContagemResultado_DataEntregaReal_To3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV65ContagemResultado_DataEntregaReal3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL3',pic:'99/99/99 99:99',nv:''},{av:'AV67ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'AV68ContagemResultado_StatusCnt3',fld:'vCONTAGEMRESULTADO_STATUSCNT3',pic:'Z9',nv:0},{av:'AV69ContagemResultado_Descricao3',fld:'vCONTAGEMRESULTADO_DESCRICAO3',pic:'',nv:''},{av:'AV70ContagemResultado_SistemaCod3',fld:'vCONTAGEMRESULTADO_SISTEMACOD3',pic:'ZZZZZ9',nv:0},{av:'AV32DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV71ContagemResultado_Agrupador3',fld:'vCONTAGEMRESULTADO_AGRUPADOR3',pic:'@!',nv:''}],oparms:[{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E17TR2',iparms:[{av:'AV21DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV22DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'Gx_date',fld:'vTODAY',pic:'',nv:''}],oparms:[{av:'AV22DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'edtavContagemresultado_demandafm1_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDAFM1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datadmn1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_dataentregareal1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAENTREGAREAL1',prop:'Visible'},{av:'cmbavContagemresultado_statusdmn1'},{av:'cmbavContagemresultado_statuscnt1'},{av:'edtavContagemresultado_descricao1_Visible',ctrl:'vCONTAGEMRESULTADO_DESCRICAO1',prop:'Visible'},{av:'dynavContagemresultado_sistemacod1'},{av:'edtavContagemresultado_agrupador1_Visible',ctrl:'vCONTAGEMRESULTADO_AGRUPADOR1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'cellContagemresultado_dataentregareal_cell1_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_CELL1',prop:'Class'},{av:'cellContagemresultado_dataentregareal_to_cell1_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_TO_CELL1',prop:'Class'},{av:'lblContagemresultado_dataentregareal_rangemiddletext_11_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_RANGEMIDDLETEXT_11',prop:'Class'},{av:'AV49ContagemResultado_DataEntregaReal1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL1',pic:'99/99/99 99:99',nv:''},{av:'AV50ContagemResultado_DataEntregaReal_To1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO1',pic:'99/99/99 99:99',nv:''}]}");
         setEventMetadata("VDYNAMICFILTERSOPERATOR1.CLICK","{handler:'E18TR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV21DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV25DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV192Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV16ContagemResultado_ServicoGrupo',fld:'vCONTAGEMRESULTADO_SERVICOGRUPO',pic:'ZZZZZ9',nv:0},{av:'AV17ContagemResultado_Servico',fld:'vCONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV18ContagemResultado_CntadaOsVinc',fld:'vCONTAGEMRESULTADO_CNTADAOSVINC',pic:'ZZZZZ9',nv:0},{av:'AV19ContagemResultado_SerGrupoVinc',fld:'vCONTAGEMRESULTADO_SERGRUPOVINC',pic:'ZZZZZ9',nv:0},{av:'AV20ContagemResultado_CodSrvVnc',fld:'vCONTAGEMRESULTADO_CODSRVVNC',pic:'ZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV36DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV48ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'AV24ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV23ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV50ContagemResultado_DataEntregaReal_To1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV49ContagemResultado_DataEntregaReal1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL1',pic:'99/99/99 99:99',nv:''},{av:'AV51ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'AV52ContagemResultado_StatusCnt1',fld:'vCONTAGEMRESULTADO_STATUSCNT1',pic:'Z9',nv:0},{av:'AV53ContagemResultado_Descricao1',fld:'vCONTAGEMRESULTADO_DESCRICAO1',pic:'',nv:''},{av:'AV54ContagemResultado_SistemaCod1',fld:'vCONTAGEMRESULTADO_SISTEMACOD1',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV55ContagemResultado_Agrupador1',fld:'vCONTAGEMRESULTADO_AGRUPADOR1',pic:'@!',nv:''},{av:'AV35DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV56ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV29ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV28ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV58ContagemResultado_DataEntregaReal_To2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV57ContagemResultado_DataEntregaReal2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL2',pic:'99/99/99 99:99',nv:''},{av:'AV59ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV60ContagemResultado_StatusCnt2',fld:'vCONTAGEMRESULTADO_STATUSCNT2',pic:'Z9',nv:0},{av:'AV61ContagemResultado_Descricao2',fld:'vCONTAGEMRESULTADO_DESCRICAO2',pic:'',nv:''},{av:'AV62ContagemResultado_SistemaCod2',fld:'vCONTAGEMRESULTADO_SISTEMACOD2',pic:'ZZZZZ9',nv:0},{av:'AV27DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV63ContagemResultado_Agrupador2',fld:'vCONTAGEMRESULTADO_AGRUPADOR2',pic:'@!',nv:''},{av:'AV64ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'AV34ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'AV33ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV66ContagemResultado_DataEntregaReal_To3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV65ContagemResultado_DataEntregaReal3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL3',pic:'99/99/99 99:99',nv:''},{av:'AV67ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'AV68ContagemResultado_StatusCnt3',fld:'vCONTAGEMRESULTADO_STATUSCNT3',pic:'Z9',nv:0},{av:'AV69ContagemResultado_Descricao3',fld:'vCONTAGEMRESULTADO_DESCRICAO3',pic:'',nv:''},{av:'AV70ContagemResultado_SistemaCod3',fld:'vCONTAGEMRESULTADO_SISTEMACOD3',pic:'ZZZZZ9',nv:0},{av:'AV32DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV71ContagemResultado_Agrupador3',fld:'vCONTAGEMRESULTADO_AGRUPADOR3',pic:'@!',nv:''},{av:'A457ContagemResultado_Demanda',fld:'CONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A764ContagemResultado_ServicoGrupo',fld:'CONTAGEMRESULTADO_SERVICOGRUPO',pic:'ZZZZZ9',nv:0},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'A2017ContagemResultado_DataEntregaReal',fld:'CONTAGEMRESULTADO_DATAENTREGAREAL',pic:'99/99/99 99:99',nv:''},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'A494ContagemResultado_Descricao',fld:'CONTAGEMRESULTADO_DESCRICAO',pic:'',nv:''},{av:'A489ContagemResultado_SistemaCod',fld:'CONTAGEMRESULTADO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A1046ContagemResultado_Agrupador',fld:'CONTAGEMRESULTADO_AGRUPADOR',pic:'@!',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A509ContagemrResultado_SistemaSigla',fld:'CONTAGEMRRESULTADO_SISTEMASIGLA',pic:'@!',nv:''},{av:'A1326ContagemResultado_ContratadaTipoFab',fld:'CONTAGEMRESULTADO_CONTRATADATIPOFAB',pic:'',nv:''},{av:'A682ContagemResultado_PFBFMUltima',fld:'CONTAGEMRESULTADO_PFBFMULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A684ContagemResultado_PFBFSUltima',fld:'CONTAGEMRESULTADO_PFBFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV14ContagemResultado_OSVinculada',fld:'vCONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'Gx_date',fld:'vTODAY',pic:'',nv:''}],oparms:[{av:'AV49ContagemResultado_DataEntregaReal1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL1',pic:'99/99/99 99:99',nv:''},{av:'AV50ContagemResultado_DataEntregaReal_To1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO1',pic:'99/99/99 99:99',nv:''},{av:'cellContagemresultado_dataentregareal_cell1_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_CELL1',prop:'Class'},{av:'cellContagemresultado_dataentregareal_to_cell1_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_TO_CELL1',prop:'Class'},{av:'lblContagemresultado_dataentregareal_rangemiddletext_11_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_RANGEMIDDLETEXT_11',prop:'Class'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E19TR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV21DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV25DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV192Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV16ContagemResultado_ServicoGrupo',fld:'vCONTAGEMRESULTADO_SERVICOGRUPO',pic:'ZZZZZ9',nv:0},{av:'AV17ContagemResultado_Servico',fld:'vCONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV18ContagemResultado_CntadaOsVinc',fld:'vCONTAGEMRESULTADO_CNTADAOSVINC',pic:'ZZZZZ9',nv:0},{av:'AV19ContagemResultado_SerGrupoVinc',fld:'vCONTAGEMRESULTADO_SERGRUPOVINC',pic:'ZZZZZ9',nv:0},{av:'AV20ContagemResultado_CodSrvVnc',fld:'vCONTAGEMRESULTADO_CODSRVVNC',pic:'ZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV36DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV48ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'AV24ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV23ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV50ContagemResultado_DataEntregaReal_To1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV49ContagemResultado_DataEntregaReal1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL1',pic:'99/99/99 99:99',nv:''},{av:'AV51ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'AV52ContagemResultado_StatusCnt1',fld:'vCONTAGEMRESULTADO_STATUSCNT1',pic:'Z9',nv:0},{av:'AV53ContagemResultado_Descricao1',fld:'vCONTAGEMRESULTADO_DESCRICAO1',pic:'',nv:''},{av:'AV54ContagemResultado_SistemaCod1',fld:'vCONTAGEMRESULTADO_SISTEMACOD1',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV55ContagemResultado_Agrupador1',fld:'vCONTAGEMRESULTADO_AGRUPADOR1',pic:'@!',nv:''},{av:'AV35DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV56ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV29ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV28ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV58ContagemResultado_DataEntregaReal_To2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV57ContagemResultado_DataEntregaReal2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL2',pic:'99/99/99 99:99',nv:''},{av:'AV59ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV60ContagemResultado_StatusCnt2',fld:'vCONTAGEMRESULTADO_STATUSCNT2',pic:'Z9',nv:0},{av:'AV61ContagemResultado_Descricao2',fld:'vCONTAGEMRESULTADO_DESCRICAO2',pic:'',nv:''},{av:'AV62ContagemResultado_SistemaCod2',fld:'vCONTAGEMRESULTADO_SISTEMACOD2',pic:'ZZZZZ9',nv:0},{av:'AV27DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV63ContagemResultado_Agrupador2',fld:'vCONTAGEMRESULTADO_AGRUPADOR2',pic:'@!',nv:''},{av:'AV64ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'AV34ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'AV33ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV66ContagemResultado_DataEntregaReal_To3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV65ContagemResultado_DataEntregaReal3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL3',pic:'99/99/99 99:99',nv:''},{av:'AV67ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'AV68ContagemResultado_StatusCnt3',fld:'vCONTAGEMRESULTADO_STATUSCNT3',pic:'Z9',nv:0},{av:'AV69ContagemResultado_Descricao3',fld:'vCONTAGEMRESULTADO_DESCRICAO3',pic:'',nv:''},{av:'AV70ContagemResultado_SistemaCod3',fld:'vCONTAGEMRESULTADO_SISTEMACOD3',pic:'ZZZZZ9',nv:0},{av:'AV32DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV71ContagemResultado_Agrupador3',fld:'vCONTAGEMRESULTADO_AGRUPADOR3',pic:'@!',nv:''},{av:'A457ContagemResultado_Demanda',fld:'CONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A764ContagemResultado_ServicoGrupo',fld:'CONTAGEMRESULTADO_SERVICOGRUPO',pic:'ZZZZZ9',nv:0},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'A2017ContagemResultado_DataEntregaReal',fld:'CONTAGEMRESULTADO_DATAENTREGAREAL',pic:'99/99/99 99:99',nv:''},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'A494ContagemResultado_Descricao',fld:'CONTAGEMRESULTADO_DESCRICAO',pic:'',nv:''},{av:'A489ContagemResultado_SistemaCod',fld:'CONTAGEMRESULTADO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A1046ContagemResultado_Agrupador',fld:'CONTAGEMRESULTADO_AGRUPADOR',pic:'@!',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A509ContagemrResultado_SistemaSigla',fld:'CONTAGEMRRESULTADO_SISTEMASIGLA',pic:'@!',nv:''},{av:'A1326ContagemResultado_ContratadaTipoFab',fld:'CONTAGEMRESULTADO_CONTRATADATIPOFAB',pic:'',nv:''},{av:'A682ContagemResultado_PFBFMUltima',fld:'CONTAGEMRESULTADO_PFBFMULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A684ContagemResultado_PFBFSUltima',fld:'CONTAGEMRESULTADO_PFBFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV14ContagemResultado_OSVinculada',fld:'vCONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E14TR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV21DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV25DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV192Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV16ContagemResultado_ServicoGrupo',fld:'vCONTAGEMRESULTADO_SERVICOGRUPO',pic:'ZZZZZ9',nv:0},{av:'AV17ContagemResultado_Servico',fld:'vCONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV18ContagemResultado_CntadaOsVinc',fld:'vCONTAGEMRESULTADO_CNTADAOSVINC',pic:'ZZZZZ9',nv:0},{av:'AV19ContagemResultado_SerGrupoVinc',fld:'vCONTAGEMRESULTADO_SERGRUPOVINC',pic:'ZZZZZ9',nv:0},{av:'AV20ContagemResultado_CodSrvVnc',fld:'vCONTAGEMRESULTADO_CODSRVVNC',pic:'ZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV36DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV48ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'AV24ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV23ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV50ContagemResultado_DataEntregaReal_To1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV49ContagemResultado_DataEntregaReal1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL1',pic:'99/99/99 99:99',nv:''},{av:'AV51ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'AV52ContagemResultado_StatusCnt1',fld:'vCONTAGEMRESULTADO_STATUSCNT1',pic:'Z9',nv:0},{av:'AV53ContagemResultado_Descricao1',fld:'vCONTAGEMRESULTADO_DESCRICAO1',pic:'',nv:''},{av:'AV54ContagemResultado_SistemaCod1',fld:'vCONTAGEMRESULTADO_SISTEMACOD1',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV55ContagemResultado_Agrupador1',fld:'vCONTAGEMRESULTADO_AGRUPADOR1',pic:'@!',nv:''},{av:'AV35DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV56ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV29ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV28ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV58ContagemResultado_DataEntregaReal_To2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV57ContagemResultado_DataEntregaReal2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL2',pic:'99/99/99 99:99',nv:''},{av:'AV59ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV60ContagemResultado_StatusCnt2',fld:'vCONTAGEMRESULTADO_STATUSCNT2',pic:'Z9',nv:0},{av:'AV61ContagemResultado_Descricao2',fld:'vCONTAGEMRESULTADO_DESCRICAO2',pic:'',nv:''},{av:'AV62ContagemResultado_SistemaCod2',fld:'vCONTAGEMRESULTADO_SISTEMACOD2',pic:'ZZZZZ9',nv:0},{av:'AV27DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV63ContagemResultado_Agrupador2',fld:'vCONTAGEMRESULTADO_AGRUPADOR2',pic:'@!',nv:''},{av:'AV64ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'AV34ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'AV33ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV66ContagemResultado_DataEntregaReal_To3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV65ContagemResultado_DataEntregaReal3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL3',pic:'99/99/99 99:99',nv:''},{av:'AV67ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'AV68ContagemResultado_StatusCnt3',fld:'vCONTAGEMRESULTADO_STATUSCNT3',pic:'Z9',nv:0},{av:'AV69ContagemResultado_Descricao3',fld:'vCONTAGEMRESULTADO_DESCRICAO3',pic:'',nv:''},{av:'AV70ContagemResultado_SistemaCod3',fld:'vCONTAGEMRESULTADO_SISTEMACOD3',pic:'ZZZZZ9',nv:0},{av:'AV32DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV71ContagemResultado_Agrupador3',fld:'vCONTAGEMRESULTADO_AGRUPADOR3',pic:'@!',nv:''},{av:'A457ContagemResultado_Demanda',fld:'CONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A764ContagemResultado_ServicoGrupo',fld:'CONTAGEMRESULTADO_SERVICOGRUPO',pic:'ZZZZZ9',nv:0},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'A2017ContagemResultado_DataEntregaReal',fld:'CONTAGEMRESULTADO_DATAENTREGAREAL',pic:'99/99/99 99:99',nv:''},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'A494ContagemResultado_Descricao',fld:'CONTAGEMRESULTADO_DESCRICAO',pic:'',nv:''},{av:'A489ContagemResultado_SistemaCod',fld:'CONTAGEMRESULTADO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A1046ContagemResultado_Agrupador',fld:'CONTAGEMRESULTADO_AGRUPADOR',pic:'@!',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A509ContagemrResultado_SistemaSigla',fld:'CONTAGEMRRESULTADO_SISTEMASIGLA',pic:'@!',nv:''},{av:'A1326ContagemResultado_ContratadaTipoFab',fld:'CONTAGEMRESULTADO_CONTRATADATIPOFAB',pic:'',nv:''},{av:'A682ContagemResultado_PFBFMUltima',fld:'CONTAGEMRESULTADO_PFBFMULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A684ContagemResultado_PFBFSUltima',fld:'CONTAGEMRESULTADO_PFBFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV14ContagemResultado_OSVinculada',fld:'vCONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'Gx_date',fld:'vTODAY',pic:'',nv:''}],oparms:[{av:'AV35DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV27DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV56ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV64ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV21DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV22DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV55ContagemResultado_Agrupador1',fld:'vCONTAGEMRESULTADO_AGRUPADOR1',pic:'@!',nv:''},{av:'AV54ContagemResultado_SistemaCod1',fld:'vCONTAGEMRESULTADO_SISTEMACOD1',pic:'ZZZZZ9',nv:0},{av:'AV53ContagemResultado_Descricao1',fld:'vCONTAGEMRESULTADO_DESCRICAO1',pic:'',nv:''},{av:'AV52ContagemResultado_StatusCnt1',fld:'vCONTAGEMRESULTADO_STATUSCNT1',pic:'Z9',nv:0},{av:'AV51ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'AV49ContagemResultado_DataEntregaReal1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL1',pic:'99/99/99 99:99',nv:''},{av:'AV50ContagemResultado_DataEntregaReal_To1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV23ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV24ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV48ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV63ContagemResultado_Agrupador2',fld:'vCONTAGEMRESULTADO_AGRUPADOR2',pic:'@!',nv:''},{av:'AV62ContagemResultado_SistemaCod2',fld:'vCONTAGEMRESULTADO_SISTEMACOD2',pic:'ZZZZZ9',nv:0},{av:'AV61ContagemResultado_Descricao2',fld:'vCONTAGEMRESULTADO_DESCRICAO2',pic:'',nv:''},{av:'AV60ContagemResultado_StatusCnt2',fld:'vCONTAGEMRESULTADO_STATUSCNT2',pic:'Z9',nv:0},{av:'AV59ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV57ContagemResultado_DataEntregaReal2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL2',pic:'99/99/99 99:99',nv:''},{av:'AV58ContagemResultado_DataEntregaReal_To2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV28ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV29ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV71ContagemResultado_Agrupador3',fld:'vCONTAGEMRESULTADO_AGRUPADOR3',pic:'@!',nv:''},{av:'AV70ContagemResultado_SistemaCod3',fld:'vCONTAGEMRESULTADO_SISTEMACOD3',pic:'ZZZZZ9',nv:0},{av:'AV69ContagemResultado_Descricao3',fld:'vCONTAGEMRESULTADO_DESCRICAO3',pic:'',nv:''},{av:'AV68ContagemResultado_StatusCnt3',fld:'vCONTAGEMRESULTADO_STATUSCNT3',pic:'Z9',nv:0},{av:'AV67ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'AV65ContagemResultado_DataEntregaReal3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL3',pic:'99/99/99 99:99',nv:''},{av:'AV66ContagemResultado_DataEntregaReal_To3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV34ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'edtavContagemresultado_demandafm2_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDAFM2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datadmn2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_dataentregareal2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAENTREGAREAL2',prop:'Visible'},{av:'cmbavContagemresultado_statusdmn2'},{av:'cmbavContagemresultado_statuscnt2'},{av:'edtavContagemresultado_descricao2_Visible',ctrl:'vCONTAGEMRESULTADO_DESCRICAO2',prop:'Visible'},{av:'dynavContagemresultado_sistemacod2'},{av:'edtavContagemresultado_agrupador2_Visible',ctrl:'vCONTAGEMRESULTADO_AGRUPADOR2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContagemresultado_demandafm3_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDAFM3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datadmn3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_dataentregareal3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAENTREGAREAL3',prop:'Visible'},{av:'cmbavContagemresultado_statusdmn3'},{av:'cmbavContagemresultado_statuscnt3'},{av:'edtavContagemresultado_descricao3_Visible',ctrl:'vCONTAGEMRESULTADO_DESCRICAO3',prop:'Visible'},{av:'dynavContagemresultado_sistemacod3'},{av:'edtavContagemresultado_agrupador3_Visible',ctrl:'vCONTAGEMRESULTADO_AGRUPADOR3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContagemresultado_demandafm1_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDAFM1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datadmn1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_dataentregareal1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAENTREGAREAL1',prop:'Visible'},{av:'cmbavContagemresultado_statusdmn1'},{av:'cmbavContagemresultado_statuscnt1'},{av:'edtavContagemresultado_descricao1_Visible',ctrl:'vCONTAGEMRESULTADO_DESCRICAO1',prop:'Visible'},{av:'dynavContagemresultado_sistemacod1'},{av:'edtavContagemresultado_agrupador1_Visible',ctrl:'vCONTAGEMRESULTADO_AGRUPADOR1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'cellContagemresultado_dataentregareal_cell2_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_CELL2',prop:'Class'},{av:'cellContagemresultado_dataentregareal_to_cell2_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_TO_CELL2',prop:'Class'},{av:'lblContagemresultado_dataentregareal_rangemiddletext_12_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_RANGEMIDDLETEXT_12',prop:'Class'},{av:'cellContagemresultado_dataentregareal_cell3_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_CELL3',prop:'Class'},{av:'cellContagemresultado_dataentregareal_to_cell3_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_TO_CELL3',prop:'Class'},{av:'lblContagemresultado_dataentregareal_rangemiddletext_13_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_RANGEMIDDLETEXT_13',prop:'Class'},{av:'cellContagemresultado_dataentregareal_cell1_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_CELL1',prop:'Class'},{av:'cellContagemresultado_dataentregareal_to_cell1_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_TO_CELL1',prop:'Class'},{av:'lblContagemresultado_dataentregareal_rangemiddletext_11_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_RANGEMIDDLETEXT_11',prop:'Class'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E20TR2',iparms:[{av:'AV26DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV27DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'Gx_date',fld:'vTODAY',pic:'',nv:''}],oparms:[{av:'AV27DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'edtavContagemresultado_demandafm2_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDAFM2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datadmn2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_dataentregareal2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAENTREGAREAL2',prop:'Visible'},{av:'cmbavContagemresultado_statusdmn2'},{av:'cmbavContagemresultado_statuscnt2'},{av:'edtavContagemresultado_descricao2_Visible',ctrl:'vCONTAGEMRESULTADO_DESCRICAO2',prop:'Visible'},{av:'dynavContagemresultado_sistemacod2'},{av:'edtavContagemresultado_agrupador2_Visible',ctrl:'vCONTAGEMRESULTADO_AGRUPADOR2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'cellContagemresultado_dataentregareal_cell2_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_CELL2',prop:'Class'},{av:'cellContagemresultado_dataentregareal_to_cell2_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_TO_CELL2',prop:'Class'},{av:'lblContagemresultado_dataentregareal_rangemiddletext_12_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_RANGEMIDDLETEXT_12',prop:'Class'},{av:'AV57ContagemResultado_DataEntregaReal2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL2',pic:'99/99/99 99:99',nv:''},{av:'AV58ContagemResultado_DataEntregaReal_To2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO2',pic:'99/99/99 99:99',nv:''}]}");
         setEventMetadata("VDYNAMICFILTERSOPERATOR2.CLICK","{handler:'E21TR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV21DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV25DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV192Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV16ContagemResultado_ServicoGrupo',fld:'vCONTAGEMRESULTADO_SERVICOGRUPO',pic:'ZZZZZ9',nv:0},{av:'AV17ContagemResultado_Servico',fld:'vCONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV18ContagemResultado_CntadaOsVinc',fld:'vCONTAGEMRESULTADO_CNTADAOSVINC',pic:'ZZZZZ9',nv:0},{av:'AV19ContagemResultado_SerGrupoVinc',fld:'vCONTAGEMRESULTADO_SERGRUPOVINC',pic:'ZZZZZ9',nv:0},{av:'AV20ContagemResultado_CodSrvVnc',fld:'vCONTAGEMRESULTADO_CODSRVVNC',pic:'ZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV36DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV48ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'AV24ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV23ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV50ContagemResultado_DataEntregaReal_To1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV49ContagemResultado_DataEntregaReal1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL1',pic:'99/99/99 99:99',nv:''},{av:'AV51ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'AV52ContagemResultado_StatusCnt1',fld:'vCONTAGEMRESULTADO_STATUSCNT1',pic:'Z9',nv:0},{av:'AV53ContagemResultado_Descricao1',fld:'vCONTAGEMRESULTADO_DESCRICAO1',pic:'',nv:''},{av:'AV54ContagemResultado_SistemaCod1',fld:'vCONTAGEMRESULTADO_SISTEMACOD1',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV55ContagemResultado_Agrupador1',fld:'vCONTAGEMRESULTADO_AGRUPADOR1',pic:'@!',nv:''},{av:'AV35DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV56ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV29ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV28ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV58ContagemResultado_DataEntregaReal_To2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV57ContagemResultado_DataEntregaReal2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL2',pic:'99/99/99 99:99',nv:''},{av:'AV59ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV60ContagemResultado_StatusCnt2',fld:'vCONTAGEMRESULTADO_STATUSCNT2',pic:'Z9',nv:0},{av:'AV61ContagemResultado_Descricao2',fld:'vCONTAGEMRESULTADO_DESCRICAO2',pic:'',nv:''},{av:'AV62ContagemResultado_SistemaCod2',fld:'vCONTAGEMRESULTADO_SISTEMACOD2',pic:'ZZZZZ9',nv:0},{av:'AV27DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV63ContagemResultado_Agrupador2',fld:'vCONTAGEMRESULTADO_AGRUPADOR2',pic:'@!',nv:''},{av:'AV64ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'AV34ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'AV33ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV66ContagemResultado_DataEntregaReal_To3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV65ContagemResultado_DataEntregaReal3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL3',pic:'99/99/99 99:99',nv:''},{av:'AV67ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'AV68ContagemResultado_StatusCnt3',fld:'vCONTAGEMRESULTADO_STATUSCNT3',pic:'Z9',nv:0},{av:'AV69ContagemResultado_Descricao3',fld:'vCONTAGEMRESULTADO_DESCRICAO3',pic:'',nv:''},{av:'AV70ContagemResultado_SistemaCod3',fld:'vCONTAGEMRESULTADO_SISTEMACOD3',pic:'ZZZZZ9',nv:0},{av:'AV32DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV71ContagemResultado_Agrupador3',fld:'vCONTAGEMRESULTADO_AGRUPADOR3',pic:'@!',nv:''},{av:'A457ContagemResultado_Demanda',fld:'CONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A764ContagemResultado_ServicoGrupo',fld:'CONTAGEMRESULTADO_SERVICOGRUPO',pic:'ZZZZZ9',nv:0},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'A2017ContagemResultado_DataEntregaReal',fld:'CONTAGEMRESULTADO_DATAENTREGAREAL',pic:'99/99/99 99:99',nv:''},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'A494ContagemResultado_Descricao',fld:'CONTAGEMRESULTADO_DESCRICAO',pic:'',nv:''},{av:'A489ContagemResultado_SistemaCod',fld:'CONTAGEMRESULTADO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A1046ContagemResultado_Agrupador',fld:'CONTAGEMRESULTADO_AGRUPADOR',pic:'@!',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A509ContagemrResultado_SistemaSigla',fld:'CONTAGEMRRESULTADO_SISTEMASIGLA',pic:'@!',nv:''},{av:'A1326ContagemResultado_ContratadaTipoFab',fld:'CONTAGEMRESULTADO_CONTRATADATIPOFAB',pic:'',nv:''},{av:'A682ContagemResultado_PFBFMUltima',fld:'CONTAGEMRESULTADO_PFBFMULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A684ContagemResultado_PFBFSUltima',fld:'CONTAGEMRESULTADO_PFBFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV14ContagemResultado_OSVinculada',fld:'vCONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'Gx_date',fld:'vTODAY',pic:'',nv:''}],oparms:[{av:'AV57ContagemResultado_DataEntregaReal2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL2',pic:'99/99/99 99:99',nv:''},{av:'AV58ContagemResultado_DataEntregaReal_To2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO2',pic:'99/99/99 99:99',nv:''},{av:'cellContagemresultado_dataentregareal_cell2_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_CELL2',prop:'Class'},{av:'cellContagemresultado_dataentregareal_to_cell2_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_TO_CELL2',prop:'Class'},{av:'lblContagemresultado_dataentregareal_rangemiddletext_12_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_RANGEMIDDLETEXT_12',prop:'Class'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E15TR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV21DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV25DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV192Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV16ContagemResultado_ServicoGrupo',fld:'vCONTAGEMRESULTADO_SERVICOGRUPO',pic:'ZZZZZ9',nv:0},{av:'AV17ContagemResultado_Servico',fld:'vCONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV18ContagemResultado_CntadaOsVinc',fld:'vCONTAGEMRESULTADO_CNTADAOSVINC',pic:'ZZZZZ9',nv:0},{av:'AV19ContagemResultado_SerGrupoVinc',fld:'vCONTAGEMRESULTADO_SERGRUPOVINC',pic:'ZZZZZ9',nv:0},{av:'AV20ContagemResultado_CodSrvVnc',fld:'vCONTAGEMRESULTADO_CODSRVVNC',pic:'ZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV36DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV48ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'AV24ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV23ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV50ContagemResultado_DataEntregaReal_To1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV49ContagemResultado_DataEntregaReal1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL1',pic:'99/99/99 99:99',nv:''},{av:'AV51ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'AV52ContagemResultado_StatusCnt1',fld:'vCONTAGEMRESULTADO_STATUSCNT1',pic:'Z9',nv:0},{av:'AV53ContagemResultado_Descricao1',fld:'vCONTAGEMRESULTADO_DESCRICAO1',pic:'',nv:''},{av:'AV54ContagemResultado_SistemaCod1',fld:'vCONTAGEMRESULTADO_SISTEMACOD1',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV55ContagemResultado_Agrupador1',fld:'vCONTAGEMRESULTADO_AGRUPADOR1',pic:'@!',nv:''},{av:'AV35DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV56ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV29ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV28ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV58ContagemResultado_DataEntregaReal_To2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV57ContagemResultado_DataEntregaReal2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL2',pic:'99/99/99 99:99',nv:''},{av:'AV59ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV60ContagemResultado_StatusCnt2',fld:'vCONTAGEMRESULTADO_STATUSCNT2',pic:'Z9',nv:0},{av:'AV61ContagemResultado_Descricao2',fld:'vCONTAGEMRESULTADO_DESCRICAO2',pic:'',nv:''},{av:'AV62ContagemResultado_SistemaCod2',fld:'vCONTAGEMRESULTADO_SISTEMACOD2',pic:'ZZZZZ9',nv:0},{av:'AV27DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV63ContagemResultado_Agrupador2',fld:'vCONTAGEMRESULTADO_AGRUPADOR2',pic:'@!',nv:''},{av:'AV64ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'AV34ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'AV33ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV66ContagemResultado_DataEntregaReal_To3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV65ContagemResultado_DataEntregaReal3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL3',pic:'99/99/99 99:99',nv:''},{av:'AV67ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'AV68ContagemResultado_StatusCnt3',fld:'vCONTAGEMRESULTADO_STATUSCNT3',pic:'Z9',nv:0},{av:'AV69ContagemResultado_Descricao3',fld:'vCONTAGEMRESULTADO_DESCRICAO3',pic:'',nv:''},{av:'AV70ContagemResultado_SistemaCod3',fld:'vCONTAGEMRESULTADO_SISTEMACOD3',pic:'ZZZZZ9',nv:0},{av:'AV32DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV71ContagemResultado_Agrupador3',fld:'vCONTAGEMRESULTADO_AGRUPADOR3',pic:'@!',nv:''},{av:'A457ContagemResultado_Demanda',fld:'CONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A764ContagemResultado_ServicoGrupo',fld:'CONTAGEMRESULTADO_SERVICOGRUPO',pic:'ZZZZZ9',nv:0},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'A2017ContagemResultado_DataEntregaReal',fld:'CONTAGEMRESULTADO_DATAENTREGAREAL',pic:'99/99/99 99:99',nv:''},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'A494ContagemResultado_Descricao',fld:'CONTAGEMRESULTADO_DESCRICAO',pic:'',nv:''},{av:'A489ContagemResultado_SistemaCod',fld:'CONTAGEMRESULTADO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A1046ContagemResultado_Agrupador',fld:'CONTAGEMRESULTADO_AGRUPADOR',pic:'@!',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A509ContagemrResultado_SistemaSigla',fld:'CONTAGEMRRESULTADO_SISTEMASIGLA',pic:'@!',nv:''},{av:'A1326ContagemResultado_ContratadaTipoFab',fld:'CONTAGEMRESULTADO_CONTRATADATIPOFAB',pic:'',nv:''},{av:'A682ContagemResultado_PFBFMUltima',fld:'CONTAGEMRESULTADO_PFBFMULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A684ContagemResultado_PFBFSUltima',fld:'CONTAGEMRESULTADO_PFBFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV14ContagemResultado_OSVinculada',fld:'vCONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'Gx_date',fld:'vTODAY',pic:'',nv:''}],oparms:[{av:'AV35DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV25DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV27DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV56ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV64ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV21DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV22DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV55ContagemResultado_Agrupador1',fld:'vCONTAGEMRESULTADO_AGRUPADOR1',pic:'@!',nv:''},{av:'AV54ContagemResultado_SistemaCod1',fld:'vCONTAGEMRESULTADO_SISTEMACOD1',pic:'ZZZZZ9',nv:0},{av:'AV53ContagemResultado_Descricao1',fld:'vCONTAGEMRESULTADO_DESCRICAO1',pic:'',nv:''},{av:'AV52ContagemResultado_StatusCnt1',fld:'vCONTAGEMRESULTADO_STATUSCNT1',pic:'Z9',nv:0},{av:'AV51ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'AV49ContagemResultado_DataEntregaReal1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL1',pic:'99/99/99 99:99',nv:''},{av:'AV50ContagemResultado_DataEntregaReal_To1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV23ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV24ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV48ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV63ContagemResultado_Agrupador2',fld:'vCONTAGEMRESULTADO_AGRUPADOR2',pic:'@!',nv:''},{av:'AV62ContagemResultado_SistemaCod2',fld:'vCONTAGEMRESULTADO_SISTEMACOD2',pic:'ZZZZZ9',nv:0},{av:'AV61ContagemResultado_Descricao2',fld:'vCONTAGEMRESULTADO_DESCRICAO2',pic:'',nv:''},{av:'AV60ContagemResultado_StatusCnt2',fld:'vCONTAGEMRESULTADO_STATUSCNT2',pic:'Z9',nv:0},{av:'AV59ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV57ContagemResultado_DataEntregaReal2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL2',pic:'99/99/99 99:99',nv:''},{av:'AV58ContagemResultado_DataEntregaReal_To2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV28ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV29ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV71ContagemResultado_Agrupador3',fld:'vCONTAGEMRESULTADO_AGRUPADOR3',pic:'@!',nv:''},{av:'AV70ContagemResultado_SistemaCod3',fld:'vCONTAGEMRESULTADO_SISTEMACOD3',pic:'ZZZZZ9',nv:0},{av:'AV69ContagemResultado_Descricao3',fld:'vCONTAGEMRESULTADO_DESCRICAO3',pic:'',nv:''},{av:'AV68ContagemResultado_StatusCnt3',fld:'vCONTAGEMRESULTADO_STATUSCNT3',pic:'Z9',nv:0},{av:'AV67ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'AV65ContagemResultado_DataEntregaReal3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL3',pic:'99/99/99 99:99',nv:''},{av:'AV66ContagemResultado_DataEntregaReal_To3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV34ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'edtavContagemresultado_demandafm2_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDAFM2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datadmn2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_dataentregareal2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAENTREGAREAL2',prop:'Visible'},{av:'cmbavContagemresultado_statusdmn2'},{av:'cmbavContagemresultado_statuscnt2'},{av:'edtavContagemresultado_descricao2_Visible',ctrl:'vCONTAGEMRESULTADO_DESCRICAO2',prop:'Visible'},{av:'dynavContagemresultado_sistemacod2'},{av:'edtavContagemresultado_agrupador2_Visible',ctrl:'vCONTAGEMRESULTADO_AGRUPADOR2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContagemresultado_demandafm3_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDAFM3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datadmn3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_dataentregareal3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAENTREGAREAL3',prop:'Visible'},{av:'cmbavContagemresultado_statusdmn3'},{av:'cmbavContagemresultado_statuscnt3'},{av:'edtavContagemresultado_descricao3_Visible',ctrl:'vCONTAGEMRESULTADO_DESCRICAO3',prop:'Visible'},{av:'dynavContagemresultado_sistemacod3'},{av:'edtavContagemresultado_agrupador3_Visible',ctrl:'vCONTAGEMRESULTADO_AGRUPADOR3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContagemresultado_demandafm1_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDAFM1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datadmn1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_dataentregareal1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAENTREGAREAL1',prop:'Visible'},{av:'cmbavContagemresultado_statusdmn1'},{av:'cmbavContagemresultado_statuscnt1'},{av:'edtavContagemresultado_descricao1_Visible',ctrl:'vCONTAGEMRESULTADO_DESCRICAO1',prop:'Visible'},{av:'dynavContagemresultado_sistemacod1'},{av:'edtavContagemresultado_agrupador1_Visible',ctrl:'vCONTAGEMRESULTADO_AGRUPADOR1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'cellContagemresultado_dataentregareal_cell2_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_CELL2',prop:'Class'},{av:'cellContagemresultado_dataentregareal_to_cell2_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_TO_CELL2',prop:'Class'},{av:'lblContagemresultado_dataentregareal_rangemiddletext_12_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_RANGEMIDDLETEXT_12',prop:'Class'},{av:'cellContagemresultado_dataentregareal_cell3_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_CELL3',prop:'Class'},{av:'cellContagemresultado_dataentregareal_to_cell3_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_TO_CELL3',prop:'Class'},{av:'lblContagemresultado_dataentregareal_rangemiddletext_13_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_RANGEMIDDLETEXT_13',prop:'Class'},{av:'cellContagemresultado_dataentregareal_cell1_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_CELL1',prop:'Class'},{av:'cellContagemresultado_dataentregareal_to_cell1_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_TO_CELL1',prop:'Class'},{av:'lblContagemresultado_dataentregareal_rangemiddletext_11_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_RANGEMIDDLETEXT_11',prop:'Class'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E22TR2',iparms:[{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'Gx_date',fld:'vTODAY',pic:'',nv:''}],oparms:[{av:'AV32DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtavContagemresultado_demandafm3_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDAFM3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datadmn3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_dataentregareal3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAENTREGAREAL3',prop:'Visible'},{av:'cmbavContagemresultado_statusdmn3'},{av:'cmbavContagemresultado_statuscnt3'},{av:'edtavContagemresultado_descricao3_Visible',ctrl:'vCONTAGEMRESULTADO_DESCRICAO3',prop:'Visible'},{av:'dynavContagemresultado_sistemacod3'},{av:'edtavContagemresultado_agrupador3_Visible',ctrl:'vCONTAGEMRESULTADO_AGRUPADOR3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'cellContagemresultado_dataentregareal_cell3_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_CELL3',prop:'Class'},{av:'cellContagemresultado_dataentregareal_to_cell3_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_TO_CELL3',prop:'Class'},{av:'lblContagemresultado_dataentregareal_rangemiddletext_13_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_RANGEMIDDLETEXT_13',prop:'Class'},{av:'AV65ContagemResultado_DataEntregaReal3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL3',pic:'99/99/99 99:99',nv:''},{av:'AV66ContagemResultado_DataEntregaReal_To3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO3',pic:'99/99/99 99:99',nv:''}]}");
         setEventMetadata("VDYNAMICFILTERSOPERATOR3.CLICK","{handler:'E23TR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV21DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV25DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV192Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV16ContagemResultado_ServicoGrupo',fld:'vCONTAGEMRESULTADO_SERVICOGRUPO',pic:'ZZZZZ9',nv:0},{av:'AV17ContagemResultado_Servico',fld:'vCONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV18ContagemResultado_CntadaOsVinc',fld:'vCONTAGEMRESULTADO_CNTADAOSVINC',pic:'ZZZZZ9',nv:0},{av:'AV19ContagemResultado_SerGrupoVinc',fld:'vCONTAGEMRESULTADO_SERGRUPOVINC',pic:'ZZZZZ9',nv:0},{av:'AV20ContagemResultado_CodSrvVnc',fld:'vCONTAGEMRESULTADO_CODSRVVNC',pic:'ZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV36DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV48ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'AV24ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV23ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV50ContagemResultado_DataEntregaReal_To1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV49ContagemResultado_DataEntregaReal1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL1',pic:'99/99/99 99:99',nv:''},{av:'AV51ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'AV52ContagemResultado_StatusCnt1',fld:'vCONTAGEMRESULTADO_STATUSCNT1',pic:'Z9',nv:0},{av:'AV53ContagemResultado_Descricao1',fld:'vCONTAGEMRESULTADO_DESCRICAO1',pic:'',nv:''},{av:'AV54ContagemResultado_SistemaCod1',fld:'vCONTAGEMRESULTADO_SISTEMACOD1',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV55ContagemResultado_Agrupador1',fld:'vCONTAGEMRESULTADO_AGRUPADOR1',pic:'@!',nv:''},{av:'AV35DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV56ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV29ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV28ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV58ContagemResultado_DataEntregaReal_To2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV57ContagemResultado_DataEntregaReal2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL2',pic:'99/99/99 99:99',nv:''},{av:'AV59ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV60ContagemResultado_StatusCnt2',fld:'vCONTAGEMRESULTADO_STATUSCNT2',pic:'Z9',nv:0},{av:'AV61ContagemResultado_Descricao2',fld:'vCONTAGEMRESULTADO_DESCRICAO2',pic:'',nv:''},{av:'AV62ContagemResultado_SistemaCod2',fld:'vCONTAGEMRESULTADO_SISTEMACOD2',pic:'ZZZZZ9',nv:0},{av:'AV27DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV63ContagemResultado_Agrupador2',fld:'vCONTAGEMRESULTADO_AGRUPADOR2',pic:'@!',nv:''},{av:'AV64ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'AV34ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'AV33ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV66ContagemResultado_DataEntregaReal_To3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV65ContagemResultado_DataEntregaReal3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL3',pic:'99/99/99 99:99',nv:''},{av:'AV67ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'AV68ContagemResultado_StatusCnt3',fld:'vCONTAGEMRESULTADO_STATUSCNT3',pic:'Z9',nv:0},{av:'AV69ContagemResultado_Descricao3',fld:'vCONTAGEMRESULTADO_DESCRICAO3',pic:'',nv:''},{av:'AV70ContagemResultado_SistemaCod3',fld:'vCONTAGEMRESULTADO_SISTEMACOD3',pic:'ZZZZZ9',nv:0},{av:'AV32DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV71ContagemResultado_Agrupador3',fld:'vCONTAGEMRESULTADO_AGRUPADOR3',pic:'@!',nv:''},{av:'A457ContagemResultado_Demanda',fld:'CONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A764ContagemResultado_ServicoGrupo',fld:'CONTAGEMRESULTADO_SERVICOGRUPO',pic:'ZZZZZ9',nv:0},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'A2017ContagemResultado_DataEntregaReal',fld:'CONTAGEMRESULTADO_DATAENTREGAREAL',pic:'99/99/99 99:99',nv:''},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'A494ContagemResultado_Descricao',fld:'CONTAGEMRESULTADO_DESCRICAO',pic:'',nv:''},{av:'A489ContagemResultado_SistemaCod',fld:'CONTAGEMRESULTADO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A1046ContagemResultado_Agrupador',fld:'CONTAGEMRESULTADO_AGRUPADOR',pic:'@!',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A509ContagemrResultado_SistemaSigla',fld:'CONTAGEMRRESULTADO_SISTEMASIGLA',pic:'@!',nv:''},{av:'A1326ContagemResultado_ContratadaTipoFab',fld:'CONTAGEMRESULTADO_CONTRATADATIPOFAB',pic:'',nv:''},{av:'A682ContagemResultado_PFBFMUltima',fld:'CONTAGEMRESULTADO_PFBFMULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A684ContagemResultado_PFBFSUltima',fld:'CONTAGEMRESULTADO_PFBFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV14ContagemResultado_OSVinculada',fld:'vCONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'Gx_date',fld:'vTODAY',pic:'',nv:''}],oparms:[{av:'AV65ContagemResultado_DataEntregaReal3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL3',pic:'99/99/99 99:99',nv:''},{av:'AV66ContagemResultado_DataEntregaReal_To3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO3',pic:'99/99/99 99:99',nv:''},{av:'cellContagemresultado_dataentregareal_cell3_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_CELL3',prop:'Class'},{av:'cellContagemresultado_dataentregareal_to_cell3_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_TO_CELL3',prop:'Class'},{av:'lblContagemresultado_dataentregareal_rangemiddletext_13_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_RANGEMIDDLETEXT_13',prop:'Class'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV21DynamicFiltersSelector1 = "";
         AV26DynamicFiltersSelector2 = "";
         AV31DynamicFiltersSelector3 = "";
         AV192Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV48ContagemResultado_DemandaFM1 = "";
         AV24ContagemResultado_DataDmn_To1 = DateTime.MinValue;
         AV23ContagemResultado_DataDmn1 = DateTime.MinValue;
         AV50ContagemResultado_DataEntregaReal_To1 = (DateTime)(DateTime.MinValue);
         AV49ContagemResultado_DataEntregaReal1 = (DateTime)(DateTime.MinValue);
         AV51ContagemResultado_StatusDmn1 = "";
         AV53ContagemResultado_Descricao1 = "";
         AV55ContagemResultado_Agrupador1 = "";
         AV56ContagemResultado_DemandaFM2 = "";
         AV29ContagemResultado_DataDmn_To2 = DateTime.MinValue;
         AV28ContagemResultado_DataDmn2 = DateTime.MinValue;
         AV58ContagemResultado_DataEntregaReal_To2 = (DateTime)(DateTime.MinValue);
         AV57ContagemResultado_DataEntregaReal2 = (DateTime)(DateTime.MinValue);
         AV59ContagemResultado_StatusDmn2 = "";
         AV61ContagemResultado_Descricao2 = "";
         AV63ContagemResultado_Agrupador2 = "";
         AV64ContagemResultado_DemandaFM3 = "";
         AV34ContagemResultado_DataDmn_To3 = DateTime.MinValue;
         AV33ContagemResultado_DataDmn3 = DateTime.MinValue;
         AV66ContagemResultado_DataEntregaReal_To3 = (DateTime)(DateTime.MinValue);
         AV65ContagemResultado_DataEntregaReal3 = (DateTime)(DateTime.MinValue);
         AV67ContagemResultado_StatusDmn3 = "";
         AV69ContagemResultado_Descricao3 = "";
         AV71ContagemResultado_Agrupador3 = "";
         A457ContagemResultado_Demanda = "";
         A493ContagemResultado_DemandaFM = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A2017ContagemResultado_DataEntregaReal = (DateTime)(DateTime.MinValue);
         A484ContagemResultado_StatusDmn = "";
         A494ContagemResultado_Descricao = "";
         A1046ContagemResultado_Agrupador = "";
         A509ContagemrResultado_SistemaSigla = "";
         A1326ContagemResultado_ContratadaTipoFab = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         Gx_date = DateTime.MinValue;
         AV98ExcelFilename = "";
         AV99ErrorMessage = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV47Display = "";
         AV189Display_GXI = "";
         AV89ContagemResultado_DemandaFM = "";
         AV90ContagemResultado_Demanda = "";
         AV91ContagemResultado_DataDmn = DateTime.MinValue;
         AV92ContagemResultado_DataEntregaReal = (DateTime)(DateTime.MinValue);
         AV93ContagemResultado_Descricao = "";
         AV94ContagemrResultado_SistemaSigla = "";
         AV95ContagemResultado_ContratadaTipoFab = "S";
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00TR2_A39Contratada_Codigo = new int[1] ;
         H00TR2_A438Contratada_Sigla = new String[] {""} ;
         H00TR2_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00TR2_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00TR3_A74Contrato_Codigo = new int[1] ;
         H00TR3_A155Servico_Codigo = new int[1] ;
         H00TR3_A160ContratoServicos_Codigo = new int[1] ;
         H00TR3_A157ServicoGrupo_Codigo = new int[1] ;
         H00TR3_A158ServicoGrupo_Descricao = new String[] {""} ;
         H00TR3_A39Contratada_Codigo = new int[1] ;
         H00TR3_A638ContratoServicos_Ativo = new bool[] {false} ;
         H00TR4_A74Contrato_Codigo = new int[1] ;
         H00TR4_A160ContratoServicos_Codigo = new int[1] ;
         H00TR4_A155Servico_Codigo = new int[1] ;
         H00TR4_A605Servico_Sigla = new String[] {""} ;
         H00TR4_A39Contratada_Codigo = new int[1] ;
         H00TR4_A157ServicoGrupo_Codigo = new int[1] ;
         H00TR4_A638ContratoServicos_Ativo = new bool[] {false} ;
         H00TR5_A39Contratada_Codigo = new int[1] ;
         H00TR5_A438Contratada_Sigla = new String[] {""} ;
         H00TR5_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00TR5_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00TR6_A74Contrato_Codigo = new int[1] ;
         H00TR6_A155Servico_Codigo = new int[1] ;
         H00TR6_A160ContratoServicos_Codigo = new int[1] ;
         H00TR6_A157ServicoGrupo_Codigo = new int[1] ;
         H00TR6_A158ServicoGrupo_Descricao = new String[] {""} ;
         H00TR6_A39Contratada_Codigo = new int[1] ;
         H00TR6_A638ContratoServicos_Ativo = new bool[] {false} ;
         H00TR7_A74Contrato_Codigo = new int[1] ;
         H00TR7_A160ContratoServicos_Codigo = new int[1] ;
         H00TR7_A155Servico_Codigo = new int[1] ;
         H00TR7_A605Servico_Sigla = new String[] {""} ;
         H00TR7_A39Contratada_Codigo = new int[1] ;
         H00TR7_A157ServicoGrupo_Codigo = new int[1] ;
         H00TR7_A638ContratoServicos_Ativo = new bool[] {false} ;
         H00TR8_A127Sistema_Codigo = new int[1] ;
         H00TR8_A129Sistema_Sigla = new String[] {""} ;
         H00TR8_A135Sistema_AreaTrabalhoCod = new int[1] ;
         H00TR8_A130Sistema_Ativo = new bool[] {false} ;
         H00TR9_A127Sistema_Codigo = new int[1] ;
         H00TR9_A129Sistema_Sigla = new String[] {""} ;
         H00TR9_A135Sistema_AreaTrabalhoCod = new int[1] ;
         H00TR9_A130Sistema_Ativo = new bool[] {false} ;
         H00TR10_A127Sistema_Codigo = new int[1] ;
         H00TR10_A129Sistema_Sigla = new String[] {""} ;
         H00TR10_A135Sistema_AreaTrabalhoCod = new int[1] ;
         H00TR10_A130Sistema_Ativo = new bool[] {false} ;
         GridContainer = new GXWebGrid( context);
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         lV48ContagemResultado_DemandaFM1 = "";
         lV53ContagemResultado_Descricao1 = "";
         lV55ContagemResultado_Agrupador1 = "";
         lV56ContagemResultado_DemandaFM2 = "";
         lV61ContagemResultado_Descricao2 = "";
         lV63ContagemResultado_Agrupador2 = "";
         lV64ContagemResultado_DemandaFM3 = "";
         lV69ContagemResultado_Descricao3 = "";
         lV71ContagemResultado_Agrupador3 = "";
         H00TR12_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00TR12_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00TR12_A1046ContagemResultado_Agrupador = new String[] {""} ;
         H00TR12_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         H00TR12_A489ContagemResultado_SistemaCod = new int[1] ;
         H00TR12_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         H00TR12_A494ContagemResultado_Descricao = new String[] {""} ;
         H00TR12_n494ContagemResultado_Descricao = new bool[] {false} ;
         H00TR12_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00TR12_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00TR12_A2017ContagemResultado_DataEntregaReal = new DateTime[] {DateTime.MinValue} ;
         H00TR12_n2017ContagemResultado_DataEntregaReal = new bool[] {false} ;
         H00TR12_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         H00TR12_A493ContagemResultado_DemandaFM = new String[] {""} ;
         H00TR12_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         H00TR12_A602ContagemResultado_OSVinculada = new int[1] ;
         H00TR12_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         H00TR12_A601ContagemResultado_Servico = new int[1] ;
         H00TR12_n601ContagemResultado_Servico = new bool[] {false} ;
         H00TR12_A764ContagemResultado_ServicoGrupo = new int[1] ;
         H00TR12_n764ContagemResultado_ServicoGrupo = new bool[] {false} ;
         H00TR12_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00TR12_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00TR12_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00TR12_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00TR12_A456ContagemResultado_Codigo = new int[1] ;
         H00TR12_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         H00TR12_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         H00TR12_A1326ContagemResultado_ContratadaTipoFab = new String[] {""} ;
         H00TR12_n1326ContagemResultado_ContratadaTipoFab = new bool[] {false} ;
         H00TR12_A457ContagemResultado_Demanda = new String[] {""} ;
         H00TR12_n457ContagemResultado_Demanda = new bool[] {false} ;
         H00TR12_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         H00TR12_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         GridRow = new GXWebRow();
         H00TR14_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00TR14_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00TR14_A1046ContagemResultado_Agrupador = new String[] {""} ;
         H00TR14_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         H00TR14_A489ContagemResultado_SistemaCod = new int[1] ;
         H00TR14_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         H00TR14_A494ContagemResultado_Descricao = new String[] {""} ;
         H00TR14_n494ContagemResultado_Descricao = new bool[] {false} ;
         H00TR14_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00TR14_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00TR14_A2017ContagemResultado_DataEntregaReal = new DateTime[] {DateTime.MinValue} ;
         H00TR14_n2017ContagemResultado_DataEntregaReal = new bool[] {false} ;
         H00TR14_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         H00TR14_A493ContagemResultado_DemandaFM = new String[] {""} ;
         H00TR14_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         H00TR14_A456ContagemResultado_Codigo = new int[1] ;
         H00TR14_A601ContagemResultado_Servico = new int[1] ;
         H00TR14_n601ContagemResultado_Servico = new bool[] {false} ;
         H00TR14_A764ContagemResultado_ServicoGrupo = new int[1] ;
         H00TR14_n764ContagemResultado_ServicoGrupo = new bool[] {false} ;
         H00TR14_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00TR14_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00TR14_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00TR14_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00TR14_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         H00TR14_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         H00TR14_A602ContagemResultado_OSVinculada = new int[1] ;
         H00TR14_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         H00TR14_A1326ContagemResultado_ContratadaTipoFab = new String[] {""} ;
         H00TR14_n1326ContagemResultado_ContratadaTipoFab = new bool[] {false} ;
         H00TR14_A457ContagemResultado_Demanda = new String[] {""} ;
         H00TR14_n457ContagemResultado_Demanda = new bool[] {false} ;
         H00TR14_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         H00TR14_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         AV37Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblFiltertextcontratada_areatrabalhocod_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblContagemresultado_dataentregareal_rangemiddletext_13_Jsonclick = "";
         lblContagemresultado_datadmn_rangemiddletext_13_Jsonclick = "";
         lblContagemresultado_dataentregareal_rangemiddletext_12_Jsonclick = "";
         lblContagemresultado_datadmn_rangemiddletext_12_Jsonclick = "";
         lblContagemresultado_dataentregareal_rangemiddletext_11_Jsonclick = "";
         lblContagemresultado_datadmn_rangemiddletext_11_Jsonclick = "";
         lblFiltertextcontagemresultado_cntadaosvinc_Jsonclick = "";
         lblFiltertextcontagemresultado_sergrupovinc_Jsonclick = "";
         lblFiltertextcontagemresultado_codsrvvnc_Jsonclick = "";
         lblFiltertextcontagemresultado_contratadacod_Jsonclick = "";
         lblFiltertextcontagemresultado_servicogrupo_Jsonclick = "";
         lblFiltertextcontagemresultado_servico_Jsonclick = "";
         lblContagemresultadotitle_Jsonclick = "";
         imgExport_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_relatoriocomparacaodemandas_old__default(),
            new Object[][] {
                new Object[] {
               H00TR2_A39Contratada_Codigo, H00TR2_A438Contratada_Sigla, H00TR2_A52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               H00TR3_A74Contrato_Codigo, H00TR3_A155Servico_Codigo, H00TR3_A160ContratoServicos_Codigo, H00TR3_A157ServicoGrupo_Codigo, H00TR3_A158ServicoGrupo_Descricao, H00TR3_A39Contratada_Codigo, H00TR3_A638ContratoServicos_Ativo
               }
               , new Object[] {
               H00TR4_A74Contrato_Codigo, H00TR4_A160ContratoServicos_Codigo, H00TR4_A155Servico_Codigo, H00TR4_A605Servico_Sigla, H00TR4_A39Contratada_Codigo, H00TR4_A157ServicoGrupo_Codigo, H00TR4_A638ContratoServicos_Ativo
               }
               , new Object[] {
               H00TR5_A39Contratada_Codigo, H00TR5_A438Contratada_Sigla, H00TR5_A52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               H00TR6_A74Contrato_Codigo, H00TR6_A155Servico_Codigo, H00TR6_A160ContratoServicos_Codigo, H00TR6_A157ServicoGrupo_Codigo, H00TR6_A158ServicoGrupo_Descricao, H00TR6_A39Contratada_Codigo, H00TR6_A638ContratoServicos_Ativo
               }
               , new Object[] {
               H00TR7_A74Contrato_Codigo, H00TR7_A160ContratoServicos_Codigo, H00TR7_A155Servico_Codigo, H00TR7_A605Servico_Sigla, H00TR7_A39Contratada_Codigo, H00TR7_A157ServicoGrupo_Codigo, H00TR7_A638ContratoServicos_Ativo
               }
               , new Object[] {
               H00TR8_A127Sistema_Codigo, H00TR8_A129Sistema_Sigla, H00TR8_A135Sistema_AreaTrabalhoCod, H00TR8_A130Sistema_Ativo
               }
               , new Object[] {
               H00TR9_A127Sistema_Codigo, H00TR9_A129Sistema_Sigla, H00TR9_A135Sistema_AreaTrabalhoCod, H00TR9_A130Sistema_Ativo
               }
               , new Object[] {
               H00TR10_A127Sistema_Codigo, H00TR10_A129Sistema_Sigla, H00TR10_A135Sistema_AreaTrabalhoCod, H00TR10_A130Sistema_Ativo
               }
               , new Object[] {
               H00TR12_A1553ContagemResultado_CntSrvCod, H00TR12_n1553ContagemResultado_CntSrvCod, H00TR12_A1046ContagemResultado_Agrupador, H00TR12_n1046ContagemResultado_Agrupador, H00TR12_A489ContagemResultado_SistemaCod, H00TR12_n489ContagemResultado_SistemaCod, H00TR12_A494ContagemResultado_Descricao, H00TR12_n494ContagemResultado_Descricao, H00TR12_A484ContagemResultado_StatusDmn, H00TR12_n484ContagemResultado_StatusDmn,
               H00TR12_A2017ContagemResultado_DataEntregaReal, H00TR12_n2017ContagemResultado_DataEntregaReal, H00TR12_A471ContagemResultado_DataDmn, H00TR12_A493ContagemResultado_DemandaFM, H00TR12_n493ContagemResultado_DemandaFM, H00TR12_A602ContagemResultado_OSVinculada, H00TR12_n602ContagemResultado_OSVinculada, H00TR12_A601ContagemResultado_Servico, H00TR12_n601ContagemResultado_Servico, H00TR12_A764ContagemResultado_ServicoGrupo,
               H00TR12_n764ContagemResultado_ServicoGrupo, H00TR12_A490ContagemResultado_ContratadaCod, H00TR12_n490ContagemResultado_ContratadaCod, H00TR12_A52Contratada_AreaTrabalhoCod, H00TR12_n52Contratada_AreaTrabalhoCod, H00TR12_A456ContagemResultado_Codigo, H00TR12_A509ContagemrResultado_SistemaSigla, H00TR12_n509ContagemrResultado_SistemaSigla, H00TR12_A1326ContagemResultado_ContratadaTipoFab, H00TR12_n1326ContagemResultado_ContratadaTipoFab,
               H00TR12_A457ContagemResultado_Demanda, H00TR12_n457ContagemResultado_Demanda, H00TR12_A682ContagemResultado_PFBFMUltima, H00TR12_A684ContagemResultado_PFBFSUltima
               }
               , new Object[] {
               H00TR14_A1553ContagemResultado_CntSrvCod, H00TR14_n1553ContagemResultado_CntSrvCod, H00TR14_A1046ContagemResultado_Agrupador, H00TR14_n1046ContagemResultado_Agrupador, H00TR14_A489ContagemResultado_SistemaCod, H00TR14_n489ContagemResultado_SistemaCod, H00TR14_A494ContagemResultado_Descricao, H00TR14_n494ContagemResultado_Descricao, H00TR14_A484ContagemResultado_StatusDmn, H00TR14_n484ContagemResultado_StatusDmn,
               H00TR14_A2017ContagemResultado_DataEntregaReal, H00TR14_n2017ContagemResultado_DataEntregaReal, H00TR14_A471ContagemResultado_DataDmn, H00TR14_A493ContagemResultado_DemandaFM, H00TR14_n493ContagemResultado_DemandaFM, H00TR14_A456ContagemResultado_Codigo, H00TR14_A601ContagemResultado_Servico, H00TR14_n601ContagemResultado_Servico, H00TR14_A764ContagemResultado_ServicoGrupo, H00TR14_n764ContagemResultado_ServicoGrupo,
               H00TR14_A490ContagemResultado_ContratadaCod, H00TR14_n490ContagemResultado_ContratadaCod, H00TR14_A52Contratada_AreaTrabalhoCod, H00TR14_n52Contratada_AreaTrabalhoCod, H00TR14_A509ContagemrResultado_SistemaSigla, H00TR14_n509ContagemrResultado_SistemaSigla, H00TR14_A602ContagemResultado_OSVinculada, H00TR14_n602ContagemResultado_OSVinculada, H00TR14_A1326ContagemResultado_ContratadaTipoFab, H00TR14_n1326ContagemResultado_ContratadaTipoFab,
               H00TR14_A457ContagemResultado_Demanda, H00TR14_n457ContagemResultado_Demanda, H00TR14_A682ContagemResultado_PFBFMUltima, H00TR14_A684ContagemResultado_PFBFSUltima
               }
            }
         );
         AV192Pgmname = "WP_RelatorioComparacaoDemandas_OLD";
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         AV192Pgmname = "WP_RelatorioComparacaoDemandas_OLD";
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         edtavContagemresultado_codigo_Enabled = 0;
         edtavContagemresultado_osvinculada_Enabled = 0;
         cmbavContagemresultado_contratadatipofab.Enabled = 0;
         edtavContagemresultado_pfbfmultima_Enabled = 0;
         edtavContagemresultado_pfbfsultima_Enabled = 0;
         edtavEsforco_Enabled = 0;
      }

      private short nRcdExists_3 ;
      private short nIsMod_3 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_187 ;
      private short nGXsfl_187_idx=1 ;
      private short AV52ContagemResultado_StatusCnt1 ;
      private short AV22DynamicFiltersOperator1 ;
      private short AV60ContagemResultado_StatusCnt2 ;
      private short AV27DynamicFiltersOperator2 ;
      private short AV68ContagemResultado_StatusCnt3 ;
      private short AV32DynamicFiltersOperator3 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_187_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private short wbTemp ;
      private int AV15ContagemResultado_ContratadaCod ;
      private int AV16ContagemResultado_ServicoGrupo ;
      private int AV18ContagemResultado_CntadaOsVinc ;
      private int AV19ContagemResultado_SerGrupoVinc ;
      private int subGrid_Rows ;
      private int AV13Contratada_AreaTrabalhoCod ;
      private int AV17ContagemResultado_Servico ;
      private int AV20ContagemResultado_CodSrvVnc ;
      private int AV54ContagemResultado_SistemaCod1 ;
      private int AV62ContagemResultado_SistemaCod2 ;
      private int AV70ContagemResultado_SistemaCod3 ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A764ContagemResultado_ServicoGrupo ;
      private int A601ContagemResultado_Servico ;
      private int A602ContagemResultado_OSVinculada ;
      private int A489ContagemResultado_SistemaCod ;
      private int A456ContagemResultado_Codigo ;
      private int AV14ContagemResultado_OSVinculada ;
      private int Gridpaginationbar_Pagestoshow ;
      private int AV88ContagemResultado_Codigo ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int edtavContagemresultado_codigo_Enabled ;
      private int edtavContagemresultado_osvinculada_Enabled ;
      private int edtavContagemresultado_pfbfmultima_Enabled ;
      private int edtavContagemresultado_pfbfsultima_Enabled ;
      private int edtavEsforco_Enabled ;
      private int GRID_nGridOutOfScope ;
      private int AV44PageToGo ;
      private int AV6WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavContagemresultado_demandafm1_Visible ;
      private int tblTablemergeddynamicfilterscontagemresultado_datadmn1_Visible ;
      private int tblTablemergeddynamicfilterscontagemresultado_dataentregareal1_Visible ;
      private int edtavContagemresultado_descricao1_Visible ;
      private int edtavContagemresultado_agrupador1_Visible ;
      private int edtavContagemresultado_demandafm2_Visible ;
      private int tblTablemergeddynamicfilterscontagemresultado_datadmn2_Visible ;
      private int tblTablemergeddynamicfilterscontagemresultado_dataentregareal2_Visible ;
      private int edtavContagemresultado_descricao2_Visible ;
      private int edtavContagemresultado_agrupador2_Visible ;
      private int edtavContagemresultado_demandafm3_Visible ;
      private int tblTablemergeddynamicfilterscontagemresultado_datadmn3_Visible ;
      private int tblTablemergeddynamicfilterscontagemresultado_dataentregareal3_Visible ;
      private int edtavContagemresultado_descricao3_Visible ;
      private int edtavContagemresultado_agrupador3_Visible ;
      private int AV193GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV45GridCurrentPage ;
      private long AV46GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal A682ContagemResultado_PFBFMUltima ;
      private decimal A684ContagemResultado_PFBFSUltima ;
      private decimal AV96ContagemResultado_PFBFMUltima ;
      private decimal AV97ContagemResultado_PFBFSUltima ;
      private decimal AV87Esforco ;
      private String Gridpaginationbar_Selectedpage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_187_idx="0001" ;
      private String AV192Pgmname ;
      private String AV51ContagemResultado_StatusDmn1 ;
      private String AV55ContagemResultado_Agrupador1 ;
      private String AV59ContagemResultado_StatusDmn2 ;
      private String AV63ContagemResultado_Agrupador2 ;
      private String AV67ContagemResultado_StatusDmn3 ;
      private String AV71ContagemResultado_Agrupador3 ;
      private String A484ContagemResultado_StatusDmn ;
      private String A1046ContagemResultado_Agrupador ;
      private String A509ContagemrResultado_SistemaSigla ;
      private String A1326ContagemResultado_ContratadaTipoFab ;
      private String edtavContagemresultado_osvinculada_Internalname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavDisplay_Internalname ;
      private String edtavContagemresultado_codigo_Internalname ;
      private String edtavContagemresultado_demandafm_Internalname ;
      private String edtavContagemresultado_demanda_Internalname ;
      private String edtavContagemresultado_datadmn_Internalname ;
      private String edtavContagemresultado_dataentregareal_Internalname ;
      private String edtavContagemresultado_descricao_Internalname ;
      private String AV94ContagemrResultado_SistemaSigla ;
      private String edtavContagemrresultado_sistemasigla_Internalname ;
      private String cmbavContagemresultado_contratadatipofab_Internalname ;
      private String AV95ContagemResultado_ContratadaTipoFab ;
      private String edtavContagemresultado_pfbfmultima_Internalname ;
      private String edtavContagemresultado_pfbfsultima_Internalname ;
      private String edtavEsforco_Internalname ;
      private String GXCCtl ;
      private String edtavContratada_areatrabalhocod_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String dynavContagemresultado_contratadacod_Internalname ;
      private String dynavContagemresultado_servicogrupo_Internalname ;
      private String dynavContagemresultado_servico_Internalname ;
      private String dynavContagemresultado_cntadaosvinc_Internalname ;
      private String dynavContagemresultado_sergrupovinc_Internalname ;
      private String dynavContagemresultado_codsrvvnc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavContagemresultado_demandafm1_Internalname ;
      private String edtavContagemresultado_datadmn1_Internalname ;
      private String edtavContagemresultado_datadmn_to1_Internalname ;
      private String edtavContagemresultado_dataentregareal1_Internalname ;
      private String edtavContagemresultado_dataentregareal_to1_Internalname ;
      private String cmbavContagemresultado_statusdmn1_Internalname ;
      private String cmbavContagemresultado_statuscnt1_Internalname ;
      private String edtavContagemresultado_descricao1_Internalname ;
      private String dynavContagemresultado_sistemacod1_Internalname ;
      private String edtavContagemresultado_agrupador1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavContagemresultado_demandafm2_Internalname ;
      private String edtavContagemresultado_datadmn2_Internalname ;
      private String edtavContagemresultado_datadmn_to2_Internalname ;
      private String edtavContagemresultado_dataentregareal2_Internalname ;
      private String edtavContagemresultado_dataentregareal_to2_Internalname ;
      private String cmbavContagemresultado_statusdmn2_Internalname ;
      private String cmbavContagemresultado_statuscnt2_Internalname ;
      private String edtavContagemresultado_descricao2_Internalname ;
      private String dynavContagemresultado_sistemacod2_Internalname ;
      private String edtavContagemresultado_agrupador2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavContagemresultado_demandafm3_Internalname ;
      private String edtavContagemresultado_datadmn3_Internalname ;
      private String edtavContagemresultado_datadmn_to3_Internalname ;
      private String edtavContagemresultado_dataentregareal3_Internalname ;
      private String edtavContagemresultado_dataentregareal_to3_Internalname ;
      private String cmbavContagemresultado_statusdmn3_Internalname ;
      private String cmbavContagemresultado_statuscnt3_Internalname ;
      private String edtavContagemresultado_descricao3_Internalname ;
      private String dynavContagemresultado_sistemacod3_Internalname ;
      private String edtavContagemresultado_agrupador3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String lV55ContagemResultado_Agrupador1 ;
      private String lV63ContagemResultado_Agrupador2 ;
      private String lV71ContagemResultado_Agrupador3 ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String tblTablemergeddynamicfilterscontagemresultado_datadmn1_Internalname ;
      private String tblTablemergeddynamicfilterscontagemresultado_dataentregareal1_Internalname ;
      private String cellContagemresultado_dataentregareal_cell1_Class ;
      private String cellContagemresultado_dataentregareal_cell1_Internalname ;
      private String cellContagemresultado_dataentregareal_to_cell1_Class ;
      private String cellContagemresultado_dataentregareal_to_cell1_Internalname ;
      private String lblContagemresultado_dataentregareal_rangemiddletext_11_Class ;
      private String lblContagemresultado_dataentregareal_rangemiddletext_11_Internalname ;
      private String tblTablemergeddynamicfilterscontagemresultado_datadmn2_Internalname ;
      private String tblTablemergeddynamicfilterscontagemresultado_dataentregareal2_Internalname ;
      private String cellContagemresultado_dataentregareal_cell2_Class ;
      private String cellContagemresultado_dataentregareal_cell2_Internalname ;
      private String cellContagemresultado_dataentregareal_to_cell2_Class ;
      private String cellContagemresultado_dataentregareal_to_cell2_Internalname ;
      private String lblContagemresultado_dataentregareal_rangemiddletext_12_Class ;
      private String lblContagemresultado_dataentregareal_rangemiddletext_12_Internalname ;
      private String tblTablemergeddynamicfilterscontagemresultado_datadmn3_Internalname ;
      private String tblTablemergeddynamicfilterscontagemresultado_dataentregareal3_Internalname ;
      private String cellContagemresultado_dataentregareal_cell3_Class ;
      private String cellContagemresultado_dataentregareal_cell3_Internalname ;
      private String cellContagemresultado_dataentregareal_to_cell3_Class ;
      private String cellContagemresultado_dataentregareal_to_cell3_Internalname ;
      private String lblContagemresultado_dataentregareal_rangemiddletext_13_Class ;
      private String lblContagemresultado_dataentregareal_rangemiddletext_13_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String tblTablefilters_Internalname ;
      private String lblFiltertextcontratada_areatrabalhocod_Internalname ;
      private String lblFiltertextcontratada_areatrabalhocod_Jsonclick ;
      private String edtavContratada_areatrabalhocod_Jsonclick ;
      private String tblTablefiltersextras_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavContagemresultado_demandafm3_Jsonclick ;
      private String cmbavContagemresultado_statusdmn3_Jsonclick ;
      private String cmbavContagemresultado_statuscnt3_Jsonclick ;
      private String edtavContagemresultado_descricao3_Jsonclick ;
      private String dynavContagemresultado_sistemacod3_Jsonclick ;
      private String edtavContagemresultado_agrupador3_Jsonclick ;
      private String edtavContagemresultado_dataentregareal3_Jsonclick ;
      private String cellContagemresultado_dataentregareal_rangemiddletext_1_cell3_Internalname ;
      private String lblContagemresultado_dataentregareal_rangemiddletext_13_Jsonclick ;
      private String edtavContagemresultado_dataentregareal_to3_Jsonclick ;
      private String edtavContagemresultado_datadmn3_Jsonclick ;
      private String cellContagemresultado_datadmn_rangemiddletext_1_cell3_Internalname ;
      private String lblContagemresultado_datadmn_rangemiddletext_13_Internalname ;
      private String lblContagemresultado_datadmn_rangemiddletext_13_Jsonclick ;
      private String cellContagemresultado_datadmn_to_cell3_Internalname ;
      private String edtavContagemresultado_datadmn_to3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavContagemresultado_demandafm2_Jsonclick ;
      private String cmbavContagemresultado_statusdmn2_Jsonclick ;
      private String cmbavContagemresultado_statuscnt2_Jsonclick ;
      private String edtavContagemresultado_descricao2_Jsonclick ;
      private String dynavContagemresultado_sistemacod2_Jsonclick ;
      private String edtavContagemresultado_agrupador2_Jsonclick ;
      private String edtavContagemresultado_dataentregareal2_Jsonclick ;
      private String cellContagemresultado_dataentregareal_rangemiddletext_1_cell2_Internalname ;
      private String lblContagemresultado_dataentregareal_rangemiddletext_12_Jsonclick ;
      private String edtavContagemresultado_dataentregareal_to2_Jsonclick ;
      private String edtavContagemresultado_datadmn2_Jsonclick ;
      private String cellContagemresultado_datadmn_rangemiddletext_1_cell2_Internalname ;
      private String lblContagemresultado_datadmn_rangemiddletext_12_Internalname ;
      private String lblContagemresultado_datadmn_rangemiddletext_12_Jsonclick ;
      private String cellContagemresultado_datadmn_to_cell2_Internalname ;
      private String edtavContagemresultado_datadmn_to2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavContagemresultado_demandafm1_Jsonclick ;
      private String cmbavContagemresultado_statusdmn1_Jsonclick ;
      private String cmbavContagemresultado_statuscnt1_Jsonclick ;
      private String edtavContagemresultado_descricao1_Jsonclick ;
      private String dynavContagemresultado_sistemacod1_Jsonclick ;
      private String edtavContagemresultado_agrupador1_Jsonclick ;
      private String edtavContagemresultado_dataentregareal1_Jsonclick ;
      private String cellContagemresultado_dataentregareal_rangemiddletext_1_cell1_Internalname ;
      private String lblContagemresultado_dataentregareal_rangemiddletext_11_Jsonclick ;
      private String edtavContagemresultado_dataentregareal_to1_Jsonclick ;
      private String edtavContagemresultado_datadmn1_Jsonclick ;
      private String cellContagemresultado_datadmn_rangemiddletext_1_cell1_Internalname ;
      private String lblContagemresultado_datadmn_rangemiddletext_11_Internalname ;
      private String lblContagemresultado_datadmn_rangemiddletext_11_Jsonclick ;
      private String cellContagemresultado_datadmn_to_cell1_Internalname ;
      private String edtavContagemresultado_datadmn_to1_Jsonclick ;
      private String tblTablefiltersb_Internalname ;
      private String lblFiltertextcontagemresultado_cntadaosvinc_Internalname ;
      private String lblFiltertextcontagemresultado_cntadaosvinc_Jsonclick ;
      private String dynavContagemresultado_cntadaosvinc_Jsonclick ;
      private String lblFiltertextcontagemresultado_sergrupovinc_Internalname ;
      private String lblFiltertextcontagemresultado_sergrupovinc_Jsonclick ;
      private String dynavContagemresultado_sergrupovinc_Jsonclick ;
      private String lblFiltertextcontagemresultado_codsrvvnc_Internalname ;
      private String lblFiltertextcontagemresultado_codsrvvnc_Jsonclick ;
      private String dynavContagemresultado_codsrvvnc_Jsonclick ;
      private String tblTablefiltersa_Internalname ;
      private String lblFiltertextcontagemresultado_contratadacod_Internalname ;
      private String lblFiltertextcontagemresultado_contratadacod_Jsonclick ;
      private String dynavContagemresultado_contratadacod_Jsonclick ;
      private String lblFiltertextcontagemresultado_servicogrupo_Internalname ;
      private String lblFiltertextcontagemresultado_servicogrupo_Jsonclick ;
      private String dynavContagemresultado_servicogrupo_Jsonclick ;
      private String lblFiltertextcontagemresultado_servico_Internalname ;
      private String lblFiltertextcontagemresultado_servico_Jsonclick ;
      private String dynavContagemresultado_servico_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String lblContagemresultadotitle_Internalname ;
      private String lblContagemresultadotitle_Jsonclick ;
      private String imgExport_Internalname ;
      private String imgExport_Jsonclick ;
      private String sGXsfl_187_fel_idx="0001" ;
      private String ROClassString ;
      private String edtavContagemresultado_codigo_Jsonclick ;
      private String edtavContagemresultado_demandafm_Jsonclick ;
      private String edtavContagemresultado_demanda_Jsonclick ;
      private String edtavContagemresultado_datadmn_Jsonclick ;
      private String edtavContagemresultado_dataentregareal_Jsonclick ;
      private String edtavContagemresultado_descricao_Jsonclick ;
      private String edtavContagemrresultado_sistemasigla_Jsonclick ;
      private String edtavContagemresultado_osvinculada_Jsonclick ;
      private String cmbavContagemresultado_contratadatipofab_Jsonclick ;
      private String edtavContagemresultado_pfbfmultima_Jsonclick ;
      private String edtavContagemresultado_pfbfsultima_Jsonclick ;
      private String edtavEsforco_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV50ContagemResultado_DataEntregaReal_To1 ;
      private DateTime AV49ContagemResultado_DataEntregaReal1 ;
      private DateTime AV58ContagemResultado_DataEntregaReal_To2 ;
      private DateTime AV57ContagemResultado_DataEntregaReal2 ;
      private DateTime AV66ContagemResultado_DataEntregaReal_To3 ;
      private DateTime AV65ContagemResultado_DataEntregaReal3 ;
      private DateTime A2017ContagemResultado_DataEntregaReal ;
      private DateTime AV92ContagemResultado_DataEntregaReal ;
      private DateTime AV24ContagemResultado_DataDmn_To1 ;
      private DateTime AV23ContagemResultado_DataDmn1 ;
      private DateTime AV29ContagemResultado_DataDmn_To2 ;
      private DateTime AV28ContagemResultado_DataDmn2 ;
      private DateTime AV34ContagemResultado_DataDmn_To3 ;
      private DateTime AV33ContagemResultado_DataDmn3 ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime Gx_date ;
      private DateTime AV91ContagemResultado_DataDmn ;
      private bool entryPointCalled ;
      private bool AV25DynamicFiltersEnabled2 ;
      private bool AV30DynamicFiltersEnabled3 ;
      private bool AV36DynamicFiltersIgnoreFirst ;
      private bool AV35DynamicFiltersRemoving ;
      private bool n457ContagemResultado_Demanda ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n764ContagemResultado_ServicoGrupo ;
      private bool n601ContagemResultado_Servico ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n2017ContagemResultado_DataEntregaReal ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n494ContagemResultado_Descricao ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n1046ContagemResultado_Agrupador ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private bool n1326ContagemResultado_ContratadaTipoFab ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool AV47Display_IsBlob ;
      private String AV21DynamicFiltersSelector1 ;
      private String AV26DynamicFiltersSelector2 ;
      private String AV31DynamicFiltersSelector3 ;
      private String AV48ContagemResultado_DemandaFM1 ;
      private String AV53ContagemResultado_Descricao1 ;
      private String AV56ContagemResultado_DemandaFM2 ;
      private String AV61ContagemResultado_Descricao2 ;
      private String AV64ContagemResultado_DemandaFM3 ;
      private String AV69ContagemResultado_Descricao3 ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String A494ContagemResultado_Descricao ;
      private String AV98ExcelFilename ;
      private String AV99ErrorMessage ;
      private String AV189Display_GXI ;
      private String AV89ContagemResultado_DemandaFM ;
      private String AV90ContagemResultado_Demanda ;
      private String AV93ContagemResultado_Descricao ;
      private String lV48ContagemResultado_DemandaFM1 ;
      private String lV53ContagemResultado_Descricao1 ;
      private String lV56ContagemResultado_DemandaFM2 ;
      private String lV61ContagemResultado_Descricao2 ;
      private String lV64ContagemResultado_DemandaFM3 ;
      private String lV69ContagemResultado_Descricao3 ;
      private String AV47Display ;
      private IGxSession AV37Session ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavContagemresultado_contratadacod ;
      private GXCombobox dynavContagemresultado_servicogrupo ;
      private GXCombobox dynavContagemresultado_servico ;
      private GXCombobox dynavContagemresultado_cntadaosvinc ;
      private GXCombobox dynavContagemresultado_sergrupovinc ;
      private GXCombobox dynavContagemresultado_codsrvvnc ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavContagemresultado_statusdmn1 ;
      private GXCombobox cmbavContagemresultado_statuscnt1 ;
      private GXCombobox dynavContagemresultado_sistemacod1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavContagemresultado_statusdmn2 ;
      private GXCombobox cmbavContagemresultado_statuscnt2 ;
      private GXCombobox dynavContagemresultado_sistemacod2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCombobox cmbavContagemresultado_statusdmn3 ;
      private GXCombobox cmbavContagemresultado_statuscnt3 ;
      private GXCombobox dynavContagemresultado_sistemacod3 ;
      private GXCombobox cmbavContagemresultado_contratadatipofab ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H00TR2_A39Contratada_Codigo ;
      private String[] H00TR2_A438Contratada_Sigla ;
      private int[] H00TR2_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00TR2_n52Contratada_AreaTrabalhoCod ;
      private int[] H00TR3_A74Contrato_Codigo ;
      private int[] H00TR3_A155Servico_Codigo ;
      private int[] H00TR3_A160ContratoServicos_Codigo ;
      private int[] H00TR3_A157ServicoGrupo_Codigo ;
      private String[] H00TR3_A158ServicoGrupo_Descricao ;
      private int[] H00TR3_A39Contratada_Codigo ;
      private bool[] H00TR3_A638ContratoServicos_Ativo ;
      private int[] H00TR4_A74Contrato_Codigo ;
      private int[] H00TR4_A160ContratoServicos_Codigo ;
      private int[] H00TR4_A155Servico_Codigo ;
      private String[] H00TR4_A605Servico_Sigla ;
      private int[] H00TR4_A39Contratada_Codigo ;
      private int[] H00TR4_A157ServicoGrupo_Codigo ;
      private bool[] H00TR4_A638ContratoServicos_Ativo ;
      private int[] H00TR5_A39Contratada_Codigo ;
      private String[] H00TR5_A438Contratada_Sigla ;
      private int[] H00TR5_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00TR5_n52Contratada_AreaTrabalhoCod ;
      private int[] H00TR6_A74Contrato_Codigo ;
      private int[] H00TR6_A155Servico_Codigo ;
      private int[] H00TR6_A160ContratoServicos_Codigo ;
      private int[] H00TR6_A157ServicoGrupo_Codigo ;
      private String[] H00TR6_A158ServicoGrupo_Descricao ;
      private int[] H00TR6_A39Contratada_Codigo ;
      private bool[] H00TR6_A638ContratoServicos_Ativo ;
      private int[] H00TR7_A74Contrato_Codigo ;
      private int[] H00TR7_A160ContratoServicos_Codigo ;
      private int[] H00TR7_A155Servico_Codigo ;
      private String[] H00TR7_A605Servico_Sigla ;
      private int[] H00TR7_A39Contratada_Codigo ;
      private int[] H00TR7_A157ServicoGrupo_Codigo ;
      private bool[] H00TR7_A638ContratoServicos_Ativo ;
      private int[] H00TR8_A127Sistema_Codigo ;
      private String[] H00TR8_A129Sistema_Sigla ;
      private int[] H00TR8_A135Sistema_AreaTrabalhoCod ;
      private bool[] H00TR8_A130Sistema_Ativo ;
      private int[] H00TR9_A127Sistema_Codigo ;
      private String[] H00TR9_A129Sistema_Sigla ;
      private int[] H00TR9_A135Sistema_AreaTrabalhoCod ;
      private bool[] H00TR9_A130Sistema_Ativo ;
      private int[] H00TR10_A127Sistema_Codigo ;
      private String[] H00TR10_A129Sistema_Sigla ;
      private int[] H00TR10_A135Sistema_AreaTrabalhoCod ;
      private bool[] H00TR10_A130Sistema_Ativo ;
      private int[] H00TR12_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00TR12_n1553ContagemResultado_CntSrvCod ;
      private String[] H00TR12_A1046ContagemResultado_Agrupador ;
      private bool[] H00TR12_n1046ContagemResultado_Agrupador ;
      private int[] H00TR12_A489ContagemResultado_SistemaCod ;
      private bool[] H00TR12_n489ContagemResultado_SistemaCod ;
      private String[] H00TR12_A494ContagemResultado_Descricao ;
      private bool[] H00TR12_n494ContagemResultado_Descricao ;
      private String[] H00TR12_A484ContagemResultado_StatusDmn ;
      private bool[] H00TR12_n484ContagemResultado_StatusDmn ;
      private DateTime[] H00TR12_A2017ContagemResultado_DataEntregaReal ;
      private bool[] H00TR12_n2017ContagemResultado_DataEntregaReal ;
      private DateTime[] H00TR12_A471ContagemResultado_DataDmn ;
      private String[] H00TR12_A493ContagemResultado_DemandaFM ;
      private bool[] H00TR12_n493ContagemResultado_DemandaFM ;
      private int[] H00TR12_A602ContagemResultado_OSVinculada ;
      private bool[] H00TR12_n602ContagemResultado_OSVinculada ;
      private int[] H00TR12_A601ContagemResultado_Servico ;
      private bool[] H00TR12_n601ContagemResultado_Servico ;
      private int[] H00TR12_A764ContagemResultado_ServicoGrupo ;
      private bool[] H00TR12_n764ContagemResultado_ServicoGrupo ;
      private int[] H00TR12_A490ContagemResultado_ContratadaCod ;
      private bool[] H00TR12_n490ContagemResultado_ContratadaCod ;
      private int[] H00TR12_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00TR12_n52Contratada_AreaTrabalhoCod ;
      private int[] H00TR12_A456ContagemResultado_Codigo ;
      private String[] H00TR12_A509ContagemrResultado_SistemaSigla ;
      private bool[] H00TR12_n509ContagemrResultado_SistemaSigla ;
      private String[] H00TR12_A1326ContagemResultado_ContratadaTipoFab ;
      private bool[] H00TR12_n1326ContagemResultado_ContratadaTipoFab ;
      private String[] H00TR12_A457ContagemResultado_Demanda ;
      private bool[] H00TR12_n457ContagemResultado_Demanda ;
      private decimal[] H00TR12_A682ContagemResultado_PFBFMUltima ;
      private decimal[] H00TR12_A684ContagemResultado_PFBFSUltima ;
      private int[] H00TR14_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00TR14_n1553ContagemResultado_CntSrvCod ;
      private String[] H00TR14_A1046ContagemResultado_Agrupador ;
      private bool[] H00TR14_n1046ContagemResultado_Agrupador ;
      private int[] H00TR14_A489ContagemResultado_SistemaCod ;
      private bool[] H00TR14_n489ContagemResultado_SistemaCod ;
      private String[] H00TR14_A494ContagemResultado_Descricao ;
      private bool[] H00TR14_n494ContagemResultado_Descricao ;
      private String[] H00TR14_A484ContagemResultado_StatusDmn ;
      private bool[] H00TR14_n484ContagemResultado_StatusDmn ;
      private DateTime[] H00TR14_A2017ContagemResultado_DataEntregaReal ;
      private bool[] H00TR14_n2017ContagemResultado_DataEntregaReal ;
      private DateTime[] H00TR14_A471ContagemResultado_DataDmn ;
      private String[] H00TR14_A493ContagemResultado_DemandaFM ;
      private bool[] H00TR14_n493ContagemResultado_DemandaFM ;
      private int[] H00TR14_A456ContagemResultado_Codigo ;
      private int[] H00TR14_A601ContagemResultado_Servico ;
      private bool[] H00TR14_n601ContagemResultado_Servico ;
      private int[] H00TR14_A764ContagemResultado_ServicoGrupo ;
      private bool[] H00TR14_n764ContagemResultado_ServicoGrupo ;
      private int[] H00TR14_A490ContagemResultado_ContratadaCod ;
      private bool[] H00TR14_n490ContagemResultado_ContratadaCod ;
      private int[] H00TR14_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00TR14_n52Contratada_AreaTrabalhoCod ;
      private String[] H00TR14_A509ContagemrResultado_SistemaSigla ;
      private bool[] H00TR14_n509ContagemrResultado_SistemaSigla ;
      private int[] H00TR14_A602ContagemResultado_OSVinculada ;
      private bool[] H00TR14_n602ContagemResultado_OSVinculada ;
      private String[] H00TR14_A1326ContagemResultado_ContratadaTipoFab ;
      private bool[] H00TR14_n1326ContagemResultado_ContratadaTipoFab ;
      private String[] H00TR14_A457ContagemResultado_Demanda ;
      private bool[] H00TR14_n457ContagemResultado_Demanda ;
      private decimal[] H00TR14_A682ContagemResultado_PFBFMUltima ;
      private decimal[] H00TR14_A684ContagemResultado_PFBFSUltima ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
   }

   public class wp_relatoriocomparacaodemandas_old__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00TR12( IGxContext context ,
                                              int AV16ContagemResultado_ServicoGrupo ,
                                              int AV17ContagemResultado_Servico ,
                                              String AV21DynamicFiltersSelector1 ,
                                              short AV22DynamicFiltersOperator1 ,
                                              String AV48ContagemResultado_DemandaFM1 ,
                                              DateTime AV23ContagemResultado_DataDmn1 ,
                                              DateTime AV24ContagemResultado_DataDmn_To1 ,
                                              DateTime AV49ContagemResultado_DataEntregaReal1 ,
                                              DateTime AV50ContagemResultado_DataEntregaReal_To1 ,
                                              String AV51ContagemResultado_StatusDmn1 ,
                                              String AV53ContagemResultado_Descricao1 ,
                                              int AV54ContagemResultado_SistemaCod1 ,
                                              String AV55ContagemResultado_Agrupador1 ,
                                              bool AV25DynamicFiltersEnabled2 ,
                                              String AV26DynamicFiltersSelector2 ,
                                              short AV27DynamicFiltersOperator2 ,
                                              String AV56ContagemResultado_DemandaFM2 ,
                                              DateTime AV28ContagemResultado_DataDmn2 ,
                                              DateTime AV29ContagemResultado_DataDmn_To2 ,
                                              DateTime AV57ContagemResultado_DataEntregaReal2 ,
                                              DateTime AV58ContagemResultado_DataEntregaReal_To2 ,
                                              String AV59ContagemResultado_StatusDmn2 ,
                                              String AV61ContagemResultado_Descricao2 ,
                                              int AV62ContagemResultado_SistemaCod2 ,
                                              String AV63ContagemResultado_Agrupador2 ,
                                              bool AV30DynamicFiltersEnabled3 ,
                                              String AV31DynamicFiltersSelector3 ,
                                              short AV32DynamicFiltersOperator3 ,
                                              String AV64ContagemResultado_DemandaFM3 ,
                                              DateTime AV33ContagemResultado_DataDmn3 ,
                                              DateTime AV34ContagemResultado_DataDmn_To3 ,
                                              DateTime AV65ContagemResultado_DataEntregaReal3 ,
                                              DateTime AV66ContagemResultado_DataEntregaReal_To3 ,
                                              String AV67ContagemResultado_StatusDmn3 ,
                                              String AV69ContagemResultado_Descricao3 ,
                                              int AV70ContagemResultado_SistemaCod3 ,
                                              String AV71ContagemResultado_Agrupador3 ,
                                              int A764ContagemResultado_ServicoGrupo ,
                                              int A601ContagemResultado_Servico ,
                                              String A493ContagemResultado_DemandaFM ,
                                              DateTime A471ContagemResultado_DataDmn ,
                                              DateTime A2017ContagemResultado_DataEntregaReal ,
                                              String A484ContagemResultado_StatusDmn ,
                                              String A494ContagemResultado_Descricao ,
                                              int A489ContagemResultado_SistemaCod ,
                                              String A1046ContagemResultado_Agrupador ,
                                              int A602ContagemResultado_OSVinculada ,
                                              int A52Contratada_AreaTrabalhoCod ,
                                              int AV6WWPContext_gxTpr_Areatrabalho_codigo ,
                                              int A490ContagemResultado_ContratadaCod ,
                                              int AV15ContagemResultado_ContratadaCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [61] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Agrupador], T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_Descricao], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DataEntregaReal], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_OSVinculada], T2.[Servico_Codigo] AS ContagemResultado_Servico, T3.[ServicoGrupo_Codigo] AS ContagemResultado_ServicoGrupo, T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T5.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_Codigo], T4.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T5.[Contratada_TipoFabrica] AS ContagemResultado_ContratadaTipoFab, T1.[ContagemResultado_Demanda], COALESCE( T6.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima, COALESCE( T6.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima FROM ((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN [Sistema] T4 WITH (NOLOCK) ON T4.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [Contratada] T5 WITH (NOLOCK) ON T5.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN (SELECT MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, [ContagemResultado_Codigo], MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T6 ON T6.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo])";
         scmdbuf = scmdbuf + " WHERE (Not (T1.[ContagemResultado_OSVinculada] = convert(int, 0)))";
         scmdbuf = scmdbuf + " and (T5.[Contratada_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_ContratadaCod] = @AV15ContagemResultado_ContratadaCod)";
         if ( ! (0==AV16ContagemResultado_ServicoGrupo) )
         {
            sWhereString = sWhereString + " and (T3.[ServicoGrupo_Codigo] = @AV16ContagemResultado_ServicoGrupo)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ! (0==AV17ContagemResultado_Servico) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV17ContagemResultado_Servico)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV22DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ContagemResultado_DemandaFM1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] = @AV48ContagemResultado_DemandaFM1)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV22DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ContagemResultado_DemandaFM1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like @lV48ContagemResultado_DemandaFM1)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV22DynamicFiltersOperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ContagemResultado_DemandaFM1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like '%' + @lV48ContagemResultado_DemandaFM1)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( AV22DynamicFiltersOperator1 == 0 ) && ( ! (DateTime.MinValue==AV23ContagemResultado_DataDmn1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV23ContagemResultado_DataDmn1)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( AV22DynamicFiltersOperator1 == 0 ) && ( ! (DateTime.MinValue==AV24ContagemResultado_DataDmn_To1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV24ContagemResultado_DataDmn_To1)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV22DynamicFiltersOperator1 == 0 ) && ( ! (DateTime.MinValue==AV49ContagemResultado_DataEntregaReal1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntregaReal] < @AV49ContagemResultado_DataEntregaReal1)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( ( AV22DynamicFiltersOperator1 == 1 ) || ( AV22DynamicFiltersOperator1 == 2 ) || ( AV22DynamicFiltersOperator1 == 3 ) ) && ( ! (DateTime.MinValue==AV49ContagemResultado_DataEntregaReal1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntregaReal] >= @AV49ContagemResultado_DataEntregaReal1)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV22DynamicFiltersOperator1 == 1 ) && ( ! (DateTime.MinValue==AV50ContagemResultado_DataEntregaReal_To1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntregaReal] < @AV50ContagemResultado_DataEntregaReal_To1)";
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV22DynamicFiltersOperator1 == 3 ) && ( ! (DateTime.MinValue==AV50ContagemResultado_DataEntregaReal_To1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntregaReal] <= @AV50ContagemResultado_DataEntregaReal_To1)";
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51ContagemResultado_StatusDmn1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV51ContagemResultado_StatusDmn1)";
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV22DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ContagemResultado_Descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like @lV53ContagemResultado_Descricao1)";
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV22DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ContagemResultado_Descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV53ContagemResultado_Descricao1)";
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV22DynamicFiltersOperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ContagemResultado_Descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] = @AV53ContagemResultado_Descricao1)";
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( AV22DynamicFiltersOperator1 == 0 ) && ( ! (0==AV54ContagemResultado_SistemaCod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] < @AV54ContagemResultado_SistemaCod1)";
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( AV22DynamicFiltersOperator1 == 1 ) && ( ! (0==AV54ContagemResultado_SistemaCod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV54ContagemResultado_SistemaCod1)";
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( AV22DynamicFiltersOperator1 == 2 ) && ( ! (0==AV54ContagemResultado_SistemaCod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] > @AV54ContagemResultado_SistemaCod1)";
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV22DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55ContagemResultado_Agrupador1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV55ContagemResultado_Agrupador1)";
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV22DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55ContagemResultado_Agrupador1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like @lV55ContagemResultado_Agrupador1)";
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV22DynamicFiltersOperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55ContagemResultado_Agrupador1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like '%' + @lV55ContagemResultado_Agrupador1)";
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( AV25DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV27DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContagemResultado_DemandaFM2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] = @AV56ContagemResultado_DemandaFM2)";
         }
         else
         {
            GXv_int1[23] = 1;
         }
         if ( AV25DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV27DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContagemResultado_DemandaFM2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like @lV56ContagemResultado_DemandaFM2)";
         }
         else
         {
            GXv_int1[24] = 1;
         }
         if ( AV25DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV27DynamicFiltersOperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContagemResultado_DemandaFM2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like '%' + @lV56ContagemResultado_DemandaFM2)";
         }
         else
         {
            GXv_int1[25] = 1;
         }
         if ( AV25DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( AV27DynamicFiltersOperator2 == 0 ) && ( ! (DateTime.MinValue==AV28ContagemResultado_DataDmn2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV28ContagemResultado_DataDmn2)";
         }
         else
         {
            GXv_int1[26] = 1;
         }
         if ( AV25DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( AV27DynamicFiltersOperator2 == 0 ) && ( ! (DateTime.MinValue==AV29ContagemResultado_DataDmn_To2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV29ContagemResultado_DataDmn_To2)";
         }
         else
         {
            GXv_int1[27] = 1;
         }
         if ( AV25DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV27DynamicFiltersOperator2 == 0 ) && ( ! (DateTime.MinValue==AV57ContagemResultado_DataEntregaReal2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntregaReal] < @AV57ContagemResultado_DataEntregaReal2)";
         }
         else
         {
            GXv_int1[28] = 1;
         }
         if ( AV25DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( ( AV27DynamicFiltersOperator2 == 1 ) || ( AV27DynamicFiltersOperator2 == 2 ) || ( AV27DynamicFiltersOperator2 == 3 ) ) && ( ! (DateTime.MinValue==AV57ContagemResultado_DataEntregaReal2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntregaReal] >= @AV57ContagemResultado_DataEntregaReal2)";
         }
         else
         {
            GXv_int1[29] = 1;
         }
         if ( AV25DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV27DynamicFiltersOperator2 == 1 ) && ( ! (DateTime.MinValue==AV58ContagemResultado_DataEntregaReal_To2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntregaReal] < @AV58ContagemResultado_DataEntregaReal_To2)";
         }
         else
         {
            GXv_int1[30] = 1;
         }
         if ( AV25DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV27DynamicFiltersOperator2 == 3 ) && ( ! (DateTime.MinValue==AV58ContagemResultado_DataEntregaReal_To2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntregaReal] <= @AV58ContagemResultado_DataEntregaReal_To2)";
         }
         else
         {
            GXv_int1[31] = 1;
         }
         if ( AV25DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59ContagemResultado_StatusDmn2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV59ContagemResultado_StatusDmn2)";
         }
         else
         {
            GXv_int1[32] = 1;
         }
         if ( AV25DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV27DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61ContagemResultado_Descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like @lV61ContagemResultado_Descricao2)";
         }
         else
         {
            GXv_int1[33] = 1;
         }
         if ( AV25DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV27DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61ContagemResultado_Descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV61ContagemResultado_Descricao2)";
         }
         else
         {
            GXv_int1[34] = 1;
         }
         if ( AV25DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV27DynamicFiltersOperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61ContagemResultado_Descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] = @AV61ContagemResultado_Descricao2)";
         }
         else
         {
            GXv_int1[35] = 1;
         }
         if ( AV25DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( AV27DynamicFiltersOperator2 == 0 ) && ( ! (0==AV62ContagemResultado_SistemaCod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] < @AV62ContagemResultado_SistemaCod2)";
         }
         else
         {
            GXv_int1[36] = 1;
         }
         if ( AV25DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( AV27DynamicFiltersOperator2 == 1 ) && ( ! (0==AV62ContagemResultado_SistemaCod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV62ContagemResultado_SistemaCod2)";
         }
         else
         {
            GXv_int1[37] = 1;
         }
         if ( AV25DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( AV27DynamicFiltersOperator2 == 2 ) && ( ! (0==AV62ContagemResultado_SistemaCod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] > @AV62ContagemResultado_SistemaCod2)";
         }
         else
         {
            GXv_int1[38] = 1;
         }
         if ( AV25DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV27DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63ContagemResultado_Agrupador2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV63ContagemResultado_Agrupador2)";
         }
         else
         {
            GXv_int1[39] = 1;
         }
         if ( AV25DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV27DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63ContagemResultado_Agrupador2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like @lV63ContagemResultado_Agrupador2)";
         }
         else
         {
            GXv_int1[40] = 1;
         }
         if ( AV25DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV27DynamicFiltersOperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63ContagemResultado_Agrupador2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like '%' + @lV63ContagemResultado_Agrupador2)";
         }
         else
         {
            GXv_int1[41] = 1;
         }
         if ( AV30DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV32DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64ContagemResultado_DemandaFM3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] = @AV64ContagemResultado_DemandaFM3)";
         }
         else
         {
            GXv_int1[42] = 1;
         }
         if ( AV30DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV32DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64ContagemResultado_DemandaFM3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like @lV64ContagemResultado_DemandaFM3)";
         }
         else
         {
            GXv_int1[43] = 1;
         }
         if ( AV30DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV32DynamicFiltersOperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64ContagemResultado_DemandaFM3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like '%' + @lV64ContagemResultado_DemandaFM3)";
         }
         else
         {
            GXv_int1[44] = 1;
         }
         if ( AV30DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( AV32DynamicFiltersOperator3 == 0 ) && ( ! (DateTime.MinValue==AV33ContagemResultado_DataDmn3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV33ContagemResultado_DataDmn3)";
         }
         else
         {
            GXv_int1[45] = 1;
         }
         if ( AV30DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( AV32DynamicFiltersOperator3 == 0 ) && ( ! (DateTime.MinValue==AV34ContagemResultado_DataDmn_To3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV34ContagemResultado_DataDmn_To3)";
         }
         else
         {
            GXv_int1[46] = 1;
         }
         if ( AV30DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV32DynamicFiltersOperator3 == 0 ) && ( ! (DateTime.MinValue==AV65ContagemResultado_DataEntregaReal3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntregaReal] < @AV65ContagemResultado_DataEntregaReal3)";
         }
         else
         {
            GXv_int1[47] = 1;
         }
         if ( AV30DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( ( AV32DynamicFiltersOperator3 == 1 ) || ( AV32DynamicFiltersOperator3 == 2 ) || ( AV32DynamicFiltersOperator3 == 3 ) ) && ( ! (DateTime.MinValue==AV65ContagemResultado_DataEntregaReal3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntregaReal] >= @AV65ContagemResultado_DataEntregaReal3)";
         }
         else
         {
            GXv_int1[48] = 1;
         }
         if ( AV30DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV32DynamicFiltersOperator3 == 1 ) && ( ! (DateTime.MinValue==AV66ContagemResultado_DataEntregaReal_To3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntregaReal] < @AV66ContagemResultado_DataEntregaReal_To3)";
         }
         else
         {
            GXv_int1[49] = 1;
         }
         if ( AV30DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV32DynamicFiltersOperator3 == 3 ) && ( ! (DateTime.MinValue==AV66ContagemResultado_DataEntregaReal_To3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntregaReal] <= @AV66ContagemResultado_DataEntregaReal_To3)";
         }
         else
         {
            GXv_int1[50] = 1;
         }
         if ( AV30DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67ContagemResultado_StatusDmn3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV67ContagemResultado_StatusDmn3)";
         }
         else
         {
            GXv_int1[51] = 1;
         }
         if ( AV30DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV32DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69ContagemResultado_Descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like @lV69ContagemResultado_Descricao3)";
         }
         else
         {
            GXv_int1[52] = 1;
         }
         if ( AV30DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV32DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69ContagemResultado_Descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV69ContagemResultado_Descricao3)";
         }
         else
         {
            GXv_int1[53] = 1;
         }
         if ( AV30DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV32DynamicFiltersOperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69ContagemResultado_Descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] = @AV69ContagemResultado_Descricao3)";
         }
         else
         {
            GXv_int1[54] = 1;
         }
         if ( AV30DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( AV32DynamicFiltersOperator3 == 0 ) && ( ! (0==AV70ContagemResultado_SistemaCod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] < @AV70ContagemResultado_SistemaCod3)";
         }
         else
         {
            GXv_int1[55] = 1;
         }
         if ( AV30DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( AV32DynamicFiltersOperator3 == 1 ) && ( ! (0==AV70ContagemResultado_SistemaCod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV70ContagemResultado_SistemaCod3)";
         }
         else
         {
            GXv_int1[56] = 1;
         }
         if ( AV30DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( AV32DynamicFiltersOperator3 == 2 ) && ( ! (0==AV70ContagemResultado_SistemaCod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] > @AV70ContagemResultado_SistemaCod3)";
         }
         else
         {
            GXv_int1[57] = 1;
         }
         if ( AV30DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV32DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71ContagemResultado_Agrupador3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV71ContagemResultado_Agrupador3)";
         }
         else
         {
            GXv_int1[58] = 1;
         }
         if ( AV30DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV32DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71ContagemResultado_Agrupador3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like @lV71ContagemResultado_Agrupador3)";
         }
         else
         {
            GXv_int1[59] = 1;
         }
         if ( AV30DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV32DynamicFiltersOperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71ContagemResultado_Agrupador3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like '%' + @lV71ContagemResultado_Agrupador3)";
         }
         else
         {
            GXv_int1[60] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Demanda]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_H00TR14( IGxContext context ,
                                              int AV18ContagemResultado_CntadaOsVinc ,
                                              int AV19ContagemResultado_SerGrupoVinc ,
                                              int AV20ContagemResultado_CodSrvVnc ,
                                              String AV21DynamicFiltersSelector1 ,
                                              short AV22DynamicFiltersOperator1 ,
                                              String AV48ContagemResultado_DemandaFM1 ,
                                              DateTime AV23ContagemResultado_DataDmn1 ,
                                              DateTime AV24ContagemResultado_DataDmn_To1 ,
                                              DateTime AV49ContagemResultado_DataEntregaReal1 ,
                                              DateTime AV50ContagemResultado_DataEntregaReal_To1 ,
                                              String AV51ContagemResultado_StatusDmn1 ,
                                              String AV53ContagemResultado_Descricao1 ,
                                              int AV54ContagemResultado_SistemaCod1 ,
                                              String AV55ContagemResultado_Agrupador1 ,
                                              bool AV25DynamicFiltersEnabled2 ,
                                              String AV26DynamicFiltersSelector2 ,
                                              short AV27DynamicFiltersOperator2 ,
                                              String AV56ContagemResultado_DemandaFM2 ,
                                              DateTime AV28ContagemResultado_DataDmn2 ,
                                              DateTime AV29ContagemResultado_DataDmn_To2 ,
                                              DateTime AV57ContagemResultado_DataEntregaReal2 ,
                                              DateTime AV58ContagemResultado_DataEntregaReal_To2 ,
                                              String AV59ContagemResultado_StatusDmn2 ,
                                              String AV61ContagemResultado_Descricao2 ,
                                              int AV62ContagemResultado_SistemaCod2 ,
                                              String AV63ContagemResultado_Agrupador2 ,
                                              bool AV30DynamicFiltersEnabled3 ,
                                              String AV31DynamicFiltersSelector3 ,
                                              short AV32DynamicFiltersOperator3 ,
                                              String AV64ContagemResultado_DemandaFM3 ,
                                              DateTime AV33ContagemResultado_DataDmn3 ,
                                              DateTime AV34ContagemResultado_DataDmn_To3 ,
                                              DateTime AV65ContagemResultado_DataEntregaReal3 ,
                                              DateTime AV66ContagemResultado_DataEntregaReal_To3 ,
                                              String AV67ContagemResultado_StatusDmn3 ,
                                              String AV69ContagemResultado_Descricao3 ,
                                              int AV70ContagemResultado_SistemaCod3 ,
                                              String AV71ContagemResultado_Agrupador3 ,
                                              int A490ContagemResultado_ContratadaCod ,
                                              int A764ContagemResultado_ServicoGrupo ,
                                              int AV16ContagemResultado_ServicoGrupo ,
                                              int A601ContagemResultado_Servico ,
                                              int AV17ContagemResultado_Servico ,
                                              String A493ContagemResultado_DemandaFM ,
                                              DateTime A471ContagemResultado_DataDmn ,
                                              DateTime A2017ContagemResultado_DataEntregaReal ,
                                              String A484ContagemResultado_StatusDmn ,
                                              String A494ContagemResultado_Descricao ,
                                              int A489ContagemResultado_SistemaCod ,
                                              String A1046ContagemResultado_Agrupador ,
                                              int A52Contratada_AreaTrabalhoCod ,
                                              int AV6WWPContext_gxTpr_Areatrabalho_codigo ,
                                              int A456ContagemResultado_Codigo ,
                                              int AV14ContagemResultado_OSVinculada )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [62] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Agrupador], T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_Descricao], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DataEntregaReal], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Codigo], T2.[Servico_Codigo] AS ContagemResultado_Servico, T3.[ServicoGrupo_Codigo] AS ContagemResultado_ServicoGrupo, T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T6.[Contratada_AreaTrabalhoCod], T4.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T1.[ContagemResultado_OSVinculada], T6.[Contratada_TipoFabrica] AS ContagemResultado_ContratadaTipoFab, T1.[ContagemResultado_Demanda], COALESCE( T5.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima, COALESCE( T5.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima FROM ((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN [Sistema] T4 WITH (NOLOCK) ON T4.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN (SELECT MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, [ContagemResultado_Codigo], MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T5 ON T5.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Contratada] T6 WITH (NOLOCK) ON T6.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod])";
         scmdbuf = scmdbuf + " WHERE (T6.[Contratada_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_Codigo] = @AV14ContagemResultado_OSVinculada)";
         if ( ! (0==AV18ContagemResultado_CntadaOsVinc) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV18ContagemResultado_CntadaOsVinc)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ! (0==AV19ContagemResultado_SerGrupoVinc) )
         {
            sWhereString = sWhereString + " and (T3.[ServicoGrupo_Codigo] = @AV16ContagemResultado_ServicoGrupo)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ! (0==AV20ContagemResultado_CodSrvVnc) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV17ContagemResultado_Servico)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV22DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ContagemResultado_DemandaFM1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] = @AV48ContagemResultado_DemandaFM1)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV22DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ContagemResultado_DemandaFM1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like @lV48ContagemResultado_DemandaFM1)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV22DynamicFiltersOperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ContagemResultado_DemandaFM1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like '%' + @lV48ContagemResultado_DemandaFM1)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( AV22DynamicFiltersOperator1 == 0 ) && ( ! (DateTime.MinValue==AV23ContagemResultado_DataDmn1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV23ContagemResultado_DataDmn1)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( AV22DynamicFiltersOperator1 == 0 ) && ( ! (DateTime.MinValue==AV24ContagemResultado_DataDmn_To1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV24ContagemResultado_DataDmn_To1)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV22DynamicFiltersOperator1 == 0 ) && ( ! (DateTime.MinValue==AV49ContagemResultado_DataEntregaReal1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntregaReal] < @AV49ContagemResultado_DataEntregaReal1)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( ( AV22DynamicFiltersOperator1 == 1 ) || ( AV22DynamicFiltersOperator1 == 2 ) || ( AV22DynamicFiltersOperator1 == 3 ) ) && ( ! (DateTime.MinValue==AV49ContagemResultado_DataEntregaReal1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntregaReal] >= @AV49ContagemResultado_DataEntregaReal1)";
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV22DynamicFiltersOperator1 == 1 ) && ( ! (DateTime.MinValue==AV50ContagemResultado_DataEntregaReal_To1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntregaReal] < @AV50ContagemResultado_DataEntregaReal_To1)";
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV22DynamicFiltersOperator1 == 3 ) && ( ! (DateTime.MinValue==AV50ContagemResultado_DataEntregaReal_To1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntregaReal] <= @AV50ContagemResultado_DataEntregaReal_To1)";
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51ContagemResultado_StatusDmn1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV51ContagemResultado_StatusDmn1)";
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV22DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ContagemResultado_Descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like @lV53ContagemResultado_Descricao1)";
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV22DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ContagemResultado_Descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV53ContagemResultado_Descricao1)";
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV22DynamicFiltersOperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ContagemResultado_Descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] = @AV53ContagemResultado_Descricao1)";
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( AV22DynamicFiltersOperator1 == 0 ) && ( ! (0==AV54ContagemResultado_SistemaCod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] < @AV54ContagemResultado_SistemaCod1)";
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( AV22DynamicFiltersOperator1 == 1 ) && ( ! (0==AV54ContagemResultado_SistemaCod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV54ContagemResultado_SistemaCod1)";
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( AV22DynamicFiltersOperator1 == 2 ) && ( ! (0==AV54ContagemResultado_SistemaCod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] > @AV54ContagemResultado_SistemaCod1)";
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV22DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55ContagemResultado_Agrupador1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV55ContagemResultado_Agrupador1)";
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV22DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55ContagemResultado_Agrupador1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like @lV55ContagemResultado_Agrupador1)";
         }
         else
         {
            GXv_int3[22] = 1;
         }
         if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV22DynamicFiltersOperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55ContagemResultado_Agrupador1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like '%' + @lV55ContagemResultado_Agrupador1)";
         }
         else
         {
            GXv_int3[23] = 1;
         }
         if ( AV25DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV27DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContagemResultado_DemandaFM2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] = @AV56ContagemResultado_DemandaFM2)";
         }
         else
         {
            GXv_int3[24] = 1;
         }
         if ( AV25DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV27DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContagemResultado_DemandaFM2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like @lV56ContagemResultado_DemandaFM2)";
         }
         else
         {
            GXv_int3[25] = 1;
         }
         if ( AV25DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV27DynamicFiltersOperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContagemResultado_DemandaFM2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like '%' + @lV56ContagemResultado_DemandaFM2)";
         }
         else
         {
            GXv_int3[26] = 1;
         }
         if ( AV25DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( AV27DynamicFiltersOperator2 == 0 ) && ( ! (DateTime.MinValue==AV28ContagemResultado_DataDmn2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV28ContagemResultado_DataDmn2)";
         }
         else
         {
            GXv_int3[27] = 1;
         }
         if ( AV25DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( AV27DynamicFiltersOperator2 == 0 ) && ( ! (DateTime.MinValue==AV29ContagemResultado_DataDmn_To2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV29ContagemResultado_DataDmn_To2)";
         }
         else
         {
            GXv_int3[28] = 1;
         }
         if ( AV25DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV27DynamicFiltersOperator2 == 0 ) && ( ! (DateTime.MinValue==AV57ContagemResultado_DataEntregaReal2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntregaReal] < @AV57ContagemResultado_DataEntregaReal2)";
         }
         else
         {
            GXv_int3[29] = 1;
         }
         if ( AV25DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( ( AV27DynamicFiltersOperator2 == 1 ) || ( AV27DynamicFiltersOperator2 == 2 ) || ( AV27DynamicFiltersOperator2 == 3 ) ) && ( ! (DateTime.MinValue==AV57ContagemResultado_DataEntregaReal2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntregaReal] >= @AV57ContagemResultado_DataEntregaReal2)";
         }
         else
         {
            GXv_int3[30] = 1;
         }
         if ( AV25DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV27DynamicFiltersOperator2 == 1 ) && ( ! (DateTime.MinValue==AV58ContagemResultado_DataEntregaReal_To2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntregaReal] < @AV58ContagemResultado_DataEntregaReal_To2)";
         }
         else
         {
            GXv_int3[31] = 1;
         }
         if ( AV25DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV27DynamicFiltersOperator2 == 3 ) && ( ! (DateTime.MinValue==AV58ContagemResultado_DataEntregaReal_To2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntregaReal] <= @AV58ContagemResultado_DataEntregaReal_To2)";
         }
         else
         {
            GXv_int3[32] = 1;
         }
         if ( AV25DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59ContagemResultado_StatusDmn2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV59ContagemResultado_StatusDmn2)";
         }
         else
         {
            GXv_int3[33] = 1;
         }
         if ( AV25DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV27DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61ContagemResultado_Descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like @lV61ContagemResultado_Descricao2)";
         }
         else
         {
            GXv_int3[34] = 1;
         }
         if ( AV25DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV27DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61ContagemResultado_Descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV61ContagemResultado_Descricao2)";
         }
         else
         {
            GXv_int3[35] = 1;
         }
         if ( AV25DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV27DynamicFiltersOperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61ContagemResultado_Descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] = @AV61ContagemResultado_Descricao2)";
         }
         else
         {
            GXv_int3[36] = 1;
         }
         if ( AV25DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( AV27DynamicFiltersOperator2 == 0 ) && ( ! (0==AV62ContagemResultado_SistemaCod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] < @AV62ContagemResultado_SistemaCod2)";
         }
         else
         {
            GXv_int3[37] = 1;
         }
         if ( AV25DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( AV27DynamicFiltersOperator2 == 1 ) && ( ! (0==AV62ContagemResultado_SistemaCod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV62ContagemResultado_SistemaCod2)";
         }
         else
         {
            GXv_int3[38] = 1;
         }
         if ( AV25DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( AV27DynamicFiltersOperator2 == 2 ) && ( ! (0==AV62ContagemResultado_SistemaCod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] > @AV62ContagemResultado_SistemaCod2)";
         }
         else
         {
            GXv_int3[39] = 1;
         }
         if ( AV25DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV27DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63ContagemResultado_Agrupador2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV63ContagemResultado_Agrupador2)";
         }
         else
         {
            GXv_int3[40] = 1;
         }
         if ( AV25DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV27DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63ContagemResultado_Agrupador2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like @lV63ContagemResultado_Agrupador2)";
         }
         else
         {
            GXv_int3[41] = 1;
         }
         if ( AV25DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV27DynamicFiltersOperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63ContagemResultado_Agrupador2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like '%' + @lV63ContagemResultado_Agrupador2)";
         }
         else
         {
            GXv_int3[42] = 1;
         }
         if ( AV30DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV32DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64ContagemResultado_DemandaFM3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] = @AV64ContagemResultado_DemandaFM3)";
         }
         else
         {
            GXv_int3[43] = 1;
         }
         if ( AV30DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV32DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64ContagemResultado_DemandaFM3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like @lV64ContagemResultado_DemandaFM3)";
         }
         else
         {
            GXv_int3[44] = 1;
         }
         if ( AV30DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV32DynamicFiltersOperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64ContagemResultado_DemandaFM3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like '%' + @lV64ContagemResultado_DemandaFM3)";
         }
         else
         {
            GXv_int3[45] = 1;
         }
         if ( AV30DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( AV32DynamicFiltersOperator3 == 0 ) && ( ! (DateTime.MinValue==AV33ContagemResultado_DataDmn3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV33ContagemResultado_DataDmn3)";
         }
         else
         {
            GXv_int3[46] = 1;
         }
         if ( AV30DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( AV32DynamicFiltersOperator3 == 0 ) && ( ! (DateTime.MinValue==AV34ContagemResultado_DataDmn_To3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV34ContagemResultado_DataDmn_To3)";
         }
         else
         {
            GXv_int3[47] = 1;
         }
         if ( AV30DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV32DynamicFiltersOperator3 == 0 ) && ( ! (DateTime.MinValue==AV65ContagemResultado_DataEntregaReal3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntregaReal] < @AV65ContagemResultado_DataEntregaReal3)";
         }
         else
         {
            GXv_int3[48] = 1;
         }
         if ( AV30DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( ( AV32DynamicFiltersOperator3 == 1 ) || ( AV32DynamicFiltersOperator3 == 2 ) || ( AV32DynamicFiltersOperator3 == 3 ) ) && ( ! (DateTime.MinValue==AV65ContagemResultado_DataEntregaReal3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntregaReal] >= @AV65ContagemResultado_DataEntregaReal3)";
         }
         else
         {
            GXv_int3[49] = 1;
         }
         if ( AV30DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV32DynamicFiltersOperator3 == 1 ) && ( ! (DateTime.MinValue==AV66ContagemResultado_DataEntregaReal_To3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntregaReal] < @AV66ContagemResultado_DataEntregaReal_To3)";
         }
         else
         {
            GXv_int3[50] = 1;
         }
         if ( AV30DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV32DynamicFiltersOperator3 == 3 ) && ( ! (DateTime.MinValue==AV66ContagemResultado_DataEntregaReal_To3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntregaReal] <= @AV66ContagemResultado_DataEntregaReal_To3)";
         }
         else
         {
            GXv_int3[51] = 1;
         }
         if ( AV30DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67ContagemResultado_StatusDmn3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV67ContagemResultado_StatusDmn3)";
         }
         else
         {
            GXv_int3[52] = 1;
         }
         if ( AV30DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV32DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69ContagemResultado_Descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like @lV69ContagemResultado_Descricao3)";
         }
         else
         {
            GXv_int3[53] = 1;
         }
         if ( AV30DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV32DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69ContagemResultado_Descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV69ContagemResultado_Descricao3)";
         }
         else
         {
            GXv_int3[54] = 1;
         }
         if ( AV30DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV32DynamicFiltersOperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69ContagemResultado_Descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] = @AV69ContagemResultado_Descricao3)";
         }
         else
         {
            GXv_int3[55] = 1;
         }
         if ( AV30DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( AV32DynamicFiltersOperator3 == 0 ) && ( ! (0==AV70ContagemResultado_SistemaCod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] < @AV70ContagemResultado_SistemaCod3)";
         }
         else
         {
            GXv_int3[56] = 1;
         }
         if ( AV30DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( AV32DynamicFiltersOperator3 == 1 ) && ( ! (0==AV70ContagemResultado_SistemaCod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV70ContagemResultado_SistemaCod3)";
         }
         else
         {
            GXv_int3[57] = 1;
         }
         if ( AV30DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( AV32DynamicFiltersOperator3 == 2 ) && ( ! (0==AV70ContagemResultado_SistemaCod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] > @AV70ContagemResultado_SistemaCod3)";
         }
         else
         {
            GXv_int3[58] = 1;
         }
         if ( AV30DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV32DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71ContagemResultado_Agrupador3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV71ContagemResultado_Agrupador3)";
         }
         else
         {
            GXv_int3[59] = 1;
         }
         if ( AV30DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV32DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71ContagemResultado_Agrupador3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like @lV71ContagemResultado_Agrupador3)";
         }
         else
         {
            GXv_int3[60] = 1;
         }
         if ( AV30DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV32DynamicFiltersOperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71ContagemResultado_Agrupador3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like '%' + @lV71ContagemResultado_Agrupador3)";
         }
         else
         {
            GXv_int3[61] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Demanda]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 9 :
                     return conditional_H00TR12(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (String)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (String)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] , (String)dynConstraints[24] , (bool)dynConstraints[25] , (String)dynConstraints[26] , (short)dynConstraints[27] , (String)dynConstraints[28] , (DateTime)dynConstraints[29] , (DateTime)dynConstraints[30] , (DateTime)dynConstraints[31] , (DateTime)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (int)dynConstraints[35] , (String)dynConstraints[36] , (int)dynConstraints[37] , (int)dynConstraints[38] , (String)dynConstraints[39] , (DateTime)dynConstraints[40] , (DateTime)dynConstraints[41] , (String)dynConstraints[42] , (String)dynConstraints[43] , (int)dynConstraints[44] , (String)dynConstraints[45] , (int)dynConstraints[46] , (int)dynConstraints[47] , (int)dynConstraints[48] , (int)dynConstraints[49] , (int)dynConstraints[50] );
               case 10 :
                     return conditional_H00TR14(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (String)dynConstraints[3] , (short)dynConstraints[4] , (String)dynConstraints[5] , (DateTime)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (bool)dynConstraints[14] , (String)dynConstraints[15] , (short)dynConstraints[16] , (String)dynConstraints[17] , (DateTime)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (DateTime)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (int)dynConstraints[24] , (String)dynConstraints[25] , (bool)dynConstraints[26] , (String)dynConstraints[27] , (short)dynConstraints[28] , (String)dynConstraints[29] , (DateTime)dynConstraints[30] , (DateTime)dynConstraints[31] , (DateTime)dynConstraints[32] , (DateTime)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (int)dynConstraints[36] , (String)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] , (int)dynConstraints[40] , (int)dynConstraints[41] , (int)dynConstraints[42] , (String)dynConstraints[43] , (DateTime)dynConstraints[44] , (DateTime)dynConstraints[45] , (String)dynConstraints[46] , (String)dynConstraints[47] , (int)dynConstraints[48] , (String)dynConstraints[49] , (int)dynConstraints[50] , (int)dynConstraints[51] , (int)dynConstraints[52] , (int)dynConstraints[53] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00TR2 ;
          prmH00TR2 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00TR3 ;
          prmH00TR3 = new Object[] {
          new Object[] {"@AV15ContagemResultado_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00TR4 ;
          prmH00TR4 = new Object[] {
          new Object[] {"@AV15ContagemResultado_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV16ContagemResultado_ServicoGrupo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00TR5 ;
          prmH00TR5 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00TR6 ;
          prmH00TR6 = new Object[] {
          new Object[] {"@AV18ContagemResultado_CntadaOsVinc",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00TR7 ;
          prmH00TR7 = new Object[] {
          new Object[] {"@AV18ContagemResultado_CntadaOsVinc",SqlDbType.Int,6,0} ,
          new Object[] {"@AV19ContagemResultado_SerGrupoVinc",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00TR8 ;
          prmH00TR8 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00TR9 ;
          prmH00TR9 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00TR10 ;
          prmH00TR10 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00TR12 ;
          prmH00TR12 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV15ContagemResultado_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV16ContagemResultado_ServicoGrupo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17ContagemResultado_Servico",SqlDbType.Int,6,0} ,
          new Object[] {"@AV48ContagemResultado_DemandaFM1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV48ContagemResultado_DemandaFM1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV48ContagemResultado_DemandaFM1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV23ContagemResultado_DataDmn1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV24ContagemResultado_DataDmn_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV49ContagemResultado_DataEntregaReal1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV49ContagemResultado_DataEntregaReal1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV50ContagemResultado_DataEntregaReal_To1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV50ContagemResultado_DataEntregaReal_To1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV51ContagemResultado_StatusDmn1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV53ContagemResultado_Descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV53ContagemResultado_Descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV53ContagemResultado_Descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV54ContagemResultado_SistemaCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV54ContagemResultado_SistemaCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV54ContagemResultado_SistemaCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV55ContagemResultado_Agrupador1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV55ContagemResultado_Agrupador1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV55ContagemResultado_Agrupador1",SqlDbType.Char,15,0} ,
          new Object[] {"@AV56ContagemResultado_DemandaFM2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV56ContagemResultado_DemandaFM2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV56ContagemResultado_DemandaFM2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV28ContagemResultado_DataDmn2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV29ContagemResultado_DataDmn_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV57ContagemResultado_DataEntregaReal2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV57ContagemResultado_DataEntregaReal2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV58ContagemResultado_DataEntregaReal_To2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV58ContagemResultado_DataEntregaReal_To2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV59ContagemResultado_StatusDmn2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV61ContagemResultado_Descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV61ContagemResultado_Descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV61ContagemResultado_Descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV62ContagemResultado_SistemaCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV62ContagemResultado_SistemaCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV62ContagemResultado_SistemaCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV63ContagemResultado_Agrupador2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV63ContagemResultado_Agrupador2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV63ContagemResultado_Agrupador2",SqlDbType.Char,15,0} ,
          new Object[] {"@AV64ContagemResultado_DemandaFM3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV64ContagemResultado_DemandaFM3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV64ContagemResultado_DemandaFM3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV33ContagemResultado_DataDmn3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV34ContagemResultado_DataDmn_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV65ContagemResultado_DataEntregaReal3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV65ContagemResultado_DataEntregaReal3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV66ContagemResultado_DataEntregaReal_To3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV66ContagemResultado_DataEntregaReal_To3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV67ContagemResultado_StatusDmn3",SqlDbType.Char,1,0} ,
          new Object[] {"@lV69ContagemResultado_Descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV69ContagemResultado_Descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV69ContagemResultado_Descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV70ContagemResultado_SistemaCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV70ContagemResultado_SistemaCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV70ContagemResultado_SistemaCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV71ContagemResultado_Agrupador3",SqlDbType.Char,15,0} ,
          new Object[] {"@lV71ContagemResultado_Agrupador3",SqlDbType.Char,15,0} ,
          new Object[] {"@lV71ContagemResultado_Agrupador3",SqlDbType.Char,15,0}
          } ;
          Object[] prmH00TR14 ;
          prmH00TR14 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV14ContagemResultado_OSVinculada",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18ContagemResultado_CntadaOsVinc",SqlDbType.Int,6,0} ,
          new Object[] {"@AV16ContagemResultado_ServicoGrupo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17ContagemResultado_Servico",SqlDbType.Int,6,0} ,
          new Object[] {"@AV48ContagemResultado_DemandaFM1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV48ContagemResultado_DemandaFM1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV48ContagemResultado_DemandaFM1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV23ContagemResultado_DataDmn1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV24ContagemResultado_DataDmn_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV49ContagemResultado_DataEntregaReal1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV49ContagemResultado_DataEntregaReal1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV50ContagemResultado_DataEntregaReal_To1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV50ContagemResultado_DataEntregaReal_To1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV51ContagemResultado_StatusDmn1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV53ContagemResultado_Descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV53ContagemResultado_Descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV53ContagemResultado_Descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV54ContagemResultado_SistemaCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV54ContagemResultado_SistemaCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV54ContagemResultado_SistemaCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV55ContagemResultado_Agrupador1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV55ContagemResultado_Agrupador1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV55ContagemResultado_Agrupador1",SqlDbType.Char,15,0} ,
          new Object[] {"@AV56ContagemResultado_DemandaFM2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV56ContagemResultado_DemandaFM2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV56ContagemResultado_DemandaFM2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV28ContagemResultado_DataDmn2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV29ContagemResultado_DataDmn_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV57ContagemResultado_DataEntregaReal2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV57ContagemResultado_DataEntregaReal2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV58ContagemResultado_DataEntregaReal_To2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV58ContagemResultado_DataEntregaReal_To2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV59ContagemResultado_StatusDmn2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV61ContagemResultado_Descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV61ContagemResultado_Descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV61ContagemResultado_Descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV62ContagemResultado_SistemaCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV62ContagemResultado_SistemaCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV62ContagemResultado_SistemaCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV63ContagemResultado_Agrupador2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV63ContagemResultado_Agrupador2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV63ContagemResultado_Agrupador2",SqlDbType.Char,15,0} ,
          new Object[] {"@AV64ContagemResultado_DemandaFM3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV64ContagemResultado_DemandaFM3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV64ContagemResultado_DemandaFM3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV33ContagemResultado_DataDmn3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV34ContagemResultado_DataDmn_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV65ContagemResultado_DataEntregaReal3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV65ContagemResultado_DataEntregaReal3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV66ContagemResultado_DataEntregaReal_To3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV66ContagemResultado_DataEntregaReal_To3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV67ContagemResultado_StatusDmn3",SqlDbType.Char,1,0} ,
          new Object[] {"@lV69ContagemResultado_Descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV69ContagemResultado_Descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV69ContagemResultado_Descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV70ContagemResultado_SistemaCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV70ContagemResultado_SistemaCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV70ContagemResultado_SistemaCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV71ContagemResultado_Agrupador3",SqlDbType.Char,15,0} ,
          new Object[] {"@lV71ContagemResultado_Agrupador3",SqlDbType.Char,15,0} ,
          new Object[] {"@lV71ContagemResultado_Agrupador3",SqlDbType.Char,15,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00TR2", "SELECT [Contratada_Codigo], [Contratada_Sigla], [Contratada_AreaTrabalhoCod] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo ORDER BY [Contratada_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00TR2,0,0,true,false )
             ,new CursorDef("H00TR3", "SELECT T4.[Contrato_Codigo], T2.[Servico_Codigo], T1.[ContratoServicos_Codigo], T2.[ServicoGrupo_Codigo], T3.[ServicoGrupo_Descricao], T4.[Contratada_Codigo], T1.[ContratoServicos_Ativo] FROM ((([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) INNER JOIN [ServicoGrupo] T3 WITH (NOLOCK) ON T3.[ServicoGrupo_Codigo] = T2.[ServicoGrupo_Codigo]) INNER JOIN [Contrato] T4 WITH (NOLOCK) ON T4.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE (T1.[ContratoServicos_Ativo] = 1) AND (T4.[Contratada_Codigo] = @AV15ContagemResultado_ContratadaCod) ORDER BY T3.[ServicoGrupo_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00TR3,0,0,true,false )
             ,new CursorDef("H00TR4", "SELECT T3.[Contrato_Codigo], T1.[ContratoServicos_Codigo], T1.[Servico_Codigo], T2.[Servico_Sigla], T3.[Contratada_Codigo], T2.[ServicoGrupo_Codigo], T1.[ContratoServicos_Ativo] FROM (([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) INNER JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE (T1.[ContratoServicos_Ativo] = 1) AND (T3.[Contratada_Codigo] = @AV15ContagemResultado_ContratadaCod) AND (T2.[ServicoGrupo_Codigo] = @AV16ContagemResultado_ServicoGrupo) ORDER BY T2.[Servico_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00TR4,0,0,true,false )
             ,new CursorDef("H00TR5", "SELECT [Contratada_Codigo], [Contratada_Sigla], [Contratada_AreaTrabalhoCod] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo ORDER BY [Contratada_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00TR5,0,0,true,false )
             ,new CursorDef("H00TR6", "SELECT T4.[Contrato_Codigo], T2.[Servico_Codigo], T1.[ContratoServicos_Codigo], T2.[ServicoGrupo_Codigo], T3.[ServicoGrupo_Descricao], T4.[Contratada_Codigo], T1.[ContratoServicos_Ativo] FROM ((([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) INNER JOIN [ServicoGrupo] T3 WITH (NOLOCK) ON T3.[ServicoGrupo_Codigo] = T2.[ServicoGrupo_Codigo]) INNER JOIN [Contrato] T4 WITH (NOLOCK) ON T4.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE (T1.[ContratoServicos_Ativo] = 1) AND (T4.[Contratada_Codigo] = @AV18ContagemResultado_CntadaOsVinc) ORDER BY T3.[ServicoGrupo_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00TR6,0,0,true,false )
             ,new CursorDef("H00TR7", "SELECT T3.[Contrato_Codigo], T1.[ContratoServicos_Codigo], T1.[Servico_Codigo], T2.[Servico_Sigla], T3.[Contratada_Codigo], T2.[ServicoGrupo_Codigo], T1.[ContratoServicos_Ativo] FROM (([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) INNER JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE (T1.[ContratoServicos_Ativo] = 1) AND (T3.[Contratada_Codigo] = @AV18ContagemResultado_CntadaOsVinc) AND (T2.[ServicoGrupo_Codigo] = @AV19ContagemResultado_SerGrupoVinc) ORDER BY T2.[Servico_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00TR7,0,0,true,false )
             ,new CursorDef("H00TR8", "SELECT [Sistema_Codigo], [Sistema_Sigla], [Sistema_AreaTrabalhoCod], [Sistema_Ativo] FROM [Sistema] WITH (NOLOCK) WHERE ([Sistema_Ativo] = 1) AND ([Sistema_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo) ORDER BY [Sistema_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00TR8,0,0,true,false )
             ,new CursorDef("H00TR9", "SELECT [Sistema_Codigo], [Sistema_Sigla], [Sistema_AreaTrabalhoCod], [Sistema_Ativo] FROM [Sistema] WITH (NOLOCK) WHERE ([Sistema_Ativo] = 1) AND ([Sistema_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo) ORDER BY [Sistema_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00TR9,0,0,true,false )
             ,new CursorDef("H00TR10", "SELECT [Sistema_Codigo], [Sistema_Sigla], [Sistema_AreaTrabalhoCod], [Sistema_Ativo] FROM [Sistema] WITH (NOLOCK) WHERE ([Sistema_Ativo] = 1) AND ([Sistema_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo) ORDER BY [Sistema_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00TR10,0,0,true,false )
             ,new CursorDef("H00TR12", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00TR12,100,0,true,false )
             ,new CursorDef("H00TR14", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00TR14,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((bool[]) buf[6])[0] = rslt.getBool(7) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 15) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((bool[]) buf[6])[0] = rslt.getBool(7) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((bool[]) buf[6])[0] = rslt.getBool(7) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 15) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((bool[]) buf[6])[0] = rslt.getBool(7) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 25) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 25) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 25) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[10])[0] = rslt.getGXDateTime(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[12])[0] = rslt.getGXDate(7) ;
                ((String[]) buf[13])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((int[]) buf[17])[0] = rslt.getInt(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((int[]) buf[19])[0] = rslt.getInt(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((int[]) buf[21])[0] = rslt.getInt(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((int[]) buf[23])[0] = rslt.getInt(13) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(13);
                ((int[]) buf[25])[0] = rslt.getInt(14) ;
                ((String[]) buf[26])[0] = rslt.getString(15, 25) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((String[]) buf[28])[0] = rslt.getString(16, 1) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((String[]) buf[30])[0] = rslt.getVarchar(17) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(17);
                ((decimal[]) buf[32])[0] = rslt.getDecimal(18) ;
                ((decimal[]) buf[33])[0] = rslt.getDecimal(19) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[10])[0] = rslt.getGXDateTime(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[12])[0] = rslt.getGXDate(7) ;
                ((String[]) buf[13])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((int[]) buf[18])[0] = rslt.getInt(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((int[]) buf[20])[0] = rslt.getInt(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((int[]) buf[22])[0] = rslt.getInt(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((String[]) buf[24])[0] = rslt.getString(14, 25) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((int[]) buf[26])[0] = rslt.getInt(15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((String[]) buf[28])[0] = rslt.getString(16, 1) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((String[]) buf[30])[0] = rslt.getVarchar(17) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(17);
                ((decimal[]) buf[32])[0] = rslt.getDecimal(18) ;
                ((decimal[]) buf[33])[0] = rslt.getDecimal(19) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[61]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[62]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[63]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[64]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[66]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[67]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[68]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[69]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[70]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[71]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[72]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[73]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[74]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[75]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[76]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[77]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[78]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[79]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[80]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[81]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[82]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[83]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[84]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[85]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[86]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[87]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[88]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[89]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[90]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[91]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[92]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[93]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[94]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[95]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[96]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[97]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[98]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[99]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[100]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[101]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[102]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[103]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[104]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[105]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[106]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[107]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[108]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[109]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[110]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[111]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[112]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[113]);
                }
                if ( (short)parms[53] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[114]);
                }
                if ( (short)parms[54] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[115]);
                }
                if ( (short)parms[55] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[116]);
                }
                if ( (short)parms[56] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[117]);
                }
                if ( (short)parms[57] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[118]);
                }
                if ( (short)parms[58] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[119]);
                }
                if ( (short)parms[59] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[120]);
                }
                if ( (short)parms[60] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[121]);
                }
                return;
             case 10 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[62]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[63]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[64]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[65]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[66]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[67]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[68]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[69]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[70]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[71]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[72]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[73]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[74]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[75]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[76]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[77]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[78]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[79]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[80]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[81]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[82]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[83]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[84]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[85]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[86]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[87]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[88]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[89]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[90]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[91]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[92]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[93]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[94]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[95]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[96]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[97]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[98]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[99]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[100]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[101]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[102]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[103]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[104]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[105]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[106]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[107]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[108]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[109]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[110]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[111]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[112]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[113]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[114]);
                }
                if ( (short)parms[53] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[115]);
                }
                if ( (short)parms[54] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[116]);
                }
                if ( (short)parms[55] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[117]);
                }
                if ( (short)parms[56] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[118]);
                }
                if ( (short)parms[57] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[119]);
                }
                if ( (short)parms[58] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[120]);
                }
                if ( (short)parms[59] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[121]);
                }
                if ( (short)parms[60] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[122]);
                }
                if ( (short)parms[61] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[123]);
                }
                return;
       }
    }

 }

}
