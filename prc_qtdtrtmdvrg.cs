/*
               File: PRC_QtdTrtmDvrg
        Description: Quantidade de tratamento de divergências
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:13:0.30
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_qtdtrtmdvrg : GXProcedure
   {
      public prc_qtdtrtmdvrg( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_qtdtrtmdvrg( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Codigo ,
                           int aP1_Codigo_OSVnc ,
                           int aP2_Prestadora ,
                           int aP3_ContratadaDoUsuario ,
                           out short aP4_Retorno )
      {
         this.AV8Codigo = aP0_Codigo;
         this.AV14Codigo_OSVnc = aP1_Codigo_OSVnc;
         this.AV11Prestadora = aP2_Prestadora;
         this.AV9ContratadaDoUsuario = aP3_ContratadaDoUsuario;
         this.AV12Retorno = 0 ;
         initialize();
         executePrivate();
         aP4_Retorno=this.AV12Retorno;
      }

      public short executeUdp( int aP0_Codigo ,
                               int aP1_Codigo_OSVnc ,
                               int aP2_Prestadora ,
                               int aP3_ContratadaDoUsuario )
      {
         this.AV8Codigo = aP0_Codigo;
         this.AV14Codigo_OSVnc = aP1_Codigo_OSVnc;
         this.AV11Prestadora = aP2_Prestadora;
         this.AV9ContratadaDoUsuario = aP3_ContratadaDoUsuario;
         this.AV12Retorno = 0 ;
         initialize();
         executePrivate();
         aP4_Retorno=this.AV12Retorno;
         return AV12Retorno ;
      }

      public void executeSubmit( int aP0_Codigo ,
                                 int aP1_Codigo_OSVnc ,
                                 int aP2_Prestadora ,
                                 int aP3_ContratadaDoUsuario ,
                                 out short aP4_Retorno )
      {
         prc_qtdtrtmdvrg objprc_qtdtrtmdvrg;
         objprc_qtdtrtmdvrg = new prc_qtdtrtmdvrg();
         objprc_qtdtrtmdvrg.AV8Codigo = aP0_Codigo;
         objprc_qtdtrtmdvrg.AV14Codigo_OSVnc = aP1_Codigo_OSVnc;
         objprc_qtdtrtmdvrg.AV11Prestadora = aP2_Prestadora;
         objprc_qtdtrtmdvrg.AV9ContratadaDoUsuario = aP3_ContratadaDoUsuario;
         objprc_qtdtrtmdvrg.AV12Retorno = 0 ;
         objprc_qtdtrtmdvrg.context.SetSubmitInitialConfig(context);
         objprc_qtdtrtmdvrg.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_qtdtrtmdvrg);
         aP4_Retorno=this.AV12Retorno;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_qtdtrtmdvrg)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV10OSVinculada = AV14Codigo_OSVnc;
         if ( (0==AV10OSVinculada) )
         {
            /* Execute user subroutine: 'BUSCAVINCULADA' */
            S111 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         if ( (0==AV10OSVinculada) )
         {
            AV12Retorno = 0;
            /* Optimized group. */
            /* Using cursor P00AT2 */
            pr_default.execute(0, new Object[] {AV8Codigo});
            cV12Retorno = P00AT2_AV12Retorno[0];
            pr_default.close(0);
            AV12Retorno = (short)(AV12Retorno+cV12Retorno*1);
            /* End optimized group. */
         }
         else
         {
            this.cleanup();
            if (true) return;
         }
         if ( ( AV12Retorno /  ( decimal )( 2 ) == Convert.ToDecimal( NumberUtil.Int( (long)(AV12Retorno/ (decimal)(2))) )) )
         {
            if ( AV11Prestadora == AV9ContratadaDoUsuario )
            {
               AV12Retorno = 1;
            }
         }
         else
         {
            AV12Retorno = 0;
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'BUSCAVINCULADA' Routine */
         /* Using cursor P00AT4 */
         pr_default.execute(1, new Object[] {AV8Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P00AT4_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00AT4_n1553ContagemResultado_CntSrvCod[0];
            A1593ContagemResultado_CntSrvTpVnc = P00AT4_A1593ContagemResultado_CntSrvTpVnc[0];
            n1593ContagemResultado_CntSrvTpVnc = P00AT4_n1593ContagemResultado_CntSrvTpVnc[0];
            A602ContagemResultado_OSVinculada = P00AT4_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P00AT4_n602ContagemResultado_OSVinculada[0];
            A456ContagemResultado_Codigo = P00AT4_A456ContagemResultado_Codigo[0];
            A531ContagemResultado_StatusUltCnt = P00AT4_A531ContagemResultado_StatusUltCnt[0];
            n531ContagemResultado_StatusUltCnt = P00AT4_n531ContagemResultado_StatusUltCnt[0];
            A1593ContagemResultado_CntSrvTpVnc = P00AT4_A1593ContagemResultado_CntSrvTpVnc[0];
            n1593ContagemResultado_CntSrvTpVnc = P00AT4_n1593ContagemResultado_CntSrvTpVnc[0];
            A531ContagemResultado_StatusUltCnt = P00AT4_A531ContagemResultado_StatusUltCnt[0];
            n531ContagemResultado_StatusUltCnt = P00AT4_n531ContagemResultado_StatusUltCnt[0];
            AV10OSVinculada = A456ContagemResultado_Codigo;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00AT2_AV12Retorno = new short[1] ;
         P00AT4_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00AT4_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00AT4_A1593ContagemResultado_CntSrvTpVnc = new String[] {""} ;
         P00AT4_n1593ContagemResultado_CntSrvTpVnc = new bool[] {false} ;
         P00AT4_A602ContagemResultado_OSVinculada = new int[1] ;
         P00AT4_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P00AT4_A456ContagemResultado_Codigo = new int[1] ;
         P00AT4_A531ContagemResultado_StatusUltCnt = new short[1] ;
         P00AT4_n531ContagemResultado_StatusUltCnt = new bool[] {false} ;
         A1593ContagemResultado_CntSrvTpVnc = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_qtdtrtmdvrg__default(),
            new Object[][] {
                new Object[] {
               P00AT2_AV12Retorno
               }
               , new Object[] {
               P00AT4_A1553ContagemResultado_CntSrvCod, P00AT4_n1553ContagemResultado_CntSrvCod, P00AT4_A1593ContagemResultado_CntSrvTpVnc, P00AT4_n1593ContagemResultado_CntSrvTpVnc, P00AT4_A602ContagemResultado_OSVinculada, P00AT4_n602ContagemResultado_OSVinculada, P00AT4_A456ContagemResultado_Codigo, P00AT4_A531ContagemResultado_StatusUltCnt, P00AT4_n531ContagemResultado_StatusUltCnt
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV12Retorno ;
      private short cV12Retorno ;
      private short A531ContagemResultado_StatusUltCnt ;
      private int AV8Codigo ;
      private int AV14Codigo_OSVnc ;
      private int AV11Prestadora ;
      private int AV9ContratadaDoUsuario ;
      private int AV10OSVinculada ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A602ContagemResultado_OSVinculada ;
      private int A456ContagemResultado_Codigo ;
      private String scmdbuf ;
      private String A1593ContagemResultado_CntSrvTpVnc ;
      private bool returnInSub ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1593ContagemResultado_CntSrvTpVnc ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n531ContagemResultado_StatusUltCnt ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private short[] P00AT2_AV12Retorno ;
      private int[] P00AT4_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00AT4_n1553ContagemResultado_CntSrvCod ;
      private String[] P00AT4_A1593ContagemResultado_CntSrvTpVnc ;
      private bool[] P00AT4_n1593ContagemResultado_CntSrvTpVnc ;
      private int[] P00AT4_A602ContagemResultado_OSVinculada ;
      private bool[] P00AT4_n602ContagemResultado_OSVinculada ;
      private int[] P00AT4_A456ContagemResultado_Codigo ;
      private short[] P00AT4_A531ContagemResultado_StatusUltCnt ;
      private bool[] P00AT4_n531ContagemResultado_StatusUltCnt ;
      private short aP4_Retorno ;
   }

   public class prc_qtdtrtmdvrg__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00AT2 ;
          prmP00AT2 = new Object[] {
          new Object[] {"@AV8Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00AT4 ;
          prmP00AT4 = new Object[] {
          new Object[] {"@AV8Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00AT2", "SELECT COUNT(*) FROM [LogResponsavel] WITH (NOLOCK) WHERE ([LogResponsavel_DemandaCod] = @AV8Codigo) AND ([LogResponsavel_Acao] = 'N') ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00AT2,1,0,true,false )
             ,new CursorDef("P00AT4", "SELECT TOP 1 T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[ContratoServicos_TipoVnc] AS ContagemResultado_CntSrvTpVnc, T1.[ContagemResultado_OSVinculada], T1.[ContagemResultado_Codigo], COALESCE( T3.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN (SELECT MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T3 ON T3.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE (T1.[ContagemResultado_OSVinculada] = @AV8Codigo) AND (T2.[ContratoServicos_TipoVnc] = 'C' or T2.[ContratoServicos_TipoVnc] = 'A') AND (COALESCE( T3.[ContagemResultado_StatusUltCnt], 0) = 7) ORDER BY T1.[ContagemResultado_OSVinculada] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00AT4,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((short[]) buf[7])[0] = rslt.getShort(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
