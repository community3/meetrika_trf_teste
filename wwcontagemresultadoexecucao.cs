/*
               File: WWContagemResultadoExecucao
        Description:  Contagem Resultado Execucao
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:24:24.17
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwcontagemresultadoexecucao : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwcontagemresultadoexecucao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwcontagemresultadoexecucao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavContratada_codigo = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         dynavContagemresultado_contratadacod1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         dynavContagemresultado_contratadacod2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         dynavContagemresultado_contratadacod3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTRATADA_CODIGO") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTRATADA_CODIGOKI2( AV6WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_CONTRATADACOD1") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_CONTRATADACOD1KI2( AV6WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_CONTRATADACOD2") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_CONTRATADACOD2KI2( AV6WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_CONTRATADACOD3") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_CONTRATADACOD3KI2( AV6WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_152 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_152_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_152_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16ContagemResultadoExecucao_Inicio1 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContagemResultadoExecucao_Inicio1", context.localUtil.TToC( AV16ContagemResultadoExecucao_Inicio1, 8, 5, 0, 3, "/", ":", " "));
               AV17ContagemResultadoExecucao_Inicio_To1 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContagemResultadoExecucao_Inicio_To1", context.localUtil.TToC( AV17ContagemResultadoExecucao_Inicio_To1, 8, 5, 0, 3, "/", ":", " "));
               AV18ContagemResultadoExecucao_Fim1 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultadoExecucao_Fim1", context.localUtil.TToC( AV18ContagemResultadoExecucao_Fim1, 8, 5, 0, 3, "/", ":", " "));
               AV19ContagemResultadoExecucao_Fim_To1 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultadoExecucao_Fim_To1", context.localUtil.TToC( AV19ContagemResultadoExecucao_Fim_To1, 8, 5, 0, 3, "/", ":", " "));
               AV20ContagemResultadoExecucao_Prevista1 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContagemResultadoExecucao_Prevista1", context.localUtil.TToC( AV20ContagemResultadoExecucao_Prevista1, 8, 5, 0, 3, "/", ":", " "));
               AV21ContagemResultadoExecucao_Prevista_To1 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContagemResultadoExecucao_Prevista_To1", context.localUtil.TToC( AV21ContagemResultadoExecucao_Prevista_To1, 8, 5, 0, 3, "/", ":", " "));
               AV43ContagemResultado_OsFsOsFm1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ContagemResultado_OsFsOsFm1", AV43ContagemResultado_OsFsOsFm1);
               AV82ContagemResultado_ContratadaCod1 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82ContagemResultado_ContratadaCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV82ContagemResultado_ContratadaCod1), 6, 0)));
               AV83ContagemResultadoExecucao_OSCod1 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83ContagemResultadoExecucao_OSCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV83ContagemResultadoExecucao_OSCod1), 6, 0)));
               AV23DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector2", AV23DynamicFiltersSelector2);
               AV24ContagemResultadoExecucao_Inicio2 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultadoExecucao_Inicio2", context.localUtil.TToC( AV24ContagemResultadoExecucao_Inicio2, 8, 5, 0, 3, "/", ":", " "));
               AV25ContagemResultadoExecucao_Inicio_To2 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContagemResultadoExecucao_Inicio_To2", context.localUtil.TToC( AV25ContagemResultadoExecucao_Inicio_To2, 8, 5, 0, 3, "/", ":", " "));
               AV26ContagemResultadoExecucao_Fim2 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ContagemResultadoExecucao_Fim2", context.localUtil.TToC( AV26ContagemResultadoExecucao_Fim2, 8, 5, 0, 3, "/", ":", " "));
               AV27ContagemResultadoExecucao_Fim_To2 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27ContagemResultadoExecucao_Fim_To2", context.localUtil.TToC( AV27ContagemResultadoExecucao_Fim_To2, 8, 5, 0, 3, "/", ":", " "));
               AV28ContagemResultadoExecucao_Prevista2 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ContagemResultadoExecucao_Prevista2", context.localUtil.TToC( AV28ContagemResultadoExecucao_Prevista2, 8, 5, 0, 3, "/", ":", " "));
               AV29ContagemResultadoExecucao_Prevista_To2 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContagemResultadoExecucao_Prevista_To2", context.localUtil.TToC( AV29ContagemResultadoExecucao_Prevista_To2, 8, 5, 0, 3, "/", ":", " "));
               AV44ContagemResultado_OsFsOsFm2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ContagemResultado_OsFsOsFm2", AV44ContagemResultado_OsFsOsFm2);
               AV84ContagemResultado_ContratadaCod2 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84ContagemResultado_ContratadaCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV84ContagemResultado_ContratadaCod2), 6, 0)));
               AV85ContagemResultadoExecucao_OSCod2 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85ContagemResultadoExecucao_OSCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV85ContagemResultadoExecucao_OSCod2), 6, 0)));
               AV31DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31DynamicFiltersSelector3", AV31DynamicFiltersSelector3);
               AV32ContagemResultadoExecucao_Inicio3 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ContagemResultadoExecucao_Inicio3", context.localUtil.TToC( AV32ContagemResultadoExecucao_Inicio3, 8, 5, 0, 3, "/", ":", " "));
               AV33ContagemResultadoExecucao_Inicio_To3 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContagemResultadoExecucao_Inicio_To3", context.localUtil.TToC( AV33ContagemResultadoExecucao_Inicio_To3, 8, 5, 0, 3, "/", ":", " "));
               AV34ContagemResultadoExecucao_Fim3 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContagemResultadoExecucao_Fim3", context.localUtil.TToC( AV34ContagemResultadoExecucao_Fim3, 8, 5, 0, 3, "/", ":", " "));
               AV35ContagemResultadoExecucao_Fim_To3 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35ContagemResultadoExecucao_Fim_To3", context.localUtil.TToC( AV35ContagemResultadoExecucao_Fim_To3, 8, 5, 0, 3, "/", ":", " "));
               AV36ContagemResultadoExecucao_Prevista3 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ContagemResultadoExecucao_Prevista3", context.localUtil.TToC( AV36ContagemResultadoExecucao_Prevista3, 8, 5, 0, 3, "/", ":", " "));
               AV37ContagemResultadoExecucao_Prevista_To3 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ContagemResultadoExecucao_Prevista_To3", context.localUtil.TToC( AV37ContagemResultadoExecucao_Prevista_To3, 8, 5, 0, 3, "/", ":", " "));
               AV45ContagemResultado_OsFsOsFm3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ContagemResultado_OsFsOsFm3", AV45ContagemResultado_OsFsOsFm3);
               AV86ContagemResultado_ContratadaCod3 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86ContagemResultado_ContratadaCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV86ContagemResultado_ContratadaCod3), 6, 0)));
               AV87ContagemResultadoExecucao_OSCod3 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87ContagemResultadoExecucao_OSCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV87ContagemResultadoExecucao_OSCod3), 6, 0)));
               AV22DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled2", AV22DynamicFiltersEnabled2);
               AV30DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersEnabled3", AV30DynamicFiltersEnabled3);
               AV49TFContagemResultado_OsFsOsFm = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContagemResultado_OsFsOsFm", AV49TFContagemResultado_OsFsOsFm);
               AV50TFContagemResultado_OsFsOsFm_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContagemResultado_OsFsOsFm_Sel", AV50TFContagemResultado_OsFsOsFm_Sel);
               AV53TFContagemResultadoExecucao_Inicio = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContagemResultadoExecucao_Inicio", context.localUtil.TToC( AV53TFContagemResultadoExecucao_Inicio, 8, 5, 0, 3, "/", ":", " "));
               AV54TFContagemResultadoExecucao_Inicio_To = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContagemResultadoExecucao_Inicio_To", context.localUtil.TToC( AV54TFContagemResultadoExecucao_Inicio_To, 8, 5, 0, 3, "/", ":", " "));
               AV59TFContagemResultadoExecucao_Prevista = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContagemResultadoExecucao_Prevista", context.localUtil.TToC( AV59TFContagemResultadoExecucao_Prevista, 8, 5, 0, 3, "/", ":", " "));
               AV60TFContagemResultadoExecucao_Prevista_To = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContagemResultadoExecucao_Prevista_To", context.localUtil.TToC( AV60TFContagemResultadoExecucao_Prevista_To, 8, 5, 0, 3, "/", ":", " "));
               AV65TFContagemResultadoExecucao_PrazoDias = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContagemResultadoExecucao_PrazoDias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65TFContagemResultadoExecucao_PrazoDias), 4, 0)));
               AV66TFContagemResultadoExecucao_PrazoDias_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContagemResultadoExecucao_PrazoDias_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66TFContagemResultadoExecucao_PrazoDias_To), 4, 0)));
               AV69TFContagemResultadoExecucao_Fim = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFContagemResultadoExecucao_Fim", context.localUtil.TToC( AV69TFContagemResultadoExecucao_Fim, 8, 5, 0, 3, "/", ":", " "));
               AV70TFContagemResultadoExecucao_Fim_To = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContagemResultadoExecucao_Fim_To", context.localUtil.TToC( AV70TFContagemResultadoExecucao_Fim_To, 8, 5, 0, 3, "/", ":", " "));
               AV75TFContagemResultadoExecucao_Dias = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFContagemResultadoExecucao_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV75TFContagemResultadoExecucao_Dias), 4, 0)));
               AV76TFContagemResultadoExecucao_Dias_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFContagemResultadoExecucao_Dias_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV76TFContagemResultadoExecucao_Dias_To), 4, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace", AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace);
               AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace", AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace);
               AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace", AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace);
               AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace", AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace);
               AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace", AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace);
               AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace", AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace);
               AV88Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV88Contratada_Codigo), 6, 0)));
               AV138Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV39DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39DynamicFiltersIgnoreFirst", AV39DynamicFiltersIgnoreFirst);
               AV38DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38DynamicFiltersRemoving", AV38DynamicFiltersRemoving);
               A1405ContagemResultadoExecucao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV16ContagemResultadoExecucao_Inicio1, AV17ContagemResultadoExecucao_Inicio_To1, AV18ContagemResultadoExecucao_Fim1, AV19ContagemResultadoExecucao_Fim_To1, AV20ContagemResultadoExecucao_Prevista1, AV21ContagemResultadoExecucao_Prevista_To1, AV43ContagemResultado_OsFsOsFm1, AV82ContagemResultado_ContratadaCod1, AV83ContagemResultadoExecucao_OSCod1, AV23DynamicFiltersSelector2, AV24ContagemResultadoExecucao_Inicio2, AV25ContagemResultadoExecucao_Inicio_To2, AV26ContagemResultadoExecucao_Fim2, AV27ContagemResultadoExecucao_Fim_To2, AV28ContagemResultadoExecucao_Prevista2, AV29ContagemResultadoExecucao_Prevista_To2, AV44ContagemResultado_OsFsOsFm2, AV84ContagemResultado_ContratadaCod2, AV85ContagemResultadoExecucao_OSCod2, AV31DynamicFiltersSelector3, AV32ContagemResultadoExecucao_Inicio3, AV33ContagemResultadoExecucao_Inicio_To3, AV34ContagemResultadoExecucao_Fim3, AV35ContagemResultadoExecucao_Fim_To3, AV36ContagemResultadoExecucao_Prevista3, AV37ContagemResultadoExecucao_Prevista_To3, AV45ContagemResultado_OsFsOsFm3, AV86ContagemResultado_ContratadaCod3, AV87ContagemResultadoExecucao_OSCod3, AV22DynamicFiltersEnabled2, AV30DynamicFiltersEnabled3, AV49TFContagemResultado_OsFsOsFm, AV50TFContagemResultado_OsFsOsFm_Sel, AV53TFContagemResultadoExecucao_Inicio, AV54TFContagemResultadoExecucao_Inicio_To, AV59TFContagemResultadoExecucao_Prevista, AV60TFContagemResultadoExecucao_Prevista_To, AV65TFContagemResultadoExecucao_PrazoDias, AV66TFContagemResultadoExecucao_PrazoDias_To, AV69TFContagemResultadoExecucao_Fim, AV70TFContagemResultadoExecucao_Fim_To, AV75TFContagemResultadoExecucao_Dias, AV76TFContagemResultadoExecucao_Dias_To, AV6WWPContext, AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace, AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace, AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace, AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace, AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace, AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace, AV88Contratada_Codigo, AV138Pgmname, AV10GridState, AV39DynamicFiltersIgnoreFirst, AV38DynamicFiltersRemoving, A1405ContagemResultadoExecucao_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAKI2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTKI2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206216242481");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwcontagemresultadoexecucao.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADOEXECUCAO_INICIO1", context.localUtil.TToC( AV16ContagemResultadoExecucao_Inicio1, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1", context.localUtil.TToC( AV17ContagemResultadoExecucao_Inicio_To1, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADOEXECUCAO_FIM1", context.localUtil.TToC( AV18ContagemResultadoExecucao_Fim1, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADOEXECUCAO_FIM_TO1", context.localUtil.TToC( AV19ContagemResultadoExecucao_Fim_To1, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADOEXECUCAO_PREVISTA1", context.localUtil.TToC( AV20ContagemResultadoExecucao_Prevista1, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1", context.localUtil.TToC( AV21ContagemResultadoExecucao_Prevista_To1, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_OSFSOSFM1", AV43ContagemResultado_OsFsOsFm1);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_CONTRATADACOD1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV82ContagemResultado_ContratadaCod1), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADOEXECUCAO_OSCOD1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV83ContagemResultadoExecucao_OSCod1), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV23DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADOEXECUCAO_INICIO2", context.localUtil.TToC( AV24ContagemResultadoExecucao_Inicio2, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2", context.localUtil.TToC( AV25ContagemResultadoExecucao_Inicio_To2, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADOEXECUCAO_FIM2", context.localUtil.TToC( AV26ContagemResultadoExecucao_Fim2, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADOEXECUCAO_FIM_TO2", context.localUtil.TToC( AV27ContagemResultadoExecucao_Fim_To2, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADOEXECUCAO_PREVISTA2", context.localUtil.TToC( AV28ContagemResultadoExecucao_Prevista2, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2", context.localUtil.TToC( AV29ContagemResultadoExecucao_Prevista_To2, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_OSFSOSFM2", AV44ContagemResultado_OsFsOsFm2);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_CONTRATADACOD2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV84ContagemResultado_ContratadaCod2), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADOEXECUCAO_OSCOD2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV85ContagemResultadoExecucao_OSCod2), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV31DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADOEXECUCAO_INICIO3", context.localUtil.TToC( AV32ContagemResultadoExecucao_Inicio3, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3", context.localUtil.TToC( AV33ContagemResultadoExecucao_Inicio_To3, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADOEXECUCAO_FIM3", context.localUtil.TToC( AV34ContagemResultadoExecucao_Fim3, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADOEXECUCAO_FIM_TO3", context.localUtil.TToC( AV35ContagemResultadoExecucao_Fim_To3, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADOEXECUCAO_PREVISTA3", context.localUtil.TToC( AV36ContagemResultadoExecucao_Prevista3, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3", context.localUtil.TToC( AV37ContagemResultadoExecucao_Prevista_To3, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_OSFSOSFM3", AV45ContagemResultado_OsFsOsFm3);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_CONTRATADACOD3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV86ContagemResultado_ContratadaCod3), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADOEXECUCAO_OSCOD3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV87ContagemResultadoExecucao_OSCod3), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV22DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV30DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_OSFSOSFM", AV49TFContagemResultado_OsFsOsFm);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_OSFSOSFM_SEL", AV50TFContagemResultado_OsFsOsFm_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADOEXECUCAO_INICIO", context.localUtil.TToC( AV53TFContagemResultadoExecucao_Inicio, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO", context.localUtil.TToC( AV54TFContagemResultadoExecucao_Inicio_To, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA", context.localUtil.TToC( AV59TFContagemResultadoExecucao_Prevista, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO", context.localUtil.TToC( AV60TFContagemResultadoExecucao_Prevista_To, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV65TFContagemResultadoExecucao_PrazoDias), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV66TFContagemResultadoExecucao_PrazoDias_To), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADOEXECUCAO_FIM", context.localUtil.TToC( AV69TFContagemResultadoExecucao_Fim, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO", context.localUtil.TToC( AV70TFContagemResultadoExecucao_Fim_To, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADOEXECUCAO_DIAS", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV75TFContagemResultadoExecucao_Dias), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV76TFContagemResultadoExecucao_Dias_To), 4, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_152", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_152), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV80GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV81GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV78DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV78DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADO_OSFSOSFMTITLEFILTERDATA", AV48ContagemResultado_OsFsOsFmTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADO_OSFSOSFMTITLEFILTERDATA", AV48ContagemResultado_OsFsOsFmTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADOEXECUCAO_INICIOTITLEFILTERDATA", AV52ContagemResultadoExecucao_InicioTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADOEXECUCAO_INICIOTITLEFILTERDATA", AV52ContagemResultadoExecucao_InicioTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADOEXECUCAO_PREVISTATITLEFILTERDATA", AV58ContagemResultadoExecucao_PrevistaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADOEXECUCAO_PREVISTATITLEFILTERDATA", AV58ContagemResultadoExecucao_PrevistaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLEFILTERDATA", AV64ContagemResultadoExecucao_PrazoDiasTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLEFILTERDATA", AV64ContagemResultadoExecucao_PrazoDiasTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADOEXECUCAO_FIMTITLEFILTERDATA", AV68ContagemResultadoExecucao_FimTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADOEXECUCAO_FIMTITLEFILTERDATA", AV68ContagemResultadoExecucao_FimTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADOEXECUCAO_DIASTITLEFILTERDATA", AV74ContagemResultadoExecucao_DiasTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADOEXECUCAO_DIASTITLEFILTERDATA", AV74ContagemResultadoExecucao_DiasTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV138Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV39DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV38DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DEMANDA", A457ContagemResultado_Demanda);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DEMANDAFM", A493ContagemResultado_DemandaFM);
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_OSFSOSFM_Caption", StringUtil.RTrim( Ddo_contagemresultado_osfsosfm_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_OSFSOSFM_Tooltip", StringUtil.RTrim( Ddo_contagemresultado_osfsosfm_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_OSFSOSFM_Cls", StringUtil.RTrim( Ddo_contagemresultado_osfsosfm_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_OSFSOSFM_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultado_osfsosfm_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_OSFSOSFM_Selectedvalue_set", StringUtil.RTrim( Ddo_contagemresultado_osfsosfm_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_OSFSOSFM_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultado_osfsosfm_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_OSFSOSFM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultado_osfsosfm_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_OSFSOSFM_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultado_osfsosfm_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_OSFSOSFM_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultado_osfsosfm_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_OSFSOSFM_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultado_osfsosfm_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_OSFSOSFM_Filtertype", StringUtil.RTrim( Ddo_contagemresultado_osfsosfm_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_OSFSOSFM_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultado_osfsosfm_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_OSFSOSFM_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultado_osfsosfm_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_OSFSOSFM_Datalisttype", StringUtil.RTrim( Ddo_contagemresultado_osfsosfm_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_OSFSOSFM_Datalistproc", StringUtil.RTrim( Ddo_contagemresultado_osfsosfm_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_OSFSOSFM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contagemresultado_osfsosfm_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_OSFSOSFM_Loadingdata", StringUtil.RTrim( Ddo_contagemresultado_osfsosfm_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_OSFSOSFM_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultado_osfsosfm_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_OSFSOSFM_Noresultsfound", StringUtil.RTrim( Ddo_contagemresultado_osfsosfm_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_OSFSOSFM_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultado_osfsosfm_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Caption", StringUtil.RTrim( Ddo_contagemresultadoexecucao_inicio_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Tooltip", StringUtil.RTrim( Ddo_contagemresultadoexecucao_inicio_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Cls", StringUtil.RTrim( Ddo_contagemresultadoexecucao_inicio_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultadoexecucao_inicio_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Filteredtextto_set", StringUtil.RTrim( Ddo_contagemresultadoexecucao_inicio_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultadoexecucao_inicio_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultadoexecucao_inicio_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_inicio_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_inicio_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_inicio_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Filtertype", StringUtil.RTrim( Ddo_contagemresultadoexecucao_inicio_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_inicio_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_inicio_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultadoexecucao_inicio_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Rangefilterfrom", StringUtil.RTrim( Ddo_contagemresultadoexecucao_inicio_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Rangefilterto", StringUtil.RTrim( Ddo_contagemresultadoexecucao_inicio_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultadoexecucao_inicio_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Caption", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prevista_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Tooltip", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prevista_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Cls", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prevista_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prevista_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Filteredtextto_set", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prevista_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prevista_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prevista_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_prevista_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_prevista_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_prevista_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Filtertype", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prevista_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_prevista_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_prevista_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prevista_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Rangefilterfrom", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prevista_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Rangefilterto", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prevista_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prevista_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Caption", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prazodias_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Tooltip", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prazodias_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Cls", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prazodias_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prazodias_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Filteredtextto_set", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prazodias_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prazodias_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prazodias_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_prazodias_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_prazodias_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_prazodias_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Filtertype", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prazodias_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_prazodias_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_prazodias_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prazodias_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Rangefilterfrom", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prazodias_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Rangefilterto", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prazodias_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prazodias_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Caption", StringUtil.RTrim( Ddo_contagemresultadoexecucao_fim_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Tooltip", StringUtil.RTrim( Ddo_contagemresultadoexecucao_fim_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Cls", StringUtil.RTrim( Ddo_contagemresultadoexecucao_fim_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultadoexecucao_fim_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Filteredtextto_set", StringUtil.RTrim( Ddo_contagemresultadoexecucao_fim_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultadoexecucao_fim_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultadoexecucao_fim_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_fim_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_fim_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_fim_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Filtertype", StringUtil.RTrim( Ddo_contagemresultadoexecucao_fim_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_fim_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_fim_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultadoexecucao_fim_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Rangefilterfrom", StringUtil.RTrim( Ddo_contagemresultadoexecucao_fim_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Rangefilterto", StringUtil.RTrim( Ddo_contagemresultadoexecucao_fim_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultadoexecucao_fim_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Caption", StringUtil.RTrim( Ddo_contagemresultadoexecucao_dias_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Tooltip", StringUtil.RTrim( Ddo_contagemresultadoexecucao_dias_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Cls", StringUtil.RTrim( Ddo_contagemresultadoexecucao_dias_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultadoexecucao_dias_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Filteredtextto_set", StringUtil.RTrim( Ddo_contagemresultadoexecucao_dias_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultadoexecucao_dias_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultadoexecucao_dias_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_dias_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_dias_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_dias_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Filtertype", StringUtil.RTrim( Ddo_contagemresultadoexecucao_dias_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_dias_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_dias_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultadoexecucao_dias_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Rangefilterfrom", StringUtil.RTrim( Ddo_contagemresultadoexecucao_dias_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Rangefilterto", StringUtil.RTrim( Ddo_contagemresultadoexecucao_dias_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultadoexecucao_dias_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_OSFSOSFM_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultado_osfsosfm_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_OSFSOSFM_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultado_osfsosfm_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_OSFSOSFM_Selectedvalue_get", StringUtil.RTrim( Ddo_contagemresultado_osfsosfm_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultadoexecucao_inicio_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultadoexecucao_inicio_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Filteredtextto_get", StringUtil.RTrim( Ddo_contagemresultadoexecucao_inicio_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prevista_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prevista_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Filteredtextto_get", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prevista_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prazodias_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prazodias_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Filteredtextto_get", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prazodias_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultadoexecucao_fim_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultadoexecucao_fim_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Filteredtextto_get", StringUtil.RTrim( Ddo_contagemresultadoexecucao_fim_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultadoexecucao_dias_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultadoexecucao_dias_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Filteredtextto_get", StringUtil.RTrim( Ddo_contagemresultadoexecucao_dias_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEKI2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTKI2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwcontagemresultadoexecucao.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWContagemResultadoExecucao" ;
      }

      public override String GetPgmdesc( )
      {
         return " Contagem Resultado Execucao" ;
      }

      protected void WBKI0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_KI2( true) ;
         }
         else
         {
            wb_table1_2_KI2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_KI2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 167,'',false,'" + sGXsfl_152_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(167, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,167);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 168,'',false,'" + sGXsfl_152_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV30DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(168, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,168);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 169,'',false,'" + sGXsfl_152_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_osfsosfm_Internalname, AV49TFContagemResultado_OsFsOsFm, StringUtil.RTrim( context.localUtil.Format( AV49TFContagemResultado_OsFsOsFm, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,169);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_osfsosfm_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_osfsosfm_Visible, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContagemResultadoExecucao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 170,'',false,'" + sGXsfl_152_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_osfsosfm_sel_Internalname, AV50TFContagemResultado_OsFsOsFm_Sel, StringUtil.RTrim( context.localUtil.Format( AV50TFContagemResultado_OsFsOsFm_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,170);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_osfsosfm_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_osfsosfm_sel_Visible, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContagemResultadoExecucao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 171,'',false,'" + sGXsfl_152_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontagemresultadoexecucao_inicio_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadoexecucao_inicio_Internalname, context.localUtil.TToC( AV53TFContagemResultadoExecucao_Inicio, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV53TFContagemResultadoExecucao_Inicio, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,171);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadoexecucao_inicio_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadoexecucao_inicio_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontagemresultadoexecucao_inicio_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontagemresultadoexecucao_inicio_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 172,'',false,'" + sGXsfl_152_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontagemresultadoexecucao_inicio_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadoexecucao_inicio_to_Internalname, context.localUtil.TToC( AV54TFContagemResultadoExecucao_Inicio_To, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV54TFContagemResultadoExecucao_Inicio_To, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,172);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadoexecucao_inicio_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadoexecucao_inicio_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontagemresultadoexecucao_inicio_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontagemresultadoexecucao_inicio_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_contagemresultadoexecucao_inicioauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 174,'',false,'" + sGXsfl_152_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contagemresultadoexecucao_inicioauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contagemresultadoexecucao_inicioauxdate_Internalname, context.localUtil.Format(AV55DDO_ContagemResultadoExecucao_InicioAuxDate, "99/99/99"), context.localUtil.Format( AV55DDO_ContagemResultadoExecucao_InicioAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,174);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contagemresultadoexecucao_inicioauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contagemresultadoexecucao_inicioauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 175,'',false,'" + sGXsfl_152_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contagemresultadoexecucao_inicioauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contagemresultadoexecucao_inicioauxdateto_Internalname, context.localUtil.Format(AV56DDO_ContagemResultadoExecucao_InicioAuxDateTo, "99/99/99"), context.localUtil.Format( AV56DDO_ContagemResultadoExecucao_InicioAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,175);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contagemresultadoexecucao_inicioauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contagemresultadoexecucao_inicioauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 176,'',false,'" + sGXsfl_152_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontagemresultadoexecucao_prevista_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadoexecucao_prevista_Internalname, context.localUtil.TToC( AV59TFContagemResultadoExecucao_Prevista, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV59TFContagemResultadoExecucao_Prevista, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,176);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadoexecucao_prevista_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadoexecucao_prevista_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontagemresultadoexecucao_prevista_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontagemresultadoexecucao_prevista_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 177,'',false,'" + sGXsfl_152_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontagemresultadoexecucao_prevista_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadoexecucao_prevista_to_Internalname, context.localUtil.TToC( AV60TFContagemResultadoExecucao_Prevista_To, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV60TFContagemResultadoExecucao_Prevista_To, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,177);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadoexecucao_prevista_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadoexecucao_prevista_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontagemresultadoexecucao_prevista_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontagemresultadoexecucao_prevista_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_contagemresultadoexecucao_previstaauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 179,'',false,'" + sGXsfl_152_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contagemresultadoexecucao_previstaauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contagemresultadoexecucao_previstaauxdate_Internalname, context.localUtil.Format(AV61DDO_ContagemResultadoExecucao_PrevistaAuxDate, "99/99/99"), context.localUtil.Format( AV61DDO_ContagemResultadoExecucao_PrevistaAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,179);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contagemresultadoexecucao_previstaauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contagemresultadoexecucao_previstaauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 180,'',false,'" + sGXsfl_152_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contagemresultadoexecucao_previstaauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contagemresultadoexecucao_previstaauxdateto_Internalname, context.localUtil.Format(AV62DDO_ContagemResultadoExecucao_PrevistaAuxDateTo, "99/99/99"), context.localUtil.Format( AV62DDO_ContagemResultadoExecucao_PrevistaAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,180);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contagemresultadoexecucao_previstaauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contagemresultadoexecucao_previstaauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 181,'',false,'" + sGXsfl_152_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadoexecucao_prazodias_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV65TFContagemResultadoExecucao_PrazoDias), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV65TFContagemResultadoExecucao_PrazoDias), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,181);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadoexecucao_prazodias_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadoexecucao_prazodias_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoExecucao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 182,'',false,'" + sGXsfl_152_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadoexecucao_prazodias_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV66TFContagemResultadoExecucao_PrazoDias_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV66TFContagemResultadoExecucao_PrazoDias_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,182);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadoexecucao_prazodias_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadoexecucao_prazodias_to_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoExecucao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 183,'',false,'" + sGXsfl_152_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontagemresultadoexecucao_fim_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadoexecucao_fim_Internalname, context.localUtil.TToC( AV69TFContagemResultadoExecucao_Fim, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV69TFContagemResultadoExecucao_Fim, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,183);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadoexecucao_fim_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadoexecucao_fim_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontagemresultadoexecucao_fim_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontagemresultadoexecucao_fim_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 184,'',false,'" + sGXsfl_152_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontagemresultadoexecucao_fim_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadoexecucao_fim_to_Internalname, context.localUtil.TToC( AV70TFContagemResultadoExecucao_Fim_To, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV70TFContagemResultadoExecucao_Fim_To, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,184);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadoexecucao_fim_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadoexecucao_fim_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontagemresultadoexecucao_fim_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontagemresultadoexecucao_fim_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_contagemresultadoexecucao_fimauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 186,'',false,'" + sGXsfl_152_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contagemresultadoexecucao_fimauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contagemresultadoexecucao_fimauxdate_Internalname, context.localUtil.Format(AV71DDO_ContagemResultadoExecucao_FimAuxDate, "99/99/99"), context.localUtil.Format( AV71DDO_ContagemResultadoExecucao_FimAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,186);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contagemresultadoexecucao_fimauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contagemresultadoexecucao_fimauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 187,'',false,'" + sGXsfl_152_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contagemresultadoexecucao_fimauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contagemresultadoexecucao_fimauxdateto_Internalname, context.localUtil.Format(AV72DDO_ContagemResultadoExecucao_FimAuxDateTo, "99/99/99"), context.localUtil.Format( AV72DDO_ContagemResultadoExecucao_FimAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,187);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contagemresultadoexecucao_fimauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contagemresultadoexecucao_fimauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 188,'',false,'" + sGXsfl_152_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadoexecucao_dias_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV75TFContagemResultadoExecucao_Dias), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV75TFContagemResultadoExecucao_Dias), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,188);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadoexecucao_dias_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadoexecucao_dias_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoExecucao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 189,'',false,'" + sGXsfl_152_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadoexecucao_dias_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV76TFContagemResultadoExecucao_Dias_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV76TFContagemResultadoExecucao_Dias_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,189);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadoexecucao_dias_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadoexecucao_dias_to_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoExecucao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADO_OSFSOSFMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 191,'',false,'" + sGXsfl_152_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultado_osfsosfmtitlecontrolidtoreplace_Internalname, AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,191);\"", 0, edtavDdo_contagemresultado_osfsosfmtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContagemResultadoExecucao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 193,'',false,'" + sGXsfl_152_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultadoexecucao_iniciotitlecontrolidtoreplace_Internalname, AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,193);\"", 0, edtavDdo_contagemresultadoexecucao_iniciotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContagemResultadoExecucao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 195,'',false,'" + sGXsfl_152_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultadoexecucao_previstatitlecontrolidtoreplace_Internalname, AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,195);\"", 0, edtavDdo_contagemresultadoexecucao_previstatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContagemResultadoExecucao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 197,'',false,'" + sGXsfl_152_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultadoexecucao_prazodiastitlecontrolidtoreplace_Internalname, AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,197);\"", 0, edtavDdo_contagemresultadoexecucao_prazodiastitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContagemResultadoExecucao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 199,'',false,'" + sGXsfl_152_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultadoexecucao_fimtitlecontrolidtoreplace_Internalname, AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,199);\"", 0, edtavDdo_contagemresultadoexecucao_fimtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContagemResultadoExecucao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 201,'',false,'" + sGXsfl_152_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultadoexecucao_diastitlecontrolidtoreplace_Internalname, AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,201);\"", 0, edtavDdo_contagemresultadoexecucao_diastitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContagemResultadoExecucao.htm");
         }
         wbLoad = true;
      }

      protected void STARTKI2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Contagem Resultado Execucao", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPKI0( ) ;
      }

      protected void WSKI2( )
      {
         STARTKI2( ) ;
         EVTKI2( ) ;
      }

      protected void EVTKI2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11KI2 */
                              E11KI2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADO_OSFSOSFM.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12KI2 */
                              E12KI2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13KI2 */
                              E13KI2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14KI2 */
                              E14KI2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15KI2 */
                              E15KI2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16KI2 */
                              E16KI2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17KI2 */
                              E17KI2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18KI2 */
                              E18KI2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19KI2 */
                              E19KI2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20KI2 */
                              E20KI2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21KI2 */
                              E21KI2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22KI2 */
                              E22KI2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23KI2 */
                              E23KI2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E24KI2 */
                              E24KI2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E25KI2 */
                              E25KI2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E26KI2 */
                              E26KI2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E27KI2 */
                              E27KI2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_152_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_152_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_152_idx), 4, 0)), 4, "0");
                              SubsflControlProps_1522( ) ;
                              AV40Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV40Update)) ? AV136Update_GXI : context.convertURL( context.PathToRelativeUrl( AV40Update))));
                              AV41Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV41Delete)) ? AV137Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV41Delete))));
                              A1405ContagemResultadoExecucao_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoExecucao_Codigo_Internalname), ",", "."));
                              A1404ContagemResultadoExecucao_OSCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoExecucao_OSCod_Internalname), ",", "."));
                              A501ContagemResultado_OsFsOsFm = cgiGet( edtContagemResultado_OsFsOsFm_Internalname);
                              A1406ContagemResultadoExecucao_Inicio = context.localUtil.CToT( cgiGet( edtContagemResultadoExecucao_Inicio_Internalname), 0);
                              A1408ContagemResultadoExecucao_Prevista = context.localUtil.CToT( cgiGet( edtContagemResultadoExecucao_Prevista_Internalname), 0);
                              n1408ContagemResultadoExecucao_Prevista = false;
                              A1411ContagemResultadoExecucao_PrazoDias = (short)(context.localUtil.CToN( cgiGet( edtContagemResultadoExecucao_PrazoDias_Internalname), ",", "."));
                              n1411ContagemResultadoExecucao_PrazoDias = false;
                              A1407ContagemResultadoExecucao_Fim = context.localUtil.CToT( cgiGet( edtContagemResultadoExecucao_Fim_Internalname), 0);
                              n1407ContagemResultadoExecucao_Fim = false;
                              A1410ContagemResultadoExecucao_Dias = (short)(context.localUtil.CToN( cgiGet( edtContagemResultadoExecucao_Dias_Internalname), ",", "."));
                              n1410ContagemResultadoExecucao_Dias = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E28KI2 */
                                    E28KI2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E29KI2 */
                                    E29KI2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E30KI2 */
                                    E30KI2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultadoexecucao_inicio1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_INICIO1"), 0) != AV16ContagemResultadoExecucao_Inicio1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultadoexecucao_inicio_to1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1"), 0) != AV17ContagemResultadoExecucao_Inicio_To1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultadoexecucao_fim1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_FIM1"), 0) != AV18ContagemResultadoExecucao_Fim1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultadoexecucao_fim_to1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_FIM_TO1"), 0) != AV19ContagemResultadoExecucao_Fim_To1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultadoexecucao_prevista1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_PREVISTA1"), 0) != AV20ContagemResultadoExecucao_Prevista1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultadoexecucao_prevista_to1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1"), 0) != AV21ContagemResultadoExecucao_Prevista_To1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_osfsosfm1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_OSFSOSFM1"), AV43ContagemResultado_OsFsOsFm1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_contratadacod1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADO_CONTRATADACOD1"), ",", ".") != Convert.ToDecimal( AV82ContagemResultado_ContratadaCod1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultadoexecucao_oscod1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_OSCOD1"), ",", ".") != Convert.ToDecimal( AV83ContagemResultadoExecucao_OSCod1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV23DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultadoexecucao_inicio2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_INICIO2"), 0) != AV24ContagemResultadoExecucao_Inicio2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultadoexecucao_inicio_to2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2"), 0) != AV25ContagemResultadoExecucao_Inicio_To2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultadoexecucao_fim2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_FIM2"), 0) != AV26ContagemResultadoExecucao_Fim2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultadoexecucao_fim_to2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_FIM_TO2"), 0) != AV27ContagemResultadoExecucao_Fim_To2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultadoexecucao_prevista2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_PREVISTA2"), 0) != AV28ContagemResultadoExecucao_Prevista2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultadoexecucao_prevista_to2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2"), 0) != AV29ContagemResultadoExecucao_Prevista_To2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_osfsosfm2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_OSFSOSFM2"), AV44ContagemResultado_OsFsOsFm2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_contratadacod2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADO_CONTRATADACOD2"), ",", ".") != Convert.ToDecimal( AV84ContagemResultado_ContratadaCod2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultadoexecucao_oscod2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_OSCOD2"), ",", ".") != Convert.ToDecimal( AV85ContagemResultadoExecucao_OSCod2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV31DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultadoexecucao_inicio3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_INICIO3"), 0) != AV32ContagemResultadoExecucao_Inicio3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultadoexecucao_inicio_to3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3"), 0) != AV33ContagemResultadoExecucao_Inicio_To3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultadoexecucao_fim3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_FIM3"), 0) != AV34ContagemResultadoExecucao_Fim3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultadoexecucao_fim_to3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_FIM_TO3"), 0) != AV35ContagemResultadoExecucao_Fim_To3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultadoexecucao_prevista3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_PREVISTA3"), 0) != AV36ContagemResultadoExecucao_Prevista3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultadoexecucao_prevista_to3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3"), 0) != AV37ContagemResultadoExecucao_Prevista_To3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_osfsosfm3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_OSFSOSFM3"), AV45ContagemResultado_OsFsOsFm3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_contratadacod3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADO_CONTRATADACOD3"), ",", ".") != Convert.ToDecimal( AV86ContagemResultado_ContratadaCod3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultadoexecucao_oscod3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_OSCOD3"), ",", ".") != Convert.ToDecimal( AV87ContagemResultadoExecucao_OSCod3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV22DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV30DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemresultado_osfsosfm Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_OSFSOSFM"), AV49TFContagemResultado_OsFsOsFm) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemresultado_osfsosfm_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_OSFSOSFM_SEL"), AV50TFContagemResultado_OsFsOsFm_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemresultadoexecucao_inicio Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_INICIO"), 0) != AV53TFContagemResultadoExecucao_Inicio )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemresultadoexecucao_inicio_to Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO"), 0) != AV54TFContagemResultadoExecucao_Inicio_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemresultadoexecucao_prevista Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA"), 0) != AV59TFContagemResultadoExecucao_Prevista )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemresultadoexecucao_prevista_to Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO"), 0) != AV60TFContagemResultadoExecucao_Prevista_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemresultadoexecucao_prazodias Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS"), ",", ".") != Convert.ToDecimal( AV65TFContagemResultadoExecucao_PrazoDias )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemresultadoexecucao_prazodias_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO"), ",", ".") != Convert.ToDecimal( AV66TFContagemResultadoExecucao_PrazoDias_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemresultadoexecucao_fim Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_FIM"), 0) != AV69TFContagemResultadoExecucao_Fim )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemresultadoexecucao_fim_to Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO"), 0) != AV70TFContagemResultadoExecucao_Fim_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemresultadoexecucao_dias Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_DIAS"), ",", ".") != Convert.ToDecimal( AV75TFContagemResultadoExecucao_Dias )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemresultadoexecucao_dias_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO"), ",", ".") != Convert.ToDecimal( AV76TFContagemResultadoExecucao_Dias_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                          /* Execute user event: E31KI2 */
                                          E31KI2 ();
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEKI2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAKI2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavContratada_codigo.Name = "vCONTRATADA_CODIGO";
            dynavContratada_codigo.WebTags = "";
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTAGEMRESULTADOEXECUCAO_INICIO", "Inicio", 0);
            cmbavDynamicfiltersselector1.addItem("CONTAGEMRESULTADOEXECUCAO_FIM", "Fim", 0);
            cmbavDynamicfiltersselector1.addItem("CONTAGEMRESULTADOEXECUCAO_PREVISTA", "Previsto", 0);
            cmbavDynamicfiltersselector1.addItem("CONTAGEMRESULTADO_OSFSOSFM", "OS Ref|OS", 0);
            cmbavDynamicfiltersselector1.addItem("CONTAGEMRESULTADO_CONTRATADACOD", "Prestadora", 0);
            cmbavDynamicfiltersselector1.addItem("CONTAGEMRESULTADOEXECUCAO_OSCOD", "ID", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            dynavContagemresultado_contratadacod1.Name = "vCONTAGEMRESULTADO_CONTRATADACOD1";
            dynavContagemresultado_contratadacod1.WebTags = "";
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTAGEMRESULTADOEXECUCAO_INICIO", "Inicio", 0);
            cmbavDynamicfiltersselector2.addItem("CONTAGEMRESULTADOEXECUCAO_FIM", "Fim", 0);
            cmbavDynamicfiltersselector2.addItem("CONTAGEMRESULTADOEXECUCAO_PREVISTA", "Previsto", 0);
            cmbavDynamicfiltersselector2.addItem("CONTAGEMRESULTADO_OSFSOSFM", "OS Ref|OS", 0);
            cmbavDynamicfiltersselector2.addItem("CONTAGEMRESULTADO_CONTRATADACOD", "Prestadora", 0);
            cmbavDynamicfiltersselector2.addItem("CONTAGEMRESULTADOEXECUCAO_OSCOD", "ID", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV23DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector2", AV23DynamicFiltersSelector2);
            }
            dynavContagemresultado_contratadacod2.Name = "vCONTAGEMRESULTADO_CONTRATADACOD2";
            dynavContagemresultado_contratadacod2.WebTags = "";
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CONTAGEMRESULTADOEXECUCAO_INICIO", "Inicio", 0);
            cmbavDynamicfiltersselector3.addItem("CONTAGEMRESULTADOEXECUCAO_FIM", "Fim", 0);
            cmbavDynamicfiltersselector3.addItem("CONTAGEMRESULTADOEXECUCAO_PREVISTA", "Previsto", 0);
            cmbavDynamicfiltersselector3.addItem("CONTAGEMRESULTADO_OSFSOSFM", "OS Ref|OS", 0);
            cmbavDynamicfiltersselector3.addItem("CONTAGEMRESULTADO_CONTRATADACOD", "Prestadora", 0);
            cmbavDynamicfiltersselector3.addItem("CONTAGEMRESULTADOEXECUCAO_OSCOD", "ID", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV31DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV31DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31DynamicFiltersSelector3", AV31DynamicFiltersSelector3);
            }
            dynavContagemresultado_contratadacod3.Name = "vCONTAGEMRESULTADO_CONTRATADACOD3";
            dynavContagemresultado_contratadacod3.WebTags = "";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = dynavContratada_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvCONTRATADA_CODIGOKI2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTRATADA_CODIGO_dataKI2( AV6WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTRATADA_CODIGO_htmlKI2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTRATADA_CODIGO_dataKI2( AV6WWPContext) ;
         gxdynajaxindex = 1;
         dynavContratada_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContratada_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContratada_codigo.ItemCount > 0 )
         {
            AV88Contratada_Codigo = (int)(NumberUtil.Val( dynavContratada_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV88Contratada_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV88Contratada_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvCONTRATADA_CODIGO_dataKI2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhuma");
         /* Using cursor H00KI2 */
         pr_default.execute(0, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00KI2_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00KI2_A41Contratada_PessoaNom[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void GXDLVvCONTAGEMRESULTADO_CONTRATADACOD1KI2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_CONTRATADACOD1_dataKI2( AV6WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_CONTRATADACOD1_htmlKI2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_CONTRATADACOD1_dataKI2( AV6WWPContext) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_contratadacod1.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_contratadacod1.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_contratadacod1.ItemCount > 0 )
         {
            AV82ContagemResultado_ContratadaCod1 = (int)(NumberUtil.Val( dynavContagemresultado_contratadacod1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV82ContagemResultado_ContratadaCod1), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82ContagemResultado_ContratadaCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV82ContagemResultado_ContratadaCod1), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_CONTRATADACOD1_dataKI2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Todas)");
         /* Using cursor H00KI3 */
         pr_default.execute(1, new Object[] {AV6WWPContext.gxTpr_Contratada_codigo, AV6WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00KI3_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00KI3_A41Contratada_PessoaNom[0]));
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void GXDLVvCONTAGEMRESULTADO_CONTRATADACOD2KI2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_CONTRATADACOD2_dataKI2( AV6WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_CONTRATADACOD2_htmlKI2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_CONTRATADACOD2_dataKI2( AV6WWPContext) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_contratadacod2.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_contratadacod2.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_contratadacod2.ItemCount > 0 )
         {
            AV84ContagemResultado_ContratadaCod2 = (int)(NumberUtil.Val( dynavContagemresultado_contratadacod2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV84ContagemResultado_ContratadaCod2), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84ContagemResultado_ContratadaCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV84ContagemResultado_ContratadaCod2), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_CONTRATADACOD2_dataKI2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Todas)");
         /* Using cursor H00KI4 */
         pr_default.execute(2, new Object[] {AV6WWPContext.gxTpr_Contratada_codigo, AV6WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00KI4_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00KI4_A41Contratada_PessoaNom[0]));
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void GXDLVvCONTAGEMRESULTADO_CONTRATADACOD3KI2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_CONTRATADACOD3_dataKI2( AV6WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_CONTRATADACOD3_htmlKI2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_CONTRATADACOD3_dataKI2( AV6WWPContext) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_contratadacod3.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_contratadacod3.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_contratadacod3.ItemCount > 0 )
         {
            AV86ContagemResultado_ContratadaCod3 = (int)(NumberUtil.Val( dynavContagemresultado_contratadacod3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV86ContagemResultado_ContratadaCod3), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86ContagemResultado_ContratadaCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV86ContagemResultado_ContratadaCod3), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_CONTRATADACOD3_dataKI2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Todas)");
         /* Using cursor H00KI5 */
         pr_default.execute(3, new Object[] {AV6WWPContext.gxTpr_Contratada_codigo, AV6WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(3) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00KI5_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00KI5_A41Contratada_PessoaNom[0]));
            pr_default.readNext(3);
         }
         pr_default.close(3);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_1522( ) ;
         while ( nGXsfl_152_idx <= nRC_GXsfl_152 )
         {
            sendrow_1522( ) ;
            nGXsfl_152_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_152_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_152_idx+1));
            sGXsfl_152_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_152_idx), 4, 0)), 4, "0");
            SubsflControlProps_1522( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       String AV15DynamicFiltersSelector1 ,
                                       DateTime AV16ContagemResultadoExecucao_Inicio1 ,
                                       DateTime AV17ContagemResultadoExecucao_Inicio_To1 ,
                                       DateTime AV18ContagemResultadoExecucao_Fim1 ,
                                       DateTime AV19ContagemResultadoExecucao_Fim_To1 ,
                                       DateTime AV20ContagemResultadoExecucao_Prevista1 ,
                                       DateTime AV21ContagemResultadoExecucao_Prevista_To1 ,
                                       String AV43ContagemResultado_OsFsOsFm1 ,
                                       int AV82ContagemResultado_ContratadaCod1 ,
                                       int AV83ContagemResultadoExecucao_OSCod1 ,
                                       String AV23DynamicFiltersSelector2 ,
                                       DateTime AV24ContagemResultadoExecucao_Inicio2 ,
                                       DateTime AV25ContagemResultadoExecucao_Inicio_To2 ,
                                       DateTime AV26ContagemResultadoExecucao_Fim2 ,
                                       DateTime AV27ContagemResultadoExecucao_Fim_To2 ,
                                       DateTime AV28ContagemResultadoExecucao_Prevista2 ,
                                       DateTime AV29ContagemResultadoExecucao_Prevista_To2 ,
                                       String AV44ContagemResultado_OsFsOsFm2 ,
                                       int AV84ContagemResultado_ContratadaCod2 ,
                                       int AV85ContagemResultadoExecucao_OSCod2 ,
                                       String AV31DynamicFiltersSelector3 ,
                                       DateTime AV32ContagemResultadoExecucao_Inicio3 ,
                                       DateTime AV33ContagemResultadoExecucao_Inicio_To3 ,
                                       DateTime AV34ContagemResultadoExecucao_Fim3 ,
                                       DateTime AV35ContagemResultadoExecucao_Fim_To3 ,
                                       DateTime AV36ContagemResultadoExecucao_Prevista3 ,
                                       DateTime AV37ContagemResultadoExecucao_Prevista_To3 ,
                                       String AV45ContagemResultado_OsFsOsFm3 ,
                                       int AV86ContagemResultado_ContratadaCod3 ,
                                       int AV87ContagemResultadoExecucao_OSCod3 ,
                                       bool AV22DynamicFiltersEnabled2 ,
                                       bool AV30DynamicFiltersEnabled3 ,
                                       String AV49TFContagemResultado_OsFsOsFm ,
                                       String AV50TFContagemResultado_OsFsOsFm_Sel ,
                                       DateTime AV53TFContagemResultadoExecucao_Inicio ,
                                       DateTime AV54TFContagemResultadoExecucao_Inicio_To ,
                                       DateTime AV59TFContagemResultadoExecucao_Prevista ,
                                       DateTime AV60TFContagemResultadoExecucao_Prevista_To ,
                                       short AV65TFContagemResultadoExecucao_PrazoDias ,
                                       short AV66TFContagemResultadoExecucao_PrazoDias_To ,
                                       DateTime AV69TFContagemResultadoExecucao_Fim ,
                                       DateTime AV70TFContagemResultadoExecucao_Fim_To ,
                                       short AV75TFContagemResultadoExecucao_Dias ,
                                       short AV76TFContagemResultadoExecucao_Dias_To ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       String AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace ,
                                       String AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace ,
                                       String AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace ,
                                       String AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace ,
                                       String AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace ,
                                       String AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace ,
                                       int AV88Contratada_Codigo ,
                                       String AV138Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV39DynamicFiltersIgnoreFirst ,
                                       bool AV38DynamicFiltersRemoving ,
                                       int A1405ContagemResultadoExecucao_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFKI2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADOEXECUCAO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1405ContagemResultadoExecucao_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEXECUCAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1405ContagemResultadoExecucao_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADOEXECUCAO_OSCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1404ContagemResultadoExecucao_OSCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEXECUCAO_OSCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1404ContagemResultadoExecucao_OSCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_OSFSOSFM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A501ContagemResultado_OsFsOsFm, ""))));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_OSFSOSFM", A501ContagemResultado_OsFsOsFm);
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADOEXECUCAO_INICIO", GetSecureSignedToken( "", context.localUtil.Format( A1406ContagemResultadoExecucao_Inicio, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEXECUCAO_INICIO", context.localUtil.TToC( A1406ContagemResultadoExecucao_Inicio, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADOEXECUCAO_PREVISTA", GetSecureSignedToken( "", context.localUtil.Format( A1408ContagemResultadoExecucao_Prevista, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEXECUCAO_PREVISTA", context.localUtil.TToC( A1408ContagemResultadoExecucao_Prevista, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1411ContagemResultadoExecucao_PrazoDias), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEXECUCAO_PRAZODIAS", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1411ContagemResultadoExecucao_PrazoDias), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADOEXECUCAO_FIM", GetSecureSignedToken( "", context.localUtil.Format( A1407ContagemResultadoExecucao_Fim, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEXECUCAO_FIM", context.localUtil.TToC( A1407ContagemResultadoExecucao_Fim, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADOEXECUCAO_DIAS", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1410ContagemResultadoExecucao_Dias), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEXECUCAO_DIAS", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1410ContagemResultadoExecucao_Dias), 4, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavContratada_codigo.ItemCount > 0 )
         {
            AV88Contratada_Codigo = (int)(NumberUtil.Val( dynavContratada_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV88Contratada_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV88Contratada_Codigo), 6, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( dynavContagemresultado_contratadacod1.ItemCount > 0 )
         {
            AV82ContagemResultado_ContratadaCod1 = (int)(NumberUtil.Val( dynavContagemresultado_contratadacod1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV82ContagemResultado_ContratadaCod1), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82ContagemResultado_ContratadaCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV82ContagemResultado_ContratadaCod1), 6, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV23DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector2", AV23DynamicFiltersSelector2);
         }
         if ( dynavContagemresultado_contratadacod2.ItemCount > 0 )
         {
            AV84ContagemResultado_ContratadaCod2 = (int)(NumberUtil.Val( dynavContagemresultado_contratadacod2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV84ContagemResultado_ContratadaCod2), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84ContagemResultado_ContratadaCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV84ContagemResultado_ContratadaCod2), 6, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV31DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV31DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31DynamicFiltersSelector3", AV31DynamicFiltersSelector3);
         }
         if ( dynavContagemresultado_contratadacod3.ItemCount > 0 )
         {
            AV86ContagemResultado_ContratadaCod3 = (int)(NumberUtil.Val( dynavContagemresultado_contratadacod3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV86ContagemResultado_ContratadaCod3), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86ContagemResultado_ContratadaCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV86ContagemResultado_ContratadaCod3), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFKI2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV138Pgmname = "WWContagemResultadoExecucao";
         context.Gx_err = 0;
      }

      protected void RFKI2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 152;
         /* Execute user event: E29KI2 */
         E29KI2 ();
         nGXsfl_152_idx = 1;
         sGXsfl_152_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_152_idx), 4, 0)), 4, "0");
         SubsflControlProps_1522( ) ;
         nGXsfl_152_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_1522( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(4, new Object[]{ new Object[]{
                                                 AV6WWPContext.gxTpr_Contratada_codigo ,
                                                 AV92WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1 ,
                                                 AV93WWContagemResultadoExecucaoDS_3_Contagemresultadoexecucao_inicio1 ,
                                                 AV94WWContagemResultadoExecucaoDS_4_Contagemresultadoexecucao_inicio_to1 ,
                                                 AV95WWContagemResultadoExecucaoDS_5_Contagemresultadoexecucao_fim1 ,
                                                 AV96WWContagemResultadoExecucaoDS_6_Contagemresultadoexecucao_fim_to1 ,
                                                 AV97WWContagemResultadoExecucaoDS_7_Contagemresultadoexecucao_prevista1 ,
                                                 AV98WWContagemResultadoExecucaoDS_8_Contagemresultadoexecucao_prevista_to1 ,
                                                 AV99WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1 ,
                                                 AV100WWContagemResultadoExecucaoDS_10_Contagemresultado_contratadacod1 ,
                                                 AV101WWContagemResultadoExecucaoDS_11_Contagemresultadoexecucao_oscod1 ,
                                                 AV102WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 ,
                                                 AV103WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2 ,
                                                 AV104WWContagemResultadoExecucaoDS_14_Contagemresultadoexecucao_inicio2 ,
                                                 AV105WWContagemResultadoExecucaoDS_15_Contagemresultadoexecucao_inicio_to2 ,
                                                 AV106WWContagemResultadoExecucaoDS_16_Contagemresultadoexecucao_fim2 ,
                                                 AV107WWContagemResultadoExecucaoDS_17_Contagemresultadoexecucao_fim_to2 ,
                                                 AV108WWContagemResultadoExecucaoDS_18_Contagemresultadoexecucao_prevista2 ,
                                                 AV109WWContagemResultadoExecucaoDS_19_Contagemresultadoexecucao_prevista_to2 ,
                                                 AV110WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2 ,
                                                 AV111WWContagemResultadoExecucaoDS_21_Contagemresultado_contratadacod2 ,
                                                 AV112WWContagemResultadoExecucaoDS_22_Contagemresultadoexecucao_oscod2 ,
                                                 AV113WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 ,
                                                 AV114WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3 ,
                                                 AV115WWContagemResultadoExecucaoDS_25_Contagemresultadoexecucao_inicio3 ,
                                                 AV116WWContagemResultadoExecucaoDS_26_Contagemresultadoexecucao_inicio_to3 ,
                                                 AV117WWContagemResultadoExecucaoDS_27_Contagemresultadoexecucao_fim3 ,
                                                 AV118WWContagemResultadoExecucaoDS_28_Contagemresultadoexecucao_fim_to3 ,
                                                 AV119WWContagemResultadoExecucaoDS_29_Contagemresultadoexecucao_prevista3 ,
                                                 AV120WWContagemResultadoExecucaoDS_30_Contagemresultadoexecucao_prevista_to3 ,
                                                 AV121WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3 ,
                                                 AV122WWContagemResultadoExecucaoDS_32_Contagemresultado_contratadacod3 ,
                                                 AV123WWContagemResultadoExecucaoDS_33_Contagemresultadoexecucao_oscod3 ,
                                                 AV125WWContagemResultadoExecucaoDS_35_Tfcontagemresultado_osfsosfm_sel ,
                                                 AV124WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm ,
                                                 AV126WWContagemResultadoExecucaoDS_36_Tfcontagemresultadoexecucao_inicio ,
                                                 AV127WWContagemResultadoExecucaoDS_37_Tfcontagemresultadoexecucao_inicio_to ,
                                                 AV128WWContagemResultadoExecucaoDS_38_Tfcontagemresultadoexecucao_prevista ,
                                                 AV129WWContagemResultadoExecucaoDS_39_Tfcontagemresultadoexecucao_prevista_to ,
                                                 AV130WWContagemResultadoExecucaoDS_40_Tfcontagemresultadoexecucao_prazodias ,
                                                 AV131WWContagemResultadoExecucaoDS_41_Tfcontagemresultadoexecucao_prazodias_to ,
                                                 AV132WWContagemResultadoExecucaoDS_42_Tfcontagemresultadoexecucao_fim ,
                                                 AV133WWContagemResultadoExecucaoDS_43_Tfcontagemresultadoexecucao_fim_to ,
                                                 AV134WWContagemResultadoExecucaoDS_44_Tfcontagemresultadoexecucao_dias ,
                                                 AV135WWContagemResultadoExecucaoDS_45_Tfcontagemresultadoexecucao_dias_to ,
                                                 A490ContagemResultado_ContratadaCod ,
                                                 A52Contratada_AreaTrabalhoCod ,
                                                 AV6WWPContext.gxTpr_Areatrabalho_codigo ,
                                                 A1406ContagemResultadoExecucao_Inicio ,
                                                 A1407ContagemResultadoExecucao_Fim ,
                                                 A1408ContagemResultadoExecucao_Prevista ,
                                                 A457ContagemResultado_Demanda ,
                                                 A493ContagemResultado_DemandaFM ,
                                                 A1404ContagemResultadoExecucao_OSCod ,
                                                 A1411ContagemResultadoExecucao_PrazoDias ,
                                                 A1410ContagemResultadoExecucao_Dias },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.INT,
                                                 TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                                 TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                                 TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT,
                                                 TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT,
                                                 TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV99WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV99WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1), "%", "");
            lV99WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV99WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1), "%", "");
            lV110WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV110WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2), "%", "");
            lV110WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV110WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2), "%", "");
            lV121WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV121WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3), "%", "");
            lV121WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV121WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3), "%", "");
            lV124WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm = StringUtil.Concat( StringUtil.RTrim( AV124WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm), "%", "");
            /* Using cursor H00KI6 */
            pr_default.execute(4, new Object[] {AV6WWPContext.gxTpr_Contratada_codigo, AV6WWPContext.gxTpr_Areatrabalho_codigo, AV93WWContagemResultadoExecucaoDS_3_Contagemresultadoexecucao_inicio1, AV94WWContagemResultadoExecucaoDS_4_Contagemresultadoexecucao_inicio_to1, AV95WWContagemResultadoExecucaoDS_5_Contagemresultadoexecucao_fim1, AV96WWContagemResultadoExecucaoDS_6_Contagemresultadoexecucao_fim_to1, AV97WWContagemResultadoExecucaoDS_7_Contagemresultadoexecucao_prevista1, AV98WWContagemResultadoExecucaoDS_8_Contagemresultadoexecucao_prevista_to1, lV99WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1, lV99WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1, AV100WWContagemResultadoExecucaoDS_10_Contagemresultado_contratadacod1, AV101WWContagemResultadoExecucaoDS_11_Contagemresultadoexecucao_oscod1, AV104WWContagemResultadoExecucaoDS_14_Contagemresultadoexecucao_inicio2, AV105WWContagemResultadoExecucaoDS_15_Contagemresultadoexecucao_inicio_to2, AV106WWContagemResultadoExecucaoDS_16_Contagemresultadoexecucao_fim2, AV107WWContagemResultadoExecucaoDS_17_Contagemresultadoexecucao_fim_to2, AV108WWContagemResultadoExecucaoDS_18_Contagemresultadoexecucao_prevista2, AV109WWContagemResultadoExecucaoDS_19_Contagemresultadoexecucao_prevista_to2, lV110WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2, lV110WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2, AV111WWContagemResultadoExecucaoDS_21_Contagemresultado_contratadacod2, AV112WWContagemResultadoExecucaoDS_22_Contagemresultadoexecucao_oscod2, AV115WWContagemResultadoExecucaoDS_25_Contagemresultadoexecucao_inicio3, AV116WWContagemResultadoExecucaoDS_26_Contagemresultadoexecucao_inicio_to3, AV117WWContagemResultadoExecucaoDS_27_Contagemresultadoexecucao_fim3, AV118WWContagemResultadoExecucaoDS_28_Contagemresultadoexecucao_fim_to3, AV119WWContagemResultadoExecucaoDS_29_Contagemresultadoexecucao_prevista3, AV120WWContagemResultadoExecucaoDS_30_Contagemresultadoexecucao_prevista_to3, lV121WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3, lV121WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3, AV122WWContagemResultadoExecucaoDS_32_Contagemresultado_contratadacod3, AV123WWContagemResultadoExecucaoDS_33_Contagemresultadoexecucao_oscod3, lV124WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm, AV125WWContagemResultadoExecucaoDS_35_Tfcontagemresultado_osfsosfm_sel, AV126WWContagemResultadoExecucaoDS_36_Tfcontagemresultadoexecucao_inicio, AV127WWContagemResultadoExecucaoDS_37_Tfcontagemresultadoexecucao_inicio_to, AV128WWContagemResultadoExecucaoDS_38_Tfcontagemresultadoexecucao_prevista, AV129WWContagemResultadoExecucaoDS_39_Tfcontagemresultadoexecucao_prevista_to, AV130WWContagemResultadoExecucaoDS_40_Tfcontagemresultadoexecucao_prazodias, AV131WWContagemResultadoExecucaoDS_41_Tfcontagemresultadoexecucao_prazodias_to, AV132WWContagemResultadoExecucaoDS_42_Tfcontagemresultadoexecucao_fim, AV133WWContagemResultadoExecucaoDS_43_Tfcontagemresultadoexecucao_fim_to, AV134WWContagemResultadoExecucaoDS_44_Tfcontagemresultadoexecucao_dias, AV135WWContagemResultadoExecucaoDS_45_Tfcontagemresultadoexecucao_dias_to, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_152_idx = 1;
            while ( ( (pr_default.getStatus(4) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A52Contratada_AreaTrabalhoCod = H00KI6_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = H00KI6_n52Contratada_AreaTrabalhoCod[0];
               A490ContagemResultado_ContratadaCod = H00KI6_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = H00KI6_n490ContagemResultado_ContratadaCod[0];
               A1410ContagemResultadoExecucao_Dias = H00KI6_A1410ContagemResultadoExecucao_Dias[0];
               n1410ContagemResultadoExecucao_Dias = H00KI6_n1410ContagemResultadoExecucao_Dias[0];
               A1407ContagemResultadoExecucao_Fim = H00KI6_A1407ContagemResultadoExecucao_Fim[0];
               n1407ContagemResultadoExecucao_Fim = H00KI6_n1407ContagemResultadoExecucao_Fim[0];
               A1411ContagemResultadoExecucao_PrazoDias = H00KI6_A1411ContagemResultadoExecucao_PrazoDias[0];
               n1411ContagemResultadoExecucao_PrazoDias = H00KI6_n1411ContagemResultadoExecucao_PrazoDias[0];
               A1408ContagemResultadoExecucao_Prevista = H00KI6_A1408ContagemResultadoExecucao_Prevista[0];
               n1408ContagemResultadoExecucao_Prevista = H00KI6_n1408ContagemResultadoExecucao_Prevista[0];
               A1406ContagemResultadoExecucao_Inicio = H00KI6_A1406ContagemResultadoExecucao_Inicio[0];
               A1404ContagemResultadoExecucao_OSCod = H00KI6_A1404ContagemResultadoExecucao_OSCod[0];
               A1405ContagemResultadoExecucao_Codigo = H00KI6_A1405ContagemResultadoExecucao_Codigo[0];
               A493ContagemResultado_DemandaFM = H00KI6_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = H00KI6_n493ContagemResultado_DemandaFM[0];
               A457ContagemResultado_Demanda = H00KI6_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = H00KI6_n457ContagemResultado_Demanda[0];
               A490ContagemResultado_ContratadaCod = H00KI6_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = H00KI6_n490ContagemResultado_ContratadaCod[0];
               A493ContagemResultado_DemandaFM = H00KI6_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = H00KI6_n493ContagemResultado_DemandaFM[0];
               A457ContagemResultado_Demanda = H00KI6_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = H00KI6_n457ContagemResultado_Demanda[0];
               A52Contratada_AreaTrabalhoCod = H00KI6_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = H00KI6_n52Contratada_AreaTrabalhoCod[0];
               A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
               /* Execute user event: E30KI2 */
               E30KI2 ();
               pr_default.readNext(4);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(4) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(4);
            wbEnd = 152;
            WBKI0( ) ;
         }
         nGXsfl_152_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV91WWContagemResultadoExecucaoDS_1_Contratada_codigo = AV88Contratada_Codigo;
         AV92WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV93WWContagemResultadoExecucaoDS_3_Contagemresultadoexecucao_inicio1 = AV16ContagemResultadoExecucao_Inicio1;
         AV94WWContagemResultadoExecucaoDS_4_Contagemresultadoexecucao_inicio_to1 = AV17ContagemResultadoExecucao_Inicio_To1;
         AV95WWContagemResultadoExecucaoDS_5_Contagemresultadoexecucao_fim1 = AV18ContagemResultadoExecucao_Fim1;
         AV96WWContagemResultadoExecucaoDS_6_Contagemresultadoexecucao_fim_to1 = AV19ContagemResultadoExecucao_Fim_To1;
         AV97WWContagemResultadoExecucaoDS_7_Contagemresultadoexecucao_prevista1 = AV20ContagemResultadoExecucao_Prevista1;
         AV98WWContagemResultadoExecucaoDS_8_Contagemresultadoexecucao_prevista_to1 = AV21ContagemResultadoExecucao_Prevista_To1;
         AV99WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1 = AV43ContagemResultado_OsFsOsFm1;
         AV100WWContagemResultadoExecucaoDS_10_Contagemresultado_contratadacod1 = AV82ContagemResultado_ContratadaCod1;
         AV101WWContagemResultadoExecucaoDS_11_Contagemresultadoexecucao_oscod1 = AV83ContagemResultadoExecucao_OSCod1;
         AV102WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 = AV22DynamicFiltersEnabled2;
         AV103WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2 = AV23DynamicFiltersSelector2;
         AV104WWContagemResultadoExecucaoDS_14_Contagemresultadoexecucao_inicio2 = AV24ContagemResultadoExecucao_Inicio2;
         AV105WWContagemResultadoExecucaoDS_15_Contagemresultadoexecucao_inicio_to2 = AV25ContagemResultadoExecucao_Inicio_To2;
         AV106WWContagemResultadoExecucaoDS_16_Contagemresultadoexecucao_fim2 = AV26ContagemResultadoExecucao_Fim2;
         AV107WWContagemResultadoExecucaoDS_17_Contagemresultadoexecucao_fim_to2 = AV27ContagemResultadoExecucao_Fim_To2;
         AV108WWContagemResultadoExecucaoDS_18_Contagemresultadoexecucao_prevista2 = AV28ContagemResultadoExecucao_Prevista2;
         AV109WWContagemResultadoExecucaoDS_19_Contagemresultadoexecucao_prevista_to2 = AV29ContagemResultadoExecucao_Prevista_To2;
         AV110WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2 = AV44ContagemResultado_OsFsOsFm2;
         AV111WWContagemResultadoExecucaoDS_21_Contagemresultado_contratadacod2 = AV84ContagemResultado_ContratadaCod2;
         AV112WWContagemResultadoExecucaoDS_22_Contagemresultadoexecucao_oscod2 = AV85ContagemResultadoExecucao_OSCod2;
         AV113WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 = AV30DynamicFiltersEnabled3;
         AV114WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3 = AV31DynamicFiltersSelector3;
         AV115WWContagemResultadoExecucaoDS_25_Contagemresultadoexecucao_inicio3 = AV32ContagemResultadoExecucao_Inicio3;
         AV116WWContagemResultadoExecucaoDS_26_Contagemresultadoexecucao_inicio_to3 = AV33ContagemResultadoExecucao_Inicio_To3;
         AV117WWContagemResultadoExecucaoDS_27_Contagemresultadoexecucao_fim3 = AV34ContagemResultadoExecucao_Fim3;
         AV118WWContagemResultadoExecucaoDS_28_Contagemresultadoexecucao_fim_to3 = AV35ContagemResultadoExecucao_Fim_To3;
         AV119WWContagemResultadoExecucaoDS_29_Contagemresultadoexecucao_prevista3 = AV36ContagemResultadoExecucao_Prevista3;
         AV120WWContagemResultadoExecucaoDS_30_Contagemresultadoexecucao_prevista_to3 = AV37ContagemResultadoExecucao_Prevista_To3;
         AV121WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3 = AV45ContagemResultado_OsFsOsFm3;
         AV122WWContagemResultadoExecucaoDS_32_Contagemresultado_contratadacod3 = AV86ContagemResultado_ContratadaCod3;
         AV123WWContagemResultadoExecucaoDS_33_Contagemresultadoexecucao_oscod3 = AV87ContagemResultadoExecucao_OSCod3;
         AV124WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm = AV49TFContagemResultado_OsFsOsFm;
         AV125WWContagemResultadoExecucaoDS_35_Tfcontagemresultado_osfsosfm_sel = AV50TFContagemResultado_OsFsOsFm_Sel;
         AV126WWContagemResultadoExecucaoDS_36_Tfcontagemresultadoexecucao_inicio = AV53TFContagemResultadoExecucao_Inicio;
         AV127WWContagemResultadoExecucaoDS_37_Tfcontagemresultadoexecucao_inicio_to = AV54TFContagemResultadoExecucao_Inicio_To;
         AV128WWContagemResultadoExecucaoDS_38_Tfcontagemresultadoexecucao_prevista = AV59TFContagemResultadoExecucao_Prevista;
         AV129WWContagemResultadoExecucaoDS_39_Tfcontagemresultadoexecucao_prevista_to = AV60TFContagemResultadoExecucao_Prevista_To;
         AV130WWContagemResultadoExecucaoDS_40_Tfcontagemresultadoexecucao_prazodias = AV65TFContagemResultadoExecucao_PrazoDias;
         AV131WWContagemResultadoExecucaoDS_41_Tfcontagemresultadoexecucao_prazodias_to = AV66TFContagemResultadoExecucao_PrazoDias_To;
         AV132WWContagemResultadoExecucaoDS_42_Tfcontagemresultadoexecucao_fim = AV69TFContagemResultadoExecucao_Fim;
         AV133WWContagemResultadoExecucaoDS_43_Tfcontagemresultadoexecucao_fim_to = AV70TFContagemResultadoExecucao_Fim_To;
         AV134WWContagemResultadoExecucaoDS_44_Tfcontagemresultadoexecucao_dias = AV75TFContagemResultadoExecucao_Dias;
         AV135WWContagemResultadoExecucaoDS_45_Tfcontagemresultadoexecucao_dias_to = AV76TFContagemResultadoExecucao_Dias_To;
         pr_default.dynParam(5, new Object[]{ new Object[]{
                                              AV6WWPContext.gxTpr_Contratada_codigo ,
                                              AV92WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1 ,
                                              AV93WWContagemResultadoExecucaoDS_3_Contagemresultadoexecucao_inicio1 ,
                                              AV94WWContagemResultadoExecucaoDS_4_Contagemresultadoexecucao_inicio_to1 ,
                                              AV95WWContagemResultadoExecucaoDS_5_Contagemresultadoexecucao_fim1 ,
                                              AV96WWContagemResultadoExecucaoDS_6_Contagemresultadoexecucao_fim_to1 ,
                                              AV97WWContagemResultadoExecucaoDS_7_Contagemresultadoexecucao_prevista1 ,
                                              AV98WWContagemResultadoExecucaoDS_8_Contagemresultadoexecucao_prevista_to1 ,
                                              AV99WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1 ,
                                              AV100WWContagemResultadoExecucaoDS_10_Contagemresultado_contratadacod1 ,
                                              AV101WWContagemResultadoExecucaoDS_11_Contagemresultadoexecucao_oscod1 ,
                                              AV102WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 ,
                                              AV103WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2 ,
                                              AV104WWContagemResultadoExecucaoDS_14_Contagemresultadoexecucao_inicio2 ,
                                              AV105WWContagemResultadoExecucaoDS_15_Contagemresultadoexecucao_inicio_to2 ,
                                              AV106WWContagemResultadoExecucaoDS_16_Contagemresultadoexecucao_fim2 ,
                                              AV107WWContagemResultadoExecucaoDS_17_Contagemresultadoexecucao_fim_to2 ,
                                              AV108WWContagemResultadoExecucaoDS_18_Contagemresultadoexecucao_prevista2 ,
                                              AV109WWContagemResultadoExecucaoDS_19_Contagemresultadoexecucao_prevista_to2 ,
                                              AV110WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2 ,
                                              AV111WWContagemResultadoExecucaoDS_21_Contagemresultado_contratadacod2 ,
                                              AV112WWContagemResultadoExecucaoDS_22_Contagemresultadoexecucao_oscod2 ,
                                              AV113WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 ,
                                              AV114WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3 ,
                                              AV115WWContagemResultadoExecucaoDS_25_Contagemresultadoexecucao_inicio3 ,
                                              AV116WWContagemResultadoExecucaoDS_26_Contagemresultadoexecucao_inicio_to3 ,
                                              AV117WWContagemResultadoExecucaoDS_27_Contagemresultadoexecucao_fim3 ,
                                              AV118WWContagemResultadoExecucaoDS_28_Contagemresultadoexecucao_fim_to3 ,
                                              AV119WWContagemResultadoExecucaoDS_29_Contagemresultadoexecucao_prevista3 ,
                                              AV120WWContagemResultadoExecucaoDS_30_Contagemresultadoexecucao_prevista_to3 ,
                                              AV121WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3 ,
                                              AV122WWContagemResultadoExecucaoDS_32_Contagemresultado_contratadacod3 ,
                                              AV123WWContagemResultadoExecucaoDS_33_Contagemresultadoexecucao_oscod3 ,
                                              AV125WWContagemResultadoExecucaoDS_35_Tfcontagemresultado_osfsosfm_sel ,
                                              AV124WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm ,
                                              AV126WWContagemResultadoExecucaoDS_36_Tfcontagemresultadoexecucao_inicio ,
                                              AV127WWContagemResultadoExecucaoDS_37_Tfcontagemresultadoexecucao_inicio_to ,
                                              AV128WWContagemResultadoExecucaoDS_38_Tfcontagemresultadoexecucao_prevista ,
                                              AV129WWContagemResultadoExecucaoDS_39_Tfcontagemresultadoexecucao_prevista_to ,
                                              AV130WWContagemResultadoExecucaoDS_40_Tfcontagemresultadoexecucao_prazodias ,
                                              AV131WWContagemResultadoExecucaoDS_41_Tfcontagemresultadoexecucao_prazodias_to ,
                                              AV132WWContagemResultadoExecucaoDS_42_Tfcontagemresultadoexecucao_fim ,
                                              AV133WWContagemResultadoExecucaoDS_43_Tfcontagemresultadoexecucao_fim_to ,
                                              AV134WWContagemResultadoExecucaoDS_44_Tfcontagemresultadoexecucao_dias ,
                                              AV135WWContagemResultadoExecucaoDS_45_Tfcontagemresultadoexecucao_dias_to ,
                                              A490ContagemResultado_ContratadaCod ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV6WWPContext.gxTpr_Areatrabalho_codigo ,
                                              A1406ContagemResultadoExecucao_Inicio ,
                                              A1407ContagemResultadoExecucao_Fim ,
                                              A1408ContagemResultadoExecucao_Prevista ,
                                              A457ContagemResultado_Demanda ,
                                              A493ContagemResultado_DemandaFM ,
                                              A1404ContagemResultadoExecucao_OSCod ,
                                              A1411ContagemResultadoExecucao_PrazoDias ,
                                              A1410ContagemResultadoExecucao_Dias },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV99WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV99WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1), "%", "");
         lV99WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV99WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1), "%", "");
         lV110WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV110WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2), "%", "");
         lV110WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV110WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2), "%", "");
         lV121WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV121WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3), "%", "");
         lV121WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV121WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3), "%", "");
         lV124WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm = StringUtil.Concat( StringUtil.RTrim( AV124WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm), "%", "");
         /* Using cursor H00KI7 */
         pr_default.execute(5, new Object[] {AV6WWPContext.gxTpr_Contratada_codigo, AV6WWPContext.gxTpr_Areatrabalho_codigo, AV93WWContagemResultadoExecucaoDS_3_Contagemresultadoexecucao_inicio1, AV94WWContagemResultadoExecucaoDS_4_Contagemresultadoexecucao_inicio_to1, AV95WWContagemResultadoExecucaoDS_5_Contagemresultadoexecucao_fim1, AV96WWContagemResultadoExecucaoDS_6_Contagemresultadoexecucao_fim_to1, AV97WWContagemResultadoExecucaoDS_7_Contagemresultadoexecucao_prevista1, AV98WWContagemResultadoExecucaoDS_8_Contagemresultadoexecucao_prevista_to1, lV99WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1, lV99WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1, AV100WWContagemResultadoExecucaoDS_10_Contagemresultado_contratadacod1, AV101WWContagemResultadoExecucaoDS_11_Contagemresultadoexecucao_oscod1, AV104WWContagemResultadoExecucaoDS_14_Contagemresultadoexecucao_inicio2, AV105WWContagemResultadoExecucaoDS_15_Contagemresultadoexecucao_inicio_to2, AV106WWContagemResultadoExecucaoDS_16_Contagemresultadoexecucao_fim2, AV107WWContagemResultadoExecucaoDS_17_Contagemresultadoexecucao_fim_to2, AV108WWContagemResultadoExecucaoDS_18_Contagemresultadoexecucao_prevista2, AV109WWContagemResultadoExecucaoDS_19_Contagemresultadoexecucao_prevista_to2, lV110WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2, lV110WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2, AV111WWContagemResultadoExecucaoDS_21_Contagemresultado_contratadacod2, AV112WWContagemResultadoExecucaoDS_22_Contagemresultadoexecucao_oscod2, AV115WWContagemResultadoExecucaoDS_25_Contagemresultadoexecucao_inicio3, AV116WWContagemResultadoExecucaoDS_26_Contagemresultadoexecucao_inicio_to3, AV117WWContagemResultadoExecucaoDS_27_Contagemresultadoexecucao_fim3, AV118WWContagemResultadoExecucaoDS_28_Contagemresultadoexecucao_fim_to3, AV119WWContagemResultadoExecucaoDS_29_Contagemresultadoexecucao_prevista3, AV120WWContagemResultadoExecucaoDS_30_Contagemresultadoexecucao_prevista_to3, lV121WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3, lV121WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3, AV122WWContagemResultadoExecucaoDS_32_Contagemresultado_contratadacod3, AV123WWContagemResultadoExecucaoDS_33_Contagemresultadoexecucao_oscod3, lV124WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm, AV125WWContagemResultadoExecucaoDS_35_Tfcontagemresultado_osfsosfm_sel, AV126WWContagemResultadoExecucaoDS_36_Tfcontagemresultadoexecucao_inicio, AV127WWContagemResultadoExecucaoDS_37_Tfcontagemresultadoexecucao_inicio_to, AV128WWContagemResultadoExecucaoDS_38_Tfcontagemresultadoexecucao_prevista, AV129WWContagemResultadoExecucaoDS_39_Tfcontagemresultadoexecucao_prevista_to, AV130WWContagemResultadoExecucaoDS_40_Tfcontagemresultadoexecucao_prazodias, AV131WWContagemResultadoExecucaoDS_41_Tfcontagemresultadoexecucao_prazodias_to, AV132WWContagemResultadoExecucaoDS_42_Tfcontagemresultadoexecucao_fim, AV133WWContagemResultadoExecucaoDS_43_Tfcontagemresultadoexecucao_fim_to, AV134WWContagemResultadoExecucaoDS_44_Tfcontagemresultadoexecucao_dias, AV135WWContagemResultadoExecucaoDS_45_Tfcontagemresultadoexecucao_dias_to});
         GRID_nRecordCount = H00KI7_AGRID_nRecordCount[0];
         pr_default.close(5);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV91WWContagemResultadoExecucaoDS_1_Contratada_codigo = AV88Contratada_Codigo;
         AV92WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV93WWContagemResultadoExecucaoDS_3_Contagemresultadoexecucao_inicio1 = AV16ContagemResultadoExecucao_Inicio1;
         AV94WWContagemResultadoExecucaoDS_4_Contagemresultadoexecucao_inicio_to1 = AV17ContagemResultadoExecucao_Inicio_To1;
         AV95WWContagemResultadoExecucaoDS_5_Contagemresultadoexecucao_fim1 = AV18ContagemResultadoExecucao_Fim1;
         AV96WWContagemResultadoExecucaoDS_6_Contagemresultadoexecucao_fim_to1 = AV19ContagemResultadoExecucao_Fim_To1;
         AV97WWContagemResultadoExecucaoDS_7_Contagemresultadoexecucao_prevista1 = AV20ContagemResultadoExecucao_Prevista1;
         AV98WWContagemResultadoExecucaoDS_8_Contagemresultadoexecucao_prevista_to1 = AV21ContagemResultadoExecucao_Prevista_To1;
         AV99WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1 = AV43ContagemResultado_OsFsOsFm1;
         AV100WWContagemResultadoExecucaoDS_10_Contagemresultado_contratadacod1 = AV82ContagemResultado_ContratadaCod1;
         AV101WWContagemResultadoExecucaoDS_11_Contagemresultadoexecucao_oscod1 = AV83ContagemResultadoExecucao_OSCod1;
         AV102WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 = AV22DynamicFiltersEnabled2;
         AV103WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2 = AV23DynamicFiltersSelector2;
         AV104WWContagemResultadoExecucaoDS_14_Contagemresultadoexecucao_inicio2 = AV24ContagemResultadoExecucao_Inicio2;
         AV105WWContagemResultadoExecucaoDS_15_Contagemresultadoexecucao_inicio_to2 = AV25ContagemResultadoExecucao_Inicio_To2;
         AV106WWContagemResultadoExecucaoDS_16_Contagemresultadoexecucao_fim2 = AV26ContagemResultadoExecucao_Fim2;
         AV107WWContagemResultadoExecucaoDS_17_Contagemresultadoexecucao_fim_to2 = AV27ContagemResultadoExecucao_Fim_To2;
         AV108WWContagemResultadoExecucaoDS_18_Contagemresultadoexecucao_prevista2 = AV28ContagemResultadoExecucao_Prevista2;
         AV109WWContagemResultadoExecucaoDS_19_Contagemresultadoexecucao_prevista_to2 = AV29ContagemResultadoExecucao_Prevista_To2;
         AV110WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2 = AV44ContagemResultado_OsFsOsFm2;
         AV111WWContagemResultadoExecucaoDS_21_Contagemresultado_contratadacod2 = AV84ContagemResultado_ContratadaCod2;
         AV112WWContagemResultadoExecucaoDS_22_Contagemresultadoexecucao_oscod2 = AV85ContagemResultadoExecucao_OSCod2;
         AV113WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 = AV30DynamicFiltersEnabled3;
         AV114WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3 = AV31DynamicFiltersSelector3;
         AV115WWContagemResultadoExecucaoDS_25_Contagemresultadoexecucao_inicio3 = AV32ContagemResultadoExecucao_Inicio3;
         AV116WWContagemResultadoExecucaoDS_26_Contagemresultadoexecucao_inicio_to3 = AV33ContagemResultadoExecucao_Inicio_To3;
         AV117WWContagemResultadoExecucaoDS_27_Contagemresultadoexecucao_fim3 = AV34ContagemResultadoExecucao_Fim3;
         AV118WWContagemResultadoExecucaoDS_28_Contagemresultadoexecucao_fim_to3 = AV35ContagemResultadoExecucao_Fim_To3;
         AV119WWContagemResultadoExecucaoDS_29_Contagemresultadoexecucao_prevista3 = AV36ContagemResultadoExecucao_Prevista3;
         AV120WWContagemResultadoExecucaoDS_30_Contagemresultadoexecucao_prevista_to3 = AV37ContagemResultadoExecucao_Prevista_To3;
         AV121WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3 = AV45ContagemResultado_OsFsOsFm3;
         AV122WWContagemResultadoExecucaoDS_32_Contagemresultado_contratadacod3 = AV86ContagemResultado_ContratadaCod3;
         AV123WWContagemResultadoExecucaoDS_33_Contagemresultadoexecucao_oscod3 = AV87ContagemResultadoExecucao_OSCod3;
         AV124WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm = AV49TFContagemResultado_OsFsOsFm;
         AV125WWContagemResultadoExecucaoDS_35_Tfcontagemresultado_osfsosfm_sel = AV50TFContagemResultado_OsFsOsFm_Sel;
         AV126WWContagemResultadoExecucaoDS_36_Tfcontagemresultadoexecucao_inicio = AV53TFContagemResultadoExecucao_Inicio;
         AV127WWContagemResultadoExecucaoDS_37_Tfcontagemresultadoexecucao_inicio_to = AV54TFContagemResultadoExecucao_Inicio_To;
         AV128WWContagemResultadoExecucaoDS_38_Tfcontagemresultadoexecucao_prevista = AV59TFContagemResultadoExecucao_Prevista;
         AV129WWContagemResultadoExecucaoDS_39_Tfcontagemresultadoexecucao_prevista_to = AV60TFContagemResultadoExecucao_Prevista_To;
         AV130WWContagemResultadoExecucaoDS_40_Tfcontagemresultadoexecucao_prazodias = AV65TFContagemResultadoExecucao_PrazoDias;
         AV131WWContagemResultadoExecucaoDS_41_Tfcontagemresultadoexecucao_prazodias_to = AV66TFContagemResultadoExecucao_PrazoDias_To;
         AV132WWContagemResultadoExecucaoDS_42_Tfcontagemresultadoexecucao_fim = AV69TFContagemResultadoExecucao_Fim;
         AV133WWContagemResultadoExecucaoDS_43_Tfcontagemresultadoexecucao_fim_to = AV70TFContagemResultadoExecucao_Fim_To;
         AV134WWContagemResultadoExecucaoDS_44_Tfcontagemresultadoexecucao_dias = AV75TFContagemResultadoExecucao_Dias;
         AV135WWContagemResultadoExecucaoDS_45_Tfcontagemresultadoexecucao_dias_to = AV76TFContagemResultadoExecucao_Dias_To;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV16ContagemResultadoExecucao_Inicio1, AV17ContagemResultadoExecucao_Inicio_To1, AV18ContagemResultadoExecucao_Fim1, AV19ContagemResultadoExecucao_Fim_To1, AV20ContagemResultadoExecucao_Prevista1, AV21ContagemResultadoExecucao_Prevista_To1, AV43ContagemResultado_OsFsOsFm1, AV82ContagemResultado_ContratadaCod1, AV83ContagemResultadoExecucao_OSCod1, AV23DynamicFiltersSelector2, AV24ContagemResultadoExecucao_Inicio2, AV25ContagemResultadoExecucao_Inicio_To2, AV26ContagemResultadoExecucao_Fim2, AV27ContagemResultadoExecucao_Fim_To2, AV28ContagemResultadoExecucao_Prevista2, AV29ContagemResultadoExecucao_Prevista_To2, AV44ContagemResultado_OsFsOsFm2, AV84ContagemResultado_ContratadaCod2, AV85ContagemResultadoExecucao_OSCod2, AV31DynamicFiltersSelector3, AV32ContagemResultadoExecucao_Inicio3, AV33ContagemResultadoExecucao_Inicio_To3, AV34ContagemResultadoExecucao_Fim3, AV35ContagemResultadoExecucao_Fim_To3, AV36ContagemResultadoExecucao_Prevista3, AV37ContagemResultadoExecucao_Prevista_To3, AV45ContagemResultado_OsFsOsFm3, AV86ContagemResultado_ContratadaCod3, AV87ContagemResultadoExecucao_OSCod3, AV22DynamicFiltersEnabled2, AV30DynamicFiltersEnabled3, AV49TFContagemResultado_OsFsOsFm, AV50TFContagemResultado_OsFsOsFm_Sel, AV53TFContagemResultadoExecucao_Inicio, AV54TFContagemResultadoExecucao_Inicio_To, AV59TFContagemResultadoExecucao_Prevista, AV60TFContagemResultadoExecucao_Prevista_To, AV65TFContagemResultadoExecucao_PrazoDias, AV66TFContagemResultadoExecucao_PrazoDias_To, AV69TFContagemResultadoExecucao_Fim, AV70TFContagemResultadoExecucao_Fim_To, AV75TFContagemResultadoExecucao_Dias, AV76TFContagemResultadoExecucao_Dias_To, AV6WWPContext, AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace, AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace, AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace, AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace, AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace, AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace, AV88Contratada_Codigo, AV138Pgmname, AV10GridState, AV39DynamicFiltersIgnoreFirst, AV38DynamicFiltersRemoving, A1405ContagemResultadoExecucao_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV91WWContagemResultadoExecucaoDS_1_Contratada_codigo = AV88Contratada_Codigo;
         AV92WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV93WWContagemResultadoExecucaoDS_3_Contagemresultadoexecucao_inicio1 = AV16ContagemResultadoExecucao_Inicio1;
         AV94WWContagemResultadoExecucaoDS_4_Contagemresultadoexecucao_inicio_to1 = AV17ContagemResultadoExecucao_Inicio_To1;
         AV95WWContagemResultadoExecucaoDS_5_Contagemresultadoexecucao_fim1 = AV18ContagemResultadoExecucao_Fim1;
         AV96WWContagemResultadoExecucaoDS_6_Contagemresultadoexecucao_fim_to1 = AV19ContagemResultadoExecucao_Fim_To1;
         AV97WWContagemResultadoExecucaoDS_7_Contagemresultadoexecucao_prevista1 = AV20ContagemResultadoExecucao_Prevista1;
         AV98WWContagemResultadoExecucaoDS_8_Contagemresultadoexecucao_prevista_to1 = AV21ContagemResultadoExecucao_Prevista_To1;
         AV99WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1 = AV43ContagemResultado_OsFsOsFm1;
         AV100WWContagemResultadoExecucaoDS_10_Contagemresultado_contratadacod1 = AV82ContagemResultado_ContratadaCod1;
         AV101WWContagemResultadoExecucaoDS_11_Contagemresultadoexecucao_oscod1 = AV83ContagemResultadoExecucao_OSCod1;
         AV102WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 = AV22DynamicFiltersEnabled2;
         AV103WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2 = AV23DynamicFiltersSelector2;
         AV104WWContagemResultadoExecucaoDS_14_Contagemresultadoexecucao_inicio2 = AV24ContagemResultadoExecucao_Inicio2;
         AV105WWContagemResultadoExecucaoDS_15_Contagemresultadoexecucao_inicio_to2 = AV25ContagemResultadoExecucao_Inicio_To2;
         AV106WWContagemResultadoExecucaoDS_16_Contagemresultadoexecucao_fim2 = AV26ContagemResultadoExecucao_Fim2;
         AV107WWContagemResultadoExecucaoDS_17_Contagemresultadoexecucao_fim_to2 = AV27ContagemResultadoExecucao_Fim_To2;
         AV108WWContagemResultadoExecucaoDS_18_Contagemresultadoexecucao_prevista2 = AV28ContagemResultadoExecucao_Prevista2;
         AV109WWContagemResultadoExecucaoDS_19_Contagemresultadoexecucao_prevista_to2 = AV29ContagemResultadoExecucao_Prevista_To2;
         AV110WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2 = AV44ContagemResultado_OsFsOsFm2;
         AV111WWContagemResultadoExecucaoDS_21_Contagemresultado_contratadacod2 = AV84ContagemResultado_ContratadaCod2;
         AV112WWContagemResultadoExecucaoDS_22_Contagemresultadoexecucao_oscod2 = AV85ContagemResultadoExecucao_OSCod2;
         AV113WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 = AV30DynamicFiltersEnabled3;
         AV114WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3 = AV31DynamicFiltersSelector3;
         AV115WWContagemResultadoExecucaoDS_25_Contagemresultadoexecucao_inicio3 = AV32ContagemResultadoExecucao_Inicio3;
         AV116WWContagemResultadoExecucaoDS_26_Contagemresultadoexecucao_inicio_to3 = AV33ContagemResultadoExecucao_Inicio_To3;
         AV117WWContagemResultadoExecucaoDS_27_Contagemresultadoexecucao_fim3 = AV34ContagemResultadoExecucao_Fim3;
         AV118WWContagemResultadoExecucaoDS_28_Contagemresultadoexecucao_fim_to3 = AV35ContagemResultadoExecucao_Fim_To3;
         AV119WWContagemResultadoExecucaoDS_29_Contagemresultadoexecucao_prevista3 = AV36ContagemResultadoExecucao_Prevista3;
         AV120WWContagemResultadoExecucaoDS_30_Contagemresultadoexecucao_prevista_to3 = AV37ContagemResultadoExecucao_Prevista_To3;
         AV121WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3 = AV45ContagemResultado_OsFsOsFm3;
         AV122WWContagemResultadoExecucaoDS_32_Contagemresultado_contratadacod3 = AV86ContagemResultado_ContratadaCod3;
         AV123WWContagemResultadoExecucaoDS_33_Contagemresultadoexecucao_oscod3 = AV87ContagemResultadoExecucao_OSCod3;
         AV124WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm = AV49TFContagemResultado_OsFsOsFm;
         AV125WWContagemResultadoExecucaoDS_35_Tfcontagemresultado_osfsosfm_sel = AV50TFContagemResultado_OsFsOsFm_Sel;
         AV126WWContagemResultadoExecucaoDS_36_Tfcontagemresultadoexecucao_inicio = AV53TFContagemResultadoExecucao_Inicio;
         AV127WWContagemResultadoExecucaoDS_37_Tfcontagemresultadoexecucao_inicio_to = AV54TFContagemResultadoExecucao_Inicio_To;
         AV128WWContagemResultadoExecucaoDS_38_Tfcontagemresultadoexecucao_prevista = AV59TFContagemResultadoExecucao_Prevista;
         AV129WWContagemResultadoExecucaoDS_39_Tfcontagemresultadoexecucao_prevista_to = AV60TFContagemResultadoExecucao_Prevista_To;
         AV130WWContagemResultadoExecucaoDS_40_Tfcontagemresultadoexecucao_prazodias = AV65TFContagemResultadoExecucao_PrazoDias;
         AV131WWContagemResultadoExecucaoDS_41_Tfcontagemresultadoexecucao_prazodias_to = AV66TFContagemResultadoExecucao_PrazoDias_To;
         AV132WWContagemResultadoExecucaoDS_42_Tfcontagemresultadoexecucao_fim = AV69TFContagemResultadoExecucao_Fim;
         AV133WWContagemResultadoExecucaoDS_43_Tfcontagemresultadoexecucao_fim_to = AV70TFContagemResultadoExecucao_Fim_To;
         AV134WWContagemResultadoExecucaoDS_44_Tfcontagemresultadoexecucao_dias = AV75TFContagemResultadoExecucao_Dias;
         AV135WWContagemResultadoExecucaoDS_45_Tfcontagemresultadoexecucao_dias_to = AV76TFContagemResultadoExecucao_Dias_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV16ContagemResultadoExecucao_Inicio1, AV17ContagemResultadoExecucao_Inicio_To1, AV18ContagemResultadoExecucao_Fim1, AV19ContagemResultadoExecucao_Fim_To1, AV20ContagemResultadoExecucao_Prevista1, AV21ContagemResultadoExecucao_Prevista_To1, AV43ContagemResultado_OsFsOsFm1, AV82ContagemResultado_ContratadaCod1, AV83ContagemResultadoExecucao_OSCod1, AV23DynamicFiltersSelector2, AV24ContagemResultadoExecucao_Inicio2, AV25ContagemResultadoExecucao_Inicio_To2, AV26ContagemResultadoExecucao_Fim2, AV27ContagemResultadoExecucao_Fim_To2, AV28ContagemResultadoExecucao_Prevista2, AV29ContagemResultadoExecucao_Prevista_To2, AV44ContagemResultado_OsFsOsFm2, AV84ContagemResultado_ContratadaCod2, AV85ContagemResultadoExecucao_OSCod2, AV31DynamicFiltersSelector3, AV32ContagemResultadoExecucao_Inicio3, AV33ContagemResultadoExecucao_Inicio_To3, AV34ContagemResultadoExecucao_Fim3, AV35ContagemResultadoExecucao_Fim_To3, AV36ContagemResultadoExecucao_Prevista3, AV37ContagemResultadoExecucao_Prevista_To3, AV45ContagemResultado_OsFsOsFm3, AV86ContagemResultado_ContratadaCod3, AV87ContagemResultadoExecucao_OSCod3, AV22DynamicFiltersEnabled2, AV30DynamicFiltersEnabled3, AV49TFContagemResultado_OsFsOsFm, AV50TFContagemResultado_OsFsOsFm_Sel, AV53TFContagemResultadoExecucao_Inicio, AV54TFContagemResultadoExecucao_Inicio_To, AV59TFContagemResultadoExecucao_Prevista, AV60TFContagemResultadoExecucao_Prevista_To, AV65TFContagemResultadoExecucao_PrazoDias, AV66TFContagemResultadoExecucao_PrazoDias_To, AV69TFContagemResultadoExecucao_Fim, AV70TFContagemResultadoExecucao_Fim_To, AV75TFContagemResultadoExecucao_Dias, AV76TFContagemResultadoExecucao_Dias_To, AV6WWPContext, AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace, AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace, AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace, AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace, AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace, AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace, AV88Contratada_Codigo, AV138Pgmname, AV10GridState, AV39DynamicFiltersIgnoreFirst, AV38DynamicFiltersRemoving, A1405ContagemResultadoExecucao_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV91WWContagemResultadoExecucaoDS_1_Contratada_codigo = AV88Contratada_Codigo;
         AV92WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV93WWContagemResultadoExecucaoDS_3_Contagemresultadoexecucao_inicio1 = AV16ContagemResultadoExecucao_Inicio1;
         AV94WWContagemResultadoExecucaoDS_4_Contagemresultadoexecucao_inicio_to1 = AV17ContagemResultadoExecucao_Inicio_To1;
         AV95WWContagemResultadoExecucaoDS_5_Contagemresultadoexecucao_fim1 = AV18ContagemResultadoExecucao_Fim1;
         AV96WWContagemResultadoExecucaoDS_6_Contagemresultadoexecucao_fim_to1 = AV19ContagemResultadoExecucao_Fim_To1;
         AV97WWContagemResultadoExecucaoDS_7_Contagemresultadoexecucao_prevista1 = AV20ContagemResultadoExecucao_Prevista1;
         AV98WWContagemResultadoExecucaoDS_8_Contagemresultadoexecucao_prevista_to1 = AV21ContagemResultadoExecucao_Prevista_To1;
         AV99WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1 = AV43ContagemResultado_OsFsOsFm1;
         AV100WWContagemResultadoExecucaoDS_10_Contagemresultado_contratadacod1 = AV82ContagemResultado_ContratadaCod1;
         AV101WWContagemResultadoExecucaoDS_11_Contagemresultadoexecucao_oscod1 = AV83ContagemResultadoExecucao_OSCod1;
         AV102WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 = AV22DynamicFiltersEnabled2;
         AV103WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2 = AV23DynamicFiltersSelector2;
         AV104WWContagemResultadoExecucaoDS_14_Contagemresultadoexecucao_inicio2 = AV24ContagemResultadoExecucao_Inicio2;
         AV105WWContagemResultadoExecucaoDS_15_Contagemresultadoexecucao_inicio_to2 = AV25ContagemResultadoExecucao_Inicio_To2;
         AV106WWContagemResultadoExecucaoDS_16_Contagemresultadoexecucao_fim2 = AV26ContagemResultadoExecucao_Fim2;
         AV107WWContagemResultadoExecucaoDS_17_Contagemresultadoexecucao_fim_to2 = AV27ContagemResultadoExecucao_Fim_To2;
         AV108WWContagemResultadoExecucaoDS_18_Contagemresultadoexecucao_prevista2 = AV28ContagemResultadoExecucao_Prevista2;
         AV109WWContagemResultadoExecucaoDS_19_Contagemresultadoexecucao_prevista_to2 = AV29ContagemResultadoExecucao_Prevista_To2;
         AV110WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2 = AV44ContagemResultado_OsFsOsFm2;
         AV111WWContagemResultadoExecucaoDS_21_Contagemresultado_contratadacod2 = AV84ContagemResultado_ContratadaCod2;
         AV112WWContagemResultadoExecucaoDS_22_Contagemresultadoexecucao_oscod2 = AV85ContagemResultadoExecucao_OSCod2;
         AV113WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 = AV30DynamicFiltersEnabled3;
         AV114WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3 = AV31DynamicFiltersSelector3;
         AV115WWContagemResultadoExecucaoDS_25_Contagemresultadoexecucao_inicio3 = AV32ContagemResultadoExecucao_Inicio3;
         AV116WWContagemResultadoExecucaoDS_26_Contagemresultadoexecucao_inicio_to3 = AV33ContagemResultadoExecucao_Inicio_To3;
         AV117WWContagemResultadoExecucaoDS_27_Contagemresultadoexecucao_fim3 = AV34ContagemResultadoExecucao_Fim3;
         AV118WWContagemResultadoExecucaoDS_28_Contagemresultadoexecucao_fim_to3 = AV35ContagemResultadoExecucao_Fim_To3;
         AV119WWContagemResultadoExecucaoDS_29_Contagemresultadoexecucao_prevista3 = AV36ContagemResultadoExecucao_Prevista3;
         AV120WWContagemResultadoExecucaoDS_30_Contagemresultadoexecucao_prevista_to3 = AV37ContagemResultadoExecucao_Prevista_To3;
         AV121WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3 = AV45ContagemResultado_OsFsOsFm3;
         AV122WWContagemResultadoExecucaoDS_32_Contagemresultado_contratadacod3 = AV86ContagemResultado_ContratadaCod3;
         AV123WWContagemResultadoExecucaoDS_33_Contagemresultadoexecucao_oscod3 = AV87ContagemResultadoExecucao_OSCod3;
         AV124WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm = AV49TFContagemResultado_OsFsOsFm;
         AV125WWContagemResultadoExecucaoDS_35_Tfcontagemresultado_osfsosfm_sel = AV50TFContagemResultado_OsFsOsFm_Sel;
         AV126WWContagemResultadoExecucaoDS_36_Tfcontagemresultadoexecucao_inicio = AV53TFContagemResultadoExecucao_Inicio;
         AV127WWContagemResultadoExecucaoDS_37_Tfcontagemresultadoexecucao_inicio_to = AV54TFContagemResultadoExecucao_Inicio_To;
         AV128WWContagemResultadoExecucaoDS_38_Tfcontagemresultadoexecucao_prevista = AV59TFContagemResultadoExecucao_Prevista;
         AV129WWContagemResultadoExecucaoDS_39_Tfcontagemresultadoexecucao_prevista_to = AV60TFContagemResultadoExecucao_Prevista_To;
         AV130WWContagemResultadoExecucaoDS_40_Tfcontagemresultadoexecucao_prazodias = AV65TFContagemResultadoExecucao_PrazoDias;
         AV131WWContagemResultadoExecucaoDS_41_Tfcontagemresultadoexecucao_prazodias_to = AV66TFContagemResultadoExecucao_PrazoDias_To;
         AV132WWContagemResultadoExecucaoDS_42_Tfcontagemresultadoexecucao_fim = AV69TFContagemResultadoExecucao_Fim;
         AV133WWContagemResultadoExecucaoDS_43_Tfcontagemresultadoexecucao_fim_to = AV70TFContagemResultadoExecucao_Fim_To;
         AV134WWContagemResultadoExecucaoDS_44_Tfcontagemresultadoexecucao_dias = AV75TFContagemResultadoExecucao_Dias;
         AV135WWContagemResultadoExecucaoDS_45_Tfcontagemresultadoexecucao_dias_to = AV76TFContagemResultadoExecucao_Dias_To;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV16ContagemResultadoExecucao_Inicio1, AV17ContagemResultadoExecucao_Inicio_To1, AV18ContagemResultadoExecucao_Fim1, AV19ContagemResultadoExecucao_Fim_To1, AV20ContagemResultadoExecucao_Prevista1, AV21ContagemResultadoExecucao_Prevista_To1, AV43ContagemResultado_OsFsOsFm1, AV82ContagemResultado_ContratadaCod1, AV83ContagemResultadoExecucao_OSCod1, AV23DynamicFiltersSelector2, AV24ContagemResultadoExecucao_Inicio2, AV25ContagemResultadoExecucao_Inicio_To2, AV26ContagemResultadoExecucao_Fim2, AV27ContagemResultadoExecucao_Fim_To2, AV28ContagemResultadoExecucao_Prevista2, AV29ContagemResultadoExecucao_Prevista_To2, AV44ContagemResultado_OsFsOsFm2, AV84ContagemResultado_ContratadaCod2, AV85ContagemResultadoExecucao_OSCod2, AV31DynamicFiltersSelector3, AV32ContagemResultadoExecucao_Inicio3, AV33ContagemResultadoExecucao_Inicio_To3, AV34ContagemResultadoExecucao_Fim3, AV35ContagemResultadoExecucao_Fim_To3, AV36ContagemResultadoExecucao_Prevista3, AV37ContagemResultadoExecucao_Prevista_To3, AV45ContagemResultado_OsFsOsFm3, AV86ContagemResultado_ContratadaCod3, AV87ContagemResultadoExecucao_OSCod3, AV22DynamicFiltersEnabled2, AV30DynamicFiltersEnabled3, AV49TFContagemResultado_OsFsOsFm, AV50TFContagemResultado_OsFsOsFm_Sel, AV53TFContagemResultadoExecucao_Inicio, AV54TFContagemResultadoExecucao_Inicio_To, AV59TFContagemResultadoExecucao_Prevista, AV60TFContagemResultadoExecucao_Prevista_To, AV65TFContagemResultadoExecucao_PrazoDias, AV66TFContagemResultadoExecucao_PrazoDias_To, AV69TFContagemResultadoExecucao_Fim, AV70TFContagemResultadoExecucao_Fim_To, AV75TFContagemResultadoExecucao_Dias, AV76TFContagemResultadoExecucao_Dias_To, AV6WWPContext, AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace, AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace, AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace, AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace, AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace, AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace, AV88Contratada_Codigo, AV138Pgmname, AV10GridState, AV39DynamicFiltersIgnoreFirst, AV38DynamicFiltersRemoving, A1405ContagemResultadoExecucao_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV91WWContagemResultadoExecucaoDS_1_Contratada_codigo = AV88Contratada_Codigo;
         AV92WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV93WWContagemResultadoExecucaoDS_3_Contagemresultadoexecucao_inicio1 = AV16ContagemResultadoExecucao_Inicio1;
         AV94WWContagemResultadoExecucaoDS_4_Contagemresultadoexecucao_inicio_to1 = AV17ContagemResultadoExecucao_Inicio_To1;
         AV95WWContagemResultadoExecucaoDS_5_Contagemresultadoexecucao_fim1 = AV18ContagemResultadoExecucao_Fim1;
         AV96WWContagemResultadoExecucaoDS_6_Contagemresultadoexecucao_fim_to1 = AV19ContagemResultadoExecucao_Fim_To1;
         AV97WWContagemResultadoExecucaoDS_7_Contagemresultadoexecucao_prevista1 = AV20ContagemResultadoExecucao_Prevista1;
         AV98WWContagemResultadoExecucaoDS_8_Contagemresultadoexecucao_prevista_to1 = AV21ContagemResultadoExecucao_Prevista_To1;
         AV99WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1 = AV43ContagemResultado_OsFsOsFm1;
         AV100WWContagemResultadoExecucaoDS_10_Contagemresultado_contratadacod1 = AV82ContagemResultado_ContratadaCod1;
         AV101WWContagemResultadoExecucaoDS_11_Contagemresultadoexecucao_oscod1 = AV83ContagemResultadoExecucao_OSCod1;
         AV102WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 = AV22DynamicFiltersEnabled2;
         AV103WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2 = AV23DynamicFiltersSelector2;
         AV104WWContagemResultadoExecucaoDS_14_Contagemresultadoexecucao_inicio2 = AV24ContagemResultadoExecucao_Inicio2;
         AV105WWContagemResultadoExecucaoDS_15_Contagemresultadoexecucao_inicio_to2 = AV25ContagemResultadoExecucao_Inicio_To2;
         AV106WWContagemResultadoExecucaoDS_16_Contagemresultadoexecucao_fim2 = AV26ContagemResultadoExecucao_Fim2;
         AV107WWContagemResultadoExecucaoDS_17_Contagemresultadoexecucao_fim_to2 = AV27ContagemResultadoExecucao_Fim_To2;
         AV108WWContagemResultadoExecucaoDS_18_Contagemresultadoexecucao_prevista2 = AV28ContagemResultadoExecucao_Prevista2;
         AV109WWContagemResultadoExecucaoDS_19_Contagemresultadoexecucao_prevista_to2 = AV29ContagemResultadoExecucao_Prevista_To2;
         AV110WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2 = AV44ContagemResultado_OsFsOsFm2;
         AV111WWContagemResultadoExecucaoDS_21_Contagemresultado_contratadacod2 = AV84ContagemResultado_ContratadaCod2;
         AV112WWContagemResultadoExecucaoDS_22_Contagemresultadoexecucao_oscod2 = AV85ContagemResultadoExecucao_OSCod2;
         AV113WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 = AV30DynamicFiltersEnabled3;
         AV114WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3 = AV31DynamicFiltersSelector3;
         AV115WWContagemResultadoExecucaoDS_25_Contagemresultadoexecucao_inicio3 = AV32ContagemResultadoExecucao_Inicio3;
         AV116WWContagemResultadoExecucaoDS_26_Contagemresultadoexecucao_inicio_to3 = AV33ContagemResultadoExecucao_Inicio_To3;
         AV117WWContagemResultadoExecucaoDS_27_Contagemresultadoexecucao_fim3 = AV34ContagemResultadoExecucao_Fim3;
         AV118WWContagemResultadoExecucaoDS_28_Contagemresultadoexecucao_fim_to3 = AV35ContagemResultadoExecucao_Fim_To3;
         AV119WWContagemResultadoExecucaoDS_29_Contagemresultadoexecucao_prevista3 = AV36ContagemResultadoExecucao_Prevista3;
         AV120WWContagemResultadoExecucaoDS_30_Contagemresultadoexecucao_prevista_to3 = AV37ContagemResultadoExecucao_Prevista_To3;
         AV121WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3 = AV45ContagemResultado_OsFsOsFm3;
         AV122WWContagemResultadoExecucaoDS_32_Contagemresultado_contratadacod3 = AV86ContagemResultado_ContratadaCod3;
         AV123WWContagemResultadoExecucaoDS_33_Contagemresultadoexecucao_oscod3 = AV87ContagemResultadoExecucao_OSCod3;
         AV124WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm = AV49TFContagemResultado_OsFsOsFm;
         AV125WWContagemResultadoExecucaoDS_35_Tfcontagemresultado_osfsosfm_sel = AV50TFContagemResultado_OsFsOsFm_Sel;
         AV126WWContagemResultadoExecucaoDS_36_Tfcontagemresultadoexecucao_inicio = AV53TFContagemResultadoExecucao_Inicio;
         AV127WWContagemResultadoExecucaoDS_37_Tfcontagemresultadoexecucao_inicio_to = AV54TFContagemResultadoExecucao_Inicio_To;
         AV128WWContagemResultadoExecucaoDS_38_Tfcontagemresultadoexecucao_prevista = AV59TFContagemResultadoExecucao_Prevista;
         AV129WWContagemResultadoExecucaoDS_39_Tfcontagemresultadoexecucao_prevista_to = AV60TFContagemResultadoExecucao_Prevista_To;
         AV130WWContagemResultadoExecucaoDS_40_Tfcontagemresultadoexecucao_prazodias = AV65TFContagemResultadoExecucao_PrazoDias;
         AV131WWContagemResultadoExecucaoDS_41_Tfcontagemresultadoexecucao_prazodias_to = AV66TFContagemResultadoExecucao_PrazoDias_To;
         AV132WWContagemResultadoExecucaoDS_42_Tfcontagemresultadoexecucao_fim = AV69TFContagemResultadoExecucao_Fim;
         AV133WWContagemResultadoExecucaoDS_43_Tfcontagemresultadoexecucao_fim_to = AV70TFContagemResultadoExecucao_Fim_To;
         AV134WWContagemResultadoExecucaoDS_44_Tfcontagemresultadoexecucao_dias = AV75TFContagemResultadoExecucao_Dias;
         AV135WWContagemResultadoExecucaoDS_45_Tfcontagemresultadoexecucao_dias_to = AV76TFContagemResultadoExecucao_Dias_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV16ContagemResultadoExecucao_Inicio1, AV17ContagemResultadoExecucao_Inicio_To1, AV18ContagemResultadoExecucao_Fim1, AV19ContagemResultadoExecucao_Fim_To1, AV20ContagemResultadoExecucao_Prevista1, AV21ContagemResultadoExecucao_Prevista_To1, AV43ContagemResultado_OsFsOsFm1, AV82ContagemResultado_ContratadaCod1, AV83ContagemResultadoExecucao_OSCod1, AV23DynamicFiltersSelector2, AV24ContagemResultadoExecucao_Inicio2, AV25ContagemResultadoExecucao_Inicio_To2, AV26ContagemResultadoExecucao_Fim2, AV27ContagemResultadoExecucao_Fim_To2, AV28ContagemResultadoExecucao_Prevista2, AV29ContagemResultadoExecucao_Prevista_To2, AV44ContagemResultado_OsFsOsFm2, AV84ContagemResultado_ContratadaCod2, AV85ContagemResultadoExecucao_OSCod2, AV31DynamicFiltersSelector3, AV32ContagemResultadoExecucao_Inicio3, AV33ContagemResultadoExecucao_Inicio_To3, AV34ContagemResultadoExecucao_Fim3, AV35ContagemResultadoExecucao_Fim_To3, AV36ContagemResultadoExecucao_Prevista3, AV37ContagemResultadoExecucao_Prevista_To3, AV45ContagemResultado_OsFsOsFm3, AV86ContagemResultado_ContratadaCod3, AV87ContagemResultadoExecucao_OSCod3, AV22DynamicFiltersEnabled2, AV30DynamicFiltersEnabled3, AV49TFContagemResultado_OsFsOsFm, AV50TFContagemResultado_OsFsOsFm_Sel, AV53TFContagemResultadoExecucao_Inicio, AV54TFContagemResultadoExecucao_Inicio_To, AV59TFContagemResultadoExecucao_Prevista, AV60TFContagemResultadoExecucao_Prevista_To, AV65TFContagemResultadoExecucao_PrazoDias, AV66TFContagemResultadoExecucao_PrazoDias_To, AV69TFContagemResultadoExecucao_Fim, AV70TFContagemResultadoExecucao_Fim_To, AV75TFContagemResultadoExecucao_Dias, AV76TFContagemResultadoExecucao_Dias_To, AV6WWPContext, AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace, AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace, AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace, AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace, AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace, AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace, AV88Contratada_Codigo, AV138Pgmname, AV10GridState, AV39DynamicFiltersIgnoreFirst, AV38DynamicFiltersRemoving, A1405ContagemResultadoExecucao_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV91WWContagemResultadoExecucaoDS_1_Contratada_codigo = AV88Contratada_Codigo;
         AV92WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV93WWContagemResultadoExecucaoDS_3_Contagemresultadoexecucao_inicio1 = AV16ContagemResultadoExecucao_Inicio1;
         AV94WWContagemResultadoExecucaoDS_4_Contagemresultadoexecucao_inicio_to1 = AV17ContagemResultadoExecucao_Inicio_To1;
         AV95WWContagemResultadoExecucaoDS_5_Contagemresultadoexecucao_fim1 = AV18ContagemResultadoExecucao_Fim1;
         AV96WWContagemResultadoExecucaoDS_6_Contagemresultadoexecucao_fim_to1 = AV19ContagemResultadoExecucao_Fim_To1;
         AV97WWContagemResultadoExecucaoDS_7_Contagemresultadoexecucao_prevista1 = AV20ContagemResultadoExecucao_Prevista1;
         AV98WWContagemResultadoExecucaoDS_8_Contagemresultadoexecucao_prevista_to1 = AV21ContagemResultadoExecucao_Prevista_To1;
         AV99WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1 = AV43ContagemResultado_OsFsOsFm1;
         AV100WWContagemResultadoExecucaoDS_10_Contagemresultado_contratadacod1 = AV82ContagemResultado_ContratadaCod1;
         AV101WWContagemResultadoExecucaoDS_11_Contagemresultadoexecucao_oscod1 = AV83ContagemResultadoExecucao_OSCod1;
         AV102WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 = AV22DynamicFiltersEnabled2;
         AV103WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2 = AV23DynamicFiltersSelector2;
         AV104WWContagemResultadoExecucaoDS_14_Contagemresultadoexecucao_inicio2 = AV24ContagemResultadoExecucao_Inicio2;
         AV105WWContagemResultadoExecucaoDS_15_Contagemresultadoexecucao_inicio_to2 = AV25ContagemResultadoExecucao_Inicio_To2;
         AV106WWContagemResultadoExecucaoDS_16_Contagemresultadoexecucao_fim2 = AV26ContagemResultadoExecucao_Fim2;
         AV107WWContagemResultadoExecucaoDS_17_Contagemresultadoexecucao_fim_to2 = AV27ContagemResultadoExecucao_Fim_To2;
         AV108WWContagemResultadoExecucaoDS_18_Contagemresultadoexecucao_prevista2 = AV28ContagemResultadoExecucao_Prevista2;
         AV109WWContagemResultadoExecucaoDS_19_Contagemresultadoexecucao_prevista_to2 = AV29ContagemResultadoExecucao_Prevista_To2;
         AV110WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2 = AV44ContagemResultado_OsFsOsFm2;
         AV111WWContagemResultadoExecucaoDS_21_Contagemresultado_contratadacod2 = AV84ContagemResultado_ContratadaCod2;
         AV112WWContagemResultadoExecucaoDS_22_Contagemresultadoexecucao_oscod2 = AV85ContagemResultadoExecucao_OSCod2;
         AV113WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 = AV30DynamicFiltersEnabled3;
         AV114WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3 = AV31DynamicFiltersSelector3;
         AV115WWContagemResultadoExecucaoDS_25_Contagemresultadoexecucao_inicio3 = AV32ContagemResultadoExecucao_Inicio3;
         AV116WWContagemResultadoExecucaoDS_26_Contagemresultadoexecucao_inicio_to3 = AV33ContagemResultadoExecucao_Inicio_To3;
         AV117WWContagemResultadoExecucaoDS_27_Contagemresultadoexecucao_fim3 = AV34ContagemResultadoExecucao_Fim3;
         AV118WWContagemResultadoExecucaoDS_28_Contagemresultadoexecucao_fim_to3 = AV35ContagemResultadoExecucao_Fim_To3;
         AV119WWContagemResultadoExecucaoDS_29_Contagemresultadoexecucao_prevista3 = AV36ContagemResultadoExecucao_Prevista3;
         AV120WWContagemResultadoExecucaoDS_30_Contagemresultadoexecucao_prevista_to3 = AV37ContagemResultadoExecucao_Prevista_To3;
         AV121WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3 = AV45ContagemResultado_OsFsOsFm3;
         AV122WWContagemResultadoExecucaoDS_32_Contagemresultado_contratadacod3 = AV86ContagemResultado_ContratadaCod3;
         AV123WWContagemResultadoExecucaoDS_33_Contagemresultadoexecucao_oscod3 = AV87ContagemResultadoExecucao_OSCod3;
         AV124WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm = AV49TFContagemResultado_OsFsOsFm;
         AV125WWContagemResultadoExecucaoDS_35_Tfcontagemresultado_osfsosfm_sel = AV50TFContagemResultado_OsFsOsFm_Sel;
         AV126WWContagemResultadoExecucaoDS_36_Tfcontagemresultadoexecucao_inicio = AV53TFContagemResultadoExecucao_Inicio;
         AV127WWContagemResultadoExecucaoDS_37_Tfcontagemresultadoexecucao_inicio_to = AV54TFContagemResultadoExecucao_Inicio_To;
         AV128WWContagemResultadoExecucaoDS_38_Tfcontagemresultadoexecucao_prevista = AV59TFContagemResultadoExecucao_Prevista;
         AV129WWContagemResultadoExecucaoDS_39_Tfcontagemresultadoexecucao_prevista_to = AV60TFContagemResultadoExecucao_Prevista_To;
         AV130WWContagemResultadoExecucaoDS_40_Tfcontagemresultadoexecucao_prazodias = AV65TFContagemResultadoExecucao_PrazoDias;
         AV131WWContagemResultadoExecucaoDS_41_Tfcontagemresultadoexecucao_prazodias_to = AV66TFContagemResultadoExecucao_PrazoDias_To;
         AV132WWContagemResultadoExecucaoDS_42_Tfcontagemresultadoexecucao_fim = AV69TFContagemResultadoExecucao_Fim;
         AV133WWContagemResultadoExecucaoDS_43_Tfcontagemresultadoexecucao_fim_to = AV70TFContagemResultadoExecucao_Fim_To;
         AV134WWContagemResultadoExecucaoDS_44_Tfcontagemresultadoexecucao_dias = AV75TFContagemResultadoExecucao_Dias;
         AV135WWContagemResultadoExecucaoDS_45_Tfcontagemresultadoexecucao_dias_to = AV76TFContagemResultadoExecucao_Dias_To;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV16ContagemResultadoExecucao_Inicio1, AV17ContagemResultadoExecucao_Inicio_To1, AV18ContagemResultadoExecucao_Fim1, AV19ContagemResultadoExecucao_Fim_To1, AV20ContagemResultadoExecucao_Prevista1, AV21ContagemResultadoExecucao_Prevista_To1, AV43ContagemResultado_OsFsOsFm1, AV82ContagemResultado_ContratadaCod1, AV83ContagemResultadoExecucao_OSCod1, AV23DynamicFiltersSelector2, AV24ContagemResultadoExecucao_Inicio2, AV25ContagemResultadoExecucao_Inicio_To2, AV26ContagemResultadoExecucao_Fim2, AV27ContagemResultadoExecucao_Fim_To2, AV28ContagemResultadoExecucao_Prevista2, AV29ContagemResultadoExecucao_Prevista_To2, AV44ContagemResultado_OsFsOsFm2, AV84ContagemResultado_ContratadaCod2, AV85ContagemResultadoExecucao_OSCod2, AV31DynamicFiltersSelector3, AV32ContagemResultadoExecucao_Inicio3, AV33ContagemResultadoExecucao_Inicio_To3, AV34ContagemResultadoExecucao_Fim3, AV35ContagemResultadoExecucao_Fim_To3, AV36ContagemResultadoExecucao_Prevista3, AV37ContagemResultadoExecucao_Prevista_To3, AV45ContagemResultado_OsFsOsFm3, AV86ContagemResultado_ContratadaCod3, AV87ContagemResultadoExecucao_OSCod3, AV22DynamicFiltersEnabled2, AV30DynamicFiltersEnabled3, AV49TFContagemResultado_OsFsOsFm, AV50TFContagemResultado_OsFsOsFm_Sel, AV53TFContagemResultadoExecucao_Inicio, AV54TFContagemResultadoExecucao_Inicio_To, AV59TFContagemResultadoExecucao_Prevista, AV60TFContagemResultadoExecucao_Prevista_To, AV65TFContagemResultadoExecucao_PrazoDias, AV66TFContagemResultadoExecucao_PrazoDias_To, AV69TFContagemResultadoExecucao_Fim, AV70TFContagemResultadoExecucao_Fim_To, AV75TFContagemResultadoExecucao_Dias, AV76TFContagemResultadoExecucao_Dias_To, AV6WWPContext, AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace, AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace, AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace, AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace, AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace, AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace, AV88Contratada_Codigo, AV138Pgmname, AV10GridState, AV39DynamicFiltersIgnoreFirst, AV38DynamicFiltersRemoving, A1405ContagemResultadoExecucao_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUPKI0( )
      {
         /* Before Start, stand alone formulas. */
         AV138Pgmname = "WWContagemResultadoExecucao";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E28KI2 */
         E28KI2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         GXVvCONTRATADA_CODIGO_htmlKI2( AV6WWPContext) ;
         GXVvCONTAGEMRESULTADO_CONTRATADACOD1_htmlKI2( AV6WWPContext) ;
         GXVvCONTAGEMRESULTADO_CONTRATADACOD2_htmlKI2( AV6WWPContext) ;
         GXVvCONTAGEMRESULTADO_CONTRATADACOD3_htmlKI2( AV6WWPContext) ;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV78DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADO_OSFSOSFMTITLEFILTERDATA"), AV48ContagemResultado_OsFsOsFmTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADOEXECUCAO_INICIOTITLEFILTERDATA"), AV52ContagemResultadoExecucao_InicioTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADOEXECUCAO_PREVISTATITLEFILTERDATA"), AV58ContagemResultadoExecucao_PrevistaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLEFILTERDATA"), AV64ContagemResultadoExecucao_PrazoDiasTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADOEXECUCAO_FIMTITLEFILTERDATA"), AV68ContagemResultadoExecucao_FimTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADOEXECUCAO_DIASTITLEFILTERDATA"), AV74ContagemResultadoExecucao_DiasTitleFilterData);
            /* Read variables values. */
            dynavContratada_codigo.Name = dynavContratada_codigo_Internalname;
            dynavContratada_codigo.CurrentValue = cgiGet( dynavContratada_codigo_Internalname);
            AV88Contratada_Codigo = (int)(NumberUtil.Val( cgiGet( dynavContratada_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV88Contratada_Codigo), 6, 0)));
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultadoexecucao_inicio1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado Execucao_Inicio1"}), 1, "vCONTAGEMRESULTADOEXECUCAO_INICIO1");
               GX_FocusControl = edtavContagemresultadoexecucao_inicio1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV16ContagemResultadoExecucao_Inicio1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContagemResultadoExecucao_Inicio1", context.localUtil.TToC( AV16ContagemResultadoExecucao_Inicio1, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV16ContagemResultadoExecucao_Inicio1 = context.localUtil.CToT( cgiGet( edtavContagemresultadoexecucao_inicio1_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContagemResultadoExecucao_Inicio1", context.localUtil.TToC( AV16ContagemResultadoExecucao_Inicio1, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultadoexecucao_inicio_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado Execucao_Inicio_To1"}), 1, "vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1");
               GX_FocusControl = edtavContagemresultadoexecucao_inicio_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV17ContagemResultadoExecucao_Inicio_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContagemResultadoExecucao_Inicio_To1", context.localUtil.TToC( AV17ContagemResultadoExecucao_Inicio_To1, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV17ContagemResultadoExecucao_Inicio_To1 = context.localUtil.CToT( cgiGet( edtavContagemresultadoexecucao_inicio_to1_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContagemResultadoExecucao_Inicio_To1", context.localUtil.TToC( AV17ContagemResultadoExecucao_Inicio_To1, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultadoexecucao_fim1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado Execucao_Fim1"}), 1, "vCONTAGEMRESULTADOEXECUCAO_FIM1");
               GX_FocusControl = edtavContagemresultadoexecucao_fim1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV18ContagemResultadoExecucao_Fim1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultadoExecucao_Fim1", context.localUtil.TToC( AV18ContagemResultadoExecucao_Fim1, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV18ContagemResultadoExecucao_Fim1 = context.localUtil.CToT( cgiGet( edtavContagemresultadoexecucao_fim1_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultadoExecucao_Fim1", context.localUtil.TToC( AV18ContagemResultadoExecucao_Fim1, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultadoexecucao_fim_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado Execucao_Fim_To1"}), 1, "vCONTAGEMRESULTADOEXECUCAO_FIM_TO1");
               GX_FocusControl = edtavContagemresultadoexecucao_fim_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV19ContagemResultadoExecucao_Fim_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultadoExecucao_Fim_To1", context.localUtil.TToC( AV19ContagemResultadoExecucao_Fim_To1, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV19ContagemResultadoExecucao_Fim_To1 = context.localUtil.CToT( cgiGet( edtavContagemresultadoexecucao_fim_to1_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultadoExecucao_Fim_To1", context.localUtil.TToC( AV19ContagemResultadoExecucao_Fim_To1, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultadoexecucao_prevista1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado Execucao_Prevista1"}), 1, "vCONTAGEMRESULTADOEXECUCAO_PREVISTA1");
               GX_FocusControl = edtavContagemresultadoexecucao_prevista1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV20ContagemResultadoExecucao_Prevista1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContagemResultadoExecucao_Prevista1", context.localUtil.TToC( AV20ContagemResultadoExecucao_Prevista1, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV20ContagemResultadoExecucao_Prevista1 = context.localUtil.CToT( cgiGet( edtavContagemresultadoexecucao_prevista1_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContagemResultadoExecucao_Prevista1", context.localUtil.TToC( AV20ContagemResultadoExecucao_Prevista1, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultadoexecucao_prevista_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado Execucao_Prevista_To1"}), 1, "vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1");
               GX_FocusControl = edtavContagemresultadoexecucao_prevista_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV21ContagemResultadoExecucao_Prevista_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContagemResultadoExecucao_Prevista_To1", context.localUtil.TToC( AV21ContagemResultadoExecucao_Prevista_To1, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV21ContagemResultadoExecucao_Prevista_To1 = context.localUtil.CToT( cgiGet( edtavContagemresultadoexecucao_prevista_to1_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContagemResultadoExecucao_Prevista_To1", context.localUtil.TToC( AV21ContagemResultadoExecucao_Prevista_To1, 8, 5, 0, 3, "/", ":", " "));
            }
            AV43ContagemResultado_OsFsOsFm1 = cgiGet( edtavContagemresultado_osfsosfm1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ContagemResultado_OsFsOsFm1", AV43ContagemResultado_OsFsOsFm1);
            dynavContagemresultado_contratadacod1.Name = dynavContagemresultado_contratadacod1_Internalname;
            dynavContagemresultado_contratadacod1.CurrentValue = cgiGet( dynavContagemresultado_contratadacod1_Internalname);
            AV82ContagemResultado_ContratadaCod1 = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_contratadacod1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82ContagemResultado_ContratadaCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV82ContagemResultado_ContratadaCod1), 6, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemresultadoexecucao_oscod1_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemresultadoexecucao_oscod1_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMRESULTADOEXECUCAO_OSCOD1");
               GX_FocusControl = edtavContagemresultadoexecucao_oscod1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV83ContagemResultadoExecucao_OSCod1 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83ContagemResultadoExecucao_OSCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV83ContagemResultadoExecucao_OSCod1), 6, 0)));
            }
            else
            {
               AV83ContagemResultadoExecucao_OSCod1 = (int)(context.localUtil.CToN( cgiGet( edtavContagemresultadoexecucao_oscod1_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83ContagemResultadoExecucao_OSCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV83ContagemResultadoExecucao_OSCod1), 6, 0)));
            }
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV23DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector2", AV23DynamicFiltersSelector2);
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultadoexecucao_inicio2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado Execucao_Inicio2"}), 1, "vCONTAGEMRESULTADOEXECUCAO_INICIO2");
               GX_FocusControl = edtavContagemresultadoexecucao_inicio2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV24ContagemResultadoExecucao_Inicio2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultadoExecucao_Inicio2", context.localUtil.TToC( AV24ContagemResultadoExecucao_Inicio2, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV24ContagemResultadoExecucao_Inicio2 = context.localUtil.CToT( cgiGet( edtavContagemresultadoexecucao_inicio2_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultadoExecucao_Inicio2", context.localUtil.TToC( AV24ContagemResultadoExecucao_Inicio2, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultadoexecucao_inicio_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado Execucao_Inicio_To2"}), 1, "vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2");
               GX_FocusControl = edtavContagemresultadoexecucao_inicio_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV25ContagemResultadoExecucao_Inicio_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContagemResultadoExecucao_Inicio_To2", context.localUtil.TToC( AV25ContagemResultadoExecucao_Inicio_To2, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV25ContagemResultadoExecucao_Inicio_To2 = context.localUtil.CToT( cgiGet( edtavContagemresultadoexecucao_inicio_to2_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContagemResultadoExecucao_Inicio_To2", context.localUtil.TToC( AV25ContagemResultadoExecucao_Inicio_To2, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultadoexecucao_fim2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado Execucao_Fim2"}), 1, "vCONTAGEMRESULTADOEXECUCAO_FIM2");
               GX_FocusControl = edtavContagemresultadoexecucao_fim2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV26ContagemResultadoExecucao_Fim2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ContagemResultadoExecucao_Fim2", context.localUtil.TToC( AV26ContagemResultadoExecucao_Fim2, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV26ContagemResultadoExecucao_Fim2 = context.localUtil.CToT( cgiGet( edtavContagemresultadoexecucao_fim2_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ContagemResultadoExecucao_Fim2", context.localUtil.TToC( AV26ContagemResultadoExecucao_Fim2, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultadoexecucao_fim_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado Execucao_Fim_To2"}), 1, "vCONTAGEMRESULTADOEXECUCAO_FIM_TO2");
               GX_FocusControl = edtavContagemresultadoexecucao_fim_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV27ContagemResultadoExecucao_Fim_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27ContagemResultadoExecucao_Fim_To2", context.localUtil.TToC( AV27ContagemResultadoExecucao_Fim_To2, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV27ContagemResultadoExecucao_Fim_To2 = context.localUtil.CToT( cgiGet( edtavContagemresultadoexecucao_fim_to2_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27ContagemResultadoExecucao_Fim_To2", context.localUtil.TToC( AV27ContagemResultadoExecucao_Fim_To2, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultadoexecucao_prevista2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado Execucao_Prevista2"}), 1, "vCONTAGEMRESULTADOEXECUCAO_PREVISTA2");
               GX_FocusControl = edtavContagemresultadoexecucao_prevista2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV28ContagemResultadoExecucao_Prevista2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ContagemResultadoExecucao_Prevista2", context.localUtil.TToC( AV28ContagemResultadoExecucao_Prevista2, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV28ContagemResultadoExecucao_Prevista2 = context.localUtil.CToT( cgiGet( edtavContagemresultadoexecucao_prevista2_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ContagemResultadoExecucao_Prevista2", context.localUtil.TToC( AV28ContagemResultadoExecucao_Prevista2, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultadoexecucao_prevista_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado Execucao_Prevista_To2"}), 1, "vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2");
               GX_FocusControl = edtavContagemresultadoexecucao_prevista_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV29ContagemResultadoExecucao_Prevista_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContagemResultadoExecucao_Prevista_To2", context.localUtil.TToC( AV29ContagemResultadoExecucao_Prevista_To2, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV29ContagemResultadoExecucao_Prevista_To2 = context.localUtil.CToT( cgiGet( edtavContagemresultadoexecucao_prevista_to2_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContagemResultadoExecucao_Prevista_To2", context.localUtil.TToC( AV29ContagemResultadoExecucao_Prevista_To2, 8, 5, 0, 3, "/", ":", " "));
            }
            AV44ContagemResultado_OsFsOsFm2 = cgiGet( edtavContagemresultado_osfsosfm2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ContagemResultado_OsFsOsFm2", AV44ContagemResultado_OsFsOsFm2);
            dynavContagemresultado_contratadacod2.Name = dynavContagemresultado_contratadacod2_Internalname;
            dynavContagemresultado_contratadacod2.CurrentValue = cgiGet( dynavContagemresultado_contratadacod2_Internalname);
            AV84ContagemResultado_ContratadaCod2 = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_contratadacod2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84ContagemResultado_ContratadaCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV84ContagemResultado_ContratadaCod2), 6, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemresultadoexecucao_oscod2_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemresultadoexecucao_oscod2_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMRESULTADOEXECUCAO_OSCOD2");
               GX_FocusControl = edtavContagemresultadoexecucao_oscod2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV85ContagemResultadoExecucao_OSCod2 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85ContagemResultadoExecucao_OSCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV85ContagemResultadoExecucao_OSCod2), 6, 0)));
            }
            else
            {
               AV85ContagemResultadoExecucao_OSCod2 = (int)(context.localUtil.CToN( cgiGet( edtavContagemresultadoexecucao_oscod2_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85ContagemResultadoExecucao_OSCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV85ContagemResultadoExecucao_OSCod2), 6, 0)));
            }
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV31DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31DynamicFiltersSelector3", AV31DynamicFiltersSelector3);
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultadoexecucao_inicio3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado Execucao_Inicio3"}), 1, "vCONTAGEMRESULTADOEXECUCAO_INICIO3");
               GX_FocusControl = edtavContagemresultadoexecucao_inicio3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV32ContagemResultadoExecucao_Inicio3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ContagemResultadoExecucao_Inicio3", context.localUtil.TToC( AV32ContagemResultadoExecucao_Inicio3, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV32ContagemResultadoExecucao_Inicio3 = context.localUtil.CToT( cgiGet( edtavContagemresultadoexecucao_inicio3_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ContagemResultadoExecucao_Inicio3", context.localUtil.TToC( AV32ContagemResultadoExecucao_Inicio3, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultadoexecucao_inicio_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado Execucao_Inicio_To3"}), 1, "vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3");
               GX_FocusControl = edtavContagemresultadoexecucao_inicio_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV33ContagemResultadoExecucao_Inicio_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContagemResultadoExecucao_Inicio_To3", context.localUtil.TToC( AV33ContagemResultadoExecucao_Inicio_To3, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV33ContagemResultadoExecucao_Inicio_To3 = context.localUtil.CToT( cgiGet( edtavContagemresultadoexecucao_inicio_to3_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContagemResultadoExecucao_Inicio_To3", context.localUtil.TToC( AV33ContagemResultadoExecucao_Inicio_To3, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultadoexecucao_fim3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado Execucao_Fim3"}), 1, "vCONTAGEMRESULTADOEXECUCAO_FIM3");
               GX_FocusControl = edtavContagemresultadoexecucao_fim3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV34ContagemResultadoExecucao_Fim3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContagemResultadoExecucao_Fim3", context.localUtil.TToC( AV34ContagemResultadoExecucao_Fim3, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV34ContagemResultadoExecucao_Fim3 = context.localUtil.CToT( cgiGet( edtavContagemresultadoexecucao_fim3_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContagemResultadoExecucao_Fim3", context.localUtil.TToC( AV34ContagemResultadoExecucao_Fim3, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultadoexecucao_fim_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado Execucao_Fim_To3"}), 1, "vCONTAGEMRESULTADOEXECUCAO_FIM_TO3");
               GX_FocusControl = edtavContagemresultadoexecucao_fim_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV35ContagemResultadoExecucao_Fim_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35ContagemResultadoExecucao_Fim_To3", context.localUtil.TToC( AV35ContagemResultadoExecucao_Fim_To3, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV35ContagemResultadoExecucao_Fim_To3 = context.localUtil.CToT( cgiGet( edtavContagemresultadoexecucao_fim_to3_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35ContagemResultadoExecucao_Fim_To3", context.localUtil.TToC( AV35ContagemResultadoExecucao_Fim_To3, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultadoexecucao_prevista3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado Execucao_Prevista3"}), 1, "vCONTAGEMRESULTADOEXECUCAO_PREVISTA3");
               GX_FocusControl = edtavContagemresultadoexecucao_prevista3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV36ContagemResultadoExecucao_Prevista3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ContagemResultadoExecucao_Prevista3", context.localUtil.TToC( AV36ContagemResultadoExecucao_Prevista3, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV36ContagemResultadoExecucao_Prevista3 = context.localUtil.CToT( cgiGet( edtavContagemresultadoexecucao_prevista3_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ContagemResultadoExecucao_Prevista3", context.localUtil.TToC( AV36ContagemResultadoExecucao_Prevista3, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultadoexecucao_prevista_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado Execucao_Prevista_To3"}), 1, "vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3");
               GX_FocusControl = edtavContagemresultadoexecucao_prevista_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV37ContagemResultadoExecucao_Prevista_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ContagemResultadoExecucao_Prevista_To3", context.localUtil.TToC( AV37ContagemResultadoExecucao_Prevista_To3, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV37ContagemResultadoExecucao_Prevista_To3 = context.localUtil.CToT( cgiGet( edtavContagemresultadoexecucao_prevista_to3_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ContagemResultadoExecucao_Prevista_To3", context.localUtil.TToC( AV37ContagemResultadoExecucao_Prevista_To3, 8, 5, 0, 3, "/", ":", " "));
            }
            AV45ContagemResultado_OsFsOsFm3 = cgiGet( edtavContagemresultado_osfsosfm3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ContagemResultado_OsFsOsFm3", AV45ContagemResultado_OsFsOsFm3);
            dynavContagemresultado_contratadacod3.Name = dynavContagemresultado_contratadacod3_Internalname;
            dynavContagemresultado_contratadacod3.CurrentValue = cgiGet( dynavContagemresultado_contratadacod3_Internalname);
            AV86ContagemResultado_ContratadaCod3 = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_contratadacod3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86ContagemResultado_ContratadaCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV86ContagemResultado_ContratadaCod3), 6, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemresultadoexecucao_oscod3_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemresultadoexecucao_oscod3_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMRESULTADOEXECUCAO_OSCOD3");
               GX_FocusControl = edtavContagemresultadoexecucao_oscod3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV87ContagemResultadoExecucao_OSCod3 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87ContagemResultadoExecucao_OSCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV87ContagemResultadoExecucao_OSCod3), 6, 0)));
            }
            else
            {
               AV87ContagemResultadoExecucao_OSCod3 = (int)(context.localUtil.CToN( cgiGet( edtavContagemresultadoexecucao_oscod3_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87ContagemResultadoExecucao_OSCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV87ContagemResultadoExecucao_OSCod3), 6, 0)));
            }
            AV22DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled2", AV22DynamicFiltersEnabled2);
            AV30DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersEnabled3", AV30DynamicFiltersEnabled3);
            AV49TFContagemResultado_OsFsOsFm = cgiGet( edtavTfcontagemresultado_osfsosfm_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContagemResultado_OsFsOsFm", AV49TFContagemResultado_OsFsOsFm);
            AV50TFContagemResultado_OsFsOsFm_Sel = cgiGet( edtavTfcontagemresultado_osfsosfm_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContagemResultado_OsFsOsFm_Sel", AV50TFContagemResultado_OsFsOsFm_Sel);
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontagemresultadoexecucao_inicio_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFContagem Resultado Execucao_Inicio"}), 1, "vTFCONTAGEMRESULTADOEXECUCAO_INICIO");
               GX_FocusControl = edtavTfcontagemresultadoexecucao_inicio_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV53TFContagemResultadoExecucao_Inicio = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContagemResultadoExecucao_Inicio", context.localUtil.TToC( AV53TFContagemResultadoExecucao_Inicio, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV53TFContagemResultadoExecucao_Inicio = context.localUtil.CToT( cgiGet( edtavTfcontagemresultadoexecucao_inicio_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContagemResultadoExecucao_Inicio", context.localUtil.TToC( AV53TFContagemResultadoExecucao_Inicio, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontagemresultadoexecucao_inicio_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFContagem Resultado Execucao_Inicio_To"}), 1, "vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO");
               GX_FocusControl = edtavTfcontagemresultadoexecucao_inicio_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV54TFContagemResultadoExecucao_Inicio_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContagemResultadoExecucao_Inicio_To", context.localUtil.TToC( AV54TFContagemResultadoExecucao_Inicio_To, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV54TFContagemResultadoExecucao_Inicio_To = context.localUtil.CToT( cgiGet( edtavTfcontagemresultadoexecucao_inicio_to_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContagemResultadoExecucao_Inicio_To", context.localUtil.TToC( AV54TFContagemResultadoExecucao_Inicio_To, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contagemresultadoexecucao_inicioauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contagem Resultado Execucao_Inicio Aux Date"}), 1, "vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOAUXDATE");
               GX_FocusControl = edtavDdo_contagemresultadoexecucao_inicioauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV55DDO_ContagemResultadoExecucao_InicioAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55DDO_ContagemResultadoExecucao_InicioAuxDate", context.localUtil.Format(AV55DDO_ContagemResultadoExecucao_InicioAuxDate, "99/99/99"));
            }
            else
            {
               AV55DDO_ContagemResultadoExecucao_InicioAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contagemresultadoexecucao_inicioauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55DDO_ContagemResultadoExecucao_InicioAuxDate", context.localUtil.Format(AV55DDO_ContagemResultadoExecucao_InicioAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contagemresultadoexecucao_inicioauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contagem Resultado Execucao_Inicio Aux Date To"}), 1, "vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOAUXDATETO");
               GX_FocusControl = edtavDdo_contagemresultadoexecucao_inicioauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV56DDO_ContagemResultadoExecucao_InicioAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56DDO_ContagemResultadoExecucao_InicioAuxDateTo", context.localUtil.Format(AV56DDO_ContagemResultadoExecucao_InicioAuxDateTo, "99/99/99"));
            }
            else
            {
               AV56DDO_ContagemResultadoExecucao_InicioAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contagemresultadoexecucao_inicioauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56DDO_ContagemResultadoExecucao_InicioAuxDateTo", context.localUtil.Format(AV56DDO_ContagemResultadoExecucao_InicioAuxDateTo, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontagemresultadoexecucao_prevista_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFContagem Resultado Execucao_Prevista"}), 1, "vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA");
               GX_FocusControl = edtavTfcontagemresultadoexecucao_prevista_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV59TFContagemResultadoExecucao_Prevista = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContagemResultadoExecucao_Prevista", context.localUtil.TToC( AV59TFContagemResultadoExecucao_Prevista, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV59TFContagemResultadoExecucao_Prevista = context.localUtil.CToT( cgiGet( edtavTfcontagemresultadoexecucao_prevista_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContagemResultadoExecucao_Prevista", context.localUtil.TToC( AV59TFContagemResultadoExecucao_Prevista, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontagemresultadoexecucao_prevista_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFContagem Resultado Execucao_Prevista_To"}), 1, "vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO");
               GX_FocusControl = edtavTfcontagemresultadoexecucao_prevista_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV60TFContagemResultadoExecucao_Prevista_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContagemResultadoExecucao_Prevista_To", context.localUtil.TToC( AV60TFContagemResultadoExecucao_Prevista_To, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV60TFContagemResultadoExecucao_Prevista_To = context.localUtil.CToT( cgiGet( edtavTfcontagemresultadoexecucao_prevista_to_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContagemResultadoExecucao_Prevista_To", context.localUtil.TToC( AV60TFContagemResultadoExecucao_Prevista_To, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contagemresultadoexecucao_previstaauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contagem Resultado Execucao_Prevista Aux Date"}), 1, "vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAAUXDATE");
               GX_FocusControl = edtavDdo_contagemresultadoexecucao_previstaauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV61DDO_ContagemResultadoExecucao_PrevistaAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61DDO_ContagemResultadoExecucao_PrevistaAuxDate", context.localUtil.Format(AV61DDO_ContagemResultadoExecucao_PrevistaAuxDate, "99/99/99"));
            }
            else
            {
               AV61DDO_ContagemResultadoExecucao_PrevistaAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contagemresultadoexecucao_previstaauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61DDO_ContagemResultadoExecucao_PrevistaAuxDate", context.localUtil.Format(AV61DDO_ContagemResultadoExecucao_PrevistaAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contagemresultadoexecucao_previstaauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contagem Resultado Execucao_Prevista Aux Date To"}), 1, "vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAAUXDATETO");
               GX_FocusControl = edtavDdo_contagemresultadoexecucao_previstaauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV62DDO_ContagemResultadoExecucao_PrevistaAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62DDO_ContagemResultadoExecucao_PrevistaAuxDateTo", context.localUtil.Format(AV62DDO_ContagemResultadoExecucao_PrevistaAuxDateTo, "99/99/99"));
            }
            else
            {
               AV62DDO_ContagemResultadoExecucao_PrevistaAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contagemresultadoexecucao_previstaauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62DDO_ContagemResultadoExecucao_PrevistaAuxDateTo", context.localUtil.Format(AV62DDO_ContagemResultadoExecucao_PrevistaAuxDateTo, "99/99/99"));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultadoexecucao_prazodias_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultadoexecucao_prazodias_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS");
               GX_FocusControl = edtavTfcontagemresultadoexecucao_prazodias_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV65TFContagemResultadoExecucao_PrazoDias = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContagemResultadoExecucao_PrazoDias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65TFContagemResultadoExecucao_PrazoDias), 4, 0)));
            }
            else
            {
               AV65TFContagemResultadoExecucao_PrazoDias = (short)(context.localUtil.CToN( cgiGet( edtavTfcontagemresultadoexecucao_prazodias_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContagemResultadoExecucao_PrazoDias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65TFContagemResultadoExecucao_PrazoDias), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultadoexecucao_prazodias_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultadoexecucao_prazodias_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO");
               GX_FocusControl = edtavTfcontagemresultadoexecucao_prazodias_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV66TFContagemResultadoExecucao_PrazoDias_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContagemResultadoExecucao_PrazoDias_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66TFContagemResultadoExecucao_PrazoDias_To), 4, 0)));
            }
            else
            {
               AV66TFContagemResultadoExecucao_PrazoDias_To = (short)(context.localUtil.CToN( cgiGet( edtavTfcontagemresultadoexecucao_prazodias_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContagemResultadoExecucao_PrazoDias_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66TFContagemResultadoExecucao_PrazoDias_To), 4, 0)));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontagemresultadoexecucao_fim_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFContagem Resultado Execucao_Fim"}), 1, "vTFCONTAGEMRESULTADOEXECUCAO_FIM");
               GX_FocusControl = edtavTfcontagemresultadoexecucao_fim_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV69TFContagemResultadoExecucao_Fim = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFContagemResultadoExecucao_Fim", context.localUtil.TToC( AV69TFContagemResultadoExecucao_Fim, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV69TFContagemResultadoExecucao_Fim = context.localUtil.CToT( cgiGet( edtavTfcontagemresultadoexecucao_fim_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFContagemResultadoExecucao_Fim", context.localUtil.TToC( AV69TFContagemResultadoExecucao_Fim, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontagemresultadoexecucao_fim_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFContagem Resultado Execucao_Fim_To"}), 1, "vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO");
               GX_FocusControl = edtavTfcontagemresultadoexecucao_fim_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV70TFContagemResultadoExecucao_Fim_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContagemResultadoExecucao_Fim_To", context.localUtil.TToC( AV70TFContagemResultadoExecucao_Fim_To, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV70TFContagemResultadoExecucao_Fim_To = context.localUtil.CToT( cgiGet( edtavTfcontagemresultadoexecucao_fim_to_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContagemResultadoExecucao_Fim_To", context.localUtil.TToC( AV70TFContagemResultadoExecucao_Fim_To, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contagemresultadoexecucao_fimauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contagem Resultado Execucao_Fim Aux Date"}), 1, "vDDO_CONTAGEMRESULTADOEXECUCAO_FIMAUXDATE");
               GX_FocusControl = edtavDdo_contagemresultadoexecucao_fimauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV71DDO_ContagemResultadoExecucao_FimAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71DDO_ContagemResultadoExecucao_FimAuxDate", context.localUtil.Format(AV71DDO_ContagemResultadoExecucao_FimAuxDate, "99/99/99"));
            }
            else
            {
               AV71DDO_ContagemResultadoExecucao_FimAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contagemresultadoexecucao_fimauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71DDO_ContagemResultadoExecucao_FimAuxDate", context.localUtil.Format(AV71DDO_ContagemResultadoExecucao_FimAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contagemresultadoexecucao_fimauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contagem Resultado Execucao_Fim Aux Date To"}), 1, "vDDO_CONTAGEMRESULTADOEXECUCAO_FIMAUXDATETO");
               GX_FocusControl = edtavDdo_contagemresultadoexecucao_fimauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV72DDO_ContagemResultadoExecucao_FimAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72DDO_ContagemResultadoExecucao_FimAuxDateTo", context.localUtil.Format(AV72DDO_ContagemResultadoExecucao_FimAuxDateTo, "99/99/99"));
            }
            else
            {
               AV72DDO_ContagemResultadoExecucao_FimAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contagemresultadoexecucao_fimauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72DDO_ContagemResultadoExecucao_FimAuxDateTo", context.localUtil.Format(AV72DDO_ContagemResultadoExecucao_FimAuxDateTo, "99/99/99"));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultadoexecucao_dias_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultadoexecucao_dias_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMRESULTADOEXECUCAO_DIAS");
               GX_FocusControl = edtavTfcontagemresultadoexecucao_dias_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV75TFContagemResultadoExecucao_Dias = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFContagemResultadoExecucao_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV75TFContagemResultadoExecucao_Dias), 4, 0)));
            }
            else
            {
               AV75TFContagemResultadoExecucao_Dias = (short)(context.localUtil.CToN( cgiGet( edtavTfcontagemresultadoexecucao_dias_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFContagemResultadoExecucao_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV75TFContagemResultadoExecucao_Dias), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultadoexecucao_dias_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultadoexecucao_dias_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO");
               GX_FocusControl = edtavTfcontagemresultadoexecucao_dias_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV76TFContagemResultadoExecucao_Dias_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFContagemResultadoExecucao_Dias_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV76TFContagemResultadoExecucao_Dias_To), 4, 0)));
            }
            else
            {
               AV76TFContagemResultadoExecucao_Dias_To = (short)(context.localUtil.CToN( cgiGet( edtavTfcontagemresultadoexecucao_dias_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFContagemResultadoExecucao_Dias_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV76TFContagemResultadoExecucao_Dias_To), 4, 0)));
            }
            AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultado_osfsosfmtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace", AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace);
            AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultadoexecucao_iniciotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace", AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace);
            AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultadoexecucao_previstatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace", AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace);
            AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultadoexecucao_prazodiastitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace", AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace);
            AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultadoexecucao_fimtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace", AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace);
            AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultadoexecucao_diastitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace", AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_152 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_152"), ",", "."));
            AV80GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV81GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contagemresultado_osfsosfm_Caption = cgiGet( "DDO_CONTAGEMRESULTADO_OSFSOSFM_Caption");
            Ddo_contagemresultado_osfsosfm_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADO_OSFSOSFM_Tooltip");
            Ddo_contagemresultado_osfsosfm_Cls = cgiGet( "DDO_CONTAGEMRESULTADO_OSFSOSFM_Cls");
            Ddo_contagemresultado_osfsosfm_Filteredtext_set = cgiGet( "DDO_CONTAGEMRESULTADO_OSFSOSFM_Filteredtext_set");
            Ddo_contagemresultado_osfsosfm_Selectedvalue_set = cgiGet( "DDO_CONTAGEMRESULTADO_OSFSOSFM_Selectedvalue_set");
            Ddo_contagemresultado_osfsosfm_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADO_OSFSOSFM_Dropdownoptionstype");
            Ddo_contagemresultado_osfsosfm_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADO_OSFSOSFM_Titlecontrolidtoreplace");
            Ddo_contagemresultado_osfsosfm_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_OSFSOSFM_Includesortasc"));
            Ddo_contagemresultado_osfsosfm_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_OSFSOSFM_Includesortdsc"));
            Ddo_contagemresultado_osfsosfm_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_OSFSOSFM_Includefilter"));
            Ddo_contagemresultado_osfsosfm_Filtertype = cgiGet( "DDO_CONTAGEMRESULTADO_OSFSOSFM_Filtertype");
            Ddo_contagemresultado_osfsosfm_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_OSFSOSFM_Filterisrange"));
            Ddo_contagemresultado_osfsosfm_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_OSFSOSFM_Includedatalist"));
            Ddo_contagemresultado_osfsosfm_Datalisttype = cgiGet( "DDO_CONTAGEMRESULTADO_OSFSOSFM_Datalisttype");
            Ddo_contagemresultado_osfsosfm_Datalistproc = cgiGet( "DDO_CONTAGEMRESULTADO_OSFSOSFM_Datalistproc");
            Ddo_contagemresultado_osfsosfm_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTAGEMRESULTADO_OSFSOSFM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contagemresultado_osfsosfm_Loadingdata = cgiGet( "DDO_CONTAGEMRESULTADO_OSFSOSFM_Loadingdata");
            Ddo_contagemresultado_osfsosfm_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADO_OSFSOSFM_Cleanfilter");
            Ddo_contagemresultado_osfsosfm_Noresultsfound = cgiGet( "DDO_CONTAGEMRESULTADO_OSFSOSFM_Noresultsfound");
            Ddo_contagemresultado_osfsosfm_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADO_OSFSOSFM_Searchbuttontext");
            Ddo_contagemresultadoexecucao_inicio_Caption = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Caption");
            Ddo_contagemresultadoexecucao_inicio_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Tooltip");
            Ddo_contagemresultadoexecucao_inicio_Cls = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Cls");
            Ddo_contagemresultadoexecucao_inicio_Filteredtext_set = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Filteredtext_set");
            Ddo_contagemresultadoexecucao_inicio_Filteredtextto_set = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Filteredtextto_set");
            Ddo_contagemresultadoexecucao_inicio_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Dropdownoptionstype");
            Ddo_contagemresultadoexecucao_inicio_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Titlecontrolidtoreplace");
            Ddo_contagemresultadoexecucao_inicio_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Includesortasc"));
            Ddo_contagemresultadoexecucao_inicio_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Includesortdsc"));
            Ddo_contagemresultadoexecucao_inicio_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Includefilter"));
            Ddo_contagemresultadoexecucao_inicio_Filtertype = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Filtertype");
            Ddo_contagemresultadoexecucao_inicio_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Filterisrange"));
            Ddo_contagemresultadoexecucao_inicio_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Includedatalist"));
            Ddo_contagemresultadoexecucao_inicio_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Cleanfilter");
            Ddo_contagemresultadoexecucao_inicio_Rangefilterfrom = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Rangefilterfrom");
            Ddo_contagemresultadoexecucao_inicio_Rangefilterto = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Rangefilterto");
            Ddo_contagemresultadoexecucao_inicio_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Searchbuttontext");
            Ddo_contagemresultadoexecucao_prevista_Caption = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Caption");
            Ddo_contagemresultadoexecucao_prevista_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Tooltip");
            Ddo_contagemresultadoexecucao_prevista_Cls = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Cls");
            Ddo_contagemresultadoexecucao_prevista_Filteredtext_set = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Filteredtext_set");
            Ddo_contagemresultadoexecucao_prevista_Filteredtextto_set = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Filteredtextto_set");
            Ddo_contagemresultadoexecucao_prevista_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Dropdownoptionstype");
            Ddo_contagemresultadoexecucao_prevista_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Titlecontrolidtoreplace");
            Ddo_contagemresultadoexecucao_prevista_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Includesortasc"));
            Ddo_contagemresultadoexecucao_prevista_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Includesortdsc"));
            Ddo_contagemresultadoexecucao_prevista_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Includefilter"));
            Ddo_contagemresultadoexecucao_prevista_Filtertype = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Filtertype");
            Ddo_contagemresultadoexecucao_prevista_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Filterisrange"));
            Ddo_contagemresultadoexecucao_prevista_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Includedatalist"));
            Ddo_contagemresultadoexecucao_prevista_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Cleanfilter");
            Ddo_contagemresultadoexecucao_prevista_Rangefilterfrom = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Rangefilterfrom");
            Ddo_contagemresultadoexecucao_prevista_Rangefilterto = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Rangefilterto");
            Ddo_contagemresultadoexecucao_prevista_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Searchbuttontext");
            Ddo_contagemresultadoexecucao_prazodias_Caption = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Caption");
            Ddo_contagemresultadoexecucao_prazodias_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Tooltip");
            Ddo_contagemresultadoexecucao_prazodias_Cls = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Cls");
            Ddo_contagemresultadoexecucao_prazodias_Filteredtext_set = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Filteredtext_set");
            Ddo_contagemresultadoexecucao_prazodias_Filteredtextto_set = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Filteredtextto_set");
            Ddo_contagemresultadoexecucao_prazodias_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Dropdownoptionstype");
            Ddo_contagemresultadoexecucao_prazodias_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Titlecontrolidtoreplace");
            Ddo_contagemresultadoexecucao_prazodias_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Includesortasc"));
            Ddo_contagemresultadoexecucao_prazodias_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Includesortdsc"));
            Ddo_contagemresultadoexecucao_prazodias_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Includefilter"));
            Ddo_contagemresultadoexecucao_prazodias_Filtertype = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Filtertype");
            Ddo_contagemresultadoexecucao_prazodias_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Filterisrange"));
            Ddo_contagemresultadoexecucao_prazodias_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Includedatalist"));
            Ddo_contagemresultadoexecucao_prazodias_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Cleanfilter");
            Ddo_contagemresultadoexecucao_prazodias_Rangefilterfrom = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Rangefilterfrom");
            Ddo_contagemresultadoexecucao_prazodias_Rangefilterto = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Rangefilterto");
            Ddo_contagemresultadoexecucao_prazodias_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Searchbuttontext");
            Ddo_contagemresultadoexecucao_fim_Caption = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Caption");
            Ddo_contagemresultadoexecucao_fim_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Tooltip");
            Ddo_contagemresultadoexecucao_fim_Cls = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Cls");
            Ddo_contagemresultadoexecucao_fim_Filteredtext_set = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Filteredtext_set");
            Ddo_contagemresultadoexecucao_fim_Filteredtextto_set = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Filteredtextto_set");
            Ddo_contagemresultadoexecucao_fim_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Dropdownoptionstype");
            Ddo_contagemresultadoexecucao_fim_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Titlecontrolidtoreplace");
            Ddo_contagemresultadoexecucao_fim_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Includesortasc"));
            Ddo_contagemresultadoexecucao_fim_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Includesortdsc"));
            Ddo_contagemresultadoexecucao_fim_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Includefilter"));
            Ddo_contagemresultadoexecucao_fim_Filtertype = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Filtertype");
            Ddo_contagemresultadoexecucao_fim_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Filterisrange"));
            Ddo_contagemresultadoexecucao_fim_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Includedatalist"));
            Ddo_contagemresultadoexecucao_fim_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Cleanfilter");
            Ddo_contagemresultadoexecucao_fim_Rangefilterfrom = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Rangefilterfrom");
            Ddo_contagemresultadoexecucao_fim_Rangefilterto = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Rangefilterto");
            Ddo_contagemresultadoexecucao_fim_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Searchbuttontext");
            Ddo_contagemresultadoexecucao_dias_Caption = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Caption");
            Ddo_contagemresultadoexecucao_dias_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Tooltip");
            Ddo_contagemresultadoexecucao_dias_Cls = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Cls");
            Ddo_contagemresultadoexecucao_dias_Filteredtext_set = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Filteredtext_set");
            Ddo_contagemresultadoexecucao_dias_Filteredtextto_set = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Filteredtextto_set");
            Ddo_contagemresultadoexecucao_dias_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Dropdownoptionstype");
            Ddo_contagemresultadoexecucao_dias_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Titlecontrolidtoreplace");
            Ddo_contagemresultadoexecucao_dias_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Includesortasc"));
            Ddo_contagemresultadoexecucao_dias_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Includesortdsc"));
            Ddo_contagemresultadoexecucao_dias_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Includefilter"));
            Ddo_contagemresultadoexecucao_dias_Filtertype = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Filtertype");
            Ddo_contagemresultadoexecucao_dias_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Filterisrange"));
            Ddo_contagemresultadoexecucao_dias_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Includedatalist"));
            Ddo_contagemresultadoexecucao_dias_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Cleanfilter");
            Ddo_contagemresultadoexecucao_dias_Rangefilterfrom = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Rangefilterfrom");
            Ddo_contagemresultadoexecucao_dias_Rangefilterto = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Rangefilterto");
            Ddo_contagemresultadoexecucao_dias_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contagemresultado_osfsosfm_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADO_OSFSOSFM_Activeeventkey");
            Ddo_contagemresultado_osfsosfm_Filteredtext_get = cgiGet( "DDO_CONTAGEMRESULTADO_OSFSOSFM_Filteredtext_get");
            Ddo_contagemresultado_osfsosfm_Selectedvalue_get = cgiGet( "DDO_CONTAGEMRESULTADO_OSFSOSFM_Selectedvalue_get");
            Ddo_contagemresultadoexecucao_inicio_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Activeeventkey");
            Ddo_contagemresultadoexecucao_inicio_Filteredtext_get = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Filteredtext_get");
            Ddo_contagemresultadoexecucao_inicio_Filteredtextto_get = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Filteredtextto_get");
            Ddo_contagemresultadoexecucao_prevista_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Activeeventkey");
            Ddo_contagemresultadoexecucao_prevista_Filteredtext_get = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Filteredtext_get");
            Ddo_contagemresultadoexecucao_prevista_Filteredtextto_get = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Filteredtextto_get");
            Ddo_contagemresultadoexecucao_prazodias_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Activeeventkey");
            Ddo_contagemresultadoexecucao_prazodias_Filteredtext_get = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Filteredtext_get");
            Ddo_contagemresultadoexecucao_prazodias_Filteredtextto_get = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Filteredtextto_get");
            Ddo_contagemresultadoexecucao_fim_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Activeeventkey");
            Ddo_contagemresultadoexecucao_fim_Filteredtext_get = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Filteredtext_get");
            Ddo_contagemresultadoexecucao_fim_Filteredtextto_get = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Filteredtextto_get");
            Ddo_contagemresultadoexecucao_dias_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Activeeventkey");
            Ddo_contagemresultadoexecucao_dias_Filteredtext_get = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Filteredtext_get");
            Ddo_contagemresultadoexecucao_dias_Filteredtextto_get = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_INICIO1"), 0) != AV16ContagemResultadoExecucao_Inicio1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1"), 0) != AV17ContagemResultadoExecucao_Inicio_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_FIM1"), 0) != AV18ContagemResultadoExecucao_Fim1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_FIM_TO1"), 0) != AV19ContagemResultadoExecucao_Fim_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_PREVISTA1"), 0) != AV20ContagemResultadoExecucao_Prevista1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1"), 0) != AV21ContagemResultadoExecucao_Prevista_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_OSFSOSFM1"), AV43ContagemResultado_OsFsOsFm1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADO_CONTRATADACOD1"), ",", ".") != Convert.ToDecimal( AV82ContagemResultado_ContratadaCod1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_OSCOD1"), ",", ".") != Convert.ToDecimal( AV83ContagemResultadoExecucao_OSCod1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV23DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_INICIO2"), 0) != AV24ContagemResultadoExecucao_Inicio2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2"), 0) != AV25ContagemResultadoExecucao_Inicio_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_FIM2"), 0) != AV26ContagemResultadoExecucao_Fim2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_FIM_TO2"), 0) != AV27ContagemResultadoExecucao_Fim_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_PREVISTA2"), 0) != AV28ContagemResultadoExecucao_Prevista2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2"), 0) != AV29ContagemResultadoExecucao_Prevista_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_OSFSOSFM2"), AV44ContagemResultado_OsFsOsFm2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADO_CONTRATADACOD2"), ",", ".") != Convert.ToDecimal( AV84ContagemResultado_ContratadaCod2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_OSCOD2"), ",", ".") != Convert.ToDecimal( AV85ContagemResultadoExecucao_OSCod2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV31DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_INICIO3"), 0) != AV32ContagemResultadoExecucao_Inicio3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3"), 0) != AV33ContagemResultadoExecucao_Inicio_To3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_FIM3"), 0) != AV34ContagemResultadoExecucao_Fim3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_FIM_TO3"), 0) != AV35ContagemResultadoExecucao_Fim_To3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_PREVISTA3"), 0) != AV36ContagemResultadoExecucao_Prevista3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3"), 0) != AV37ContagemResultadoExecucao_Prevista_To3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_OSFSOSFM3"), AV45ContagemResultado_OsFsOsFm3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADO_CONTRATADACOD3"), ",", ".") != Convert.ToDecimal( AV86ContagemResultado_ContratadaCod3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_OSCOD3"), ",", ".") != Convert.ToDecimal( AV87ContagemResultadoExecucao_OSCod3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV22DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV30DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_OSFSOSFM"), AV49TFContagemResultado_OsFsOsFm) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADO_OSFSOSFM_SEL"), AV50TFContagemResultado_OsFsOsFm_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_INICIO"), 0) != AV53TFContagemResultadoExecucao_Inicio )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO"), 0) != AV54TFContagemResultadoExecucao_Inicio_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA"), 0) != AV59TFContagemResultadoExecucao_Prevista )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO"), 0) != AV60TFContagemResultadoExecucao_Prevista_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS"), ",", ".") != Convert.ToDecimal( AV65TFContagemResultadoExecucao_PrazoDias )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO"), ",", ".") != Convert.ToDecimal( AV66TFContagemResultadoExecucao_PrazoDias_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_FIM"), 0) != AV69TFContagemResultadoExecucao_Fim )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO"), 0) != AV70TFContagemResultadoExecucao_Fim_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_DIAS"), ",", ".") != Convert.ToDecimal( AV75TFContagemResultadoExecucao_Dias )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO"), ",", ".") != Convert.ToDecimal( AV76TFContagemResultadoExecucao_Dias_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E28KI2 */
         E28KI2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E28KI2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "CONTAGEMRESULTADOEXECUCAO_INICIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector2 = "CONTAGEMRESULTADOEXECUCAO_INICIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector2", AV23DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV31DynamicFiltersSelector3 = "CONTAGEMRESULTADOEXECUCAO_INICIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31DynamicFiltersSelector3", AV31DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfcontagemresultado_osfsosfm_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_osfsosfm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_osfsosfm_Visible), 5, 0)));
         edtavTfcontagemresultado_osfsosfm_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_osfsosfm_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_osfsosfm_sel_Visible), 5, 0)));
         edtavTfcontagemresultadoexecucao_inicio_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultadoexecucao_inicio_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadoexecucao_inicio_Visible), 5, 0)));
         edtavTfcontagemresultadoexecucao_inicio_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultadoexecucao_inicio_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadoexecucao_inicio_to_Visible), 5, 0)));
         edtavTfcontagemresultadoexecucao_prevista_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultadoexecucao_prevista_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadoexecucao_prevista_Visible), 5, 0)));
         edtavTfcontagemresultadoexecucao_prevista_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultadoexecucao_prevista_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadoexecucao_prevista_to_Visible), 5, 0)));
         edtavTfcontagemresultadoexecucao_prazodias_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultadoexecucao_prazodias_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadoexecucao_prazodias_Visible), 5, 0)));
         edtavTfcontagemresultadoexecucao_prazodias_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultadoexecucao_prazodias_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadoexecucao_prazodias_to_Visible), 5, 0)));
         edtavTfcontagemresultadoexecucao_fim_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultadoexecucao_fim_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadoexecucao_fim_Visible), 5, 0)));
         edtavTfcontagemresultadoexecucao_fim_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultadoexecucao_fim_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadoexecucao_fim_to_Visible), 5, 0)));
         edtavTfcontagemresultadoexecucao_dias_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultadoexecucao_dias_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadoexecucao_dias_Visible), 5, 0)));
         edtavTfcontagemresultadoexecucao_dias_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultadoexecucao_dias_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadoexecucao_dias_to_Visible), 5, 0)));
         Ddo_contagemresultado_osfsosfm_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultado_OsFsOsFm";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_osfsosfm_Internalname, "TitleControlIdToReplace", Ddo_contagemresultado_osfsosfm_Titlecontrolidtoreplace);
         AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace = Ddo_contagemresultado_osfsosfm_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace", AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace);
         edtavDdo_contagemresultado_osfsosfmtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultado_osfsosfmtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultado_osfsosfmtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultadoexecucao_inicio_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultadoExecucao_Inicio";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_inicio_Internalname, "TitleControlIdToReplace", Ddo_contagemresultadoexecucao_inicio_Titlecontrolidtoreplace);
         AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace = Ddo_contagemresultadoexecucao_inicio_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace", AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace);
         edtavDdo_contagemresultadoexecucao_iniciotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultadoexecucao_iniciotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultadoexecucao_iniciotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultadoexecucao_prevista_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultadoExecucao_Prevista";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_prevista_Internalname, "TitleControlIdToReplace", Ddo_contagemresultadoexecucao_prevista_Titlecontrolidtoreplace);
         AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace = Ddo_contagemresultadoexecucao_prevista_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace", AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace);
         edtavDdo_contagemresultadoexecucao_previstatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultadoexecucao_previstatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultadoexecucao_previstatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultadoexecucao_prazodias_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultadoExecucao_PrazoDias";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_prazodias_Internalname, "TitleControlIdToReplace", Ddo_contagemresultadoexecucao_prazodias_Titlecontrolidtoreplace);
         AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace = Ddo_contagemresultadoexecucao_prazodias_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace", AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace);
         edtavDdo_contagemresultadoexecucao_prazodiastitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultadoexecucao_prazodiastitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultadoexecucao_prazodiastitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultadoexecucao_fim_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultadoExecucao_Fim";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_fim_Internalname, "TitleControlIdToReplace", Ddo_contagemresultadoexecucao_fim_Titlecontrolidtoreplace);
         AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace = Ddo_contagemresultadoexecucao_fim_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace", AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace);
         edtavDdo_contagemresultadoexecucao_fimtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultadoexecucao_fimtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultadoexecucao_fimtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultadoexecucao_dias_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultadoExecucao_Dias";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_dias_Internalname, "TitleControlIdToReplace", Ddo_contagemresultadoexecucao_dias_Titlecontrolidtoreplace);
         AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace = Ddo_contagemresultadoexecucao_dias_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace", AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace);
         edtavDdo_contagemresultadoexecucao_diastitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultadoexecucao_diastitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultadoexecucao_diastitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Contagem Resultado Execucao";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV78DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV78DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
         if ( AV6WWPContext.gxTpr_Contratada_codigo > 0 )
         {
            cmbavDynamicfiltersselector1.removeItem("CONTAGEMRESULTADO_CONTRATADACOD");
            cmbavDynamicfiltersselector2.removeItem("CONTAGEMRESULTADO_CONTRATADACOD");
            cmbavDynamicfiltersselector3.removeItem("CONTAGEMRESULTADO_CONTRATADACOD");
         }
      }

      protected void E29KI2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV48ContagemResultado_OsFsOsFmTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV52ContagemResultadoExecucao_InicioTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV58ContagemResultadoExecucao_PrevistaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV64ContagemResultadoExecucao_PrazoDiasTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV68ContagemResultadoExecucao_FimTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV74ContagemResultadoExecucao_DiasTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContagemResultado_OsFsOsFm_Titleformat = 2;
         edtContagemResultado_OsFsOsFm_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "OS Ref|OS", AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_OsFsOsFm_Internalname, "Title", edtContagemResultado_OsFsOsFm_Title);
         edtContagemResultadoExecucao_Inicio_Titleformat = 2;
         edtContagemResultadoExecucao_Inicio_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Inicio", AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoExecucao_Inicio_Internalname, "Title", edtContagemResultadoExecucao_Inicio_Title);
         edtContagemResultadoExecucao_Prevista_Titleformat = 2;
         edtContagemResultadoExecucao_Prevista_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Previsto", AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoExecucao_Prevista_Internalname, "Title", edtContagemResultadoExecucao_Prevista_Title);
         edtContagemResultadoExecucao_PrazoDias_Titleformat = 2;
         edtContagemResultadoExecucao_PrazoDias_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Dias", AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoExecucao_PrazoDias_Internalname, "Title", edtContagemResultadoExecucao_PrazoDias_Title);
         edtContagemResultadoExecucao_Fim_Titleformat = 2;
         edtContagemResultadoExecucao_Fim_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Fim", AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoExecucao_Fim_Internalname, "Title", edtContagemResultadoExecucao_Fim_Title);
         edtContagemResultadoExecucao_Dias_Titleformat = 2;
         edtContagemResultadoExecucao_Dias_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Usado", AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoExecucao_Dias_Internalname, "Title", edtContagemResultadoExecucao_Dias_Title);
         AV80GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV80GridCurrentPage), 10, 0)));
         AV81GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV81GridPageCount), 10, 0)));
         AV91WWContagemResultadoExecucaoDS_1_Contratada_codigo = AV88Contratada_Codigo;
         AV92WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV93WWContagemResultadoExecucaoDS_3_Contagemresultadoexecucao_inicio1 = AV16ContagemResultadoExecucao_Inicio1;
         AV94WWContagemResultadoExecucaoDS_4_Contagemresultadoexecucao_inicio_to1 = AV17ContagemResultadoExecucao_Inicio_To1;
         AV95WWContagemResultadoExecucaoDS_5_Contagemresultadoexecucao_fim1 = AV18ContagemResultadoExecucao_Fim1;
         AV96WWContagemResultadoExecucaoDS_6_Contagemresultadoexecucao_fim_to1 = AV19ContagemResultadoExecucao_Fim_To1;
         AV97WWContagemResultadoExecucaoDS_7_Contagemresultadoexecucao_prevista1 = AV20ContagemResultadoExecucao_Prevista1;
         AV98WWContagemResultadoExecucaoDS_8_Contagemresultadoexecucao_prevista_to1 = AV21ContagemResultadoExecucao_Prevista_To1;
         AV99WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1 = AV43ContagemResultado_OsFsOsFm1;
         AV100WWContagemResultadoExecucaoDS_10_Contagemresultado_contratadacod1 = AV82ContagemResultado_ContratadaCod1;
         AV101WWContagemResultadoExecucaoDS_11_Contagemresultadoexecucao_oscod1 = AV83ContagemResultadoExecucao_OSCod1;
         AV102WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 = AV22DynamicFiltersEnabled2;
         AV103WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2 = AV23DynamicFiltersSelector2;
         AV104WWContagemResultadoExecucaoDS_14_Contagemresultadoexecucao_inicio2 = AV24ContagemResultadoExecucao_Inicio2;
         AV105WWContagemResultadoExecucaoDS_15_Contagemresultadoexecucao_inicio_to2 = AV25ContagemResultadoExecucao_Inicio_To2;
         AV106WWContagemResultadoExecucaoDS_16_Contagemresultadoexecucao_fim2 = AV26ContagemResultadoExecucao_Fim2;
         AV107WWContagemResultadoExecucaoDS_17_Contagemresultadoexecucao_fim_to2 = AV27ContagemResultadoExecucao_Fim_To2;
         AV108WWContagemResultadoExecucaoDS_18_Contagemresultadoexecucao_prevista2 = AV28ContagemResultadoExecucao_Prevista2;
         AV109WWContagemResultadoExecucaoDS_19_Contagemresultadoexecucao_prevista_to2 = AV29ContagemResultadoExecucao_Prevista_To2;
         AV110WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2 = AV44ContagemResultado_OsFsOsFm2;
         AV111WWContagemResultadoExecucaoDS_21_Contagemresultado_contratadacod2 = AV84ContagemResultado_ContratadaCod2;
         AV112WWContagemResultadoExecucaoDS_22_Contagemresultadoexecucao_oscod2 = AV85ContagemResultadoExecucao_OSCod2;
         AV113WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 = AV30DynamicFiltersEnabled3;
         AV114WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3 = AV31DynamicFiltersSelector3;
         AV115WWContagemResultadoExecucaoDS_25_Contagemresultadoexecucao_inicio3 = AV32ContagemResultadoExecucao_Inicio3;
         AV116WWContagemResultadoExecucaoDS_26_Contagemresultadoexecucao_inicio_to3 = AV33ContagemResultadoExecucao_Inicio_To3;
         AV117WWContagemResultadoExecucaoDS_27_Contagemresultadoexecucao_fim3 = AV34ContagemResultadoExecucao_Fim3;
         AV118WWContagemResultadoExecucaoDS_28_Contagemresultadoexecucao_fim_to3 = AV35ContagemResultadoExecucao_Fim_To3;
         AV119WWContagemResultadoExecucaoDS_29_Contagemresultadoexecucao_prevista3 = AV36ContagemResultadoExecucao_Prevista3;
         AV120WWContagemResultadoExecucaoDS_30_Contagemresultadoexecucao_prevista_to3 = AV37ContagemResultadoExecucao_Prevista_To3;
         AV121WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3 = AV45ContagemResultado_OsFsOsFm3;
         AV122WWContagemResultadoExecucaoDS_32_Contagemresultado_contratadacod3 = AV86ContagemResultado_ContratadaCod3;
         AV123WWContagemResultadoExecucaoDS_33_Contagemresultadoexecucao_oscod3 = AV87ContagemResultadoExecucao_OSCod3;
         AV124WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm = AV49TFContagemResultado_OsFsOsFm;
         AV125WWContagemResultadoExecucaoDS_35_Tfcontagemresultado_osfsosfm_sel = AV50TFContagemResultado_OsFsOsFm_Sel;
         AV126WWContagemResultadoExecucaoDS_36_Tfcontagemresultadoexecucao_inicio = AV53TFContagemResultadoExecucao_Inicio;
         AV127WWContagemResultadoExecucaoDS_37_Tfcontagemresultadoexecucao_inicio_to = AV54TFContagemResultadoExecucao_Inicio_To;
         AV128WWContagemResultadoExecucaoDS_38_Tfcontagemresultadoexecucao_prevista = AV59TFContagemResultadoExecucao_Prevista;
         AV129WWContagemResultadoExecucaoDS_39_Tfcontagemresultadoexecucao_prevista_to = AV60TFContagemResultadoExecucao_Prevista_To;
         AV130WWContagemResultadoExecucaoDS_40_Tfcontagemresultadoexecucao_prazodias = AV65TFContagemResultadoExecucao_PrazoDias;
         AV131WWContagemResultadoExecucaoDS_41_Tfcontagemresultadoexecucao_prazodias_to = AV66TFContagemResultadoExecucao_PrazoDias_To;
         AV132WWContagemResultadoExecucaoDS_42_Tfcontagemresultadoexecucao_fim = AV69TFContagemResultadoExecucao_Fim;
         AV133WWContagemResultadoExecucaoDS_43_Tfcontagemresultadoexecucao_fim_to = AV70TFContagemResultadoExecucao_Fim_To;
         AV134WWContagemResultadoExecucaoDS_44_Tfcontagemresultadoexecucao_dias = AV75TFContagemResultadoExecucao_Dias;
         AV135WWContagemResultadoExecucaoDS_45_Tfcontagemresultadoexecucao_dias_to = AV76TFContagemResultadoExecucao_Dias_To;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV48ContagemResultado_OsFsOsFmTitleFilterData", AV48ContagemResultado_OsFsOsFmTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV52ContagemResultadoExecucao_InicioTitleFilterData", AV52ContagemResultadoExecucao_InicioTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV58ContagemResultadoExecucao_PrevistaTitleFilterData", AV58ContagemResultadoExecucao_PrevistaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV64ContagemResultadoExecucao_PrazoDiasTitleFilterData", AV64ContagemResultadoExecucao_PrazoDiasTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV68ContagemResultadoExecucao_FimTitleFilterData", AV68ContagemResultadoExecucao_FimTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV74ContagemResultadoExecucao_DiasTitleFilterData", AV74ContagemResultadoExecucao_DiasTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11KI2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV79PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV79PageToGo) ;
         }
      }

      protected void E12KI2( )
      {
         /* Ddo_contagemresultado_osfsosfm_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultado_osfsosfm_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV49TFContagemResultado_OsFsOsFm = Ddo_contagemresultado_osfsosfm_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContagemResultado_OsFsOsFm", AV49TFContagemResultado_OsFsOsFm);
            AV50TFContagemResultado_OsFsOsFm_Sel = Ddo_contagemresultado_osfsosfm_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContagemResultado_OsFsOsFm_Sel", AV50TFContagemResultado_OsFsOsFm_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E13KI2( )
      {
         /* Ddo_contagemresultadoexecucao_inicio_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultadoexecucao_inicio_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV53TFContagemResultadoExecucao_Inicio = context.localUtil.CToT( Ddo_contagemresultadoexecucao_inicio_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContagemResultadoExecucao_Inicio", context.localUtil.TToC( AV53TFContagemResultadoExecucao_Inicio, 8, 5, 0, 3, "/", ":", " "));
            AV54TFContagemResultadoExecucao_Inicio_To = context.localUtil.CToT( Ddo_contagemresultadoexecucao_inicio_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContagemResultadoExecucao_Inicio_To", context.localUtil.TToC( AV54TFContagemResultadoExecucao_Inicio_To, 8, 5, 0, 3, "/", ":", " "));
            if ( ! (DateTime.MinValue==AV54TFContagemResultadoExecucao_Inicio_To) )
            {
               AV54TFContagemResultadoExecucao_Inicio_To = context.localUtil.YMDHMSToT( (short)(DateTimeUtil.Year( AV54TFContagemResultadoExecucao_Inicio_To)), (short)(DateTimeUtil.Month( AV54TFContagemResultadoExecucao_Inicio_To)), (short)(DateTimeUtil.Day( AV54TFContagemResultadoExecucao_Inicio_To)), 23, 59, 59);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContagemResultadoExecucao_Inicio_To", context.localUtil.TToC( AV54TFContagemResultadoExecucao_Inicio_To, 8, 5, 0, 3, "/", ":", " "));
            }
            subgrid_firstpage( ) ;
         }
      }

      protected void E14KI2( )
      {
         /* Ddo_contagemresultadoexecucao_prevista_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultadoexecucao_prevista_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV59TFContagemResultadoExecucao_Prevista = context.localUtil.CToT( Ddo_contagemresultadoexecucao_prevista_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContagemResultadoExecucao_Prevista", context.localUtil.TToC( AV59TFContagemResultadoExecucao_Prevista, 8, 5, 0, 3, "/", ":", " "));
            AV60TFContagemResultadoExecucao_Prevista_To = context.localUtil.CToT( Ddo_contagemresultadoexecucao_prevista_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContagemResultadoExecucao_Prevista_To", context.localUtil.TToC( AV60TFContagemResultadoExecucao_Prevista_To, 8, 5, 0, 3, "/", ":", " "));
            if ( ! (DateTime.MinValue==AV60TFContagemResultadoExecucao_Prevista_To) )
            {
               AV60TFContagemResultadoExecucao_Prevista_To = context.localUtil.YMDHMSToT( (short)(DateTimeUtil.Year( AV60TFContagemResultadoExecucao_Prevista_To)), (short)(DateTimeUtil.Month( AV60TFContagemResultadoExecucao_Prevista_To)), (short)(DateTimeUtil.Day( AV60TFContagemResultadoExecucao_Prevista_To)), 23, 59, 59);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContagemResultadoExecucao_Prevista_To", context.localUtil.TToC( AV60TFContagemResultadoExecucao_Prevista_To, 8, 5, 0, 3, "/", ":", " "));
            }
            subgrid_firstpage( ) ;
         }
      }

      protected void E15KI2( )
      {
         /* Ddo_contagemresultadoexecucao_prazodias_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultadoexecucao_prazodias_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV65TFContagemResultadoExecucao_PrazoDias = (short)(NumberUtil.Val( Ddo_contagemresultadoexecucao_prazodias_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContagemResultadoExecucao_PrazoDias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65TFContagemResultadoExecucao_PrazoDias), 4, 0)));
            AV66TFContagemResultadoExecucao_PrazoDias_To = (short)(NumberUtil.Val( Ddo_contagemresultadoexecucao_prazodias_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContagemResultadoExecucao_PrazoDias_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66TFContagemResultadoExecucao_PrazoDias_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E16KI2( )
      {
         /* Ddo_contagemresultadoexecucao_fim_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultadoexecucao_fim_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV69TFContagemResultadoExecucao_Fim = context.localUtil.CToT( Ddo_contagemresultadoexecucao_fim_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFContagemResultadoExecucao_Fim", context.localUtil.TToC( AV69TFContagemResultadoExecucao_Fim, 8, 5, 0, 3, "/", ":", " "));
            AV70TFContagemResultadoExecucao_Fim_To = context.localUtil.CToT( Ddo_contagemresultadoexecucao_fim_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContagemResultadoExecucao_Fim_To", context.localUtil.TToC( AV70TFContagemResultadoExecucao_Fim_To, 8, 5, 0, 3, "/", ":", " "));
            if ( ! (DateTime.MinValue==AV70TFContagemResultadoExecucao_Fim_To) )
            {
               AV70TFContagemResultadoExecucao_Fim_To = context.localUtil.YMDHMSToT( (short)(DateTimeUtil.Year( AV70TFContagemResultadoExecucao_Fim_To)), (short)(DateTimeUtil.Month( AV70TFContagemResultadoExecucao_Fim_To)), (short)(DateTimeUtil.Day( AV70TFContagemResultadoExecucao_Fim_To)), 23, 59, 59);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContagemResultadoExecucao_Fim_To", context.localUtil.TToC( AV70TFContagemResultadoExecucao_Fim_To, 8, 5, 0, 3, "/", ":", " "));
            }
            subgrid_firstpage( ) ;
         }
      }

      protected void E17KI2( )
      {
         /* Ddo_contagemresultadoexecucao_dias_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultadoexecucao_dias_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV75TFContagemResultadoExecucao_Dias = (short)(NumberUtil.Val( Ddo_contagemresultadoexecucao_dias_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFContagemResultadoExecucao_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV75TFContagemResultadoExecucao_Dias), 4, 0)));
            AV76TFContagemResultadoExecucao_Dias_To = (short)(NumberUtil.Val( Ddo_contagemresultadoexecucao_dias_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFContagemResultadoExecucao_Dias_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV76TFContagemResultadoExecucao_Dias_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
      }

      private void E30KI2( )
      {
         /* Grid_Load Routine */
         AV40Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV40Update);
         AV136Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("contagemresultadoexecucao.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A1405ContagemResultadoExecucao_Codigo);
         AV41Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV41Delete);
         AV137Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("contagemresultadoexecucao.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A1405ContagemResultadoExecucao_Codigo);
         edtContagemResultadoExecucao_Inicio_Link = formatLink("viewcontagemresultadoexecucao.aspx") + "?" + UrlEncode("" +A1405ContagemResultadoExecucao_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 152;
         }
         sendrow_1522( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_152_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(152, GridRow);
         }
      }

      protected void E23KI2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV22DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled2", AV22DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E18KI2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV38DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38DynamicFiltersRemoving", AV38DynamicFiltersRemoving);
         AV39DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39DynamicFiltersIgnoreFirst", AV39DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV38DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38DynamicFiltersRemoving", AV38DynamicFiltersRemoving);
         AV39DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39DynamicFiltersIgnoreFirst", AV39DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV16ContagemResultadoExecucao_Inicio1, AV17ContagemResultadoExecucao_Inicio_To1, AV18ContagemResultadoExecucao_Fim1, AV19ContagemResultadoExecucao_Fim_To1, AV20ContagemResultadoExecucao_Prevista1, AV21ContagemResultadoExecucao_Prevista_To1, AV43ContagemResultado_OsFsOsFm1, AV82ContagemResultado_ContratadaCod1, AV83ContagemResultadoExecucao_OSCod1, AV23DynamicFiltersSelector2, AV24ContagemResultadoExecucao_Inicio2, AV25ContagemResultadoExecucao_Inicio_To2, AV26ContagemResultadoExecucao_Fim2, AV27ContagemResultadoExecucao_Fim_To2, AV28ContagemResultadoExecucao_Prevista2, AV29ContagemResultadoExecucao_Prevista_To2, AV44ContagemResultado_OsFsOsFm2, AV84ContagemResultado_ContratadaCod2, AV85ContagemResultadoExecucao_OSCod2, AV31DynamicFiltersSelector3, AV32ContagemResultadoExecucao_Inicio3, AV33ContagemResultadoExecucao_Inicio_To3, AV34ContagemResultadoExecucao_Fim3, AV35ContagemResultadoExecucao_Fim_To3, AV36ContagemResultadoExecucao_Prevista3, AV37ContagemResultadoExecucao_Prevista_To3, AV45ContagemResultado_OsFsOsFm3, AV86ContagemResultado_ContratadaCod3, AV87ContagemResultadoExecucao_OSCod3, AV22DynamicFiltersEnabled2, AV30DynamicFiltersEnabled3, AV49TFContagemResultado_OsFsOsFm, AV50TFContagemResultado_OsFsOsFm_Sel, AV53TFContagemResultadoExecucao_Inicio, AV54TFContagemResultadoExecucao_Inicio_To, AV59TFContagemResultadoExecucao_Prevista, AV60TFContagemResultadoExecucao_Prevista_To, AV65TFContagemResultadoExecucao_PrazoDias, AV66TFContagemResultadoExecucao_PrazoDias_To, AV69TFContagemResultadoExecucao_Fim, AV70TFContagemResultadoExecucao_Fim_To, AV75TFContagemResultadoExecucao_Dias, AV76TFContagemResultadoExecucao_Dias_To, AV6WWPContext, AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace, AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace, AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace, AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace, AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace, AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace, AV88Contratada_Codigo, AV138Pgmname, AV10GridState, AV39DynamicFiltersIgnoreFirst, AV38DynamicFiltersRemoving, A1405ContagemResultadoExecucao_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV31DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         dynavContagemresultado_contratadacod1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV82ContagemResultado_ContratadaCod1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contratadacod1_Internalname, "Values", dynavContagemresultado_contratadacod1.ToJavascriptSource());
         dynavContagemresultado_contratadacod2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV84ContagemResultado_ContratadaCod2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contratadacod2_Internalname, "Values", dynavContagemresultado_contratadacod2.ToJavascriptSource());
         dynavContagemresultado_contratadacod3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV86ContagemResultado_ContratadaCod3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contratadacod3_Internalname, "Values", dynavContagemresultado_contratadacod3.ToJavascriptSource());
      }

      protected void E24KI2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E25KI2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV30DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersEnabled3", AV30DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E19KI2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV38DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38DynamicFiltersRemoving", AV38DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled2", AV22DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV38DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38DynamicFiltersRemoving", AV38DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV16ContagemResultadoExecucao_Inicio1, AV17ContagemResultadoExecucao_Inicio_To1, AV18ContagemResultadoExecucao_Fim1, AV19ContagemResultadoExecucao_Fim_To1, AV20ContagemResultadoExecucao_Prevista1, AV21ContagemResultadoExecucao_Prevista_To1, AV43ContagemResultado_OsFsOsFm1, AV82ContagemResultado_ContratadaCod1, AV83ContagemResultadoExecucao_OSCod1, AV23DynamicFiltersSelector2, AV24ContagemResultadoExecucao_Inicio2, AV25ContagemResultadoExecucao_Inicio_To2, AV26ContagemResultadoExecucao_Fim2, AV27ContagemResultadoExecucao_Fim_To2, AV28ContagemResultadoExecucao_Prevista2, AV29ContagemResultadoExecucao_Prevista_To2, AV44ContagemResultado_OsFsOsFm2, AV84ContagemResultado_ContratadaCod2, AV85ContagemResultadoExecucao_OSCod2, AV31DynamicFiltersSelector3, AV32ContagemResultadoExecucao_Inicio3, AV33ContagemResultadoExecucao_Inicio_To3, AV34ContagemResultadoExecucao_Fim3, AV35ContagemResultadoExecucao_Fim_To3, AV36ContagemResultadoExecucao_Prevista3, AV37ContagemResultadoExecucao_Prevista_To3, AV45ContagemResultado_OsFsOsFm3, AV86ContagemResultado_ContratadaCod3, AV87ContagemResultadoExecucao_OSCod3, AV22DynamicFiltersEnabled2, AV30DynamicFiltersEnabled3, AV49TFContagemResultado_OsFsOsFm, AV50TFContagemResultado_OsFsOsFm_Sel, AV53TFContagemResultadoExecucao_Inicio, AV54TFContagemResultadoExecucao_Inicio_To, AV59TFContagemResultadoExecucao_Prevista, AV60TFContagemResultadoExecucao_Prevista_To, AV65TFContagemResultadoExecucao_PrazoDias, AV66TFContagemResultadoExecucao_PrazoDias_To, AV69TFContagemResultadoExecucao_Fim, AV70TFContagemResultadoExecucao_Fim_To, AV75TFContagemResultadoExecucao_Dias, AV76TFContagemResultadoExecucao_Dias_To, AV6WWPContext, AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace, AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace, AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace, AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace, AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace, AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace, AV88Contratada_Codigo, AV138Pgmname, AV10GridState, AV39DynamicFiltersIgnoreFirst, AV38DynamicFiltersRemoving, A1405ContagemResultadoExecucao_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV31DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         dynavContagemresultado_contratadacod1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV82ContagemResultado_ContratadaCod1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contratadacod1_Internalname, "Values", dynavContagemresultado_contratadacod1.ToJavascriptSource());
         dynavContagemresultado_contratadacod2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV84ContagemResultado_ContratadaCod2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contratadacod2_Internalname, "Values", dynavContagemresultado_contratadacod2.ToJavascriptSource());
         dynavContagemresultado_contratadacod3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV86ContagemResultado_ContratadaCod3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contratadacod3_Internalname, "Values", dynavContagemresultado_contratadacod3.ToJavascriptSource());
      }

      protected void E26KI2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E20KI2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV38DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38DynamicFiltersRemoving", AV38DynamicFiltersRemoving);
         AV30DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersEnabled3", AV30DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV38DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38DynamicFiltersRemoving", AV38DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV16ContagemResultadoExecucao_Inicio1, AV17ContagemResultadoExecucao_Inicio_To1, AV18ContagemResultadoExecucao_Fim1, AV19ContagemResultadoExecucao_Fim_To1, AV20ContagemResultadoExecucao_Prevista1, AV21ContagemResultadoExecucao_Prevista_To1, AV43ContagemResultado_OsFsOsFm1, AV82ContagemResultado_ContratadaCod1, AV83ContagemResultadoExecucao_OSCod1, AV23DynamicFiltersSelector2, AV24ContagemResultadoExecucao_Inicio2, AV25ContagemResultadoExecucao_Inicio_To2, AV26ContagemResultadoExecucao_Fim2, AV27ContagemResultadoExecucao_Fim_To2, AV28ContagemResultadoExecucao_Prevista2, AV29ContagemResultadoExecucao_Prevista_To2, AV44ContagemResultado_OsFsOsFm2, AV84ContagemResultado_ContratadaCod2, AV85ContagemResultadoExecucao_OSCod2, AV31DynamicFiltersSelector3, AV32ContagemResultadoExecucao_Inicio3, AV33ContagemResultadoExecucao_Inicio_To3, AV34ContagemResultadoExecucao_Fim3, AV35ContagemResultadoExecucao_Fim_To3, AV36ContagemResultadoExecucao_Prevista3, AV37ContagemResultadoExecucao_Prevista_To3, AV45ContagemResultado_OsFsOsFm3, AV86ContagemResultado_ContratadaCod3, AV87ContagemResultadoExecucao_OSCod3, AV22DynamicFiltersEnabled2, AV30DynamicFiltersEnabled3, AV49TFContagemResultado_OsFsOsFm, AV50TFContagemResultado_OsFsOsFm_Sel, AV53TFContagemResultadoExecucao_Inicio, AV54TFContagemResultadoExecucao_Inicio_To, AV59TFContagemResultadoExecucao_Prevista, AV60TFContagemResultadoExecucao_Prevista_To, AV65TFContagemResultadoExecucao_PrazoDias, AV66TFContagemResultadoExecucao_PrazoDias_To, AV69TFContagemResultadoExecucao_Fim, AV70TFContagemResultadoExecucao_Fim_To, AV75TFContagemResultadoExecucao_Dias, AV76TFContagemResultadoExecucao_Dias_To, AV6WWPContext, AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace, AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace, AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace, AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace, AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace, AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace, AV88Contratada_Codigo, AV138Pgmname, AV10GridState, AV39DynamicFiltersIgnoreFirst, AV38DynamicFiltersRemoving, A1405ContagemResultadoExecucao_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV31DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         dynavContagemresultado_contratadacod1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV82ContagemResultado_ContratadaCod1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contratadacod1_Internalname, "Values", dynavContagemresultado_contratadacod1.ToJavascriptSource());
         dynavContagemresultado_contratadacod2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV84ContagemResultado_ContratadaCod2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contratadacod2_Internalname, "Values", dynavContagemresultado_contratadacod2.ToJavascriptSource());
         dynavContagemresultado_contratadacod3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV86ContagemResultado_ContratadaCod3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contratadacod3_Internalname, "Values", dynavContagemresultado_contratadacod3.ToJavascriptSource());
      }

      protected void E27KI2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E21KI2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         dynavContratada_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV88Contratada_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_codigo_Internalname, "Values", dynavContratada_codigo.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV31DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         dynavContagemresultado_contratadacod1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV82ContagemResultado_ContratadaCod1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contratadacod1_Internalname, "Values", dynavContagemresultado_contratadacod1.ToJavascriptSource());
         dynavContagemresultado_contratadacod2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV84ContagemResultado_ContratadaCod2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contratadacod2_Internalname, "Values", dynavContagemresultado_contratadacod2.ToJavascriptSource());
         dynavContagemresultado_contratadacod3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV86ContagemResultado_ContratadaCod3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contratadacod3_Internalname, "Values", dynavContagemresultado_contratadacod3.ToJavascriptSource());
      }

      protected void E22KI2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("contagemresultadoexecucao.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio1_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontagemresultadoexecucao_fim1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultadoexecucao_fim1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultadoexecucao_fim1_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista1_Visible), 5, 0)));
         edtavContagemresultado_osfsosfm1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_osfsosfm1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_osfsosfm1_Visible), 5, 0)));
         dynavContagemresultado_contratadacod1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contratadacod1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_contratadacod1.Visible), 5, 0)));
         edtavContagemresultadoexecucao_oscod1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultadoexecucao_oscod1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadoexecucao_oscod1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 )
         {
            tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADOEXECUCAO_FIM") == 0 )
         {
            tblTablemergeddynamicfilterscontagemresultadoexecucao_fim1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultadoexecucao_fim1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultadoexecucao_fim1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADOEXECUCAO_PREVISTA") == 0 )
         {
            tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
         {
            edtavContagemresultado_osfsosfm1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_osfsosfm1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_osfsosfm1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
         {
            dynavContagemresultado_contratadacod1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contratadacod1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_contratadacod1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADOEXECUCAO_OSCOD") == 0 )
         {
            edtavContagemresultadoexecucao_oscod1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultadoexecucao_oscod1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadoexecucao_oscod1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio2_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontagemresultadoexecucao_fim2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultadoexecucao_fim2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultadoexecucao_fim2_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista2_Visible), 5, 0)));
         edtavContagemresultado_osfsosfm2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_osfsosfm2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_osfsosfm2_Visible), 5, 0)));
         dynavContagemresultado_contratadacod2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contratadacod2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_contratadacod2.Visible), 5, 0)));
         edtavContagemresultadoexecucao_oscod2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultadoexecucao_oscod2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadoexecucao_oscod2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 )
         {
            tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTAGEMRESULTADOEXECUCAO_FIM") == 0 )
         {
            tblTablemergeddynamicfilterscontagemresultadoexecucao_fim2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultadoexecucao_fim2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultadoexecucao_fim2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTAGEMRESULTADOEXECUCAO_PREVISTA") == 0 )
         {
            tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
         {
            edtavContagemresultado_osfsosfm2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_osfsosfm2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_osfsosfm2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
         {
            dynavContagemresultado_contratadacod2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contratadacod2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_contratadacod2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTAGEMRESULTADOEXECUCAO_OSCOD") == 0 )
         {
            edtavContagemresultadoexecucao_oscod2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultadoexecucao_oscod2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadoexecucao_oscod2_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio3_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontagemresultadoexecucao_fim3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultadoexecucao_fim3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultadoexecucao_fim3_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista3_Visible), 5, 0)));
         edtavContagemresultado_osfsosfm3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_osfsosfm3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_osfsosfm3_Visible), 5, 0)));
         dynavContagemresultado_contratadacod3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contratadacod3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_contratadacod3.Visible), 5, 0)));
         edtavContagemresultadoexecucao_oscod3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultadoexecucao_oscod3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadoexecucao_oscod3_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 )
         {
            tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADOEXECUCAO_FIM") == 0 )
         {
            tblTablemergeddynamicfilterscontagemresultadoexecucao_fim3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultadoexecucao_fim3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultadoexecucao_fim3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADOEXECUCAO_PREVISTA") == 0 )
         {
            tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
         {
            edtavContagemresultado_osfsosfm3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_osfsosfm3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_osfsosfm3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
         {
            dynavContagemresultado_contratadacod3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contratadacod3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_contratadacod3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADOEXECUCAO_OSCOD") == 0 )
         {
            edtavContagemresultadoexecucao_oscod3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultadoexecucao_oscod3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadoexecucao_oscod3_Visible), 5, 0)));
         }
      }

      protected void S182( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV22DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled2", AV22DynamicFiltersEnabled2);
         AV23DynamicFiltersSelector2 = "CONTAGEMRESULTADOEXECUCAO_INICIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector2", AV23DynamicFiltersSelector2);
         AV24ContagemResultadoExecucao_Inicio2 = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultadoExecucao_Inicio2", context.localUtil.TToC( AV24ContagemResultadoExecucao_Inicio2, 8, 5, 0, 3, "/", ":", " "));
         AV25ContagemResultadoExecucao_Inicio_To2 = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContagemResultadoExecucao_Inicio_To2", context.localUtil.TToC( AV25ContagemResultadoExecucao_Inicio_To2, 8, 5, 0, 3, "/", ":", " "));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV30DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersEnabled3", AV30DynamicFiltersEnabled3);
         AV31DynamicFiltersSelector3 = "CONTAGEMRESULTADOEXECUCAO_INICIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31DynamicFiltersSelector3", AV31DynamicFiltersSelector3);
         AV32ContagemResultadoExecucao_Inicio3 = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ContagemResultadoExecucao_Inicio3", context.localUtil.TToC( AV32ContagemResultadoExecucao_Inicio3, 8, 5, 0, 3, "/", ":", " "));
         AV33ContagemResultadoExecucao_Inicio_To3 = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContagemResultadoExecucao_Inicio_To3", context.localUtil.TToC( AV33ContagemResultadoExecucao_Inicio_To3, 8, 5, 0, 3, "/", ":", " "));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'CLEANFILTERS' Routine */
         AV88Contratada_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV88Contratada_Codigo), 6, 0)));
         AV49TFContagemResultado_OsFsOsFm = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContagemResultado_OsFsOsFm", AV49TFContagemResultado_OsFsOsFm);
         Ddo_contagemresultado_osfsosfm_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_osfsosfm_Internalname, "FilteredText_set", Ddo_contagemresultado_osfsosfm_Filteredtext_set);
         AV50TFContagemResultado_OsFsOsFm_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContagemResultado_OsFsOsFm_Sel", AV50TFContagemResultado_OsFsOsFm_Sel);
         Ddo_contagemresultado_osfsosfm_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_osfsosfm_Internalname, "SelectedValue_set", Ddo_contagemresultado_osfsosfm_Selectedvalue_set);
         AV53TFContagemResultadoExecucao_Inicio = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContagemResultadoExecucao_Inicio", context.localUtil.TToC( AV53TFContagemResultadoExecucao_Inicio, 8, 5, 0, 3, "/", ":", " "));
         Ddo_contagemresultadoexecucao_inicio_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_inicio_Internalname, "FilteredText_set", Ddo_contagemresultadoexecucao_inicio_Filteredtext_set);
         AV54TFContagemResultadoExecucao_Inicio_To = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContagemResultadoExecucao_Inicio_To", context.localUtil.TToC( AV54TFContagemResultadoExecucao_Inicio_To, 8, 5, 0, 3, "/", ":", " "));
         Ddo_contagemresultadoexecucao_inicio_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_inicio_Internalname, "FilteredTextTo_set", Ddo_contagemresultadoexecucao_inicio_Filteredtextto_set);
         AV59TFContagemResultadoExecucao_Prevista = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContagemResultadoExecucao_Prevista", context.localUtil.TToC( AV59TFContagemResultadoExecucao_Prevista, 8, 5, 0, 3, "/", ":", " "));
         Ddo_contagemresultadoexecucao_prevista_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_prevista_Internalname, "FilteredText_set", Ddo_contagemresultadoexecucao_prevista_Filteredtext_set);
         AV60TFContagemResultadoExecucao_Prevista_To = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContagemResultadoExecucao_Prevista_To", context.localUtil.TToC( AV60TFContagemResultadoExecucao_Prevista_To, 8, 5, 0, 3, "/", ":", " "));
         Ddo_contagemresultadoexecucao_prevista_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_prevista_Internalname, "FilteredTextTo_set", Ddo_contagemresultadoexecucao_prevista_Filteredtextto_set);
         AV65TFContagemResultadoExecucao_PrazoDias = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContagemResultadoExecucao_PrazoDias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65TFContagemResultadoExecucao_PrazoDias), 4, 0)));
         Ddo_contagemresultadoexecucao_prazodias_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_prazodias_Internalname, "FilteredText_set", Ddo_contagemresultadoexecucao_prazodias_Filteredtext_set);
         AV66TFContagemResultadoExecucao_PrazoDias_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContagemResultadoExecucao_PrazoDias_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66TFContagemResultadoExecucao_PrazoDias_To), 4, 0)));
         Ddo_contagemresultadoexecucao_prazodias_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_prazodias_Internalname, "FilteredTextTo_set", Ddo_contagemresultadoexecucao_prazodias_Filteredtextto_set);
         AV69TFContagemResultadoExecucao_Fim = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFContagemResultadoExecucao_Fim", context.localUtil.TToC( AV69TFContagemResultadoExecucao_Fim, 8, 5, 0, 3, "/", ":", " "));
         Ddo_contagemresultadoexecucao_fim_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_fim_Internalname, "FilteredText_set", Ddo_contagemresultadoexecucao_fim_Filteredtext_set);
         AV70TFContagemResultadoExecucao_Fim_To = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContagemResultadoExecucao_Fim_To", context.localUtil.TToC( AV70TFContagemResultadoExecucao_Fim_To, 8, 5, 0, 3, "/", ":", " "));
         Ddo_contagemresultadoexecucao_fim_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_fim_Internalname, "FilteredTextTo_set", Ddo_contagemresultadoexecucao_fim_Filteredtextto_set);
         AV75TFContagemResultadoExecucao_Dias = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFContagemResultadoExecucao_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV75TFContagemResultadoExecucao_Dias), 4, 0)));
         Ddo_contagemresultadoexecucao_dias_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_dias_Internalname, "FilteredText_set", Ddo_contagemresultadoexecucao_dias_Filteredtext_set);
         AV76TFContagemResultadoExecucao_Dias_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFContagemResultadoExecucao_Dias_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV76TFContagemResultadoExecucao_Dias_To), 4, 0)));
         Ddo_contagemresultadoexecucao_dias_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_dias_Internalname, "FilteredTextTo_set", Ddo_contagemresultadoexecucao_dias_Filteredtextto_set);
         AV15DynamicFiltersSelector1 = "CONTAGEMRESULTADOEXECUCAO_INICIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16ContagemResultadoExecucao_Inicio1 = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContagemResultadoExecucao_Inicio1", context.localUtil.TToC( AV16ContagemResultadoExecucao_Inicio1, 8, 5, 0, 3, "/", ":", " "));
         AV17ContagemResultadoExecucao_Inicio_To1 = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContagemResultadoExecucao_Inicio_To1", context.localUtil.TToC( AV17ContagemResultadoExecucao_Inicio_To1, 8, 5, 0, 3, "/", ":", " "));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV42Session.Get(AV138Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV138Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV42Session.Get(AV138Pgmname+"GridState"), "");
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S222( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV139GXV1 = 1;
         while ( AV139GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV139GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "CONTRATADA_CODIGO") == 0 )
            {
               AV88Contratada_Codigo = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV88Contratada_Codigo), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_OSFSOSFM") == 0 )
            {
               AV49TFContagemResultado_OsFsOsFm = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContagemResultado_OsFsOsFm", AV49TFContagemResultado_OsFsOsFm);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49TFContagemResultado_OsFsOsFm)) )
               {
                  Ddo_contagemresultado_osfsosfm_Filteredtext_set = AV49TFContagemResultado_OsFsOsFm;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_osfsosfm_Internalname, "FilteredText_set", Ddo_contagemresultado_osfsosfm_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_OSFSOSFM_SEL") == 0 )
            {
               AV50TFContagemResultado_OsFsOsFm_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContagemResultado_OsFsOsFm_Sel", AV50TFContagemResultado_OsFsOsFm_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50TFContagemResultado_OsFsOsFm_Sel)) )
               {
                  Ddo_contagemresultado_osfsosfm_Selectedvalue_set = AV50TFContagemResultado_OsFsOsFm_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_osfsosfm_Internalname, "SelectedValue_set", Ddo_contagemresultado_osfsosfm_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADOEXECUCAO_INICIO") == 0 )
            {
               AV53TFContagemResultadoExecucao_Inicio = context.localUtil.CToT( AV11GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContagemResultadoExecucao_Inicio", context.localUtil.TToC( AV53TFContagemResultadoExecucao_Inicio, 8, 5, 0, 3, "/", ":", " "));
               AV54TFContagemResultadoExecucao_Inicio_To = context.localUtil.CToT( AV11GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContagemResultadoExecucao_Inicio_To", context.localUtil.TToC( AV54TFContagemResultadoExecucao_Inicio_To, 8, 5, 0, 3, "/", ":", " "));
               if ( ! (DateTime.MinValue==AV53TFContagemResultadoExecucao_Inicio) )
               {
                  AV55DDO_ContagemResultadoExecucao_InicioAuxDate = DateTimeUtil.ResetTime(AV53TFContagemResultadoExecucao_Inicio);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55DDO_ContagemResultadoExecucao_InicioAuxDate", context.localUtil.Format(AV55DDO_ContagemResultadoExecucao_InicioAuxDate, "99/99/99"));
                  Ddo_contagemresultadoexecucao_inicio_Filteredtext_set = context.localUtil.DToC( AV55DDO_ContagemResultadoExecucao_InicioAuxDate, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_inicio_Internalname, "FilteredText_set", Ddo_contagemresultadoexecucao_inicio_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV54TFContagemResultadoExecucao_Inicio_To) )
               {
                  AV56DDO_ContagemResultadoExecucao_InicioAuxDateTo = DateTimeUtil.ResetTime(AV54TFContagemResultadoExecucao_Inicio_To);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56DDO_ContagemResultadoExecucao_InicioAuxDateTo", context.localUtil.Format(AV56DDO_ContagemResultadoExecucao_InicioAuxDateTo, "99/99/99"));
                  Ddo_contagemresultadoexecucao_inicio_Filteredtextto_set = context.localUtil.DToC( AV56DDO_ContagemResultadoExecucao_InicioAuxDateTo, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_inicio_Internalname, "FilteredTextTo_set", Ddo_contagemresultadoexecucao_inicio_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADOEXECUCAO_PREVISTA") == 0 )
            {
               AV59TFContagemResultadoExecucao_Prevista = context.localUtil.CToT( AV11GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContagemResultadoExecucao_Prevista", context.localUtil.TToC( AV59TFContagemResultadoExecucao_Prevista, 8, 5, 0, 3, "/", ":", " "));
               AV60TFContagemResultadoExecucao_Prevista_To = context.localUtil.CToT( AV11GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContagemResultadoExecucao_Prevista_To", context.localUtil.TToC( AV60TFContagemResultadoExecucao_Prevista_To, 8, 5, 0, 3, "/", ":", " "));
               if ( ! (DateTime.MinValue==AV59TFContagemResultadoExecucao_Prevista) )
               {
                  AV61DDO_ContagemResultadoExecucao_PrevistaAuxDate = DateTimeUtil.ResetTime(AV59TFContagemResultadoExecucao_Prevista);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61DDO_ContagemResultadoExecucao_PrevistaAuxDate", context.localUtil.Format(AV61DDO_ContagemResultadoExecucao_PrevistaAuxDate, "99/99/99"));
                  Ddo_contagemresultadoexecucao_prevista_Filteredtext_set = context.localUtil.DToC( AV61DDO_ContagemResultadoExecucao_PrevistaAuxDate, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_prevista_Internalname, "FilteredText_set", Ddo_contagemresultadoexecucao_prevista_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV60TFContagemResultadoExecucao_Prevista_To) )
               {
                  AV62DDO_ContagemResultadoExecucao_PrevistaAuxDateTo = DateTimeUtil.ResetTime(AV60TFContagemResultadoExecucao_Prevista_To);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62DDO_ContagemResultadoExecucao_PrevistaAuxDateTo", context.localUtil.Format(AV62DDO_ContagemResultadoExecucao_PrevistaAuxDateTo, "99/99/99"));
                  Ddo_contagemresultadoexecucao_prevista_Filteredtextto_set = context.localUtil.DToC( AV62DDO_ContagemResultadoExecucao_PrevistaAuxDateTo, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_prevista_Internalname, "FilteredTextTo_set", Ddo_contagemresultadoexecucao_prevista_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS") == 0 )
            {
               AV65TFContagemResultadoExecucao_PrazoDias = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContagemResultadoExecucao_PrazoDias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65TFContagemResultadoExecucao_PrazoDias), 4, 0)));
               AV66TFContagemResultadoExecucao_PrazoDias_To = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContagemResultadoExecucao_PrazoDias_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66TFContagemResultadoExecucao_PrazoDias_To), 4, 0)));
               if ( ! (0==AV65TFContagemResultadoExecucao_PrazoDias) )
               {
                  Ddo_contagemresultadoexecucao_prazodias_Filteredtext_set = StringUtil.Str( (decimal)(AV65TFContagemResultadoExecucao_PrazoDias), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_prazodias_Internalname, "FilteredText_set", Ddo_contagemresultadoexecucao_prazodias_Filteredtext_set);
               }
               if ( ! (0==AV66TFContagemResultadoExecucao_PrazoDias_To) )
               {
                  Ddo_contagemresultadoexecucao_prazodias_Filteredtextto_set = StringUtil.Str( (decimal)(AV66TFContagemResultadoExecucao_PrazoDias_To), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_prazodias_Internalname, "FilteredTextTo_set", Ddo_contagemresultadoexecucao_prazodias_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADOEXECUCAO_FIM") == 0 )
            {
               AV69TFContagemResultadoExecucao_Fim = context.localUtil.CToT( AV11GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFContagemResultadoExecucao_Fim", context.localUtil.TToC( AV69TFContagemResultadoExecucao_Fim, 8, 5, 0, 3, "/", ":", " "));
               AV70TFContagemResultadoExecucao_Fim_To = context.localUtil.CToT( AV11GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContagemResultadoExecucao_Fim_To", context.localUtil.TToC( AV70TFContagemResultadoExecucao_Fim_To, 8, 5, 0, 3, "/", ":", " "));
               if ( ! (DateTime.MinValue==AV69TFContagemResultadoExecucao_Fim) )
               {
                  AV71DDO_ContagemResultadoExecucao_FimAuxDate = DateTimeUtil.ResetTime(AV69TFContagemResultadoExecucao_Fim);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71DDO_ContagemResultadoExecucao_FimAuxDate", context.localUtil.Format(AV71DDO_ContagemResultadoExecucao_FimAuxDate, "99/99/99"));
                  Ddo_contagemresultadoexecucao_fim_Filteredtext_set = context.localUtil.DToC( AV71DDO_ContagemResultadoExecucao_FimAuxDate, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_fim_Internalname, "FilteredText_set", Ddo_contagemresultadoexecucao_fim_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV70TFContagemResultadoExecucao_Fim_To) )
               {
                  AV72DDO_ContagemResultadoExecucao_FimAuxDateTo = DateTimeUtil.ResetTime(AV70TFContagemResultadoExecucao_Fim_To);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72DDO_ContagemResultadoExecucao_FimAuxDateTo", context.localUtil.Format(AV72DDO_ContagemResultadoExecucao_FimAuxDateTo, "99/99/99"));
                  Ddo_contagemresultadoexecucao_fim_Filteredtextto_set = context.localUtil.DToC( AV72DDO_ContagemResultadoExecucao_FimAuxDateTo, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_fim_Internalname, "FilteredTextTo_set", Ddo_contagemresultadoexecucao_fim_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADOEXECUCAO_DIAS") == 0 )
            {
               AV75TFContagemResultadoExecucao_Dias = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFContagemResultadoExecucao_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV75TFContagemResultadoExecucao_Dias), 4, 0)));
               AV76TFContagemResultadoExecucao_Dias_To = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFContagemResultadoExecucao_Dias_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV76TFContagemResultadoExecucao_Dias_To), 4, 0)));
               if ( ! (0==AV75TFContagemResultadoExecucao_Dias) )
               {
                  Ddo_contagemresultadoexecucao_dias_Filteredtext_set = StringUtil.Str( (decimal)(AV75TFContagemResultadoExecucao_Dias), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_dias_Internalname, "FilteredText_set", Ddo_contagemresultadoexecucao_dias_Filteredtext_set);
               }
               if ( ! (0==AV76TFContagemResultadoExecucao_Dias_To) )
               {
                  Ddo_contagemresultadoexecucao_dias_Filteredtextto_set = StringUtil.Str( (decimal)(AV76TFContagemResultadoExecucao_Dias_To), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_dias_Internalname, "FilteredTextTo_set", Ddo_contagemresultadoexecucao_dias_Filteredtextto_set);
               }
            }
            AV139GXV1 = (int)(AV139GXV1+1);
         }
      }

      protected void S192( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 )
            {
               AV16ContagemResultadoExecucao_Inicio1 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContagemResultadoExecucao_Inicio1", context.localUtil.TToC( AV16ContagemResultadoExecucao_Inicio1, 8, 5, 0, 3, "/", ":", " "));
               AV17ContagemResultadoExecucao_Inicio_To1 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContagemResultadoExecucao_Inicio_To1", context.localUtil.TToC( AV17ContagemResultadoExecucao_Inicio_To1, 8, 5, 0, 3, "/", ":", " "));
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADOEXECUCAO_FIM") == 0 )
            {
               AV18ContagemResultadoExecucao_Fim1 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultadoExecucao_Fim1", context.localUtil.TToC( AV18ContagemResultadoExecucao_Fim1, 8, 5, 0, 3, "/", ":", " "));
               AV19ContagemResultadoExecucao_Fim_To1 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultadoExecucao_Fim_To1", context.localUtil.TToC( AV19ContagemResultadoExecucao_Fim_To1, 8, 5, 0, 3, "/", ":", " "));
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADOEXECUCAO_PREVISTA") == 0 )
            {
               AV20ContagemResultadoExecucao_Prevista1 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContagemResultadoExecucao_Prevista1", context.localUtil.TToC( AV20ContagemResultadoExecucao_Prevista1, 8, 5, 0, 3, "/", ":", " "));
               AV21ContagemResultadoExecucao_Prevista_To1 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContagemResultadoExecucao_Prevista_To1", context.localUtil.TToC( AV21ContagemResultadoExecucao_Prevista_To1, 8, 5, 0, 3, "/", ":", " "));
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
            {
               AV43ContagemResultado_OsFsOsFm1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ContagemResultado_OsFsOsFm1", AV43ContagemResultado_OsFsOsFm1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
            {
               AV82ContagemResultado_ContratadaCod1 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82ContagemResultado_ContratadaCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV82ContagemResultado_ContratadaCod1), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADOEXECUCAO_OSCOD") == 0 )
            {
               AV83ContagemResultadoExecucao_OSCod1 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83ContagemResultadoExecucao_OSCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV83ContagemResultadoExecucao_OSCod1), 6, 0)));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV22DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled2", AV22DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV23DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector2", AV23DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 )
               {
                  AV24ContagemResultadoExecucao_Inicio2 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultadoExecucao_Inicio2", context.localUtil.TToC( AV24ContagemResultadoExecucao_Inicio2, 8, 5, 0, 3, "/", ":", " "));
                  AV25ContagemResultadoExecucao_Inicio_To2 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContagemResultadoExecucao_Inicio_To2", context.localUtil.TToC( AV25ContagemResultadoExecucao_Inicio_To2, 8, 5, 0, 3, "/", ":", " "));
               }
               else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTAGEMRESULTADOEXECUCAO_FIM") == 0 )
               {
                  AV26ContagemResultadoExecucao_Fim2 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ContagemResultadoExecucao_Fim2", context.localUtil.TToC( AV26ContagemResultadoExecucao_Fim2, 8, 5, 0, 3, "/", ":", " "));
                  AV27ContagemResultadoExecucao_Fim_To2 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27ContagemResultadoExecucao_Fim_To2", context.localUtil.TToC( AV27ContagemResultadoExecucao_Fim_To2, 8, 5, 0, 3, "/", ":", " "));
               }
               else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTAGEMRESULTADOEXECUCAO_PREVISTA") == 0 )
               {
                  AV28ContagemResultadoExecucao_Prevista2 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ContagemResultadoExecucao_Prevista2", context.localUtil.TToC( AV28ContagemResultadoExecucao_Prevista2, 8, 5, 0, 3, "/", ":", " "));
                  AV29ContagemResultadoExecucao_Prevista_To2 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContagemResultadoExecucao_Prevista_To2", context.localUtil.TToC( AV29ContagemResultadoExecucao_Prevista_To2, 8, 5, 0, 3, "/", ":", " "));
               }
               else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
               {
                  AV44ContagemResultado_OsFsOsFm2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ContagemResultado_OsFsOsFm2", AV44ContagemResultado_OsFsOsFm2);
               }
               else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
               {
                  AV84ContagemResultado_ContratadaCod2 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84ContagemResultado_ContratadaCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV84ContagemResultado_ContratadaCod2), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTAGEMRESULTADOEXECUCAO_OSCOD") == 0 )
               {
                  AV85ContagemResultadoExecucao_OSCod2 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85ContagemResultadoExecucao_OSCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV85ContagemResultadoExecucao_OSCod2), 6, 0)));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV30DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersEnabled3", AV30DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV31DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31DynamicFiltersSelector3", AV31DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 )
                  {
                     AV32ContagemResultadoExecucao_Inicio3 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ContagemResultadoExecucao_Inicio3", context.localUtil.TToC( AV32ContagemResultadoExecucao_Inicio3, 8, 5, 0, 3, "/", ":", " "));
                     AV33ContagemResultadoExecucao_Inicio_To3 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContagemResultadoExecucao_Inicio_To3", context.localUtil.TToC( AV33ContagemResultadoExecucao_Inicio_To3, 8, 5, 0, 3, "/", ":", " "));
                  }
                  else if ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADOEXECUCAO_FIM") == 0 )
                  {
                     AV34ContagemResultadoExecucao_Fim3 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContagemResultadoExecucao_Fim3", context.localUtil.TToC( AV34ContagemResultadoExecucao_Fim3, 8, 5, 0, 3, "/", ":", " "));
                     AV35ContagemResultadoExecucao_Fim_To3 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35ContagemResultadoExecucao_Fim_To3", context.localUtil.TToC( AV35ContagemResultadoExecucao_Fim_To3, 8, 5, 0, 3, "/", ":", " "));
                  }
                  else if ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADOEXECUCAO_PREVISTA") == 0 )
                  {
                     AV36ContagemResultadoExecucao_Prevista3 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ContagemResultadoExecucao_Prevista3", context.localUtil.TToC( AV36ContagemResultadoExecucao_Prevista3, 8, 5, 0, 3, "/", ":", " "));
                     AV37ContagemResultadoExecucao_Prevista_To3 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ContagemResultadoExecucao_Prevista_To3", context.localUtil.TToC( AV37ContagemResultadoExecucao_Prevista_To3, 8, 5, 0, 3, "/", ":", " "));
                  }
                  else if ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
                  {
                     AV45ContagemResultado_OsFsOsFm3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ContagemResultado_OsFsOsFm3", AV45ContagemResultado_OsFsOsFm3);
                  }
                  else if ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
                  {
                     AV86ContagemResultado_ContratadaCod3 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86ContagemResultado_ContratadaCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV86ContagemResultado_ContratadaCod3), 6, 0)));
                  }
                  else if ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADOEXECUCAO_OSCOD") == 0 )
                  {
                     AV87ContagemResultadoExecucao_OSCod3 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87ContagemResultadoExecucao_OSCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV87ContagemResultadoExecucao_OSCod3), 6, 0)));
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV38DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV42Session.Get(AV138Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! (0==AV88Contratada_Codigo) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "CONTRATADA_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV88Contratada_Codigo), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49TFContagemResultado_OsFsOsFm)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_OSFSOSFM";
            AV11GridStateFilterValue.gxTpr_Value = AV49TFContagemResultado_OsFsOsFm;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50TFContagemResultado_OsFsOsFm_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_OSFSOSFM_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV50TFContagemResultado_OsFsOsFm_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV53TFContagemResultadoExecucao_Inicio) && (DateTime.MinValue==AV54TFContagemResultadoExecucao_Inicio_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADOEXECUCAO_INICIO";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.TToC( AV53TFContagemResultadoExecucao_Inicio, 8, 5, 0, 3, "/", ":", " ");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.TToC( AV54TFContagemResultadoExecucao_Inicio_To, 8, 5, 0, 3, "/", ":", " ");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV59TFContagemResultadoExecucao_Prevista) && (DateTime.MinValue==AV60TFContagemResultadoExecucao_Prevista_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADOEXECUCAO_PREVISTA";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.TToC( AV59TFContagemResultadoExecucao_Prevista, 8, 5, 0, 3, "/", ":", " ");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.TToC( AV60TFContagemResultadoExecucao_Prevista_To, 8, 5, 0, 3, "/", ":", " ");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV65TFContagemResultadoExecucao_PrazoDias) && (0==AV66TFContagemResultadoExecucao_PrazoDias_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV65TFContagemResultadoExecucao_PrazoDias), 4, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV66TFContagemResultadoExecucao_PrazoDias_To), 4, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV69TFContagemResultadoExecucao_Fim) && (DateTime.MinValue==AV70TFContagemResultadoExecucao_Fim_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADOEXECUCAO_FIM";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.TToC( AV69TFContagemResultadoExecucao_Fim, 8, 5, 0, 3, "/", ":", " ");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.TToC( AV70TFContagemResultadoExecucao_Fim_To, 8, 5, 0, 3, "/", ":", " ");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV75TFContagemResultadoExecucao_Dias) && (0==AV76TFContagemResultadoExecucao_Dias_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADOEXECUCAO_DIAS";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV75TFContagemResultadoExecucao_Dias), 4, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV76TFContagemResultadoExecucao_Dias_To), 4, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV138Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S172( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV39DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 ) && ! ( (DateTime.MinValue==AV16ContagemResultadoExecucao_Inicio1) && (DateTime.MinValue==AV17ContagemResultadoExecucao_Inicio_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.TToC( AV16ContagemResultadoExecucao_Inicio1, 8, 5, 0, 3, "/", ":", " ");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.TToC( AV17ContagemResultadoExecucao_Inicio_To1, 8, 5, 0, 3, "/", ":", " ");
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADOEXECUCAO_FIM") == 0 ) && ! ( (DateTime.MinValue==AV18ContagemResultadoExecucao_Fim1) && (DateTime.MinValue==AV19ContagemResultadoExecucao_Fim_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.TToC( AV18ContagemResultadoExecucao_Fim1, 8, 5, 0, 3, "/", ":", " ");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.TToC( AV19ContagemResultadoExecucao_Fim_To1, 8, 5, 0, 3, "/", ":", " ");
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADOEXECUCAO_PREVISTA") == 0 ) && ! ( (DateTime.MinValue==AV20ContagemResultadoExecucao_Prevista1) && (DateTime.MinValue==AV21ContagemResultadoExecucao_Prevista_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.TToC( AV20ContagemResultadoExecucao_Prevista1, 8, 5, 0, 3, "/", ":", " ");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.TToC( AV21ContagemResultadoExecucao_Prevista_To1, 8, 5, 0, 3, "/", ":", " ");
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV43ContagemResultado_OsFsOsFm1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV43ContagemResultado_OsFsOsFm1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ! (0==AV82ContagemResultado_ContratadaCod1) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV82ContagemResultado_ContratadaCod1), 6, 0);
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADOEXECUCAO_OSCOD") == 0 ) && ! (0==AV83ContagemResultadoExecucao_OSCod1) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV83ContagemResultadoExecucao_OSCod1), 6, 0);
            }
            if ( AV38DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 ) && ! ( (DateTime.MinValue==AV24ContagemResultadoExecucao_Inicio2) && (DateTime.MinValue==AV25ContagemResultadoExecucao_Inicio_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.TToC( AV24ContagemResultadoExecucao_Inicio2, 8, 5, 0, 3, "/", ":", " ");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.TToC( AV25ContagemResultadoExecucao_Inicio_To2, 8, 5, 0, 3, "/", ":", " ");
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTAGEMRESULTADOEXECUCAO_FIM") == 0 ) && ! ( (DateTime.MinValue==AV26ContagemResultadoExecucao_Fim2) && (DateTime.MinValue==AV27ContagemResultadoExecucao_Fim_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.TToC( AV26ContagemResultadoExecucao_Fim2, 8, 5, 0, 3, "/", ":", " ");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.TToC( AV27ContagemResultadoExecucao_Fim_To2, 8, 5, 0, 3, "/", ":", " ");
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTAGEMRESULTADOEXECUCAO_PREVISTA") == 0 ) && ! ( (DateTime.MinValue==AV28ContagemResultadoExecucao_Prevista2) && (DateTime.MinValue==AV29ContagemResultadoExecucao_Prevista_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.TToC( AV28ContagemResultadoExecucao_Prevista2, 8, 5, 0, 3, "/", ":", " ");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.TToC( AV29ContagemResultadoExecucao_Prevista_To2, 8, 5, 0, 3, "/", ":", " ");
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV44ContagemResultado_OsFsOsFm2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV44ContagemResultado_OsFsOsFm2;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ! (0==AV84ContagemResultado_ContratadaCod2) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV84ContagemResultado_ContratadaCod2), 6, 0);
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTAGEMRESULTADOEXECUCAO_OSCOD") == 0 ) && ! (0==AV85ContagemResultadoExecucao_OSCod2) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV85ContagemResultadoExecucao_OSCod2), 6, 0);
            }
            if ( AV38DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV30DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV31DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 ) && ! ( (DateTime.MinValue==AV32ContagemResultadoExecucao_Inicio3) && (DateTime.MinValue==AV33ContagemResultadoExecucao_Inicio_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.TToC( AV32ContagemResultadoExecucao_Inicio3, 8, 5, 0, 3, "/", ":", " ");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.TToC( AV33ContagemResultadoExecucao_Inicio_To3, 8, 5, 0, 3, "/", ":", " ");
            }
            else if ( ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADOEXECUCAO_FIM") == 0 ) && ! ( (DateTime.MinValue==AV34ContagemResultadoExecucao_Fim3) && (DateTime.MinValue==AV35ContagemResultadoExecucao_Fim_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.TToC( AV34ContagemResultadoExecucao_Fim3, 8, 5, 0, 3, "/", ":", " ");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.TToC( AV35ContagemResultadoExecucao_Fim_To3, 8, 5, 0, 3, "/", ":", " ");
            }
            else if ( ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADOEXECUCAO_PREVISTA") == 0 ) && ! ( (DateTime.MinValue==AV36ContagemResultadoExecucao_Prevista3) && (DateTime.MinValue==AV37ContagemResultadoExecucao_Prevista_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.TToC( AV36ContagemResultadoExecucao_Prevista3, 8, 5, 0, 3, "/", ":", " ");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.TToC( AV37ContagemResultadoExecucao_Prevista_To3, 8, 5, 0, 3, "/", ":", " ");
            }
            else if ( ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV45ContagemResultado_OsFsOsFm3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV45ContagemResultado_OsFsOsFm3;
            }
            else if ( ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ! (0==AV86ContagemResultado_ContratadaCod3) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV86ContagemResultado_ContratadaCod3), 6, 0);
            }
            else if ( ( StringUtil.StrCmp(AV31DynamicFiltersSelector3, "CONTAGEMRESULTADOEXECUCAO_OSCOD") == 0 ) && ! (0==AV87ContagemResultadoExecucao_OSCod3) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV87ContagemResultadoExecucao_OSCod3), 6, 0);
            }
            if ( AV38DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV138Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ContagemResultadoExecucao";
         AV42Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      public void GXEnter( )
      {
         /* Execute user event: E31KI2 */
         E31KI2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E31KI2( )
      {
         /* Enter Routine */
         /* Execute user subroutine: 'DOATUALIZAR' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S212( )
      {
         /* 'DOATUALIZAR' Routine */
         gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV16ContagemResultadoExecucao_Inicio1, AV17ContagemResultadoExecucao_Inicio_To1, AV18ContagemResultadoExecucao_Fim1, AV19ContagemResultadoExecucao_Fim_To1, AV20ContagemResultadoExecucao_Prevista1, AV21ContagemResultadoExecucao_Prevista_To1, AV43ContagemResultado_OsFsOsFm1, AV82ContagemResultado_ContratadaCod1, AV83ContagemResultadoExecucao_OSCod1, AV23DynamicFiltersSelector2, AV24ContagemResultadoExecucao_Inicio2, AV25ContagemResultadoExecucao_Inicio_To2, AV26ContagemResultadoExecucao_Fim2, AV27ContagemResultadoExecucao_Fim_To2, AV28ContagemResultadoExecucao_Prevista2, AV29ContagemResultadoExecucao_Prevista_To2, AV44ContagemResultado_OsFsOsFm2, AV84ContagemResultado_ContratadaCod2, AV85ContagemResultadoExecucao_OSCod2, AV31DynamicFiltersSelector3, AV32ContagemResultadoExecucao_Inicio3, AV33ContagemResultadoExecucao_Inicio_To3, AV34ContagemResultadoExecucao_Fim3, AV35ContagemResultadoExecucao_Fim_To3, AV36ContagemResultadoExecucao_Prevista3, AV37ContagemResultadoExecucao_Prevista_To3, AV45ContagemResultado_OsFsOsFm3, AV86ContagemResultado_ContratadaCod3, AV87ContagemResultadoExecucao_OSCod3, AV22DynamicFiltersEnabled2, AV30DynamicFiltersEnabled3, AV49TFContagemResultado_OsFsOsFm, AV50TFContagemResultado_OsFsOsFm_Sel, AV53TFContagemResultadoExecucao_Inicio, AV54TFContagemResultadoExecucao_Inicio_To, AV59TFContagemResultadoExecucao_Prevista, AV60TFContagemResultadoExecucao_Prevista_To, AV65TFContagemResultadoExecucao_PrazoDias, AV66TFContagemResultadoExecucao_PrazoDias_To, AV69TFContagemResultadoExecucao_Fim, AV70TFContagemResultadoExecucao_Fim_To, AV75TFContagemResultadoExecucao_Dias, AV76TFContagemResultadoExecucao_Dias_To, AV6WWPContext, AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace, AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace, AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace, AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace, AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace, AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace, AV88Contratada_Codigo, AV138Pgmname, AV10GridState, AV39DynamicFiltersIgnoreFirst, AV38DynamicFiltersRemoving, A1405ContagemResultadoExecucao_Codigo) ;
      }

      protected void wb_table1_2_KI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_KI2( true) ;
         }
         else
         {
            wb_table2_8_KI2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_KI2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_146_KI2( true) ;
         }
         else
         {
            wb_table3_146_KI2( false) ;
         }
         return  ;
      }

      protected void wb_table3_146_KI2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_KI2e( true) ;
         }
         else
         {
            wb_table1_2_KI2e( false) ;
         }
      }

      protected void wb_table3_146_KI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_149_KI2( true) ;
         }
         else
         {
            wb_table4_149_KI2( false) ;
         }
         return  ;
      }

      protected void wb_table4_149_KI2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_146_KI2e( true) ;
         }
         else
         {
            wb_table3_146_KI2e( false) ;
         }
      }

      protected void wb_table4_149_KI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"152\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "OS / OS Ref.") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultado_OsFsOsFm_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultado_OsFsOsFm_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultado_OsFsOsFm_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoExecucao_Inicio_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoExecucao_Inicio_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoExecucao_Inicio_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoExecucao_Prevista_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoExecucao_Prevista_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoExecucao_Prevista_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoExecucao_PrazoDias_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoExecucao_PrazoDias_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoExecucao_PrazoDias_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoExecucao_Fim_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoExecucao_Fim_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoExecucao_Fim_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoExecucao_Dias_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoExecucao_Dias_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoExecucao_Dias_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV40Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV41Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1405ContagemResultadoExecucao_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1404ContagemResultadoExecucao_OSCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A501ContagemResultado_OsFsOsFm);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultado_OsFsOsFm_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_OsFsOsFm_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A1406ContagemResultadoExecucao_Inicio, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoExecucao_Inicio_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoExecucao_Inicio_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContagemResultadoExecucao_Inicio_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A1408ContagemResultadoExecucao_Prevista, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoExecucao_Prevista_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoExecucao_Prevista_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1411ContagemResultadoExecucao_PrazoDias), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoExecucao_PrazoDias_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoExecucao_PrazoDias_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A1407ContagemResultadoExecucao_Fim, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoExecucao_Fim_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoExecucao_Fim_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1410ContagemResultadoExecucao_Dias), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoExecucao_Dias_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoExecucao_Dias_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 152 )
         {
            wbEnd = 0;
            nRC_GXsfl_152 = (short)(nGXsfl_152_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_149_KI2e( true) ;
         }
         else
         {
            wb_table4_149_KI2e( false) ;
         }
      }

      protected void wb_table2_8_KI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemresultadoexecucaotitle_Internalname, "Ciclos de Execu��o", "", "", lblContagemresultadoexecucaotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_13_KI2( true) ;
         }
         else
         {
            wb_table5_13_KI2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_KI2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_18_KI2( true) ;
         }
         else
         {
            wb_table6_18_KI2( false) ;
         }
         return  ;
      }

      protected void wb_table6_18_KI2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_KI2e( true) ;
         }
         else
         {
            wb_table2_8_KI2e( false) ;
         }
      }

      protected void wb_table6_18_KI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextcontratada_codigo_Internalname, "Contratada", "", "", lblFiltertextcontratada_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'" + sGXsfl_152_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContratada_codigo, dynavContratada_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV88Contratada_Codigo), 6, 0)), 1, dynavContratada_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,25);\"", "", true, "HLP_WWContagemResultadoExecucao.htm");
            dynavContratada_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV88Contratada_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_codigo_Internalname, "Values", (String)(dynavContratada_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_27_KI2( true) ;
         }
         else
         {
            wb_table7_27_KI2( false) ;
         }
         return  ;
      }

      protected void wb_table7_27_KI2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 143,'',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnatualizar_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(152), 3, 0)+","+"null"+");", "Procurar", bttBtnatualizar_Jsonclick, 7, "Procurar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"e32ki1_client"+"'", TempTags, "", 2, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_18_KI2e( true) ;
         }
         else
         {
            wb_table6_18_KI2e( false) ;
         }
      }

      protected void wb_table7_27_KI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'" + sGXsfl_152_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,32);\"", "", true, "HLP_WWContagemResultadoExecucao.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_36_KI2( true) ;
         }
         else
         {
            wb_table8_36_KI2( false) ;
         }
         return  ;
      }

      protected void wb_table8_36_KI2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table9_44_KI2( true) ;
         }
         else
         {
            wb_table9_44_KI2( false) ;
         }
         return  ;
      }

      protected void wb_table9_44_KI2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table10_52_KI2( true) ;
         }
         else
         {
            wb_table10_52_KI2( false) ;
         }
         return  ;
      }

      protected void wb_table10_52_KI2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'" + sGXsfl_152_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_osfsosfm1_Internalname, AV43ContagemResultado_OsFsOsFm1, StringUtil.RTrim( context.localUtil.Format( AV43ContagemResultado_OsFsOsFm1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,60);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_osfsosfm1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_osfsosfm1_Visible, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContagemResultadoExecucao.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'" + sGXsfl_152_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_contratadacod1, dynavContagemresultado_contratadacod1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV82ContagemResultado_ContratadaCod1), 6, 0)), 1, dynavContagemresultado_contratadacod1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynavContagemresultado_contratadacod1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,61);\"", "", true, "HLP_WWContagemResultadoExecucao.htm");
            dynavContagemresultado_contratadacod1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV82ContagemResultado_ContratadaCod1), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contratadacod1_Internalname, "Values", (String)(dynavContagemresultado_contratadacod1.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'" + sGXsfl_152_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadoexecucao_oscod1_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV83ContagemResultadoExecucao_OSCod1), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV83ContagemResultadoExecucao_OSCod1), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,62);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadoexecucao_oscod1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultadoexecucao_oscod1_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContagemResultadoExecucao.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'" + sGXsfl_152_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,70);\"", "", true, "HLP_WWContagemResultadoExecucao.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table11_74_KI2( true) ;
         }
         else
         {
            wb_table11_74_KI2( false) ;
         }
         return  ;
      }

      protected void wb_table11_74_KI2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table12_82_KI2( true) ;
         }
         else
         {
            wb_table12_82_KI2( false) ;
         }
         return  ;
      }

      protected void wb_table12_82_KI2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table13_90_KI2( true) ;
         }
         else
         {
            wb_table13_90_KI2( false) ;
         }
         return  ;
      }

      protected void wb_table13_90_KI2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'" + sGXsfl_152_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_osfsosfm2_Internalname, AV44ContagemResultado_OsFsOsFm2, StringUtil.RTrim( context.localUtil.Format( AV44ContagemResultado_OsFsOsFm2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,98);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_osfsosfm2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_osfsosfm2_Visible, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContagemResultadoExecucao.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'" + sGXsfl_152_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_contratadacod2, dynavContagemresultado_contratadacod2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV84ContagemResultado_ContratadaCod2), 6, 0)), 1, dynavContagemresultado_contratadacod2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynavContagemresultado_contratadacod2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,99);\"", "", true, "HLP_WWContagemResultadoExecucao.htm");
            dynavContagemresultado_contratadacod2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV84ContagemResultado_ContratadaCod2), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contratadacod2_Internalname, "Values", (String)(dynavContagemresultado_contratadacod2.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_152_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadoexecucao_oscod2_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV85ContagemResultadoExecucao_OSCod2), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV85ContagemResultadoExecucao_OSCod2), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,100);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadoexecucao_oscod2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultadoexecucao_oscod2_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContagemResultadoExecucao.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_152_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV31DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,108);\"", "", true, "HLP_WWContagemResultadoExecucao.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV31DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table14_112_KI2( true) ;
         }
         else
         {
            wb_table14_112_KI2( false) ;
         }
         return  ;
      }

      protected void wb_table14_112_KI2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table15_120_KI2( true) ;
         }
         else
         {
            wb_table15_120_KI2( false) ;
         }
         return  ;
      }

      protected void wb_table15_120_KI2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table16_128_KI2( true) ;
         }
         else
         {
            wb_table16_128_KI2( false) ;
         }
         return  ;
      }

      protected void wb_table16_128_KI2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 136,'',false,'" + sGXsfl_152_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_osfsosfm3_Internalname, AV45ContagemResultado_OsFsOsFm3, StringUtil.RTrim( context.localUtil.Format( AV45ContagemResultado_OsFsOsFm3, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,136);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_osfsosfm3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_osfsosfm3_Visible, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContagemResultadoExecucao.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 137,'',false,'" + sGXsfl_152_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_contratadacod3, dynavContagemresultado_contratadacod3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV86ContagemResultado_ContratadaCod3), 6, 0)), 1, dynavContagemresultado_contratadacod3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynavContagemresultado_contratadacod3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,137);\"", "", true, "HLP_WWContagemResultadoExecucao.htm");
            dynavContagemresultado_contratadacod3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV86ContagemResultado_ContratadaCod3), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contratadacod3_Internalname, "Values", (String)(dynavContagemresultado_contratadacod3.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 138,'',false,'" + sGXsfl_152_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadoexecucao_oscod3_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV87ContagemResultadoExecucao_OSCod3), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV87ContagemResultadoExecucao_OSCod3), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,138);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadoexecucao_oscod3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultadoexecucao_oscod3_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 140,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_27_KI2e( true) ;
         }
         else
         {
            wb_table7_27_KI2e( false) ;
         }
      }

      protected void wb_table16_128_KI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista3_Internalname, tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 131,'',false,'" + sGXsfl_152_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultadoexecucao_prevista3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadoexecucao_prevista3_Internalname, context.localUtil.TToC( AV36ContagemResultadoExecucao_Prevista3, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV36ContagemResultadoExecucao_Prevista3, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,131);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadoexecucao_prevista3_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultadoexecucao_prevista3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontagemresultadoexecucao_prevista_rangemiddletext3_Internalname, "at�", "", "", lblDynamicfilterscontagemresultadoexecucao_prevista_rangemiddletext3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 135,'',false,'" + sGXsfl_152_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultadoexecucao_prevista_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadoexecucao_prevista_to3_Internalname, context.localUtil.TToC( AV37ContagemResultadoExecucao_Prevista_To3, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV37ContagemResultadoExecucao_Prevista_To3, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,135);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadoexecucao_prevista_to3_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultadoexecucao_prevista_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table16_128_KI2e( true) ;
         }
         else
         {
            wb_table16_128_KI2e( false) ;
         }
      }

      protected void wb_table15_120_KI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagemresultadoexecucao_fim3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagemresultadoexecucao_fim3_Internalname, tblTablemergeddynamicfilterscontagemresultadoexecucao_fim3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 123,'',false,'" + sGXsfl_152_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultadoexecucao_fim3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadoexecucao_fim3_Internalname, context.localUtil.TToC( AV34ContagemResultadoExecucao_Fim3, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV34ContagemResultadoExecucao_Fim3, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,123);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadoexecucao_fim3_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultadoexecucao_fim3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontagemresultadoexecucao_fim_rangemiddletext3_Internalname, "at�", "", "", lblDynamicfilterscontagemresultadoexecucao_fim_rangemiddletext3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 127,'',false,'" + sGXsfl_152_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultadoexecucao_fim_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadoexecucao_fim_to3_Internalname, context.localUtil.TToC( AV35ContagemResultadoExecucao_Fim_To3, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV35ContagemResultadoExecucao_Fim_To3, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,127);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadoexecucao_fim_to3_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultadoexecucao_fim_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table15_120_KI2e( true) ;
         }
         else
         {
            wb_table15_120_KI2e( false) ;
         }
      }

      protected void wb_table14_112_KI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio3_Internalname, tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_152_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultadoexecucao_inicio3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadoexecucao_inicio3_Internalname, context.localUtil.TToC( AV32ContagemResultadoExecucao_Inicio3, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV32ContagemResultadoExecucao_Inicio3, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,115);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadoexecucao_inicio3_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultadoexecucao_inicio3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontagemresultadoexecucao_inicio_rangemiddletext3_Internalname, "at�", "", "", lblDynamicfilterscontagemresultadoexecucao_inicio_rangemiddletext3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'" + sGXsfl_152_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultadoexecucao_inicio_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadoexecucao_inicio_to3_Internalname, context.localUtil.TToC( AV33ContagemResultadoExecucao_Inicio_To3, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV33ContagemResultadoExecucao_Inicio_To3, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,119);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadoexecucao_inicio_to3_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultadoexecucao_inicio_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table14_112_KI2e( true) ;
         }
         else
         {
            wb_table14_112_KI2e( false) ;
         }
      }

      protected void wb_table13_90_KI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista2_Internalname, tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 93,'',false,'" + sGXsfl_152_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultadoexecucao_prevista2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadoexecucao_prevista2_Internalname, context.localUtil.TToC( AV28ContagemResultadoExecucao_Prevista2, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV28ContagemResultadoExecucao_Prevista2, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,93);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadoexecucao_prevista2_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultadoexecucao_prevista2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontagemresultadoexecucao_prevista_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfilterscontagemresultadoexecucao_prevista_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'" + sGXsfl_152_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultadoexecucao_prevista_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadoexecucao_prevista_to2_Internalname, context.localUtil.TToC( AV29ContagemResultadoExecucao_Prevista_To2, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV29ContagemResultadoExecucao_Prevista_To2, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,97);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadoexecucao_prevista_to2_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultadoexecucao_prevista_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table13_90_KI2e( true) ;
         }
         else
         {
            wb_table13_90_KI2e( false) ;
         }
      }

      protected void wb_table12_82_KI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagemresultadoexecucao_fim2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagemresultadoexecucao_fim2_Internalname, tblTablemergeddynamicfilterscontagemresultadoexecucao_fim2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 85,'',false,'" + sGXsfl_152_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultadoexecucao_fim2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadoexecucao_fim2_Internalname, context.localUtil.TToC( AV26ContagemResultadoExecucao_Fim2, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV26ContagemResultadoExecucao_Fim2, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,85);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadoexecucao_fim2_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultadoexecucao_fim2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontagemresultadoexecucao_fim_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfilterscontagemresultadoexecucao_fim_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'" + sGXsfl_152_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultadoexecucao_fim_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadoexecucao_fim_to2_Internalname, context.localUtil.TToC( AV27ContagemResultadoExecucao_Fim_To2, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV27ContagemResultadoExecucao_Fim_To2, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,89);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadoexecucao_fim_to2_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultadoexecucao_fim_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table12_82_KI2e( true) ;
         }
         else
         {
            wb_table12_82_KI2e( false) ;
         }
      }

      protected void wb_table11_74_KI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio2_Internalname, tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'" + sGXsfl_152_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultadoexecucao_inicio2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadoexecucao_inicio2_Internalname, context.localUtil.TToC( AV24ContagemResultadoExecucao_Inicio2, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV24ContagemResultadoExecucao_Inicio2, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,77);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadoexecucao_inicio2_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultadoexecucao_inicio2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontagemresultadoexecucao_inicio_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfilterscontagemresultadoexecucao_inicio_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'" + sGXsfl_152_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultadoexecucao_inicio_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadoexecucao_inicio_to2_Internalname, context.localUtil.TToC( AV25ContagemResultadoExecucao_Inicio_To2, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV25ContagemResultadoExecucao_Inicio_To2, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,81);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadoexecucao_inicio_to2_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultadoexecucao_inicio_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table11_74_KI2e( true) ;
         }
         else
         {
            wb_table11_74_KI2e( false) ;
         }
      }

      protected void wb_table10_52_KI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista1_Internalname, tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'" + sGXsfl_152_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultadoexecucao_prevista1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadoexecucao_prevista1_Internalname, context.localUtil.TToC( AV20ContagemResultadoExecucao_Prevista1, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV20ContagemResultadoExecucao_Prevista1, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,55);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadoexecucao_prevista1_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultadoexecucao_prevista1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontagemresultadoexecucao_prevista_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfilterscontagemresultadoexecucao_prevista_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_152_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultadoexecucao_prevista_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadoexecucao_prevista_to1_Internalname, context.localUtil.TToC( AV21ContagemResultadoExecucao_Prevista_To1, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV21ContagemResultadoExecucao_Prevista_To1, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadoexecucao_prevista_to1_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultadoexecucao_prevista_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_52_KI2e( true) ;
         }
         else
         {
            wb_table10_52_KI2e( false) ;
         }
      }

      protected void wb_table9_44_KI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagemresultadoexecucao_fim1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagemresultadoexecucao_fim1_Internalname, tblTablemergeddynamicfilterscontagemresultadoexecucao_fim1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'" + sGXsfl_152_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultadoexecucao_fim1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadoexecucao_fim1_Internalname, context.localUtil.TToC( AV18ContagemResultadoExecucao_Fim1, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV18ContagemResultadoExecucao_Fim1, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,47);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadoexecucao_fim1_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultadoexecucao_fim1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontagemresultadoexecucao_fim_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfilterscontagemresultadoexecucao_fim_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'" + sGXsfl_152_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultadoexecucao_fim_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadoexecucao_fim_to1_Internalname, context.localUtil.TToC( AV19ContagemResultadoExecucao_Fim_To1, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV19ContagemResultadoExecucao_Fim_To1, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,51);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadoexecucao_fim_to1_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultadoexecucao_fim_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_44_KI2e( true) ;
         }
         else
         {
            wb_table9_44_KI2e( false) ;
         }
      }

      protected void wb_table8_36_KI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio1_Internalname, tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'" + sGXsfl_152_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultadoexecucao_inicio1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadoexecucao_inicio1_Internalname, context.localUtil.TToC( AV16ContagemResultadoExecucao_Inicio1, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV16ContagemResultadoExecucao_Inicio1, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadoexecucao_inicio1_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultadoexecucao_inicio1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontagemresultadoexecucao_inicio_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfilterscontagemresultadoexecucao_inicio_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'" + sGXsfl_152_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultadoexecucao_inicio_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadoexecucao_inicio_to1_Internalname, context.localUtil.TToC( AV17ContagemResultadoExecucao_Inicio_To1, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV17ContagemResultadoExecucao_Inicio_To1, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,43);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadoexecucao_inicio_to1_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultadoexecucao_inicio_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_36_KI2e( true) ;
         }
         else
         {
            wb_table8_36_KI2e( false) ;
         }
      }

      protected void wb_table5_13_KI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_KI2e( true) ;
         }
         else
         {
            wb_table5_13_KI2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAKI2( ) ;
         WSKI2( ) ;
         WEKI2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20206216243657");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwcontagemresultadoexecucao.js", "?20206216243657");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_1522( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_152_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_152_idx;
         edtContagemResultadoExecucao_Codigo_Internalname = "CONTAGEMRESULTADOEXECUCAO_CODIGO_"+sGXsfl_152_idx;
         edtContagemResultadoExecucao_OSCod_Internalname = "CONTAGEMRESULTADOEXECUCAO_OSCOD_"+sGXsfl_152_idx;
         edtContagemResultado_OsFsOsFm_Internalname = "CONTAGEMRESULTADO_OSFSOSFM_"+sGXsfl_152_idx;
         edtContagemResultadoExecucao_Inicio_Internalname = "CONTAGEMRESULTADOEXECUCAO_INICIO_"+sGXsfl_152_idx;
         edtContagemResultadoExecucao_Prevista_Internalname = "CONTAGEMRESULTADOEXECUCAO_PREVISTA_"+sGXsfl_152_idx;
         edtContagemResultadoExecucao_PrazoDias_Internalname = "CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_"+sGXsfl_152_idx;
         edtContagemResultadoExecucao_Fim_Internalname = "CONTAGEMRESULTADOEXECUCAO_FIM_"+sGXsfl_152_idx;
         edtContagemResultadoExecucao_Dias_Internalname = "CONTAGEMRESULTADOEXECUCAO_DIAS_"+sGXsfl_152_idx;
      }

      protected void SubsflControlProps_fel_1522( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_152_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_152_fel_idx;
         edtContagemResultadoExecucao_Codigo_Internalname = "CONTAGEMRESULTADOEXECUCAO_CODIGO_"+sGXsfl_152_fel_idx;
         edtContagemResultadoExecucao_OSCod_Internalname = "CONTAGEMRESULTADOEXECUCAO_OSCOD_"+sGXsfl_152_fel_idx;
         edtContagemResultado_OsFsOsFm_Internalname = "CONTAGEMRESULTADO_OSFSOSFM_"+sGXsfl_152_fel_idx;
         edtContagemResultadoExecucao_Inicio_Internalname = "CONTAGEMRESULTADOEXECUCAO_INICIO_"+sGXsfl_152_fel_idx;
         edtContagemResultadoExecucao_Prevista_Internalname = "CONTAGEMRESULTADOEXECUCAO_PREVISTA_"+sGXsfl_152_fel_idx;
         edtContagemResultadoExecucao_PrazoDias_Internalname = "CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_"+sGXsfl_152_fel_idx;
         edtContagemResultadoExecucao_Fim_Internalname = "CONTAGEMRESULTADOEXECUCAO_FIM_"+sGXsfl_152_fel_idx;
         edtContagemResultadoExecucao_Dias_Internalname = "CONTAGEMRESULTADOEXECUCAO_DIAS_"+sGXsfl_152_fel_idx;
      }

      protected void sendrow_1522( )
      {
         SubsflControlProps_1522( ) ;
         WBKI0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_152_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_152_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_152_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV40Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV40Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV136Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV40Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV40Update)) ? AV136Update_GXI : context.PathToRelativeUrl( AV40Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV40Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV41Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV41Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV137Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV41Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV41Delete)) ? AV137Delete_GXI : context.PathToRelativeUrl( AV41Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV41Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoExecucao_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1405ContagemResultadoExecucao_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1405ContagemResultadoExecucao_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoExecucao_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)152,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoExecucao_OSCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1404ContagemResultadoExecucao_OSCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1404ContagemResultadoExecucao_OSCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoExecucao_OSCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)152,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_OsFsOsFm_Internalname,(String)A501ContagemResultado_OsFsOsFm,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_OsFsOsFm_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)30,(short)0,(short)0,(short)152,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoExecucao_Inicio_Internalname,context.localUtil.TToC( A1406ContagemResultadoExecucao_Inicio, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A1406ContagemResultadoExecucao_Inicio, "99/99/99 99:99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtContagemResultadoExecucao_Inicio_Link,(String)"",(String)"",(String)"",(String)edtContagemResultadoExecucao_Inicio_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)152,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoExecucao_Prevista_Internalname,context.localUtil.TToC( A1408ContagemResultadoExecucao_Prevista, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A1408ContagemResultadoExecucao_Prevista, "99/99/99 99:99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoExecucao_Prevista_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)152,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoExecucao_PrazoDias_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1411ContagemResultadoExecucao_PrazoDias), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A1411ContagemResultadoExecucao_PrazoDias), "ZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoExecucao_PrazoDias_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)152,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoExecucao_Fim_Internalname,context.localUtil.TToC( A1407ContagemResultadoExecucao_Fim, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A1407ContagemResultadoExecucao_Fim, "99/99/99 99:99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoExecucao_Fim_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)152,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoExecucao_Dias_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1410ContagemResultadoExecucao_Dias), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A1410ContagemResultadoExecucao_Dias), "ZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoExecucao_Dias_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)152,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADOEXECUCAO_CODIGO"+"_"+sGXsfl_152_idx, GetSecureSignedToken( sGXsfl_152_idx, context.localUtil.Format( (decimal)(A1405ContagemResultadoExecucao_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADOEXECUCAO_OSCOD"+"_"+sGXsfl_152_idx, GetSecureSignedToken( sGXsfl_152_idx, context.localUtil.Format( (decimal)(A1404ContagemResultadoExecucao_OSCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_OSFSOSFM"+"_"+sGXsfl_152_idx, GetSecureSignedToken( sGXsfl_152_idx, StringUtil.RTrim( context.localUtil.Format( A501ContagemResultado_OsFsOsFm, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADOEXECUCAO_INICIO"+"_"+sGXsfl_152_idx, GetSecureSignedToken( sGXsfl_152_idx, context.localUtil.Format( A1406ContagemResultadoExecucao_Inicio, "99/99/99 99:99")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADOEXECUCAO_PREVISTA"+"_"+sGXsfl_152_idx, GetSecureSignedToken( sGXsfl_152_idx, context.localUtil.Format( A1408ContagemResultadoExecucao_Prevista, "99/99/99 99:99")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS"+"_"+sGXsfl_152_idx, GetSecureSignedToken( sGXsfl_152_idx, context.localUtil.Format( (decimal)(A1411ContagemResultadoExecucao_PrazoDias), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADOEXECUCAO_FIM"+"_"+sGXsfl_152_idx, GetSecureSignedToken( sGXsfl_152_idx, context.localUtil.Format( A1407ContagemResultadoExecucao_Fim, "99/99/99 99:99")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADOEXECUCAO_DIAS"+"_"+sGXsfl_152_idx, GetSecureSignedToken( sGXsfl_152_idx, context.localUtil.Format( (decimal)(A1410ContagemResultadoExecucao_Dias), "ZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_152_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_152_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_152_idx+1));
            sGXsfl_152_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_152_idx), 4, 0)), 4, "0");
            SubsflControlProps_1522( ) ;
         }
         /* End function sendrow_1522 */
      }

      protected void init_default_properties( )
      {
         lblContagemresultadoexecucaotitle_Internalname = "CONTAGEMRESULTADOEXECUCAOTITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblFiltertextcontratada_codigo_Internalname = "FILTERTEXTCONTRATADA_CODIGO";
         dynavContratada_codigo_Internalname = "vCONTRATADA_CODIGO";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavContagemresultadoexecucao_inicio1_Internalname = "vCONTAGEMRESULTADOEXECUCAO_INICIO1";
         lblDynamicfilterscontagemresultadoexecucao_inicio_rangemiddletext1_Internalname = "DYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO_RANGEMIDDLETEXT1";
         edtavContagemresultadoexecucao_inicio_to1_Internalname = "vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1";
         tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio1_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO1";
         edtavContagemresultadoexecucao_fim1_Internalname = "vCONTAGEMRESULTADOEXECUCAO_FIM1";
         lblDynamicfilterscontagemresultadoexecucao_fim_rangemiddletext1_Internalname = "DYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM_RANGEMIDDLETEXT1";
         edtavContagemresultadoexecucao_fim_to1_Internalname = "vCONTAGEMRESULTADOEXECUCAO_FIM_TO1";
         tblTablemergeddynamicfilterscontagemresultadoexecucao_fim1_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM1";
         edtavContagemresultadoexecucao_prevista1_Internalname = "vCONTAGEMRESULTADOEXECUCAO_PREVISTA1";
         lblDynamicfilterscontagemresultadoexecucao_prevista_rangemiddletext1_Internalname = "DYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA_RANGEMIDDLETEXT1";
         edtavContagemresultadoexecucao_prevista_to1_Internalname = "vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1";
         tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista1_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA1";
         edtavContagemresultado_osfsosfm1_Internalname = "vCONTAGEMRESULTADO_OSFSOSFM1";
         dynavContagemresultado_contratadacod1_Internalname = "vCONTAGEMRESULTADO_CONTRATADACOD1";
         edtavContagemresultadoexecucao_oscod1_Internalname = "vCONTAGEMRESULTADOEXECUCAO_OSCOD1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavContagemresultadoexecucao_inicio2_Internalname = "vCONTAGEMRESULTADOEXECUCAO_INICIO2";
         lblDynamicfilterscontagemresultadoexecucao_inicio_rangemiddletext2_Internalname = "DYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO_RANGEMIDDLETEXT2";
         edtavContagemresultadoexecucao_inicio_to2_Internalname = "vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2";
         tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio2_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO2";
         edtavContagemresultadoexecucao_fim2_Internalname = "vCONTAGEMRESULTADOEXECUCAO_FIM2";
         lblDynamicfilterscontagemresultadoexecucao_fim_rangemiddletext2_Internalname = "DYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM_RANGEMIDDLETEXT2";
         edtavContagemresultadoexecucao_fim_to2_Internalname = "vCONTAGEMRESULTADOEXECUCAO_FIM_TO2";
         tblTablemergeddynamicfilterscontagemresultadoexecucao_fim2_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM2";
         edtavContagemresultadoexecucao_prevista2_Internalname = "vCONTAGEMRESULTADOEXECUCAO_PREVISTA2";
         lblDynamicfilterscontagemresultadoexecucao_prevista_rangemiddletext2_Internalname = "DYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA_RANGEMIDDLETEXT2";
         edtavContagemresultadoexecucao_prevista_to2_Internalname = "vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2";
         tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista2_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA2";
         edtavContagemresultado_osfsosfm2_Internalname = "vCONTAGEMRESULTADO_OSFSOSFM2";
         dynavContagemresultado_contratadacod2_Internalname = "vCONTAGEMRESULTADO_CONTRATADACOD2";
         edtavContagemresultadoexecucao_oscod2_Internalname = "vCONTAGEMRESULTADOEXECUCAO_OSCOD2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavContagemresultadoexecucao_inicio3_Internalname = "vCONTAGEMRESULTADOEXECUCAO_INICIO3";
         lblDynamicfilterscontagemresultadoexecucao_inicio_rangemiddletext3_Internalname = "DYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO_RANGEMIDDLETEXT3";
         edtavContagemresultadoexecucao_inicio_to3_Internalname = "vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3";
         tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio3_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO3";
         edtavContagemresultadoexecucao_fim3_Internalname = "vCONTAGEMRESULTADOEXECUCAO_FIM3";
         lblDynamicfilterscontagemresultadoexecucao_fim_rangemiddletext3_Internalname = "DYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM_RANGEMIDDLETEXT3";
         edtavContagemresultadoexecucao_fim_to3_Internalname = "vCONTAGEMRESULTADOEXECUCAO_FIM_TO3";
         tblTablemergeddynamicfilterscontagemresultadoexecucao_fim3_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM3";
         edtavContagemresultadoexecucao_prevista3_Internalname = "vCONTAGEMRESULTADOEXECUCAO_PREVISTA3";
         lblDynamicfilterscontagemresultadoexecucao_prevista_rangemiddletext3_Internalname = "DYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA_RANGEMIDDLETEXT3";
         edtavContagemresultadoexecucao_prevista_to3_Internalname = "vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3";
         tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista3_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA3";
         edtavContagemresultado_osfsosfm3_Internalname = "vCONTAGEMRESULTADO_OSFSOSFM3";
         dynavContagemresultado_contratadacod3_Internalname = "vCONTAGEMRESULTADO_CONTRATADACOD3";
         edtavContagemresultadoexecucao_oscod3_Internalname = "vCONTAGEMRESULTADOEXECUCAO_OSCOD3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         bttBtnatualizar_Internalname = "BTNATUALIZAR";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtContagemResultadoExecucao_Codigo_Internalname = "CONTAGEMRESULTADOEXECUCAO_CODIGO";
         edtContagemResultadoExecucao_OSCod_Internalname = "CONTAGEMRESULTADOEXECUCAO_OSCOD";
         edtContagemResultado_OsFsOsFm_Internalname = "CONTAGEMRESULTADO_OSFSOSFM";
         edtContagemResultadoExecucao_Inicio_Internalname = "CONTAGEMRESULTADOEXECUCAO_INICIO";
         edtContagemResultadoExecucao_Prevista_Internalname = "CONTAGEMRESULTADOEXECUCAO_PREVISTA";
         edtContagemResultadoExecucao_PrazoDias_Internalname = "CONTAGEMRESULTADOEXECUCAO_PRAZODIAS";
         edtContagemResultadoExecucao_Fim_Internalname = "CONTAGEMRESULTADOEXECUCAO_FIM";
         edtContagemResultadoExecucao_Dias_Internalname = "CONTAGEMRESULTADOEXECUCAO_DIAS";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfcontagemresultado_osfsosfm_Internalname = "vTFCONTAGEMRESULTADO_OSFSOSFM";
         edtavTfcontagemresultado_osfsosfm_sel_Internalname = "vTFCONTAGEMRESULTADO_OSFSOSFM_SEL";
         edtavTfcontagemresultadoexecucao_inicio_Internalname = "vTFCONTAGEMRESULTADOEXECUCAO_INICIO";
         edtavTfcontagemresultadoexecucao_inicio_to_Internalname = "vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO";
         edtavDdo_contagemresultadoexecucao_inicioauxdate_Internalname = "vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOAUXDATE";
         edtavDdo_contagemresultadoexecucao_inicioauxdateto_Internalname = "vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOAUXDATETO";
         divDdo_contagemresultadoexecucao_inicioauxdates_Internalname = "DDO_CONTAGEMRESULTADOEXECUCAO_INICIOAUXDATES";
         edtavTfcontagemresultadoexecucao_prevista_Internalname = "vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA";
         edtavTfcontagemresultadoexecucao_prevista_to_Internalname = "vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO";
         edtavDdo_contagemresultadoexecucao_previstaauxdate_Internalname = "vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAAUXDATE";
         edtavDdo_contagemresultadoexecucao_previstaauxdateto_Internalname = "vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAAUXDATETO";
         divDdo_contagemresultadoexecucao_previstaauxdates_Internalname = "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAAUXDATES";
         edtavTfcontagemresultadoexecucao_prazodias_Internalname = "vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS";
         edtavTfcontagemresultadoexecucao_prazodias_to_Internalname = "vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO";
         edtavTfcontagemresultadoexecucao_fim_Internalname = "vTFCONTAGEMRESULTADOEXECUCAO_FIM";
         edtavTfcontagemresultadoexecucao_fim_to_Internalname = "vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO";
         edtavDdo_contagemresultadoexecucao_fimauxdate_Internalname = "vDDO_CONTAGEMRESULTADOEXECUCAO_FIMAUXDATE";
         edtavDdo_contagemresultadoexecucao_fimauxdateto_Internalname = "vDDO_CONTAGEMRESULTADOEXECUCAO_FIMAUXDATETO";
         divDdo_contagemresultadoexecucao_fimauxdates_Internalname = "DDO_CONTAGEMRESULTADOEXECUCAO_FIMAUXDATES";
         edtavTfcontagemresultadoexecucao_dias_Internalname = "vTFCONTAGEMRESULTADOEXECUCAO_DIAS";
         edtavTfcontagemresultadoexecucao_dias_to_Internalname = "vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO";
         Ddo_contagemresultado_osfsosfm_Internalname = "DDO_CONTAGEMRESULTADO_OSFSOSFM";
         edtavDdo_contagemresultado_osfsosfmtitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADO_OSFSOSFMTITLECONTROLIDTOREPLACE";
         Ddo_contagemresultadoexecucao_inicio_Internalname = "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO";
         edtavDdo_contagemresultadoexecucao_iniciotitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE";
         Ddo_contagemresultadoexecucao_prevista_Internalname = "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA";
         edtavDdo_contagemresultadoexecucao_previstatitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE";
         Ddo_contagemresultadoexecucao_prazodias_Internalname = "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS";
         edtavDdo_contagemresultadoexecucao_prazodiastitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE";
         Ddo_contagemresultadoexecucao_fim_Internalname = "DDO_CONTAGEMRESULTADOEXECUCAO_FIM";
         edtavDdo_contagemresultadoexecucao_fimtitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE";
         Ddo_contagemresultadoexecucao_dias_Internalname = "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS";
         edtavDdo_contagemresultadoexecucao_diastitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtContagemResultadoExecucao_Dias_Jsonclick = "";
         edtContagemResultadoExecucao_Fim_Jsonclick = "";
         edtContagemResultadoExecucao_PrazoDias_Jsonclick = "";
         edtContagemResultadoExecucao_Prevista_Jsonclick = "";
         edtContagemResultadoExecucao_Inicio_Jsonclick = "";
         edtContagemResultado_OsFsOsFm_Jsonclick = "";
         edtContagemResultadoExecucao_OSCod_Jsonclick = "";
         edtContagemResultadoExecucao_Codigo_Jsonclick = "";
         edtavContagemresultadoexecucao_inicio_to1_Jsonclick = "";
         edtavContagemresultadoexecucao_inicio1_Jsonclick = "";
         edtavContagemresultadoexecucao_fim_to1_Jsonclick = "";
         edtavContagemresultadoexecucao_fim1_Jsonclick = "";
         edtavContagemresultadoexecucao_prevista_to1_Jsonclick = "";
         edtavContagemresultadoexecucao_prevista1_Jsonclick = "";
         edtavContagemresultadoexecucao_inicio_to2_Jsonclick = "";
         edtavContagemresultadoexecucao_inicio2_Jsonclick = "";
         edtavContagemresultadoexecucao_fim_to2_Jsonclick = "";
         edtavContagemresultadoexecucao_fim2_Jsonclick = "";
         edtavContagemresultadoexecucao_prevista_to2_Jsonclick = "";
         edtavContagemresultadoexecucao_prevista2_Jsonclick = "";
         edtavContagemresultadoexecucao_inicio_to3_Jsonclick = "";
         edtavContagemresultadoexecucao_inicio3_Jsonclick = "";
         edtavContagemresultadoexecucao_fim_to3_Jsonclick = "";
         edtavContagemresultadoexecucao_fim3_Jsonclick = "";
         edtavContagemresultadoexecucao_prevista_to3_Jsonclick = "";
         edtavContagemresultadoexecucao_prevista3_Jsonclick = "";
         edtavContagemresultadoexecucao_oscod3_Jsonclick = "";
         dynavContagemresultado_contratadacod3_Jsonclick = "";
         edtavContagemresultado_osfsosfm3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         edtavContagemresultadoexecucao_oscod2_Jsonclick = "";
         dynavContagemresultado_contratadacod2_Jsonclick = "";
         edtavContagemresultado_osfsosfm2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         edtavContagemresultadoexecucao_oscod1_Jsonclick = "";
         dynavContagemresultado_contratadacod1_Jsonclick = "";
         edtavContagemresultado_osfsosfm1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         dynavContratada_codigo_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtContagemResultadoExecucao_Inicio_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtContagemResultadoExecucao_Dias_Titleformat = 0;
         edtContagemResultadoExecucao_Fim_Titleformat = 0;
         edtContagemResultadoExecucao_PrazoDias_Titleformat = 0;
         edtContagemResultadoExecucao_Prevista_Titleformat = 0;
         edtContagemResultadoExecucao_Inicio_Titleformat = 0;
         edtContagemResultado_OsFsOsFm_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavContagemresultadoexecucao_oscod3_Visible = 1;
         dynavContagemresultado_contratadacod3.Visible = 1;
         edtavContagemresultado_osfsosfm3_Visible = 1;
         tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista3_Visible = 1;
         tblTablemergeddynamicfilterscontagemresultadoexecucao_fim3_Visible = 1;
         tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio3_Visible = 1;
         edtavContagemresultadoexecucao_oscod2_Visible = 1;
         dynavContagemresultado_contratadacod2.Visible = 1;
         edtavContagemresultado_osfsosfm2_Visible = 1;
         tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista2_Visible = 1;
         tblTablemergeddynamicfilterscontagemresultadoexecucao_fim2_Visible = 1;
         tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio2_Visible = 1;
         edtavContagemresultadoexecucao_oscod1_Visible = 1;
         dynavContagemresultado_contratadacod1.Visible = 1;
         edtavContagemresultado_osfsosfm1_Visible = 1;
         tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista1_Visible = 1;
         tblTablemergeddynamicfilterscontagemresultadoexecucao_fim1_Visible = 1;
         tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio1_Visible = 1;
         edtContagemResultadoExecucao_Dias_Title = "Usado";
         edtContagemResultadoExecucao_Fim_Title = "Fim";
         edtContagemResultadoExecucao_PrazoDias_Title = "Dias";
         edtContagemResultadoExecucao_Prevista_Title = "Previsto";
         edtContagemResultadoExecucao_Inicio_Title = "Inicio";
         edtContagemResultado_OsFsOsFm_Title = "OS Ref|OS";
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_contagemresultadoexecucao_diastitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultadoexecucao_fimtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultadoexecucao_prazodiastitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultadoexecucao_previstatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultadoexecucao_iniciotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultado_osfsosfmtitlecontrolidtoreplace_Visible = 1;
         edtavTfcontagemresultadoexecucao_dias_to_Jsonclick = "";
         edtavTfcontagemresultadoexecucao_dias_to_Visible = 1;
         edtavTfcontagemresultadoexecucao_dias_Jsonclick = "";
         edtavTfcontagemresultadoexecucao_dias_Visible = 1;
         edtavDdo_contagemresultadoexecucao_fimauxdateto_Jsonclick = "";
         edtavDdo_contagemresultadoexecucao_fimauxdate_Jsonclick = "";
         edtavTfcontagemresultadoexecucao_fim_to_Jsonclick = "";
         edtavTfcontagemresultadoexecucao_fim_to_Visible = 1;
         edtavTfcontagemresultadoexecucao_fim_Jsonclick = "";
         edtavTfcontagemresultadoexecucao_fim_Visible = 1;
         edtavTfcontagemresultadoexecucao_prazodias_to_Jsonclick = "";
         edtavTfcontagemresultadoexecucao_prazodias_to_Visible = 1;
         edtavTfcontagemresultadoexecucao_prazodias_Jsonclick = "";
         edtavTfcontagemresultadoexecucao_prazodias_Visible = 1;
         edtavDdo_contagemresultadoexecucao_previstaauxdateto_Jsonclick = "";
         edtavDdo_contagemresultadoexecucao_previstaauxdate_Jsonclick = "";
         edtavTfcontagemresultadoexecucao_prevista_to_Jsonclick = "";
         edtavTfcontagemresultadoexecucao_prevista_to_Visible = 1;
         edtavTfcontagemresultadoexecucao_prevista_Jsonclick = "";
         edtavTfcontagemresultadoexecucao_prevista_Visible = 1;
         edtavDdo_contagemresultadoexecucao_inicioauxdateto_Jsonclick = "";
         edtavDdo_contagemresultadoexecucao_inicioauxdate_Jsonclick = "";
         edtavTfcontagemresultadoexecucao_inicio_to_Jsonclick = "";
         edtavTfcontagemresultadoexecucao_inicio_to_Visible = 1;
         edtavTfcontagemresultadoexecucao_inicio_Jsonclick = "";
         edtavTfcontagemresultadoexecucao_inicio_Visible = 1;
         edtavTfcontagemresultado_osfsosfm_sel_Jsonclick = "";
         edtavTfcontagemresultado_osfsosfm_sel_Visible = 1;
         edtavTfcontagemresultado_osfsosfm_Jsonclick = "";
         edtavTfcontagemresultado_osfsosfm_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_contagemresultadoexecucao_dias_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultadoexecucao_dias_Rangefilterto = "At�";
         Ddo_contagemresultadoexecucao_dias_Rangefilterfrom = "Desde";
         Ddo_contagemresultadoexecucao_dias_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultadoexecucao_dias_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagemresultadoexecucao_dias_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagemresultadoexecucao_dias_Filtertype = "Numeric";
         Ddo_contagemresultadoexecucao_dias_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultadoexecucao_dias_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_contagemresultadoexecucao_dias_Includesortasc = Convert.ToBoolean( 0);
         Ddo_contagemresultadoexecucao_dias_Titlecontrolidtoreplace = "";
         Ddo_contagemresultadoexecucao_dias_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultadoexecucao_dias_Cls = "ColumnSettings";
         Ddo_contagemresultadoexecucao_dias_Tooltip = "Op��es";
         Ddo_contagemresultadoexecucao_dias_Caption = "";
         Ddo_contagemresultadoexecucao_fim_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultadoexecucao_fim_Rangefilterto = "At�";
         Ddo_contagemresultadoexecucao_fim_Rangefilterfrom = "Desde";
         Ddo_contagemresultadoexecucao_fim_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultadoexecucao_fim_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagemresultadoexecucao_fim_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagemresultadoexecucao_fim_Filtertype = "Date";
         Ddo_contagemresultadoexecucao_fim_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultadoexecucao_fim_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_contagemresultadoexecucao_fim_Includesortasc = Convert.ToBoolean( 0);
         Ddo_contagemresultadoexecucao_fim_Titlecontrolidtoreplace = "";
         Ddo_contagemresultadoexecucao_fim_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultadoexecucao_fim_Cls = "ColumnSettings";
         Ddo_contagemresultadoexecucao_fim_Tooltip = "Op��es";
         Ddo_contagemresultadoexecucao_fim_Caption = "";
         Ddo_contagemresultadoexecucao_prazodias_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultadoexecucao_prazodias_Rangefilterto = "At�";
         Ddo_contagemresultadoexecucao_prazodias_Rangefilterfrom = "Desde";
         Ddo_contagemresultadoexecucao_prazodias_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultadoexecucao_prazodias_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagemresultadoexecucao_prazodias_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagemresultadoexecucao_prazodias_Filtertype = "Numeric";
         Ddo_contagemresultadoexecucao_prazodias_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultadoexecucao_prazodias_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_contagemresultadoexecucao_prazodias_Includesortasc = Convert.ToBoolean( 0);
         Ddo_contagemresultadoexecucao_prazodias_Titlecontrolidtoreplace = "";
         Ddo_contagemresultadoexecucao_prazodias_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultadoexecucao_prazodias_Cls = "ColumnSettings";
         Ddo_contagemresultadoexecucao_prazodias_Tooltip = "Op��es";
         Ddo_contagemresultadoexecucao_prazodias_Caption = "";
         Ddo_contagemresultadoexecucao_prevista_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultadoexecucao_prevista_Rangefilterto = "At�";
         Ddo_contagemresultadoexecucao_prevista_Rangefilterfrom = "Desde";
         Ddo_contagemresultadoexecucao_prevista_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultadoexecucao_prevista_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagemresultadoexecucao_prevista_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagemresultadoexecucao_prevista_Filtertype = "Date";
         Ddo_contagemresultadoexecucao_prevista_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultadoexecucao_prevista_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_contagemresultadoexecucao_prevista_Includesortasc = Convert.ToBoolean( 0);
         Ddo_contagemresultadoexecucao_prevista_Titlecontrolidtoreplace = "";
         Ddo_contagemresultadoexecucao_prevista_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultadoexecucao_prevista_Cls = "ColumnSettings";
         Ddo_contagemresultadoexecucao_prevista_Tooltip = "Op��es";
         Ddo_contagemresultadoexecucao_prevista_Caption = "";
         Ddo_contagemresultadoexecucao_inicio_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultadoexecucao_inicio_Rangefilterto = "At�";
         Ddo_contagemresultadoexecucao_inicio_Rangefilterfrom = "Desde";
         Ddo_contagemresultadoexecucao_inicio_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultadoexecucao_inicio_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagemresultadoexecucao_inicio_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagemresultadoexecucao_inicio_Filtertype = "Date";
         Ddo_contagemresultadoexecucao_inicio_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultadoexecucao_inicio_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_contagemresultadoexecucao_inicio_Includesortasc = Convert.ToBoolean( 0);
         Ddo_contagemresultadoexecucao_inicio_Titlecontrolidtoreplace = "";
         Ddo_contagemresultadoexecucao_inicio_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultadoexecucao_inicio_Cls = "ColumnSettings";
         Ddo_contagemresultadoexecucao_inicio_Tooltip = "Op��es";
         Ddo_contagemresultadoexecucao_inicio_Caption = "";
         Ddo_contagemresultado_osfsosfm_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultado_osfsosfm_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contagemresultado_osfsosfm_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultado_osfsosfm_Loadingdata = "Carregando dados...";
         Ddo_contagemresultado_osfsosfm_Datalistupdateminimumcharacters = 0;
         Ddo_contagemresultado_osfsosfm_Datalistproc = "GetWWContagemResultadoExecucaoFilterData";
         Ddo_contagemresultado_osfsosfm_Datalisttype = "Dynamic";
         Ddo_contagemresultado_osfsosfm_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contagemresultado_osfsosfm_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contagemresultado_osfsosfm_Filtertype = "Character";
         Ddo_contagemresultado_osfsosfm_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultado_osfsosfm_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_contagemresultado_osfsosfm_Includesortasc = Convert.ToBoolean( 0);
         Ddo_contagemresultado_osfsosfm_Titlecontrolidtoreplace = "";
         Ddo_contagemresultado_osfsosfm_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultado_osfsosfm_Cls = "ColumnSettings";
         Ddo_contagemresultado_osfsosfm_Tooltip = "Op��es";
         Ddo_contagemresultado_osfsosfm_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Contagem Resultado Execucao";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1405ContagemResultadoExecucao_Codigo',fld:'CONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_OSFSOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoExecucao_Fim1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoExecucao_Fim_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultadoExecucao_Prevista1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Prevista_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV43ContagemResultado_OsFsOsFm1',fld:'vCONTAGEMRESULTADO_OSFSOSFM1',pic:'',nv:''},{av:'AV82ContagemResultado_ContratadaCod1',fld:'vCONTAGEMRESULTADO_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV83ContagemResultadoExecucao_OSCod1',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoExecucao_Fim2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM2',pic:'99/99/99 99:99',nv:''},{av:'AV27ContagemResultadoExecucao_Fim_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV28ContagemResultadoExecucao_Prevista2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA2',pic:'99/99/99 99:99',nv:''},{av:'AV29ContagemResultadoExecucao_Prevista_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV44ContagemResultado_OsFsOsFm2',fld:'vCONTAGEMRESULTADO_OSFSOSFM2',pic:'',nv:''},{av:'AV84ContagemResultado_ContratadaCod2',fld:'vCONTAGEMRESULTADO_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV85ContagemResultadoExecucao_OSCod2',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',pic:'ZZZZZ9',nv:0},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultadoExecucao_Fim3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM3',pic:'99/99/99 99:99',nv:''},{av:'AV35ContagemResultadoExecucao_Fim_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV36ContagemResultadoExecucao_Prevista3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA3',pic:'99/99/99 99:99',nv:''},{av:'AV37ContagemResultadoExecucao_Prevista_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV45ContagemResultado_OsFsOsFm3',fld:'vCONTAGEMRESULTADO_OSFSOSFM3',pic:'',nv:''},{av:'AV86ContagemResultado_ContratadaCod3',fld:'vCONTAGEMRESULTADO_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV87ContagemResultadoExecucao_OSCod3',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',pic:'ZZZZZ9',nv:0},{av:'AV49TFContagemResultado_OsFsOsFm',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM',pic:'',nv:''},{av:'AV50TFContagemResultado_OsFsOsFm_Sel',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM_SEL',pic:'',nv:''},{av:'AV53TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV54TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV59TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV60TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV65TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV66TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'AV69TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV70TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'AV75TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV76TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'AV138Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV39DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV38DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV48ContagemResultado_OsFsOsFmTitleFilterData',fld:'vCONTAGEMRESULTADO_OSFSOSFMTITLEFILTERDATA',pic:'',nv:null},{av:'AV52ContagemResultadoExecucao_InicioTitleFilterData',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIOTITLEFILTERDATA',pic:'',nv:null},{av:'AV58ContagemResultadoExecucao_PrevistaTitleFilterData',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTATITLEFILTERDATA',pic:'',nv:null},{av:'AV64ContagemResultadoExecucao_PrazoDiasTitleFilterData',fld:'vCONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLEFILTERDATA',pic:'',nv:null},{av:'AV68ContagemResultadoExecucao_FimTitleFilterData',fld:'vCONTAGEMRESULTADOEXECUCAO_FIMTITLEFILTERDATA',pic:'',nv:null},{av:'AV74ContagemResultadoExecucao_DiasTitleFilterData',fld:'vCONTAGEMRESULTADOEXECUCAO_DIASTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtContagemResultado_OsFsOsFm_Titleformat',ctrl:'CONTAGEMRESULTADO_OSFSOSFM',prop:'Titleformat'},{av:'edtContagemResultado_OsFsOsFm_Title',ctrl:'CONTAGEMRESULTADO_OSFSOSFM',prop:'Title'},{av:'edtContagemResultadoExecucao_Inicio_Titleformat',ctrl:'CONTAGEMRESULTADOEXECUCAO_INICIO',prop:'Titleformat'},{av:'edtContagemResultadoExecucao_Inicio_Title',ctrl:'CONTAGEMRESULTADOEXECUCAO_INICIO',prop:'Title'},{av:'edtContagemResultadoExecucao_Prevista_Titleformat',ctrl:'CONTAGEMRESULTADOEXECUCAO_PREVISTA',prop:'Titleformat'},{av:'edtContagemResultadoExecucao_Prevista_Title',ctrl:'CONTAGEMRESULTADOEXECUCAO_PREVISTA',prop:'Title'},{av:'edtContagemResultadoExecucao_PrazoDias_Titleformat',ctrl:'CONTAGEMRESULTADOEXECUCAO_PRAZODIAS',prop:'Titleformat'},{av:'edtContagemResultadoExecucao_PrazoDias_Title',ctrl:'CONTAGEMRESULTADOEXECUCAO_PRAZODIAS',prop:'Title'},{av:'edtContagemResultadoExecucao_Fim_Titleformat',ctrl:'CONTAGEMRESULTADOEXECUCAO_FIM',prop:'Titleformat'},{av:'edtContagemResultadoExecucao_Fim_Title',ctrl:'CONTAGEMRESULTADOEXECUCAO_FIM',prop:'Title'},{av:'edtContagemResultadoExecucao_Dias_Titleformat',ctrl:'CONTAGEMRESULTADOEXECUCAO_DIAS',prop:'Titleformat'},{av:'edtContagemResultadoExecucao_Dias_Title',ctrl:'CONTAGEMRESULTADOEXECUCAO_DIAS',prop:'Title'},{av:'AV80GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV81GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11KI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoExecucao_Fim1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoExecucao_Fim_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultadoExecucao_Prevista1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Prevista_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV43ContagemResultado_OsFsOsFm1',fld:'vCONTAGEMRESULTADO_OSFSOSFM1',pic:'',nv:''},{av:'AV82ContagemResultado_ContratadaCod1',fld:'vCONTAGEMRESULTADO_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV83ContagemResultadoExecucao_OSCod1',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoExecucao_Fim2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM2',pic:'99/99/99 99:99',nv:''},{av:'AV27ContagemResultadoExecucao_Fim_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV28ContagemResultadoExecucao_Prevista2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA2',pic:'99/99/99 99:99',nv:''},{av:'AV29ContagemResultadoExecucao_Prevista_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV44ContagemResultado_OsFsOsFm2',fld:'vCONTAGEMRESULTADO_OSFSOSFM2',pic:'',nv:''},{av:'AV84ContagemResultado_ContratadaCod2',fld:'vCONTAGEMRESULTADO_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV85ContagemResultadoExecucao_OSCod2',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',pic:'ZZZZZ9',nv:0},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultadoExecucao_Fim3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM3',pic:'99/99/99 99:99',nv:''},{av:'AV35ContagemResultadoExecucao_Fim_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV36ContagemResultadoExecucao_Prevista3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA3',pic:'99/99/99 99:99',nv:''},{av:'AV37ContagemResultadoExecucao_Prevista_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV45ContagemResultado_OsFsOsFm3',fld:'vCONTAGEMRESULTADO_OSFSOSFM3',pic:'',nv:''},{av:'AV86ContagemResultado_ContratadaCod3',fld:'vCONTAGEMRESULTADO_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV87ContagemResultadoExecucao_OSCod3',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFContagemResultado_OsFsOsFm',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM',pic:'',nv:''},{av:'AV50TFContagemResultado_OsFsOsFm_Sel',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM_SEL',pic:'',nv:''},{av:'AV53TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV54TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV59TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV60TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV65TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV66TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'AV69TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV70TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'AV75TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV76TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_OSFSOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV138Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV39DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV38DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1405ContagemResultadoExecucao_Codigo',fld:'CONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTAGEMRESULTADO_OSFSOSFM.ONOPTIONCLICKED","{handler:'E12KI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoExecucao_Fim1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoExecucao_Fim_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultadoExecucao_Prevista1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Prevista_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV43ContagemResultado_OsFsOsFm1',fld:'vCONTAGEMRESULTADO_OSFSOSFM1',pic:'',nv:''},{av:'AV82ContagemResultado_ContratadaCod1',fld:'vCONTAGEMRESULTADO_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV83ContagemResultadoExecucao_OSCod1',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoExecucao_Fim2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM2',pic:'99/99/99 99:99',nv:''},{av:'AV27ContagemResultadoExecucao_Fim_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV28ContagemResultadoExecucao_Prevista2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA2',pic:'99/99/99 99:99',nv:''},{av:'AV29ContagemResultadoExecucao_Prevista_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV44ContagemResultado_OsFsOsFm2',fld:'vCONTAGEMRESULTADO_OSFSOSFM2',pic:'',nv:''},{av:'AV84ContagemResultado_ContratadaCod2',fld:'vCONTAGEMRESULTADO_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV85ContagemResultadoExecucao_OSCod2',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',pic:'ZZZZZ9',nv:0},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultadoExecucao_Fim3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM3',pic:'99/99/99 99:99',nv:''},{av:'AV35ContagemResultadoExecucao_Fim_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV36ContagemResultadoExecucao_Prevista3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA3',pic:'99/99/99 99:99',nv:''},{av:'AV37ContagemResultadoExecucao_Prevista_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV45ContagemResultado_OsFsOsFm3',fld:'vCONTAGEMRESULTADO_OSFSOSFM3',pic:'',nv:''},{av:'AV86ContagemResultado_ContratadaCod3',fld:'vCONTAGEMRESULTADO_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV87ContagemResultadoExecucao_OSCod3',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFContagemResultado_OsFsOsFm',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM',pic:'',nv:''},{av:'AV50TFContagemResultado_OsFsOsFm_Sel',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM_SEL',pic:'',nv:''},{av:'AV53TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV54TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV59TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV60TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV65TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV66TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'AV69TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV70TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'AV75TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV76TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_OSFSOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV138Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV39DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV38DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1405ContagemResultadoExecucao_Codigo',fld:'CONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contagemresultado_osfsosfm_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADO_OSFSOSFM',prop:'ActiveEventKey'},{av:'Ddo_contagemresultado_osfsosfm_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADO_OSFSOSFM',prop:'FilteredText_get'},{av:'Ddo_contagemresultado_osfsosfm_Selectedvalue_get',ctrl:'DDO_CONTAGEMRESULTADO_OSFSOSFM',prop:'SelectedValue_get'}],oparms:[{av:'AV49TFContagemResultado_OsFsOsFm',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM',pic:'',nv:''},{av:'AV50TFContagemResultado_OsFsOsFm_Sel',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM_SEL',pic:'',nv:''}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADOEXECUCAO_INICIO.ONOPTIONCLICKED","{handler:'E13KI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoExecucao_Fim1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoExecucao_Fim_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultadoExecucao_Prevista1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Prevista_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV43ContagemResultado_OsFsOsFm1',fld:'vCONTAGEMRESULTADO_OSFSOSFM1',pic:'',nv:''},{av:'AV82ContagemResultado_ContratadaCod1',fld:'vCONTAGEMRESULTADO_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV83ContagemResultadoExecucao_OSCod1',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoExecucao_Fim2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM2',pic:'99/99/99 99:99',nv:''},{av:'AV27ContagemResultadoExecucao_Fim_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV28ContagemResultadoExecucao_Prevista2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA2',pic:'99/99/99 99:99',nv:''},{av:'AV29ContagemResultadoExecucao_Prevista_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV44ContagemResultado_OsFsOsFm2',fld:'vCONTAGEMRESULTADO_OSFSOSFM2',pic:'',nv:''},{av:'AV84ContagemResultado_ContratadaCod2',fld:'vCONTAGEMRESULTADO_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV85ContagemResultadoExecucao_OSCod2',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',pic:'ZZZZZ9',nv:0},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultadoExecucao_Fim3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM3',pic:'99/99/99 99:99',nv:''},{av:'AV35ContagemResultadoExecucao_Fim_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV36ContagemResultadoExecucao_Prevista3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA3',pic:'99/99/99 99:99',nv:''},{av:'AV37ContagemResultadoExecucao_Prevista_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV45ContagemResultado_OsFsOsFm3',fld:'vCONTAGEMRESULTADO_OSFSOSFM3',pic:'',nv:''},{av:'AV86ContagemResultado_ContratadaCod3',fld:'vCONTAGEMRESULTADO_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV87ContagemResultadoExecucao_OSCod3',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFContagemResultado_OsFsOsFm',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM',pic:'',nv:''},{av:'AV50TFContagemResultado_OsFsOsFm_Sel',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM_SEL',pic:'',nv:''},{av:'AV53TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV54TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV59TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV60TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV65TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV66TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'AV69TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV70TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'AV75TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV76TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_OSFSOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV138Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV39DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV38DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1405ContagemResultadoExecucao_Codigo',fld:'CONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contagemresultadoexecucao_inicio_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_INICIO',prop:'ActiveEventKey'},{av:'Ddo_contagemresultadoexecucao_inicio_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_INICIO',prop:'FilteredText_get'},{av:'Ddo_contagemresultadoexecucao_inicio_Filteredtextto_get',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_INICIO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV53TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV54TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA.ONOPTIONCLICKED","{handler:'E14KI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoExecucao_Fim1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoExecucao_Fim_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultadoExecucao_Prevista1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Prevista_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV43ContagemResultado_OsFsOsFm1',fld:'vCONTAGEMRESULTADO_OSFSOSFM1',pic:'',nv:''},{av:'AV82ContagemResultado_ContratadaCod1',fld:'vCONTAGEMRESULTADO_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV83ContagemResultadoExecucao_OSCod1',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoExecucao_Fim2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM2',pic:'99/99/99 99:99',nv:''},{av:'AV27ContagemResultadoExecucao_Fim_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV28ContagemResultadoExecucao_Prevista2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA2',pic:'99/99/99 99:99',nv:''},{av:'AV29ContagemResultadoExecucao_Prevista_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV44ContagemResultado_OsFsOsFm2',fld:'vCONTAGEMRESULTADO_OSFSOSFM2',pic:'',nv:''},{av:'AV84ContagemResultado_ContratadaCod2',fld:'vCONTAGEMRESULTADO_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV85ContagemResultadoExecucao_OSCod2',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',pic:'ZZZZZ9',nv:0},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultadoExecucao_Fim3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM3',pic:'99/99/99 99:99',nv:''},{av:'AV35ContagemResultadoExecucao_Fim_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV36ContagemResultadoExecucao_Prevista3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA3',pic:'99/99/99 99:99',nv:''},{av:'AV37ContagemResultadoExecucao_Prevista_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV45ContagemResultado_OsFsOsFm3',fld:'vCONTAGEMRESULTADO_OSFSOSFM3',pic:'',nv:''},{av:'AV86ContagemResultado_ContratadaCod3',fld:'vCONTAGEMRESULTADO_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV87ContagemResultadoExecucao_OSCod3',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFContagemResultado_OsFsOsFm',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM',pic:'',nv:''},{av:'AV50TFContagemResultado_OsFsOsFm_Sel',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM_SEL',pic:'',nv:''},{av:'AV53TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV54TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV59TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV60TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV65TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV66TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'AV69TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV70TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'AV75TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV76TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_OSFSOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV138Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV39DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV38DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1405ContagemResultadoExecucao_Codigo',fld:'CONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contagemresultadoexecucao_prevista_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA',prop:'ActiveEventKey'},{av:'Ddo_contagemresultadoexecucao_prevista_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA',prop:'FilteredText_get'},{av:'Ddo_contagemresultadoexecucao_prevista_Filteredtextto_get',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV59TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV60TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS.ONOPTIONCLICKED","{handler:'E15KI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoExecucao_Fim1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoExecucao_Fim_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultadoExecucao_Prevista1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Prevista_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV43ContagemResultado_OsFsOsFm1',fld:'vCONTAGEMRESULTADO_OSFSOSFM1',pic:'',nv:''},{av:'AV82ContagemResultado_ContratadaCod1',fld:'vCONTAGEMRESULTADO_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV83ContagemResultadoExecucao_OSCod1',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoExecucao_Fim2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM2',pic:'99/99/99 99:99',nv:''},{av:'AV27ContagemResultadoExecucao_Fim_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV28ContagemResultadoExecucao_Prevista2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA2',pic:'99/99/99 99:99',nv:''},{av:'AV29ContagemResultadoExecucao_Prevista_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV44ContagemResultado_OsFsOsFm2',fld:'vCONTAGEMRESULTADO_OSFSOSFM2',pic:'',nv:''},{av:'AV84ContagemResultado_ContratadaCod2',fld:'vCONTAGEMRESULTADO_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV85ContagemResultadoExecucao_OSCod2',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',pic:'ZZZZZ9',nv:0},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultadoExecucao_Fim3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM3',pic:'99/99/99 99:99',nv:''},{av:'AV35ContagemResultadoExecucao_Fim_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV36ContagemResultadoExecucao_Prevista3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA3',pic:'99/99/99 99:99',nv:''},{av:'AV37ContagemResultadoExecucao_Prevista_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV45ContagemResultado_OsFsOsFm3',fld:'vCONTAGEMRESULTADO_OSFSOSFM3',pic:'',nv:''},{av:'AV86ContagemResultado_ContratadaCod3',fld:'vCONTAGEMRESULTADO_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV87ContagemResultadoExecucao_OSCod3',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFContagemResultado_OsFsOsFm',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM',pic:'',nv:''},{av:'AV50TFContagemResultado_OsFsOsFm_Sel',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM_SEL',pic:'',nv:''},{av:'AV53TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV54TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV59TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV60TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV65TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV66TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'AV69TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV70TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'AV75TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV76TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_OSFSOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV138Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV39DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV38DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1405ContagemResultadoExecucao_Codigo',fld:'CONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contagemresultadoexecucao_prazodias_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS',prop:'ActiveEventKey'},{av:'Ddo_contagemresultadoexecucao_prazodias_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS',prop:'FilteredText_get'},{av:'Ddo_contagemresultadoexecucao_prazodias_Filteredtextto_get',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS',prop:'FilteredTextTo_get'}],oparms:[{av:'AV65TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV66TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADOEXECUCAO_FIM.ONOPTIONCLICKED","{handler:'E16KI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoExecucao_Fim1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoExecucao_Fim_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultadoExecucao_Prevista1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Prevista_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV43ContagemResultado_OsFsOsFm1',fld:'vCONTAGEMRESULTADO_OSFSOSFM1',pic:'',nv:''},{av:'AV82ContagemResultado_ContratadaCod1',fld:'vCONTAGEMRESULTADO_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV83ContagemResultadoExecucao_OSCod1',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoExecucao_Fim2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM2',pic:'99/99/99 99:99',nv:''},{av:'AV27ContagemResultadoExecucao_Fim_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV28ContagemResultadoExecucao_Prevista2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA2',pic:'99/99/99 99:99',nv:''},{av:'AV29ContagemResultadoExecucao_Prevista_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV44ContagemResultado_OsFsOsFm2',fld:'vCONTAGEMRESULTADO_OSFSOSFM2',pic:'',nv:''},{av:'AV84ContagemResultado_ContratadaCod2',fld:'vCONTAGEMRESULTADO_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV85ContagemResultadoExecucao_OSCod2',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',pic:'ZZZZZ9',nv:0},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultadoExecucao_Fim3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM3',pic:'99/99/99 99:99',nv:''},{av:'AV35ContagemResultadoExecucao_Fim_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV36ContagemResultadoExecucao_Prevista3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA3',pic:'99/99/99 99:99',nv:''},{av:'AV37ContagemResultadoExecucao_Prevista_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV45ContagemResultado_OsFsOsFm3',fld:'vCONTAGEMRESULTADO_OSFSOSFM3',pic:'',nv:''},{av:'AV86ContagemResultado_ContratadaCod3',fld:'vCONTAGEMRESULTADO_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV87ContagemResultadoExecucao_OSCod3',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFContagemResultado_OsFsOsFm',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM',pic:'',nv:''},{av:'AV50TFContagemResultado_OsFsOsFm_Sel',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM_SEL',pic:'',nv:''},{av:'AV53TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV54TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV59TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV60TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV65TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV66TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'AV69TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV70TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'AV75TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV76TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_OSFSOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV138Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV39DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV38DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1405ContagemResultadoExecucao_Codigo',fld:'CONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contagemresultadoexecucao_fim_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_FIM',prop:'ActiveEventKey'},{av:'Ddo_contagemresultadoexecucao_fim_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_FIM',prop:'FilteredText_get'},{av:'Ddo_contagemresultadoexecucao_fim_Filteredtextto_get',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_FIM',prop:'FilteredTextTo_get'}],oparms:[{av:'AV69TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV70TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADOEXECUCAO_DIAS.ONOPTIONCLICKED","{handler:'E17KI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoExecucao_Fim1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoExecucao_Fim_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultadoExecucao_Prevista1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Prevista_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV43ContagemResultado_OsFsOsFm1',fld:'vCONTAGEMRESULTADO_OSFSOSFM1',pic:'',nv:''},{av:'AV82ContagemResultado_ContratadaCod1',fld:'vCONTAGEMRESULTADO_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV83ContagemResultadoExecucao_OSCod1',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoExecucao_Fim2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM2',pic:'99/99/99 99:99',nv:''},{av:'AV27ContagemResultadoExecucao_Fim_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV28ContagemResultadoExecucao_Prevista2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA2',pic:'99/99/99 99:99',nv:''},{av:'AV29ContagemResultadoExecucao_Prevista_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV44ContagemResultado_OsFsOsFm2',fld:'vCONTAGEMRESULTADO_OSFSOSFM2',pic:'',nv:''},{av:'AV84ContagemResultado_ContratadaCod2',fld:'vCONTAGEMRESULTADO_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV85ContagemResultadoExecucao_OSCod2',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',pic:'ZZZZZ9',nv:0},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultadoExecucao_Fim3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM3',pic:'99/99/99 99:99',nv:''},{av:'AV35ContagemResultadoExecucao_Fim_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV36ContagemResultadoExecucao_Prevista3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA3',pic:'99/99/99 99:99',nv:''},{av:'AV37ContagemResultadoExecucao_Prevista_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV45ContagemResultado_OsFsOsFm3',fld:'vCONTAGEMRESULTADO_OSFSOSFM3',pic:'',nv:''},{av:'AV86ContagemResultado_ContratadaCod3',fld:'vCONTAGEMRESULTADO_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV87ContagemResultadoExecucao_OSCod3',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFContagemResultado_OsFsOsFm',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM',pic:'',nv:''},{av:'AV50TFContagemResultado_OsFsOsFm_Sel',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM_SEL',pic:'',nv:''},{av:'AV53TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV54TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV59TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV60TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV65TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV66TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'AV69TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV70TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'AV75TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV76TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_OSFSOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV138Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV39DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV38DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1405ContagemResultadoExecucao_Codigo',fld:'CONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contagemresultadoexecucao_dias_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_DIAS',prop:'ActiveEventKey'},{av:'Ddo_contagemresultadoexecucao_dias_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_DIAS',prop:'FilteredText_get'},{av:'Ddo_contagemresultadoexecucao_dias_Filteredtextto_get',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_DIAS',prop:'FilteredTextTo_get'}],oparms:[{av:'AV75TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV76TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("GRID.LOAD","{handler:'E30KI2',iparms:[{av:'A1405ContagemResultadoExecucao_Codigo',fld:'CONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV40Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV41Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'edtContagemResultadoExecucao_Inicio_Link',ctrl:'CONTAGEMRESULTADOEXECUCAO_INICIO',prop:'Link'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E23KI2',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E18KI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoExecucao_Fim1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoExecucao_Fim_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultadoExecucao_Prevista1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Prevista_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV43ContagemResultado_OsFsOsFm1',fld:'vCONTAGEMRESULTADO_OSFSOSFM1',pic:'',nv:''},{av:'AV82ContagemResultado_ContratadaCod1',fld:'vCONTAGEMRESULTADO_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV83ContagemResultadoExecucao_OSCod1',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoExecucao_Fim2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM2',pic:'99/99/99 99:99',nv:''},{av:'AV27ContagemResultadoExecucao_Fim_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV28ContagemResultadoExecucao_Prevista2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA2',pic:'99/99/99 99:99',nv:''},{av:'AV29ContagemResultadoExecucao_Prevista_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV44ContagemResultado_OsFsOsFm2',fld:'vCONTAGEMRESULTADO_OSFSOSFM2',pic:'',nv:''},{av:'AV84ContagemResultado_ContratadaCod2',fld:'vCONTAGEMRESULTADO_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV85ContagemResultadoExecucao_OSCod2',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',pic:'ZZZZZ9',nv:0},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultadoExecucao_Fim3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM3',pic:'99/99/99 99:99',nv:''},{av:'AV35ContagemResultadoExecucao_Fim_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV36ContagemResultadoExecucao_Prevista3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA3',pic:'99/99/99 99:99',nv:''},{av:'AV37ContagemResultadoExecucao_Prevista_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV45ContagemResultado_OsFsOsFm3',fld:'vCONTAGEMRESULTADO_OSFSOSFM3',pic:'',nv:''},{av:'AV86ContagemResultado_ContratadaCod3',fld:'vCONTAGEMRESULTADO_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV87ContagemResultadoExecucao_OSCod3',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFContagemResultado_OsFsOsFm',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM',pic:'',nv:''},{av:'AV50TFContagemResultado_OsFsOsFm_Sel',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM_SEL',pic:'',nv:''},{av:'AV53TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV54TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV59TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV60TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV65TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV66TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'AV69TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV70TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'AV75TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV76TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_OSFSOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV138Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV39DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV38DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1405ContagemResultadoExecucao_Codigo',fld:'CONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV38DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV39DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV83ContagemResultadoExecucao_OSCod1',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',pic:'ZZZZZ9',nv:0},{av:'AV82ContagemResultado_ContratadaCod1',fld:'vCONTAGEMRESULTADO_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV43ContagemResultado_OsFsOsFm1',fld:'vCONTAGEMRESULTADO_OSFSOSFM1',pic:'',nv:''},{av:'AV20ContagemResultadoExecucao_Prevista1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Prevista_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoExecucao_Fim1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoExecucao_Fim_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV85ContagemResultadoExecucao_OSCod2',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',pic:'ZZZZZ9',nv:0},{av:'AV84ContagemResultado_ContratadaCod2',fld:'vCONTAGEMRESULTADO_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV44ContagemResultado_OsFsOsFm2',fld:'vCONTAGEMRESULTADO_OSFSOSFM2',pic:'',nv:''},{av:'AV28ContagemResultadoExecucao_Prevista2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA2',pic:'99/99/99 99:99',nv:''},{av:'AV29ContagemResultadoExecucao_Prevista_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoExecucao_Fim2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM2',pic:'99/99/99 99:99',nv:''},{av:'AV27ContagemResultadoExecucao_Fim_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV87ContagemResultadoExecucao_OSCod3',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',pic:'ZZZZZ9',nv:0},{av:'AV86ContagemResultado_ContratadaCod3',fld:'vCONTAGEMRESULTADO_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV45ContagemResultado_OsFsOsFm3',fld:'vCONTAGEMRESULTADO_OSFSOSFM3',pic:'',nv:''},{av:'AV36ContagemResultadoExecucao_Prevista3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA3',pic:'99/99/99 99:99',nv:''},{av:'AV37ContagemResultadoExecucao_Prevista_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultadoExecucao_Fim3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM3',pic:'99/99/99 99:99',nv:''},{av:'AV35ContagemResultadoExecucao_Fim_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO3',pic:'99/99/99 99:99',nv:''},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_fim2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA2',prop:'Visible'},{av:'edtavContagemresultado_osfsosfm2_Visible',ctrl:'vCONTAGEMRESULTADO_OSFSOSFM2',prop:'Visible'},{av:'dynavContagemresultado_contratadacod2'},{av:'edtavContagemresultadoexecucao_oscod2_Visible',ctrl:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_fim3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA3',prop:'Visible'},{av:'edtavContagemresultado_osfsosfm3_Visible',ctrl:'vCONTAGEMRESULTADO_OSFSOSFM3',prop:'Visible'},{av:'dynavContagemresultado_contratadacod3'},{av:'edtavContagemresultadoexecucao_oscod3_Visible',ctrl:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_fim1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA1',prop:'Visible'},{av:'edtavContagemresultado_osfsosfm1_Visible',ctrl:'vCONTAGEMRESULTADO_OSFSOSFM1',prop:'Visible'},{av:'dynavContagemresultado_contratadacod1'},{av:'edtavContagemresultadoexecucao_oscod1_Visible',ctrl:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E24KI2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_fim1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA1',prop:'Visible'},{av:'edtavContagemresultado_osfsosfm1_Visible',ctrl:'vCONTAGEMRESULTADO_OSFSOSFM1',prop:'Visible'},{av:'dynavContagemresultado_contratadacod1'},{av:'edtavContagemresultadoexecucao_oscod1_Visible',ctrl:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',prop:'Visible'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E25KI2',iparms:[],oparms:[{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E19KI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoExecucao_Fim1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoExecucao_Fim_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultadoExecucao_Prevista1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Prevista_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV43ContagemResultado_OsFsOsFm1',fld:'vCONTAGEMRESULTADO_OSFSOSFM1',pic:'',nv:''},{av:'AV82ContagemResultado_ContratadaCod1',fld:'vCONTAGEMRESULTADO_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV83ContagemResultadoExecucao_OSCod1',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoExecucao_Fim2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM2',pic:'99/99/99 99:99',nv:''},{av:'AV27ContagemResultadoExecucao_Fim_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV28ContagemResultadoExecucao_Prevista2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA2',pic:'99/99/99 99:99',nv:''},{av:'AV29ContagemResultadoExecucao_Prevista_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV44ContagemResultado_OsFsOsFm2',fld:'vCONTAGEMRESULTADO_OSFSOSFM2',pic:'',nv:''},{av:'AV84ContagemResultado_ContratadaCod2',fld:'vCONTAGEMRESULTADO_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV85ContagemResultadoExecucao_OSCod2',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',pic:'ZZZZZ9',nv:0},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultadoExecucao_Fim3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM3',pic:'99/99/99 99:99',nv:''},{av:'AV35ContagemResultadoExecucao_Fim_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV36ContagemResultadoExecucao_Prevista3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA3',pic:'99/99/99 99:99',nv:''},{av:'AV37ContagemResultadoExecucao_Prevista_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV45ContagemResultado_OsFsOsFm3',fld:'vCONTAGEMRESULTADO_OSFSOSFM3',pic:'',nv:''},{av:'AV86ContagemResultado_ContratadaCod3',fld:'vCONTAGEMRESULTADO_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV87ContagemResultadoExecucao_OSCod3',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFContagemResultado_OsFsOsFm',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM',pic:'',nv:''},{av:'AV50TFContagemResultado_OsFsOsFm_Sel',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM_SEL',pic:'',nv:''},{av:'AV53TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV54TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV59TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV60TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV65TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV66TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'AV69TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV70TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'AV75TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV76TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_OSFSOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV138Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV39DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV38DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1405ContagemResultadoExecucao_Codigo',fld:'CONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV38DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV83ContagemResultadoExecucao_OSCod1',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',pic:'ZZZZZ9',nv:0},{av:'AV82ContagemResultado_ContratadaCod1',fld:'vCONTAGEMRESULTADO_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV43ContagemResultado_OsFsOsFm1',fld:'vCONTAGEMRESULTADO_OSFSOSFM1',pic:'',nv:''},{av:'AV20ContagemResultadoExecucao_Prevista1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Prevista_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoExecucao_Fim1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoExecucao_Fim_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV85ContagemResultadoExecucao_OSCod2',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',pic:'ZZZZZ9',nv:0},{av:'AV84ContagemResultado_ContratadaCod2',fld:'vCONTAGEMRESULTADO_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV44ContagemResultado_OsFsOsFm2',fld:'vCONTAGEMRESULTADO_OSFSOSFM2',pic:'',nv:''},{av:'AV28ContagemResultadoExecucao_Prevista2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA2',pic:'99/99/99 99:99',nv:''},{av:'AV29ContagemResultadoExecucao_Prevista_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoExecucao_Fim2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM2',pic:'99/99/99 99:99',nv:''},{av:'AV27ContagemResultadoExecucao_Fim_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV87ContagemResultadoExecucao_OSCod3',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',pic:'ZZZZZ9',nv:0},{av:'AV86ContagemResultado_ContratadaCod3',fld:'vCONTAGEMRESULTADO_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV45ContagemResultado_OsFsOsFm3',fld:'vCONTAGEMRESULTADO_OSFSOSFM3',pic:'',nv:''},{av:'AV36ContagemResultadoExecucao_Prevista3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA3',pic:'99/99/99 99:99',nv:''},{av:'AV37ContagemResultadoExecucao_Prevista_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultadoExecucao_Fim3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM3',pic:'99/99/99 99:99',nv:''},{av:'AV35ContagemResultadoExecucao_Fim_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO3',pic:'99/99/99 99:99',nv:''},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_fim2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA2',prop:'Visible'},{av:'edtavContagemresultado_osfsosfm2_Visible',ctrl:'vCONTAGEMRESULTADO_OSFSOSFM2',prop:'Visible'},{av:'dynavContagemresultado_contratadacod2'},{av:'edtavContagemresultadoexecucao_oscod2_Visible',ctrl:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_fim3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA3',prop:'Visible'},{av:'edtavContagemresultado_osfsosfm3_Visible',ctrl:'vCONTAGEMRESULTADO_OSFSOSFM3',prop:'Visible'},{av:'dynavContagemresultado_contratadacod3'},{av:'edtavContagemresultadoexecucao_oscod3_Visible',ctrl:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_fim1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA1',prop:'Visible'},{av:'edtavContagemresultado_osfsosfm1_Visible',ctrl:'vCONTAGEMRESULTADO_OSFSOSFM1',prop:'Visible'},{av:'dynavContagemresultado_contratadacod1'},{av:'edtavContagemresultadoexecucao_oscod1_Visible',ctrl:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E26KI2',iparms:[{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_fim2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA2',prop:'Visible'},{av:'edtavContagemresultado_osfsosfm2_Visible',ctrl:'vCONTAGEMRESULTADO_OSFSOSFM2',prop:'Visible'},{av:'dynavContagemresultado_contratadacod2'},{av:'edtavContagemresultadoexecucao_oscod2_Visible',ctrl:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E20KI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoExecucao_Fim1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoExecucao_Fim_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultadoExecucao_Prevista1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Prevista_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV43ContagemResultado_OsFsOsFm1',fld:'vCONTAGEMRESULTADO_OSFSOSFM1',pic:'',nv:''},{av:'AV82ContagemResultado_ContratadaCod1',fld:'vCONTAGEMRESULTADO_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV83ContagemResultadoExecucao_OSCod1',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoExecucao_Fim2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM2',pic:'99/99/99 99:99',nv:''},{av:'AV27ContagemResultadoExecucao_Fim_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV28ContagemResultadoExecucao_Prevista2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA2',pic:'99/99/99 99:99',nv:''},{av:'AV29ContagemResultadoExecucao_Prevista_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV44ContagemResultado_OsFsOsFm2',fld:'vCONTAGEMRESULTADO_OSFSOSFM2',pic:'',nv:''},{av:'AV84ContagemResultado_ContratadaCod2',fld:'vCONTAGEMRESULTADO_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV85ContagemResultadoExecucao_OSCod2',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',pic:'ZZZZZ9',nv:0},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultadoExecucao_Fim3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM3',pic:'99/99/99 99:99',nv:''},{av:'AV35ContagemResultadoExecucao_Fim_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV36ContagemResultadoExecucao_Prevista3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA3',pic:'99/99/99 99:99',nv:''},{av:'AV37ContagemResultadoExecucao_Prevista_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV45ContagemResultado_OsFsOsFm3',fld:'vCONTAGEMRESULTADO_OSFSOSFM3',pic:'',nv:''},{av:'AV86ContagemResultado_ContratadaCod3',fld:'vCONTAGEMRESULTADO_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV87ContagemResultadoExecucao_OSCod3',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFContagemResultado_OsFsOsFm',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM',pic:'',nv:''},{av:'AV50TFContagemResultado_OsFsOsFm_Sel',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM_SEL',pic:'',nv:''},{av:'AV53TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV54TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV59TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV60TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV65TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV66TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'AV69TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV70TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'AV75TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV76TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_OSFSOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV138Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV39DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV38DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1405ContagemResultadoExecucao_Codigo',fld:'CONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV38DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV83ContagemResultadoExecucao_OSCod1',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',pic:'ZZZZZ9',nv:0},{av:'AV82ContagemResultado_ContratadaCod1',fld:'vCONTAGEMRESULTADO_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV43ContagemResultado_OsFsOsFm1',fld:'vCONTAGEMRESULTADO_OSFSOSFM1',pic:'',nv:''},{av:'AV20ContagemResultadoExecucao_Prevista1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Prevista_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoExecucao_Fim1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoExecucao_Fim_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV85ContagemResultadoExecucao_OSCod2',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',pic:'ZZZZZ9',nv:0},{av:'AV84ContagemResultado_ContratadaCod2',fld:'vCONTAGEMRESULTADO_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV44ContagemResultado_OsFsOsFm2',fld:'vCONTAGEMRESULTADO_OSFSOSFM2',pic:'',nv:''},{av:'AV28ContagemResultadoExecucao_Prevista2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA2',pic:'99/99/99 99:99',nv:''},{av:'AV29ContagemResultadoExecucao_Prevista_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoExecucao_Fim2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM2',pic:'99/99/99 99:99',nv:''},{av:'AV27ContagemResultadoExecucao_Fim_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV87ContagemResultadoExecucao_OSCod3',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',pic:'ZZZZZ9',nv:0},{av:'AV86ContagemResultado_ContratadaCod3',fld:'vCONTAGEMRESULTADO_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV45ContagemResultado_OsFsOsFm3',fld:'vCONTAGEMRESULTADO_OSFSOSFM3',pic:'',nv:''},{av:'AV36ContagemResultadoExecucao_Prevista3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA3',pic:'99/99/99 99:99',nv:''},{av:'AV37ContagemResultadoExecucao_Prevista_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultadoExecucao_Fim3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM3',pic:'99/99/99 99:99',nv:''},{av:'AV35ContagemResultadoExecucao_Fim_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO3',pic:'99/99/99 99:99',nv:''},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_fim2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA2',prop:'Visible'},{av:'edtavContagemresultado_osfsosfm2_Visible',ctrl:'vCONTAGEMRESULTADO_OSFSOSFM2',prop:'Visible'},{av:'dynavContagemresultado_contratadacod2'},{av:'edtavContagemresultadoexecucao_oscod2_Visible',ctrl:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_fim3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA3',prop:'Visible'},{av:'edtavContagemresultado_osfsosfm3_Visible',ctrl:'vCONTAGEMRESULTADO_OSFSOSFM3',prop:'Visible'},{av:'dynavContagemresultado_contratadacod3'},{av:'edtavContagemresultadoexecucao_oscod3_Visible',ctrl:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_fim1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA1',prop:'Visible'},{av:'edtavContagemresultado_osfsosfm1_Visible',ctrl:'vCONTAGEMRESULTADO_OSFSOSFM1',prop:'Visible'},{av:'dynavContagemresultado_contratadacod1'},{av:'edtavContagemresultadoexecucao_oscod1_Visible',ctrl:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E27KI2',iparms:[{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_fim3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA3',prop:'Visible'},{av:'edtavContagemresultado_osfsosfm3_Visible',ctrl:'vCONTAGEMRESULTADO_OSFSOSFM3',prop:'Visible'},{av:'dynavContagemresultado_contratadacod3'},{av:'edtavContagemresultadoexecucao_oscod3_Visible',ctrl:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E21KI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoExecucao_Fim1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoExecucao_Fim_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultadoExecucao_Prevista1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Prevista_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV43ContagemResultado_OsFsOsFm1',fld:'vCONTAGEMRESULTADO_OSFSOSFM1',pic:'',nv:''},{av:'AV82ContagemResultado_ContratadaCod1',fld:'vCONTAGEMRESULTADO_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV83ContagemResultadoExecucao_OSCod1',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoExecucao_Fim2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM2',pic:'99/99/99 99:99',nv:''},{av:'AV27ContagemResultadoExecucao_Fim_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV28ContagemResultadoExecucao_Prevista2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA2',pic:'99/99/99 99:99',nv:''},{av:'AV29ContagemResultadoExecucao_Prevista_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV44ContagemResultado_OsFsOsFm2',fld:'vCONTAGEMRESULTADO_OSFSOSFM2',pic:'',nv:''},{av:'AV84ContagemResultado_ContratadaCod2',fld:'vCONTAGEMRESULTADO_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV85ContagemResultadoExecucao_OSCod2',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',pic:'ZZZZZ9',nv:0},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultadoExecucao_Fim3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM3',pic:'99/99/99 99:99',nv:''},{av:'AV35ContagemResultadoExecucao_Fim_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV36ContagemResultadoExecucao_Prevista3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA3',pic:'99/99/99 99:99',nv:''},{av:'AV37ContagemResultadoExecucao_Prevista_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV45ContagemResultado_OsFsOsFm3',fld:'vCONTAGEMRESULTADO_OSFSOSFM3',pic:'',nv:''},{av:'AV86ContagemResultado_ContratadaCod3',fld:'vCONTAGEMRESULTADO_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV87ContagemResultadoExecucao_OSCod3',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFContagemResultado_OsFsOsFm',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM',pic:'',nv:''},{av:'AV50TFContagemResultado_OsFsOsFm_Sel',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM_SEL',pic:'',nv:''},{av:'AV53TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV54TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV59TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV60TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV65TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV66TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'AV69TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV70TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'AV75TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV76TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_OSFSOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV138Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV39DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV38DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1405ContagemResultadoExecucao_Codigo',fld:'CONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV88Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV49TFContagemResultado_OsFsOsFm',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM',pic:'',nv:''},{av:'Ddo_contagemresultado_osfsosfm_Filteredtext_set',ctrl:'DDO_CONTAGEMRESULTADO_OSFSOSFM',prop:'FilteredText_set'},{av:'AV50TFContagemResultado_OsFsOsFm_Sel',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM_SEL',pic:'',nv:''},{av:'Ddo_contagemresultado_osfsosfm_Selectedvalue_set',ctrl:'DDO_CONTAGEMRESULTADO_OSFSOSFM',prop:'SelectedValue_set'},{av:'AV53TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_contagemresultadoexecucao_inicio_Filteredtext_set',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_INICIO',prop:'FilteredText_set'},{av:'AV54TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_contagemresultadoexecucao_inicio_Filteredtextto_set',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_INICIO',prop:'FilteredTextTo_set'},{av:'AV59TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'Ddo_contagemresultadoexecucao_prevista_Filteredtext_set',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA',prop:'FilteredText_set'},{av:'AV60TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_contagemresultadoexecucao_prevista_Filteredtextto_set',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA',prop:'FilteredTextTo_set'},{av:'AV65TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'Ddo_contagemresultadoexecucao_prazodias_Filteredtext_set',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS',prop:'FilteredText_set'},{av:'AV66TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'Ddo_contagemresultadoexecucao_prazodias_Filteredtextto_set',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS',prop:'FilteredTextTo_set'},{av:'AV69TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'Ddo_contagemresultadoexecucao_fim_Filteredtext_set',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_FIM',prop:'FilteredText_set'},{av:'AV70TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_contagemresultadoexecucao_fim_Filteredtextto_set',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_FIM',prop:'FilteredTextTo_set'},{av:'AV75TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'Ddo_contagemresultadoexecucao_dias_Filteredtext_set',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_DIAS',prop:'FilteredText_set'},{av:'AV76TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'Ddo_contagemresultadoexecucao_dias_Filteredtextto_set',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_DIAS',prop:'FilteredTextTo_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_fim1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA1',prop:'Visible'},{av:'edtavContagemresultado_osfsosfm1_Visible',ctrl:'vCONTAGEMRESULTADO_OSFSOSFM1',prop:'Visible'},{av:'dynavContagemresultado_contratadacod1'},{av:'edtavContagemresultadoexecucao_oscod1_Visible',ctrl:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',prop:'Visible'},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV83ContagemResultadoExecucao_OSCod1',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',pic:'ZZZZZ9',nv:0},{av:'AV82ContagemResultado_ContratadaCod1',fld:'vCONTAGEMRESULTADO_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV43ContagemResultado_OsFsOsFm1',fld:'vCONTAGEMRESULTADO_OSFSOSFM1',pic:'',nv:''},{av:'AV20ContagemResultadoExecucao_Prevista1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Prevista_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoExecucao_Fim1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoExecucao_Fim_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO1',pic:'99/99/99 99:99',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV85ContagemResultadoExecucao_OSCod2',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',pic:'ZZZZZ9',nv:0},{av:'AV84ContagemResultado_ContratadaCod2',fld:'vCONTAGEMRESULTADO_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV44ContagemResultado_OsFsOsFm2',fld:'vCONTAGEMRESULTADO_OSFSOSFM2',pic:'',nv:''},{av:'AV28ContagemResultadoExecucao_Prevista2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA2',pic:'99/99/99 99:99',nv:''},{av:'AV29ContagemResultadoExecucao_Prevista_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoExecucao_Fim2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM2',pic:'99/99/99 99:99',nv:''},{av:'AV27ContagemResultadoExecucao_Fim_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV87ContagemResultadoExecucao_OSCod3',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',pic:'ZZZZZ9',nv:0},{av:'AV86ContagemResultado_ContratadaCod3',fld:'vCONTAGEMRESULTADO_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV45ContagemResultado_OsFsOsFm3',fld:'vCONTAGEMRESULTADO_OSFSOSFM3',pic:'',nv:''},{av:'AV36ContagemResultadoExecucao_Prevista3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA3',pic:'99/99/99 99:99',nv:''},{av:'AV37ContagemResultadoExecucao_Prevista_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultadoExecucao_Fim3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM3',pic:'99/99/99 99:99',nv:''},{av:'AV35ContagemResultadoExecucao_Fim_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO3',pic:'99/99/99 99:99',nv:''},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_fim2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA2',prop:'Visible'},{av:'edtavContagemresultado_osfsosfm2_Visible',ctrl:'vCONTAGEMRESULTADO_OSFSOSFM2',prop:'Visible'},{av:'dynavContagemresultado_contratadacod2'},{av:'edtavContagemresultadoexecucao_oscod2_Visible',ctrl:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_fim3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA3',prop:'Visible'},{av:'edtavContagemresultado_osfsosfm3_Visible',ctrl:'vCONTAGEMRESULTADO_OSFSOSFM3',prop:'Visible'},{av:'dynavContagemresultado_contratadacod3'},{av:'edtavContagemresultadoexecucao_oscod3_Visible',ctrl:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',prop:'Visible'}]}");
         setEventMetadata("'DOATUALIZAR'","{handler:'E32KI1',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoExecucao_Fim1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoExecucao_Fim_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultadoExecucao_Prevista1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Prevista_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV43ContagemResultado_OsFsOsFm1',fld:'vCONTAGEMRESULTADO_OSFSOSFM1',pic:'',nv:''},{av:'AV82ContagemResultado_ContratadaCod1',fld:'vCONTAGEMRESULTADO_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV83ContagemResultadoExecucao_OSCod1',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoExecucao_Fim2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM2',pic:'99/99/99 99:99',nv:''},{av:'AV27ContagemResultadoExecucao_Fim_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV28ContagemResultadoExecucao_Prevista2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA2',pic:'99/99/99 99:99',nv:''},{av:'AV29ContagemResultadoExecucao_Prevista_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV44ContagemResultado_OsFsOsFm2',fld:'vCONTAGEMRESULTADO_OSFSOSFM2',pic:'',nv:''},{av:'AV84ContagemResultado_ContratadaCod2',fld:'vCONTAGEMRESULTADO_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV85ContagemResultadoExecucao_OSCod2',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',pic:'ZZZZZ9',nv:0},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultadoExecucao_Fim3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM3',pic:'99/99/99 99:99',nv:''},{av:'AV35ContagemResultadoExecucao_Fim_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV36ContagemResultadoExecucao_Prevista3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA3',pic:'99/99/99 99:99',nv:''},{av:'AV37ContagemResultadoExecucao_Prevista_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV45ContagemResultado_OsFsOsFm3',fld:'vCONTAGEMRESULTADO_OSFSOSFM3',pic:'',nv:''},{av:'AV86ContagemResultado_ContratadaCod3',fld:'vCONTAGEMRESULTADO_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV87ContagemResultadoExecucao_OSCod3',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFContagemResultado_OsFsOsFm',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM',pic:'',nv:''},{av:'AV50TFContagemResultado_OsFsOsFm_Sel',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM_SEL',pic:'',nv:''},{av:'AV53TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV54TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV59TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV60TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV65TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV66TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'AV69TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV70TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'AV75TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV76TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_OSFSOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV138Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV39DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV38DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1405ContagemResultadoExecucao_Codigo',fld:'CONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'DOINSERT'","{handler:'E22KI2',iparms:[{av:'A1405ContagemResultadoExecucao_Codigo',fld:'CONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("ENTER","{handler:'E31KI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoExecucao_Fim1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoExecucao_Fim_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultadoExecucao_Prevista1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Prevista_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV43ContagemResultado_OsFsOsFm1',fld:'vCONTAGEMRESULTADO_OSFSOSFM1',pic:'',nv:''},{av:'AV82ContagemResultado_ContratadaCod1',fld:'vCONTAGEMRESULTADO_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV83ContagemResultadoExecucao_OSCod1',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoExecucao_Fim2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM2',pic:'99/99/99 99:99',nv:''},{av:'AV27ContagemResultadoExecucao_Fim_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV28ContagemResultadoExecucao_Prevista2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA2',pic:'99/99/99 99:99',nv:''},{av:'AV29ContagemResultadoExecucao_Prevista_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV44ContagemResultado_OsFsOsFm2',fld:'vCONTAGEMRESULTADO_OSFSOSFM2',pic:'',nv:''},{av:'AV84ContagemResultado_ContratadaCod2',fld:'vCONTAGEMRESULTADO_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV85ContagemResultadoExecucao_OSCod2',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',pic:'ZZZZZ9',nv:0},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultadoExecucao_Fim3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM3',pic:'99/99/99 99:99',nv:''},{av:'AV35ContagemResultadoExecucao_Fim_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV36ContagemResultadoExecucao_Prevista3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA3',pic:'99/99/99 99:99',nv:''},{av:'AV37ContagemResultadoExecucao_Prevista_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV45ContagemResultado_OsFsOsFm3',fld:'vCONTAGEMRESULTADO_OSFSOSFM3',pic:'',nv:''},{av:'AV86ContagemResultado_ContratadaCod3',fld:'vCONTAGEMRESULTADO_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV87ContagemResultadoExecucao_OSCod3',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFContagemResultado_OsFsOsFm',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM',pic:'',nv:''},{av:'AV50TFContagemResultado_OsFsOsFm_Sel',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM_SEL',pic:'',nv:''},{av:'AV53TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV54TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV59TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV60TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV65TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV66TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'AV69TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV70TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'AV75TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV76TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_OSFSOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV138Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV39DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV38DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1405ContagemResultadoExecucao_Codigo',fld:'CONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_contagemresultado_osfsosfm_Activeeventkey = "";
         Ddo_contagemresultado_osfsosfm_Filteredtext_get = "";
         Ddo_contagemresultado_osfsosfm_Selectedvalue_get = "";
         Ddo_contagemresultadoexecucao_inicio_Activeeventkey = "";
         Ddo_contagemresultadoexecucao_inicio_Filteredtext_get = "";
         Ddo_contagemresultadoexecucao_inicio_Filteredtextto_get = "";
         Ddo_contagemresultadoexecucao_prevista_Activeeventkey = "";
         Ddo_contagemresultadoexecucao_prevista_Filteredtext_get = "";
         Ddo_contagemresultadoexecucao_prevista_Filteredtextto_get = "";
         Ddo_contagemresultadoexecucao_prazodias_Activeeventkey = "";
         Ddo_contagemresultadoexecucao_prazodias_Filteredtext_get = "";
         Ddo_contagemresultadoexecucao_prazodias_Filteredtextto_get = "";
         Ddo_contagemresultadoexecucao_fim_Activeeventkey = "";
         Ddo_contagemresultadoexecucao_fim_Filteredtext_get = "";
         Ddo_contagemresultadoexecucao_fim_Filteredtextto_get = "";
         Ddo_contagemresultadoexecucao_dias_Activeeventkey = "";
         Ddo_contagemresultadoexecucao_dias_Filteredtext_get = "";
         Ddo_contagemresultadoexecucao_dias_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV15DynamicFiltersSelector1 = "";
         AV16ContagemResultadoExecucao_Inicio1 = (DateTime)(DateTime.MinValue);
         AV17ContagemResultadoExecucao_Inicio_To1 = (DateTime)(DateTime.MinValue);
         AV18ContagemResultadoExecucao_Fim1 = (DateTime)(DateTime.MinValue);
         AV19ContagemResultadoExecucao_Fim_To1 = (DateTime)(DateTime.MinValue);
         AV20ContagemResultadoExecucao_Prevista1 = (DateTime)(DateTime.MinValue);
         AV21ContagemResultadoExecucao_Prevista_To1 = (DateTime)(DateTime.MinValue);
         AV43ContagemResultado_OsFsOsFm1 = "";
         AV23DynamicFiltersSelector2 = "";
         AV24ContagemResultadoExecucao_Inicio2 = (DateTime)(DateTime.MinValue);
         AV25ContagemResultadoExecucao_Inicio_To2 = (DateTime)(DateTime.MinValue);
         AV26ContagemResultadoExecucao_Fim2 = (DateTime)(DateTime.MinValue);
         AV27ContagemResultadoExecucao_Fim_To2 = (DateTime)(DateTime.MinValue);
         AV28ContagemResultadoExecucao_Prevista2 = (DateTime)(DateTime.MinValue);
         AV29ContagemResultadoExecucao_Prevista_To2 = (DateTime)(DateTime.MinValue);
         AV44ContagemResultado_OsFsOsFm2 = "";
         AV31DynamicFiltersSelector3 = "";
         AV32ContagemResultadoExecucao_Inicio3 = (DateTime)(DateTime.MinValue);
         AV33ContagemResultadoExecucao_Inicio_To3 = (DateTime)(DateTime.MinValue);
         AV34ContagemResultadoExecucao_Fim3 = (DateTime)(DateTime.MinValue);
         AV35ContagemResultadoExecucao_Fim_To3 = (DateTime)(DateTime.MinValue);
         AV36ContagemResultadoExecucao_Prevista3 = (DateTime)(DateTime.MinValue);
         AV37ContagemResultadoExecucao_Prevista_To3 = (DateTime)(DateTime.MinValue);
         AV45ContagemResultado_OsFsOsFm3 = "";
         AV49TFContagemResultado_OsFsOsFm = "";
         AV50TFContagemResultado_OsFsOsFm_Sel = "";
         AV53TFContagemResultadoExecucao_Inicio = (DateTime)(DateTime.MinValue);
         AV54TFContagemResultadoExecucao_Inicio_To = (DateTime)(DateTime.MinValue);
         AV59TFContagemResultadoExecucao_Prevista = (DateTime)(DateTime.MinValue);
         AV60TFContagemResultadoExecucao_Prevista_To = (DateTime)(DateTime.MinValue);
         AV69TFContagemResultadoExecucao_Fim = (DateTime)(DateTime.MinValue);
         AV70TFContagemResultadoExecucao_Fim_To = (DateTime)(DateTime.MinValue);
         AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace = "";
         AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace = "";
         AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace = "";
         AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace = "";
         AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace = "";
         AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace = "";
         AV138Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV78DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV48ContagemResultado_OsFsOsFmTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV52ContagemResultadoExecucao_InicioTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV58ContagemResultadoExecucao_PrevistaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV64ContagemResultadoExecucao_PrazoDiasTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV68ContagemResultadoExecucao_FimTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV74ContagemResultadoExecucao_DiasTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         A457ContagemResultado_Demanda = "";
         A493ContagemResultado_DemandaFM = "";
         Ddo_contagemresultado_osfsosfm_Filteredtext_set = "";
         Ddo_contagemresultado_osfsosfm_Selectedvalue_set = "";
         Ddo_contagemresultadoexecucao_inicio_Filteredtext_set = "";
         Ddo_contagemresultadoexecucao_inicio_Filteredtextto_set = "";
         Ddo_contagemresultadoexecucao_prevista_Filteredtext_set = "";
         Ddo_contagemresultadoexecucao_prevista_Filteredtextto_set = "";
         Ddo_contagemresultadoexecucao_prazodias_Filteredtext_set = "";
         Ddo_contagemresultadoexecucao_prazodias_Filteredtextto_set = "";
         Ddo_contagemresultadoexecucao_fim_Filteredtext_set = "";
         Ddo_contagemresultadoexecucao_fim_Filteredtextto_set = "";
         Ddo_contagemresultadoexecucao_dias_Filteredtext_set = "";
         Ddo_contagemresultadoexecucao_dias_Filteredtextto_set = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV55DDO_ContagemResultadoExecucao_InicioAuxDate = DateTime.MinValue;
         AV56DDO_ContagemResultadoExecucao_InicioAuxDateTo = DateTime.MinValue;
         AV61DDO_ContagemResultadoExecucao_PrevistaAuxDate = DateTime.MinValue;
         AV62DDO_ContagemResultadoExecucao_PrevistaAuxDateTo = DateTime.MinValue;
         AV71DDO_ContagemResultadoExecucao_FimAuxDate = DateTime.MinValue;
         AV72DDO_ContagemResultadoExecucao_FimAuxDateTo = DateTime.MinValue;
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV40Update = "";
         AV136Update_GXI = "";
         AV41Delete = "";
         AV137Delete_GXI = "";
         A501ContagemResultado_OsFsOsFm = "";
         A1406ContagemResultadoExecucao_Inicio = (DateTime)(DateTime.MinValue);
         A1408ContagemResultadoExecucao_Prevista = (DateTime)(DateTime.MinValue);
         A1407ContagemResultadoExecucao_Fim = (DateTime)(DateTime.MinValue);
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00KI2_A40Contratada_PessoaCod = new int[1] ;
         H00KI2_A39Contratada_Codigo = new int[1] ;
         H00KI2_A41Contratada_PessoaNom = new String[] {""} ;
         H00KI2_n41Contratada_PessoaNom = new bool[] {false} ;
         H00KI2_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00KI2_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00KI3_A40Contratada_PessoaCod = new int[1] ;
         H00KI3_A39Contratada_Codigo = new int[1] ;
         H00KI3_A41Contratada_PessoaNom = new String[] {""} ;
         H00KI3_n41Contratada_PessoaNom = new bool[] {false} ;
         H00KI3_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00KI3_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00KI4_A40Contratada_PessoaCod = new int[1] ;
         H00KI4_A39Contratada_Codigo = new int[1] ;
         H00KI4_A41Contratada_PessoaNom = new String[] {""} ;
         H00KI4_n41Contratada_PessoaNom = new bool[] {false} ;
         H00KI4_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00KI4_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00KI5_A40Contratada_PessoaCod = new int[1] ;
         H00KI5_A39Contratada_Codigo = new int[1] ;
         H00KI5_A41Contratada_PessoaNom = new String[] {""} ;
         H00KI5_n41Contratada_PessoaNom = new bool[] {false} ;
         H00KI5_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00KI5_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         GridContainer = new GXWebGrid( context);
         lV99WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1 = "";
         lV110WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2 = "";
         lV121WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3 = "";
         lV124WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm = "";
         AV92WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1 = "";
         AV93WWContagemResultadoExecucaoDS_3_Contagemresultadoexecucao_inicio1 = (DateTime)(DateTime.MinValue);
         AV94WWContagemResultadoExecucaoDS_4_Contagemresultadoexecucao_inicio_to1 = (DateTime)(DateTime.MinValue);
         AV95WWContagemResultadoExecucaoDS_5_Contagemresultadoexecucao_fim1 = (DateTime)(DateTime.MinValue);
         AV96WWContagemResultadoExecucaoDS_6_Contagemresultadoexecucao_fim_to1 = (DateTime)(DateTime.MinValue);
         AV97WWContagemResultadoExecucaoDS_7_Contagemresultadoexecucao_prevista1 = (DateTime)(DateTime.MinValue);
         AV98WWContagemResultadoExecucaoDS_8_Contagemresultadoexecucao_prevista_to1 = (DateTime)(DateTime.MinValue);
         AV99WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1 = "";
         AV103WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2 = "";
         AV104WWContagemResultadoExecucaoDS_14_Contagemresultadoexecucao_inicio2 = (DateTime)(DateTime.MinValue);
         AV105WWContagemResultadoExecucaoDS_15_Contagemresultadoexecucao_inicio_to2 = (DateTime)(DateTime.MinValue);
         AV106WWContagemResultadoExecucaoDS_16_Contagemresultadoexecucao_fim2 = (DateTime)(DateTime.MinValue);
         AV107WWContagemResultadoExecucaoDS_17_Contagemresultadoexecucao_fim_to2 = (DateTime)(DateTime.MinValue);
         AV108WWContagemResultadoExecucaoDS_18_Contagemresultadoexecucao_prevista2 = (DateTime)(DateTime.MinValue);
         AV109WWContagemResultadoExecucaoDS_19_Contagemresultadoexecucao_prevista_to2 = (DateTime)(DateTime.MinValue);
         AV110WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2 = "";
         AV114WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3 = "";
         AV115WWContagemResultadoExecucaoDS_25_Contagemresultadoexecucao_inicio3 = (DateTime)(DateTime.MinValue);
         AV116WWContagemResultadoExecucaoDS_26_Contagemresultadoexecucao_inicio_to3 = (DateTime)(DateTime.MinValue);
         AV117WWContagemResultadoExecucaoDS_27_Contagemresultadoexecucao_fim3 = (DateTime)(DateTime.MinValue);
         AV118WWContagemResultadoExecucaoDS_28_Contagemresultadoexecucao_fim_to3 = (DateTime)(DateTime.MinValue);
         AV119WWContagemResultadoExecucaoDS_29_Contagemresultadoexecucao_prevista3 = (DateTime)(DateTime.MinValue);
         AV120WWContagemResultadoExecucaoDS_30_Contagemresultadoexecucao_prevista_to3 = (DateTime)(DateTime.MinValue);
         AV121WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3 = "";
         AV125WWContagemResultadoExecucaoDS_35_Tfcontagemresultado_osfsosfm_sel = "";
         AV124WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm = "";
         AV126WWContagemResultadoExecucaoDS_36_Tfcontagemresultadoexecucao_inicio = (DateTime)(DateTime.MinValue);
         AV127WWContagemResultadoExecucaoDS_37_Tfcontagemresultadoexecucao_inicio_to = (DateTime)(DateTime.MinValue);
         AV128WWContagemResultadoExecucaoDS_38_Tfcontagemresultadoexecucao_prevista = (DateTime)(DateTime.MinValue);
         AV129WWContagemResultadoExecucaoDS_39_Tfcontagemresultadoexecucao_prevista_to = (DateTime)(DateTime.MinValue);
         AV132WWContagemResultadoExecucaoDS_42_Tfcontagemresultadoexecucao_fim = (DateTime)(DateTime.MinValue);
         AV133WWContagemResultadoExecucaoDS_43_Tfcontagemresultadoexecucao_fim_to = (DateTime)(DateTime.MinValue);
         H00KI6_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00KI6_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00KI6_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00KI6_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00KI6_A1410ContagemResultadoExecucao_Dias = new short[1] ;
         H00KI6_n1410ContagemResultadoExecucao_Dias = new bool[] {false} ;
         H00KI6_A1407ContagemResultadoExecucao_Fim = new DateTime[] {DateTime.MinValue} ;
         H00KI6_n1407ContagemResultadoExecucao_Fim = new bool[] {false} ;
         H00KI6_A1411ContagemResultadoExecucao_PrazoDias = new short[1] ;
         H00KI6_n1411ContagemResultadoExecucao_PrazoDias = new bool[] {false} ;
         H00KI6_A1408ContagemResultadoExecucao_Prevista = new DateTime[] {DateTime.MinValue} ;
         H00KI6_n1408ContagemResultadoExecucao_Prevista = new bool[] {false} ;
         H00KI6_A1406ContagemResultadoExecucao_Inicio = new DateTime[] {DateTime.MinValue} ;
         H00KI6_A1404ContagemResultadoExecucao_OSCod = new int[1] ;
         H00KI6_A1405ContagemResultadoExecucao_Codigo = new int[1] ;
         H00KI6_A493ContagemResultado_DemandaFM = new String[] {""} ;
         H00KI6_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         H00KI6_A457ContagemResultado_Demanda = new String[] {""} ;
         H00KI6_n457ContagemResultado_Demanda = new bool[] {false} ;
         H00KI7_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         GridRow = new GXWebRow();
         AV42Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblContagemresultadoexecucaotitle_Jsonclick = "";
         lblFiltertextcontratada_codigo_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         bttBtnatualizar_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblDynamicfilterscontagemresultadoexecucao_prevista_rangemiddletext3_Jsonclick = "";
         lblDynamicfilterscontagemresultadoexecucao_fim_rangemiddletext3_Jsonclick = "";
         lblDynamicfilterscontagemresultadoexecucao_inicio_rangemiddletext3_Jsonclick = "";
         lblDynamicfilterscontagemresultadoexecucao_prevista_rangemiddletext2_Jsonclick = "";
         lblDynamicfilterscontagemresultadoexecucao_fim_rangemiddletext2_Jsonclick = "";
         lblDynamicfilterscontagemresultadoexecucao_inicio_rangemiddletext2_Jsonclick = "";
         lblDynamicfilterscontagemresultadoexecucao_prevista_rangemiddletext1_Jsonclick = "";
         lblDynamicfilterscontagemresultadoexecucao_fim_rangemiddletext1_Jsonclick = "";
         lblDynamicfilterscontagemresultadoexecucao_inicio_rangemiddletext1_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwcontagemresultadoexecucao__default(),
            new Object[][] {
                new Object[] {
               H00KI2_A40Contratada_PessoaCod, H00KI2_A39Contratada_Codigo, H00KI2_A41Contratada_PessoaNom, H00KI2_n41Contratada_PessoaNom, H00KI2_A52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               H00KI3_A40Contratada_PessoaCod, H00KI3_A39Contratada_Codigo, H00KI3_A41Contratada_PessoaNom, H00KI3_n41Contratada_PessoaNom, H00KI3_A52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               H00KI4_A40Contratada_PessoaCod, H00KI4_A39Contratada_Codigo, H00KI4_A41Contratada_PessoaNom, H00KI4_n41Contratada_PessoaNom, H00KI4_A52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               H00KI5_A40Contratada_PessoaCod, H00KI5_A39Contratada_Codigo, H00KI5_A41Contratada_PessoaNom, H00KI5_n41Contratada_PessoaNom, H00KI5_A52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               H00KI6_A52Contratada_AreaTrabalhoCod, H00KI6_n52Contratada_AreaTrabalhoCod, H00KI6_A490ContagemResultado_ContratadaCod, H00KI6_n490ContagemResultado_ContratadaCod, H00KI6_A1410ContagemResultadoExecucao_Dias, H00KI6_n1410ContagemResultadoExecucao_Dias, H00KI6_A1407ContagemResultadoExecucao_Fim, H00KI6_n1407ContagemResultadoExecucao_Fim, H00KI6_A1411ContagemResultadoExecucao_PrazoDias, H00KI6_n1411ContagemResultadoExecucao_PrazoDias,
               H00KI6_A1408ContagemResultadoExecucao_Prevista, H00KI6_n1408ContagemResultadoExecucao_Prevista, H00KI6_A1406ContagemResultadoExecucao_Inicio, H00KI6_A1404ContagemResultadoExecucao_OSCod, H00KI6_A1405ContagemResultadoExecucao_Codigo, H00KI6_A493ContagemResultado_DemandaFM, H00KI6_n493ContagemResultado_DemandaFM, H00KI6_A457ContagemResultado_Demanda, H00KI6_n457ContagemResultado_Demanda
               }
               , new Object[] {
               H00KI7_AGRID_nRecordCount
               }
            }
         );
         AV138Pgmname = "WWContagemResultadoExecucao";
         /* GeneXus formulas. */
         AV138Pgmname = "WWContagemResultadoExecucao";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_152 ;
      private short nGXsfl_152_idx=1 ;
      private short AV65TFContagemResultadoExecucao_PrazoDias ;
      private short AV66TFContagemResultadoExecucao_PrazoDias_To ;
      private short AV75TFContagemResultadoExecucao_Dias ;
      private short AV76TFContagemResultadoExecucao_Dias_To ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A1411ContagemResultadoExecucao_PrazoDias ;
      private short A1410ContagemResultadoExecucao_Dias ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_152_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV130WWContagemResultadoExecucaoDS_40_Tfcontagemresultadoexecucao_prazodias ;
      private short AV131WWContagemResultadoExecucaoDS_41_Tfcontagemresultadoexecucao_prazodias_to ;
      private short AV134WWContagemResultadoExecucaoDS_44_Tfcontagemresultadoexecucao_dias ;
      private short AV135WWContagemResultadoExecucaoDS_45_Tfcontagemresultadoexecucao_dias_to ;
      private short edtContagemResultado_OsFsOsFm_Titleformat ;
      private short edtContagemResultadoExecucao_Inicio_Titleformat ;
      private short edtContagemResultadoExecucao_Prevista_Titleformat ;
      private short edtContagemResultadoExecucao_PrazoDias_Titleformat ;
      private short edtContagemResultadoExecucao_Fim_Titleformat ;
      private short edtContagemResultadoExecucao_Dias_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV82ContagemResultado_ContratadaCod1 ;
      private int AV83ContagemResultadoExecucao_OSCod1 ;
      private int AV84ContagemResultado_ContratadaCod2 ;
      private int AV85ContagemResultadoExecucao_OSCod2 ;
      private int AV86ContagemResultado_ContratadaCod3 ;
      private int AV87ContagemResultadoExecucao_OSCod3 ;
      private int AV88Contratada_Codigo ;
      private int A1405ContagemResultadoExecucao_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_contagemresultado_osfsosfm_Datalistupdateminimumcharacters ;
      private int edtavTfcontagemresultado_osfsosfm_Visible ;
      private int edtavTfcontagemresultado_osfsosfm_sel_Visible ;
      private int edtavTfcontagemresultadoexecucao_inicio_Visible ;
      private int edtavTfcontagemresultadoexecucao_inicio_to_Visible ;
      private int edtavTfcontagemresultadoexecucao_prevista_Visible ;
      private int edtavTfcontagemresultadoexecucao_prevista_to_Visible ;
      private int edtavTfcontagemresultadoexecucao_prazodias_Visible ;
      private int edtavTfcontagemresultadoexecucao_prazodias_to_Visible ;
      private int edtavTfcontagemresultadoexecucao_fim_Visible ;
      private int edtavTfcontagemresultadoexecucao_fim_to_Visible ;
      private int edtavTfcontagemresultadoexecucao_dias_Visible ;
      private int edtavTfcontagemresultadoexecucao_dias_to_Visible ;
      private int edtavDdo_contagemresultado_osfsosfmtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultadoexecucao_iniciotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultadoexecucao_previstatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultadoexecucao_prazodiastitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultadoexecucao_fimtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultadoexecucao_diastitlecontrolidtoreplace_Visible ;
      private int A1404ContagemResultadoExecucao_OSCod ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV6WWPContext_gxTpr_Contratada_codigo ;
      private int AV6WWPContext_gxTpr_Areatrabalho_codigo ;
      private int AV100WWContagemResultadoExecucaoDS_10_Contagemresultado_contratadacod1 ;
      private int AV101WWContagemResultadoExecucaoDS_11_Contagemresultadoexecucao_oscod1 ;
      private int AV111WWContagemResultadoExecucaoDS_21_Contagemresultado_contratadacod2 ;
      private int AV112WWContagemResultadoExecucaoDS_22_Contagemresultadoexecucao_oscod2 ;
      private int AV122WWContagemResultadoExecucaoDS_32_Contagemresultado_contratadacod3 ;
      private int AV123WWContagemResultadoExecucaoDS_33_Contagemresultadoexecucao_oscod3 ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int AV91WWContagemResultadoExecucaoDS_1_Contratada_codigo ;
      private int AV79PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio1_Visible ;
      private int tblTablemergeddynamicfilterscontagemresultadoexecucao_fim1_Visible ;
      private int tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista1_Visible ;
      private int edtavContagemresultado_osfsosfm1_Visible ;
      private int edtavContagemresultadoexecucao_oscod1_Visible ;
      private int tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio2_Visible ;
      private int tblTablemergeddynamicfilterscontagemresultadoexecucao_fim2_Visible ;
      private int tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista2_Visible ;
      private int edtavContagemresultado_osfsosfm2_Visible ;
      private int edtavContagemresultadoexecucao_oscod2_Visible ;
      private int tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio3_Visible ;
      private int tblTablemergeddynamicfilterscontagemresultadoexecucao_fim3_Visible ;
      private int tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista3_Visible ;
      private int edtavContagemresultado_osfsosfm3_Visible ;
      private int edtavContagemresultadoexecucao_oscod3_Visible ;
      private int AV139GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV80GridCurrentPage ;
      private long AV81GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contagemresultado_osfsosfm_Activeeventkey ;
      private String Ddo_contagemresultado_osfsosfm_Filteredtext_get ;
      private String Ddo_contagemresultado_osfsosfm_Selectedvalue_get ;
      private String Ddo_contagemresultadoexecucao_inicio_Activeeventkey ;
      private String Ddo_contagemresultadoexecucao_inicio_Filteredtext_get ;
      private String Ddo_contagemresultadoexecucao_inicio_Filteredtextto_get ;
      private String Ddo_contagemresultadoexecucao_prevista_Activeeventkey ;
      private String Ddo_contagemresultadoexecucao_prevista_Filteredtext_get ;
      private String Ddo_contagemresultadoexecucao_prevista_Filteredtextto_get ;
      private String Ddo_contagemresultadoexecucao_prazodias_Activeeventkey ;
      private String Ddo_contagemresultadoexecucao_prazodias_Filteredtext_get ;
      private String Ddo_contagemresultadoexecucao_prazodias_Filteredtextto_get ;
      private String Ddo_contagemresultadoexecucao_fim_Activeeventkey ;
      private String Ddo_contagemresultadoexecucao_fim_Filteredtext_get ;
      private String Ddo_contagemresultadoexecucao_fim_Filteredtextto_get ;
      private String Ddo_contagemresultadoexecucao_dias_Activeeventkey ;
      private String Ddo_contagemresultadoexecucao_dias_Filteredtext_get ;
      private String Ddo_contagemresultadoexecucao_dias_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_152_idx="0001" ;
      private String AV138Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contagemresultado_osfsosfm_Caption ;
      private String Ddo_contagemresultado_osfsosfm_Tooltip ;
      private String Ddo_contagemresultado_osfsosfm_Cls ;
      private String Ddo_contagemresultado_osfsosfm_Filteredtext_set ;
      private String Ddo_contagemresultado_osfsosfm_Selectedvalue_set ;
      private String Ddo_contagemresultado_osfsosfm_Dropdownoptionstype ;
      private String Ddo_contagemresultado_osfsosfm_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultado_osfsosfm_Filtertype ;
      private String Ddo_contagemresultado_osfsosfm_Datalisttype ;
      private String Ddo_contagemresultado_osfsosfm_Datalistproc ;
      private String Ddo_contagemresultado_osfsosfm_Loadingdata ;
      private String Ddo_contagemresultado_osfsosfm_Cleanfilter ;
      private String Ddo_contagemresultado_osfsosfm_Noresultsfound ;
      private String Ddo_contagemresultado_osfsosfm_Searchbuttontext ;
      private String Ddo_contagemresultadoexecucao_inicio_Caption ;
      private String Ddo_contagemresultadoexecucao_inicio_Tooltip ;
      private String Ddo_contagemresultadoexecucao_inicio_Cls ;
      private String Ddo_contagemresultadoexecucao_inicio_Filteredtext_set ;
      private String Ddo_contagemresultadoexecucao_inicio_Filteredtextto_set ;
      private String Ddo_contagemresultadoexecucao_inicio_Dropdownoptionstype ;
      private String Ddo_contagemresultadoexecucao_inicio_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultadoexecucao_inicio_Filtertype ;
      private String Ddo_contagemresultadoexecucao_inicio_Cleanfilter ;
      private String Ddo_contagemresultadoexecucao_inicio_Rangefilterfrom ;
      private String Ddo_contagemresultadoexecucao_inicio_Rangefilterto ;
      private String Ddo_contagemresultadoexecucao_inicio_Searchbuttontext ;
      private String Ddo_contagemresultadoexecucao_prevista_Caption ;
      private String Ddo_contagemresultadoexecucao_prevista_Tooltip ;
      private String Ddo_contagemresultadoexecucao_prevista_Cls ;
      private String Ddo_contagemresultadoexecucao_prevista_Filteredtext_set ;
      private String Ddo_contagemresultadoexecucao_prevista_Filteredtextto_set ;
      private String Ddo_contagemresultadoexecucao_prevista_Dropdownoptionstype ;
      private String Ddo_contagemresultadoexecucao_prevista_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultadoexecucao_prevista_Filtertype ;
      private String Ddo_contagemresultadoexecucao_prevista_Cleanfilter ;
      private String Ddo_contagemresultadoexecucao_prevista_Rangefilterfrom ;
      private String Ddo_contagemresultadoexecucao_prevista_Rangefilterto ;
      private String Ddo_contagemresultadoexecucao_prevista_Searchbuttontext ;
      private String Ddo_contagemresultadoexecucao_prazodias_Caption ;
      private String Ddo_contagemresultadoexecucao_prazodias_Tooltip ;
      private String Ddo_contagemresultadoexecucao_prazodias_Cls ;
      private String Ddo_contagemresultadoexecucao_prazodias_Filteredtext_set ;
      private String Ddo_contagemresultadoexecucao_prazodias_Filteredtextto_set ;
      private String Ddo_contagemresultadoexecucao_prazodias_Dropdownoptionstype ;
      private String Ddo_contagemresultadoexecucao_prazodias_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultadoexecucao_prazodias_Filtertype ;
      private String Ddo_contagemresultadoexecucao_prazodias_Cleanfilter ;
      private String Ddo_contagemresultadoexecucao_prazodias_Rangefilterfrom ;
      private String Ddo_contagemresultadoexecucao_prazodias_Rangefilterto ;
      private String Ddo_contagemresultadoexecucao_prazodias_Searchbuttontext ;
      private String Ddo_contagemresultadoexecucao_fim_Caption ;
      private String Ddo_contagemresultadoexecucao_fim_Tooltip ;
      private String Ddo_contagemresultadoexecucao_fim_Cls ;
      private String Ddo_contagemresultadoexecucao_fim_Filteredtext_set ;
      private String Ddo_contagemresultadoexecucao_fim_Filteredtextto_set ;
      private String Ddo_contagemresultadoexecucao_fim_Dropdownoptionstype ;
      private String Ddo_contagemresultadoexecucao_fim_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultadoexecucao_fim_Filtertype ;
      private String Ddo_contagemresultadoexecucao_fim_Cleanfilter ;
      private String Ddo_contagemresultadoexecucao_fim_Rangefilterfrom ;
      private String Ddo_contagemresultadoexecucao_fim_Rangefilterto ;
      private String Ddo_contagemresultadoexecucao_fim_Searchbuttontext ;
      private String Ddo_contagemresultadoexecucao_dias_Caption ;
      private String Ddo_contagemresultadoexecucao_dias_Tooltip ;
      private String Ddo_contagemresultadoexecucao_dias_Cls ;
      private String Ddo_contagemresultadoexecucao_dias_Filteredtext_set ;
      private String Ddo_contagemresultadoexecucao_dias_Filteredtextto_set ;
      private String Ddo_contagemresultadoexecucao_dias_Dropdownoptionstype ;
      private String Ddo_contagemresultadoexecucao_dias_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultadoexecucao_dias_Filtertype ;
      private String Ddo_contagemresultadoexecucao_dias_Cleanfilter ;
      private String Ddo_contagemresultadoexecucao_dias_Rangefilterfrom ;
      private String Ddo_contagemresultadoexecucao_dias_Rangefilterto ;
      private String Ddo_contagemresultadoexecucao_dias_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfcontagemresultado_osfsosfm_Internalname ;
      private String edtavTfcontagemresultado_osfsosfm_Jsonclick ;
      private String edtavTfcontagemresultado_osfsosfm_sel_Internalname ;
      private String edtavTfcontagemresultado_osfsosfm_sel_Jsonclick ;
      private String edtavTfcontagemresultadoexecucao_inicio_Internalname ;
      private String edtavTfcontagemresultadoexecucao_inicio_Jsonclick ;
      private String edtavTfcontagemresultadoexecucao_inicio_to_Internalname ;
      private String edtavTfcontagemresultadoexecucao_inicio_to_Jsonclick ;
      private String divDdo_contagemresultadoexecucao_inicioauxdates_Internalname ;
      private String edtavDdo_contagemresultadoexecucao_inicioauxdate_Internalname ;
      private String edtavDdo_contagemresultadoexecucao_inicioauxdate_Jsonclick ;
      private String edtavDdo_contagemresultadoexecucao_inicioauxdateto_Internalname ;
      private String edtavDdo_contagemresultadoexecucao_inicioauxdateto_Jsonclick ;
      private String edtavTfcontagemresultadoexecucao_prevista_Internalname ;
      private String edtavTfcontagemresultadoexecucao_prevista_Jsonclick ;
      private String edtavTfcontagemresultadoexecucao_prevista_to_Internalname ;
      private String edtavTfcontagemresultadoexecucao_prevista_to_Jsonclick ;
      private String divDdo_contagemresultadoexecucao_previstaauxdates_Internalname ;
      private String edtavDdo_contagemresultadoexecucao_previstaauxdate_Internalname ;
      private String edtavDdo_contagemresultadoexecucao_previstaauxdate_Jsonclick ;
      private String edtavDdo_contagemresultadoexecucao_previstaauxdateto_Internalname ;
      private String edtavDdo_contagemresultadoexecucao_previstaauxdateto_Jsonclick ;
      private String edtavTfcontagemresultadoexecucao_prazodias_Internalname ;
      private String edtavTfcontagemresultadoexecucao_prazodias_Jsonclick ;
      private String edtavTfcontagemresultadoexecucao_prazodias_to_Internalname ;
      private String edtavTfcontagemresultadoexecucao_prazodias_to_Jsonclick ;
      private String edtavTfcontagemresultadoexecucao_fim_Internalname ;
      private String edtavTfcontagemresultadoexecucao_fim_Jsonclick ;
      private String edtavTfcontagemresultadoexecucao_fim_to_Internalname ;
      private String edtavTfcontagemresultadoexecucao_fim_to_Jsonclick ;
      private String divDdo_contagemresultadoexecucao_fimauxdates_Internalname ;
      private String edtavDdo_contagemresultadoexecucao_fimauxdate_Internalname ;
      private String edtavDdo_contagemresultadoexecucao_fimauxdate_Jsonclick ;
      private String edtavDdo_contagemresultadoexecucao_fimauxdateto_Internalname ;
      private String edtavDdo_contagemresultadoexecucao_fimauxdateto_Jsonclick ;
      private String edtavTfcontagemresultadoexecucao_dias_Internalname ;
      private String edtavTfcontagemresultadoexecucao_dias_Jsonclick ;
      private String edtavTfcontagemresultadoexecucao_dias_to_Internalname ;
      private String edtavTfcontagemresultadoexecucao_dias_to_Jsonclick ;
      private String edtavDdo_contagemresultado_osfsosfmtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultadoexecucao_iniciotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultadoexecucao_previstatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultadoexecucao_prazodiastitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultadoexecucao_fimtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultadoexecucao_diastitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtContagemResultadoExecucao_Codigo_Internalname ;
      private String edtContagemResultadoExecucao_OSCod_Internalname ;
      private String edtContagemResultado_OsFsOsFm_Internalname ;
      private String edtContagemResultadoExecucao_Inicio_Internalname ;
      private String edtContagemResultadoExecucao_Prevista_Internalname ;
      private String edtContagemResultadoExecucao_PrazoDias_Internalname ;
      private String edtContagemResultadoExecucao_Fim_Internalname ;
      private String edtContagemResultadoExecucao_Dias_Internalname ;
      private String dynavContratada_codigo_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavContagemresultadoexecucao_inicio1_Internalname ;
      private String edtavContagemresultadoexecucao_inicio_to1_Internalname ;
      private String edtavContagemresultadoexecucao_fim1_Internalname ;
      private String edtavContagemresultadoexecucao_fim_to1_Internalname ;
      private String edtavContagemresultadoexecucao_prevista1_Internalname ;
      private String edtavContagemresultadoexecucao_prevista_to1_Internalname ;
      private String edtavContagemresultado_osfsosfm1_Internalname ;
      private String dynavContagemresultado_contratadacod1_Internalname ;
      private String edtavContagemresultadoexecucao_oscod1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavContagemresultadoexecucao_inicio2_Internalname ;
      private String edtavContagemresultadoexecucao_inicio_to2_Internalname ;
      private String edtavContagemresultadoexecucao_fim2_Internalname ;
      private String edtavContagemresultadoexecucao_fim_to2_Internalname ;
      private String edtavContagemresultadoexecucao_prevista2_Internalname ;
      private String edtavContagemresultadoexecucao_prevista_to2_Internalname ;
      private String edtavContagemresultado_osfsosfm2_Internalname ;
      private String dynavContagemresultado_contratadacod2_Internalname ;
      private String edtavContagemresultadoexecucao_oscod2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavContagemresultadoexecucao_inicio3_Internalname ;
      private String edtavContagemresultadoexecucao_inicio_to3_Internalname ;
      private String edtavContagemresultadoexecucao_fim3_Internalname ;
      private String edtavContagemresultadoexecucao_fim_to3_Internalname ;
      private String edtavContagemresultadoexecucao_prevista3_Internalname ;
      private String edtavContagemresultadoexecucao_prevista_to3_Internalname ;
      private String edtavContagemresultado_osfsosfm3_Internalname ;
      private String dynavContagemresultado_contratadacod3_Internalname ;
      private String edtavContagemresultadoexecucao_oscod3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_contagemresultado_osfsosfm_Internalname ;
      private String Ddo_contagemresultadoexecucao_inicio_Internalname ;
      private String Ddo_contagemresultadoexecucao_prevista_Internalname ;
      private String Ddo_contagemresultadoexecucao_prazodias_Internalname ;
      private String Ddo_contagemresultadoexecucao_fim_Internalname ;
      private String Ddo_contagemresultadoexecucao_dias_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtContagemResultado_OsFsOsFm_Title ;
      private String edtContagemResultadoExecucao_Inicio_Title ;
      private String edtContagemResultadoExecucao_Prevista_Title ;
      private String edtContagemResultadoExecucao_PrazoDias_Title ;
      private String edtContagemResultadoExecucao_Fim_Title ;
      private String edtContagemResultadoExecucao_Dias_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtContagemResultadoExecucao_Inicio_Link ;
      private String tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio1_Internalname ;
      private String tblTablemergeddynamicfilterscontagemresultadoexecucao_fim1_Internalname ;
      private String tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista1_Internalname ;
      private String tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio2_Internalname ;
      private String tblTablemergeddynamicfilterscontagemresultadoexecucao_fim2_Internalname ;
      private String tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista2_Internalname ;
      private String tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio3_Internalname ;
      private String tblTablemergeddynamicfilterscontagemresultadoexecucao_fim3_Internalname ;
      private String tblTablemergeddynamicfilterscontagemresultadoexecucao_prevista3_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblContagemresultadoexecucaotitle_Internalname ;
      private String lblContagemresultadoexecucaotitle_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblFiltertextcontratada_codigo_Internalname ;
      private String lblFiltertextcontratada_codigo_Jsonclick ;
      private String dynavContratada_codigo_Jsonclick ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String bttBtnatualizar_Internalname ;
      private String bttBtnatualizar_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavContagemresultado_osfsosfm1_Jsonclick ;
      private String dynavContagemresultado_contratadacod1_Jsonclick ;
      private String edtavContagemresultadoexecucao_oscod1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavContagemresultado_osfsosfm2_Jsonclick ;
      private String dynavContagemresultado_contratadacod2_Jsonclick ;
      private String edtavContagemresultadoexecucao_oscod2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavContagemresultado_osfsosfm3_Jsonclick ;
      private String dynavContagemresultado_contratadacod3_Jsonclick ;
      private String edtavContagemresultadoexecucao_oscod3_Jsonclick ;
      private String edtavContagemresultadoexecucao_prevista3_Jsonclick ;
      private String lblDynamicfilterscontagemresultadoexecucao_prevista_rangemiddletext3_Internalname ;
      private String lblDynamicfilterscontagemresultadoexecucao_prevista_rangemiddletext3_Jsonclick ;
      private String edtavContagemresultadoexecucao_prevista_to3_Jsonclick ;
      private String edtavContagemresultadoexecucao_fim3_Jsonclick ;
      private String lblDynamicfilterscontagemresultadoexecucao_fim_rangemiddletext3_Internalname ;
      private String lblDynamicfilterscontagemresultadoexecucao_fim_rangemiddletext3_Jsonclick ;
      private String edtavContagemresultadoexecucao_fim_to3_Jsonclick ;
      private String edtavContagemresultadoexecucao_inicio3_Jsonclick ;
      private String lblDynamicfilterscontagemresultadoexecucao_inicio_rangemiddletext3_Internalname ;
      private String lblDynamicfilterscontagemresultadoexecucao_inicio_rangemiddletext3_Jsonclick ;
      private String edtavContagemresultadoexecucao_inicio_to3_Jsonclick ;
      private String edtavContagemresultadoexecucao_prevista2_Jsonclick ;
      private String lblDynamicfilterscontagemresultadoexecucao_prevista_rangemiddletext2_Internalname ;
      private String lblDynamicfilterscontagemresultadoexecucao_prevista_rangemiddletext2_Jsonclick ;
      private String edtavContagemresultadoexecucao_prevista_to2_Jsonclick ;
      private String edtavContagemresultadoexecucao_fim2_Jsonclick ;
      private String lblDynamicfilterscontagemresultadoexecucao_fim_rangemiddletext2_Internalname ;
      private String lblDynamicfilterscontagemresultadoexecucao_fim_rangemiddletext2_Jsonclick ;
      private String edtavContagemresultadoexecucao_fim_to2_Jsonclick ;
      private String edtavContagemresultadoexecucao_inicio2_Jsonclick ;
      private String lblDynamicfilterscontagemresultadoexecucao_inicio_rangemiddletext2_Internalname ;
      private String lblDynamicfilterscontagemresultadoexecucao_inicio_rangemiddletext2_Jsonclick ;
      private String edtavContagemresultadoexecucao_inicio_to2_Jsonclick ;
      private String edtavContagemresultadoexecucao_prevista1_Jsonclick ;
      private String lblDynamicfilterscontagemresultadoexecucao_prevista_rangemiddletext1_Internalname ;
      private String lblDynamicfilterscontagemresultadoexecucao_prevista_rangemiddletext1_Jsonclick ;
      private String edtavContagemresultadoexecucao_prevista_to1_Jsonclick ;
      private String edtavContagemresultadoexecucao_fim1_Jsonclick ;
      private String lblDynamicfilterscontagemresultadoexecucao_fim_rangemiddletext1_Internalname ;
      private String lblDynamicfilterscontagemresultadoexecucao_fim_rangemiddletext1_Jsonclick ;
      private String edtavContagemresultadoexecucao_fim_to1_Jsonclick ;
      private String edtavContagemresultadoexecucao_inicio1_Jsonclick ;
      private String lblDynamicfilterscontagemresultadoexecucao_inicio_rangemiddletext1_Internalname ;
      private String lblDynamicfilterscontagemresultadoexecucao_inicio_rangemiddletext1_Jsonclick ;
      private String edtavContagemresultadoexecucao_inicio_to1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_152_fel_idx="0001" ;
      private String ROClassString ;
      private String edtContagemResultadoExecucao_Codigo_Jsonclick ;
      private String edtContagemResultadoExecucao_OSCod_Jsonclick ;
      private String edtContagemResultado_OsFsOsFm_Jsonclick ;
      private String edtContagemResultadoExecucao_Inicio_Jsonclick ;
      private String edtContagemResultadoExecucao_Prevista_Jsonclick ;
      private String edtContagemResultadoExecucao_PrazoDias_Jsonclick ;
      private String edtContagemResultadoExecucao_Fim_Jsonclick ;
      private String edtContagemResultadoExecucao_Dias_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV16ContagemResultadoExecucao_Inicio1 ;
      private DateTime AV17ContagemResultadoExecucao_Inicio_To1 ;
      private DateTime AV18ContagemResultadoExecucao_Fim1 ;
      private DateTime AV19ContagemResultadoExecucao_Fim_To1 ;
      private DateTime AV20ContagemResultadoExecucao_Prevista1 ;
      private DateTime AV21ContagemResultadoExecucao_Prevista_To1 ;
      private DateTime AV24ContagemResultadoExecucao_Inicio2 ;
      private DateTime AV25ContagemResultadoExecucao_Inicio_To2 ;
      private DateTime AV26ContagemResultadoExecucao_Fim2 ;
      private DateTime AV27ContagemResultadoExecucao_Fim_To2 ;
      private DateTime AV28ContagemResultadoExecucao_Prevista2 ;
      private DateTime AV29ContagemResultadoExecucao_Prevista_To2 ;
      private DateTime AV32ContagemResultadoExecucao_Inicio3 ;
      private DateTime AV33ContagemResultadoExecucao_Inicio_To3 ;
      private DateTime AV34ContagemResultadoExecucao_Fim3 ;
      private DateTime AV35ContagemResultadoExecucao_Fim_To3 ;
      private DateTime AV36ContagemResultadoExecucao_Prevista3 ;
      private DateTime AV37ContagemResultadoExecucao_Prevista_To3 ;
      private DateTime AV53TFContagemResultadoExecucao_Inicio ;
      private DateTime AV54TFContagemResultadoExecucao_Inicio_To ;
      private DateTime AV59TFContagemResultadoExecucao_Prevista ;
      private DateTime AV60TFContagemResultadoExecucao_Prevista_To ;
      private DateTime AV69TFContagemResultadoExecucao_Fim ;
      private DateTime AV70TFContagemResultadoExecucao_Fim_To ;
      private DateTime A1406ContagemResultadoExecucao_Inicio ;
      private DateTime A1408ContagemResultadoExecucao_Prevista ;
      private DateTime A1407ContagemResultadoExecucao_Fim ;
      private DateTime AV93WWContagemResultadoExecucaoDS_3_Contagemresultadoexecucao_inicio1 ;
      private DateTime AV94WWContagemResultadoExecucaoDS_4_Contagemresultadoexecucao_inicio_to1 ;
      private DateTime AV95WWContagemResultadoExecucaoDS_5_Contagemresultadoexecucao_fim1 ;
      private DateTime AV96WWContagemResultadoExecucaoDS_6_Contagemresultadoexecucao_fim_to1 ;
      private DateTime AV97WWContagemResultadoExecucaoDS_7_Contagemresultadoexecucao_prevista1 ;
      private DateTime AV98WWContagemResultadoExecucaoDS_8_Contagemresultadoexecucao_prevista_to1 ;
      private DateTime AV104WWContagemResultadoExecucaoDS_14_Contagemresultadoexecucao_inicio2 ;
      private DateTime AV105WWContagemResultadoExecucaoDS_15_Contagemresultadoexecucao_inicio_to2 ;
      private DateTime AV106WWContagemResultadoExecucaoDS_16_Contagemresultadoexecucao_fim2 ;
      private DateTime AV107WWContagemResultadoExecucaoDS_17_Contagemresultadoexecucao_fim_to2 ;
      private DateTime AV108WWContagemResultadoExecucaoDS_18_Contagemresultadoexecucao_prevista2 ;
      private DateTime AV109WWContagemResultadoExecucaoDS_19_Contagemresultadoexecucao_prevista_to2 ;
      private DateTime AV115WWContagemResultadoExecucaoDS_25_Contagemresultadoexecucao_inicio3 ;
      private DateTime AV116WWContagemResultadoExecucaoDS_26_Contagemresultadoexecucao_inicio_to3 ;
      private DateTime AV117WWContagemResultadoExecucaoDS_27_Contagemresultadoexecucao_fim3 ;
      private DateTime AV118WWContagemResultadoExecucaoDS_28_Contagemresultadoexecucao_fim_to3 ;
      private DateTime AV119WWContagemResultadoExecucaoDS_29_Contagemresultadoexecucao_prevista3 ;
      private DateTime AV120WWContagemResultadoExecucaoDS_30_Contagemresultadoexecucao_prevista_to3 ;
      private DateTime AV126WWContagemResultadoExecucaoDS_36_Tfcontagemresultadoexecucao_inicio ;
      private DateTime AV127WWContagemResultadoExecucaoDS_37_Tfcontagemresultadoexecucao_inicio_to ;
      private DateTime AV128WWContagemResultadoExecucaoDS_38_Tfcontagemresultadoexecucao_prevista ;
      private DateTime AV129WWContagemResultadoExecucaoDS_39_Tfcontagemresultadoexecucao_prevista_to ;
      private DateTime AV132WWContagemResultadoExecucaoDS_42_Tfcontagemresultadoexecucao_fim ;
      private DateTime AV133WWContagemResultadoExecucaoDS_43_Tfcontagemresultadoexecucao_fim_to ;
      private DateTime AV55DDO_ContagemResultadoExecucao_InicioAuxDate ;
      private DateTime AV56DDO_ContagemResultadoExecucao_InicioAuxDateTo ;
      private DateTime AV61DDO_ContagemResultadoExecucao_PrevistaAuxDate ;
      private DateTime AV62DDO_ContagemResultadoExecucao_PrevistaAuxDateTo ;
      private DateTime AV71DDO_ContagemResultadoExecucao_FimAuxDate ;
      private DateTime AV72DDO_ContagemResultadoExecucao_FimAuxDateTo ;
      private bool entryPointCalled ;
      private bool AV22DynamicFiltersEnabled2 ;
      private bool AV30DynamicFiltersEnabled3 ;
      private bool AV39DynamicFiltersIgnoreFirst ;
      private bool AV38DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contagemresultado_osfsosfm_Includesortasc ;
      private bool Ddo_contagemresultado_osfsosfm_Includesortdsc ;
      private bool Ddo_contagemresultado_osfsosfm_Includefilter ;
      private bool Ddo_contagemresultado_osfsosfm_Filterisrange ;
      private bool Ddo_contagemresultado_osfsosfm_Includedatalist ;
      private bool Ddo_contagemresultadoexecucao_inicio_Includesortasc ;
      private bool Ddo_contagemresultadoexecucao_inicio_Includesortdsc ;
      private bool Ddo_contagemresultadoexecucao_inicio_Includefilter ;
      private bool Ddo_contagemresultadoexecucao_inicio_Filterisrange ;
      private bool Ddo_contagemresultadoexecucao_inicio_Includedatalist ;
      private bool Ddo_contagemresultadoexecucao_prevista_Includesortasc ;
      private bool Ddo_contagemresultadoexecucao_prevista_Includesortdsc ;
      private bool Ddo_contagemresultadoexecucao_prevista_Includefilter ;
      private bool Ddo_contagemresultadoexecucao_prevista_Filterisrange ;
      private bool Ddo_contagemresultadoexecucao_prevista_Includedatalist ;
      private bool Ddo_contagemresultadoexecucao_prazodias_Includesortasc ;
      private bool Ddo_contagemresultadoexecucao_prazodias_Includesortdsc ;
      private bool Ddo_contagemresultadoexecucao_prazodias_Includefilter ;
      private bool Ddo_contagemresultadoexecucao_prazodias_Filterisrange ;
      private bool Ddo_contagemresultadoexecucao_prazodias_Includedatalist ;
      private bool Ddo_contagemresultadoexecucao_fim_Includesortasc ;
      private bool Ddo_contagemresultadoexecucao_fim_Includesortdsc ;
      private bool Ddo_contagemresultadoexecucao_fim_Includefilter ;
      private bool Ddo_contagemresultadoexecucao_fim_Filterisrange ;
      private bool Ddo_contagemresultadoexecucao_fim_Includedatalist ;
      private bool Ddo_contagemresultadoexecucao_dias_Includesortasc ;
      private bool Ddo_contagemresultadoexecucao_dias_Includesortdsc ;
      private bool Ddo_contagemresultadoexecucao_dias_Includefilter ;
      private bool Ddo_contagemresultadoexecucao_dias_Filterisrange ;
      private bool Ddo_contagemresultadoexecucao_dias_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1408ContagemResultadoExecucao_Prevista ;
      private bool n1411ContagemResultadoExecucao_PrazoDias ;
      private bool n1407ContagemResultadoExecucao_Fim ;
      private bool n1410ContagemResultadoExecucao_Dias ;
      private bool AV102WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 ;
      private bool AV113WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n457ContagemResultado_Demanda ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV40Update_IsBlob ;
      private bool AV41Delete_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV43ContagemResultado_OsFsOsFm1 ;
      private String AV23DynamicFiltersSelector2 ;
      private String AV44ContagemResultado_OsFsOsFm2 ;
      private String AV31DynamicFiltersSelector3 ;
      private String AV45ContagemResultado_OsFsOsFm3 ;
      private String AV49TFContagemResultado_OsFsOsFm ;
      private String AV50TFContagemResultado_OsFsOsFm_Sel ;
      private String AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace ;
      private String AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace ;
      private String AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace ;
      private String AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace ;
      private String AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace ;
      private String AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String AV136Update_GXI ;
      private String AV137Delete_GXI ;
      private String A501ContagemResultado_OsFsOsFm ;
      private String lV99WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1 ;
      private String lV110WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2 ;
      private String lV121WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3 ;
      private String lV124WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm ;
      private String AV92WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1 ;
      private String AV99WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1 ;
      private String AV103WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2 ;
      private String AV110WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2 ;
      private String AV114WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3 ;
      private String AV121WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3 ;
      private String AV125WWContagemResultadoExecucaoDS_35_Tfcontagemresultado_osfsosfm_sel ;
      private String AV124WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm ;
      private String AV40Update ;
      private String AV41Delete ;
      private IGxSession AV42Session ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavContratada_codigo ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox dynavContagemresultado_contratadacod1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox dynavContagemresultado_contratadacod2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox dynavContagemresultado_contratadacod3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H00KI2_A40Contratada_PessoaCod ;
      private int[] H00KI2_A39Contratada_Codigo ;
      private String[] H00KI2_A41Contratada_PessoaNom ;
      private bool[] H00KI2_n41Contratada_PessoaNom ;
      private int[] H00KI2_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00KI2_n52Contratada_AreaTrabalhoCod ;
      private int[] H00KI3_A40Contratada_PessoaCod ;
      private int[] H00KI3_A39Contratada_Codigo ;
      private String[] H00KI3_A41Contratada_PessoaNom ;
      private bool[] H00KI3_n41Contratada_PessoaNom ;
      private int[] H00KI3_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00KI3_n52Contratada_AreaTrabalhoCod ;
      private int[] H00KI4_A40Contratada_PessoaCod ;
      private int[] H00KI4_A39Contratada_Codigo ;
      private String[] H00KI4_A41Contratada_PessoaNom ;
      private bool[] H00KI4_n41Contratada_PessoaNom ;
      private int[] H00KI4_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00KI4_n52Contratada_AreaTrabalhoCod ;
      private int[] H00KI5_A40Contratada_PessoaCod ;
      private int[] H00KI5_A39Contratada_Codigo ;
      private String[] H00KI5_A41Contratada_PessoaNom ;
      private bool[] H00KI5_n41Contratada_PessoaNom ;
      private int[] H00KI5_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00KI5_n52Contratada_AreaTrabalhoCod ;
      private int[] H00KI6_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00KI6_n52Contratada_AreaTrabalhoCod ;
      private int[] H00KI6_A490ContagemResultado_ContratadaCod ;
      private bool[] H00KI6_n490ContagemResultado_ContratadaCod ;
      private short[] H00KI6_A1410ContagemResultadoExecucao_Dias ;
      private bool[] H00KI6_n1410ContagemResultadoExecucao_Dias ;
      private DateTime[] H00KI6_A1407ContagemResultadoExecucao_Fim ;
      private bool[] H00KI6_n1407ContagemResultadoExecucao_Fim ;
      private short[] H00KI6_A1411ContagemResultadoExecucao_PrazoDias ;
      private bool[] H00KI6_n1411ContagemResultadoExecucao_PrazoDias ;
      private DateTime[] H00KI6_A1408ContagemResultadoExecucao_Prevista ;
      private bool[] H00KI6_n1408ContagemResultadoExecucao_Prevista ;
      private DateTime[] H00KI6_A1406ContagemResultadoExecucao_Inicio ;
      private int[] H00KI6_A1404ContagemResultadoExecucao_OSCod ;
      private int[] H00KI6_A1405ContagemResultadoExecucao_Codigo ;
      private String[] H00KI6_A493ContagemResultado_DemandaFM ;
      private bool[] H00KI6_n493ContagemResultado_DemandaFM ;
      private String[] H00KI6_A457ContagemResultado_Demanda ;
      private bool[] H00KI6_n457ContagemResultado_Demanda ;
      private long[] H00KI7_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV48ContagemResultado_OsFsOsFmTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV52ContagemResultadoExecucao_InicioTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV58ContagemResultadoExecucao_PrevistaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV64ContagemResultadoExecucao_PrazoDiasTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV68ContagemResultadoExecucao_FimTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV74ContagemResultadoExecucao_DiasTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV78DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwcontagemresultadoexecucao__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00KI6( IGxContext context ,
                                             int AV6WWPContext_gxTpr_Contratada_codigo ,
                                             String AV92WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1 ,
                                             DateTime AV93WWContagemResultadoExecucaoDS_3_Contagemresultadoexecucao_inicio1 ,
                                             DateTime AV94WWContagemResultadoExecucaoDS_4_Contagemresultadoexecucao_inicio_to1 ,
                                             DateTime AV95WWContagemResultadoExecucaoDS_5_Contagemresultadoexecucao_fim1 ,
                                             DateTime AV96WWContagemResultadoExecucaoDS_6_Contagemresultadoexecucao_fim_to1 ,
                                             DateTime AV97WWContagemResultadoExecucaoDS_7_Contagemresultadoexecucao_prevista1 ,
                                             DateTime AV98WWContagemResultadoExecucaoDS_8_Contagemresultadoexecucao_prevista_to1 ,
                                             String AV99WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1 ,
                                             int AV100WWContagemResultadoExecucaoDS_10_Contagemresultado_contratadacod1 ,
                                             int AV101WWContagemResultadoExecucaoDS_11_Contagemresultadoexecucao_oscod1 ,
                                             bool AV102WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 ,
                                             String AV103WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2 ,
                                             DateTime AV104WWContagemResultadoExecucaoDS_14_Contagemresultadoexecucao_inicio2 ,
                                             DateTime AV105WWContagemResultadoExecucaoDS_15_Contagemresultadoexecucao_inicio_to2 ,
                                             DateTime AV106WWContagemResultadoExecucaoDS_16_Contagemresultadoexecucao_fim2 ,
                                             DateTime AV107WWContagemResultadoExecucaoDS_17_Contagemresultadoexecucao_fim_to2 ,
                                             DateTime AV108WWContagemResultadoExecucaoDS_18_Contagemresultadoexecucao_prevista2 ,
                                             DateTime AV109WWContagemResultadoExecucaoDS_19_Contagemresultadoexecucao_prevista_to2 ,
                                             String AV110WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2 ,
                                             int AV111WWContagemResultadoExecucaoDS_21_Contagemresultado_contratadacod2 ,
                                             int AV112WWContagemResultadoExecucaoDS_22_Contagemresultadoexecucao_oscod2 ,
                                             bool AV113WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 ,
                                             String AV114WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3 ,
                                             DateTime AV115WWContagemResultadoExecucaoDS_25_Contagemresultadoexecucao_inicio3 ,
                                             DateTime AV116WWContagemResultadoExecucaoDS_26_Contagemresultadoexecucao_inicio_to3 ,
                                             DateTime AV117WWContagemResultadoExecucaoDS_27_Contagemresultadoexecucao_fim3 ,
                                             DateTime AV118WWContagemResultadoExecucaoDS_28_Contagemresultadoexecucao_fim_to3 ,
                                             DateTime AV119WWContagemResultadoExecucaoDS_29_Contagemresultadoexecucao_prevista3 ,
                                             DateTime AV120WWContagemResultadoExecucaoDS_30_Contagemresultadoexecucao_prevista_to3 ,
                                             String AV121WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3 ,
                                             int AV122WWContagemResultadoExecucaoDS_32_Contagemresultado_contratadacod3 ,
                                             int AV123WWContagemResultadoExecucaoDS_33_Contagemresultadoexecucao_oscod3 ,
                                             String AV125WWContagemResultadoExecucaoDS_35_Tfcontagemresultado_osfsosfm_sel ,
                                             String AV124WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm ,
                                             DateTime AV126WWContagemResultadoExecucaoDS_36_Tfcontagemresultadoexecucao_inicio ,
                                             DateTime AV127WWContagemResultadoExecucaoDS_37_Tfcontagemresultadoexecucao_inicio_to ,
                                             DateTime AV128WWContagemResultadoExecucaoDS_38_Tfcontagemresultadoexecucao_prevista ,
                                             DateTime AV129WWContagemResultadoExecucaoDS_39_Tfcontagemresultadoexecucao_prevista_to ,
                                             short AV130WWContagemResultadoExecucaoDS_40_Tfcontagemresultadoexecucao_prazodias ,
                                             short AV131WWContagemResultadoExecucaoDS_41_Tfcontagemresultadoexecucao_prazodias_to ,
                                             DateTime AV132WWContagemResultadoExecucaoDS_42_Tfcontagemresultadoexecucao_fim ,
                                             DateTime AV133WWContagemResultadoExecucaoDS_43_Tfcontagemresultadoexecucao_fim_to ,
                                             short AV134WWContagemResultadoExecucaoDS_44_Tfcontagemresultadoexecucao_dias ,
                                             short AV135WWContagemResultadoExecucaoDS_45_Tfcontagemresultadoexecucao_dias_to ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo ,
                                             DateTime A1406ContagemResultadoExecucao_Inicio ,
                                             DateTime A1407ContagemResultadoExecucao_Fim ,
                                             DateTime A1408ContagemResultadoExecucao_Prevista ,
                                             String A457ContagemResultado_Demanda ,
                                             String A493ContagemResultado_DemandaFM ,
                                             int A1404ContagemResultadoExecucao_OSCod ,
                                             short A1411ContagemResultadoExecucao_PrazoDias ,
                                             short A1410ContagemResultadoExecucao_Dias )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [49] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T3.[Contratada_AreaTrabalhoCod], T2.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultadoExecucao_Dias], T1.[ContagemResultadoExecucao_Fim], T1.[ContagemResultadoExecucao_PrazoDias], T1.[ContagemResultadoExecucao_Prevista], T1.[ContagemResultadoExecucao_Inicio], T1.[ContagemResultadoExecucao_OSCod] AS ContagemResultadoExecucao_OSCod, T1.[ContagemResultadoExecucao_Codigo], T2.[ContagemResultado_DemandaFM], T2.[ContagemResultado_Demanda]";
         sFromString = " FROM (([ContagemResultadoExecucao] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultadoExecucao_OSCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod])";
         sOrderString = "";
         if ( AV6WWPContext_gxTpr_Contratada_codigo > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaCod] = @AV6WWPCo_2Contratada_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_ContratadaCod] = @AV6WWPCo_2Contratada_codigo)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( (0==AV6WWPContext_gxTpr_Contratada_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contratada_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contratada_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV92WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 ) && ( ! (DateTime.MinValue==AV93WWContagemResultadoExecucaoDS_3_Contagemresultadoexecucao_inicio1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Inicio] >= @AV93WWContagemResultadoExecucaoDS_3_Contagemresultadoexecucao_inicio1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Inicio] >= @AV93WWContagemResultadoExecucaoDS_3_Contagemresultadoexecucao_inicio1)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV92WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 ) && ( ! (DateTime.MinValue==AV94WWContagemResultadoExecucaoDS_4_Contagemresultadoexecucao_inicio_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Inicio] <= @AV94WWContagemResultadoExecucaoDS_4_Contagemresultadoexecucao_inicio_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Inicio] <= @AV94WWContagemResultadoExecucaoDS_4_Contagemresultadoexecucao_inicio_to1)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV92WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADOEXECUCAO_FIM") == 0 ) && ( ! (DateTime.MinValue==AV95WWContagemResultadoExecucaoDS_5_Contagemresultadoexecucao_fim1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Fim] >= @AV95WWContagemResultadoExecucaoDS_5_Contagemresultadoexecucao_fim1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Fim] >= @AV95WWContagemResultadoExecucaoDS_5_Contagemresultadoexecucao_fim1)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV92WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADOEXECUCAO_FIM") == 0 ) && ( ! (DateTime.MinValue==AV96WWContagemResultadoExecucaoDS_6_Contagemresultadoexecucao_fim_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Fim] <= @AV96WWContagemResultadoExecucaoDS_6_Contagemresultadoexecucao_fim_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Fim] <= @AV96WWContagemResultadoExecucaoDS_6_Contagemresultadoexecucao_fim_to1)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV92WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADOEXECUCAO_PREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV97WWContagemResultadoExecucaoDS_7_Contagemresultadoexecucao_prevista1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Prevista] >= @AV97WWContagemResultadoExecucaoDS_7_Contagemresultadoexecucao_prevista1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Prevista] >= @AV97WWContagemResultadoExecucaoDS_7_Contagemresultadoexecucao_prevista1)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV92WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADOEXECUCAO_PREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV98WWContagemResultadoExecucaoDS_8_Contagemresultadoexecucao_prevista_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Prevista] <= @AV98WWContagemResultadoExecucaoDS_8_Contagemresultadoexecucao_prevista_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Prevista] <= @AV98WWContagemResultadoExecucaoDS_8_Contagemresultadoexecucao_prevista_to1)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ( StringUtil.StrCmp(AV92WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV99WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1 + '%' or T2.[ContagemResultado_DemandaFM] like @lV99WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_Demanda] like @lV99WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1 + '%' or T2.[ContagemResultado_DemandaFM] like @lV99WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1 + '%')";
            }
         }
         else
         {
            GXv_int2[8] = 1;
            GXv_int2[9] = 1;
         }
         if ( ( StringUtil.StrCmp(AV92WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV100WWContagemResultadoExecucaoDS_10_Contagemresultado_contratadacod1 > 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaCod] = @AV100WWContagemResultadoExecucaoDS_10_Contagemresultado_contratadacod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_ContratadaCod] = @AV100WWContagemResultadoExecucaoDS_10_Contagemresultado_contratadacod1)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ( StringUtil.StrCmp(AV92WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADOEXECUCAO_OSCOD") == 0 ) && ( ( AV101WWContagemResultadoExecucaoDS_11_Contagemresultadoexecucao_oscod1 > 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_OSCod] = @AV101WWContagemResultadoExecucaoDS_11_Contagemresultadoexecucao_oscod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_OSCod] = @AV101WWContagemResultadoExecucaoDS_11_Contagemresultadoexecucao_oscod1)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( AV102WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV103WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 ) && ( ! (DateTime.MinValue==AV104WWContagemResultadoExecucaoDS_14_Contagemresultadoexecucao_inicio2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Inicio] >= @AV104WWContagemResultadoExecucaoDS_14_Contagemresultadoexecucao_inicio2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Inicio] >= @AV104WWContagemResultadoExecucaoDS_14_Contagemresultadoexecucao_inicio2)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( AV102WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV103WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 ) && ( ! (DateTime.MinValue==AV105WWContagemResultadoExecucaoDS_15_Contagemresultadoexecucao_inicio_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Inicio] <= @AV105WWContagemResultadoExecucaoDS_15_Contagemresultadoexecucao_inicio_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Inicio] <= @AV105WWContagemResultadoExecucaoDS_15_Contagemresultadoexecucao_inicio_to2)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( AV102WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV103WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2, "CONTAGEMRESULTADOEXECUCAO_FIM") == 0 ) && ( ! (DateTime.MinValue==AV106WWContagemResultadoExecucaoDS_16_Contagemresultadoexecucao_fim2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Fim] >= @AV106WWContagemResultadoExecucaoDS_16_Contagemresultadoexecucao_fim2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Fim] >= @AV106WWContagemResultadoExecucaoDS_16_Contagemresultadoexecucao_fim2)";
            }
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( AV102WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV103WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2, "CONTAGEMRESULTADOEXECUCAO_FIM") == 0 ) && ( ! (DateTime.MinValue==AV107WWContagemResultadoExecucaoDS_17_Contagemresultadoexecucao_fim_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Fim] <= @AV107WWContagemResultadoExecucaoDS_17_Contagemresultadoexecucao_fim_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Fim] <= @AV107WWContagemResultadoExecucaoDS_17_Contagemresultadoexecucao_fim_to2)";
            }
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( AV102WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV103WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2, "CONTAGEMRESULTADOEXECUCAO_PREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV108WWContagemResultadoExecucaoDS_18_Contagemresultadoexecucao_prevista2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Prevista] >= @AV108WWContagemResultadoExecucaoDS_18_Contagemresultadoexecucao_prevista2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Prevista] >= @AV108WWContagemResultadoExecucaoDS_18_Contagemresultadoexecucao_prevista2)";
            }
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( AV102WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV103WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2, "CONTAGEMRESULTADOEXECUCAO_PREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV109WWContagemResultadoExecucaoDS_19_Contagemresultadoexecucao_prevista_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Prevista] <= @AV109WWContagemResultadoExecucaoDS_19_Contagemresultadoexecucao_prevista_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Prevista] <= @AV109WWContagemResultadoExecucaoDS_19_Contagemresultadoexecucao_prevista_to2)";
            }
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( AV102WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV103WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV110WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV110WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2 + '%' or T2.[ContagemResultado_DemandaFM] like @lV110WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_Demanda] like @lV110WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2 + '%' or T2.[ContagemResultado_DemandaFM] like @lV110WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2 + '%')";
            }
         }
         else
         {
            GXv_int2[18] = 1;
            GXv_int2[19] = 1;
         }
         if ( AV102WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV103WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV111WWContagemResultadoExecucaoDS_21_Contagemresultado_contratadacod2 > 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaCod] = @AV111WWContagemResultadoExecucaoDS_21_Contagemresultado_contratadacod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_ContratadaCod] = @AV111WWContagemResultadoExecucaoDS_21_Contagemresultado_contratadacod2)";
            }
         }
         else
         {
            GXv_int2[20] = 1;
         }
         if ( AV102WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV103WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2, "CONTAGEMRESULTADOEXECUCAO_OSCOD") == 0 ) && ( ( AV112WWContagemResultadoExecucaoDS_22_Contagemresultadoexecucao_oscod2 > 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_OSCod] = @AV112WWContagemResultadoExecucaoDS_22_Contagemresultadoexecucao_oscod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_OSCod] = @AV112WWContagemResultadoExecucaoDS_22_Contagemresultadoexecucao_oscod2)";
            }
         }
         else
         {
            GXv_int2[21] = 1;
         }
         if ( AV113WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV114WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 ) && ( ! (DateTime.MinValue==AV115WWContagemResultadoExecucaoDS_25_Contagemresultadoexecucao_inicio3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Inicio] >= @AV115WWContagemResultadoExecucaoDS_25_Contagemresultadoexecucao_inicio3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Inicio] >= @AV115WWContagemResultadoExecucaoDS_25_Contagemresultadoexecucao_inicio3)";
            }
         }
         else
         {
            GXv_int2[22] = 1;
         }
         if ( AV113WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV114WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 ) && ( ! (DateTime.MinValue==AV116WWContagemResultadoExecucaoDS_26_Contagemresultadoexecucao_inicio_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Inicio] <= @AV116WWContagemResultadoExecucaoDS_26_Contagemresultadoexecucao_inicio_to3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Inicio] <= @AV116WWContagemResultadoExecucaoDS_26_Contagemresultadoexecucao_inicio_to3)";
            }
         }
         else
         {
            GXv_int2[23] = 1;
         }
         if ( AV113WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV114WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3, "CONTAGEMRESULTADOEXECUCAO_FIM") == 0 ) && ( ! (DateTime.MinValue==AV117WWContagemResultadoExecucaoDS_27_Contagemresultadoexecucao_fim3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Fim] >= @AV117WWContagemResultadoExecucaoDS_27_Contagemresultadoexecucao_fim3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Fim] >= @AV117WWContagemResultadoExecucaoDS_27_Contagemresultadoexecucao_fim3)";
            }
         }
         else
         {
            GXv_int2[24] = 1;
         }
         if ( AV113WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV114WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3, "CONTAGEMRESULTADOEXECUCAO_FIM") == 0 ) && ( ! (DateTime.MinValue==AV118WWContagemResultadoExecucaoDS_28_Contagemresultadoexecucao_fim_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Fim] <= @AV118WWContagemResultadoExecucaoDS_28_Contagemresultadoexecucao_fim_to3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Fim] <= @AV118WWContagemResultadoExecucaoDS_28_Contagemresultadoexecucao_fim_to3)";
            }
         }
         else
         {
            GXv_int2[25] = 1;
         }
         if ( AV113WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV114WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3, "CONTAGEMRESULTADOEXECUCAO_PREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV119WWContagemResultadoExecucaoDS_29_Contagemresultadoexecucao_prevista3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Prevista] >= @AV119WWContagemResultadoExecucaoDS_29_Contagemresultadoexecucao_prevista3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Prevista] >= @AV119WWContagemResultadoExecucaoDS_29_Contagemresultadoexecucao_prevista3)";
            }
         }
         else
         {
            GXv_int2[26] = 1;
         }
         if ( AV113WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV114WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3, "CONTAGEMRESULTADOEXECUCAO_PREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV120WWContagemResultadoExecucaoDS_30_Contagemresultadoexecucao_prevista_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Prevista] <= @AV120WWContagemResultadoExecucaoDS_30_Contagemresultadoexecucao_prevista_to3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Prevista] <= @AV120WWContagemResultadoExecucaoDS_30_Contagemresultadoexecucao_prevista_to3)";
            }
         }
         else
         {
            GXv_int2[27] = 1;
         }
         if ( AV113WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV114WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV121WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV121WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3 + '%' or T2.[ContagemResultado_DemandaFM] like @lV121WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_Demanda] like @lV121WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3 + '%' or T2.[ContagemResultado_DemandaFM] like @lV121WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3 + '%')";
            }
         }
         else
         {
            GXv_int2[28] = 1;
            GXv_int2[29] = 1;
         }
         if ( AV113WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV114WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV122WWContagemResultadoExecucaoDS_32_Contagemresultado_contratadacod3 > 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaCod] = @AV122WWContagemResultadoExecucaoDS_32_Contagemresultado_contratadacod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_ContratadaCod] = @AV122WWContagemResultadoExecucaoDS_32_Contagemresultado_contratadacod3)";
            }
         }
         else
         {
            GXv_int2[30] = 1;
         }
         if ( AV113WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV114WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3, "CONTAGEMRESULTADOEXECUCAO_OSCOD") == 0 ) && ( ( AV123WWContagemResultadoExecucaoDS_33_Contagemresultadoexecucao_oscod3 > 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_OSCod] = @AV123WWContagemResultadoExecucaoDS_33_Contagemresultadoexecucao_oscod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_OSCod] = @AV123WWContagemResultadoExecucaoDS_33_Contagemresultadoexecucao_oscod3)";
            }
         }
         else
         {
            GXv_int2[31] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV125WWContagemResultadoExecucaoDS_35_Tfcontagemresultado_osfsosfm_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV124WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (( RTRIM(LTRIM(T2.[ContagemResultado_Demanda])) + CASE  WHEN (T2.[ContagemResultado_DemandaFM] = '') THEN '' ELSE '|' + RTRIM(LTRIM(T2.[ContagemResultado_DemandaFM])) END) like @lV124WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm)";
            }
            else
            {
               sWhereString = sWhereString + " (( RTRIM(LTRIM(T2.[ContagemResultado_Demanda])) + CASE  WHEN (T2.[ContagemResultado_DemandaFM] = '') THEN '' ELSE '|' + RTRIM(LTRIM(T2.[ContagemResultado_DemandaFM])) END) like @lV124WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm)";
            }
         }
         else
         {
            GXv_int2[32] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV125WWContagemResultadoExecucaoDS_35_Tfcontagemresultado_osfsosfm_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (( RTRIM(LTRIM(T2.[ContagemResultado_Demanda])) + CASE  WHEN (T2.[ContagemResultado_DemandaFM] = '') THEN '' ELSE '|' + RTRIM(LTRIM(T2.[ContagemResultado_DemandaFM])) END) = @AV125WWContagemResultadoExecucaoDS_35_Tfcontagemresultado_osfsosfm_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (( RTRIM(LTRIM(T2.[ContagemResultado_Demanda])) + CASE  WHEN (T2.[ContagemResultado_DemandaFM] = '') THEN '' ELSE '|' + RTRIM(LTRIM(T2.[ContagemResultado_DemandaFM])) END) = @AV125WWContagemResultadoExecucaoDS_35_Tfcontagemresultado_osfsosfm_sel)";
            }
         }
         else
         {
            GXv_int2[33] = 1;
         }
         if ( ! (DateTime.MinValue==AV126WWContagemResultadoExecucaoDS_36_Tfcontagemresultadoexecucao_inicio) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Inicio] >= @AV126WWContagemResultadoExecucaoDS_36_Tfcontagemresultadoexecucao_inicio)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Inicio] >= @AV126WWContagemResultadoExecucaoDS_36_Tfcontagemresultadoexecucao_inicio)";
            }
         }
         else
         {
            GXv_int2[34] = 1;
         }
         if ( ! (DateTime.MinValue==AV127WWContagemResultadoExecucaoDS_37_Tfcontagemresultadoexecucao_inicio_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Inicio] <= @AV127WWContagemResultadoExecucaoDS_37_Tfcontagemresultadoexecucao_inicio_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Inicio] <= @AV127WWContagemResultadoExecucaoDS_37_Tfcontagemresultadoexecucao_inicio_to)";
            }
         }
         else
         {
            GXv_int2[35] = 1;
         }
         if ( ! (DateTime.MinValue==AV128WWContagemResultadoExecucaoDS_38_Tfcontagemresultadoexecucao_prevista) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Prevista] >= @AV128WWContagemResultadoExecucaoDS_38_Tfcontagemresultadoexecucao_prevista)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Prevista] >= @AV128WWContagemResultadoExecucaoDS_38_Tfcontagemresultadoexecucao_prevista)";
            }
         }
         else
         {
            GXv_int2[36] = 1;
         }
         if ( ! (DateTime.MinValue==AV129WWContagemResultadoExecucaoDS_39_Tfcontagemresultadoexecucao_prevista_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Prevista] <= @AV129WWContagemResultadoExecucaoDS_39_Tfcontagemresultadoexecucao_prevista_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Prevista] <= @AV129WWContagemResultadoExecucaoDS_39_Tfcontagemresultadoexecucao_prevista_to)";
            }
         }
         else
         {
            GXv_int2[37] = 1;
         }
         if ( ! (0==AV130WWContagemResultadoExecucaoDS_40_Tfcontagemresultadoexecucao_prazodias) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_PrazoDias] >= @AV130WWContagemResultadoExecucaoDS_40_Tfcontagemresultadoexecucao_prazodias)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_PrazoDias] >= @AV130WWContagemResultadoExecucaoDS_40_Tfcontagemresultadoexecucao_prazodias)";
            }
         }
         else
         {
            GXv_int2[38] = 1;
         }
         if ( ! (0==AV131WWContagemResultadoExecucaoDS_41_Tfcontagemresultadoexecucao_prazodias_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_PrazoDias] <= @AV131WWContagemResultadoExecucaoDS_41_Tfcontagemresultadoexecucao_prazodias_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_PrazoDias] <= @AV131WWContagemResultadoExecucaoDS_41_Tfcontagemresultadoexecucao_prazodias_to)";
            }
         }
         else
         {
            GXv_int2[39] = 1;
         }
         if ( ! (DateTime.MinValue==AV132WWContagemResultadoExecucaoDS_42_Tfcontagemresultadoexecucao_fim) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Fim] >= @AV132WWContagemResultadoExecucaoDS_42_Tfcontagemresultadoexecucao_fim)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Fim] >= @AV132WWContagemResultadoExecucaoDS_42_Tfcontagemresultadoexecucao_fim)";
            }
         }
         else
         {
            GXv_int2[40] = 1;
         }
         if ( ! (DateTime.MinValue==AV133WWContagemResultadoExecucaoDS_43_Tfcontagemresultadoexecucao_fim_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Fim] <= @AV133WWContagemResultadoExecucaoDS_43_Tfcontagemresultadoexecucao_fim_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Fim] <= @AV133WWContagemResultadoExecucaoDS_43_Tfcontagemresultadoexecucao_fim_to)";
            }
         }
         else
         {
            GXv_int2[41] = 1;
         }
         if ( ! (0==AV134WWContagemResultadoExecucaoDS_44_Tfcontagemresultadoexecucao_dias) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Dias] >= @AV134WWContagemResultadoExecucaoDS_44_Tfcontagemresultadoexecucao_dias)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Dias] >= @AV134WWContagemResultadoExecucaoDS_44_Tfcontagemresultadoexecucao_dias)";
            }
         }
         else
         {
            GXv_int2[42] = 1;
         }
         if ( ! (0==AV135WWContagemResultadoExecucaoDS_45_Tfcontagemresultadoexecucao_dias_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Dias] <= @AV135WWContagemResultadoExecucaoDS_45_Tfcontagemresultadoexecucao_dias_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Dias] <= @AV135WWContagemResultadoExecucaoDS_45_Tfcontagemresultadoexecucao_dias_to)";
            }
         }
         else
         {
            GXv_int2[43] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoExecucao_OSCod], T1.[ContagemResultadoExecucao_Inicio]";
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00KI7( IGxContext context ,
                                             int AV6WWPContext_gxTpr_Contratada_codigo ,
                                             String AV92WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1 ,
                                             DateTime AV93WWContagemResultadoExecucaoDS_3_Contagemresultadoexecucao_inicio1 ,
                                             DateTime AV94WWContagemResultadoExecucaoDS_4_Contagemresultadoexecucao_inicio_to1 ,
                                             DateTime AV95WWContagemResultadoExecucaoDS_5_Contagemresultadoexecucao_fim1 ,
                                             DateTime AV96WWContagemResultadoExecucaoDS_6_Contagemresultadoexecucao_fim_to1 ,
                                             DateTime AV97WWContagemResultadoExecucaoDS_7_Contagemresultadoexecucao_prevista1 ,
                                             DateTime AV98WWContagemResultadoExecucaoDS_8_Contagemresultadoexecucao_prevista_to1 ,
                                             String AV99WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1 ,
                                             int AV100WWContagemResultadoExecucaoDS_10_Contagemresultado_contratadacod1 ,
                                             int AV101WWContagemResultadoExecucaoDS_11_Contagemresultadoexecucao_oscod1 ,
                                             bool AV102WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 ,
                                             String AV103WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2 ,
                                             DateTime AV104WWContagemResultadoExecucaoDS_14_Contagemresultadoexecucao_inicio2 ,
                                             DateTime AV105WWContagemResultadoExecucaoDS_15_Contagemresultadoexecucao_inicio_to2 ,
                                             DateTime AV106WWContagemResultadoExecucaoDS_16_Contagemresultadoexecucao_fim2 ,
                                             DateTime AV107WWContagemResultadoExecucaoDS_17_Contagemresultadoexecucao_fim_to2 ,
                                             DateTime AV108WWContagemResultadoExecucaoDS_18_Contagemresultadoexecucao_prevista2 ,
                                             DateTime AV109WWContagemResultadoExecucaoDS_19_Contagemresultadoexecucao_prevista_to2 ,
                                             String AV110WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2 ,
                                             int AV111WWContagemResultadoExecucaoDS_21_Contagemresultado_contratadacod2 ,
                                             int AV112WWContagemResultadoExecucaoDS_22_Contagemresultadoexecucao_oscod2 ,
                                             bool AV113WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 ,
                                             String AV114WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3 ,
                                             DateTime AV115WWContagemResultadoExecucaoDS_25_Contagemresultadoexecucao_inicio3 ,
                                             DateTime AV116WWContagemResultadoExecucaoDS_26_Contagemresultadoexecucao_inicio_to3 ,
                                             DateTime AV117WWContagemResultadoExecucaoDS_27_Contagemresultadoexecucao_fim3 ,
                                             DateTime AV118WWContagemResultadoExecucaoDS_28_Contagemresultadoexecucao_fim_to3 ,
                                             DateTime AV119WWContagemResultadoExecucaoDS_29_Contagemresultadoexecucao_prevista3 ,
                                             DateTime AV120WWContagemResultadoExecucaoDS_30_Contagemresultadoexecucao_prevista_to3 ,
                                             String AV121WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3 ,
                                             int AV122WWContagemResultadoExecucaoDS_32_Contagemresultado_contratadacod3 ,
                                             int AV123WWContagemResultadoExecucaoDS_33_Contagemresultadoexecucao_oscod3 ,
                                             String AV125WWContagemResultadoExecucaoDS_35_Tfcontagemresultado_osfsosfm_sel ,
                                             String AV124WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm ,
                                             DateTime AV126WWContagemResultadoExecucaoDS_36_Tfcontagemresultadoexecucao_inicio ,
                                             DateTime AV127WWContagemResultadoExecucaoDS_37_Tfcontagemresultadoexecucao_inicio_to ,
                                             DateTime AV128WWContagemResultadoExecucaoDS_38_Tfcontagemresultadoexecucao_prevista ,
                                             DateTime AV129WWContagemResultadoExecucaoDS_39_Tfcontagemresultadoexecucao_prevista_to ,
                                             short AV130WWContagemResultadoExecucaoDS_40_Tfcontagemresultadoexecucao_prazodias ,
                                             short AV131WWContagemResultadoExecucaoDS_41_Tfcontagemresultadoexecucao_prazodias_to ,
                                             DateTime AV132WWContagemResultadoExecucaoDS_42_Tfcontagemresultadoexecucao_fim ,
                                             DateTime AV133WWContagemResultadoExecucaoDS_43_Tfcontagemresultadoexecucao_fim_to ,
                                             short AV134WWContagemResultadoExecucaoDS_44_Tfcontagemresultadoexecucao_dias ,
                                             short AV135WWContagemResultadoExecucaoDS_45_Tfcontagemresultadoexecucao_dias_to ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo ,
                                             DateTime A1406ContagemResultadoExecucao_Inicio ,
                                             DateTime A1407ContagemResultadoExecucao_Fim ,
                                             DateTime A1408ContagemResultadoExecucao_Prevista ,
                                             String A457ContagemResultado_Demanda ,
                                             String A493ContagemResultado_DemandaFM ,
                                             int A1404ContagemResultadoExecucao_OSCod ,
                                             short A1411ContagemResultadoExecucao_PrazoDias ,
                                             short A1410ContagemResultadoExecucao_Dias )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [44] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM (([ContagemResultadoExecucao] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultadoExecucao_OSCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod])";
         if ( AV6WWPContext_gxTpr_Contratada_codigo > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaCod] = @AV6WWPCo_2Contratada_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_ContratadaCod] = @AV6WWPCo_2Contratada_codigo)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( (0==AV6WWPContext_gxTpr_Contratada_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contratada_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contratada_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV92WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 ) && ( ! (DateTime.MinValue==AV93WWContagemResultadoExecucaoDS_3_Contagemresultadoexecucao_inicio1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Inicio] >= @AV93WWContagemResultadoExecucaoDS_3_Contagemresultadoexecucao_inicio1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Inicio] >= @AV93WWContagemResultadoExecucaoDS_3_Contagemresultadoexecucao_inicio1)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV92WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 ) && ( ! (DateTime.MinValue==AV94WWContagemResultadoExecucaoDS_4_Contagemresultadoexecucao_inicio_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Inicio] <= @AV94WWContagemResultadoExecucaoDS_4_Contagemresultadoexecucao_inicio_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Inicio] <= @AV94WWContagemResultadoExecucaoDS_4_Contagemresultadoexecucao_inicio_to1)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV92WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADOEXECUCAO_FIM") == 0 ) && ( ! (DateTime.MinValue==AV95WWContagemResultadoExecucaoDS_5_Contagemresultadoexecucao_fim1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Fim] >= @AV95WWContagemResultadoExecucaoDS_5_Contagemresultadoexecucao_fim1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Fim] >= @AV95WWContagemResultadoExecucaoDS_5_Contagemresultadoexecucao_fim1)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV92WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADOEXECUCAO_FIM") == 0 ) && ( ! (DateTime.MinValue==AV96WWContagemResultadoExecucaoDS_6_Contagemresultadoexecucao_fim_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Fim] <= @AV96WWContagemResultadoExecucaoDS_6_Contagemresultadoexecucao_fim_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Fim] <= @AV96WWContagemResultadoExecucaoDS_6_Contagemresultadoexecucao_fim_to1)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV92WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADOEXECUCAO_PREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV97WWContagemResultadoExecucaoDS_7_Contagemresultadoexecucao_prevista1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Prevista] >= @AV97WWContagemResultadoExecucaoDS_7_Contagemresultadoexecucao_prevista1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Prevista] >= @AV97WWContagemResultadoExecucaoDS_7_Contagemresultadoexecucao_prevista1)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV92WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADOEXECUCAO_PREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV98WWContagemResultadoExecucaoDS_8_Contagemresultadoexecucao_prevista_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Prevista] <= @AV98WWContagemResultadoExecucaoDS_8_Contagemresultadoexecucao_prevista_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Prevista] <= @AV98WWContagemResultadoExecucaoDS_8_Contagemresultadoexecucao_prevista_to1)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ( StringUtil.StrCmp(AV92WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV99WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1 + '%' or T2.[ContagemResultado_DemandaFM] like @lV99WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_Demanda] like @lV99WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1 + '%' or T2.[ContagemResultado_DemandaFM] like @lV99WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1 + '%')";
            }
         }
         else
         {
            GXv_int4[8] = 1;
            GXv_int4[9] = 1;
         }
         if ( ( StringUtil.StrCmp(AV92WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV100WWContagemResultadoExecucaoDS_10_Contagemresultado_contratadacod1 > 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaCod] = @AV100WWContagemResultadoExecucaoDS_10_Contagemresultado_contratadacod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_ContratadaCod] = @AV100WWContagemResultadoExecucaoDS_10_Contagemresultado_contratadacod1)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ( StringUtil.StrCmp(AV92WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADOEXECUCAO_OSCOD") == 0 ) && ( ( AV101WWContagemResultadoExecucaoDS_11_Contagemresultadoexecucao_oscod1 > 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_OSCod] = @AV101WWContagemResultadoExecucaoDS_11_Contagemresultadoexecucao_oscod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_OSCod] = @AV101WWContagemResultadoExecucaoDS_11_Contagemresultadoexecucao_oscod1)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( AV102WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV103WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 ) && ( ! (DateTime.MinValue==AV104WWContagemResultadoExecucaoDS_14_Contagemresultadoexecucao_inicio2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Inicio] >= @AV104WWContagemResultadoExecucaoDS_14_Contagemresultadoexecucao_inicio2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Inicio] >= @AV104WWContagemResultadoExecucaoDS_14_Contagemresultadoexecucao_inicio2)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( AV102WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV103WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 ) && ( ! (DateTime.MinValue==AV105WWContagemResultadoExecucaoDS_15_Contagemresultadoexecucao_inicio_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Inicio] <= @AV105WWContagemResultadoExecucaoDS_15_Contagemresultadoexecucao_inicio_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Inicio] <= @AV105WWContagemResultadoExecucaoDS_15_Contagemresultadoexecucao_inicio_to2)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( AV102WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV103WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2, "CONTAGEMRESULTADOEXECUCAO_FIM") == 0 ) && ( ! (DateTime.MinValue==AV106WWContagemResultadoExecucaoDS_16_Contagemresultadoexecucao_fim2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Fim] >= @AV106WWContagemResultadoExecucaoDS_16_Contagemresultadoexecucao_fim2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Fim] >= @AV106WWContagemResultadoExecucaoDS_16_Contagemresultadoexecucao_fim2)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( AV102WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV103WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2, "CONTAGEMRESULTADOEXECUCAO_FIM") == 0 ) && ( ! (DateTime.MinValue==AV107WWContagemResultadoExecucaoDS_17_Contagemresultadoexecucao_fim_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Fim] <= @AV107WWContagemResultadoExecucaoDS_17_Contagemresultadoexecucao_fim_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Fim] <= @AV107WWContagemResultadoExecucaoDS_17_Contagemresultadoexecucao_fim_to2)";
            }
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( AV102WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV103WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2, "CONTAGEMRESULTADOEXECUCAO_PREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV108WWContagemResultadoExecucaoDS_18_Contagemresultadoexecucao_prevista2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Prevista] >= @AV108WWContagemResultadoExecucaoDS_18_Contagemresultadoexecucao_prevista2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Prevista] >= @AV108WWContagemResultadoExecucaoDS_18_Contagemresultadoexecucao_prevista2)";
            }
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( AV102WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV103WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2, "CONTAGEMRESULTADOEXECUCAO_PREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV109WWContagemResultadoExecucaoDS_19_Contagemresultadoexecucao_prevista_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Prevista] <= @AV109WWContagemResultadoExecucaoDS_19_Contagemresultadoexecucao_prevista_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Prevista] <= @AV109WWContagemResultadoExecucaoDS_19_Contagemresultadoexecucao_prevista_to2)";
            }
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( AV102WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV103WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV110WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV110WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2 + '%' or T2.[ContagemResultado_DemandaFM] like @lV110WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_Demanda] like @lV110WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2 + '%' or T2.[ContagemResultado_DemandaFM] like @lV110WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2 + '%')";
            }
         }
         else
         {
            GXv_int4[18] = 1;
            GXv_int4[19] = 1;
         }
         if ( AV102WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV103WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV111WWContagemResultadoExecucaoDS_21_Contagemresultado_contratadacod2 > 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaCod] = @AV111WWContagemResultadoExecucaoDS_21_Contagemresultado_contratadacod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_ContratadaCod] = @AV111WWContagemResultadoExecucaoDS_21_Contagemresultado_contratadacod2)";
            }
         }
         else
         {
            GXv_int4[20] = 1;
         }
         if ( AV102WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV103WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2, "CONTAGEMRESULTADOEXECUCAO_OSCOD") == 0 ) && ( ( AV112WWContagemResultadoExecucaoDS_22_Contagemresultadoexecucao_oscod2 > 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_OSCod] = @AV112WWContagemResultadoExecucaoDS_22_Contagemresultadoexecucao_oscod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_OSCod] = @AV112WWContagemResultadoExecucaoDS_22_Contagemresultadoexecucao_oscod2)";
            }
         }
         else
         {
            GXv_int4[21] = 1;
         }
         if ( AV113WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV114WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 ) && ( ! (DateTime.MinValue==AV115WWContagemResultadoExecucaoDS_25_Contagemresultadoexecucao_inicio3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Inicio] >= @AV115WWContagemResultadoExecucaoDS_25_Contagemresultadoexecucao_inicio3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Inicio] >= @AV115WWContagemResultadoExecucaoDS_25_Contagemresultadoexecucao_inicio3)";
            }
         }
         else
         {
            GXv_int4[22] = 1;
         }
         if ( AV113WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV114WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 ) && ( ! (DateTime.MinValue==AV116WWContagemResultadoExecucaoDS_26_Contagemresultadoexecucao_inicio_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Inicio] <= @AV116WWContagemResultadoExecucaoDS_26_Contagemresultadoexecucao_inicio_to3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Inicio] <= @AV116WWContagemResultadoExecucaoDS_26_Contagemresultadoexecucao_inicio_to3)";
            }
         }
         else
         {
            GXv_int4[23] = 1;
         }
         if ( AV113WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV114WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3, "CONTAGEMRESULTADOEXECUCAO_FIM") == 0 ) && ( ! (DateTime.MinValue==AV117WWContagemResultadoExecucaoDS_27_Contagemresultadoexecucao_fim3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Fim] >= @AV117WWContagemResultadoExecucaoDS_27_Contagemresultadoexecucao_fim3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Fim] >= @AV117WWContagemResultadoExecucaoDS_27_Contagemresultadoexecucao_fim3)";
            }
         }
         else
         {
            GXv_int4[24] = 1;
         }
         if ( AV113WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV114WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3, "CONTAGEMRESULTADOEXECUCAO_FIM") == 0 ) && ( ! (DateTime.MinValue==AV118WWContagemResultadoExecucaoDS_28_Contagemresultadoexecucao_fim_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Fim] <= @AV118WWContagemResultadoExecucaoDS_28_Contagemresultadoexecucao_fim_to3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Fim] <= @AV118WWContagemResultadoExecucaoDS_28_Contagemresultadoexecucao_fim_to3)";
            }
         }
         else
         {
            GXv_int4[25] = 1;
         }
         if ( AV113WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV114WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3, "CONTAGEMRESULTADOEXECUCAO_PREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV119WWContagemResultadoExecucaoDS_29_Contagemresultadoexecucao_prevista3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Prevista] >= @AV119WWContagemResultadoExecucaoDS_29_Contagemresultadoexecucao_prevista3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Prevista] >= @AV119WWContagemResultadoExecucaoDS_29_Contagemresultadoexecucao_prevista3)";
            }
         }
         else
         {
            GXv_int4[26] = 1;
         }
         if ( AV113WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV114WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3, "CONTAGEMRESULTADOEXECUCAO_PREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV120WWContagemResultadoExecucaoDS_30_Contagemresultadoexecucao_prevista_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Prevista] <= @AV120WWContagemResultadoExecucaoDS_30_Contagemresultadoexecucao_prevista_to3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Prevista] <= @AV120WWContagemResultadoExecucaoDS_30_Contagemresultadoexecucao_prevista_to3)";
            }
         }
         else
         {
            GXv_int4[27] = 1;
         }
         if ( AV113WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV114WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV121WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV121WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3 + '%' or T2.[ContagemResultado_DemandaFM] like @lV121WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_Demanda] like @lV121WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3 + '%' or T2.[ContagemResultado_DemandaFM] like @lV121WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3 + '%')";
            }
         }
         else
         {
            GXv_int4[28] = 1;
            GXv_int4[29] = 1;
         }
         if ( AV113WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV114WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV122WWContagemResultadoExecucaoDS_32_Contagemresultado_contratadacod3 > 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaCod] = @AV122WWContagemResultadoExecucaoDS_32_Contagemresultado_contratadacod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_ContratadaCod] = @AV122WWContagemResultadoExecucaoDS_32_Contagemresultado_contratadacod3)";
            }
         }
         else
         {
            GXv_int4[30] = 1;
         }
         if ( AV113WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV114WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3, "CONTAGEMRESULTADOEXECUCAO_OSCOD") == 0 ) && ( ( AV123WWContagemResultadoExecucaoDS_33_Contagemresultadoexecucao_oscod3 > 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_OSCod] = @AV123WWContagemResultadoExecucaoDS_33_Contagemresultadoexecucao_oscod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_OSCod] = @AV123WWContagemResultadoExecucaoDS_33_Contagemresultadoexecucao_oscod3)";
            }
         }
         else
         {
            GXv_int4[31] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV125WWContagemResultadoExecucaoDS_35_Tfcontagemresultado_osfsosfm_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV124WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (( RTRIM(LTRIM(T2.[ContagemResultado_Demanda])) + CASE  WHEN (T2.[ContagemResultado_DemandaFM] = '') THEN '' ELSE '|' + RTRIM(LTRIM(T2.[ContagemResultado_DemandaFM])) END) like @lV124WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm)";
            }
            else
            {
               sWhereString = sWhereString + " (( RTRIM(LTRIM(T2.[ContagemResultado_Demanda])) + CASE  WHEN (T2.[ContagemResultado_DemandaFM] = '') THEN '' ELSE '|' + RTRIM(LTRIM(T2.[ContagemResultado_DemandaFM])) END) like @lV124WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm)";
            }
         }
         else
         {
            GXv_int4[32] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV125WWContagemResultadoExecucaoDS_35_Tfcontagemresultado_osfsosfm_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (( RTRIM(LTRIM(T2.[ContagemResultado_Demanda])) + CASE  WHEN (T2.[ContagemResultado_DemandaFM] = '') THEN '' ELSE '|' + RTRIM(LTRIM(T2.[ContagemResultado_DemandaFM])) END) = @AV125WWContagemResultadoExecucaoDS_35_Tfcontagemresultado_osfsosfm_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (( RTRIM(LTRIM(T2.[ContagemResultado_Demanda])) + CASE  WHEN (T2.[ContagemResultado_DemandaFM] = '') THEN '' ELSE '|' + RTRIM(LTRIM(T2.[ContagemResultado_DemandaFM])) END) = @AV125WWContagemResultadoExecucaoDS_35_Tfcontagemresultado_osfsosfm_sel)";
            }
         }
         else
         {
            GXv_int4[33] = 1;
         }
         if ( ! (DateTime.MinValue==AV126WWContagemResultadoExecucaoDS_36_Tfcontagemresultadoexecucao_inicio) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Inicio] >= @AV126WWContagemResultadoExecucaoDS_36_Tfcontagemresultadoexecucao_inicio)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Inicio] >= @AV126WWContagemResultadoExecucaoDS_36_Tfcontagemresultadoexecucao_inicio)";
            }
         }
         else
         {
            GXv_int4[34] = 1;
         }
         if ( ! (DateTime.MinValue==AV127WWContagemResultadoExecucaoDS_37_Tfcontagemresultadoexecucao_inicio_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Inicio] <= @AV127WWContagemResultadoExecucaoDS_37_Tfcontagemresultadoexecucao_inicio_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Inicio] <= @AV127WWContagemResultadoExecucaoDS_37_Tfcontagemresultadoexecucao_inicio_to)";
            }
         }
         else
         {
            GXv_int4[35] = 1;
         }
         if ( ! (DateTime.MinValue==AV128WWContagemResultadoExecucaoDS_38_Tfcontagemresultadoexecucao_prevista) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Prevista] >= @AV128WWContagemResultadoExecucaoDS_38_Tfcontagemresultadoexecucao_prevista)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Prevista] >= @AV128WWContagemResultadoExecucaoDS_38_Tfcontagemresultadoexecucao_prevista)";
            }
         }
         else
         {
            GXv_int4[36] = 1;
         }
         if ( ! (DateTime.MinValue==AV129WWContagemResultadoExecucaoDS_39_Tfcontagemresultadoexecucao_prevista_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Prevista] <= @AV129WWContagemResultadoExecucaoDS_39_Tfcontagemresultadoexecucao_prevista_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Prevista] <= @AV129WWContagemResultadoExecucaoDS_39_Tfcontagemresultadoexecucao_prevista_to)";
            }
         }
         else
         {
            GXv_int4[37] = 1;
         }
         if ( ! (0==AV130WWContagemResultadoExecucaoDS_40_Tfcontagemresultadoexecucao_prazodias) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_PrazoDias] >= @AV130WWContagemResultadoExecucaoDS_40_Tfcontagemresultadoexecucao_prazodias)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_PrazoDias] >= @AV130WWContagemResultadoExecucaoDS_40_Tfcontagemresultadoexecucao_prazodias)";
            }
         }
         else
         {
            GXv_int4[38] = 1;
         }
         if ( ! (0==AV131WWContagemResultadoExecucaoDS_41_Tfcontagemresultadoexecucao_prazodias_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_PrazoDias] <= @AV131WWContagemResultadoExecucaoDS_41_Tfcontagemresultadoexecucao_prazodias_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_PrazoDias] <= @AV131WWContagemResultadoExecucaoDS_41_Tfcontagemresultadoexecucao_prazodias_to)";
            }
         }
         else
         {
            GXv_int4[39] = 1;
         }
         if ( ! (DateTime.MinValue==AV132WWContagemResultadoExecucaoDS_42_Tfcontagemresultadoexecucao_fim) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Fim] >= @AV132WWContagemResultadoExecucaoDS_42_Tfcontagemresultadoexecucao_fim)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Fim] >= @AV132WWContagemResultadoExecucaoDS_42_Tfcontagemresultadoexecucao_fim)";
            }
         }
         else
         {
            GXv_int4[40] = 1;
         }
         if ( ! (DateTime.MinValue==AV133WWContagemResultadoExecucaoDS_43_Tfcontagemresultadoexecucao_fim_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Fim] <= @AV133WWContagemResultadoExecucaoDS_43_Tfcontagemresultadoexecucao_fim_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Fim] <= @AV133WWContagemResultadoExecucaoDS_43_Tfcontagemresultadoexecucao_fim_to)";
            }
         }
         else
         {
            GXv_int4[41] = 1;
         }
         if ( ! (0==AV134WWContagemResultadoExecucaoDS_44_Tfcontagemresultadoexecucao_dias) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Dias] >= @AV134WWContagemResultadoExecucaoDS_44_Tfcontagemresultadoexecucao_dias)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Dias] >= @AV134WWContagemResultadoExecucaoDS_44_Tfcontagemresultadoexecucao_dias)";
            }
         }
         else
         {
            GXv_int4[42] = 1;
         }
         if ( ! (0==AV135WWContagemResultadoExecucaoDS_45_Tfcontagemresultadoexecucao_dias_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Dias] <= @AV135WWContagemResultadoExecucaoDS_45_Tfcontagemresultadoexecucao_dias_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Dias] <= @AV135WWContagemResultadoExecucaoDS_45_Tfcontagemresultadoexecucao_dias_to)";
            }
         }
         else
         {
            GXv_int4[43] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + "";
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 4 :
                     return conditional_H00KI6(context, (int)dynConstraints[0] , (String)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (DateTime)dynConstraints[7] , (String)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (String)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (bool)dynConstraints[22] , (String)dynConstraints[23] , (DateTime)dynConstraints[24] , (DateTime)dynConstraints[25] , (DateTime)dynConstraints[26] , (DateTime)dynConstraints[27] , (DateTime)dynConstraints[28] , (DateTime)dynConstraints[29] , (String)dynConstraints[30] , (int)dynConstraints[31] , (int)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (DateTime)dynConstraints[35] , (DateTime)dynConstraints[36] , (DateTime)dynConstraints[37] , (DateTime)dynConstraints[38] , (short)dynConstraints[39] , (short)dynConstraints[40] , (DateTime)dynConstraints[41] , (DateTime)dynConstraints[42] , (short)dynConstraints[43] , (short)dynConstraints[44] , (int)dynConstraints[45] , (int)dynConstraints[46] , (int)dynConstraints[47] , (DateTime)dynConstraints[48] , (DateTime)dynConstraints[49] , (DateTime)dynConstraints[50] , (String)dynConstraints[51] , (String)dynConstraints[52] , (int)dynConstraints[53] , (short)dynConstraints[54] , (short)dynConstraints[55] );
               case 5 :
                     return conditional_H00KI7(context, (int)dynConstraints[0] , (String)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (DateTime)dynConstraints[7] , (String)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (String)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (bool)dynConstraints[22] , (String)dynConstraints[23] , (DateTime)dynConstraints[24] , (DateTime)dynConstraints[25] , (DateTime)dynConstraints[26] , (DateTime)dynConstraints[27] , (DateTime)dynConstraints[28] , (DateTime)dynConstraints[29] , (String)dynConstraints[30] , (int)dynConstraints[31] , (int)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (DateTime)dynConstraints[35] , (DateTime)dynConstraints[36] , (DateTime)dynConstraints[37] , (DateTime)dynConstraints[38] , (short)dynConstraints[39] , (short)dynConstraints[40] , (DateTime)dynConstraints[41] , (DateTime)dynConstraints[42] , (short)dynConstraints[43] , (short)dynConstraints[44] , (int)dynConstraints[45] , (int)dynConstraints[46] , (int)dynConstraints[47] , (DateTime)dynConstraints[48] , (DateTime)dynConstraints[49] , (DateTime)dynConstraints[50] , (String)dynConstraints[51] , (String)dynConstraints[52] , (int)dynConstraints[53] , (short)dynConstraints[54] , (short)dynConstraints[55] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00KI2 ;
          prmH00KI2 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00KI3 ;
          prmH00KI3 = new Object[] {
          new Object[] {"@AV6WWPCo_2Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00KI4 ;
          prmH00KI4 = new Object[] {
          new Object[] {"@AV6WWPCo_2Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00KI5 ;
          prmH00KI5 = new Object[] {
          new Object[] {"@AV6WWPCo_2Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00KI6 ;
          prmH00KI6 = new Object[] {
          new Object[] {"@AV6WWPCo_2Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV93WWContagemResultadoExecucaoDS_3_Contagemresultadoexecucao_inicio1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV94WWContagemResultadoExecucaoDS_4_Contagemresultadoexecucao_inicio_to1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV95WWContagemResultadoExecucaoDS_5_Contagemresultadoexecucao_fim1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV96WWContagemResultadoExecucaoDS_6_Contagemresultadoexecucao_fim_to1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV97WWContagemResultadoExecucaoDS_7_Contagemresultadoexecucao_prevista1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV98WWContagemResultadoExecucaoDS_8_Contagemresultadoexecucao_prevista_to1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV99WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV99WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV100WWContagemResultadoExecucaoDS_10_Contagemresultado_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV101WWContagemResultadoExecucaoDS_11_Contagemresultadoexecucao_oscod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV104WWContagemResultadoExecucaoDS_14_Contagemresultadoexecucao_inicio2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV105WWContagemResultadoExecucaoDS_15_Contagemresultadoexecucao_inicio_to2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV106WWContagemResultadoExecucaoDS_16_Contagemresultadoexecucao_fim2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV107WWContagemResultadoExecucaoDS_17_Contagemresultadoexecucao_fim_to2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV108WWContagemResultadoExecucaoDS_18_Contagemresultadoexecucao_prevista2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV109WWContagemResultadoExecucaoDS_19_Contagemresultadoexecucao_prevista_to2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV110WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV110WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV111WWContagemResultadoExecucaoDS_21_Contagemresultado_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV112WWContagemResultadoExecucaoDS_22_Contagemresultadoexecucao_oscod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV115WWContagemResultadoExecucaoDS_25_Contagemresultadoexecucao_inicio3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV116WWContagemResultadoExecucaoDS_26_Contagemresultadoexecucao_inicio_to3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV117WWContagemResultadoExecucaoDS_27_Contagemresultadoexecucao_fim3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV118WWContagemResultadoExecucaoDS_28_Contagemresultadoexecucao_fim_to3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV119WWContagemResultadoExecucaoDS_29_Contagemresultadoexecucao_prevista3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV120WWContagemResultadoExecucaoDS_30_Contagemresultadoexecucao_prevista_to3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV121WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV121WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV122WWContagemResultadoExecucaoDS_32_Contagemresultado_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV123WWContagemResultadoExecucaoDS_33_Contagemresultadoexecucao_oscod3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV124WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV125WWContagemResultadoExecucaoDS_35_Tfcontagemresultado_osfsosfm_sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV126WWContagemResultadoExecucaoDS_36_Tfcontagemresultadoexecucao_inicio",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV127WWContagemResultadoExecucaoDS_37_Tfcontagemresultadoexecucao_inicio_to",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV128WWContagemResultadoExecucaoDS_38_Tfcontagemresultadoexecucao_prevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV129WWContagemResultadoExecucaoDS_39_Tfcontagemresultadoexecucao_prevista_to",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV130WWContagemResultadoExecucaoDS_40_Tfcontagemresultadoexecucao_prazodias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV131WWContagemResultadoExecucaoDS_41_Tfcontagemresultadoexecucao_prazodias_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV132WWContagemResultadoExecucaoDS_42_Tfcontagemresultadoexecucao_fim",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV133WWContagemResultadoExecucaoDS_43_Tfcontagemresultadoexecucao_fim_to",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV134WWContagemResultadoExecucaoDS_44_Tfcontagemresultadoexecucao_dias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV135WWContagemResultadoExecucaoDS_45_Tfcontagemresultadoexecucao_dias_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00KI7 ;
          prmH00KI7 = new Object[] {
          new Object[] {"@AV6WWPCo_2Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV93WWContagemResultadoExecucaoDS_3_Contagemresultadoexecucao_inicio1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV94WWContagemResultadoExecucaoDS_4_Contagemresultadoexecucao_inicio_to1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV95WWContagemResultadoExecucaoDS_5_Contagemresultadoexecucao_fim1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV96WWContagemResultadoExecucaoDS_6_Contagemresultadoexecucao_fim_to1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV97WWContagemResultadoExecucaoDS_7_Contagemresultadoexecucao_prevista1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV98WWContagemResultadoExecucaoDS_8_Contagemresultadoexecucao_prevista_to1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV99WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV99WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV100WWContagemResultadoExecucaoDS_10_Contagemresultado_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV101WWContagemResultadoExecucaoDS_11_Contagemresultadoexecucao_oscod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV104WWContagemResultadoExecucaoDS_14_Contagemresultadoexecucao_inicio2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV105WWContagemResultadoExecucaoDS_15_Contagemresultadoexecucao_inicio_to2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV106WWContagemResultadoExecucaoDS_16_Contagemresultadoexecucao_fim2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV107WWContagemResultadoExecucaoDS_17_Contagemresultadoexecucao_fim_to2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV108WWContagemResultadoExecucaoDS_18_Contagemresultadoexecucao_prevista2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV109WWContagemResultadoExecucaoDS_19_Contagemresultadoexecucao_prevista_to2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV110WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV110WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV111WWContagemResultadoExecucaoDS_21_Contagemresultado_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV112WWContagemResultadoExecucaoDS_22_Contagemresultadoexecucao_oscod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV115WWContagemResultadoExecucaoDS_25_Contagemresultadoexecucao_inicio3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV116WWContagemResultadoExecucaoDS_26_Contagemresultadoexecucao_inicio_to3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV117WWContagemResultadoExecucaoDS_27_Contagemresultadoexecucao_fim3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV118WWContagemResultadoExecucaoDS_28_Contagemresultadoexecucao_fim_to3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV119WWContagemResultadoExecucaoDS_29_Contagemresultadoexecucao_prevista3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV120WWContagemResultadoExecucaoDS_30_Contagemresultadoexecucao_prevista_to3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV121WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV121WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV122WWContagemResultadoExecucaoDS_32_Contagemresultado_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV123WWContagemResultadoExecucaoDS_33_Contagemresultadoexecucao_oscod3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV124WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV125WWContagemResultadoExecucaoDS_35_Tfcontagemresultado_osfsosfm_sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV126WWContagemResultadoExecucaoDS_36_Tfcontagemresultadoexecucao_inicio",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV127WWContagemResultadoExecucaoDS_37_Tfcontagemresultadoexecucao_inicio_to",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV128WWContagemResultadoExecucaoDS_38_Tfcontagemresultadoexecucao_prevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV129WWContagemResultadoExecucaoDS_39_Tfcontagemresultadoexecucao_prevista_to",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV130WWContagemResultadoExecucaoDS_40_Tfcontagemresultadoexecucao_prazodias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV131WWContagemResultadoExecucaoDS_41_Tfcontagemresultadoexecucao_prazodias_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV132WWContagemResultadoExecucaoDS_42_Tfcontagemresultadoexecucao_fim",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV133WWContagemResultadoExecucaoDS_43_Tfcontagemresultadoexecucao_fim_to",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV134WWContagemResultadoExecucaoDS_44_Tfcontagemresultadoexecucao_dias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV135WWContagemResultadoExecucaoDS_45_Tfcontagemresultadoexecucao_dias_to",SqlDbType.SmallInt,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00KI2", "SELECT T2.[Pessoa_Codigo] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_AreaTrabalhoCod] FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) WHERE T1.[Contratada_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo ORDER BY T2.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KI2,0,0,true,false )
             ,new CursorDef("H00KI3", "SELECT T2.[Pessoa_Codigo] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_AreaTrabalhoCod] FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) WHERE ( @AV6WWPCo_2Contratada_codigo > 0 and T1.[Contratada_Codigo] = @AV6WWPCo_2Contratada_codigo) or ( (@AV6WWPCo_2Contratada_codigo = convert(int, 0)) and T1.[Contratada_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo) ORDER BY T2.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KI3,0,0,true,false )
             ,new CursorDef("H00KI4", "SELECT T2.[Pessoa_Codigo] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_AreaTrabalhoCod] FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) WHERE ( @AV6WWPCo_2Contratada_codigo > 0 and T1.[Contratada_Codigo] = @AV6WWPCo_2Contratada_codigo) or ( (@AV6WWPCo_2Contratada_codigo = convert(int, 0)) and T1.[Contratada_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo) ORDER BY T2.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KI4,0,0,true,false )
             ,new CursorDef("H00KI5", "SELECT T2.[Pessoa_Codigo] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_AreaTrabalhoCod] FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) WHERE ( @AV6WWPCo_2Contratada_codigo > 0 and T1.[Contratada_Codigo] = @AV6WWPCo_2Contratada_codigo) or ( (@AV6WWPCo_2Contratada_codigo = convert(int, 0)) and T1.[Contratada_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo) ORDER BY T2.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KI5,0,0,true,false )
             ,new CursorDef("H00KI6", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KI6,11,0,true,false )
             ,new CursorDef("H00KI7", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KI7,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((short[]) buf[4])[0] = rslt.getShort(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[6])[0] = rslt.getGXDateTime(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((short[]) buf[8])[0] = rslt.getShort(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[10])[0] = rslt.getGXDateTime(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[12])[0] = rslt.getGXDateTime(7) ;
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((String[]) buf[15])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((String[]) buf[17])[0] = rslt.getVarchar(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                return;
             case 5 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 4 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[50]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[51]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[52]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[53]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[54]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[55]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[56]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[58]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[59]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[60]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[61]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[62]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[63]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[64]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[65]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[66]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[67]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[68]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[69]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[70]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[71]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[72]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[73]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[74]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[75]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[76]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[77]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[78]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[79]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[80]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[81]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[82]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[83]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[84]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[85]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[86]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[87]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[88]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[89]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[90]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[91]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[92]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[93]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[94]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[95]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[96]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[97]);
                }
                return;
             case 5 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[46]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[47]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[48]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[49]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[50]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[51]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[54]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[55]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[56]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[57]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[58]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[59]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[60]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[61]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[64]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[65]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[66]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[67]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[68]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[69]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[70]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[71]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[72]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[73]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[74]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[75]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[76]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[77]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[78]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[79]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[80]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[81]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[82]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[83]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[84]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[85]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[86]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[87]);
                }
                return;
       }
    }

 }

}
