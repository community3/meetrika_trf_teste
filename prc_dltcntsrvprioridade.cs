/*
               File: PRC_DLTCntSrvPrioridade
        Description: Delete Cnt Srv Prioridade
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:56:15.37
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_dltcntsrvprioridade : GXProcedure
   {
      public prc_dltcntsrvprioridade( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_dltcntsrvprioridade( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_CntSrvCodigo ,
                           short aP1_Ordem )
      {
         this.AV10CntSrvCodigo = aP0_CntSrvCodigo;
         this.AV9Ordem = aP1_Ordem;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_CntSrvCodigo ,
                                 short aP1_Ordem )
      {
         prc_dltcntsrvprioridade objprc_dltcntsrvprioridade;
         objprc_dltcntsrvprioridade = new prc_dltcntsrvprioridade();
         objprc_dltcntsrvprioridade.AV10CntSrvCodigo = aP0_CntSrvCodigo;
         objprc_dltcntsrvprioridade.AV9Ordem = aP1_Ordem;
         objprc_dltcntsrvprioridade.context.SetSubmitInitialConfig(context);
         objprc_dltcntsrvprioridade.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_dltcntsrvprioridade);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_dltcntsrvprioridade)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Optimized UPDATE. */
         /* Using cursor P00X32 */
         pr_default.execute(0, new Object[] {AV10CntSrvCodigo, AV9Ordem});
         pr_default.close(0);
         dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosPrioridade") ;
         dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosPrioridade") ;
         /* End optimized UPDATE. */
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_DLTCntSrvPrioridade");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_dltcntsrvprioridade__default(),
            new Object[][] {
                new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV9Ordem ;
      private int AV10CntSrvCodigo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
   }

   public class prc_dltcntsrvprioridade__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new UpdateCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00X32 ;
          prmP00X32 = new Object[] {
          new Object[] {"@AV10CntSrvCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9Ordem",SqlDbType.SmallInt,3,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00X32", "UPDATE [ContratoServicosPrioridade] SET [ContratoServicosPrioridade_Ordem]=[ContratoServicosPrioridade_Ordem] - 1  WHERE ([ContratoServicosPrioridade_CntSrvCod] = @AV10CntSrvCodigo) AND ([ContratoServicosPrioridade_Ordem] > @AV9Ordem)", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00X32)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
       }
    }

 }

}
