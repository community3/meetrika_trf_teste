/*
               File: ContratadaContratadaUsuarioWC
        Description: Contratada Contratada Usuario WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/30/2020 21:26:36.23
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratadacontratadausuariowc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contratadacontratadausuariowc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contratadacontratadausuariowc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratadaUsuario_ContratadaCod )
      {
         this.AV32ContratadaUsuario_ContratadaCod = aP0_ContratadaUsuario_ContratadaCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         chkContratadaUsuario_UsuarioAtivo = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV32ContratadaUsuario_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32ContratadaUsuario_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32ContratadaUsuario_ContratadaCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV32ContratadaUsuario_ContratadaCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_42 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_42_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_42_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV14OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
                  AV15OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
                  AV16DynamicFiltersSelector1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
                  AV18ContratadaUsuario_UsuarioPessoaNom1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ContratadaUsuario_UsuarioPessoaNom1", AV18ContratadaUsuario_UsuarioPessoaNom1);
                  AV52TFUsuario_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52TFUsuario_Nome", AV52TFUsuario_Nome);
                  AV53TFUsuario_Nome_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53TFUsuario_Nome_Sel", AV53TFUsuario_Nome_Sel);
                  AV56TFContratadaUsuario_UsuarioPessoaNom = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56TFContratadaUsuario_UsuarioPessoaNom", AV56TFContratadaUsuario_UsuarioPessoaNom);
                  AV57TFContratadaUsuario_UsuarioPessoaNom_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57TFContratadaUsuario_UsuarioPessoaNom_Sel", AV57TFContratadaUsuario_UsuarioPessoaNom_Sel);
                  AV60TFContratadaUsuario_UsuarioPessoaDoc = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60TFContratadaUsuario_UsuarioPessoaDoc", AV60TFContratadaUsuario_UsuarioPessoaDoc);
                  AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel", AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel);
                  AV74TFContratadaUsuario_UsuarioAtivo_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV74TFContratadaUsuario_UsuarioAtivo_Sel", StringUtil.Str( (decimal)(AV74TFContratadaUsuario_UsuarioAtivo_Sel), 1, 0));
                  AV32ContratadaUsuario_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32ContratadaUsuario_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32ContratadaUsuario_ContratadaCod), 6, 0)));
                  AV54ddo_Usuario_NomeTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV54ddo_Usuario_NomeTitleControlIdToReplace", AV54ddo_Usuario_NomeTitleControlIdToReplace);
                  AV58ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV58ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace", AV58ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace);
                  AV62ddo_ContratadaUsuario_UsuarioPessoaDocTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV62ddo_ContratadaUsuario_UsuarioPessoaDocTitleControlIdToReplace", AV62ddo_ContratadaUsuario_UsuarioPessoaDocTitleControlIdToReplace);
                  AV75ddo_ContratadaUsuario_UsuarioAtivoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV75ddo_ContratadaUsuario_UsuarioAtivoTitleControlIdToReplace", AV75ddo_ContratadaUsuario_UsuarioAtivoTitleControlIdToReplace);
                  A1136ContratoGestor_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n1136ContratoGestor_ContratadaCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1136ContratoGestor_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1136ContratoGestor_ContratadaCod), 6, 0)));
                  A1013Contrato_PrepostoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n1013Contrato_PrepostoCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1013Contrato_PrepostoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1013Contrato_PrepostoCod), 6, 0)));
                  A1079ContratoGestor_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
                  A1135ContratoGestor_UsuarioEhContratante = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1135ContratoGestor_UsuarioEhContratante", A1135ContratoGestor_UsuarioEhContratante);
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
                  AV91Pgmname = GetNextPar( );
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV11GridState);
                  A66ContratadaUsuario_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A69ContratadaUsuario_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A71ContratadaUsuario_UsuarioPessoaNom = GetNextPar( );
                  n71ContratadaUsuario_UsuarioPessoaNom = false;
                  A1394ContratadaUsuario_UsuarioAtivo = (bool)(BooleanUtil.Val(GetNextPar( )));
                  n1394ContratadaUsuario_UsuarioAtivo = false;
                  A1908Usuario_DeFerias = (bool)(BooleanUtil.Val(GetNextPar( )));
                  n1908Usuario_DeFerias = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1908Usuario_DeFerias", A1908Usuario_DeFerias);
                  A341Usuario_UserGamGuid = GetNextPar( );
                  n341Usuario_UserGamGuid = false;
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV18ContratadaUsuario_UsuarioPessoaNom1, AV52TFUsuario_Nome, AV53TFUsuario_Nome_Sel, AV56TFContratadaUsuario_UsuarioPessoaNom, AV57TFContratadaUsuario_UsuarioPessoaNom_Sel, AV60TFContratadaUsuario_UsuarioPessoaDoc, AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel, AV74TFContratadaUsuario_UsuarioAtivo_Sel, AV32ContratadaUsuario_ContratadaCod, AV54ddo_Usuario_NomeTitleControlIdToReplace, AV58ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace, AV62ddo_ContratadaUsuario_UsuarioPessoaDocTitleControlIdToReplace, AV75ddo_ContratadaUsuario_UsuarioAtivoTitleControlIdToReplace, A1136ContratoGestor_ContratadaCod, A1013Contrato_PrepostoCod, A1079ContratoGestor_UsuarioCod, A1135ContratoGestor_UsuarioEhContratante, AV6WWPContext, AV91Pgmname, AV11GridState, A66ContratadaUsuario_ContratadaCod, A69ContratadaUsuario_UsuarioCod, A71ContratadaUsuario_UsuarioPessoaNom, A1394ContratadaUsuario_UsuarioAtivo, A1908Usuario_DeFerias, A341Usuario_UserGamGuid, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA8A2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV91Pgmname = "ContratadaContratadaUsuarioWC";
               context.Gx_err = 0;
               edtavGamemail_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavGamemail_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGamemail_Enabled), 5, 0)));
               edtavNotificar_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavNotificar_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNotificar_Enabled), 5, 0)));
               WS8A2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contratada Contratada Usuario WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202053021263849");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratadacontratadausuariowc.aspx") + "?" + UrlEncode("" +AV32ContratadaUsuario_ContratadaCod)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV15OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1", AV16DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTRATADAUSUARIO_USUARIOPESSOANOM1", StringUtil.RTrim( AV18ContratadaUsuario_UsuarioPessoaNom1));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFUSUARIO_NOME", StringUtil.RTrim( AV52TFUsuario_Nome));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFUSUARIO_NOME_SEL", StringUtil.RTrim( AV53TFUsuario_Nome_Sel));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATADAUSUARIO_USUARIOPESSOANOM", StringUtil.RTrim( AV56TFContratadaUsuario_UsuarioPessoaNom));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATADAUSUARIO_USUARIOPESSOANOM_SEL", StringUtil.RTrim( AV57TFContratadaUsuario_UsuarioPessoaNom_Sel));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATADAUSUARIO_USUARIOPESSOADOC", AV60TFContratadaUsuario_UsuarioPessoaDoc);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATADAUSUARIO_USUARIOPESSOADOC_SEL", AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATADAUSUARIO_USUARIOATIVO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV74TFContratadaUsuario_UsuarioAtivo_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_42", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_42), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV67DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV67DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vUSUARIO_NOMETITLEFILTERDATA", AV51Usuario_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vUSUARIO_NOMETITLEFILTERDATA", AV51Usuario_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATADAUSUARIO_USUARIOPESSOANOMTITLEFILTERDATA", AV55ContratadaUsuario_UsuarioPessoaNomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATADAUSUARIO_USUARIOPESSOANOMTITLEFILTERDATA", AV55ContratadaUsuario_UsuarioPessoaNomTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATADAUSUARIO_USUARIOPESSOADOCTITLEFILTERDATA", AV59ContratadaUsuario_UsuarioPessoaDocTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATADAUSUARIO_USUARIOPESSOADOCTITLEFILTERDATA", AV59ContratadaUsuario_UsuarioPessoaDocTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATADAUSUARIO_USUARIOATIVOTITLEFILTERDATA", AV73ContratadaUsuario_UsuarioAtivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATADAUSUARIO_USUARIOATIVOTITLEFILTERDATA", AV73ContratadaUsuario_UsuarioAtivoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV32ContratadaUsuario_ContratadaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV32ContratadaUsuario_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTRATADAUSUARIO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32ContratadaUsuario_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOGESTOR_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1136ContratoGestor_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATO_PREPOSTOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1013Contrato_PrepostoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOGESTOR_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"CONTRATOGESTOR_USUARIOEHCONTRATANTE", A1135ContratoGestor_UsuarioEhContratante);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV91Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vGRIDSTATE", AV11GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vGRIDSTATE", AV11GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"USUARIO_DEFERIAS", A1908Usuario_DeFerias);
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIO_NOME_Caption", StringUtil.RTrim( Ddo_usuario_nome_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIO_NOME_Tooltip", StringUtil.RTrim( Ddo_usuario_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIO_NOME_Cls", StringUtil.RTrim( Ddo_usuario_nome_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_usuario_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_usuario_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_usuario_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_usuario_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_usuario_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_usuario_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIO_NOME_Sortedstatus", StringUtil.RTrim( Ddo_usuario_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_usuario_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIO_NOME_Filtertype", StringUtil.RTrim( Ddo_usuario_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_usuario_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_usuario_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIO_NOME_Datalisttype", StringUtil.RTrim( Ddo_usuario_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIO_NOME_Datalistproc", StringUtil.RTrim( Ddo_usuario_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_usuario_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIO_NOME_Sortasc", StringUtil.RTrim( Ddo_usuario_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIO_NOME_Sortdsc", StringUtil.RTrim( Ddo_usuario_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIO_NOME_Loadingdata", StringUtil.RTrim( Ddo_usuario_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_usuario_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_usuario_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_usuario_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Caption", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoanom_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Tooltip", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoanom_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Cls", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoanom_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Filteredtext_set", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoanom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Selectedvalue_set", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoanom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoanom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoanom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Includesortasc", StringUtil.BoolToStr( Ddo_contratadausuario_usuariopessoanom_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Includesortdsc", StringUtil.BoolToStr( Ddo_contratadausuario_usuariopessoanom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Sortedstatus", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoanom_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Includefilter", StringUtil.BoolToStr( Ddo_contratadausuario_usuariopessoanom_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Filtertype", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoanom_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Filterisrange", StringUtil.BoolToStr( Ddo_contratadausuario_usuariopessoanom_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Includedatalist", StringUtil.BoolToStr( Ddo_contratadausuario_usuariopessoanom_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Datalisttype", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoanom_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Datalistproc", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoanom_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratadausuario_usuariopessoanom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Sortasc", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoanom_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Sortdsc", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoanom_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Loadingdata", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoanom_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Cleanfilter", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoanom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Noresultsfound", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoanom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Searchbuttontext", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoanom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Caption", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoadoc_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Tooltip", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoadoc_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Cls", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoadoc_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Filteredtext_set", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoadoc_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Selectedvalue_set", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoadoc_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoadoc_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoadoc_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Includesortasc", StringUtil.BoolToStr( Ddo_contratadausuario_usuariopessoadoc_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Includesortdsc", StringUtil.BoolToStr( Ddo_contratadausuario_usuariopessoadoc_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Sortedstatus", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoadoc_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Includefilter", StringUtil.BoolToStr( Ddo_contratadausuario_usuariopessoadoc_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Filtertype", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoadoc_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Filterisrange", StringUtil.BoolToStr( Ddo_contratadausuario_usuariopessoadoc_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Includedatalist", StringUtil.BoolToStr( Ddo_contratadausuario_usuariopessoadoc_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Datalisttype", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoadoc_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Datalistproc", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoadoc_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratadausuario_usuariopessoadoc_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Sortasc", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoadoc_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Sortdsc", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoadoc_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Loadingdata", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoadoc_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Cleanfilter", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoadoc_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Noresultsfound", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoadoc_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Searchbuttontext", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoadoc_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOATIVO_Caption", StringUtil.RTrim( Ddo_contratadausuario_usuarioativo_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOATIVO_Tooltip", StringUtil.RTrim( Ddo_contratadausuario_usuarioativo_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOATIVO_Cls", StringUtil.RTrim( Ddo_contratadausuario_usuarioativo_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOATIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_contratadausuario_usuarioativo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOATIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratadausuario_usuarioativo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOATIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratadausuario_usuarioativo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOATIVO_Includesortasc", StringUtil.BoolToStr( Ddo_contratadausuario_usuarioativo_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOATIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratadausuario_usuarioativo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOATIVO_Sortedstatus", StringUtil.RTrim( Ddo_contratadausuario_usuarioativo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOATIVO_Includefilter", StringUtil.BoolToStr( Ddo_contratadausuario_usuarioativo_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOATIVO_Includedatalist", StringUtil.BoolToStr( Ddo_contratadausuario_usuarioativo_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOATIVO_Datalisttype", StringUtil.RTrim( Ddo_contratadausuario_usuarioativo_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOATIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_contratadausuario_usuarioativo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOATIVO_Sortasc", StringUtil.RTrim( Ddo_contratadausuario_usuarioativo_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOATIVO_Sortdsc", StringUtil.RTrim( Ddo_contratadausuario_usuarioativo_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOATIVO_Cleanfilter", StringUtil.RTrim( Ddo_contratadausuario_usuarioativo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOATIVO_Searchbuttontext", StringUtil.RTrim( Ddo_contratadausuario_usuarioativo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"vWWPCONTEXT_Userid", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV6WWPContext.gxTpr_Userid), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_usuario_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_usuario_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_usuario_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Activeeventkey", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoanom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Filteredtext_get", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoanom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Selectedvalue_get", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoanom_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Activeeventkey", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoadoc_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Filteredtext_get", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoadoc_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Selectedvalue_get", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoadoc_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOATIVO_Activeeventkey", StringUtil.RTrim( Ddo_contratadausuario_usuarioativo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOATIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_contratadausuario_usuarioativo_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm8A2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contratadacontratadausuariowc.js", "?202053021264057");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContratadaContratadaUsuarioWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contratada Contratada Usuario WC" ;
      }

      protected void WB8A0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contratadacontratadausuariowc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            wb_table1_2_8A2( true) ;
         }
         else
         {
            wb_table1_2_8A2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_8A2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'" + sPrefix + "',false,'" + sGXsfl_42_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfusuario_nome_Internalname, StringUtil.RTrim( AV52TFUsuario_Nome), StringUtil.RTrim( context.localUtil.Format( AV52TFUsuario_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,63);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfusuario_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfusuario_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratadaContratadaUsuarioWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'" + sPrefix + "',false,'" + sGXsfl_42_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfusuario_nome_sel_Internalname, StringUtil.RTrim( AV53TFUsuario_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV53TFUsuario_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,64);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfusuario_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfusuario_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratadaContratadaUsuarioWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'" + sPrefix + "',false,'" + sGXsfl_42_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratadausuario_usuariopessoanom_Internalname, StringUtil.RTrim( AV56TFContratadaUsuario_UsuarioPessoaNom), StringUtil.RTrim( context.localUtil.Format( AV56TFContratadaUsuario_UsuarioPessoaNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,65);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratadausuario_usuariopessoanom_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontratadausuario_usuariopessoanom_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratadaContratadaUsuarioWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'" + sPrefix + "',false,'" + sGXsfl_42_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratadausuario_usuariopessoanom_sel_Internalname, StringUtil.RTrim( AV57TFContratadaUsuario_UsuarioPessoaNom_Sel), StringUtil.RTrim( context.localUtil.Format( AV57TFContratadaUsuario_UsuarioPessoaNom_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,66);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratadausuario_usuariopessoanom_sel_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontratadausuario_usuariopessoanom_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratadaContratadaUsuarioWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'" + sPrefix + "',false,'" + sGXsfl_42_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratadausuario_usuariopessoadoc_Internalname, AV60TFContratadaUsuario_UsuarioPessoaDoc, StringUtil.RTrim( context.localUtil.Format( AV60TFContratadaUsuario_UsuarioPessoaDoc, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,67);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratadausuario_usuariopessoadoc_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratadausuario_usuariopessoadoc_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratadaContratadaUsuarioWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'" + sPrefix + "',false,'" + sGXsfl_42_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratadausuario_usuariopessoadoc_sel_Internalname, AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel, StringUtil.RTrim( context.localUtil.Format( AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,68);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratadausuario_usuariopessoadoc_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratadausuario_usuariopessoadoc_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratadaContratadaUsuarioWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'" + sPrefix + "',false,'" + sGXsfl_42_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratadausuario_usuarioativo_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV74TFContratadaUsuario_UsuarioAtivo_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV74TFContratadaUsuario_UsuarioAtivo_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,69);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratadausuario_usuarioativo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratadausuario_usuarioativo_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratadaContratadaUsuarioWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_USUARIO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'" + sPrefix + "',false,'" + sGXsfl_42_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_usuario_nometitlecontrolidtoreplace_Internalname, AV54ddo_Usuario_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,71);\"", 0, edtavDdo_usuario_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratadaContratadaUsuarioWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'" + sPrefix + "',false,'" + sGXsfl_42_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratadausuario_usuariopessoanomtitlecontrolidtoreplace_Internalname, AV58ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,73);\"", 0, edtavDdo_contratadausuario_usuariopessoanomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratadaContratadaUsuarioWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOCContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'" + sPrefix + "',false,'" + sGXsfl_42_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratadausuario_usuariopessoadoctitlecontrolidtoreplace_Internalname, AV62ddo_ContratadaUsuario_UsuarioPessoaDocTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,75);\"", 0, edtavDdo_contratadausuario_usuariopessoadoctitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratadaContratadaUsuarioWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOATIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'" + sPrefix + "',false,'" + sGXsfl_42_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratadausuario_usuarioativotitlecontrolidtoreplace_Internalname, AV75ddo_ContratadaUsuario_UsuarioAtivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,77);\"", 0, edtavDdo_contratadausuario_usuarioativotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratadaContratadaUsuarioWC.htm");
         }
         wbLoad = true;
      }

      protected void START8A2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contratada Contratada Usuario WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP8A0( ) ;
            }
         }
      }

      protected void WS8A2( )
      {
         START8A2( ) ;
         EVT8A2( ) ;
      }

      protected void EVT8A2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP8A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_USUARIO_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP8A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E118A2 */
                                    E118A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP8A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E128A2 */
                                    E128A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP8A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E138A2 */
                                    E138A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATADAUSUARIO_USUARIOATIVO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP8A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E148A2 */
                                    E148A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP8A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E158A2 */
                                    E158A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP8A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E168A2 */
                                    E168A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP8A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E178A2 */
                                    E178A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOIMPORTAR'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP8A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E188A2 */
                                    E188A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP8A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E198A2 */
                                    E198A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP8A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavGamemail_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP8A0( ) ;
                              }
                              sEvt = cgiGet( sPrefix+"GRIDPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgrid_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgrid_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgrid_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgrid_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 16), "VATUALIZAR.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 18), "USUARIO_NOME.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 13), "VFERIAS.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 16), "VATUALIZAR.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 18), "USUARIO_NOME.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 13), "VFERIAS.CLICK") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP8A0( ) ;
                              }
                              nGXsfl_42_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_42_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_42_idx), 4, 0)), 4, "0");
                              SubsflControlProps_422( ) ;
                              AV45Atualizar = cgiGet( edtavAtualizar_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAtualizar_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV45Atualizar)) ? AV84Atualizar_GXI : context.convertURL( context.PathToRelativeUrl( AV45Atualizar))));
                              AV29Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV85Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV29Delete))));
                              A66ContratadaUsuario_ContratadaCod = (int)(context.localUtil.CToN( cgiGet( edtContratadaUsuario_ContratadaCod_Internalname), ",", "."));
                              A69ContratadaUsuario_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( edtContratadaUsuario_UsuarioCod_Internalname), ",", "."));
                              A70ContratadaUsuario_UsuarioPessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContratadaUsuario_UsuarioPessoaCod_Internalname), ",", "."));
                              n70ContratadaUsuario_UsuarioPessoaCod = false;
                              A341Usuario_UserGamGuid = cgiGet( edtUsuario_UserGamGuid_Internalname);
                              n341Usuario_UserGamGuid = false;
                              A2Usuario_Nome = StringUtil.Upper( cgiGet( edtUsuario_Nome_Internalname));
                              n2Usuario_Nome = false;
                              A71ContratadaUsuario_UsuarioPessoaNom = StringUtil.Upper( cgiGet( edtContratadaUsuario_UsuarioPessoaNom_Internalname));
                              n71ContratadaUsuario_UsuarioPessoaNom = false;
                              A491ContratadaUsuario_UsuarioPessoaDoc = cgiGet( edtContratadaUsuario_UsuarioPessoaDoc_Internalname);
                              n491ContratadaUsuario_UsuarioPessoaDoc = false;
                              AV43GAMEMail = cgiGet( edtavGamemail_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavGamemail_Internalname, AV43GAMEMail);
                              AV80Notificar = cgiGet( edtavNotificar_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavNotificar_Internalname, AV80Notificar);
                              A1394ContratadaUsuario_UsuarioAtivo = StringUtil.StrToBool( cgiGet( chkContratadaUsuario_UsuarioAtivo_Internalname));
                              n1394ContratadaUsuario_UsuarioAtivo = false;
                              AV68Ferias = cgiGet( edtavFerias_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFerias_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV68Ferias)) ? AV86Ferias_GXI : context.convertURL( context.PathToRelativeUrl( AV68Ferias))));
                              AV33AssociarPerfil = cgiGet( edtavAssociarperfil_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAssociarperfil_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV33AssociarPerfil)) ? AV87Associarperfil_GXI : context.convertURL( context.PathToRelativeUrl( AV33AssociarPerfil))));
                              AV42AssociarServico = cgiGet( edtavAssociarservico_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAssociarservico_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV42AssociarServico)) ? AV88Associarservico_GXI : context.convertURL( context.PathToRelativeUrl( AV42AssociarServico))));
                              AV41Config = cgiGet( edtavConfig_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavConfig_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV41Config)) ? AV89Config_GXI : context.convertURL( context.PathToRelativeUrl( AV41Config))));
                              AV76ClonarDados = cgiGet( edtavClonardados_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavClonardados_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV76ClonarDados)) ? AV90Clonardados_GXI : context.convertURL( context.PathToRelativeUrl( AV76ClonarDados))));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavGamemail_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E208A2 */
                                          E208A2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavGamemail_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E218A2 */
                                          E218A2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavGamemail_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E228A2 */
                                          E228A2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VATUALIZAR.CLICK") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavGamemail_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E238A2 */
                                          E238A2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "USUARIO_NOME.CLICK") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavGamemail_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E248A2 */
                                          E248A2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VFERIAS.CLICK") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavGamemail_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E258A2 */
                                          E258A2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contratadausuario_usuariopessoanom1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATADAUSUARIO_USUARIOPESSOANOM1"), AV18ContratadaUsuario_UsuarioPessoaNom1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfusuario_nome Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFUSUARIO_NOME"), AV52TFUsuario_Nome) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfusuario_nome_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFUSUARIO_NOME_SEL"), AV53TFUsuario_Nome_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratadausuario_usuariopessoanom Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATADAUSUARIO_USUARIOPESSOANOM"), AV56TFContratadaUsuario_UsuarioPessoaNom) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratadausuario_usuariopessoanom_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATADAUSUARIO_USUARIOPESSOANOM_SEL"), AV57TFContratadaUsuario_UsuarioPessoaNom_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratadausuario_usuariopessoadoc Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATADAUSUARIO_USUARIOPESSOADOC"), AV60TFContratadaUsuario_UsuarioPessoaDoc) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratadausuario_usuariopessoadoc_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATADAUSUARIO_USUARIOPESSOADOC_SEL"), AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratadausuario_usuarioativo_sel Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATADAUSUARIO_USUARIOATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV74TFContratadaUsuario_UsuarioAtivo_Sel )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavGamemail_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUP8A0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavGamemail_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE8A2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm8A2( ) ;
            }
         }
      }

      protected void PA8A2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTRATADAUSUARIO_USUARIOPESSOANOM", "Pessoa", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            }
            GXCCtl = "CONTRATADAUSUARIO_USUARIOATIVO_" + sGXsfl_42_idx;
            chkContratadaUsuario_UsuarioAtivo.Name = GXCCtl;
            chkContratadaUsuario_UsuarioAtivo.WebTags = "";
            chkContratadaUsuario_UsuarioAtivo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkContratadaUsuario_UsuarioAtivo_Internalname, "TitleCaption", chkContratadaUsuario_UsuarioAtivo.Caption);
            chkContratadaUsuario_UsuarioAtivo.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_422( ) ;
         while ( nGXsfl_42_idx <= nRC_GXsfl_42 )
         {
            sendrow_422( ) ;
            nGXsfl_42_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_42_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_42_idx+1));
            sGXsfl_42_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_42_idx), 4, 0)), 4, "0");
            SubsflControlProps_422( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV14OrderedBy ,
                                       bool AV15OrderedDsc ,
                                       String AV16DynamicFiltersSelector1 ,
                                       String AV18ContratadaUsuario_UsuarioPessoaNom1 ,
                                       String AV52TFUsuario_Nome ,
                                       String AV53TFUsuario_Nome_Sel ,
                                       String AV56TFContratadaUsuario_UsuarioPessoaNom ,
                                       String AV57TFContratadaUsuario_UsuarioPessoaNom_Sel ,
                                       String AV60TFContratadaUsuario_UsuarioPessoaDoc ,
                                       String AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel ,
                                       short AV74TFContratadaUsuario_UsuarioAtivo_Sel ,
                                       int AV32ContratadaUsuario_ContratadaCod ,
                                       String AV54ddo_Usuario_NomeTitleControlIdToReplace ,
                                       String AV58ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace ,
                                       String AV62ddo_ContratadaUsuario_UsuarioPessoaDocTitleControlIdToReplace ,
                                       String AV75ddo_ContratadaUsuario_UsuarioAtivoTitleControlIdToReplace ,
                                       int A1136ContratoGestor_ContratadaCod ,
                                       int A1013Contrato_PrepostoCod ,
                                       int A1079ContratoGestor_UsuarioCod ,
                                       bool A1135ContratoGestor_UsuarioEhContratante ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       String AV91Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV11GridState ,
                                       int A66ContratadaUsuario_ContratadaCod ,
                                       int A69ContratadaUsuario_UsuarioCod ,
                                       String A71ContratadaUsuario_UsuarioPessoaNom ,
                                       bool A1394ContratadaUsuario_UsuarioAtivo ,
                                       bool A1908Usuario_DeFerias ,
                                       String A341Usuario_UserGamGuid ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF8A2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADAUSUARIO_USUARIOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A69ContratadaUsuario_UsuarioCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATADAUSUARIO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF8A2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV91Pgmname = "ContratadaContratadaUsuarioWC";
         context.Gx_err = 0;
         edtavGamemail_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavGamemail_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGamemail_Enabled), 5, 0)));
         edtavNotificar_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavNotificar_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNotificar_Enabled), 5, 0)));
      }

      protected void RF8A2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 42;
         /* Execute user event: E218A2 */
         E218A2 ();
         nGXsfl_42_idx = 1;
         sGXsfl_42_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_42_idx), 4, 0)), 4, "0");
         SubsflControlProps_422( ) ;
         nGXsfl_42_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_422( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV16DynamicFiltersSelector1 ,
                                                 AV18ContratadaUsuario_UsuarioPessoaNom1 ,
                                                 AV53TFUsuario_Nome_Sel ,
                                                 AV52TFUsuario_Nome ,
                                                 AV57TFContratadaUsuario_UsuarioPessoaNom_Sel ,
                                                 AV56TFContratadaUsuario_UsuarioPessoaNom ,
                                                 AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel ,
                                                 AV60TFContratadaUsuario_UsuarioPessoaDoc ,
                                                 AV74TFContratadaUsuario_UsuarioAtivo_Sel ,
                                                 A71ContratadaUsuario_UsuarioPessoaNom ,
                                                 A2Usuario_Nome ,
                                                 A491ContratadaUsuario_UsuarioPessoaDoc ,
                                                 A1394ContratadaUsuario_UsuarioAtivo ,
                                                 AV14OrderedBy ,
                                                 AV15OrderedDsc ,
                                                 A66ContratadaUsuario_ContratadaCod ,
                                                 AV32ContratadaUsuario_ContratadaCod },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                                 TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT,
                                                 TypeConstants.INT
                                                 }
            });
            lV18ContratadaUsuario_UsuarioPessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV18ContratadaUsuario_UsuarioPessoaNom1), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ContratadaUsuario_UsuarioPessoaNom1", AV18ContratadaUsuario_UsuarioPessoaNom1);
            lV52TFUsuario_Nome = StringUtil.PadR( StringUtil.RTrim( AV52TFUsuario_Nome), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52TFUsuario_Nome", AV52TFUsuario_Nome);
            lV56TFContratadaUsuario_UsuarioPessoaNom = StringUtil.PadR( StringUtil.RTrim( AV56TFContratadaUsuario_UsuarioPessoaNom), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56TFContratadaUsuario_UsuarioPessoaNom", AV56TFContratadaUsuario_UsuarioPessoaNom);
            lV60TFContratadaUsuario_UsuarioPessoaDoc = StringUtil.Concat( StringUtil.RTrim( AV60TFContratadaUsuario_UsuarioPessoaDoc), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60TFContratadaUsuario_UsuarioPessoaDoc", AV60TFContratadaUsuario_UsuarioPessoaDoc);
            /* Using cursor H008A3 */
            pr_default.execute(0, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, AV32ContratadaUsuario_ContratadaCod, lV18ContratadaUsuario_UsuarioPessoaNom1, lV52TFUsuario_Nome, AV53TFUsuario_Nome_Sel, lV56TFContratadaUsuario_UsuarioPessoaNom, AV57TFContratadaUsuario_UsuarioPessoaNom_Sel, lV60TFContratadaUsuario_UsuarioPessoaDoc, AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_42_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1908Usuario_DeFerias = H008A3_A1908Usuario_DeFerias[0];
               n1908Usuario_DeFerias = H008A3_n1908Usuario_DeFerias[0];
               A1394ContratadaUsuario_UsuarioAtivo = H008A3_A1394ContratadaUsuario_UsuarioAtivo[0];
               n1394ContratadaUsuario_UsuarioAtivo = H008A3_n1394ContratadaUsuario_UsuarioAtivo[0];
               A491ContratadaUsuario_UsuarioPessoaDoc = H008A3_A491ContratadaUsuario_UsuarioPessoaDoc[0];
               n491ContratadaUsuario_UsuarioPessoaDoc = H008A3_n491ContratadaUsuario_UsuarioPessoaDoc[0];
               A71ContratadaUsuario_UsuarioPessoaNom = H008A3_A71ContratadaUsuario_UsuarioPessoaNom[0];
               n71ContratadaUsuario_UsuarioPessoaNom = H008A3_n71ContratadaUsuario_UsuarioPessoaNom[0];
               A2Usuario_Nome = H008A3_A2Usuario_Nome[0];
               n2Usuario_Nome = H008A3_n2Usuario_Nome[0];
               A341Usuario_UserGamGuid = H008A3_A341Usuario_UserGamGuid[0];
               n341Usuario_UserGamGuid = H008A3_n341Usuario_UserGamGuid[0];
               A70ContratadaUsuario_UsuarioPessoaCod = H008A3_A70ContratadaUsuario_UsuarioPessoaCod[0];
               n70ContratadaUsuario_UsuarioPessoaCod = H008A3_n70ContratadaUsuario_UsuarioPessoaCod[0];
               A69ContratadaUsuario_UsuarioCod = H008A3_A69ContratadaUsuario_UsuarioCod[0];
               A66ContratadaUsuario_ContratadaCod = H008A3_A66ContratadaUsuario_ContratadaCod[0];
               A40000UsuarioNotifica_NoStatus = H008A3_A40000UsuarioNotifica_NoStatus[0];
               n40000UsuarioNotifica_NoStatus = H008A3_n40000UsuarioNotifica_NoStatus[0];
               A1908Usuario_DeFerias = H008A3_A1908Usuario_DeFerias[0];
               n1908Usuario_DeFerias = H008A3_n1908Usuario_DeFerias[0];
               A1394ContratadaUsuario_UsuarioAtivo = H008A3_A1394ContratadaUsuario_UsuarioAtivo[0];
               n1394ContratadaUsuario_UsuarioAtivo = H008A3_n1394ContratadaUsuario_UsuarioAtivo[0];
               A2Usuario_Nome = H008A3_A2Usuario_Nome[0];
               n2Usuario_Nome = H008A3_n2Usuario_Nome[0];
               A341Usuario_UserGamGuid = H008A3_A341Usuario_UserGamGuid[0];
               n341Usuario_UserGamGuid = H008A3_n341Usuario_UserGamGuid[0];
               A70ContratadaUsuario_UsuarioPessoaCod = H008A3_A70ContratadaUsuario_UsuarioPessoaCod[0];
               n70ContratadaUsuario_UsuarioPessoaCod = H008A3_n70ContratadaUsuario_UsuarioPessoaCod[0];
               A491ContratadaUsuario_UsuarioPessoaDoc = H008A3_A491ContratadaUsuario_UsuarioPessoaDoc[0];
               n491ContratadaUsuario_UsuarioPessoaDoc = H008A3_n491ContratadaUsuario_UsuarioPessoaDoc[0];
               A71ContratadaUsuario_UsuarioPessoaNom = H008A3_A71ContratadaUsuario_UsuarioPessoaNom[0];
               n71ContratadaUsuario_UsuarioPessoaNom = H008A3_n71ContratadaUsuario_UsuarioPessoaNom[0];
               A40000UsuarioNotifica_NoStatus = H008A3_A40000UsuarioNotifica_NoStatus[0];
               n40000UsuarioNotifica_NoStatus = H008A3_n40000UsuarioNotifica_NoStatus[0];
               /* Execute user event: E228A2 */
               E228A2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 42;
            WB8A0( ) ;
         }
         nGXsfl_42_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV16DynamicFiltersSelector1 ,
                                              AV18ContratadaUsuario_UsuarioPessoaNom1 ,
                                              AV53TFUsuario_Nome_Sel ,
                                              AV52TFUsuario_Nome ,
                                              AV57TFContratadaUsuario_UsuarioPessoaNom_Sel ,
                                              AV56TFContratadaUsuario_UsuarioPessoaNom ,
                                              AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel ,
                                              AV60TFContratadaUsuario_UsuarioPessoaDoc ,
                                              AV74TFContratadaUsuario_UsuarioAtivo_Sel ,
                                              A71ContratadaUsuario_UsuarioPessoaNom ,
                                              A2Usuario_Nome ,
                                              A491ContratadaUsuario_UsuarioPessoaDoc ,
                                              A1394ContratadaUsuario_UsuarioAtivo ,
                                              AV14OrderedBy ,
                                              AV15OrderedDsc ,
                                              A66ContratadaUsuario_ContratadaCod ,
                                              AV32ContratadaUsuario_ContratadaCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.INT
                                              }
         });
         lV18ContratadaUsuario_UsuarioPessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV18ContratadaUsuario_UsuarioPessoaNom1), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ContratadaUsuario_UsuarioPessoaNom1", AV18ContratadaUsuario_UsuarioPessoaNom1);
         lV52TFUsuario_Nome = StringUtil.PadR( StringUtil.RTrim( AV52TFUsuario_Nome), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52TFUsuario_Nome", AV52TFUsuario_Nome);
         lV56TFContratadaUsuario_UsuarioPessoaNom = StringUtil.PadR( StringUtil.RTrim( AV56TFContratadaUsuario_UsuarioPessoaNom), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56TFContratadaUsuario_UsuarioPessoaNom", AV56TFContratadaUsuario_UsuarioPessoaNom);
         lV60TFContratadaUsuario_UsuarioPessoaDoc = StringUtil.Concat( StringUtil.RTrim( AV60TFContratadaUsuario_UsuarioPessoaDoc), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60TFContratadaUsuario_UsuarioPessoaDoc", AV60TFContratadaUsuario_UsuarioPessoaDoc);
         /* Using cursor H008A5 */
         pr_default.execute(1, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, AV32ContratadaUsuario_ContratadaCod, lV18ContratadaUsuario_UsuarioPessoaNom1, lV52TFUsuario_Nome, AV53TFUsuario_Nome_Sel, lV56TFContratadaUsuario_UsuarioPessoaNom, AV57TFContratadaUsuario_UsuarioPessoaNom_Sel, lV60TFContratadaUsuario_UsuarioPessoaDoc, AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel});
         GRID_nRecordCount = H008A5_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV18ContratadaUsuario_UsuarioPessoaNom1, AV52TFUsuario_Nome, AV53TFUsuario_Nome_Sel, AV56TFContratadaUsuario_UsuarioPessoaNom, AV57TFContratadaUsuario_UsuarioPessoaNom_Sel, AV60TFContratadaUsuario_UsuarioPessoaDoc, AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel, AV74TFContratadaUsuario_UsuarioAtivo_Sel, AV32ContratadaUsuario_ContratadaCod, AV54ddo_Usuario_NomeTitleControlIdToReplace, AV58ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace, AV62ddo_ContratadaUsuario_UsuarioPessoaDocTitleControlIdToReplace, AV75ddo_ContratadaUsuario_UsuarioAtivoTitleControlIdToReplace, A1136ContratoGestor_ContratadaCod, A1013Contrato_PrepostoCod, A1079ContratoGestor_UsuarioCod, A1135ContratoGestor_UsuarioEhContratante, AV6WWPContext, AV91Pgmname, AV11GridState, A66ContratadaUsuario_ContratadaCod, A69ContratadaUsuario_UsuarioCod, A71ContratadaUsuario_UsuarioPessoaNom, A1394ContratadaUsuario_UsuarioAtivo, A1908Usuario_DeFerias, A341Usuario_UserGamGuid, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV18ContratadaUsuario_UsuarioPessoaNom1, AV52TFUsuario_Nome, AV53TFUsuario_Nome_Sel, AV56TFContratadaUsuario_UsuarioPessoaNom, AV57TFContratadaUsuario_UsuarioPessoaNom_Sel, AV60TFContratadaUsuario_UsuarioPessoaDoc, AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel, AV74TFContratadaUsuario_UsuarioAtivo_Sel, AV32ContratadaUsuario_ContratadaCod, AV54ddo_Usuario_NomeTitleControlIdToReplace, AV58ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace, AV62ddo_ContratadaUsuario_UsuarioPessoaDocTitleControlIdToReplace, AV75ddo_ContratadaUsuario_UsuarioAtivoTitleControlIdToReplace, A1136ContratoGestor_ContratadaCod, A1013Contrato_PrepostoCod, A1079ContratoGestor_UsuarioCod, A1135ContratoGestor_UsuarioEhContratante, AV6WWPContext, AV91Pgmname, AV11GridState, A66ContratadaUsuario_ContratadaCod, A69ContratadaUsuario_UsuarioCod, A71ContratadaUsuario_UsuarioPessoaNom, A1394ContratadaUsuario_UsuarioAtivo, A1908Usuario_DeFerias, A341Usuario_UserGamGuid, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV18ContratadaUsuario_UsuarioPessoaNom1, AV52TFUsuario_Nome, AV53TFUsuario_Nome_Sel, AV56TFContratadaUsuario_UsuarioPessoaNom, AV57TFContratadaUsuario_UsuarioPessoaNom_Sel, AV60TFContratadaUsuario_UsuarioPessoaDoc, AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel, AV74TFContratadaUsuario_UsuarioAtivo_Sel, AV32ContratadaUsuario_ContratadaCod, AV54ddo_Usuario_NomeTitleControlIdToReplace, AV58ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace, AV62ddo_ContratadaUsuario_UsuarioPessoaDocTitleControlIdToReplace, AV75ddo_ContratadaUsuario_UsuarioAtivoTitleControlIdToReplace, A1136ContratoGestor_ContratadaCod, A1013Contrato_PrepostoCod, A1079ContratoGestor_UsuarioCod, A1135ContratoGestor_UsuarioEhContratante, AV6WWPContext, AV91Pgmname, AV11GridState, A66ContratadaUsuario_ContratadaCod, A69ContratadaUsuario_UsuarioCod, A71ContratadaUsuario_UsuarioPessoaNom, A1394ContratadaUsuario_UsuarioAtivo, A1908Usuario_DeFerias, A341Usuario_UserGamGuid, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV18ContratadaUsuario_UsuarioPessoaNom1, AV52TFUsuario_Nome, AV53TFUsuario_Nome_Sel, AV56TFContratadaUsuario_UsuarioPessoaNom, AV57TFContratadaUsuario_UsuarioPessoaNom_Sel, AV60TFContratadaUsuario_UsuarioPessoaDoc, AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel, AV74TFContratadaUsuario_UsuarioAtivo_Sel, AV32ContratadaUsuario_ContratadaCod, AV54ddo_Usuario_NomeTitleControlIdToReplace, AV58ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace, AV62ddo_ContratadaUsuario_UsuarioPessoaDocTitleControlIdToReplace, AV75ddo_ContratadaUsuario_UsuarioAtivoTitleControlIdToReplace, A1136ContratoGestor_ContratadaCod, A1013Contrato_PrepostoCod, A1079ContratoGestor_UsuarioCod, A1135ContratoGestor_UsuarioEhContratante, AV6WWPContext, AV91Pgmname, AV11GridState, A66ContratadaUsuario_ContratadaCod, A69ContratadaUsuario_UsuarioCod, A71ContratadaUsuario_UsuarioPessoaNom, A1394ContratadaUsuario_UsuarioAtivo, A1908Usuario_DeFerias, A341Usuario_UserGamGuid, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV18ContratadaUsuario_UsuarioPessoaNom1, AV52TFUsuario_Nome, AV53TFUsuario_Nome_Sel, AV56TFContratadaUsuario_UsuarioPessoaNom, AV57TFContratadaUsuario_UsuarioPessoaNom_Sel, AV60TFContratadaUsuario_UsuarioPessoaDoc, AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel, AV74TFContratadaUsuario_UsuarioAtivo_Sel, AV32ContratadaUsuario_ContratadaCod, AV54ddo_Usuario_NomeTitleControlIdToReplace, AV58ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace, AV62ddo_ContratadaUsuario_UsuarioPessoaDocTitleControlIdToReplace, AV75ddo_ContratadaUsuario_UsuarioAtivoTitleControlIdToReplace, A1136ContratoGestor_ContratadaCod, A1013Contrato_PrepostoCod, A1079ContratoGestor_UsuarioCod, A1135ContratoGestor_UsuarioEhContratante, AV6WWPContext, AV91Pgmname, AV11GridState, A66ContratadaUsuario_ContratadaCod, A69ContratadaUsuario_UsuarioCod, A71ContratadaUsuario_UsuarioPessoaNom, A1394ContratadaUsuario_UsuarioAtivo, A1908Usuario_DeFerias, A341Usuario_UserGamGuid, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUP8A0( )
      {
         /* Before Start, stand alone formulas. */
         AV91Pgmname = "ContratadaContratadaUsuarioWC";
         context.Gx_err = 0;
         edtavGamemail_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavGamemail_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGamemail_Enabled), 5, 0)));
         edtavNotificar_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavNotificar_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNotificar_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E208A2 */
         E208A2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV67DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vUSUARIO_NOMETITLEFILTERDATA"), AV51Usuario_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATADAUSUARIO_USUARIOPESSOANOMTITLEFILTERDATA"), AV55ContratadaUsuario_UsuarioPessoaNomTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATADAUSUARIO_USUARIOPESSOADOCTITLEFILTERDATA"), AV59ContratadaUsuario_UsuarioPessoaDocTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATADAUSUARIO_USUARIOATIVOTITLEFILTERDATA"), AV73ContratadaUsuario_UsuarioAtivoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV14OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV16DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            AV18ContratadaUsuario_UsuarioPessoaNom1 = StringUtil.Upper( cgiGet( edtavContratadausuario_usuariopessoanom1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ContratadaUsuario_UsuarioPessoaNom1", AV18ContratadaUsuario_UsuarioPessoaNom1);
            AV52TFUsuario_Nome = StringUtil.Upper( cgiGet( edtavTfusuario_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52TFUsuario_Nome", AV52TFUsuario_Nome);
            AV53TFUsuario_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfusuario_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53TFUsuario_Nome_Sel", AV53TFUsuario_Nome_Sel);
            AV56TFContratadaUsuario_UsuarioPessoaNom = StringUtil.Upper( cgiGet( edtavTfcontratadausuario_usuariopessoanom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56TFContratadaUsuario_UsuarioPessoaNom", AV56TFContratadaUsuario_UsuarioPessoaNom);
            AV57TFContratadaUsuario_UsuarioPessoaNom_Sel = StringUtil.Upper( cgiGet( edtavTfcontratadausuario_usuariopessoanom_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57TFContratadaUsuario_UsuarioPessoaNom_Sel", AV57TFContratadaUsuario_UsuarioPessoaNom_Sel);
            AV60TFContratadaUsuario_UsuarioPessoaDoc = cgiGet( edtavTfcontratadausuario_usuariopessoadoc_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60TFContratadaUsuario_UsuarioPessoaDoc", AV60TFContratadaUsuario_UsuarioPessoaDoc);
            AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel = cgiGet( edtavTfcontratadausuario_usuariopessoadoc_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel", AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratadausuario_usuarioativo_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratadausuario_usuarioativo_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATADAUSUARIO_USUARIOATIVO_SEL");
               GX_FocusControl = edtavTfcontratadausuario_usuarioativo_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV74TFContratadaUsuario_UsuarioAtivo_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV74TFContratadaUsuario_UsuarioAtivo_Sel", StringUtil.Str( (decimal)(AV74TFContratadaUsuario_UsuarioAtivo_Sel), 1, 0));
            }
            else
            {
               AV74TFContratadaUsuario_UsuarioAtivo_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratadausuario_usuarioativo_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV74TFContratadaUsuario_UsuarioAtivo_Sel", StringUtil.Str( (decimal)(AV74TFContratadaUsuario_UsuarioAtivo_Sel), 1, 0));
            }
            AV54ddo_Usuario_NomeTitleControlIdToReplace = cgiGet( edtavDdo_usuario_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV54ddo_Usuario_NomeTitleControlIdToReplace", AV54ddo_Usuario_NomeTitleControlIdToReplace);
            AV58ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace = cgiGet( edtavDdo_contratadausuario_usuariopessoanomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV58ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace", AV58ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace);
            AV62ddo_ContratadaUsuario_UsuarioPessoaDocTitleControlIdToReplace = cgiGet( edtavDdo_contratadausuario_usuariopessoadoctitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV62ddo_ContratadaUsuario_UsuarioPessoaDocTitleControlIdToReplace", AV62ddo_ContratadaUsuario_UsuarioPessoaDocTitleControlIdToReplace);
            AV75ddo_ContratadaUsuario_UsuarioAtivoTitleControlIdToReplace = cgiGet( edtavDdo_contratadausuario_usuarioativotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV75ddo_ContratadaUsuario_UsuarioAtivoTitleControlIdToReplace", AV75ddo_ContratadaUsuario_UsuarioAtivoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_42 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_42"), ",", "."));
            wcpOAV32ContratadaUsuario_ContratadaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV32ContratadaUsuario_ContratadaCod"), ",", "."));
            AV32ContratadaUsuario_ContratadaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"vCONTRATADAUSUARIO_CONTRATADACOD"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Ddo_usuario_nome_Caption = cgiGet( sPrefix+"DDO_USUARIO_NOME_Caption");
            Ddo_usuario_nome_Tooltip = cgiGet( sPrefix+"DDO_USUARIO_NOME_Tooltip");
            Ddo_usuario_nome_Cls = cgiGet( sPrefix+"DDO_USUARIO_NOME_Cls");
            Ddo_usuario_nome_Filteredtext_set = cgiGet( sPrefix+"DDO_USUARIO_NOME_Filteredtext_set");
            Ddo_usuario_nome_Selectedvalue_set = cgiGet( sPrefix+"DDO_USUARIO_NOME_Selectedvalue_set");
            Ddo_usuario_nome_Dropdownoptionstype = cgiGet( sPrefix+"DDO_USUARIO_NOME_Dropdownoptionstype");
            Ddo_usuario_nome_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_USUARIO_NOME_Titlecontrolidtoreplace");
            Ddo_usuario_nome_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_USUARIO_NOME_Includesortasc"));
            Ddo_usuario_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_USUARIO_NOME_Includesortdsc"));
            Ddo_usuario_nome_Sortedstatus = cgiGet( sPrefix+"DDO_USUARIO_NOME_Sortedstatus");
            Ddo_usuario_nome_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_USUARIO_NOME_Includefilter"));
            Ddo_usuario_nome_Filtertype = cgiGet( sPrefix+"DDO_USUARIO_NOME_Filtertype");
            Ddo_usuario_nome_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_USUARIO_NOME_Filterisrange"));
            Ddo_usuario_nome_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_USUARIO_NOME_Includedatalist"));
            Ddo_usuario_nome_Datalisttype = cgiGet( sPrefix+"DDO_USUARIO_NOME_Datalisttype");
            Ddo_usuario_nome_Datalistproc = cgiGet( sPrefix+"DDO_USUARIO_NOME_Datalistproc");
            Ddo_usuario_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_USUARIO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_usuario_nome_Sortasc = cgiGet( sPrefix+"DDO_USUARIO_NOME_Sortasc");
            Ddo_usuario_nome_Sortdsc = cgiGet( sPrefix+"DDO_USUARIO_NOME_Sortdsc");
            Ddo_usuario_nome_Loadingdata = cgiGet( sPrefix+"DDO_USUARIO_NOME_Loadingdata");
            Ddo_usuario_nome_Cleanfilter = cgiGet( sPrefix+"DDO_USUARIO_NOME_Cleanfilter");
            Ddo_usuario_nome_Noresultsfound = cgiGet( sPrefix+"DDO_USUARIO_NOME_Noresultsfound");
            Ddo_usuario_nome_Searchbuttontext = cgiGet( sPrefix+"DDO_USUARIO_NOME_Searchbuttontext");
            Ddo_contratadausuario_usuariopessoanom_Caption = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Caption");
            Ddo_contratadausuario_usuariopessoanom_Tooltip = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Tooltip");
            Ddo_contratadausuario_usuariopessoanom_Cls = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Cls");
            Ddo_contratadausuario_usuariopessoanom_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Filteredtext_set");
            Ddo_contratadausuario_usuariopessoanom_Selectedvalue_set = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Selectedvalue_set");
            Ddo_contratadausuario_usuariopessoanom_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Dropdownoptionstype");
            Ddo_contratadausuario_usuariopessoanom_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Titlecontrolidtoreplace");
            Ddo_contratadausuario_usuariopessoanom_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Includesortasc"));
            Ddo_contratadausuario_usuariopessoanom_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Includesortdsc"));
            Ddo_contratadausuario_usuariopessoanom_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Sortedstatus");
            Ddo_contratadausuario_usuariopessoanom_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Includefilter"));
            Ddo_contratadausuario_usuariopessoanom_Filtertype = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Filtertype");
            Ddo_contratadausuario_usuariopessoanom_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Filterisrange"));
            Ddo_contratadausuario_usuariopessoanom_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Includedatalist"));
            Ddo_contratadausuario_usuariopessoanom_Datalisttype = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Datalisttype");
            Ddo_contratadausuario_usuariopessoanom_Datalistproc = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Datalistproc");
            Ddo_contratadausuario_usuariopessoanom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratadausuario_usuariopessoanom_Sortasc = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Sortasc");
            Ddo_contratadausuario_usuariopessoanom_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Sortdsc");
            Ddo_contratadausuario_usuariopessoanom_Loadingdata = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Loadingdata");
            Ddo_contratadausuario_usuariopessoanom_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Cleanfilter");
            Ddo_contratadausuario_usuariopessoanom_Noresultsfound = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Noresultsfound");
            Ddo_contratadausuario_usuariopessoanom_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Searchbuttontext");
            Ddo_contratadausuario_usuariopessoadoc_Caption = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Caption");
            Ddo_contratadausuario_usuariopessoadoc_Tooltip = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Tooltip");
            Ddo_contratadausuario_usuariopessoadoc_Cls = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Cls");
            Ddo_contratadausuario_usuariopessoadoc_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Filteredtext_set");
            Ddo_contratadausuario_usuariopessoadoc_Selectedvalue_set = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Selectedvalue_set");
            Ddo_contratadausuario_usuariopessoadoc_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Dropdownoptionstype");
            Ddo_contratadausuario_usuariopessoadoc_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Titlecontrolidtoreplace");
            Ddo_contratadausuario_usuariopessoadoc_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Includesortasc"));
            Ddo_contratadausuario_usuariopessoadoc_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Includesortdsc"));
            Ddo_contratadausuario_usuariopessoadoc_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Sortedstatus");
            Ddo_contratadausuario_usuariopessoadoc_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Includefilter"));
            Ddo_contratadausuario_usuariopessoadoc_Filtertype = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Filtertype");
            Ddo_contratadausuario_usuariopessoadoc_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Filterisrange"));
            Ddo_contratadausuario_usuariopessoadoc_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Includedatalist"));
            Ddo_contratadausuario_usuariopessoadoc_Datalisttype = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Datalisttype");
            Ddo_contratadausuario_usuariopessoadoc_Datalistproc = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Datalistproc");
            Ddo_contratadausuario_usuariopessoadoc_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratadausuario_usuariopessoadoc_Sortasc = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Sortasc");
            Ddo_contratadausuario_usuariopessoadoc_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Sortdsc");
            Ddo_contratadausuario_usuariopessoadoc_Loadingdata = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Loadingdata");
            Ddo_contratadausuario_usuariopessoadoc_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Cleanfilter");
            Ddo_contratadausuario_usuariopessoadoc_Noresultsfound = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Noresultsfound");
            Ddo_contratadausuario_usuariopessoadoc_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Searchbuttontext");
            Ddo_contratadausuario_usuarioativo_Caption = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOATIVO_Caption");
            Ddo_contratadausuario_usuarioativo_Tooltip = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOATIVO_Tooltip");
            Ddo_contratadausuario_usuarioativo_Cls = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOATIVO_Cls");
            Ddo_contratadausuario_usuarioativo_Selectedvalue_set = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOATIVO_Selectedvalue_set");
            Ddo_contratadausuario_usuarioativo_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOATIVO_Dropdownoptionstype");
            Ddo_contratadausuario_usuarioativo_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOATIVO_Titlecontrolidtoreplace");
            Ddo_contratadausuario_usuarioativo_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOATIVO_Includesortasc"));
            Ddo_contratadausuario_usuarioativo_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOATIVO_Includesortdsc"));
            Ddo_contratadausuario_usuarioativo_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOATIVO_Sortedstatus");
            Ddo_contratadausuario_usuarioativo_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOATIVO_Includefilter"));
            Ddo_contratadausuario_usuarioativo_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOATIVO_Includedatalist"));
            Ddo_contratadausuario_usuarioativo_Datalisttype = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOATIVO_Datalisttype");
            Ddo_contratadausuario_usuarioativo_Datalistfixedvalues = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOATIVO_Datalistfixedvalues");
            Ddo_contratadausuario_usuarioativo_Sortasc = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOATIVO_Sortasc");
            Ddo_contratadausuario_usuarioativo_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOATIVO_Sortdsc");
            Ddo_contratadausuario_usuarioativo_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOATIVO_Cleanfilter");
            Ddo_contratadausuario_usuarioativo_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOATIVO_Searchbuttontext");
            Ddo_usuario_nome_Activeeventkey = cgiGet( sPrefix+"DDO_USUARIO_NOME_Activeeventkey");
            Ddo_usuario_nome_Filteredtext_get = cgiGet( sPrefix+"DDO_USUARIO_NOME_Filteredtext_get");
            Ddo_usuario_nome_Selectedvalue_get = cgiGet( sPrefix+"DDO_USUARIO_NOME_Selectedvalue_get");
            Ddo_contratadausuario_usuariopessoanom_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Activeeventkey");
            Ddo_contratadausuario_usuariopessoanom_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Filteredtext_get");
            Ddo_contratadausuario_usuariopessoanom_Selectedvalue_get = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Selectedvalue_get");
            Ddo_contratadausuario_usuariopessoadoc_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Activeeventkey");
            Ddo_contratadausuario_usuariopessoadoc_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Filteredtext_get");
            Ddo_contratadausuario_usuariopessoadoc_Selectedvalue_get = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC_Selectedvalue_get");
            Ddo_contratadausuario_usuarioativo_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOATIVO_Activeeventkey");
            Ddo_contratadausuario_usuarioativo_Selectedvalue_get = cgiGet( sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOATIVO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATADAUSUARIO_USUARIOPESSOANOM1"), AV18ContratadaUsuario_UsuarioPessoaNom1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFUSUARIO_NOME"), AV52TFUsuario_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFUSUARIO_NOME_SEL"), AV53TFUsuario_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATADAUSUARIO_USUARIOPESSOANOM"), AV56TFContratadaUsuario_UsuarioPessoaNom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATADAUSUARIO_USUARIOPESSOANOM_SEL"), AV57TFContratadaUsuario_UsuarioPessoaNom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATADAUSUARIO_USUARIOPESSOADOC"), AV60TFContratadaUsuario_UsuarioPessoaDoc) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATADAUSUARIO_USUARIOPESSOADOC_SEL"), AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATADAUSUARIO_USUARIOATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV74TFContratadaUsuario_UsuarioAtivo_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E208A2 */
         E208A2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E208A2( )
      {
         /* Start Routine */
         Form.Meta.addItem("Versao", "1.0 - Data: 14/04/2020 04:25", 0) ;
         subGrid_Rows = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         AV16DynamicFiltersSelector1 = "CONTRATADAUSUARIO_USUARIOPESSOANOM";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavTfusuario_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfusuario_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfusuario_nome_Visible), 5, 0)));
         edtavTfusuario_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfusuario_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfusuario_nome_sel_Visible), 5, 0)));
         edtavTfcontratadausuario_usuariopessoanom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratadausuario_usuariopessoanom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratadausuario_usuariopessoanom_Visible), 5, 0)));
         edtavTfcontratadausuario_usuariopessoanom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratadausuario_usuariopessoanom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratadausuario_usuariopessoanom_sel_Visible), 5, 0)));
         edtavTfcontratadausuario_usuariopessoadoc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratadausuario_usuariopessoadoc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratadausuario_usuariopessoadoc_Visible), 5, 0)));
         edtavTfcontratadausuario_usuariopessoadoc_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratadausuario_usuariopessoadoc_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratadausuario_usuariopessoadoc_sel_Visible), 5, 0)));
         edtavTfcontratadausuario_usuarioativo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratadausuario_usuarioativo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratadausuario_usuarioativo_sel_Visible), 5, 0)));
         Ddo_usuario_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Usuario_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_usuario_nome_Internalname, "TitleControlIdToReplace", Ddo_usuario_nome_Titlecontrolidtoreplace);
         AV54ddo_Usuario_NomeTitleControlIdToReplace = Ddo_usuario_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV54ddo_Usuario_NomeTitleControlIdToReplace", AV54ddo_Usuario_NomeTitleControlIdToReplace);
         edtavDdo_usuario_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_usuario_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_usuario_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratadausuario_usuariopessoanom_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratadaUsuario_UsuarioPessoaNom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratadausuario_usuariopessoanom_Internalname, "TitleControlIdToReplace", Ddo_contratadausuario_usuariopessoanom_Titlecontrolidtoreplace);
         AV58ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace = Ddo_contratadausuario_usuariopessoanom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV58ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace", AV58ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace);
         edtavDdo_contratadausuario_usuariopessoanomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratadausuario_usuariopessoanomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratadausuario_usuariopessoanomtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratadausuario_usuariopessoadoc_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratadaUsuario_UsuarioPessoaDoc";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratadausuario_usuariopessoadoc_Internalname, "TitleControlIdToReplace", Ddo_contratadausuario_usuariopessoadoc_Titlecontrolidtoreplace);
         AV62ddo_ContratadaUsuario_UsuarioPessoaDocTitleControlIdToReplace = Ddo_contratadausuario_usuariopessoadoc_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV62ddo_ContratadaUsuario_UsuarioPessoaDocTitleControlIdToReplace", AV62ddo_ContratadaUsuario_UsuarioPessoaDocTitleControlIdToReplace);
         edtavDdo_contratadausuario_usuariopessoadoctitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratadausuario_usuariopessoadoctitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratadausuario_usuariopessoadoctitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratadausuario_usuarioativo_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratadaUsuario_UsuarioAtivo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratadausuario_usuarioativo_Internalname, "TitleControlIdToReplace", Ddo_contratadausuario_usuarioativo_Titlecontrolidtoreplace);
         AV75ddo_ContratadaUsuario_UsuarioAtivoTitleControlIdToReplace = Ddo_contratadausuario_usuarioativo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV75ddo_ContratadaUsuario_UsuarioAtivoTitleControlIdToReplace", AV75ddo_ContratadaUsuario_UsuarioAtivoTitleControlIdToReplace);
         edtavDdo_contratadausuario_usuarioativotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratadausuario_usuarioativotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratadausuario_usuarioativotitlecontrolidtoreplace_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Pessoa", 0);
         cmbavOrderedby.addItem("2", "Nome", 0);
         cmbavOrderedby.addItem("3", "CPF", 0);
         cmbavOrderedby.addItem("4", "Ativo", 0);
         if ( AV14OrderedBy < 1 )
         {
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S142 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV67DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV67DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E218A2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV51Usuario_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV55ContratadaUsuario_UsuarioPessoaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV59ContratadaUsuario_UsuarioPessoaDocTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV73ContratadaUsuario_UsuarioAtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'CHECKSECURITYFORACTIONS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtUsuario_Nome_Titleformat = 2;
         edtUsuario_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV54ddo_Usuario_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtUsuario_Nome_Internalname, "Title", edtUsuario_Nome_Title);
         edtContratadaUsuario_UsuarioPessoaNom_Titleformat = 2;
         edtContratadaUsuario_UsuarioPessoaNom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Pessoa", AV58ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratadaUsuario_UsuarioPessoaNom_Internalname, "Title", edtContratadaUsuario_UsuarioPessoaNom_Title);
         edtContratadaUsuario_UsuarioPessoaDoc_Titleformat = 2;
         edtContratadaUsuario_UsuarioPessoaDoc_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "CPF", AV62ddo_ContratadaUsuario_UsuarioPessoaDocTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratadaUsuario_UsuarioPessoaDoc_Internalname, "Title", edtContratadaUsuario_UsuarioPessoaDoc_Title);
         chkContratadaUsuario_UsuarioAtivo_Titleformat = 2;
         chkContratadaUsuario_UsuarioAtivo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ativo", AV75ddo_ContratadaUsuario_UsuarioAtivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkContratadaUsuario_UsuarioAtivo_Internalname, "Title", chkContratadaUsuario_UsuarioAtivo.Title.Text);
         AV83GXLvl77 = 0;
         /* Using cursor H008A6 */
         pr_default.execute(2, new Object[] {AV6WWPContext.gxTpr_Userid, AV32ContratadaUsuario_ContratadaCod});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A1078ContratoGestor_ContratoCod = H008A6_A1078ContratoGestor_ContratoCod[0];
            A1013Contrato_PrepostoCod = H008A6_A1013Contrato_PrepostoCod[0];
            n1013Contrato_PrepostoCod = H008A6_n1013Contrato_PrepostoCod[0];
            A1136ContratoGestor_ContratadaCod = H008A6_A1136ContratoGestor_ContratadaCod[0];
            n1136ContratoGestor_ContratadaCod = H008A6_n1136ContratoGestor_ContratadaCod[0];
            A1079ContratoGestor_UsuarioCod = H008A6_A1079ContratoGestor_UsuarioCod[0];
            A1446ContratoGestor_ContratadaAreaCod = H008A6_A1446ContratoGestor_ContratadaAreaCod[0];
            n1446ContratoGestor_ContratadaAreaCod = H008A6_n1446ContratoGestor_ContratadaAreaCod[0];
            A1013Contrato_PrepostoCod = H008A6_A1013Contrato_PrepostoCod[0];
            n1013Contrato_PrepostoCod = H008A6_n1013Contrato_PrepostoCod[0];
            A1136ContratoGestor_ContratadaCod = H008A6_A1136ContratoGestor_ContratadaCod[0];
            n1136ContratoGestor_ContratadaCod = H008A6_n1136ContratoGestor_ContratadaCod[0];
            A1446ContratoGestor_ContratadaAreaCod = H008A6_A1446ContratoGestor_ContratadaAreaCod[0];
            n1446ContratoGestor_ContratadaAreaCod = H008A6_n1446ContratoGestor_ContratadaAreaCod[0];
            GXt_boolean2 = A1135ContratoGestor_UsuarioEhContratante;
            new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean2) ;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1446ContratoGestor_ContratadaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
            A1135ContratoGestor_UsuarioEhContratante = GXt_boolean2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1135ContratoGestor_UsuarioEhContratante", A1135ContratoGestor_UsuarioEhContratante);
            if ( ! A1135ContratoGestor_UsuarioEhContratante )
            {
               AV83GXLvl77 = 1;
               edtavAssociarservico_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAssociarservico_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAssociarservico_Visible), 5, 0)));
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            pr_default.readNext(2);
         }
         pr_default.close(2);
         if ( AV83GXLvl77 == 0 )
         {
            edtavAssociarservico_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAssociarservico_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAssociarservico_Visible), 5, 0)));
         }
         edtavAssociarperfil_Title = "Perfis";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAssociarperfil_Internalname, "Title", edtavAssociarperfil_Title);
         edtavAssociarservico_Title = "Servi�os";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAssociarservico_Internalname, "Title", edtavAssociarservico_Title);
         edtavConfig_Title = "Config";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavConfig_Internalname, "Title", edtavConfig_Title);
         edtavFerias_Title = "F�rias";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFerias_Internalname, "Title", edtavFerias_Title);
         edtavClonardados_Title = "Clonar";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavClonardados_Internalname, "Title", edtavClonardados_Title);
         AV40Websession.Set("Contratada_Codigo", StringUtil.Str( (decimal)(AV32ContratadaUsuario_ContratadaCod), 6, 0));
         edtavClonardados_Visible = ((AV6WWPContext.gxTpr_Userehcontratada&&AV6WWPContext.gxTpr_Userehgestor)||AV6WWPContext.gxTpr_Userehadministradorgam ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavClonardados_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavClonardados_Visible), 5, 0)));
         edtavAssociarperfil_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam||AV6WWPContext.gxTpr_Userehgestor ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAssociarperfil_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAssociarperfil_Visible), 5, 0)));
         edtavConfig_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam||AV6WWPContext.gxTpr_Userehgestor ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavConfig_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavConfig_Visible), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV51Usuario_NomeTitleFilterData", AV51Usuario_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV55ContratadaUsuario_UsuarioPessoaNomTitleFilterData", AV55ContratadaUsuario_UsuarioPessoaNomTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV59ContratadaUsuario_UsuarioPessoaDocTitleFilterData", AV59ContratadaUsuario_UsuarioPessoaDocTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV73ContratadaUsuario_UsuarioAtivoTitleFilterData", AV73ContratadaUsuario_UsuarioAtivoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV6WWPContext", AV6WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
      }

      protected void E118A2( )
      {
         /* Ddo_usuario_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_usuario_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_usuario_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_usuario_nome_Internalname, "SortedStatus", Ddo_usuario_nome_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_usuario_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_usuario_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_usuario_nome_Internalname, "SortedStatus", Ddo_usuario_nome_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_usuario_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV52TFUsuario_Nome = Ddo_usuario_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52TFUsuario_Nome", AV52TFUsuario_Nome);
            AV53TFUsuario_Nome_Sel = Ddo_usuario_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53TFUsuario_Nome_Sel", AV53TFUsuario_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E128A2( )
      {
         /* Ddo_contratadausuario_usuariopessoanom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratadausuario_usuariopessoanom_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contratadausuario_usuariopessoanom_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratadausuario_usuariopessoanom_Internalname, "SortedStatus", Ddo_contratadausuario_usuariopessoanom_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_contratadausuario_usuariopessoanom_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contratadausuario_usuariopessoanom_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratadausuario_usuariopessoanom_Internalname, "SortedStatus", Ddo_contratadausuario_usuariopessoanom_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_contratadausuario_usuariopessoanom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV56TFContratadaUsuario_UsuarioPessoaNom = Ddo_contratadausuario_usuariopessoanom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56TFContratadaUsuario_UsuarioPessoaNom", AV56TFContratadaUsuario_UsuarioPessoaNom);
            AV57TFContratadaUsuario_UsuarioPessoaNom_Sel = Ddo_contratadausuario_usuariopessoanom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57TFContratadaUsuario_UsuarioPessoaNom_Sel", AV57TFContratadaUsuario_UsuarioPessoaNom_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E138A2( )
      {
         /* Ddo_contratadausuario_usuariopessoadoc_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratadausuario_usuariopessoadoc_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contratadausuario_usuariopessoadoc_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratadausuario_usuariopessoadoc_Internalname, "SortedStatus", Ddo_contratadausuario_usuariopessoadoc_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_contratadausuario_usuariopessoadoc_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contratadausuario_usuariopessoadoc_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratadausuario_usuariopessoadoc_Internalname, "SortedStatus", Ddo_contratadausuario_usuariopessoadoc_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_contratadausuario_usuariopessoadoc_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV60TFContratadaUsuario_UsuarioPessoaDoc = Ddo_contratadausuario_usuariopessoadoc_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60TFContratadaUsuario_UsuarioPessoaDoc", AV60TFContratadaUsuario_UsuarioPessoaDoc);
            AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel = Ddo_contratadausuario_usuariopessoadoc_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel", AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E148A2( )
      {
         /* Ddo_contratadausuario_usuarioativo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratadausuario_usuarioativo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contratadausuario_usuarioativo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratadausuario_usuarioativo_Internalname, "SortedStatus", Ddo_contratadausuario_usuarioativo_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_contratadausuario_usuarioativo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contratadausuario_usuarioativo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratadausuario_usuarioativo_Internalname, "SortedStatus", Ddo_contratadausuario_usuarioativo_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_contratadausuario_usuarioativo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV74TFContratadaUsuario_UsuarioAtivo_Sel = (short)(NumberUtil.Val( Ddo_contratadausuario_usuarioativo_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV74TFContratadaUsuario_UsuarioAtivo_Sel", StringUtil.Str( (decimal)(AV74TFContratadaUsuario_UsuarioAtivo_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E228A2( )
      {
         /* Grid_Load Routine */
         edtavAtualizar_Tooltiptext = "";
         if ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam )
         {
            AV45Atualizar = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavAtualizar_Internalname, AV45Atualizar);
            AV84Atualizar_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            edtavAtualizar_Enabled = 1;
         }
         else
         {
            AV45Atualizar = context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavAtualizar_Internalname, AV45Atualizar);
            AV84Atualizar_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( )));
            edtavAtualizar_Enabled = 0;
         }
         edtavDelete_Tooltiptext = "Eliminar";
         if ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam )
         {
            edtavDelete_Link = formatLink("contratadausuario.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A66ContratadaUsuario_ContratadaCod) + "," + UrlEncode("" +A69ContratadaUsuario_UsuarioCod);
            AV29Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV29Delete);
            AV85Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavDelete_Enabled = 1;
         }
         else
         {
            edtavDelete_Link = "";
            AV29Delete = context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV29Delete);
            AV85Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( )));
            edtavDelete_Enabled = 0;
         }
         edtavFerias_Tooltiptext = "Clique aqui p/ marcar o usu�rio como ausente";
         if ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) && ( AV6WWPContext.gxTpr_Userehadministradorgam || AV6WWPContext.gxTpr_Userehgestor || ( AV6WWPContext.gxTpr_Userid == A69ContratadaUsuario_UsuarioCod ) ) )
         {
            AV68Ferias = context.GetImagePath( "f3aa001a-9bb4-4496-90e2-794d4244d8d1", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavFerias_Internalname, AV68Ferias);
            AV86Ferias_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f3aa001a-9bb4-4496-90e2-794d4244d8d1", "", context.GetTheme( )));
            edtavFerias_Enabled = 1;
         }
         else
         {
            AV68Ferias = context.GetImagePath( "f3aa001a-9bb4-4496-90e2-794d4244d8d1", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavFerias_Internalname, AV68Ferias);
            AV86Ferias_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f3aa001a-9bb4-4496-90e2-794d4244d8d1", "", context.GetTheme( )));
            edtavFerias_Enabled = 0;
         }
         edtavAssociarperfil_Tooltiptext = "Clique aqui p/ associar os perfis deste usu�rio!";
         if ( AV6WWPContext.gxTpr_Userehadministradorgam || AV6WWPContext.gxTpr_Userehgestor )
         {
            AV33AssociarPerfil = context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavAssociarperfil_Internalname, AV33AssociarPerfil);
            AV87Associarperfil_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )));
            edtavAssociarperfil_Enabled = 1;
         }
         else
         {
            AV33AssociarPerfil = context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavAssociarperfil_Internalname, AV33AssociarPerfil);
            AV87Associarperfil_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )));
            edtavAssociarperfil_Enabled = 0;
         }
         edtavAssociarservico_Tooltiptext = "Clique aqui p/ associar os servi�os deste usu�rio!";
         if ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam )
         {
            AV42AssociarServico = context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavAssociarservico_Internalname, AV42AssociarServico);
            AV88Associarservico_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )));
            edtavAssociarservico_Enabled = 1;
         }
         else
         {
            AV42AssociarServico = context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavAssociarservico_Internalname, AV42AssociarServico);
            AV88Associarservico_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )));
            edtavAssociarservico_Enabled = 0;
         }
         edtavConfig_Tooltiptext = "Permiss�es do usu�rio";
         if ( AV6WWPContext.gxTpr_Userehadministradorgam || AV6WWPContext.gxTpr_Userehgestor )
         {
            edtavConfig_Link = formatLink("wp_cfgperfil.aspx") + "?" + UrlEncode("" +A69ContratadaUsuario_UsuarioCod) + "," + UrlEncode(StringUtil.RTrim(A71ContratadaUsuario_UsuarioPessoaNom));
            AV41Config = context.GetImagePath( "70d5df5e-e723-4cca-9402-f0ce8679d27f", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavConfig_Internalname, AV41Config);
            AV89Config_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "70d5df5e-e723-4cca-9402-f0ce8679d27f", "", context.GetTheme( )));
            edtavConfig_Enabled = 1;
         }
         else
         {
            edtavConfig_Link = "";
            AV41Config = context.GetImagePath( "70d5df5e-e723-4cca-9402-f0ce8679d27f", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavConfig_Internalname, AV41Config);
            AV89Config_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "70d5df5e-e723-4cca-9402-f0ce8679d27f", "", context.GetTheme( )));
            edtavConfig_Enabled = 0;
         }
         edtavClonardados_Tooltiptext = "Clonar dados neste usu�rio!";
         if ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam )
         {
            AV76ClonarDados = context.GetImagePath( "550f386a-e6ac-4748-b314-ee8e1f72ce4d", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavClonardados_Internalname, AV76ClonarDados);
            AV90Clonardados_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "550f386a-e6ac-4748-b314-ee8e1f72ce4d", "", context.GetTheme( )));
            edtavClonardados_Enabled = 1;
         }
         else
         {
            AV76ClonarDados = context.GetImagePath( "550f386a-e6ac-4748-b314-ee8e1f72ce4d", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavClonardados_Internalname, AV76ClonarDados);
            AV90Clonardados_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "550f386a-e6ac-4748-b314-ee8e1f72ce4d", "", context.GetTheme( )));
            edtavClonardados_Enabled = 0;
         }
         if ( A1394ContratadaUsuario_UsuarioAtivo )
         {
            AV72Color = GXUtil.RGB( 0, 0, 0);
         }
         else
         {
            AV72Color = GXUtil.RGB( 255, 0, 0);
         }
         edtUsuario_Nome_Forecolor = (int)(AV72Color);
         edtContratadaUsuario_UsuarioPessoaNom_Forecolor = (int)(AV72Color);
         edtContratadaUsuario_UsuarioPessoaDoc_Forecolor = (int)(AV72Color);
         edtavGamemail_Forecolor = (int)(AV72Color);
         edtavNotificar_Forecolor = (int)(AV72Color);
         if ( ( NumberUtil.Val( StringUtil.Substring( AV6WWPContext.gxTpr_Validationkey, 5, 2), ".") <= Convert.ToDecimal( subGrid_Recordcount( ) )) && ( StringUtil.StrCmp(StringUtil.Substring( AV6WWPContext.gxTpr_Validationkey, 1, 2), "00") != 0 ) )
         {
            imgInsert_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Enabled), 5, 0)));
            imgInsert_Tooltiptext = "Limite da sua vers�o atingido!";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Tooltiptext", imgInsert_Tooltiptext);
            bttBtnimportar_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnimportar_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnimportar_Enabled), 5, 0)));
            bttBtnimportar_Tooltiptext = "Limite da sua vers�o atingido!";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnimportar_Internalname, "Tooltiptext", bttBtnimportar_Tooltiptext);
         }
         edtavFerias_Visible = ((AV6WWPContext.gxTpr_Insert&&AV6WWPContext.gxTpr_Update&&AV6WWPContext.gxTpr_Delete)&&(AV6WWPContext.gxTpr_Userehgestor||(AV6WWPContext.gxTpr_Userid==A69ContratadaUsuario_UsuarioCod)) ? 1 : 0);
         if ( A1908Usuario_DeFerias )
         {
            AV68Ferias = context.GetImagePath( "4474bccb-05fc-489f-ad8d-570ee718e701", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavFerias_Internalname, AV68Ferias);
            AV86Ferias_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "4474bccb-05fc-489f-ad8d-570ee718e701", "", context.GetTheme( )));
            edtavFerias_Tooltiptext = "Marcar o retorno do usu�rio �s atividades";
         }
         AV44GamUser.load( A341Usuario_UserGamGuid);
         AV43GAMEMail = AV44GamUser.gxTpr_Email;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavGamemail_Internalname, AV43GAMEMail);
         GXt_char3 = AV80Notificar;
         new prc_listadestatus(context ).execute(  A40000UsuarioNotifica_NoStatus, out  GXt_char3) ;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A40000UsuarioNotifica_NoStatus", A40000UsuarioNotifica_NoStatus);
         AV80Notificar = GXt_char3;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavNotificar_Internalname, AV80Notificar);
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 42;
         }
         sendrow_422( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_42_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(42, GridRow);
         }
      }

      protected void E158A2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E198A2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E168A2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
      }

      protected void E178A2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("wp_novousuario.aspx") + "?" + UrlEncode("" +0) + "," + UrlEncode("" +AV32ContratadaUsuario_ContratadaCod);
         context.wjLocDisableFrm = 1;
      }

      protected void E188A2( )
      {
         /* 'DoImportar' Routine */
         context.wjLoc = formatLink("wp_importarusuarios.aspx") + "?" + UrlEncode("" +0) + "," + UrlEncode("" +AV32ContratadaUsuario_ContratadaCod);
         context.wjLocDisableFrm = 1;
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_usuario_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_usuario_nome_Internalname, "SortedStatus", Ddo_usuario_nome_Sortedstatus);
         Ddo_contratadausuario_usuariopessoanom_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratadausuario_usuariopessoanom_Internalname, "SortedStatus", Ddo_contratadausuario_usuariopessoanom_Sortedstatus);
         Ddo_contratadausuario_usuariopessoadoc_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratadausuario_usuariopessoadoc_Internalname, "SortedStatus", Ddo_contratadausuario_usuariopessoadoc_Sortedstatus);
         Ddo_contratadausuario_usuarioativo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratadausuario_usuarioativo_Internalname, "SortedStatus", Ddo_contratadausuario_usuarioativo_Sortedstatus);
      }

      protected void S142( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV14OrderedBy == 2 )
         {
            Ddo_usuario_nome_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_usuario_nome_Internalname, "SortedStatus", Ddo_usuario_nome_Sortedstatus);
         }
         else if ( AV14OrderedBy == 1 )
         {
            Ddo_contratadausuario_usuariopessoanom_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratadausuario_usuariopessoanom_Internalname, "SortedStatus", Ddo_contratadausuario_usuariopessoanom_Sortedstatus);
         }
         else if ( AV14OrderedBy == 3 )
         {
            Ddo_contratadausuario_usuariopessoadoc_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratadausuario_usuariopessoadoc_Internalname, "SortedStatus", Ddo_contratadausuario_usuariopessoadoc_Sortedstatus);
         }
         else if ( AV14OrderedBy == 4 )
         {
            Ddo_contratadausuario_usuarioativo_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratadausuario_usuarioativo_Internalname, "SortedStatus", Ddo_contratadausuario_usuarioativo_Sortedstatus);
         }
      }

      protected void S152( )
      {
         /* 'CHECKSECURITYFORACTIONS' Routine */
         if ( ! ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam ) )
         {
            imgInsert_Link = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Link", imgInsert_Link);
            imgInsert_Bitmap = context.GetImagePath( "13d28f37-c579-4dd9-a404-ba51ab71d1e3", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Bitmap", context.convertURL( context.PathToRelativeUrl( imgInsert_Bitmap)));
            imgInsert_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Enabled), 5, 0)));
         }
         if ( ! ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam ) )
         {
            bttBtnimportar_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnimportar_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnimportar_Enabled), 5, 0)));
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavContratadausuario_usuariopessoanom1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratadausuario_usuariopessoanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratadausuario_usuariopessoanom1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 )
         {
            edtavContratadausuario_usuariopessoanom1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratadausuario_usuariopessoanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratadausuario_usuariopessoanom1_Visible), 5, 0)));
         }
      }

      protected void S182( )
      {
         /* 'CLEANFILTERS' Routine */
         AV52TFUsuario_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52TFUsuario_Nome", AV52TFUsuario_Nome);
         Ddo_usuario_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_usuario_nome_Internalname, "FilteredText_set", Ddo_usuario_nome_Filteredtext_set);
         AV53TFUsuario_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53TFUsuario_Nome_Sel", AV53TFUsuario_Nome_Sel);
         Ddo_usuario_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_usuario_nome_Internalname, "SelectedValue_set", Ddo_usuario_nome_Selectedvalue_set);
         AV56TFContratadaUsuario_UsuarioPessoaNom = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56TFContratadaUsuario_UsuarioPessoaNom", AV56TFContratadaUsuario_UsuarioPessoaNom);
         Ddo_contratadausuario_usuariopessoanom_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratadausuario_usuariopessoanom_Internalname, "FilteredText_set", Ddo_contratadausuario_usuariopessoanom_Filteredtext_set);
         AV57TFContratadaUsuario_UsuarioPessoaNom_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57TFContratadaUsuario_UsuarioPessoaNom_Sel", AV57TFContratadaUsuario_UsuarioPessoaNom_Sel);
         Ddo_contratadausuario_usuariopessoanom_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratadausuario_usuariopessoanom_Internalname, "SelectedValue_set", Ddo_contratadausuario_usuariopessoanom_Selectedvalue_set);
         AV60TFContratadaUsuario_UsuarioPessoaDoc = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60TFContratadaUsuario_UsuarioPessoaDoc", AV60TFContratadaUsuario_UsuarioPessoaDoc);
         Ddo_contratadausuario_usuariopessoadoc_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratadausuario_usuariopessoadoc_Internalname, "FilteredText_set", Ddo_contratadausuario_usuariopessoadoc_Filteredtext_set);
         AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel", AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel);
         Ddo_contratadausuario_usuariopessoadoc_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratadausuario_usuariopessoadoc_Internalname, "SelectedValue_set", Ddo_contratadausuario_usuariopessoadoc_Selectedvalue_set);
         AV74TFContratadaUsuario_UsuarioAtivo_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV74TFContratadaUsuario_UsuarioAtivo_Sel", StringUtil.Str( (decimal)(AV74TFContratadaUsuario_UsuarioAtivo_Sel), 1, 0));
         Ddo_contratadausuario_usuarioativo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratadausuario_usuarioativo_Internalname, "SelectedValue_set", Ddo_contratadausuario_usuarioativo_Selectedvalue_set);
         AV16DynamicFiltersSelector1 = "CONTRATADAUSUARIO_USUARIOPESSOANOM";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         AV18ContratadaUsuario_UsuarioPessoaNom1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ContratadaUsuario_UsuarioPessoaNom1", AV18ContratadaUsuario_UsuarioPessoaNom1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S132( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV31Session.Get(AV91Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV91Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV31Session.Get(AV91Pgmname+"GridState"), "");
         }
         AV14OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         AV15OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S212( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV92GXV1 = 1;
         while ( AV92GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV92GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFUSUARIO_NOME") == 0 )
            {
               AV52TFUsuario_Nome = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52TFUsuario_Nome", AV52TFUsuario_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52TFUsuario_Nome)) )
               {
                  Ddo_usuario_nome_Filteredtext_set = AV52TFUsuario_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_usuario_nome_Internalname, "FilteredText_set", Ddo_usuario_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFUSUARIO_NOME_SEL") == 0 )
            {
               AV53TFUsuario_Nome_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53TFUsuario_Nome_Sel", AV53TFUsuario_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53TFUsuario_Nome_Sel)) )
               {
                  Ddo_usuario_nome_Selectedvalue_set = AV53TFUsuario_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_usuario_nome_Internalname, "SelectedValue_set", Ddo_usuario_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 )
            {
               AV56TFContratadaUsuario_UsuarioPessoaNom = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56TFContratadaUsuario_UsuarioPessoaNom", AV56TFContratadaUsuario_UsuarioPessoaNom);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56TFContratadaUsuario_UsuarioPessoaNom)) )
               {
                  Ddo_contratadausuario_usuariopessoanom_Filteredtext_set = AV56TFContratadaUsuario_UsuarioPessoaNom;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratadausuario_usuariopessoanom_Internalname, "FilteredText_set", Ddo_contratadausuario_usuariopessoanom_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATADAUSUARIO_USUARIOPESSOANOM_SEL") == 0 )
            {
               AV57TFContratadaUsuario_UsuarioPessoaNom_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57TFContratadaUsuario_UsuarioPessoaNom_Sel", AV57TFContratadaUsuario_UsuarioPessoaNom_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57TFContratadaUsuario_UsuarioPessoaNom_Sel)) )
               {
                  Ddo_contratadausuario_usuariopessoanom_Selectedvalue_set = AV57TFContratadaUsuario_UsuarioPessoaNom_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratadausuario_usuariopessoanom_Internalname, "SelectedValue_set", Ddo_contratadausuario_usuariopessoanom_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATADAUSUARIO_USUARIOPESSOADOC") == 0 )
            {
               AV60TFContratadaUsuario_UsuarioPessoaDoc = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60TFContratadaUsuario_UsuarioPessoaDoc", AV60TFContratadaUsuario_UsuarioPessoaDoc);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFContratadaUsuario_UsuarioPessoaDoc)) )
               {
                  Ddo_contratadausuario_usuariopessoadoc_Filteredtext_set = AV60TFContratadaUsuario_UsuarioPessoaDoc;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratadausuario_usuariopessoadoc_Internalname, "FilteredText_set", Ddo_contratadausuario_usuariopessoadoc_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATADAUSUARIO_USUARIOPESSOADOC_SEL") == 0 )
            {
               AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel", AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel)) )
               {
                  Ddo_contratadausuario_usuariopessoadoc_Selectedvalue_set = AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratadausuario_usuariopessoadoc_Internalname, "SelectedValue_set", Ddo_contratadausuario_usuariopessoadoc_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATADAUSUARIO_USUARIOATIVO_SEL") == 0 )
            {
               AV74TFContratadaUsuario_UsuarioAtivo_Sel = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV74TFContratadaUsuario_UsuarioAtivo_Sel", StringUtil.Str( (decimal)(AV74TFContratadaUsuario_UsuarioAtivo_Sel), 1, 0));
               if ( ! (0==AV74TFContratadaUsuario_UsuarioAtivo_Sel) )
               {
                  Ddo_contratadausuario_usuarioativo_Selectedvalue_set = StringUtil.Str( (decimal)(AV74TFContratadaUsuario_UsuarioAtivo_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratadausuario_usuarioativo_Internalname, "SelectedValue_set", Ddo_contratadausuario_usuarioativo_Selectedvalue_set);
               }
            }
            AV92GXV1 = (int)(AV92GXV1+1);
         }
      }

      protected void S202( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(1));
            AV16DynamicFiltersSelector1 = AV13GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 )
            {
               AV18ContratadaUsuario_UsuarioPessoaNom1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ContratadaUsuario_UsuarioPessoaNom1", AV18ContratadaUsuario_UsuarioPessoaNom1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV31Session.Get(AV91Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV14OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV15OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52TFUsuario_Nome)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFUSUARIO_NOME";
            AV12GridStateFilterValue.gxTpr_Value = AV52TFUsuario_Nome;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53TFUsuario_Nome_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFUSUARIO_NOME_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV53TFUsuario_Nome_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56TFContratadaUsuario_UsuarioPessoaNom)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATADAUSUARIO_USUARIOPESSOANOM";
            AV12GridStateFilterValue.gxTpr_Value = AV56TFContratadaUsuario_UsuarioPessoaNom;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57TFContratadaUsuario_UsuarioPessoaNom_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATADAUSUARIO_USUARIOPESSOANOM_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV57TFContratadaUsuario_UsuarioPessoaNom_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFContratadaUsuario_UsuarioPessoaDoc)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATADAUSUARIO_USUARIOPESSOADOC";
            AV12GridStateFilterValue.gxTpr_Value = AV60TFContratadaUsuario_UsuarioPessoaDoc;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATADAUSUARIO_USUARIOPESSOADOC_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV74TFContratadaUsuario_UsuarioAtivo_Sel) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATADAUSUARIO_USUARIOATIVO_SEL";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV74TFContratadaUsuario_UsuarioAtivo_Sel), 1, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV32ContratadaUsuario_ContratadaCod) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "PARM_&CONTRATADAUSUARIO_CONTRATADACOD";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV32ContratadaUsuario_ContratadaCod), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV91Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S222( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV13GridStateDynamicFilter.gxTpr_Selected = AV16DynamicFiltersSelector1;
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV18ContratadaUsuario_UsuarioPessoaNom1)) )
         {
            AV13GridStateDynamicFilter.gxTpr_Value = AV18ContratadaUsuario_UsuarioPessoaNom1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
         {
            AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
         }
      }

      protected void S122( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV91Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "ContratadaUsuario";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "ContratadaUsuario_ContratadaCod";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV32ContratadaUsuario_ContratadaCod), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV31Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void E238A2( )
      {
         /* Atualizar_Click Routine */
         AV40Websession.Set("Entidade", "A"+StringUtil.LTrim( StringUtil.Str( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0)));
         context.wjLoc = formatLink("usuario.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A69ContratadaUsuario_UsuarioCod);
         context.wjLocDisableFrm = 1;
      }

      protected void E248A2( )
      {
         /* Usuario_Nome_Click Routine */
         AV40Websession.Set("Entidade", "A"+StringUtil.LTrim( StringUtil.Str( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0)));
         context.wjLoc = formatLink("viewusuario.aspx") + "?" + UrlEncode("" +A69ContratadaUsuario_UsuarioCod) + "," + UrlEncode(StringUtil.RTrim(""));
         context.wjLocDisableFrm = 1;
      }

      protected void E258A2( )
      {
         /* Ferias_Click Routine */
         AV46Usuario.Load(A69ContratadaUsuario_UsuarioCod);
         AV77AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         AV77AuditingObject.gxTpr_Mode = "UPD";
         AV79AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
         AV79AuditingObjectRecordItem.gxTpr_Tablename = "Usuario";
         AV79AuditingObjectRecordItem.gxTpr_Mode = "UPD";
         AV77AuditingObject.gxTpr_Record.Add(AV79AuditingObjectRecordItem, 0);
         AV78AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
         AV78AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_DeFerias";
         AV78AuditingObjectRecordItemAttributeItem.gxTpr_Description = "De f�rias";
         AV78AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
         AV78AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
         AV78AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( AV46Usuario.gxTpr_Usuario_deferias);
         if ( AV46Usuario.gxTpr_Usuario_deferias )
         {
            AV68Ferias = context.GetImagePath( "f3aa001a-9bb4-4496-90e2-794d4244d8d1", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFerias_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV68Ferias)) ? AV86Ferias_GXI : context.convertURL( context.PathToRelativeUrl( AV68Ferias))));
            AV86Ferias_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f3aa001a-9bb4-4496-90e2-794d4244d8d1", "", context.GetTheme( )));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFerias_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV68Ferias)) ? AV86Ferias_GXI : context.convertURL( context.PathToRelativeUrl( AV68Ferias))));
            edtavFerias_Tooltiptext = "Marcar o usu�rio como afastado das atividades";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFerias_Internalname, "Tooltiptext", edtavFerias_Tooltiptext);
            AV46Usuario.gxTpr_Usuario_deferias = false;
         }
         else
         {
            AV68Ferias = context.GetImagePath( "4474bccb-05fc-489f-ad8d-570ee718e701", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFerias_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV68Ferias)) ? AV86Ferias_GXI : context.convertURL( context.PathToRelativeUrl( AV68Ferias))));
            AV86Ferias_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "4474bccb-05fc-489f-ad8d-570ee718e701", "", context.GetTheme( )));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFerias_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV68Ferias)) ? AV86Ferias_GXI : context.convertURL( context.PathToRelativeUrl( AV68Ferias))));
            edtavFerias_Tooltiptext = "Marcar o retorno do usu�rio �s atividades";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFerias_Internalname, "Tooltiptext", edtavFerias_Tooltiptext);
            AV46Usuario.gxTpr_Usuario_deferias = true;
         }
         AV78AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( AV46Usuario.gxTpr_Usuario_deferias);
         AV79AuditingObjectRecordItem.gxTpr_Attribute.Add(AV78AuditingObjectRecordItemAttributeItem, 0);
         AV46Usuario.Save();
         context.CommitDataStores( "ContratadaContratadaUsuarioWC");
         new wwpbaseobjects.audittransaction(context ).execute(  AV77AuditingObject,  AV91Pgmname) ;
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
      }

      protected void wb_table1_2_8A2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='TableSearchCell'>") ;
            wb_table2_8_8A2( true) ;
         }
         else
         {
            wb_table2_8_8A2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_8A2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_39_8A2( true) ;
         }
         else
         {
            wb_table3_39_8A2( false) ;
         }
         return  ;
      }

      protected void wb_table3_39_8A2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_8A2e( true) ;
         }
         else
         {
            wb_table1_2_8A2e( false) ;
         }
      }

      protected void wb_table3_39_8A2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedgrid_Internalname, tblTablemergedgrid_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"42\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Contratada Usuario_Contratada Cod") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Usuario_Usuario Cod") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Pessoa Cod") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Gam Guid") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtUsuario_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtUsuario_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtUsuario_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratadaUsuario_UsuarioPessoaNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratadaUsuario_UsuarioPessoaNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratadaUsuario_UsuarioPessoaNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratadaUsuario_UsuarioPessoaDoc_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratadaUsuario_UsuarioPessoaDoc_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratadaUsuario_UsuarioPessoaDoc_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "E-Mail") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Notificar") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkContratadaUsuario_UsuarioAtivo_Titleformat == 0 )
               {
                  context.SendWebValue( chkContratadaUsuario_UsuarioAtivo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkContratadaUsuario_UsuarioAtivo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavFerias_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( edtavFerias_Title) ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavAssociarperfil_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( edtavAssociarperfil_Title) ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavAssociarservico_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( edtavAssociarservico_Title) ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavConfig_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( edtavConfig_Title) ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavClonardados_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( edtavClonardados_Title) ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV45Atualizar));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavAtualizar_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavAtualizar_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Delete));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A70ContratadaUsuario_UsuarioPessoaCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A341Usuario_UserGamGuid));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A2Usuario_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtUsuario_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUsuario_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUsuario_Nome_Forecolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A71ContratadaUsuario_UsuarioPessoaNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratadaUsuario_UsuarioPessoaNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratadaUsuario_UsuarioPessoaNom_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratadaUsuario_UsuarioPessoaNom_Forecolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A491ContratadaUsuario_UsuarioPessoaDoc);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratadaUsuario_UsuarioPessoaDoc_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratadaUsuario_UsuarioPessoaDoc_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratadaUsuario_UsuarioPessoaDoc_Forecolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", AV43GAMEMail);
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavGamemail_Forecolor), 9, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavGamemail_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV80Notificar));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavNotificar_Forecolor), 9, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavNotificar_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A1394ContratadaUsuario_UsuarioAtivo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkContratadaUsuario_UsuarioAtivo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkContratadaUsuario_UsuarioAtivo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV68Ferias));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtavFerias_Title));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavFerias_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavFerias_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavFerias_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV33AssociarPerfil));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtavAssociarperfil_Title));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavAssociarperfil_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavAssociarperfil_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavAssociarperfil_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV42AssociarServico));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtavAssociarservico_Title));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavAssociarservico_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavAssociarservico_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavAssociarservico_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV41Config));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtavConfig_Title));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavConfig_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavConfig_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavConfig_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavConfig_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV76ClonarDados));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtavClonardados_Title));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavClonardados_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavClonardados_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavClonardados_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 42 )
         {
            wbEnd = 0;
            nRC_GXsfl_42 = (short)(nGXsfl_42_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GridContainer.AddObjectProperty("GRID_nEOF", GRID_nEOF);
               GridContainer.AddObjectProperty("GRID_nFirstRecordOnPage", GRID_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, imgInsert_Bitmap, "", "", "", context.GetTheme( ), 1, imgInsert_Enabled, "", imgInsert_Tooltiptext, 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratadaContratadaUsuarioWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_39_8A2e( true) ;
         }
         else
         {
            wb_table3_39_8A2e( false) ;
         }
      }

      protected void wb_table2_8_8A2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_11_8A2( true) ;
         }
         else
         {
            wb_table4_11_8A2( false) ;
         }
         return  ;
      }

      protected void wb_table4_11_8A2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_ContratadaContratadaUsuarioWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'" + sPrefix + "',false,'" + sGXsfl_42_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_ContratadaContratadaUsuarioWC.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'" + sPrefix + "',false,'" + sGXsfl_42_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV15OrderedDsc), StringUtil.BoolToStr( AV15OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_ContratadaContratadaUsuarioWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_21_8A2( true) ;
         }
         else
         {
            wb_table5_21_8A2( false) ;
         }
         return  ;
      }

      protected void wb_table5_21_8A2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_8A2e( true) ;
         }
         else
         {
            wb_table2_8_8A2e( false) ;
         }
      }

      protected void wb_table5_21_8A2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratadaContratadaUsuarioWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_26_8A2( true) ;
         }
         else
         {
            wb_table6_26_8A2( false) ;
         }
         return  ;
      }

      protected void wb_table6_26_8A2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_21_8A2e( true) ;
         }
         else
         {
            wb_table5_21_8A2e( false) ;
         }
      }

      protected void wb_table6_26_8A2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ContratadaContratadaUsuarioWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'" + sPrefix + "',false,'" + sGXsfl_42_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV16DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_ContratadaContratadaUsuarioWC.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ContratadaContratadaUsuarioWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'" + sPrefix + "',false,'" + sGXsfl_42_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratadausuario_usuariopessoanom1_Internalname, StringUtil.RTrim( AV18ContratadaUsuario_UsuarioPessoaNom1), StringUtil.RTrim( context.localUtil.Format( AV18ContratadaUsuario_UsuarioPessoaNom1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,35);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratadausuario_usuariopessoanom1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratadausuario_usuariopessoanom1_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratadaContratadaUsuarioWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_26_8A2e( true) ;
         }
         else
         {
            wb_table6_26_8A2e( false) ;
         }
      }

      protected void wb_table4_11_8A2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnimportar_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(42), 2, 0)+","+"null"+");", "Importar", bttBtnimportar_Jsonclick, 5, bttBtnimportar_Tooltiptext, "", StyleString, ClassString, 1, bttBtnimportar_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOIMPORTAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratadaContratadaUsuarioWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_11_8A2e( true) ;
         }
         else
         {
            wb_table4_11_8A2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV32ContratadaUsuario_ContratadaCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32ContratadaUsuario_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32ContratadaUsuario_ContratadaCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA8A2( ) ;
         WS8A2( ) ;
         WE8A2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV32ContratadaUsuario_ContratadaCod = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA8A2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contratadacontratadausuariowc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA8A2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV32ContratadaUsuario_ContratadaCod = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32ContratadaUsuario_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32ContratadaUsuario_ContratadaCod), 6, 0)));
         }
         wcpOAV32ContratadaUsuario_ContratadaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV32ContratadaUsuario_ContratadaCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV32ContratadaUsuario_ContratadaCod != wcpOAV32ContratadaUsuario_ContratadaCod ) ) )
         {
            setjustcreated();
         }
         wcpOAV32ContratadaUsuario_ContratadaCod = AV32ContratadaUsuario_ContratadaCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV32ContratadaUsuario_ContratadaCod = cgiGet( sPrefix+"AV32ContratadaUsuario_ContratadaCod_CTRL");
         if ( StringUtil.Len( sCtrlAV32ContratadaUsuario_ContratadaCod) > 0 )
         {
            AV32ContratadaUsuario_ContratadaCod = (int)(context.localUtil.CToN( cgiGet( sCtrlAV32ContratadaUsuario_ContratadaCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32ContratadaUsuario_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32ContratadaUsuario_ContratadaCod), 6, 0)));
         }
         else
         {
            AV32ContratadaUsuario_ContratadaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV32ContratadaUsuario_ContratadaCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA8A2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS8A2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS8A2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV32ContratadaUsuario_ContratadaCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32ContratadaUsuario_ContratadaCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV32ContratadaUsuario_ContratadaCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV32ContratadaUsuario_ContratadaCod_CTRL", StringUtil.RTrim( sCtrlAV32ContratadaUsuario_ContratadaCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE8A2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202053021265441");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contratadacontratadausuariowc.js", "?202053021265443");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_422( )
      {
         edtavAtualizar_Internalname = sPrefix+"vATUALIZAR_"+sGXsfl_42_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_42_idx;
         edtContratadaUsuario_ContratadaCod_Internalname = sPrefix+"CONTRATADAUSUARIO_CONTRATADACOD_"+sGXsfl_42_idx;
         edtContratadaUsuario_UsuarioCod_Internalname = sPrefix+"CONTRATADAUSUARIO_USUARIOCOD_"+sGXsfl_42_idx;
         edtContratadaUsuario_UsuarioPessoaCod_Internalname = sPrefix+"CONTRATADAUSUARIO_USUARIOPESSOACOD_"+sGXsfl_42_idx;
         edtUsuario_UserGamGuid_Internalname = sPrefix+"USUARIO_USERGAMGUID_"+sGXsfl_42_idx;
         edtUsuario_Nome_Internalname = sPrefix+"USUARIO_NOME_"+sGXsfl_42_idx;
         edtContratadaUsuario_UsuarioPessoaNom_Internalname = sPrefix+"CONTRATADAUSUARIO_USUARIOPESSOANOM_"+sGXsfl_42_idx;
         edtContratadaUsuario_UsuarioPessoaDoc_Internalname = sPrefix+"CONTRATADAUSUARIO_USUARIOPESSOADOC_"+sGXsfl_42_idx;
         edtavGamemail_Internalname = sPrefix+"vGAMEMAIL_"+sGXsfl_42_idx;
         edtavNotificar_Internalname = sPrefix+"vNOTIFICAR_"+sGXsfl_42_idx;
         chkContratadaUsuario_UsuarioAtivo_Internalname = sPrefix+"CONTRATADAUSUARIO_USUARIOATIVO_"+sGXsfl_42_idx;
         edtavFerias_Internalname = sPrefix+"vFERIAS_"+sGXsfl_42_idx;
         edtavAssociarperfil_Internalname = sPrefix+"vASSOCIARPERFIL_"+sGXsfl_42_idx;
         edtavAssociarservico_Internalname = sPrefix+"vASSOCIARSERVICO_"+sGXsfl_42_idx;
         edtavConfig_Internalname = sPrefix+"vCONFIG_"+sGXsfl_42_idx;
         edtavClonardados_Internalname = sPrefix+"vCLONARDADOS_"+sGXsfl_42_idx;
      }

      protected void SubsflControlProps_fel_422( )
      {
         edtavAtualizar_Internalname = sPrefix+"vATUALIZAR_"+sGXsfl_42_fel_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_42_fel_idx;
         edtContratadaUsuario_ContratadaCod_Internalname = sPrefix+"CONTRATADAUSUARIO_CONTRATADACOD_"+sGXsfl_42_fel_idx;
         edtContratadaUsuario_UsuarioCod_Internalname = sPrefix+"CONTRATADAUSUARIO_USUARIOCOD_"+sGXsfl_42_fel_idx;
         edtContratadaUsuario_UsuarioPessoaCod_Internalname = sPrefix+"CONTRATADAUSUARIO_USUARIOPESSOACOD_"+sGXsfl_42_fel_idx;
         edtUsuario_UserGamGuid_Internalname = sPrefix+"USUARIO_USERGAMGUID_"+sGXsfl_42_fel_idx;
         edtUsuario_Nome_Internalname = sPrefix+"USUARIO_NOME_"+sGXsfl_42_fel_idx;
         edtContratadaUsuario_UsuarioPessoaNom_Internalname = sPrefix+"CONTRATADAUSUARIO_USUARIOPESSOANOM_"+sGXsfl_42_fel_idx;
         edtContratadaUsuario_UsuarioPessoaDoc_Internalname = sPrefix+"CONTRATADAUSUARIO_USUARIOPESSOADOC_"+sGXsfl_42_fel_idx;
         edtavGamemail_Internalname = sPrefix+"vGAMEMAIL_"+sGXsfl_42_fel_idx;
         edtavNotificar_Internalname = sPrefix+"vNOTIFICAR_"+sGXsfl_42_fel_idx;
         chkContratadaUsuario_UsuarioAtivo_Internalname = sPrefix+"CONTRATADAUSUARIO_USUARIOATIVO_"+sGXsfl_42_fel_idx;
         edtavFerias_Internalname = sPrefix+"vFERIAS_"+sGXsfl_42_fel_idx;
         edtavAssociarperfil_Internalname = sPrefix+"vASSOCIARPERFIL_"+sGXsfl_42_fel_idx;
         edtavAssociarservico_Internalname = sPrefix+"vASSOCIARSERVICO_"+sGXsfl_42_fel_idx;
         edtavConfig_Internalname = sPrefix+"vCONFIG_"+sGXsfl_42_fel_idx;
         edtavClonardados_Internalname = sPrefix+"vCLONARDADOS_"+sGXsfl_42_fel_idx;
      }

      protected void sendrow_422( )
      {
         SubsflControlProps_422( ) ;
         WB8A0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_42_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_42_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_42_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavAtualizar_Enabled!=0)&&(edtavAtualizar_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 43,'"+sPrefix+"',false,'',42)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV45Atualizar_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV45Atualizar))&&String.IsNullOrEmpty(StringUtil.RTrim( AV84Atualizar_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV45Atualizar)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavAtualizar_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV45Atualizar)) ? AV84Atualizar_GXI : context.PathToRelativeUrl( AV45Atualizar)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavAtualizar_Enabled,(String)"",(String)edtavAtualizar_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavAtualizar_Jsonclick,"'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVATUALIZAR.CLICK."+sGXsfl_42_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV45Atualizar_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV29Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV85Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV85Delete_GXI : context.PathToRelativeUrl( AV29Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDelete_Enabled,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV29Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratadaUsuario_ContratadaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A66ContratadaUsuario_ContratadaCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratadaUsuario_ContratadaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)42,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratadaUsuario_UsuarioCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A69ContratadaUsuario_UsuarioCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratadaUsuario_UsuarioCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)42,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratadaUsuario_UsuarioPessoaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A70ContratadaUsuario_UsuarioPessoaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A70ContratadaUsuario_UsuarioPessoaCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratadaUsuario_UsuarioPessoaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)42,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUsuario_UserGamGuid_Internalname,StringUtil.RTrim( A341Usuario_UserGamGuid),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUsuario_UserGamGuid_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)40,(short)0,(short)0,(short)42,(short)1,(short)-1,(short)0,(bool)true,(String)"GAMGUID",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUsuario_Nome_Internalname,StringUtil.RTrim( A2Usuario_Nome),StringUtil.RTrim( context.localUtil.Format( A2Usuario_Nome, "@!")),(String)"","'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EUSUARIO_NOME.CLICK."+sGXsfl_42_idx+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUsuario_Nome_Jsonclick,(short)5,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtUsuario_Nome_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)42,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratadaUsuario_UsuarioPessoaNom_Internalname,StringUtil.RTrim( A71ContratadaUsuario_UsuarioPessoaNom),StringUtil.RTrim( context.localUtil.Format( A71ContratadaUsuario_UsuarioPessoaNom, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratadaUsuario_UsuarioPessoaNom_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtContratadaUsuario_UsuarioPessoaNom_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)42,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratadaUsuario_UsuarioPessoaDoc_Internalname,(String)A491ContratadaUsuario_UsuarioPessoaDoc,(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratadaUsuario_UsuarioPessoaDoc_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtContratadaUsuario_UsuarioPessoaDoc_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)42,(short)1,(short)-1,(short)-1,(bool)true,(String)"Docto",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavGamemail_Enabled!=0)&&(edtavGamemail_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 52,'"+sPrefix+"',false,'"+sGXsfl_42_idx+"',42)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavGamemail_Internalname,(String)AV43GAMEMail,(String)"",TempTags+((edtavGamemail_Enabled!=0)&&(edtavGamemail_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavGamemail_Enabled!=0)&&(edtavGamemail_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,52);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavGamemail_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtavGamemail_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(int)edtavGamemail_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)42,(short)1,(short)-1,(short)0,(bool)true,(String)"GAMEMail",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavNotificar_Enabled!=0)&&(edtavNotificar_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 53,'"+sPrefix+"',false,'"+sGXsfl_42_idx+"',42)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavNotificar_Internalname,StringUtil.RTrim( AV80Notificar),(String)"",TempTags+((edtavNotificar_Enabled!=0)&&(edtavNotificar_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavNotificar_Enabled!=0)&&(edtavNotificar_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,53);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavNotificar_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtavNotificar_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(int)edtavNotificar_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)40,(short)0,(short)0,(short)42,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkContratadaUsuario_UsuarioAtivo_Internalname,StringUtil.BoolToStr( A1394ContratadaUsuario_UsuarioAtivo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavFerias_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavFerias_Enabled!=0)&&(edtavFerias_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 55,'"+sPrefix+"',false,'',42)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV68Ferias_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV68Ferias))&&String.IsNullOrEmpty(StringUtil.RTrim( AV86Ferias_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV68Ferias)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavFerias_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV68Ferias)) ? AV86Ferias_GXI : context.PathToRelativeUrl( AV68Ferias)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavFerias_Visible,(int)edtavFerias_Enabled,(String)"",(String)edtavFerias_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavFerias_Jsonclick,"'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVFERIAS.CLICK."+sGXsfl_42_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV68Ferias_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavAssociarperfil_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavAssociarperfil_Enabled!=0)&&(edtavAssociarperfil_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 56,'"+sPrefix+"',false,'',42)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV33AssociarPerfil_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV33AssociarPerfil))&&String.IsNullOrEmpty(StringUtil.RTrim( AV87Associarperfil_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV33AssociarPerfil)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavAssociarperfil_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV33AssociarPerfil)) ? AV87Associarperfil_GXI : context.PathToRelativeUrl( AV33AssociarPerfil)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavAssociarperfil_Visible,(int)edtavAssociarperfil_Enabled,(String)"",(String)edtavAssociarperfil_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)7,(String)edtavAssociarperfil_Jsonclick,(String)"'"+sPrefix+"'"+",false,"+"'"+"e268a2_client"+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV33AssociarPerfil_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavAssociarservico_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavAssociarservico_Enabled!=0)&&(edtavAssociarservico_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 57,'"+sPrefix+"',false,'',42)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV42AssociarServico_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV42AssociarServico))&&String.IsNullOrEmpty(StringUtil.RTrim( AV88Associarservico_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV42AssociarServico)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavAssociarservico_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV42AssociarServico)) ? AV88Associarservico_GXI : context.PathToRelativeUrl( AV42AssociarServico)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavAssociarservico_Visible,(int)edtavAssociarservico_Enabled,(String)"",(String)edtavAssociarservico_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)7,(String)edtavAssociarservico_Jsonclick,(String)"'"+sPrefix+"'"+",false,"+"'"+"e278a2_client"+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV42AssociarServico_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavConfig_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV41Config_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV41Config))&&String.IsNullOrEmpty(StringUtil.RTrim( AV89Config_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV41Config)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavConfig_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV41Config)) ? AV89Config_GXI : context.PathToRelativeUrl( AV41Config)),(String)edtavConfig_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavConfig_Visible,(int)edtavConfig_Enabled,(String)"",(String)edtavConfig_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV41Config_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavClonardados_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavClonardados_Enabled!=0)&&(edtavClonardados_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 59,'"+sPrefix+"',false,'',42)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV76ClonarDados_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV76ClonarDados))&&String.IsNullOrEmpty(StringUtil.RTrim( AV90Clonardados_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV76ClonarDados)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavClonardados_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV76ClonarDados)) ? AV90Clonardados_GXI : context.PathToRelativeUrl( AV76ClonarDados)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavClonardados_Visible,(int)edtavClonardados_Enabled,(String)"",(String)edtavClonardados_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)7,(String)edtavClonardados_Jsonclick,(String)"'"+sPrefix+"'"+",false,"+"'"+"e288a2_client"+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV76ClonarDados_IsBlob,(bool)false});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADAUSUARIO_USUARIOCOD"+"_"+sGXsfl_42_idx, GetSecureSignedToken( sPrefix+sGXsfl_42_idx, context.localUtil.Format( (decimal)(A69ContratadaUsuario_UsuarioCod), "ZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_42_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_42_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_42_idx+1));
            sGXsfl_42_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_42_idx), 4, 0)), 4, "0");
            SubsflControlProps_422( ) ;
         }
         /* End function sendrow_422 */
      }

      protected void init_default_properties( )
      {
         bttBtnimportar_Internalname = sPrefix+"BTNIMPORTAR";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         lblOrderedtext_Internalname = sPrefix+"ORDEREDTEXT";
         cmbavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         imgCleanfilters_Internalname = sPrefix+"CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = sPrefix+"DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE1";
         edtavContratadausuario_usuariopessoanom1_Internalname = sPrefix+"vCONTRATADAUSUARIO_USUARIOPESSOANOM1";
         tblTabledynamicfilters_Internalname = sPrefix+"TABLEDYNAMICFILTERS";
         tblTablefilters_Internalname = sPrefix+"TABLEFILTERS";
         tblTablesearch_Internalname = sPrefix+"TABLESEARCH";
         edtavAtualizar_Internalname = sPrefix+"vATUALIZAR";
         edtavDelete_Internalname = sPrefix+"vDELETE";
         edtContratadaUsuario_ContratadaCod_Internalname = sPrefix+"CONTRATADAUSUARIO_CONTRATADACOD";
         edtContratadaUsuario_UsuarioCod_Internalname = sPrefix+"CONTRATADAUSUARIO_USUARIOCOD";
         edtContratadaUsuario_UsuarioPessoaCod_Internalname = sPrefix+"CONTRATADAUSUARIO_USUARIOPESSOACOD";
         edtUsuario_UserGamGuid_Internalname = sPrefix+"USUARIO_USERGAMGUID";
         edtUsuario_Nome_Internalname = sPrefix+"USUARIO_NOME";
         edtContratadaUsuario_UsuarioPessoaNom_Internalname = sPrefix+"CONTRATADAUSUARIO_USUARIOPESSOANOM";
         edtContratadaUsuario_UsuarioPessoaDoc_Internalname = sPrefix+"CONTRATADAUSUARIO_USUARIOPESSOADOC";
         edtavGamemail_Internalname = sPrefix+"vGAMEMAIL";
         edtavNotificar_Internalname = sPrefix+"vNOTIFICAR";
         chkContratadaUsuario_UsuarioAtivo_Internalname = sPrefix+"CONTRATADAUSUARIO_USUARIOATIVO";
         edtavFerias_Internalname = sPrefix+"vFERIAS";
         edtavAssociarperfil_Internalname = sPrefix+"vASSOCIARPERFIL";
         edtavAssociarservico_Internalname = sPrefix+"vASSOCIARSERVICO";
         edtavConfig_Internalname = sPrefix+"vCONFIG";
         edtavClonardados_Internalname = sPrefix+"vCLONARDADOS";
         imgInsert_Internalname = sPrefix+"INSERT";
         tblTablemergedgrid_Internalname = sPrefix+"TABLEMERGEDGRID";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         edtavTfusuario_nome_Internalname = sPrefix+"vTFUSUARIO_NOME";
         edtavTfusuario_nome_sel_Internalname = sPrefix+"vTFUSUARIO_NOME_SEL";
         edtavTfcontratadausuario_usuariopessoanom_Internalname = sPrefix+"vTFCONTRATADAUSUARIO_USUARIOPESSOANOM";
         edtavTfcontratadausuario_usuariopessoanom_sel_Internalname = sPrefix+"vTFCONTRATADAUSUARIO_USUARIOPESSOANOM_SEL";
         edtavTfcontratadausuario_usuariopessoadoc_Internalname = sPrefix+"vTFCONTRATADAUSUARIO_USUARIOPESSOADOC";
         edtavTfcontratadausuario_usuariopessoadoc_sel_Internalname = sPrefix+"vTFCONTRATADAUSUARIO_USUARIOPESSOADOC_SEL";
         edtavTfcontratadausuario_usuarioativo_sel_Internalname = sPrefix+"vTFCONTRATADAUSUARIO_USUARIOATIVO_SEL";
         Ddo_usuario_nome_Internalname = sPrefix+"DDO_USUARIO_NOME";
         edtavDdo_usuario_nometitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE";
         Ddo_contratadausuario_usuariopessoanom_Internalname = sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM";
         edtavDdo_contratadausuario_usuariopessoanomtitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATADAUSUARIO_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE";
         Ddo_contratadausuario_usuariopessoadoc_Internalname = sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC";
         edtavDdo_contratadausuario_usuariopessoadoctitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATADAUSUARIO_USUARIOPESSOADOCTITLECONTROLIDTOREPLACE";
         Ddo_contratadausuario_usuarioativo_Internalname = sPrefix+"DDO_CONTRATADAUSUARIO_USUARIOATIVO";
         edtavDdo_contratadausuario_usuarioativotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATADAUSUARIO_USUARIOATIVOTITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtavClonardados_Jsonclick = "";
         edtavAssociarservico_Jsonclick = "";
         edtavAssociarperfil_Jsonclick = "";
         edtavFerias_Jsonclick = "";
         edtavNotificar_Jsonclick = "";
         edtavNotificar_Visible = -1;
         edtavGamemail_Jsonclick = "";
         edtavGamemail_Visible = -1;
         edtContratadaUsuario_UsuarioPessoaDoc_Jsonclick = "";
         edtContratadaUsuario_UsuarioPessoaNom_Jsonclick = "";
         edtUsuario_Nome_Jsonclick = "";
         edtUsuario_UserGamGuid_Jsonclick = "";
         edtContratadaUsuario_UsuarioPessoaCod_Jsonclick = "";
         edtContratadaUsuario_UsuarioCod_Jsonclick = "";
         edtContratadaUsuario_ContratadaCod_Jsonclick = "";
         edtavAtualizar_Jsonclick = "";
         edtavAtualizar_Visible = -1;
         bttBtnimportar_Enabled = 1;
         edtavContratadausuario_usuariopessoanom1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         imgInsert_Enabled = 1;
         imgInsert_Bitmap = (String)(context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )));
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavClonardados_Tooltiptext = "Clonar dados neste usu�rio!";
         edtavClonardados_Enabled = 1;
         edtavConfig_Tooltiptext = "Permiss�es do usu�rio";
         edtavConfig_Link = "";
         edtavConfig_Enabled = 1;
         edtavAssociarservico_Tooltiptext = "Clique aqui p/ associar os servi�os deste usu�rio!";
         edtavAssociarservico_Enabled = 1;
         edtavAssociarperfil_Tooltiptext = "Clique aqui p/ associar os perfis deste usu�rio!";
         edtavAssociarperfil_Enabled = 1;
         edtavFerias_Enabled = 1;
         edtavNotificar_Enabled = 1;
         edtavNotificar_Forecolor = (int)(0xFFFFFF);
         edtavGamemail_Enabled = 1;
         edtavGamemail_Forecolor = (int)(0xFFFFFF);
         edtContratadaUsuario_UsuarioPessoaDoc_Forecolor = (int)(0xFFFFFF);
         edtContratadaUsuario_UsuarioPessoaNom_Forecolor = (int)(0xFFFFFF);
         edtUsuario_Nome_Forecolor = (int)(0xFFFFFF);
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavDelete_Enabled = 1;
         edtavAtualizar_Tooltiptext = "";
         edtavAtualizar_Enabled = 1;
         edtavFerias_Visible = -1;
         chkContratadaUsuario_UsuarioAtivo_Titleformat = 0;
         edtContratadaUsuario_UsuarioPessoaDoc_Titleformat = 0;
         edtContratadaUsuario_UsuarioPessoaNom_Titleformat = 0;
         edtUsuario_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavFerias_Tooltiptext = "Clique aqui p/ marcar o usu�rio como ausente";
         edtavContratadausuario_usuariopessoanom1_Visible = 1;
         bttBtnimportar_Tooltiptext = "Importar Usu�rios de outra �rea de Trabalho";
         imgInsert_Tooltiptext = "Inserir";
         edtavConfig_Visible = -1;
         edtavAssociarperfil_Visible = -1;
         edtavClonardados_Visible = -1;
         edtavClonardados_Title = "";
         edtavFerias_Title = "";
         edtavConfig_Title = "";
         edtavAssociarservico_Title = "";
         edtavAssociarperfil_Title = "";
         edtavAssociarservico_Visible = -1;
         chkContratadaUsuario_UsuarioAtivo.Title.Text = "Ativo";
         edtContratadaUsuario_UsuarioPessoaDoc_Title = "CPF";
         edtContratadaUsuario_UsuarioPessoaNom_Title = "Pessoa";
         edtUsuario_Nome_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkContratadaUsuario_UsuarioAtivo.Caption = "";
         edtavDdo_contratadausuario_usuarioativotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratadausuario_usuariopessoadoctitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratadausuario_usuariopessoanomtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_usuario_nometitlecontrolidtoreplace_Visible = 1;
         edtavTfcontratadausuario_usuarioativo_sel_Jsonclick = "";
         edtavTfcontratadausuario_usuarioativo_sel_Visible = 1;
         edtavTfcontratadausuario_usuariopessoadoc_sel_Jsonclick = "";
         edtavTfcontratadausuario_usuariopessoadoc_sel_Visible = 1;
         edtavTfcontratadausuario_usuariopessoadoc_Jsonclick = "";
         edtavTfcontratadausuario_usuariopessoadoc_Visible = 1;
         edtavTfcontratadausuario_usuariopessoanom_sel_Jsonclick = "";
         edtavTfcontratadausuario_usuariopessoanom_sel_Visible = 1;
         edtavTfcontratadausuario_usuariopessoanom_Jsonclick = "";
         edtavTfcontratadausuario_usuariopessoanom_Visible = 1;
         edtavTfusuario_nome_sel_Jsonclick = "";
         edtavTfusuario_nome_sel_Visible = 1;
         edtavTfusuario_nome_Jsonclick = "";
         edtavTfusuario_nome_Visible = 1;
         Ddo_contratadausuario_usuarioativo_Searchbuttontext = "Pesquisar";
         Ddo_contratadausuario_usuarioativo_Cleanfilter = "Limpar pesquisa";
         Ddo_contratadausuario_usuarioativo_Sortdsc = "Ordenar de Z � A";
         Ddo_contratadausuario_usuarioativo_Sortasc = "Ordenar de A � Z";
         Ddo_contratadausuario_usuarioativo_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_contratadausuario_usuarioativo_Datalisttype = "FixedValues";
         Ddo_contratadausuario_usuarioativo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratadausuario_usuarioativo_Includefilter = Convert.ToBoolean( 0);
         Ddo_contratadausuario_usuarioativo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratadausuario_usuarioativo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratadausuario_usuarioativo_Titlecontrolidtoreplace = "";
         Ddo_contratadausuario_usuarioativo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratadausuario_usuarioativo_Cls = "ColumnSettings";
         Ddo_contratadausuario_usuarioativo_Tooltip = "Op��es";
         Ddo_contratadausuario_usuarioativo_Caption = "";
         Ddo_contratadausuario_usuariopessoadoc_Searchbuttontext = "Pesquisar";
         Ddo_contratadausuario_usuariopessoadoc_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratadausuario_usuariopessoadoc_Cleanfilter = "Limpar pesquisa";
         Ddo_contratadausuario_usuariopessoadoc_Loadingdata = "Carregando dados...";
         Ddo_contratadausuario_usuariopessoadoc_Sortdsc = "Ordenar de Z � A";
         Ddo_contratadausuario_usuariopessoadoc_Sortasc = "Ordenar de A � Z";
         Ddo_contratadausuario_usuariopessoadoc_Datalistupdateminimumcharacters = 0;
         Ddo_contratadausuario_usuariopessoadoc_Datalistproc = "GetContratadaContratadaUsuarioWCFilterData";
         Ddo_contratadausuario_usuariopessoadoc_Datalisttype = "Dynamic";
         Ddo_contratadausuario_usuariopessoadoc_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratadausuario_usuariopessoadoc_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratadausuario_usuariopessoadoc_Filtertype = "Character";
         Ddo_contratadausuario_usuariopessoadoc_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratadausuario_usuariopessoadoc_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratadausuario_usuariopessoadoc_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratadausuario_usuariopessoadoc_Titlecontrolidtoreplace = "";
         Ddo_contratadausuario_usuariopessoadoc_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratadausuario_usuariopessoadoc_Cls = "ColumnSettings";
         Ddo_contratadausuario_usuariopessoadoc_Tooltip = "Op��es";
         Ddo_contratadausuario_usuariopessoadoc_Caption = "";
         Ddo_contratadausuario_usuariopessoanom_Searchbuttontext = "Pesquisar";
         Ddo_contratadausuario_usuariopessoanom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratadausuario_usuariopessoanom_Cleanfilter = "Limpar pesquisa";
         Ddo_contratadausuario_usuariopessoanom_Loadingdata = "Carregando dados...";
         Ddo_contratadausuario_usuariopessoanom_Sortdsc = "Ordenar de Z � A";
         Ddo_contratadausuario_usuariopessoanom_Sortasc = "Ordenar de A � Z";
         Ddo_contratadausuario_usuariopessoanom_Datalistupdateminimumcharacters = 0;
         Ddo_contratadausuario_usuariopessoanom_Datalistproc = "GetContratadaContratadaUsuarioWCFilterData";
         Ddo_contratadausuario_usuariopessoanom_Datalisttype = "Dynamic";
         Ddo_contratadausuario_usuariopessoanom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratadausuario_usuariopessoanom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratadausuario_usuariopessoanom_Filtertype = "Character";
         Ddo_contratadausuario_usuariopessoanom_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratadausuario_usuariopessoanom_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratadausuario_usuariopessoanom_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratadausuario_usuariopessoanom_Titlecontrolidtoreplace = "";
         Ddo_contratadausuario_usuariopessoanom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratadausuario_usuariopessoanom_Cls = "ColumnSettings";
         Ddo_contratadausuario_usuariopessoanom_Tooltip = "Op��es";
         Ddo_contratadausuario_usuariopessoanom_Caption = "";
         Ddo_usuario_nome_Searchbuttontext = "Pesquisar";
         Ddo_usuario_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_usuario_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_usuario_nome_Loadingdata = "Carregando dados...";
         Ddo_usuario_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_usuario_nome_Sortasc = "Ordenar de A � Z";
         Ddo_usuario_nome_Datalistupdateminimumcharacters = 0;
         Ddo_usuario_nome_Datalistproc = "GetContratadaContratadaUsuarioWCFilterData";
         Ddo_usuario_nome_Datalisttype = "Dynamic";
         Ddo_usuario_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_usuario_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_usuario_nome_Filtertype = "Character";
         Ddo_usuario_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_usuario_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_usuario_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_usuario_nome_Titlecontrolidtoreplace = "";
         Ddo_usuario_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_usuario_nome_Cls = "ColumnSettings";
         Ddo_usuario_nome_Tooltip = "Op��es";
         Ddo_usuario_nome_Caption = "";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A71ContratadaUsuario_UsuarioPessoaNom',fld:'CONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'A1394ContratadaUsuario_UsuarioAtivo',fld:'CONTRATADAUSUARIO_USUARIOATIVO',pic:'',nv:false},{av:'A1908Usuario_DeFerias',fld:'USUARIO_DEFERIAS',pic:'',nv:false},{av:'A341Usuario_UserGamGuid',fld:'USUARIO_USERGAMGUID',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'AV54ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratadaUsuario_UsuarioPessoaDocTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOPESSOADOCTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratadaUsuario_UsuarioAtivoTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A1136ContratoGestor_ContratadaCod',fld:'CONTRATOGESTOR_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV32ContratadaUsuario_ContratadaCod',fld:'vCONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A1013Contrato_PrepostoCod',fld:'CONTRATO_PREPOSTOCOD',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1135ContratoGestor_UsuarioEhContratante',fld:'CONTRATOGESTOR_USUARIOEHCONTRATANTE',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV91Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV52TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV53TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV56TFContratadaUsuario_UsuarioPessoaNom',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV57TFContratadaUsuario_UsuarioPessoaNom_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV60TFContratadaUsuario_UsuarioPessoaDoc',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOADOC',pic:'',nv:''},{av:'AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOADOC_SEL',pic:'',nv:''},{av:'AV74TFContratadaUsuario_UsuarioAtivo_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOATIVO_SEL',pic:'9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18ContratadaUsuario_UsuarioPessoaNom1',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''}],oparms:[{av:'AV51Usuario_NomeTitleFilterData',fld:'vUSUARIO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV55ContratadaUsuario_UsuarioPessoaNomTitleFilterData',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV59ContratadaUsuario_UsuarioPessoaDocTitleFilterData',fld:'vCONTRATADAUSUARIO_USUARIOPESSOADOCTITLEFILTERDATA',pic:'',nv:null},{av:'AV73ContratadaUsuario_UsuarioAtivoTitleFilterData',fld:'vCONTRATADAUSUARIO_USUARIOATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtUsuario_Nome_Titleformat',ctrl:'USUARIO_NOME',prop:'Titleformat'},{av:'edtUsuario_Nome_Title',ctrl:'USUARIO_NOME',prop:'Title'},{av:'edtContratadaUsuario_UsuarioPessoaNom_Titleformat',ctrl:'CONTRATADAUSUARIO_USUARIOPESSOANOM',prop:'Titleformat'},{av:'edtContratadaUsuario_UsuarioPessoaNom_Title',ctrl:'CONTRATADAUSUARIO_USUARIOPESSOANOM',prop:'Title'},{av:'edtContratadaUsuario_UsuarioPessoaDoc_Titleformat',ctrl:'CONTRATADAUSUARIO_USUARIOPESSOADOC',prop:'Titleformat'},{av:'edtContratadaUsuario_UsuarioPessoaDoc_Title',ctrl:'CONTRATADAUSUARIO_USUARIOPESSOADOC',prop:'Title'},{av:'chkContratadaUsuario_UsuarioAtivo_Titleformat',ctrl:'CONTRATADAUSUARIO_USUARIOATIVO',prop:'Titleformat'},{av:'chkContratadaUsuario_UsuarioAtivo.Title.Text',ctrl:'CONTRATADAUSUARIO_USUARIOATIVO',prop:'Title'},{av:'edtavAssociarservico_Visible',ctrl:'vASSOCIARSERVICO',prop:'Visible'},{av:'edtavAssociarperfil_Title',ctrl:'vASSOCIARPERFIL',prop:'Title'},{av:'edtavAssociarservico_Title',ctrl:'vASSOCIARSERVICO',prop:'Title'},{av:'edtavConfig_Title',ctrl:'vCONFIG',prop:'Title'},{av:'edtavFerias_Title',ctrl:'vFERIAS',prop:'Title'},{av:'edtavClonardados_Title',ctrl:'vCLONARDADOS',prop:'Title'},{av:'edtavClonardados_Visible',ctrl:'vCLONARDADOS',prop:'Visible'},{av:'edtavAssociarperfil_Visible',ctrl:'vASSOCIARPERFIL',prop:'Visible'},{av:'edtavConfig_Visible',ctrl:'vCONFIG',prop:'Visible'},{av:'imgInsert_Link',ctrl:'INSERT',prop:'Link'},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'},{ctrl:'BTNIMPORTAR',prop:'Enabled'},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("DDO_USUARIO_NOME.ONOPTIONCLICKED","{handler:'E118A2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18ContratadaUsuario_UsuarioPessoaNom1',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV52TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV53TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV56TFContratadaUsuario_UsuarioPessoaNom',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV57TFContratadaUsuario_UsuarioPessoaNom_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV60TFContratadaUsuario_UsuarioPessoaDoc',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOADOC',pic:'',nv:''},{av:'AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOADOC_SEL',pic:'',nv:''},{av:'AV74TFContratadaUsuario_UsuarioAtivo_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOATIVO_SEL',pic:'9',nv:0},{av:'AV32ContratadaUsuario_ContratadaCod',fld:'vCONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV54ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratadaUsuario_UsuarioPessoaDocTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOPESSOADOCTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratadaUsuario_UsuarioAtivoTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A1136ContratoGestor_ContratadaCod',fld:'CONTRATOGESTOR_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A1013Contrato_PrepostoCod',fld:'CONTRATO_PREPOSTOCOD',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1135ContratoGestor_UsuarioEhContratante',fld:'CONTRATOGESTOR_USUARIOEHCONTRATANTE',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV91Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A71ContratadaUsuario_UsuarioPessoaNom',fld:'CONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'A1394ContratadaUsuario_UsuarioAtivo',fld:'CONTRATADAUSUARIO_USUARIOATIVO',pic:'',nv:false},{av:'A1908Usuario_DeFerias',fld:'USUARIO_DEFERIAS',pic:'',nv:false},{av:'A341Usuario_UserGamGuid',fld:'USUARIO_USERGAMGUID',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_usuario_nome_Activeeventkey',ctrl:'DDO_USUARIO_NOME',prop:'ActiveEventKey'},{av:'Ddo_usuario_nome_Filteredtext_get',ctrl:'DDO_USUARIO_NOME',prop:'FilteredText_get'},{av:'Ddo_usuario_nome_Selectedvalue_get',ctrl:'DDO_USUARIO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_usuario_nome_Sortedstatus',ctrl:'DDO_USUARIO_NOME',prop:'SortedStatus'},{av:'AV52TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV53TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_contratadausuario_usuariopessoanom_Sortedstatus',ctrl:'DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratadausuario_usuariopessoadoc_Sortedstatus',ctrl:'DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC',prop:'SortedStatus'},{av:'Ddo_contratadausuario_usuarioativo_Sortedstatus',ctrl:'DDO_CONTRATADAUSUARIO_USUARIOATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM.ONOPTIONCLICKED","{handler:'E128A2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18ContratadaUsuario_UsuarioPessoaNom1',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV52TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV53TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV56TFContratadaUsuario_UsuarioPessoaNom',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV57TFContratadaUsuario_UsuarioPessoaNom_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV60TFContratadaUsuario_UsuarioPessoaDoc',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOADOC',pic:'',nv:''},{av:'AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOADOC_SEL',pic:'',nv:''},{av:'AV74TFContratadaUsuario_UsuarioAtivo_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOATIVO_SEL',pic:'9',nv:0},{av:'AV32ContratadaUsuario_ContratadaCod',fld:'vCONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV54ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratadaUsuario_UsuarioPessoaDocTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOPESSOADOCTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratadaUsuario_UsuarioAtivoTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A1136ContratoGestor_ContratadaCod',fld:'CONTRATOGESTOR_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A1013Contrato_PrepostoCod',fld:'CONTRATO_PREPOSTOCOD',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1135ContratoGestor_UsuarioEhContratante',fld:'CONTRATOGESTOR_USUARIOEHCONTRATANTE',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV91Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A71ContratadaUsuario_UsuarioPessoaNom',fld:'CONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'A1394ContratadaUsuario_UsuarioAtivo',fld:'CONTRATADAUSUARIO_USUARIOATIVO',pic:'',nv:false},{av:'A1908Usuario_DeFerias',fld:'USUARIO_DEFERIAS',pic:'',nv:false},{av:'A341Usuario_UserGamGuid',fld:'USUARIO_USERGAMGUID',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_contratadausuario_usuariopessoanom_Activeeventkey',ctrl:'DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM',prop:'ActiveEventKey'},{av:'Ddo_contratadausuario_usuariopessoanom_Filteredtext_get',ctrl:'DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM',prop:'FilteredText_get'},{av:'Ddo_contratadausuario_usuariopessoanom_Selectedvalue_get',ctrl:'DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratadausuario_usuariopessoanom_Sortedstatus',ctrl:'DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM',prop:'SortedStatus'},{av:'AV56TFContratadaUsuario_UsuarioPessoaNom',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV57TFContratadaUsuario_UsuarioPessoaNom_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'Ddo_usuario_nome_Sortedstatus',ctrl:'DDO_USUARIO_NOME',prop:'SortedStatus'},{av:'Ddo_contratadausuario_usuariopessoadoc_Sortedstatus',ctrl:'DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC',prop:'SortedStatus'},{av:'Ddo_contratadausuario_usuarioativo_Sortedstatus',ctrl:'DDO_CONTRATADAUSUARIO_USUARIOATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC.ONOPTIONCLICKED","{handler:'E138A2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18ContratadaUsuario_UsuarioPessoaNom1',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV52TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV53TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV56TFContratadaUsuario_UsuarioPessoaNom',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV57TFContratadaUsuario_UsuarioPessoaNom_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV60TFContratadaUsuario_UsuarioPessoaDoc',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOADOC',pic:'',nv:''},{av:'AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOADOC_SEL',pic:'',nv:''},{av:'AV74TFContratadaUsuario_UsuarioAtivo_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOATIVO_SEL',pic:'9',nv:0},{av:'AV32ContratadaUsuario_ContratadaCod',fld:'vCONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV54ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratadaUsuario_UsuarioPessoaDocTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOPESSOADOCTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratadaUsuario_UsuarioAtivoTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A1136ContratoGestor_ContratadaCod',fld:'CONTRATOGESTOR_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A1013Contrato_PrepostoCod',fld:'CONTRATO_PREPOSTOCOD',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1135ContratoGestor_UsuarioEhContratante',fld:'CONTRATOGESTOR_USUARIOEHCONTRATANTE',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV91Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A71ContratadaUsuario_UsuarioPessoaNom',fld:'CONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'A1394ContratadaUsuario_UsuarioAtivo',fld:'CONTRATADAUSUARIO_USUARIOATIVO',pic:'',nv:false},{av:'A1908Usuario_DeFerias',fld:'USUARIO_DEFERIAS',pic:'',nv:false},{av:'A341Usuario_UserGamGuid',fld:'USUARIO_USERGAMGUID',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_contratadausuario_usuariopessoadoc_Activeeventkey',ctrl:'DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC',prop:'ActiveEventKey'},{av:'Ddo_contratadausuario_usuariopessoadoc_Filteredtext_get',ctrl:'DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC',prop:'FilteredText_get'},{av:'Ddo_contratadausuario_usuariopessoadoc_Selectedvalue_get',ctrl:'DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratadausuario_usuariopessoadoc_Sortedstatus',ctrl:'DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC',prop:'SortedStatus'},{av:'AV60TFContratadaUsuario_UsuarioPessoaDoc',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOADOC',pic:'',nv:''},{av:'AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOADOC_SEL',pic:'',nv:''},{av:'Ddo_usuario_nome_Sortedstatus',ctrl:'DDO_USUARIO_NOME',prop:'SortedStatus'},{av:'Ddo_contratadausuario_usuariopessoanom_Sortedstatus',ctrl:'DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratadausuario_usuarioativo_Sortedstatus',ctrl:'DDO_CONTRATADAUSUARIO_USUARIOATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATADAUSUARIO_USUARIOATIVO.ONOPTIONCLICKED","{handler:'E148A2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18ContratadaUsuario_UsuarioPessoaNom1',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV52TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV53TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV56TFContratadaUsuario_UsuarioPessoaNom',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV57TFContratadaUsuario_UsuarioPessoaNom_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV60TFContratadaUsuario_UsuarioPessoaDoc',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOADOC',pic:'',nv:''},{av:'AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOADOC_SEL',pic:'',nv:''},{av:'AV74TFContratadaUsuario_UsuarioAtivo_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOATIVO_SEL',pic:'9',nv:0},{av:'AV32ContratadaUsuario_ContratadaCod',fld:'vCONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV54ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratadaUsuario_UsuarioPessoaDocTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOPESSOADOCTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratadaUsuario_UsuarioAtivoTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A1136ContratoGestor_ContratadaCod',fld:'CONTRATOGESTOR_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A1013Contrato_PrepostoCod',fld:'CONTRATO_PREPOSTOCOD',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1135ContratoGestor_UsuarioEhContratante',fld:'CONTRATOGESTOR_USUARIOEHCONTRATANTE',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV91Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A71ContratadaUsuario_UsuarioPessoaNom',fld:'CONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'A1394ContratadaUsuario_UsuarioAtivo',fld:'CONTRATADAUSUARIO_USUARIOATIVO',pic:'',nv:false},{av:'A1908Usuario_DeFerias',fld:'USUARIO_DEFERIAS',pic:'',nv:false},{av:'A341Usuario_UserGamGuid',fld:'USUARIO_USERGAMGUID',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_contratadausuario_usuarioativo_Activeeventkey',ctrl:'DDO_CONTRATADAUSUARIO_USUARIOATIVO',prop:'ActiveEventKey'},{av:'Ddo_contratadausuario_usuarioativo_Selectedvalue_get',ctrl:'DDO_CONTRATADAUSUARIO_USUARIOATIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratadausuario_usuarioativo_Sortedstatus',ctrl:'DDO_CONTRATADAUSUARIO_USUARIOATIVO',prop:'SortedStatus'},{av:'AV74TFContratadaUsuario_UsuarioAtivo_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOATIVO_SEL',pic:'9',nv:0},{av:'Ddo_usuario_nome_Sortedstatus',ctrl:'DDO_USUARIO_NOME',prop:'SortedStatus'},{av:'Ddo_contratadausuario_usuariopessoanom_Sortedstatus',ctrl:'DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratadausuario_usuariopessoadoc_Sortedstatus',ctrl:'DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E228A2',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A71ContratadaUsuario_UsuarioPessoaNom',fld:'CONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'A1394ContratadaUsuario_UsuarioAtivo',fld:'CONTRATADAUSUARIO_USUARIOATIVO',pic:'',nv:false},{av:'A1908Usuario_DeFerias',fld:'USUARIO_DEFERIAS',pic:'',nv:false},{av:'A341Usuario_UserGamGuid',fld:'USUARIO_USERGAMGUID',pic:'',nv:''}],oparms:[{av:'edtavAtualizar_Tooltiptext',ctrl:'vATUALIZAR',prop:'Tooltiptext'},{av:'AV45Atualizar',fld:'vATUALIZAR',pic:'',nv:''},{av:'edtavAtualizar_Enabled',ctrl:'vATUALIZAR',prop:'Enabled'},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV29Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Enabled',ctrl:'vDELETE',prop:'Enabled'},{av:'edtavFerias_Tooltiptext',ctrl:'vFERIAS',prop:'Tooltiptext'},{av:'AV68Ferias',fld:'vFERIAS',pic:'',nv:''},{av:'edtavFerias_Enabled',ctrl:'vFERIAS',prop:'Enabled'},{av:'edtavAssociarperfil_Tooltiptext',ctrl:'vASSOCIARPERFIL',prop:'Tooltiptext'},{av:'AV33AssociarPerfil',fld:'vASSOCIARPERFIL',pic:'',nv:''},{av:'edtavAssociarperfil_Enabled',ctrl:'vASSOCIARPERFIL',prop:'Enabled'},{av:'edtavAssociarservico_Tooltiptext',ctrl:'vASSOCIARSERVICO',prop:'Tooltiptext'},{av:'AV42AssociarServico',fld:'vASSOCIARSERVICO',pic:'',nv:''},{av:'edtavAssociarservico_Enabled',ctrl:'vASSOCIARSERVICO',prop:'Enabled'},{av:'edtavConfig_Tooltiptext',ctrl:'vCONFIG',prop:'Tooltiptext'},{av:'edtavConfig_Link',ctrl:'vCONFIG',prop:'Link'},{av:'AV41Config',fld:'vCONFIG',pic:'',nv:''},{av:'edtavConfig_Enabled',ctrl:'vCONFIG',prop:'Enabled'},{av:'edtavClonardados_Tooltiptext',ctrl:'vCLONARDADOS',prop:'Tooltiptext'},{av:'AV76ClonarDados',fld:'vCLONARDADOS',pic:'',nv:''},{av:'edtavClonardados_Enabled',ctrl:'vCLONARDADOS',prop:'Enabled'},{av:'edtUsuario_Nome_Forecolor',ctrl:'USUARIO_NOME',prop:'Forecolor'},{av:'edtContratadaUsuario_UsuarioPessoaNom_Forecolor',ctrl:'CONTRATADAUSUARIO_USUARIOPESSOANOM',prop:'Forecolor'},{av:'edtContratadaUsuario_UsuarioPessoaDoc_Forecolor',ctrl:'CONTRATADAUSUARIO_USUARIOPESSOADOC',prop:'Forecolor'},{av:'edtavGamemail_Forecolor',ctrl:'vGAMEMAIL',prop:'Forecolor'},{av:'edtavNotificar_Forecolor',ctrl:'vNOTIFICAR',prop:'Forecolor'},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'},{av:'imgInsert_Tooltiptext',ctrl:'INSERT',prop:'Tooltiptext'},{ctrl:'BTNIMPORTAR',prop:'Enabled'},{ctrl:'BTNIMPORTAR',prop:'Tooltiptext'},{av:'edtavFerias_Visible',ctrl:'vFERIAS',prop:'Visible'},{av:'AV43GAMEMail',fld:'vGAMEMAIL',pic:'',nv:''},{av:'AV80Notificar',fld:'vNOTIFICAR',pic:'',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E158A2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18ContratadaUsuario_UsuarioPessoaNom1',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV52TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV53TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV56TFContratadaUsuario_UsuarioPessoaNom',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV57TFContratadaUsuario_UsuarioPessoaNom_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV60TFContratadaUsuario_UsuarioPessoaDoc',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOADOC',pic:'',nv:''},{av:'AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOADOC_SEL',pic:'',nv:''},{av:'AV74TFContratadaUsuario_UsuarioAtivo_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOATIVO_SEL',pic:'9',nv:0},{av:'AV32ContratadaUsuario_ContratadaCod',fld:'vCONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV54ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratadaUsuario_UsuarioPessoaDocTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOPESSOADOCTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratadaUsuario_UsuarioAtivoTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A1136ContratoGestor_ContratadaCod',fld:'CONTRATOGESTOR_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A1013Contrato_PrepostoCod',fld:'CONTRATO_PREPOSTOCOD',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1135ContratoGestor_UsuarioEhContratante',fld:'CONTRATOGESTOR_USUARIOEHCONTRATANTE',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV91Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A71ContratadaUsuario_UsuarioPessoaNom',fld:'CONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'A1394ContratadaUsuario_UsuarioAtivo',fld:'CONTRATADAUSUARIO_USUARIOATIVO',pic:'',nv:false},{av:'A1908Usuario_DeFerias',fld:'USUARIO_DEFERIAS',pic:'',nv:false},{av:'A341Usuario_UserGamGuid',fld:'USUARIO_USERGAMGUID',pic:'',nv:''},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E198A2',iparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavContratadausuario_usuariopessoanom1_Visible',ctrl:'vCONTRATADAUSUARIO_USUARIOPESSOANOM1',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E168A2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18ContratadaUsuario_UsuarioPessoaNom1',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV52TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV53TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV56TFContratadaUsuario_UsuarioPessoaNom',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV57TFContratadaUsuario_UsuarioPessoaNom_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV60TFContratadaUsuario_UsuarioPessoaDoc',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOADOC',pic:'',nv:''},{av:'AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOADOC_SEL',pic:'',nv:''},{av:'AV74TFContratadaUsuario_UsuarioAtivo_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOATIVO_SEL',pic:'9',nv:0},{av:'AV32ContratadaUsuario_ContratadaCod',fld:'vCONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV54ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratadaUsuario_UsuarioPessoaDocTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOPESSOADOCTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratadaUsuario_UsuarioAtivoTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A1136ContratoGestor_ContratadaCod',fld:'CONTRATOGESTOR_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A1013Contrato_PrepostoCod',fld:'CONTRATO_PREPOSTOCOD',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1135ContratoGestor_UsuarioEhContratante',fld:'CONTRATOGESTOR_USUARIOEHCONTRATANTE',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV91Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A71ContratadaUsuario_UsuarioPessoaNom',fld:'CONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'A1394ContratadaUsuario_UsuarioAtivo',fld:'CONTRATADAUSUARIO_USUARIOATIVO',pic:'',nv:false},{av:'A1908Usuario_DeFerias',fld:'USUARIO_DEFERIAS',pic:'',nv:false},{av:'A341Usuario_UserGamGuid',fld:'USUARIO_USERGAMGUID',pic:'',nv:''},{av:'sPrefix',nv:''}],oparms:[{av:'AV52TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'Ddo_usuario_nome_Filteredtext_set',ctrl:'DDO_USUARIO_NOME',prop:'FilteredText_set'},{av:'AV53TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_usuario_nome_Selectedvalue_set',ctrl:'DDO_USUARIO_NOME',prop:'SelectedValue_set'},{av:'AV56TFContratadaUsuario_UsuarioPessoaNom',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'Ddo_contratadausuario_usuariopessoanom_Filteredtext_set',ctrl:'DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM',prop:'FilteredText_set'},{av:'AV57TFContratadaUsuario_UsuarioPessoaNom_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'Ddo_contratadausuario_usuariopessoanom_Selectedvalue_set',ctrl:'DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM',prop:'SelectedValue_set'},{av:'AV60TFContratadaUsuario_UsuarioPessoaDoc',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOADOC',pic:'',nv:''},{av:'Ddo_contratadausuario_usuariopessoadoc_Filteredtext_set',ctrl:'DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC',prop:'FilteredText_set'},{av:'AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOADOC_SEL',pic:'',nv:''},{av:'Ddo_contratadausuario_usuariopessoadoc_Selectedvalue_set',ctrl:'DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC',prop:'SelectedValue_set'},{av:'AV74TFContratadaUsuario_UsuarioAtivo_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOATIVO_SEL',pic:'9',nv:0},{av:'Ddo_contratadausuario_usuarioativo_Selectedvalue_set',ctrl:'DDO_CONTRATADAUSUARIO_USUARIOATIVO',prop:'SelectedValue_set'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18ContratadaUsuario_UsuarioPessoaNom1',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavContratadausuario_usuariopessoanom1_Visible',ctrl:'vCONTRATADAUSUARIO_USUARIOPESSOANOM1',prop:'Visible'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E178A2',iparms:[{av:'AV32ContratadaUsuario_ContratadaCod',fld:'vCONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DOASSOCIARPERFIL'","{handler:'E268A2',iparms:[{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'DOASSOCIARSERVICO'","{handler:'E278A2',iparms:[{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DOCLONARDADOS'","{handler:'E288A2',iparms:[{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV32ContratadaUsuario_ContratadaCod',fld:'vCONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DOIMPORTAR'","{handler:'E188A2',iparms:[{av:'AV32ContratadaUsuario_ContratadaCod',fld:'vCONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("VATUALIZAR.CLICK","{handler:'E238A2',iparms:[{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("USUARIO_NOME.CLICK","{handler:'E248A2',iparms:[{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("VFERIAS.CLICK","{handler:'E258A2',iparms:[{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV91Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'AV68Ferias',fld:'vFERIAS',pic:'',nv:''},{av:'edtavFerias_Tooltiptext',ctrl:'vFERIAS',prop:'Tooltiptext'}]}");
         setEventMetadata("GRID_FIRSTPAGE","{handler:'subgrid_firstpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A71ContratadaUsuario_UsuarioPessoaNom',fld:'CONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'A1394ContratadaUsuario_UsuarioAtivo',fld:'CONTRATADAUSUARIO_USUARIOATIVO',pic:'',nv:false},{av:'A1908Usuario_DeFerias',fld:'USUARIO_DEFERIAS',pic:'',nv:false},{av:'A341Usuario_UserGamGuid',fld:'USUARIO_USERGAMGUID',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'AV54ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratadaUsuario_UsuarioPessoaDocTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOPESSOADOCTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratadaUsuario_UsuarioAtivoTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A1136ContratoGestor_ContratadaCod',fld:'CONTRATOGESTOR_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV32ContratadaUsuario_ContratadaCod',fld:'vCONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A1013Contrato_PrepostoCod',fld:'CONTRATO_PREPOSTOCOD',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1135ContratoGestor_UsuarioEhContratante',fld:'CONTRATOGESTOR_USUARIOEHCONTRATANTE',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV91Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV52TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV53TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV56TFContratadaUsuario_UsuarioPessoaNom',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV57TFContratadaUsuario_UsuarioPessoaNom_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV60TFContratadaUsuario_UsuarioPessoaDoc',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOADOC',pic:'',nv:''},{av:'AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOADOC_SEL',pic:'',nv:''},{av:'AV74TFContratadaUsuario_UsuarioAtivo_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOATIVO_SEL',pic:'9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18ContratadaUsuario_UsuarioPessoaNom1',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''}],oparms:[{av:'AV51Usuario_NomeTitleFilterData',fld:'vUSUARIO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV55ContratadaUsuario_UsuarioPessoaNomTitleFilterData',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV59ContratadaUsuario_UsuarioPessoaDocTitleFilterData',fld:'vCONTRATADAUSUARIO_USUARIOPESSOADOCTITLEFILTERDATA',pic:'',nv:null},{av:'AV73ContratadaUsuario_UsuarioAtivoTitleFilterData',fld:'vCONTRATADAUSUARIO_USUARIOATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtUsuario_Nome_Titleformat',ctrl:'USUARIO_NOME',prop:'Titleformat'},{av:'edtUsuario_Nome_Title',ctrl:'USUARIO_NOME',prop:'Title'},{av:'edtContratadaUsuario_UsuarioPessoaNom_Titleformat',ctrl:'CONTRATADAUSUARIO_USUARIOPESSOANOM',prop:'Titleformat'},{av:'edtContratadaUsuario_UsuarioPessoaNom_Title',ctrl:'CONTRATADAUSUARIO_USUARIOPESSOANOM',prop:'Title'},{av:'edtContratadaUsuario_UsuarioPessoaDoc_Titleformat',ctrl:'CONTRATADAUSUARIO_USUARIOPESSOADOC',prop:'Titleformat'},{av:'edtContratadaUsuario_UsuarioPessoaDoc_Title',ctrl:'CONTRATADAUSUARIO_USUARIOPESSOADOC',prop:'Title'},{av:'chkContratadaUsuario_UsuarioAtivo_Titleformat',ctrl:'CONTRATADAUSUARIO_USUARIOATIVO',prop:'Titleformat'},{av:'chkContratadaUsuario_UsuarioAtivo.Title.Text',ctrl:'CONTRATADAUSUARIO_USUARIOATIVO',prop:'Title'},{av:'edtavAssociarservico_Visible',ctrl:'vASSOCIARSERVICO',prop:'Visible'},{av:'edtavAssociarperfil_Title',ctrl:'vASSOCIARPERFIL',prop:'Title'},{av:'edtavAssociarservico_Title',ctrl:'vASSOCIARSERVICO',prop:'Title'},{av:'edtavConfig_Title',ctrl:'vCONFIG',prop:'Title'},{av:'edtavFerias_Title',ctrl:'vFERIAS',prop:'Title'},{av:'edtavClonardados_Title',ctrl:'vCLONARDADOS',prop:'Title'},{av:'edtavClonardados_Visible',ctrl:'vCLONARDADOS',prop:'Visible'},{av:'edtavAssociarperfil_Visible',ctrl:'vASSOCIARPERFIL',prop:'Visible'},{av:'edtavConfig_Visible',ctrl:'vCONFIG',prop:'Visible'},{av:'imgInsert_Link',ctrl:'INSERT',prop:'Link'},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'},{ctrl:'BTNIMPORTAR',prop:'Enabled'},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRID_PREVPAGE","{handler:'subgrid_previouspage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A71ContratadaUsuario_UsuarioPessoaNom',fld:'CONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'A1394ContratadaUsuario_UsuarioAtivo',fld:'CONTRATADAUSUARIO_USUARIOATIVO',pic:'',nv:false},{av:'A1908Usuario_DeFerias',fld:'USUARIO_DEFERIAS',pic:'',nv:false},{av:'A341Usuario_UserGamGuid',fld:'USUARIO_USERGAMGUID',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'AV54ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratadaUsuario_UsuarioPessoaDocTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOPESSOADOCTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratadaUsuario_UsuarioAtivoTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A1136ContratoGestor_ContratadaCod',fld:'CONTRATOGESTOR_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV32ContratadaUsuario_ContratadaCod',fld:'vCONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A1013Contrato_PrepostoCod',fld:'CONTRATO_PREPOSTOCOD',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1135ContratoGestor_UsuarioEhContratante',fld:'CONTRATOGESTOR_USUARIOEHCONTRATANTE',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV91Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV52TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV53TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV56TFContratadaUsuario_UsuarioPessoaNom',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV57TFContratadaUsuario_UsuarioPessoaNom_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV60TFContratadaUsuario_UsuarioPessoaDoc',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOADOC',pic:'',nv:''},{av:'AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOADOC_SEL',pic:'',nv:''},{av:'AV74TFContratadaUsuario_UsuarioAtivo_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOATIVO_SEL',pic:'9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18ContratadaUsuario_UsuarioPessoaNom1',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''}],oparms:[{av:'AV51Usuario_NomeTitleFilterData',fld:'vUSUARIO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV55ContratadaUsuario_UsuarioPessoaNomTitleFilterData',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV59ContratadaUsuario_UsuarioPessoaDocTitleFilterData',fld:'vCONTRATADAUSUARIO_USUARIOPESSOADOCTITLEFILTERDATA',pic:'',nv:null},{av:'AV73ContratadaUsuario_UsuarioAtivoTitleFilterData',fld:'vCONTRATADAUSUARIO_USUARIOATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtUsuario_Nome_Titleformat',ctrl:'USUARIO_NOME',prop:'Titleformat'},{av:'edtUsuario_Nome_Title',ctrl:'USUARIO_NOME',prop:'Title'},{av:'edtContratadaUsuario_UsuarioPessoaNom_Titleformat',ctrl:'CONTRATADAUSUARIO_USUARIOPESSOANOM',prop:'Titleformat'},{av:'edtContratadaUsuario_UsuarioPessoaNom_Title',ctrl:'CONTRATADAUSUARIO_USUARIOPESSOANOM',prop:'Title'},{av:'edtContratadaUsuario_UsuarioPessoaDoc_Titleformat',ctrl:'CONTRATADAUSUARIO_USUARIOPESSOADOC',prop:'Titleformat'},{av:'edtContratadaUsuario_UsuarioPessoaDoc_Title',ctrl:'CONTRATADAUSUARIO_USUARIOPESSOADOC',prop:'Title'},{av:'chkContratadaUsuario_UsuarioAtivo_Titleformat',ctrl:'CONTRATADAUSUARIO_USUARIOATIVO',prop:'Titleformat'},{av:'chkContratadaUsuario_UsuarioAtivo.Title.Text',ctrl:'CONTRATADAUSUARIO_USUARIOATIVO',prop:'Title'},{av:'edtavAssociarservico_Visible',ctrl:'vASSOCIARSERVICO',prop:'Visible'},{av:'edtavAssociarperfil_Title',ctrl:'vASSOCIARPERFIL',prop:'Title'},{av:'edtavAssociarservico_Title',ctrl:'vASSOCIARSERVICO',prop:'Title'},{av:'edtavConfig_Title',ctrl:'vCONFIG',prop:'Title'},{av:'edtavFerias_Title',ctrl:'vFERIAS',prop:'Title'},{av:'edtavClonardados_Title',ctrl:'vCLONARDADOS',prop:'Title'},{av:'edtavClonardados_Visible',ctrl:'vCLONARDADOS',prop:'Visible'},{av:'edtavAssociarperfil_Visible',ctrl:'vASSOCIARPERFIL',prop:'Visible'},{av:'edtavConfig_Visible',ctrl:'vCONFIG',prop:'Visible'},{av:'imgInsert_Link',ctrl:'INSERT',prop:'Link'},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'},{ctrl:'BTNIMPORTAR',prop:'Enabled'},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRID_NEXTPAGE","{handler:'subgrid_nextpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A71ContratadaUsuario_UsuarioPessoaNom',fld:'CONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'A1394ContratadaUsuario_UsuarioAtivo',fld:'CONTRATADAUSUARIO_USUARIOATIVO',pic:'',nv:false},{av:'A1908Usuario_DeFerias',fld:'USUARIO_DEFERIAS',pic:'',nv:false},{av:'A341Usuario_UserGamGuid',fld:'USUARIO_USERGAMGUID',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'AV54ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratadaUsuario_UsuarioPessoaDocTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOPESSOADOCTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratadaUsuario_UsuarioAtivoTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A1136ContratoGestor_ContratadaCod',fld:'CONTRATOGESTOR_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV32ContratadaUsuario_ContratadaCod',fld:'vCONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A1013Contrato_PrepostoCod',fld:'CONTRATO_PREPOSTOCOD',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1135ContratoGestor_UsuarioEhContratante',fld:'CONTRATOGESTOR_USUARIOEHCONTRATANTE',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV91Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV52TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV53TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV56TFContratadaUsuario_UsuarioPessoaNom',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV57TFContratadaUsuario_UsuarioPessoaNom_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV60TFContratadaUsuario_UsuarioPessoaDoc',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOADOC',pic:'',nv:''},{av:'AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOADOC_SEL',pic:'',nv:''},{av:'AV74TFContratadaUsuario_UsuarioAtivo_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOATIVO_SEL',pic:'9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18ContratadaUsuario_UsuarioPessoaNom1',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''}],oparms:[{av:'AV51Usuario_NomeTitleFilterData',fld:'vUSUARIO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV55ContratadaUsuario_UsuarioPessoaNomTitleFilterData',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV59ContratadaUsuario_UsuarioPessoaDocTitleFilterData',fld:'vCONTRATADAUSUARIO_USUARIOPESSOADOCTITLEFILTERDATA',pic:'',nv:null},{av:'AV73ContratadaUsuario_UsuarioAtivoTitleFilterData',fld:'vCONTRATADAUSUARIO_USUARIOATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtUsuario_Nome_Titleformat',ctrl:'USUARIO_NOME',prop:'Titleformat'},{av:'edtUsuario_Nome_Title',ctrl:'USUARIO_NOME',prop:'Title'},{av:'edtContratadaUsuario_UsuarioPessoaNom_Titleformat',ctrl:'CONTRATADAUSUARIO_USUARIOPESSOANOM',prop:'Titleformat'},{av:'edtContratadaUsuario_UsuarioPessoaNom_Title',ctrl:'CONTRATADAUSUARIO_USUARIOPESSOANOM',prop:'Title'},{av:'edtContratadaUsuario_UsuarioPessoaDoc_Titleformat',ctrl:'CONTRATADAUSUARIO_USUARIOPESSOADOC',prop:'Titleformat'},{av:'edtContratadaUsuario_UsuarioPessoaDoc_Title',ctrl:'CONTRATADAUSUARIO_USUARIOPESSOADOC',prop:'Title'},{av:'chkContratadaUsuario_UsuarioAtivo_Titleformat',ctrl:'CONTRATADAUSUARIO_USUARIOATIVO',prop:'Titleformat'},{av:'chkContratadaUsuario_UsuarioAtivo.Title.Text',ctrl:'CONTRATADAUSUARIO_USUARIOATIVO',prop:'Title'},{av:'edtavAssociarservico_Visible',ctrl:'vASSOCIARSERVICO',prop:'Visible'},{av:'edtavAssociarperfil_Title',ctrl:'vASSOCIARPERFIL',prop:'Title'},{av:'edtavAssociarservico_Title',ctrl:'vASSOCIARSERVICO',prop:'Title'},{av:'edtavConfig_Title',ctrl:'vCONFIG',prop:'Title'},{av:'edtavFerias_Title',ctrl:'vFERIAS',prop:'Title'},{av:'edtavClonardados_Title',ctrl:'vCLONARDADOS',prop:'Title'},{av:'edtavClonardados_Visible',ctrl:'vCLONARDADOS',prop:'Visible'},{av:'edtavAssociarperfil_Visible',ctrl:'vASSOCIARPERFIL',prop:'Visible'},{av:'edtavConfig_Visible',ctrl:'vCONFIG',prop:'Visible'},{av:'imgInsert_Link',ctrl:'INSERT',prop:'Link'},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'},{ctrl:'BTNIMPORTAR',prop:'Enabled'},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRID_LASTPAGE","{handler:'subgrid_lastpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A71ContratadaUsuario_UsuarioPessoaNom',fld:'CONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'A1394ContratadaUsuario_UsuarioAtivo',fld:'CONTRATADAUSUARIO_USUARIOATIVO',pic:'',nv:false},{av:'A1908Usuario_DeFerias',fld:'USUARIO_DEFERIAS',pic:'',nv:false},{av:'A341Usuario_UserGamGuid',fld:'USUARIO_USERGAMGUID',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'AV54ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratadaUsuario_UsuarioPessoaDocTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOPESSOADOCTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratadaUsuario_UsuarioAtivoTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A1136ContratoGestor_ContratadaCod',fld:'CONTRATOGESTOR_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV32ContratadaUsuario_ContratadaCod',fld:'vCONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A1013Contrato_PrepostoCod',fld:'CONTRATO_PREPOSTOCOD',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1135ContratoGestor_UsuarioEhContratante',fld:'CONTRATOGESTOR_USUARIOEHCONTRATANTE',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV91Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV52TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV53TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV56TFContratadaUsuario_UsuarioPessoaNom',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV57TFContratadaUsuario_UsuarioPessoaNom_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV60TFContratadaUsuario_UsuarioPessoaDoc',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOADOC',pic:'',nv:''},{av:'AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOADOC_SEL',pic:'',nv:''},{av:'AV74TFContratadaUsuario_UsuarioAtivo_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOATIVO_SEL',pic:'9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18ContratadaUsuario_UsuarioPessoaNom1',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''}],oparms:[{av:'AV51Usuario_NomeTitleFilterData',fld:'vUSUARIO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV55ContratadaUsuario_UsuarioPessoaNomTitleFilterData',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV59ContratadaUsuario_UsuarioPessoaDocTitleFilterData',fld:'vCONTRATADAUSUARIO_USUARIOPESSOADOCTITLEFILTERDATA',pic:'',nv:null},{av:'AV73ContratadaUsuario_UsuarioAtivoTitleFilterData',fld:'vCONTRATADAUSUARIO_USUARIOATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtUsuario_Nome_Titleformat',ctrl:'USUARIO_NOME',prop:'Titleformat'},{av:'edtUsuario_Nome_Title',ctrl:'USUARIO_NOME',prop:'Title'},{av:'edtContratadaUsuario_UsuarioPessoaNom_Titleformat',ctrl:'CONTRATADAUSUARIO_USUARIOPESSOANOM',prop:'Titleformat'},{av:'edtContratadaUsuario_UsuarioPessoaNom_Title',ctrl:'CONTRATADAUSUARIO_USUARIOPESSOANOM',prop:'Title'},{av:'edtContratadaUsuario_UsuarioPessoaDoc_Titleformat',ctrl:'CONTRATADAUSUARIO_USUARIOPESSOADOC',prop:'Titleformat'},{av:'edtContratadaUsuario_UsuarioPessoaDoc_Title',ctrl:'CONTRATADAUSUARIO_USUARIOPESSOADOC',prop:'Title'},{av:'chkContratadaUsuario_UsuarioAtivo_Titleformat',ctrl:'CONTRATADAUSUARIO_USUARIOATIVO',prop:'Titleformat'},{av:'chkContratadaUsuario_UsuarioAtivo.Title.Text',ctrl:'CONTRATADAUSUARIO_USUARIOATIVO',prop:'Title'},{av:'edtavAssociarservico_Visible',ctrl:'vASSOCIARSERVICO',prop:'Visible'},{av:'edtavAssociarperfil_Title',ctrl:'vASSOCIARPERFIL',prop:'Title'},{av:'edtavAssociarservico_Title',ctrl:'vASSOCIARSERVICO',prop:'Title'},{av:'edtavConfig_Title',ctrl:'vCONFIG',prop:'Title'},{av:'edtavFerias_Title',ctrl:'vFERIAS',prop:'Title'},{av:'edtavClonardados_Title',ctrl:'vCLONARDADOS',prop:'Title'},{av:'edtavClonardados_Visible',ctrl:'vCLONARDADOS',prop:'Visible'},{av:'edtavAssociarperfil_Visible',ctrl:'vASSOCIARPERFIL',prop:'Visible'},{av:'edtavConfig_Visible',ctrl:'vCONFIG',prop:'Visible'},{av:'imgInsert_Link',ctrl:'INSERT',prop:'Link'},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'},{ctrl:'BTNIMPORTAR',prop:'Enabled'},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         Ddo_usuario_nome_Activeeventkey = "";
         Ddo_usuario_nome_Filteredtext_get = "";
         Ddo_usuario_nome_Selectedvalue_get = "";
         Ddo_contratadausuario_usuariopessoanom_Activeeventkey = "";
         Ddo_contratadausuario_usuariopessoanom_Filteredtext_get = "";
         Ddo_contratadausuario_usuariopessoanom_Selectedvalue_get = "";
         Ddo_contratadausuario_usuariopessoadoc_Activeeventkey = "";
         Ddo_contratadausuario_usuariopessoadoc_Filteredtext_get = "";
         Ddo_contratadausuario_usuariopessoadoc_Selectedvalue_get = "";
         Ddo_contratadausuario_usuarioativo_Activeeventkey = "";
         Ddo_contratadausuario_usuarioativo_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV16DynamicFiltersSelector1 = "";
         AV18ContratadaUsuario_UsuarioPessoaNom1 = "";
         AV52TFUsuario_Nome = "";
         AV53TFUsuario_Nome_Sel = "";
         AV56TFContratadaUsuario_UsuarioPessoaNom = "";
         AV57TFContratadaUsuario_UsuarioPessoaNom_Sel = "";
         AV60TFContratadaUsuario_UsuarioPessoaDoc = "";
         AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel = "";
         AV54ddo_Usuario_NomeTitleControlIdToReplace = "";
         AV58ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace = "";
         AV62ddo_ContratadaUsuario_UsuarioPessoaDocTitleControlIdToReplace = "";
         AV75ddo_ContratadaUsuario_UsuarioAtivoTitleControlIdToReplace = "";
         AV91Pgmname = "";
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         A71ContratadaUsuario_UsuarioPessoaNom = "";
         A341Usuario_UserGamGuid = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV67DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV51Usuario_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV55ContratadaUsuario_UsuarioPessoaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV59ContratadaUsuario_UsuarioPessoaDocTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV73ContratadaUsuario_UsuarioAtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_usuario_nome_Filteredtext_set = "";
         Ddo_usuario_nome_Selectedvalue_set = "";
         Ddo_usuario_nome_Sortedstatus = "";
         Ddo_contratadausuario_usuariopessoanom_Filteredtext_set = "";
         Ddo_contratadausuario_usuariopessoanom_Selectedvalue_set = "";
         Ddo_contratadausuario_usuariopessoanom_Sortedstatus = "";
         Ddo_contratadausuario_usuariopessoadoc_Filteredtext_set = "";
         Ddo_contratadausuario_usuariopessoadoc_Selectedvalue_set = "";
         Ddo_contratadausuario_usuariopessoadoc_Sortedstatus = "";
         Ddo_contratadausuario_usuarioativo_Selectedvalue_set = "";
         Ddo_contratadausuario_usuarioativo_Sortedstatus = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV45Atualizar = "";
         AV84Atualizar_GXI = "";
         AV29Delete = "";
         AV85Delete_GXI = "";
         A2Usuario_Nome = "";
         A491ContratadaUsuario_UsuarioPessoaDoc = "";
         AV43GAMEMail = "";
         AV80Notificar = "";
         AV68Ferias = "";
         AV86Ferias_GXI = "";
         AV33AssociarPerfil = "";
         AV87Associarperfil_GXI = "";
         AV42AssociarServico = "";
         AV88Associarservico_GXI = "";
         AV41Config = "";
         AV89Config_GXI = "";
         AV76ClonarDados = "";
         AV90Clonardados_GXI = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV18ContratadaUsuario_UsuarioPessoaNom1 = "";
         lV52TFUsuario_Nome = "";
         lV56TFContratadaUsuario_UsuarioPessoaNom = "";
         lV60TFContratadaUsuario_UsuarioPessoaDoc = "";
         H008A3_A1908Usuario_DeFerias = new bool[] {false} ;
         H008A3_n1908Usuario_DeFerias = new bool[] {false} ;
         H008A3_A1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         H008A3_n1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         H008A3_A491ContratadaUsuario_UsuarioPessoaDoc = new String[] {""} ;
         H008A3_n491ContratadaUsuario_UsuarioPessoaDoc = new bool[] {false} ;
         H008A3_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         H008A3_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         H008A3_A2Usuario_Nome = new String[] {""} ;
         H008A3_n2Usuario_Nome = new bool[] {false} ;
         H008A3_A341Usuario_UserGamGuid = new String[] {""} ;
         H008A3_n341Usuario_UserGamGuid = new bool[] {false} ;
         H008A3_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         H008A3_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H008A3_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H008A3_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H008A3_A40000UsuarioNotifica_NoStatus = new String[] {""} ;
         H008A3_n40000UsuarioNotifica_NoStatus = new bool[] {false} ;
         A40000UsuarioNotifica_NoStatus = "";
         H008A5_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         H008A6_A1078ContratoGestor_ContratoCod = new int[1] ;
         H008A6_A1013Contrato_PrepostoCod = new int[1] ;
         H008A6_n1013Contrato_PrepostoCod = new bool[] {false} ;
         H008A6_A1136ContratoGestor_ContratadaCod = new int[1] ;
         H008A6_n1136ContratoGestor_ContratadaCod = new bool[] {false} ;
         H008A6_A1079ContratoGestor_UsuarioCod = new int[1] ;
         H008A6_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         H008A6_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         AV40Websession = context.GetSession();
         AV44GamUser = new SdtGAMUser(context);
         GXt_char3 = "";
         GridRow = new GXWebRow();
         imgInsert_Link = "";
         AV31Session = context.GetSession();
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV46Usuario = new SdtUsuario(context);
         AV77AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         AV79AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
         AV78AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         imgInsert_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         bttBtnimportar_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV32ContratadaUsuario_ContratadaCod = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratadacontratadausuariowc__default(),
            new Object[][] {
                new Object[] {
               H008A3_A1908Usuario_DeFerias, H008A3_n1908Usuario_DeFerias, H008A3_A1394ContratadaUsuario_UsuarioAtivo, H008A3_n1394ContratadaUsuario_UsuarioAtivo, H008A3_A491ContratadaUsuario_UsuarioPessoaDoc, H008A3_n491ContratadaUsuario_UsuarioPessoaDoc, H008A3_A71ContratadaUsuario_UsuarioPessoaNom, H008A3_n71ContratadaUsuario_UsuarioPessoaNom, H008A3_A2Usuario_Nome, H008A3_n2Usuario_Nome,
               H008A3_A341Usuario_UserGamGuid, H008A3_n341Usuario_UserGamGuid, H008A3_A70ContratadaUsuario_UsuarioPessoaCod, H008A3_n70ContratadaUsuario_UsuarioPessoaCod, H008A3_A69ContratadaUsuario_UsuarioCod, H008A3_A66ContratadaUsuario_ContratadaCod, H008A3_A40000UsuarioNotifica_NoStatus, H008A3_n40000UsuarioNotifica_NoStatus
               }
               , new Object[] {
               H008A5_AGRID_nRecordCount
               }
               , new Object[] {
               H008A6_A1078ContratoGestor_ContratoCod, H008A6_A1013Contrato_PrepostoCod, H008A6_n1013Contrato_PrepostoCod, H008A6_A1136ContratoGestor_ContratadaCod, H008A6_n1136ContratoGestor_ContratadaCod, H008A6_A1079ContratoGestor_UsuarioCod, H008A6_A1446ContratoGestor_ContratadaAreaCod, H008A6_n1446ContratoGestor_ContratadaAreaCod
               }
            }
         );
         AV91Pgmname = "ContratadaContratadaUsuarioWC";
         /* GeneXus formulas. */
         AV91Pgmname = "ContratadaContratadaUsuarioWC";
         context.Gx_err = 0;
         edtavGamemail_Enabled = 0;
         edtavNotificar_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_42 ;
      private short nGXsfl_42_idx=1 ;
      private short AV14OrderedBy ;
      private short AV74TFContratadaUsuario_UsuarioAtivo_Sel ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_42_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtUsuario_Nome_Titleformat ;
      private short edtContratadaUsuario_UsuarioPessoaNom_Titleformat ;
      private short edtContratadaUsuario_UsuarioPessoaDoc_Titleformat ;
      private short chkContratadaUsuario_UsuarioAtivo_Titleformat ;
      private short AV83GXLvl77 ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV32ContratadaUsuario_ContratadaCod ;
      private int wcpOAV32ContratadaUsuario_ContratadaCod ;
      private int subGrid_Rows ;
      private int A1136ContratoGestor_ContratadaCod ;
      private int A1013Contrato_PrepostoCod ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int edtavGamemail_Enabled ;
      private int edtavNotificar_Enabled ;
      private int Ddo_usuario_nome_Datalistupdateminimumcharacters ;
      private int Ddo_contratadausuario_usuariopessoanom_Datalistupdateminimumcharacters ;
      private int Ddo_contratadausuario_usuariopessoadoc_Datalistupdateminimumcharacters ;
      private int edtavTfusuario_nome_Visible ;
      private int edtavTfusuario_nome_sel_Visible ;
      private int edtavTfcontratadausuario_usuariopessoanom_Visible ;
      private int edtavTfcontratadausuario_usuariopessoanom_sel_Visible ;
      private int edtavTfcontratadausuario_usuariopessoadoc_Visible ;
      private int edtavTfcontratadausuario_usuariopessoadoc_sel_Visible ;
      private int edtavTfcontratadausuario_usuarioativo_sel_Visible ;
      private int edtavDdo_usuario_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratadausuario_usuariopessoanomtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratadausuario_usuariopessoadoctitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratadausuario_usuarioativotitlecontrolidtoreplace_Visible ;
      private int A70ContratadaUsuario_UsuarioPessoaCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int A1078ContratoGestor_ContratoCod ;
      private int A1446ContratoGestor_ContratadaAreaCod ;
      private int edtavAssociarservico_Visible ;
      private int edtavClonardados_Visible ;
      private int edtavAssociarperfil_Visible ;
      private int edtavConfig_Visible ;
      private int edtavAtualizar_Enabled ;
      private int edtavDelete_Enabled ;
      private int edtavFerias_Enabled ;
      private int edtavAssociarperfil_Enabled ;
      private int edtavAssociarservico_Enabled ;
      private int edtavConfig_Enabled ;
      private int edtavClonardados_Enabled ;
      private int edtUsuario_Nome_Forecolor ;
      private int edtContratadaUsuario_UsuarioPessoaNom_Forecolor ;
      private int edtContratadaUsuario_UsuarioPessoaDoc_Forecolor ;
      private int edtavGamemail_Forecolor ;
      private int edtavNotificar_Forecolor ;
      private int imgInsert_Enabled ;
      private int bttBtnimportar_Enabled ;
      private int edtavFerias_Visible ;
      private int edtavContratadausuario_usuariopessoanom1_Visible ;
      private int AV92GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavAtualizar_Visible ;
      private int edtavGamemail_Visible ;
      private int edtavNotificar_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private long AV72Color ;
      private String Ddo_usuario_nome_Activeeventkey ;
      private String Ddo_usuario_nome_Filteredtext_get ;
      private String Ddo_usuario_nome_Selectedvalue_get ;
      private String Ddo_contratadausuario_usuariopessoanom_Activeeventkey ;
      private String Ddo_contratadausuario_usuariopessoanom_Filteredtext_get ;
      private String Ddo_contratadausuario_usuariopessoanom_Selectedvalue_get ;
      private String Ddo_contratadausuario_usuariopessoadoc_Activeeventkey ;
      private String Ddo_contratadausuario_usuariopessoadoc_Filteredtext_get ;
      private String Ddo_contratadausuario_usuariopessoadoc_Selectedvalue_get ;
      private String Ddo_contratadausuario_usuarioativo_Activeeventkey ;
      private String Ddo_contratadausuario_usuarioativo_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_42_idx="0001" ;
      private String AV18ContratadaUsuario_UsuarioPessoaNom1 ;
      private String AV52TFUsuario_Nome ;
      private String AV53TFUsuario_Nome_Sel ;
      private String AV56TFContratadaUsuario_UsuarioPessoaNom ;
      private String AV57TFContratadaUsuario_UsuarioPessoaNom_Sel ;
      private String AV91Pgmname ;
      private String A71ContratadaUsuario_UsuarioPessoaNom ;
      private String A341Usuario_UserGamGuid ;
      private String GXKey ;
      private String edtavGamemail_Internalname ;
      private String edtavNotificar_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Ddo_usuario_nome_Caption ;
      private String Ddo_usuario_nome_Tooltip ;
      private String Ddo_usuario_nome_Cls ;
      private String Ddo_usuario_nome_Filteredtext_set ;
      private String Ddo_usuario_nome_Selectedvalue_set ;
      private String Ddo_usuario_nome_Dropdownoptionstype ;
      private String Ddo_usuario_nome_Titlecontrolidtoreplace ;
      private String Ddo_usuario_nome_Sortedstatus ;
      private String Ddo_usuario_nome_Filtertype ;
      private String Ddo_usuario_nome_Datalisttype ;
      private String Ddo_usuario_nome_Datalistproc ;
      private String Ddo_usuario_nome_Sortasc ;
      private String Ddo_usuario_nome_Sortdsc ;
      private String Ddo_usuario_nome_Loadingdata ;
      private String Ddo_usuario_nome_Cleanfilter ;
      private String Ddo_usuario_nome_Noresultsfound ;
      private String Ddo_usuario_nome_Searchbuttontext ;
      private String Ddo_contratadausuario_usuariopessoanom_Caption ;
      private String Ddo_contratadausuario_usuariopessoanom_Tooltip ;
      private String Ddo_contratadausuario_usuariopessoanom_Cls ;
      private String Ddo_contratadausuario_usuariopessoanom_Filteredtext_set ;
      private String Ddo_contratadausuario_usuariopessoanom_Selectedvalue_set ;
      private String Ddo_contratadausuario_usuariopessoanom_Dropdownoptionstype ;
      private String Ddo_contratadausuario_usuariopessoanom_Titlecontrolidtoreplace ;
      private String Ddo_contratadausuario_usuariopessoanom_Sortedstatus ;
      private String Ddo_contratadausuario_usuariopessoanom_Filtertype ;
      private String Ddo_contratadausuario_usuariopessoanom_Datalisttype ;
      private String Ddo_contratadausuario_usuariopessoanom_Datalistproc ;
      private String Ddo_contratadausuario_usuariopessoanom_Sortasc ;
      private String Ddo_contratadausuario_usuariopessoanom_Sortdsc ;
      private String Ddo_contratadausuario_usuariopessoanom_Loadingdata ;
      private String Ddo_contratadausuario_usuariopessoanom_Cleanfilter ;
      private String Ddo_contratadausuario_usuariopessoanom_Noresultsfound ;
      private String Ddo_contratadausuario_usuariopessoanom_Searchbuttontext ;
      private String Ddo_contratadausuario_usuariopessoadoc_Caption ;
      private String Ddo_contratadausuario_usuariopessoadoc_Tooltip ;
      private String Ddo_contratadausuario_usuariopessoadoc_Cls ;
      private String Ddo_contratadausuario_usuariopessoadoc_Filteredtext_set ;
      private String Ddo_contratadausuario_usuariopessoadoc_Selectedvalue_set ;
      private String Ddo_contratadausuario_usuariopessoadoc_Dropdownoptionstype ;
      private String Ddo_contratadausuario_usuariopessoadoc_Titlecontrolidtoreplace ;
      private String Ddo_contratadausuario_usuariopessoadoc_Sortedstatus ;
      private String Ddo_contratadausuario_usuariopessoadoc_Filtertype ;
      private String Ddo_contratadausuario_usuariopessoadoc_Datalisttype ;
      private String Ddo_contratadausuario_usuariopessoadoc_Datalistproc ;
      private String Ddo_contratadausuario_usuariopessoadoc_Sortasc ;
      private String Ddo_contratadausuario_usuariopessoadoc_Sortdsc ;
      private String Ddo_contratadausuario_usuariopessoadoc_Loadingdata ;
      private String Ddo_contratadausuario_usuariopessoadoc_Cleanfilter ;
      private String Ddo_contratadausuario_usuariopessoadoc_Noresultsfound ;
      private String Ddo_contratadausuario_usuariopessoadoc_Searchbuttontext ;
      private String Ddo_contratadausuario_usuarioativo_Caption ;
      private String Ddo_contratadausuario_usuarioativo_Tooltip ;
      private String Ddo_contratadausuario_usuarioativo_Cls ;
      private String Ddo_contratadausuario_usuarioativo_Selectedvalue_set ;
      private String Ddo_contratadausuario_usuarioativo_Dropdownoptionstype ;
      private String Ddo_contratadausuario_usuarioativo_Titlecontrolidtoreplace ;
      private String Ddo_contratadausuario_usuarioativo_Sortedstatus ;
      private String Ddo_contratadausuario_usuarioativo_Datalisttype ;
      private String Ddo_contratadausuario_usuarioativo_Datalistfixedvalues ;
      private String Ddo_contratadausuario_usuarioativo_Sortasc ;
      private String Ddo_contratadausuario_usuarioativo_Sortdsc ;
      private String Ddo_contratadausuario_usuarioativo_Cleanfilter ;
      private String Ddo_contratadausuario_usuarioativo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String TempTags ;
      private String edtavTfusuario_nome_Internalname ;
      private String edtavTfusuario_nome_Jsonclick ;
      private String edtavTfusuario_nome_sel_Internalname ;
      private String edtavTfusuario_nome_sel_Jsonclick ;
      private String edtavTfcontratadausuario_usuariopessoanom_Internalname ;
      private String edtavTfcontratadausuario_usuariopessoanom_Jsonclick ;
      private String edtavTfcontratadausuario_usuariopessoanom_sel_Internalname ;
      private String edtavTfcontratadausuario_usuariopessoanom_sel_Jsonclick ;
      private String edtavTfcontratadausuario_usuariopessoadoc_Internalname ;
      private String edtavTfcontratadausuario_usuariopessoadoc_Jsonclick ;
      private String edtavTfcontratadausuario_usuariopessoadoc_sel_Internalname ;
      private String edtavTfcontratadausuario_usuariopessoadoc_sel_Jsonclick ;
      private String edtavTfcontratadausuario_usuarioativo_sel_Internalname ;
      private String edtavTfcontratadausuario_usuarioativo_sel_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_usuario_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratadausuario_usuariopessoanomtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratadausuario_usuariopessoadoctitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratadausuario_usuarioativotitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavAtualizar_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtContratadaUsuario_ContratadaCod_Internalname ;
      private String edtContratadaUsuario_UsuarioCod_Internalname ;
      private String edtContratadaUsuario_UsuarioPessoaCod_Internalname ;
      private String edtUsuario_UserGamGuid_Internalname ;
      private String A2Usuario_Nome ;
      private String edtUsuario_Nome_Internalname ;
      private String edtContratadaUsuario_UsuarioPessoaNom_Internalname ;
      private String edtContratadaUsuario_UsuarioPessoaDoc_Internalname ;
      private String AV80Notificar ;
      private String chkContratadaUsuario_UsuarioAtivo_Internalname ;
      private String edtavFerias_Internalname ;
      private String edtavAssociarperfil_Internalname ;
      private String edtavAssociarservico_Internalname ;
      private String edtavConfig_Internalname ;
      private String edtavClonardados_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV18ContratadaUsuario_UsuarioPessoaNom1 ;
      private String lV52TFUsuario_Nome ;
      private String lV56TFContratadaUsuario_UsuarioPessoaNom ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavContratadausuario_usuariopessoanom1_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_usuario_nome_Internalname ;
      private String Ddo_contratadausuario_usuariopessoanom_Internalname ;
      private String Ddo_contratadausuario_usuariopessoadoc_Internalname ;
      private String Ddo_contratadausuario_usuarioativo_Internalname ;
      private String edtUsuario_Nome_Title ;
      private String edtContratadaUsuario_UsuarioPessoaNom_Title ;
      private String edtContratadaUsuario_UsuarioPessoaDoc_Title ;
      private String edtavAssociarperfil_Title ;
      private String edtavAssociarservico_Title ;
      private String edtavConfig_Title ;
      private String edtavFerias_Title ;
      private String edtavClonardados_Title ;
      private String edtavAtualizar_Tooltiptext ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavFerias_Tooltiptext ;
      private String edtavAssociarperfil_Tooltiptext ;
      private String edtavAssociarservico_Tooltiptext ;
      private String edtavConfig_Tooltiptext ;
      private String edtavConfig_Link ;
      private String edtavClonardados_Tooltiptext ;
      private String imgInsert_Internalname ;
      private String imgInsert_Tooltiptext ;
      private String bttBtnimportar_Internalname ;
      private String bttBtnimportar_Tooltiptext ;
      private String GXt_char3 ;
      private String imgInsert_Link ;
      private String sStyleString ;
      private String tblTablegridheader_Internalname ;
      private String tblTablemergedgrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String imgInsert_Jsonclick ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String imgCleanfilters_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavContratadausuario_usuariopessoanom1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String bttBtnimportar_Jsonclick ;
      private String sCtrlAV32ContratadaUsuario_ContratadaCod ;
      private String sGXsfl_42_fel_idx="0001" ;
      private String edtavAtualizar_Jsonclick ;
      private String ROClassString ;
      private String edtContratadaUsuario_ContratadaCod_Jsonclick ;
      private String edtContratadaUsuario_UsuarioCod_Jsonclick ;
      private String edtContratadaUsuario_UsuarioPessoaCod_Jsonclick ;
      private String edtUsuario_UserGamGuid_Jsonclick ;
      private String edtUsuario_Nome_Jsonclick ;
      private String edtContratadaUsuario_UsuarioPessoaNom_Jsonclick ;
      private String edtContratadaUsuario_UsuarioPessoaDoc_Jsonclick ;
      private String edtavGamemail_Jsonclick ;
      private String edtavNotificar_Jsonclick ;
      private String edtavFerias_Jsonclick ;
      private String edtavAssociarperfil_Jsonclick ;
      private String edtavAssociarservico_Jsonclick ;
      private String edtavClonardados_Jsonclick ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV15OrderedDsc ;
      private bool n1136ContratoGestor_ContratadaCod ;
      private bool n1013Contrato_PrepostoCod ;
      private bool A1135ContratoGestor_UsuarioEhContratante ;
      private bool n71ContratadaUsuario_UsuarioPessoaNom ;
      private bool A1394ContratadaUsuario_UsuarioAtivo ;
      private bool n1394ContratadaUsuario_UsuarioAtivo ;
      private bool A1908Usuario_DeFerias ;
      private bool n1908Usuario_DeFerias ;
      private bool n341Usuario_UserGamGuid ;
      private bool toggleJsOutput ;
      private bool Ddo_usuario_nome_Includesortasc ;
      private bool Ddo_usuario_nome_Includesortdsc ;
      private bool Ddo_usuario_nome_Includefilter ;
      private bool Ddo_usuario_nome_Filterisrange ;
      private bool Ddo_usuario_nome_Includedatalist ;
      private bool Ddo_contratadausuario_usuariopessoanom_Includesortasc ;
      private bool Ddo_contratadausuario_usuariopessoanom_Includesortdsc ;
      private bool Ddo_contratadausuario_usuariopessoanom_Includefilter ;
      private bool Ddo_contratadausuario_usuariopessoanom_Filterisrange ;
      private bool Ddo_contratadausuario_usuariopessoanom_Includedatalist ;
      private bool Ddo_contratadausuario_usuariopessoadoc_Includesortasc ;
      private bool Ddo_contratadausuario_usuariopessoadoc_Includesortdsc ;
      private bool Ddo_contratadausuario_usuariopessoadoc_Includefilter ;
      private bool Ddo_contratadausuario_usuariopessoadoc_Filterisrange ;
      private bool Ddo_contratadausuario_usuariopessoadoc_Includedatalist ;
      private bool Ddo_contratadausuario_usuarioativo_Includesortasc ;
      private bool Ddo_contratadausuario_usuarioativo_Includesortdsc ;
      private bool Ddo_contratadausuario_usuarioativo_Includefilter ;
      private bool Ddo_contratadausuario_usuarioativo_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n70ContratadaUsuario_UsuarioPessoaCod ;
      private bool n2Usuario_Nome ;
      private bool n491ContratadaUsuario_UsuarioPessoaDoc ;
      private bool n40000UsuarioNotifica_NoStatus ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool n1446ContratoGestor_ContratadaAreaCod ;
      private bool GXt_boolean2 ;
      private bool AV45Atualizar_IsBlob ;
      private bool AV29Delete_IsBlob ;
      private bool AV68Ferias_IsBlob ;
      private bool AV33AssociarPerfil_IsBlob ;
      private bool AV42AssociarServico_IsBlob ;
      private bool AV41Config_IsBlob ;
      private bool AV76ClonarDados_IsBlob ;
      private String AV16DynamicFiltersSelector1 ;
      private String AV60TFContratadaUsuario_UsuarioPessoaDoc ;
      private String AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel ;
      private String AV54ddo_Usuario_NomeTitleControlIdToReplace ;
      private String AV58ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace ;
      private String AV62ddo_ContratadaUsuario_UsuarioPessoaDocTitleControlIdToReplace ;
      private String AV75ddo_ContratadaUsuario_UsuarioAtivoTitleControlIdToReplace ;
      private String AV84Atualizar_GXI ;
      private String AV85Delete_GXI ;
      private String A491ContratadaUsuario_UsuarioPessoaDoc ;
      private String AV43GAMEMail ;
      private String AV86Ferias_GXI ;
      private String AV87Associarperfil_GXI ;
      private String AV88Associarservico_GXI ;
      private String AV89Config_GXI ;
      private String AV90Clonardados_GXI ;
      private String lV60TFContratadaUsuario_UsuarioPessoaDoc ;
      private String A40000UsuarioNotifica_NoStatus ;
      private String AV45Atualizar ;
      private String AV29Delete ;
      private String AV68Ferias ;
      private String AV33AssociarPerfil ;
      private String AV42AssociarServico ;
      private String AV41Config ;
      private String AV76ClonarDados ;
      private String imgInsert_Bitmap ;
      private IGxSession AV31Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCheckbox chkContratadaUsuario_UsuarioAtivo ;
      private IDataStoreProvider pr_default ;
      private bool[] H008A3_A1908Usuario_DeFerias ;
      private bool[] H008A3_n1908Usuario_DeFerias ;
      private bool[] H008A3_A1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] H008A3_n1394ContratadaUsuario_UsuarioAtivo ;
      private String[] H008A3_A491ContratadaUsuario_UsuarioPessoaDoc ;
      private bool[] H008A3_n491ContratadaUsuario_UsuarioPessoaDoc ;
      private String[] H008A3_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] H008A3_n71ContratadaUsuario_UsuarioPessoaNom ;
      private String[] H008A3_A2Usuario_Nome ;
      private bool[] H008A3_n2Usuario_Nome ;
      private String[] H008A3_A341Usuario_UserGamGuid ;
      private bool[] H008A3_n341Usuario_UserGamGuid ;
      private int[] H008A3_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] H008A3_n70ContratadaUsuario_UsuarioPessoaCod ;
      private int[] H008A3_A69ContratadaUsuario_UsuarioCod ;
      private int[] H008A3_A66ContratadaUsuario_ContratadaCod ;
      private String[] H008A3_A40000UsuarioNotifica_NoStatus ;
      private bool[] H008A3_n40000UsuarioNotifica_NoStatus ;
      private long[] H008A5_AGRID_nRecordCount ;
      private int[] H008A6_A1078ContratoGestor_ContratoCod ;
      private int[] H008A6_A1013Contrato_PrepostoCod ;
      private bool[] H008A6_n1013Contrato_PrepostoCod ;
      private int[] H008A6_A1136ContratoGestor_ContratadaCod ;
      private bool[] H008A6_n1136ContratoGestor_ContratadaCod ;
      private int[] H008A6_A1079ContratoGestor_UsuarioCod ;
      private int[] H008A6_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] H008A6_n1446ContratoGestor_ContratadaAreaCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      private IGxSession AV40Websession ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV51Usuario_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV55ContratadaUsuario_UsuarioPessoaNomTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV59ContratadaUsuario_UsuarioPessoaDocTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV73ContratadaUsuario_UsuarioAtivoTitleFilterData ;
      private wwpbaseobjects.SdtAuditingObject AV77AuditingObject ;
      private wwpbaseobjects.SdtAuditingObject_RecordItem AV79AuditingObjectRecordItem ;
      private wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem AV78AuditingObjectRecordItemAttributeItem ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV67DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
      private SdtGAMUser AV44GamUser ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV13GridStateDynamicFilter ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private SdtUsuario AV46Usuario ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
   }

   public class contratadacontratadausuariowc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H008A3( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             String AV18ContratadaUsuario_UsuarioPessoaNom1 ,
                                             String AV53TFUsuario_Nome_Sel ,
                                             String AV52TFUsuario_Nome ,
                                             String AV57TFContratadaUsuario_UsuarioPessoaNom_Sel ,
                                             String AV56TFContratadaUsuario_UsuarioPessoaNom ,
                                             String AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel ,
                                             String AV60TFContratadaUsuario_UsuarioPessoaDoc ,
                                             short AV74TFContratadaUsuario_UsuarioAtivo_Sel ,
                                             String A71ContratadaUsuario_UsuarioPessoaNom ,
                                             String A2Usuario_Nome ,
                                             String A491ContratadaUsuario_UsuarioPessoaDoc ,
                                             bool A1394ContratadaUsuario_UsuarioAtivo ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A66ContratadaUsuario_ContratadaCod ,
                                             int AV32ContratadaUsuario_ContratadaCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [14] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T2.[Usuario_DeFerias], T2.[Usuario_Ativo] AS ContratadaUsuario_UsuarioAtivo, T3.[Pessoa_Docto] AS ContratadaUsuario_UsuarioPessoaDoc, T3.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPessoaNom, T2.[Usuario_Nome], T2.[Usuario_UserGamGuid], T2.[Usuario_PessoaCod] AS ContratadaUsuario_UsuarioPessoaCod, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T1.[ContratadaUsuario_ContratadaCod], COALESCE( T4.[UsuarioNotifica_NoStatus], '') AS UsuarioNotifica_NoStatus";
         sFromString = " FROM ((([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) LEFT JOIN (SELECT MIN(T5.[UsuarioNotifica_NoStatus]) AS UsuarioNotifica_NoStatus, T6.[ContratadaUsuario_ContratadaCod], T6.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod FROM [UsuarioNotifica] T5 WITH (NOLOCK),  [ContratadaUsuario] T6 WITH (NOLOCK) WHERE (T5.[UsuarioNotifica_UsuarioCod] = T6.[ContratadaUsuario_UsuarioCod]) AND (T5.[UsuarioNotifica_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo) GROUP BY T6.[ContratadaUsuario_ContratadaCod], T6.[ContratadaUsuario_UsuarioCod] ) T4 ON T4.[ContratadaUsuario_ContratadaCod] = T1.[ContratadaUsuario_ContratadaCod] AND T4.[ContratadaUsuario_UsuarioCod] = T1.[ContratadaUsuario_UsuarioCod])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[ContratadaUsuario_ContratadaCod] = @AV32ContratadaUsuario_ContratadaCod)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18ContratadaUsuario_UsuarioPessoaNom1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV18ContratadaUsuario_UsuarioPessoaNom1 + '%')";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV53TFUsuario_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52TFUsuario_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Usuario_Nome] like @lV52TFUsuario_Nome)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53TFUsuario_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Usuario_Nome] = @AV53TFUsuario_Nome_Sel)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV57TFContratadaUsuario_UsuarioPessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56TFContratadaUsuario_UsuarioPessoaNom)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV56TFContratadaUsuario_UsuarioPessoaNom)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57TFContratadaUsuario_UsuarioPessoaNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV57TFContratadaUsuario_UsuarioPessoaNom_Sel)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFContratadaUsuario_UsuarioPessoaDoc)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Docto] like @lV60TFContratadaUsuario_UsuarioPessoaDoc)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Docto] = @AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( AV74TFContratadaUsuario_UsuarioAtivo_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T2.[Usuario_Ativo] = 1)";
         }
         if ( AV74TFContratadaUsuario_UsuarioAtivo_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T2.[Usuario_Ativo] = 0)";
         }
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratadaUsuario_ContratadaCod], T3.[Pessoa_Nome]";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratadaUsuario_ContratadaCod] DESC, T3.[Pessoa_Nome] DESC";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratadaUsuario_ContratadaCod], T2.[Usuario_Nome]";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratadaUsuario_ContratadaCod] DESC, T2.[Usuario_Nome] DESC";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratadaUsuario_ContratadaCod], T3.[Pessoa_Docto]";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratadaUsuario_ContratadaCod] DESC, T3.[Pessoa_Docto] DESC";
         }
         else if ( ( AV14OrderedBy == 4 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratadaUsuario_ContratadaCod], T2.[Usuario_Ativo]";
         }
         else if ( ( AV14OrderedBy == 4 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratadaUsuario_ContratadaCod] DESC, T2.[Usuario_Ativo] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratadaUsuario_ContratadaCod], T1.[ContratadaUsuario_UsuarioCod]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      protected Object[] conditional_H008A5( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             String AV18ContratadaUsuario_UsuarioPessoaNom1 ,
                                             String AV53TFUsuario_Nome_Sel ,
                                             String AV52TFUsuario_Nome ,
                                             String AV57TFContratadaUsuario_UsuarioPessoaNom_Sel ,
                                             String AV56TFContratadaUsuario_UsuarioPessoaNom ,
                                             String AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel ,
                                             String AV60TFContratadaUsuario_UsuarioPessoaDoc ,
                                             short AV74TFContratadaUsuario_UsuarioAtivo_Sel ,
                                             String A71ContratadaUsuario_UsuarioPessoaNom ,
                                             String A2Usuario_Nome ,
                                             String A491ContratadaUsuario_UsuarioPessoaDoc ,
                                             bool A1394ContratadaUsuario_UsuarioAtivo ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A66ContratadaUsuario_ContratadaCod ,
                                             int AV32ContratadaUsuario_ContratadaCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int6 ;
         GXv_int6 = new short [9] ;
         Object[] GXv_Object7 ;
         GXv_Object7 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ((([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) LEFT JOIN (SELECT MIN(T5.[UsuarioNotifica_NoStatus]) AS UsuarioNotifica_NoStatus, T6.[ContratadaUsuario_ContratadaCod], T6.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod FROM [UsuarioNotifica] T5 WITH (NOLOCK),  [ContratadaUsuario] T6 WITH (NOLOCK) WHERE (T5.[UsuarioNotifica_UsuarioCod] = T6.[ContratadaUsuario_UsuarioCod]) AND (T5.[UsuarioNotifica_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo) GROUP BY T6.[ContratadaUsuario_ContratadaCod], T6.[ContratadaUsuario_UsuarioCod] ) T4 ON T4.[ContratadaUsuario_ContratadaCod] = T1.[ContratadaUsuario_ContratadaCod] AND T4.[ContratadaUsuario_UsuarioCod] = T1.[ContratadaUsuario_UsuarioCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratadaUsuario_ContratadaCod] = @AV32ContratadaUsuario_ContratadaCod)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18ContratadaUsuario_UsuarioPessoaNom1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV18ContratadaUsuario_UsuarioPessoaNom1 + '%')";
         }
         else
         {
            GXv_int6[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV53TFUsuario_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52TFUsuario_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Usuario_Nome] like @lV52TFUsuario_Nome)";
         }
         else
         {
            GXv_int6[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53TFUsuario_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Usuario_Nome] = @AV53TFUsuario_Nome_Sel)";
         }
         else
         {
            GXv_int6[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV57TFContratadaUsuario_UsuarioPessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56TFContratadaUsuario_UsuarioPessoaNom)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV56TFContratadaUsuario_UsuarioPessoaNom)";
         }
         else
         {
            GXv_int6[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57TFContratadaUsuario_UsuarioPessoaNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV57TFContratadaUsuario_UsuarioPessoaNom_Sel)";
         }
         else
         {
            GXv_int6[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFContratadaUsuario_UsuarioPessoaDoc)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Docto] like @lV60TFContratadaUsuario_UsuarioPessoaDoc)";
         }
         else
         {
            GXv_int6[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Docto] = @AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel)";
         }
         else
         {
            GXv_int6[8] = 1;
         }
         if ( AV74TFContratadaUsuario_UsuarioAtivo_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T2.[Usuario_Ativo] = 1)";
         }
         if ( AV74TFContratadaUsuario_UsuarioAtivo_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T2.[Usuario_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 4 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 4 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object7[0] = scmdbuf;
         GXv_Object7[1] = GXv_int6;
         return GXv_Object7 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H008A3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (bool)dynConstraints[12] , (short)dynConstraints[13] , (bool)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] );
               case 1 :
                     return conditional_H008A5(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (bool)dynConstraints[12] , (short)dynConstraints[13] , (bool)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH008A6 ;
          prmH008A6 = new Object[] {
          new Object[] {"@AV6WWPContext__Userid",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV32ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH008A3 ;
          prmH008A3 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV32ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV18ContratadaUsuario_UsuarioPessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV52TFUsuario_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV53TFUsuario_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV56TFContratadaUsuario_UsuarioPessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV57TFContratadaUsuario_UsuarioPessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV60TFContratadaUsuario_UsuarioPessoaDoc",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH008A5 ;
          prmH008A5 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV32ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV18ContratadaUsuario_UsuarioPessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV52TFUsuario_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV53TFUsuario_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV56TFContratadaUsuario_UsuarioPessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV57TFContratadaUsuario_UsuarioPessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV60TFContratadaUsuario_UsuarioPessoaDoc",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV61TFContratadaUsuario_UsuarioPessoaDoc_Sel",SqlDbType.VarChar,15,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H008A3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH008A3,11,0,true,false )
             ,new CursorDef("H008A5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH008A5,1,0,true,false )
             ,new CursorDef("H008A6", "SELECT T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T2.[Contrato_PrepostoCod], T2.[Contratada_Codigo] AS ContratoGestor_ContratadaCod, T1.[ContratoGestor_UsuarioCod], T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM ([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) WHERE (T2.[Contrato_PrepostoCod] = @AV6WWPContext__Userid or T1.[ContratoGestor_UsuarioCod] = @AV6WWPContext__Userid) AND (T2.[Contratada_Codigo] = @AV32ContratadaUsuario_ContratadaCod) ORDER BY T1.[ContratoGestor_ContratoCod], T1.[ContratoGestor_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH008A6,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getString(6, 40) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((int[]) buf[14])[0] = rslt.getInt(8) ;
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                ((String[]) buf[16])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
