/*
               File: Menu
        Description: Menu
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:0:21.56
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class menu : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_14") == 0 )
         {
            A285Menu_PaiCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n285Menu_PaiCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A285Menu_PaiCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A285Menu_PaiCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_14( A285Menu_PaiCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7Menu_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Menu_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Menu_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMENU_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Menu_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         cmbMenu_Tipo.Name = "MENU_TIPO";
         cmbMenu_Tipo.WebTags = "";
         cmbMenu_Tipo.addItem("1", "Superior", 0);
         cmbMenu_Tipo.addItem("2", "Acesso R�pido", 0);
         if ( cmbMenu_Tipo.ItemCount > 0 )
         {
            A280Menu_Tipo = (short)(NumberUtil.Val( cmbMenu_Tipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A280Menu_Tipo), 2, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A280Menu_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A280Menu_Tipo), 2, 0)));
         }
         chkMenu_Ativo.Name = "MENU_ATIVO";
         chkMenu_Ativo.WebTags = "";
         chkMenu_Ativo.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkMenu_Ativo_Internalname, "TitleCaption", chkMenu_Ativo.Caption);
         chkMenu_Ativo.CheckedValue = "false";
         cmbMenu_PaiTip.Name = "MENU_PAITIP";
         cmbMenu_PaiTip.WebTags = "";
         cmbMenu_PaiTip.addItem("1", "Superior", 0);
         cmbMenu_PaiTip.addItem("2", "Acesso R�pido", 0);
         if ( cmbMenu_PaiTip.ItemCount > 0 )
         {
            A287Menu_PaiTip = (short)(NumberUtil.Val( cmbMenu_PaiTip.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A287Menu_PaiTip), 2, 0))), "."));
            n287Menu_PaiTip = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A287Menu_PaiTip", StringUtil.LTrim( StringUtil.Str( (decimal)(A287Menu_PaiTip), 2, 0)));
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Menu", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtMenu_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public menu( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public menu( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Menu_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7Menu_Codigo = aP1_Menu_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbMenu_Tipo = new GXCombobox();
         chkMenu_Ativo = new GXCheckbox();
         cmbMenu_PaiTip = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbMenu_Tipo.ItemCount > 0 )
         {
            A280Menu_Tipo = (short)(NumberUtil.Val( cmbMenu_Tipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A280Menu_Tipo), 2, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A280Menu_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A280Menu_Tipo), 2, 0)));
         }
         if ( cmbMenu_PaiTip.ItemCount > 0 )
         {
            A287Menu_PaiTip = (short)(NumberUtil.Val( cmbMenu_PaiTip.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A287Menu_PaiTip), 2, 0))), "."));
            n287Menu_PaiTip = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A287Menu_PaiTip", StringUtil.LTrim( StringUtil.Str( (decimal)(A287Menu_PaiTip), 2, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_1B48( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_1B48e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtMenu_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A277Menu_Codigo), 6, 0, ",", "")), ((edtMenu_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A277Menu_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A277Menu_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtMenu_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtMenu_Codigo_Visible, edtMenu_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Menu.htm");
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbMenu_PaiTip, cmbMenu_PaiTip_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A287Menu_PaiTip), 2, 0)), 1, cmbMenu_PaiTip_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbMenu_PaiTip.Visible, cmbMenu_PaiTip.Enabled, 1, 0, 0, "em", 0, "", "", "Attribute", "", "", "", true, "HLP_Menu.htm");
            cmbMenu_PaiTip.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A287Menu_PaiTip), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbMenu_PaiTip_Internalname, "Values", (String)(cmbMenu_PaiTip.ToJavascriptSource()));
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtMenu_PaiAti_Internalname, StringUtil.BoolToStr( A288Menu_PaiAti), StringUtil.BoolToStr( A288Menu_PaiAti), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtMenu_PaiAti_Jsonclick, 0, "Attribute", "", "", "", edtMenu_PaiAti_Visible, edtMenu_PaiAti_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "Flag", "right", false, "HLP_Menu.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_1B48( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_1B48( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_1B48e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_70_1B48( true) ;
         }
         return  ;
      }

      protected void wb_table3_70_1B48e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_1B48e( true) ;
         }
         else
         {
            wb_table1_2_1B48e( false) ;
         }
      }

      protected void wb_table3_70_1B48( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Menu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Menu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Menu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_70_1B48e( true) ;
         }
         else
         {
            wb_table3_70_1B48e( false) ;
         }
      }

      protected void wb_table2_5_1B48( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_1B48( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_1B48e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_1B48e( true) ;
         }
         else
         {
            wb_table2_5_1B48e( false) ;
         }
      }

      protected void wb_table4_13_1B48( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmenu_nome_Internalname, "Nome", "", "", lblTextblockmenu_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Menu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtMenu_Nome_Internalname, StringUtil.RTrim( A278Menu_Nome), StringUtil.RTrim( context.localUtil.Format( A278Menu_Nome, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtMenu_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtMenu_Nome_Enabled, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "NomeMenu30", "left", true, "HLP_Menu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmenu_descricao_Internalname, "Descri��o", "", "", lblTextblockmenu_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Menu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtMenu_Descricao_Internalname, A279Menu_Descricao, StringUtil.RTrim( context.localUtil.Format( A279Menu_Descricao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,23);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtMenu_Descricao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtMenu_Descricao_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_Menu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmenu_tipo_Internalname, "Tipo", "", "", lblTextblockmenu_tipo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Menu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbMenu_Tipo, cmbMenu_Tipo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A280Menu_Tipo), 2, 0)), 1, cmbMenu_Tipo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbMenu_Tipo.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "", true, "HLP_Menu.htm");
            cmbMenu_Tipo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A280Menu_Tipo), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbMenu_Tipo_Internalname, "Values", (String)(cmbMenu_Tipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmenu_link_Internalname, "Link do Menu", "", "", lblTextblockmenu_link_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Menu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table5_33_1B48( true) ;
         }
         return  ;
      }

      protected void wb_table5_33_1B48e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmenu_imagem_Internalname, "Imagem", "", "", lblTextblockmenu_imagem_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Menu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table6_43_1B48( true) ;
         }
         return  ;
      }

      protected void wb_table6_43_1B48e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmenu_ordem_Internalname, "Ordena��o", "", "", lblTextblockmenu_ordem_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Menu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtMenu_Ordem_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A283Menu_Ordem), 3, 0, ",", "")), ((edtMenu_Ordem_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A283Menu_Ordem), "ZZ9")) : context.localUtil.Format( (decimal)(A283Menu_Ordem), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,53);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtMenu_Ordem_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtMenu_Ordem_Enabled, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "Ordem", "right", false, "HLP_Menu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmenu_paicod_Internalname, "Menu Pai", "", "", lblTextblockmenu_paicod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Menu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtMenu_PaiCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A285Menu_PaiCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A285Menu_PaiCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,58);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtMenu_PaiCod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtMenu_PaiCod_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Menu.htm");
            /* Static images/pictures */
            ClassString = "gx-prompt Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgprompt_285_Internalname, context.GetImagePath( "f5b04895-0024-488b-8e3b-b687ca4598ee", "", context.GetTheme( )), imgprompt_285_Link, "", "", context.GetTheme( ), imgprompt_285_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, false, false, "HLP_Menu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtMenu_PaiNom_Internalname, StringUtil.RTrim( A286Menu_PaiNom), StringUtil.RTrim( context.localUtil.Format( A286Menu_PaiNom, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtMenu_PaiNom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtMenu_PaiNom_Enabled, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "NomeMenu30", "left", true, "HLP_Menu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmenu_ativo_Internalname, "Ativo", "", "", lblTextblockmenu_ativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Menu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkMenu_Ativo_Internalname, StringUtil.BoolToStr( A284Menu_Ativo), "", "", 1, chkMenu_Ativo.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(67, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,67);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_1B48e( true) ;
         }
         else
         {
            wb_table4_13_1B48e( false) ;
         }
      }

      protected void wb_table6_43_1B48( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedmenu_imagem_Internalname, tblTablemergedmenu_imagem_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Static Bitmap Variable */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            A282Menu_Imagem_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( A282Menu_Imagem))&&String.IsNullOrEmpty(StringUtil.RTrim( A40000Menu_Imagem_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( A282Menu_Imagem)));
            GxWebStd.gx_bitmap( context, imgMenu_Imagem_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( A282Menu_Imagem)) ? A40000Menu_Imagem_GXI : context.PathToRelativeUrl( A282Menu_Imagem)), "", "", "", context.GetTheme( ), 1, imgMenu_Imagem_Enabled, "", "", 1, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", "", "", "", 0, A282Menu_Imagem_IsBlob, true, "HLP_Menu.htm");
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgMenu_Imagem_Internalname, "URL", (String.IsNullOrEmpty(StringUtil.RTrim( A282Menu_Imagem)) ? A40000Menu_Imagem_GXI : context.PathToRelativeUrl( A282Menu_Imagem)));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgMenu_Imagem_Internalname, "IsBlob", StringUtil.BoolToStr( A282Menu_Imagem_IsBlob));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblMenu_imagem_righttext_Internalname, "(�cone)", "", "", lblMenu_imagem_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Menu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_43_1B48e( true) ;
         }
         else
         {
            wb_table6_43_1B48e( false) ;
         }
      }

      protected void wb_table5_33_1B48( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedmenu_link_Internalname, tblTablemergedmenu_link_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtMenu_Link_Internalname, A281Menu_Link, StringUtil.RTrim( context.localUtil.Format( A281Menu_Link, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,36);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtMenu_Link_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtMenu_Link_Enabled, 0, "text", "", 80, "chr", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "LinkMenu", "left", true, "HLP_Menu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblMenu_link_righttext_Internalname, "(URL)", "", "", lblMenu_link_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Menu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_33_1B48e( true) ;
         }
         else
         {
            wb_table5_33_1B48e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E111B2 */
         E111B2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A278Menu_Nome = cgiGet( edtMenu_Nome_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A278Menu_Nome", A278Menu_Nome);
               A279Menu_Descricao = StringUtil.Upper( cgiGet( edtMenu_Descricao_Internalname));
               n279Menu_Descricao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A279Menu_Descricao", A279Menu_Descricao);
               n279Menu_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A279Menu_Descricao)) ? true : false);
               cmbMenu_Tipo.CurrentValue = cgiGet( cmbMenu_Tipo_Internalname);
               A280Menu_Tipo = (short)(NumberUtil.Val( cgiGet( cmbMenu_Tipo_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A280Menu_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A280Menu_Tipo), 2, 0)));
               A281Menu_Link = cgiGet( edtMenu_Link_Internalname);
               n281Menu_Link = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A281Menu_Link", A281Menu_Link);
               n281Menu_Link = (String.IsNullOrEmpty(StringUtil.RTrim( A281Menu_Link)) ? true : false);
               A282Menu_Imagem = cgiGet( imgMenu_Imagem_Internalname);
               n282Menu_Imagem = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A282Menu_Imagem", A282Menu_Imagem);
               n282Menu_Imagem = (String.IsNullOrEmpty(StringUtil.RTrim( A282Menu_Imagem)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtMenu_Ordem_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtMenu_Ordem_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "MENU_ORDEM");
                  AnyError = 1;
                  GX_FocusControl = edtMenu_Ordem_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A283Menu_Ordem = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A283Menu_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A283Menu_Ordem), 3, 0)));
               }
               else
               {
                  A283Menu_Ordem = (short)(context.localUtil.CToN( cgiGet( edtMenu_Ordem_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A283Menu_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A283Menu_Ordem), 3, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtMenu_PaiCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtMenu_PaiCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "MENU_PAICOD");
                  AnyError = 1;
                  GX_FocusControl = edtMenu_PaiCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A285Menu_PaiCod = 0;
                  n285Menu_PaiCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A285Menu_PaiCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A285Menu_PaiCod), 6, 0)));
               }
               else
               {
                  A285Menu_PaiCod = (int)(context.localUtil.CToN( cgiGet( edtMenu_PaiCod_Internalname), ",", "."));
                  n285Menu_PaiCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A285Menu_PaiCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A285Menu_PaiCod), 6, 0)));
               }
               n285Menu_PaiCod = ((0==A285Menu_PaiCod) ? true : false);
               A286Menu_PaiNom = cgiGet( edtMenu_PaiNom_Internalname);
               n286Menu_PaiNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A286Menu_PaiNom", A286Menu_PaiNom);
               A284Menu_Ativo = StringUtil.StrToBool( cgiGet( chkMenu_Ativo_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A284Menu_Ativo", A284Menu_Ativo);
               A277Menu_Codigo = (int)(context.localUtil.CToN( cgiGet( edtMenu_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A277Menu_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A277Menu_Codigo), 6, 0)));
               cmbMenu_PaiTip.CurrentValue = cgiGet( cmbMenu_PaiTip_Internalname);
               A287Menu_PaiTip = (short)(NumberUtil.Val( cgiGet( cmbMenu_PaiTip_Internalname), "."));
               n287Menu_PaiTip = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A287Menu_PaiTip", StringUtil.LTrim( StringUtil.Str( (decimal)(A287Menu_PaiTip), 2, 0)));
               A288Menu_PaiAti = StringUtil.StrToBool( cgiGet( edtMenu_PaiAti_Internalname));
               n288Menu_PaiAti = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A288Menu_PaiAti", A288Menu_PaiAti);
               /* Read saved values. */
               Z277Menu_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z277Menu_Codigo"), ",", "."));
               Z278Menu_Nome = cgiGet( "Z278Menu_Nome");
               Z279Menu_Descricao = cgiGet( "Z279Menu_Descricao");
               n279Menu_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A279Menu_Descricao)) ? true : false);
               Z280Menu_Tipo = (short)(context.localUtil.CToN( cgiGet( "Z280Menu_Tipo"), ",", "."));
               Z281Menu_Link = cgiGet( "Z281Menu_Link");
               n281Menu_Link = (String.IsNullOrEmpty(StringUtil.RTrim( A281Menu_Link)) ? true : false);
               Z283Menu_Ordem = (short)(context.localUtil.CToN( cgiGet( "Z283Menu_Ordem"), ",", "."));
               Z284Menu_Ativo = StringUtil.StrToBool( cgiGet( "Z284Menu_Ativo"));
               Z285Menu_PaiCod = (int)(context.localUtil.CToN( cgiGet( "Z285Menu_PaiCod"), ",", "."));
               n285Menu_PaiCod = ((0==A285Menu_PaiCod) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N285Menu_PaiCod = (int)(context.localUtil.CToN( cgiGet( "N285Menu_PaiCod"), ",", "."));
               n285Menu_PaiCod = ((0==A285Menu_PaiCod) ? true : false);
               AV7Menu_Codigo = (int)(context.localUtil.CToN( cgiGet( "vMENU_CODIGO"), ",", "."));
               AV11Insert_Menu_PaiCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_MENU_PAICOD"), ",", "."));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A40000Menu_Imagem_GXI = cgiGet( "MENU_IMAGEM_GXI");
               n40000Menu_Imagem_GXI = (String.IsNullOrEmpty(StringUtil.RTrim( A40000Menu_Imagem_GXI))&&String.IsNullOrEmpty(StringUtil.RTrim( A282Menu_Imagem)) ? true : false);
               AV13Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               getMultimediaValue(imgMenu_Imagem_Internalname, ref  A282Menu_Imagem, ref  A40000Menu_Imagem_GXI);
               n40000Menu_Imagem_GXI = (String.IsNullOrEmpty(StringUtil.RTrim( A40000Menu_Imagem_GXI))&&String.IsNullOrEmpty(StringUtil.RTrim( A282Menu_Imagem)) ? true : false);
               n282Menu_Imagem = (String.IsNullOrEmpty(StringUtil.RTrim( A282Menu_Imagem)) ? true : false);
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Menu";
               A277Menu_Codigo = (int)(context.localUtil.CToN( cgiGet( edtMenu_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A277Menu_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A277Menu_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A277Menu_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A277Menu_Codigo != Z277Menu_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("menu:[SecurityCheckFailed value for]"+"Menu_Codigo:"+context.localUtil.Format( (decimal)(A277Menu_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("menu:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A277Menu_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A277Menu_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A277Menu_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode48 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode48;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound48 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_1B0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "MENU_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtMenu_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E111B2 */
                           E111B2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E121B2 */
                           E121B2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E121B2 */
            E121B2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll1B48( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes1B48( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_1B0( )
      {
         BeforeValidate1B48( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls1B48( ) ;
            }
            else
            {
               CheckExtendedTable1B48( ) ;
               CloseExtendedTableCursors1B48( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption1B0( )
      {
      }

      protected void E111B2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV13Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV14GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GXV1), 8, 0)));
            while ( AV14GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV14GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Menu_PaiCod") == 0 )
               {
                  AV11Insert_Menu_PaiCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Menu_PaiCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Menu_PaiCod), 6, 0)));
               }
               AV14GXV1 = (int)(AV14GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GXV1), 8, 0)));
            }
         }
         edtMenu_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMenu_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtMenu_Codigo_Visible), 5, 0)));
         cmbMenu_PaiTip.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbMenu_PaiTip_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbMenu_PaiTip.Visible), 5, 0)));
         edtMenu_PaiAti_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMenu_PaiAti_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtMenu_PaiAti_Visible), 5, 0)));
      }

      protected void E121B2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwmenu.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM1B48( short GX_JID )
      {
         if ( ( GX_JID == 13 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z278Menu_Nome = T001B3_A278Menu_Nome[0];
               Z279Menu_Descricao = T001B3_A279Menu_Descricao[0];
               Z280Menu_Tipo = T001B3_A280Menu_Tipo[0];
               Z281Menu_Link = T001B3_A281Menu_Link[0];
               Z283Menu_Ordem = T001B3_A283Menu_Ordem[0];
               Z284Menu_Ativo = T001B3_A284Menu_Ativo[0];
               Z285Menu_PaiCod = T001B3_A285Menu_PaiCod[0];
            }
            else
            {
               Z278Menu_Nome = A278Menu_Nome;
               Z279Menu_Descricao = A279Menu_Descricao;
               Z280Menu_Tipo = A280Menu_Tipo;
               Z281Menu_Link = A281Menu_Link;
               Z283Menu_Ordem = A283Menu_Ordem;
               Z284Menu_Ativo = A284Menu_Ativo;
               Z285Menu_PaiCod = A285Menu_PaiCod;
            }
         }
         if ( GX_JID == -13 )
         {
            Z277Menu_Codigo = A277Menu_Codigo;
            Z278Menu_Nome = A278Menu_Nome;
            Z279Menu_Descricao = A279Menu_Descricao;
            Z280Menu_Tipo = A280Menu_Tipo;
            Z281Menu_Link = A281Menu_Link;
            Z282Menu_Imagem = A282Menu_Imagem;
            Z40000Menu_Imagem_GXI = A40000Menu_Imagem_GXI;
            Z283Menu_Ordem = A283Menu_Ordem;
            Z284Menu_Ativo = A284Menu_Ativo;
            Z285Menu_PaiCod = A285Menu_PaiCod;
            Z286Menu_PaiNom = A286Menu_PaiNom;
            Z287Menu_PaiTip = A287Menu_PaiTip;
            Z288Menu_PaiAti = A288Menu_PaiAti;
         }
      }

      protected void standaloneNotModal( )
      {
         edtMenu_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMenu_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtMenu_Codigo_Enabled), 5, 0)));
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         AV13Pgmname = "Menu";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Pgmname", AV13Pgmname);
         imgprompt_285_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"promptmenu.aspx"+"',["+"{Ctrl:gx.dom.el('"+"MENU_PAICOD"+"'), id:'"+"MENU_PAICOD"+"'"+",IOType:'inout'}"+","+"{Ctrl:gx.dom.el('"+"MENU_PAINOM"+"'), id:'"+"MENU_PAINOM"+"'"+",IOType:'inout'}"+"],"+"null"+","+"'', false"+","+"false"+");");
         edtMenu_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMenu_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtMenu_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7Menu_Codigo) )
         {
            A277Menu_Codigo = AV7Menu_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A277Menu_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A277Menu_Codigo), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Menu_PaiCod) )
         {
            edtMenu_PaiCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMenu_PaiCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtMenu_PaiCod_Enabled), 5, 0)));
         }
         else
         {
            edtMenu_PaiCod_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMenu_PaiCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtMenu_PaiCod_Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Menu_PaiCod) )
         {
            A285Menu_PaiCod = AV11Insert_Menu_PaiCod;
            n285Menu_PaiCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A285Menu_PaiCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A285Menu_PaiCod), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A284Menu_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A284Menu_Ativo = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A284Menu_Ativo", A284Menu_Ativo);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A283Menu_Ordem) && ( Gx_BScreen == 0 ) )
         {
            A283Menu_Ordem = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A283Menu_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A283Menu_Ordem), 3, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T001B4 */
            pr_default.execute(2, new Object[] {n285Menu_PaiCod, A285Menu_PaiCod});
            A286Menu_PaiNom = T001B4_A286Menu_PaiNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A286Menu_PaiNom", A286Menu_PaiNom);
            n286Menu_PaiNom = T001B4_n286Menu_PaiNom[0];
            A287Menu_PaiTip = T001B4_A287Menu_PaiTip[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A287Menu_PaiTip", StringUtil.LTrim( StringUtil.Str( (decimal)(A287Menu_PaiTip), 2, 0)));
            n287Menu_PaiTip = T001B4_n287Menu_PaiTip[0];
            A288Menu_PaiAti = T001B4_A288Menu_PaiAti[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A288Menu_PaiAti", A288Menu_PaiAti);
            n288Menu_PaiAti = T001B4_n288Menu_PaiAti[0];
            pr_default.close(2);
         }
      }

      protected void Load1B48( )
      {
         /* Using cursor T001B5 */
         pr_default.execute(3, new Object[] {A277Menu_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound48 = 1;
            A278Menu_Nome = T001B5_A278Menu_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A278Menu_Nome", A278Menu_Nome);
            A279Menu_Descricao = T001B5_A279Menu_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A279Menu_Descricao", A279Menu_Descricao);
            n279Menu_Descricao = T001B5_n279Menu_Descricao[0];
            A280Menu_Tipo = T001B5_A280Menu_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A280Menu_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A280Menu_Tipo), 2, 0)));
            A281Menu_Link = T001B5_A281Menu_Link[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A281Menu_Link", A281Menu_Link);
            n281Menu_Link = T001B5_n281Menu_Link[0];
            A40000Menu_Imagem_GXI = T001B5_A40000Menu_Imagem_GXI[0];
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgMenu_Imagem_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A282Menu_Imagem)) ? A40000Menu_Imagem_GXI : context.convertURL( context.PathToRelativeUrl( A282Menu_Imagem))));
            n40000Menu_Imagem_GXI = T001B5_n40000Menu_Imagem_GXI[0];
            A283Menu_Ordem = T001B5_A283Menu_Ordem[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A283Menu_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A283Menu_Ordem), 3, 0)));
            A286Menu_PaiNom = T001B5_A286Menu_PaiNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A286Menu_PaiNom", A286Menu_PaiNom);
            n286Menu_PaiNom = T001B5_n286Menu_PaiNom[0];
            A287Menu_PaiTip = T001B5_A287Menu_PaiTip[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A287Menu_PaiTip", StringUtil.LTrim( StringUtil.Str( (decimal)(A287Menu_PaiTip), 2, 0)));
            n287Menu_PaiTip = T001B5_n287Menu_PaiTip[0];
            A288Menu_PaiAti = T001B5_A288Menu_PaiAti[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A288Menu_PaiAti", A288Menu_PaiAti);
            n288Menu_PaiAti = T001B5_n288Menu_PaiAti[0];
            A284Menu_Ativo = T001B5_A284Menu_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A284Menu_Ativo", A284Menu_Ativo);
            A285Menu_PaiCod = T001B5_A285Menu_PaiCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A285Menu_PaiCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A285Menu_PaiCod), 6, 0)));
            n285Menu_PaiCod = T001B5_n285Menu_PaiCod[0];
            A282Menu_Imagem = T001B5_A282Menu_Imagem[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A282Menu_Imagem", A282Menu_Imagem);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgMenu_Imagem_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A282Menu_Imagem)) ? A40000Menu_Imagem_GXI : context.convertURL( context.PathToRelativeUrl( A282Menu_Imagem))));
            n282Menu_Imagem = T001B5_n282Menu_Imagem[0];
            ZM1B48( -13) ;
         }
         pr_default.close(3);
         OnLoadActions1B48( ) ;
      }

      protected void OnLoadActions1B48( )
      {
      }

      protected void CheckExtendedTable1B48( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A278Menu_Nome)) )
         {
            GX_msglist.addItem("Nome � obrigat�rio.", 1, "MENU_NOME");
            AnyError = 1;
            GX_FocusControl = edtMenu_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( ( A280Menu_Tipo == 1 ) || ( A280Menu_Tipo == 2 ) ) )
         {
            GX_msglist.addItem("Campo Tipo(Superior ou Acesso R�pido) fora do intervalo", "OutOfRange", 1, "MENU_TIPO");
            AnyError = 1;
            GX_FocusControl = cmbMenu_Tipo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( (0==A283Menu_Ordem) )
         {
            GX_msglist.addItem("Ordem do Menu � obrigat�rio.", 1, "MENU_ORDEM");
            AnyError = 1;
            GX_FocusControl = edtMenu_Ordem_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T001B4 */
         pr_default.execute(2, new Object[] {n285Menu_PaiCod, A285Menu_PaiCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (0==A285Menu_PaiCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Menu_Pai'.", "ForeignKeyNotFound", 1, "MENU_PAICOD");
               AnyError = 1;
               GX_FocusControl = edtMenu_PaiCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A286Menu_PaiNom = T001B4_A286Menu_PaiNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A286Menu_PaiNom", A286Menu_PaiNom);
         n286Menu_PaiNom = T001B4_n286Menu_PaiNom[0];
         A287Menu_PaiTip = T001B4_A287Menu_PaiTip[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A287Menu_PaiTip", StringUtil.LTrim( StringUtil.Str( (decimal)(A287Menu_PaiTip), 2, 0)));
         n287Menu_PaiTip = T001B4_n287Menu_PaiTip[0];
         A288Menu_PaiAti = T001B4_A288Menu_PaiAti[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A288Menu_PaiAti", A288Menu_PaiAti);
         n288Menu_PaiAti = T001B4_n288Menu_PaiAti[0];
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors1B48( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_14( int A285Menu_PaiCod )
      {
         /* Using cursor T001B6 */
         pr_default.execute(4, new Object[] {n285Menu_PaiCod, A285Menu_PaiCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A285Menu_PaiCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Menu_Pai'.", "ForeignKeyNotFound", 1, "MENU_PAICOD");
               AnyError = 1;
               GX_FocusControl = edtMenu_PaiCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A286Menu_PaiNom = T001B6_A286Menu_PaiNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A286Menu_PaiNom", A286Menu_PaiNom);
         n286Menu_PaiNom = T001B6_n286Menu_PaiNom[0];
         A287Menu_PaiTip = T001B6_A287Menu_PaiTip[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A287Menu_PaiTip", StringUtil.LTrim( StringUtil.Str( (decimal)(A287Menu_PaiTip), 2, 0)));
         n287Menu_PaiTip = T001B6_n287Menu_PaiTip[0];
         A288Menu_PaiAti = T001B6_A288Menu_PaiAti[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A288Menu_PaiAti", A288Menu_PaiAti);
         n288Menu_PaiAti = T001B6_n288Menu_PaiAti[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A286Menu_PaiNom))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A287Menu_PaiTip), 2, 0, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.BoolToStr( A288Menu_PaiAti))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(4) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(4);
      }

      protected void GetKey1B48( )
      {
         /* Using cursor T001B7 */
         pr_default.execute(5, new Object[] {A277Menu_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound48 = 1;
         }
         else
         {
            RcdFound48 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T001B3 */
         pr_default.execute(1, new Object[] {A277Menu_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM1B48( 13) ;
            RcdFound48 = 1;
            A277Menu_Codigo = T001B3_A277Menu_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A277Menu_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A277Menu_Codigo), 6, 0)));
            A278Menu_Nome = T001B3_A278Menu_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A278Menu_Nome", A278Menu_Nome);
            A279Menu_Descricao = T001B3_A279Menu_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A279Menu_Descricao", A279Menu_Descricao);
            n279Menu_Descricao = T001B3_n279Menu_Descricao[0];
            A280Menu_Tipo = T001B3_A280Menu_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A280Menu_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A280Menu_Tipo), 2, 0)));
            A281Menu_Link = T001B3_A281Menu_Link[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A281Menu_Link", A281Menu_Link);
            n281Menu_Link = T001B3_n281Menu_Link[0];
            A40000Menu_Imagem_GXI = T001B3_A40000Menu_Imagem_GXI[0];
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgMenu_Imagem_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A282Menu_Imagem)) ? A40000Menu_Imagem_GXI : context.convertURL( context.PathToRelativeUrl( A282Menu_Imagem))));
            n40000Menu_Imagem_GXI = T001B3_n40000Menu_Imagem_GXI[0];
            A283Menu_Ordem = T001B3_A283Menu_Ordem[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A283Menu_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A283Menu_Ordem), 3, 0)));
            A284Menu_Ativo = T001B3_A284Menu_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A284Menu_Ativo", A284Menu_Ativo);
            A285Menu_PaiCod = T001B3_A285Menu_PaiCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A285Menu_PaiCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A285Menu_PaiCod), 6, 0)));
            n285Menu_PaiCod = T001B3_n285Menu_PaiCod[0];
            A282Menu_Imagem = T001B3_A282Menu_Imagem[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A282Menu_Imagem", A282Menu_Imagem);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgMenu_Imagem_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A282Menu_Imagem)) ? A40000Menu_Imagem_GXI : context.convertURL( context.PathToRelativeUrl( A282Menu_Imagem))));
            n282Menu_Imagem = T001B3_n282Menu_Imagem[0];
            Z277Menu_Codigo = A277Menu_Codigo;
            sMode48 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load1B48( ) ;
            if ( AnyError == 1 )
            {
               RcdFound48 = 0;
               InitializeNonKey1B48( ) ;
            }
            Gx_mode = sMode48;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound48 = 0;
            InitializeNonKey1B48( ) ;
            sMode48 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode48;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1B48( ) ;
         if ( RcdFound48 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound48 = 0;
         /* Using cursor T001B8 */
         pr_default.execute(6, new Object[] {A277Menu_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            while ( (pr_default.getStatus(6) != 101) && ( ( T001B8_A277Menu_Codigo[0] < A277Menu_Codigo ) ) )
            {
               pr_default.readNext(6);
            }
            if ( (pr_default.getStatus(6) != 101) && ( ( T001B8_A277Menu_Codigo[0] > A277Menu_Codigo ) ) )
            {
               A277Menu_Codigo = T001B8_A277Menu_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A277Menu_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A277Menu_Codigo), 6, 0)));
               RcdFound48 = 1;
            }
         }
         pr_default.close(6);
      }

      protected void move_previous( )
      {
         RcdFound48 = 0;
         /* Using cursor T001B9 */
         pr_default.execute(7, new Object[] {A277Menu_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T001B9_A277Menu_Codigo[0] > A277Menu_Codigo ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T001B9_A277Menu_Codigo[0] < A277Menu_Codigo ) ) )
            {
               A277Menu_Codigo = T001B9_A277Menu_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A277Menu_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A277Menu_Codigo), 6, 0)));
               RcdFound48 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey1B48( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtMenu_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert1B48( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound48 == 1 )
            {
               if ( A277Menu_Codigo != Z277Menu_Codigo )
               {
                  A277Menu_Codigo = Z277Menu_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A277Menu_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A277Menu_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "MENU_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtMenu_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtMenu_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update1B48( ) ;
                  GX_FocusControl = edtMenu_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A277Menu_Codigo != Z277Menu_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtMenu_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert1B48( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "MENU_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtMenu_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtMenu_Nome_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert1B48( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A277Menu_Codigo != Z277Menu_Codigo )
         {
            A277Menu_Codigo = Z277Menu_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A277Menu_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A277Menu_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "MENU_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtMenu_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtMenu_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency1B48( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T001B2 */
            pr_default.execute(0, new Object[] {A277Menu_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Menu"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z278Menu_Nome, T001B2_A278Menu_Nome[0]) != 0 ) || ( StringUtil.StrCmp(Z279Menu_Descricao, T001B2_A279Menu_Descricao[0]) != 0 ) || ( Z280Menu_Tipo != T001B2_A280Menu_Tipo[0] ) || ( StringUtil.StrCmp(Z281Menu_Link, T001B2_A281Menu_Link[0]) != 0 ) || ( Z283Menu_Ordem != T001B2_A283Menu_Ordem[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z284Menu_Ativo != T001B2_A284Menu_Ativo[0] ) || ( Z285Menu_PaiCod != T001B2_A285Menu_PaiCod[0] ) )
            {
               if ( StringUtil.StrCmp(Z278Menu_Nome, T001B2_A278Menu_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("menu:[seudo value changed for attri]"+"Menu_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z278Menu_Nome);
                  GXUtil.WriteLogRaw("Current: ",T001B2_A278Menu_Nome[0]);
               }
               if ( StringUtil.StrCmp(Z279Menu_Descricao, T001B2_A279Menu_Descricao[0]) != 0 )
               {
                  GXUtil.WriteLog("menu:[seudo value changed for attri]"+"Menu_Descricao");
                  GXUtil.WriteLogRaw("Old: ",Z279Menu_Descricao);
                  GXUtil.WriteLogRaw("Current: ",T001B2_A279Menu_Descricao[0]);
               }
               if ( Z280Menu_Tipo != T001B2_A280Menu_Tipo[0] )
               {
                  GXUtil.WriteLog("menu:[seudo value changed for attri]"+"Menu_Tipo");
                  GXUtil.WriteLogRaw("Old: ",Z280Menu_Tipo);
                  GXUtil.WriteLogRaw("Current: ",T001B2_A280Menu_Tipo[0]);
               }
               if ( StringUtil.StrCmp(Z281Menu_Link, T001B2_A281Menu_Link[0]) != 0 )
               {
                  GXUtil.WriteLog("menu:[seudo value changed for attri]"+"Menu_Link");
                  GXUtil.WriteLogRaw("Old: ",Z281Menu_Link);
                  GXUtil.WriteLogRaw("Current: ",T001B2_A281Menu_Link[0]);
               }
               if ( Z283Menu_Ordem != T001B2_A283Menu_Ordem[0] )
               {
                  GXUtil.WriteLog("menu:[seudo value changed for attri]"+"Menu_Ordem");
                  GXUtil.WriteLogRaw("Old: ",Z283Menu_Ordem);
                  GXUtil.WriteLogRaw("Current: ",T001B2_A283Menu_Ordem[0]);
               }
               if ( Z284Menu_Ativo != T001B2_A284Menu_Ativo[0] )
               {
                  GXUtil.WriteLog("menu:[seudo value changed for attri]"+"Menu_Ativo");
                  GXUtil.WriteLogRaw("Old: ",Z284Menu_Ativo);
                  GXUtil.WriteLogRaw("Current: ",T001B2_A284Menu_Ativo[0]);
               }
               if ( Z285Menu_PaiCod != T001B2_A285Menu_PaiCod[0] )
               {
                  GXUtil.WriteLog("menu:[seudo value changed for attri]"+"Menu_PaiCod");
                  GXUtil.WriteLogRaw("Old: ",Z285Menu_PaiCod);
                  GXUtil.WriteLogRaw("Current: ",T001B2_A285Menu_PaiCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Menu"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1B48( )
      {
         BeforeValidate1B48( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1B48( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1B48( 0) ;
            CheckOptimisticConcurrency1B48( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1B48( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1B48( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001B10 */
                     pr_default.execute(8, new Object[] {A278Menu_Nome, n279Menu_Descricao, A279Menu_Descricao, A280Menu_Tipo, n281Menu_Link, A281Menu_Link, n282Menu_Imagem, A282Menu_Imagem, n40000Menu_Imagem_GXI, A40000Menu_Imagem_GXI, A283Menu_Ordem, A284Menu_Ativo, n285Menu_PaiCod, A285Menu_PaiCod});
                     A277Menu_Codigo = T001B10_A277Menu_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A277Menu_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A277Menu_Codigo), 6, 0)));
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("Menu") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption1B0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1B48( ) ;
            }
            EndLevel1B48( ) ;
         }
         CloseExtendedTableCursors1B48( ) ;
      }

      protected void Update1B48( )
      {
         BeforeValidate1B48( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1B48( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1B48( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1B48( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1B48( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001B11 */
                     pr_default.execute(9, new Object[] {A278Menu_Nome, n279Menu_Descricao, A279Menu_Descricao, A280Menu_Tipo, n281Menu_Link, A281Menu_Link, A283Menu_Ordem, A284Menu_Ativo, n285Menu_PaiCod, A285Menu_PaiCod, A277Menu_Codigo});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("Menu") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Menu"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate1B48( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1B48( ) ;
         }
         CloseExtendedTableCursors1B48( ) ;
      }

      protected void DeferredUpdate1B48( )
      {
         if ( AnyError == 0 )
         {
            /* Using cursor T001B12 */
            pr_default.execute(10, new Object[] {n282Menu_Imagem, A282Menu_Imagem, n40000Menu_Imagem_GXI, A40000Menu_Imagem_GXI, A277Menu_Codigo});
            pr_default.close(10);
            dsDefault.SmartCacheProvider.SetUpdated("Menu") ;
         }
      }

      protected void delete( )
      {
         BeforeValidate1B48( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1B48( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1B48( ) ;
            AfterConfirm1B48( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1B48( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T001B13 */
                  pr_default.execute(11, new Object[] {A277Menu_Codigo});
                  pr_default.close(11);
                  dsDefault.SmartCacheProvider.SetUpdated("Menu") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode48 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel1B48( ) ;
         Gx_mode = sMode48;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls1B48( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T001B14 */
            pr_default.execute(12, new Object[] {n285Menu_PaiCod, A285Menu_PaiCod});
            A286Menu_PaiNom = T001B14_A286Menu_PaiNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A286Menu_PaiNom", A286Menu_PaiNom);
            n286Menu_PaiNom = T001B14_n286Menu_PaiNom[0];
            A287Menu_PaiTip = T001B14_A287Menu_PaiTip[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A287Menu_PaiTip", StringUtil.LTrim( StringUtil.Str( (decimal)(A287Menu_PaiTip), 2, 0)));
            n287Menu_PaiTip = T001B14_n287Menu_PaiTip[0];
            A288Menu_PaiAti = T001B14_A288Menu_PaiAti[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A288Menu_PaiAti", A288Menu_PaiAti);
            n288Menu_PaiAti = T001B14_n288Menu_PaiAti[0];
            pr_default.close(12);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T001B15 */
            pr_default.execute(13, new Object[] {A277Menu_Codigo});
            if ( (pr_default.getStatus(13) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Menu"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(13);
            /* Using cursor T001B16 */
            pr_default.execute(14, new Object[] {A277Menu_Codigo});
            if ( (pr_default.getStatus(14) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Menu Perfil"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(14);
         }
      }

      protected void EndLevel1B48( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1B48( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(12);
            context.CommitDataStores( "Menu");
            if ( AnyError == 0 )
            {
               ConfirmValues1B0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(12);
            context.RollbackDataStores( "Menu");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart1B48( )
      {
         /* Scan By routine */
         /* Using cursor T001B17 */
         pr_default.execute(15);
         RcdFound48 = 0;
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound48 = 1;
            A277Menu_Codigo = T001B17_A277Menu_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A277Menu_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A277Menu_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext1B48( )
      {
         /* Scan next routine */
         pr_default.readNext(15);
         RcdFound48 = 0;
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound48 = 1;
            A277Menu_Codigo = T001B17_A277Menu_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A277Menu_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A277Menu_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd1B48( )
      {
         pr_default.close(15);
      }

      protected void AfterConfirm1B48( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1B48( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1B48( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1B48( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1B48( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1B48( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes1B48( )
      {
         edtMenu_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMenu_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtMenu_Nome_Enabled), 5, 0)));
         edtMenu_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMenu_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtMenu_Descricao_Enabled), 5, 0)));
         cmbMenu_Tipo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbMenu_Tipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbMenu_Tipo.Enabled), 5, 0)));
         edtMenu_Link_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMenu_Link_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtMenu_Link_Enabled), 5, 0)));
         imgMenu_Imagem_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgMenu_Imagem_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgMenu_Imagem_Enabled), 5, 0)));
         edtMenu_Ordem_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMenu_Ordem_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtMenu_Ordem_Enabled), 5, 0)));
         edtMenu_PaiCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMenu_PaiCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtMenu_PaiCod_Enabled), 5, 0)));
         edtMenu_PaiNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMenu_PaiNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtMenu_PaiNom_Enabled), 5, 0)));
         chkMenu_Ativo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkMenu_Ativo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkMenu_Ativo.Enabled), 5, 0)));
         edtMenu_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMenu_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtMenu_Codigo_Enabled), 5, 0)));
         cmbMenu_PaiTip.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbMenu_PaiTip_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbMenu_PaiTip.Enabled), 5, 0)));
         edtMenu_PaiAti_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMenu_PaiAti_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtMenu_PaiAti_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues1B0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20204282302295");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("menu.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Menu_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z277Menu_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z277Menu_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z278Menu_Nome", StringUtil.RTrim( Z278Menu_Nome));
         GxWebStd.gx_hidden_field( context, "Z279Menu_Descricao", Z279Menu_Descricao);
         GxWebStd.gx_hidden_field( context, "Z280Menu_Tipo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z280Menu_Tipo), 2, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z281Menu_Link", Z281Menu_Link);
         GxWebStd.gx_hidden_field( context, "Z283Menu_Ordem", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z283Menu_Ordem), 3, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z284Menu_Ativo", Z284Menu_Ativo);
         GxWebStd.gx_hidden_field( context, "Z285Menu_PaiCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z285Menu_PaiCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N285Menu_PaiCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A285Menu_PaiCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vMENU_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Menu_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_MENU_PAICOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_Menu_PaiCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "MENU_IMAGEM_GXI", A40000Menu_Imagem_GXI);
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV13Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vMENU_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Menu_Codigo), "ZZZZZ9")));
         GXCCtlgxBlob = "MENU_IMAGEM" + "_gxBlob";
         GxWebStd.gx_hidden_field( context, GXCCtlgxBlob, A282Menu_Imagem);
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Menu";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A277Menu_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("menu:[SendSecurityCheck value for]"+"Menu_Codigo:"+context.localUtil.Format( (decimal)(A277Menu_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("menu:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("menu.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Menu_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "Menu" ;
      }

      public override String GetPgmdesc( )
      {
         return "Menu" ;
      }

      protected void InitializeNonKey1B48( )
      {
         A285Menu_PaiCod = 0;
         n285Menu_PaiCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A285Menu_PaiCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A285Menu_PaiCod), 6, 0)));
         n285Menu_PaiCod = ((0==A285Menu_PaiCod) ? true : false);
         A278Menu_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A278Menu_Nome", A278Menu_Nome);
         A279Menu_Descricao = "";
         n279Menu_Descricao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A279Menu_Descricao", A279Menu_Descricao);
         n279Menu_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A279Menu_Descricao)) ? true : false);
         A280Menu_Tipo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A280Menu_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A280Menu_Tipo), 2, 0)));
         A281Menu_Link = "";
         n281Menu_Link = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A281Menu_Link", A281Menu_Link);
         n281Menu_Link = (String.IsNullOrEmpty(StringUtil.RTrim( A281Menu_Link)) ? true : false);
         A282Menu_Imagem = "";
         n282Menu_Imagem = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A282Menu_Imagem", A282Menu_Imagem);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgMenu_Imagem_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A282Menu_Imagem)) ? A40000Menu_Imagem_GXI : context.convertURL( context.PathToRelativeUrl( A282Menu_Imagem))));
         n282Menu_Imagem = (String.IsNullOrEmpty(StringUtil.RTrim( A282Menu_Imagem)) ? true : false);
         A40000Menu_Imagem_GXI = "";
         n40000Menu_Imagem_GXI = false;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgMenu_Imagem_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A282Menu_Imagem)) ? A40000Menu_Imagem_GXI : context.convertURL( context.PathToRelativeUrl( A282Menu_Imagem))));
         A286Menu_PaiNom = "";
         n286Menu_PaiNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A286Menu_PaiNom", A286Menu_PaiNom);
         A287Menu_PaiTip = 0;
         n287Menu_PaiTip = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A287Menu_PaiTip", StringUtil.LTrim( StringUtil.Str( (decimal)(A287Menu_PaiTip), 2, 0)));
         A288Menu_PaiAti = false;
         n288Menu_PaiAti = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A288Menu_PaiAti", A288Menu_PaiAti);
         A283Menu_Ordem = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A283Menu_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A283Menu_Ordem), 3, 0)));
         A284Menu_Ativo = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A284Menu_Ativo", A284Menu_Ativo);
         Z278Menu_Nome = "";
         Z279Menu_Descricao = "";
         Z280Menu_Tipo = 0;
         Z281Menu_Link = "";
         Z283Menu_Ordem = 0;
         Z284Menu_Ativo = false;
         Z285Menu_PaiCod = 0;
      }

      protected void InitAll1B48( )
      {
         A277Menu_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A277Menu_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A277Menu_Codigo), 6, 0)));
         InitializeNonKey1B48( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A284Menu_Ativo = i284Menu_Ativo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A284Menu_Ativo", A284Menu_Ativo);
         A283Menu_Ordem = i283Menu_Ordem;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A283Menu_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A283Menu_Ordem), 3, 0)));
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20204282302315");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("menu.js", "?20204282302316");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockmenu_nome_Internalname = "TEXTBLOCKMENU_NOME";
         edtMenu_Nome_Internalname = "MENU_NOME";
         lblTextblockmenu_descricao_Internalname = "TEXTBLOCKMENU_DESCRICAO";
         edtMenu_Descricao_Internalname = "MENU_DESCRICAO";
         lblTextblockmenu_tipo_Internalname = "TEXTBLOCKMENU_TIPO";
         cmbMenu_Tipo_Internalname = "MENU_TIPO";
         lblTextblockmenu_link_Internalname = "TEXTBLOCKMENU_LINK";
         edtMenu_Link_Internalname = "MENU_LINK";
         lblMenu_link_righttext_Internalname = "MENU_LINK_RIGHTTEXT";
         tblTablemergedmenu_link_Internalname = "TABLEMERGEDMENU_LINK";
         lblTextblockmenu_imagem_Internalname = "TEXTBLOCKMENU_IMAGEM";
         imgMenu_Imagem_Internalname = "MENU_IMAGEM";
         lblMenu_imagem_righttext_Internalname = "MENU_IMAGEM_RIGHTTEXT";
         tblTablemergedmenu_imagem_Internalname = "TABLEMERGEDMENU_IMAGEM";
         lblTextblockmenu_ordem_Internalname = "TEXTBLOCKMENU_ORDEM";
         edtMenu_Ordem_Internalname = "MENU_ORDEM";
         lblTextblockmenu_paicod_Internalname = "TEXTBLOCKMENU_PAICOD";
         edtMenu_PaiCod_Internalname = "MENU_PAICOD";
         edtMenu_PaiNom_Internalname = "MENU_PAINOM";
         lblTextblockmenu_ativo_Internalname = "TEXTBLOCKMENU_ATIVO";
         chkMenu_Ativo_Internalname = "MENU_ATIVO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtMenu_Codigo_Internalname = "MENU_CODIGO";
         cmbMenu_PaiTip_Internalname = "MENU_PAITIP";
         edtMenu_PaiAti_Internalname = "MENU_PAIATI";
         Form.Internalname = "FORM";
         imgprompt_285_Internalname = "PROMPT_285";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Menu";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Menu";
         edtMenu_Link_Jsonclick = "";
         edtMenu_Link_Enabled = 1;
         imgMenu_Imagem_Enabled = 1;
         chkMenu_Ativo.Enabled = 1;
         edtMenu_PaiNom_Jsonclick = "";
         edtMenu_PaiNom_Enabled = 0;
         imgprompt_285_Visible = 1;
         imgprompt_285_Link = "";
         edtMenu_PaiCod_Jsonclick = "";
         edtMenu_PaiCod_Enabled = 1;
         edtMenu_Ordem_Jsonclick = "";
         edtMenu_Ordem_Enabled = 1;
         cmbMenu_Tipo_Jsonclick = "";
         cmbMenu_Tipo.Enabled = 1;
         edtMenu_Descricao_Jsonclick = "";
         edtMenu_Descricao_Enabled = 1;
         edtMenu_Nome_Jsonclick = "";
         edtMenu_Nome_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtMenu_PaiAti_Jsonclick = "";
         edtMenu_PaiAti_Enabled = 0;
         edtMenu_PaiAti_Visible = 1;
         cmbMenu_PaiTip_Jsonclick = "";
         cmbMenu_PaiTip.Enabled = 0;
         cmbMenu_PaiTip.Visible = 1;
         edtMenu_Codigo_Jsonclick = "";
         edtMenu_Codigo_Enabled = 0;
         edtMenu_Codigo_Visible = 1;
         chkMenu_Ativo.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public void Valid_Menu_paicod( int GX_Parm1 ,
                                     String GX_Parm2 ,
                                     GXCombobox cmbGX_Parm3 ,
                                     bool GX_Parm4 )
      {
         A285Menu_PaiCod = GX_Parm1;
         n285Menu_PaiCod = false;
         A286Menu_PaiNom = GX_Parm2;
         n286Menu_PaiNom = false;
         cmbMenu_PaiTip = cmbGX_Parm3;
         A287Menu_PaiTip = (short)(NumberUtil.Val( cmbMenu_PaiTip.CurrentValue, "."));
         n287Menu_PaiTip = false;
         cmbMenu_PaiTip.CurrentValue = StringUtil.LTrim( StringUtil.Str( (decimal)(A287Menu_PaiTip), 2, 0));
         A288Menu_PaiAti = GX_Parm4;
         n288Menu_PaiAti = false;
         /* Using cursor T001B14 */
         pr_default.execute(12, new Object[] {n285Menu_PaiCod, A285Menu_PaiCod});
         if ( (pr_default.getStatus(12) == 101) )
         {
            if ( ! ( (0==A285Menu_PaiCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Menu_Pai'.", "ForeignKeyNotFound", 1, "MENU_PAICOD");
               AnyError = 1;
               GX_FocusControl = edtMenu_PaiCod_Internalname;
            }
         }
         A286Menu_PaiNom = T001B14_A286Menu_PaiNom[0];
         n286Menu_PaiNom = T001B14_n286Menu_PaiNom[0];
         A287Menu_PaiTip = T001B14_A287Menu_PaiTip[0];
         cmbMenu_PaiTip.CurrentValue = StringUtil.LTrim( StringUtil.Str( (decimal)(A287Menu_PaiTip), 2, 0));
         n287Menu_PaiTip = T001B14_n287Menu_PaiTip[0];
         A288Menu_PaiAti = T001B14_A288Menu_PaiAti[0];
         n288Menu_PaiAti = T001B14_n288Menu_PaiAti[0];
         pr_default.close(12);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A286Menu_PaiNom = "";
            n286Menu_PaiNom = false;
            A287Menu_PaiTip = 0;
            n287Menu_PaiTip = false;
            cmbMenu_PaiTip.CurrentValue = StringUtil.LTrim( StringUtil.Str( (decimal)(A287Menu_PaiTip), 2, 0));
            A288Menu_PaiAti = false;
            n288Menu_PaiAti = false;
         }
         isValidOutput.Add(StringUtil.RTrim( A286Menu_PaiNom));
         cmbMenu_PaiTip.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A287Menu_PaiTip), 2, 0));
         isValidOutput.Add(cmbMenu_PaiTip);
         isValidOutput.Add(A288Menu_PaiAti);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7Menu_Codigo',fld:'vMENU_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E121B2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(12);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z278Menu_Nome = "";
         Z279Menu_Descricao = "";
         Z281Menu_Link = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockmenu_nome_Jsonclick = "";
         A278Menu_Nome = "";
         lblTextblockmenu_descricao_Jsonclick = "";
         A279Menu_Descricao = "";
         lblTextblockmenu_tipo_Jsonclick = "";
         lblTextblockmenu_link_Jsonclick = "";
         lblTextblockmenu_imagem_Jsonclick = "";
         lblTextblockmenu_ordem_Jsonclick = "";
         lblTextblockmenu_paicod_Jsonclick = "";
         A286Menu_PaiNom = "";
         lblTextblockmenu_ativo_Jsonclick = "";
         A282Menu_Imagem = "";
         A40000Menu_Imagem_GXI = "";
         lblMenu_imagem_righttext_Jsonclick = "";
         A281Menu_Link = "";
         lblMenu_link_righttext_Jsonclick = "";
         AV13Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode48 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z282Menu_Imagem = "";
         Z40000Menu_Imagem_GXI = "";
         Z286Menu_PaiNom = "";
         T001B4_A286Menu_PaiNom = new String[] {""} ;
         T001B4_n286Menu_PaiNom = new bool[] {false} ;
         T001B4_A287Menu_PaiTip = new short[1] ;
         T001B4_n287Menu_PaiTip = new bool[] {false} ;
         T001B4_A288Menu_PaiAti = new bool[] {false} ;
         T001B4_n288Menu_PaiAti = new bool[] {false} ;
         T001B5_A277Menu_Codigo = new int[1] ;
         T001B5_A278Menu_Nome = new String[] {""} ;
         T001B5_A279Menu_Descricao = new String[] {""} ;
         T001B5_n279Menu_Descricao = new bool[] {false} ;
         T001B5_A280Menu_Tipo = new short[1] ;
         T001B5_A281Menu_Link = new String[] {""} ;
         T001B5_n281Menu_Link = new bool[] {false} ;
         T001B5_A40000Menu_Imagem_GXI = new String[] {""} ;
         T001B5_n40000Menu_Imagem_GXI = new bool[] {false} ;
         T001B5_A283Menu_Ordem = new short[1] ;
         T001B5_A286Menu_PaiNom = new String[] {""} ;
         T001B5_n286Menu_PaiNom = new bool[] {false} ;
         T001B5_A287Menu_PaiTip = new short[1] ;
         T001B5_n287Menu_PaiTip = new bool[] {false} ;
         T001B5_A288Menu_PaiAti = new bool[] {false} ;
         T001B5_n288Menu_PaiAti = new bool[] {false} ;
         T001B5_A284Menu_Ativo = new bool[] {false} ;
         T001B5_A285Menu_PaiCod = new int[1] ;
         T001B5_n285Menu_PaiCod = new bool[] {false} ;
         T001B5_A282Menu_Imagem = new String[] {""} ;
         T001B5_n282Menu_Imagem = new bool[] {false} ;
         T001B6_A286Menu_PaiNom = new String[] {""} ;
         T001B6_n286Menu_PaiNom = new bool[] {false} ;
         T001B6_A287Menu_PaiTip = new short[1] ;
         T001B6_n287Menu_PaiTip = new bool[] {false} ;
         T001B6_A288Menu_PaiAti = new bool[] {false} ;
         T001B6_n288Menu_PaiAti = new bool[] {false} ;
         T001B7_A277Menu_Codigo = new int[1] ;
         T001B3_A277Menu_Codigo = new int[1] ;
         T001B3_A278Menu_Nome = new String[] {""} ;
         T001B3_A279Menu_Descricao = new String[] {""} ;
         T001B3_n279Menu_Descricao = new bool[] {false} ;
         T001B3_A280Menu_Tipo = new short[1] ;
         T001B3_A281Menu_Link = new String[] {""} ;
         T001B3_n281Menu_Link = new bool[] {false} ;
         T001B3_A40000Menu_Imagem_GXI = new String[] {""} ;
         T001B3_n40000Menu_Imagem_GXI = new bool[] {false} ;
         T001B3_A283Menu_Ordem = new short[1] ;
         T001B3_A284Menu_Ativo = new bool[] {false} ;
         T001B3_A285Menu_PaiCod = new int[1] ;
         T001B3_n285Menu_PaiCod = new bool[] {false} ;
         T001B3_A282Menu_Imagem = new String[] {""} ;
         T001B3_n282Menu_Imagem = new bool[] {false} ;
         T001B8_A277Menu_Codigo = new int[1] ;
         T001B9_A277Menu_Codigo = new int[1] ;
         T001B2_A277Menu_Codigo = new int[1] ;
         T001B2_A278Menu_Nome = new String[] {""} ;
         T001B2_A279Menu_Descricao = new String[] {""} ;
         T001B2_n279Menu_Descricao = new bool[] {false} ;
         T001B2_A280Menu_Tipo = new short[1] ;
         T001B2_A281Menu_Link = new String[] {""} ;
         T001B2_n281Menu_Link = new bool[] {false} ;
         T001B2_A40000Menu_Imagem_GXI = new String[] {""} ;
         T001B2_n40000Menu_Imagem_GXI = new bool[] {false} ;
         T001B2_A283Menu_Ordem = new short[1] ;
         T001B2_A284Menu_Ativo = new bool[] {false} ;
         T001B2_A285Menu_PaiCod = new int[1] ;
         T001B2_n285Menu_PaiCod = new bool[] {false} ;
         T001B2_A282Menu_Imagem = new String[] {""} ;
         T001B2_n282Menu_Imagem = new bool[] {false} ;
         T001B10_A277Menu_Codigo = new int[1] ;
         T001B14_A286Menu_PaiNom = new String[] {""} ;
         T001B14_n286Menu_PaiNom = new bool[] {false} ;
         T001B14_A287Menu_PaiTip = new short[1] ;
         T001B14_n287Menu_PaiTip = new bool[] {false} ;
         T001B14_A288Menu_PaiAti = new bool[] {false} ;
         T001B14_n288Menu_PaiAti = new bool[] {false} ;
         T001B15_A285Menu_PaiCod = new int[1] ;
         T001B15_n285Menu_PaiCod = new bool[] {false} ;
         T001B16_A277Menu_Codigo = new int[1] ;
         T001B16_A3Perfil_Codigo = new int[1] ;
         T001B17_A277Menu_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXCCtlgxBlob = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.menu__default(),
            new Object[][] {
                new Object[] {
               T001B2_A277Menu_Codigo, T001B2_A278Menu_Nome, T001B2_A279Menu_Descricao, T001B2_n279Menu_Descricao, T001B2_A280Menu_Tipo, T001B2_A281Menu_Link, T001B2_n281Menu_Link, T001B2_A40000Menu_Imagem_GXI, T001B2_n40000Menu_Imagem_GXI, T001B2_A283Menu_Ordem,
               T001B2_A284Menu_Ativo, T001B2_A285Menu_PaiCod, T001B2_n285Menu_PaiCod, T001B2_A282Menu_Imagem, T001B2_n282Menu_Imagem
               }
               , new Object[] {
               T001B3_A277Menu_Codigo, T001B3_A278Menu_Nome, T001B3_A279Menu_Descricao, T001B3_n279Menu_Descricao, T001B3_A280Menu_Tipo, T001B3_A281Menu_Link, T001B3_n281Menu_Link, T001B3_A40000Menu_Imagem_GXI, T001B3_n40000Menu_Imagem_GXI, T001B3_A283Menu_Ordem,
               T001B3_A284Menu_Ativo, T001B3_A285Menu_PaiCod, T001B3_n285Menu_PaiCod, T001B3_A282Menu_Imagem, T001B3_n282Menu_Imagem
               }
               , new Object[] {
               T001B4_A286Menu_PaiNom, T001B4_n286Menu_PaiNom, T001B4_A287Menu_PaiTip, T001B4_n287Menu_PaiTip, T001B4_A288Menu_PaiAti, T001B4_n288Menu_PaiAti
               }
               , new Object[] {
               T001B5_A277Menu_Codigo, T001B5_A278Menu_Nome, T001B5_A279Menu_Descricao, T001B5_n279Menu_Descricao, T001B5_A280Menu_Tipo, T001B5_A281Menu_Link, T001B5_n281Menu_Link, T001B5_A40000Menu_Imagem_GXI, T001B5_n40000Menu_Imagem_GXI, T001B5_A283Menu_Ordem,
               T001B5_A286Menu_PaiNom, T001B5_n286Menu_PaiNom, T001B5_A287Menu_PaiTip, T001B5_n287Menu_PaiTip, T001B5_A288Menu_PaiAti, T001B5_n288Menu_PaiAti, T001B5_A284Menu_Ativo, T001B5_A285Menu_PaiCod, T001B5_n285Menu_PaiCod, T001B5_A282Menu_Imagem,
               T001B5_n282Menu_Imagem
               }
               , new Object[] {
               T001B6_A286Menu_PaiNom, T001B6_n286Menu_PaiNom, T001B6_A287Menu_PaiTip, T001B6_n287Menu_PaiTip, T001B6_A288Menu_PaiAti, T001B6_n288Menu_PaiAti
               }
               , new Object[] {
               T001B7_A277Menu_Codigo
               }
               , new Object[] {
               T001B8_A277Menu_Codigo
               }
               , new Object[] {
               T001B9_A277Menu_Codigo
               }
               , new Object[] {
               T001B10_A277Menu_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T001B14_A286Menu_PaiNom, T001B14_n286Menu_PaiNom, T001B14_A287Menu_PaiTip, T001B14_n287Menu_PaiTip, T001B14_A288Menu_PaiAti, T001B14_n288Menu_PaiAti
               }
               , new Object[] {
               T001B15_A285Menu_PaiCod
               }
               , new Object[] {
               T001B16_A277Menu_Codigo, T001B16_A3Perfil_Codigo
               }
               , new Object[] {
               T001B17_A277Menu_Codigo
               }
            }
         );
         Z284Menu_Ativo = true;
         A284Menu_Ativo = true;
         i284Menu_Ativo = true;
         Z283Menu_Ordem = 0;
         A283Menu_Ordem = 0;
         i283Menu_Ordem = 0;
         AV13Pgmname = "Menu";
      }

      private short Z280Menu_Tipo ;
      private short Z283Menu_Ordem ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short A280Menu_Tipo ;
      private short A287Menu_PaiTip ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A283Menu_Ordem ;
      private short Gx_BScreen ;
      private short RcdFound48 ;
      private short GX_JID ;
      private short Z287Menu_PaiTip ;
      private short gxajaxcallmode ;
      private short i283Menu_Ordem ;
      private short wbTemp ;
      private int wcpOAV7Menu_Codigo ;
      private int Z277Menu_Codigo ;
      private int Z285Menu_PaiCod ;
      private int N285Menu_PaiCod ;
      private int A285Menu_PaiCod ;
      private int AV7Menu_Codigo ;
      private int trnEnded ;
      private int A277Menu_Codigo ;
      private int edtMenu_Codigo_Enabled ;
      private int edtMenu_Codigo_Visible ;
      private int edtMenu_PaiAti_Visible ;
      private int edtMenu_PaiAti_Enabled ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtMenu_Nome_Enabled ;
      private int edtMenu_Descricao_Enabled ;
      private int edtMenu_Ordem_Enabled ;
      private int edtMenu_PaiCod_Enabled ;
      private int imgprompt_285_Visible ;
      private int edtMenu_PaiNom_Enabled ;
      private int imgMenu_Imagem_Enabled ;
      private int edtMenu_Link_Enabled ;
      private int AV11Insert_Menu_PaiCod ;
      private int AV14GXV1 ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z278Menu_Nome ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String chkMenu_Ativo_Internalname ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtMenu_Nome_Internalname ;
      private String edtMenu_Codigo_Internalname ;
      private String edtMenu_Codigo_Jsonclick ;
      private String cmbMenu_PaiTip_Internalname ;
      private String cmbMenu_PaiTip_Jsonclick ;
      private String edtMenu_PaiAti_Internalname ;
      private String edtMenu_PaiAti_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockmenu_nome_Internalname ;
      private String lblTextblockmenu_nome_Jsonclick ;
      private String A278Menu_Nome ;
      private String edtMenu_Nome_Jsonclick ;
      private String lblTextblockmenu_descricao_Internalname ;
      private String lblTextblockmenu_descricao_Jsonclick ;
      private String edtMenu_Descricao_Internalname ;
      private String edtMenu_Descricao_Jsonclick ;
      private String lblTextblockmenu_tipo_Internalname ;
      private String lblTextblockmenu_tipo_Jsonclick ;
      private String cmbMenu_Tipo_Internalname ;
      private String cmbMenu_Tipo_Jsonclick ;
      private String lblTextblockmenu_link_Internalname ;
      private String lblTextblockmenu_link_Jsonclick ;
      private String lblTextblockmenu_imagem_Internalname ;
      private String lblTextblockmenu_imagem_Jsonclick ;
      private String lblTextblockmenu_ordem_Internalname ;
      private String lblTextblockmenu_ordem_Jsonclick ;
      private String edtMenu_Ordem_Internalname ;
      private String edtMenu_Ordem_Jsonclick ;
      private String lblTextblockmenu_paicod_Internalname ;
      private String lblTextblockmenu_paicod_Jsonclick ;
      private String edtMenu_PaiCod_Internalname ;
      private String edtMenu_PaiCod_Jsonclick ;
      private String imgprompt_285_Internalname ;
      private String imgprompt_285_Link ;
      private String edtMenu_PaiNom_Internalname ;
      private String A286Menu_PaiNom ;
      private String edtMenu_PaiNom_Jsonclick ;
      private String lblTextblockmenu_ativo_Internalname ;
      private String lblTextblockmenu_ativo_Jsonclick ;
      private String tblTablemergedmenu_imagem_Internalname ;
      private String imgMenu_Imagem_Internalname ;
      private String lblMenu_imagem_righttext_Internalname ;
      private String lblMenu_imagem_righttext_Jsonclick ;
      private String tblTablemergedmenu_link_Internalname ;
      private String edtMenu_Link_Internalname ;
      private String edtMenu_Link_Jsonclick ;
      private String lblMenu_link_righttext_Internalname ;
      private String lblMenu_link_righttext_Jsonclick ;
      private String AV13Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode48 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z286Menu_PaiNom ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXCCtlgxBlob ;
      private String Dvpanel_tableattributes_Internalname ;
      private bool Z284Menu_Ativo ;
      private bool entryPointCalled ;
      private bool n285Menu_PaiCod ;
      private bool toggleJsOutput ;
      private bool n287Menu_PaiTip ;
      private bool wbErr ;
      private bool A288Menu_PaiAti ;
      private bool A284Menu_Ativo ;
      private bool A282Menu_Imagem_IsBlob ;
      private bool n279Menu_Descricao ;
      private bool n281Menu_Link ;
      private bool n282Menu_Imagem ;
      private bool n286Menu_PaiNom ;
      private bool n288Menu_PaiAti ;
      private bool n40000Menu_Imagem_GXI ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool Z288Menu_PaiAti ;
      private bool Gx_longc ;
      private bool i284Menu_Ativo ;
      private String Z279Menu_Descricao ;
      private String Z281Menu_Link ;
      private String A279Menu_Descricao ;
      private String A40000Menu_Imagem_GXI ;
      private String A281Menu_Link ;
      private String Z40000Menu_Imagem_GXI ;
      private String A282Menu_Imagem ;
      private String Z282Menu_Imagem ;
      private IGxSession AV10WebSession ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbMenu_Tipo ;
      private GXCheckbox chkMenu_Ativo ;
      private GXCombobox cmbMenu_PaiTip ;
      private IDataStoreProvider pr_default ;
      private String[] T001B4_A286Menu_PaiNom ;
      private bool[] T001B4_n286Menu_PaiNom ;
      private short[] T001B4_A287Menu_PaiTip ;
      private bool[] T001B4_n287Menu_PaiTip ;
      private bool[] T001B4_A288Menu_PaiAti ;
      private bool[] T001B4_n288Menu_PaiAti ;
      private int[] T001B5_A277Menu_Codigo ;
      private String[] T001B5_A278Menu_Nome ;
      private String[] T001B5_A279Menu_Descricao ;
      private bool[] T001B5_n279Menu_Descricao ;
      private short[] T001B5_A280Menu_Tipo ;
      private String[] T001B5_A281Menu_Link ;
      private bool[] T001B5_n281Menu_Link ;
      private String[] T001B5_A40000Menu_Imagem_GXI ;
      private bool[] T001B5_n40000Menu_Imagem_GXI ;
      private short[] T001B5_A283Menu_Ordem ;
      private String[] T001B5_A286Menu_PaiNom ;
      private bool[] T001B5_n286Menu_PaiNom ;
      private short[] T001B5_A287Menu_PaiTip ;
      private bool[] T001B5_n287Menu_PaiTip ;
      private bool[] T001B5_A288Menu_PaiAti ;
      private bool[] T001B5_n288Menu_PaiAti ;
      private bool[] T001B5_A284Menu_Ativo ;
      private int[] T001B5_A285Menu_PaiCod ;
      private bool[] T001B5_n285Menu_PaiCod ;
      private String[] T001B5_A282Menu_Imagem ;
      private bool[] T001B5_n282Menu_Imagem ;
      private String[] T001B6_A286Menu_PaiNom ;
      private bool[] T001B6_n286Menu_PaiNom ;
      private short[] T001B6_A287Menu_PaiTip ;
      private bool[] T001B6_n287Menu_PaiTip ;
      private bool[] T001B6_A288Menu_PaiAti ;
      private bool[] T001B6_n288Menu_PaiAti ;
      private int[] T001B7_A277Menu_Codigo ;
      private int[] T001B3_A277Menu_Codigo ;
      private String[] T001B3_A278Menu_Nome ;
      private String[] T001B3_A279Menu_Descricao ;
      private bool[] T001B3_n279Menu_Descricao ;
      private short[] T001B3_A280Menu_Tipo ;
      private String[] T001B3_A281Menu_Link ;
      private bool[] T001B3_n281Menu_Link ;
      private String[] T001B3_A40000Menu_Imagem_GXI ;
      private bool[] T001B3_n40000Menu_Imagem_GXI ;
      private short[] T001B3_A283Menu_Ordem ;
      private bool[] T001B3_A284Menu_Ativo ;
      private int[] T001B3_A285Menu_PaiCod ;
      private bool[] T001B3_n285Menu_PaiCod ;
      private String[] T001B3_A282Menu_Imagem ;
      private bool[] T001B3_n282Menu_Imagem ;
      private int[] T001B8_A277Menu_Codigo ;
      private int[] T001B9_A277Menu_Codigo ;
      private int[] T001B2_A277Menu_Codigo ;
      private String[] T001B2_A278Menu_Nome ;
      private String[] T001B2_A279Menu_Descricao ;
      private bool[] T001B2_n279Menu_Descricao ;
      private short[] T001B2_A280Menu_Tipo ;
      private String[] T001B2_A281Menu_Link ;
      private bool[] T001B2_n281Menu_Link ;
      private String[] T001B2_A40000Menu_Imagem_GXI ;
      private bool[] T001B2_n40000Menu_Imagem_GXI ;
      private short[] T001B2_A283Menu_Ordem ;
      private bool[] T001B2_A284Menu_Ativo ;
      private int[] T001B2_A285Menu_PaiCod ;
      private bool[] T001B2_n285Menu_PaiCod ;
      private String[] T001B2_A282Menu_Imagem ;
      private bool[] T001B2_n282Menu_Imagem ;
      private int[] T001B10_A277Menu_Codigo ;
      private String[] T001B14_A286Menu_PaiNom ;
      private bool[] T001B14_n286Menu_PaiNom ;
      private short[] T001B14_A287Menu_PaiTip ;
      private bool[] T001B14_n287Menu_PaiTip ;
      private bool[] T001B14_A288Menu_PaiAti ;
      private bool[] T001B14_n288Menu_PaiAti ;
      private int[] T001B15_A285Menu_PaiCod ;
      private bool[] T001B15_n285Menu_PaiCod ;
      private int[] T001B16_A277Menu_Codigo ;
      private int[] T001B16_A3Perfil_Codigo ;
      private int[] T001B17_A277Menu_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class menu__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT001B5 ;
          prmT001B5 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001B4 ;
          prmT001B4 = new Object[] {
          new Object[] {"@Menu_PaiCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001B6 ;
          prmT001B6 = new Object[] {
          new Object[] {"@Menu_PaiCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001B7 ;
          prmT001B7 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001B3 ;
          prmT001B3 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001B8 ;
          prmT001B8 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001B9 ;
          prmT001B9 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001B2 ;
          prmT001B2 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001B10 ;
          prmT001B10 = new Object[] {
          new Object[] {"@Menu_Nome",SqlDbType.Char,30,0} ,
          new Object[] {"@Menu_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@Menu_Tipo",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@Menu_Link",SqlDbType.VarChar,80,0} ,
          new Object[] {"@Menu_Imagem",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@Menu_Imagem_GXI",SqlDbType.VarChar,2048,0} ,
          new Object[] {"@Menu_Ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@Menu_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Menu_PaiCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001B11 ;
          prmT001B11 = new Object[] {
          new Object[] {"@Menu_Nome",SqlDbType.Char,30,0} ,
          new Object[] {"@Menu_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@Menu_Tipo",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@Menu_Link",SqlDbType.VarChar,80,0} ,
          new Object[] {"@Menu_Ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@Menu_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Menu_PaiCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001B12 ;
          prmT001B12 = new Object[] {
          new Object[] {"@Menu_Imagem",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@Menu_Imagem_GXI",SqlDbType.VarChar,2048,0} ,
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001B13 ;
          prmT001B13 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001B15 ;
          prmT001B15 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001B16 ;
          prmT001B16 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001B17 ;
          prmT001B17 = new Object[] {
          } ;
          Object[] prmT001B14 ;
          prmT001B14 = new Object[] {
          new Object[] {"@Menu_PaiCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T001B2", "SELECT [Menu_Codigo], [Menu_Nome], [Menu_Descricao], [Menu_Tipo], [Menu_Link], [Menu_Imagem_GXI], [Menu_Ordem], [Menu_Ativo], [Menu_PaiCod] AS Menu_PaiCod, [Menu_Imagem] FROM [Menu] WITH (UPDLOCK) WHERE [Menu_Codigo] = @Menu_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001B2,1,0,true,false )
             ,new CursorDef("T001B3", "SELECT [Menu_Codigo], [Menu_Nome], [Menu_Descricao], [Menu_Tipo], [Menu_Link], [Menu_Imagem_GXI], [Menu_Ordem], [Menu_Ativo], [Menu_PaiCod] AS Menu_PaiCod, [Menu_Imagem] FROM [Menu] WITH (NOLOCK) WHERE [Menu_Codigo] = @Menu_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001B3,1,0,true,false )
             ,new CursorDef("T001B4", "SELECT [Menu_Nome] AS Menu_PaiNom, [Menu_Tipo] AS Menu_PaiTip, [Menu_Ativo] AS Menu_PaiAti FROM [Menu] WITH (NOLOCK) WHERE [Menu_Codigo] = @Menu_PaiCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001B4,1,0,true,false )
             ,new CursorDef("T001B5", "SELECT TM1.[Menu_Codigo], TM1.[Menu_Nome], TM1.[Menu_Descricao], TM1.[Menu_Tipo], TM1.[Menu_Link], TM1.[Menu_Imagem_GXI], TM1.[Menu_Ordem], T2.[Menu_Nome] AS Menu_PaiNom, T2.[Menu_Tipo] AS Menu_PaiTip, T2.[Menu_Ativo] AS Menu_PaiAti, TM1.[Menu_Ativo], TM1.[Menu_PaiCod] AS Menu_PaiCod, TM1.[Menu_Imagem] FROM ([Menu] TM1 WITH (NOLOCK) LEFT JOIN [Menu] T2 WITH (NOLOCK) ON T2.[Menu_Codigo] = TM1.[Menu_PaiCod]) WHERE TM1.[Menu_Codigo] = @Menu_Codigo ORDER BY TM1.[Menu_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001B5,100,0,true,false )
             ,new CursorDef("T001B6", "SELECT [Menu_Nome] AS Menu_PaiNom, [Menu_Tipo] AS Menu_PaiTip, [Menu_Ativo] AS Menu_PaiAti FROM [Menu] WITH (NOLOCK) WHERE [Menu_Codigo] = @Menu_PaiCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001B6,1,0,true,false )
             ,new CursorDef("T001B7", "SELECT [Menu_Codigo] FROM [Menu] WITH (NOLOCK) WHERE [Menu_Codigo] = @Menu_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001B7,1,0,true,false )
             ,new CursorDef("T001B8", "SELECT TOP 1 [Menu_Codigo] FROM [Menu] WITH (NOLOCK) WHERE ( [Menu_Codigo] > @Menu_Codigo) ORDER BY [Menu_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001B8,1,0,true,true )
             ,new CursorDef("T001B9", "SELECT TOP 1 [Menu_Codigo] FROM [Menu] WITH (NOLOCK) WHERE ( [Menu_Codigo] < @Menu_Codigo) ORDER BY [Menu_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001B9,1,0,true,true )
             ,new CursorDef("T001B10", "INSERT INTO [Menu]([Menu_Nome], [Menu_Descricao], [Menu_Tipo], [Menu_Link], [Menu_Imagem], [Menu_Imagem_GXI], [Menu_Ordem], [Menu_Ativo], [Menu_PaiCod]) VALUES(@Menu_Nome, @Menu_Descricao, @Menu_Tipo, @Menu_Link, @Menu_Imagem, @Menu_Imagem_GXI, @Menu_Ordem, @Menu_Ativo, @Menu_PaiCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT001B10)
             ,new CursorDef("T001B11", "UPDATE [Menu] SET [Menu_Nome]=@Menu_Nome, [Menu_Descricao]=@Menu_Descricao, [Menu_Tipo]=@Menu_Tipo, [Menu_Link]=@Menu_Link, [Menu_Ordem]=@Menu_Ordem, [Menu_Ativo]=@Menu_Ativo, [Menu_PaiCod]=@Menu_PaiCod  WHERE [Menu_Codigo] = @Menu_Codigo", GxErrorMask.GX_NOMASK,prmT001B11)
             ,new CursorDef("T001B12", "UPDATE [Menu] SET [Menu_Imagem]=@Menu_Imagem, [Menu_Imagem_GXI]=@Menu_Imagem_GXI  WHERE [Menu_Codigo] = @Menu_Codigo", GxErrorMask.GX_NOMASK,prmT001B12)
             ,new CursorDef("T001B13", "DELETE FROM [Menu]  WHERE [Menu_Codigo] = @Menu_Codigo", GxErrorMask.GX_NOMASK,prmT001B13)
             ,new CursorDef("T001B14", "SELECT [Menu_Nome] AS Menu_PaiNom, [Menu_Tipo] AS Menu_PaiTip, [Menu_Ativo] AS Menu_PaiAti FROM [Menu] WITH (NOLOCK) WHERE [Menu_Codigo] = @Menu_PaiCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001B14,1,0,true,false )
             ,new CursorDef("T001B15", "SELECT TOP 1 [Menu_Codigo] AS Menu_PaiCod FROM [Menu] WITH (NOLOCK) WHERE [Menu_PaiCod] = @Menu_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001B15,1,0,true,true )
             ,new CursorDef("T001B16", "SELECT TOP 1 [Menu_Codigo], [Perfil_Codigo] FROM [MenuPerfil] WITH (NOLOCK) WHERE [Menu_Codigo] = @Menu_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001B16,1,0,true,true )
             ,new CursorDef("T001B17", "SELECT [Menu_Codigo] FROM [Menu] WITH (NOLOCK) ORDER BY [Menu_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001B17,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 30) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getMultimediaUri(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((short[]) buf[9])[0] = rslt.getShort(7) ;
                ((bool[]) buf[10])[0] = rslt.getBool(8) ;
                ((int[]) buf[11])[0] = rslt.getInt(9) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((String[]) buf[13])[0] = rslt.getMultimediaFile(10, rslt.getVarchar(6)) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 30) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getMultimediaUri(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((short[]) buf[9])[0] = rslt.getShort(7) ;
                ((bool[]) buf[10])[0] = rslt.getBool(8) ;
                ((int[]) buf[11])[0] = rslt.getInt(9) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((String[]) buf[13])[0] = rslt.getMultimediaFile(10, rslt.getVarchar(6)) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 30) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((short[]) buf[2])[0] = rslt.getShort(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((bool[]) buf[4])[0] = rslt.getBool(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 30) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getMultimediaUri(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((short[]) buf[9])[0] = rslt.getShort(7) ;
                ((String[]) buf[10])[0] = rslt.getString(8, 30) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((short[]) buf[12])[0] = rslt.getShort(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((bool[]) buf[14])[0] = rslt.getBool(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((bool[]) buf[16])[0] = rslt.getBool(11) ;
                ((int[]) buf[17])[0] = rslt.getInt(12) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                ((String[]) buf[19])[0] = rslt.getMultimediaFile(13, rslt.getVarchar(6)) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(13);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 30) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((short[]) buf[2])[0] = rslt.getShort(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((bool[]) buf[4])[0] = rslt.getBool(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((String[]) buf[0])[0] = rslt.getString(1, 30) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((short[]) buf[2])[0] = rslt.getShort(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((bool[]) buf[4])[0] = rslt.getBool(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                stmt.SetParameter(3, (short)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameterMultimedia(6, (String)parms[9], (String)parms[7]);
                }
                stmt.SetParameter(7, (short)parms[10]);
                stmt.SetParameter(8, (bool)parms[11]);
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[13]);
                }
                return;
             case 9 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                stmt.SetParameter(3, (short)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[5]);
                }
                stmt.SetParameter(5, (short)parms[6]);
                stmt.SetParameter(6, (bool)parms[7]);
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[9]);
                }
                stmt.SetParameter(8, (int)parms[10]);
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameterMultimedia(2, (String)parms[3], (String)parms[1]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
