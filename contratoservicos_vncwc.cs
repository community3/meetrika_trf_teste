/*
               File: ContratoServicos_VncWC
        Description: Contrato Servicos_Vnc WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:30:41.77
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratoservicos_vncwc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contratoservicos_vncwc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contratoservicos_vncwc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratoSrvVnc_CntSrvCod )
      {
         this.AV18ContratoSrvVnc_CntSrvCod = aP0_ContratoSrvVnc_CntSrvCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbContratoSrvVnc_DoStatusDmn = new GXCombobox();
         cmbContratoSrvVnc_StatusDmn = new GXCombobox();
         cmbContratoSrvVnc_SrvVncStatus = new GXCombobox();
         cmbContratoSrvVnc_SemCusto = new GXCombobox();
         chkContratoServicosVnc_Ativo = new GXCheckbox();
         cmbavAreatrabalho_codigo = new GXCombobox();
         dynavServico_codigo = new GXCombobox();
         cmbavRegra = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV18ContratoSrvVnc_CntSrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ContratoSrvVnc_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ContratoSrvVnc_CntSrvCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV18ContratoSrvVnc_CntSrvCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vSERVICO_CODIGO") == 0 )
               {
                  AV19AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19AreaTrabalho_Codigo), 6, 0)));
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  GXDLVvSERVICO_CODIGOGA2( AV19AreaTrabalho_Codigo) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_8 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_8_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_8_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
                  AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
                  AV58TFContratoServicosVnc_Descricao = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV58TFContratoServicosVnc_Descricao", AV58TFContratoServicosVnc_Descricao);
                  AV59TFContratoServicosVnc_Descricao_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV59TFContratoServicosVnc_Descricao_Sel", AV59TFContratoServicosVnc_Descricao_Sel);
                  AV35TFContratoSrvVnc_ServicoVncSigla = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35TFContratoSrvVnc_ServicoVncSigla", AV35TFContratoSrvVnc_ServicoVncSigla);
                  AV36TFContratoSrvVnc_ServicoVncSigla_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36TFContratoSrvVnc_ServicoVncSigla_Sel", AV36TFContratoSrvVnc_ServicoVncSigla_Sel);
                  AV43TFContratoSrvVnc_SrvVncCntNum = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFContratoSrvVnc_SrvVncCntNum", AV43TFContratoSrvVnc_SrvVncCntNum);
                  AV44TFContratoSrvVnc_SrvVncCntNum_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44TFContratoSrvVnc_SrvVncCntNum_Sel", AV44TFContratoSrvVnc_SrvVncCntNum_Sel);
                  AV47TFContratoSrvVnc_SemCusto_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV47TFContratoSrvVnc_SemCusto_Sel", StringUtil.Str( (decimal)(AV47TFContratoSrvVnc_SemCusto_Sel), 1, 0));
                  AV50TFContratoServicosVnc_Ativo_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV50TFContratoServicosVnc_Ativo_Sel", StringUtil.Str( (decimal)(AV50TFContratoServicosVnc_Ativo_Sel), 1, 0));
                  AV18ContratoSrvVnc_CntSrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ContratoSrvVnc_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ContratoSrvVnc_CntSrvCod), 6, 0)));
                  AV60ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace", AV60ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace);
                  AV29ddo_ContratoSrvVnc_DoStatusDmnTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29ddo_ContratoSrvVnc_DoStatusDmnTitleControlIdToReplace", AV29ddo_ContratoSrvVnc_DoStatusDmnTitleControlIdToReplace);
                  AV33ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace", AV33ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace);
                  AV37ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace", AV37ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace);
                  AV41ddo_ContratoSrvVnc_SrvVncStatusTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41ddo_ContratoSrvVnc_SrvVncStatusTitleControlIdToReplace", AV41ddo_ContratoSrvVnc_SrvVncStatusTitleControlIdToReplace);
                  AV45ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace", AV45ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace);
                  AV48ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace", AV48ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace);
                  AV51ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV51ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace", AV51ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace);
                  A6AreaTrabalho_Descricao = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A6AreaTrabalho_Descricao", A6AreaTrabalho_Descricao);
                  A72AreaTrabalho_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A72AreaTrabalho_Ativo", A72AreaTrabalho_Ativo);
                  A5AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
                  A1297ContratadaUsuario_AreaTrabalhoDes = GetNextPar( );
                  n1297ContratadaUsuario_AreaTrabalhoDes = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1297ContratadaUsuario_AreaTrabalhoDes", A1297ContratadaUsuario_AreaTrabalhoDes);
                  A69ContratadaUsuario_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A69ContratadaUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0)));
                  A1228ContratadaUsuario_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n1228ContratadaUsuario_AreaTrabalhoCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1228ContratadaUsuario_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1228ContratadaUsuario_AreaTrabalhoCod), 6, 0)));
                  AV67Pgmname = GetNextPar( );
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV28TFContratoSrvVnc_DoStatusDmn_Sels);
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV32TFContratoSrvVnc_StatusDmn_Sels);
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV40TFContratoSrvVnc_SrvVncStatus_Sels);
                  A75Contrato_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n75Contrato_AreaTrabalhoCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A75Contrato_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A75Contrato_AreaTrabalhoCod), 6, 0)));
                  AV19AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19AreaTrabalho_Codigo), 6, 0)));
                  A921ContratoSrvVnc_ServicoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n921ContratoSrvVnc_ServicoCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A921ContratoSrvVnc_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A921ContratoSrvVnc_ServicoCod), 6, 0)));
                  AV20Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Servico_Codigo), 6, 0)));
                  A917ContratoSrvVnc_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A924ContratoSrvVnc_ServicoVncSigla = GetNextPar( );
                  n924ContratoSrvVnc_ServicoVncSigla = false;
                  A1800ContratoSrvVnc_DoStatusDmn = GetNextPar( );
                  n1800ContratoSrvVnc_DoStatusDmn = false;
                  A1084ContratoSrvVnc_StatusDmn = GetNextPar( );
                  n1084ContratoSrvVnc_StatusDmn = false;
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
                  A1453ContratoServicosVnc_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
                  n1453ContratoServicosVnc_Ativo = false;
                  A1092ContratoSrvVnc_PrestadoraPesNom = GetNextPar( );
                  n1092ContratoSrvVnc_PrestadoraPesNom = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1092ContratoSrvVnc_PrestadoraPesNom", A1092ContratoSrvVnc_PrestadoraPesNom);
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV58TFContratoServicosVnc_Descricao, AV59TFContratoServicosVnc_Descricao_Sel, AV35TFContratoSrvVnc_ServicoVncSigla, AV36TFContratoSrvVnc_ServicoVncSigla_Sel, AV43TFContratoSrvVnc_SrvVncCntNum, AV44TFContratoSrvVnc_SrvVncCntNum_Sel, AV47TFContratoSrvVnc_SemCusto_Sel, AV50TFContratoServicosVnc_Ativo_Sel, AV18ContratoSrvVnc_CntSrvCod, AV60ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace, AV29ddo_ContratoSrvVnc_DoStatusDmnTitleControlIdToReplace, AV33ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace, AV37ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace, AV41ddo_ContratoSrvVnc_SrvVncStatusTitleControlIdToReplace, AV45ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace, AV48ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace, AV51ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace, A6AreaTrabalho_Descricao, A72AreaTrabalho_Ativo, A5AreaTrabalho_Codigo, A1297ContratadaUsuario_AreaTrabalhoDes, A69ContratadaUsuario_UsuarioCod, A1228ContratadaUsuario_AreaTrabalhoCod, AV67Pgmname, AV28TFContratoSrvVnc_DoStatusDmn_Sels, AV32TFContratoSrvVnc_StatusDmn_Sels, AV40TFContratoSrvVnc_SrvVncStatus_Sels, A75Contrato_AreaTrabalhoCod, AV19AreaTrabalho_Codigo, A921ContratoSrvVnc_ServicoCod, AV20Servico_Codigo, A917ContratoSrvVnc_Codigo, A924ContratoSrvVnc_ServicoVncSigla, A1800ContratoSrvVnc_DoStatusDmn, A1084ContratoSrvVnc_StatusDmn, AV6WWPContext, A1453ContratoServicosVnc_Ativo, A1092ContratoSrvVnc_PrestadoraPesNom, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAGA2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV67Pgmname = "ContratoServicos_VncWC";
               context.Gx_err = 0;
               edtavPrestador_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPrestador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPrestador_Enabled), 5, 0)));
               WSGA2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contrato Servicos_Vnc WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299304238");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratoservicos_vncwc.aspx") + "?" + UrlEncode("" +AV18ContratoSrvVnc_CntSrvCod)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSVNC_DESCRICAO", AV58TFContratoServicosVnc_Descricao);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSVNC_DESCRICAO_SEL", AV59TFContratoServicosVnc_Descricao_Sel);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSRVVNC_SERVICOVNCSIGLA", StringUtil.RTrim( AV35TFContratoSrvVnc_ServicoVncSigla));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL", StringUtil.RTrim( AV36TFContratoSrvVnc_ServicoVncSigla_Sel));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSRVVNC_SRVVNCCNTNUM", StringUtil.RTrim( AV43TFContratoSrvVnc_SrvVncCntNum));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSRVVNC_SRVVNCCNTNUM_SEL", StringUtil.RTrim( AV44TFContratoSrvVnc_SrvVncCntNum_Sel));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSRVVNC_SEMCUSTO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV47TFContratoSrvVnc_SemCusto_Sel), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSVNC_ATIVO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV50TFContratoServicosVnc_Ativo_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_8", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_8), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV54GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV55GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV52DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV52DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOSERVICOSVNC_DESCRICAOTITLEFILTERDATA", AV57ContratoServicosVnc_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOSERVICOSVNC_DESCRICAOTITLEFILTERDATA", AV57ContratoServicosVnc_DescricaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOSRVVNC_DOSTATUSDMNTITLEFILTERDATA", AV26ContratoSrvVnc_DoStatusDmnTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOSRVVNC_DOSTATUSDMNTITLEFILTERDATA", AV26ContratoSrvVnc_DoStatusDmnTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOSRVVNC_STATUSDMNTITLEFILTERDATA", AV30ContratoSrvVnc_StatusDmnTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOSRVVNC_STATUSDMNTITLEFILTERDATA", AV30ContratoSrvVnc_StatusDmnTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOSRVVNC_SERVICOVNCSIGLATITLEFILTERDATA", AV34ContratoSrvVnc_ServicoVncSiglaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOSRVVNC_SERVICOVNCSIGLATITLEFILTERDATA", AV34ContratoSrvVnc_ServicoVncSiglaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOSRVVNC_SRVVNCSTATUSTITLEFILTERDATA", AV38ContratoSrvVnc_SrvVncStatusTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOSRVVNC_SRVVNCSTATUSTITLEFILTERDATA", AV38ContratoSrvVnc_SrvVncStatusTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOSRVVNC_SRVVNCCNTNUMTITLEFILTERDATA", AV42ContratoSrvVnc_SrvVncCntNumTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOSRVVNC_SRVVNCCNTNUMTITLEFILTERDATA", AV42ContratoSrvVnc_SrvVncCntNumTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOSRVVNC_SEMCUSTOTITLEFILTERDATA", AV46ContratoSrvVnc_SemCustoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOSRVVNC_SEMCUSTOTITLEFILTERDATA", AV46ContratoSrvVnc_SemCustoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOSERVICOSVNC_ATIVOTITLEFILTERDATA", AV49ContratoServicosVnc_AtivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOSERVICOSVNC_ATIVOTITLEFILTERDATA", AV49ContratoServicosVnc_AtivoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV18ContratoSrvVnc_CntSrvCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV18ContratoSrvVnc_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTRATOSRVVNC_CNTSRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18ContratoSrvVnc_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"AREATRABALHO_DESCRICAO", A6AreaTrabalho_Descricao);
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"AREATRABALHO_ATIVO", A72AreaTrabalho_Ativo);
         GxWebStd.gx_hidden_field( context, sPrefix+"AREATRABALHO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A5AreaTrabalho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATADAUSUARIO_AREATRABALHODES", A1297ContratadaUsuario_AreaTrabalhoDes);
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATADAUSUARIO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATADAUSUARIO_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1228ContratadaUsuario_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV67Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vTFCONTRATOSRVVNC_DOSTATUSDMN_SELS", AV28TFContratoSrvVnc_DoStatusDmn_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vTFCONTRATOSRVVNC_DOSTATUSDMN_SELS", AV28TFContratoSrvVnc_DoStatusDmn_Sels);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vTFCONTRATOSRVVNC_STATUSDMN_SELS", AV32TFContratoSrvVnc_StatusDmn_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vTFCONTRATOSRVVNC_STATUSDMN_SELS", AV32TFContratoSrvVnc_StatusDmn_Sels);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vTFCONTRATOSRVVNC_SRVVNCSTATUS_SELS", AV40TFContratoSrvVnc_SrvVncStatus_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vTFCONTRATOSRVVNC_SRVVNCSTATUS_SELS", AV40TFContratoSrvVnc_SrvVncStatus_Sels);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATO_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A75Contrato_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSRVVNC_SERVICOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A921ContratoSrvVnc_ServicoCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSRVVNC_PRESTADORAPESNOM", StringUtil.RTrim( A1092ContratoSrvVnc_PrestadoraPesNom));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Caption", StringUtil.RTrim( Ddo_contratoservicosvnc_descricao_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_contratoservicosvnc_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Cls", StringUtil.RTrim( Ddo_contratoservicosvnc_descricao_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicosvnc_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoservicosvnc_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosvnc_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosvnc_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosvnc_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosvnc_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosvnc_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosvnc_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_contratoservicosvnc_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicosvnc_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosvnc_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_contratoservicosvnc_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_contratoservicosvnc_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratoservicosvnc_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_contratoservicosvnc_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosvnc_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_contratoservicosvnc_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosvnc_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_contratoservicosvnc_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosvnc_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_DOSTATUSDMN_Caption", StringUtil.RTrim( Ddo_contratosrvvnc_dostatusdmn_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_DOSTATUSDMN_Tooltip", StringUtil.RTrim( Ddo_contratosrvvnc_dostatusdmn_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_DOSTATUSDMN_Cls", StringUtil.RTrim( Ddo_contratosrvvnc_dostatusdmn_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_DOSTATUSDMN_Selectedvalue_set", StringUtil.RTrim( Ddo_contratosrvvnc_dostatusdmn_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_DOSTATUSDMN_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratosrvvnc_dostatusdmn_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_DOSTATUSDMN_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratosrvvnc_dostatusdmn_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_DOSTATUSDMN_Includesortasc", StringUtil.BoolToStr( Ddo_contratosrvvnc_dostatusdmn_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_DOSTATUSDMN_Includesortdsc", StringUtil.BoolToStr( Ddo_contratosrvvnc_dostatusdmn_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_DOSTATUSDMN_Sortedstatus", StringUtil.RTrim( Ddo_contratosrvvnc_dostatusdmn_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_DOSTATUSDMN_Includefilter", StringUtil.BoolToStr( Ddo_contratosrvvnc_dostatusdmn_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_DOSTATUSDMN_Includedatalist", StringUtil.BoolToStr( Ddo_contratosrvvnc_dostatusdmn_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_DOSTATUSDMN_Datalisttype", StringUtil.RTrim( Ddo_contratosrvvnc_dostatusdmn_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_DOSTATUSDMN_Allowmultipleselection", StringUtil.BoolToStr( Ddo_contratosrvvnc_dostatusdmn_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_DOSTATUSDMN_Datalistfixedvalues", StringUtil.RTrim( Ddo_contratosrvvnc_dostatusdmn_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_DOSTATUSDMN_Sortasc", StringUtil.RTrim( Ddo_contratosrvvnc_dostatusdmn_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_DOSTATUSDMN_Sortdsc", StringUtil.RTrim( Ddo_contratosrvvnc_dostatusdmn_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_DOSTATUSDMN_Cleanfilter", StringUtil.RTrim( Ddo_contratosrvvnc_dostatusdmn_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_DOSTATUSDMN_Searchbuttontext", StringUtil.RTrim( Ddo_contratosrvvnc_dostatusdmn_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_STATUSDMN_Caption", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_STATUSDMN_Tooltip", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_STATUSDMN_Cls", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_STATUSDMN_Selectedvalue_set", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_STATUSDMN_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_STATUSDMN_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_STATUSDMN_Includesortasc", StringUtil.BoolToStr( Ddo_contratosrvvnc_statusdmn_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_STATUSDMN_Includesortdsc", StringUtil.BoolToStr( Ddo_contratosrvvnc_statusdmn_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_STATUSDMN_Sortedstatus", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_STATUSDMN_Includefilter", StringUtil.BoolToStr( Ddo_contratosrvvnc_statusdmn_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_STATUSDMN_Includedatalist", StringUtil.BoolToStr( Ddo_contratosrvvnc_statusdmn_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_STATUSDMN_Datalisttype", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_STATUSDMN_Allowmultipleselection", StringUtil.BoolToStr( Ddo_contratosrvvnc_statusdmn_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_STATUSDMN_Datalistfixedvalues", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_STATUSDMN_Sortasc", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_STATUSDMN_Sortdsc", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_STATUSDMN_Cleanfilter", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_STATUSDMN_Searchbuttontext", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Caption", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Tooltip", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Cls", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Filteredtext_set", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Selectedvalue_set", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Includesortasc", StringUtil.BoolToStr( Ddo_contratosrvvnc_servicovncsigla_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Includesortdsc", StringUtil.BoolToStr( Ddo_contratosrvvnc_servicovncsigla_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Sortedstatus", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Includefilter", StringUtil.BoolToStr( Ddo_contratosrvvnc_servicovncsigla_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Filtertype", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Filterisrange", StringUtil.BoolToStr( Ddo_contratosrvvnc_servicovncsigla_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Includedatalist", StringUtil.BoolToStr( Ddo_contratosrvvnc_servicovncsigla_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Datalisttype", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Datalistproc", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratosrvvnc_servicovncsigla_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Sortasc", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Sortdsc", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Loadingdata", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Cleanfilter", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Noresultsfound", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Searchbuttontext", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCSTATUS_Caption", StringUtil.RTrim( Ddo_contratosrvvnc_srvvncstatus_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCSTATUS_Tooltip", StringUtil.RTrim( Ddo_contratosrvvnc_srvvncstatus_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCSTATUS_Cls", StringUtil.RTrim( Ddo_contratosrvvnc_srvvncstatus_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCSTATUS_Selectedvalue_set", StringUtil.RTrim( Ddo_contratosrvvnc_srvvncstatus_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCSTATUS_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratosrvvnc_srvvncstatus_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCSTATUS_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratosrvvnc_srvvncstatus_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCSTATUS_Includesortasc", StringUtil.BoolToStr( Ddo_contratosrvvnc_srvvncstatus_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCSTATUS_Includesortdsc", StringUtil.BoolToStr( Ddo_contratosrvvnc_srvvncstatus_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCSTATUS_Sortedstatus", StringUtil.RTrim( Ddo_contratosrvvnc_srvvncstatus_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCSTATUS_Includefilter", StringUtil.BoolToStr( Ddo_contratosrvvnc_srvvncstatus_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCSTATUS_Includedatalist", StringUtil.BoolToStr( Ddo_contratosrvvnc_srvvncstatus_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCSTATUS_Datalisttype", StringUtil.RTrim( Ddo_contratosrvvnc_srvvncstatus_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCSTATUS_Allowmultipleselection", StringUtil.BoolToStr( Ddo_contratosrvvnc_srvvncstatus_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCSTATUS_Datalistfixedvalues", StringUtil.RTrim( Ddo_contratosrvvnc_srvvncstatus_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCSTATUS_Sortasc", StringUtil.RTrim( Ddo_contratosrvvnc_srvvncstatus_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCSTATUS_Sortdsc", StringUtil.RTrim( Ddo_contratosrvvnc_srvvncstatus_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCSTATUS_Cleanfilter", StringUtil.RTrim( Ddo_contratosrvvnc_srvvncstatus_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCSTATUS_Searchbuttontext", StringUtil.RTrim( Ddo_contratosrvvnc_srvvncstatus_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Caption", StringUtil.RTrim( Ddo_contratosrvvnc_srvvnccntnum_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Tooltip", StringUtil.RTrim( Ddo_contratosrvvnc_srvvnccntnum_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Cls", StringUtil.RTrim( Ddo_contratosrvvnc_srvvnccntnum_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Filteredtext_set", StringUtil.RTrim( Ddo_contratosrvvnc_srvvnccntnum_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Selectedvalue_set", StringUtil.RTrim( Ddo_contratosrvvnc_srvvnccntnum_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratosrvvnc_srvvnccntnum_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratosrvvnc_srvvnccntnum_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Includesortasc", StringUtil.BoolToStr( Ddo_contratosrvvnc_srvvnccntnum_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Includesortdsc", StringUtil.BoolToStr( Ddo_contratosrvvnc_srvvnccntnum_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Sortedstatus", StringUtil.RTrim( Ddo_contratosrvvnc_srvvnccntnum_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Includefilter", StringUtil.BoolToStr( Ddo_contratosrvvnc_srvvnccntnum_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Filtertype", StringUtil.RTrim( Ddo_contratosrvvnc_srvvnccntnum_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Filterisrange", StringUtil.BoolToStr( Ddo_contratosrvvnc_srvvnccntnum_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Includedatalist", StringUtil.BoolToStr( Ddo_contratosrvvnc_srvvnccntnum_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Datalisttype", StringUtil.RTrim( Ddo_contratosrvvnc_srvvnccntnum_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Datalistproc", StringUtil.RTrim( Ddo_contratosrvvnc_srvvnccntnum_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratosrvvnc_srvvnccntnum_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Sortasc", StringUtil.RTrim( Ddo_contratosrvvnc_srvvnccntnum_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Sortdsc", StringUtil.RTrim( Ddo_contratosrvvnc_srvvnccntnum_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Loadingdata", StringUtil.RTrim( Ddo_contratosrvvnc_srvvnccntnum_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Cleanfilter", StringUtil.RTrim( Ddo_contratosrvvnc_srvvnccntnum_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Noresultsfound", StringUtil.RTrim( Ddo_contratosrvvnc_srvvnccntnum_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Searchbuttontext", StringUtil.RTrim( Ddo_contratosrvvnc_srvvnccntnum_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SEMCUSTO_Caption", StringUtil.RTrim( Ddo_contratosrvvnc_semcusto_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SEMCUSTO_Tooltip", StringUtil.RTrim( Ddo_contratosrvvnc_semcusto_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SEMCUSTO_Cls", StringUtil.RTrim( Ddo_contratosrvvnc_semcusto_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SEMCUSTO_Selectedvalue_set", StringUtil.RTrim( Ddo_contratosrvvnc_semcusto_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SEMCUSTO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratosrvvnc_semcusto_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SEMCUSTO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratosrvvnc_semcusto_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SEMCUSTO_Includesortasc", StringUtil.BoolToStr( Ddo_contratosrvvnc_semcusto_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SEMCUSTO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratosrvvnc_semcusto_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SEMCUSTO_Sortedstatus", StringUtil.RTrim( Ddo_contratosrvvnc_semcusto_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SEMCUSTO_Includefilter", StringUtil.BoolToStr( Ddo_contratosrvvnc_semcusto_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SEMCUSTO_Includedatalist", StringUtil.BoolToStr( Ddo_contratosrvvnc_semcusto_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SEMCUSTO_Datalisttype", StringUtil.RTrim( Ddo_contratosrvvnc_semcusto_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SEMCUSTO_Datalistfixedvalues", StringUtil.RTrim( Ddo_contratosrvvnc_semcusto_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SEMCUSTO_Sortasc", StringUtil.RTrim( Ddo_contratosrvvnc_semcusto_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SEMCUSTO_Sortdsc", StringUtil.RTrim( Ddo_contratosrvvnc_semcusto_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SEMCUSTO_Cleanfilter", StringUtil.RTrim( Ddo_contratosrvvnc_semcusto_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SEMCUSTO_Searchbuttontext", StringUtil.RTrim( Ddo_contratosrvvnc_semcusto_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_ATIVO_Caption", StringUtil.RTrim( Ddo_contratoservicosvnc_ativo_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_ATIVO_Tooltip", StringUtil.RTrim( Ddo_contratoservicosvnc_ativo_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_ATIVO_Cls", StringUtil.RTrim( Ddo_contratoservicosvnc_ativo_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_ATIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoservicosvnc_ativo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_ATIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosvnc_ativo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_ATIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosvnc_ativo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_ATIVO_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosvnc_ativo_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_ATIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosvnc_ativo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_ATIVO_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosvnc_ativo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_ATIVO_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosvnc_ativo_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_ATIVO_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosvnc_ativo_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_ATIVO_Datalisttype", StringUtil.RTrim( Ddo_contratoservicosvnc_ativo_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_ATIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_contratoservicosvnc_ativo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_ATIVO_Sortasc", StringUtil.RTrim( Ddo_contratoservicosvnc_ativo_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_ATIVO_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosvnc_ativo_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_ATIVO_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosvnc_ativo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_ATIVO_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosvnc_ativo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"vWWPCONTEXT_Userid", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV6WWPContext.gxTpr_Userid), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosvnc_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicosvnc_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoservicosvnc_descricao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_DOSTATUSDMN_Activeeventkey", StringUtil.RTrim( Ddo_contratosrvvnc_dostatusdmn_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_DOSTATUSDMN_Selectedvalue_get", StringUtil.RTrim( Ddo_contratosrvvnc_dostatusdmn_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_STATUSDMN_Activeeventkey", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_STATUSDMN_Selectedvalue_get", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Activeeventkey", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Filteredtext_get", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Selectedvalue_get", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCSTATUS_Activeeventkey", StringUtil.RTrim( Ddo_contratosrvvnc_srvvncstatus_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCSTATUS_Selectedvalue_get", StringUtil.RTrim( Ddo_contratosrvvnc_srvvncstatus_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Activeeventkey", StringUtil.RTrim( Ddo_contratosrvvnc_srvvnccntnum_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Filteredtext_get", StringUtil.RTrim( Ddo_contratosrvvnc_srvvnccntnum_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Selectedvalue_get", StringUtil.RTrim( Ddo_contratosrvvnc_srvvnccntnum_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SEMCUSTO_Activeeventkey", StringUtil.RTrim( Ddo_contratosrvvnc_semcusto_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSRVVNC_SEMCUSTO_Selectedvalue_get", StringUtil.RTrim( Ddo_contratosrvvnc_semcusto_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_ATIVO_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosvnc_ativo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSVNC_ATIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoservicosvnc_ativo_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormGA2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contratoservicos_vncwc.js", "?20205299304591");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContratoServicos_VncWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contrato Servicos_Vnc WC" ;
      }

      protected void WBGA0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contratoservicos_vncwc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_GA2( true) ;
         }
         else
         {
            wb_table1_2_GA2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_GA2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrderedby_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV13OrderedBy), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,61);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrderedby_Jsonclick, 0, "Attribute", "", "", "", edtavOrderedby_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicos_VncWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,62);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_ContratoServicos_VncWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosvnc_descricao_Internalname, AV58TFContratoServicosVnc_Descricao, StringUtil.RTrim( context.localUtil.Format( AV58TFContratoServicosVnc_Descricao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,63);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosvnc_descricao_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosvnc_descricao_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoServicos_VncWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosvnc_descricao_sel_Internalname, AV59TFContratoServicosVnc_Descricao_Sel, StringUtil.RTrim( context.localUtil.Format( AV59TFContratoServicosVnc_Descricao_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,64);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosvnc_descricao_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosvnc_descricao_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoServicos_VncWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratosrvvnc_servicovncsigla_Internalname, StringUtil.RTrim( AV35TFContratoSrvVnc_ServicoVncSigla), StringUtil.RTrim( context.localUtil.Format( AV35TFContratoSrvVnc_ServicoVncSigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,65);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratosrvvnc_servicovncsigla_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratosrvvnc_servicovncsigla_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoServicos_VncWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratosrvvnc_servicovncsigla_sel_Internalname, StringUtil.RTrim( AV36TFContratoSrvVnc_ServicoVncSigla_Sel), StringUtil.RTrim( context.localUtil.Format( AV36TFContratoSrvVnc_ServicoVncSigla_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,66);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratosrvvnc_servicovncsigla_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratosrvvnc_servicovncsigla_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoServicos_VncWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratosrvvnc_srvvnccntnum_Internalname, StringUtil.RTrim( AV43TFContratoSrvVnc_SrvVncCntNum), StringUtil.RTrim( context.localUtil.Format( AV43TFContratoSrvVnc_SrvVncCntNum, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,67);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratosrvvnc_srvvnccntnum_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratosrvvnc_srvvnccntnum_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoServicos_VncWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratosrvvnc_srvvnccntnum_sel_Internalname, StringUtil.RTrim( AV44TFContratoSrvVnc_SrvVncCntNum_Sel), StringUtil.RTrim( context.localUtil.Format( AV44TFContratoSrvVnc_SrvVncCntNum_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,68);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratosrvvnc_srvvnccntnum_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratosrvvnc_srvvnccntnum_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoServicos_VncWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratosrvvnc_semcusto_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV47TFContratoSrvVnc_SemCusto_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV47TFContratoSrvVnc_SemCusto_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,69);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratosrvvnc_semcusto_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratosrvvnc_semcusto_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicos_VncWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosvnc_ativo_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV50TFContratoServicosVnc_Ativo_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV50TFContratoServicosVnc_Ativo_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,70);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosvnc_ativo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosvnc_ativo_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicos_VncWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosvnc_descricaotitlecontrolidtoreplace_Internalname, AV60ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,72);\"", 0, edtavDdo_contratoservicosvnc_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoServicos_VncWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOSRVVNC_DOSTATUSDMNContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratosrvvnc_dostatusdmntitlecontrolidtoreplace_Internalname, AV29ddo_ContratoSrvVnc_DoStatusDmnTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", 0, edtavDdo_contratosrvvnc_dostatusdmntitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoServicos_VncWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOSRVVNC_STATUSDMNContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratosrvvnc_statusdmntitlecontrolidtoreplace_Internalname, AV33ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,76);\"", 0, edtavDdo_contratosrvvnc_statusdmntitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoServicos_VncWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratosrvvnc_servicovncsiglatitlecontrolidtoreplace_Internalname, AV37ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,78);\"", 0, edtavDdo_contratosrvvnc_servicovncsiglatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoServicos_VncWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCSTATUSContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratosrvvnc_srvvncstatustitlecontrolidtoreplace_Internalname, AV41ddo_ContratoSrvVnc_SrvVncStatusTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,80);\"", 0, edtavDdo_contratosrvvnc_srvvncstatustitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoServicos_VncWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratosrvvnc_srvvnccntnumtitlecontrolidtoreplace_Internalname, AV45ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,82);\"", 0, edtavDdo_contratosrvvnc_srvvnccntnumtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoServicos_VncWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOSRVVNC_SEMCUSTOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratosrvvnc_semcustotitlecontrolidtoreplace_Internalname, AV48ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,84);\"", 0, edtavDdo_contratosrvvnc_semcustotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoServicos_VncWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOSERVICOSVNC_ATIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosvnc_ativotitlecontrolidtoreplace_Internalname, AV51ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,86);\"", 0, edtavDdo_contratoservicosvnc_ativotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoServicos_VncWC.htm");
         }
         wbLoad = true;
      }

      protected void STARTGA2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contrato Servicos_Vnc WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPGA0( ) ;
            }
         }
      }

      protected void WSGA2( )
      {
         STARTGA2( ) ;
         EVTGA2( ) ;
      }

      protected void EVTGA2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGA0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGA0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11GA2 */
                                    E11GA2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSVNC_DESCRICAO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGA0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12GA2 */
                                    E12GA2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSRVVNC_DOSTATUSDMN.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGA0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13GA2 */
                                    E13GA2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSRVVNC_STATUSDMN.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGA0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14GA2 */
                                    E14GA2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGA0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15GA2 */
                                    E15GA2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSRVVNC_SRVVNCSTATUS.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGA0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E16GA2 */
                                    E16GA2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGA0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E17GA2 */
                                    E17GA2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSRVVNC_SEMCUSTO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGA0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E18GA2 */
                                    E18GA2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSVNC_ATIVO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGA0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E19GA2 */
                                    E19GA2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGA0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E20GA2 */
                                    E20GA2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLONAR'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGA0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E21GA2 */
                                    E21GA2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VAREATRABALHO_CODIGO.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGA0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E22GA2 */
                                    E22GA2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VSERVICO_CODIGO.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGA0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E23GA2 */
                                    E23GA2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGA0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = cmbavAreatrabalho_codigo_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGA0( ) ;
                              }
                              nGXsfl_8_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
                              SubsflControlProps_82( ) ;
                              AV15Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV15Update)) ? AV65Update_GXI : context.convertURL( context.PathToRelativeUrl( AV15Update))));
                              AV16Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV16Delete)) ? AV66Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV16Delete))));
                              A917ContratoSrvVnc_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoSrvVnc_Codigo_Internalname), ",", "."));
                              A933ContratoSrvVnc_ContratoCod = (int)(context.localUtil.CToN( cgiGet( edtContratoSrvVnc_ContratoCod_Internalname), ",", "."));
                              n933ContratoSrvVnc_ContratoCod = false;
                              A915ContratoSrvVnc_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( edtContratoSrvVnc_CntSrvCod_Internalname), ",", "."));
                              A923ContratoSrvVnc_ServicoVncCod = (int)(context.localUtil.CToN( cgiGet( edtContratoSrvVnc_ServicoVncCod_Internalname), ",", "."));
                              n923ContratoSrvVnc_ServicoVncCod = false;
                              A2108ContratoServicosVnc_Descricao = StringUtil.Upper( cgiGet( edtContratoServicosVnc_Descricao_Internalname));
                              n2108ContratoServicosVnc_Descricao = false;
                              cmbContratoSrvVnc_DoStatusDmn.Name = cmbContratoSrvVnc_DoStatusDmn_Internalname;
                              cmbContratoSrvVnc_DoStatusDmn.CurrentValue = cgiGet( cmbContratoSrvVnc_DoStatusDmn_Internalname);
                              A1800ContratoSrvVnc_DoStatusDmn = cgiGet( cmbContratoSrvVnc_DoStatusDmn_Internalname);
                              n1800ContratoSrvVnc_DoStatusDmn = false;
                              cmbContratoSrvVnc_StatusDmn.Name = cmbContratoSrvVnc_StatusDmn_Internalname;
                              cmbContratoSrvVnc_StatusDmn.CurrentValue = cgiGet( cmbContratoSrvVnc_StatusDmn_Internalname);
                              A1084ContratoSrvVnc_StatusDmn = cgiGet( cmbContratoSrvVnc_StatusDmn_Internalname);
                              n1084ContratoSrvVnc_StatusDmn = false;
                              A924ContratoSrvVnc_ServicoVncSigla = StringUtil.Upper( cgiGet( edtContratoSrvVnc_ServicoVncSigla_Internalname));
                              n924ContratoSrvVnc_ServicoVncSigla = false;
                              AV25Prestador = StringUtil.Upper( cgiGet( edtavPrestador_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavPrestador_Internalname, AV25Prestador);
                              cmbContratoSrvVnc_SrvVncStatus.Name = cmbContratoSrvVnc_SrvVncStatus_Internalname;
                              cmbContratoSrvVnc_SrvVncStatus.CurrentValue = cgiGet( cmbContratoSrvVnc_SrvVncStatus_Internalname);
                              A1663ContratoSrvVnc_SrvVncStatus = cgiGet( cmbContratoSrvVnc_SrvVncStatus_Internalname);
                              n1663ContratoSrvVnc_SrvVncStatus = false;
                              A1631ContratoSrvVnc_SrvVncCntNum = cgiGet( edtContratoSrvVnc_SrvVncCntNum_Internalname);
                              n1631ContratoSrvVnc_SrvVncCntNum = false;
                              cmbContratoSrvVnc_SemCusto.Name = cmbContratoSrvVnc_SemCusto_Internalname;
                              cmbContratoSrvVnc_SemCusto.CurrentValue = cgiGet( cmbContratoSrvVnc_SemCusto_Internalname);
                              A1090ContratoSrvVnc_SemCusto = StringUtil.StrToBool( cgiGet( cmbContratoSrvVnc_SemCusto_Internalname));
                              n1090ContratoSrvVnc_SemCusto = false;
                              A1453ContratoServicosVnc_Ativo = StringUtil.StrToBool( cgiGet( chkContratoServicosVnc_Ativo_Internalname));
                              n1453ContratoServicosVnc_Ativo = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavAreatrabalho_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E24GA2 */
                                          E24GA2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavAreatrabalho_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E25GA2 */
                                          E25GA2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavAreatrabalho_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E26GA2 */
                                          E26GA2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicosvnc_descricao Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSVNC_DESCRICAO"), AV58TFContratoServicosVnc_Descricao) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicosvnc_descricao_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSVNC_DESCRICAO_SEL"), AV59TFContratoServicosVnc_Descricao_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratosrvvnc_servicovncsigla Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOSRVVNC_SERVICOVNCSIGLA"), AV35TFContratoSrvVnc_ServicoVncSigla) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratosrvvnc_servicovncsigla_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL"), AV36TFContratoSrvVnc_ServicoVncSigla_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratosrvvnc_srvvnccntnum Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOSRVVNC_SRVVNCCNTNUM"), AV43TFContratoSrvVnc_SrvVncCntNum) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratosrvvnc_srvvnccntnum_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOSRVVNC_SRVVNCCNTNUM_SEL"), AV44TFContratoSrvVnc_SrvVncCntNum_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratosrvvnc_semcusto_sel Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSRVVNC_SEMCUSTO_SEL"), ",", ".") != Convert.ToDecimal( AV47TFContratoSrvVnc_SemCusto_Sel )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicosvnc_ativo_sel Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSVNC_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV50TFContratoServicosVnc_Ativo_Sel )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavAreatrabalho_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPGA0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavAreatrabalho_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEGA2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormGA2( ) ;
            }
         }
      }

      protected void PAGA2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            GXCCtl = "CONTRATOSRVVNC_DOSTATUSDMN_" + sGXsfl_8_idx;
            cmbContratoSrvVnc_DoStatusDmn.Name = GXCCtl;
            cmbContratoSrvVnc_DoStatusDmn.WebTags = "";
            cmbContratoSrvVnc_DoStatusDmn.addItem("", "(Qualquer um)", 0);
            cmbContratoSrvVnc_DoStatusDmn.addItem("B", "Stand by", 0);
            cmbContratoSrvVnc_DoStatusDmn.addItem("S", "Solicitada", 0);
            cmbContratoSrvVnc_DoStatusDmn.addItem("E", "Em An�lise", 0);
            cmbContratoSrvVnc_DoStatusDmn.addItem("A", "Em execu��o", 0);
            cmbContratoSrvVnc_DoStatusDmn.addItem("R", "Resolvida", 0);
            cmbContratoSrvVnc_DoStatusDmn.addItem("C", "Conferida", 0);
            cmbContratoSrvVnc_DoStatusDmn.addItem("D", "Retornada", 0);
            cmbContratoSrvVnc_DoStatusDmn.addItem("H", "Homologada", 0);
            cmbContratoSrvVnc_DoStatusDmn.addItem("O", "Aceite", 0);
            cmbContratoSrvVnc_DoStatusDmn.addItem("P", "A Pagar", 0);
            cmbContratoSrvVnc_DoStatusDmn.addItem("L", "Liquidada", 0);
            cmbContratoSrvVnc_DoStatusDmn.addItem("X", "Cancelada", 0);
            cmbContratoSrvVnc_DoStatusDmn.addItem("N", "N�o Faturada", 0);
            cmbContratoSrvVnc_DoStatusDmn.addItem("J", "Planejamento", 0);
            cmbContratoSrvVnc_DoStatusDmn.addItem("I", "An�lise Planejamento", 0);
            cmbContratoSrvVnc_DoStatusDmn.addItem("T", "Validacao T�cnica", 0);
            cmbContratoSrvVnc_DoStatusDmn.addItem("Q", "Validacao Qualidade", 0);
            cmbContratoSrvVnc_DoStatusDmn.addItem("G", "Em Homologa��o", 0);
            cmbContratoSrvVnc_DoStatusDmn.addItem("M", "Valida��o Mensura��o", 0);
            cmbContratoSrvVnc_DoStatusDmn.addItem("U", "Rascunho", 0);
            if ( cmbContratoSrvVnc_DoStatusDmn.ItemCount > 0 )
            {
               A1800ContratoSrvVnc_DoStatusDmn = cmbContratoSrvVnc_DoStatusDmn.getValidValue(A1800ContratoSrvVnc_DoStatusDmn);
               n1800ContratoSrvVnc_DoStatusDmn = false;
            }
            GXCCtl = "CONTRATOSRVVNC_STATUSDMN_" + sGXsfl_8_idx;
            cmbContratoSrvVnc_StatusDmn.Name = GXCCtl;
            cmbContratoSrvVnc_StatusDmn.WebTags = "";
            cmbContratoSrvVnc_StatusDmn.addItem("B", "Stand by", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("S", "Solicitada", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("E", "Em An�lise", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("A", "Em execu��o", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("R", "Resolvida", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("C", "Conferida", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("D", "Retornada", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("H", "Homologada", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("O", "Aceite", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("P", "A Pagar", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("L", "Liquidada", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("X", "Cancelada", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("N", "N�o Faturada", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("J", "Planejamento", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("I", "An�lise Planejamento", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("T", "Validacao T�cnica", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("Q", "Validacao Qualidade", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("G", "Em Homologa��o", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("M", "Valida��o Mensura��o", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("U", "Rascunho", 0);
            if ( cmbContratoSrvVnc_StatusDmn.ItemCount > 0 )
            {
               A1084ContratoSrvVnc_StatusDmn = cmbContratoSrvVnc_StatusDmn.getValidValue(A1084ContratoSrvVnc_StatusDmn);
               n1084ContratoSrvVnc_StatusDmn = false;
            }
            GXCCtl = "CONTRATOSRVVNC_SRVVNCSTATUS_" + sGXsfl_8_idx;
            cmbContratoSrvVnc_SrvVncStatus.Name = GXCCtl;
            cmbContratoSrvVnc_SrvVncStatus.WebTags = "";
            cmbContratoSrvVnc_SrvVncStatus.addItem("B", "Stand by", 0);
            cmbContratoSrvVnc_SrvVncStatus.addItem("S", "Solicitada", 0);
            cmbContratoSrvVnc_SrvVncStatus.addItem("E", "Em An�lise", 0);
            cmbContratoSrvVnc_SrvVncStatus.addItem("A", "Em execu��o", 0);
            cmbContratoSrvVnc_SrvVncStatus.addItem("R", "Resolvida", 0);
            cmbContratoSrvVnc_SrvVncStatus.addItem("C", "Conferida", 0);
            cmbContratoSrvVnc_SrvVncStatus.addItem("D", "Retornada", 0);
            cmbContratoSrvVnc_SrvVncStatus.addItem("H", "Homologada", 0);
            cmbContratoSrvVnc_SrvVncStatus.addItem("O", "Aceite", 0);
            cmbContratoSrvVnc_SrvVncStatus.addItem("P", "A Pagar", 0);
            cmbContratoSrvVnc_SrvVncStatus.addItem("L", "Liquidada", 0);
            cmbContratoSrvVnc_SrvVncStatus.addItem("X", "Cancelada", 0);
            cmbContratoSrvVnc_SrvVncStatus.addItem("N", "N�o Faturada", 0);
            cmbContratoSrvVnc_SrvVncStatus.addItem("J", "Planejamento", 0);
            cmbContratoSrvVnc_SrvVncStatus.addItem("I", "An�lise Planejamento", 0);
            cmbContratoSrvVnc_SrvVncStatus.addItem("T", "Validacao T�cnica", 0);
            cmbContratoSrvVnc_SrvVncStatus.addItem("Q", "Validacao Qualidade", 0);
            cmbContratoSrvVnc_SrvVncStatus.addItem("G", "Em Homologa��o", 0);
            cmbContratoSrvVnc_SrvVncStatus.addItem("M", "Valida��o Mensura��o", 0);
            cmbContratoSrvVnc_SrvVncStatus.addItem("U", "Rascunho", 0);
            if ( cmbContratoSrvVnc_SrvVncStatus.ItemCount > 0 )
            {
               A1663ContratoSrvVnc_SrvVncStatus = cmbContratoSrvVnc_SrvVncStatus.getValidValue(A1663ContratoSrvVnc_SrvVncStatus);
               n1663ContratoSrvVnc_SrvVncStatus = false;
            }
            GXCCtl = "CONTRATOSRVVNC_SEMCUSTO_" + sGXsfl_8_idx;
            cmbContratoSrvVnc_SemCusto.Name = GXCCtl;
            cmbContratoSrvVnc_SemCusto.WebTags = "";
            cmbContratoSrvVnc_SemCusto.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbContratoSrvVnc_SemCusto.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbContratoSrvVnc_SemCusto.ItemCount > 0 )
            {
               A1090ContratoSrvVnc_SemCusto = StringUtil.StrToBool( cmbContratoSrvVnc_SemCusto.getValidValue(StringUtil.BoolToStr( A1090ContratoSrvVnc_SemCusto)));
               n1090ContratoSrvVnc_SemCusto = false;
            }
            GXCCtl = "CONTRATOSERVICOSVNC_ATIVO_" + sGXsfl_8_idx;
            chkContratoServicosVnc_Ativo.Name = GXCCtl;
            chkContratoServicosVnc_Ativo.WebTags = "";
            chkContratoServicosVnc_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkContratoServicosVnc_Ativo_Internalname, "TitleCaption", chkContratoServicosVnc_Ativo.Caption);
            chkContratoServicosVnc_Ativo.CheckedValue = "false";
            cmbavAreatrabalho_codigo.Name = "vAREATRABALHO_CODIGO";
            cmbavAreatrabalho_codigo.WebTags = "";
            cmbavAreatrabalho_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "(Todas)", 0);
            if ( cmbavAreatrabalho_codigo.ItemCount > 0 )
            {
               AV19AreaTrabalho_Codigo = (int)(NumberUtil.Val( cmbavAreatrabalho_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV19AreaTrabalho_Codigo), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19AreaTrabalho_Codigo), 6, 0)));
            }
            dynavServico_codigo.Name = "vSERVICO_CODIGO";
            dynavServico_codigo.WebTags = "";
            cmbavRegra.Name = "vREGRA";
            cmbavRegra.WebTags = "";
            cmbavRegra.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "(Nenhum)", 0);
            if ( cmbavRegra.ItemCount > 0 )
            {
               AV21Regra = (int)(NumberUtil.Val( cmbavRegra.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21Regra), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21Regra", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Regra), 6, 0)));
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavAreatrabalho_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         GXVvSERVICO_CODIGO_htmlGA2( AV19AreaTrabalho_Codigo) ;
         /* End function dynload_actions */
      }

      protected void GXDLVvSERVICO_CODIGOGA2( int AV19AreaTrabalho_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvSERVICO_CODIGO_dataGA2( AV19AreaTrabalho_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvSERVICO_CODIGO_htmlGA2( int AV19AreaTrabalho_Codigo )
      {
         int gxdynajaxvalue ;
         GXDLVvSERVICO_CODIGO_dataGA2( AV19AreaTrabalho_Codigo) ;
         gxdynajaxindex = 1;
         dynavServico_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavServico_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavServico_codigo.ItemCount > 0 )
         {
            AV20Servico_Codigo = (int)(NumberUtil.Val( dynavServico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20Servico_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Servico_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvSERVICO_CODIGO_dataGA2( int AV19AreaTrabalho_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Todos)");
         /* Using cursor H00GA2 */
         pr_default.execute(0, new Object[] {AV19AreaTrabalho_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00GA2_A827ContratoServicos_ServicoCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00GA2_A826ContratoServicos_ServicoSigla[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_82( ) ;
         while ( nGXsfl_8_idx <= nRC_GXsfl_8 )
         {
            sendrow_82( ) ;
            nGXsfl_8_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_8_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_8_idx+1));
            sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
            SubsflControlProps_82( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV58TFContratoServicosVnc_Descricao ,
                                       String AV59TFContratoServicosVnc_Descricao_Sel ,
                                       String AV35TFContratoSrvVnc_ServicoVncSigla ,
                                       String AV36TFContratoSrvVnc_ServicoVncSigla_Sel ,
                                       String AV43TFContratoSrvVnc_SrvVncCntNum ,
                                       String AV44TFContratoSrvVnc_SrvVncCntNum_Sel ,
                                       short AV47TFContratoSrvVnc_SemCusto_Sel ,
                                       short AV50TFContratoServicosVnc_Ativo_Sel ,
                                       int AV18ContratoSrvVnc_CntSrvCod ,
                                       String AV60ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace ,
                                       String AV29ddo_ContratoSrvVnc_DoStatusDmnTitleControlIdToReplace ,
                                       String AV33ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace ,
                                       String AV37ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace ,
                                       String AV41ddo_ContratoSrvVnc_SrvVncStatusTitleControlIdToReplace ,
                                       String AV45ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace ,
                                       String AV48ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace ,
                                       String AV51ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace ,
                                       String A6AreaTrabalho_Descricao ,
                                       bool A72AreaTrabalho_Ativo ,
                                       int A5AreaTrabalho_Codigo ,
                                       String A1297ContratadaUsuario_AreaTrabalhoDes ,
                                       int A69ContratadaUsuario_UsuarioCod ,
                                       int A1228ContratadaUsuario_AreaTrabalhoCod ,
                                       String AV67Pgmname ,
                                       IGxCollection AV28TFContratoSrvVnc_DoStatusDmn_Sels ,
                                       IGxCollection AV32TFContratoSrvVnc_StatusDmn_Sels ,
                                       IGxCollection AV40TFContratoSrvVnc_SrvVncStatus_Sels ,
                                       int A75Contrato_AreaTrabalhoCod ,
                                       int AV19AreaTrabalho_Codigo ,
                                       int A921ContratoSrvVnc_ServicoCod ,
                                       int AV20Servico_Codigo ,
                                       int A917ContratoSrvVnc_Codigo ,
                                       String A924ContratoSrvVnc_ServicoVncSigla ,
                                       String A1800ContratoSrvVnc_DoStatusDmn ,
                                       String A1084ContratoSrvVnc_StatusDmn ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       bool A1453ContratoServicosVnc_Ativo ,
                                       String A1092ContratoSrvVnc_PrestadoraPesNom ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFGA2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSRVVNC_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A917ContratoSrvVnc_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSRVVNC_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A917ContratoSrvVnc_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSRVVNC_CNTSRVCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A915ContratoSrvVnc_CntSrvCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSRVVNC_CNTSRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A915ContratoSrvVnc_CntSrvCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSVNC_DESCRICAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2108ContratoServicosVnc_Descricao, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOSVNC_DESCRICAO", A2108ContratoServicosVnc_Descricao);
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSRVVNC_DOSTATUSDMN", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1800ContratoSrvVnc_DoStatusDmn, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSRVVNC_DOSTATUSDMN", StringUtil.RTrim( A1800ContratoSrvVnc_DoStatusDmn));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSRVVNC_STATUSDMN", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1084ContratoSrvVnc_StatusDmn, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSRVVNC_STATUSDMN", StringUtil.RTrim( A1084ContratoSrvVnc_StatusDmn));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSRVVNC_SRVVNCSTATUS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1663ContratoSrvVnc_SrvVncStatus, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSRVVNC_SRVVNCSTATUS", StringUtil.RTrim( A1663ContratoSrvVnc_SrvVncStatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSRVVNC_SEMCUSTO", GetSecureSignedToken( sPrefix, A1090ContratoSrvVnc_SemCusto));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSRVVNC_SEMCUSTO", StringUtil.BoolToStr( A1090ContratoSrvVnc_SemCusto));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSVNC_ATIVO", GetSecureSignedToken( sPrefix, A1453ContratoServicosVnc_Ativo));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOSVNC_ATIVO", StringUtil.BoolToStr( A1453ContratoServicosVnc_Ativo));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavAreatrabalho_codigo.ItemCount > 0 )
         {
            AV19AreaTrabalho_Codigo = (int)(NumberUtil.Val( cmbavAreatrabalho_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV19AreaTrabalho_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19AreaTrabalho_Codigo), 6, 0)));
         }
         if ( dynavServico_codigo.ItemCount > 0 )
         {
            AV20Servico_Codigo = (int)(NumberUtil.Val( dynavServico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20Servico_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Servico_Codigo), 6, 0)));
         }
         if ( cmbavRegra.ItemCount > 0 )
         {
            AV21Regra = (int)(NumberUtil.Val( cmbavRegra.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21Regra), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21Regra", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Regra), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFGA2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV67Pgmname = "ContratoServicos_VncWC";
         context.Gx_err = 0;
         edtavPrestador_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPrestador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPrestador_Enabled), 5, 0)));
      }

      protected void RFGA2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 8;
         /* Execute user event: E25GA2 */
         E25GA2 ();
         nGXsfl_8_idx = 1;
         sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
         SubsflControlProps_82( ) ;
         nGXsfl_8_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_82( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(1, new Object[]{ new Object[]{
                                                 A1800ContratoSrvVnc_DoStatusDmn ,
                                                 AV28TFContratoSrvVnc_DoStatusDmn_Sels ,
                                                 A1084ContratoSrvVnc_StatusDmn ,
                                                 AV32TFContratoSrvVnc_StatusDmn_Sels ,
                                                 A1663ContratoSrvVnc_SrvVncStatus ,
                                                 AV40TFContratoSrvVnc_SrvVncStatus_Sels ,
                                                 AV59TFContratoServicosVnc_Descricao_Sel ,
                                                 AV58TFContratoServicosVnc_Descricao ,
                                                 AV28TFContratoSrvVnc_DoStatusDmn_Sels.Count ,
                                                 AV32TFContratoSrvVnc_StatusDmn_Sels.Count ,
                                                 AV36TFContratoSrvVnc_ServicoVncSigla_Sel ,
                                                 AV35TFContratoSrvVnc_ServicoVncSigla ,
                                                 AV40TFContratoSrvVnc_SrvVncStatus_Sels.Count ,
                                                 AV44TFContratoSrvVnc_SrvVncCntNum_Sel ,
                                                 AV43TFContratoSrvVnc_SrvVncCntNum ,
                                                 AV47TFContratoSrvVnc_SemCusto_Sel ,
                                                 AV50TFContratoServicosVnc_Ativo_Sel ,
                                                 A2108ContratoServicosVnc_Descricao ,
                                                 A924ContratoSrvVnc_ServicoVncSigla ,
                                                 A1631ContratoSrvVnc_SrvVncCntNum ,
                                                 A1090ContratoSrvVnc_SemCusto ,
                                                 A1453ContratoServicosVnc_Ativo ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A915ContratoSrvVnc_CntSrvCod ,
                                                 AV18ContratoSrvVnc_CntSrvCod },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                                 TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT,
                                                 TypeConstants.INT
                                                 }
            });
            lV58TFContratoServicosVnc_Descricao = StringUtil.Concat( StringUtil.RTrim( AV58TFContratoServicosVnc_Descricao), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV58TFContratoServicosVnc_Descricao", AV58TFContratoServicosVnc_Descricao);
            lV35TFContratoSrvVnc_ServicoVncSigla = StringUtil.PadR( StringUtil.RTrim( AV35TFContratoSrvVnc_ServicoVncSigla), 15, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35TFContratoSrvVnc_ServicoVncSigla", AV35TFContratoSrvVnc_ServicoVncSigla);
            lV43TFContratoSrvVnc_SrvVncCntNum = StringUtil.PadR( StringUtil.RTrim( AV43TFContratoSrvVnc_SrvVncCntNum), 20, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFContratoSrvVnc_SrvVncCntNum", AV43TFContratoSrvVnc_SrvVncCntNum);
            /* Using cursor H00GA5 */
            pr_default.execute(1, new Object[] {AV18ContratoSrvVnc_CntSrvCod, lV58TFContratoServicosVnc_Descricao, AV59TFContratoServicosVnc_Descricao_Sel, lV35TFContratoSrvVnc_ServicoVncSigla, AV36TFContratoSrvVnc_ServicoVncSigla_Sel, lV43TFContratoSrvVnc_SrvVncCntNum, AV44TFContratoSrvVnc_SrvVncCntNum_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_8_idx = 1;
            while ( ( (pr_default.getStatus(1) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1589ContratoSrvVnc_SrvVncCntSrvCod = H00GA5_A1589ContratoSrvVnc_SrvVncCntSrvCod[0];
               n1589ContratoSrvVnc_SrvVncCntSrvCod = H00GA5_n1589ContratoSrvVnc_SrvVncCntSrvCod[0];
               A1628ContratoSrvVnc_SrvVncCntCod = H00GA5_A1628ContratoSrvVnc_SrvVncCntCod[0];
               n1628ContratoSrvVnc_SrvVncCntCod = H00GA5_n1628ContratoSrvVnc_SrvVncCntCod[0];
               A1453ContratoServicosVnc_Ativo = H00GA5_A1453ContratoServicosVnc_Ativo[0];
               n1453ContratoServicosVnc_Ativo = H00GA5_n1453ContratoServicosVnc_Ativo[0];
               A1090ContratoSrvVnc_SemCusto = H00GA5_A1090ContratoSrvVnc_SemCusto[0];
               n1090ContratoSrvVnc_SemCusto = H00GA5_n1090ContratoSrvVnc_SemCusto[0];
               A1631ContratoSrvVnc_SrvVncCntNum = H00GA5_A1631ContratoSrvVnc_SrvVncCntNum[0];
               n1631ContratoSrvVnc_SrvVncCntNum = H00GA5_n1631ContratoSrvVnc_SrvVncCntNum[0];
               A1663ContratoSrvVnc_SrvVncStatus = H00GA5_A1663ContratoSrvVnc_SrvVncStatus[0];
               n1663ContratoSrvVnc_SrvVncStatus = H00GA5_n1663ContratoSrvVnc_SrvVncStatus[0];
               A924ContratoSrvVnc_ServicoVncSigla = H00GA5_A924ContratoSrvVnc_ServicoVncSigla[0];
               n924ContratoSrvVnc_ServicoVncSigla = H00GA5_n924ContratoSrvVnc_ServicoVncSigla[0];
               A1084ContratoSrvVnc_StatusDmn = H00GA5_A1084ContratoSrvVnc_StatusDmn[0];
               n1084ContratoSrvVnc_StatusDmn = H00GA5_n1084ContratoSrvVnc_StatusDmn[0];
               A1800ContratoSrvVnc_DoStatusDmn = H00GA5_A1800ContratoSrvVnc_DoStatusDmn[0];
               n1800ContratoSrvVnc_DoStatusDmn = H00GA5_n1800ContratoSrvVnc_DoStatusDmn[0];
               A2108ContratoServicosVnc_Descricao = H00GA5_A2108ContratoServicosVnc_Descricao[0];
               n2108ContratoServicosVnc_Descricao = H00GA5_n2108ContratoServicosVnc_Descricao[0];
               A923ContratoSrvVnc_ServicoVncCod = H00GA5_A923ContratoSrvVnc_ServicoVncCod[0];
               n923ContratoSrvVnc_ServicoVncCod = H00GA5_n923ContratoSrvVnc_ServicoVncCod[0];
               A915ContratoSrvVnc_CntSrvCod = H00GA5_A915ContratoSrvVnc_CntSrvCod[0];
               A933ContratoSrvVnc_ContratoCod = H00GA5_A933ContratoSrvVnc_ContratoCod[0];
               n933ContratoSrvVnc_ContratoCod = H00GA5_n933ContratoSrvVnc_ContratoCod[0];
               A917ContratoSrvVnc_Codigo = H00GA5_A917ContratoSrvVnc_Codigo[0];
               A1092ContratoSrvVnc_PrestadoraPesNom = H00GA5_A1092ContratoSrvVnc_PrestadoraPesNom[0];
               n1092ContratoSrvVnc_PrestadoraPesNom = H00GA5_n1092ContratoSrvVnc_PrestadoraPesNom[0];
               A1628ContratoSrvVnc_SrvVncCntCod = H00GA5_A1628ContratoSrvVnc_SrvVncCntCod[0];
               n1628ContratoSrvVnc_SrvVncCntCod = H00GA5_n1628ContratoSrvVnc_SrvVncCntCod[0];
               A923ContratoSrvVnc_ServicoVncCod = H00GA5_A923ContratoSrvVnc_ServicoVncCod[0];
               n923ContratoSrvVnc_ServicoVncCod = H00GA5_n923ContratoSrvVnc_ServicoVncCod[0];
               A1631ContratoSrvVnc_SrvVncCntNum = H00GA5_A1631ContratoSrvVnc_SrvVncCntNum[0];
               n1631ContratoSrvVnc_SrvVncCntNum = H00GA5_n1631ContratoSrvVnc_SrvVncCntNum[0];
               A924ContratoSrvVnc_ServicoVncSigla = H00GA5_A924ContratoSrvVnc_ServicoVncSigla[0];
               n924ContratoSrvVnc_ServicoVncSigla = H00GA5_n924ContratoSrvVnc_ServicoVncSigla[0];
               A933ContratoSrvVnc_ContratoCod = H00GA5_A933ContratoSrvVnc_ContratoCod[0];
               n933ContratoSrvVnc_ContratoCod = H00GA5_n933ContratoSrvVnc_ContratoCod[0];
               A1092ContratoSrvVnc_PrestadoraPesNom = H00GA5_A1092ContratoSrvVnc_PrestadoraPesNom[0];
               n1092ContratoSrvVnc_PrestadoraPesNom = H00GA5_n1092ContratoSrvVnc_PrestadoraPesNom[0];
               /* Execute user event: E26GA2 */
               E26GA2 ();
               pr_default.readNext(1);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(1) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(1);
            wbEnd = 8;
            WBGA0( ) ;
         }
         nGXsfl_8_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A1800ContratoSrvVnc_DoStatusDmn ,
                                              AV28TFContratoSrvVnc_DoStatusDmn_Sels ,
                                              A1084ContratoSrvVnc_StatusDmn ,
                                              AV32TFContratoSrvVnc_StatusDmn_Sels ,
                                              A1663ContratoSrvVnc_SrvVncStatus ,
                                              AV40TFContratoSrvVnc_SrvVncStatus_Sels ,
                                              AV59TFContratoServicosVnc_Descricao_Sel ,
                                              AV58TFContratoServicosVnc_Descricao ,
                                              AV28TFContratoSrvVnc_DoStatusDmn_Sels.Count ,
                                              AV32TFContratoSrvVnc_StatusDmn_Sels.Count ,
                                              AV36TFContratoSrvVnc_ServicoVncSigla_Sel ,
                                              AV35TFContratoSrvVnc_ServicoVncSigla ,
                                              AV40TFContratoSrvVnc_SrvVncStatus_Sels.Count ,
                                              AV44TFContratoSrvVnc_SrvVncCntNum_Sel ,
                                              AV43TFContratoSrvVnc_SrvVncCntNum ,
                                              AV47TFContratoSrvVnc_SemCusto_Sel ,
                                              AV50TFContratoServicosVnc_Ativo_Sel ,
                                              A2108ContratoServicosVnc_Descricao ,
                                              A924ContratoSrvVnc_ServicoVncSigla ,
                                              A1631ContratoSrvVnc_SrvVncCntNum ,
                                              A1090ContratoSrvVnc_SemCusto ,
                                              A1453ContratoServicosVnc_Ativo ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              A915ContratoSrvVnc_CntSrvCod ,
                                              AV18ContratoSrvVnc_CntSrvCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.INT
                                              }
         });
         lV58TFContratoServicosVnc_Descricao = StringUtil.Concat( StringUtil.RTrim( AV58TFContratoServicosVnc_Descricao), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV58TFContratoServicosVnc_Descricao", AV58TFContratoServicosVnc_Descricao);
         lV35TFContratoSrvVnc_ServicoVncSigla = StringUtil.PadR( StringUtil.RTrim( AV35TFContratoSrvVnc_ServicoVncSigla), 15, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35TFContratoSrvVnc_ServicoVncSigla", AV35TFContratoSrvVnc_ServicoVncSigla);
         lV43TFContratoSrvVnc_SrvVncCntNum = StringUtil.PadR( StringUtil.RTrim( AV43TFContratoSrvVnc_SrvVncCntNum), 20, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFContratoSrvVnc_SrvVncCntNum", AV43TFContratoSrvVnc_SrvVncCntNum);
         /* Using cursor H00GA8 */
         pr_default.execute(2, new Object[] {AV18ContratoSrvVnc_CntSrvCod, lV58TFContratoServicosVnc_Descricao, AV59TFContratoServicosVnc_Descricao_Sel, lV35TFContratoSrvVnc_ServicoVncSigla, AV36TFContratoSrvVnc_ServicoVncSigla_Sel, lV43TFContratoSrvVnc_SrvVncCntNum, AV44TFContratoSrvVnc_SrvVncCntNum_Sel});
         GRID_nRecordCount = H00GA8_AGRID_nRecordCount[0];
         pr_default.close(2);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV58TFContratoServicosVnc_Descricao, AV59TFContratoServicosVnc_Descricao_Sel, AV35TFContratoSrvVnc_ServicoVncSigla, AV36TFContratoSrvVnc_ServicoVncSigla_Sel, AV43TFContratoSrvVnc_SrvVncCntNum, AV44TFContratoSrvVnc_SrvVncCntNum_Sel, AV47TFContratoSrvVnc_SemCusto_Sel, AV50TFContratoServicosVnc_Ativo_Sel, AV18ContratoSrvVnc_CntSrvCod, AV60ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace, AV29ddo_ContratoSrvVnc_DoStatusDmnTitleControlIdToReplace, AV33ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace, AV37ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace, AV41ddo_ContratoSrvVnc_SrvVncStatusTitleControlIdToReplace, AV45ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace, AV48ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace, AV51ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace, A6AreaTrabalho_Descricao, A72AreaTrabalho_Ativo, A5AreaTrabalho_Codigo, A1297ContratadaUsuario_AreaTrabalhoDes, A69ContratadaUsuario_UsuarioCod, A1228ContratadaUsuario_AreaTrabalhoCod, AV67Pgmname, AV28TFContratoSrvVnc_DoStatusDmn_Sels, AV32TFContratoSrvVnc_StatusDmn_Sels, AV40TFContratoSrvVnc_SrvVncStatus_Sels, A75Contrato_AreaTrabalhoCod, AV19AreaTrabalho_Codigo, A921ContratoSrvVnc_ServicoCod, AV20Servico_Codigo, A917ContratoSrvVnc_Codigo, A924ContratoSrvVnc_ServicoVncSigla, A1800ContratoSrvVnc_DoStatusDmn, A1084ContratoSrvVnc_StatusDmn, AV6WWPContext, A1453ContratoServicosVnc_Ativo, A1092ContratoSrvVnc_PrestadoraPesNom, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV58TFContratoServicosVnc_Descricao, AV59TFContratoServicosVnc_Descricao_Sel, AV35TFContratoSrvVnc_ServicoVncSigla, AV36TFContratoSrvVnc_ServicoVncSigla_Sel, AV43TFContratoSrvVnc_SrvVncCntNum, AV44TFContratoSrvVnc_SrvVncCntNum_Sel, AV47TFContratoSrvVnc_SemCusto_Sel, AV50TFContratoServicosVnc_Ativo_Sel, AV18ContratoSrvVnc_CntSrvCod, AV60ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace, AV29ddo_ContratoSrvVnc_DoStatusDmnTitleControlIdToReplace, AV33ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace, AV37ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace, AV41ddo_ContratoSrvVnc_SrvVncStatusTitleControlIdToReplace, AV45ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace, AV48ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace, AV51ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace, A6AreaTrabalho_Descricao, A72AreaTrabalho_Ativo, A5AreaTrabalho_Codigo, A1297ContratadaUsuario_AreaTrabalhoDes, A69ContratadaUsuario_UsuarioCod, A1228ContratadaUsuario_AreaTrabalhoCod, AV67Pgmname, AV28TFContratoSrvVnc_DoStatusDmn_Sels, AV32TFContratoSrvVnc_StatusDmn_Sels, AV40TFContratoSrvVnc_SrvVncStatus_Sels, A75Contrato_AreaTrabalhoCod, AV19AreaTrabalho_Codigo, A921ContratoSrvVnc_ServicoCod, AV20Servico_Codigo, A917ContratoSrvVnc_Codigo, A924ContratoSrvVnc_ServicoVncSigla, A1800ContratoSrvVnc_DoStatusDmn, A1084ContratoSrvVnc_StatusDmn, AV6WWPContext, A1453ContratoServicosVnc_Ativo, A1092ContratoSrvVnc_PrestadoraPesNom, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV58TFContratoServicosVnc_Descricao, AV59TFContratoServicosVnc_Descricao_Sel, AV35TFContratoSrvVnc_ServicoVncSigla, AV36TFContratoSrvVnc_ServicoVncSigla_Sel, AV43TFContratoSrvVnc_SrvVncCntNum, AV44TFContratoSrvVnc_SrvVncCntNum_Sel, AV47TFContratoSrvVnc_SemCusto_Sel, AV50TFContratoServicosVnc_Ativo_Sel, AV18ContratoSrvVnc_CntSrvCod, AV60ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace, AV29ddo_ContratoSrvVnc_DoStatusDmnTitleControlIdToReplace, AV33ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace, AV37ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace, AV41ddo_ContratoSrvVnc_SrvVncStatusTitleControlIdToReplace, AV45ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace, AV48ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace, AV51ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace, A6AreaTrabalho_Descricao, A72AreaTrabalho_Ativo, A5AreaTrabalho_Codigo, A1297ContratadaUsuario_AreaTrabalhoDes, A69ContratadaUsuario_UsuarioCod, A1228ContratadaUsuario_AreaTrabalhoCod, AV67Pgmname, AV28TFContratoSrvVnc_DoStatusDmn_Sels, AV32TFContratoSrvVnc_StatusDmn_Sels, AV40TFContratoSrvVnc_SrvVncStatus_Sels, A75Contrato_AreaTrabalhoCod, AV19AreaTrabalho_Codigo, A921ContratoSrvVnc_ServicoCod, AV20Servico_Codigo, A917ContratoSrvVnc_Codigo, A924ContratoSrvVnc_ServicoVncSigla, A1800ContratoSrvVnc_DoStatusDmn, A1084ContratoSrvVnc_StatusDmn, AV6WWPContext, A1453ContratoServicosVnc_Ativo, A1092ContratoSrvVnc_PrestadoraPesNom, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV58TFContratoServicosVnc_Descricao, AV59TFContratoServicosVnc_Descricao_Sel, AV35TFContratoSrvVnc_ServicoVncSigla, AV36TFContratoSrvVnc_ServicoVncSigla_Sel, AV43TFContratoSrvVnc_SrvVncCntNum, AV44TFContratoSrvVnc_SrvVncCntNum_Sel, AV47TFContratoSrvVnc_SemCusto_Sel, AV50TFContratoServicosVnc_Ativo_Sel, AV18ContratoSrvVnc_CntSrvCod, AV60ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace, AV29ddo_ContratoSrvVnc_DoStatusDmnTitleControlIdToReplace, AV33ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace, AV37ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace, AV41ddo_ContratoSrvVnc_SrvVncStatusTitleControlIdToReplace, AV45ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace, AV48ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace, AV51ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace, A6AreaTrabalho_Descricao, A72AreaTrabalho_Ativo, A5AreaTrabalho_Codigo, A1297ContratadaUsuario_AreaTrabalhoDes, A69ContratadaUsuario_UsuarioCod, A1228ContratadaUsuario_AreaTrabalhoCod, AV67Pgmname, AV28TFContratoSrvVnc_DoStatusDmn_Sels, AV32TFContratoSrvVnc_StatusDmn_Sels, AV40TFContratoSrvVnc_SrvVncStatus_Sels, A75Contrato_AreaTrabalhoCod, AV19AreaTrabalho_Codigo, A921ContratoSrvVnc_ServicoCod, AV20Servico_Codigo, A917ContratoSrvVnc_Codigo, A924ContratoSrvVnc_ServicoVncSigla, A1800ContratoSrvVnc_DoStatusDmn, A1084ContratoSrvVnc_StatusDmn, AV6WWPContext, A1453ContratoServicosVnc_Ativo, A1092ContratoSrvVnc_PrestadoraPesNom, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV58TFContratoServicosVnc_Descricao, AV59TFContratoServicosVnc_Descricao_Sel, AV35TFContratoSrvVnc_ServicoVncSigla, AV36TFContratoSrvVnc_ServicoVncSigla_Sel, AV43TFContratoSrvVnc_SrvVncCntNum, AV44TFContratoSrvVnc_SrvVncCntNum_Sel, AV47TFContratoSrvVnc_SemCusto_Sel, AV50TFContratoServicosVnc_Ativo_Sel, AV18ContratoSrvVnc_CntSrvCod, AV60ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace, AV29ddo_ContratoSrvVnc_DoStatusDmnTitleControlIdToReplace, AV33ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace, AV37ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace, AV41ddo_ContratoSrvVnc_SrvVncStatusTitleControlIdToReplace, AV45ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace, AV48ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace, AV51ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace, A6AreaTrabalho_Descricao, A72AreaTrabalho_Ativo, A5AreaTrabalho_Codigo, A1297ContratadaUsuario_AreaTrabalhoDes, A69ContratadaUsuario_UsuarioCod, A1228ContratadaUsuario_AreaTrabalhoCod, AV67Pgmname, AV28TFContratoSrvVnc_DoStatusDmn_Sels, AV32TFContratoSrvVnc_StatusDmn_Sels, AV40TFContratoSrvVnc_SrvVncStatus_Sels, A75Contrato_AreaTrabalhoCod, AV19AreaTrabalho_Codigo, A921ContratoSrvVnc_ServicoCod, AV20Servico_Codigo, A917ContratoSrvVnc_Codigo, A924ContratoSrvVnc_ServicoVncSigla, A1800ContratoSrvVnc_DoStatusDmn, A1084ContratoSrvVnc_StatusDmn, AV6WWPContext, A1453ContratoServicosVnc_Ativo, A1092ContratoSrvVnc_PrestadoraPesNom, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPGA0( )
      {
         /* Before Start, stand alone formulas. */
         AV67Pgmname = "ContratoServicos_VncWC";
         context.Gx_err = 0;
         edtavPrestador_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPrestador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPrestador_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E24GA2 */
         E24GA2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV52DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOSERVICOSVNC_DESCRICAOTITLEFILTERDATA"), AV57ContratoServicosVnc_DescricaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOSRVVNC_DOSTATUSDMNTITLEFILTERDATA"), AV26ContratoSrvVnc_DoStatusDmnTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOSRVVNC_STATUSDMNTITLEFILTERDATA"), AV30ContratoSrvVnc_StatusDmnTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOSRVVNC_SERVICOVNCSIGLATITLEFILTERDATA"), AV34ContratoSrvVnc_ServicoVncSiglaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOSRVVNC_SRVVNCSTATUSTITLEFILTERDATA"), AV38ContratoSrvVnc_SrvVncStatusTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOSRVVNC_SRVVNCCNTNUMTITLEFILTERDATA"), AV42ContratoSrvVnc_SrvVncCntNumTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOSRVVNC_SEMCUSTOTITLEFILTERDATA"), AV46ContratoSrvVnc_SemCustoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOSERVICOSVNC_ATIVOTITLEFILTERDATA"), AV49ContratoServicosVnc_AtivoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vWWPCONTEXT"), AV6WWPContext);
            /* Read variables values. */
            cmbavAreatrabalho_codigo.Name = cmbavAreatrabalho_codigo_Internalname;
            cmbavAreatrabalho_codigo.CurrentValue = cgiGet( cmbavAreatrabalho_codigo_Internalname);
            AV19AreaTrabalho_Codigo = (int)(NumberUtil.Val( cgiGet( cmbavAreatrabalho_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19AreaTrabalho_Codigo), 6, 0)));
            dynavServico_codigo.Name = dynavServico_codigo_Internalname;
            dynavServico_codigo.CurrentValue = cgiGet( dynavServico_codigo_Internalname);
            AV20Servico_Codigo = (int)(NumberUtil.Val( cgiGet( dynavServico_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Servico_Codigo), 6, 0)));
            cmbavRegra.Name = cmbavRegra_Internalname;
            cmbavRegra.CurrentValue = cgiGet( cmbavRegra_Internalname);
            AV21Regra = (int)(NumberUtil.Val( cgiGet( cmbavRegra_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21Regra", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Regra), 6, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vORDEREDBY");
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV13OrderedBy = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            else
            {
               AV13OrderedBy = (short)(context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            AV58TFContratoServicosVnc_Descricao = StringUtil.Upper( cgiGet( edtavTfcontratoservicosvnc_descricao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV58TFContratoServicosVnc_Descricao", AV58TFContratoServicosVnc_Descricao);
            AV59TFContratoServicosVnc_Descricao_Sel = StringUtil.Upper( cgiGet( edtavTfcontratoservicosvnc_descricao_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV59TFContratoServicosVnc_Descricao_Sel", AV59TFContratoServicosVnc_Descricao_Sel);
            AV35TFContratoSrvVnc_ServicoVncSigla = StringUtil.Upper( cgiGet( edtavTfcontratosrvvnc_servicovncsigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35TFContratoSrvVnc_ServicoVncSigla", AV35TFContratoSrvVnc_ServicoVncSigla);
            AV36TFContratoSrvVnc_ServicoVncSigla_Sel = StringUtil.Upper( cgiGet( edtavTfcontratosrvvnc_servicovncsigla_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36TFContratoSrvVnc_ServicoVncSigla_Sel", AV36TFContratoSrvVnc_ServicoVncSigla_Sel);
            AV43TFContratoSrvVnc_SrvVncCntNum = cgiGet( edtavTfcontratosrvvnc_srvvnccntnum_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFContratoSrvVnc_SrvVncCntNum", AV43TFContratoSrvVnc_SrvVncCntNum);
            AV44TFContratoSrvVnc_SrvVncCntNum_Sel = cgiGet( edtavTfcontratosrvvnc_srvvnccntnum_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44TFContratoSrvVnc_SrvVncCntNum_Sel", AV44TFContratoSrvVnc_SrvVncCntNum_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratosrvvnc_semcusto_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratosrvvnc_semcusto_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSRVVNC_SEMCUSTO_SEL");
               GX_FocusControl = edtavTfcontratosrvvnc_semcusto_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV47TFContratoSrvVnc_SemCusto_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV47TFContratoSrvVnc_SemCusto_Sel", StringUtil.Str( (decimal)(AV47TFContratoSrvVnc_SemCusto_Sel), 1, 0));
            }
            else
            {
               AV47TFContratoSrvVnc_SemCusto_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratosrvvnc_semcusto_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV47TFContratoSrvVnc_SemCusto_Sel", StringUtil.Str( (decimal)(AV47TFContratoSrvVnc_SemCusto_Sel), 1, 0));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosvnc_ativo_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosvnc_ativo_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSVNC_ATIVO_SEL");
               GX_FocusControl = edtavTfcontratoservicosvnc_ativo_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV50TFContratoServicosVnc_Ativo_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV50TFContratoServicosVnc_Ativo_Sel", StringUtil.Str( (decimal)(AV50TFContratoServicosVnc_Ativo_Sel), 1, 0));
            }
            else
            {
               AV50TFContratoServicosVnc_Ativo_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicosvnc_ativo_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV50TFContratoServicosVnc_Ativo_Sel", StringUtil.Str( (decimal)(AV50TFContratoServicosVnc_Ativo_Sel), 1, 0));
            }
            AV60ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosvnc_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace", AV60ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace);
            AV29ddo_ContratoSrvVnc_DoStatusDmnTitleControlIdToReplace = cgiGet( edtavDdo_contratosrvvnc_dostatusdmntitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29ddo_ContratoSrvVnc_DoStatusDmnTitleControlIdToReplace", AV29ddo_ContratoSrvVnc_DoStatusDmnTitleControlIdToReplace);
            AV33ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace = cgiGet( edtavDdo_contratosrvvnc_statusdmntitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace", AV33ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace);
            AV37ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace = cgiGet( edtavDdo_contratosrvvnc_servicovncsiglatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace", AV37ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace);
            AV41ddo_ContratoSrvVnc_SrvVncStatusTitleControlIdToReplace = cgiGet( edtavDdo_contratosrvvnc_srvvncstatustitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41ddo_ContratoSrvVnc_SrvVncStatusTitleControlIdToReplace", AV41ddo_ContratoSrvVnc_SrvVncStatusTitleControlIdToReplace);
            AV45ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace = cgiGet( edtavDdo_contratosrvvnc_srvvnccntnumtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace", AV45ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace);
            AV48ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace = cgiGet( edtavDdo_contratosrvvnc_semcustotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace", AV48ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace);
            AV51ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosvnc_ativotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV51ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace", AV51ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_8 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_8"), ",", "."));
            AV54GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV55GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV18ContratoSrvVnc_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV18ContratoSrvVnc_CntSrvCod"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contratoservicosvnc_descricao_Caption = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Caption");
            Ddo_contratoservicosvnc_descricao_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Tooltip");
            Ddo_contratoservicosvnc_descricao_Cls = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Cls");
            Ddo_contratoservicosvnc_descricao_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Filteredtext_set");
            Ddo_contratoservicosvnc_descricao_Selectedvalue_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Selectedvalue_set");
            Ddo_contratoservicosvnc_descricao_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Dropdownoptionstype");
            Ddo_contratoservicosvnc_descricao_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_contratoservicosvnc_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Includesortasc"));
            Ddo_contratoservicosvnc_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Includesortdsc"));
            Ddo_contratoservicosvnc_descricao_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Sortedstatus");
            Ddo_contratoservicosvnc_descricao_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Includefilter"));
            Ddo_contratoservicosvnc_descricao_Filtertype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Filtertype");
            Ddo_contratoservicosvnc_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Filterisrange"));
            Ddo_contratoservicosvnc_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Includedatalist"));
            Ddo_contratoservicosvnc_descricao_Datalisttype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Datalisttype");
            Ddo_contratoservicosvnc_descricao_Datalistproc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Datalistproc");
            Ddo_contratoservicosvnc_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratoservicosvnc_descricao_Sortasc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Sortasc");
            Ddo_contratoservicosvnc_descricao_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Sortdsc");
            Ddo_contratoservicosvnc_descricao_Loadingdata = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Loadingdata");
            Ddo_contratoservicosvnc_descricao_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Cleanfilter");
            Ddo_contratoservicosvnc_descricao_Noresultsfound = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Noresultsfound");
            Ddo_contratoservicosvnc_descricao_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Searchbuttontext");
            Ddo_contratosrvvnc_dostatusdmn_Caption = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_DOSTATUSDMN_Caption");
            Ddo_contratosrvvnc_dostatusdmn_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_DOSTATUSDMN_Tooltip");
            Ddo_contratosrvvnc_dostatusdmn_Cls = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_DOSTATUSDMN_Cls");
            Ddo_contratosrvvnc_dostatusdmn_Selectedvalue_set = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_DOSTATUSDMN_Selectedvalue_set");
            Ddo_contratosrvvnc_dostatusdmn_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_DOSTATUSDMN_Dropdownoptionstype");
            Ddo_contratosrvvnc_dostatusdmn_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_DOSTATUSDMN_Titlecontrolidtoreplace");
            Ddo_contratosrvvnc_dostatusdmn_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_DOSTATUSDMN_Includesortasc"));
            Ddo_contratosrvvnc_dostatusdmn_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_DOSTATUSDMN_Includesortdsc"));
            Ddo_contratosrvvnc_dostatusdmn_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_DOSTATUSDMN_Sortedstatus");
            Ddo_contratosrvvnc_dostatusdmn_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_DOSTATUSDMN_Includefilter"));
            Ddo_contratosrvvnc_dostatusdmn_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_DOSTATUSDMN_Includedatalist"));
            Ddo_contratosrvvnc_dostatusdmn_Datalisttype = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_DOSTATUSDMN_Datalisttype");
            Ddo_contratosrvvnc_dostatusdmn_Allowmultipleselection = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_DOSTATUSDMN_Allowmultipleselection"));
            Ddo_contratosrvvnc_dostatusdmn_Datalistfixedvalues = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_DOSTATUSDMN_Datalistfixedvalues");
            Ddo_contratosrvvnc_dostatusdmn_Sortasc = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_DOSTATUSDMN_Sortasc");
            Ddo_contratosrvvnc_dostatusdmn_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_DOSTATUSDMN_Sortdsc");
            Ddo_contratosrvvnc_dostatusdmn_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_DOSTATUSDMN_Cleanfilter");
            Ddo_contratosrvvnc_dostatusdmn_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_DOSTATUSDMN_Searchbuttontext");
            Ddo_contratosrvvnc_statusdmn_Caption = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_STATUSDMN_Caption");
            Ddo_contratosrvvnc_statusdmn_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_STATUSDMN_Tooltip");
            Ddo_contratosrvvnc_statusdmn_Cls = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_STATUSDMN_Cls");
            Ddo_contratosrvvnc_statusdmn_Selectedvalue_set = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_STATUSDMN_Selectedvalue_set");
            Ddo_contratosrvvnc_statusdmn_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_STATUSDMN_Dropdownoptionstype");
            Ddo_contratosrvvnc_statusdmn_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_STATUSDMN_Titlecontrolidtoreplace");
            Ddo_contratosrvvnc_statusdmn_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_STATUSDMN_Includesortasc"));
            Ddo_contratosrvvnc_statusdmn_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_STATUSDMN_Includesortdsc"));
            Ddo_contratosrvvnc_statusdmn_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_STATUSDMN_Sortedstatus");
            Ddo_contratosrvvnc_statusdmn_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_STATUSDMN_Includefilter"));
            Ddo_contratosrvvnc_statusdmn_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_STATUSDMN_Includedatalist"));
            Ddo_contratosrvvnc_statusdmn_Datalisttype = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_STATUSDMN_Datalisttype");
            Ddo_contratosrvvnc_statusdmn_Allowmultipleselection = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_STATUSDMN_Allowmultipleselection"));
            Ddo_contratosrvvnc_statusdmn_Datalistfixedvalues = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_STATUSDMN_Datalistfixedvalues");
            Ddo_contratosrvvnc_statusdmn_Sortasc = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_STATUSDMN_Sortasc");
            Ddo_contratosrvvnc_statusdmn_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_STATUSDMN_Sortdsc");
            Ddo_contratosrvvnc_statusdmn_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_STATUSDMN_Cleanfilter");
            Ddo_contratosrvvnc_statusdmn_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_STATUSDMN_Searchbuttontext");
            Ddo_contratosrvvnc_servicovncsigla_Caption = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Caption");
            Ddo_contratosrvvnc_servicovncsigla_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Tooltip");
            Ddo_contratosrvvnc_servicovncsigla_Cls = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Cls");
            Ddo_contratosrvvnc_servicovncsigla_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Filteredtext_set");
            Ddo_contratosrvvnc_servicovncsigla_Selectedvalue_set = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Selectedvalue_set");
            Ddo_contratosrvvnc_servicovncsigla_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Dropdownoptionstype");
            Ddo_contratosrvvnc_servicovncsigla_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Titlecontrolidtoreplace");
            Ddo_contratosrvvnc_servicovncsigla_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Includesortasc"));
            Ddo_contratosrvvnc_servicovncsigla_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Includesortdsc"));
            Ddo_contratosrvvnc_servicovncsigla_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Sortedstatus");
            Ddo_contratosrvvnc_servicovncsigla_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Includefilter"));
            Ddo_contratosrvvnc_servicovncsigla_Filtertype = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Filtertype");
            Ddo_contratosrvvnc_servicovncsigla_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Filterisrange"));
            Ddo_contratosrvvnc_servicovncsigla_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Includedatalist"));
            Ddo_contratosrvvnc_servicovncsigla_Datalisttype = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Datalisttype");
            Ddo_contratosrvvnc_servicovncsigla_Datalistproc = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Datalistproc");
            Ddo_contratosrvvnc_servicovncsigla_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratosrvvnc_servicovncsigla_Sortasc = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Sortasc");
            Ddo_contratosrvvnc_servicovncsigla_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Sortdsc");
            Ddo_contratosrvvnc_servicovncsigla_Loadingdata = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Loadingdata");
            Ddo_contratosrvvnc_servicovncsigla_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Cleanfilter");
            Ddo_contratosrvvnc_servicovncsigla_Noresultsfound = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Noresultsfound");
            Ddo_contratosrvvnc_servicovncsigla_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Searchbuttontext");
            Ddo_contratosrvvnc_srvvncstatus_Caption = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCSTATUS_Caption");
            Ddo_contratosrvvnc_srvvncstatus_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCSTATUS_Tooltip");
            Ddo_contratosrvvnc_srvvncstatus_Cls = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCSTATUS_Cls");
            Ddo_contratosrvvnc_srvvncstatus_Selectedvalue_set = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCSTATUS_Selectedvalue_set");
            Ddo_contratosrvvnc_srvvncstatus_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCSTATUS_Dropdownoptionstype");
            Ddo_contratosrvvnc_srvvncstatus_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCSTATUS_Titlecontrolidtoreplace");
            Ddo_contratosrvvnc_srvvncstatus_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCSTATUS_Includesortasc"));
            Ddo_contratosrvvnc_srvvncstatus_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCSTATUS_Includesortdsc"));
            Ddo_contratosrvvnc_srvvncstatus_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCSTATUS_Sortedstatus");
            Ddo_contratosrvvnc_srvvncstatus_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCSTATUS_Includefilter"));
            Ddo_contratosrvvnc_srvvncstatus_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCSTATUS_Includedatalist"));
            Ddo_contratosrvvnc_srvvncstatus_Datalisttype = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCSTATUS_Datalisttype");
            Ddo_contratosrvvnc_srvvncstatus_Allowmultipleselection = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCSTATUS_Allowmultipleselection"));
            Ddo_contratosrvvnc_srvvncstatus_Datalistfixedvalues = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCSTATUS_Datalistfixedvalues");
            Ddo_contratosrvvnc_srvvncstatus_Sortasc = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCSTATUS_Sortasc");
            Ddo_contratosrvvnc_srvvncstatus_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCSTATUS_Sortdsc");
            Ddo_contratosrvvnc_srvvncstatus_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCSTATUS_Cleanfilter");
            Ddo_contratosrvvnc_srvvncstatus_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCSTATUS_Searchbuttontext");
            Ddo_contratosrvvnc_srvvnccntnum_Caption = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Caption");
            Ddo_contratosrvvnc_srvvnccntnum_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Tooltip");
            Ddo_contratosrvvnc_srvvnccntnum_Cls = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Cls");
            Ddo_contratosrvvnc_srvvnccntnum_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Filteredtext_set");
            Ddo_contratosrvvnc_srvvnccntnum_Selectedvalue_set = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Selectedvalue_set");
            Ddo_contratosrvvnc_srvvnccntnum_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Dropdownoptionstype");
            Ddo_contratosrvvnc_srvvnccntnum_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Titlecontrolidtoreplace");
            Ddo_contratosrvvnc_srvvnccntnum_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Includesortasc"));
            Ddo_contratosrvvnc_srvvnccntnum_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Includesortdsc"));
            Ddo_contratosrvvnc_srvvnccntnum_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Sortedstatus");
            Ddo_contratosrvvnc_srvvnccntnum_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Includefilter"));
            Ddo_contratosrvvnc_srvvnccntnum_Filtertype = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Filtertype");
            Ddo_contratosrvvnc_srvvnccntnum_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Filterisrange"));
            Ddo_contratosrvvnc_srvvnccntnum_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Includedatalist"));
            Ddo_contratosrvvnc_srvvnccntnum_Datalisttype = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Datalisttype");
            Ddo_contratosrvvnc_srvvnccntnum_Datalistproc = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Datalistproc");
            Ddo_contratosrvvnc_srvvnccntnum_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratosrvvnc_srvvnccntnum_Sortasc = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Sortasc");
            Ddo_contratosrvvnc_srvvnccntnum_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Sortdsc");
            Ddo_contratosrvvnc_srvvnccntnum_Loadingdata = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Loadingdata");
            Ddo_contratosrvvnc_srvvnccntnum_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Cleanfilter");
            Ddo_contratosrvvnc_srvvnccntnum_Noresultsfound = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Noresultsfound");
            Ddo_contratosrvvnc_srvvnccntnum_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Searchbuttontext");
            Ddo_contratosrvvnc_semcusto_Caption = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SEMCUSTO_Caption");
            Ddo_contratosrvvnc_semcusto_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SEMCUSTO_Tooltip");
            Ddo_contratosrvvnc_semcusto_Cls = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SEMCUSTO_Cls");
            Ddo_contratosrvvnc_semcusto_Selectedvalue_set = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SEMCUSTO_Selectedvalue_set");
            Ddo_contratosrvvnc_semcusto_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SEMCUSTO_Dropdownoptionstype");
            Ddo_contratosrvvnc_semcusto_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SEMCUSTO_Titlecontrolidtoreplace");
            Ddo_contratosrvvnc_semcusto_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SEMCUSTO_Includesortasc"));
            Ddo_contratosrvvnc_semcusto_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SEMCUSTO_Includesortdsc"));
            Ddo_contratosrvvnc_semcusto_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SEMCUSTO_Sortedstatus");
            Ddo_contratosrvvnc_semcusto_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SEMCUSTO_Includefilter"));
            Ddo_contratosrvvnc_semcusto_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SEMCUSTO_Includedatalist"));
            Ddo_contratosrvvnc_semcusto_Datalisttype = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SEMCUSTO_Datalisttype");
            Ddo_contratosrvvnc_semcusto_Datalistfixedvalues = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SEMCUSTO_Datalistfixedvalues");
            Ddo_contratosrvvnc_semcusto_Sortasc = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SEMCUSTO_Sortasc");
            Ddo_contratosrvvnc_semcusto_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SEMCUSTO_Sortdsc");
            Ddo_contratosrvvnc_semcusto_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SEMCUSTO_Cleanfilter");
            Ddo_contratosrvvnc_semcusto_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SEMCUSTO_Searchbuttontext");
            Ddo_contratoservicosvnc_ativo_Caption = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_ATIVO_Caption");
            Ddo_contratoservicosvnc_ativo_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_ATIVO_Tooltip");
            Ddo_contratoservicosvnc_ativo_Cls = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_ATIVO_Cls");
            Ddo_contratoservicosvnc_ativo_Selectedvalue_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_ATIVO_Selectedvalue_set");
            Ddo_contratoservicosvnc_ativo_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_ATIVO_Dropdownoptionstype");
            Ddo_contratoservicosvnc_ativo_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_ATIVO_Titlecontrolidtoreplace");
            Ddo_contratoservicosvnc_ativo_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_ATIVO_Includesortasc"));
            Ddo_contratoservicosvnc_ativo_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_ATIVO_Includesortdsc"));
            Ddo_contratoservicosvnc_ativo_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_ATIVO_Sortedstatus");
            Ddo_contratoservicosvnc_ativo_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_ATIVO_Includefilter"));
            Ddo_contratoservicosvnc_ativo_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_ATIVO_Includedatalist"));
            Ddo_contratoservicosvnc_ativo_Datalisttype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_ATIVO_Datalisttype");
            Ddo_contratoservicosvnc_ativo_Datalistfixedvalues = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_ATIVO_Datalistfixedvalues");
            Ddo_contratoservicosvnc_ativo_Sortasc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_ATIVO_Sortasc");
            Ddo_contratoservicosvnc_ativo_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_ATIVO_Sortdsc");
            Ddo_contratoservicosvnc_ativo_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_ATIVO_Cleanfilter");
            Ddo_contratoservicosvnc_ativo_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_ATIVO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contratoservicosvnc_descricao_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Activeeventkey");
            Ddo_contratoservicosvnc_descricao_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Filteredtext_get");
            Ddo_contratoservicosvnc_descricao_Selectedvalue_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO_Selectedvalue_get");
            Ddo_contratosrvvnc_dostatusdmn_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_DOSTATUSDMN_Activeeventkey");
            Ddo_contratosrvvnc_dostatusdmn_Selectedvalue_get = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_DOSTATUSDMN_Selectedvalue_get");
            Ddo_contratosrvvnc_statusdmn_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_STATUSDMN_Activeeventkey");
            Ddo_contratosrvvnc_statusdmn_Selectedvalue_get = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_STATUSDMN_Selectedvalue_get");
            Ddo_contratosrvvnc_servicovncsigla_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Activeeventkey");
            Ddo_contratosrvvnc_servicovncsigla_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Filteredtext_get");
            Ddo_contratosrvvnc_servicovncsigla_Selectedvalue_get = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Selectedvalue_get");
            Ddo_contratosrvvnc_srvvncstatus_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCSTATUS_Activeeventkey");
            Ddo_contratosrvvnc_srvvncstatus_Selectedvalue_get = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCSTATUS_Selectedvalue_get");
            Ddo_contratosrvvnc_srvvnccntnum_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Activeeventkey");
            Ddo_contratosrvvnc_srvvnccntnum_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Filteredtext_get");
            Ddo_contratosrvvnc_srvvnccntnum_Selectedvalue_get = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Selectedvalue_get");
            Ddo_contratosrvvnc_semcusto_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SEMCUSTO_Activeeventkey");
            Ddo_contratosrvvnc_semcusto_Selectedvalue_get = cgiGet( sPrefix+"DDO_CONTRATOSRVVNC_SEMCUSTO_Selectedvalue_get");
            Ddo_contratoservicosvnc_ativo_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_ATIVO_Activeeventkey");
            Ddo_contratoservicosvnc_ativo_Selectedvalue_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSVNC_ATIVO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSVNC_DESCRICAO"), AV58TFContratoServicosVnc_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSVNC_DESCRICAO_SEL"), AV59TFContratoServicosVnc_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOSRVVNC_SERVICOVNCSIGLA"), AV35TFContratoSrvVnc_ServicoVncSigla) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL"), AV36TFContratoSrvVnc_ServicoVncSigla_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOSRVVNC_SRVVNCCNTNUM"), AV43TFContratoSrvVnc_SrvVncCntNum) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOSRVVNC_SRVVNCCNTNUM_SEL"), AV44TFContratoSrvVnc_SrvVncCntNum_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSRVVNC_SEMCUSTO_SEL"), ",", ".") != Convert.ToDecimal( AV47TFContratoSrvVnc_SemCusto_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSVNC_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV50TFContratoServicosVnc_Ativo_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E24GA2 */
         E24GA2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E24GA2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTfcontratoservicosvnc_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicosvnc_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosvnc_descricao_Visible), 5, 0)));
         edtavTfcontratoservicosvnc_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicosvnc_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosvnc_descricao_sel_Visible), 5, 0)));
         edtavTfcontratosrvvnc_servicovncsigla_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratosrvvnc_servicovncsigla_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratosrvvnc_servicovncsigla_Visible), 5, 0)));
         edtavTfcontratosrvvnc_servicovncsigla_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratosrvvnc_servicovncsigla_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratosrvvnc_servicovncsigla_sel_Visible), 5, 0)));
         edtavTfcontratosrvvnc_srvvnccntnum_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratosrvvnc_srvvnccntnum_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratosrvvnc_srvvnccntnum_Visible), 5, 0)));
         edtavTfcontratosrvvnc_srvvnccntnum_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratosrvvnc_srvvnccntnum_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratosrvvnc_srvvnccntnum_sel_Visible), 5, 0)));
         edtavTfcontratosrvvnc_semcusto_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratosrvvnc_semcusto_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratosrvvnc_semcusto_sel_Visible), 5, 0)));
         edtavTfcontratoservicosvnc_ativo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicosvnc_ativo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosvnc_ativo_sel_Visible), 5, 0)));
         Ddo_contratoservicosvnc_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosVnc_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosvnc_descricao_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosvnc_descricao_Titlecontrolidtoreplace);
         AV60ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace = Ddo_contratoservicosvnc_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace", AV60ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace);
         edtavDdo_contratoservicosvnc_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratoservicosvnc_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosvnc_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratosrvvnc_dostatusdmn_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoSrvVnc_DoStatusDmn";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratosrvvnc_dostatusdmn_Internalname, "TitleControlIdToReplace", Ddo_contratosrvvnc_dostatusdmn_Titlecontrolidtoreplace);
         AV29ddo_ContratoSrvVnc_DoStatusDmnTitleControlIdToReplace = Ddo_contratosrvvnc_dostatusdmn_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29ddo_ContratoSrvVnc_DoStatusDmnTitleControlIdToReplace", AV29ddo_ContratoSrvVnc_DoStatusDmnTitleControlIdToReplace);
         edtavDdo_contratosrvvnc_dostatusdmntitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratosrvvnc_dostatusdmntitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratosrvvnc_dostatusdmntitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratosrvvnc_statusdmn_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoSrvVnc_StatusDmn";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratosrvvnc_statusdmn_Internalname, "TitleControlIdToReplace", Ddo_contratosrvvnc_statusdmn_Titlecontrolidtoreplace);
         AV33ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace = Ddo_contratosrvvnc_statusdmn_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace", AV33ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace);
         edtavDdo_contratosrvvnc_statusdmntitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratosrvvnc_statusdmntitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratosrvvnc_statusdmntitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratosrvvnc_servicovncsigla_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoSrvVnc_ServicoVncSigla";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratosrvvnc_servicovncsigla_Internalname, "TitleControlIdToReplace", Ddo_contratosrvvnc_servicovncsigla_Titlecontrolidtoreplace);
         AV37ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace = Ddo_contratosrvvnc_servicovncsigla_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace", AV37ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace);
         edtavDdo_contratosrvvnc_servicovncsiglatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratosrvvnc_servicovncsiglatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratosrvvnc_servicovncsiglatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratosrvvnc_srvvncstatus_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoSrvVnc_SrvVncStatus";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratosrvvnc_srvvncstatus_Internalname, "TitleControlIdToReplace", Ddo_contratosrvvnc_srvvncstatus_Titlecontrolidtoreplace);
         AV41ddo_ContratoSrvVnc_SrvVncStatusTitleControlIdToReplace = Ddo_contratosrvvnc_srvvncstatus_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41ddo_ContratoSrvVnc_SrvVncStatusTitleControlIdToReplace", AV41ddo_ContratoSrvVnc_SrvVncStatusTitleControlIdToReplace);
         edtavDdo_contratosrvvnc_srvvncstatustitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratosrvvnc_srvvncstatustitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratosrvvnc_srvvncstatustitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratosrvvnc_srvvnccntnum_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoSrvVnc_SrvVncCntNum";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratosrvvnc_srvvnccntnum_Internalname, "TitleControlIdToReplace", Ddo_contratosrvvnc_srvvnccntnum_Titlecontrolidtoreplace);
         AV45ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace = Ddo_contratosrvvnc_srvvnccntnum_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace", AV45ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace);
         edtavDdo_contratosrvvnc_srvvnccntnumtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratosrvvnc_srvvnccntnumtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratosrvvnc_srvvnccntnumtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratosrvvnc_semcusto_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoSrvVnc_SemCusto";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratosrvvnc_semcusto_Internalname, "TitleControlIdToReplace", Ddo_contratosrvvnc_semcusto_Titlecontrolidtoreplace);
         AV48ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace = Ddo_contratosrvvnc_semcusto_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace", AV48ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace);
         edtavDdo_contratosrvvnc_semcustotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratosrvvnc_semcustotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratosrvvnc_semcustotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicosvnc_ativo_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosVnc_Ativo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosvnc_ativo_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosvnc_ativo_Titlecontrolidtoreplace);
         AV51ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace = Ddo_contratoservicosvnc_ativo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV51ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace", AV51ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace);
         edtavDdo_contratoservicosvnc_ativotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratoservicosvnc_ativotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosvnc_ativotitlecontrolidtoreplace_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrderedby_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrderedby_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrderedby_Visible), 5, 0)));
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV52DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV52DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
         tblTblclonar_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, tblTblclonar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblclonar_Visible), 5, 0)));
         bttBtnclonar_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnclonar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnclonar_Visible), 5, 0)));
      }

      protected void E25GA2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV57ContratoServicosVnc_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV26ContratoSrvVnc_DoStatusDmnTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV30ContratoSrvVnc_StatusDmnTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV34ContratoSrvVnc_ServicoVncSiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38ContratoSrvVnc_SrvVncStatusTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42ContratoSrvVnc_SrvVncCntNumTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46ContratoSrvVnc_SemCustoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV49ContratoServicosVnc_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContratoServicosVnc_Descricao_Titleformat = 2;
         edtContratoServicosVnc_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Descri��o", AV60ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicosVnc_Descricao_Internalname, "Title", edtContratoServicosVnc_Descricao_Title);
         cmbContratoSrvVnc_DoStatusDmn_Titleformat = 2;
         cmbContratoSrvVnc_DoStatusDmn.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Do status", AV29ddo_ContratoSrvVnc_DoStatusDmnTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratoSrvVnc_DoStatusDmn_Internalname, "Title", cmbContratoSrvVnc_DoStatusDmn.Title.Text);
         cmbContratoSrvVnc_StatusDmn_Titleformat = 2;
         cmbContratoSrvVnc_StatusDmn.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "No status", AV33ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratoSrvVnc_StatusDmn_Internalname, "Title", cmbContratoSrvVnc_StatusDmn.Title.Text);
         edtContratoSrvVnc_ServicoVncSigla_Titleformat = 2;
         edtContratoSrvVnc_ServicoVncSigla_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Criar", AV37ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoSrvVnc_ServicoVncSigla_Internalname, "Title", edtContratoSrvVnc_ServicoVncSigla_Title);
         cmbContratoSrvVnc_SrvVncStatus_Titleformat = 2;
         cmbContratoSrvVnc_SrvVncStatus.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Com status", AV41ddo_ContratoSrvVnc_SrvVncStatusTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratoSrvVnc_SrvVncStatus_Internalname, "Title", cmbContratoSrvVnc_SrvVncStatus.Title.Text);
         edtContratoSrvVnc_SrvVncCntNum_Titleformat = 2;
         edtContratoSrvVnc_SrvVncCntNum_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Contrato", AV45ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoSrvVnc_SrvVncCntNum_Internalname, "Title", edtContratoSrvVnc_SrvVncCntNum_Title);
         cmbContratoSrvVnc_SemCusto_Titleformat = 2;
         cmbContratoSrvVnc_SemCusto.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Sem custo", AV48ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratoSrvVnc_SemCusto_Internalname, "Title", cmbContratoSrvVnc_SemCusto.Title.Text);
         chkContratoServicosVnc_Ativo_Titleformat = 2;
         chkContratoServicosVnc_Ativo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ativa?", AV51ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkContratoServicosVnc_Ativo_Internalname, "Title", chkContratoServicosVnc_Ativo.Title.Text);
         AV54GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV54GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54GridCurrentPage), 10, 0)));
         AV55GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV55GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55GridPageCount), 10, 0)));
         edtavUpdate_Visible = (AV6WWPContext.gxTpr_Update&&AV6WWPContext.gxTpr_Userehgestor ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUpdate_Visible), 5, 0)));
         edtavDelete_Visible = (AV6WWPContext.gxTpr_Delete&&AV6WWPContext.gxTpr_Userehgestor ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDelete_Visible), 5, 0)));
         imgInsert_Visible = (AV6WWPContext.gxTpr_Insert&&AV6WWPContext.gxTpr_Userehgestor ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Visible), 5, 0)));
         cmbavAreatrabalho_codigo.removeAllItems();
         cmbavAreatrabalho_codigo.addItem("0", "(Todas)", 0);
         if ( AV6WWPContext.gxTpr_Userehcontratante )
         {
            cmbavAreatrabalho_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV6WWPContext.gxTpr_Areatrabalho_codigo), 6, 0)), AV6WWPContext.gxTpr_Areatrabalho_descricao, 0);
            AV19AreaTrabalho_Codigo = AV6WWPContext.gxTpr_Areatrabalho_codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19AreaTrabalho_Codigo), 6, 0)));
         }
         else
         {
            if ( AV6WWPContext.gxTpr_Userehadministradorgam )
            {
               /* Using cursor H00GA9 */
               pr_default.execute(3);
               while ( (pr_default.getStatus(3) != 101) )
               {
                  A72AreaTrabalho_Ativo = H00GA9_A72AreaTrabalho_Ativo[0];
                  A5AreaTrabalho_Codigo = H00GA9_A5AreaTrabalho_Codigo[0];
                  A6AreaTrabalho_Descricao = H00GA9_A6AreaTrabalho_Descricao[0];
                  cmbavAreatrabalho_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)), A6AreaTrabalho_Descricao, 0);
                  pr_default.readNext(3);
               }
               pr_default.close(3);
            }
            else
            {
               /* Using cursor H00GA10 */
               pr_default.execute(4, new Object[] {AV6WWPContext.gxTpr_Userid});
               while ( (pr_default.getStatus(4) != 101) )
               {
                  A66ContratadaUsuario_ContratadaCod = H00GA10_A66ContratadaUsuario_ContratadaCod[0];
                  A69ContratadaUsuario_UsuarioCod = H00GA10_A69ContratadaUsuario_UsuarioCod[0];
                  A1228ContratadaUsuario_AreaTrabalhoCod = H00GA10_A1228ContratadaUsuario_AreaTrabalhoCod[0];
                  n1228ContratadaUsuario_AreaTrabalhoCod = H00GA10_n1228ContratadaUsuario_AreaTrabalhoCod[0];
                  A1297ContratadaUsuario_AreaTrabalhoDes = H00GA10_A1297ContratadaUsuario_AreaTrabalhoDes[0];
                  n1297ContratadaUsuario_AreaTrabalhoDes = H00GA10_n1297ContratadaUsuario_AreaTrabalhoDes[0];
                  A1228ContratadaUsuario_AreaTrabalhoCod = H00GA10_A1228ContratadaUsuario_AreaTrabalhoCod[0];
                  n1228ContratadaUsuario_AreaTrabalhoCod = H00GA10_n1228ContratadaUsuario_AreaTrabalhoCod[0];
                  A1297ContratadaUsuario_AreaTrabalhoDes = H00GA10_A1297ContratadaUsuario_AreaTrabalhoDes[0];
                  n1297ContratadaUsuario_AreaTrabalhoDes = H00GA10_n1297ContratadaUsuario_AreaTrabalhoDes[0];
                  cmbavAreatrabalho_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A1228ContratadaUsuario_AreaTrabalhoCod), 6, 0)), A1297ContratadaUsuario_AreaTrabalhoDes, 0);
                  pr_default.readNext(4);
               }
               pr_default.close(4);
            }
         }
         /* Execute user subroutine: 'CARREGARREGRA' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV57ContratoServicosVnc_DescricaoTitleFilterData", AV57ContratoServicosVnc_DescricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV26ContratoSrvVnc_DoStatusDmnTitleFilterData", AV26ContratoSrvVnc_DoStatusDmnTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV30ContratoSrvVnc_StatusDmnTitleFilterData", AV30ContratoSrvVnc_StatusDmnTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV34ContratoSrvVnc_ServicoVncSiglaTitleFilterData", AV34ContratoSrvVnc_ServicoVncSiglaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV38ContratoSrvVnc_SrvVncStatusTitleFilterData", AV38ContratoSrvVnc_SrvVncStatusTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV42ContratoSrvVnc_SrvVncCntNumTitleFilterData", AV42ContratoSrvVnc_SrvVncCntNumTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV46ContratoSrvVnc_SemCustoTitleFilterData", AV46ContratoSrvVnc_SemCustoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV49ContratoServicosVnc_AtivoTitleFilterData", AV49ContratoServicosVnc_AtivoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV6WWPContext", AV6WWPContext);
         cmbavAreatrabalho_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV19AreaTrabalho_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavAreatrabalho_codigo_Internalname, "Values", cmbavAreatrabalho_codigo.ToJavascriptSource());
         cmbavRegra.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21Regra), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavRegra_Internalname, "Values", cmbavRegra.ToJavascriptSource());
      }

      protected void E11GA2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV53PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV53PageToGo) ;
         }
      }

      protected void E12GA2( )
      {
         /* Ddo_contratoservicosvnc_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosvnc_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosvnc_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosvnc_descricao_Internalname, "SortedStatus", Ddo_contratoservicosvnc_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosvnc_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosvnc_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosvnc_descricao_Internalname, "SortedStatus", Ddo_contratoservicosvnc_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosvnc_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV58TFContratoServicosVnc_Descricao = Ddo_contratoservicosvnc_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV58TFContratoServicosVnc_Descricao", AV58TFContratoServicosVnc_Descricao);
            AV59TFContratoServicosVnc_Descricao_Sel = Ddo_contratoservicosvnc_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV59TFContratoServicosVnc_Descricao_Sel", AV59TFContratoServicosVnc_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E13GA2( )
      {
         /* Ddo_contratosrvvnc_dostatusdmn_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratosrvvnc_dostatusdmn_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratosrvvnc_dostatusdmn_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratosrvvnc_dostatusdmn_Internalname, "SortedStatus", Ddo_contratosrvvnc_dostatusdmn_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratosrvvnc_dostatusdmn_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratosrvvnc_dostatusdmn_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratosrvvnc_dostatusdmn_Internalname, "SortedStatus", Ddo_contratosrvvnc_dostatusdmn_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratosrvvnc_dostatusdmn_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV27TFContratoSrvVnc_DoStatusDmn_SelsJson = Ddo_contratosrvvnc_dostatusdmn_Selectedvalue_get;
            AV28TFContratoSrvVnc_DoStatusDmn_Sels.FromJSonString(AV27TFContratoSrvVnc_DoStatusDmn_SelsJson);
            subgrid_firstpage( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV28TFContratoSrvVnc_DoStatusDmn_Sels", AV28TFContratoSrvVnc_DoStatusDmn_Sels);
      }

      protected void E14GA2( )
      {
         /* Ddo_contratosrvvnc_statusdmn_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratosrvvnc_statusdmn_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratosrvvnc_statusdmn_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratosrvvnc_statusdmn_Internalname, "SortedStatus", Ddo_contratosrvvnc_statusdmn_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratosrvvnc_statusdmn_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratosrvvnc_statusdmn_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratosrvvnc_statusdmn_Internalname, "SortedStatus", Ddo_contratosrvvnc_statusdmn_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratosrvvnc_statusdmn_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV31TFContratoSrvVnc_StatusDmn_SelsJson = Ddo_contratosrvvnc_statusdmn_Selectedvalue_get;
            AV32TFContratoSrvVnc_StatusDmn_Sels.FromJSonString(AV31TFContratoSrvVnc_StatusDmn_SelsJson);
            subgrid_firstpage( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV32TFContratoSrvVnc_StatusDmn_Sels", AV32TFContratoSrvVnc_StatusDmn_Sels);
      }

      protected void E15GA2( )
      {
         /* Ddo_contratosrvvnc_servicovncsigla_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratosrvvnc_servicovncsigla_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratosrvvnc_servicovncsigla_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratosrvvnc_servicovncsigla_Internalname, "SortedStatus", Ddo_contratosrvvnc_servicovncsigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratosrvvnc_servicovncsigla_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratosrvvnc_servicovncsigla_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratosrvvnc_servicovncsigla_Internalname, "SortedStatus", Ddo_contratosrvvnc_servicovncsigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratosrvvnc_servicovncsigla_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV35TFContratoSrvVnc_ServicoVncSigla = Ddo_contratosrvvnc_servicovncsigla_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35TFContratoSrvVnc_ServicoVncSigla", AV35TFContratoSrvVnc_ServicoVncSigla);
            AV36TFContratoSrvVnc_ServicoVncSigla_Sel = Ddo_contratosrvvnc_servicovncsigla_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36TFContratoSrvVnc_ServicoVncSigla_Sel", AV36TFContratoSrvVnc_ServicoVncSigla_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E16GA2( )
      {
         /* Ddo_contratosrvvnc_srvvncstatus_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratosrvvnc_srvvncstatus_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratosrvvnc_srvvncstatus_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratosrvvnc_srvvncstatus_Internalname, "SortedStatus", Ddo_contratosrvvnc_srvvncstatus_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratosrvvnc_srvvncstatus_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratosrvvnc_srvvncstatus_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratosrvvnc_srvvncstatus_Internalname, "SortedStatus", Ddo_contratosrvvnc_srvvncstatus_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratosrvvnc_srvvncstatus_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV39TFContratoSrvVnc_SrvVncStatus_SelsJson = Ddo_contratosrvvnc_srvvncstatus_Selectedvalue_get;
            AV40TFContratoSrvVnc_SrvVncStatus_Sels.FromJSonString(AV39TFContratoSrvVnc_SrvVncStatus_SelsJson);
            subgrid_firstpage( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV40TFContratoSrvVnc_SrvVncStatus_Sels", AV40TFContratoSrvVnc_SrvVncStatus_Sels);
      }

      protected void E17GA2( )
      {
         /* Ddo_contratosrvvnc_srvvnccntnum_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratosrvvnc_srvvnccntnum_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratosrvvnc_srvvnccntnum_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratosrvvnc_srvvnccntnum_Internalname, "SortedStatus", Ddo_contratosrvvnc_srvvnccntnum_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratosrvvnc_srvvnccntnum_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratosrvvnc_srvvnccntnum_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratosrvvnc_srvvnccntnum_Internalname, "SortedStatus", Ddo_contratosrvvnc_srvvnccntnum_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratosrvvnc_srvvnccntnum_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV43TFContratoSrvVnc_SrvVncCntNum = Ddo_contratosrvvnc_srvvnccntnum_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFContratoSrvVnc_SrvVncCntNum", AV43TFContratoSrvVnc_SrvVncCntNum);
            AV44TFContratoSrvVnc_SrvVncCntNum_Sel = Ddo_contratosrvvnc_srvvnccntnum_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44TFContratoSrvVnc_SrvVncCntNum_Sel", AV44TFContratoSrvVnc_SrvVncCntNum_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E18GA2( )
      {
         /* Ddo_contratosrvvnc_semcusto_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratosrvvnc_semcusto_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratosrvvnc_semcusto_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratosrvvnc_semcusto_Internalname, "SortedStatus", Ddo_contratosrvvnc_semcusto_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratosrvvnc_semcusto_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratosrvvnc_semcusto_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratosrvvnc_semcusto_Internalname, "SortedStatus", Ddo_contratosrvvnc_semcusto_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratosrvvnc_semcusto_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV47TFContratoSrvVnc_SemCusto_Sel = (short)(NumberUtil.Val( Ddo_contratosrvvnc_semcusto_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV47TFContratoSrvVnc_SemCusto_Sel", StringUtil.Str( (decimal)(AV47TFContratoSrvVnc_SemCusto_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
      }

      protected void E19GA2( )
      {
         /* Ddo_contratoservicosvnc_ativo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosvnc_ativo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosvnc_ativo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosvnc_ativo_Internalname, "SortedStatus", Ddo_contratoservicosvnc_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosvnc_ativo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosvnc_ativo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosvnc_ativo_Internalname, "SortedStatus", Ddo_contratoservicosvnc_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosvnc_ativo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV50TFContratoServicosVnc_Ativo_Sel = (short)(NumberUtil.Val( Ddo_contratoservicosvnc_ativo_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV50TFContratoServicosVnc_Ativo_Sel", StringUtil.Str( (decimal)(AV50TFContratoServicosVnc_Ativo_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
      }

      private void E26GA2( )
      {
         /* Grid_Load Routine */
         AV15Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV15Update);
         AV65Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("contratoservicosvnc.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A917ContratoSrvVnc_Codigo) + "," + UrlEncode("" +AV18ContratoSrvVnc_CntSrvCod);
         AV16Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV16Delete);
         AV66Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("contratoservicosvnc.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A917ContratoSrvVnc_Codigo) + "," + UrlEncode("" +AV18ContratoSrvVnc_CntSrvCod);
         if ( A1453ContratoServicosVnc_Ativo )
         {
            AV56Color = GXUtil.RGB( 0, 0, 0);
         }
         else
         {
            AV56Color = GXUtil.RGB( 255, 0, 0);
         }
         cmbContratoSrvVnc_DoStatusDmn.ForeColor = (int)(AV56Color);
         cmbContratoSrvVnc_StatusDmn.ForeColor = (int)(AV56Color);
         edtContratoSrvVnc_ServicoVncSigla_Forecolor = (int)(AV56Color);
         edtavPrestador_Forecolor = (int)(AV56Color);
         cmbContratoSrvVnc_SrvVncStatus.ForeColor = (int)(AV56Color);
         edtContratoSrvVnc_SrvVncCntNum_Forecolor = (int)(AV56Color);
         cmbContratoSrvVnc_SemCusto.ForeColor = (int)(AV56Color);
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A1092ContratoSrvVnc_PrestadoraPesNom)) )
         {
            AV25Prestador = "Criador";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavPrestador_Internalname, AV25Prestador);
         }
         else
         {
            AV25Prestador = A1092ContratoSrvVnc_PrestadoraPesNom;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavPrestador_Internalname, AV25Prestador);
         }
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 8;
         }
         sendrow_82( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_8_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(8, GridRow);
         }
      }

      protected void E20GA2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("contratoservicosvnc.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +AV18ContratoSrvVnc_CntSrvCod);
         context.wjLocDisableFrm = 1;
      }

      protected void E21GA2( )
      {
         /* 'DoClonar' Routine */
         new prc_clonarregra(context ).execute(  AV18ContratoSrvVnc_CntSrvCod,  AV21Regra) ;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ContratoSrvVnc_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ContratoSrvVnc_CntSrvCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21Regra", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Regra), 6, 0)));
         tblTblclonar_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, tblTblclonar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblclonar_Visible), 5, 0)));
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void S162( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contratoservicosvnc_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosvnc_descricao_Internalname, "SortedStatus", Ddo_contratoservicosvnc_descricao_Sortedstatus);
         Ddo_contratosrvvnc_dostatusdmn_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratosrvvnc_dostatusdmn_Internalname, "SortedStatus", Ddo_contratosrvvnc_dostatusdmn_Sortedstatus);
         Ddo_contratosrvvnc_statusdmn_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratosrvvnc_statusdmn_Internalname, "SortedStatus", Ddo_contratosrvvnc_statusdmn_Sortedstatus);
         Ddo_contratosrvvnc_servicovncsigla_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratosrvvnc_servicovncsigla_Internalname, "SortedStatus", Ddo_contratosrvvnc_servicovncsigla_Sortedstatus);
         Ddo_contratosrvvnc_srvvncstatus_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratosrvvnc_srvvncstatus_Internalname, "SortedStatus", Ddo_contratosrvvnc_srvvncstatus_Sortedstatus);
         Ddo_contratosrvvnc_srvvnccntnum_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratosrvvnc_srvvnccntnum_Internalname, "SortedStatus", Ddo_contratosrvvnc_srvvnccntnum_Sortedstatus);
         Ddo_contratosrvvnc_semcusto_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratosrvvnc_semcusto_Internalname, "SortedStatus", Ddo_contratosrvvnc_semcusto_Sortedstatus);
         Ddo_contratoservicosvnc_ativo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosvnc_ativo_Internalname, "SortedStatus", Ddo_contratoservicosvnc_ativo_Sortedstatus);
      }

      protected void S132( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_contratoservicosvnc_descricao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosvnc_descricao_Internalname, "SortedStatus", Ddo_contratoservicosvnc_descricao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_contratosrvvnc_dostatusdmn_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratosrvvnc_dostatusdmn_Internalname, "SortedStatus", Ddo_contratosrvvnc_dostatusdmn_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_contratosrvvnc_statusdmn_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratosrvvnc_statusdmn_Internalname, "SortedStatus", Ddo_contratosrvvnc_statusdmn_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_contratosrvvnc_servicovncsigla_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratosrvvnc_servicovncsigla_Internalname, "SortedStatus", Ddo_contratosrvvnc_servicovncsigla_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_contratosrvvnc_srvvncstatus_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratosrvvnc_srvvncstatus_Internalname, "SortedStatus", Ddo_contratosrvvnc_srvvncstatus_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_contratosrvvnc_srvvnccntnum_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratosrvvnc_srvvnccntnum_Internalname, "SortedStatus", Ddo_contratosrvvnc_srvvnccntnum_Sortedstatus);
         }
         else if ( AV13OrderedBy == 7 )
         {
            Ddo_contratosrvvnc_semcusto_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratosrvvnc_semcusto_Internalname, "SortedStatus", Ddo_contratosrvvnc_semcusto_Sortedstatus);
         }
         else if ( AV13OrderedBy == 8 )
         {
            Ddo_contratoservicosvnc_ativo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosvnc_ativo_Internalname, "SortedStatus", Ddo_contratoservicosvnc_ativo_Sortedstatus);
         }
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV17Session.Get(AV67Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV67Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV17Session.Get(AV67Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV68GXV1 = 1;
         while ( AV68GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV68GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSVNC_DESCRICAO") == 0 )
            {
               AV58TFContratoServicosVnc_Descricao = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV58TFContratoServicosVnc_Descricao", AV58TFContratoServicosVnc_Descricao);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58TFContratoServicosVnc_Descricao)) )
               {
                  Ddo_contratoservicosvnc_descricao_Filteredtext_set = AV58TFContratoServicosVnc_Descricao;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosvnc_descricao_Internalname, "FilteredText_set", Ddo_contratoservicosvnc_descricao_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSVNC_DESCRICAO_SEL") == 0 )
            {
               AV59TFContratoServicosVnc_Descricao_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV59TFContratoServicosVnc_Descricao_Sel", AV59TFContratoServicosVnc_Descricao_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59TFContratoServicosVnc_Descricao_Sel)) )
               {
                  Ddo_contratoservicosvnc_descricao_Selectedvalue_set = AV59TFContratoServicosVnc_Descricao_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosvnc_descricao_Internalname, "SelectedValue_set", Ddo_contratoservicosvnc_descricao_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOSRVVNC_DOSTATUSDMN_SEL") == 0 )
            {
               AV27TFContratoSrvVnc_DoStatusDmn_SelsJson = AV12GridStateFilterValue.gxTpr_Value;
               AV28TFContratoSrvVnc_DoStatusDmn_Sels.FromJSonString(AV27TFContratoSrvVnc_DoStatusDmn_SelsJson);
               if ( ! ( AV28TFContratoSrvVnc_DoStatusDmn_Sels.Count == 0 ) )
               {
                  Ddo_contratosrvvnc_dostatusdmn_Selectedvalue_set = AV27TFContratoSrvVnc_DoStatusDmn_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratosrvvnc_dostatusdmn_Internalname, "SelectedValue_set", Ddo_contratosrvvnc_dostatusdmn_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOSRVVNC_STATUSDMN_SEL") == 0 )
            {
               AV31TFContratoSrvVnc_StatusDmn_SelsJson = AV12GridStateFilterValue.gxTpr_Value;
               AV32TFContratoSrvVnc_StatusDmn_Sels.FromJSonString(AV31TFContratoSrvVnc_StatusDmn_SelsJson);
               if ( ! ( AV32TFContratoSrvVnc_StatusDmn_Sels.Count == 0 ) )
               {
                  Ddo_contratosrvvnc_statusdmn_Selectedvalue_set = AV31TFContratoSrvVnc_StatusDmn_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratosrvvnc_statusdmn_Internalname, "SelectedValue_set", Ddo_contratosrvvnc_statusdmn_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOSRVVNC_SERVICOVNCSIGLA") == 0 )
            {
               AV35TFContratoSrvVnc_ServicoVncSigla = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35TFContratoSrvVnc_ServicoVncSigla", AV35TFContratoSrvVnc_ServicoVncSigla);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TFContratoSrvVnc_ServicoVncSigla)) )
               {
                  Ddo_contratosrvvnc_servicovncsigla_Filteredtext_set = AV35TFContratoSrvVnc_ServicoVncSigla;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratosrvvnc_servicovncsigla_Internalname, "FilteredText_set", Ddo_contratosrvvnc_servicovncsigla_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL") == 0 )
            {
               AV36TFContratoSrvVnc_ServicoVncSigla_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36TFContratoSrvVnc_ServicoVncSigla_Sel", AV36TFContratoSrvVnc_ServicoVncSigla_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TFContratoSrvVnc_ServicoVncSigla_Sel)) )
               {
                  Ddo_contratosrvvnc_servicovncsigla_Selectedvalue_set = AV36TFContratoSrvVnc_ServicoVncSigla_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratosrvvnc_servicovncsigla_Internalname, "SelectedValue_set", Ddo_contratosrvvnc_servicovncsigla_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOSRVVNC_SRVVNCSTATUS_SEL") == 0 )
            {
               AV39TFContratoSrvVnc_SrvVncStatus_SelsJson = AV12GridStateFilterValue.gxTpr_Value;
               AV40TFContratoSrvVnc_SrvVncStatus_Sels.FromJSonString(AV39TFContratoSrvVnc_SrvVncStatus_SelsJson);
               if ( ! ( AV40TFContratoSrvVnc_SrvVncStatus_Sels.Count == 0 ) )
               {
                  Ddo_contratosrvvnc_srvvncstatus_Selectedvalue_set = AV39TFContratoSrvVnc_SrvVncStatus_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratosrvvnc_srvvncstatus_Internalname, "SelectedValue_set", Ddo_contratosrvvnc_srvvncstatus_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOSRVVNC_SRVVNCCNTNUM") == 0 )
            {
               AV43TFContratoSrvVnc_SrvVncCntNum = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFContratoSrvVnc_SrvVncCntNum", AV43TFContratoSrvVnc_SrvVncCntNum);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFContratoSrvVnc_SrvVncCntNum)) )
               {
                  Ddo_contratosrvvnc_srvvnccntnum_Filteredtext_set = AV43TFContratoSrvVnc_SrvVncCntNum;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratosrvvnc_srvvnccntnum_Internalname, "FilteredText_set", Ddo_contratosrvvnc_srvvnccntnum_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOSRVVNC_SRVVNCCNTNUM_SEL") == 0 )
            {
               AV44TFContratoSrvVnc_SrvVncCntNum_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44TFContratoSrvVnc_SrvVncCntNum_Sel", AV44TFContratoSrvVnc_SrvVncCntNum_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFContratoSrvVnc_SrvVncCntNum_Sel)) )
               {
                  Ddo_contratosrvvnc_srvvnccntnum_Selectedvalue_set = AV44TFContratoSrvVnc_SrvVncCntNum_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratosrvvnc_srvvnccntnum_Internalname, "SelectedValue_set", Ddo_contratosrvvnc_srvvnccntnum_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOSRVVNC_SEMCUSTO_SEL") == 0 )
            {
               AV47TFContratoSrvVnc_SemCusto_Sel = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV47TFContratoSrvVnc_SemCusto_Sel", StringUtil.Str( (decimal)(AV47TFContratoSrvVnc_SemCusto_Sel), 1, 0));
               if ( ! (0==AV47TFContratoSrvVnc_SemCusto_Sel) )
               {
                  Ddo_contratosrvvnc_semcusto_Selectedvalue_set = StringUtil.Str( (decimal)(AV47TFContratoSrvVnc_SemCusto_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratosrvvnc_semcusto_Internalname, "SelectedValue_set", Ddo_contratosrvvnc_semcusto_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSVNC_ATIVO_SEL") == 0 )
            {
               AV50TFContratoServicosVnc_Ativo_Sel = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV50TFContratoServicosVnc_Ativo_Sel", StringUtil.Str( (decimal)(AV50TFContratoServicosVnc_Ativo_Sel), 1, 0));
               if ( ! (0==AV50TFContratoServicosVnc_Ativo_Sel) )
               {
                  Ddo_contratoservicosvnc_ativo_Selectedvalue_set = StringUtil.Str( (decimal)(AV50TFContratoServicosVnc_Ativo_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosvnc_ativo_Internalname, "SelectedValue_set", Ddo_contratoservicosvnc_ativo_Selectedvalue_set);
               }
            }
            AV68GXV1 = (int)(AV68GXV1+1);
         }
      }

      protected void S142( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV17Session.Get(AV67Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58TFContratoServicosVnc_Descricao)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSVNC_DESCRICAO";
            AV12GridStateFilterValue.gxTpr_Value = AV58TFContratoServicosVnc_Descricao;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59TFContratoServicosVnc_Descricao_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSVNC_DESCRICAO_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV59TFContratoServicosVnc_Descricao_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( AV28TFContratoSrvVnc_DoStatusDmn_Sels.Count == 0 ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSRVVNC_DOSTATUSDMN_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV28TFContratoSrvVnc_DoStatusDmn_Sels.ToJSonString(false);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( AV32TFContratoSrvVnc_StatusDmn_Sels.Count == 0 ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSRVVNC_STATUSDMN_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV32TFContratoSrvVnc_StatusDmn_Sels.ToJSonString(false);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TFContratoSrvVnc_ServicoVncSigla)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSRVVNC_SERVICOVNCSIGLA";
            AV12GridStateFilterValue.gxTpr_Value = AV35TFContratoSrvVnc_ServicoVncSigla;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TFContratoSrvVnc_ServicoVncSigla_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV36TFContratoSrvVnc_ServicoVncSigla_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( AV40TFContratoSrvVnc_SrvVncStatus_Sels.Count == 0 ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSRVVNC_SRVVNCSTATUS_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV40TFContratoSrvVnc_SrvVncStatus_Sels.ToJSonString(false);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFContratoSrvVnc_SrvVncCntNum)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSRVVNC_SRVVNCCNTNUM";
            AV12GridStateFilterValue.gxTpr_Value = AV43TFContratoSrvVnc_SrvVncCntNum;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFContratoSrvVnc_SrvVncCntNum_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSRVVNC_SRVVNCCNTNUM_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV44TFContratoSrvVnc_SrvVncCntNum_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV47TFContratoSrvVnc_SemCusto_Sel) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSRVVNC_SEMCUSTO_SEL";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV47TFContratoSrvVnc_SemCusto_Sel), 1, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV50TFContratoServicosVnc_Ativo_Sel) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSVNC_ATIVO_SEL";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV50TFContratoServicosVnc_Ativo_Sel), 1, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV18ContratoSrvVnc_CntSrvCod) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "PARM_&CONTRATOSRVVNC_CNTSRVCOD";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV18ContratoSrvVnc_CntSrvCod), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV67Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV67Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "ContratoServicosVnc";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "ContratoSrvVnc_CntSrvCod";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV18ContratoSrvVnc_CntSrvCod), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV17Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void E22GA2( )
      {
         /* Areatrabalho_codigo_Click Routine */
         /* Execute user subroutine: 'CARREGARREGRA' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavRegra.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21Regra), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavRegra_Internalname, "Values", cmbavRegra.ToJavascriptSource());
      }

      protected void E23GA2( )
      {
         /* Servico_codigo_Click Routine */
         /* Execute user subroutine: 'CARREGARREGRA' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavRegra.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21Regra), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavRegra_Internalname, "Values", cmbavRegra.ToJavascriptSource());
      }

      protected void S152( )
      {
         /* 'CARREGARREGRA' Routine */
         cmbavRegra.removeAllItems();
         cmbavRegra.addItem("0", "(Nenhuma)", 0);
         pr_default.dynParam(5, new Object[]{ new Object[]{
                                              AV19AreaTrabalho_Codigo ,
                                              AV20Servico_Codigo ,
                                              A75Contrato_AreaTrabalhoCod ,
                                              A921ContratoSrvVnc_ServicoCod },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor H00GA11 */
         pr_default.execute(5, new Object[] {AV19AreaTrabalho_Codigo, AV20Servico_Codigo});
         while ( (pr_default.getStatus(5) != 101) )
         {
            A1589ContratoSrvVnc_SrvVncCntSrvCod = H00GA11_A1589ContratoSrvVnc_SrvVncCntSrvCod[0];
            n1589ContratoSrvVnc_SrvVncCntSrvCod = H00GA11_n1589ContratoSrvVnc_SrvVncCntSrvCod[0];
            A1628ContratoSrvVnc_SrvVncCntCod = H00GA11_A1628ContratoSrvVnc_SrvVncCntCod[0];
            n1628ContratoSrvVnc_SrvVncCntCod = H00GA11_n1628ContratoSrvVnc_SrvVncCntCod[0];
            A923ContratoSrvVnc_ServicoVncCod = H00GA11_A923ContratoSrvVnc_ServicoVncCod[0];
            n923ContratoSrvVnc_ServicoVncCod = H00GA11_n923ContratoSrvVnc_ServicoVncCod[0];
            A915ContratoSrvVnc_CntSrvCod = H00GA11_A915ContratoSrvVnc_CntSrvCod[0];
            A921ContratoSrvVnc_ServicoCod = H00GA11_A921ContratoSrvVnc_ServicoCod[0];
            n921ContratoSrvVnc_ServicoCod = H00GA11_n921ContratoSrvVnc_ServicoCod[0];
            A75Contrato_AreaTrabalhoCod = H00GA11_A75Contrato_AreaTrabalhoCod[0];
            n75Contrato_AreaTrabalhoCod = H00GA11_n75Contrato_AreaTrabalhoCod[0];
            A1084ContratoSrvVnc_StatusDmn = H00GA11_A1084ContratoSrvVnc_StatusDmn[0];
            n1084ContratoSrvVnc_StatusDmn = H00GA11_n1084ContratoSrvVnc_StatusDmn[0];
            A1800ContratoSrvVnc_DoStatusDmn = H00GA11_A1800ContratoSrvVnc_DoStatusDmn[0];
            n1800ContratoSrvVnc_DoStatusDmn = H00GA11_n1800ContratoSrvVnc_DoStatusDmn[0];
            A924ContratoSrvVnc_ServicoVncSigla = H00GA11_A924ContratoSrvVnc_ServicoVncSigla[0];
            n924ContratoSrvVnc_ServicoVncSigla = H00GA11_n924ContratoSrvVnc_ServicoVncSigla[0];
            A917ContratoSrvVnc_Codigo = H00GA11_A917ContratoSrvVnc_Codigo[0];
            A1628ContratoSrvVnc_SrvVncCntCod = H00GA11_A1628ContratoSrvVnc_SrvVncCntCod[0];
            n1628ContratoSrvVnc_SrvVncCntCod = H00GA11_n1628ContratoSrvVnc_SrvVncCntCod[0];
            A923ContratoSrvVnc_ServicoVncCod = H00GA11_A923ContratoSrvVnc_ServicoVncCod[0];
            n923ContratoSrvVnc_ServicoVncCod = H00GA11_n923ContratoSrvVnc_ServicoVncCod[0];
            A75Contrato_AreaTrabalhoCod = H00GA11_A75Contrato_AreaTrabalhoCod[0];
            n75Contrato_AreaTrabalhoCod = H00GA11_n75Contrato_AreaTrabalhoCod[0];
            A924ContratoSrvVnc_ServicoVncSigla = H00GA11_A924ContratoSrvVnc_ServicoVncSigla[0];
            n924ContratoSrvVnc_ServicoVncSigla = H00GA11_n924ContratoSrvVnc_ServicoVncSigla[0];
            A921ContratoSrvVnc_ServicoCod = H00GA11_A921ContratoSrvVnc_ServicoCod[0];
            n921ContratoSrvVnc_ServicoCod = H00GA11_n921ContratoSrvVnc_ServicoCod[0];
            cmbavRegra.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A917ContratoSrvVnc_Codigo), 6, 0)), StringUtil.Trim( A924ContratoSrvVnc_ServicoVncSigla)+" ("+(H00GA11_n1800ContratoSrvVnc_DoStatusDmn[0] ? "" : gxdomainstatusdemanda.getDescription(context,A1800ContratoSrvVnc_DoStatusDmn)+"->")+gxdomainstatusdemanda.getDescription(context,A1084ContratoSrvVnc_StatusDmn)+")", 0);
            pr_default.readNext(5);
         }
         pr_default.close(5);
      }

      protected void wb_table1_2_GA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table2_5_GA2( true) ;
         }
         else
         {
            wb_table2_5_GA2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_GA2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table3_28_GA2( true) ;
         }
         else
         {
            wb_table3_28_GA2( false) ;
         }
         return  ;
      }

      protected void wb_table3_28_GA2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_GA2e( true) ;
         }
         else
         {
            wb_table1_2_GA2e( false) ;
         }
      }

      protected void wb_table3_28_GA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable2_Internalname, tblUnnamedtable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgInsert_Visible, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicos_VncWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgClonarregra_Internalname, context.GetImagePath( "550f386a-e6ac-4748-b314-ee8e1f72ce4d", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Clonar regra de outro servi�o", 0, 0, 0, "px", 0, "px", 0, 0, 7, imgClonarregra_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+"e27ga1_client"+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicos_VncWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_37_GA2( true) ;
         }
         else
         {
            wb_table4_37_GA2( false) ;
         }
         return  ;
      }

      protected void wb_table4_37_GA2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_GA2e( true) ;
         }
         else
         {
            wb_table3_28_GA2e( false) ;
         }
      }

      protected void wb_table4_37_GA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTblclonar_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTblclonar_Internalname, tblTblclonar_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockareatrabalho_codigo_Internalname, "�rea de Trabalho:", "", "", lblTextblockareatrabalho_codigo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContratoServicos_VncWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavAreatrabalho_codigo, cmbavAreatrabalho_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV19AreaTrabalho_Codigo), 6, 0)), 1, cmbavAreatrabalho_codigo_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVAREATRABALHO_CODIGO.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"", "", true, "HLP_ContratoServicos_VncWC.htm");
            cmbavAreatrabalho_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV19AreaTrabalho_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavAreatrabalho_codigo_Internalname, "Values", (String)(cmbavAreatrabalho_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_codigo_Internalname, "Servi�o:", "", "", lblTextblockservico_codigo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContratoServicos_VncWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavServico_codigo, dynavServico_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV20Servico_Codigo), 6, 0)), 1, dynavServico_codigo_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVSERVICO_CODIGO.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", "", true, "HLP_ContratoServicos_VncWC.htm");
            dynavServico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20Servico_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavServico_codigo_Internalname, "Values", (String)(dynavServico_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockregra_Internalname, "Regra a ser clonada:", "", "", lblTextblockregra_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContratoServicos_VncWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavRegra, cmbavRegra_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV21Regra), 6, 0)), 1, cmbavRegra_Jsonclick, 7, "'"+sPrefix+"'"+",false,"+"'"+"e28ga1_client"+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", "", true, "HLP_ContratoServicos_VncWC.htm");
            cmbavRegra.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21Regra), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavRegra_Internalname, "Values", (String)(cmbavRegra.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnclonar_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(8), 1, 0)+","+"null"+");", "Clonar", bttBtnclonar_Jsonclick, 5, "Clonar", "", StyleString, ClassString, bttBtnclonar_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOCLONAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicos_VncWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_37_GA2e( true) ;
         }
         else
         {
            wb_table4_37_GA2e( false) ;
         }
      }

      protected void wb_table2_5_GA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"8\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "do Servi�o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "do Contrato") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "servi�o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosVnc_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosVnc_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosVnc_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContratoSrvVnc_DoStatusDmn_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContratoSrvVnc_DoStatusDmn.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContratoSrvVnc_DoStatusDmn.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContratoSrvVnc_StatusDmn_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContratoSrvVnc_StatusDmn.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContratoSrvVnc_StatusDmn.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoSrvVnc_ServicoVncSigla_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoSrvVnc_ServicoVncSigla_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoSrvVnc_ServicoVncSigla_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Para") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContratoSrvVnc_SrvVncStatus_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContratoSrvVnc_SrvVncStatus.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContratoSrvVnc_SrvVncStatus.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(110), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoSrvVnc_SrvVncCntNum_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoSrvVnc_SrvVncCntNum_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoSrvVnc_SrvVncCntNum_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContratoSrvVnc_SemCusto_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContratoSrvVnc_SemCusto.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContratoSrvVnc_SemCusto.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkContratoServicosVnc_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( chkContratoServicosVnc_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkContratoServicosVnc_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV15Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV16Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A917ContratoSrvVnc_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A933ContratoSrvVnc_ContratoCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A915ContratoSrvVnc_CntSrvCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A923ContratoSrvVnc_ServicoVncCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A2108ContratoServicosVnc_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosVnc_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosVnc_Descricao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1800ContratoSrvVnc_DoStatusDmn));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContratoSrvVnc_DoStatusDmn.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContratoSrvVnc_DoStatusDmn_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContratoSrvVnc_DoStatusDmn.ForeColor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1084ContratoSrvVnc_StatusDmn));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContratoSrvVnc_StatusDmn.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContratoSrvVnc_StatusDmn_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContratoSrvVnc_StatusDmn.ForeColor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A924ContratoSrvVnc_ServicoVncSigla));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoSrvVnc_ServicoVncSigla_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoSrvVnc_ServicoVncSigla_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoSrvVnc_ServicoVncSigla_Forecolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV25Prestador));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavPrestador_Forecolor), 9, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavPrestador_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1663ContratoSrvVnc_SrvVncStatus));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContratoSrvVnc_SrvVncStatus.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContratoSrvVnc_SrvVncStatus_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContratoSrvVnc_SrvVncStatus.ForeColor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1631ContratoSrvVnc_SrvVncCntNum));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoSrvVnc_SrvVncCntNum_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoSrvVnc_SrvVncCntNum_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoSrvVnc_SrvVncCntNum_Forecolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A1090ContratoSrvVnc_SemCusto));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContratoSrvVnc_SemCusto.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContratoSrvVnc_SemCusto_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContratoSrvVnc_SemCusto.ForeColor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A1453ContratoServicosVnc_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkContratoServicosVnc_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkContratoServicosVnc_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 8 )
         {
            wbEnd = 0;
            nRC_GXsfl_8 = (short)(nGXsfl_8_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_GA2e( true) ;
         }
         else
         {
            wb_table2_5_GA2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV18ContratoSrvVnc_CntSrvCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ContratoSrvVnc_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ContratoSrvVnc_CntSrvCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAGA2( ) ;
         WSGA2( ) ;
         WEGA2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV18ContratoSrvVnc_CntSrvCod = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAGA2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contratoservicos_vncwc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAGA2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV18ContratoSrvVnc_CntSrvCod = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ContratoSrvVnc_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ContratoSrvVnc_CntSrvCod), 6, 0)));
         }
         wcpOAV18ContratoSrvVnc_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV18ContratoSrvVnc_CntSrvCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV18ContratoSrvVnc_CntSrvCod != wcpOAV18ContratoSrvVnc_CntSrvCod ) ) )
         {
            setjustcreated();
         }
         wcpOAV18ContratoSrvVnc_CntSrvCod = AV18ContratoSrvVnc_CntSrvCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV18ContratoSrvVnc_CntSrvCod = cgiGet( sPrefix+"AV18ContratoSrvVnc_CntSrvCod_CTRL");
         if ( StringUtil.Len( sCtrlAV18ContratoSrvVnc_CntSrvCod) > 0 )
         {
            AV18ContratoSrvVnc_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( sCtrlAV18ContratoSrvVnc_CntSrvCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ContratoSrvVnc_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ContratoSrvVnc_CntSrvCod), 6, 0)));
         }
         else
         {
            AV18ContratoSrvVnc_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV18ContratoSrvVnc_CntSrvCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAGA2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSGA2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSGA2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV18ContratoSrvVnc_CntSrvCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18ContratoSrvVnc_CntSrvCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV18ContratoSrvVnc_CntSrvCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV18ContratoSrvVnc_CntSrvCod_CTRL", StringUtil.RTrim( sCtrlAV18ContratoSrvVnc_CntSrvCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEGA2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205299305489");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contratoservicos_vncwc.js", "?20205299305489");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_82( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_8_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_8_idx;
         edtContratoSrvVnc_Codigo_Internalname = sPrefix+"CONTRATOSRVVNC_CODIGO_"+sGXsfl_8_idx;
         edtContratoSrvVnc_ContratoCod_Internalname = sPrefix+"CONTRATOSRVVNC_CONTRATOCOD_"+sGXsfl_8_idx;
         edtContratoSrvVnc_CntSrvCod_Internalname = sPrefix+"CONTRATOSRVVNC_CNTSRVCOD_"+sGXsfl_8_idx;
         edtContratoSrvVnc_ServicoVncCod_Internalname = sPrefix+"CONTRATOSRVVNC_SERVICOVNCCOD_"+sGXsfl_8_idx;
         edtContratoServicosVnc_Descricao_Internalname = sPrefix+"CONTRATOSERVICOSVNC_DESCRICAO_"+sGXsfl_8_idx;
         cmbContratoSrvVnc_DoStatusDmn_Internalname = sPrefix+"CONTRATOSRVVNC_DOSTATUSDMN_"+sGXsfl_8_idx;
         cmbContratoSrvVnc_StatusDmn_Internalname = sPrefix+"CONTRATOSRVVNC_STATUSDMN_"+sGXsfl_8_idx;
         edtContratoSrvVnc_ServicoVncSigla_Internalname = sPrefix+"CONTRATOSRVVNC_SERVICOVNCSIGLA_"+sGXsfl_8_idx;
         edtavPrestador_Internalname = sPrefix+"vPRESTADOR_"+sGXsfl_8_idx;
         cmbContratoSrvVnc_SrvVncStatus_Internalname = sPrefix+"CONTRATOSRVVNC_SRVVNCSTATUS_"+sGXsfl_8_idx;
         edtContratoSrvVnc_SrvVncCntNum_Internalname = sPrefix+"CONTRATOSRVVNC_SRVVNCCNTNUM_"+sGXsfl_8_idx;
         cmbContratoSrvVnc_SemCusto_Internalname = sPrefix+"CONTRATOSRVVNC_SEMCUSTO_"+sGXsfl_8_idx;
         chkContratoServicosVnc_Ativo_Internalname = sPrefix+"CONTRATOSERVICOSVNC_ATIVO_"+sGXsfl_8_idx;
      }

      protected void SubsflControlProps_fel_82( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_8_fel_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_8_fel_idx;
         edtContratoSrvVnc_Codigo_Internalname = sPrefix+"CONTRATOSRVVNC_CODIGO_"+sGXsfl_8_fel_idx;
         edtContratoSrvVnc_ContratoCod_Internalname = sPrefix+"CONTRATOSRVVNC_CONTRATOCOD_"+sGXsfl_8_fel_idx;
         edtContratoSrvVnc_CntSrvCod_Internalname = sPrefix+"CONTRATOSRVVNC_CNTSRVCOD_"+sGXsfl_8_fel_idx;
         edtContratoSrvVnc_ServicoVncCod_Internalname = sPrefix+"CONTRATOSRVVNC_SERVICOVNCCOD_"+sGXsfl_8_fel_idx;
         edtContratoServicosVnc_Descricao_Internalname = sPrefix+"CONTRATOSERVICOSVNC_DESCRICAO_"+sGXsfl_8_fel_idx;
         cmbContratoSrvVnc_DoStatusDmn_Internalname = sPrefix+"CONTRATOSRVVNC_DOSTATUSDMN_"+sGXsfl_8_fel_idx;
         cmbContratoSrvVnc_StatusDmn_Internalname = sPrefix+"CONTRATOSRVVNC_STATUSDMN_"+sGXsfl_8_fel_idx;
         edtContratoSrvVnc_ServicoVncSigla_Internalname = sPrefix+"CONTRATOSRVVNC_SERVICOVNCSIGLA_"+sGXsfl_8_fel_idx;
         edtavPrestador_Internalname = sPrefix+"vPRESTADOR_"+sGXsfl_8_fel_idx;
         cmbContratoSrvVnc_SrvVncStatus_Internalname = sPrefix+"CONTRATOSRVVNC_SRVVNCSTATUS_"+sGXsfl_8_fel_idx;
         edtContratoSrvVnc_SrvVncCntNum_Internalname = sPrefix+"CONTRATOSRVVNC_SRVVNCCNTNUM_"+sGXsfl_8_fel_idx;
         cmbContratoSrvVnc_SemCusto_Internalname = sPrefix+"CONTRATOSRVVNC_SEMCUSTO_"+sGXsfl_8_fel_idx;
         chkContratoServicosVnc_Ativo_Internalname = sPrefix+"CONTRATOSERVICOSVNC_ATIVO_"+sGXsfl_8_fel_idx;
      }

      protected void sendrow_82( )
      {
         SubsflControlProps_82( ) ;
         WBGA0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_8_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_8_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_8_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV15Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV15Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV65Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV15Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV15Update)) ? AV65Update_GXI : context.PathToRelativeUrl( AV15Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavUpdate_Visible,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV15Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV16Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV16Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV66Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV16Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV16Delete)) ? AV66Delete_GXI : context.PathToRelativeUrl( AV16Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDelete_Visible,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV16Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoSrvVnc_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A917ContratoSrvVnc_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A917ContratoSrvVnc_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoSrvVnc_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoSrvVnc_ContratoCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A933ContratoSrvVnc_ContratoCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A933ContratoSrvVnc_ContratoCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoSrvVnc_ContratoCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoSrvVnc_CntSrvCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A915ContratoSrvVnc_CntSrvCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A915ContratoSrvVnc_CntSrvCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoSrvVnc_CntSrvCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoSrvVnc_ServicoVncCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A923ContratoSrvVnc_ServicoVncCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A923ContratoSrvVnc_ServicoVncCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoSrvVnc_ServicoVncCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosVnc_Descricao_Internalname,(String)A2108ContratoServicosVnc_Descricao,StringUtil.RTrim( context.localUtil.Format( A2108ContratoServicosVnc_Descricao, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosVnc_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)-1,(bool)true,(String)"Descricao",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_8_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTRATOSRVVNC_DOSTATUSDMN_" + sGXsfl_8_idx;
               cmbContratoSrvVnc_DoStatusDmn.Name = GXCCtl;
               cmbContratoSrvVnc_DoStatusDmn.WebTags = "";
               cmbContratoSrvVnc_DoStatusDmn.addItem("", "(Qualquer um)", 0);
               cmbContratoSrvVnc_DoStatusDmn.addItem("B", "Stand by", 0);
               cmbContratoSrvVnc_DoStatusDmn.addItem("S", "Solicitada", 0);
               cmbContratoSrvVnc_DoStatusDmn.addItem("E", "Em An�lise", 0);
               cmbContratoSrvVnc_DoStatusDmn.addItem("A", "Em execu��o", 0);
               cmbContratoSrvVnc_DoStatusDmn.addItem("R", "Resolvida", 0);
               cmbContratoSrvVnc_DoStatusDmn.addItem("C", "Conferida", 0);
               cmbContratoSrvVnc_DoStatusDmn.addItem("D", "Retornada", 0);
               cmbContratoSrvVnc_DoStatusDmn.addItem("H", "Homologada", 0);
               cmbContratoSrvVnc_DoStatusDmn.addItem("O", "Aceite", 0);
               cmbContratoSrvVnc_DoStatusDmn.addItem("P", "A Pagar", 0);
               cmbContratoSrvVnc_DoStatusDmn.addItem("L", "Liquidada", 0);
               cmbContratoSrvVnc_DoStatusDmn.addItem("X", "Cancelada", 0);
               cmbContratoSrvVnc_DoStatusDmn.addItem("N", "N�o Faturada", 0);
               cmbContratoSrvVnc_DoStatusDmn.addItem("J", "Planejamento", 0);
               cmbContratoSrvVnc_DoStatusDmn.addItem("I", "An�lise Planejamento", 0);
               cmbContratoSrvVnc_DoStatusDmn.addItem("T", "Validacao T�cnica", 0);
               cmbContratoSrvVnc_DoStatusDmn.addItem("Q", "Validacao Qualidade", 0);
               cmbContratoSrvVnc_DoStatusDmn.addItem("G", "Em Homologa��o", 0);
               cmbContratoSrvVnc_DoStatusDmn.addItem("M", "Valida��o Mensura��o", 0);
               cmbContratoSrvVnc_DoStatusDmn.addItem("U", "Rascunho", 0);
               if ( cmbContratoSrvVnc_DoStatusDmn.ItemCount > 0 )
               {
                  A1800ContratoSrvVnc_DoStatusDmn = cmbContratoSrvVnc_DoStatusDmn.getValidValue(A1800ContratoSrvVnc_DoStatusDmn);
                  n1800ContratoSrvVnc_DoStatusDmn = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContratoSrvVnc_DoStatusDmn,(String)cmbContratoSrvVnc_DoStatusDmn_Internalname,StringUtil.RTrim( A1800ContratoSrvVnc_DoStatusDmn),(short)1,(String)cmbContratoSrvVnc_DoStatusDmn_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px","color:"+context.BuildHTMLColor( cmbContratoSrvVnc_DoStatusDmn.ForeColor)+";",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContratoSrvVnc_DoStatusDmn.CurrentValue = StringUtil.RTrim( A1800ContratoSrvVnc_DoStatusDmn);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratoSrvVnc_DoStatusDmn_Internalname, "Values", (String)(cmbContratoSrvVnc_DoStatusDmn.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_8_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTRATOSRVVNC_STATUSDMN_" + sGXsfl_8_idx;
               cmbContratoSrvVnc_StatusDmn.Name = GXCCtl;
               cmbContratoSrvVnc_StatusDmn.WebTags = "";
               cmbContratoSrvVnc_StatusDmn.addItem("B", "Stand by", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("S", "Solicitada", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("E", "Em An�lise", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("A", "Em execu��o", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("R", "Resolvida", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("C", "Conferida", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("D", "Retornada", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("H", "Homologada", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("O", "Aceite", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("P", "A Pagar", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("L", "Liquidada", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("X", "Cancelada", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("N", "N�o Faturada", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("J", "Planejamento", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("I", "An�lise Planejamento", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("T", "Validacao T�cnica", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("Q", "Validacao Qualidade", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("G", "Em Homologa��o", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("M", "Valida��o Mensura��o", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("U", "Rascunho", 0);
               if ( cmbContratoSrvVnc_StatusDmn.ItemCount > 0 )
               {
                  A1084ContratoSrvVnc_StatusDmn = cmbContratoSrvVnc_StatusDmn.getValidValue(A1084ContratoSrvVnc_StatusDmn);
                  n1084ContratoSrvVnc_StatusDmn = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContratoSrvVnc_StatusDmn,(String)cmbContratoSrvVnc_StatusDmn_Internalname,StringUtil.RTrim( A1084ContratoSrvVnc_StatusDmn),(short)1,(String)cmbContratoSrvVnc_StatusDmn_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px","color:"+context.BuildHTMLColor( cmbContratoSrvVnc_StatusDmn.ForeColor)+";",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContratoSrvVnc_StatusDmn.CurrentValue = StringUtil.RTrim( A1084ContratoSrvVnc_StatusDmn);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratoSrvVnc_StatusDmn_Internalname, "Values", (String)(cmbContratoSrvVnc_StatusDmn.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoSrvVnc_ServicoVncSigla_Internalname,StringUtil.RTrim( A924ContratoSrvVnc_ServicoVncSigla),StringUtil.RTrim( context.localUtil.Format( A924ContratoSrvVnc_ServicoVncSigla, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoSrvVnc_ServicoVncSigla_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtContratoSrvVnc_ServicoVncSigla_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)-1,(bool)true,(String)"Sigla",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavPrestador_Internalname,StringUtil.RTrim( AV25Prestador),StringUtil.RTrim( context.localUtil.Format( AV25Prestador, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavPrestador_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtavPrestador_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(int)edtavPrestador_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_8_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTRATOSRVVNC_SRVVNCSTATUS_" + sGXsfl_8_idx;
               cmbContratoSrvVnc_SrvVncStatus.Name = GXCCtl;
               cmbContratoSrvVnc_SrvVncStatus.WebTags = "";
               cmbContratoSrvVnc_SrvVncStatus.addItem("B", "Stand by", 0);
               cmbContratoSrvVnc_SrvVncStatus.addItem("S", "Solicitada", 0);
               cmbContratoSrvVnc_SrvVncStatus.addItem("E", "Em An�lise", 0);
               cmbContratoSrvVnc_SrvVncStatus.addItem("A", "Em execu��o", 0);
               cmbContratoSrvVnc_SrvVncStatus.addItem("R", "Resolvida", 0);
               cmbContratoSrvVnc_SrvVncStatus.addItem("C", "Conferida", 0);
               cmbContratoSrvVnc_SrvVncStatus.addItem("D", "Retornada", 0);
               cmbContratoSrvVnc_SrvVncStatus.addItem("H", "Homologada", 0);
               cmbContratoSrvVnc_SrvVncStatus.addItem("O", "Aceite", 0);
               cmbContratoSrvVnc_SrvVncStatus.addItem("P", "A Pagar", 0);
               cmbContratoSrvVnc_SrvVncStatus.addItem("L", "Liquidada", 0);
               cmbContratoSrvVnc_SrvVncStatus.addItem("X", "Cancelada", 0);
               cmbContratoSrvVnc_SrvVncStatus.addItem("N", "N�o Faturada", 0);
               cmbContratoSrvVnc_SrvVncStatus.addItem("J", "Planejamento", 0);
               cmbContratoSrvVnc_SrvVncStatus.addItem("I", "An�lise Planejamento", 0);
               cmbContratoSrvVnc_SrvVncStatus.addItem("T", "Validacao T�cnica", 0);
               cmbContratoSrvVnc_SrvVncStatus.addItem("Q", "Validacao Qualidade", 0);
               cmbContratoSrvVnc_SrvVncStatus.addItem("G", "Em Homologa��o", 0);
               cmbContratoSrvVnc_SrvVncStatus.addItem("M", "Valida��o Mensura��o", 0);
               cmbContratoSrvVnc_SrvVncStatus.addItem("U", "Rascunho", 0);
               if ( cmbContratoSrvVnc_SrvVncStatus.ItemCount > 0 )
               {
                  A1663ContratoSrvVnc_SrvVncStatus = cmbContratoSrvVnc_SrvVncStatus.getValidValue(A1663ContratoSrvVnc_SrvVncStatus);
                  n1663ContratoSrvVnc_SrvVncStatus = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContratoSrvVnc_SrvVncStatus,(String)cmbContratoSrvVnc_SrvVncStatus_Internalname,StringUtil.RTrim( A1663ContratoSrvVnc_SrvVncStatus),(short)1,(String)cmbContratoSrvVnc_SrvVncStatus_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px","color:"+context.BuildHTMLColor( cmbContratoSrvVnc_SrvVncStatus.ForeColor)+";",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContratoSrvVnc_SrvVncStatus.CurrentValue = StringUtil.RTrim( A1663ContratoSrvVnc_SrvVncStatus);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratoSrvVnc_SrvVncStatus_Internalname, "Values", (String)(cmbContratoSrvVnc_SrvVncStatus.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoSrvVnc_SrvVncCntNum_Internalname,StringUtil.RTrim( A1631ContratoSrvVnc_SrvVncCntNum),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoSrvVnc_SrvVncCntNum_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtContratoSrvVnc_SrvVncCntNum_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)110,(String)"px",(short)17,(String)"px",(short)20,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)-1,(bool)true,(String)"NumeroContrato",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_8_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTRATOSRVVNC_SEMCUSTO_" + sGXsfl_8_idx;
               cmbContratoSrvVnc_SemCusto.Name = GXCCtl;
               cmbContratoSrvVnc_SemCusto.WebTags = "";
               cmbContratoSrvVnc_SemCusto.addItem(StringUtil.BoolToStr( false), "N�o", 0);
               cmbContratoSrvVnc_SemCusto.addItem(StringUtil.BoolToStr( true), "Sim", 0);
               if ( cmbContratoSrvVnc_SemCusto.ItemCount > 0 )
               {
                  A1090ContratoSrvVnc_SemCusto = StringUtil.StrToBool( cmbContratoSrvVnc_SemCusto.getValidValue(StringUtil.BoolToStr( A1090ContratoSrvVnc_SemCusto)));
                  n1090ContratoSrvVnc_SemCusto = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContratoSrvVnc_SemCusto,(String)cmbContratoSrvVnc_SemCusto_Internalname,StringUtil.BoolToStr( A1090ContratoSrvVnc_SemCusto),(short)1,(String)cmbContratoSrvVnc_SemCusto_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"boolean",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px","color:"+context.BuildHTMLColor( cmbContratoSrvVnc_SemCusto.ForeColor)+";",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContratoSrvVnc_SemCusto.CurrentValue = StringUtil.BoolToStr( A1090ContratoSrvVnc_SemCusto);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratoSrvVnc_SemCusto_Internalname, "Values", (String)(cmbContratoSrvVnc_SemCusto.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkContratoServicosVnc_Ativo_Internalname,StringUtil.BoolToStr( A1453ContratoServicosVnc_Ativo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSRVVNC_CODIGO"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, context.localUtil.Format( (decimal)(A917ContratoSrvVnc_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSRVVNC_CNTSRVCOD"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, context.localUtil.Format( (decimal)(A915ContratoSrvVnc_CntSrvCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSVNC_DESCRICAO"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( A2108ContratoServicosVnc_Descricao, "@!"))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSRVVNC_DOSTATUSDMN"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( A1800ContratoSrvVnc_DoStatusDmn, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSRVVNC_STATUSDMN"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( A1084ContratoSrvVnc_StatusDmn, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSRVVNC_SRVVNCSTATUS"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( A1663ContratoSrvVnc_SrvVncStatus, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSRVVNC_SEMCUSTO"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, A1090ContratoSrvVnc_SemCusto));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSVNC_ATIVO"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, A1453ContratoServicosVnc_Ativo));
            GridContainer.AddRow(GridRow);
            nGXsfl_8_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_8_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_8_idx+1));
            sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
            SubsflControlProps_82( ) ;
         }
         /* End function sendrow_82 */
      }

      protected void init_default_properties( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE";
         edtavDelete_Internalname = sPrefix+"vDELETE";
         edtContratoSrvVnc_Codigo_Internalname = sPrefix+"CONTRATOSRVVNC_CODIGO";
         edtContratoSrvVnc_ContratoCod_Internalname = sPrefix+"CONTRATOSRVVNC_CONTRATOCOD";
         edtContratoSrvVnc_CntSrvCod_Internalname = sPrefix+"CONTRATOSRVVNC_CNTSRVCOD";
         edtContratoSrvVnc_ServicoVncCod_Internalname = sPrefix+"CONTRATOSRVVNC_SERVICOVNCCOD";
         edtContratoServicosVnc_Descricao_Internalname = sPrefix+"CONTRATOSERVICOSVNC_DESCRICAO";
         cmbContratoSrvVnc_DoStatusDmn_Internalname = sPrefix+"CONTRATOSRVVNC_DOSTATUSDMN";
         cmbContratoSrvVnc_StatusDmn_Internalname = sPrefix+"CONTRATOSRVVNC_STATUSDMN";
         edtContratoSrvVnc_ServicoVncSigla_Internalname = sPrefix+"CONTRATOSRVVNC_SERVICOVNCSIGLA";
         edtavPrestador_Internalname = sPrefix+"vPRESTADOR";
         cmbContratoSrvVnc_SrvVncStatus_Internalname = sPrefix+"CONTRATOSRVVNC_SRVVNCSTATUS";
         edtContratoSrvVnc_SrvVncCntNum_Internalname = sPrefix+"CONTRATOSRVVNC_SRVVNCCNTNUM";
         cmbContratoSrvVnc_SemCusto_Internalname = sPrefix+"CONTRATOSRVVNC_SEMCUSTO";
         chkContratoServicosVnc_Ativo_Internalname = sPrefix+"CONTRATOSERVICOSVNC_ATIVO";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         imgInsert_Internalname = sPrefix+"INSERT";
         imgClonarregra_Internalname = sPrefix+"CLONARREGRA";
         lblTextblockareatrabalho_codigo_Internalname = sPrefix+"TEXTBLOCKAREATRABALHO_CODIGO";
         cmbavAreatrabalho_codigo_Internalname = sPrefix+"vAREATRABALHO_CODIGO";
         lblTextblockservico_codigo_Internalname = sPrefix+"TEXTBLOCKSERVICO_CODIGO";
         dynavServico_codigo_Internalname = sPrefix+"vSERVICO_CODIGO";
         lblTextblockregra_Internalname = sPrefix+"TEXTBLOCKREGRA";
         cmbavRegra_Internalname = sPrefix+"vREGRA";
         bttBtnclonar_Internalname = sPrefix+"BTNCLONAR";
         tblTblclonar_Internalname = sPrefix+"TBLCLONAR";
         tblUnnamedtable2_Internalname = sPrefix+"UNNAMEDTABLE2";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         edtavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         edtavTfcontratoservicosvnc_descricao_Internalname = sPrefix+"vTFCONTRATOSERVICOSVNC_DESCRICAO";
         edtavTfcontratoservicosvnc_descricao_sel_Internalname = sPrefix+"vTFCONTRATOSERVICOSVNC_DESCRICAO_SEL";
         edtavTfcontratosrvvnc_servicovncsigla_Internalname = sPrefix+"vTFCONTRATOSRVVNC_SERVICOVNCSIGLA";
         edtavTfcontratosrvvnc_servicovncsigla_sel_Internalname = sPrefix+"vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL";
         edtavTfcontratosrvvnc_srvvnccntnum_Internalname = sPrefix+"vTFCONTRATOSRVVNC_SRVVNCCNTNUM";
         edtavTfcontratosrvvnc_srvvnccntnum_sel_Internalname = sPrefix+"vTFCONTRATOSRVVNC_SRVVNCCNTNUM_SEL";
         edtavTfcontratosrvvnc_semcusto_sel_Internalname = sPrefix+"vTFCONTRATOSRVVNC_SEMCUSTO_SEL";
         edtavTfcontratoservicosvnc_ativo_sel_Internalname = sPrefix+"vTFCONTRATOSERVICOSVNC_ATIVO_SEL";
         Ddo_contratoservicosvnc_descricao_Internalname = sPrefix+"DDO_CONTRATOSERVICOSVNC_DESCRICAO";
         edtavDdo_contratoservicosvnc_descricaotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOSERVICOSVNC_DESCRICAOTITLECONTROLIDTOREPLACE";
         Ddo_contratosrvvnc_dostatusdmn_Internalname = sPrefix+"DDO_CONTRATOSRVVNC_DOSTATUSDMN";
         edtavDdo_contratosrvvnc_dostatusdmntitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOSRVVNC_DOSTATUSDMNTITLECONTROLIDTOREPLACE";
         Ddo_contratosrvvnc_statusdmn_Internalname = sPrefix+"DDO_CONTRATOSRVVNC_STATUSDMN";
         edtavDdo_contratosrvvnc_statusdmntitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOSRVVNC_STATUSDMNTITLECONTROLIDTOREPLACE";
         Ddo_contratosrvvnc_servicovncsigla_Internalname = sPrefix+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA";
         edtavDdo_contratosrvvnc_servicovncsiglatitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOSRVVNC_SERVICOVNCSIGLATITLECONTROLIDTOREPLACE";
         Ddo_contratosrvvnc_srvvncstatus_Internalname = sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCSTATUS";
         edtavDdo_contratosrvvnc_srvvncstatustitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOSRVVNC_SRVVNCSTATUSTITLECONTROLIDTOREPLACE";
         Ddo_contratosrvvnc_srvvnccntnum_Internalname = sPrefix+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUM";
         edtavDdo_contratosrvvnc_srvvnccntnumtitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOSRVVNC_SRVVNCCNTNUMTITLECONTROLIDTOREPLACE";
         Ddo_contratosrvvnc_semcusto_Internalname = sPrefix+"DDO_CONTRATOSRVVNC_SEMCUSTO";
         edtavDdo_contratosrvvnc_semcustotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOSRVVNC_SEMCUSTOTITLECONTROLIDTOREPLACE";
         Ddo_contratoservicosvnc_ativo_Internalname = sPrefix+"DDO_CONTRATOSERVICOSVNC_ATIVO";
         edtavDdo_contratoservicosvnc_ativotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOSERVICOSVNC_ATIVOTITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         cmbContratoSrvVnc_SemCusto_Jsonclick = "";
         edtContratoSrvVnc_SrvVncCntNum_Jsonclick = "";
         cmbContratoSrvVnc_SrvVncStatus_Jsonclick = "";
         edtavPrestador_Jsonclick = "";
         edtContratoSrvVnc_ServicoVncSigla_Jsonclick = "";
         cmbContratoSrvVnc_StatusDmn_Jsonclick = "";
         cmbContratoSrvVnc_DoStatusDmn_Jsonclick = "";
         edtContratoServicosVnc_Descricao_Jsonclick = "";
         edtContratoSrvVnc_ServicoVncCod_Jsonclick = "";
         edtContratoSrvVnc_CntSrvCod_Jsonclick = "";
         edtContratoSrvVnc_ContratoCod_Jsonclick = "";
         edtContratoSrvVnc_Codigo_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         cmbContratoSrvVnc_SemCusto.ForeColor = (int)(0xFFFFFF);
         edtContratoSrvVnc_SrvVncCntNum_Forecolor = (int)(0xFFFFFF);
         cmbContratoSrvVnc_SrvVncStatus.ForeColor = (int)(0xFFFFFF);
         edtavPrestador_Enabled = 0;
         edtavPrestador_Forecolor = (int)(0xFFFFFF);
         edtContratoSrvVnc_ServicoVncSigla_Forecolor = (int)(0xFFFFFF);
         cmbContratoSrvVnc_StatusDmn.ForeColor = (int)(0xFFFFFF);
         cmbContratoSrvVnc_DoStatusDmn.ForeColor = (int)(0xFFFFFF);
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         chkContratoServicosVnc_Ativo_Titleformat = 0;
         cmbContratoSrvVnc_SemCusto_Titleformat = 0;
         edtContratoSrvVnc_SrvVncCntNum_Titleformat = 0;
         cmbContratoSrvVnc_SrvVncStatus_Titleformat = 0;
         edtContratoSrvVnc_ServicoVncSigla_Titleformat = 0;
         cmbContratoSrvVnc_StatusDmn_Titleformat = 0;
         cmbContratoSrvVnc_DoStatusDmn_Titleformat = 0;
         edtContratoServicosVnc_Descricao_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         bttBtnclonar_Visible = 1;
         cmbavRegra_Jsonclick = "";
         dynavServico_codigo_Jsonclick = "";
         cmbavAreatrabalho_codigo_Jsonclick = "";
         imgInsert_Visible = 1;
         edtavDelete_Visible = -1;
         edtavUpdate_Visible = -1;
         chkContratoServicosVnc_Ativo.Title.Text = "Ativa?";
         cmbContratoSrvVnc_SemCusto.Title.Text = "Sem custo";
         edtContratoSrvVnc_SrvVncCntNum_Title = "Contrato";
         cmbContratoSrvVnc_SrvVncStatus.Title.Text = "Com status";
         edtContratoSrvVnc_ServicoVncSigla_Title = "Criar";
         cmbContratoSrvVnc_StatusDmn.Title.Text = "No status";
         cmbContratoSrvVnc_DoStatusDmn.Title.Text = "Do status";
         edtContratoServicosVnc_Descricao_Title = "Descri��o";
         tblTblclonar_Visible = 1;
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkContratoServicosVnc_Ativo.Caption = "";
         edtavDdo_contratoservicosvnc_ativotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratosrvvnc_semcustotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratosrvvnc_srvvnccntnumtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratosrvvnc_srvvncstatustitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratosrvvnc_servicovncsiglatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratosrvvnc_statusdmntitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratosrvvnc_dostatusdmntitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicosvnc_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavTfcontratoservicosvnc_ativo_sel_Jsonclick = "";
         edtavTfcontratoservicosvnc_ativo_sel_Visible = 1;
         edtavTfcontratosrvvnc_semcusto_sel_Jsonclick = "";
         edtavTfcontratosrvvnc_semcusto_sel_Visible = 1;
         edtavTfcontratosrvvnc_srvvnccntnum_sel_Jsonclick = "";
         edtavTfcontratosrvvnc_srvvnccntnum_sel_Visible = 1;
         edtavTfcontratosrvvnc_srvvnccntnum_Jsonclick = "";
         edtavTfcontratosrvvnc_srvvnccntnum_Visible = 1;
         edtavTfcontratosrvvnc_servicovncsigla_sel_Jsonclick = "";
         edtavTfcontratosrvvnc_servicovncsigla_sel_Visible = 1;
         edtavTfcontratosrvvnc_servicovncsigla_Jsonclick = "";
         edtavTfcontratosrvvnc_servicovncsigla_Visible = 1;
         edtavTfcontratoservicosvnc_descricao_sel_Jsonclick = "";
         edtavTfcontratoservicosvnc_descricao_sel_Visible = 1;
         edtavTfcontratoservicosvnc_descricao_Jsonclick = "";
         edtavTfcontratoservicosvnc_descricao_Visible = 1;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         edtavOrderedby_Jsonclick = "";
         edtavOrderedby_Visible = 1;
         Ddo_contratoservicosvnc_ativo_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicosvnc_ativo_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosvnc_ativo_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosvnc_ativo_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosvnc_ativo_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_contratoservicosvnc_ativo_Datalisttype = "FixedValues";
         Ddo_contratoservicosvnc_ativo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoservicosvnc_ativo_Includefilter = Convert.ToBoolean( 0);
         Ddo_contratoservicosvnc_ativo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosvnc_ativo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosvnc_ativo_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosvnc_ativo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosvnc_ativo_Cls = "ColumnSettings";
         Ddo_contratoservicosvnc_ativo_Tooltip = "Op��es";
         Ddo_contratoservicosvnc_ativo_Caption = "";
         Ddo_contratosrvvnc_semcusto_Searchbuttontext = "Pesquisar";
         Ddo_contratosrvvnc_semcusto_Cleanfilter = "Limpar pesquisa";
         Ddo_contratosrvvnc_semcusto_Sortdsc = "Ordenar de Z � A";
         Ddo_contratosrvvnc_semcusto_Sortasc = "Ordenar de A � Z";
         Ddo_contratosrvvnc_semcusto_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_contratosrvvnc_semcusto_Datalisttype = "FixedValues";
         Ddo_contratosrvvnc_semcusto_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_semcusto_Includefilter = Convert.ToBoolean( 0);
         Ddo_contratosrvvnc_semcusto_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_semcusto_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_semcusto_Titlecontrolidtoreplace = "";
         Ddo_contratosrvvnc_semcusto_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratosrvvnc_semcusto_Cls = "ColumnSettings";
         Ddo_contratosrvvnc_semcusto_Tooltip = "Op��es";
         Ddo_contratosrvvnc_semcusto_Caption = "";
         Ddo_contratosrvvnc_srvvnccntnum_Searchbuttontext = "Pesquisar";
         Ddo_contratosrvvnc_srvvnccntnum_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratosrvvnc_srvvnccntnum_Cleanfilter = "Limpar pesquisa";
         Ddo_contratosrvvnc_srvvnccntnum_Loadingdata = "Carregando dados...";
         Ddo_contratosrvvnc_srvvnccntnum_Sortdsc = "Ordenar de Z � A";
         Ddo_contratosrvvnc_srvvnccntnum_Sortasc = "Ordenar de A � Z";
         Ddo_contratosrvvnc_srvvnccntnum_Datalistupdateminimumcharacters = 0;
         Ddo_contratosrvvnc_srvvnccntnum_Datalistproc = "GetContratoServicos_VncWCFilterData";
         Ddo_contratosrvvnc_srvvnccntnum_Datalisttype = "Dynamic";
         Ddo_contratosrvvnc_srvvnccntnum_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_srvvnccntnum_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratosrvvnc_srvvnccntnum_Filtertype = "Character";
         Ddo_contratosrvvnc_srvvnccntnum_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_srvvnccntnum_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_srvvnccntnum_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_srvvnccntnum_Titlecontrolidtoreplace = "";
         Ddo_contratosrvvnc_srvvnccntnum_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratosrvvnc_srvvnccntnum_Cls = "ColumnSettings";
         Ddo_contratosrvvnc_srvvnccntnum_Tooltip = "Op��es";
         Ddo_contratosrvvnc_srvvnccntnum_Caption = "";
         Ddo_contratosrvvnc_srvvncstatus_Searchbuttontext = "Filtrar Selecionados";
         Ddo_contratosrvvnc_srvvncstatus_Cleanfilter = "Limpar pesquisa";
         Ddo_contratosrvvnc_srvvncstatus_Sortdsc = "Ordenar de Z � A";
         Ddo_contratosrvvnc_srvvncstatus_Sortasc = "Ordenar de A � Z";
         Ddo_contratosrvvnc_srvvncstatus_Datalistfixedvalues = "B:Stand by,S:Solicitada,E:Em An�lise,A:Em execu��o,R:Resolvida,C:Conferida,D:Retornada,H:Homologada,O:Aceite,P:A Pagar,L:Liquidada,X:Cancelada,N:N�o Faturada,J:Planejamento,I:An�lise Planejamento,T:Validacao T�cnica,Q:Validacao Qualidade,G:Em Homologa��o,M:Valida��o Mensura��o,U:Rascunho";
         Ddo_contratosrvvnc_srvvncstatus_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_srvvncstatus_Datalisttype = "FixedValues";
         Ddo_contratosrvvnc_srvvncstatus_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_srvvncstatus_Includefilter = Convert.ToBoolean( 0);
         Ddo_contratosrvvnc_srvvncstatus_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_srvvncstatus_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_srvvncstatus_Titlecontrolidtoreplace = "";
         Ddo_contratosrvvnc_srvvncstatus_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratosrvvnc_srvvncstatus_Cls = "ColumnSettings";
         Ddo_contratosrvvnc_srvvncstatus_Tooltip = "Op��es";
         Ddo_contratosrvvnc_srvvncstatus_Caption = "";
         Ddo_contratosrvvnc_servicovncsigla_Searchbuttontext = "Pesquisar";
         Ddo_contratosrvvnc_servicovncsigla_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratosrvvnc_servicovncsigla_Cleanfilter = "Limpar pesquisa";
         Ddo_contratosrvvnc_servicovncsigla_Loadingdata = "Carregando dados...";
         Ddo_contratosrvvnc_servicovncsigla_Sortdsc = "Ordenar de Z � A";
         Ddo_contratosrvvnc_servicovncsigla_Sortasc = "Ordenar de A � Z";
         Ddo_contratosrvvnc_servicovncsigla_Datalistupdateminimumcharacters = 0;
         Ddo_contratosrvvnc_servicovncsigla_Datalistproc = "GetContratoServicos_VncWCFilterData";
         Ddo_contratosrvvnc_servicovncsigla_Datalisttype = "Dynamic";
         Ddo_contratosrvvnc_servicovncsigla_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_servicovncsigla_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratosrvvnc_servicovncsigla_Filtertype = "Character";
         Ddo_contratosrvvnc_servicovncsigla_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_servicovncsigla_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_servicovncsigla_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_servicovncsigla_Titlecontrolidtoreplace = "";
         Ddo_contratosrvvnc_servicovncsigla_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratosrvvnc_servicovncsigla_Cls = "ColumnSettings";
         Ddo_contratosrvvnc_servicovncsigla_Tooltip = "Op��es";
         Ddo_contratosrvvnc_servicovncsigla_Caption = "";
         Ddo_contratosrvvnc_statusdmn_Searchbuttontext = "Filtrar Selecionados";
         Ddo_contratosrvvnc_statusdmn_Cleanfilter = "Limpar pesquisa";
         Ddo_contratosrvvnc_statusdmn_Sortdsc = "Ordenar de Z � A";
         Ddo_contratosrvvnc_statusdmn_Sortasc = "Ordenar de A � Z";
         Ddo_contratosrvvnc_statusdmn_Datalistfixedvalues = "B:Stand by,S:Solicitada,E:Em An�lise,A:Em execu��o,R:Resolvida,C:Conferida,D:Retornada,H:Homologada,O:Aceite,P:A Pagar,L:Liquidada,X:Cancelada,N:N�o Faturada,J:Planejamento,I:An�lise Planejamento,T:Validacao T�cnica,Q:Validacao Qualidade,G:Em Homologa��o,M:Valida��o Mensura��o,U:Rascunho";
         Ddo_contratosrvvnc_statusdmn_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_statusdmn_Datalisttype = "FixedValues";
         Ddo_contratosrvvnc_statusdmn_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_statusdmn_Includefilter = Convert.ToBoolean( 0);
         Ddo_contratosrvvnc_statusdmn_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_statusdmn_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_statusdmn_Titlecontrolidtoreplace = "";
         Ddo_contratosrvvnc_statusdmn_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratosrvvnc_statusdmn_Cls = "ColumnSettings";
         Ddo_contratosrvvnc_statusdmn_Tooltip = "Op��es";
         Ddo_contratosrvvnc_statusdmn_Caption = "";
         Ddo_contratosrvvnc_dostatusdmn_Searchbuttontext = "Filtrar Selecionados";
         Ddo_contratosrvvnc_dostatusdmn_Cleanfilter = "Limpar pesquisa";
         Ddo_contratosrvvnc_dostatusdmn_Sortdsc = "Ordenar de Z � A";
         Ddo_contratosrvvnc_dostatusdmn_Sortasc = "Ordenar de A � Z";
         Ddo_contratosrvvnc_dostatusdmn_Datalistfixedvalues = "B:Stand by,S:Solicitada,E:Em An�lise,A:Em execu��o,R:Resolvida,C:Conferida,D:Retornada,H:Homologada,O:Aceite,P:A Pagar,L:Liquidada,X:Cancelada,N:N�o Faturada,J:Planejamento,I:An�lise Planejamento,T:Validacao T�cnica,Q:Validacao Qualidade,G:Em Homologa��o,M:Valida��o Mensura��o,U:Rascunho";
         Ddo_contratosrvvnc_dostatusdmn_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_dostatusdmn_Datalisttype = "FixedValues";
         Ddo_contratosrvvnc_dostatusdmn_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_dostatusdmn_Includefilter = Convert.ToBoolean( 0);
         Ddo_contratosrvvnc_dostatusdmn_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_dostatusdmn_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_dostatusdmn_Titlecontrolidtoreplace = "";
         Ddo_contratosrvvnc_dostatusdmn_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratosrvvnc_dostatusdmn_Cls = "ColumnSettings";
         Ddo_contratosrvvnc_dostatusdmn_Tooltip = "Op��es";
         Ddo_contratosrvvnc_dostatusdmn_Caption = "";
         Ddo_contratoservicosvnc_descricao_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicosvnc_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratoservicosvnc_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosvnc_descricao_Loadingdata = "Carregando dados...";
         Ddo_contratoservicosvnc_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosvnc_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosvnc_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_contratoservicosvnc_descricao_Datalistproc = "GetContratoServicos_VncWCFilterData";
         Ddo_contratoservicosvnc_descricao_Datalisttype = "Dynamic";
         Ddo_contratoservicosvnc_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoservicosvnc_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratoservicosvnc_descricao_Filtertype = "Character";
         Ddo_contratoservicosvnc_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicosvnc_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosvnc_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosvnc_descricao_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosvnc_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosvnc_descricao_Cls = "ColumnSettings";
         Ddo_contratoservicosvnc_descricao_Tooltip = "Op��es";
         Ddo_contratoservicosvnc_descricao_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public void Validv_Areatrabalho_codigo( GXCombobox cmbGX_Parm1 ,
                                              GXCombobox dynGX_Parm2 ,
                                              String GX_Parm3 )
      {
         cmbavAreatrabalho_codigo = cmbGX_Parm1;
         AV19AreaTrabalho_Codigo = (int)(NumberUtil.Val( cmbavAreatrabalho_codigo.CurrentValue, "."));
         dynavServico_codigo = dynGX_Parm2;
         AV20Servico_Codigo = (int)(NumberUtil.Val( dynavServico_codigo.CurrentValue, "."));
         sPrefix = GX_Parm3;
         GXVvSERVICO_CODIGO_htmlGA2( AV19AreaTrabalho_Codigo) ;
         dynload_actions( ) ;
         if ( dynavServico_codigo.ItemCount > 0 )
         {
            AV20Servico_Codigo = (int)(NumberUtil.Val( dynavServico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20Servico_Codigo), 6, 0))), "."));
         }
         dynavServico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20Servico_Codigo), 6, 0));
         isValidOutput.Add(dynavServico_codigo);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A1453ContratoServicosVnc_Ativo',fld:'CONTRATOSERVICOSVNC_ATIVO',pic:'',hsh:true,nv:false},{av:'A1092ContratoSrvVnc_PrestadoraPesNom',fld:'CONTRATOSRVVNC_PRESTADORAPESNOM',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'AV60ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29ddo_ContratoSrvVnc_DoStatusDmnTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_DOSTATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOVNCSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContratoSrvVnc_SrvVncStatusTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SRVVNCSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SRVVNCCNTNUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SEMCUSTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A6AreaTrabalho_Descricao',fld:'AREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'A72AreaTrabalho_Ativo',fld:'AREATRABALHO_ATIVO',pic:'',nv:false},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1297ContratadaUsuario_AreaTrabalhoDes',fld:'CONTRATADAUSUARIO_AREATRABALHODES',pic:'@!',nv:''},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV58TFContratoServicosVnc_Descricao',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO',pic:'@!',nv:''},{av:'AV59TFContratoServicosVnc_Descricao_Sel',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV28TFContratoSrvVnc_DoStatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_DOSTATUSDMN_SELS',pic:'',nv:null},{av:'AV32TFContratoSrvVnc_StatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_STATUSDMN_SELS',pic:'',nv:null},{av:'AV35TFContratoSrvVnc_ServicoVncSigla',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'AV36TFContratoSrvVnc_ServicoVncSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL',pic:'@!',nv:''},{av:'AV40TFContratoSrvVnc_SrvVncStatus_Sels',fld:'vTFCONTRATOSRVVNC_SRVVNCSTATUS_SELS',pic:'',nv:null},{av:'AV43TFContratoSrvVnc_SrvVncCntNum',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM',pic:'',nv:''},{av:'AV44TFContratoSrvVnc_SrvVncCntNum_Sel',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM_SEL',pic:'',nv:''},{av:'AV47TFContratoSrvVnc_SemCusto_Sel',fld:'vTFCONTRATOSRVVNC_SEMCUSTO_SEL',pic:'9',nv:0},{av:'AV50TFContratoServicosVnc_Ativo_Sel',fld:'vTFCONTRATOSERVICOSVNC_ATIVO_SEL',pic:'9',nv:0},{av:'AV18ContratoSrvVnc_CntSrvCod',fld:'vCONTRATOSRVVNC_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A75Contrato_AreaTrabalhoCod',fld:'CONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV19AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A921ContratoSrvVnc_ServicoCod',fld:'CONTRATOSRVVNC_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV20Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A917ContratoSrvVnc_Codigo',fld:'CONTRATOSRVVNC_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A924ContratoSrvVnc_ServicoVncSigla',fld:'CONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'A1800ContratoSrvVnc_DoStatusDmn',fld:'CONTRATOSRVVNC_DOSTATUSDMN',pic:'',hsh:true,nv:''},{av:'A1084ContratoSrvVnc_StatusDmn',fld:'CONTRATOSRVVNC_STATUSDMN',pic:'',hsh:true,nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],oparms:[{av:'AV57ContratoServicosVnc_DescricaoTitleFilterData',fld:'vCONTRATOSERVICOSVNC_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV26ContratoSrvVnc_DoStatusDmnTitleFilterData',fld:'vCONTRATOSRVVNC_DOSTATUSDMNTITLEFILTERDATA',pic:'',nv:null},{av:'AV30ContratoSrvVnc_StatusDmnTitleFilterData',fld:'vCONTRATOSRVVNC_STATUSDMNTITLEFILTERDATA',pic:'',nv:null},{av:'AV34ContratoSrvVnc_ServicoVncSiglaTitleFilterData',fld:'vCONTRATOSRVVNC_SERVICOVNCSIGLATITLEFILTERDATA',pic:'',nv:null},{av:'AV38ContratoSrvVnc_SrvVncStatusTitleFilterData',fld:'vCONTRATOSRVVNC_SRVVNCSTATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV42ContratoSrvVnc_SrvVncCntNumTitleFilterData',fld:'vCONTRATOSRVVNC_SRVVNCCNTNUMTITLEFILTERDATA',pic:'',nv:null},{av:'AV46ContratoSrvVnc_SemCustoTitleFilterData',fld:'vCONTRATOSRVVNC_SEMCUSTOTITLEFILTERDATA',pic:'',nv:null},{av:'AV49ContratoServicosVnc_AtivoTitleFilterData',fld:'vCONTRATOSERVICOSVNC_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtContratoServicosVnc_Descricao_Titleformat',ctrl:'CONTRATOSERVICOSVNC_DESCRICAO',prop:'Titleformat'},{av:'edtContratoServicosVnc_Descricao_Title',ctrl:'CONTRATOSERVICOSVNC_DESCRICAO',prop:'Title'},{av:'cmbContratoSrvVnc_DoStatusDmn'},{av:'cmbContratoSrvVnc_StatusDmn'},{av:'edtContratoSrvVnc_ServicoVncSigla_Titleformat',ctrl:'CONTRATOSRVVNC_SERVICOVNCSIGLA',prop:'Titleformat'},{av:'edtContratoSrvVnc_ServicoVncSigla_Title',ctrl:'CONTRATOSRVVNC_SERVICOVNCSIGLA',prop:'Title'},{av:'cmbContratoSrvVnc_SrvVncStatus'},{av:'edtContratoSrvVnc_SrvVncCntNum_Titleformat',ctrl:'CONTRATOSRVVNC_SRVVNCCNTNUM',prop:'Titleformat'},{av:'edtContratoSrvVnc_SrvVncCntNum_Title',ctrl:'CONTRATOSRVVNC_SRVVNCCNTNUM',prop:'Title'},{av:'cmbContratoSrvVnc_SemCusto'},{av:'chkContratoServicosVnc_Ativo_Titleformat',ctrl:'CONTRATOSERVICOSVNC_ATIVO',prop:'Titleformat'},{av:'chkContratoServicosVnc_Ativo.Title.Text',ctrl:'CONTRATOSERVICOSVNC_ATIVO',prop:'Title'},{av:'AV54GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV55GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'imgInsert_Visible',ctrl:'INSERT',prop:'Visible'},{av:'AV19AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV21Regra',fld:'vREGRA',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11GA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV58TFContratoServicosVnc_Descricao',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO',pic:'@!',nv:''},{av:'AV59TFContratoServicosVnc_Descricao_Sel',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV35TFContratoSrvVnc_ServicoVncSigla',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'AV36TFContratoSrvVnc_ServicoVncSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL',pic:'@!',nv:''},{av:'AV43TFContratoSrvVnc_SrvVncCntNum',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM',pic:'',nv:''},{av:'AV44TFContratoSrvVnc_SrvVncCntNum_Sel',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM_SEL',pic:'',nv:''},{av:'AV47TFContratoSrvVnc_SemCusto_Sel',fld:'vTFCONTRATOSRVVNC_SEMCUSTO_SEL',pic:'9',nv:0},{av:'AV50TFContratoServicosVnc_Ativo_Sel',fld:'vTFCONTRATOSERVICOSVNC_ATIVO_SEL',pic:'9',nv:0},{av:'AV18ContratoSrvVnc_CntSrvCod',fld:'vCONTRATOSRVVNC_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV60ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29ddo_ContratoSrvVnc_DoStatusDmnTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_DOSTATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOVNCSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContratoSrvVnc_SrvVncStatusTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SRVVNCSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SRVVNCCNTNUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SEMCUSTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A6AreaTrabalho_Descricao',fld:'AREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'A72AreaTrabalho_Ativo',fld:'AREATRABALHO_ATIVO',pic:'',nv:false},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1297ContratadaUsuario_AreaTrabalhoDes',fld:'CONTRATADAUSUARIO_AREATRABALHODES',pic:'@!',nv:''},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV28TFContratoSrvVnc_DoStatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_DOSTATUSDMN_SELS',pic:'',nv:null},{av:'AV32TFContratoSrvVnc_StatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_STATUSDMN_SELS',pic:'',nv:null},{av:'AV40TFContratoSrvVnc_SrvVncStatus_Sels',fld:'vTFCONTRATOSRVVNC_SRVVNCSTATUS_SELS',pic:'',nv:null},{av:'A75Contrato_AreaTrabalhoCod',fld:'CONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV19AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A921ContratoSrvVnc_ServicoCod',fld:'CONTRATOSRVVNC_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV20Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A917ContratoSrvVnc_Codigo',fld:'CONTRATOSRVVNC_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A924ContratoSrvVnc_ServicoVncSigla',fld:'CONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'A1800ContratoSrvVnc_DoStatusDmn',fld:'CONTRATOSRVVNC_DOSTATUSDMN',pic:'',hsh:true,nv:''},{av:'A1084ContratoSrvVnc_StatusDmn',fld:'CONTRATOSRVVNC_STATUSDMN',pic:'',hsh:true,nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1453ContratoServicosVnc_Ativo',fld:'CONTRATOSERVICOSVNC_ATIVO',pic:'',hsh:true,nv:false},{av:'A1092ContratoSrvVnc_PrestadoraPesNom',fld:'CONTRATOSRVVNC_PRESTADORAPESNOM',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTRATOSERVICOSVNC_DESCRICAO.ONOPTIONCLICKED","{handler:'E12GA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV58TFContratoServicosVnc_Descricao',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO',pic:'@!',nv:''},{av:'AV59TFContratoServicosVnc_Descricao_Sel',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV35TFContratoSrvVnc_ServicoVncSigla',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'AV36TFContratoSrvVnc_ServicoVncSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL',pic:'@!',nv:''},{av:'AV43TFContratoSrvVnc_SrvVncCntNum',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM',pic:'',nv:''},{av:'AV44TFContratoSrvVnc_SrvVncCntNum_Sel',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM_SEL',pic:'',nv:''},{av:'AV47TFContratoSrvVnc_SemCusto_Sel',fld:'vTFCONTRATOSRVVNC_SEMCUSTO_SEL',pic:'9',nv:0},{av:'AV50TFContratoServicosVnc_Ativo_Sel',fld:'vTFCONTRATOSERVICOSVNC_ATIVO_SEL',pic:'9',nv:0},{av:'AV18ContratoSrvVnc_CntSrvCod',fld:'vCONTRATOSRVVNC_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV60ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29ddo_ContratoSrvVnc_DoStatusDmnTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_DOSTATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOVNCSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContratoSrvVnc_SrvVncStatusTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SRVVNCSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SRVVNCCNTNUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SEMCUSTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A6AreaTrabalho_Descricao',fld:'AREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'A72AreaTrabalho_Ativo',fld:'AREATRABALHO_ATIVO',pic:'',nv:false},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1297ContratadaUsuario_AreaTrabalhoDes',fld:'CONTRATADAUSUARIO_AREATRABALHODES',pic:'@!',nv:''},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV28TFContratoSrvVnc_DoStatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_DOSTATUSDMN_SELS',pic:'',nv:null},{av:'AV32TFContratoSrvVnc_StatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_STATUSDMN_SELS',pic:'',nv:null},{av:'AV40TFContratoSrvVnc_SrvVncStatus_Sels',fld:'vTFCONTRATOSRVVNC_SRVVNCSTATUS_SELS',pic:'',nv:null},{av:'A75Contrato_AreaTrabalhoCod',fld:'CONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV19AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A921ContratoSrvVnc_ServicoCod',fld:'CONTRATOSRVVNC_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV20Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A917ContratoSrvVnc_Codigo',fld:'CONTRATOSRVVNC_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A924ContratoSrvVnc_ServicoVncSigla',fld:'CONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'A1800ContratoSrvVnc_DoStatusDmn',fld:'CONTRATOSRVVNC_DOSTATUSDMN',pic:'',hsh:true,nv:''},{av:'A1084ContratoSrvVnc_StatusDmn',fld:'CONTRATOSRVVNC_STATUSDMN',pic:'',hsh:true,nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1453ContratoServicosVnc_Ativo',fld:'CONTRATOSERVICOSVNC_ATIVO',pic:'',hsh:true,nv:false},{av:'A1092ContratoSrvVnc_PrestadoraPesNom',fld:'CONTRATOSRVVNC_PRESTADORAPESNOM',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_contratoservicosvnc_descricao_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSVNC_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosvnc_descricao_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSVNC_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_contratoservicosvnc_descricao_Selectedvalue_get',ctrl:'DDO_CONTRATOSERVICOSVNC_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosvnc_descricao_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSVNC_DESCRICAO',prop:'SortedStatus'},{av:'AV58TFContratoServicosVnc_Descricao',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO',pic:'@!',nv:''},{av:'AV59TFContratoServicosVnc_Descricao_Sel',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_contratosrvvnc_dostatusdmn_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_DOSTATUSDMN',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_statusdmn_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_STATUSDMN',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_servicovncsigla_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_srvvncstatus_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SRVVNCSTATUS',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_srvvnccntnum_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SRVVNCCNTNUM',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_semcusto_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SEMCUSTO',prop:'SortedStatus'},{av:'Ddo_contratoservicosvnc_ativo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSVNC_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSRVVNC_DOSTATUSDMN.ONOPTIONCLICKED","{handler:'E13GA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV58TFContratoServicosVnc_Descricao',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO',pic:'@!',nv:''},{av:'AV59TFContratoServicosVnc_Descricao_Sel',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV35TFContratoSrvVnc_ServicoVncSigla',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'AV36TFContratoSrvVnc_ServicoVncSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL',pic:'@!',nv:''},{av:'AV43TFContratoSrvVnc_SrvVncCntNum',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM',pic:'',nv:''},{av:'AV44TFContratoSrvVnc_SrvVncCntNum_Sel',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM_SEL',pic:'',nv:''},{av:'AV47TFContratoSrvVnc_SemCusto_Sel',fld:'vTFCONTRATOSRVVNC_SEMCUSTO_SEL',pic:'9',nv:0},{av:'AV50TFContratoServicosVnc_Ativo_Sel',fld:'vTFCONTRATOSERVICOSVNC_ATIVO_SEL',pic:'9',nv:0},{av:'AV18ContratoSrvVnc_CntSrvCod',fld:'vCONTRATOSRVVNC_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV60ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29ddo_ContratoSrvVnc_DoStatusDmnTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_DOSTATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOVNCSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContratoSrvVnc_SrvVncStatusTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SRVVNCSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SRVVNCCNTNUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SEMCUSTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A6AreaTrabalho_Descricao',fld:'AREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'A72AreaTrabalho_Ativo',fld:'AREATRABALHO_ATIVO',pic:'',nv:false},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1297ContratadaUsuario_AreaTrabalhoDes',fld:'CONTRATADAUSUARIO_AREATRABALHODES',pic:'@!',nv:''},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV28TFContratoSrvVnc_DoStatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_DOSTATUSDMN_SELS',pic:'',nv:null},{av:'AV32TFContratoSrvVnc_StatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_STATUSDMN_SELS',pic:'',nv:null},{av:'AV40TFContratoSrvVnc_SrvVncStatus_Sels',fld:'vTFCONTRATOSRVVNC_SRVVNCSTATUS_SELS',pic:'',nv:null},{av:'A75Contrato_AreaTrabalhoCod',fld:'CONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV19AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A921ContratoSrvVnc_ServicoCod',fld:'CONTRATOSRVVNC_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV20Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A917ContratoSrvVnc_Codigo',fld:'CONTRATOSRVVNC_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A924ContratoSrvVnc_ServicoVncSigla',fld:'CONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'A1800ContratoSrvVnc_DoStatusDmn',fld:'CONTRATOSRVVNC_DOSTATUSDMN',pic:'',hsh:true,nv:''},{av:'A1084ContratoSrvVnc_StatusDmn',fld:'CONTRATOSRVVNC_STATUSDMN',pic:'',hsh:true,nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1453ContratoServicosVnc_Ativo',fld:'CONTRATOSERVICOSVNC_ATIVO',pic:'',hsh:true,nv:false},{av:'A1092ContratoSrvVnc_PrestadoraPesNom',fld:'CONTRATOSRVVNC_PRESTADORAPESNOM',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_contratosrvvnc_dostatusdmn_Activeeventkey',ctrl:'DDO_CONTRATOSRVVNC_DOSTATUSDMN',prop:'ActiveEventKey'},{av:'Ddo_contratosrvvnc_dostatusdmn_Selectedvalue_get',ctrl:'DDO_CONTRATOSRVVNC_DOSTATUSDMN',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratosrvvnc_dostatusdmn_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_DOSTATUSDMN',prop:'SortedStatus'},{av:'AV28TFContratoSrvVnc_DoStatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_DOSTATUSDMN_SELS',pic:'',nv:null},{av:'Ddo_contratoservicosvnc_descricao_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSVNC_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_statusdmn_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_STATUSDMN',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_servicovncsigla_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_srvvncstatus_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SRVVNCSTATUS',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_srvvnccntnum_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SRVVNCCNTNUM',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_semcusto_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SEMCUSTO',prop:'SortedStatus'},{av:'Ddo_contratoservicosvnc_ativo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSVNC_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSRVVNC_STATUSDMN.ONOPTIONCLICKED","{handler:'E14GA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV58TFContratoServicosVnc_Descricao',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO',pic:'@!',nv:''},{av:'AV59TFContratoServicosVnc_Descricao_Sel',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV35TFContratoSrvVnc_ServicoVncSigla',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'AV36TFContratoSrvVnc_ServicoVncSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL',pic:'@!',nv:''},{av:'AV43TFContratoSrvVnc_SrvVncCntNum',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM',pic:'',nv:''},{av:'AV44TFContratoSrvVnc_SrvVncCntNum_Sel',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM_SEL',pic:'',nv:''},{av:'AV47TFContratoSrvVnc_SemCusto_Sel',fld:'vTFCONTRATOSRVVNC_SEMCUSTO_SEL',pic:'9',nv:0},{av:'AV50TFContratoServicosVnc_Ativo_Sel',fld:'vTFCONTRATOSERVICOSVNC_ATIVO_SEL',pic:'9',nv:0},{av:'AV18ContratoSrvVnc_CntSrvCod',fld:'vCONTRATOSRVVNC_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV60ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29ddo_ContratoSrvVnc_DoStatusDmnTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_DOSTATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOVNCSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContratoSrvVnc_SrvVncStatusTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SRVVNCSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SRVVNCCNTNUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SEMCUSTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A6AreaTrabalho_Descricao',fld:'AREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'A72AreaTrabalho_Ativo',fld:'AREATRABALHO_ATIVO',pic:'',nv:false},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1297ContratadaUsuario_AreaTrabalhoDes',fld:'CONTRATADAUSUARIO_AREATRABALHODES',pic:'@!',nv:''},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV28TFContratoSrvVnc_DoStatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_DOSTATUSDMN_SELS',pic:'',nv:null},{av:'AV32TFContratoSrvVnc_StatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_STATUSDMN_SELS',pic:'',nv:null},{av:'AV40TFContratoSrvVnc_SrvVncStatus_Sels',fld:'vTFCONTRATOSRVVNC_SRVVNCSTATUS_SELS',pic:'',nv:null},{av:'A75Contrato_AreaTrabalhoCod',fld:'CONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV19AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A921ContratoSrvVnc_ServicoCod',fld:'CONTRATOSRVVNC_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV20Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A917ContratoSrvVnc_Codigo',fld:'CONTRATOSRVVNC_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A924ContratoSrvVnc_ServicoVncSigla',fld:'CONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'A1800ContratoSrvVnc_DoStatusDmn',fld:'CONTRATOSRVVNC_DOSTATUSDMN',pic:'',hsh:true,nv:''},{av:'A1084ContratoSrvVnc_StatusDmn',fld:'CONTRATOSRVVNC_STATUSDMN',pic:'',hsh:true,nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1453ContratoServicosVnc_Ativo',fld:'CONTRATOSERVICOSVNC_ATIVO',pic:'',hsh:true,nv:false},{av:'A1092ContratoSrvVnc_PrestadoraPesNom',fld:'CONTRATOSRVVNC_PRESTADORAPESNOM',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_contratosrvvnc_statusdmn_Activeeventkey',ctrl:'DDO_CONTRATOSRVVNC_STATUSDMN',prop:'ActiveEventKey'},{av:'Ddo_contratosrvvnc_statusdmn_Selectedvalue_get',ctrl:'DDO_CONTRATOSRVVNC_STATUSDMN',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratosrvvnc_statusdmn_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_STATUSDMN',prop:'SortedStatus'},{av:'AV32TFContratoSrvVnc_StatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_STATUSDMN_SELS',pic:'',nv:null},{av:'Ddo_contratoservicosvnc_descricao_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSVNC_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_dostatusdmn_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_DOSTATUSDMN',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_servicovncsigla_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_srvvncstatus_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SRVVNCSTATUS',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_srvvnccntnum_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SRVVNCCNTNUM',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_semcusto_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SEMCUSTO',prop:'SortedStatus'},{av:'Ddo_contratoservicosvnc_ativo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSVNC_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA.ONOPTIONCLICKED","{handler:'E15GA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV58TFContratoServicosVnc_Descricao',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO',pic:'@!',nv:''},{av:'AV59TFContratoServicosVnc_Descricao_Sel',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV35TFContratoSrvVnc_ServicoVncSigla',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'AV36TFContratoSrvVnc_ServicoVncSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL',pic:'@!',nv:''},{av:'AV43TFContratoSrvVnc_SrvVncCntNum',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM',pic:'',nv:''},{av:'AV44TFContratoSrvVnc_SrvVncCntNum_Sel',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM_SEL',pic:'',nv:''},{av:'AV47TFContratoSrvVnc_SemCusto_Sel',fld:'vTFCONTRATOSRVVNC_SEMCUSTO_SEL',pic:'9',nv:0},{av:'AV50TFContratoServicosVnc_Ativo_Sel',fld:'vTFCONTRATOSERVICOSVNC_ATIVO_SEL',pic:'9',nv:0},{av:'AV18ContratoSrvVnc_CntSrvCod',fld:'vCONTRATOSRVVNC_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV60ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29ddo_ContratoSrvVnc_DoStatusDmnTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_DOSTATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOVNCSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContratoSrvVnc_SrvVncStatusTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SRVVNCSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SRVVNCCNTNUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SEMCUSTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A6AreaTrabalho_Descricao',fld:'AREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'A72AreaTrabalho_Ativo',fld:'AREATRABALHO_ATIVO',pic:'',nv:false},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1297ContratadaUsuario_AreaTrabalhoDes',fld:'CONTRATADAUSUARIO_AREATRABALHODES',pic:'@!',nv:''},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV28TFContratoSrvVnc_DoStatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_DOSTATUSDMN_SELS',pic:'',nv:null},{av:'AV32TFContratoSrvVnc_StatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_STATUSDMN_SELS',pic:'',nv:null},{av:'AV40TFContratoSrvVnc_SrvVncStatus_Sels',fld:'vTFCONTRATOSRVVNC_SRVVNCSTATUS_SELS',pic:'',nv:null},{av:'A75Contrato_AreaTrabalhoCod',fld:'CONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV19AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A921ContratoSrvVnc_ServicoCod',fld:'CONTRATOSRVVNC_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV20Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A917ContratoSrvVnc_Codigo',fld:'CONTRATOSRVVNC_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A924ContratoSrvVnc_ServicoVncSigla',fld:'CONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'A1800ContratoSrvVnc_DoStatusDmn',fld:'CONTRATOSRVVNC_DOSTATUSDMN',pic:'',hsh:true,nv:''},{av:'A1084ContratoSrvVnc_StatusDmn',fld:'CONTRATOSRVVNC_STATUSDMN',pic:'',hsh:true,nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1453ContratoServicosVnc_Ativo',fld:'CONTRATOSERVICOSVNC_ATIVO',pic:'',hsh:true,nv:false},{av:'A1092ContratoSrvVnc_PrestadoraPesNom',fld:'CONTRATOSRVVNC_PRESTADORAPESNOM',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_contratosrvvnc_servicovncsigla_Activeeventkey',ctrl:'DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA',prop:'ActiveEventKey'},{av:'Ddo_contratosrvvnc_servicovncsigla_Filteredtext_get',ctrl:'DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA',prop:'FilteredText_get'},{av:'Ddo_contratosrvvnc_servicovncsigla_Selectedvalue_get',ctrl:'DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratosrvvnc_servicovncsigla_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA',prop:'SortedStatus'},{av:'AV35TFContratoSrvVnc_ServicoVncSigla',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'AV36TFContratoSrvVnc_ServicoVncSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_contratoservicosvnc_descricao_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSVNC_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_dostatusdmn_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_DOSTATUSDMN',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_statusdmn_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_STATUSDMN',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_srvvncstatus_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SRVVNCSTATUS',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_srvvnccntnum_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SRVVNCCNTNUM',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_semcusto_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SEMCUSTO',prop:'SortedStatus'},{av:'Ddo_contratoservicosvnc_ativo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSVNC_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSRVVNC_SRVVNCSTATUS.ONOPTIONCLICKED","{handler:'E16GA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV58TFContratoServicosVnc_Descricao',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO',pic:'@!',nv:''},{av:'AV59TFContratoServicosVnc_Descricao_Sel',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV35TFContratoSrvVnc_ServicoVncSigla',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'AV36TFContratoSrvVnc_ServicoVncSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL',pic:'@!',nv:''},{av:'AV43TFContratoSrvVnc_SrvVncCntNum',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM',pic:'',nv:''},{av:'AV44TFContratoSrvVnc_SrvVncCntNum_Sel',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM_SEL',pic:'',nv:''},{av:'AV47TFContratoSrvVnc_SemCusto_Sel',fld:'vTFCONTRATOSRVVNC_SEMCUSTO_SEL',pic:'9',nv:0},{av:'AV50TFContratoServicosVnc_Ativo_Sel',fld:'vTFCONTRATOSERVICOSVNC_ATIVO_SEL',pic:'9',nv:0},{av:'AV18ContratoSrvVnc_CntSrvCod',fld:'vCONTRATOSRVVNC_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV60ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29ddo_ContratoSrvVnc_DoStatusDmnTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_DOSTATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOVNCSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContratoSrvVnc_SrvVncStatusTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SRVVNCSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SRVVNCCNTNUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SEMCUSTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A6AreaTrabalho_Descricao',fld:'AREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'A72AreaTrabalho_Ativo',fld:'AREATRABALHO_ATIVO',pic:'',nv:false},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1297ContratadaUsuario_AreaTrabalhoDes',fld:'CONTRATADAUSUARIO_AREATRABALHODES',pic:'@!',nv:''},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV28TFContratoSrvVnc_DoStatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_DOSTATUSDMN_SELS',pic:'',nv:null},{av:'AV32TFContratoSrvVnc_StatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_STATUSDMN_SELS',pic:'',nv:null},{av:'AV40TFContratoSrvVnc_SrvVncStatus_Sels',fld:'vTFCONTRATOSRVVNC_SRVVNCSTATUS_SELS',pic:'',nv:null},{av:'A75Contrato_AreaTrabalhoCod',fld:'CONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV19AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A921ContratoSrvVnc_ServicoCod',fld:'CONTRATOSRVVNC_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV20Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A917ContratoSrvVnc_Codigo',fld:'CONTRATOSRVVNC_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A924ContratoSrvVnc_ServicoVncSigla',fld:'CONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'A1800ContratoSrvVnc_DoStatusDmn',fld:'CONTRATOSRVVNC_DOSTATUSDMN',pic:'',hsh:true,nv:''},{av:'A1084ContratoSrvVnc_StatusDmn',fld:'CONTRATOSRVVNC_STATUSDMN',pic:'',hsh:true,nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1453ContratoServicosVnc_Ativo',fld:'CONTRATOSERVICOSVNC_ATIVO',pic:'',hsh:true,nv:false},{av:'A1092ContratoSrvVnc_PrestadoraPesNom',fld:'CONTRATOSRVVNC_PRESTADORAPESNOM',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_contratosrvvnc_srvvncstatus_Activeeventkey',ctrl:'DDO_CONTRATOSRVVNC_SRVVNCSTATUS',prop:'ActiveEventKey'},{av:'Ddo_contratosrvvnc_srvvncstatus_Selectedvalue_get',ctrl:'DDO_CONTRATOSRVVNC_SRVVNCSTATUS',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratosrvvnc_srvvncstatus_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SRVVNCSTATUS',prop:'SortedStatus'},{av:'AV40TFContratoSrvVnc_SrvVncStatus_Sels',fld:'vTFCONTRATOSRVVNC_SRVVNCSTATUS_SELS',pic:'',nv:null},{av:'Ddo_contratoservicosvnc_descricao_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSVNC_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_dostatusdmn_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_DOSTATUSDMN',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_statusdmn_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_STATUSDMN',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_servicovncsigla_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_srvvnccntnum_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SRVVNCCNTNUM',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_semcusto_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SEMCUSTO',prop:'SortedStatus'},{av:'Ddo_contratoservicosvnc_ativo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSVNC_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSRVVNC_SRVVNCCNTNUM.ONOPTIONCLICKED","{handler:'E17GA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV58TFContratoServicosVnc_Descricao',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO',pic:'@!',nv:''},{av:'AV59TFContratoServicosVnc_Descricao_Sel',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV35TFContratoSrvVnc_ServicoVncSigla',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'AV36TFContratoSrvVnc_ServicoVncSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL',pic:'@!',nv:''},{av:'AV43TFContratoSrvVnc_SrvVncCntNum',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM',pic:'',nv:''},{av:'AV44TFContratoSrvVnc_SrvVncCntNum_Sel',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM_SEL',pic:'',nv:''},{av:'AV47TFContratoSrvVnc_SemCusto_Sel',fld:'vTFCONTRATOSRVVNC_SEMCUSTO_SEL',pic:'9',nv:0},{av:'AV50TFContratoServicosVnc_Ativo_Sel',fld:'vTFCONTRATOSERVICOSVNC_ATIVO_SEL',pic:'9',nv:0},{av:'AV18ContratoSrvVnc_CntSrvCod',fld:'vCONTRATOSRVVNC_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV60ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29ddo_ContratoSrvVnc_DoStatusDmnTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_DOSTATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOVNCSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContratoSrvVnc_SrvVncStatusTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SRVVNCSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SRVVNCCNTNUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SEMCUSTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A6AreaTrabalho_Descricao',fld:'AREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'A72AreaTrabalho_Ativo',fld:'AREATRABALHO_ATIVO',pic:'',nv:false},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1297ContratadaUsuario_AreaTrabalhoDes',fld:'CONTRATADAUSUARIO_AREATRABALHODES',pic:'@!',nv:''},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV28TFContratoSrvVnc_DoStatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_DOSTATUSDMN_SELS',pic:'',nv:null},{av:'AV32TFContratoSrvVnc_StatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_STATUSDMN_SELS',pic:'',nv:null},{av:'AV40TFContratoSrvVnc_SrvVncStatus_Sels',fld:'vTFCONTRATOSRVVNC_SRVVNCSTATUS_SELS',pic:'',nv:null},{av:'A75Contrato_AreaTrabalhoCod',fld:'CONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV19AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A921ContratoSrvVnc_ServicoCod',fld:'CONTRATOSRVVNC_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV20Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A917ContratoSrvVnc_Codigo',fld:'CONTRATOSRVVNC_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A924ContratoSrvVnc_ServicoVncSigla',fld:'CONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'A1800ContratoSrvVnc_DoStatusDmn',fld:'CONTRATOSRVVNC_DOSTATUSDMN',pic:'',hsh:true,nv:''},{av:'A1084ContratoSrvVnc_StatusDmn',fld:'CONTRATOSRVVNC_STATUSDMN',pic:'',hsh:true,nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1453ContratoServicosVnc_Ativo',fld:'CONTRATOSERVICOSVNC_ATIVO',pic:'',hsh:true,nv:false},{av:'A1092ContratoSrvVnc_PrestadoraPesNom',fld:'CONTRATOSRVVNC_PRESTADORAPESNOM',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_contratosrvvnc_srvvnccntnum_Activeeventkey',ctrl:'DDO_CONTRATOSRVVNC_SRVVNCCNTNUM',prop:'ActiveEventKey'},{av:'Ddo_contratosrvvnc_srvvnccntnum_Filteredtext_get',ctrl:'DDO_CONTRATOSRVVNC_SRVVNCCNTNUM',prop:'FilteredText_get'},{av:'Ddo_contratosrvvnc_srvvnccntnum_Selectedvalue_get',ctrl:'DDO_CONTRATOSRVVNC_SRVVNCCNTNUM',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratosrvvnc_srvvnccntnum_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SRVVNCCNTNUM',prop:'SortedStatus'},{av:'AV43TFContratoSrvVnc_SrvVncCntNum',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM',pic:'',nv:''},{av:'AV44TFContratoSrvVnc_SrvVncCntNum_Sel',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM_SEL',pic:'',nv:''},{av:'Ddo_contratoservicosvnc_descricao_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSVNC_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_dostatusdmn_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_DOSTATUSDMN',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_statusdmn_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_STATUSDMN',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_servicovncsigla_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_srvvncstatus_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SRVVNCSTATUS',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_semcusto_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SEMCUSTO',prop:'SortedStatus'},{av:'Ddo_contratoservicosvnc_ativo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSVNC_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSRVVNC_SEMCUSTO.ONOPTIONCLICKED","{handler:'E18GA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV58TFContratoServicosVnc_Descricao',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO',pic:'@!',nv:''},{av:'AV59TFContratoServicosVnc_Descricao_Sel',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV35TFContratoSrvVnc_ServicoVncSigla',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'AV36TFContratoSrvVnc_ServicoVncSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL',pic:'@!',nv:''},{av:'AV43TFContratoSrvVnc_SrvVncCntNum',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM',pic:'',nv:''},{av:'AV44TFContratoSrvVnc_SrvVncCntNum_Sel',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM_SEL',pic:'',nv:''},{av:'AV47TFContratoSrvVnc_SemCusto_Sel',fld:'vTFCONTRATOSRVVNC_SEMCUSTO_SEL',pic:'9',nv:0},{av:'AV50TFContratoServicosVnc_Ativo_Sel',fld:'vTFCONTRATOSERVICOSVNC_ATIVO_SEL',pic:'9',nv:0},{av:'AV18ContratoSrvVnc_CntSrvCod',fld:'vCONTRATOSRVVNC_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV60ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29ddo_ContratoSrvVnc_DoStatusDmnTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_DOSTATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOVNCSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContratoSrvVnc_SrvVncStatusTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SRVVNCSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SRVVNCCNTNUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SEMCUSTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A6AreaTrabalho_Descricao',fld:'AREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'A72AreaTrabalho_Ativo',fld:'AREATRABALHO_ATIVO',pic:'',nv:false},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1297ContratadaUsuario_AreaTrabalhoDes',fld:'CONTRATADAUSUARIO_AREATRABALHODES',pic:'@!',nv:''},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV28TFContratoSrvVnc_DoStatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_DOSTATUSDMN_SELS',pic:'',nv:null},{av:'AV32TFContratoSrvVnc_StatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_STATUSDMN_SELS',pic:'',nv:null},{av:'AV40TFContratoSrvVnc_SrvVncStatus_Sels',fld:'vTFCONTRATOSRVVNC_SRVVNCSTATUS_SELS',pic:'',nv:null},{av:'A75Contrato_AreaTrabalhoCod',fld:'CONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV19AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A921ContratoSrvVnc_ServicoCod',fld:'CONTRATOSRVVNC_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV20Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A917ContratoSrvVnc_Codigo',fld:'CONTRATOSRVVNC_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A924ContratoSrvVnc_ServicoVncSigla',fld:'CONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'A1800ContratoSrvVnc_DoStatusDmn',fld:'CONTRATOSRVVNC_DOSTATUSDMN',pic:'',hsh:true,nv:''},{av:'A1084ContratoSrvVnc_StatusDmn',fld:'CONTRATOSRVVNC_STATUSDMN',pic:'',hsh:true,nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1453ContratoServicosVnc_Ativo',fld:'CONTRATOSERVICOSVNC_ATIVO',pic:'',hsh:true,nv:false},{av:'A1092ContratoSrvVnc_PrestadoraPesNom',fld:'CONTRATOSRVVNC_PRESTADORAPESNOM',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_contratosrvvnc_semcusto_Activeeventkey',ctrl:'DDO_CONTRATOSRVVNC_SEMCUSTO',prop:'ActiveEventKey'},{av:'Ddo_contratosrvvnc_semcusto_Selectedvalue_get',ctrl:'DDO_CONTRATOSRVVNC_SEMCUSTO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratosrvvnc_semcusto_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SEMCUSTO',prop:'SortedStatus'},{av:'AV47TFContratoSrvVnc_SemCusto_Sel',fld:'vTFCONTRATOSRVVNC_SEMCUSTO_SEL',pic:'9',nv:0},{av:'Ddo_contratoservicosvnc_descricao_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSVNC_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_dostatusdmn_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_DOSTATUSDMN',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_statusdmn_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_STATUSDMN',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_servicovncsigla_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_srvvncstatus_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SRVVNCSTATUS',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_srvvnccntnum_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SRVVNCCNTNUM',prop:'SortedStatus'},{av:'Ddo_contratoservicosvnc_ativo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSVNC_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSVNC_ATIVO.ONOPTIONCLICKED","{handler:'E19GA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV58TFContratoServicosVnc_Descricao',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO',pic:'@!',nv:''},{av:'AV59TFContratoServicosVnc_Descricao_Sel',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV35TFContratoSrvVnc_ServicoVncSigla',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'AV36TFContratoSrvVnc_ServicoVncSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL',pic:'@!',nv:''},{av:'AV43TFContratoSrvVnc_SrvVncCntNum',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM',pic:'',nv:''},{av:'AV44TFContratoSrvVnc_SrvVncCntNum_Sel',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM_SEL',pic:'',nv:''},{av:'AV47TFContratoSrvVnc_SemCusto_Sel',fld:'vTFCONTRATOSRVVNC_SEMCUSTO_SEL',pic:'9',nv:0},{av:'AV50TFContratoServicosVnc_Ativo_Sel',fld:'vTFCONTRATOSERVICOSVNC_ATIVO_SEL',pic:'9',nv:0},{av:'AV18ContratoSrvVnc_CntSrvCod',fld:'vCONTRATOSRVVNC_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV60ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29ddo_ContratoSrvVnc_DoStatusDmnTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_DOSTATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOVNCSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContratoSrvVnc_SrvVncStatusTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SRVVNCSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SRVVNCCNTNUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SEMCUSTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A6AreaTrabalho_Descricao',fld:'AREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'A72AreaTrabalho_Ativo',fld:'AREATRABALHO_ATIVO',pic:'',nv:false},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1297ContratadaUsuario_AreaTrabalhoDes',fld:'CONTRATADAUSUARIO_AREATRABALHODES',pic:'@!',nv:''},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV28TFContratoSrvVnc_DoStatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_DOSTATUSDMN_SELS',pic:'',nv:null},{av:'AV32TFContratoSrvVnc_StatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_STATUSDMN_SELS',pic:'',nv:null},{av:'AV40TFContratoSrvVnc_SrvVncStatus_Sels',fld:'vTFCONTRATOSRVVNC_SRVVNCSTATUS_SELS',pic:'',nv:null},{av:'A75Contrato_AreaTrabalhoCod',fld:'CONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV19AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A921ContratoSrvVnc_ServicoCod',fld:'CONTRATOSRVVNC_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV20Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A917ContratoSrvVnc_Codigo',fld:'CONTRATOSRVVNC_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A924ContratoSrvVnc_ServicoVncSigla',fld:'CONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'A1800ContratoSrvVnc_DoStatusDmn',fld:'CONTRATOSRVVNC_DOSTATUSDMN',pic:'',hsh:true,nv:''},{av:'A1084ContratoSrvVnc_StatusDmn',fld:'CONTRATOSRVVNC_STATUSDMN',pic:'',hsh:true,nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1453ContratoServicosVnc_Ativo',fld:'CONTRATOSERVICOSVNC_ATIVO',pic:'',hsh:true,nv:false},{av:'A1092ContratoSrvVnc_PrestadoraPesNom',fld:'CONTRATOSRVVNC_PRESTADORAPESNOM',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_contratoservicosvnc_ativo_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSVNC_ATIVO',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosvnc_ativo_Selectedvalue_get',ctrl:'DDO_CONTRATOSERVICOSVNC_ATIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosvnc_ativo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSVNC_ATIVO',prop:'SortedStatus'},{av:'AV50TFContratoServicosVnc_Ativo_Sel',fld:'vTFCONTRATOSERVICOSVNC_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_contratoservicosvnc_descricao_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSVNC_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_dostatusdmn_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_DOSTATUSDMN',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_statusdmn_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_STATUSDMN',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_servicovncsigla_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_srvvncstatus_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SRVVNCSTATUS',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_srvvnccntnum_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SRVVNCCNTNUM',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_semcusto_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SEMCUSTO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E26GA2',iparms:[{av:'A917ContratoSrvVnc_Codigo',fld:'CONTRATOSRVVNC_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV18ContratoSrvVnc_CntSrvCod',fld:'vCONTRATOSRVVNC_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A1453ContratoServicosVnc_Ativo',fld:'CONTRATOSERVICOSVNC_ATIVO',pic:'',hsh:true,nv:false},{av:'A1092ContratoSrvVnc_PrestadoraPesNom',fld:'CONTRATOSRVVNC_PRESTADORAPESNOM',pic:'@!',nv:''}],oparms:[{av:'AV15Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV16Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'cmbContratoSrvVnc_DoStatusDmn'},{av:'cmbContratoSrvVnc_StatusDmn'},{av:'edtContratoSrvVnc_ServicoVncSigla_Forecolor',ctrl:'CONTRATOSRVVNC_SERVICOVNCSIGLA',prop:'Forecolor'},{av:'edtavPrestador_Forecolor',ctrl:'vPRESTADOR',prop:'Forecolor'},{av:'cmbContratoSrvVnc_SrvVncStatus'},{av:'edtContratoSrvVnc_SrvVncCntNum_Forecolor',ctrl:'CONTRATOSRVVNC_SRVVNCCNTNUM',prop:'Forecolor'},{av:'cmbContratoSrvVnc_SemCusto'},{av:'AV25Prestador',fld:'vPRESTADOR',pic:'@!',nv:''}]}");
         setEventMetadata("'DOINSERT'","{handler:'E20GA2',iparms:[{av:'A917ContratoSrvVnc_Codigo',fld:'CONTRATOSRVVNC_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV18ContratoSrvVnc_CntSrvCod',fld:'vCONTRATOSRVVNC_CNTSRVCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV18ContratoSrvVnc_CntSrvCod',fld:'vCONTRATOSRVVNC_CNTSRVCOD',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DOCLONARREGRA'","{handler:'E27GA1',iparms:[],oparms:[{av:'tblTblclonar_Visible',ctrl:'TBLCLONAR',prop:'Visible'}]}");
         setEventMetadata("'DOCLONAR'","{handler:'E21GA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV58TFContratoServicosVnc_Descricao',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO',pic:'@!',nv:''},{av:'AV59TFContratoServicosVnc_Descricao_Sel',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV35TFContratoSrvVnc_ServicoVncSigla',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'AV36TFContratoSrvVnc_ServicoVncSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL',pic:'@!',nv:''},{av:'AV43TFContratoSrvVnc_SrvVncCntNum',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM',pic:'',nv:''},{av:'AV44TFContratoSrvVnc_SrvVncCntNum_Sel',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM_SEL',pic:'',nv:''},{av:'AV47TFContratoSrvVnc_SemCusto_Sel',fld:'vTFCONTRATOSRVVNC_SEMCUSTO_SEL',pic:'9',nv:0},{av:'AV50TFContratoServicosVnc_Ativo_Sel',fld:'vTFCONTRATOSERVICOSVNC_ATIVO_SEL',pic:'9',nv:0},{av:'AV18ContratoSrvVnc_CntSrvCod',fld:'vCONTRATOSRVVNC_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV60ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29ddo_ContratoSrvVnc_DoStatusDmnTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_DOSTATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOVNCSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContratoSrvVnc_SrvVncStatusTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SRVVNCSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SRVVNCCNTNUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SEMCUSTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A6AreaTrabalho_Descricao',fld:'AREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'A72AreaTrabalho_Ativo',fld:'AREATRABALHO_ATIVO',pic:'',nv:false},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1297ContratadaUsuario_AreaTrabalhoDes',fld:'CONTRATADAUSUARIO_AREATRABALHODES',pic:'@!',nv:''},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV28TFContratoSrvVnc_DoStatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_DOSTATUSDMN_SELS',pic:'',nv:null},{av:'AV32TFContratoSrvVnc_StatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_STATUSDMN_SELS',pic:'',nv:null},{av:'AV40TFContratoSrvVnc_SrvVncStatus_Sels',fld:'vTFCONTRATOSRVVNC_SRVVNCSTATUS_SELS',pic:'',nv:null},{av:'A75Contrato_AreaTrabalhoCod',fld:'CONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV19AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A921ContratoSrvVnc_ServicoCod',fld:'CONTRATOSRVVNC_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV20Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A917ContratoSrvVnc_Codigo',fld:'CONTRATOSRVVNC_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A924ContratoSrvVnc_ServicoVncSigla',fld:'CONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'A1800ContratoSrvVnc_DoStatusDmn',fld:'CONTRATOSRVVNC_DOSTATUSDMN',pic:'',hsh:true,nv:''},{av:'A1084ContratoSrvVnc_StatusDmn',fld:'CONTRATOSRVVNC_STATUSDMN',pic:'',hsh:true,nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1453ContratoServicosVnc_Ativo',fld:'CONTRATOSERVICOSVNC_ATIVO',pic:'',hsh:true,nv:false},{av:'A1092ContratoSrvVnc_PrestadoraPesNom',fld:'CONTRATOSRVVNC_PRESTADORAPESNOM',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'AV21Regra',fld:'vREGRA',pic:'ZZZZZ9',nv:0}],oparms:[{av:'tblTblclonar_Visible',ctrl:'TBLCLONAR',prop:'Visible'}]}");
         setEventMetadata("VAREATRABALHO_CODIGO.CLICK","{handler:'E22GA2',iparms:[{av:'A75Contrato_AreaTrabalhoCod',fld:'CONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV19AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A921ContratoSrvVnc_ServicoCod',fld:'CONTRATOSRVVNC_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV20Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A917ContratoSrvVnc_Codigo',fld:'CONTRATOSRVVNC_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A924ContratoSrvVnc_ServicoVncSigla',fld:'CONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'A1800ContratoSrvVnc_DoStatusDmn',fld:'CONTRATOSRVVNC_DOSTATUSDMN',pic:'',hsh:true,nv:''},{av:'A1084ContratoSrvVnc_StatusDmn',fld:'CONTRATOSRVVNC_STATUSDMN',pic:'',hsh:true,nv:''}],oparms:[{av:'AV21Regra',fld:'vREGRA',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VSERVICO_CODIGO.CLICK","{handler:'E23GA2',iparms:[{av:'A75Contrato_AreaTrabalhoCod',fld:'CONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV19AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A921ContratoSrvVnc_ServicoCod',fld:'CONTRATOSRVVNC_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV20Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A917ContratoSrvVnc_Codigo',fld:'CONTRATOSRVVNC_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A924ContratoSrvVnc_ServicoVncSigla',fld:'CONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'A1800ContratoSrvVnc_DoStatusDmn',fld:'CONTRATOSRVVNC_DOSTATUSDMN',pic:'',hsh:true,nv:''},{av:'A1084ContratoSrvVnc_StatusDmn',fld:'CONTRATOSRVVNC_STATUSDMN',pic:'',hsh:true,nv:''}],oparms:[{av:'AV21Regra',fld:'vREGRA',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VREGRA.CLICK","{handler:'E28GA1',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV21Regra',fld:'vREGRA',pic:'ZZZZZ9',nv:0}],oparms:[{ctrl:'BTNCLONAR',prop:'Visible'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         Gridpaginationbar_Selectedpage = "";
         Ddo_contratoservicosvnc_descricao_Activeeventkey = "";
         Ddo_contratoservicosvnc_descricao_Filteredtext_get = "";
         Ddo_contratoservicosvnc_descricao_Selectedvalue_get = "";
         Ddo_contratosrvvnc_dostatusdmn_Activeeventkey = "";
         Ddo_contratosrvvnc_dostatusdmn_Selectedvalue_get = "";
         Ddo_contratosrvvnc_statusdmn_Activeeventkey = "";
         Ddo_contratosrvvnc_statusdmn_Selectedvalue_get = "";
         Ddo_contratosrvvnc_servicovncsigla_Activeeventkey = "";
         Ddo_contratosrvvnc_servicovncsigla_Filteredtext_get = "";
         Ddo_contratosrvvnc_servicovncsigla_Selectedvalue_get = "";
         Ddo_contratosrvvnc_srvvncstatus_Activeeventkey = "";
         Ddo_contratosrvvnc_srvvncstatus_Selectedvalue_get = "";
         Ddo_contratosrvvnc_srvvnccntnum_Activeeventkey = "";
         Ddo_contratosrvvnc_srvvnccntnum_Filteredtext_get = "";
         Ddo_contratosrvvnc_srvvnccntnum_Selectedvalue_get = "";
         Ddo_contratosrvvnc_semcusto_Activeeventkey = "";
         Ddo_contratosrvvnc_semcusto_Selectedvalue_get = "";
         Ddo_contratoservicosvnc_ativo_Activeeventkey = "";
         Ddo_contratoservicosvnc_ativo_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV58TFContratoServicosVnc_Descricao = "";
         AV59TFContratoServicosVnc_Descricao_Sel = "";
         AV35TFContratoSrvVnc_ServicoVncSigla = "";
         AV36TFContratoSrvVnc_ServicoVncSigla_Sel = "";
         AV43TFContratoSrvVnc_SrvVncCntNum = "";
         AV44TFContratoSrvVnc_SrvVncCntNum_Sel = "";
         AV60ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace = "";
         AV29ddo_ContratoSrvVnc_DoStatusDmnTitleControlIdToReplace = "";
         AV33ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace = "";
         AV37ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace = "";
         AV41ddo_ContratoSrvVnc_SrvVncStatusTitleControlIdToReplace = "";
         AV45ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace = "";
         AV48ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace = "";
         AV51ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace = "";
         A6AreaTrabalho_Descricao = "";
         A1297ContratadaUsuario_AreaTrabalhoDes = "";
         AV67Pgmname = "";
         AV28TFContratoSrvVnc_DoStatusDmn_Sels = new GxSimpleCollection();
         AV32TFContratoSrvVnc_StatusDmn_Sels = new GxSimpleCollection();
         AV40TFContratoSrvVnc_SrvVncStatus_Sels = new GxSimpleCollection();
         A924ContratoSrvVnc_ServicoVncSigla = "";
         A1800ContratoSrvVnc_DoStatusDmn = "";
         A1084ContratoSrvVnc_StatusDmn = "";
         A1092ContratoSrvVnc_PrestadoraPesNom = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV52DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV57ContratoServicosVnc_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV26ContratoSrvVnc_DoStatusDmnTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV30ContratoSrvVnc_StatusDmnTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV34ContratoSrvVnc_ServicoVncSiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38ContratoSrvVnc_SrvVncStatusTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42ContratoSrvVnc_SrvVncCntNumTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46ContratoSrvVnc_SemCustoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV49ContratoServicosVnc_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_contratoservicosvnc_descricao_Filteredtext_set = "";
         Ddo_contratoservicosvnc_descricao_Selectedvalue_set = "";
         Ddo_contratoservicosvnc_descricao_Sortedstatus = "";
         Ddo_contratosrvvnc_dostatusdmn_Selectedvalue_set = "";
         Ddo_contratosrvvnc_dostatusdmn_Sortedstatus = "";
         Ddo_contratosrvvnc_statusdmn_Selectedvalue_set = "";
         Ddo_contratosrvvnc_statusdmn_Sortedstatus = "";
         Ddo_contratosrvvnc_servicovncsigla_Filteredtext_set = "";
         Ddo_contratosrvvnc_servicovncsigla_Selectedvalue_set = "";
         Ddo_contratosrvvnc_servicovncsigla_Sortedstatus = "";
         Ddo_contratosrvvnc_srvvncstatus_Selectedvalue_set = "";
         Ddo_contratosrvvnc_srvvncstatus_Sortedstatus = "";
         Ddo_contratosrvvnc_srvvnccntnum_Filteredtext_set = "";
         Ddo_contratosrvvnc_srvvnccntnum_Selectedvalue_set = "";
         Ddo_contratosrvvnc_srvvnccntnum_Sortedstatus = "";
         Ddo_contratosrvvnc_semcusto_Selectedvalue_set = "";
         Ddo_contratosrvvnc_semcusto_Sortedstatus = "";
         Ddo_contratoservicosvnc_ativo_Selectedvalue_set = "";
         Ddo_contratoservicosvnc_ativo_Sortedstatus = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV15Update = "";
         AV65Update_GXI = "";
         AV16Delete = "";
         AV66Delete_GXI = "";
         A2108ContratoServicosVnc_Descricao = "";
         AV25Prestador = "";
         A1663ContratoSrvVnc_SrvVncStatus = "";
         A1631ContratoSrvVnc_SrvVncCntNum = "";
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00GA2_A74Contrato_Codigo = new int[1] ;
         H00GA2_A155Servico_Codigo = new int[1] ;
         H00GA2_A160ContratoServicos_Codigo = new int[1] ;
         H00GA2_A75Contrato_AreaTrabalhoCod = new int[1] ;
         H00GA2_n75Contrato_AreaTrabalhoCod = new bool[] {false} ;
         H00GA2_A827ContratoServicos_ServicoCod = new int[1] ;
         H00GA2_A826ContratoServicos_ServicoSigla = new String[] {""} ;
         GridContainer = new GXWebGrid( context);
         lV58TFContratoServicosVnc_Descricao = "";
         lV35TFContratoSrvVnc_ServicoVncSigla = "";
         lV43TFContratoSrvVnc_SrvVncCntNum = "";
         H00GA5_A1589ContratoSrvVnc_SrvVncCntSrvCod = new int[1] ;
         H00GA5_n1589ContratoSrvVnc_SrvVncCntSrvCod = new bool[] {false} ;
         H00GA5_A1628ContratoSrvVnc_SrvVncCntCod = new int[1] ;
         H00GA5_n1628ContratoSrvVnc_SrvVncCntCod = new bool[] {false} ;
         H00GA5_A1453ContratoServicosVnc_Ativo = new bool[] {false} ;
         H00GA5_n1453ContratoServicosVnc_Ativo = new bool[] {false} ;
         H00GA5_A1090ContratoSrvVnc_SemCusto = new bool[] {false} ;
         H00GA5_n1090ContratoSrvVnc_SemCusto = new bool[] {false} ;
         H00GA5_A1631ContratoSrvVnc_SrvVncCntNum = new String[] {""} ;
         H00GA5_n1631ContratoSrvVnc_SrvVncCntNum = new bool[] {false} ;
         H00GA5_A1663ContratoSrvVnc_SrvVncStatus = new String[] {""} ;
         H00GA5_n1663ContratoSrvVnc_SrvVncStatus = new bool[] {false} ;
         H00GA5_A924ContratoSrvVnc_ServicoVncSigla = new String[] {""} ;
         H00GA5_n924ContratoSrvVnc_ServicoVncSigla = new bool[] {false} ;
         H00GA5_A1084ContratoSrvVnc_StatusDmn = new String[] {""} ;
         H00GA5_n1084ContratoSrvVnc_StatusDmn = new bool[] {false} ;
         H00GA5_A1800ContratoSrvVnc_DoStatusDmn = new String[] {""} ;
         H00GA5_n1800ContratoSrvVnc_DoStatusDmn = new bool[] {false} ;
         H00GA5_A2108ContratoServicosVnc_Descricao = new String[] {""} ;
         H00GA5_n2108ContratoServicosVnc_Descricao = new bool[] {false} ;
         H00GA5_A923ContratoSrvVnc_ServicoVncCod = new int[1] ;
         H00GA5_n923ContratoSrvVnc_ServicoVncCod = new bool[] {false} ;
         H00GA5_A915ContratoSrvVnc_CntSrvCod = new int[1] ;
         H00GA5_A933ContratoSrvVnc_ContratoCod = new int[1] ;
         H00GA5_n933ContratoSrvVnc_ContratoCod = new bool[] {false} ;
         H00GA5_A917ContratoSrvVnc_Codigo = new int[1] ;
         H00GA5_A1092ContratoSrvVnc_PrestadoraPesNom = new String[] {""} ;
         H00GA5_n1092ContratoSrvVnc_PrestadoraPesNom = new bool[] {false} ;
         H00GA8_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         H00GA9_A72AreaTrabalho_Ativo = new bool[] {false} ;
         H00GA9_A5AreaTrabalho_Codigo = new int[1] ;
         H00GA9_A6AreaTrabalho_Descricao = new String[] {""} ;
         H00GA10_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00GA10_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00GA10_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         H00GA10_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         H00GA10_A1297ContratadaUsuario_AreaTrabalhoDes = new String[] {""} ;
         H00GA10_n1297ContratadaUsuario_AreaTrabalhoDes = new bool[] {false} ;
         AV27TFContratoSrvVnc_DoStatusDmn_SelsJson = "";
         AV31TFContratoSrvVnc_StatusDmn_SelsJson = "";
         AV39TFContratoSrvVnc_SrvVncStatus_SelsJson = "";
         GridRow = new GXWebRow();
         AV17Session = context.GetSession();
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         H00GA11_A1589ContratoSrvVnc_SrvVncCntSrvCod = new int[1] ;
         H00GA11_n1589ContratoSrvVnc_SrvVncCntSrvCod = new bool[] {false} ;
         H00GA11_A1628ContratoSrvVnc_SrvVncCntCod = new int[1] ;
         H00GA11_n1628ContratoSrvVnc_SrvVncCntCod = new bool[] {false} ;
         H00GA11_A923ContratoSrvVnc_ServicoVncCod = new int[1] ;
         H00GA11_n923ContratoSrvVnc_ServicoVncCod = new bool[] {false} ;
         H00GA11_A915ContratoSrvVnc_CntSrvCod = new int[1] ;
         H00GA11_A921ContratoSrvVnc_ServicoCod = new int[1] ;
         H00GA11_n921ContratoSrvVnc_ServicoCod = new bool[] {false} ;
         H00GA11_A75Contrato_AreaTrabalhoCod = new int[1] ;
         H00GA11_n75Contrato_AreaTrabalhoCod = new bool[] {false} ;
         H00GA11_A1084ContratoSrvVnc_StatusDmn = new String[] {""} ;
         H00GA11_n1084ContratoSrvVnc_StatusDmn = new bool[] {false} ;
         H00GA11_A1800ContratoSrvVnc_DoStatusDmn = new String[] {""} ;
         H00GA11_n1800ContratoSrvVnc_DoStatusDmn = new bool[] {false} ;
         H00GA11_A924ContratoSrvVnc_ServicoVncSigla = new String[] {""} ;
         H00GA11_n924ContratoSrvVnc_ServicoVncSigla = new bool[] {false} ;
         H00GA11_A917ContratoSrvVnc_Codigo = new int[1] ;
         sStyleString = "";
         imgInsert_Jsonclick = "";
         imgClonarregra_Jsonclick = "";
         lblTextblockareatrabalho_codigo_Jsonclick = "";
         lblTextblockservico_codigo_Jsonclick = "";
         lblTextblockregra_Jsonclick = "";
         bttBtnclonar_Jsonclick = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV18ContratoSrvVnc_CntSrvCod = "";
         ROClassString = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratoservicos_vncwc__default(),
            new Object[][] {
                new Object[] {
               H00GA2_A74Contrato_Codigo, H00GA2_A155Servico_Codigo, H00GA2_A160ContratoServicos_Codigo, H00GA2_A75Contrato_AreaTrabalhoCod, H00GA2_A827ContratoServicos_ServicoCod, H00GA2_A826ContratoServicos_ServicoSigla
               }
               , new Object[] {
               H00GA5_A1589ContratoSrvVnc_SrvVncCntSrvCod, H00GA5_n1589ContratoSrvVnc_SrvVncCntSrvCod, H00GA5_A1628ContratoSrvVnc_SrvVncCntCod, H00GA5_n1628ContratoSrvVnc_SrvVncCntCod, H00GA5_A1453ContratoServicosVnc_Ativo, H00GA5_n1453ContratoServicosVnc_Ativo, H00GA5_A1090ContratoSrvVnc_SemCusto, H00GA5_n1090ContratoSrvVnc_SemCusto, H00GA5_A1631ContratoSrvVnc_SrvVncCntNum, H00GA5_n1631ContratoSrvVnc_SrvVncCntNum,
               H00GA5_A1663ContratoSrvVnc_SrvVncStatus, H00GA5_n1663ContratoSrvVnc_SrvVncStatus, H00GA5_A924ContratoSrvVnc_ServicoVncSigla, H00GA5_n924ContratoSrvVnc_ServicoVncSigla, H00GA5_A1084ContratoSrvVnc_StatusDmn, H00GA5_n1084ContratoSrvVnc_StatusDmn, H00GA5_A1800ContratoSrvVnc_DoStatusDmn, H00GA5_n1800ContratoSrvVnc_DoStatusDmn, H00GA5_A2108ContratoServicosVnc_Descricao, H00GA5_n2108ContratoServicosVnc_Descricao,
               H00GA5_A923ContratoSrvVnc_ServicoVncCod, H00GA5_n923ContratoSrvVnc_ServicoVncCod, H00GA5_A915ContratoSrvVnc_CntSrvCod, H00GA5_A933ContratoSrvVnc_ContratoCod, H00GA5_n933ContratoSrvVnc_ContratoCod, H00GA5_A917ContratoSrvVnc_Codigo, H00GA5_A1092ContratoSrvVnc_PrestadoraPesNom, H00GA5_n1092ContratoSrvVnc_PrestadoraPesNom
               }
               , new Object[] {
               H00GA8_AGRID_nRecordCount
               }
               , new Object[] {
               H00GA9_A72AreaTrabalho_Ativo, H00GA9_A5AreaTrabalho_Codigo, H00GA9_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               H00GA10_A66ContratadaUsuario_ContratadaCod, H00GA10_A69ContratadaUsuario_UsuarioCod, H00GA10_A1228ContratadaUsuario_AreaTrabalhoCod, H00GA10_n1228ContratadaUsuario_AreaTrabalhoCod, H00GA10_A1297ContratadaUsuario_AreaTrabalhoDes, H00GA10_n1297ContratadaUsuario_AreaTrabalhoDes
               }
               , new Object[] {
               H00GA11_A1589ContratoSrvVnc_SrvVncCntSrvCod, H00GA11_n1589ContratoSrvVnc_SrvVncCntSrvCod, H00GA11_A1628ContratoSrvVnc_SrvVncCntCod, H00GA11_n1628ContratoSrvVnc_SrvVncCntCod, H00GA11_A923ContratoSrvVnc_ServicoVncCod, H00GA11_n923ContratoSrvVnc_ServicoVncCod, H00GA11_A915ContratoSrvVnc_CntSrvCod, H00GA11_A921ContratoSrvVnc_ServicoCod, H00GA11_n921ContratoSrvVnc_ServicoCod, H00GA11_A75Contrato_AreaTrabalhoCod,
               H00GA11_n75Contrato_AreaTrabalhoCod, H00GA11_A1084ContratoSrvVnc_StatusDmn, H00GA11_n1084ContratoSrvVnc_StatusDmn, H00GA11_A1800ContratoSrvVnc_DoStatusDmn, H00GA11_n1800ContratoSrvVnc_DoStatusDmn, H00GA11_A924ContratoSrvVnc_ServicoVncSigla, H00GA11_n924ContratoSrvVnc_ServicoVncSigla, H00GA11_A917ContratoSrvVnc_Codigo
               }
            }
         );
         AV67Pgmname = "ContratoServicos_VncWC";
         /* GeneXus formulas. */
         AV67Pgmname = "ContratoServicos_VncWC";
         context.Gx_err = 0;
         edtavPrestador_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_8 ;
      private short nGXsfl_8_idx=1 ;
      private short AV13OrderedBy ;
      private short AV47TFContratoSrvVnc_SemCusto_Sel ;
      private short AV50TFContratoServicosVnc_Ativo_Sel ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_8_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContratoServicosVnc_Descricao_Titleformat ;
      private short cmbContratoSrvVnc_DoStatusDmn_Titleformat ;
      private short cmbContratoSrvVnc_StatusDmn_Titleformat ;
      private short edtContratoSrvVnc_ServicoVncSigla_Titleformat ;
      private short cmbContratoSrvVnc_SrvVncStatus_Titleformat ;
      private short edtContratoSrvVnc_SrvVncCntNum_Titleformat ;
      private short cmbContratoSrvVnc_SemCusto_Titleformat ;
      private short chkContratoServicosVnc_Ativo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private short wbTemp ;
      private int AV18ContratoSrvVnc_CntSrvCod ;
      private int wcpOAV18ContratoSrvVnc_CntSrvCod ;
      private int AV19AreaTrabalho_Codigo ;
      private int subGrid_Rows ;
      private int A5AreaTrabalho_Codigo ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A1228ContratadaUsuario_AreaTrabalhoCod ;
      private int A75Contrato_AreaTrabalhoCod ;
      private int A921ContratoSrvVnc_ServicoCod ;
      private int AV20Servico_Codigo ;
      private int A917ContratoSrvVnc_Codigo ;
      private int edtavPrestador_Enabled ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_contratoservicosvnc_descricao_Datalistupdateminimumcharacters ;
      private int Ddo_contratosrvvnc_servicovncsigla_Datalistupdateminimumcharacters ;
      private int Ddo_contratosrvvnc_srvvnccntnum_Datalistupdateminimumcharacters ;
      private int edtavOrderedby_Visible ;
      private int edtavOrdereddsc_Visible ;
      private int edtavTfcontratoservicosvnc_descricao_Visible ;
      private int edtavTfcontratoservicosvnc_descricao_sel_Visible ;
      private int edtavTfcontratosrvvnc_servicovncsigla_Visible ;
      private int edtavTfcontratosrvvnc_servicovncsigla_sel_Visible ;
      private int edtavTfcontratosrvvnc_srvvnccntnum_Visible ;
      private int edtavTfcontratosrvvnc_srvvnccntnum_sel_Visible ;
      private int edtavTfcontratosrvvnc_semcusto_sel_Visible ;
      private int edtavTfcontratoservicosvnc_ativo_sel_Visible ;
      private int edtavDdo_contratoservicosvnc_descricaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratosrvvnc_dostatusdmntitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratosrvvnc_statusdmntitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratosrvvnc_servicovncsiglatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratosrvvnc_srvvncstatustitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratosrvvnc_srvvnccntnumtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratosrvvnc_semcustotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicosvnc_ativotitlecontrolidtoreplace_Visible ;
      private int A933ContratoSrvVnc_ContratoCod ;
      private int A915ContratoSrvVnc_CntSrvCod ;
      private int A923ContratoSrvVnc_ServicoVncCod ;
      private int AV21Regra ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV28TFContratoSrvVnc_DoStatusDmn_Sels_Count ;
      private int AV32TFContratoSrvVnc_StatusDmn_Sels_Count ;
      private int AV40TFContratoSrvVnc_SrvVncStatus_Sels_Count ;
      private int A1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private int A1628ContratoSrvVnc_SrvVncCntCod ;
      private int tblTblclonar_Visible ;
      private int bttBtnclonar_Visible ;
      private int edtavUpdate_Visible ;
      private int edtavDelete_Visible ;
      private int imgInsert_Visible ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int AV53PageToGo ;
      private int edtContratoSrvVnc_ServicoVncSigla_Forecolor ;
      private int edtavPrestador_Forecolor ;
      private int edtContratoSrvVnc_SrvVncCntNum_Forecolor ;
      private int AV68GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV54GridCurrentPage ;
      private long AV55GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private long AV56Color ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contratoservicosvnc_descricao_Activeeventkey ;
      private String Ddo_contratoservicosvnc_descricao_Filteredtext_get ;
      private String Ddo_contratoservicosvnc_descricao_Selectedvalue_get ;
      private String Ddo_contratosrvvnc_dostatusdmn_Activeeventkey ;
      private String Ddo_contratosrvvnc_dostatusdmn_Selectedvalue_get ;
      private String Ddo_contratosrvvnc_statusdmn_Activeeventkey ;
      private String Ddo_contratosrvvnc_statusdmn_Selectedvalue_get ;
      private String Ddo_contratosrvvnc_servicovncsigla_Activeeventkey ;
      private String Ddo_contratosrvvnc_servicovncsigla_Filteredtext_get ;
      private String Ddo_contratosrvvnc_servicovncsigla_Selectedvalue_get ;
      private String Ddo_contratosrvvnc_srvvncstatus_Activeeventkey ;
      private String Ddo_contratosrvvnc_srvvncstatus_Selectedvalue_get ;
      private String Ddo_contratosrvvnc_srvvnccntnum_Activeeventkey ;
      private String Ddo_contratosrvvnc_srvvnccntnum_Filteredtext_get ;
      private String Ddo_contratosrvvnc_srvvnccntnum_Selectedvalue_get ;
      private String Ddo_contratosrvvnc_semcusto_Activeeventkey ;
      private String Ddo_contratosrvvnc_semcusto_Selectedvalue_get ;
      private String Ddo_contratoservicosvnc_ativo_Activeeventkey ;
      private String Ddo_contratoservicosvnc_ativo_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_8_idx="0001" ;
      private String AV35TFContratoSrvVnc_ServicoVncSigla ;
      private String AV36TFContratoSrvVnc_ServicoVncSigla_Sel ;
      private String AV43TFContratoSrvVnc_SrvVncCntNum ;
      private String AV44TFContratoSrvVnc_SrvVncCntNum_Sel ;
      private String AV67Pgmname ;
      private String A924ContratoSrvVnc_ServicoVncSigla ;
      private String A1800ContratoSrvVnc_DoStatusDmn ;
      private String A1084ContratoSrvVnc_StatusDmn ;
      private String A1092ContratoSrvVnc_PrestadoraPesNom ;
      private String GXKey ;
      private String edtavPrestador_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contratoservicosvnc_descricao_Caption ;
      private String Ddo_contratoservicosvnc_descricao_Tooltip ;
      private String Ddo_contratoservicosvnc_descricao_Cls ;
      private String Ddo_contratoservicosvnc_descricao_Filteredtext_set ;
      private String Ddo_contratoservicosvnc_descricao_Selectedvalue_set ;
      private String Ddo_contratoservicosvnc_descricao_Dropdownoptionstype ;
      private String Ddo_contratoservicosvnc_descricao_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosvnc_descricao_Sortedstatus ;
      private String Ddo_contratoservicosvnc_descricao_Filtertype ;
      private String Ddo_contratoservicosvnc_descricao_Datalisttype ;
      private String Ddo_contratoservicosvnc_descricao_Datalistproc ;
      private String Ddo_contratoservicosvnc_descricao_Sortasc ;
      private String Ddo_contratoservicosvnc_descricao_Sortdsc ;
      private String Ddo_contratoservicosvnc_descricao_Loadingdata ;
      private String Ddo_contratoservicosvnc_descricao_Cleanfilter ;
      private String Ddo_contratoservicosvnc_descricao_Noresultsfound ;
      private String Ddo_contratoservicosvnc_descricao_Searchbuttontext ;
      private String Ddo_contratosrvvnc_dostatusdmn_Caption ;
      private String Ddo_contratosrvvnc_dostatusdmn_Tooltip ;
      private String Ddo_contratosrvvnc_dostatusdmn_Cls ;
      private String Ddo_contratosrvvnc_dostatusdmn_Selectedvalue_set ;
      private String Ddo_contratosrvvnc_dostatusdmn_Dropdownoptionstype ;
      private String Ddo_contratosrvvnc_dostatusdmn_Titlecontrolidtoreplace ;
      private String Ddo_contratosrvvnc_dostatusdmn_Sortedstatus ;
      private String Ddo_contratosrvvnc_dostatusdmn_Datalisttype ;
      private String Ddo_contratosrvvnc_dostatusdmn_Datalistfixedvalues ;
      private String Ddo_contratosrvvnc_dostatusdmn_Sortasc ;
      private String Ddo_contratosrvvnc_dostatusdmn_Sortdsc ;
      private String Ddo_contratosrvvnc_dostatusdmn_Cleanfilter ;
      private String Ddo_contratosrvvnc_dostatusdmn_Searchbuttontext ;
      private String Ddo_contratosrvvnc_statusdmn_Caption ;
      private String Ddo_contratosrvvnc_statusdmn_Tooltip ;
      private String Ddo_contratosrvvnc_statusdmn_Cls ;
      private String Ddo_contratosrvvnc_statusdmn_Selectedvalue_set ;
      private String Ddo_contratosrvvnc_statusdmn_Dropdownoptionstype ;
      private String Ddo_contratosrvvnc_statusdmn_Titlecontrolidtoreplace ;
      private String Ddo_contratosrvvnc_statusdmn_Sortedstatus ;
      private String Ddo_contratosrvvnc_statusdmn_Datalisttype ;
      private String Ddo_contratosrvvnc_statusdmn_Datalistfixedvalues ;
      private String Ddo_contratosrvvnc_statusdmn_Sortasc ;
      private String Ddo_contratosrvvnc_statusdmn_Sortdsc ;
      private String Ddo_contratosrvvnc_statusdmn_Cleanfilter ;
      private String Ddo_contratosrvvnc_statusdmn_Searchbuttontext ;
      private String Ddo_contratosrvvnc_servicovncsigla_Caption ;
      private String Ddo_contratosrvvnc_servicovncsigla_Tooltip ;
      private String Ddo_contratosrvvnc_servicovncsigla_Cls ;
      private String Ddo_contratosrvvnc_servicovncsigla_Filteredtext_set ;
      private String Ddo_contratosrvvnc_servicovncsigla_Selectedvalue_set ;
      private String Ddo_contratosrvvnc_servicovncsigla_Dropdownoptionstype ;
      private String Ddo_contratosrvvnc_servicovncsigla_Titlecontrolidtoreplace ;
      private String Ddo_contratosrvvnc_servicovncsigla_Sortedstatus ;
      private String Ddo_contratosrvvnc_servicovncsigla_Filtertype ;
      private String Ddo_contratosrvvnc_servicovncsigla_Datalisttype ;
      private String Ddo_contratosrvvnc_servicovncsigla_Datalistproc ;
      private String Ddo_contratosrvvnc_servicovncsigla_Sortasc ;
      private String Ddo_contratosrvvnc_servicovncsigla_Sortdsc ;
      private String Ddo_contratosrvvnc_servicovncsigla_Loadingdata ;
      private String Ddo_contratosrvvnc_servicovncsigla_Cleanfilter ;
      private String Ddo_contratosrvvnc_servicovncsigla_Noresultsfound ;
      private String Ddo_contratosrvvnc_servicovncsigla_Searchbuttontext ;
      private String Ddo_contratosrvvnc_srvvncstatus_Caption ;
      private String Ddo_contratosrvvnc_srvvncstatus_Tooltip ;
      private String Ddo_contratosrvvnc_srvvncstatus_Cls ;
      private String Ddo_contratosrvvnc_srvvncstatus_Selectedvalue_set ;
      private String Ddo_contratosrvvnc_srvvncstatus_Dropdownoptionstype ;
      private String Ddo_contratosrvvnc_srvvncstatus_Titlecontrolidtoreplace ;
      private String Ddo_contratosrvvnc_srvvncstatus_Sortedstatus ;
      private String Ddo_contratosrvvnc_srvvncstatus_Datalisttype ;
      private String Ddo_contratosrvvnc_srvvncstatus_Datalistfixedvalues ;
      private String Ddo_contratosrvvnc_srvvncstatus_Sortasc ;
      private String Ddo_contratosrvvnc_srvvncstatus_Sortdsc ;
      private String Ddo_contratosrvvnc_srvvncstatus_Cleanfilter ;
      private String Ddo_contratosrvvnc_srvvncstatus_Searchbuttontext ;
      private String Ddo_contratosrvvnc_srvvnccntnum_Caption ;
      private String Ddo_contratosrvvnc_srvvnccntnum_Tooltip ;
      private String Ddo_contratosrvvnc_srvvnccntnum_Cls ;
      private String Ddo_contratosrvvnc_srvvnccntnum_Filteredtext_set ;
      private String Ddo_contratosrvvnc_srvvnccntnum_Selectedvalue_set ;
      private String Ddo_contratosrvvnc_srvvnccntnum_Dropdownoptionstype ;
      private String Ddo_contratosrvvnc_srvvnccntnum_Titlecontrolidtoreplace ;
      private String Ddo_contratosrvvnc_srvvnccntnum_Sortedstatus ;
      private String Ddo_contratosrvvnc_srvvnccntnum_Filtertype ;
      private String Ddo_contratosrvvnc_srvvnccntnum_Datalisttype ;
      private String Ddo_contratosrvvnc_srvvnccntnum_Datalistproc ;
      private String Ddo_contratosrvvnc_srvvnccntnum_Sortasc ;
      private String Ddo_contratosrvvnc_srvvnccntnum_Sortdsc ;
      private String Ddo_contratosrvvnc_srvvnccntnum_Loadingdata ;
      private String Ddo_contratosrvvnc_srvvnccntnum_Cleanfilter ;
      private String Ddo_contratosrvvnc_srvvnccntnum_Noresultsfound ;
      private String Ddo_contratosrvvnc_srvvnccntnum_Searchbuttontext ;
      private String Ddo_contratosrvvnc_semcusto_Caption ;
      private String Ddo_contratosrvvnc_semcusto_Tooltip ;
      private String Ddo_contratosrvvnc_semcusto_Cls ;
      private String Ddo_contratosrvvnc_semcusto_Selectedvalue_set ;
      private String Ddo_contratosrvvnc_semcusto_Dropdownoptionstype ;
      private String Ddo_contratosrvvnc_semcusto_Titlecontrolidtoreplace ;
      private String Ddo_contratosrvvnc_semcusto_Sortedstatus ;
      private String Ddo_contratosrvvnc_semcusto_Datalisttype ;
      private String Ddo_contratosrvvnc_semcusto_Datalistfixedvalues ;
      private String Ddo_contratosrvvnc_semcusto_Sortasc ;
      private String Ddo_contratosrvvnc_semcusto_Sortdsc ;
      private String Ddo_contratosrvvnc_semcusto_Cleanfilter ;
      private String Ddo_contratosrvvnc_semcusto_Searchbuttontext ;
      private String Ddo_contratoservicosvnc_ativo_Caption ;
      private String Ddo_contratoservicosvnc_ativo_Tooltip ;
      private String Ddo_contratoservicosvnc_ativo_Cls ;
      private String Ddo_contratoservicosvnc_ativo_Selectedvalue_set ;
      private String Ddo_contratoservicosvnc_ativo_Dropdownoptionstype ;
      private String Ddo_contratoservicosvnc_ativo_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosvnc_ativo_Sortedstatus ;
      private String Ddo_contratoservicosvnc_ativo_Datalisttype ;
      private String Ddo_contratoservicosvnc_ativo_Datalistfixedvalues ;
      private String Ddo_contratoservicosvnc_ativo_Sortasc ;
      private String Ddo_contratoservicosvnc_ativo_Sortdsc ;
      private String Ddo_contratoservicosvnc_ativo_Cleanfilter ;
      private String Ddo_contratoservicosvnc_ativo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String TempTags ;
      private String edtavOrderedby_Internalname ;
      private String edtavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String edtavTfcontratoservicosvnc_descricao_Internalname ;
      private String edtavTfcontratoservicosvnc_descricao_Jsonclick ;
      private String edtavTfcontratoservicosvnc_descricao_sel_Internalname ;
      private String edtavTfcontratoservicosvnc_descricao_sel_Jsonclick ;
      private String edtavTfcontratosrvvnc_servicovncsigla_Internalname ;
      private String edtavTfcontratosrvvnc_servicovncsigla_Jsonclick ;
      private String edtavTfcontratosrvvnc_servicovncsigla_sel_Internalname ;
      private String edtavTfcontratosrvvnc_servicovncsigla_sel_Jsonclick ;
      private String edtavTfcontratosrvvnc_srvvnccntnum_Internalname ;
      private String edtavTfcontratosrvvnc_srvvnccntnum_Jsonclick ;
      private String edtavTfcontratosrvvnc_srvvnccntnum_sel_Internalname ;
      private String edtavTfcontratosrvvnc_srvvnccntnum_sel_Jsonclick ;
      private String edtavTfcontratosrvvnc_semcusto_sel_Internalname ;
      private String edtavTfcontratosrvvnc_semcusto_sel_Jsonclick ;
      private String edtavTfcontratoservicosvnc_ativo_sel_Internalname ;
      private String edtavTfcontratoservicosvnc_ativo_sel_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_contratoservicosvnc_descricaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratosrvvnc_dostatusdmntitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratosrvvnc_statusdmntitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratosrvvnc_servicovncsiglatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratosrvvnc_srvvncstatustitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratosrvvnc_srvvnccntnumtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratosrvvnc_semcustotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicosvnc_ativotitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String cmbavAreatrabalho_codigo_Internalname ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtContratoSrvVnc_Codigo_Internalname ;
      private String edtContratoSrvVnc_ContratoCod_Internalname ;
      private String edtContratoSrvVnc_CntSrvCod_Internalname ;
      private String edtContratoSrvVnc_ServicoVncCod_Internalname ;
      private String edtContratoServicosVnc_Descricao_Internalname ;
      private String cmbContratoSrvVnc_DoStatusDmn_Internalname ;
      private String cmbContratoSrvVnc_StatusDmn_Internalname ;
      private String edtContratoSrvVnc_ServicoVncSigla_Internalname ;
      private String AV25Prestador ;
      private String cmbContratoSrvVnc_SrvVncStatus_Internalname ;
      private String A1663ContratoSrvVnc_SrvVncStatus ;
      private String A1631ContratoSrvVnc_SrvVncCntNum ;
      private String edtContratoSrvVnc_SrvVncCntNum_Internalname ;
      private String cmbContratoSrvVnc_SemCusto_Internalname ;
      private String chkContratoServicosVnc_Ativo_Internalname ;
      private String GXCCtl ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String lV35TFContratoSrvVnc_ServicoVncSigla ;
      private String lV43TFContratoSrvVnc_SrvVncCntNum ;
      private String dynavServico_codigo_Internalname ;
      private String cmbavRegra_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_contratoservicosvnc_descricao_Internalname ;
      private String Ddo_contratosrvvnc_dostatusdmn_Internalname ;
      private String Ddo_contratosrvvnc_statusdmn_Internalname ;
      private String Ddo_contratosrvvnc_servicovncsigla_Internalname ;
      private String Ddo_contratosrvvnc_srvvncstatus_Internalname ;
      private String Ddo_contratosrvvnc_srvvnccntnum_Internalname ;
      private String Ddo_contratosrvvnc_semcusto_Internalname ;
      private String Ddo_contratoservicosvnc_ativo_Internalname ;
      private String tblTblclonar_Internalname ;
      private String bttBtnclonar_Internalname ;
      private String edtContratoServicosVnc_Descricao_Title ;
      private String edtContratoSrvVnc_ServicoVncSigla_Title ;
      private String edtContratoSrvVnc_SrvVncCntNum_Title ;
      private String imgInsert_Internalname ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String sStyleString ;
      private String tblUnnamedtable1_Internalname ;
      private String tblUnnamedtable2_Internalname ;
      private String imgInsert_Jsonclick ;
      private String imgClonarregra_Internalname ;
      private String imgClonarregra_Jsonclick ;
      private String lblTextblockareatrabalho_codigo_Internalname ;
      private String lblTextblockareatrabalho_codigo_Jsonclick ;
      private String cmbavAreatrabalho_codigo_Jsonclick ;
      private String lblTextblockservico_codigo_Internalname ;
      private String lblTextblockservico_codigo_Jsonclick ;
      private String dynavServico_codigo_Jsonclick ;
      private String lblTextblockregra_Internalname ;
      private String lblTextblockregra_Jsonclick ;
      private String cmbavRegra_Jsonclick ;
      private String bttBtnclonar_Jsonclick ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String sCtrlAV18ContratoSrvVnc_CntSrvCod ;
      private String sGXsfl_8_fel_idx="0001" ;
      private String ROClassString ;
      private String edtContratoSrvVnc_Codigo_Jsonclick ;
      private String edtContratoSrvVnc_ContratoCod_Jsonclick ;
      private String edtContratoSrvVnc_CntSrvCod_Jsonclick ;
      private String edtContratoSrvVnc_ServicoVncCod_Jsonclick ;
      private String edtContratoServicosVnc_Descricao_Jsonclick ;
      private String cmbContratoSrvVnc_DoStatusDmn_Jsonclick ;
      private String cmbContratoSrvVnc_StatusDmn_Jsonclick ;
      private String edtContratoSrvVnc_ServicoVncSigla_Jsonclick ;
      private String edtavPrestador_Jsonclick ;
      private String cmbContratoSrvVnc_SrvVncStatus_Jsonclick ;
      private String edtContratoSrvVnc_SrvVncCntNum_Jsonclick ;
      private String cmbContratoSrvVnc_SemCusto_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool A72AreaTrabalho_Ativo ;
      private bool n1297ContratadaUsuario_AreaTrabalhoDes ;
      private bool n1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool n75Contrato_AreaTrabalhoCod ;
      private bool n921ContratoSrvVnc_ServicoCod ;
      private bool n924ContratoSrvVnc_ServicoVncSigla ;
      private bool n1800ContratoSrvVnc_DoStatusDmn ;
      private bool n1084ContratoSrvVnc_StatusDmn ;
      private bool A1453ContratoServicosVnc_Ativo ;
      private bool n1453ContratoServicosVnc_Ativo ;
      private bool n1092ContratoSrvVnc_PrestadoraPesNom ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contratoservicosvnc_descricao_Includesortasc ;
      private bool Ddo_contratoservicosvnc_descricao_Includesortdsc ;
      private bool Ddo_contratoservicosvnc_descricao_Includefilter ;
      private bool Ddo_contratoservicosvnc_descricao_Filterisrange ;
      private bool Ddo_contratoservicosvnc_descricao_Includedatalist ;
      private bool Ddo_contratosrvvnc_dostatusdmn_Includesortasc ;
      private bool Ddo_contratosrvvnc_dostatusdmn_Includesortdsc ;
      private bool Ddo_contratosrvvnc_dostatusdmn_Includefilter ;
      private bool Ddo_contratosrvvnc_dostatusdmn_Includedatalist ;
      private bool Ddo_contratosrvvnc_dostatusdmn_Allowmultipleselection ;
      private bool Ddo_contratosrvvnc_statusdmn_Includesortasc ;
      private bool Ddo_contratosrvvnc_statusdmn_Includesortdsc ;
      private bool Ddo_contratosrvvnc_statusdmn_Includefilter ;
      private bool Ddo_contratosrvvnc_statusdmn_Includedatalist ;
      private bool Ddo_contratosrvvnc_statusdmn_Allowmultipleselection ;
      private bool Ddo_contratosrvvnc_servicovncsigla_Includesortasc ;
      private bool Ddo_contratosrvvnc_servicovncsigla_Includesortdsc ;
      private bool Ddo_contratosrvvnc_servicovncsigla_Includefilter ;
      private bool Ddo_contratosrvvnc_servicovncsigla_Filterisrange ;
      private bool Ddo_contratosrvvnc_servicovncsigla_Includedatalist ;
      private bool Ddo_contratosrvvnc_srvvncstatus_Includesortasc ;
      private bool Ddo_contratosrvvnc_srvvncstatus_Includesortdsc ;
      private bool Ddo_contratosrvvnc_srvvncstatus_Includefilter ;
      private bool Ddo_contratosrvvnc_srvvncstatus_Includedatalist ;
      private bool Ddo_contratosrvvnc_srvvncstatus_Allowmultipleselection ;
      private bool Ddo_contratosrvvnc_srvvnccntnum_Includesortasc ;
      private bool Ddo_contratosrvvnc_srvvnccntnum_Includesortdsc ;
      private bool Ddo_contratosrvvnc_srvvnccntnum_Includefilter ;
      private bool Ddo_contratosrvvnc_srvvnccntnum_Filterisrange ;
      private bool Ddo_contratosrvvnc_srvvnccntnum_Includedatalist ;
      private bool Ddo_contratosrvvnc_semcusto_Includesortasc ;
      private bool Ddo_contratosrvvnc_semcusto_Includesortdsc ;
      private bool Ddo_contratosrvvnc_semcusto_Includefilter ;
      private bool Ddo_contratosrvvnc_semcusto_Includedatalist ;
      private bool Ddo_contratoservicosvnc_ativo_Includesortasc ;
      private bool Ddo_contratoservicosvnc_ativo_Includesortdsc ;
      private bool Ddo_contratoservicosvnc_ativo_Includefilter ;
      private bool Ddo_contratoservicosvnc_ativo_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n933ContratoSrvVnc_ContratoCod ;
      private bool n923ContratoSrvVnc_ServicoVncCod ;
      private bool n2108ContratoServicosVnc_Descricao ;
      private bool n1663ContratoSrvVnc_SrvVncStatus ;
      private bool n1631ContratoSrvVnc_SrvVncCntNum ;
      private bool A1090ContratoSrvVnc_SemCusto ;
      private bool n1090ContratoSrvVnc_SemCusto ;
      private bool n1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private bool n1628ContratoSrvVnc_SrvVncCntCod ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV15Update_IsBlob ;
      private bool AV16Delete_IsBlob ;
      private String AV27TFContratoSrvVnc_DoStatusDmn_SelsJson ;
      private String AV31TFContratoSrvVnc_StatusDmn_SelsJson ;
      private String AV39TFContratoSrvVnc_SrvVncStatus_SelsJson ;
      private String AV58TFContratoServicosVnc_Descricao ;
      private String AV59TFContratoServicosVnc_Descricao_Sel ;
      private String AV60ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace ;
      private String AV29ddo_ContratoSrvVnc_DoStatusDmnTitleControlIdToReplace ;
      private String AV33ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace ;
      private String AV37ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace ;
      private String AV41ddo_ContratoSrvVnc_SrvVncStatusTitleControlIdToReplace ;
      private String AV45ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace ;
      private String AV48ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace ;
      private String AV51ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace ;
      private String A6AreaTrabalho_Descricao ;
      private String A1297ContratadaUsuario_AreaTrabalhoDes ;
      private String AV65Update_GXI ;
      private String AV66Delete_GXI ;
      private String A2108ContratoServicosVnc_Descricao ;
      private String lV58TFContratoServicosVnc_Descricao ;
      private String AV15Update ;
      private String AV16Delete ;
      private IGxSession AV17Session ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbContratoSrvVnc_DoStatusDmn ;
      private GXCombobox cmbContratoSrvVnc_StatusDmn ;
      private GXCombobox cmbContratoSrvVnc_SrvVncStatus ;
      private GXCombobox cmbContratoSrvVnc_SemCusto ;
      private GXCheckbox chkContratoServicosVnc_Ativo ;
      private GXCombobox cmbavAreatrabalho_codigo ;
      private GXCombobox dynavServico_codigo ;
      private GXCombobox cmbavRegra ;
      private IDataStoreProvider pr_default ;
      private int[] H00GA2_A74Contrato_Codigo ;
      private int[] H00GA2_A155Servico_Codigo ;
      private int[] H00GA2_A160ContratoServicos_Codigo ;
      private int[] H00GA2_A75Contrato_AreaTrabalhoCod ;
      private bool[] H00GA2_n75Contrato_AreaTrabalhoCod ;
      private int[] H00GA2_A827ContratoServicos_ServicoCod ;
      private String[] H00GA2_A826ContratoServicos_ServicoSigla ;
      private int[] H00GA5_A1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private bool[] H00GA5_n1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private int[] H00GA5_A1628ContratoSrvVnc_SrvVncCntCod ;
      private bool[] H00GA5_n1628ContratoSrvVnc_SrvVncCntCod ;
      private bool[] H00GA5_A1453ContratoServicosVnc_Ativo ;
      private bool[] H00GA5_n1453ContratoServicosVnc_Ativo ;
      private bool[] H00GA5_A1090ContratoSrvVnc_SemCusto ;
      private bool[] H00GA5_n1090ContratoSrvVnc_SemCusto ;
      private String[] H00GA5_A1631ContratoSrvVnc_SrvVncCntNum ;
      private bool[] H00GA5_n1631ContratoSrvVnc_SrvVncCntNum ;
      private String[] H00GA5_A1663ContratoSrvVnc_SrvVncStatus ;
      private bool[] H00GA5_n1663ContratoSrvVnc_SrvVncStatus ;
      private String[] H00GA5_A924ContratoSrvVnc_ServicoVncSigla ;
      private bool[] H00GA5_n924ContratoSrvVnc_ServicoVncSigla ;
      private String[] H00GA5_A1084ContratoSrvVnc_StatusDmn ;
      private bool[] H00GA5_n1084ContratoSrvVnc_StatusDmn ;
      private String[] H00GA5_A1800ContratoSrvVnc_DoStatusDmn ;
      private bool[] H00GA5_n1800ContratoSrvVnc_DoStatusDmn ;
      private String[] H00GA5_A2108ContratoServicosVnc_Descricao ;
      private bool[] H00GA5_n2108ContratoServicosVnc_Descricao ;
      private int[] H00GA5_A923ContratoSrvVnc_ServicoVncCod ;
      private bool[] H00GA5_n923ContratoSrvVnc_ServicoVncCod ;
      private int[] H00GA5_A915ContratoSrvVnc_CntSrvCod ;
      private int[] H00GA5_A933ContratoSrvVnc_ContratoCod ;
      private bool[] H00GA5_n933ContratoSrvVnc_ContratoCod ;
      private int[] H00GA5_A917ContratoSrvVnc_Codigo ;
      private String[] H00GA5_A1092ContratoSrvVnc_PrestadoraPesNom ;
      private bool[] H00GA5_n1092ContratoSrvVnc_PrestadoraPesNom ;
      private long[] H00GA8_AGRID_nRecordCount ;
      private bool[] H00GA9_A72AreaTrabalho_Ativo ;
      private int[] H00GA9_A5AreaTrabalho_Codigo ;
      private String[] H00GA9_A6AreaTrabalho_Descricao ;
      private int[] H00GA10_A66ContratadaUsuario_ContratadaCod ;
      private int[] H00GA10_A69ContratadaUsuario_UsuarioCod ;
      private int[] H00GA10_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] H00GA10_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private String[] H00GA10_A1297ContratadaUsuario_AreaTrabalhoDes ;
      private bool[] H00GA10_n1297ContratadaUsuario_AreaTrabalhoDes ;
      private int[] H00GA11_A1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private bool[] H00GA11_n1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private int[] H00GA11_A1628ContratoSrvVnc_SrvVncCntCod ;
      private bool[] H00GA11_n1628ContratoSrvVnc_SrvVncCntCod ;
      private int[] H00GA11_A923ContratoSrvVnc_ServicoVncCod ;
      private bool[] H00GA11_n923ContratoSrvVnc_ServicoVncCod ;
      private int[] H00GA11_A915ContratoSrvVnc_CntSrvCod ;
      private int[] H00GA11_A921ContratoSrvVnc_ServicoCod ;
      private bool[] H00GA11_n921ContratoSrvVnc_ServicoCod ;
      private int[] H00GA11_A75Contrato_AreaTrabalhoCod ;
      private bool[] H00GA11_n75Contrato_AreaTrabalhoCod ;
      private String[] H00GA11_A1084ContratoSrvVnc_StatusDmn ;
      private bool[] H00GA11_n1084ContratoSrvVnc_StatusDmn ;
      private String[] H00GA11_A1800ContratoSrvVnc_DoStatusDmn ;
      private bool[] H00GA11_n1800ContratoSrvVnc_DoStatusDmn ;
      private String[] H00GA11_A924ContratoSrvVnc_ServicoVncSigla ;
      private bool[] H00GA11_n924ContratoSrvVnc_ServicoVncSigla ;
      private int[] H00GA11_A917ContratoSrvVnc_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV28TFContratoSrvVnc_DoStatusDmn_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV32TFContratoSrvVnc_StatusDmn_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV40TFContratoSrvVnc_SrvVncStatus_Sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV57ContratoServicosVnc_DescricaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV26ContratoSrvVnc_DoStatusDmnTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV30ContratoSrvVnc_StatusDmnTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV34ContratoSrvVnc_ServicoVncSiglaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV38ContratoSrvVnc_SrvVncStatusTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV42ContratoSrvVnc_SrvVncCntNumTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV46ContratoSrvVnc_SemCustoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV49ContratoServicosVnc_AtivoTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV52DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class contratoservicos_vncwc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00GA5( IGxContext context ,
                                             String A1800ContratoSrvVnc_DoStatusDmn ,
                                             IGxCollection AV28TFContratoSrvVnc_DoStatusDmn_Sels ,
                                             String A1084ContratoSrvVnc_StatusDmn ,
                                             IGxCollection AV32TFContratoSrvVnc_StatusDmn_Sels ,
                                             String A1663ContratoSrvVnc_SrvVncStatus ,
                                             IGxCollection AV40TFContratoSrvVnc_SrvVncStatus_Sels ,
                                             String AV59TFContratoServicosVnc_Descricao_Sel ,
                                             String AV58TFContratoServicosVnc_Descricao ,
                                             int AV28TFContratoSrvVnc_DoStatusDmn_Sels_Count ,
                                             int AV32TFContratoSrvVnc_StatusDmn_Sels_Count ,
                                             String AV36TFContratoSrvVnc_ServicoVncSigla_Sel ,
                                             String AV35TFContratoSrvVnc_ServicoVncSigla ,
                                             int AV40TFContratoSrvVnc_SrvVncStatus_Sels_Count ,
                                             String AV44TFContratoSrvVnc_SrvVncCntNum_Sel ,
                                             String AV43TFContratoSrvVnc_SrvVncCntNum ,
                                             short AV47TFContratoSrvVnc_SemCusto_Sel ,
                                             short AV50TFContratoServicosVnc_Ativo_Sel ,
                                             String A2108ContratoServicosVnc_Descricao ,
                                             String A924ContratoSrvVnc_ServicoVncSigla ,
                                             String A1631ContratoSrvVnc_SrvVncCntNum ,
                                             bool A1090ContratoSrvVnc_SemCusto ,
                                             bool A1453ContratoServicosVnc_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A915ContratoSrvVnc_CntSrvCod ,
                                             int AV18ContratoSrvVnc_CntSrvCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [12] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[ContratoSrvVnc_SrvVncCntSrvCod] AS ContratoSrvVnc_SrvVncCntSrvCod, T2.[Contrato_Codigo] AS ContratoSrvVnc_SrvVncCntCod, T1.[ContratoServicosVnc_Ativo], T1.[ContratoSrvVnc_SemCusto], T3.[Contrato_Numero] AS ContratoSrvVnc_SrvVncCntNum, T1.[ContratoSrvVnc_SrvVncStatus], T4.[Servico_Sigla] AS ContratoSrvVnc_ServicoVncSigla, T1.[ContratoSrvVnc_StatusDmn], T1.[ContratoSrvVnc_DoStatusDmn], T1.[ContratoServicosVnc_Descricao], T2.[Servico_Codigo] AS ContratoSrvVnc_ServicoVncCod, T1.[ContratoSrvVnc_CntSrvCod] AS ContratoSrvVnc_CntSrvCod, T5.[Contrato_Codigo] AS ContratoSrvVnc_ContratoCod, T1.[ContratoSrvVnc_Codigo], COALESCE( T6.[ContratoSrvVnc_PrestadoraPesNom], '') AS ContratoSrvVnc_PrestadoraPesNom";
         sFromString = " FROM ((((([ContratoServicosVnc] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoSrvVnc_SrvVncCntSrvCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN [Servico] T4 WITH (NOLOCK) ON T4.[Servico_Codigo] = T2.[Servico_Codigo]) INNER JOIN [ContratoServicos] T5 WITH (NOLOCK) ON T5.[ContratoServicos_Codigo] = T1.[ContratoSrvVnc_CntSrvCod]) LEFT JOIN (SELECT CASE  WHEN COALESCE( T7.[ContratoSrvVnc_PrestadoraCod], 0) < 910000 THEN COALESCE( T8.[Contratada_Sigla], '') WHEN COALESCE( T7.[ContratoSrvVnc_PrestadoraCod], 0) = 910000 THEN 'Criador da SS' WHEN COALESCE( T7.[ContratoSrvVnc_PrestadoraCod], 0) = 920000 THEN 'Criador da OS' END AS ContratoSrvVnc_PrestadoraPesNom, T7.[ContratoSrvVnc_Codigo] FROM ([ContratoServicosVnc] T7 WITH (NOLOCK) LEFT JOIN (SELECT [Contratada_Sigla] AS Contratada_Sigla, [Contratada_Codigo] FROM [Contratada] WITH (NOLOCK) ) T8 ON T8.[Contratada_Codigo] = T7.[ContratoSrvVnc_PrestadoraCod]) ) T6 ON T6.[ContratoSrvVnc_Codigo] = T1.[ContratoSrvVnc_Codigo])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[ContratoSrvVnc_CntSrvCod] = @AV18ContratoSrvVnc_CntSrvCod)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV59TFContratoServicosVnc_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58TFContratoServicosVnc_Descricao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Descricao] like @lV58TFContratoServicosVnc_Descricao)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59TFContratoServicosVnc_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Descricao] = @AV59TFContratoServicosVnc_Descricao_Sel)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV28TFContratoSrvVnc_DoStatusDmn_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV28TFContratoSrvVnc_DoStatusDmn_Sels, "T1.[ContratoSrvVnc_DoStatusDmn] IN (", ")") + ")";
         }
         if ( AV32TFContratoSrvVnc_StatusDmn_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV32TFContratoSrvVnc_StatusDmn_Sels, "T1.[ContratoSrvVnc_StatusDmn] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV36TFContratoSrvVnc_ServicoVncSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TFContratoSrvVnc_ServicoVncSigla)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Servico_Sigla] like @lV35TFContratoSrvVnc_ServicoVncSigla)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TFContratoSrvVnc_ServicoVncSigla_Sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Servico_Sigla] = @AV36TFContratoSrvVnc_ServicoVncSigla_Sel)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV40TFContratoSrvVnc_SrvVncStatus_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV40TFContratoSrvVnc_SrvVncStatus_Sels, "T1.[ContratoSrvVnc_SrvVncStatus] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV44TFContratoSrvVnc_SrvVncCntNum_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFContratoSrvVnc_SrvVncCntNum)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Contrato_Numero] like @lV43TFContratoSrvVnc_SrvVncCntNum)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFContratoSrvVnc_SrvVncCntNum_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Contrato_Numero] = @AV44TFContratoSrvVnc_SrvVncCntNum_Sel)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV47TFContratoSrvVnc_SemCusto_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoSrvVnc_SemCusto] = 1)";
         }
         if ( AV47TFContratoSrvVnc_SemCusto_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoSrvVnc_SemCusto] = 0)";
         }
         if ( AV50TFContratoServicosVnc_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Ativo] = 1)";
         }
         if ( AV50TFContratoServicosVnc_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Ativo] = 0)";
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoSrvVnc_CntSrvCod], T1.[ContratoServicosVnc_Descricao]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoSrvVnc_CntSrvCod] DESC, T1.[ContratoServicosVnc_Descricao] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoSrvVnc_CntSrvCod], T1.[ContratoSrvVnc_DoStatusDmn]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoSrvVnc_CntSrvCod] DESC, T1.[ContratoSrvVnc_DoStatusDmn] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoSrvVnc_CntSrvCod], T1.[ContratoSrvVnc_StatusDmn]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoSrvVnc_CntSrvCod] DESC, T1.[ContratoSrvVnc_StatusDmn] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoSrvVnc_CntSrvCod], T4.[Servico_Sigla]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoSrvVnc_CntSrvCod] DESC, T4.[Servico_Sigla] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoSrvVnc_CntSrvCod], T1.[ContratoSrvVnc_SrvVncStatus]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoSrvVnc_CntSrvCod] DESC, T1.[ContratoSrvVnc_SrvVncStatus] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoSrvVnc_CntSrvCod], T3.[Contrato_Numero]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoSrvVnc_CntSrvCod] DESC, T3.[Contrato_Numero] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoSrvVnc_CntSrvCod], T1.[ContratoSrvVnc_SemCusto]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoSrvVnc_CntSrvCod] DESC, T1.[ContratoSrvVnc_SemCusto] DESC";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoSrvVnc_CntSrvCod], T1.[ContratoServicosVnc_Ativo]";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoSrvVnc_CntSrvCod] DESC, T1.[ContratoServicosVnc_Ativo] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoSrvVnc_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00GA8( IGxContext context ,
                                             String A1800ContratoSrvVnc_DoStatusDmn ,
                                             IGxCollection AV28TFContratoSrvVnc_DoStatusDmn_Sels ,
                                             String A1084ContratoSrvVnc_StatusDmn ,
                                             IGxCollection AV32TFContratoSrvVnc_StatusDmn_Sels ,
                                             String A1663ContratoSrvVnc_SrvVncStatus ,
                                             IGxCollection AV40TFContratoSrvVnc_SrvVncStatus_Sels ,
                                             String AV59TFContratoServicosVnc_Descricao_Sel ,
                                             String AV58TFContratoServicosVnc_Descricao ,
                                             int AV28TFContratoSrvVnc_DoStatusDmn_Sels_Count ,
                                             int AV32TFContratoSrvVnc_StatusDmn_Sels_Count ,
                                             String AV36TFContratoSrvVnc_ServicoVncSigla_Sel ,
                                             String AV35TFContratoSrvVnc_ServicoVncSigla ,
                                             int AV40TFContratoSrvVnc_SrvVncStatus_Sels_Count ,
                                             String AV44TFContratoSrvVnc_SrvVncCntNum_Sel ,
                                             String AV43TFContratoSrvVnc_SrvVncCntNum ,
                                             short AV47TFContratoSrvVnc_SemCusto_Sel ,
                                             short AV50TFContratoServicosVnc_Ativo_Sel ,
                                             String A2108ContratoServicosVnc_Descricao ,
                                             String A924ContratoSrvVnc_ServicoVncSigla ,
                                             String A1631ContratoSrvVnc_SrvVncCntNum ,
                                             bool A1090ContratoSrvVnc_SemCusto ,
                                             bool A1453ContratoServicosVnc_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A915ContratoSrvVnc_CntSrvCod ,
                                             int AV18ContratoSrvVnc_CntSrvCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [7] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ((((([ContratoServicosVnc] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T1.[ContratoSrvVnc_SrvVncCntSrvCod]) LEFT JOIN [Contrato] T4 WITH (NOLOCK) ON T4.[Contrato_Codigo] = T3.[Contrato_Codigo]) LEFT JOIN [Servico] T5 WITH (NOLOCK) ON T5.[Servico_Codigo] = T3.[Servico_Codigo]) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoSrvVnc_CntSrvCod]) LEFT JOIN (SELECT CASE  WHEN COALESCE( T7.[ContratoSrvVnc_PrestadoraCod], 0) < 910000 THEN COALESCE( T8.[Contratada_Sigla], '') WHEN COALESCE( T7.[ContratoSrvVnc_PrestadoraCod], 0) = 910000 THEN 'Criador da SS' WHEN COALESCE( T7.[ContratoSrvVnc_PrestadoraCod], 0) = 920000 THEN 'Criador da OS' END AS ContratoSrvVnc_PrestadoraPesNom, T7.[ContratoSrvVnc_Codigo] FROM ([ContratoServicosVnc] T7 WITH (NOLOCK) LEFT JOIN (SELECT [Contratada_Sigla] AS Contratada_Sigla, [Contratada_Codigo] FROM [Contratada] WITH (NOLOCK) ) T8 ON T8.[Contratada_Codigo] = T7.[ContratoSrvVnc_PrestadoraCod]) ) T6 ON T6.[ContratoSrvVnc_Codigo] = T1.[ContratoSrvVnc_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratoSrvVnc_CntSrvCod] = @AV18ContratoSrvVnc_CntSrvCod)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV59TFContratoServicosVnc_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58TFContratoServicosVnc_Descricao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Descricao] like @lV58TFContratoServicosVnc_Descricao)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59TFContratoServicosVnc_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Descricao] = @AV59TFContratoServicosVnc_Descricao_Sel)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV28TFContratoSrvVnc_DoStatusDmn_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV28TFContratoSrvVnc_DoStatusDmn_Sels, "T1.[ContratoSrvVnc_DoStatusDmn] IN (", ")") + ")";
         }
         if ( AV32TFContratoSrvVnc_StatusDmn_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV32TFContratoSrvVnc_StatusDmn_Sels, "T1.[ContratoSrvVnc_StatusDmn] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV36TFContratoSrvVnc_ServicoVncSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TFContratoSrvVnc_ServicoVncSigla)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Servico_Sigla] like @lV35TFContratoSrvVnc_ServicoVncSigla)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TFContratoSrvVnc_ServicoVncSigla_Sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Servico_Sigla] = @AV36TFContratoSrvVnc_ServicoVncSigla_Sel)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV40TFContratoSrvVnc_SrvVncStatus_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV40TFContratoSrvVnc_SrvVncStatus_Sels, "T1.[ContratoSrvVnc_SrvVncStatus] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV44TFContratoSrvVnc_SrvVncCntNum_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFContratoSrvVnc_SrvVncCntNum)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Contrato_Numero] like @lV43TFContratoSrvVnc_SrvVncCntNum)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFContratoSrvVnc_SrvVncCntNum_Sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Contrato_Numero] = @AV44TFContratoSrvVnc_SrvVncCntNum_Sel)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV47TFContratoSrvVnc_SemCusto_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoSrvVnc_SemCusto] = 1)";
         }
         if ( AV47TFContratoSrvVnc_SemCusto_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoSrvVnc_SemCusto] = 0)";
         }
         if ( AV50TFContratoServicosVnc_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Ativo] = 1)";
         }
         if ( AV50TFContratoServicosVnc_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      protected Object[] conditional_H00GA11( IGxContext context ,
                                              int AV19AreaTrabalho_Codigo ,
                                              int AV20Servico_Codigo ,
                                              int A75Contrato_AreaTrabalhoCod ,
                                              int A921ContratoSrvVnc_ServicoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int6 ;
         GXv_int6 = new short [2] ;
         Object[] GXv_Object7 ;
         GXv_Object7 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoSrvVnc_SrvVncCntSrvCod] AS ContratoSrvVnc_SrvVncCntSrvCod, T2.[Contrato_Codigo] AS ContratoSrvVnc_SrvVncCntCod, T2.[Servico_Codigo] AS ContratoSrvVnc_ServicoVncCod, T1.[ContratoSrvVnc_CntSrvCod] AS ContratoSrvVnc_CntSrvCod, T5.[Servico_Codigo] AS ContratoSrvVnc_ServicoCod, T3.[Contrato_AreaTrabalhoCod], T1.[ContratoSrvVnc_StatusDmn], T1.[ContratoSrvVnc_DoStatusDmn], T4.[Servico_Sigla] AS ContratoSrvVnc_ServicoVncSigla, T1.[ContratoSrvVnc_Codigo] FROM (((([ContratoServicosVnc] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoSrvVnc_SrvVncCntSrvCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN [Servico] T4 WITH (NOLOCK) ON T4.[Servico_Codigo] = T2.[Servico_Codigo]) INNER JOIN [ContratoServicos] T5 WITH (NOLOCK) ON T5.[ContratoServicos_Codigo] = T1.[ContratoSrvVnc_CntSrvCod])";
         if ( ! (0==AV19AreaTrabalho_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_AreaTrabalhoCod] = @AV19AreaTrabalho_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_AreaTrabalhoCod] = @AV19AreaTrabalho_Codigo)";
            }
         }
         else
         {
            GXv_int6[0] = 1;
         }
         if ( ! (0==AV20Servico_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Servico_Codigo] = @AV20Servico_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Servico_Codigo] = @AV20Servico_Codigo)";
            }
         }
         else
         {
            GXv_int6[1] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T3.[Contrato_AreaTrabalhoCod]";
         GXv_Object7[0] = scmdbuf;
         GXv_Object7[1] = GXv_int6;
         return GXv_Object7 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 1 :
                     return conditional_H00GA5(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (short)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (bool)dynConstraints[20] , (bool)dynConstraints[21] , (short)dynConstraints[22] , (bool)dynConstraints[23] , (int)dynConstraints[24] , (int)dynConstraints[25] );
               case 2 :
                     return conditional_H00GA8(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (short)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (bool)dynConstraints[20] , (bool)dynConstraints[21] , (short)dynConstraints[22] , (bool)dynConstraints[23] , (int)dynConstraints[24] , (int)dynConstraints[25] );
               case 5 :
                     return conditional_H00GA11(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00GA2 ;
          prmH00GA2 = new Object[] {
          new Object[] {"@AV19AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00GA9 ;
          prmH00GA9 = new Object[] {
          } ;
          Object[] prmH00GA10 ;
          prmH00GA10 = new Object[] {
          new Object[] {"@AV6WWPContext__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH00GA5 ;
          prmH00GA5 = new Object[] {
          new Object[] {"@AV18ContratoSrvVnc_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV58TFContratoServicosVnc_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV59TFContratoServicosVnc_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV35TFContratoSrvVnc_ServicoVncSigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV36TFContratoSrvVnc_ServicoVncSigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV43TFContratoSrvVnc_SrvVncCntNum",SqlDbType.Char,20,0} ,
          new Object[] {"@AV44TFContratoSrvVnc_SrvVncCntNum_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00GA8 ;
          prmH00GA8 = new Object[] {
          new Object[] {"@AV18ContratoSrvVnc_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV58TFContratoServicosVnc_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV59TFContratoServicosVnc_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV35TFContratoSrvVnc_ServicoVncSigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV36TFContratoSrvVnc_ServicoVncSigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV43TFContratoSrvVnc_SrvVncCntNum",SqlDbType.Char,20,0} ,
          new Object[] {"@AV44TFContratoSrvVnc_SrvVncCntNum_Sel",SqlDbType.Char,20,0}
          } ;
          Object[] prmH00GA11 ;
          prmH00GA11 = new Object[] {
          new Object[] {"@AV19AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV20Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00GA2", "SELECT T3.[Contrato_Codigo], T2.[Servico_Codigo], T1.[ContratoServicos_Codigo], T3.[Contrato_AreaTrabalhoCod], T1.[Servico_Codigo] AS ContratoServicos_ServicoCod, T2.[Servico_Sigla] AS ContratoServicos_ServicoSigla FROM (([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) INNER JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE (@AV19AreaTrabalho_Codigo = convert(int, 0)) or T3.[Contrato_AreaTrabalhoCod] = @AV19AreaTrabalho_Codigo ORDER BY [ContratoServicos_ServicoSigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00GA2,0,0,true,false )
             ,new CursorDef("H00GA5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00GA5,11,0,true,false )
             ,new CursorDef("H00GA8", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00GA8,1,0,true,false )
             ,new CursorDef("H00GA9", "SELECT [AreaTrabalho_Ativo], [AreaTrabalho_Codigo], [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Ativo] = 1 ORDER BY [AreaTrabalho_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00GA9,100,0,false,false )
             ,new CursorDef("H00GA10", "SELECT DISTINCT NULL AS [ContratadaUsuario_ContratadaCod], NULL AS [ContratadaUsuario_UsuarioCod], [ContratadaUsuario_AreaTrabalhoCod], [ContratadaUsuario_AreaTrabalhoDes] FROM ( SELECT TOP(100) PERCENT T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T1.[ContratadaUsuario_UsuarioCod], T2.[Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod, T3.[AreaTrabalho_Descricao] AS ContratadaUsuario_AreaTrabalhoDes FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) LEFT JOIN [AreaTrabalho] T3 WITH (NOLOCK) ON T3.[AreaTrabalho_Codigo] = T2.[Contratada_AreaTrabalhoCod]) WHERE T1.[ContratadaUsuario_UsuarioCod] = @AV6WWPContext__Userid ORDER BY T3.[AreaTrabalho_Descricao]) DistinctT ORDER BY [ContratadaUsuario_AreaTrabalhoDes] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00GA10,100,0,false,false )
             ,new CursorDef("H00GA11", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00GA11,11,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 15) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((bool[]) buf[4])[0] = rslt.getBool(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((bool[]) buf[6])[0] = rslt.getBool(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 20) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getString(7, 15) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((String[]) buf[14])[0] = rslt.getString(8, 1) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((String[]) buf[16])[0] = rslt.getString(9, 1) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((String[]) buf[18])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((int[]) buf[20])[0] = rslt.getInt(11) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((int[]) buf[22])[0] = rslt.getInt(12) ;
                ((int[]) buf[23])[0] = rslt.getInt(13) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(13);
                ((int[]) buf[25])[0] = rslt.getInt(14) ;
                ((String[]) buf[26])[0] = rslt.getString(15, 50) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                return;
             case 2 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 3 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 1) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 1) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((int[]) buf[17])[0] = rslt.getInt(10) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 5 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[2]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                return;
       }
    }

 }

}
