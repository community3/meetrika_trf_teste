/*
               File: WWMetodologiaFases
        Description:  Metodologia Fases
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 13:1:41.18
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwmetodologiafases : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwmetodologiafases( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwmetodologiafases( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_73 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_73_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_73_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV16DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
               AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV18MetodologiaFases_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18MetodologiaFases_Nome1", AV18MetodologiaFases_Nome1);
               AV19Metodologia_Descricao1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Metodologia_Descricao1", AV19Metodologia_Descricao1);
               AV21DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
               AV23MetodologiaFases_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23MetodologiaFases_Nome2", AV23MetodologiaFases_Nome2);
               AV24Metodologia_Descricao2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Metodologia_Descricao2", AV24Metodologia_Descricao2);
               AV20DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV65TFMetodologiaFases_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFMetodologiaFases_Nome", AV65TFMetodologiaFases_Nome);
               AV66TFMetodologiaFases_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFMetodologiaFases_Nome_Sel", AV66TFMetodologiaFases_Nome_Sel);
               AV69TFMetodologiaFases_Percentual = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFMetodologiaFases_Percentual", StringUtil.LTrim( StringUtil.Str( AV69TFMetodologiaFases_Percentual, 6, 2)));
               AV70TFMetodologiaFases_Percentual_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFMetodologiaFases_Percentual_To", StringUtil.LTrim( StringUtil.Str( AV70TFMetodologiaFases_Percentual_To, 6, 2)));
               AV81TFMetodologiaFases_Prazo = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFMetodologiaFases_Prazo", StringUtil.LTrim( StringUtil.Str( AV81TFMetodologiaFases_Prazo, 7, 2)));
               AV82TFMetodologiaFases_Prazo_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFMetodologiaFases_Prazo_To", StringUtil.LTrim( StringUtil.Str( AV82TFMetodologiaFases_Prazo_To, 7, 2)));
               AV73TFMetodologia_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFMetodologia_Descricao", AV73TFMetodologia_Descricao);
               AV74TFMetodologia_Descricao_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFMetodologia_Descricao_Sel", AV74TFMetodologia_Descricao_Sel);
               AV67ddo_MetodologiaFases_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ddo_MetodologiaFases_NomeTitleControlIdToReplace", AV67ddo_MetodologiaFases_NomeTitleControlIdToReplace);
               AV71ddo_MetodologiaFases_PercentualTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ddo_MetodologiaFases_PercentualTitleControlIdToReplace", AV71ddo_MetodologiaFases_PercentualTitleControlIdToReplace);
               AV83ddo_MetodologiaFases_PrazoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83ddo_MetodologiaFases_PrazoTitleControlIdToReplace", AV83ddo_MetodologiaFases_PrazoTitleControlIdToReplace);
               AV75ddo_Metodologia_DescricaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75ddo_Metodologia_DescricaoTitleControlIdToReplace", AV75ddo_Metodologia_DescricaoTitleControlIdToReplace);
               AV106Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV31DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31DynamicFiltersIgnoreFirst", AV31DynamicFiltersIgnoreFirst);
               AV30DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
               A147MetodologiaFases_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A137Metodologia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18MetodologiaFases_Nome1, AV19Metodologia_Descricao1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23MetodologiaFases_Nome2, AV24Metodologia_Descricao2, AV20DynamicFiltersEnabled2, AV65TFMetodologiaFases_Nome, AV66TFMetodologiaFases_Nome_Sel, AV69TFMetodologiaFases_Percentual, AV70TFMetodologiaFases_Percentual_To, AV81TFMetodologiaFases_Prazo, AV82TFMetodologiaFases_Prazo_To, AV73TFMetodologia_Descricao, AV74TFMetodologia_Descricao_Sel, AV67ddo_MetodologiaFases_NomeTitleControlIdToReplace, AV71ddo_MetodologiaFases_PercentualTitleControlIdToReplace, AV83ddo_MetodologiaFases_PrazoTitleControlIdToReplace, AV75ddo_Metodologia_DescricaoTitleControlIdToReplace, AV106Pgmname, AV10GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A147MetodologiaFases_Codigo, A137Metodologia_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA7X2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START7X2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205181314163");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwmetodologiafases.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV16DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vMETODOLOGIAFASES_NOME1", StringUtil.RTrim( AV18MetodologiaFases_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vMETODOLOGIA_DESCRICAO1", AV19Metodologia_Descricao1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV21DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vMETODOLOGIAFASES_NOME2", StringUtil.RTrim( AV23MetodologiaFases_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vMETODOLOGIA_DESCRICAO2", AV24Metodologia_Descricao2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV20DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vTFMETODOLOGIAFASES_NOME", StringUtil.RTrim( AV65TFMetodologiaFases_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFMETODOLOGIAFASES_NOME_SEL", StringUtil.RTrim( AV66TFMetodologiaFases_Nome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFMETODOLOGIAFASES_PERCENTUAL", StringUtil.LTrim( StringUtil.NToC( AV69TFMetodologiaFases_Percentual, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFMETODOLOGIAFASES_PERCENTUAL_TO", StringUtil.LTrim( StringUtil.NToC( AV70TFMetodologiaFases_Percentual_To, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFMETODOLOGIAFASES_PRAZO", StringUtil.LTrim( StringUtil.NToC( AV81TFMetodologiaFases_Prazo, 7, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFMETODOLOGIAFASES_PRAZO_TO", StringUtil.LTrim( StringUtil.NToC( AV82TFMetodologiaFases_Prazo_To, 7, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFMETODOLOGIA_DESCRICAO", AV73TFMetodologia_Descricao);
         GxWebStd.gx_hidden_field( context, "GXH_vTFMETODOLOGIA_DESCRICAO_SEL", AV74TFMetodologia_Descricao_Sel);
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_73", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_73), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV78GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV79GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV76DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV76DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMETODOLOGIAFASES_NOMETITLEFILTERDATA", AV64MetodologiaFases_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMETODOLOGIAFASES_NOMETITLEFILTERDATA", AV64MetodologiaFases_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMETODOLOGIAFASES_PERCENTUALTITLEFILTERDATA", AV68MetodologiaFases_PercentualTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMETODOLOGIAFASES_PERCENTUALTITLEFILTERDATA", AV68MetodologiaFases_PercentualTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMETODOLOGIAFASES_PRAZOTITLEFILTERDATA", AV80MetodologiaFases_PrazoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMETODOLOGIAFASES_PRAZOTITLEFILTERDATA", AV80MetodologiaFases_PrazoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMETODOLOGIA_DESCRICAOTITLEFILTERDATA", AV72Metodologia_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMETODOLOGIA_DESCRICAOTITLEFILTERDATA", AV72Metodologia_DescricaoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV106Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV31DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV30DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Caption", StringUtil.RTrim( Ddo_metodologiafases_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Tooltip", StringUtil.RTrim( Ddo_metodologiafases_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Cls", StringUtil.RTrim( Ddo_metodologiafases_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_metodologiafases_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_metodologiafases_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_metodologiafases_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_metodologiafases_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_metodologiafases_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_metodologiafases_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Sortedstatus", StringUtil.RTrim( Ddo_metodologiafases_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Includefilter", StringUtil.BoolToStr( Ddo_metodologiafases_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Filtertype", StringUtil.RTrim( Ddo_metodologiafases_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_metodologiafases_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_metodologiafases_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Datalisttype", StringUtil.RTrim( Ddo_metodologiafases_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Datalistproc", StringUtil.RTrim( Ddo_metodologiafases_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_metodologiafases_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Sortasc", StringUtil.RTrim( Ddo_metodologiafases_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Sortdsc", StringUtil.RTrim( Ddo_metodologiafases_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Loadingdata", StringUtil.RTrim( Ddo_metodologiafases_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Cleanfilter", StringUtil.RTrim( Ddo_metodologiafases_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Noresultsfound", StringUtil.RTrim( Ddo_metodologiafases_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_metodologiafases_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PERCENTUAL_Caption", StringUtil.RTrim( Ddo_metodologiafases_percentual_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PERCENTUAL_Tooltip", StringUtil.RTrim( Ddo_metodologiafases_percentual_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PERCENTUAL_Cls", StringUtil.RTrim( Ddo_metodologiafases_percentual_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PERCENTUAL_Filteredtext_set", StringUtil.RTrim( Ddo_metodologiafases_percentual_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PERCENTUAL_Filteredtextto_set", StringUtil.RTrim( Ddo_metodologiafases_percentual_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PERCENTUAL_Dropdownoptionstype", StringUtil.RTrim( Ddo_metodologiafases_percentual_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PERCENTUAL_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_metodologiafases_percentual_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PERCENTUAL_Includesortasc", StringUtil.BoolToStr( Ddo_metodologiafases_percentual_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PERCENTUAL_Includesortdsc", StringUtil.BoolToStr( Ddo_metodologiafases_percentual_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PERCENTUAL_Sortedstatus", StringUtil.RTrim( Ddo_metodologiafases_percentual_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PERCENTUAL_Includefilter", StringUtil.BoolToStr( Ddo_metodologiafases_percentual_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PERCENTUAL_Filtertype", StringUtil.RTrim( Ddo_metodologiafases_percentual_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PERCENTUAL_Filterisrange", StringUtil.BoolToStr( Ddo_metodologiafases_percentual_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PERCENTUAL_Includedatalist", StringUtil.BoolToStr( Ddo_metodologiafases_percentual_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PERCENTUAL_Sortasc", StringUtil.RTrim( Ddo_metodologiafases_percentual_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PERCENTUAL_Sortdsc", StringUtil.RTrim( Ddo_metodologiafases_percentual_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PERCENTUAL_Cleanfilter", StringUtil.RTrim( Ddo_metodologiafases_percentual_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PERCENTUAL_Rangefilterfrom", StringUtil.RTrim( Ddo_metodologiafases_percentual_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PERCENTUAL_Rangefilterto", StringUtil.RTrim( Ddo_metodologiafases_percentual_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PERCENTUAL_Searchbuttontext", StringUtil.RTrim( Ddo_metodologiafases_percentual_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PRAZO_Caption", StringUtil.RTrim( Ddo_metodologiafases_prazo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PRAZO_Tooltip", StringUtil.RTrim( Ddo_metodologiafases_prazo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PRAZO_Cls", StringUtil.RTrim( Ddo_metodologiafases_prazo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PRAZO_Filteredtext_set", StringUtil.RTrim( Ddo_metodologiafases_prazo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PRAZO_Filteredtextto_set", StringUtil.RTrim( Ddo_metodologiafases_prazo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PRAZO_Dropdownoptionstype", StringUtil.RTrim( Ddo_metodologiafases_prazo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PRAZO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_metodologiafases_prazo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PRAZO_Includesortasc", StringUtil.BoolToStr( Ddo_metodologiafases_prazo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PRAZO_Includesortdsc", StringUtil.BoolToStr( Ddo_metodologiafases_prazo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PRAZO_Sortedstatus", StringUtil.RTrim( Ddo_metodologiafases_prazo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PRAZO_Includefilter", StringUtil.BoolToStr( Ddo_metodologiafases_prazo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PRAZO_Filtertype", StringUtil.RTrim( Ddo_metodologiafases_prazo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PRAZO_Filterisrange", StringUtil.BoolToStr( Ddo_metodologiafases_prazo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PRAZO_Includedatalist", StringUtil.BoolToStr( Ddo_metodologiafases_prazo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PRAZO_Sortasc", StringUtil.RTrim( Ddo_metodologiafases_prazo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PRAZO_Sortdsc", StringUtil.RTrim( Ddo_metodologiafases_prazo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PRAZO_Cleanfilter", StringUtil.RTrim( Ddo_metodologiafases_prazo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PRAZO_Rangefilterfrom", StringUtil.RTrim( Ddo_metodologiafases_prazo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PRAZO_Rangefilterto", StringUtil.RTrim( Ddo_metodologiafases_prazo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PRAZO_Searchbuttontext", StringUtil.RTrim( Ddo_metodologiafases_prazo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Caption", StringUtil.RTrim( Ddo_metodologia_descricao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_metodologia_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Cls", StringUtil.RTrim( Ddo_metodologia_descricao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_metodologia_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_metodologia_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_metodologia_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_metodologia_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_metodologia_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_metodologia_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_metodologia_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_metodologia_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_metodologia_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_metodologia_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_metodologia_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_metodologia_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_metodologia_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_metodologia_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_metodologia_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_metodologia_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_metodologia_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_metodologia_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_metodologia_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_metodologia_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Activeeventkey", StringUtil.RTrim( Ddo_metodologiafases_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_metodologiafases_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_metodologiafases_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PERCENTUAL_Activeeventkey", StringUtil.RTrim( Ddo_metodologiafases_percentual_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PERCENTUAL_Filteredtext_get", StringUtil.RTrim( Ddo_metodologiafases_percentual_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PERCENTUAL_Filteredtextto_get", StringUtil.RTrim( Ddo_metodologiafases_percentual_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PRAZO_Activeeventkey", StringUtil.RTrim( Ddo_metodologiafases_prazo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PRAZO_Filteredtext_get", StringUtil.RTrim( Ddo_metodologiafases_prazo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_PRAZO_Filteredtextto_get", StringUtil.RTrim( Ddo_metodologiafases_prazo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_metodologia_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_metodologia_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_metodologia_descricao_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE7X2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT7X2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwmetodologiafases.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWMetodologiaFases" ;
      }

      public override String GetPgmdesc( )
      {
         return " Metodologia Fases" ;
      }

      protected void WB7X0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_7X2( true) ;
         }
         else
         {
            wb_table1_2_7X2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_7X2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 87,'',false,'" + sGXsfl_73_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV20DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(87, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,87);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 88,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfmetodologiafases_nome_Internalname, StringUtil.RTrim( AV65TFMetodologiaFases_Nome), StringUtil.RTrim( context.localUtil.Format( AV65TFMetodologiaFases_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,88);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfmetodologiafases_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfmetodologiafases_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWMetodologiaFases.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfmetodologiafases_nome_sel_Internalname, StringUtil.RTrim( AV66TFMetodologiaFases_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV66TFMetodologiaFases_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,89);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfmetodologiafases_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfmetodologiafases_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWMetodologiaFases.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 90,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfmetodologiafases_percentual_Internalname, StringUtil.LTrim( StringUtil.NToC( AV69TFMetodologiaFases_Percentual, 6, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV69TFMetodologiaFases_Percentual, "ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,90);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfmetodologiafases_percentual_Jsonclick, 0, "Attribute", "", "", "", edtavTfmetodologiafases_percentual_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWMetodologiaFases.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfmetodologiafases_percentual_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV70TFMetodologiaFases_Percentual_To, 6, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV70TFMetodologiaFases_Percentual_To, "ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,91);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfmetodologiafases_percentual_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfmetodologiafases_percentual_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWMetodologiaFases.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfmetodologiafases_prazo_Internalname, StringUtil.LTrim( StringUtil.NToC( AV81TFMetodologiaFases_Prazo, 7, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV81TFMetodologiaFases_Prazo, "ZZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,92);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfmetodologiafases_prazo_Jsonclick, 0, "Attribute", "", "", "", edtavTfmetodologiafases_prazo_Visible, 1, 0, "text", "", 7, "chr", 1, "row", 7, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWMetodologiaFases.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 93,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfmetodologiafases_prazo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV82TFMetodologiaFases_Prazo_To, 7, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV82TFMetodologiaFases_Prazo_To, "ZZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,93);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfmetodologiafases_prazo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfmetodologiafases_prazo_to_Visible, 1, 0, "text", "", 7, "chr", 1, "row", 7, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWMetodologiaFases.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfmetodologia_descricao_Internalname, AV73TFMetodologia_Descricao, StringUtil.RTrim( context.localUtil.Format( AV73TFMetodologia_Descricao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,94);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfmetodologia_descricao_Jsonclick, 0, "Attribute", "", "", "", edtavTfmetodologia_descricao_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWMetodologiaFases.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfmetodologia_descricao_sel_Internalname, AV74TFMetodologia_Descricao_Sel, StringUtil.RTrim( context.localUtil.Format( AV74TFMetodologia_Descricao_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,95);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfmetodologia_descricao_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfmetodologia_descricao_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWMetodologiaFases.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_METODOLOGIAFASES_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'" + sGXsfl_73_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_metodologiafases_nometitlecontrolidtoreplace_Internalname, AV67ddo_MetodologiaFases_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,97);\"", 0, edtavDdo_metodologiafases_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWMetodologiaFases.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_METODOLOGIAFASES_PERCENTUALContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'" + sGXsfl_73_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_metodologiafases_percentualtitlecontrolidtoreplace_Internalname, AV71ddo_MetodologiaFases_PercentualTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,99);\"", 0, edtavDdo_metodologiafases_percentualtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWMetodologiaFases.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_METODOLOGIAFASES_PRAZOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_73_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_metodologiafases_prazotitlecontrolidtoreplace_Internalname, AV83ddo_MetodologiaFases_PrazoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,101);\"", 0, edtavDdo_metodologiafases_prazotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWMetodologiaFases.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_METODOLOGIA_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_73_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_metodologia_descricaotitlecontrolidtoreplace_Internalname, AV75ddo_Metodologia_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,103);\"", 0, edtavDdo_metodologia_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWMetodologiaFases.htm");
         }
         wbLoad = true;
      }

      protected void START7X2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Metodologia Fases", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP7X0( ) ;
      }

      protected void WS7X2( )
      {
         START7X2( ) ;
         EVT7X2( ) ;
      }

      protected void EVT7X2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E117X2 */
                              E117X2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_METODOLOGIAFASES_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E127X2 */
                              E127X2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_METODOLOGIAFASES_PERCENTUAL.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E137X2 */
                              E137X2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_METODOLOGIAFASES_PRAZO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E147X2 */
                              E147X2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_METODOLOGIA_DESCRICAO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E157X2 */
                              E157X2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E167X2 */
                              E167X2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E177X2 */
                              E177X2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E187X2 */
                              E187X2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E197X2 */
                              E197X2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E207X2 */
                              E207X2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E217X2 */
                              E217X2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E227X2 */
                              E227X2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E237X2 */
                              E237X2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_73_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_73_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_73_idx), 4, 0)), 4, "0");
                              SubsflControlProps_732( ) ;
                              AV32Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV32Update)) ? AV103Update_GXI : context.convertURL( context.PathToRelativeUrl( AV32Update))));
                              AV33Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV33Delete)) ? AV104Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV33Delete))));
                              AV34Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV34Display)) ? AV105Display_GXI : context.convertURL( context.PathToRelativeUrl( AV34Display))));
                              A147MetodologiaFases_Codigo = (int)(context.localUtil.CToN( cgiGet( edtMetodologiaFases_Codigo_Internalname), ",", "."));
                              A148MetodologiaFases_Nome = StringUtil.Upper( cgiGet( edtMetodologiaFases_Nome_Internalname));
                              A149MetodologiaFases_Percentual = context.localUtil.CToN( cgiGet( edtMetodologiaFases_Percentual_Internalname), ",", ".");
                              A2135MetodologiaFases_Prazo = context.localUtil.CToN( cgiGet( edtMetodologiaFases_Prazo_Internalname), ",", ".");
                              n2135MetodologiaFases_Prazo = false;
                              A137Metodologia_Codigo = (int)(context.localUtil.CToN( cgiGet( edtMetodologia_Codigo_Internalname), ",", "."));
                              A138Metodologia_Descricao = StringUtil.Upper( cgiGet( edtMetodologia_Descricao_Internalname));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E247X2 */
                                    E247X2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E257X2 */
                                    E257X2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E267X2 */
                                    E267X2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV17DynamicFiltersOperator1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Metodologiafases_nome1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vMETODOLOGIAFASES_NOME1"), AV18MetodologiaFases_Nome1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Metodologia_descricao1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vMETODOLOGIA_DESCRICAO1"), AV19Metodologia_Descricao1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV22DynamicFiltersOperator2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Metodologiafases_nome2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vMETODOLOGIAFASES_NOME2"), AV23MetodologiaFases_Nome2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Metodologia_descricao2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vMETODOLOGIA_DESCRICAO2"), AV24Metodologia_Descricao2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfmetodologiafases_nome Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMETODOLOGIAFASES_NOME"), AV65TFMetodologiaFases_Nome) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfmetodologiafases_nome_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMETODOLOGIAFASES_NOME_SEL"), AV66TFMetodologiaFases_Nome_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfmetodologiafases_percentual Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFMETODOLOGIAFASES_PERCENTUAL"), ",", ".") != AV69TFMetodologiaFases_Percentual )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfmetodologiafases_percentual_to Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFMETODOLOGIAFASES_PERCENTUAL_TO"), ",", ".") != AV70TFMetodologiaFases_Percentual_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfmetodologiafases_prazo Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFMETODOLOGIAFASES_PRAZO"), ",", ".") != AV81TFMetodologiaFases_Prazo )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfmetodologiafases_prazo_to Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFMETODOLOGIAFASES_PRAZO_TO"), ",", ".") != AV82TFMetodologiaFases_Prazo_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfmetodologia_descricao Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMETODOLOGIA_DESCRICAO"), AV73TFMetodologia_Descricao) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfmetodologia_descricao_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMETODOLOGIA_DESCRICAO_SEL"), AV74TFMetodologia_Descricao_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE7X2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA7X2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("METODOLOGIAFASES_NOME", "Nome da Fase", 0);
            cmbavDynamicfiltersselector1.addItem("METODOLOGIA_DESCRICAO", "Metodologia", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("METODOLOGIAFASES_NOME", "Nome da Fase", 0);
            cmbavDynamicfiltersselector2.addItem("METODOLOGIA_DESCRICAO", "Metodologia", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_732( ) ;
         while ( nGXsfl_73_idx <= nRC_GXsfl_73 )
         {
            sendrow_732( ) ;
            nGXsfl_73_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_73_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_73_idx+1));
            sGXsfl_73_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_73_idx), 4, 0)), 4, "0");
            SubsflControlProps_732( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV16DynamicFiltersSelector1 ,
                                       short AV17DynamicFiltersOperator1 ,
                                       String AV18MetodologiaFases_Nome1 ,
                                       String AV19Metodologia_Descricao1 ,
                                       String AV21DynamicFiltersSelector2 ,
                                       short AV22DynamicFiltersOperator2 ,
                                       String AV23MetodologiaFases_Nome2 ,
                                       String AV24Metodologia_Descricao2 ,
                                       bool AV20DynamicFiltersEnabled2 ,
                                       String AV65TFMetodologiaFases_Nome ,
                                       String AV66TFMetodologiaFases_Nome_Sel ,
                                       decimal AV69TFMetodologiaFases_Percentual ,
                                       decimal AV70TFMetodologiaFases_Percentual_To ,
                                       decimal AV81TFMetodologiaFases_Prazo ,
                                       decimal AV82TFMetodologiaFases_Prazo_To ,
                                       String AV73TFMetodologia_Descricao ,
                                       String AV74TFMetodologia_Descricao_Sel ,
                                       String AV67ddo_MetodologiaFases_NomeTitleControlIdToReplace ,
                                       String AV71ddo_MetodologiaFases_PercentualTitleControlIdToReplace ,
                                       String AV83ddo_MetodologiaFases_PrazoTitleControlIdToReplace ,
                                       String AV75ddo_Metodologia_DescricaoTitleControlIdToReplace ,
                                       String AV106Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV31DynamicFiltersIgnoreFirst ,
                                       bool AV30DynamicFiltersRemoving ,
                                       int A147MetodologiaFases_Codigo ,
                                       int A137Metodologia_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF7X2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_METODOLOGIAFASES_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A147MetodologiaFases_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "METODOLOGIAFASES_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A147MetodologiaFases_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_METODOLOGIAFASES_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A148MetodologiaFases_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "METODOLOGIAFASES_NOME", StringUtil.RTrim( A148MetodologiaFases_Nome));
         GxWebStd.gx_hidden_field( context, "gxhash_METODOLOGIAFASES_PERCENTUAL", GetSecureSignedToken( "", context.localUtil.Format( A149MetodologiaFases_Percentual, "ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "METODOLOGIAFASES_PERCENTUAL", StringUtil.LTrim( StringUtil.NToC( A149MetodologiaFases_Percentual, 6, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_METODOLOGIAFASES_PRAZO", GetSecureSignedToken( "", context.localUtil.Format( A2135MetodologiaFases_Prazo, "ZZZ9.99")));
         GxWebStd.gx_hidden_field( context, "METODOLOGIAFASES_PRAZO", StringUtil.LTrim( StringUtil.NToC( A2135MetodologiaFases_Prazo, 7, 2, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF7X2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV106Pgmname = "WWMetodologiaFases";
         context.Gx_err = 0;
      }

      protected void RF7X2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 73;
         /* Execute user event: E257X2 */
         E257X2 ();
         nGXsfl_73_idx = 1;
         sGXsfl_73_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_73_idx), 4, 0)), 4, "0");
         SubsflControlProps_732( ) ;
         nGXsfl_73_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_732( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV86WWMetodologiaFasesDS_1_Dynamicfiltersselector1 ,
                                                 AV87WWMetodologiaFasesDS_2_Dynamicfiltersoperator1 ,
                                                 AV88WWMetodologiaFasesDS_3_Metodologiafases_nome1 ,
                                                 AV89WWMetodologiaFasesDS_4_Metodologia_descricao1 ,
                                                 AV90WWMetodologiaFasesDS_5_Dynamicfiltersenabled2 ,
                                                 AV91WWMetodologiaFasesDS_6_Dynamicfiltersselector2 ,
                                                 AV92WWMetodologiaFasesDS_7_Dynamicfiltersoperator2 ,
                                                 AV93WWMetodologiaFasesDS_8_Metodologiafases_nome2 ,
                                                 AV94WWMetodologiaFasesDS_9_Metodologia_descricao2 ,
                                                 AV96WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel ,
                                                 AV95WWMetodologiaFasesDS_10_Tfmetodologiafases_nome ,
                                                 AV97WWMetodologiaFasesDS_12_Tfmetodologiafases_percentual ,
                                                 AV98WWMetodologiaFasesDS_13_Tfmetodologiafases_percentual_to ,
                                                 AV99WWMetodologiaFasesDS_14_Tfmetodologiafases_prazo ,
                                                 AV100WWMetodologiaFasesDS_15_Tfmetodologiafases_prazo_to ,
                                                 AV102WWMetodologiaFasesDS_17_Tfmetodologia_descricao_sel ,
                                                 AV101WWMetodologiaFasesDS_16_Tfmetodologia_descricao ,
                                                 A148MetodologiaFases_Nome ,
                                                 A138Metodologia_Descricao ,
                                                 A149MetodologiaFases_Percentual ,
                                                 A2135MetodologiaFases_Prazo ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL,
                                                 TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV88WWMetodologiaFasesDS_3_Metodologiafases_nome1 = StringUtil.PadR( StringUtil.RTrim( AV88WWMetodologiaFasesDS_3_Metodologiafases_nome1), 50, "%");
            lV88WWMetodologiaFasesDS_3_Metodologiafases_nome1 = StringUtil.PadR( StringUtil.RTrim( AV88WWMetodologiaFasesDS_3_Metodologiafases_nome1), 50, "%");
            lV89WWMetodologiaFasesDS_4_Metodologia_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV89WWMetodologiaFasesDS_4_Metodologia_descricao1), "%", "");
            lV89WWMetodologiaFasesDS_4_Metodologia_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV89WWMetodologiaFasesDS_4_Metodologia_descricao1), "%", "");
            lV93WWMetodologiaFasesDS_8_Metodologiafases_nome2 = StringUtil.PadR( StringUtil.RTrim( AV93WWMetodologiaFasesDS_8_Metodologiafases_nome2), 50, "%");
            lV93WWMetodologiaFasesDS_8_Metodologiafases_nome2 = StringUtil.PadR( StringUtil.RTrim( AV93WWMetodologiaFasesDS_8_Metodologiafases_nome2), 50, "%");
            lV94WWMetodologiaFasesDS_9_Metodologia_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV94WWMetodologiaFasesDS_9_Metodologia_descricao2), "%", "");
            lV94WWMetodologiaFasesDS_9_Metodologia_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV94WWMetodologiaFasesDS_9_Metodologia_descricao2), "%", "");
            lV95WWMetodologiaFasesDS_10_Tfmetodologiafases_nome = StringUtil.PadR( StringUtil.RTrim( AV95WWMetodologiaFasesDS_10_Tfmetodologiafases_nome), 50, "%");
            lV101WWMetodologiaFasesDS_16_Tfmetodologia_descricao = StringUtil.Concat( StringUtil.RTrim( AV101WWMetodologiaFasesDS_16_Tfmetodologia_descricao), "%", "");
            /* Using cursor H007X2 */
            pr_default.execute(0, new Object[] {lV88WWMetodologiaFasesDS_3_Metodologiafases_nome1, lV88WWMetodologiaFasesDS_3_Metodologiafases_nome1, lV89WWMetodologiaFasesDS_4_Metodologia_descricao1, lV89WWMetodologiaFasesDS_4_Metodologia_descricao1, lV93WWMetodologiaFasesDS_8_Metodologiafases_nome2, lV93WWMetodologiaFasesDS_8_Metodologiafases_nome2, lV94WWMetodologiaFasesDS_9_Metodologia_descricao2, lV94WWMetodologiaFasesDS_9_Metodologia_descricao2, lV95WWMetodologiaFasesDS_10_Tfmetodologiafases_nome, AV96WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel, AV97WWMetodologiaFasesDS_12_Tfmetodologiafases_percentual, AV98WWMetodologiaFasesDS_13_Tfmetodologiafases_percentual_to, AV99WWMetodologiaFasesDS_14_Tfmetodologiafases_prazo, AV100WWMetodologiaFasesDS_15_Tfmetodologiafases_prazo_to, lV101WWMetodologiaFasesDS_16_Tfmetodologia_descricao, AV102WWMetodologiaFasesDS_17_Tfmetodologia_descricao_sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_73_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A138Metodologia_Descricao = H007X2_A138Metodologia_Descricao[0];
               A137Metodologia_Codigo = H007X2_A137Metodologia_Codigo[0];
               A2135MetodologiaFases_Prazo = H007X2_A2135MetodologiaFases_Prazo[0];
               n2135MetodologiaFases_Prazo = H007X2_n2135MetodologiaFases_Prazo[0];
               A149MetodologiaFases_Percentual = H007X2_A149MetodologiaFases_Percentual[0];
               A148MetodologiaFases_Nome = H007X2_A148MetodologiaFases_Nome[0];
               A147MetodologiaFases_Codigo = H007X2_A147MetodologiaFases_Codigo[0];
               A138Metodologia_Descricao = H007X2_A138Metodologia_Descricao[0];
               /* Execute user event: E267X2 */
               E267X2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 73;
            WB7X0( ) ;
         }
         nGXsfl_73_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV86WWMetodologiaFasesDS_1_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV87WWMetodologiaFasesDS_2_Dynamicfiltersoperator1 = AV17DynamicFiltersOperator1;
         AV88WWMetodologiaFasesDS_3_Metodologiafases_nome1 = AV18MetodologiaFases_Nome1;
         AV89WWMetodologiaFasesDS_4_Metodologia_descricao1 = AV19Metodologia_Descricao1;
         AV90WWMetodologiaFasesDS_5_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV91WWMetodologiaFasesDS_6_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV92WWMetodologiaFasesDS_7_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV93WWMetodologiaFasesDS_8_Metodologiafases_nome2 = AV23MetodologiaFases_Nome2;
         AV94WWMetodologiaFasesDS_9_Metodologia_descricao2 = AV24Metodologia_Descricao2;
         AV95WWMetodologiaFasesDS_10_Tfmetodologiafases_nome = AV65TFMetodologiaFases_Nome;
         AV96WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel = AV66TFMetodologiaFases_Nome_Sel;
         AV97WWMetodologiaFasesDS_12_Tfmetodologiafases_percentual = AV69TFMetodologiaFases_Percentual;
         AV98WWMetodologiaFasesDS_13_Tfmetodologiafases_percentual_to = AV70TFMetodologiaFases_Percentual_To;
         AV99WWMetodologiaFasesDS_14_Tfmetodologiafases_prazo = AV81TFMetodologiaFases_Prazo;
         AV100WWMetodologiaFasesDS_15_Tfmetodologiafases_prazo_to = AV82TFMetodologiaFases_Prazo_To;
         AV101WWMetodologiaFasesDS_16_Tfmetodologia_descricao = AV73TFMetodologia_Descricao;
         AV102WWMetodologiaFasesDS_17_Tfmetodologia_descricao_sel = AV74TFMetodologia_Descricao_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV86WWMetodologiaFasesDS_1_Dynamicfiltersselector1 ,
                                              AV87WWMetodologiaFasesDS_2_Dynamicfiltersoperator1 ,
                                              AV88WWMetodologiaFasesDS_3_Metodologiafases_nome1 ,
                                              AV89WWMetodologiaFasesDS_4_Metodologia_descricao1 ,
                                              AV90WWMetodologiaFasesDS_5_Dynamicfiltersenabled2 ,
                                              AV91WWMetodologiaFasesDS_6_Dynamicfiltersselector2 ,
                                              AV92WWMetodologiaFasesDS_7_Dynamicfiltersoperator2 ,
                                              AV93WWMetodologiaFasesDS_8_Metodologiafases_nome2 ,
                                              AV94WWMetodologiaFasesDS_9_Metodologia_descricao2 ,
                                              AV96WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel ,
                                              AV95WWMetodologiaFasesDS_10_Tfmetodologiafases_nome ,
                                              AV97WWMetodologiaFasesDS_12_Tfmetodologiafases_percentual ,
                                              AV98WWMetodologiaFasesDS_13_Tfmetodologiafases_percentual_to ,
                                              AV99WWMetodologiaFasesDS_14_Tfmetodologiafases_prazo ,
                                              AV100WWMetodologiaFasesDS_15_Tfmetodologiafases_prazo_to ,
                                              AV102WWMetodologiaFasesDS_17_Tfmetodologia_descricao_sel ,
                                              AV101WWMetodologiaFasesDS_16_Tfmetodologia_descricao ,
                                              A148MetodologiaFases_Nome ,
                                              A138Metodologia_Descricao ,
                                              A149MetodologiaFases_Percentual ,
                                              A2135MetodologiaFases_Prazo ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV88WWMetodologiaFasesDS_3_Metodologiafases_nome1 = StringUtil.PadR( StringUtil.RTrim( AV88WWMetodologiaFasesDS_3_Metodologiafases_nome1), 50, "%");
         lV88WWMetodologiaFasesDS_3_Metodologiafases_nome1 = StringUtil.PadR( StringUtil.RTrim( AV88WWMetodologiaFasesDS_3_Metodologiafases_nome1), 50, "%");
         lV89WWMetodologiaFasesDS_4_Metodologia_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV89WWMetodologiaFasesDS_4_Metodologia_descricao1), "%", "");
         lV89WWMetodologiaFasesDS_4_Metodologia_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV89WWMetodologiaFasesDS_4_Metodologia_descricao1), "%", "");
         lV93WWMetodologiaFasesDS_8_Metodologiafases_nome2 = StringUtil.PadR( StringUtil.RTrim( AV93WWMetodologiaFasesDS_8_Metodologiafases_nome2), 50, "%");
         lV93WWMetodologiaFasesDS_8_Metodologiafases_nome2 = StringUtil.PadR( StringUtil.RTrim( AV93WWMetodologiaFasesDS_8_Metodologiafases_nome2), 50, "%");
         lV94WWMetodologiaFasesDS_9_Metodologia_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV94WWMetodologiaFasesDS_9_Metodologia_descricao2), "%", "");
         lV94WWMetodologiaFasesDS_9_Metodologia_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV94WWMetodologiaFasesDS_9_Metodologia_descricao2), "%", "");
         lV95WWMetodologiaFasesDS_10_Tfmetodologiafases_nome = StringUtil.PadR( StringUtil.RTrim( AV95WWMetodologiaFasesDS_10_Tfmetodologiafases_nome), 50, "%");
         lV101WWMetodologiaFasesDS_16_Tfmetodologia_descricao = StringUtil.Concat( StringUtil.RTrim( AV101WWMetodologiaFasesDS_16_Tfmetodologia_descricao), "%", "");
         /* Using cursor H007X3 */
         pr_default.execute(1, new Object[] {lV88WWMetodologiaFasesDS_3_Metodologiafases_nome1, lV88WWMetodologiaFasesDS_3_Metodologiafases_nome1, lV89WWMetodologiaFasesDS_4_Metodologia_descricao1, lV89WWMetodologiaFasesDS_4_Metodologia_descricao1, lV93WWMetodologiaFasesDS_8_Metodologiafases_nome2, lV93WWMetodologiaFasesDS_8_Metodologiafases_nome2, lV94WWMetodologiaFasesDS_9_Metodologia_descricao2, lV94WWMetodologiaFasesDS_9_Metodologia_descricao2, lV95WWMetodologiaFasesDS_10_Tfmetodologiafases_nome, AV96WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel, AV97WWMetodologiaFasesDS_12_Tfmetodologiafases_percentual, AV98WWMetodologiaFasesDS_13_Tfmetodologiafases_percentual_to, AV99WWMetodologiaFasesDS_14_Tfmetodologiafases_prazo, AV100WWMetodologiaFasesDS_15_Tfmetodologiafases_prazo_to, lV101WWMetodologiaFasesDS_16_Tfmetodologia_descricao, AV102WWMetodologiaFasesDS_17_Tfmetodologia_descricao_sel});
         GRID_nRecordCount = H007X3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV86WWMetodologiaFasesDS_1_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV87WWMetodologiaFasesDS_2_Dynamicfiltersoperator1 = AV17DynamicFiltersOperator1;
         AV88WWMetodologiaFasesDS_3_Metodologiafases_nome1 = AV18MetodologiaFases_Nome1;
         AV89WWMetodologiaFasesDS_4_Metodologia_descricao1 = AV19Metodologia_Descricao1;
         AV90WWMetodologiaFasesDS_5_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV91WWMetodologiaFasesDS_6_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV92WWMetodologiaFasesDS_7_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV93WWMetodologiaFasesDS_8_Metodologiafases_nome2 = AV23MetodologiaFases_Nome2;
         AV94WWMetodologiaFasesDS_9_Metodologia_descricao2 = AV24Metodologia_Descricao2;
         AV95WWMetodologiaFasesDS_10_Tfmetodologiafases_nome = AV65TFMetodologiaFases_Nome;
         AV96WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel = AV66TFMetodologiaFases_Nome_Sel;
         AV97WWMetodologiaFasesDS_12_Tfmetodologiafases_percentual = AV69TFMetodologiaFases_Percentual;
         AV98WWMetodologiaFasesDS_13_Tfmetodologiafases_percentual_to = AV70TFMetodologiaFases_Percentual_To;
         AV99WWMetodologiaFasesDS_14_Tfmetodologiafases_prazo = AV81TFMetodologiaFases_Prazo;
         AV100WWMetodologiaFasesDS_15_Tfmetodologiafases_prazo_to = AV82TFMetodologiaFases_Prazo_To;
         AV101WWMetodologiaFasesDS_16_Tfmetodologia_descricao = AV73TFMetodologia_Descricao;
         AV102WWMetodologiaFasesDS_17_Tfmetodologia_descricao_sel = AV74TFMetodologia_Descricao_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18MetodologiaFases_Nome1, AV19Metodologia_Descricao1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23MetodologiaFases_Nome2, AV24Metodologia_Descricao2, AV20DynamicFiltersEnabled2, AV65TFMetodologiaFases_Nome, AV66TFMetodologiaFases_Nome_Sel, AV69TFMetodologiaFases_Percentual, AV70TFMetodologiaFases_Percentual_To, AV81TFMetodologiaFases_Prazo, AV82TFMetodologiaFases_Prazo_To, AV73TFMetodologia_Descricao, AV74TFMetodologia_Descricao_Sel, AV67ddo_MetodologiaFases_NomeTitleControlIdToReplace, AV71ddo_MetodologiaFases_PercentualTitleControlIdToReplace, AV83ddo_MetodologiaFases_PrazoTitleControlIdToReplace, AV75ddo_Metodologia_DescricaoTitleControlIdToReplace, AV106Pgmname, AV10GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A147MetodologiaFases_Codigo, A137Metodologia_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV86WWMetodologiaFasesDS_1_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV87WWMetodologiaFasesDS_2_Dynamicfiltersoperator1 = AV17DynamicFiltersOperator1;
         AV88WWMetodologiaFasesDS_3_Metodologiafases_nome1 = AV18MetodologiaFases_Nome1;
         AV89WWMetodologiaFasesDS_4_Metodologia_descricao1 = AV19Metodologia_Descricao1;
         AV90WWMetodologiaFasesDS_5_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV91WWMetodologiaFasesDS_6_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV92WWMetodologiaFasesDS_7_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV93WWMetodologiaFasesDS_8_Metodologiafases_nome2 = AV23MetodologiaFases_Nome2;
         AV94WWMetodologiaFasesDS_9_Metodologia_descricao2 = AV24Metodologia_Descricao2;
         AV95WWMetodologiaFasesDS_10_Tfmetodologiafases_nome = AV65TFMetodologiaFases_Nome;
         AV96WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel = AV66TFMetodologiaFases_Nome_Sel;
         AV97WWMetodologiaFasesDS_12_Tfmetodologiafases_percentual = AV69TFMetodologiaFases_Percentual;
         AV98WWMetodologiaFasesDS_13_Tfmetodologiafases_percentual_to = AV70TFMetodologiaFases_Percentual_To;
         AV99WWMetodologiaFasesDS_14_Tfmetodologiafases_prazo = AV81TFMetodologiaFases_Prazo;
         AV100WWMetodologiaFasesDS_15_Tfmetodologiafases_prazo_to = AV82TFMetodologiaFases_Prazo_To;
         AV101WWMetodologiaFasesDS_16_Tfmetodologia_descricao = AV73TFMetodologia_Descricao;
         AV102WWMetodologiaFasesDS_17_Tfmetodologia_descricao_sel = AV74TFMetodologia_Descricao_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18MetodologiaFases_Nome1, AV19Metodologia_Descricao1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23MetodologiaFases_Nome2, AV24Metodologia_Descricao2, AV20DynamicFiltersEnabled2, AV65TFMetodologiaFases_Nome, AV66TFMetodologiaFases_Nome_Sel, AV69TFMetodologiaFases_Percentual, AV70TFMetodologiaFases_Percentual_To, AV81TFMetodologiaFases_Prazo, AV82TFMetodologiaFases_Prazo_To, AV73TFMetodologia_Descricao, AV74TFMetodologia_Descricao_Sel, AV67ddo_MetodologiaFases_NomeTitleControlIdToReplace, AV71ddo_MetodologiaFases_PercentualTitleControlIdToReplace, AV83ddo_MetodologiaFases_PrazoTitleControlIdToReplace, AV75ddo_Metodologia_DescricaoTitleControlIdToReplace, AV106Pgmname, AV10GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A147MetodologiaFases_Codigo, A137Metodologia_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV86WWMetodologiaFasesDS_1_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV87WWMetodologiaFasesDS_2_Dynamicfiltersoperator1 = AV17DynamicFiltersOperator1;
         AV88WWMetodologiaFasesDS_3_Metodologiafases_nome1 = AV18MetodologiaFases_Nome1;
         AV89WWMetodologiaFasesDS_4_Metodologia_descricao1 = AV19Metodologia_Descricao1;
         AV90WWMetodologiaFasesDS_5_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV91WWMetodologiaFasesDS_6_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV92WWMetodologiaFasesDS_7_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV93WWMetodologiaFasesDS_8_Metodologiafases_nome2 = AV23MetodologiaFases_Nome2;
         AV94WWMetodologiaFasesDS_9_Metodologia_descricao2 = AV24Metodologia_Descricao2;
         AV95WWMetodologiaFasesDS_10_Tfmetodologiafases_nome = AV65TFMetodologiaFases_Nome;
         AV96WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel = AV66TFMetodologiaFases_Nome_Sel;
         AV97WWMetodologiaFasesDS_12_Tfmetodologiafases_percentual = AV69TFMetodologiaFases_Percentual;
         AV98WWMetodologiaFasesDS_13_Tfmetodologiafases_percentual_to = AV70TFMetodologiaFases_Percentual_To;
         AV99WWMetodologiaFasesDS_14_Tfmetodologiafases_prazo = AV81TFMetodologiaFases_Prazo;
         AV100WWMetodologiaFasesDS_15_Tfmetodologiafases_prazo_to = AV82TFMetodologiaFases_Prazo_To;
         AV101WWMetodologiaFasesDS_16_Tfmetodologia_descricao = AV73TFMetodologia_Descricao;
         AV102WWMetodologiaFasesDS_17_Tfmetodologia_descricao_sel = AV74TFMetodologia_Descricao_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18MetodologiaFases_Nome1, AV19Metodologia_Descricao1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23MetodologiaFases_Nome2, AV24Metodologia_Descricao2, AV20DynamicFiltersEnabled2, AV65TFMetodologiaFases_Nome, AV66TFMetodologiaFases_Nome_Sel, AV69TFMetodologiaFases_Percentual, AV70TFMetodologiaFases_Percentual_To, AV81TFMetodologiaFases_Prazo, AV82TFMetodologiaFases_Prazo_To, AV73TFMetodologia_Descricao, AV74TFMetodologia_Descricao_Sel, AV67ddo_MetodologiaFases_NomeTitleControlIdToReplace, AV71ddo_MetodologiaFases_PercentualTitleControlIdToReplace, AV83ddo_MetodologiaFases_PrazoTitleControlIdToReplace, AV75ddo_Metodologia_DescricaoTitleControlIdToReplace, AV106Pgmname, AV10GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A147MetodologiaFases_Codigo, A137Metodologia_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV86WWMetodologiaFasesDS_1_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV87WWMetodologiaFasesDS_2_Dynamicfiltersoperator1 = AV17DynamicFiltersOperator1;
         AV88WWMetodologiaFasesDS_3_Metodologiafases_nome1 = AV18MetodologiaFases_Nome1;
         AV89WWMetodologiaFasesDS_4_Metodologia_descricao1 = AV19Metodologia_Descricao1;
         AV90WWMetodologiaFasesDS_5_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV91WWMetodologiaFasesDS_6_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV92WWMetodologiaFasesDS_7_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV93WWMetodologiaFasesDS_8_Metodologiafases_nome2 = AV23MetodologiaFases_Nome2;
         AV94WWMetodologiaFasesDS_9_Metodologia_descricao2 = AV24Metodologia_Descricao2;
         AV95WWMetodologiaFasesDS_10_Tfmetodologiafases_nome = AV65TFMetodologiaFases_Nome;
         AV96WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel = AV66TFMetodologiaFases_Nome_Sel;
         AV97WWMetodologiaFasesDS_12_Tfmetodologiafases_percentual = AV69TFMetodologiaFases_Percentual;
         AV98WWMetodologiaFasesDS_13_Tfmetodologiafases_percentual_to = AV70TFMetodologiaFases_Percentual_To;
         AV99WWMetodologiaFasesDS_14_Tfmetodologiafases_prazo = AV81TFMetodologiaFases_Prazo;
         AV100WWMetodologiaFasesDS_15_Tfmetodologiafases_prazo_to = AV82TFMetodologiaFases_Prazo_To;
         AV101WWMetodologiaFasesDS_16_Tfmetodologia_descricao = AV73TFMetodologia_Descricao;
         AV102WWMetodologiaFasesDS_17_Tfmetodologia_descricao_sel = AV74TFMetodologia_Descricao_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18MetodologiaFases_Nome1, AV19Metodologia_Descricao1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23MetodologiaFases_Nome2, AV24Metodologia_Descricao2, AV20DynamicFiltersEnabled2, AV65TFMetodologiaFases_Nome, AV66TFMetodologiaFases_Nome_Sel, AV69TFMetodologiaFases_Percentual, AV70TFMetodologiaFases_Percentual_To, AV81TFMetodologiaFases_Prazo, AV82TFMetodologiaFases_Prazo_To, AV73TFMetodologia_Descricao, AV74TFMetodologia_Descricao_Sel, AV67ddo_MetodologiaFases_NomeTitleControlIdToReplace, AV71ddo_MetodologiaFases_PercentualTitleControlIdToReplace, AV83ddo_MetodologiaFases_PrazoTitleControlIdToReplace, AV75ddo_Metodologia_DescricaoTitleControlIdToReplace, AV106Pgmname, AV10GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A147MetodologiaFases_Codigo, A137Metodologia_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV86WWMetodologiaFasesDS_1_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV87WWMetodologiaFasesDS_2_Dynamicfiltersoperator1 = AV17DynamicFiltersOperator1;
         AV88WWMetodologiaFasesDS_3_Metodologiafases_nome1 = AV18MetodologiaFases_Nome1;
         AV89WWMetodologiaFasesDS_4_Metodologia_descricao1 = AV19Metodologia_Descricao1;
         AV90WWMetodologiaFasesDS_5_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV91WWMetodologiaFasesDS_6_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV92WWMetodologiaFasesDS_7_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV93WWMetodologiaFasesDS_8_Metodologiafases_nome2 = AV23MetodologiaFases_Nome2;
         AV94WWMetodologiaFasesDS_9_Metodologia_descricao2 = AV24Metodologia_Descricao2;
         AV95WWMetodologiaFasesDS_10_Tfmetodologiafases_nome = AV65TFMetodologiaFases_Nome;
         AV96WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel = AV66TFMetodologiaFases_Nome_Sel;
         AV97WWMetodologiaFasesDS_12_Tfmetodologiafases_percentual = AV69TFMetodologiaFases_Percentual;
         AV98WWMetodologiaFasesDS_13_Tfmetodologiafases_percentual_to = AV70TFMetodologiaFases_Percentual_To;
         AV99WWMetodologiaFasesDS_14_Tfmetodologiafases_prazo = AV81TFMetodologiaFases_Prazo;
         AV100WWMetodologiaFasesDS_15_Tfmetodologiafases_prazo_to = AV82TFMetodologiaFases_Prazo_To;
         AV101WWMetodologiaFasesDS_16_Tfmetodologia_descricao = AV73TFMetodologia_Descricao;
         AV102WWMetodologiaFasesDS_17_Tfmetodologia_descricao_sel = AV74TFMetodologia_Descricao_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18MetodologiaFases_Nome1, AV19Metodologia_Descricao1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23MetodologiaFases_Nome2, AV24Metodologia_Descricao2, AV20DynamicFiltersEnabled2, AV65TFMetodologiaFases_Nome, AV66TFMetodologiaFases_Nome_Sel, AV69TFMetodologiaFases_Percentual, AV70TFMetodologiaFases_Percentual_To, AV81TFMetodologiaFases_Prazo, AV82TFMetodologiaFases_Prazo_To, AV73TFMetodologia_Descricao, AV74TFMetodologia_Descricao_Sel, AV67ddo_MetodologiaFases_NomeTitleControlIdToReplace, AV71ddo_MetodologiaFases_PercentualTitleControlIdToReplace, AV83ddo_MetodologiaFases_PrazoTitleControlIdToReplace, AV75ddo_Metodologia_DescricaoTitleControlIdToReplace, AV106Pgmname, AV10GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A147MetodologiaFases_Codigo, A137Metodologia_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUP7X0( )
      {
         /* Before Start, stand alone formulas. */
         AV106Pgmname = "WWMetodologiaFases";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E247X2 */
         E247X2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV76DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vMETODOLOGIAFASES_NOMETITLEFILTERDATA"), AV64MetodologiaFases_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vMETODOLOGIAFASES_PERCENTUALTITLEFILTERDATA"), AV68MetodologiaFases_PercentualTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vMETODOLOGIAFASES_PRAZOTITLEFILTERDATA"), AV80MetodologiaFases_PrazoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vMETODOLOGIA_DESCRICAOTITLEFILTERDATA"), AV72Metodologia_DescricaoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV16DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
            AV18MetodologiaFases_Nome1 = StringUtil.Upper( cgiGet( edtavMetodologiafases_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18MetodologiaFases_Nome1", AV18MetodologiaFases_Nome1);
            AV19Metodologia_Descricao1 = StringUtil.Upper( cgiGet( edtavMetodologia_descricao1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Metodologia_Descricao1", AV19Metodologia_Descricao1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV21DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
            AV23MetodologiaFases_Nome2 = StringUtil.Upper( cgiGet( edtavMetodologiafases_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23MetodologiaFases_Nome2", AV23MetodologiaFases_Nome2);
            AV24Metodologia_Descricao2 = StringUtil.Upper( cgiGet( edtavMetodologia_descricao2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Metodologia_Descricao2", AV24Metodologia_Descricao2);
            AV20DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
            AV65TFMetodologiaFases_Nome = StringUtil.Upper( cgiGet( edtavTfmetodologiafases_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFMetodologiaFases_Nome", AV65TFMetodologiaFases_Nome);
            AV66TFMetodologiaFases_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfmetodologiafases_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFMetodologiaFases_Nome_Sel", AV66TFMetodologiaFases_Nome_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfmetodologiafases_percentual_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfmetodologiafases_percentual_Internalname), ",", ".") > 999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFMETODOLOGIAFASES_PERCENTUAL");
               GX_FocusControl = edtavTfmetodologiafases_percentual_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV69TFMetodologiaFases_Percentual = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFMetodologiaFases_Percentual", StringUtil.LTrim( StringUtil.Str( AV69TFMetodologiaFases_Percentual, 6, 2)));
            }
            else
            {
               AV69TFMetodologiaFases_Percentual = context.localUtil.CToN( cgiGet( edtavTfmetodologiafases_percentual_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFMetodologiaFases_Percentual", StringUtil.LTrim( StringUtil.Str( AV69TFMetodologiaFases_Percentual, 6, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfmetodologiafases_percentual_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfmetodologiafases_percentual_to_Internalname), ",", ".") > 999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFMETODOLOGIAFASES_PERCENTUAL_TO");
               GX_FocusControl = edtavTfmetodologiafases_percentual_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV70TFMetodologiaFases_Percentual_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFMetodologiaFases_Percentual_To", StringUtil.LTrim( StringUtil.Str( AV70TFMetodologiaFases_Percentual_To, 6, 2)));
            }
            else
            {
               AV70TFMetodologiaFases_Percentual_To = context.localUtil.CToN( cgiGet( edtavTfmetodologiafases_percentual_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFMetodologiaFases_Percentual_To", StringUtil.LTrim( StringUtil.Str( AV70TFMetodologiaFases_Percentual_To, 6, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfmetodologiafases_prazo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfmetodologiafases_prazo_Internalname), ",", ".") > 9999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFMETODOLOGIAFASES_PRAZO");
               GX_FocusControl = edtavTfmetodologiafases_prazo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV81TFMetodologiaFases_Prazo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFMetodologiaFases_Prazo", StringUtil.LTrim( StringUtil.Str( AV81TFMetodologiaFases_Prazo, 7, 2)));
            }
            else
            {
               AV81TFMetodologiaFases_Prazo = context.localUtil.CToN( cgiGet( edtavTfmetodologiafases_prazo_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFMetodologiaFases_Prazo", StringUtil.LTrim( StringUtil.Str( AV81TFMetodologiaFases_Prazo, 7, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfmetodologiafases_prazo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfmetodologiafases_prazo_to_Internalname), ",", ".") > 9999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFMETODOLOGIAFASES_PRAZO_TO");
               GX_FocusControl = edtavTfmetodologiafases_prazo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV82TFMetodologiaFases_Prazo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFMetodologiaFases_Prazo_To", StringUtil.LTrim( StringUtil.Str( AV82TFMetodologiaFases_Prazo_To, 7, 2)));
            }
            else
            {
               AV82TFMetodologiaFases_Prazo_To = context.localUtil.CToN( cgiGet( edtavTfmetodologiafases_prazo_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFMetodologiaFases_Prazo_To", StringUtil.LTrim( StringUtil.Str( AV82TFMetodologiaFases_Prazo_To, 7, 2)));
            }
            AV73TFMetodologia_Descricao = StringUtil.Upper( cgiGet( edtavTfmetodologia_descricao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFMetodologia_Descricao", AV73TFMetodologia_Descricao);
            AV74TFMetodologia_Descricao_Sel = StringUtil.Upper( cgiGet( edtavTfmetodologia_descricao_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFMetodologia_Descricao_Sel", AV74TFMetodologia_Descricao_Sel);
            AV67ddo_MetodologiaFases_NomeTitleControlIdToReplace = cgiGet( edtavDdo_metodologiafases_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ddo_MetodologiaFases_NomeTitleControlIdToReplace", AV67ddo_MetodologiaFases_NomeTitleControlIdToReplace);
            AV71ddo_MetodologiaFases_PercentualTitleControlIdToReplace = cgiGet( edtavDdo_metodologiafases_percentualtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ddo_MetodologiaFases_PercentualTitleControlIdToReplace", AV71ddo_MetodologiaFases_PercentualTitleControlIdToReplace);
            AV83ddo_MetodologiaFases_PrazoTitleControlIdToReplace = cgiGet( edtavDdo_metodologiafases_prazotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83ddo_MetodologiaFases_PrazoTitleControlIdToReplace", AV83ddo_MetodologiaFases_PrazoTitleControlIdToReplace);
            AV75ddo_Metodologia_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_metodologia_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75ddo_Metodologia_DescricaoTitleControlIdToReplace", AV75ddo_Metodologia_DescricaoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_73 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_73"), ",", "."));
            AV78GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV79GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_metodologiafases_nome_Caption = cgiGet( "DDO_METODOLOGIAFASES_NOME_Caption");
            Ddo_metodologiafases_nome_Tooltip = cgiGet( "DDO_METODOLOGIAFASES_NOME_Tooltip");
            Ddo_metodologiafases_nome_Cls = cgiGet( "DDO_METODOLOGIAFASES_NOME_Cls");
            Ddo_metodologiafases_nome_Filteredtext_set = cgiGet( "DDO_METODOLOGIAFASES_NOME_Filteredtext_set");
            Ddo_metodologiafases_nome_Selectedvalue_set = cgiGet( "DDO_METODOLOGIAFASES_NOME_Selectedvalue_set");
            Ddo_metodologiafases_nome_Dropdownoptionstype = cgiGet( "DDO_METODOLOGIAFASES_NOME_Dropdownoptionstype");
            Ddo_metodologiafases_nome_Titlecontrolidtoreplace = cgiGet( "DDO_METODOLOGIAFASES_NOME_Titlecontrolidtoreplace");
            Ddo_metodologiafases_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_METODOLOGIAFASES_NOME_Includesortasc"));
            Ddo_metodologiafases_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_METODOLOGIAFASES_NOME_Includesortdsc"));
            Ddo_metodologiafases_nome_Sortedstatus = cgiGet( "DDO_METODOLOGIAFASES_NOME_Sortedstatus");
            Ddo_metodologiafases_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_METODOLOGIAFASES_NOME_Includefilter"));
            Ddo_metodologiafases_nome_Filtertype = cgiGet( "DDO_METODOLOGIAFASES_NOME_Filtertype");
            Ddo_metodologiafases_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_METODOLOGIAFASES_NOME_Filterisrange"));
            Ddo_metodologiafases_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_METODOLOGIAFASES_NOME_Includedatalist"));
            Ddo_metodologiafases_nome_Datalisttype = cgiGet( "DDO_METODOLOGIAFASES_NOME_Datalisttype");
            Ddo_metodologiafases_nome_Datalistproc = cgiGet( "DDO_METODOLOGIAFASES_NOME_Datalistproc");
            Ddo_metodologiafases_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_METODOLOGIAFASES_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_metodologiafases_nome_Sortasc = cgiGet( "DDO_METODOLOGIAFASES_NOME_Sortasc");
            Ddo_metodologiafases_nome_Sortdsc = cgiGet( "DDO_METODOLOGIAFASES_NOME_Sortdsc");
            Ddo_metodologiafases_nome_Loadingdata = cgiGet( "DDO_METODOLOGIAFASES_NOME_Loadingdata");
            Ddo_metodologiafases_nome_Cleanfilter = cgiGet( "DDO_METODOLOGIAFASES_NOME_Cleanfilter");
            Ddo_metodologiafases_nome_Noresultsfound = cgiGet( "DDO_METODOLOGIAFASES_NOME_Noresultsfound");
            Ddo_metodologiafases_nome_Searchbuttontext = cgiGet( "DDO_METODOLOGIAFASES_NOME_Searchbuttontext");
            Ddo_metodologiafases_percentual_Caption = cgiGet( "DDO_METODOLOGIAFASES_PERCENTUAL_Caption");
            Ddo_metodologiafases_percentual_Tooltip = cgiGet( "DDO_METODOLOGIAFASES_PERCENTUAL_Tooltip");
            Ddo_metodologiafases_percentual_Cls = cgiGet( "DDO_METODOLOGIAFASES_PERCENTUAL_Cls");
            Ddo_metodologiafases_percentual_Filteredtext_set = cgiGet( "DDO_METODOLOGIAFASES_PERCENTUAL_Filteredtext_set");
            Ddo_metodologiafases_percentual_Filteredtextto_set = cgiGet( "DDO_METODOLOGIAFASES_PERCENTUAL_Filteredtextto_set");
            Ddo_metodologiafases_percentual_Dropdownoptionstype = cgiGet( "DDO_METODOLOGIAFASES_PERCENTUAL_Dropdownoptionstype");
            Ddo_metodologiafases_percentual_Titlecontrolidtoreplace = cgiGet( "DDO_METODOLOGIAFASES_PERCENTUAL_Titlecontrolidtoreplace");
            Ddo_metodologiafases_percentual_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_METODOLOGIAFASES_PERCENTUAL_Includesortasc"));
            Ddo_metodologiafases_percentual_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_METODOLOGIAFASES_PERCENTUAL_Includesortdsc"));
            Ddo_metodologiafases_percentual_Sortedstatus = cgiGet( "DDO_METODOLOGIAFASES_PERCENTUAL_Sortedstatus");
            Ddo_metodologiafases_percentual_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_METODOLOGIAFASES_PERCENTUAL_Includefilter"));
            Ddo_metodologiafases_percentual_Filtertype = cgiGet( "DDO_METODOLOGIAFASES_PERCENTUAL_Filtertype");
            Ddo_metodologiafases_percentual_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_METODOLOGIAFASES_PERCENTUAL_Filterisrange"));
            Ddo_metodologiafases_percentual_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_METODOLOGIAFASES_PERCENTUAL_Includedatalist"));
            Ddo_metodologiafases_percentual_Sortasc = cgiGet( "DDO_METODOLOGIAFASES_PERCENTUAL_Sortasc");
            Ddo_metodologiafases_percentual_Sortdsc = cgiGet( "DDO_METODOLOGIAFASES_PERCENTUAL_Sortdsc");
            Ddo_metodologiafases_percentual_Cleanfilter = cgiGet( "DDO_METODOLOGIAFASES_PERCENTUAL_Cleanfilter");
            Ddo_metodologiafases_percentual_Rangefilterfrom = cgiGet( "DDO_METODOLOGIAFASES_PERCENTUAL_Rangefilterfrom");
            Ddo_metodologiafases_percentual_Rangefilterto = cgiGet( "DDO_METODOLOGIAFASES_PERCENTUAL_Rangefilterto");
            Ddo_metodologiafases_percentual_Searchbuttontext = cgiGet( "DDO_METODOLOGIAFASES_PERCENTUAL_Searchbuttontext");
            Ddo_metodologiafases_prazo_Caption = cgiGet( "DDO_METODOLOGIAFASES_PRAZO_Caption");
            Ddo_metodologiafases_prazo_Tooltip = cgiGet( "DDO_METODOLOGIAFASES_PRAZO_Tooltip");
            Ddo_metodologiafases_prazo_Cls = cgiGet( "DDO_METODOLOGIAFASES_PRAZO_Cls");
            Ddo_metodologiafases_prazo_Filteredtext_set = cgiGet( "DDO_METODOLOGIAFASES_PRAZO_Filteredtext_set");
            Ddo_metodologiafases_prazo_Filteredtextto_set = cgiGet( "DDO_METODOLOGIAFASES_PRAZO_Filteredtextto_set");
            Ddo_metodologiafases_prazo_Dropdownoptionstype = cgiGet( "DDO_METODOLOGIAFASES_PRAZO_Dropdownoptionstype");
            Ddo_metodologiafases_prazo_Titlecontrolidtoreplace = cgiGet( "DDO_METODOLOGIAFASES_PRAZO_Titlecontrolidtoreplace");
            Ddo_metodologiafases_prazo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_METODOLOGIAFASES_PRAZO_Includesortasc"));
            Ddo_metodologiafases_prazo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_METODOLOGIAFASES_PRAZO_Includesortdsc"));
            Ddo_metodologiafases_prazo_Sortedstatus = cgiGet( "DDO_METODOLOGIAFASES_PRAZO_Sortedstatus");
            Ddo_metodologiafases_prazo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_METODOLOGIAFASES_PRAZO_Includefilter"));
            Ddo_metodologiafases_prazo_Filtertype = cgiGet( "DDO_METODOLOGIAFASES_PRAZO_Filtertype");
            Ddo_metodologiafases_prazo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_METODOLOGIAFASES_PRAZO_Filterisrange"));
            Ddo_metodologiafases_prazo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_METODOLOGIAFASES_PRAZO_Includedatalist"));
            Ddo_metodologiafases_prazo_Sortasc = cgiGet( "DDO_METODOLOGIAFASES_PRAZO_Sortasc");
            Ddo_metodologiafases_prazo_Sortdsc = cgiGet( "DDO_METODOLOGIAFASES_PRAZO_Sortdsc");
            Ddo_metodologiafases_prazo_Cleanfilter = cgiGet( "DDO_METODOLOGIAFASES_PRAZO_Cleanfilter");
            Ddo_metodologiafases_prazo_Rangefilterfrom = cgiGet( "DDO_METODOLOGIAFASES_PRAZO_Rangefilterfrom");
            Ddo_metodologiafases_prazo_Rangefilterto = cgiGet( "DDO_METODOLOGIAFASES_PRAZO_Rangefilterto");
            Ddo_metodologiafases_prazo_Searchbuttontext = cgiGet( "DDO_METODOLOGIAFASES_PRAZO_Searchbuttontext");
            Ddo_metodologia_descricao_Caption = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Caption");
            Ddo_metodologia_descricao_Tooltip = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Tooltip");
            Ddo_metodologia_descricao_Cls = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Cls");
            Ddo_metodologia_descricao_Filteredtext_set = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Filteredtext_set");
            Ddo_metodologia_descricao_Selectedvalue_set = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Selectedvalue_set");
            Ddo_metodologia_descricao_Dropdownoptionstype = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Dropdownoptionstype");
            Ddo_metodologia_descricao_Titlecontrolidtoreplace = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_metodologia_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_METODOLOGIA_DESCRICAO_Includesortasc"));
            Ddo_metodologia_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_METODOLOGIA_DESCRICAO_Includesortdsc"));
            Ddo_metodologia_descricao_Sortedstatus = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Sortedstatus");
            Ddo_metodologia_descricao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_METODOLOGIA_DESCRICAO_Includefilter"));
            Ddo_metodologia_descricao_Filtertype = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Filtertype");
            Ddo_metodologia_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_METODOLOGIA_DESCRICAO_Filterisrange"));
            Ddo_metodologia_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_METODOLOGIA_DESCRICAO_Includedatalist"));
            Ddo_metodologia_descricao_Datalisttype = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Datalisttype");
            Ddo_metodologia_descricao_Datalistproc = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Datalistproc");
            Ddo_metodologia_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_METODOLOGIA_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_metodologia_descricao_Sortasc = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Sortasc");
            Ddo_metodologia_descricao_Sortdsc = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Sortdsc");
            Ddo_metodologia_descricao_Loadingdata = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Loadingdata");
            Ddo_metodologia_descricao_Cleanfilter = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Cleanfilter");
            Ddo_metodologia_descricao_Noresultsfound = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Noresultsfound");
            Ddo_metodologia_descricao_Searchbuttontext = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_metodologiafases_nome_Activeeventkey = cgiGet( "DDO_METODOLOGIAFASES_NOME_Activeeventkey");
            Ddo_metodologiafases_nome_Filteredtext_get = cgiGet( "DDO_METODOLOGIAFASES_NOME_Filteredtext_get");
            Ddo_metodologiafases_nome_Selectedvalue_get = cgiGet( "DDO_METODOLOGIAFASES_NOME_Selectedvalue_get");
            Ddo_metodologiafases_percentual_Activeeventkey = cgiGet( "DDO_METODOLOGIAFASES_PERCENTUAL_Activeeventkey");
            Ddo_metodologiafases_percentual_Filteredtext_get = cgiGet( "DDO_METODOLOGIAFASES_PERCENTUAL_Filteredtext_get");
            Ddo_metodologiafases_percentual_Filteredtextto_get = cgiGet( "DDO_METODOLOGIAFASES_PERCENTUAL_Filteredtextto_get");
            Ddo_metodologiafases_prazo_Activeeventkey = cgiGet( "DDO_METODOLOGIAFASES_PRAZO_Activeeventkey");
            Ddo_metodologiafases_prazo_Filteredtext_get = cgiGet( "DDO_METODOLOGIAFASES_PRAZO_Filteredtext_get");
            Ddo_metodologiafases_prazo_Filteredtextto_get = cgiGet( "DDO_METODOLOGIAFASES_PRAZO_Filteredtextto_get");
            Ddo_metodologia_descricao_Activeeventkey = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Activeeventkey");
            Ddo_metodologia_descricao_Filteredtext_get = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Filteredtext_get");
            Ddo_metodologia_descricao_Selectedvalue_get = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV17DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vMETODOLOGIAFASES_NOME1"), AV18MetodologiaFases_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vMETODOLOGIA_DESCRICAO1"), AV19Metodologia_Descricao1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV22DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vMETODOLOGIAFASES_NOME2"), AV23MetodologiaFases_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vMETODOLOGIA_DESCRICAO2"), AV24Metodologia_Descricao2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMETODOLOGIAFASES_NOME"), AV65TFMetodologiaFases_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMETODOLOGIAFASES_NOME_SEL"), AV66TFMetodologiaFases_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFMETODOLOGIAFASES_PERCENTUAL"), ",", ".") != AV69TFMetodologiaFases_Percentual )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFMETODOLOGIAFASES_PERCENTUAL_TO"), ",", ".") != AV70TFMetodologiaFases_Percentual_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFMETODOLOGIAFASES_PRAZO"), ",", ".") != AV81TFMetodologiaFases_Prazo )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFMETODOLOGIAFASES_PRAZO_TO"), ",", ".") != AV82TFMetodologiaFases_Prazo_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMETODOLOGIA_DESCRICAO"), AV73TFMetodologia_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMETODOLOGIA_DESCRICAO_SEL"), AV74TFMetodologia_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E247X2 */
         E247X2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E247X2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         AV16DynamicFiltersSelector1 = "METODOLOGIAFASES_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV21DynamicFiltersSelector2 = "METODOLOGIAFASES_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         edtavTfmetodologiafases_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfmetodologiafases_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfmetodologiafases_nome_Visible), 5, 0)));
         edtavTfmetodologiafases_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfmetodologiafases_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfmetodologiafases_nome_sel_Visible), 5, 0)));
         edtavTfmetodologiafases_percentual_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfmetodologiafases_percentual_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfmetodologiafases_percentual_Visible), 5, 0)));
         edtavTfmetodologiafases_percentual_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfmetodologiafases_percentual_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfmetodologiafases_percentual_to_Visible), 5, 0)));
         edtavTfmetodologiafases_prazo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfmetodologiafases_prazo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfmetodologiafases_prazo_Visible), 5, 0)));
         edtavTfmetodologiafases_prazo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfmetodologiafases_prazo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfmetodologiafases_prazo_to_Visible), 5, 0)));
         edtavTfmetodologia_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfmetodologia_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfmetodologia_descricao_Visible), 5, 0)));
         edtavTfmetodologia_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfmetodologia_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfmetodologia_descricao_sel_Visible), 5, 0)));
         Ddo_metodologiafases_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_MetodologiaFases_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologiafases_nome_Internalname, "TitleControlIdToReplace", Ddo_metodologiafases_nome_Titlecontrolidtoreplace);
         AV67ddo_MetodologiaFases_NomeTitleControlIdToReplace = Ddo_metodologiafases_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ddo_MetodologiaFases_NomeTitleControlIdToReplace", AV67ddo_MetodologiaFases_NomeTitleControlIdToReplace);
         edtavDdo_metodologiafases_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_metodologiafases_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_metodologiafases_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_metodologiafases_percentual_Titlecontrolidtoreplace = subGrid_Internalname+"_MetodologiaFases_Percentual";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologiafases_percentual_Internalname, "TitleControlIdToReplace", Ddo_metodologiafases_percentual_Titlecontrolidtoreplace);
         AV71ddo_MetodologiaFases_PercentualTitleControlIdToReplace = Ddo_metodologiafases_percentual_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ddo_MetodologiaFases_PercentualTitleControlIdToReplace", AV71ddo_MetodologiaFases_PercentualTitleControlIdToReplace);
         edtavDdo_metodologiafases_percentualtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_metodologiafases_percentualtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_metodologiafases_percentualtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_metodologiafases_prazo_Titlecontrolidtoreplace = subGrid_Internalname+"_MetodologiaFases_Prazo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologiafases_prazo_Internalname, "TitleControlIdToReplace", Ddo_metodologiafases_prazo_Titlecontrolidtoreplace);
         AV83ddo_MetodologiaFases_PrazoTitleControlIdToReplace = Ddo_metodologiafases_prazo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83ddo_MetodologiaFases_PrazoTitleControlIdToReplace", AV83ddo_MetodologiaFases_PrazoTitleControlIdToReplace);
         edtavDdo_metodologiafases_prazotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_metodologiafases_prazotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_metodologiafases_prazotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_metodologia_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_Metodologia_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologia_descricao_Internalname, "TitleControlIdToReplace", Ddo_metodologia_descricao_Titlecontrolidtoreplace);
         AV75ddo_Metodologia_DescricaoTitleControlIdToReplace = Ddo_metodologia_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75ddo_Metodologia_DescricaoTitleControlIdToReplace", AV75ddo_Metodologia_DescricaoTitleControlIdToReplace);
         edtavDdo_metodologia_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_metodologia_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_metodologia_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Metodologia Fases";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Nome da Fase", 0);
         cmbavOrderedby.addItem("2", "Percentual", 0);
         cmbavOrderedby.addItem("3", "Prazo", 0);
         cmbavOrderedby.addItem("4", "Metodologia", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV76DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV76DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E257X2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV64MetodologiaFases_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV68MetodologiaFases_PercentualTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV80MetodologiaFases_PrazoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV72Metodologia_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         cmbavDynamicfiltersoperator1.removeAllItems();
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "METODOLOGIAFASES_NOME") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "METODOLOGIA_DESCRICAO") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         if ( AV20DynamicFiltersEnabled2 )
         {
            cmbavDynamicfiltersoperator2.removeAllItems();
            if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "METODOLOGIAFASES_NOME") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "METODOLOGIA_DESCRICAO") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtMetodologiaFases_Nome_Titleformat = 2;
         edtMetodologiaFases_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Fase", AV67ddo_MetodologiaFases_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMetodologiaFases_Nome_Internalname, "Title", edtMetodologiaFases_Nome_Title);
         edtMetodologiaFases_Percentual_Titleformat = 2;
         edtMetodologiaFases_Percentual_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Percentual", AV71ddo_MetodologiaFases_PercentualTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMetodologiaFases_Percentual_Internalname, "Title", edtMetodologiaFases_Percentual_Title);
         edtMetodologiaFases_Prazo_Titleformat = 2;
         edtMetodologiaFases_Prazo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Prazo", AV83ddo_MetodologiaFases_PrazoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMetodologiaFases_Prazo_Internalname, "Title", edtMetodologiaFases_Prazo_Title);
         edtMetodologia_Descricao_Titleformat = 2;
         edtMetodologia_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Metodologia", AV75ddo_Metodologia_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMetodologia_Descricao_Internalname, "Title", edtMetodologia_Descricao_Title);
         AV78GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV78GridCurrentPage), 10, 0)));
         AV79GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV79GridPageCount), 10, 0)));
         AV86WWMetodologiaFasesDS_1_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV87WWMetodologiaFasesDS_2_Dynamicfiltersoperator1 = AV17DynamicFiltersOperator1;
         AV88WWMetodologiaFasesDS_3_Metodologiafases_nome1 = AV18MetodologiaFases_Nome1;
         AV89WWMetodologiaFasesDS_4_Metodologia_descricao1 = AV19Metodologia_Descricao1;
         AV90WWMetodologiaFasesDS_5_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV91WWMetodologiaFasesDS_6_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV92WWMetodologiaFasesDS_7_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV93WWMetodologiaFasesDS_8_Metodologiafases_nome2 = AV23MetodologiaFases_Nome2;
         AV94WWMetodologiaFasesDS_9_Metodologia_descricao2 = AV24Metodologia_Descricao2;
         AV95WWMetodologiaFasesDS_10_Tfmetodologiafases_nome = AV65TFMetodologiaFases_Nome;
         AV96WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel = AV66TFMetodologiaFases_Nome_Sel;
         AV97WWMetodologiaFasesDS_12_Tfmetodologiafases_percentual = AV69TFMetodologiaFases_Percentual;
         AV98WWMetodologiaFasesDS_13_Tfmetodologiafases_percentual_to = AV70TFMetodologiaFases_Percentual_To;
         AV99WWMetodologiaFasesDS_14_Tfmetodologiafases_prazo = AV81TFMetodologiaFases_Prazo;
         AV100WWMetodologiaFasesDS_15_Tfmetodologiafases_prazo_to = AV82TFMetodologiaFases_Prazo_To;
         AV101WWMetodologiaFasesDS_16_Tfmetodologia_descricao = AV73TFMetodologia_Descricao;
         AV102WWMetodologiaFasesDS_17_Tfmetodologia_descricao_sel = AV74TFMetodologia_Descricao_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV64MetodologiaFases_NomeTitleFilterData", AV64MetodologiaFases_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV68MetodologiaFases_PercentualTitleFilterData", AV68MetodologiaFases_PercentualTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV80MetodologiaFases_PrazoTitleFilterData", AV80MetodologiaFases_PrazoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV72Metodologia_DescricaoTitleFilterData", AV72Metodologia_DescricaoTitleFilterData);
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E117X2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV77PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV77PageToGo) ;
         }
      }

      protected void E127X2( )
      {
         /* Ddo_metodologiafases_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_metodologiafases_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_metodologiafases_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologiafases_nome_Internalname, "SortedStatus", Ddo_metodologiafases_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_metodologiafases_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_metodologiafases_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologiafases_nome_Internalname, "SortedStatus", Ddo_metodologiafases_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_metodologiafases_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV65TFMetodologiaFases_Nome = Ddo_metodologiafases_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFMetodologiaFases_Nome", AV65TFMetodologiaFases_Nome);
            AV66TFMetodologiaFases_Nome_Sel = Ddo_metodologiafases_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFMetodologiaFases_Nome_Sel", AV66TFMetodologiaFases_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E137X2( )
      {
         /* Ddo_metodologiafases_percentual_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_metodologiafases_percentual_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_metodologiafases_percentual_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologiafases_percentual_Internalname, "SortedStatus", Ddo_metodologiafases_percentual_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_metodologiafases_percentual_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_metodologiafases_percentual_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologiafases_percentual_Internalname, "SortedStatus", Ddo_metodologiafases_percentual_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_metodologiafases_percentual_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV69TFMetodologiaFases_Percentual = NumberUtil.Val( Ddo_metodologiafases_percentual_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFMetodologiaFases_Percentual", StringUtil.LTrim( StringUtil.Str( AV69TFMetodologiaFases_Percentual, 6, 2)));
            AV70TFMetodologiaFases_Percentual_To = NumberUtil.Val( Ddo_metodologiafases_percentual_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFMetodologiaFases_Percentual_To", StringUtil.LTrim( StringUtil.Str( AV70TFMetodologiaFases_Percentual_To, 6, 2)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E147X2( )
      {
         /* Ddo_metodologiafases_prazo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_metodologiafases_prazo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_metodologiafases_prazo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologiafases_prazo_Internalname, "SortedStatus", Ddo_metodologiafases_prazo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_metodologiafases_prazo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_metodologiafases_prazo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologiafases_prazo_Internalname, "SortedStatus", Ddo_metodologiafases_prazo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_metodologiafases_prazo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV81TFMetodologiaFases_Prazo = NumberUtil.Val( Ddo_metodologiafases_prazo_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFMetodologiaFases_Prazo", StringUtil.LTrim( StringUtil.Str( AV81TFMetodologiaFases_Prazo, 7, 2)));
            AV82TFMetodologiaFases_Prazo_To = NumberUtil.Val( Ddo_metodologiafases_prazo_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFMetodologiaFases_Prazo_To", StringUtil.LTrim( StringUtil.Str( AV82TFMetodologiaFases_Prazo_To, 7, 2)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E157X2( )
      {
         /* Ddo_metodologia_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_metodologia_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_metodologia_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologia_descricao_Internalname, "SortedStatus", Ddo_metodologia_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_metodologia_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_metodologia_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologia_descricao_Internalname, "SortedStatus", Ddo_metodologia_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_metodologia_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV73TFMetodologia_Descricao = Ddo_metodologia_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFMetodologia_Descricao", AV73TFMetodologia_Descricao);
            AV74TFMetodologia_Descricao_Sel = Ddo_metodologia_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFMetodologia_Descricao_Sel", AV74TFMetodologia_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E267X2( )
      {
         /* Grid_Load Routine */
         AV32Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV32Update);
         AV103Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("metodologiafases.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A147MetodologiaFases_Codigo) + "," + UrlEncode("" +A137Metodologia_Codigo);
         AV33Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV33Delete);
         AV104Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("metodologiafases.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A147MetodologiaFases_Codigo) + "," + UrlEncode("" +A137Metodologia_Codigo);
         AV34Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV34Display);
         AV105Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = formatLink("viewmetodologiafases.aspx") + "?" + UrlEncode("" +A147MetodologiaFases_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         edtMetodologiaFases_Nome_Link = formatLink("viewmetodologiafases.aspx") + "?" + UrlEncode("" +A147MetodologiaFases_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         edtMetodologia_Descricao_Link = formatLink("viewmetodologia.aspx") + "?" + UrlEncode("" +A137Metodologia_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 73;
         }
         sendrow_732( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_73_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(73, GridRow);
         }
      }

      protected void E167X2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E217X2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV20DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18MetodologiaFases_Nome1, AV19Metodologia_Descricao1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23MetodologiaFases_Nome2, AV24Metodologia_Descricao2, AV20DynamicFiltersEnabled2, AV65TFMetodologiaFases_Nome, AV66TFMetodologiaFases_Nome_Sel, AV69TFMetodologiaFases_Percentual, AV70TFMetodologiaFases_Percentual_To, AV81TFMetodologiaFases_Prazo, AV82TFMetodologiaFases_Prazo_To, AV73TFMetodologia_Descricao, AV74TFMetodologia_Descricao_Sel, AV67ddo_MetodologiaFases_NomeTitleControlIdToReplace, AV71ddo_MetodologiaFases_PercentualTitleControlIdToReplace, AV83ddo_MetodologiaFases_PrazoTitleControlIdToReplace, AV75ddo_Metodologia_DescricaoTitleControlIdToReplace, AV106Pgmname, AV10GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A147MetodologiaFases_Codigo, A137Metodologia_Codigo) ;
      }

      protected void E177X2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV30DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV31DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31DynamicFiltersIgnoreFirst", AV31DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV30DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV31DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31DynamicFiltersIgnoreFirst", AV31DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18MetodologiaFases_Nome1, AV19Metodologia_Descricao1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23MetodologiaFases_Nome2, AV24Metodologia_Descricao2, AV20DynamicFiltersEnabled2, AV65TFMetodologiaFases_Nome, AV66TFMetodologiaFases_Nome_Sel, AV69TFMetodologiaFases_Percentual, AV70TFMetodologiaFases_Percentual_To, AV81TFMetodologiaFases_Prazo, AV82TFMetodologiaFases_Prazo_To, AV73TFMetodologia_Descricao, AV74TFMetodologia_Descricao_Sel, AV67ddo_MetodologiaFases_NomeTitleControlIdToReplace, AV71ddo_MetodologiaFases_PercentualTitleControlIdToReplace, AV83ddo_MetodologiaFases_PrazoTitleControlIdToReplace, AV75ddo_Metodologia_DescricaoTitleControlIdToReplace, AV106Pgmname, AV10GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A147MetodologiaFases_Codigo, A137Metodologia_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E227X2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         AV17DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E187X2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV30DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV30DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18MetodologiaFases_Nome1, AV19Metodologia_Descricao1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23MetodologiaFases_Nome2, AV24Metodologia_Descricao2, AV20DynamicFiltersEnabled2, AV65TFMetodologiaFases_Nome, AV66TFMetodologiaFases_Nome_Sel, AV69TFMetodologiaFases_Percentual, AV70TFMetodologiaFases_Percentual_To, AV81TFMetodologiaFases_Prazo, AV82TFMetodologiaFases_Prazo_To, AV73TFMetodologia_Descricao, AV74TFMetodologia_Descricao_Sel, AV67ddo_MetodologiaFases_NomeTitleControlIdToReplace, AV71ddo_MetodologiaFases_PercentualTitleControlIdToReplace, AV83ddo_MetodologiaFases_PrazoTitleControlIdToReplace, AV75ddo_Metodologia_DescricaoTitleControlIdToReplace, AV106Pgmname, AV10GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A147MetodologiaFases_Codigo, A137Metodologia_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E237X2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         AV22DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E197X2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E207X2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("metodologiafases.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_metodologiafases_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologiafases_nome_Internalname, "SortedStatus", Ddo_metodologiafases_nome_Sortedstatus);
         Ddo_metodologiafases_percentual_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologiafases_percentual_Internalname, "SortedStatus", Ddo_metodologiafases_percentual_Sortedstatus);
         Ddo_metodologiafases_prazo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologiafases_prazo_Internalname, "SortedStatus", Ddo_metodologiafases_prazo_Sortedstatus);
         Ddo_metodologia_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologia_descricao_Internalname, "SortedStatus", Ddo_metodologia_descricao_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_metodologiafases_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologiafases_nome_Internalname, "SortedStatus", Ddo_metodologiafases_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_metodologiafases_percentual_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologiafases_percentual_Internalname, "SortedStatus", Ddo_metodologiafases_percentual_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_metodologiafases_prazo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologiafases_prazo_Internalname, "SortedStatus", Ddo_metodologiafases_prazo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_metodologia_descricao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologia_descricao_Internalname, "SortedStatus", Ddo_metodologia_descricao_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavMetodologiafases_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMetodologiafases_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMetodologiafases_nome1_Visible), 5, 0)));
         edtavMetodologia_descricao1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMetodologia_descricao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMetodologia_descricao1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "METODOLOGIAFASES_NOME") == 0 )
         {
            edtavMetodologiafases_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMetodologiafases_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMetodologiafases_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "METODOLOGIA_DESCRICAO") == 0 )
         {
            edtavMetodologia_descricao1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMetodologia_descricao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMetodologia_descricao1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavMetodologiafases_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMetodologiafases_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMetodologiafases_nome2_Visible), 5, 0)));
         edtavMetodologia_descricao2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMetodologia_descricao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMetodologia_descricao2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "METODOLOGIAFASES_NOME") == 0 )
         {
            edtavMetodologiafases_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMetodologiafases_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMetodologiafases_nome2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "METODOLOGIA_DESCRICAO") == 0 )
         {
            edtavMetodologia_descricao2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMetodologia_descricao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMetodologia_descricao2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         AV21DynamicFiltersSelector2 = "METODOLOGIAFASES_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         AV22DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         AV23MetodologiaFases_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23MetodologiaFases_Nome2", AV23MetodologiaFases_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S212( )
      {
         /* 'CLEANFILTERS' Routine */
         AV65TFMetodologiaFases_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFMetodologiaFases_Nome", AV65TFMetodologiaFases_Nome);
         Ddo_metodologiafases_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologiafases_nome_Internalname, "FilteredText_set", Ddo_metodologiafases_nome_Filteredtext_set);
         AV66TFMetodologiaFases_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFMetodologiaFases_Nome_Sel", AV66TFMetodologiaFases_Nome_Sel);
         Ddo_metodologiafases_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologiafases_nome_Internalname, "SelectedValue_set", Ddo_metodologiafases_nome_Selectedvalue_set);
         AV69TFMetodologiaFases_Percentual = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFMetodologiaFases_Percentual", StringUtil.LTrim( StringUtil.Str( AV69TFMetodologiaFases_Percentual, 6, 2)));
         Ddo_metodologiafases_percentual_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologiafases_percentual_Internalname, "FilteredText_set", Ddo_metodologiafases_percentual_Filteredtext_set);
         AV70TFMetodologiaFases_Percentual_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFMetodologiaFases_Percentual_To", StringUtil.LTrim( StringUtil.Str( AV70TFMetodologiaFases_Percentual_To, 6, 2)));
         Ddo_metodologiafases_percentual_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologiafases_percentual_Internalname, "FilteredTextTo_set", Ddo_metodologiafases_percentual_Filteredtextto_set);
         AV81TFMetodologiaFases_Prazo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFMetodologiaFases_Prazo", StringUtil.LTrim( StringUtil.Str( AV81TFMetodologiaFases_Prazo, 7, 2)));
         Ddo_metodologiafases_prazo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologiafases_prazo_Internalname, "FilteredText_set", Ddo_metodologiafases_prazo_Filteredtext_set);
         AV82TFMetodologiaFases_Prazo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFMetodologiaFases_Prazo_To", StringUtil.LTrim( StringUtil.Str( AV82TFMetodologiaFases_Prazo_To, 7, 2)));
         Ddo_metodologiafases_prazo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologiafases_prazo_Internalname, "FilteredTextTo_set", Ddo_metodologiafases_prazo_Filteredtextto_set);
         AV73TFMetodologia_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFMetodologia_Descricao", AV73TFMetodologia_Descricao);
         Ddo_metodologia_descricao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologia_descricao_Internalname, "FilteredText_set", Ddo_metodologia_descricao_Filteredtext_set);
         AV74TFMetodologia_Descricao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFMetodologia_Descricao_Sel", AV74TFMetodologia_Descricao_Sel);
         Ddo_metodologia_descricao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologia_descricao_Internalname, "SelectedValue_set", Ddo_metodologia_descricao_Selectedvalue_set);
         AV16DynamicFiltersSelector1 = "METODOLOGIAFASES_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         AV17DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         AV18MetodologiaFases_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18MetodologiaFases_Nome1", AV18MetodologiaFases_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV55Session.Get(AV106Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV106Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV55Session.Get(AV106Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S222( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV107GXV1 = 1;
         while ( AV107GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV107GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFMETODOLOGIAFASES_NOME") == 0 )
            {
               AV65TFMetodologiaFases_Nome = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFMetodologiaFases_Nome", AV65TFMetodologiaFases_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65TFMetodologiaFases_Nome)) )
               {
                  Ddo_metodologiafases_nome_Filteredtext_set = AV65TFMetodologiaFases_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologiafases_nome_Internalname, "FilteredText_set", Ddo_metodologiafases_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFMETODOLOGIAFASES_NOME_SEL") == 0 )
            {
               AV66TFMetodologiaFases_Nome_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFMetodologiaFases_Nome_Sel", AV66TFMetodologiaFases_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66TFMetodologiaFases_Nome_Sel)) )
               {
                  Ddo_metodologiafases_nome_Selectedvalue_set = AV66TFMetodologiaFases_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologiafases_nome_Internalname, "SelectedValue_set", Ddo_metodologiafases_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFMETODOLOGIAFASES_PERCENTUAL") == 0 )
            {
               AV69TFMetodologiaFases_Percentual = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFMetodologiaFases_Percentual", StringUtil.LTrim( StringUtil.Str( AV69TFMetodologiaFases_Percentual, 6, 2)));
               AV70TFMetodologiaFases_Percentual_To = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFMetodologiaFases_Percentual_To", StringUtil.LTrim( StringUtil.Str( AV70TFMetodologiaFases_Percentual_To, 6, 2)));
               if ( ! (Convert.ToDecimal(0)==AV69TFMetodologiaFases_Percentual) )
               {
                  Ddo_metodologiafases_percentual_Filteredtext_set = StringUtil.Str( AV69TFMetodologiaFases_Percentual, 6, 2);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologiafases_percentual_Internalname, "FilteredText_set", Ddo_metodologiafases_percentual_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV70TFMetodologiaFases_Percentual_To) )
               {
                  Ddo_metodologiafases_percentual_Filteredtextto_set = StringUtil.Str( AV70TFMetodologiaFases_Percentual_To, 6, 2);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologiafases_percentual_Internalname, "FilteredTextTo_set", Ddo_metodologiafases_percentual_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFMETODOLOGIAFASES_PRAZO") == 0 )
            {
               AV81TFMetodologiaFases_Prazo = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFMetodologiaFases_Prazo", StringUtil.LTrim( StringUtil.Str( AV81TFMetodologiaFases_Prazo, 7, 2)));
               AV82TFMetodologiaFases_Prazo_To = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFMetodologiaFases_Prazo_To", StringUtil.LTrim( StringUtil.Str( AV82TFMetodologiaFases_Prazo_To, 7, 2)));
               if ( ! (Convert.ToDecimal(0)==AV81TFMetodologiaFases_Prazo) )
               {
                  Ddo_metodologiafases_prazo_Filteredtext_set = StringUtil.Str( AV81TFMetodologiaFases_Prazo, 7, 2);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologiafases_prazo_Internalname, "FilteredText_set", Ddo_metodologiafases_prazo_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV82TFMetodologiaFases_Prazo_To) )
               {
                  Ddo_metodologiafases_prazo_Filteredtextto_set = StringUtil.Str( AV82TFMetodologiaFases_Prazo_To, 7, 2);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologiafases_prazo_Internalname, "FilteredTextTo_set", Ddo_metodologiafases_prazo_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFMETODOLOGIA_DESCRICAO") == 0 )
            {
               AV73TFMetodologia_Descricao = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFMetodologia_Descricao", AV73TFMetodologia_Descricao);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73TFMetodologia_Descricao)) )
               {
                  Ddo_metodologia_descricao_Filteredtext_set = AV73TFMetodologia_Descricao;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologia_descricao_Internalname, "FilteredText_set", Ddo_metodologia_descricao_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFMETODOLOGIA_DESCRICAO_SEL") == 0 )
            {
               AV74TFMetodologia_Descricao_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFMetodologia_Descricao_Sel", AV74TFMetodologia_Descricao_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74TFMetodologia_Descricao_Sel)) )
               {
                  Ddo_metodologia_descricao_Selectedvalue_set = AV74TFMetodologia_Descricao_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologia_descricao_Internalname, "SelectedValue_set", Ddo_metodologia_descricao_Selectedvalue_set);
               }
            }
            AV107GXV1 = (int)(AV107GXV1+1);
         }
      }

      protected void S202( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV16DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "METODOLOGIAFASES_NOME") == 0 )
            {
               AV17DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV18MetodologiaFases_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18MetodologiaFases_Nome1", AV18MetodologiaFases_Nome1);
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "METODOLOGIA_DESCRICAO") == 0 )
            {
               AV17DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV19Metodologia_Descricao1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Metodologia_Descricao1", AV19Metodologia_Descricao1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV20DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV21DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "METODOLOGIAFASES_NOME") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV23MetodologiaFases_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23MetodologiaFases_Nome2", AV23MetodologiaFases_Nome2);
               }
               else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "METODOLOGIA_DESCRICAO") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV24Metodologia_Descricao2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Metodologia_Descricao2", AV24Metodologia_Descricao2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV30DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV55Session.Get(AV106Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65TFMetodologiaFases_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFMETODOLOGIAFASES_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV65TFMetodologiaFases_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66TFMetodologiaFases_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFMETODOLOGIAFASES_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV66TFMetodologiaFases_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV69TFMetodologiaFases_Percentual) && (Convert.ToDecimal(0)==AV70TFMetodologiaFases_Percentual_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFMETODOLOGIAFASES_PERCENTUAL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV69TFMetodologiaFases_Percentual, 6, 2);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV70TFMetodologiaFases_Percentual_To, 6, 2);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV81TFMetodologiaFases_Prazo) && (Convert.ToDecimal(0)==AV82TFMetodologiaFases_Prazo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFMETODOLOGIAFASES_PRAZO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV81TFMetodologiaFases_Prazo, 7, 2);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV82TFMetodologiaFases_Prazo_To, 7, 2);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73TFMetodologia_Descricao)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFMETODOLOGIA_DESCRICAO";
            AV11GridStateFilterValue.gxTpr_Value = AV73TFMetodologia_Descricao;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74TFMetodologia_Descricao_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFMETODOLOGIA_DESCRICAO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV74TFMetodologia_Descricao_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV106Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV31DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV16DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "METODOLOGIAFASES_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV18MetodologiaFases_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV18MetodologiaFases_Nome1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV17DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "METODOLOGIA_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Metodologia_Descricao1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV19Metodologia_Descricao1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV17DynamicFiltersOperator1;
            }
            if ( AV30DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV20DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV21DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "METODOLOGIAFASES_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV23MetodologiaFases_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV23MetodologiaFases_Nome2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "METODOLOGIA_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Metodologia_Descricao2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV24Metodologia_Descricao2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
            }
            if ( AV30DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S132( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV106Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "MetodologiaFases";
         AV55Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_7X2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_7X2( true) ;
         }
         else
         {
            wb_table2_8_7X2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_7X2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_67_7X2( true) ;
         }
         else
         {
            wb_table3_67_7X2( false) ;
         }
         return  ;
      }

      protected void wb_table3_67_7X2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_7X2e( true) ;
         }
         else
         {
            wb_table1_2_7X2e( false) ;
         }
      }

      protected void wb_table3_67_7X2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "TableGridHeader", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_70_7X2( true) ;
         }
         else
         {
            wb_table4_70_7X2( false) ;
         }
         return  ;
      }

      protected void wb_table4_70_7X2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_67_7X2e( true) ;
         }
         else
         {
            wb_table3_67_7X2e( false) ;
         }
      }

      protected void wb_table4_70_7X2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"73\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtMetodologiaFases_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtMetodologiaFases_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtMetodologiaFases_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtMetodologiaFases_Percentual_Titleformat == 0 )
               {
                  context.SendWebValue( edtMetodologiaFases_Percentual_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtMetodologiaFases_Percentual_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtMetodologiaFases_Prazo_Titleformat == 0 )
               {
                  context.SendWebValue( edtMetodologiaFases_Prazo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtMetodologiaFases_Prazo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Metodolog�a") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtMetodologia_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtMetodologia_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtMetodologia_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV32Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV33Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV34Display));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A147MetodologiaFases_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A148MetodologiaFases_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtMetodologiaFases_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtMetodologiaFases_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtMetodologiaFases_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A149MetodologiaFases_Percentual, 6, 2, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtMetodologiaFases_Percentual_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtMetodologiaFases_Percentual_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A2135MetodologiaFases_Prazo, 7, 2, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtMetodologiaFases_Prazo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtMetodologiaFases_Prazo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A137Metodologia_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A138Metodologia_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtMetodologia_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtMetodologia_Descricao_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtMetodologia_Descricao_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 73 )
         {
            wbEnd = 0;
            nRC_GXsfl_73 = (short)(nGXsfl_73_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_70_7X2e( true) ;
         }
         else
         {
            wb_table4_70_7X2e( false) ;
         }
      }

      protected void wb_table2_8_7X2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblMetodologiafasestitle_Internalname, "Fases", "", "", lblMetodologiafasestitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWMetodologiaFases.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table5_13_7X2( true) ;
         }
         else
         {
            wb_table5_13_7X2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_7X2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWMetodologiaFases.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_73_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWMetodologiaFases.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWMetodologiaFases.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_23_7X2( true) ;
         }
         else
         {
            wb_table6_23_7X2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_7X2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_7X2e( true) ;
         }
         else
         {
            wb_table2_8_7X2e( false) ;
         }
      }

      protected void wb_table6_23_7X2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWMetodologiaFases.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_7X2( true) ;
         }
         else
         {
            wb_table7_28_7X2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_7X2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWMetodologiaFases.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_7X2e( true) ;
         }
         else
         {
            wb_table6_23_7X2e( false) ;
         }
      }

      protected void wb_table7_28_7X2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWMetodologiaFases.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_73_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV16DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WWMetodologiaFases.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWMetodologiaFases.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_37_7X2( true) ;
         }
         else
         {
            wb_table8_37_7X2( false) ;
         }
         return  ;
      }

      protected void wb_table8_37_7X2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWMetodologiaFases.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWMetodologiaFases.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWMetodologiaFases.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'" + sGXsfl_73_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV21DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,51);\"", "", true, "HLP_WWMetodologiaFases.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWMetodologiaFases.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_55_7X2( true) ;
         }
         else
         {
            wb_table9_55_7X2( false) ;
         }
         return  ;
      }

      protected void wb_table9_55_7X2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWMetodologiaFases.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_7X2e( true) ;
         }
         else
         {
            wb_table7_28_7X2e( false) ;
         }
      }

      protected void wb_table9_55_7X2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'" + sGXsfl_73_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,58);\"", "", true, "HLP_WWMetodologiaFases.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavMetodologiafases_nome2_Internalname, StringUtil.RTrim( AV23MetodologiaFases_Nome2), StringUtil.RTrim( context.localUtil.Format( AV23MetodologiaFases_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,60);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavMetodologiafases_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavMetodologiafases_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWMetodologiaFases.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavMetodologia_descricao2_Internalname, AV24Metodologia_Descricao2, StringUtil.RTrim( context.localUtil.Format( AV24Metodologia_Descricao2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,61);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavMetodologia_descricao2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavMetodologia_descricao2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWMetodologiaFases.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_55_7X2e( true) ;
         }
         else
         {
            wb_table9_55_7X2e( false) ;
         }
      }

      protected void wb_table8_37_7X2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_73_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", "", true, "HLP_WWMetodologiaFases.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavMetodologiafases_nome1_Internalname, StringUtil.RTrim( AV18MetodologiaFases_Nome1), StringUtil.RTrim( context.localUtil.Format( AV18MetodologiaFases_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,42);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavMetodologiafases_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavMetodologiafases_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWMetodologiaFases.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavMetodologia_descricao1_Internalname, AV19Metodologia_Descricao1, StringUtil.RTrim( context.localUtil.Format( AV19Metodologia_Descricao1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,43);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavMetodologia_descricao1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavMetodologia_descricao1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWMetodologiaFases.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_37_7X2e( true) ;
         }
         else
         {
            wb_table8_37_7X2e( false) ;
         }
      }

      protected void wb_table5_13_7X2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWMetodologiaFases.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_7X2e( true) ;
         }
         else
         {
            wb_table5_13_7X2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA7X2( ) ;
         WS7X2( ) ;
         WE7X2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205181315254");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwmetodologiafases.js", "?20205181315254");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_732( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_73_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_73_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_73_idx;
         edtMetodologiaFases_Codigo_Internalname = "METODOLOGIAFASES_CODIGO_"+sGXsfl_73_idx;
         edtMetodologiaFases_Nome_Internalname = "METODOLOGIAFASES_NOME_"+sGXsfl_73_idx;
         edtMetodologiaFases_Percentual_Internalname = "METODOLOGIAFASES_PERCENTUAL_"+sGXsfl_73_idx;
         edtMetodologiaFases_Prazo_Internalname = "METODOLOGIAFASES_PRAZO_"+sGXsfl_73_idx;
         edtMetodologia_Codigo_Internalname = "METODOLOGIA_CODIGO_"+sGXsfl_73_idx;
         edtMetodologia_Descricao_Internalname = "METODOLOGIA_DESCRICAO_"+sGXsfl_73_idx;
      }

      protected void SubsflControlProps_fel_732( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_73_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_73_fel_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_73_fel_idx;
         edtMetodologiaFases_Codigo_Internalname = "METODOLOGIAFASES_CODIGO_"+sGXsfl_73_fel_idx;
         edtMetodologiaFases_Nome_Internalname = "METODOLOGIAFASES_NOME_"+sGXsfl_73_fel_idx;
         edtMetodologiaFases_Percentual_Internalname = "METODOLOGIAFASES_PERCENTUAL_"+sGXsfl_73_fel_idx;
         edtMetodologiaFases_Prazo_Internalname = "METODOLOGIAFASES_PRAZO_"+sGXsfl_73_fel_idx;
         edtMetodologia_Codigo_Internalname = "METODOLOGIA_CODIGO_"+sGXsfl_73_fel_idx;
         edtMetodologia_Descricao_Internalname = "METODOLOGIA_DESCRICAO_"+sGXsfl_73_fel_idx;
      }

      protected void sendrow_732( )
      {
         SubsflControlProps_732( ) ;
         WB7X0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_73_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_73_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_73_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV32Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV32Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV103Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV32Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV32Update)) ? AV103Update_GXI : context.PathToRelativeUrl( AV32Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV32Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV33Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV33Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV104Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV33Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV33Delete)) ? AV104Delete_GXI : context.PathToRelativeUrl( AV33Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV33Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV34Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV34Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV105Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV34Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV34Display)) ? AV105Display_GXI : context.PathToRelativeUrl( AV34Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV34Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtMetodologiaFases_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A147MetodologiaFases_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A147MetodologiaFases_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtMetodologiaFases_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)73,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtMetodologiaFases_Nome_Internalname,StringUtil.RTrim( A148MetodologiaFases_Nome),StringUtil.RTrim( context.localUtil.Format( A148MetodologiaFases_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtMetodologiaFases_Nome_Link,(String)"",(String)"",(String)"",(String)edtMetodologiaFases_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)73,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtMetodologiaFases_Percentual_Internalname,StringUtil.LTrim( StringUtil.NToC( A149MetodologiaFases_Percentual, 6, 2, ",", "")),context.localUtil.Format( A149MetodologiaFases_Percentual, "ZZ9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtMetodologiaFases_Percentual_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)73,(short)1,(short)-1,(short)0,(bool)true,(String)"Percentual",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtMetodologiaFases_Prazo_Internalname,StringUtil.LTrim( StringUtil.NToC( A2135MetodologiaFases_Prazo, 7, 2, ",", "")),context.localUtil.Format( A2135MetodologiaFases_Prazo, "ZZZ9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtMetodologiaFases_Prazo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)7,(short)0,(short)0,(short)73,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtMetodologia_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A137Metodologia_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A137Metodologia_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtMetodologia_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)73,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtMetodologia_Descricao_Internalname,(String)A138Metodologia_Descricao,StringUtil.RTrim( context.localUtil.Format( A138Metodologia_Descricao, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtMetodologia_Descricao_Link,(String)"",(String)"",(String)"",(String)edtMetodologia_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)73,(short)1,(short)-1,(short)-1,(bool)true,(String)"Descricao",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, "gxhash_METODOLOGIAFASES_CODIGO"+"_"+sGXsfl_73_idx, GetSecureSignedToken( sGXsfl_73_idx, context.localUtil.Format( (decimal)(A147MetodologiaFases_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_METODOLOGIAFASES_NOME"+"_"+sGXsfl_73_idx, GetSecureSignedToken( sGXsfl_73_idx, StringUtil.RTrim( context.localUtil.Format( A148MetodologiaFases_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_METODOLOGIAFASES_PERCENTUAL"+"_"+sGXsfl_73_idx, GetSecureSignedToken( sGXsfl_73_idx, context.localUtil.Format( A149MetodologiaFases_Percentual, "ZZ9.99")));
            GxWebStd.gx_hidden_field( context, "gxhash_METODOLOGIAFASES_PRAZO"+"_"+sGXsfl_73_idx, GetSecureSignedToken( sGXsfl_73_idx, context.localUtil.Format( A2135MetodologiaFases_Prazo, "ZZZ9.99")));
            GridContainer.AddRow(GridRow);
            nGXsfl_73_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_73_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_73_idx+1));
            sGXsfl_73_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_73_idx), 4, 0)), 4, "0");
            SubsflControlProps_732( ) ;
         }
         /* End function sendrow_732 */
      }

      protected void init_default_properties( )
      {
         lblMetodologiafasestitle_Internalname = "METODOLOGIAFASESTITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavMetodologiafases_nome1_Internalname = "vMETODOLOGIAFASES_NOME1";
         edtavMetodologia_descricao1_Internalname = "vMETODOLOGIA_DESCRICAO1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavMetodologiafases_nome2_Internalname = "vMETODOLOGIAFASES_NOME2";
         edtavMetodologia_descricao2_Internalname = "vMETODOLOGIA_DESCRICAO2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtavDisplay_Internalname = "vDISPLAY";
         edtMetodologiaFases_Codigo_Internalname = "METODOLOGIAFASES_CODIGO";
         edtMetodologiaFases_Nome_Internalname = "METODOLOGIAFASES_NOME";
         edtMetodologiaFases_Percentual_Internalname = "METODOLOGIAFASES_PERCENTUAL";
         edtMetodologiaFases_Prazo_Internalname = "METODOLOGIAFASES_PRAZO";
         edtMetodologia_Codigo_Internalname = "METODOLOGIA_CODIGO";
         edtMetodologia_Descricao_Internalname = "METODOLOGIA_DESCRICAO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         edtavTfmetodologiafases_nome_Internalname = "vTFMETODOLOGIAFASES_NOME";
         edtavTfmetodologiafases_nome_sel_Internalname = "vTFMETODOLOGIAFASES_NOME_SEL";
         edtavTfmetodologiafases_percentual_Internalname = "vTFMETODOLOGIAFASES_PERCENTUAL";
         edtavTfmetodologiafases_percentual_to_Internalname = "vTFMETODOLOGIAFASES_PERCENTUAL_TO";
         edtavTfmetodologiafases_prazo_Internalname = "vTFMETODOLOGIAFASES_PRAZO";
         edtavTfmetodologiafases_prazo_to_Internalname = "vTFMETODOLOGIAFASES_PRAZO_TO";
         edtavTfmetodologia_descricao_Internalname = "vTFMETODOLOGIA_DESCRICAO";
         edtavTfmetodologia_descricao_sel_Internalname = "vTFMETODOLOGIA_DESCRICAO_SEL";
         Ddo_metodologiafases_nome_Internalname = "DDO_METODOLOGIAFASES_NOME";
         edtavDdo_metodologiafases_nometitlecontrolidtoreplace_Internalname = "vDDO_METODOLOGIAFASES_NOMETITLECONTROLIDTOREPLACE";
         Ddo_metodologiafases_percentual_Internalname = "DDO_METODOLOGIAFASES_PERCENTUAL";
         edtavDdo_metodologiafases_percentualtitlecontrolidtoreplace_Internalname = "vDDO_METODOLOGIAFASES_PERCENTUALTITLECONTROLIDTOREPLACE";
         Ddo_metodologiafases_prazo_Internalname = "DDO_METODOLOGIAFASES_PRAZO";
         edtavDdo_metodologiafases_prazotitlecontrolidtoreplace_Internalname = "vDDO_METODOLOGIAFASES_PRAZOTITLECONTROLIDTOREPLACE";
         Ddo_metodologia_descricao_Internalname = "DDO_METODOLOGIA_DESCRICAO";
         edtavDdo_metodologia_descricaotitlecontrolidtoreplace_Internalname = "vDDO_METODOLOGIA_DESCRICAOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtMetodologia_Descricao_Jsonclick = "";
         edtMetodologia_Codigo_Jsonclick = "";
         edtMetodologiaFases_Prazo_Jsonclick = "";
         edtMetodologiaFases_Percentual_Jsonclick = "";
         edtMetodologiaFases_Nome_Jsonclick = "";
         edtMetodologiaFases_Codigo_Jsonclick = "";
         edtavMetodologia_descricao1_Jsonclick = "";
         edtavMetodologiafases_nome1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavMetodologia_descricao2_Jsonclick = "";
         edtavMetodologiafases_nome2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtMetodologia_Descricao_Link = "";
         edtMetodologiaFases_Nome_Link = "";
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtMetodologia_Descricao_Titleformat = 0;
         edtMetodologiaFases_Prazo_Titleformat = 0;
         edtMetodologiaFases_Percentual_Titleformat = 0;
         edtMetodologiaFases_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavMetodologia_descricao2_Visible = 1;
         edtavMetodologiafases_nome2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavMetodologia_descricao1_Visible = 1;
         edtavMetodologiafases_nome1_Visible = 1;
         edtMetodologia_Descricao_Title = "Metodologia";
         edtMetodologiaFases_Prazo_Title = "Prazo";
         edtMetodologiaFases_Percentual_Title = "Percentual";
         edtMetodologiaFases_Nome_Title = "Fase";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_metodologia_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_metodologiafases_prazotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_metodologiafases_percentualtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_metodologiafases_nometitlecontrolidtoreplace_Visible = 1;
         edtavTfmetodologia_descricao_sel_Jsonclick = "";
         edtavTfmetodologia_descricao_sel_Visible = 1;
         edtavTfmetodologia_descricao_Jsonclick = "";
         edtavTfmetodologia_descricao_Visible = 1;
         edtavTfmetodologiafases_prazo_to_Jsonclick = "";
         edtavTfmetodologiafases_prazo_to_Visible = 1;
         edtavTfmetodologiafases_prazo_Jsonclick = "";
         edtavTfmetodologiafases_prazo_Visible = 1;
         edtavTfmetodologiafases_percentual_to_Jsonclick = "";
         edtavTfmetodologiafases_percentual_to_Visible = 1;
         edtavTfmetodologiafases_percentual_Jsonclick = "";
         edtavTfmetodologiafases_percentual_Visible = 1;
         edtavTfmetodologiafases_nome_sel_Jsonclick = "";
         edtavTfmetodologiafases_nome_sel_Visible = 1;
         edtavTfmetodologiafases_nome_Jsonclick = "";
         edtavTfmetodologiafases_nome_Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_metodologia_descricao_Searchbuttontext = "Pesquisar";
         Ddo_metodologia_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_metodologia_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_metodologia_descricao_Loadingdata = "Carregando dados...";
         Ddo_metodologia_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_metodologia_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_metodologia_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_metodologia_descricao_Datalistproc = "GetWWMetodologiaFasesFilterData";
         Ddo_metodologia_descricao_Datalisttype = "Dynamic";
         Ddo_metodologia_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_metodologia_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_metodologia_descricao_Filtertype = "Character";
         Ddo_metodologia_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_metodologia_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_metodologia_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_metodologia_descricao_Titlecontrolidtoreplace = "";
         Ddo_metodologia_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_metodologia_descricao_Cls = "ColumnSettings";
         Ddo_metodologia_descricao_Tooltip = "Op��es";
         Ddo_metodologia_descricao_Caption = "";
         Ddo_metodologiafases_prazo_Searchbuttontext = "Pesquisar";
         Ddo_metodologiafases_prazo_Rangefilterto = "At�";
         Ddo_metodologiafases_prazo_Rangefilterfrom = "Desde";
         Ddo_metodologiafases_prazo_Cleanfilter = "Limpar pesquisa";
         Ddo_metodologiafases_prazo_Sortdsc = "Ordenar de Z � A";
         Ddo_metodologiafases_prazo_Sortasc = "Ordenar de A � Z";
         Ddo_metodologiafases_prazo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_metodologiafases_prazo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_metodologiafases_prazo_Filtertype = "Numeric";
         Ddo_metodologiafases_prazo_Includefilter = Convert.ToBoolean( -1);
         Ddo_metodologiafases_prazo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_metodologiafases_prazo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_metodologiafases_prazo_Titlecontrolidtoreplace = "";
         Ddo_metodologiafases_prazo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_metodologiafases_prazo_Cls = "ColumnSettings";
         Ddo_metodologiafases_prazo_Tooltip = "Op��es";
         Ddo_metodologiafases_prazo_Caption = "";
         Ddo_metodologiafases_percentual_Searchbuttontext = "Pesquisar";
         Ddo_metodologiafases_percentual_Rangefilterto = "At�";
         Ddo_metodologiafases_percentual_Rangefilterfrom = "Desde";
         Ddo_metodologiafases_percentual_Cleanfilter = "Limpar pesquisa";
         Ddo_metodologiafases_percentual_Sortdsc = "Ordenar de Z � A";
         Ddo_metodologiafases_percentual_Sortasc = "Ordenar de A � Z";
         Ddo_metodologiafases_percentual_Includedatalist = Convert.ToBoolean( 0);
         Ddo_metodologiafases_percentual_Filterisrange = Convert.ToBoolean( -1);
         Ddo_metodologiafases_percentual_Filtertype = "Numeric";
         Ddo_metodologiafases_percentual_Includefilter = Convert.ToBoolean( -1);
         Ddo_metodologiafases_percentual_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_metodologiafases_percentual_Includesortasc = Convert.ToBoolean( -1);
         Ddo_metodologiafases_percentual_Titlecontrolidtoreplace = "";
         Ddo_metodologiafases_percentual_Dropdownoptionstype = "GridTitleSettings";
         Ddo_metodologiafases_percentual_Cls = "ColumnSettings";
         Ddo_metodologiafases_percentual_Tooltip = "Op��es";
         Ddo_metodologiafases_percentual_Caption = "";
         Ddo_metodologiafases_nome_Searchbuttontext = "Pesquisar";
         Ddo_metodologiafases_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_metodologiafases_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_metodologiafases_nome_Loadingdata = "Carregando dados...";
         Ddo_metodologiafases_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_metodologiafases_nome_Sortasc = "Ordenar de A � Z";
         Ddo_metodologiafases_nome_Datalistupdateminimumcharacters = 0;
         Ddo_metodologiafases_nome_Datalistproc = "GetWWMetodologiaFasesFilterData";
         Ddo_metodologiafases_nome_Datalisttype = "Dynamic";
         Ddo_metodologiafases_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_metodologiafases_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_metodologiafases_nome_Filtertype = "Character";
         Ddo_metodologiafases_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_metodologiafases_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_metodologiafases_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_metodologiafases_nome_Titlecontrolidtoreplace = "";
         Ddo_metodologiafases_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_metodologiafases_nome_Cls = "ColumnSettings";
         Ddo_metodologiafases_nome_Tooltip = "Op��es";
         Ddo_metodologiafases_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Metodologia Fases";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A147MetodologiaFases_Codigo',fld:'METODOLOGIAFASES_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A137Metodologia_Codigo',fld:'METODOLOGIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV67ddo_MetodologiaFases_NomeTitleControlIdToReplace',fld:'vDDO_METODOLOGIAFASES_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_MetodologiaFases_PercentualTitleControlIdToReplace',fld:'vDDO_METODOLOGIAFASES_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_MetodologiaFases_PrazoTitleControlIdToReplace',fld:'vDDO_METODOLOGIAFASES_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Metodologia_DescricaoTitleControlIdToReplace',fld:'vDDO_METODOLOGIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18MetodologiaFases_Nome1',fld:'vMETODOLOGIAFASES_NOME1',pic:'@!',nv:''},{av:'AV19Metodologia_Descricao1',fld:'vMETODOLOGIA_DESCRICAO1',pic:'@!',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23MetodologiaFases_Nome2',fld:'vMETODOLOGIAFASES_NOME2',pic:'@!',nv:''},{av:'AV24Metodologia_Descricao2',fld:'vMETODOLOGIA_DESCRICAO2',pic:'@!',nv:''},{av:'AV65TFMetodologiaFases_Nome',fld:'vTFMETODOLOGIAFASES_NOME',pic:'@!',nv:''},{av:'AV66TFMetodologiaFases_Nome_Sel',fld:'vTFMETODOLOGIAFASES_NOME_SEL',pic:'@!',nv:''},{av:'AV69TFMetodologiaFases_Percentual',fld:'vTFMETODOLOGIAFASES_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV70TFMetodologiaFases_Percentual_To',fld:'vTFMETODOLOGIAFASES_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV81TFMetodologiaFases_Prazo',fld:'vTFMETODOLOGIAFASES_PRAZO',pic:'ZZZ9.99',nv:0.0},{av:'AV82TFMetodologiaFases_Prazo_To',fld:'vTFMETODOLOGIAFASES_PRAZO_TO',pic:'ZZZ9.99',nv:0.0},{av:'AV73TFMetodologia_Descricao',fld:'vTFMETODOLOGIA_DESCRICAO',pic:'@!',nv:''},{av:'AV74TFMetodologia_Descricao_Sel',fld:'vTFMETODOLOGIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV106Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV64MetodologiaFases_NomeTitleFilterData',fld:'vMETODOLOGIAFASES_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV68MetodologiaFases_PercentualTitleFilterData',fld:'vMETODOLOGIAFASES_PERCENTUALTITLEFILTERDATA',pic:'',nv:null},{av:'AV80MetodologiaFases_PrazoTitleFilterData',fld:'vMETODOLOGIAFASES_PRAZOTITLEFILTERDATA',pic:'',nv:null},{av:'AV72Metodologia_DescricaoTitleFilterData',fld:'vMETODOLOGIA_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'edtMetodologiaFases_Nome_Titleformat',ctrl:'METODOLOGIAFASES_NOME',prop:'Titleformat'},{av:'edtMetodologiaFases_Nome_Title',ctrl:'METODOLOGIAFASES_NOME',prop:'Title'},{av:'edtMetodologiaFases_Percentual_Titleformat',ctrl:'METODOLOGIAFASES_PERCENTUAL',prop:'Titleformat'},{av:'edtMetodologiaFases_Percentual_Title',ctrl:'METODOLOGIAFASES_PERCENTUAL',prop:'Title'},{av:'edtMetodologiaFases_Prazo_Titleformat',ctrl:'METODOLOGIAFASES_PRAZO',prop:'Titleformat'},{av:'edtMetodologiaFases_Prazo_Title',ctrl:'METODOLOGIAFASES_PRAZO',prop:'Title'},{av:'edtMetodologia_Descricao_Titleformat',ctrl:'METODOLOGIA_DESCRICAO',prop:'Titleformat'},{av:'edtMetodologia_Descricao_Title',ctrl:'METODOLOGIA_DESCRICAO',prop:'Title'},{av:'AV78GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV79GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E117X2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18MetodologiaFases_Nome1',fld:'vMETODOLOGIAFASES_NOME1',pic:'@!',nv:''},{av:'AV19Metodologia_Descricao1',fld:'vMETODOLOGIA_DESCRICAO1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23MetodologiaFases_Nome2',fld:'vMETODOLOGIAFASES_NOME2',pic:'@!',nv:''},{av:'AV24Metodologia_Descricao2',fld:'vMETODOLOGIA_DESCRICAO2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV65TFMetodologiaFases_Nome',fld:'vTFMETODOLOGIAFASES_NOME',pic:'@!',nv:''},{av:'AV66TFMetodologiaFases_Nome_Sel',fld:'vTFMETODOLOGIAFASES_NOME_SEL',pic:'@!',nv:''},{av:'AV69TFMetodologiaFases_Percentual',fld:'vTFMETODOLOGIAFASES_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV70TFMetodologiaFases_Percentual_To',fld:'vTFMETODOLOGIAFASES_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV81TFMetodologiaFases_Prazo',fld:'vTFMETODOLOGIAFASES_PRAZO',pic:'ZZZ9.99',nv:0.0},{av:'AV82TFMetodologiaFases_Prazo_To',fld:'vTFMETODOLOGIAFASES_PRAZO_TO',pic:'ZZZ9.99',nv:0.0},{av:'AV73TFMetodologia_Descricao',fld:'vTFMETODOLOGIA_DESCRICAO',pic:'@!',nv:''},{av:'AV74TFMetodologia_Descricao_Sel',fld:'vTFMETODOLOGIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV67ddo_MetodologiaFases_NomeTitleControlIdToReplace',fld:'vDDO_METODOLOGIAFASES_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_MetodologiaFases_PercentualTitleControlIdToReplace',fld:'vDDO_METODOLOGIAFASES_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_MetodologiaFases_PrazoTitleControlIdToReplace',fld:'vDDO_METODOLOGIAFASES_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Metodologia_DescricaoTitleControlIdToReplace',fld:'vDDO_METODOLOGIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A147MetodologiaFases_Codigo',fld:'METODOLOGIAFASES_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A137Metodologia_Codigo',fld:'METODOLOGIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_METODOLOGIAFASES_NOME.ONOPTIONCLICKED","{handler:'E127X2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18MetodologiaFases_Nome1',fld:'vMETODOLOGIAFASES_NOME1',pic:'@!',nv:''},{av:'AV19Metodologia_Descricao1',fld:'vMETODOLOGIA_DESCRICAO1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23MetodologiaFases_Nome2',fld:'vMETODOLOGIAFASES_NOME2',pic:'@!',nv:''},{av:'AV24Metodologia_Descricao2',fld:'vMETODOLOGIA_DESCRICAO2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV65TFMetodologiaFases_Nome',fld:'vTFMETODOLOGIAFASES_NOME',pic:'@!',nv:''},{av:'AV66TFMetodologiaFases_Nome_Sel',fld:'vTFMETODOLOGIAFASES_NOME_SEL',pic:'@!',nv:''},{av:'AV69TFMetodologiaFases_Percentual',fld:'vTFMETODOLOGIAFASES_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV70TFMetodologiaFases_Percentual_To',fld:'vTFMETODOLOGIAFASES_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV81TFMetodologiaFases_Prazo',fld:'vTFMETODOLOGIAFASES_PRAZO',pic:'ZZZ9.99',nv:0.0},{av:'AV82TFMetodologiaFases_Prazo_To',fld:'vTFMETODOLOGIAFASES_PRAZO_TO',pic:'ZZZ9.99',nv:0.0},{av:'AV73TFMetodologia_Descricao',fld:'vTFMETODOLOGIA_DESCRICAO',pic:'@!',nv:''},{av:'AV74TFMetodologia_Descricao_Sel',fld:'vTFMETODOLOGIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV67ddo_MetodologiaFases_NomeTitleControlIdToReplace',fld:'vDDO_METODOLOGIAFASES_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_MetodologiaFases_PercentualTitleControlIdToReplace',fld:'vDDO_METODOLOGIAFASES_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_MetodologiaFases_PrazoTitleControlIdToReplace',fld:'vDDO_METODOLOGIAFASES_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Metodologia_DescricaoTitleControlIdToReplace',fld:'vDDO_METODOLOGIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A147MetodologiaFases_Codigo',fld:'METODOLOGIAFASES_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A137Metodologia_Codigo',fld:'METODOLOGIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_metodologiafases_nome_Activeeventkey',ctrl:'DDO_METODOLOGIAFASES_NOME',prop:'ActiveEventKey'},{av:'Ddo_metodologiafases_nome_Filteredtext_get',ctrl:'DDO_METODOLOGIAFASES_NOME',prop:'FilteredText_get'},{av:'Ddo_metodologiafases_nome_Selectedvalue_get',ctrl:'DDO_METODOLOGIAFASES_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_metodologiafases_nome_Sortedstatus',ctrl:'DDO_METODOLOGIAFASES_NOME',prop:'SortedStatus'},{av:'AV65TFMetodologiaFases_Nome',fld:'vTFMETODOLOGIAFASES_NOME',pic:'@!',nv:''},{av:'AV66TFMetodologiaFases_Nome_Sel',fld:'vTFMETODOLOGIAFASES_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_metodologiafases_percentual_Sortedstatus',ctrl:'DDO_METODOLOGIAFASES_PERCENTUAL',prop:'SortedStatus'},{av:'Ddo_metodologiafases_prazo_Sortedstatus',ctrl:'DDO_METODOLOGIAFASES_PRAZO',prop:'SortedStatus'},{av:'Ddo_metodologia_descricao_Sortedstatus',ctrl:'DDO_METODOLOGIA_DESCRICAO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_METODOLOGIAFASES_PERCENTUAL.ONOPTIONCLICKED","{handler:'E137X2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18MetodologiaFases_Nome1',fld:'vMETODOLOGIAFASES_NOME1',pic:'@!',nv:''},{av:'AV19Metodologia_Descricao1',fld:'vMETODOLOGIA_DESCRICAO1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23MetodologiaFases_Nome2',fld:'vMETODOLOGIAFASES_NOME2',pic:'@!',nv:''},{av:'AV24Metodologia_Descricao2',fld:'vMETODOLOGIA_DESCRICAO2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV65TFMetodologiaFases_Nome',fld:'vTFMETODOLOGIAFASES_NOME',pic:'@!',nv:''},{av:'AV66TFMetodologiaFases_Nome_Sel',fld:'vTFMETODOLOGIAFASES_NOME_SEL',pic:'@!',nv:''},{av:'AV69TFMetodologiaFases_Percentual',fld:'vTFMETODOLOGIAFASES_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV70TFMetodologiaFases_Percentual_To',fld:'vTFMETODOLOGIAFASES_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV81TFMetodologiaFases_Prazo',fld:'vTFMETODOLOGIAFASES_PRAZO',pic:'ZZZ9.99',nv:0.0},{av:'AV82TFMetodologiaFases_Prazo_To',fld:'vTFMETODOLOGIAFASES_PRAZO_TO',pic:'ZZZ9.99',nv:0.0},{av:'AV73TFMetodologia_Descricao',fld:'vTFMETODOLOGIA_DESCRICAO',pic:'@!',nv:''},{av:'AV74TFMetodologia_Descricao_Sel',fld:'vTFMETODOLOGIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV67ddo_MetodologiaFases_NomeTitleControlIdToReplace',fld:'vDDO_METODOLOGIAFASES_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_MetodologiaFases_PercentualTitleControlIdToReplace',fld:'vDDO_METODOLOGIAFASES_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_MetodologiaFases_PrazoTitleControlIdToReplace',fld:'vDDO_METODOLOGIAFASES_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Metodologia_DescricaoTitleControlIdToReplace',fld:'vDDO_METODOLOGIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A147MetodologiaFases_Codigo',fld:'METODOLOGIAFASES_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A137Metodologia_Codigo',fld:'METODOLOGIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_metodologiafases_percentual_Activeeventkey',ctrl:'DDO_METODOLOGIAFASES_PERCENTUAL',prop:'ActiveEventKey'},{av:'Ddo_metodologiafases_percentual_Filteredtext_get',ctrl:'DDO_METODOLOGIAFASES_PERCENTUAL',prop:'FilteredText_get'},{av:'Ddo_metodologiafases_percentual_Filteredtextto_get',ctrl:'DDO_METODOLOGIAFASES_PERCENTUAL',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_metodologiafases_percentual_Sortedstatus',ctrl:'DDO_METODOLOGIAFASES_PERCENTUAL',prop:'SortedStatus'},{av:'AV69TFMetodologiaFases_Percentual',fld:'vTFMETODOLOGIAFASES_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV70TFMetodologiaFases_Percentual_To',fld:'vTFMETODOLOGIAFASES_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'Ddo_metodologiafases_nome_Sortedstatus',ctrl:'DDO_METODOLOGIAFASES_NOME',prop:'SortedStatus'},{av:'Ddo_metodologiafases_prazo_Sortedstatus',ctrl:'DDO_METODOLOGIAFASES_PRAZO',prop:'SortedStatus'},{av:'Ddo_metodologia_descricao_Sortedstatus',ctrl:'DDO_METODOLOGIA_DESCRICAO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_METODOLOGIAFASES_PRAZO.ONOPTIONCLICKED","{handler:'E147X2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18MetodologiaFases_Nome1',fld:'vMETODOLOGIAFASES_NOME1',pic:'@!',nv:''},{av:'AV19Metodologia_Descricao1',fld:'vMETODOLOGIA_DESCRICAO1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23MetodologiaFases_Nome2',fld:'vMETODOLOGIAFASES_NOME2',pic:'@!',nv:''},{av:'AV24Metodologia_Descricao2',fld:'vMETODOLOGIA_DESCRICAO2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV65TFMetodologiaFases_Nome',fld:'vTFMETODOLOGIAFASES_NOME',pic:'@!',nv:''},{av:'AV66TFMetodologiaFases_Nome_Sel',fld:'vTFMETODOLOGIAFASES_NOME_SEL',pic:'@!',nv:''},{av:'AV69TFMetodologiaFases_Percentual',fld:'vTFMETODOLOGIAFASES_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV70TFMetodologiaFases_Percentual_To',fld:'vTFMETODOLOGIAFASES_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV81TFMetodologiaFases_Prazo',fld:'vTFMETODOLOGIAFASES_PRAZO',pic:'ZZZ9.99',nv:0.0},{av:'AV82TFMetodologiaFases_Prazo_To',fld:'vTFMETODOLOGIAFASES_PRAZO_TO',pic:'ZZZ9.99',nv:0.0},{av:'AV73TFMetodologia_Descricao',fld:'vTFMETODOLOGIA_DESCRICAO',pic:'@!',nv:''},{av:'AV74TFMetodologia_Descricao_Sel',fld:'vTFMETODOLOGIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV67ddo_MetodologiaFases_NomeTitleControlIdToReplace',fld:'vDDO_METODOLOGIAFASES_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_MetodologiaFases_PercentualTitleControlIdToReplace',fld:'vDDO_METODOLOGIAFASES_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_MetodologiaFases_PrazoTitleControlIdToReplace',fld:'vDDO_METODOLOGIAFASES_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Metodologia_DescricaoTitleControlIdToReplace',fld:'vDDO_METODOLOGIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A147MetodologiaFases_Codigo',fld:'METODOLOGIAFASES_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A137Metodologia_Codigo',fld:'METODOLOGIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_metodologiafases_prazo_Activeeventkey',ctrl:'DDO_METODOLOGIAFASES_PRAZO',prop:'ActiveEventKey'},{av:'Ddo_metodologiafases_prazo_Filteredtext_get',ctrl:'DDO_METODOLOGIAFASES_PRAZO',prop:'FilteredText_get'},{av:'Ddo_metodologiafases_prazo_Filteredtextto_get',ctrl:'DDO_METODOLOGIAFASES_PRAZO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_metodologiafases_prazo_Sortedstatus',ctrl:'DDO_METODOLOGIAFASES_PRAZO',prop:'SortedStatus'},{av:'AV81TFMetodologiaFases_Prazo',fld:'vTFMETODOLOGIAFASES_PRAZO',pic:'ZZZ9.99',nv:0.0},{av:'AV82TFMetodologiaFases_Prazo_To',fld:'vTFMETODOLOGIAFASES_PRAZO_TO',pic:'ZZZ9.99',nv:0.0},{av:'Ddo_metodologiafases_nome_Sortedstatus',ctrl:'DDO_METODOLOGIAFASES_NOME',prop:'SortedStatus'},{av:'Ddo_metodologiafases_percentual_Sortedstatus',ctrl:'DDO_METODOLOGIAFASES_PERCENTUAL',prop:'SortedStatus'},{av:'Ddo_metodologia_descricao_Sortedstatus',ctrl:'DDO_METODOLOGIA_DESCRICAO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_METODOLOGIA_DESCRICAO.ONOPTIONCLICKED","{handler:'E157X2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18MetodologiaFases_Nome1',fld:'vMETODOLOGIAFASES_NOME1',pic:'@!',nv:''},{av:'AV19Metodologia_Descricao1',fld:'vMETODOLOGIA_DESCRICAO1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23MetodologiaFases_Nome2',fld:'vMETODOLOGIAFASES_NOME2',pic:'@!',nv:''},{av:'AV24Metodologia_Descricao2',fld:'vMETODOLOGIA_DESCRICAO2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV65TFMetodologiaFases_Nome',fld:'vTFMETODOLOGIAFASES_NOME',pic:'@!',nv:''},{av:'AV66TFMetodologiaFases_Nome_Sel',fld:'vTFMETODOLOGIAFASES_NOME_SEL',pic:'@!',nv:''},{av:'AV69TFMetodologiaFases_Percentual',fld:'vTFMETODOLOGIAFASES_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV70TFMetodologiaFases_Percentual_To',fld:'vTFMETODOLOGIAFASES_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV81TFMetodologiaFases_Prazo',fld:'vTFMETODOLOGIAFASES_PRAZO',pic:'ZZZ9.99',nv:0.0},{av:'AV82TFMetodologiaFases_Prazo_To',fld:'vTFMETODOLOGIAFASES_PRAZO_TO',pic:'ZZZ9.99',nv:0.0},{av:'AV73TFMetodologia_Descricao',fld:'vTFMETODOLOGIA_DESCRICAO',pic:'@!',nv:''},{av:'AV74TFMetodologia_Descricao_Sel',fld:'vTFMETODOLOGIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV67ddo_MetodologiaFases_NomeTitleControlIdToReplace',fld:'vDDO_METODOLOGIAFASES_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_MetodologiaFases_PercentualTitleControlIdToReplace',fld:'vDDO_METODOLOGIAFASES_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_MetodologiaFases_PrazoTitleControlIdToReplace',fld:'vDDO_METODOLOGIAFASES_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Metodologia_DescricaoTitleControlIdToReplace',fld:'vDDO_METODOLOGIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A147MetodologiaFases_Codigo',fld:'METODOLOGIAFASES_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A137Metodologia_Codigo',fld:'METODOLOGIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_metodologia_descricao_Activeeventkey',ctrl:'DDO_METODOLOGIA_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_metodologia_descricao_Filteredtext_get',ctrl:'DDO_METODOLOGIA_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_metodologia_descricao_Selectedvalue_get',ctrl:'DDO_METODOLOGIA_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_metodologia_descricao_Sortedstatus',ctrl:'DDO_METODOLOGIA_DESCRICAO',prop:'SortedStatus'},{av:'AV73TFMetodologia_Descricao',fld:'vTFMETODOLOGIA_DESCRICAO',pic:'@!',nv:''},{av:'AV74TFMetodologia_Descricao_Sel',fld:'vTFMETODOLOGIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_metodologiafases_nome_Sortedstatus',ctrl:'DDO_METODOLOGIAFASES_NOME',prop:'SortedStatus'},{av:'Ddo_metodologiafases_percentual_Sortedstatus',ctrl:'DDO_METODOLOGIAFASES_PERCENTUAL',prop:'SortedStatus'},{av:'Ddo_metodologiafases_prazo_Sortedstatus',ctrl:'DDO_METODOLOGIAFASES_PRAZO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E267X2',iparms:[{av:'A147MetodologiaFases_Codigo',fld:'METODOLOGIAFASES_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A137Metodologia_Codigo',fld:'METODOLOGIA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV32Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV33Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV34Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'edtMetodologiaFases_Nome_Link',ctrl:'METODOLOGIAFASES_NOME',prop:'Link'},{av:'edtMetodologia_Descricao_Link',ctrl:'METODOLOGIA_DESCRICAO',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E167X2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18MetodologiaFases_Nome1',fld:'vMETODOLOGIAFASES_NOME1',pic:'@!',nv:''},{av:'AV19Metodologia_Descricao1',fld:'vMETODOLOGIA_DESCRICAO1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23MetodologiaFases_Nome2',fld:'vMETODOLOGIAFASES_NOME2',pic:'@!',nv:''},{av:'AV24Metodologia_Descricao2',fld:'vMETODOLOGIA_DESCRICAO2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV65TFMetodologiaFases_Nome',fld:'vTFMETODOLOGIAFASES_NOME',pic:'@!',nv:''},{av:'AV66TFMetodologiaFases_Nome_Sel',fld:'vTFMETODOLOGIAFASES_NOME_SEL',pic:'@!',nv:''},{av:'AV69TFMetodologiaFases_Percentual',fld:'vTFMETODOLOGIAFASES_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV70TFMetodologiaFases_Percentual_To',fld:'vTFMETODOLOGIAFASES_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV81TFMetodologiaFases_Prazo',fld:'vTFMETODOLOGIAFASES_PRAZO',pic:'ZZZ9.99',nv:0.0},{av:'AV82TFMetodologiaFases_Prazo_To',fld:'vTFMETODOLOGIAFASES_PRAZO_TO',pic:'ZZZ9.99',nv:0.0},{av:'AV73TFMetodologia_Descricao',fld:'vTFMETODOLOGIA_DESCRICAO',pic:'@!',nv:''},{av:'AV74TFMetodologia_Descricao_Sel',fld:'vTFMETODOLOGIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV67ddo_MetodologiaFases_NomeTitleControlIdToReplace',fld:'vDDO_METODOLOGIAFASES_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_MetodologiaFases_PercentualTitleControlIdToReplace',fld:'vDDO_METODOLOGIAFASES_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_MetodologiaFases_PrazoTitleControlIdToReplace',fld:'vDDO_METODOLOGIAFASES_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Metodologia_DescricaoTitleControlIdToReplace',fld:'vDDO_METODOLOGIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A147MetodologiaFases_Codigo',fld:'METODOLOGIAFASES_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A137Metodologia_Codigo',fld:'METODOLOGIA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E217X2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18MetodologiaFases_Nome1',fld:'vMETODOLOGIAFASES_NOME1',pic:'@!',nv:''},{av:'AV19Metodologia_Descricao1',fld:'vMETODOLOGIA_DESCRICAO1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23MetodologiaFases_Nome2',fld:'vMETODOLOGIAFASES_NOME2',pic:'@!',nv:''},{av:'AV24Metodologia_Descricao2',fld:'vMETODOLOGIA_DESCRICAO2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV65TFMetodologiaFases_Nome',fld:'vTFMETODOLOGIAFASES_NOME',pic:'@!',nv:''},{av:'AV66TFMetodologiaFases_Nome_Sel',fld:'vTFMETODOLOGIAFASES_NOME_SEL',pic:'@!',nv:''},{av:'AV69TFMetodologiaFases_Percentual',fld:'vTFMETODOLOGIAFASES_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV70TFMetodologiaFases_Percentual_To',fld:'vTFMETODOLOGIAFASES_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV81TFMetodologiaFases_Prazo',fld:'vTFMETODOLOGIAFASES_PRAZO',pic:'ZZZ9.99',nv:0.0},{av:'AV82TFMetodologiaFases_Prazo_To',fld:'vTFMETODOLOGIAFASES_PRAZO_TO',pic:'ZZZ9.99',nv:0.0},{av:'AV73TFMetodologia_Descricao',fld:'vTFMETODOLOGIA_DESCRICAO',pic:'@!',nv:''},{av:'AV74TFMetodologia_Descricao_Sel',fld:'vTFMETODOLOGIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV67ddo_MetodologiaFases_NomeTitleControlIdToReplace',fld:'vDDO_METODOLOGIAFASES_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_MetodologiaFases_PercentualTitleControlIdToReplace',fld:'vDDO_METODOLOGIAFASES_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_MetodologiaFases_PrazoTitleControlIdToReplace',fld:'vDDO_METODOLOGIAFASES_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Metodologia_DescricaoTitleControlIdToReplace',fld:'vDDO_METODOLOGIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A147MetodologiaFases_Codigo',fld:'METODOLOGIAFASES_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A137Metodologia_Codigo',fld:'METODOLOGIA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E177X2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18MetodologiaFases_Nome1',fld:'vMETODOLOGIAFASES_NOME1',pic:'@!',nv:''},{av:'AV19Metodologia_Descricao1',fld:'vMETODOLOGIA_DESCRICAO1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23MetodologiaFases_Nome2',fld:'vMETODOLOGIAFASES_NOME2',pic:'@!',nv:''},{av:'AV24Metodologia_Descricao2',fld:'vMETODOLOGIA_DESCRICAO2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV65TFMetodologiaFases_Nome',fld:'vTFMETODOLOGIAFASES_NOME',pic:'@!',nv:''},{av:'AV66TFMetodologiaFases_Nome_Sel',fld:'vTFMETODOLOGIAFASES_NOME_SEL',pic:'@!',nv:''},{av:'AV69TFMetodologiaFases_Percentual',fld:'vTFMETODOLOGIAFASES_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV70TFMetodologiaFases_Percentual_To',fld:'vTFMETODOLOGIAFASES_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV81TFMetodologiaFases_Prazo',fld:'vTFMETODOLOGIAFASES_PRAZO',pic:'ZZZ9.99',nv:0.0},{av:'AV82TFMetodologiaFases_Prazo_To',fld:'vTFMETODOLOGIAFASES_PRAZO_TO',pic:'ZZZ9.99',nv:0.0},{av:'AV73TFMetodologia_Descricao',fld:'vTFMETODOLOGIA_DESCRICAO',pic:'@!',nv:''},{av:'AV74TFMetodologia_Descricao_Sel',fld:'vTFMETODOLOGIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV67ddo_MetodologiaFases_NomeTitleControlIdToReplace',fld:'vDDO_METODOLOGIAFASES_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_MetodologiaFases_PercentualTitleControlIdToReplace',fld:'vDDO_METODOLOGIAFASES_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_MetodologiaFases_PrazoTitleControlIdToReplace',fld:'vDDO_METODOLOGIAFASES_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Metodologia_DescricaoTitleControlIdToReplace',fld:'vDDO_METODOLOGIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A147MetodologiaFases_Codigo',fld:'METODOLOGIAFASES_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A137Metodologia_Codigo',fld:'METODOLOGIA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23MetodologiaFases_Nome2',fld:'vMETODOLOGIAFASES_NOME2',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18MetodologiaFases_Nome1',fld:'vMETODOLOGIAFASES_NOME1',pic:'@!',nv:''},{av:'AV19Metodologia_Descricao1',fld:'vMETODOLOGIA_DESCRICAO1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24Metodologia_Descricao2',fld:'vMETODOLOGIA_DESCRICAO2',pic:'@!',nv:''},{av:'edtavMetodologiafases_nome2_Visible',ctrl:'vMETODOLOGIAFASES_NOME2',prop:'Visible'},{av:'edtavMetodologia_descricao2_Visible',ctrl:'vMETODOLOGIA_DESCRICAO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavMetodologiafases_nome1_Visible',ctrl:'vMETODOLOGIAFASES_NOME1',prop:'Visible'},{av:'edtavMetodologia_descricao1_Visible',ctrl:'vMETODOLOGIA_DESCRICAO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E227X2',iparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'edtavMetodologiafases_nome1_Visible',ctrl:'vMETODOLOGIAFASES_NOME1',prop:'Visible'},{av:'edtavMetodologia_descricao1_Visible',ctrl:'vMETODOLOGIA_DESCRICAO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E187X2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18MetodologiaFases_Nome1',fld:'vMETODOLOGIAFASES_NOME1',pic:'@!',nv:''},{av:'AV19Metodologia_Descricao1',fld:'vMETODOLOGIA_DESCRICAO1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23MetodologiaFases_Nome2',fld:'vMETODOLOGIAFASES_NOME2',pic:'@!',nv:''},{av:'AV24Metodologia_Descricao2',fld:'vMETODOLOGIA_DESCRICAO2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV65TFMetodologiaFases_Nome',fld:'vTFMETODOLOGIAFASES_NOME',pic:'@!',nv:''},{av:'AV66TFMetodologiaFases_Nome_Sel',fld:'vTFMETODOLOGIAFASES_NOME_SEL',pic:'@!',nv:''},{av:'AV69TFMetodologiaFases_Percentual',fld:'vTFMETODOLOGIAFASES_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV70TFMetodologiaFases_Percentual_To',fld:'vTFMETODOLOGIAFASES_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV81TFMetodologiaFases_Prazo',fld:'vTFMETODOLOGIAFASES_PRAZO',pic:'ZZZ9.99',nv:0.0},{av:'AV82TFMetodologiaFases_Prazo_To',fld:'vTFMETODOLOGIAFASES_PRAZO_TO',pic:'ZZZ9.99',nv:0.0},{av:'AV73TFMetodologia_Descricao',fld:'vTFMETODOLOGIA_DESCRICAO',pic:'@!',nv:''},{av:'AV74TFMetodologia_Descricao_Sel',fld:'vTFMETODOLOGIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV67ddo_MetodologiaFases_NomeTitleControlIdToReplace',fld:'vDDO_METODOLOGIAFASES_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_MetodologiaFases_PercentualTitleControlIdToReplace',fld:'vDDO_METODOLOGIAFASES_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_MetodologiaFases_PrazoTitleControlIdToReplace',fld:'vDDO_METODOLOGIAFASES_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Metodologia_DescricaoTitleControlIdToReplace',fld:'vDDO_METODOLOGIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A147MetodologiaFases_Codigo',fld:'METODOLOGIAFASES_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A137Metodologia_Codigo',fld:'METODOLOGIA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23MetodologiaFases_Nome2',fld:'vMETODOLOGIAFASES_NOME2',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18MetodologiaFases_Nome1',fld:'vMETODOLOGIAFASES_NOME1',pic:'@!',nv:''},{av:'AV19Metodologia_Descricao1',fld:'vMETODOLOGIA_DESCRICAO1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24Metodologia_Descricao2',fld:'vMETODOLOGIA_DESCRICAO2',pic:'@!',nv:''},{av:'edtavMetodologiafases_nome2_Visible',ctrl:'vMETODOLOGIAFASES_NOME2',prop:'Visible'},{av:'edtavMetodologia_descricao2_Visible',ctrl:'vMETODOLOGIA_DESCRICAO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavMetodologiafases_nome1_Visible',ctrl:'vMETODOLOGIAFASES_NOME1',prop:'Visible'},{av:'edtavMetodologia_descricao1_Visible',ctrl:'vMETODOLOGIA_DESCRICAO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E237X2',iparms:[{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'edtavMetodologiafases_nome2_Visible',ctrl:'vMETODOLOGIAFASES_NOME2',prop:'Visible'},{av:'edtavMetodologia_descricao2_Visible',ctrl:'vMETODOLOGIA_DESCRICAO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E197X2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18MetodologiaFases_Nome1',fld:'vMETODOLOGIAFASES_NOME1',pic:'@!',nv:''},{av:'AV19Metodologia_Descricao1',fld:'vMETODOLOGIA_DESCRICAO1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23MetodologiaFases_Nome2',fld:'vMETODOLOGIAFASES_NOME2',pic:'@!',nv:''},{av:'AV24Metodologia_Descricao2',fld:'vMETODOLOGIA_DESCRICAO2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV65TFMetodologiaFases_Nome',fld:'vTFMETODOLOGIAFASES_NOME',pic:'@!',nv:''},{av:'AV66TFMetodologiaFases_Nome_Sel',fld:'vTFMETODOLOGIAFASES_NOME_SEL',pic:'@!',nv:''},{av:'AV69TFMetodologiaFases_Percentual',fld:'vTFMETODOLOGIAFASES_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV70TFMetodologiaFases_Percentual_To',fld:'vTFMETODOLOGIAFASES_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV81TFMetodologiaFases_Prazo',fld:'vTFMETODOLOGIAFASES_PRAZO',pic:'ZZZ9.99',nv:0.0},{av:'AV82TFMetodologiaFases_Prazo_To',fld:'vTFMETODOLOGIAFASES_PRAZO_TO',pic:'ZZZ9.99',nv:0.0},{av:'AV73TFMetodologia_Descricao',fld:'vTFMETODOLOGIA_DESCRICAO',pic:'@!',nv:''},{av:'AV74TFMetodologia_Descricao_Sel',fld:'vTFMETODOLOGIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV67ddo_MetodologiaFases_NomeTitleControlIdToReplace',fld:'vDDO_METODOLOGIAFASES_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_MetodologiaFases_PercentualTitleControlIdToReplace',fld:'vDDO_METODOLOGIAFASES_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_MetodologiaFases_PrazoTitleControlIdToReplace',fld:'vDDO_METODOLOGIAFASES_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Metodologia_DescricaoTitleControlIdToReplace',fld:'vDDO_METODOLOGIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A147MetodologiaFases_Codigo',fld:'METODOLOGIAFASES_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A137Metodologia_Codigo',fld:'METODOLOGIA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV65TFMetodologiaFases_Nome',fld:'vTFMETODOLOGIAFASES_NOME',pic:'@!',nv:''},{av:'Ddo_metodologiafases_nome_Filteredtext_set',ctrl:'DDO_METODOLOGIAFASES_NOME',prop:'FilteredText_set'},{av:'AV66TFMetodologiaFases_Nome_Sel',fld:'vTFMETODOLOGIAFASES_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_metodologiafases_nome_Selectedvalue_set',ctrl:'DDO_METODOLOGIAFASES_NOME',prop:'SelectedValue_set'},{av:'AV69TFMetodologiaFases_Percentual',fld:'vTFMETODOLOGIAFASES_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'Ddo_metodologiafases_percentual_Filteredtext_set',ctrl:'DDO_METODOLOGIAFASES_PERCENTUAL',prop:'FilteredText_set'},{av:'AV70TFMetodologiaFases_Percentual_To',fld:'vTFMETODOLOGIAFASES_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'Ddo_metodologiafases_percentual_Filteredtextto_set',ctrl:'DDO_METODOLOGIAFASES_PERCENTUAL',prop:'FilteredTextTo_set'},{av:'AV81TFMetodologiaFases_Prazo',fld:'vTFMETODOLOGIAFASES_PRAZO',pic:'ZZZ9.99',nv:0.0},{av:'Ddo_metodologiafases_prazo_Filteredtext_set',ctrl:'DDO_METODOLOGIAFASES_PRAZO',prop:'FilteredText_set'},{av:'AV82TFMetodologiaFases_Prazo_To',fld:'vTFMETODOLOGIAFASES_PRAZO_TO',pic:'ZZZ9.99',nv:0.0},{av:'Ddo_metodologiafases_prazo_Filteredtextto_set',ctrl:'DDO_METODOLOGIAFASES_PRAZO',prop:'FilteredTextTo_set'},{av:'AV73TFMetodologia_Descricao',fld:'vTFMETODOLOGIA_DESCRICAO',pic:'@!',nv:''},{av:'Ddo_metodologia_descricao_Filteredtext_set',ctrl:'DDO_METODOLOGIA_DESCRICAO',prop:'FilteredText_set'},{av:'AV74TFMetodologia_Descricao_Sel',fld:'vTFMETODOLOGIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_metodologia_descricao_Selectedvalue_set',ctrl:'DDO_METODOLOGIA_DESCRICAO',prop:'SelectedValue_set'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18MetodologiaFases_Nome1',fld:'vMETODOLOGIAFASES_NOME1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavMetodologiafases_nome1_Visible',ctrl:'vMETODOLOGIAFASES_NOME1',prop:'Visible'},{av:'edtavMetodologia_descricao1_Visible',ctrl:'vMETODOLOGIA_DESCRICAO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23MetodologiaFases_Nome2',fld:'vMETODOLOGIAFASES_NOME2',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV19Metodologia_Descricao1',fld:'vMETODOLOGIA_DESCRICAO1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24Metodologia_Descricao2',fld:'vMETODOLOGIA_DESCRICAO2',pic:'@!',nv:''},{av:'edtavMetodologiafases_nome2_Visible',ctrl:'vMETODOLOGIAFASES_NOME2',prop:'Visible'},{av:'edtavMetodologia_descricao2_Visible',ctrl:'vMETODOLOGIA_DESCRICAO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E207X2',iparms:[{av:'A147MetodologiaFases_Codigo',fld:'METODOLOGIAFASES_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A137Metodologia_Codigo',fld:'METODOLOGIA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'A137Metodologia_Codigo',fld:'METODOLOGIA_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_metodologiafases_nome_Activeeventkey = "";
         Ddo_metodologiafases_nome_Filteredtext_get = "";
         Ddo_metodologiafases_nome_Selectedvalue_get = "";
         Ddo_metodologiafases_percentual_Activeeventkey = "";
         Ddo_metodologiafases_percentual_Filteredtext_get = "";
         Ddo_metodologiafases_percentual_Filteredtextto_get = "";
         Ddo_metodologiafases_prazo_Activeeventkey = "";
         Ddo_metodologiafases_prazo_Filteredtext_get = "";
         Ddo_metodologiafases_prazo_Filteredtextto_get = "";
         Ddo_metodologia_descricao_Activeeventkey = "";
         Ddo_metodologia_descricao_Filteredtext_get = "";
         Ddo_metodologia_descricao_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV16DynamicFiltersSelector1 = "";
         AV18MetodologiaFases_Nome1 = "";
         AV19Metodologia_Descricao1 = "";
         AV21DynamicFiltersSelector2 = "";
         AV23MetodologiaFases_Nome2 = "";
         AV24Metodologia_Descricao2 = "";
         AV65TFMetodologiaFases_Nome = "";
         AV66TFMetodologiaFases_Nome_Sel = "";
         AV73TFMetodologia_Descricao = "";
         AV74TFMetodologia_Descricao_Sel = "";
         AV67ddo_MetodologiaFases_NomeTitleControlIdToReplace = "";
         AV71ddo_MetodologiaFases_PercentualTitleControlIdToReplace = "";
         AV83ddo_MetodologiaFases_PrazoTitleControlIdToReplace = "";
         AV75ddo_Metodologia_DescricaoTitleControlIdToReplace = "";
         AV106Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV76DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV64MetodologiaFases_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV68MetodologiaFases_PercentualTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV80MetodologiaFases_PrazoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV72Metodologia_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_metodologiafases_nome_Filteredtext_set = "";
         Ddo_metodologiafases_nome_Selectedvalue_set = "";
         Ddo_metodologiafases_nome_Sortedstatus = "";
         Ddo_metodologiafases_percentual_Filteredtext_set = "";
         Ddo_metodologiafases_percentual_Filteredtextto_set = "";
         Ddo_metodologiafases_percentual_Sortedstatus = "";
         Ddo_metodologiafases_prazo_Filteredtext_set = "";
         Ddo_metodologiafases_prazo_Filteredtextto_set = "";
         Ddo_metodologiafases_prazo_Sortedstatus = "";
         Ddo_metodologia_descricao_Filteredtext_set = "";
         Ddo_metodologia_descricao_Selectedvalue_set = "";
         Ddo_metodologia_descricao_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV32Update = "";
         AV103Update_GXI = "";
         AV33Delete = "";
         AV104Delete_GXI = "";
         AV34Display = "";
         AV105Display_GXI = "";
         A148MetodologiaFases_Nome = "";
         A138Metodologia_Descricao = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV88WWMetodologiaFasesDS_3_Metodologiafases_nome1 = "";
         lV89WWMetodologiaFasesDS_4_Metodologia_descricao1 = "";
         lV93WWMetodologiaFasesDS_8_Metodologiafases_nome2 = "";
         lV94WWMetodologiaFasesDS_9_Metodologia_descricao2 = "";
         lV95WWMetodologiaFasesDS_10_Tfmetodologiafases_nome = "";
         lV101WWMetodologiaFasesDS_16_Tfmetodologia_descricao = "";
         AV86WWMetodologiaFasesDS_1_Dynamicfiltersselector1 = "";
         AV88WWMetodologiaFasesDS_3_Metodologiafases_nome1 = "";
         AV89WWMetodologiaFasesDS_4_Metodologia_descricao1 = "";
         AV91WWMetodologiaFasesDS_6_Dynamicfiltersselector2 = "";
         AV93WWMetodologiaFasesDS_8_Metodologiafases_nome2 = "";
         AV94WWMetodologiaFasesDS_9_Metodologia_descricao2 = "";
         AV96WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel = "";
         AV95WWMetodologiaFasesDS_10_Tfmetodologiafases_nome = "";
         AV102WWMetodologiaFasesDS_17_Tfmetodologia_descricao_sel = "";
         AV101WWMetodologiaFasesDS_16_Tfmetodologia_descricao = "";
         H007X2_A138Metodologia_Descricao = new String[] {""} ;
         H007X2_A137Metodologia_Codigo = new int[1] ;
         H007X2_A2135MetodologiaFases_Prazo = new decimal[1] ;
         H007X2_n2135MetodologiaFases_Prazo = new bool[] {false} ;
         H007X2_A149MetodologiaFases_Percentual = new decimal[1] ;
         H007X2_A148MetodologiaFases_Nome = new String[] {""} ;
         H007X2_A147MetodologiaFases_Codigo = new int[1] ;
         H007X3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV55Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblMetodologiafasestitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwmetodologiafases__default(),
            new Object[][] {
                new Object[] {
               H007X2_A138Metodologia_Descricao, H007X2_A137Metodologia_Codigo, H007X2_A2135MetodologiaFases_Prazo, H007X2_n2135MetodologiaFases_Prazo, H007X2_A149MetodologiaFases_Percentual, H007X2_A148MetodologiaFases_Nome, H007X2_A147MetodologiaFases_Codigo
               }
               , new Object[] {
               H007X3_AGRID_nRecordCount
               }
            }
         );
         AV106Pgmname = "WWMetodologiaFases";
         /* GeneXus formulas. */
         AV106Pgmname = "WWMetodologiaFases";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_73 ;
      private short nGXsfl_73_idx=1 ;
      private short AV13OrderedBy ;
      private short AV17DynamicFiltersOperator1 ;
      private short AV22DynamicFiltersOperator2 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_73_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV87WWMetodologiaFasesDS_2_Dynamicfiltersoperator1 ;
      private short AV92WWMetodologiaFasesDS_7_Dynamicfiltersoperator2 ;
      private short edtMetodologiaFases_Nome_Titleformat ;
      private short edtMetodologiaFases_Percentual_Titleformat ;
      private short edtMetodologiaFases_Prazo_Titleformat ;
      private short edtMetodologia_Descricao_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int A147MetodologiaFases_Codigo ;
      private int A137Metodologia_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_metodologiafases_nome_Datalistupdateminimumcharacters ;
      private int Ddo_metodologia_descricao_Datalistupdateminimumcharacters ;
      private int edtavTfmetodologiafases_nome_Visible ;
      private int edtavTfmetodologiafases_nome_sel_Visible ;
      private int edtavTfmetodologiafases_percentual_Visible ;
      private int edtavTfmetodologiafases_percentual_to_Visible ;
      private int edtavTfmetodologiafases_prazo_Visible ;
      private int edtavTfmetodologiafases_prazo_to_Visible ;
      private int edtavTfmetodologia_descricao_Visible ;
      private int edtavTfmetodologia_descricao_sel_Visible ;
      private int edtavDdo_metodologiafases_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_metodologiafases_percentualtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_metodologiafases_prazotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_metodologia_descricaotitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV77PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int edtavMetodologiafases_nome1_Visible ;
      private int edtavMetodologia_descricao1_Visible ;
      private int edtavMetodologiafases_nome2_Visible ;
      private int edtavMetodologia_descricao2_Visible ;
      private int AV107GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV78GridCurrentPage ;
      private long AV79GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV69TFMetodologiaFases_Percentual ;
      private decimal AV70TFMetodologiaFases_Percentual_To ;
      private decimal AV81TFMetodologiaFases_Prazo ;
      private decimal AV82TFMetodologiaFases_Prazo_To ;
      private decimal A149MetodologiaFases_Percentual ;
      private decimal A2135MetodologiaFases_Prazo ;
      private decimal AV97WWMetodologiaFasesDS_12_Tfmetodologiafases_percentual ;
      private decimal AV98WWMetodologiaFasesDS_13_Tfmetodologiafases_percentual_to ;
      private decimal AV99WWMetodologiaFasesDS_14_Tfmetodologiafases_prazo ;
      private decimal AV100WWMetodologiaFasesDS_15_Tfmetodologiafases_prazo_to ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_metodologiafases_nome_Activeeventkey ;
      private String Ddo_metodologiafases_nome_Filteredtext_get ;
      private String Ddo_metodologiafases_nome_Selectedvalue_get ;
      private String Ddo_metodologiafases_percentual_Activeeventkey ;
      private String Ddo_metodologiafases_percentual_Filteredtext_get ;
      private String Ddo_metodologiafases_percentual_Filteredtextto_get ;
      private String Ddo_metodologiafases_prazo_Activeeventkey ;
      private String Ddo_metodologiafases_prazo_Filteredtext_get ;
      private String Ddo_metodologiafases_prazo_Filteredtextto_get ;
      private String Ddo_metodologia_descricao_Activeeventkey ;
      private String Ddo_metodologia_descricao_Filteredtext_get ;
      private String Ddo_metodologia_descricao_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_73_idx="0001" ;
      private String AV18MetodologiaFases_Nome1 ;
      private String AV23MetodologiaFases_Nome2 ;
      private String AV65TFMetodologiaFases_Nome ;
      private String AV66TFMetodologiaFases_Nome_Sel ;
      private String AV106Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_metodologiafases_nome_Caption ;
      private String Ddo_metodologiafases_nome_Tooltip ;
      private String Ddo_metodologiafases_nome_Cls ;
      private String Ddo_metodologiafases_nome_Filteredtext_set ;
      private String Ddo_metodologiafases_nome_Selectedvalue_set ;
      private String Ddo_metodologiafases_nome_Dropdownoptionstype ;
      private String Ddo_metodologiafases_nome_Titlecontrolidtoreplace ;
      private String Ddo_metodologiafases_nome_Sortedstatus ;
      private String Ddo_metodologiafases_nome_Filtertype ;
      private String Ddo_metodologiafases_nome_Datalisttype ;
      private String Ddo_metodologiafases_nome_Datalistproc ;
      private String Ddo_metodologiafases_nome_Sortasc ;
      private String Ddo_metodologiafases_nome_Sortdsc ;
      private String Ddo_metodologiafases_nome_Loadingdata ;
      private String Ddo_metodologiafases_nome_Cleanfilter ;
      private String Ddo_metodologiafases_nome_Noresultsfound ;
      private String Ddo_metodologiafases_nome_Searchbuttontext ;
      private String Ddo_metodologiafases_percentual_Caption ;
      private String Ddo_metodologiafases_percentual_Tooltip ;
      private String Ddo_metodologiafases_percentual_Cls ;
      private String Ddo_metodologiafases_percentual_Filteredtext_set ;
      private String Ddo_metodologiafases_percentual_Filteredtextto_set ;
      private String Ddo_metodologiafases_percentual_Dropdownoptionstype ;
      private String Ddo_metodologiafases_percentual_Titlecontrolidtoreplace ;
      private String Ddo_metodologiafases_percentual_Sortedstatus ;
      private String Ddo_metodologiafases_percentual_Filtertype ;
      private String Ddo_metodologiafases_percentual_Sortasc ;
      private String Ddo_metodologiafases_percentual_Sortdsc ;
      private String Ddo_metodologiafases_percentual_Cleanfilter ;
      private String Ddo_metodologiafases_percentual_Rangefilterfrom ;
      private String Ddo_metodologiafases_percentual_Rangefilterto ;
      private String Ddo_metodologiafases_percentual_Searchbuttontext ;
      private String Ddo_metodologiafases_prazo_Caption ;
      private String Ddo_metodologiafases_prazo_Tooltip ;
      private String Ddo_metodologiafases_prazo_Cls ;
      private String Ddo_metodologiafases_prazo_Filteredtext_set ;
      private String Ddo_metodologiafases_prazo_Filteredtextto_set ;
      private String Ddo_metodologiafases_prazo_Dropdownoptionstype ;
      private String Ddo_metodologiafases_prazo_Titlecontrolidtoreplace ;
      private String Ddo_metodologiafases_prazo_Sortedstatus ;
      private String Ddo_metodologiafases_prazo_Filtertype ;
      private String Ddo_metodologiafases_prazo_Sortasc ;
      private String Ddo_metodologiafases_prazo_Sortdsc ;
      private String Ddo_metodologiafases_prazo_Cleanfilter ;
      private String Ddo_metodologiafases_prazo_Rangefilterfrom ;
      private String Ddo_metodologiafases_prazo_Rangefilterto ;
      private String Ddo_metodologiafases_prazo_Searchbuttontext ;
      private String Ddo_metodologia_descricao_Caption ;
      private String Ddo_metodologia_descricao_Tooltip ;
      private String Ddo_metodologia_descricao_Cls ;
      private String Ddo_metodologia_descricao_Filteredtext_set ;
      private String Ddo_metodologia_descricao_Selectedvalue_set ;
      private String Ddo_metodologia_descricao_Dropdownoptionstype ;
      private String Ddo_metodologia_descricao_Titlecontrolidtoreplace ;
      private String Ddo_metodologia_descricao_Sortedstatus ;
      private String Ddo_metodologia_descricao_Filtertype ;
      private String Ddo_metodologia_descricao_Datalisttype ;
      private String Ddo_metodologia_descricao_Datalistproc ;
      private String Ddo_metodologia_descricao_Sortasc ;
      private String Ddo_metodologia_descricao_Sortdsc ;
      private String Ddo_metodologia_descricao_Loadingdata ;
      private String Ddo_metodologia_descricao_Cleanfilter ;
      private String Ddo_metodologia_descricao_Noresultsfound ;
      private String Ddo_metodologia_descricao_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String edtavTfmetodologiafases_nome_Internalname ;
      private String edtavTfmetodologiafases_nome_Jsonclick ;
      private String edtavTfmetodologiafases_nome_sel_Internalname ;
      private String edtavTfmetodologiafases_nome_sel_Jsonclick ;
      private String edtavTfmetodologiafases_percentual_Internalname ;
      private String edtavTfmetodologiafases_percentual_Jsonclick ;
      private String edtavTfmetodologiafases_percentual_to_Internalname ;
      private String edtavTfmetodologiafases_percentual_to_Jsonclick ;
      private String edtavTfmetodologiafases_prazo_Internalname ;
      private String edtavTfmetodologiafases_prazo_Jsonclick ;
      private String edtavTfmetodologiafases_prazo_to_Internalname ;
      private String edtavTfmetodologiafases_prazo_to_Jsonclick ;
      private String edtavTfmetodologia_descricao_Internalname ;
      private String edtavTfmetodologia_descricao_Jsonclick ;
      private String edtavTfmetodologia_descricao_sel_Internalname ;
      private String edtavTfmetodologia_descricao_sel_Jsonclick ;
      private String edtavDdo_metodologiafases_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_metodologiafases_percentualtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_metodologiafases_prazotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_metodologia_descricaotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtMetodologiaFases_Codigo_Internalname ;
      private String A148MetodologiaFases_Nome ;
      private String edtMetodologiaFases_Nome_Internalname ;
      private String edtMetodologiaFases_Percentual_Internalname ;
      private String edtMetodologiaFases_Prazo_Internalname ;
      private String edtMetodologia_Codigo_Internalname ;
      private String edtMetodologia_Descricao_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV88WWMetodologiaFasesDS_3_Metodologiafases_nome1 ;
      private String lV93WWMetodologiaFasesDS_8_Metodologiafases_nome2 ;
      private String lV95WWMetodologiaFasesDS_10_Tfmetodologiafases_nome ;
      private String AV88WWMetodologiaFasesDS_3_Metodologiafases_nome1 ;
      private String AV93WWMetodologiaFasesDS_8_Metodologiafases_nome2 ;
      private String AV96WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel ;
      private String AV95WWMetodologiaFasesDS_10_Tfmetodologiafases_nome ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavMetodologiafases_nome1_Internalname ;
      private String edtavMetodologia_descricao1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavMetodologiafases_nome2_Internalname ;
      private String edtavMetodologia_descricao2_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_metodologiafases_nome_Internalname ;
      private String Ddo_metodologiafases_percentual_Internalname ;
      private String Ddo_metodologiafases_prazo_Internalname ;
      private String Ddo_metodologia_descricao_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtMetodologiaFases_Nome_Title ;
      private String edtMetodologiaFases_Percentual_Title ;
      private String edtMetodologiaFases_Prazo_Title ;
      private String edtMetodologia_Descricao_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String edtMetodologiaFases_Nome_Link ;
      private String edtMetodologia_Descricao_Link ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblMetodologiafasestitle_Internalname ;
      private String lblMetodologiafasestitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavMetodologiafases_nome2_Jsonclick ;
      private String edtavMetodologia_descricao2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavMetodologiafases_nome1_Jsonclick ;
      private String edtavMetodologia_descricao1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_73_fel_idx="0001" ;
      private String ROClassString ;
      private String edtMetodologiaFases_Codigo_Jsonclick ;
      private String edtMetodologiaFases_Nome_Jsonclick ;
      private String edtMetodologiaFases_Percentual_Jsonclick ;
      private String edtMetodologiaFases_Prazo_Jsonclick ;
      private String edtMetodologia_Codigo_Jsonclick ;
      private String edtMetodologia_Descricao_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV20DynamicFiltersEnabled2 ;
      private bool AV31DynamicFiltersIgnoreFirst ;
      private bool AV30DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_metodologiafases_nome_Includesortasc ;
      private bool Ddo_metodologiafases_nome_Includesortdsc ;
      private bool Ddo_metodologiafases_nome_Includefilter ;
      private bool Ddo_metodologiafases_nome_Filterisrange ;
      private bool Ddo_metodologiafases_nome_Includedatalist ;
      private bool Ddo_metodologiafases_percentual_Includesortasc ;
      private bool Ddo_metodologiafases_percentual_Includesortdsc ;
      private bool Ddo_metodologiafases_percentual_Includefilter ;
      private bool Ddo_metodologiafases_percentual_Filterisrange ;
      private bool Ddo_metodologiafases_percentual_Includedatalist ;
      private bool Ddo_metodologiafases_prazo_Includesortasc ;
      private bool Ddo_metodologiafases_prazo_Includesortdsc ;
      private bool Ddo_metodologiafases_prazo_Includefilter ;
      private bool Ddo_metodologiafases_prazo_Filterisrange ;
      private bool Ddo_metodologiafases_prazo_Includedatalist ;
      private bool Ddo_metodologia_descricao_Includesortasc ;
      private bool Ddo_metodologia_descricao_Includesortdsc ;
      private bool Ddo_metodologia_descricao_Includefilter ;
      private bool Ddo_metodologia_descricao_Filterisrange ;
      private bool Ddo_metodologia_descricao_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n2135MetodologiaFases_Prazo ;
      private bool AV90WWMetodologiaFasesDS_5_Dynamicfiltersenabled2 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV32Update_IsBlob ;
      private bool AV33Delete_IsBlob ;
      private bool AV34Display_IsBlob ;
      private String AV16DynamicFiltersSelector1 ;
      private String AV19Metodologia_Descricao1 ;
      private String AV21DynamicFiltersSelector2 ;
      private String AV24Metodologia_Descricao2 ;
      private String AV73TFMetodologia_Descricao ;
      private String AV74TFMetodologia_Descricao_Sel ;
      private String AV67ddo_MetodologiaFases_NomeTitleControlIdToReplace ;
      private String AV71ddo_MetodologiaFases_PercentualTitleControlIdToReplace ;
      private String AV83ddo_MetodologiaFases_PrazoTitleControlIdToReplace ;
      private String AV75ddo_Metodologia_DescricaoTitleControlIdToReplace ;
      private String AV103Update_GXI ;
      private String AV104Delete_GXI ;
      private String AV105Display_GXI ;
      private String A138Metodologia_Descricao ;
      private String lV89WWMetodologiaFasesDS_4_Metodologia_descricao1 ;
      private String lV94WWMetodologiaFasesDS_9_Metodologia_descricao2 ;
      private String lV101WWMetodologiaFasesDS_16_Tfmetodologia_descricao ;
      private String AV86WWMetodologiaFasesDS_1_Dynamicfiltersselector1 ;
      private String AV89WWMetodologiaFasesDS_4_Metodologia_descricao1 ;
      private String AV91WWMetodologiaFasesDS_6_Dynamicfiltersselector2 ;
      private String AV94WWMetodologiaFasesDS_9_Metodologia_descricao2 ;
      private String AV102WWMetodologiaFasesDS_17_Tfmetodologia_descricao_sel ;
      private String AV101WWMetodologiaFasesDS_16_Tfmetodologia_descricao ;
      private String AV32Update ;
      private String AV33Delete ;
      private String AV34Display ;
      private IGxSession AV55Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private IDataStoreProvider pr_default ;
      private String[] H007X2_A138Metodologia_Descricao ;
      private int[] H007X2_A137Metodologia_Codigo ;
      private decimal[] H007X2_A2135MetodologiaFases_Prazo ;
      private bool[] H007X2_n2135MetodologiaFases_Prazo ;
      private decimal[] H007X2_A149MetodologiaFases_Percentual ;
      private String[] H007X2_A148MetodologiaFases_Nome ;
      private int[] H007X2_A147MetodologiaFases_Codigo ;
      private long[] H007X3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV64MetodologiaFases_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV68MetodologiaFases_PercentualTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV80MetodologiaFases_PrazoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV72Metodologia_DescricaoTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV76DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwmetodologiafases__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H007X2( IGxContext context ,
                                             String AV86WWMetodologiaFasesDS_1_Dynamicfiltersselector1 ,
                                             short AV87WWMetodologiaFasesDS_2_Dynamicfiltersoperator1 ,
                                             String AV88WWMetodologiaFasesDS_3_Metodologiafases_nome1 ,
                                             String AV89WWMetodologiaFasesDS_4_Metodologia_descricao1 ,
                                             bool AV90WWMetodologiaFasesDS_5_Dynamicfiltersenabled2 ,
                                             String AV91WWMetodologiaFasesDS_6_Dynamicfiltersselector2 ,
                                             short AV92WWMetodologiaFasesDS_7_Dynamicfiltersoperator2 ,
                                             String AV93WWMetodologiaFasesDS_8_Metodologiafases_nome2 ,
                                             String AV94WWMetodologiaFasesDS_9_Metodologia_descricao2 ,
                                             String AV96WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel ,
                                             String AV95WWMetodologiaFasesDS_10_Tfmetodologiafases_nome ,
                                             decimal AV97WWMetodologiaFasesDS_12_Tfmetodologiafases_percentual ,
                                             decimal AV98WWMetodologiaFasesDS_13_Tfmetodologiafases_percentual_to ,
                                             decimal AV99WWMetodologiaFasesDS_14_Tfmetodologiafases_prazo ,
                                             decimal AV100WWMetodologiaFasesDS_15_Tfmetodologiafases_prazo_to ,
                                             String AV102WWMetodologiaFasesDS_17_Tfmetodologia_descricao_sel ,
                                             String AV101WWMetodologiaFasesDS_16_Tfmetodologia_descricao ,
                                             String A148MetodologiaFases_Nome ,
                                             String A138Metodologia_Descricao ,
                                             decimal A149MetodologiaFases_Percentual ,
                                             decimal A2135MetodologiaFases_Prazo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [21] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T2.[Metodologia_Descricao], T1.[Metodologia_Codigo], T1.[MetodologiaFases_Prazo], T1.[MetodologiaFases_Percentual], T1.[MetodologiaFases_Nome], T1.[MetodologiaFases_Codigo]";
         sFromString = " FROM ([MetodologiaFases] T1 WITH (NOLOCK) INNER JOIN [Metodologia] T2 WITH (NOLOCK) ON T2.[Metodologia_Codigo] = T1.[Metodologia_Codigo])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV86WWMetodologiaFasesDS_1_Dynamicfiltersselector1, "METODOLOGIAFASES_NOME") == 0 ) && ( AV87WWMetodologiaFasesDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWMetodologiaFasesDS_3_Metodologiafases_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Nome] like @lV88WWMetodologiaFasesDS_3_Metodologiafases_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Nome] like @lV88WWMetodologiaFasesDS_3_Metodologiafases_nome1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV86WWMetodologiaFasesDS_1_Dynamicfiltersselector1, "METODOLOGIAFASES_NOME") == 0 ) && ( AV87WWMetodologiaFasesDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWMetodologiaFasesDS_3_Metodologiafases_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Nome] like '%' + @lV88WWMetodologiaFasesDS_3_Metodologiafases_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Nome] like '%' + @lV88WWMetodologiaFasesDS_3_Metodologiafases_nome1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV86WWMetodologiaFasesDS_1_Dynamicfiltersselector1, "METODOLOGIA_DESCRICAO") == 0 ) && ( AV87WWMetodologiaFasesDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWMetodologiaFasesDS_4_Metodologia_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] like @lV89WWMetodologiaFasesDS_4_Metodologia_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Metodologia_Descricao] like @lV89WWMetodologiaFasesDS_4_Metodologia_descricao1)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV86WWMetodologiaFasesDS_1_Dynamicfiltersselector1, "METODOLOGIA_DESCRICAO") == 0 ) && ( AV87WWMetodologiaFasesDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWMetodologiaFasesDS_4_Metodologia_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] like '%' + @lV89WWMetodologiaFasesDS_4_Metodologia_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Metodologia_Descricao] like '%' + @lV89WWMetodologiaFasesDS_4_Metodologia_descricao1)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV90WWMetodologiaFasesDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV91WWMetodologiaFasesDS_6_Dynamicfiltersselector2, "METODOLOGIAFASES_NOME") == 0 ) && ( AV92WWMetodologiaFasesDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWMetodologiaFasesDS_8_Metodologiafases_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Nome] like @lV93WWMetodologiaFasesDS_8_Metodologiafases_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Nome] like @lV93WWMetodologiaFasesDS_8_Metodologiafases_nome2)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV90WWMetodologiaFasesDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV91WWMetodologiaFasesDS_6_Dynamicfiltersselector2, "METODOLOGIAFASES_NOME") == 0 ) && ( AV92WWMetodologiaFasesDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWMetodologiaFasesDS_8_Metodologiafases_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Nome] like '%' + @lV93WWMetodologiaFasesDS_8_Metodologiafases_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Nome] like '%' + @lV93WWMetodologiaFasesDS_8_Metodologiafases_nome2)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV90WWMetodologiaFasesDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV91WWMetodologiaFasesDS_6_Dynamicfiltersselector2, "METODOLOGIA_DESCRICAO") == 0 ) && ( AV92WWMetodologiaFasesDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWMetodologiaFasesDS_9_Metodologia_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] like @lV94WWMetodologiaFasesDS_9_Metodologia_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Metodologia_Descricao] like @lV94WWMetodologiaFasesDS_9_Metodologia_descricao2)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV90WWMetodologiaFasesDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV91WWMetodologiaFasesDS_6_Dynamicfiltersselector2, "METODOLOGIA_DESCRICAO") == 0 ) && ( AV92WWMetodologiaFasesDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWMetodologiaFasesDS_9_Metodologia_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] like '%' + @lV94WWMetodologiaFasesDS_9_Metodologia_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Metodologia_Descricao] like '%' + @lV94WWMetodologiaFasesDS_9_Metodologia_descricao2)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV96WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV95WWMetodologiaFasesDS_10_Tfmetodologiafases_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Nome] like @lV95WWMetodologiaFasesDS_10_Tfmetodologiafases_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Nome] like @lV95WWMetodologiaFasesDS_10_Tfmetodologiafases_nome)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Nome] = @AV96WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Nome] = @AV96WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV97WWMetodologiaFasesDS_12_Tfmetodologiafases_percentual) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Percentual] >= @AV97WWMetodologiaFasesDS_12_Tfmetodologiafases_percentual)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Percentual] >= @AV97WWMetodologiaFasesDS_12_Tfmetodologiafases_percentual)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV98WWMetodologiaFasesDS_13_Tfmetodologiafases_percentual_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Percentual] <= @AV98WWMetodologiaFasesDS_13_Tfmetodologiafases_percentual_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Percentual] <= @AV98WWMetodologiaFasesDS_13_Tfmetodologiafases_percentual_to)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV99WWMetodologiaFasesDS_14_Tfmetodologiafases_prazo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Prazo] >= @AV99WWMetodologiaFasesDS_14_Tfmetodologiafases_prazo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Prazo] >= @AV99WWMetodologiaFasesDS_14_Tfmetodologiafases_prazo)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV100WWMetodologiaFasesDS_15_Tfmetodologiafases_prazo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Prazo] <= @AV100WWMetodologiaFasesDS_15_Tfmetodologiafases_prazo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Prazo] <= @AV100WWMetodologiaFasesDS_15_Tfmetodologiafases_prazo_to)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV102WWMetodologiaFasesDS_17_Tfmetodologia_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWMetodologiaFasesDS_16_Tfmetodologia_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] like @lV101WWMetodologiaFasesDS_16_Tfmetodologia_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Metodologia_Descricao] like @lV101WWMetodologiaFasesDS_16_Tfmetodologia_descricao)";
            }
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWMetodologiaFasesDS_17_Tfmetodologia_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] = @AV102WWMetodologiaFasesDS_17_Tfmetodologia_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Metodologia_Descricao] = @AV102WWMetodologiaFasesDS_17_Tfmetodologia_descricao_sel)";
            }
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[MetodologiaFases_Nome]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[MetodologiaFases_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[MetodologiaFases_Percentual]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[MetodologiaFases_Percentual] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[MetodologiaFases_Prazo]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[MetodologiaFases_Prazo] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Metodologia_Descricao]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Metodologia_Descricao] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[MetodologiaFases_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H007X3( IGxContext context ,
                                             String AV86WWMetodologiaFasesDS_1_Dynamicfiltersselector1 ,
                                             short AV87WWMetodologiaFasesDS_2_Dynamicfiltersoperator1 ,
                                             String AV88WWMetodologiaFasesDS_3_Metodologiafases_nome1 ,
                                             String AV89WWMetodologiaFasesDS_4_Metodologia_descricao1 ,
                                             bool AV90WWMetodologiaFasesDS_5_Dynamicfiltersenabled2 ,
                                             String AV91WWMetodologiaFasesDS_6_Dynamicfiltersselector2 ,
                                             short AV92WWMetodologiaFasesDS_7_Dynamicfiltersoperator2 ,
                                             String AV93WWMetodologiaFasesDS_8_Metodologiafases_nome2 ,
                                             String AV94WWMetodologiaFasesDS_9_Metodologia_descricao2 ,
                                             String AV96WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel ,
                                             String AV95WWMetodologiaFasesDS_10_Tfmetodologiafases_nome ,
                                             decimal AV97WWMetodologiaFasesDS_12_Tfmetodologiafases_percentual ,
                                             decimal AV98WWMetodologiaFasesDS_13_Tfmetodologiafases_percentual_to ,
                                             decimal AV99WWMetodologiaFasesDS_14_Tfmetodologiafases_prazo ,
                                             decimal AV100WWMetodologiaFasesDS_15_Tfmetodologiafases_prazo_to ,
                                             String AV102WWMetodologiaFasesDS_17_Tfmetodologia_descricao_sel ,
                                             String AV101WWMetodologiaFasesDS_16_Tfmetodologia_descricao ,
                                             String A148MetodologiaFases_Nome ,
                                             String A138Metodologia_Descricao ,
                                             decimal A149MetodologiaFases_Percentual ,
                                             decimal A2135MetodologiaFases_Prazo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [16] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([MetodologiaFases] T1 WITH (NOLOCK) INNER JOIN [Metodologia] T2 WITH (NOLOCK) ON T2.[Metodologia_Codigo] = T1.[Metodologia_Codigo])";
         if ( ( StringUtil.StrCmp(AV86WWMetodologiaFasesDS_1_Dynamicfiltersselector1, "METODOLOGIAFASES_NOME") == 0 ) && ( AV87WWMetodologiaFasesDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWMetodologiaFasesDS_3_Metodologiafases_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Nome] like @lV88WWMetodologiaFasesDS_3_Metodologiafases_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Nome] like @lV88WWMetodologiaFasesDS_3_Metodologiafases_nome1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV86WWMetodologiaFasesDS_1_Dynamicfiltersselector1, "METODOLOGIAFASES_NOME") == 0 ) && ( AV87WWMetodologiaFasesDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWMetodologiaFasesDS_3_Metodologiafases_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Nome] like '%' + @lV88WWMetodologiaFasesDS_3_Metodologiafases_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Nome] like '%' + @lV88WWMetodologiaFasesDS_3_Metodologiafases_nome1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV86WWMetodologiaFasesDS_1_Dynamicfiltersselector1, "METODOLOGIA_DESCRICAO") == 0 ) && ( AV87WWMetodologiaFasesDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWMetodologiaFasesDS_4_Metodologia_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] like @lV89WWMetodologiaFasesDS_4_Metodologia_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Metodologia_Descricao] like @lV89WWMetodologiaFasesDS_4_Metodologia_descricao1)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV86WWMetodologiaFasesDS_1_Dynamicfiltersselector1, "METODOLOGIA_DESCRICAO") == 0 ) && ( AV87WWMetodologiaFasesDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWMetodologiaFasesDS_4_Metodologia_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] like '%' + @lV89WWMetodologiaFasesDS_4_Metodologia_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Metodologia_Descricao] like '%' + @lV89WWMetodologiaFasesDS_4_Metodologia_descricao1)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV90WWMetodologiaFasesDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV91WWMetodologiaFasesDS_6_Dynamicfiltersselector2, "METODOLOGIAFASES_NOME") == 0 ) && ( AV92WWMetodologiaFasesDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWMetodologiaFasesDS_8_Metodologiafases_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Nome] like @lV93WWMetodologiaFasesDS_8_Metodologiafases_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Nome] like @lV93WWMetodologiaFasesDS_8_Metodologiafases_nome2)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV90WWMetodologiaFasesDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV91WWMetodologiaFasesDS_6_Dynamicfiltersselector2, "METODOLOGIAFASES_NOME") == 0 ) && ( AV92WWMetodologiaFasesDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWMetodologiaFasesDS_8_Metodologiafases_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Nome] like '%' + @lV93WWMetodologiaFasesDS_8_Metodologiafases_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Nome] like '%' + @lV93WWMetodologiaFasesDS_8_Metodologiafases_nome2)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV90WWMetodologiaFasesDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV91WWMetodologiaFasesDS_6_Dynamicfiltersselector2, "METODOLOGIA_DESCRICAO") == 0 ) && ( AV92WWMetodologiaFasesDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWMetodologiaFasesDS_9_Metodologia_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] like @lV94WWMetodologiaFasesDS_9_Metodologia_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Metodologia_Descricao] like @lV94WWMetodologiaFasesDS_9_Metodologia_descricao2)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV90WWMetodologiaFasesDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV91WWMetodologiaFasesDS_6_Dynamicfiltersselector2, "METODOLOGIA_DESCRICAO") == 0 ) && ( AV92WWMetodologiaFasesDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWMetodologiaFasesDS_9_Metodologia_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] like '%' + @lV94WWMetodologiaFasesDS_9_Metodologia_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Metodologia_Descricao] like '%' + @lV94WWMetodologiaFasesDS_9_Metodologia_descricao2)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV96WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV95WWMetodologiaFasesDS_10_Tfmetodologiafases_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Nome] like @lV95WWMetodologiaFasesDS_10_Tfmetodologiafases_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Nome] like @lV95WWMetodologiaFasesDS_10_Tfmetodologiafases_nome)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Nome] = @AV96WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Nome] = @AV96WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV97WWMetodologiaFasesDS_12_Tfmetodologiafases_percentual) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Percentual] >= @AV97WWMetodologiaFasesDS_12_Tfmetodologiafases_percentual)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Percentual] >= @AV97WWMetodologiaFasesDS_12_Tfmetodologiafases_percentual)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV98WWMetodologiaFasesDS_13_Tfmetodologiafases_percentual_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Percentual] <= @AV98WWMetodologiaFasesDS_13_Tfmetodologiafases_percentual_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Percentual] <= @AV98WWMetodologiaFasesDS_13_Tfmetodologiafases_percentual_to)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV99WWMetodologiaFasesDS_14_Tfmetodologiafases_prazo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Prazo] >= @AV99WWMetodologiaFasesDS_14_Tfmetodologiafases_prazo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Prazo] >= @AV99WWMetodologiaFasesDS_14_Tfmetodologiafases_prazo)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV100WWMetodologiaFasesDS_15_Tfmetodologiafases_prazo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Prazo] <= @AV100WWMetodologiaFasesDS_15_Tfmetodologiafases_prazo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Prazo] <= @AV100WWMetodologiaFasesDS_15_Tfmetodologiafases_prazo_to)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV102WWMetodologiaFasesDS_17_Tfmetodologia_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWMetodologiaFasesDS_16_Tfmetodologia_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] like @lV101WWMetodologiaFasesDS_16_Tfmetodologia_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Metodologia_Descricao] like @lV101WWMetodologiaFasesDS_16_Tfmetodologia_descricao)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWMetodologiaFasesDS_17_Tfmetodologia_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] = @AV102WWMetodologiaFasesDS_17_Tfmetodologia_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Metodologia_Descricao] = @AV102WWMetodologiaFasesDS_17_Tfmetodologia_descricao_sel)";
            }
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H007X2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (decimal)dynConstraints[11] , (decimal)dynConstraints[12] , (decimal)dynConstraints[13] , (decimal)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (decimal)dynConstraints[19] , (decimal)dynConstraints[20] , (short)dynConstraints[21] , (bool)dynConstraints[22] );
               case 1 :
                     return conditional_H007X3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (decimal)dynConstraints[11] , (decimal)dynConstraints[12] , (decimal)dynConstraints[13] , (decimal)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (decimal)dynConstraints[19] , (decimal)dynConstraints[20] , (short)dynConstraints[21] , (bool)dynConstraints[22] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH007X2 ;
          prmH007X2 = new Object[] {
          new Object[] {"@lV88WWMetodologiaFasesDS_3_Metodologiafases_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV88WWMetodologiaFasesDS_3_Metodologiafases_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV89WWMetodologiaFasesDS_4_Metodologia_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV89WWMetodologiaFasesDS_4_Metodologia_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV93WWMetodologiaFasesDS_8_Metodologiafases_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV93WWMetodologiaFasesDS_8_Metodologiafases_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV94WWMetodologiaFasesDS_9_Metodologia_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV94WWMetodologiaFasesDS_9_Metodologia_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV95WWMetodologiaFasesDS_10_Tfmetodologiafases_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV96WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV97WWMetodologiaFasesDS_12_Tfmetodologiafases_percentual",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV98WWMetodologiaFasesDS_13_Tfmetodologiafases_percentual_to",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV99WWMetodologiaFasesDS_14_Tfmetodologiafases_prazo",SqlDbType.Decimal,7,2} ,
          new Object[] {"@AV100WWMetodologiaFasesDS_15_Tfmetodologiafases_prazo_to",SqlDbType.Decimal,7,2} ,
          new Object[] {"@lV101WWMetodologiaFasesDS_16_Tfmetodologia_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV102WWMetodologiaFasesDS_17_Tfmetodologia_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH007X3 ;
          prmH007X3 = new Object[] {
          new Object[] {"@lV88WWMetodologiaFasesDS_3_Metodologiafases_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV88WWMetodologiaFasesDS_3_Metodologiafases_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV89WWMetodologiaFasesDS_4_Metodologia_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV89WWMetodologiaFasesDS_4_Metodologia_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV93WWMetodologiaFasesDS_8_Metodologiafases_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV93WWMetodologiaFasesDS_8_Metodologiafases_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV94WWMetodologiaFasesDS_9_Metodologia_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV94WWMetodologiaFasesDS_9_Metodologia_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV95WWMetodologiaFasesDS_10_Tfmetodologiafases_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV96WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV97WWMetodologiaFasesDS_12_Tfmetodologiafases_percentual",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV98WWMetodologiaFasesDS_13_Tfmetodologiafases_percentual_to",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV99WWMetodologiaFasesDS_14_Tfmetodologiafases_prazo",SqlDbType.Decimal,7,2} ,
          new Object[] {"@AV100WWMetodologiaFasesDS_15_Tfmetodologiafases_prazo_to",SqlDbType.Decimal,7,2} ,
          new Object[] {"@lV101WWMetodologiaFasesDS_16_Tfmetodologia_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV102WWMetodologiaFasesDS_17_Tfmetodologia_descricao_sel",SqlDbType.VarChar,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H007X2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH007X2,11,0,true,false )
             ,new CursorDef("H007X3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH007X3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 50) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[31]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[32]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[33]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[34]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[26]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[27]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[28]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[29]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                return;
       }
    }

 }

}
