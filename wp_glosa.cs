/*
               File: WP_Glosa
        Description: Glosa
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:23:40.50
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_glosa : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_glosa( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_glosa( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_AreaTrabalho_Codigo ,
                           int aP1_ContagemResultado_GlsUser ,
                           out bool aP2_Confirmado )
      {
         this.AV11AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV12ContagemResultado_GlsUser = aP1_ContagemResultado_GlsUser;
         this.AV19Confirmado = false ;
         executePrivate();
         aP2_Confirmado=this.AV19Confirmado;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavSistema_codigo = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxSuggest"+"_"+"vCONTAGEMRESULTADO_CODIGO") == 0 )
            {
               AV11AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11AreaTrabalho_Codigo), 6, 0)));
               AV21Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Sistema_Codigo), 6, 0)));
               AV20ContagemResultado_DemandaFM = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContagemResultado_DemandaFM", AV20ContagemResultado_DemandaFM);
               A457ContagemResultado_Demanda = GetNextPar( );
               n457ContagemResultado_Demanda = false;
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXSGVvCONTAGEMRESULTADO_CODIGOHF0( AV11AreaTrabalho_Codigo, AV21Sistema_Codigo, AV20ContagemResultado_DemandaFM, A457ContagemResultado_Demanda) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxSuggest"+"_"+"vCONTAGEMRESULTADO_CODIGO") == 0 )
            {
               AV11AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11AreaTrabalho_Codigo), 6, 0)));
               AV21Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Sistema_Codigo), 6, 0)));
               AV20ContagemResultado_DemandaFM = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContagemResultado_DemandaFM", AV20ContagemResultado_DemandaFM);
               A457ContagemResultado_Demanda = GetNextPar( );
               n457ContagemResultado_Demanda = false;
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXSGVvCONTAGEMRESULTADO_CODIGOHF0( AV11AreaTrabalho_Codigo, AV21Sistema_Codigo, AV20ContagemResultado_DemandaFM, A457ContagemResultado_Demanda) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxHideCode"+"_"+"vCONTAGEMRESULTADO_CODIGO") == 0 )
            {
               AV11AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11AreaTrabalho_Codigo), 6, 0)));
               AV21Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Sistema_Codigo), 6, 0)));
               AV20ContagemResultado_DemandaFM = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContagemResultado_DemandaFM", AV20ContagemResultado_DemandaFM);
               hV8ContagemResultado_Codigo = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXHCVvCONTAGEMRESULTADO_CODIGOHF2( AV11AreaTrabalho_Codigo, AV21Sistema_Codigo, AV20ContagemResultado_DemandaFM, hV8ContagemResultado_Codigo) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vSISTEMA_CODIGO") == 0 )
            {
               AV11AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11AreaTrabalho_Codigo), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvSISTEMA_CODIGOHF2( AV11AreaTrabalho_Codigo) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV11AreaTrabalho_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11AreaTrabalho_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV12ContagemResultado_GlsUser = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ContagemResultado_GlsUser", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12ContagemResultado_GlsUser), 6, 0)));
                  AV19Confirmado = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Confirmado", AV19Confirmado);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAHF2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTHF2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206216234053");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_glosa.aspx") + "?" + UrlEncode("" +AV11AreaTrabalho_Codigo) + "," + UrlEncode("" +AV12ContagemResultado_GlsUser) + "," + UrlEncode(StringUtil.BoolToStr(AV19Confirmado))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_STATUSDMN", StringUtil.RTrim( A484ContagemResultado_StatusDmn));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_GLSDATA", context.localUtil.DToC( A1049ContagemResultado_GlsData, 0, "/"));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_GLSDESCRICAO", A1050ContagemResultado_GlsDescricao);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_GLSVALOR", StringUtil.LTrim( StringUtil.NToC( A1051ContagemResultado_GlsValor, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_GLSUSER", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12ContagemResultado_GlsUser), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "vCONFIRMADO", AV19Confirmado);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFFINAL", StringUtil.LTrim( StringUtil.NToC( A574ContagemResultado_PFFinal, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_VALORPF", StringUtil.LTrim( StringUtil.NToC( A512ContagemResultado_ValorPF, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_VALORFINAL", StringUtil.LTrim( StringUtil.NToC( A606ContagemResultado_ValorFinal, 18, 5, ",", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEHF2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTHF2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_glosa.aspx") + "?" + UrlEncode("" +AV11AreaTrabalho_Codigo) + "," + UrlEncode("" +AV12ContagemResultado_GlsUser) + "," + UrlEncode(StringUtil.BoolToStr(AV19Confirmado)) ;
      }

      public override String GetPgmname( )
      {
         return "WP_Glosa" ;
      }

      public override String GetPgmdesc( )
      {
         return "Glosa" ;
      }

      protected void WBHF0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_HF2( true) ;
         }
         else
         {
            wb_table1_2_HF2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_HF2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtavAreatrabalho_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11AreaTrabalho_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(AV11AreaTrabalho_Codigo), "ZZZZZ9"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAreatrabalho_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavAreatrabalho_codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_Glosa.htm");
         }
         wbLoad = true;
      }

      protected void STARTHF2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Glosa", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPHF0( ) ;
      }

      protected void WSHF2( )
      {
         STARTHF2( ) ;
         EVTHF2( ) ;
      }

      protected void EVTHF2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11HF2 */
                              E11HF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VCONTAGEMRESULTADO_CODIGO.ISVALID") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12HF2 */
                              E12HF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ENTER'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13HF2 */
                              E13HF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14HF2 */
                              E14HF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E13HF2 */
                                    E13HF2 ();
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEHF2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAHF2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavSistema_codigo.Name = "vSISTEMA_CODIGO";
            dynavSistema_codigo.WebTags = "";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = dynavSistema_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXSGVvCONTAGEMRESULTADO_CODIGOHF0( int AV11AreaTrabalho_Codigo ,
                                                        int AV21Sistema_Codigo ,
                                                        String AV20ContagemResultado_DemandaFM ,
                                                        String A457ContagemResultado_Demanda )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXSGVvCONTAGEMRESULTADO_CODIGO_dataHF0( AV11AreaTrabalho_Codigo, AV21Sistema_Codigo, AV20ContagemResultado_DemandaFM, A457ContagemResultado_Demanda) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXSGVvCONTAGEMRESULTADO_CODIGO_dataHF0( int AV11AreaTrabalho_Codigo ,
                                                             int AV21Sistema_Codigo ,
                                                             String AV20ContagemResultado_DemandaFM ,
                                                             String A457ContagemResultado_Demanda )
      {
         l457ContagemResultado_Demanda = StringUtil.Concat( StringUtil.RTrim( A457ContagemResultado_Demanda), "%", "");
         n457ContagemResultado_Demanda = false;
         /* Using cursor H00HF2 */
         pr_default.execute(0, new Object[] {l457ContagemResultado_Demanda, AV11AreaTrabalho_Codigo, AV21Sistema_Codigo, AV20ContagemResultado_DemandaFM});
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(H00HF2_A457ContagemResultado_Demanda[0]);
            gxdynajaxctrldescr.Add(H00HF2_A457ContagemResultado_Demanda[0]);
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void GXHCVvCONTAGEMRESULTADO_CODIGOHF2( int AV11AreaTrabalho_Codigo ,
                                                        int AV21Sistema_Codigo ,
                                                        String AV20ContagemResultado_DemandaFM ,
                                                        String A457ContagemResultado_Demanda )
      {
         /* Using cursor H00HF3 */
         pr_default.execute(1, new Object[] {n457ContagemResultado_Demanda, A457ContagemResultado_Demanda, AV11AreaTrabalho_Codigo, AV21Sistema_Codigo, AV20ContagemResultado_DemandaFM});
         gxhchits = 0;
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxhchits = (short)(gxhchits+1);
            if ( gxhchits > 1 )
            {
               if (true) break;
            }
            A457ContagemResultado_Demanda = H00HF3_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = H00HF3_n457ContagemResultado_Demanda[0];
            A52Contratada_AreaTrabalhoCod = H00HF3_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00HF3_n52Contratada_AreaTrabalhoCod[0];
            A489ContagemResultado_SistemaCod = H00HF3_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = H00HF3_n489ContagemResultado_SistemaCod[0];
            A493ContagemResultado_DemandaFM = H00HF3_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = H00HF3_n493ContagemResultado_DemandaFM[0];
            A456ContagemResultado_Codigo = H00HF3_A456ContagemResultado_Codigo[0];
            pr_default.readNext(1);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( gxhchits > 1 )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("\"ambiguousck\"");
         }
         if ( gxhchits == 0 )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(1);
      }

      protected void GXDLVvSISTEMA_CODIGOHF2( int AV11AreaTrabalho_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvSISTEMA_CODIGO_dataHF2( AV11AreaTrabalho_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvSISTEMA_CODIGO_htmlHF2( int AV11AreaTrabalho_Codigo )
      {
         int gxdynajaxvalue ;
         GXDLVvSISTEMA_CODIGO_dataHF2( AV11AreaTrabalho_Codigo) ;
         gxdynajaxindex = 1;
         dynavSistema_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavSistema_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavSistema_codigo.ItemCount > 0 )
         {
            AV21Sistema_Codigo = (int)(NumberUtil.Val( dynavSistema_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21Sistema_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Sistema_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvSISTEMA_CODIGO_dataHF2( int AV11AreaTrabalho_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhum");
         /* Using cursor H00HF4 */
         pr_default.execute(2, new Object[] {AV11AreaTrabalho_Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00HF4_A127Sistema_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00HF4_A129Sistema_Sigla[0]));
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavSistema_codigo.ItemCount > 0 )
         {
            AV21Sistema_Codigo = (int)(NumberUtil.Val( dynavSistema_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21Sistema_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Sistema_Codigo), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFHF2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavContagemresultado_valorfinal_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_valorfinal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_valorfinal_Enabled), 5, 0)));
      }

      protected void RFHF2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E14HF2 */
            E14HF2 ();
            WBHF0( ) ;
         }
      }

      protected void STRUPHF0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavContagemresultado_valorfinal_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_valorfinal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_valorfinal_Enabled), 5, 0)));
         GXVvSISTEMA_CODIGO_htmlHF2( AV11AreaTrabalho_Codigo) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11HF2 */
         E11HF2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            dynavSistema_codigo.CurrentValue = cgiGet( dynavSistema_codigo_Internalname);
            AV21Sistema_Codigo = (int)(NumberUtil.Val( cgiGet( dynavSistema_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Sistema_Codigo), 6, 0)));
            AV20ContagemResultado_DemandaFM = cgiGet( edtavContagemresultado_demandafm_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContagemResultado_DemandaFM", AV20ContagemResultado_DemandaFM);
            hV8ContagemResultado_Codigo = StringUtil.Upper( cgiGet( edtavContagemresultado_codigo_Internalname));
            if ( String.IsNullOrEmpty(StringUtil.RTrim( hV8ContagemResultado_Codigo)) )
            {
               AV8ContagemResultado_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8ContagemResultado_Codigo), 6, 0)));
            }
            else
            {
               A457ContagemResultado_Demanda = hV8ContagemResultado_Codigo;
               n457ContagemResultado_Demanda = false;
               /* Using cursor H00HF5 */
               pr_default.execute(3, new Object[] {n457ContagemResultado_Demanda, A457ContagemResultado_Demanda, AV11AreaTrabalho_Codigo, AV21Sistema_Codigo, AV20ContagemResultado_DemandaFM});
               AV8ContagemResultado_Codigo = H00HF5_A456ContagemResultado_Codigo[0];
               if ( ! ( (pr_default.getStatus(3) == 101) ) )
               {
                  pr_default.readNext(3);
                  if ( ! ( (pr_default.getStatus(3) == 101) ) )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_ambiguousck", new   object[]  {"N� Refer�ncia"}), 1, "vCONTAGEMRESULTADO_CODIGO");
                     GX_FocusControl = edtavContagemresultado_codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
               }
               pr_default.close(3);
            }
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "hV8ContagemResultado_Codigo", hV8ContagemResultado_Codigo);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_valorfinal_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_valorfinal_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMRESULTADO_VALORFINAL");
               GX_FocusControl = edtavContagemresultado_valorfinal_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV14ContagemResultado_ValorFinal = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ContagemResultado_ValorFinal", StringUtil.LTrim( StringUtil.Str( AV14ContagemResultado_ValorFinal, 18, 5)));
            }
            else
            {
               AV14ContagemResultado_ValorFinal = context.localUtil.CToN( cgiGet( edtavContagemresultado_valorfinal_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ContagemResultado_ValorFinal", StringUtil.LTrim( StringUtil.Str( AV14ContagemResultado_ValorFinal, 18, 5)));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_glsdata_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data da glosa"}), 1, "vCONTAGEMRESULTADO_GLSDATA");
               GX_FocusControl = edtavContagemresultado_glsdata_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV5ContagemResultado_GlsData = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5ContagemResultado_GlsData", context.localUtil.Format(AV5ContagemResultado_GlsData, "99/99/99"));
            }
            else
            {
               AV5ContagemResultado_GlsData = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_glsdata_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5ContagemResultado_GlsData", context.localUtil.Format(AV5ContagemResultado_GlsData, "99/99/99"));
            }
            AV6ContagemResultado_GlsDescricao = cgiGet( edtavContagemresultado_glsdescricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6ContagemResultado_GlsDescricao", AV6ContagemResultado_GlsDescricao);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_glsvalor_Internalname), ",", ".") < -99999999.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_glsvalor_Internalname), ",", ".") > 999999999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMRESULTADO_GLSVALOR");
               GX_FocusControl = edtavContagemresultado_glsvalor_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV7ContagemResultado_GlsValor = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContagemResultado_GlsValor", StringUtil.LTrim( StringUtil.Str( AV7ContagemResultado_GlsValor, 12, 2)));
            }
            else
            {
               AV7ContagemResultado_GlsValor = context.localUtil.CToN( cgiGet( edtavContagemresultado_glsvalor_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContagemResultado_GlsValor", StringUtil.LTrim( StringUtil.Str( AV7ContagemResultado_GlsValor, 12, 2)));
            }
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            GXVvSISTEMA_CODIGO_htmlHF2( AV11AreaTrabalho_Codigo) ;
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11HF2 */
         E11HF2 ();
         if (returnInSub) return;
      }

      protected void E11HF2( )
      {
         /* Start Routine */
         edtavAreatrabalho_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAreatrabalho_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAreatrabalho_codigo_Visible), 5, 0)));
      }

      protected void E12HF2( )
      {
         /* Contagemresultado_codigo_Isvalid Routine */
         AV24GXLvl10 = 0;
         /* Using cursor H00HF6 */
         pr_default.execute(4, new Object[] {AV8ContagemResultado_Codigo});
         while ( (pr_default.getStatus(4) != 101) )
         {
            A484ContagemResultado_StatusDmn = H00HF6_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00HF6_n484ContagemResultado_StatusDmn[0];
            A1049ContagemResultado_GlsData = H00HF6_A1049ContagemResultado_GlsData[0];
            n1049ContagemResultado_GlsData = H00HF6_n1049ContagemResultado_GlsData[0];
            A1050ContagemResultado_GlsDescricao = H00HF6_A1050ContagemResultado_GlsDescricao[0];
            n1050ContagemResultado_GlsDescricao = H00HF6_n1050ContagemResultado_GlsDescricao[0];
            A1051ContagemResultado_GlsValor = H00HF6_A1051ContagemResultado_GlsValor[0];
            n1051ContagemResultado_GlsValor = H00HF6_n1051ContagemResultado_GlsValor[0];
            A456ContagemResultado_Codigo = H00HF6_A456ContagemResultado_Codigo[0];
            A512ContagemResultado_ValorPF = H00HF6_A512ContagemResultado_ValorPF[0];
            n512ContagemResultado_ValorPF = H00HF6_n512ContagemResultado_ValorPF[0];
            GXt_decimal1 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            A574ContagemResultado_PFFinal = GXt_decimal1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A574ContagemResultado_PFFinal", StringUtil.LTrim( StringUtil.Str( A574ContagemResultado_PFFinal, 14, 5)));
            A606ContagemResultado_ValorFinal = (decimal)(A574ContagemResultado_PFFinal*A512ContagemResultado_ValorPF);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A606ContagemResultado_ValorFinal", StringUtil.LTrim( StringUtil.Str( A606ContagemResultado_ValorFinal, 18, 5)));
            AV24GXLvl10 = 1;
            if ( ! ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "H") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "O") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "L") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "P") == 0 ) ) )
            {
               GX_msglist.addItem("Demanda n�o homologada!");
            }
            AV14ContagemResultado_ValorFinal = A606ContagemResultado_ValorFinal;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ContagemResultado_ValorFinal", StringUtil.LTrim( StringUtil.Str( AV14ContagemResultado_ValorFinal, 18, 5)));
            AV5ContagemResultado_GlsData = A1049ContagemResultado_GlsData;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5ContagemResultado_GlsData", context.localUtil.Format(AV5ContagemResultado_GlsData, "99/99/99"));
            AV6ContagemResultado_GlsDescricao = A1050ContagemResultado_GlsDescricao;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6ContagemResultado_GlsDescricao", AV6ContagemResultado_GlsDescricao);
            AV7ContagemResultado_GlsValor = A1051ContagemResultado_GlsValor;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContagemResultado_GlsValor", StringUtil.LTrim( StringUtil.Str( AV7ContagemResultado_GlsValor, 12, 2)));
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(4);
         if ( AV24GXLvl10 == 0 )
         {
            GX_msglist.addItem("N� de demanda inexistente");
         }
      }

      protected void E13HF2( )
      {
         /* 'Enter' Routine */
         AV25GXLvl33 = 0;
         /* Using cursor H00HF7 */
         pr_default.execute(5, new Object[] {AV8ContagemResultado_Codigo});
         while ( (pr_default.getStatus(5) != 101) )
         {
            A456ContagemResultado_Codigo = H00HF7_A456ContagemResultado_Codigo[0];
            A484ContagemResultado_StatusDmn = H00HF7_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00HF7_n484ContagemResultado_StatusDmn[0];
            AV25GXLvl33 = 1;
            if ( ! ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "H") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "O") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "L") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "P") == 0 ) ) )
            {
               GX_msglist.addItem("Demanda n�o homologada!");
            }
            else
            {
               new prc_insereglosa(context ).execute( ref  A456ContagemResultado_Codigo, ref  AV5ContagemResultado_GlsData, ref  AV6ContagemResultado_GlsDescricao, ref  AV7ContagemResultado_GlsValor, ref  AV12ContagemResultado_GlsUser) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5ContagemResultado_GlsData", context.localUtil.Format(AV5ContagemResultado_GlsData, "99/99/99"));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6ContagemResultado_GlsDescricao", AV6ContagemResultado_GlsDescricao);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContagemResultado_GlsValor", StringUtil.LTrim( StringUtil.Str( AV7ContagemResultado_GlsValor, 12, 2)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ContagemResultado_GlsUser", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12ContagemResultado_GlsUser), 6, 0)));
               AV19Confirmado = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Confirmado", AV19Confirmado);
               context.setWebReturnParms(new Object[] {(bool)AV19Confirmado});
               context.wjLocDisableFrm = 1;
               context.nUserReturn = 1;
               pr_default.close(5);
               returnInSub = true;
               if (true) return;
            }
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(5);
         if ( AV25GXLvl33 == 0 )
         {
            GX_msglist.addItem("N� de demanda inexistente");
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E14HF2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_HF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"4\"  class=''>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock6_Internalname, "Sistema", "", "", lblTextblock6_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Glosa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavSistema_codigo, dynavSistema_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV21Sistema_Codigo), 6, 0)), 1, dynavSistema_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_WP_Glosa.htm");
            dynavSistema_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21Sistema_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSistema_codigo_Internalname, "Values", (String)(dynavSistema_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock7_Internalname, "OS FM", "", "", lblTextblock7_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Glosa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_demandafm_Internalname, AV20ContagemResultado_DemandaFM, StringUtil.RTrim( context.localUtil.Format( AV20ContagemResultado_DemandaFM, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,14);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_demandafm_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavContagemresultado_demandafm_Enabled, 0, "text", "", 80, "px", 1, "row", 50, 0, 0, 0, 1, -1, 0, true, "", "left", true, "HLP_WP_Glosa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock4_Internalname, "Demanda", "", "", lblTextblock4_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Glosa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_codigo_Internalname, hV8ContagemResultado_Codigo, StringUtil.RTrim( context.localUtil.Format( hV8ContagemResultado_Codigo, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,19);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_codigo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_WP_Glosa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock5_Internalname, "Valor a faturar R$", "", "", lblTextblock5_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Glosa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_valorfinal_Internalname, StringUtil.LTrim( StringUtil.NToC( AV14ContagemResultado_ValorFinal, 18, 5, ",", "")), ((edtavContagemresultado_valorfinal_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV14ContagemResultado_ValorFinal, "ZZ,ZZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV14ContagemResultado_ValorFinal, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,23);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_valorfinal_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavContagemresultado_valorfinal_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_Glosa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "Data da glosa", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Glosa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_glsdata_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_glsdata_Internalname, context.localUtil.Format(AV5ContagemResultado_GlsData, "99/99/99"), context.localUtil.Format( AV5ContagemResultado_GlsData, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,28);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_glsdata_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 80, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_Glosa.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_glsdata_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_Glosa.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock2_Internalname, "Descri��o", "", "", lblTextblock2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Glosa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavContagemresultado_glsdescricao_Internalname, AV6ContagemResultado_GlsDescricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", 0, 1, 1, 0, 80, "chr", 7, "row", StyleString, ClassString, "", "500", -1, "", "", -1, true, "", "HLP_WP_Glosa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock3_Internalname, "Valor da glosa R$", "", "", lblTextblock3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Glosa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_glsvalor_Internalname, StringUtil.LTrim( StringUtil.NToC( AV7ContagemResultado_GlsValor, 18, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV7ContagemResultado_GlsValor, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,38);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_glsvalor_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 100, "px", 1, "row", 18, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WP_Glosa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\" colspan=\"4\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;height:60px")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnconfirm_Internalname, "", "Confirmar", bttBtnconfirm_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'ENTER\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_Glosa.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtncancel_Internalname, "", "Fechar", bttBtncancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_Glosa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_HF2e( true) ;
         }
         else
         {
            wb_table1_2_HF2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV11AreaTrabalho_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11AreaTrabalho_Codigo), 6, 0)));
         AV12ContagemResultado_GlsUser = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ContagemResultado_GlsUser", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12ContagemResultado_GlsUser), 6, 0)));
         AV19Confirmado = (bool)getParm(obj,2);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Confirmado", AV19Confirmado);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAHF2( ) ;
         WSHF2( ) ;
         WEHF2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020621623413");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_glosa.js", "?2020621623414");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblock6_Internalname = "TEXTBLOCK6";
         dynavSistema_codigo_Internalname = "vSISTEMA_CODIGO";
         lblTextblock7_Internalname = "TEXTBLOCK7";
         edtavContagemresultado_demandafm_Internalname = "vCONTAGEMRESULTADO_DEMANDAFM";
         lblTextblock4_Internalname = "TEXTBLOCK4";
         edtavContagemresultado_codigo_Internalname = "vCONTAGEMRESULTADO_CODIGO";
         lblTextblock5_Internalname = "TEXTBLOCK5";
         edtavContagemresultado_valorfinal_Internalname = "vCONTAGEMRESULTADO_VALORFINAL";
         lblTextblock1_Internalname = "TEXTBLOCK1";
         edtavContagemresultado_glsdata_Internalname = "vCONTAGEMRESULTADO_GLSDATA";
         lblTextblock2_Internalname = "TEXTBLOCK2";
         edtavContagemresultado_glsdescricao_Internalname = "vCONTAGEMRESULTADO_GLSDESCRICAO";
         lblTextblock3_Internalname = "TEXTBLOCK3";
         edtavContagemresultado_glsvalor_Internalname = "vCONTAGEMRESULTADO_GLSVALOR";
         bttBtnconfirm_Internalname = "BTNCONFIRM";
         bttBtncancel_Internalname = "BTNCANCEL";
         tblTable1_Internalname = "TABLE1";
         edtavAreatrabalho_codigo_Internalname = "vAREATRABALHO_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavContagemresultado_glsvalor_Jsonclick = "";
         edtavContagemresultado_glsdata_Jsonclick = "";
         edtavContagemresultado_valorfinal_Jsonclick = "";
         edtavContagemresultado_valorfinal_Enabled = 1;
         edtavContagemresultado_codigo_Jsonclick = "";
         edtavContagemresultado_demandafm_Jsonclick = "";
         edtavContagemresultado_demandafm_Enabled = 1;
         dynavSistema_codigo_Jsonclick = "";
         edtavAreatrabalho_codigo_Jsonclick = "";
         edtavAreatrabalho_codigo_Visible = 1;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Glosa";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public void Validv_Contagemresultado_codigo( String GX_Parm1 ,
                                                   int GX_Parm2 ,
                                                   int GX_Parm3 ,
                                                   GXCombobox dynGX_Parm4 ,
                                                   String GX_Parm5 )
      {
         hV8ContagemResultado_Codigo = GX_Parm1;
         AV8ContagemResultado_Codigo = GX_Parm2;
         AV11AreaTrabalho_Codigo = GX_Parm3;
         dynavSistema_codigo = dynGX_Parm4;
         AV21Sistema_Codigo = (int)(NumberUtil.Val( dynavSistema_codigo.CurrentValue, "."));
         AV20ContagemResultado_DemandaFM = GX_Parm5;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( hV8ContagemResultado_Codigo)) )
         {
            AV8ContagemResultado_Codigo = 0;
         }
         else
         {
            A457ContagemResultado_Demanda = hV8ContagemResultado_Codigo;
            n457ContagemResultado_Demanda = false;
            /* Using cursor H00HF8 */
            pr_default.execute(6, new Object[] {n457ContagemResultado_Demanda, A457ContagemResultado_Demanda, AV11AreaTrabalho_Codigo, AV21Sistema_Codigo, AV20ContagemResultado_DemandaFM});
            AV8ContagemResultado_Codigo = H00HF8_A456ContagemResultado_Codigo[0];
            if ( ! ( (pr_default.getStatus(6) == 101) ) )
            {
               pr_default.readNext(6);
               if ( ! ( (pr_default.getStatus(6) == 101) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_ambiguousck", new   object[]  {"N� Refer�ncia"}), 1, "vCONTAGEMRESULTADO_CODIGO");
                  GX_FocusControl = edtavContagemresultado_codigo_Internalname;
               }
            }
            else
            {
            }
            pr_default.close(6);
         }
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "hV8ContagemResultado_Codigo", hV8ContagemResultado_Codigo);
         dynload_actions( ) ;
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8ContagemResultado_Codigo), 6, 0, ".", "")));
         isValidOutput.Add(hV8ContagemResultado_Codigo);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Validv_Areatrabalho_codigo( int GX_Parm1 ,
                                              GXCombobox dynGX_Parm2 )
      {
         AV11AreaTrabalho_Codigo = GX_Parm1;
         dynavSistema_codigo = dynGX_Parm2;
         AV21Sistema_Codigo = (int)(NumberUtil.Val( dynavSistema_codigo.CurrentValue, "."));
         GXVvSISTEMA_CODIGO_htmlHF2( AV11AreaTrabalho_Codigo) ;
         dynload_actions( ) ;
         if ( dynavSistema_codigo.ItemCount > 0 )
         {
            AV21Sistema_Codigo = (int)(NumberUtil.Val( dynavSistema_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21Sistema_Codigo), 6, 0))), "."));
         }
         dynavSistema_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21Sistema_Codigo), 6, 0));
         isValidOutput.Add(dynavSistema_codigo);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("VCONTAGEMRESULTADO_CODIGO.ISVALID","{handler:'E12HF2',iparms:[{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'A606ContagemResultado_ValorFinal',fld:'CONTAGEMRESULTADO_VALORFINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1049ContagemResultado_GlsData',fld:'CONTAGEMRESULTADO_GLSDATA',pic:'',nv:''},{av:'A1050ContagemResultado_GlsDescricao',fld:'CONTAGEMRESULTADO_GLSDESCRICAO',pic:'',nv:''},{av:'A1051ContagemResultado_GlsValor',fld:'CONTAGEMRESULTADO_GLSVALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0}],oparms:[{av:'AV14ContagemResultado_ValorFinal',fld:'vCONTAGEMRESULTADO_VALORFINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV5ContagemResultado_GlsData',fld:'vCONTAGEMRESULTADO_GLSDATA',pic:'',nv:''},{av:'AV6ContagemResultado_GlsDescricao',fld:'vCONTAGEMRESULTADO_GLSDESCRICAO',pic:'',nv:''},{av:'AV7ContagemResultado_GlsValor',fld:'vCONTAGEMRESULTADO_GLSVALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0}]}");
         setEventMetadata("'ENTER'","{handler:'E13HF2',iparms:[{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'AV5ContagemResultado_GlsData',fld:'vCONTAGEMRESULTADO_GLSDATA',pic:'',nv:''},{av:'AV6ContagemResultado_GlsDescricao',fld:'vCONTAGEMRESULTADO_GLSDESCRICAO',pic:'',nv:''},{av:'AV7ContagemResultado_GlsValor',fld:'vCONTAGEMRESULTADO_GLSVALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV12ContagemResultado_GlsUser',fld:'vCONTAGEMRESULTADO_GLSUSER',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV12ContagemResultado_GlsUser',fld:'vCONTAGEMRESULTADO_GLSUSER',pic:'ZZZZZ9',nv:0},{av:'AV7ContagemResultado_GlsValor',fld:'vCONTAGEMRESULTADO_GLSVALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV6ContagemResultado_GlsDescricao',fld:'vCONTAGEMRESULTADO_GLSDESCRICAO',pic:'',nv:''},{av:'AV5ContagemResultado_GlsData',fld:'vCONTAGEMRESULTADO_GLSDATA',pic:'',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV19Confirmado',fld:'vCONFIRMADO',pic:'',nv:false}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV20ContagemResultado_DemandaFM = "";
         A457ContagemResultado_Demanda = "";
         hV8ContagemResultado_Codigo = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A484ContagemResultado_StatusDmn = "";
         A1049ContagemResultado_GlsData = DateTime.MinValue;
         A1050ContagemResultado_GlsDescricao = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         l457ContagemResultado_Demanda = "";
         H00HF2_A457ContagemResultado_Demanda = new String[] {""} ;
         H00HF2_n457ContagemResultado_Demanda = new bool[] {false} ;
         H00HF3_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         H00HF3_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         H00HF3_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00HF3_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00HF3_A457ContagemResultado_Demanda = new String[] {""} ;
         H00HF3_n457ContagemResultado_Demanda = new bool[] {false} ;
         H00HF3_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00HF3_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00HF3_A489ContagemResultado_SistemaCod = new int[1] ;
         H00HF3_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         H00HF3_A493ContagemResultado_DemandaFM = new String[] {""} ;
         H00HF3_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         H00HF3_A456ContagemResultado_Codigo = new int[1] ;
         A493ContagemResultado_DemandaFM = "";
         H00HF4_A127Sistema_Codigo = new int[1] ;
         H00HF4_A129Sistema_Sigla = new String[] {""} ;
         H00HF4_A135Sistema_AreaTrabalhoCod = new int[1] ;
         H00HF4_A130Sistema_Ativo = new bool[] {false} ;
         H00HF5_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         H00HF5_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         H00HF5_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00HF5_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00HF5_A457ContagemResultado_Demanda = new String[] {""} ;
         H00HF5_n457ContagemResultado_Demanda = new bool[] {false} ;
         H00HF5_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00HF5_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00HF5_A489ContagemResultado_SistemaCod = new int[1] ;
         H00HF5_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         H00HF5_A493ContagemResultado_DemandaFM = new String[] {""} ;
         H00HF5_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         H00HF5_A456ContagemResultado_Codigo = new int[1] ;
         AV5ContagemResultado_GlsData = DateTime.MinValue;
         AV6ContagemResultado_GlsDescricao = "";
         H00HF6_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00HF6_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00HF6_A1049ContagemResultado_GlsData = new DateTime[] {DateTime.MinValue} ;
         H00HF6_n1049ContagemResultado_GlsData = new bool[] {false} ;
         H00HF6_A1050ContagemResultado_GlsDescricao = new String[] {""} ;
         H00HF6_n1050ContagemResultado_GlsDescricao = new bool[] {false} ;
         H00HF6_A1051ContagemResultado_GlsValor = new decimal[1] ;
         H00HF6_n1051ContagemResultado_GlsValor = new bool[] {false} ;
         H00HF6_A456ContagemResultado_Codigo = new int[1] ;
         H00HF6_A512ContagemResultado_ValorPF = new decimal[1] ;
         H00HF6_n512ContagemResultado_ValorPF = new bool[] {false} ;
         H00HF7_A456ContagemResultado_Codigo = new int[1] ;
         H00HF7_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00HF7_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblTextblock6_Jsonclick = "";
         TempTags = "";
         lblTextblock7_Jsonclick = "";
         lblTextblock4_Jsonclick = "";
         lblTextblock5_Jsonclick = "";
         lblTextblock1_Jsonclick = "";
         lblTextblock2_Jsonclick = "";
         lblTextblock3_Jsonclick = "";
         bttBtnconfirm_Jsonclick = "";
         bttBtncancel_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         H00HF8_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         H00HF8_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         H00HF8_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00HF8_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00HF8_A457ContagemResultado_Demanda = new String[] {""} ;
         H00HF8_n457ContagemResultado_Demanda = new bool[] {false} ;
         H00HF8_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00HF8_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00HF8_A489ContagemResultado_SistemaCod = new int[1] ;
         H00HF8_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         H00HF8_A493ContagemResultado_DemandaFM = new String[] {""} ;
         H00HF8_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         H00HF8_A456ContagemResultado_Codigo = new int[1] ;
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_glosa__default(),
            new Object[][] {
                new Object[] {
               H00HF2_A457ContagemResultado_Demanda, H00HF2_n457ContagemResultado_Demanda
               }
               , new Object[] {
               H00HF3_A805ContagemResultado_ContratadaOrigemCod, H00HF3_n805ContagemResultado_ContratadaOrigemCod, H00HF3_A490ContagemResultado_ContratadaCod, H00HF3_n490ContagemResultado_ContratadaCod, H00HF3_A457ContagemResultado_Demanda, H00HF3_n457ContagemResultado_Demanda, H00HF3_A52Contratada_AreaTrabalhoCod, H00HF3_n52Contratada_AreaTrabalhoCod, H00HF3_A489ContagemResultado_SistemaCod, H00HF3_n489ContagemResultado_SistemaCod,
               H00HF3_A493ContagemResultado_DemandaFM, H00HF3_n493ContagemResultado_DemandaFM, H00HF3_A456ContagemResultado_Codigo
               }
               , new Object[] {
               H00HF4_A127Sistema_Codigo, H00HF4_A129Sistema_Sigla, H00HF4_A135Sistema_AreaTrabalhoCod, H00HF4_A130Sistema_Ativo
               }
               , new Object[] {
               H00HF5_A805ContagemResultado_ContratadaOrigemCod, H00HF5_n805ContagemResultado_ContratadaOrigemCod, H00HF5_A490ContagemResultado_ContratadaCod, H00HF5_n490ContagemResultado_ContratadaCod, H00HF5_A457ContagemResultado_Demanda, H00HF5_n457ContagemResultado_Demanda, H00HF5_A52Contratada_AreaTrabalhoCod, H00HF5_n52Contratada_AreaTrabalhoCod, H00HF5_A489ContagemResultado_SistemaCod, H00HF5_n489ContagemResultado_SistemaCod,
               H00HF5_A493ContagemResultado_DemandaFM, H00HF5_n493ContagemResultado_DemandaFM, H00HF5_A456ContagemResultado_Codigo
               }
               , new Object[] {
               H00HF6_A484ContagemResultado_StatusDmn, H00HF6_n484ContagemResultado_StatusDmn, H00HF6_A1049ContagemResultado_GlsData, H00HF6_n1049ContagemResultado_GlsData, H00HF6_A1050ContagemResultado_GlsDescricao, H00HF6_n1050ContagemResultado_GlsDescricao, H00HF6_A1051ContagemResultado_GlsValor, H00HF6_n1051ContagemResultado_GlsValor, H00HF6_A456ContagemResultado_Codigo, H00HF6_A512ContagemResultado_ValorPF,
               H00HF6_n512ContagemResultado_ValorPF
               }
               , new Object[] {
               H00HF7_A456ContagemResultado_Codigo, H00HF7_A484ContagemResultado_StatusDmn, H00HF7_n484ContagemResultado_StatusDmn
               }
               , new Object[] {
               H00HF8_A805ContagemResultado_ContratadaOrigemCod, H00HF8_n805ContagemResultado_ContratadaOrigemCod, H00HF8_A490ContagemResultado_ContratadaCod, H00HF8_n490ContagemResultado_ContratadaCod, H00HF8_A457ContagemResultado_Demanda, H00HF8_n457ContagemResultado_Demanda, H00HF8_A52Contratada_AreaTrabalhoCod, H00HF8_n52Contratada_AreaTrabalhoCod, H00HF8_A489ContagemResultado_SistemaCod, H00HF8_n489ContagemResultado_SistemaCod,
               H00HF8_A493ContagemResultado_DemandaFM, H00HF8_n493ContagemResultado_DemandaFM, H00HF8_A456ContagemResultado_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavContagemresultado_valorfinal_Enabled = 0;
      }

      private short nRcdExists_4 ;
      private short nIsMod_4 ;
      private short nRcdExists_3 ;
      private short nIsMod_3 ;
      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short gxhchits ;
      private short AV24GXLvl10 ;
      private short AV25GXLvl33 ;
      private short nGXWrapped ;
      private short wbTemp ;
      private int AV11AreaTrabalho_Codigo ;
      private int AV12ContagemResultado_GlsUser ;
      private int wcpOAV11AreaTrabalho_Codigo ;
      private int wcpOAV12ContagemResultado_GlsUser ;
      private int AV21Sistema_Codigo ;
      private int A456ContagemResultado_Codigo ;
      private int edtavAreatrabalho_codigo_Visible ;
      private int gxdynajaxindex ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A489ContagemResultado_SistemaCod ;
      private int edtavContagemresultado_valorfinal_Enabled ;
      private int AV8ContagemResultado_Codigo ;
      private int edtavContagemresultado_demandafm_Enabled ;
      private int idxLst ;
      private decimal A1051ContagemResultado_GlsValor ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal A512ContagemResultado_ValorPF ;
      private decimal A606ContagemResultado_ValorFinal ;
      private decimal AV14ContagemResultado_ValorFinal ;
      private decimal AV7ContagemResultado_GlsValor ;
      private decimal GXt_decimal1 ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A484ContagemResultado_StatusDmn ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String edtavAreatrabalho_codigo_Internalname ;
      private String edtavAreatrabalho_codigo_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String dynavSistema_codigo_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String edtavContagemresultado_valorfinal_Internalname ;
      private String edtavContagemresultado_demandafm_Internalname ;
      private String edtavContagemresultado_codigo_Internalname ;
      private String edtavContagemresultado_glsdata_Internalname ;
      private String edtavContagemresultado_glsdescricao_Internalname ;
      private String edtavContagemresultado_glsvalor_Internalname ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String lblTextblock6_Internalname ;
      private String lblTextblock6_Jsonclick ;
      private String TempTags ;
      private String dynavSistema_codigo_Jsonclick ;
      private String lblTextblock7_Internalname ;
      private String lblTextblock7_Jsonclick ;
      private String edtavContagemresultado_demandafm_Jsonclick ;
      private String lblTextblock4_Internalname ;
      private String lblTextblock4_Jsonclick ;
      private String edtavContagemresultado_codigo_Jsonclick ;
      private String lblTextblock5_Internalname ;
      private String lblTextblock5_Jsonclick ;
      private String edtavContagemresultado_valorfinal_Jsonclick ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String edtavContagemresultado_glsdata_Jsonclick ;
      private String lblTextblock2_Internalname ;
      private String lblTextblock2_Jsonclick ;
      private String lblTextblock3_Internalname ;
      private String lblTextblock3_Jsonclick ;
      private String edtavContagemresultado_glsvalor_Jsonclick ;
      private String bttBtnconfirm_Internalname ;
      private String bttBtnconfirm_Jsonclick ;
      private String bttBtncancel_Internalname ;
      private String bttBtncancel_Jsonclick ;
      private DateTime A1049ContagemResultado_GlsData ;
      private DateTime AV5ContagemResultado_GlsData ;
      private bool entryPointCalled ;
      private bool n457ContagemResultado_Demanda ;
      private bool AV19Confirmado ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool returnInSub ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n1049ContagemResultado_GlsData ;
      private bool n1050ContagemResultado_GlsDescricao ;
      private bool n1051ContagemResultado_GlsValor ;
      private bool n512ContagemResultado_ValorPF ;
      private String A1050ContagemResultado_GlsDescricao ;
      private String AV6ContagemResultado_GlsDescricao ;
      private String AV20ContagemResultado_DemandaFM ;
      private String A457ContagemResultado_Demanda ;
      private String hV8ContagemResultado_Codigo ;
      private String l457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavSistema_codigo ;
      private IDataStoreProvider pr_default ;
      private String[] H00HF2_A457ContagemResultado_Demanda ;
      private bool[] H00HF2_n457ContagemResultado_Demanda ;
      private int[] H00HF3_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] H00HF3_n805ContagemResultado_ContratadaOrigemCod ;
      private int[] H00HF3_A490ContagemResultado_ContratadaCod ;
      private bool[] H00HF3_n490ContagemResultado_ContratadaCod ;
      private String[] H00HF3_A457ContagemResultado_Demanda ;
      private bool[] H00HF3_n457ContagemResultado_Demanda ;
      private int[] H00HF3_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00HF3_n52Contratada_AreaTrabalhoCod ;
      private int[] H00HF3_A489ContagemResultado_SistemaCod ;
      private bool[] H00HF3_n489ContagemResultado_SistemaCod ;
      private String[] H00HF3_A493ContagemResultado_DemandaFM ;
      private bool[] H00HF3_n493ContagemResultado_DemandaFM ;
      private int[] H00HF3_A456ContagemResultado_Codigo ;
      private int[] H00HF4_A127Sistema_Codigo ;
      private String[] H00HF4_A129Sistema_Sigla ;
      private int[] H00HF4_A135Sistema_AreaTrabalhoCod ;
      private bool[] H00HF4_A130Sistema_Ativo ;
      private int[] H00HF5_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] H00HF5_n805ContagemResultado_ContratadaOrigemCod ;
      private int[] H00HF5_A490ContagemResultado_ContratadaCod ;
      private bool[] H00HF5_n490ContagemResultado_ContratadaCod ;
      private String[] H00HF5_A457ContagemResultado_Demanda ;
      private bool[] H00HF5_n457ContagemResultado_Demanda ;
      private int[] H00HF5_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00HF5_n52Contratada_AreaTrabalhoCod ;
      private int[] H00HF5_A489ContagemResultado_SistemaCod ;
      private bool[] H00HF5_n489ContagemResultado_SistemaCod ;
      private String[] H00HF5_A493ContagemResultado_DemandaFM ;
      private bool[] H00HF5_n493ContagemResultado_DemandaFM ;
      private int[] H00HF5_A456ContagemResultado_Codigo ;
      private String[] H00HF6_A484ContagemResultado_StatusDmn ;
      private bool[] H00HF6_n484ContagemResultado_StatusDmn ;
      private DateTime[] H00HF6_A1049ContagemResultado_GlsData ;
      private bool[] H00HF6_n1049ContagemResultado_GlsData ;
      private String[] H00HF6_A1050ContagemResultado_GlsDescricao ;
      private bool[] H00HF6_n1050ContagemResultado_GlsDescricao ;
      private decimal[] H00HF6_A1051ContagemResultado_GlsValor ;
      private bool[] H00HF6_n1051ContagemResultado_GlsValor ;
      private int[] H00HF6_A456ContagemResultado_Codigo ;
      private decimal[] H00HF6_A512ContagemResultado_ValorPF ;
      private bool[] H00HF6_n512ContagemResultado_ValorPF ;
      private int[] H00HF7_A456ContagemResultado_Codigo ;
      private String[] H00HF7_A484ContagemResultado_StatusDmn ;
      private bool[] H00HF7_n484ContagemResultado_StatusDmn ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private int[] H00HF8_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] H00HF8_n805ContagemResultado_ContratadaOrigemCod ;
      private int[] H00HF8_A490ContagemResultado_ContratadaCod ;
      private bool[] H00HF8_n490ContagemResultado_ContratadaCod ;
      private String[] H00HF8_A457ContagemResultado_Demanda ;
      private bool[] H00HF8_n457ContagemResultado_Demanda ;
      private int[] H00HF8_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00HF8_n52Contratada_AreaTrabalhoCod ;
      private int[] H00HF8_A489ContagemResultado_SistemaCod ;
      private bool[] H00HF8_n489ContagemResultado_SistemaCod ;
      private String[] H00HF8_A493ContagemResultado_DemandaFM ;
      private bool[] H00HF8_n493ContagemResultado_DemandaFM ;
      private int[] H00HF8_A456ContagemResultado_Codigo ;
      private bool aP2_Confirmado ;
      private GXWebForm Form ;
   }

   public class wp_glosa__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00HF2 ;
          prmH00HF2 = new Object[] {
          new Object[] {"@l457ContagemResultado_Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV11AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV21Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV20ContagemResultado_DemandaFM",SqlDbType.VarChar,50,0}
          } ;
          Object[] prmH00HF3 ;
          prmH00HF3 = new Object[] {
          new Object[] {"@ContagemResultado_Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV11AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV21Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV20ContagemResultado_DemandaFM",SqlDbType.VarChar,50,0}
          } ;
          Object[] prmH00HF4 ;
          prmH00HF4 = new Object[] {
          new Object[] {"@AV11AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00HF5 ;
          prmH00HF5 = new Object[] {
          new Object[] {"@ContagemResultado_Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV11AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV21Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV20ContagemResultado_DemandaFM",SqlDbType.VarChar,50,0}
          } ;
          Object[] prmH00HF6 ;
          prmH00HF6 = new Object[] {
          new Object[] {"@AV8ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00HF7 ;
          prmH00HF7 = new Object[] {
          new Object[] {"@AV8ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00HF8 ;
          prmH00HF8 = new Object[] {
          new Object[] {"@ContagemResultado_Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV11AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV21Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV20ContagemResultado_DemandaFM",SqlDbType.VarChar,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00HF2", "SELECT DISTINCT TOP 5 T1.[ContagemResultado_Demanda] FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaOrigemCod]) WHERE (UPPER(T1.[ContagemResultado_Demanda]) like UPPER(@l457ContagemResultado_Demanda)) AND (T2.[Contratada_AreaTrabalhoCod] = @AV11AreaTrabalho_Codigo and ( (@AV21Sistema_Codigo = convert(int, 0)) or T1.[ContagemResultado_SistemaCod] = @AV21Sistema_Codigo) and ( (@AV20ContagemResultado_DemandaFM = '') or T1.[ContagemResultado_DemandaFM] = @AV20ContagemResultado_DemandaFM)) ORDER BY T1.[ContagemResultado_Demanda] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00HF2,0,0,true,false )
             ,new CursorDef("H00HF3", "SELECT T3.[Contratada_Codigo] AS ContagemResultado_ContratadaOr, T2.[Contratada_Codigo] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_Demanda], T2.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_SistemaCod], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Codigo] FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaOrigemCod]) WHERE (T1.[ContagemResultado_Demanda] = @ContagemResultado_Demanda) AND (T2.[Contratada_AreaTrabalhoCod] = @AV11AreaTrabalho_Codigo and ( (@AV21Sistema_Codigo = convert(int, 0)) or T1.[ContagemResultado_SistemaCod] = @AV21Sistema_Codigo) and ( (@AV20ContagemResultado_DemandaFM = '') or T1.[ContagemResultado_DemandaFM] = @AV20ContagemResultado_DemandaFM)) ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00HF3,0,0,true,false )
             ,new CursorDef("H00HF4", "SELECT [Sistema_Codigo], [Sistema_Sigla], [Sistema_AreaTrabalhoCod], [Sistema_Ativo] FROM [Sistema] WITH (NOLOCK) WHERE ([Sistema_Ativo] = 1) AND ([Sistema_AreaTrabalhoCod] = @AV11AreaTrabalho_Codigo) ORDER BY [Sistema_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00HF4,0,0,true,false )
             ,new CursorDef("H00HF5", "SELECT T3.[Contratada_Codigo] AS ContagemResultado_ContratadaOr, T2.[Contratada_Codigo] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_Demanda], T2.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_SistemaCod], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Codigo] FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaOrigemCod]) WHERE (T1.[ContagemResultado_Demanda] = @ContagemResultado_Demanda) AND (T2.[Contratada_AreaTrabalhoCod] = @AV11AreaTrabalho_Codigo and ( (@AV21Sistema_Codigo = convert(int, 0)) or T1.[ContagemResultado_SistemaCod] = @AV21Sistema_Codigo) and ( (@AV20ContagemResultado_DemandaFM = '') or T1.[ContagemResultado_DemandaFM] = @AV20ContagemResultado_DemandaFM)) ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00HF5,0,0,true,false )
             ,new CursorDef("H00HF6", "SELECT [ContagemResultado_StatusDmn], [ContagemResultado_GlsData], [ContagemResultado_GlsDescricao], [ContagemResultado_GlsValor], [ContagemResultado_Codigo], [ContagemResultado_ValorPF] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @AV8ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00HF6,1,0,true,true )
             ,new CursorDef("H00HF7", "SELECT [ContagemResultado_Codigo], [ContagemResultado_StatusDmn] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @AV8ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00HF7,1,0,true,true )
             ,new CursorDef("H00HF8", "SELECT T3.[Contratada_Codigo] AS ContagemResultado_ContratadaOr, T2.[Contratada_Codigo] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_Demanda], T2.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_SistemaCod], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Codigo] FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaOrigemCod]) WHERE (T1.[ContagemResultado_Demanda] = @ContagemResultado_Demanda) AND (T2.[Contratada_AreaTrabalhoCod] = @AV11AreaTrabalho_Codigo and ( (@AV21Sistema_Codigo = convert(int, 0)) or T1.[ContagemResultado_SistemaCod] = @AV21Sistema_Codigo) and ( (@AV20ContagemResultado_DemandaFM = '') or T1.[ContagemResultado_DemandaFM] = @AV20ContagemResultado_DemandaFM)) ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00HF8,0,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 25) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((decimal[]) buf[9])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (int)parms[3]);
                stmt.SetParameter(4, (String)parms[4]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (int)parms[3]);
                stmt.SetParameter(4, (String)parms[4]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (int)parms[3]);
                stmt.SetParameter(4, (String)parms[4]);
                return;
       }
    }

 }

}
