/*
               File: PRC_SelectUnselect
        Description: Selectec Unselect
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:50:50.63
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_selectunselect : GXProcedure
   {
      public prc_selectunselect( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_selectunselect( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Usuario_Codigo ,
                           ref int aP1_Selected_Codigo ,
                           ref bool aP2_Flag )
      {
         this.A1Usuario_Codigo = aP0_Usuario_Codigo;
         this.A487Selected_Codigo = aP1_Selected_Codigo;
         this.AV8Flag = aP2_Flag;
         initialize();
         executePrivate();
         aP0_Usuario_Codigo=this.A1Usuario_Codigo;
         aP1_Selected_Codigo=this.A487Selected_Codigo;
         aP2_Flag=this.AV8Flag;
      }

      public bool executeUdp( ref int aP0_Usuario_Codigo ,
                              ref int aP1_Selected_Codigo )
      {
         this.A1Usuario_Codigo = aP0_Usuario_Codigo;
         this.A487Selected_Codigo = aP1_Selected_Codigo;
         this.AV8Flag = aP2_Flag;
         initialize();
         executePrivate();
         aP0_Usuario_Codigo=this.A1Usuario_Codigo;
         aP1_Selected_Codigo=this.A487Selected_Codigo;
         aP2_Flag=this.AV8Flag;
         return AV8Flag ;
      }

      public void executeSubmit( ref int aP0_Usuario_Codigo ,
                                 ref int aP1_Selected_Codigo ,
                                 ref bool aP2_Flag )
      {
         prc_selectunselect objprc_selectunselect;
         objprc_selectunselect = new prc_selectunselect();
         objprc_selectunselect.A1Usuario_Codigo = aP0_Usuario_Codigo;
         objprc_selectunselect.A487Selected_Codigo = aP1_Selected_Codigo;
         objprc_selectunselect.AV8Flag = aP2_Flag;
         objprc_selectunselect.context.SetSubmitInitialConfig(context);
         objprc_selectunselect.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_selectunselect);
         aP0_Usuario_Codigo=this.A1Usuario_Codigo;
         aP1_Selected_Codigo=this.A487Selected_Codigo;
         aP2_Flag=this.AV8Flag;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_selectunselect)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( AV8Flag )
         {
            /*
               INSERT RECORD ON TABLE Selected

            */
            A488Selected_Flag = AV8Flag;
            /* Using cursor P002U2 */
            pr_default.execute(0, new Object[] {A1Usuario_Codigo, A487Selected_Codigo, A488Selected_Flag});
            pr_default.close(0);
            dsDefault.SmartCacheProvider.SetUpdated("Selected") ;
            if ( (pr_default.getStatus(0) == 1) )
            {
               context.Gx_err = 1;
               Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
            }
            else
            {
               context.Gx_err = 0;
               Gx_emsg = "";
            }
            /* End Insert */
         }
         else
         {
            /* Optimized DELETE. */
            /* Using cursor P002U3 */
            pr_default.execute(1, new Object[] {A1Usuario_Codigo, A487Selected_Codigo});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("Selected") ;
            /* End optimized DELETE. */
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_SelectUnselect");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gx_emsg = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_selectunselect__default(),
            new Object[][] {
                new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A1Usuario_Codigo ;
      private int A487Selected_Codigo ;
      private int GX_INS71 ;
      private String Gx_emsg ;
      private bool AV8Flag ;
      private bool A488Selected_Flag ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Usuario_Codigo ;
      private int aP1_Selected_Codigo ;
      private bool aP2_Flag ;
      private IDataStoreProvider pr_default ;
   }

   public class prc_selectunselect__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new UpdateCursor(def[0])
         ,new UpdateCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP002U2 ;
          prmP002U2 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Selected_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Selected_Flag",SqlDbType.Bit,4,0}
          } ;
          Object[] prmP002U3 ;
          prmP002U3 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Selected_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P002U2", "INSERT INTO [Selected]([Usuario_Codigo], [Selected_Codigo], [Selected_Flag]) VALUES(@Usuario_Codigo, @Selected_Codigo, @Selected_Flag)", GxErrorMask.GX_NOMASK,prmP002U2)
             ,new CursorDef("P002U3", "DELETE FROM [Selected]  WHERE [Usuario_Codigo] = @Usuario_Codigo and [Selected_Codigo] = @Selected_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP002U3)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (bool)parms[2]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
