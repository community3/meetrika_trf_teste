/*
               File: ContratadaUsuario
        Description: Contratada Usuario
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:10:15.61
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratadausuario : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_8") == 0 )
         {
            A69ContratadaUsuario_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A69ContratadaUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_8( A69ContratadaUsuario_UsuarioCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_11") == 0 )
         {
            A70ContratadaUsuario_UsuarioPessoaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n70ContratadaUsuario_UsuarioPessoaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A70ContratadaUsuario_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A70ContratadaUsuario_UsuarioPessoaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_11( A70ContratadaUsuario_UsuarioPessoaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7ContratadaUsuario_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContratadaUsuario_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratadaUsuario_ContratadaCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATADAUSUARIO_CONTRATADACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratadaUsuario_ContratadaCod), "ZZZZZ9")));
               AV8ContratadaUsuario_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContratadaUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8ContratadaUsuario_UsuarioCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATADAUSUARIO_USUARIOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8ContratadaUsuario_UsuarioCod), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Contratada Usuario", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtContratadaUsuario_UsuarioCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contratadausuario( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contratadausuario( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_ContratadaUsuario_ContratadaCod ,
                           int aP2_ContratadaUsuario_UsuarioCod )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7ContratadaUsuario_ContratadaCod = aP1_ContratadaUsuario_ContratadaCod;
         this.AV8ContratadaUsuario_UsuarioCod = aP2_ContratadaUsuario_UsuarioCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_0F16( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_0F16e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratadaUsuario_ContratadaPessoaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A67ContratadaUsuario_ContratadaPessoaCod), 6, 0, ",", "")), ((edtContratadaUsuario_ContratadaPessoaCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A67ContratadaUsuario_ContratadaPessoaCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A67ContratadaUsuario_ContratadaPessoaCod), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratadaUsuario_ContratadaPessoaCod_Jsonclick, 0, "Attribute", "", "", "", edtContratadaUsuario_ContratadaPessoaCod_Visible, edtContratadaUsuario_ContratadaPessoaCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratadaUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratadaUsuario_UsuarioCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A69ContratadaUsuario_UsuarioCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,40);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratadaUsuario_UsuarioCod_Jsonclick, 0, "Attribute", "", "", "", edtContratadaUsuario_UsuarioCod_Visible, edtContratadaUsuario_UsuarioCod_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratadaUsuario.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratadaUsuario_UsuarioPessoaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A70ContratadaUsuario_UsuarioPessoaCod), 6, 0, ",", "")), ((edtContratadaUsuario_UsuarioPessoaCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A70ContratadaUsuario_UsuarioPessoaCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A70ContratadaUsuario_UsuarioPessoaCod), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratadaUsuario_UsuarioPessoaCod_Jsonclick, 0, "Attribute", "", "", "", edtContratadaUsuario_UsuarioPessoaCod_Visible, edtContratadaUsuario_UsuarioPessoaCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratadaUsuario.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_0F16( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_0F16( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_0F16e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_31_0F16( true) ;
         }
         return  ;
      }

      protected void wb_table3_31_0F16e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_0F16e( true) ;
         }
         else
         {
            wb_table1_2_0F16e( false) ;
         }
      }

      protected void wb_table3_31_0F16( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratadaUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratadaUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratadaUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_31_0F16e( true) ;
         }
         else
         {
            wb_table3_31_0F16e( false) ;
         }
      }

      protected void wb_table2_5_0F16( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_0F16( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_0F16e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_0F16e( true) ;
         }
         else
         {
            wb_table2_5_0F16e( false) ;
         }
      }

      protected void wb_table4_13_0F16( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_nome_Internalname, "Usu�rio", "", "", lblTextblockusuario_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratadaUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtUsuario_Nome_Internalname, StringUtil.RTrim( A2Usuario_Nome), StringUtil.RTrim( context.localUtil.Format( A2Usuario_Nome, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUsuario_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtUsuario_Nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ContratadaUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratadausuario_usuariopessoanom_Internalname, "Pessoa", "", "", lblTextblockcontratadausuario_usuariopessoanom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratadaUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratadaUsuario_UsuarioPessoaNom_Internalname, StringUtil.RTrim( A71ContratadaUsuario_UsuarioPessoaNom), StringUtil.RTrim( context.localUtil.Format( A71ContratadaUsuario_UsuarioPessoaNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratadaUsuario_UsuarioPessoaNom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratadaUsuario_UsuarioPessoaNom_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContratadaUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratadausuario_contratadapessoanom_Internalname, "Contratada", "", "", lblTextblockcontratadausuario_contratadapessoanom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratadaUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratadaUsuario_ContratadaPessoaNom_Internalname, StringUtil.RTrim( A68ContratadaUsuario_ContratadaPessoaNom), StringUtil.RTrim( context.localUtil.Format( A68ContratadaUsuario_ContratadaPessoaNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratadaUsuario_ContratadaPessoaNom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratadaUsuario_ContratadaPessoaNom_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContratadaUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_0F16e( true) ;
         }
         else
         {
            wb_table4_13_0F16e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E110F2 */
         E110F2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A2Usuario_Nome = StringUtil.Upper( cgiGet( edtUsuario_Nome_Internalname));
               n2Usuario_Nome = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2Usuario_Nome", A2Usuario_Nome);
               A71ContratadaUsuario_UsuarioPessoaNom = StringUtil.Upper( cgiGet( edtContratadaUsuario_UsuarioPessoaNom_Internalname));
               n71ContratadaUsuario_UsuarioPessoaNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A71ContratadaUsuario_UsuarioPessoaNom", A71ContratadaUsuario_UsuarioPessoaNom);
               A68ContratadaUsuario_ContratadaPessoaNom = StringUtil.Upper( cgiGet( edtContratadaUsuario_ContratadaPessoaNom_Internalname));
               n68ContratadaUsuario_ContratadaPessoaNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A68ContratadaUsuario_ContratadaPessoaNom", A68ContratadaUsuario_ContratadaPessoaNom);
               A67ContratadaUsuario_ContratadaPessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContratadaUsuario_ContratadaPessoaCod_Internalname), ",", "."));
               n67ContratadaUsuario_ContratadaPessoaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A67ContratadaUsuario_ContratadaPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A67ContratadaUsuario_ContratadaPessoaCod), 6, 0)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratadaUsuario_UsuarioCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratadaUsuario_UsuarioCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATADAUSUARIO_USUARIOCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContratadaUsuario_UsuarioCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A69ContratadaUsuario_UsuarioCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A69ContratadaUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0)));
               }
               else
               {
                  A69ContratadaUsuario_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( edtContratadaUsuario_UsuarioCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A69ContratadaUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0)));
               }
               A70ContratadaUsuario_UsuarioPessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContratadaUsuario_UsuarioPessoaCod_Internalname), ",", "."));
               n70ContratadaUsuario_UsuarioPessoaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A70ContratadaUsuario_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A70ContratadaUsuario_UsuarioPessoaCod), 6, 0)));
               /* Read saved values. */
               Z66ContratadaUsuario_ContratadaCod = (int)(context.localUtil.CToN( cgiGet( "Z66ContratadaUsuario_ContratadaCod"), ",", "."));
               Z69ContratadaUsuario_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( "Z69ContratadaUsuario_UsuarioCod"), ",", "."));
               Z576ContratadaUsuario_CstUntPrdNrm = context.localUtil.CToN( cgiGet( "Z576ContratadaUsuario_CstUntPrdNrm"), ",", ".");
               n576ContratadaUsuario_CstUntPrdNrm = ((Convert.ToDecimal(0)==A576ContratadaUsuario_CstUntPrdNrm) ? true : false);
               Z577ContratadaUsuario_CstUntPrdExt = context.localUtil.CToN( cgiGet( "Z577ContratadaUsuario_CstUntPrdExt"), ",", ".");
               n577ContratadaUsuario_CstUntPrdExt = ((Convert.ToDecimal(0)==A577ContratadaUsuario_CstUntPrdExt) ? true : false);
               Z1518ContratadaUsuario_TmpEstAnl = (int)(context.localUtil.CToN( cgiGet( "Z1518ContratadaUsuario_TmpEstAnl"), ",", "."));
               n1518ContratadaUsuario_TmpEstAnl = ((0==A1518ContratadaUsuario_TmpEstAnl) ? true : false);
               Z1503ContratadaUsuario_TmpEstExc = (int)(context.localUtil.CToN( cgiGet( "Z1503ContratadaUsuario_TmpEstExc"), ",", "."));
               n1503ContratadaUsuario_TmpEstExc = ((0==A1503ContratadaUsuario_TmpEstExc) ? true : false);
               Z1504ContratadaUsuario_TmpEstCrr = (int)(context.localUtil.CToN( cgiGet( "Z1504ContratadaUsuario_TmpEstCrr"), ",", "."));
               n1504ContratadaUsuario_TmpEstCrr = ((0==A1504ContratadaUsuario_TmpEstCrr) ? true : false);
               A576ContratadaUsuario_CstUntPrdNrm = context.localUtil.CToN( cgiGet( "Z576ContratadaUsuario_CstUntPrdNrm"), ",", ".");
               n576ContratadaUsuario_CstUntPrdNrm = false;
               n576ContratadaUsuario_CstUntPrdNrm = ((Convert.ToDecimal(0)==A576ContratadaUsuario_CstUntPrdNrm) ? true : false);
               A577ContratadaUsuario_CstUntPrdExt = context.localUtil.CToN( cgiGet( "Z577ContratadaUsuario_CstUntPrdExt"), ",", ".");
               n577ContratadaUsuario_CstUntPrdExt = false;
               n577ContratadaUsuario_CstUntPrdExt = ((Convert.ToDecimal(0)==A577ContratadaUsuario_CstUntPrdExt) ? true : false);
               A1518ContratadaUsuario_TmpEstAnl = (int)(context.localUtil.CToN( cgiGet( "Z1518ContratadaUsuario_TmpEstAnl"), ",", "."));
               n1518ContratadaUsuario_TmpEstAnl = false;
               n1518ContratadaUsuario_TmpEstAnl = ((0==A1518ContratadaUsuario_TmpEstAnl) ? true : false);
               A1503ContratadaUsuario_TmpEstExc = (int)(context.localUtil.CToN( cgiGet( "Z1503ContratadaUsuario_TmpEstExc"), ",", "."));
               n1503ContratadaUsuario_TmpEstExc = false;
               n1503ContratadaUsuario_TmpEstExc = ((0==A1503ContratadaUsuario_TmpEstExc) ? true : false);
               A1504ContratadaUsuario_TmpEstCrr = (int)(context.localUtil.CToN( cgiGet( "Z1504ContratadaUsuario_TmpEstCrr"), ",", "."));
               n1504ContratadaUsuario_TmpEstCrr = false;
               n1504ContratadaUsuario_TmpEstCrr = ((0==A1504ContratadaUsuario_TmpEstCrr) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               AV7ContratadaUsuario_ContratadaCod = (int)(context.localUtil.CToN( cgiGet( "vCONTRATADAUSUARIO_CONTRATADACOD"), ",", "."));
               A66ContratadaUsuario_ContratadaCod = (int)(context.localUtil.CToN( cgiGet( "CONTRATADAUSUARIO_CONTRATADACOD"), ",", "."));
               AV8ContratadaUsuario_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( "vCONTRATADAUSUARIO_USUARIOCOD"), ",", "."));
               A576ContratadaUsuario_CstUntPrdNrm = context.localUtil.CToN( cgiGet( "CONTRATADAUSUARIO_CSTUNTPRDNRM"), ",", ".");
               n576ContratadaUsuario_CstUntPrdNrm = ((Convert.ToDecimal(0)==A576ContratadaUsuario_CstUntPrdNrm) ? true : false);
               A577ContratadaUsuario_CstUntPrdExt = context.localUtil.CToN( cgiGet( "CONTRATADAUSUARIO_CSTUNTPRDEXT"), ",", ".");
               n577ContratadaUsuario_CstUntPrdExt = ((Convert.ToDecimal(0)==A577ContratadaUsuario_CstUntPrdExt) ? true : false);
               A1518ContratadaUsuario_TmpEstAnl = (int)(context.localUtil.CToN( cgiGet( "CONTRATADAUSUARIO_TMPESTANL"), ",", "."));
               n1518ContratadaUsuario_TmpEstAnl = ((0==A1518ContratadaUsuario_TmpEstAnl) ? true : false);
               A1503ContratadaUsuario_TmpEstExc = (int)(context.localUtil.CToN( cgiGet( "CONTRATADAUSUARIO_TMPESTEXC"), ",", "."));
               n1503ContratadaUsuario_TmpEstExc = ((0==A1503ContratadaUsuario_TmpEstExc) ? true : false);
               A1504ContratadaUsuario_TmpEstCrr = (int)(context.localUtil.CToN( cgiGet( "CONTRATADAUSUARIO_TMPESTCRR"), ",", "."));
               n1504ContratadaUsuario_TmpEstCrr = ((0==A1504ContratadaUsuario_TmpEstCrr) ? true : false);
               A1228ContratadaUsuario_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "CONTRATADAUSUARIO_AREATRABALHOCOD"), ",", "."));
               n1228ContratadaUsuario_AreaTrabalhoCod = false;
               A1018ContratadaUsuario_UsuarioEhContratada = StringUtil.StrToBool( cgiGet( "CONTRATADAUSUARIO_USUARIOEHCONTRATADA"));
               n1018ContratadaUsuario_UsuarioEhContratada = false;
               A1394ContratadaUsuario_UsuarioAtivo = StringUtil.StrToBool( cgiGet( "CONTRATADAUSUARIO_USUARIOATIVO"));
               n1394ContratadaUsuario_UsuarioAtivo = false;
               A341Usuario_UserGamGuid = cgiGet( "USUARIO_USERGAMGUID");
               n341Usuario_UserGamGuid = false;
               A348ContratadaUsuario_ContratadaPessoaCNPJ = cgiGet( "CONTRATADAUSUARIO_CONTRATADAPESSOACNPJ");
               n348ContratadaUsuario_ContratadaPessoaCNPJ = false;
               A1297ContratadaUsuario_AreaTrabalhoDes = cgiGet( "CONTRATADAUSUARIO_AREATRABALHODES");
               n1297ContratadaUsuario_AreaTrabalhoDes = false;
               A491ContratadaUsuario_UsuarioPessoaDoc = cgiGet( "CONTRATADAUSUARIO_USUARIOPESSOADOC");
               n491ContratadaUsuario_UsuarioPessoaDoc = false;
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "ContratadaUsuario";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A66ContratadaUsuario_ContratadaCod), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A576ContratadaUsuario_CstUntPrdNrm, "ZZZ,ZZZ,ZZZ,ZZ9.99");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A577ContratadaUsuario_CstUntPrdExt, "ZZZ,ZZZ,ZZZ,ZZ9.99");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1518ContratadaUsuario_TmpEstAnl), "ZZZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1503ContratadaUsuario_TmpEstExc), "ZZZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1504ContratadaUsuario_TmpEstCrr), "ZZZZZZZ9");
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A69ContratadaUsuario_UsuarioCod != Z69ContratadaUsuario_UsuarioCod ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("contratadausuario:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("contratadausuario:[SecurityCheckFailed value for]"+"ContratadaUsuario_ContratadaCod:"+context.localUtil.Format( (decimal)(A66ContratadaUsuario_ContratadaCod), "ZZZZZ9"));
                  GXUtil.WriteLog("contratadausuario:[SecurityCheckFailed value for]"+"ContratadaUsuario_CstUntPrdNrm:"+context.localUtil.Format( A576ContratadaUsuario_CstUntPrdNrm, "ZZZ,ZZZ,ZZZ,ZZ9.99"));
                  GXUtil.WriteLog("contratadausuario:[SecurityCheckFailed value for]"+"ContratadaUsuario_CstUntPrdExt:"+context.localUtil.Format( A577ContratadaUsuario_CstUntPrdExt, "ZZZ,ZZZ,ZZZ,ZZ9.99"));
                  GXUtil.WriteLog("contratadausuario:[SecurityCheckFailed value for]"+"ContratadaUsuario_TmpEstAnl:"+context.localUtil.Format( (decimal)(A1518ContratadaUsuario_TmpEstAnl), "ZZZZZZZ9"));
                  GXUtil.WriteLog("contratadausuario:[SecurityCheckFailed value for]"+"ContratadaUsuario_TmpEstExc:"+context.localUtil.Format( (decimal)(A1503ContratadaUsuario_TmpEstExc), "ZZZZZZZ9"));
                  GXUtil.WriteLog("contratadausuario:[SecurityCheckFailed value for]"+"ContratadaUsuario_TmpEstCrr:"+context.localUtil.Format( (decimal)(A1504ContratadaUsuario_TmpEstCrr), "ZZZZZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A66ContratadaUsuario_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A66ContratadaUsuario_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0)));
                  A69ContratadaUsuario_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A69ContratadaUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode16 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode16;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound16 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_0F0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "CONTRATADAUSUARIO_USUARIOCOD");
                        AnyError = 1;
                        GX_FocusControl = edtContratadaUsuario_UsuarioCod_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E110F2 */
                           E110F2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E120F2 */
                           E120F2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E120F2 */
            E120F2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll0F16( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes0F16( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_0F0( )
      {
         BeforeValidate0F16( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls0F16( ) ;
            }
            else
            {
               CheckExtendedTable0F16( ) ;
               CloseExtendedTableCursors0F16( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption0F0( )
      {
      }

      protected void E110F2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         AV10TrnContext.FromXml(AV11WebSession.Get("TrnContext"), "");
         edtContratadaUsuario_ContratadaPessoaCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratadaUsuario_ContratadaPessoaCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratadaUsuario_ContratadaPessoaCod_Visible), 5, 0)));
         edtContratadaUsuario_UsuarioCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratadaUsuario_UsuarioCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratadaUsuario_UsuarioCod_Visible), 5, 0)));
         edtContratadaUsuario_UsuarioPessoaCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratadaUsuario_UsuarioPessoaCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratadaUsuario_UsuarioPessoaCod_Visible), 5, 0)));
      }

      protected void E120F2( )
      {
         /* After Trn Routine */
         if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
         {
            new prc_tirardagestaodecontratos(context ).execute( ref  AV8ContratadaUsuario_UsuarioCod) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContratadaUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8ContratadaUsuario_UsuarioCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATADAUSUARIO_USUARIOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8ContratadaUsuario_UsuarioCod), "ZZZZZ9")));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV10TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwcontratadausuario.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM0F16( short GX_JID )
      {
         if ( ( GX_JID == 6 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z576ContratadaUsuario_CstUntPrdNrm = T000F3_A576ContratadaUsuario_CstUntPrdNrm[0];
               Z577ContratadaUsuario_CstUntPrdExt = T000F3_A577ContratadaUsuario_CstUntPrdExt[0];
               Z1518ContratadaUsuario_TmpEstAnl = T000F3_A1518ContratadaUsuario_TmpEstAnl[0];
               Z1503ContratadaUsuario_TmpEstExc = T000F3_A1503ContratadaUsuario_TmpEstExc[0];
               Z1504ContratadaUsuario_TmpEstCrr = T000F3_A1504ContratadaUsuario_TmpEstCrr[0];
            }
            else
            {
               Z576ContratadaUsuario_CstUntPrdNrm = A576ContratadaUsuario_CstUntPrdNrm;
               Z577ContratadaUsuario_CstUntPrdExt = A577ContratadaUsuario_CstUntPrdExt;
               Z1518ContratadaUsuario_TmpEstAnl = A1518ContratadaUsuario_TmpEstAnl;
               Z1503ContratadaUsuario_TmpEstExc = A1503ContratadaUsuario_TmpEstExc;
               Z1504ContratadaUsuario_TmpEstCrr = A1504ContratadaUsuario_TmpEstCrr;
            }
         }
         if ( GX_JID == -6 )
         {
            Z576ContratadaUsuario_CstUntPrdNrm = A576ContratadaUsuario_CstUntPrdNrm;
            Z577ContratadaUsuario_CstUntPrdExt = A577ContratadaUsuario_CstUntPrdExt;
            Z1518ContratadaUsuario_TmpEstAnl = A1518ContratadaUsuario_TmpEstAnl;
            Z1503ContratadaUsuario_TmpEstExc = A1503ContratadaUsuario_TmpEstExc;
            Z1504ContratadaUsuario_TmpEstCrr = A1504ContratadaUsuario_TmpEstCrr;
            Z66ContratadaUsuario_ContratadaCod = A66ContratadaUsuario_ContratadaCod;
            Z69ContratadaUsuario_UsuarioCod = A69ContratadaUsuario_UsuarioCod;
            Z67ContratadaUsuario_ContratadaPessoaCod = A67ContratadaUsuario_ContratadaPessoaCod;
            Z1228ContratadaUsuario_AreaTrabalhoCod = A1228ContratadaUsuario_AreaTrabalhoCod;
            Z68ContratadaUsuario_ContratadaPessoaNom = A68ContratadaUsuario_ContratadaPessoaNom;
            Z348ContratadaUsuario_ContratadaPessoaCNPJ = A348ContratadaUsuario_ContratadaPessoaCNPJ;
            Z1297ContratadaUsuario_AreaTrabalhoDes = A1297ContratadaUsuario_AreaTrabalhoDes;
            Z1018ContratadaUsuario_UsuarioEhContratada = A1018ContratadaUsuario_UsuarioEhContratada;
            Z1394ContratadaUsuario_UsuarioAtivo = A1394ContratadaUsuario_UsuarioAtivo;
            Z2Usuario_Nome = A2Usuario_Nome;
            Z341Usuario_UserGamGuid = A341Usuario_UserGamGuid;
            Z70ContratadaUsuario_UsuarioPessoaCod = A70ContratadaUsuario_UsuarioPessoaCod;
            Z71ContratadaUsuario_UsuarioPessoaNom = A71ContratadaUsuario_UsuarioPessoaNom;
            Z491ContratadaUsuario_UsuarioPessoaDoc = A491ContratadaUsuario_UsuarioPessoaDoc;
         }
      }

      protected void standaloneNotModal( )
      {
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7ContratadaUsuario_ContratadaCod) )
         {
            A66ContratadaUsuario_ContratadaCod = AV7ContratadaUsuario_ContratadaCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A66ContratadaUsuario_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0)));
         }
         /* Using cursor T000F4 */
         pr_default.execute(2, new Object[] {A66ContratadaUsuario_ContratadaCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contratada Usuario_Contratada'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A67ContratadaUsuario_ContratadaPessoaCod = T000F4_A67ContratadaUsuario_ContratadaPessoaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A67ContratadaUsuario_ContratadaPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A67ContratadaUsuario_ContratadaPessoaCod), 6, 0)));
         n67ContratadaUsuario_ContratadaPessoaCod = T000F4_n67ContratadaUsuario_ContratadaPessoaCod[0];
         A1228ContratadaUsuario_AreaTrabalhoCod = T000F4_A1228ContratadaUsuario_AreaTrabalhoCod[0];
         n1228ContratadaUsuario_AreaTrabalhoCod = T000F4_n1228ContratadaUsuario_AreaTrabalhoCod[0];
         pr_default.close(2);
         /* Using cursor T000F6 */
         pr_default.execute(4, new Object[] {n67ContratadaUsuario_ContratadaPessoaCod, A67ContratadaUsuario_ContratadaPessoaCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A68ContratadaUsuario_ContratadaPessoaNom = T000F6_A68ContratadaUsuario_ContratadaPessoaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A68ContratadaUsuario_ContratadaPessoaNom", A68ContratadaUsuario_ContratadaPessoaNom);
         n68ContratadaUsuario_ContratadaPessoaNom = T000F6_n68ContratadaUsuario_ContratadaPessoaNom[0];
         A348ContratadaUsuario_ContratadaPessoaCNPJ = T000F6_A348ContratadaUsuario_ContratadaPessoaCNPJ[0];
         n348ContratadaUsuario_ContratadaPessoaCNPJ = T000F6_n348ContratadaUsuario_ContratadaPessoaCNPJ[0];
         pr_default.close(4);
         /* Using cursor T000F7 */
         pr_default.execute(5, new Object[] {n1228ContratadaUsuario_AreaTrabalhoCod, A1228ContratadaUsuario_AreaTrabalhoCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Area de Trabalho'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1297ContratadaUsuario_AreaTrabalhoDes = T000F7_A1297ContratadaUsuario_AreaTrabalhoDes[0];
         n1297ContratadaUsuario_AreaTrabalhoDes = T000F7_n1297ContratadaUsuario_AreaTrabalhoDes[0];
         pr_default.close(5);
         if ( ! (0==AV8ContratadaUsuario_UsuarioCod) )
         {
            A69ContratadaUsuario_UsuarioCod = AV8ContratadaUsuario_UsuarioCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A69ContratadaUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0)));
         }
         if ( ! (0==AV8ContratadaUsuario_UsuarioCod) )
         {
            edtContratadaUsuario_UsuarioCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratadaUsuario_UsuarioCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratadaUsuario_UsuarioCod_Enabled), 5, 0)));
         }
         else
         {
            edtContratadaUsuario_UsuarioCod_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratadaUsuario_UsuarioCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratadaUsuario_UsuarioCod_Enabled), 5, 0)));
         }
         if ( ! (0==AV8ContratadaUsuario_UsuarioCod) )
         {
            edtContratadaUsuario_UsuarioCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratadaUsuario_UsuarioCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratadaUsuario_UsuarioCod_Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T000F5 */
            pr_default.execute(3, new Object[] {A69ContratadaUsuario_UsuarioCod});
            A1018ContratadaUsuario_UsuarioEhContratada = T000F5_A1018ContratadaUsuario_UsuarioEhContratada[0];
            n1018ContratadaUsuario_UsuarioEhContratada = T000F5_n1018ContratadaUsuario_UsuarioEhContratada[0];
            A1394ContratadaUsuario_UsuarioAtivo = T000F5_A1394ContratadaUsuario_UsuarioAtivo[0];
            n1394ContratadaUsuario_UsuarioAtivo = T000F5_n1394ContratadaUsuario_UsuarioAtivo[0];
            A2Usuario_Nome = T000F5_A2Usuario_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2Usuario_Nome", A2Usuario_Nome);
            n2Usuario_Nome = T000F5_n2Usuario_Nome[0];
            A341Usuario_UserGamGuid = T000F5_A341Usuario_UserGamGuid[0];
            n341Usuario_UserGamGuid = T000F5_n341Usuario_UserGamGuid[0];
            A70ContratadaUsuario_UsuarioPessoaCod = T000F5_A70ContratadaUsuario_UsuarioPessoaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A70ContratadaUsuario_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A70ContratadaUsuario_UsuarioPessoaCod), 6, 0)));
            n70ContratadaUsuario_UsuarioPessoaCod = T000F5_n70ContratadaUsuario_UsuarioPessoaCod[0];
            pr_default.close(3);
            /* Using cursor T000F8 */
            pr_default.execute(6, new Object[] {n70ContratadaUsuario_UsuarioPessoaCod, A70ContratadaUsuario_UsuarioPessoaCod});
            A71ContratadaUsuario_UsuarioPessoaNom = T000F8_A71ContratadaUsuario_UsuarioPessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A71ContratadaUsuario_UsuarioPessoaNom", A71ContratadaUsuario_UsuarioPessoaNom);
            n71ContratadaUsuario_UsuarioPessoaNom = T000F8_n71ContratadaUsuario_UsuarioPessoaNom[0];
            A491ContratadaUsuario_UsuarioPessoaDoc = T000F8_A491ContratadaUsuario_UsuarioPessoaDoc[0];
            n491ContratadaUsuario_UsuarioPessoaDoc = T000F8_n491ContratadaUsuario_UsuarioPessoaDoc[0];
            pr_default.close(6);
         }
      }

      protected void Load0F16( )
      {
         /* Using cursor T000F9 */
         pr_default.execute(7, new Object[] {A66ContratadaUsuario_ContratadaCod, A69ContratadaUsuario_UsuarioCod});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound16 = 1;
            A1297ContratadaUsuario_AreaTrabalhoDes = T000F9_A1297ContratadaUsuario_AreaTrabalhoDes[0];
            n1297ContratadaUsuario_AreaTrabalhoDes = T000F9_n1297ContratadaUsuario_AreaTrabalhoDes[0];
            A68ContratadaUsuario_ContratadaPessoaNom = T000F9_A68ContratadaUsuario_ContratadaPessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A68ContratadaUsuario_ContratadaPessoaNom", A68ContratadaUsuario_ContratadaPessoaNom);
            n68ContratadaUsuario_ContratadaPessoaNom = T000F9_n68ContratadaUsuario_ContratadaPessoaNom[0];
            A348ContratadaUsuario_ContratadaPessoaCNPJ = T000F9_A348ContratadaUsuario_ContratadaPessoaCNPJ[0];
            n348ContratadaUsuario_ContratadaPessoaCNPJ = T000F9_n348ContratadaUsuario_ContratadaPessoaCNPJ[0];
            A71ContratadaUsuario_UsuarioPessoaNom = T000F9_A71ContratadaUsuario_UsuarioPessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A71ContratadaUsuario_UsuarioPessoaNom", A71ContratadaUsuario_UsuarioPessoaNom);
            n71ContratadaUsuario_UsuarioPessoaNom = T000F9_n71ContratadaUsuario_UsuarioPessoaNom[0];
            A491ContratadaUsuario_UsuarioPessoaDoc = T000F9_A491ContratadaUsuario_UsuarioPessoaDoc[0];
            n491ContratadaUsuario_UsuarioPessoaDoc = T000F9_n491ContratadaUsuario_UsuarioPessoaDoc[0];
            A1018ContratadaUsuario_UsuarioEhContratada = T000F9_A1018ContratadaUsuario_UsuarioEhContratada[0];
            n1018ContratadaUsuario_UsuarioEhContratada = T000F9_n1018ContratadaUsuario_UsuarioEhContratada[0];
            A1394ContratadaUsuario_UsuarioAtivo = T000F9_A1394ContratadaUsuario_UsuarioAtivo[0];
            n1394ContratadaUsuario_UsuarioAtivo = T000F9_n1394ContratadaUsuario_UsuarioAtivo[0];
            A2Usuario_Nome = T000F9_A2Usuario_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2Usuario_Nome", A2Usuario_Nome);
            n2Usuario_Nome = T000F9_n2Usuario_Nome[0];
            A341Usuario_UserGamGuid = T000F9_A341Usuario_UserGamGuid[0];
            n341Usuario_UserGamGuid = T000F9_n341Usuario_UserGamGuid[0];
            A576ContratadaUsuario_CstUntPrdNrm = T000F9_A576ContratadaUsuario_CstUntPrdNrm[0];
            n576ContratadaUsuario_CstUntPrdNrm = T000F9_n576ContratadaUsuario_CstUntPrdNrm[0];
            A577ContratadaUsuario_CstUntPrdExt = T000F9_A577ContratadaUsuario_CstUntPrdExt[0];
            n577ContratadaUsuario_CstUntPrdExt = T000F9_n577ContratadaUsuario_CstUntPrdExt[0];
            A1518ContratadaUsuario_TmpEstAnl = T000F9_A1518ContratadaUsuario_TmpEstAnl[0];
            n1518ContratadaUsuario_TmpEstAnl = T000F9_n1518ContratadaUsuario_TmpEstAnl[0];
            A1503ContratadaUsuario_TmpEstExc = T000F9_A1503ContratadaUsuario_TmpEstExc[0];
            n1503ContratadaUsuario_TmpEstExc = T000F9_n1503ContratadaUsuario_TmpEstExc[0];
            A1504ContratadaUsuario_TmpEstCrr = T000F9_A1504ContratadaUsuario_TmpEstCrr[0];
            n1504ContratadaUsuario_TmpEstCrr = T000F9_n1504ContratadaUsuario_TmpEstCrr[0];
            A67ContratadaUsuario_ContratadaPessoaCod = T000F9_A67ContratadaUsuario_ContratadaPessoaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A67ContratadaUsuario_ContratadaPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A67ContratadaUsuario_ContratadaPessoaCod), 6, 0)));
            n67ContratadaUsuario_ContratadaPessoaCod = T000F9_n67ContratadaUsuario_ContratadaPessoaCod[0];
            A1228ContratadaUsuario_AreaTrabalhoCod = T000F9_A1228ContratadaUsuario_AreaTrabalhoCod[0];
            n1228ContratadaUsuario_AreaTrabalhoCod = T000F9_n1228ContratadaUsuario_AreaTrabalhoCod[0];
            A70ContratadaUsuario_UsuarioPessoaCod = T000F9_A70ContratadaUsuario_UsuarioPessoaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A70ContratadaUsuario_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A70ContratadaUsuario_UsuarioPessoaCod), 6, 0)));
            n70ContratadaUsuario_UsuarioPessoaCod = T000F9_n70ContratadaUsuario_UsuarioPessoaCod[0];
            ZM0F16( -6) ;
         }
         pr_default.close(7);
         OnLoadActions0F16( ) ;
      }

      protected void OnLoadActions0F16( )
      {
      }

      protected void CheckExtendedTable0F16( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T000F5 */
         pr_default.execute(3, new Object[] {A69ContratadaUsuario_UsuarioCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contratada Usuario_Usuario'.", "ForeignKeyNotFound", 1, "CONTRATADAUSUARIO_USUARIOCOD");
            AnyError = 1;
            GX_FocusControl = edtContratadaUsuario_UsuarioCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1018ContratadaUsuario_UsuarioEhContratada = T000F5_A1018ContratadaUsuario_UsuarioEhContratada[0];
         n1018ContratadaUsuario_UsuarioEhContratada = T000F5_n1018ContratadaUsuario_UsuarioEhContratada[0];
         A1394ContratadaUsuario_UsuarioAtivo = T000F5_A1394ContratadaUsuario_UsuarioAtivo[0];
         n1394ContratadaUsuario_UsuarioAtivo = T000F5_n1394ContratadaUsuario_UsuarioAtivo[0];
         A2Usuario_Nome = T000F5_A2Usuario_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2Usuario_Nome", A2Usuario_Nome);
         n2Usuario_Nome = T000F5_n2Usuario_Nome[0];
         A341Usuario_UserGamGuid = T000F5_A341Usuario_UserGamGuid[0];
         n341Usuario_UserGamGuid = T000F5_n341Usuario_UserGamGuid[0];
         A70ContratadaUsuario_UsuarioPessoaCod = T000F5_A70ContratadaUsuario_UsuarioPessoaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A70ContratadaUsuario_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A70ContratadaUsuario_UsuarioPessoaCod), 6, 0)));
         n70ContratadaUsuario_UsuarioPessoaCod = T000F5_n70ContratadaUsuario_UsuarioPessoaCod[0];
         pr_default.close(3);
         /* Using cursor T000F8 */
         pr_default.execute(6, new Object[] {n70ContratadaUsuario_UsuarioPessoaCod, A70ContratadaUsuario_UsuarioPessoaCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A71ContratadaUsuario_UsuarioPessoaNom = T000F8_A71ContratadaUsuario_UsuarioPessoaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A71ContratadaUsuario_UsuarioPessoaNom", A71ContratadaUsuario_UsuarioPessoaNom);
         n71ContratadaUsuario_UsuarioPessoaNom = T000F8_n71ContratadaUsuario_UsuarioPessoaNom[0];
         A491ContratadaUsuario_UsuarioPessoaDoc = T000F8_A491ContratadaUsuario_UsuarioPessoaDoc[0];
         n491ContratadaUsuario_UsuarioPessoaDoc = T000F8_n491ContratadaUsuario_UsuarioPessoaDoc[0];
         pr_default.close(6);
      }

      protected void CloseExtendedTableCursors0F16( )
      {
         pr_default.close(3);
         pr_default.close(6);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_8( int A69ContratadaUsuario_UsuarioCod )
      {
         /* Using cursor T000F10 */
         pr_default.execute(8, new Object[] {A69ContratadaUsuario_UsuarioCod});
         if ( (pr_default.getStatus(8) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contratada Usuario_Usuario'.", "ForeignKeyNotFound", 1, "CONTRATADAUSUARIO_USUARIOCOD");
            AnyError = 1;
            GX_FocusControl = edtContratadaUsuario_UsuarioCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1018ContratadaUsuario_UsuarioEhContratada = T000F10_A1018ContratadaUsuario_UsuarioEhContratada[0];
         n1018ContratadaUsuario_UsuarioEhContratada = T000F10_n1018ContratadaUsuario_UsuarioEhContratada[0];
         A1394ContratadaUsuario_UsuarioAtivo = T000F10_A1394ContratadaUsuario_UsuarioAtivo[0];
         n1394ContratadaUsuario_UsuarioAtivo = T000F10_n1394ContratadaUsuario_UsuarioAtivo[0];
         A2Usuario_Nome = T000F10_A2Usuario_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2Usuario_Nome", A2Usuario_Nome);
         n2Usuario_Nome = T000F10_n2Usuario_Nome[0];
         A341Usuario_UserGamGuid = T000F10_A341Usuario_UserGamGuid[0];
         n341Usuario_UserGamGuid = T000F10_n341Usuario_UserGamGuid[0];
         A70ContratadaUsuario_UsuarioPessoaCod = T000F10_A70ContratadaUsuario_UsuarioPessoaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A70ContratadaUsuario_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A70ContratadaUsuario_UsuarioPessoaCod), 6, 0)));
         n70ContratadaUsuario_UsuarioPessoaCod = T000F10_n70ContratadaUsuario_UsuarioPessoaCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.BoolToStr( A1018ContratadaUsuario_UsuarioEhContratada))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.BoolToStr( A1394ContratadaUsuario_UsuarioAtivo))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A2Usuario_Nome))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A341Usuario_UserGamGuid))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A70ContratadaUsuario_UsuarioPessoaCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(8) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(8);
      }

      protected void gxLoad_11( int A70ContratadaUsuario_UsuarioPessoaCod )
      {
         /* Using cursor T000F11 */
         pr_default.execute(9, new Object[] {n70ContratadaUsuario_UsuarioPessoaCod, A70ContratadaUsuario_UsuarioPessoaCod});
         if ( (pr_default.getStatus(9) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A71ContratadaUsuario_UsuarioPessoaNom = T000F11_A71ContratadaUsuario_UsuarioPessoaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A71ContratadaUsuario_UsuarioPessoaNom", A71ContratadaUsuario_UsuarioPessoaNom);
         n71ContratadaUsuario_UsuarioPessoaNom = T000F11_n71ContratadaUsuario_UsuarioPessoaNom[0];
         A491ContratadaUsuario_UsuarioPessoaDoc = T000F11_A491ContratadaUsuario_UsuarioPessoaDoc[0];
         n491ContratadaUsuario_UsuarioPessoaDoc = T000F11_n491ContratadaUsuario_UsuarioPessoaDoc[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A71ContratadaUsuario_UsuarioPessoaNom))+"\""+","+"\""+GXUtil.EncodeJSConstant( A491ContratadaUsuario_UsuarioPessoaDoc)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(9) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(9);
      }

      protected void GetKey0F16( )
      {
         /* Using cursor T000F12 */
         pr_default.execute(10, new Object[] {A66ContratadaUsuario_ContratadaCod, A69ContratadaUsuario_UsuarioCod});
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound16 = 1;
         }
         else
         {
            RcdFound16 = 0;
         }
         pr_default.close(10);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T000F3 */
         pr_default.execute(1, new Object[] {A66ContratadaUsuario_ContratadaCod, A69ContratadaUsuario_UsuarioCod});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM0F16( 6) ;
            RcdFound16 = 1;
            A576ContratadaUsuario_CstUntPrdNrm = T000F3_A576ContratadaUsuario_CstUntPrdNrm[0];
            n576ContratadaUsuario_CstUntPrdNrm = T000F3_n576ContratadaUsuario_CstUntPrdNrm[0];
            A577ContratadaUsuario_CstUntPrdExt = T000F3_A577ContratadaUsuario_CstUntPrdExt[0];
            n577ContratadaUsuario_CstUntPrdExt = T000F3_n577ContratadaUsuario_CstUntPrdExt[0];
            A1518ContratadaUsuario_TmpEstAnl = T000F3_A1518ContratadaUsuario_TmpEstAnl[0];
            n1518ContratadaUsuario_TmpEstAnl = T000F3_n1518ContratadaUsuario_TmpEstAnl[0];
            A1503ContratadaUsuario_TmpEstExc = T000F3_A1503ContratadaUsuario_TmpEstExc[0];
            n1503ContratadaUsuario_TmpEstExc = T000F3_n1503ContratadaUsuario_TmpEstExc[0];
            A1504ContratadaUsuario_TmpEstCrr = T000F3_A1504ContratadaUsuario_TmpEstCrr[0];
            n1504ContratadaUsuario_TmpEstCrr = T000F3_n1504ContratadaUsuario_TmpEstCrr[0];
            A66ContratadaUsuario_ContratadaCod = T000F3_A66ContratadaUsuario_ContratadaCod[0];
            A69ContratadaUsuario_UsuarioCod = T000F3_A69ContratadaUsuario_UsuarioCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A69ContratadaUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0)));
            Z66ContratadaUsuario_ContratadaCod = A66ContratadaUsuario_ContratadaCod;
            Z69ContratadaUsuario_UsuarioCod = A69ContratadaUsuario_UsuarioCod;
            sMode16 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load0F16( ) ;
            if ( AnyError == 1 )
            {
               RcdFound16 = 0;
               InitializeNonKey0F16( ) ;
            }
            Gx_mode = sMode16;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound16 = 0;
            InitializeNonKey0F16( ) ;
            sMode16 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode16;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey0F16( ) ;
         if ( RcdFound16 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound16 = 0;
         /* Using cursor T000F13 */
         pr_default.execute(11, new Object[] {A66ContratadaUsuario_ContratadaCod, A69ContratadaUsuario_UsuarioCod});
         if ( (pr_default.getStatus(11) != 101) )
         {
            while ( (pr_default.getStatus(11) != 101) && ( ( T000F13_A66ContratadaUsuario_ContratadaCod[0] < A66ContratadaUsuario_ContratadaCod ) || ( T000F13_A66ContratadaUsuario_ContratadaCod[0] == A66ContratadaUsuario_ContratadaCod ) && ( T000F13_A69ContratadaUsuario_UsuarioCod[0] < A69ContratadaUsuario_UsuarioCod ) ) )
            {
               pr_default.readNext(11);
            }
            if ( (pr_default.getStatus(11) != 101) && ( ( T000F13_A66ContratadaUsuario_ContratadaCod[0] > A66ContratadaUsuario_ContratadaCod ) || ( T000F13_A66ContratadaUsuario_ContratadaCod[0] == A66ContratadaUsuario_ContratadaCod ) && ( T000F13_A69ContratadaUsuario_UsuarioCod[0] > A69ContratadaUsuario_UsuarioCod ) ) )
            {
               A66ContratadaUsuario_ContratadaCod = T000F13_A66ContratadaUsuario_ContratadaCod[0];
               A69ContratadaUsuario_UsuarioCod = T000F13_A69ContratadaUsuario_UsuarioCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A69ContratadaUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0)));
               RcdFound16 = 1;
            }
         }
         pr_default.close(11);
      }

      protected void move_previous( )
      {
         RcdFound16 = 0;
         /* Using cursor T000F14 */
         pr_default.execute(12, new Object[] {A66ContratadaUsuario_ContratadaCod, A69ContratadaUsuario_UsuarioCod});
         if ( (pr_default.getStatus(12) != 101) )
         {
            while ( (pr_default.getStatus(12) != 101) && ( ( T000F14_A66ContratadaUsuario_ContratadaCod[0] > A66ContratadaUsuario_ContratadaCod ) || ( T000F14_A66ContratadaUsuario_ContratadaCod[0] == A66ContratadaUsuario_ContratadaCod ) && ( T000F14_A69ContratadaUsuario_UsuarioCod[0] > A69ContratadaUsuario_UsuarioCod ) ) )
            {
               pr_default.readNext(12);
            }
            if ( (pr_default.getStatus(12) != 101) && ( ( T000F14_A66ContratadaUsuario_ContratadaCod[0] < A66ContratadaUsuario_ContratadaCod ) || ( T000F14_A66ContratadaUsuario_ContratadaCod[0] == A66ContratadaUsuario_ContratadaCod ) && ( T000F14_A69ContratadaUsuario_UsuarioCod[0] < A69ContratadaUsuario_UsuarioCod ) ) )
            {
               A66ContratadaUsuario_ContratadaCod = T000F14_A66ContratadaUsuario_ContratadaCod[0];
               A69ContratadaUsuario_UsuarioCod = T000F14_A69ContratadaUsuario_UsuarioCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A69ContratadaUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0)));
               RcdFound16 = 1;
            }
         }
         pr_default.close(12);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey0F16( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtContratadaUsuario_UsuarioCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert0F16( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound16 == 1 )
            {
               if ( ( A66ContratadaUsuario_ContratadaCod != Z66ContratadaUsuario_ContratadaCod ) || ( A69ContratadaUsuario_UsuarioCod != Z69ContratadaUsuario_UsuarioCod ) )
               {
                  A66ContratadaUsuario_ContratadaCod = Z66ContratadaUsuario_ContratadaCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A66ContratadaUsuario_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0)));
                  A69ContratadaUsuario_UsuarioCod = Z69ContratadaUsuario_UsuarioCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A69ContratadaUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CONTRATADAUSUARIO_USUARIOCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContratadaUsuario_UsuarioCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtContratadaUsuario_UsuarioCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update0F16( ) ;
                  GX_FocusControl = edtContratadaUsuario_UsuarioCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( ( A66ContratadaUsuario_ContratadaCod != Z66ContratadaUsuario_ContratadaCod ) || ( A69ContratadaUsuario_UsuarioCod != Z69ContratadaUsuario_UsuarioCod ) )
               {
                  /* Insert record */
                  GX_FocusControl = edtContratadaUsuario_UsuarioCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert0F16( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTRATADAUSUARIO_USUARIOCOD");
                     AnyError = 1;
                     GX_FocusControl = edtContratadaUsuario_UsuarioCod_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtContratadaUsuario_UsuarioCod_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert0F16( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( ( A66ContratadaUsuario_ContratadaCod != Z66ContratadaUsuario_ContratadaCod ) || ( A69ContratadaUsuario_UsuarioCod != Z69ContratadaUsuario_UsuarioCod ) )
         {
            A66ContratadaUsuario_ContratadaCod = Z66ContratadaUsuario_ContratadaCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A66ContratadaUsuario_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0)));
            A69ContratadaUsuario_UsuarioCod = Z69ContratadaUsuario_UsuarioCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A69ContratadaUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CONTRATADAUSUARIO_USUARIOCOD");
            AnyError = 1;
            GX_FocusControl = edtContratadaUsuario_UsuarioCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtContratadaUsuario_UsuarioCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency0F16( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T000F2 */
            pr_default.execute(0, new Object[] {A66ContratadaUsuario_ContratadaCod, A69ContratadaUsuario_UsuarioCod});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratadaUsuario"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z576ContratadaUsuario_CstUntPrdNrm != T000F2_A576ContratadaUsuario_CstUntPrdNrm[0] ) || ( Z577ContratadaUsuario_CstUntPrdExt != T000F2_A577ContratadaUsuario_CstUntPrdExt[0] ) || ( Z1518ContratadaUsuario_TmpEstAnl != T000F2_A1518ContratadaUsuario_TmpEstAnl[0] ) || ( Z1503ContratadaUsuario_TmpEstExc != T000F2_A1503ContratadaUsuario_TmpEstExc[0] ) || ( Z1504ContratadaUsuario_TmpEstCrr != T000F2_A1504ContratadaUsuario_TmpEstCrr[0] ) )
            {
               if ( Z576ContratadaUsuario_CstUntPrdNrm != T000F2_A576ContratadaUsuario_CstUntPrdNrm[0] )
               {
                  GXUtil.WriteLog("contratadausuario:[seudo value changed for attri]"+"ContratadaUsuario_CstUntPrdNrm");
                  GXUtil.WriteLogRaw("Old: ",Z576ContratadaUsuario_CstUntPrdNrm);
                  GXUtil.WriteLogRaw("Current: ",T000F2_A576ContratadaUsuario_CstUntPrdNrm[0]);
               }
               if ( Z577ContratadaUsuario_CstUntPrdExt != T000F2_A577ContratadaUsuario_CstUntPrdExt[0] )
               {
                  GXUtil.WriteLog("contratadausuario:[seudo value changed for attri]"+"ContratadaUsuario_CstUntPrdExt");
                  GXUtil.WriteLogRaw("Old: ",Z577ContratadaUsuario_CstUntPrdExt);
                  GXUtil.WriteLogRaw("Current: ",T000F2_A577ContratadaUsuario_CstUntPrdExt[0]);
               }
               if ( Z1518ContratadaUsuario_TmpEstAnl != T000F2_A1518ContratadaUsuario_TmpEstAnl[0] )
               {
                  GXUtil.WriteLog("contratadausuario:[seudo value changed for attri]"+"ContratadaUsuario_TmpEstAnl");
                  GXUtil.WriteLogRaw("Old: ",Z1518ContratadaUsuario_TmpEstAnl);
                  GXUtil.WriteLogRaw("Current: ",T000F2_A1518ContratadaUsuario_TmpEstAnl[0]);
               }
               if ( Z1503ContratadaUsuario_TmpEstExc != T000F2_A1503ContratadaUsuario_TmpEstExc[0] )
               {
                  GXUtil.WriteLog("contratadausuario:[seudo value changed for attri]"+"ContratadaUsuario_TmpEstExc");
                  GXUtil.WriteLogRaw("Old: ",Z1503ContratadaUsuario_TmpEstExc);
                  GXUtil.WriteLogRaw("Current: ",T000F2_A1503ContratadaUsuario_TmpEstExc[0]);
               }
               if ( Z1504ContratadaUsuario_TmpEstCrr != T000F2_A1504ContratadaUsuario_TmpEstCrr[0] )
               {
                  GXUtil.WriteLog("contratadausuario:[seudo value changed for attri]"+"ContratadaUsuario_TmpEstCrr");
                  GXUtil.WriteLogRaw("Old: ",Z1504ContratadaUsuario_TmpEstCrr);
                  GXUtil.WriteLogRaw("Current: ",T000F2_A1504ContratadaUsuario_TmpEstCrr[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContratadaUsuario"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0F16( )
      {
         BeforeValidate0F16( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0F16( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0F16( 0) ;
            CheckOptimisticConcurrency0F16( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0F16( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0F16( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000F15 */
                     pr_default.execute(13, new Object[] {n576ContratadaUsuario_CstUntPrdNrm, A576ContratadaUsuario_CstUntPrdNrm, n577ContratadaUsuario_CstUntPrdExt, A577ContratadaUsuario_CstUntPrdExt, n1518ContratadaUsuario_TmpEstAnl, A1518ContratadaUsuario_TmpEstAnl, n1503ContratadaUsuario_TmpEstExc, A1503ContratadaUsuario_TmpEstExc, n1504ContratadaUsuario_TmpEstCrr, A1504ContratadaUsuario_TmpEstCrr, A66ContratadaUsuario_ContratadaCod, A69ContratadaUsuario_UsuarioCod});
                     pr_default.close(13);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratadaUsuario") ;
                     if ( (pr_default.getStatus(13) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption0F0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0F16( ) ;
            }
            EndLevel0F16( ) ;
         }
         CloseExtendedTableCursors0F16( ) ;
      }

      protected void Update0F16( )
      {
         BeforeValidate0F16( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0F16( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0F16( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0F16( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0F16( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000F16 */
                     pr_default.execute(14, new Object[] {n576ContratadaUsuario_CstUntPrdNrm, A576ContratadaUsuario_CstUntPrdNrm, n577ContratadaUsuario_CstUntPrdExt, A577ContratadaUsuario_CstUntPrdExt, n1518ContratadaUsuario_TmpEstAnl, A1518ContratadaUsuario_TmpEstAnl, n1503ContratadaUsuario_TmpEstExc, A1503ContratadaUsuario_TmpEstExc, n1504ContratadaUsuario_TmpEstCrr, A1504ContratadaUsuario_TmpEstCrr, A66ContratadaUsuario_ContratadaCod, A69ContratadaUsuario_UsuarioCod});
                     pr_default.close(14);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratadaUsuario") ;
                     if ( (pr_default.getStatus(14) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratadaUsuario"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0F16( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0F16( ) ;
         }
         CloseExtendedTableCursors0F16( ) ;
      }

      protected void DeferredUpdate0F16( )
      {
      }

      protected void delete( )
      {
         BeforeValidate0F16( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0F16( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0F16( ) ;
            AfterConfirm0F16( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0F16( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000F17 */
                  pr_default.execute(15, new Object[] {A66ContratadaUsuario_ContratadaCod, A69ContratadaUsuario_UsuarioCod});
                  pr_default.close(15);
                  dsDefault.SmartCacheProvider.SetUpdated("ContratadaUsuario") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode16 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel0F16( ) ;
         Gx_mode = sMode16;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls0F16( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T000F18 */
            pr_default.execute(16, new Object[] {A69ContratadaUsuario_UsuarioCod});
            A1018ContratadaUsuario_UsuarioEhContratada = T000F18_A1018ContratadaUsuario_UsuarioEhContratada[0];
            n1018ContratadaUsuario_UsuarioEhContratada = T000F18_n1018ContratadaUsuario_UsuarioEhContratada[0];
            A1394ContratadaUsuario_UsuarioAtivo = T000F18_A1394ContratadaUsuario_UsuarioAtivo[0];
            n1394ContratadaUsuario_UsuarioAtivo = T000F18_n1394ContratadaUsuario_UsuarioAtivo[0];
            A2Usuario_Nome = T000F18_A2Usuario_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2Usuario_Nome", A2Usuario_Nome);
            n2Usuario_Nome = T000F18_n2Usuario_Nome[0];
            A341Usuario_UserGamGuid = T000F18_A341Usuario_UserGamGuid[0];
            n341Usuario_UserGamGuid = T000F18_n341Usuario_UserGamGuid[0];
            A70ContratadaUsuario_UsuarioPessoaCod = T000F18_A70ContratadaUsuario_UsuarioPessoaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A70ContratadaUsuario_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A70ContratadaUsuario_UsuarioPessoaCod), 6, 0)));
            n70ContratadaUsuario_UsuarioPessoaCod = T000F18_n70ContratadaUsuario_UsuarioPessoaCod[0];
            pr_default.close(16);
            /* Using cursor T000F19 */
            pr_default.execute(17, new Object[] {n70ContratadaUsuario_UsuarioPessoaCod, A70ContratadaUsuario_UsuarioPessoaCod});
            A71ContratadaUsuario_UsuarioPessoaNom = T000F19_A71ContratadaUsuario_UsuarioPessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A71ContratadaUsuario_UsuarioPessoaNom", A71ContratadaUsuario_UsuarioPessoaNom);
            n71ContratadaUsuario_UsuarioPessoaNom = T000F19_n71ContratadaUsuario_UsuarioPessoaNom[0];
            A491ContratadaUsuario_UsuarioPessoaDoc = T000F19_A491ContratadaUsuario_UsuarioPessoaDoc[0];
            n491ContratadaUsuario_UsuarioPessoaDoc = T000F19_n491ContratadaUsuario_UsuarioPessoaDoc[0];
            pr_default.close(17);
         }
      }

      protected void EndLevel0F16( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete0F16( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(16);
            pr_default.close(17);
            context.CommitDataStores( "ContratadaUsuario");
            if ( AnyError == 0 )
            {
               ConfirmValues0F0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(16);
            pr_default.close(17);
            context.RollbackDataStores( "ContratadaUsuario");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart0F16( )
      {
         /* Scan By routine */
         /* Using cursor T000F20 */
         pr_default.execute(18);
         RcdFound16 = 0;
         if ( (pr_default.getStatus(18) != 101) )
         {
            RcdFound16 = 1;
            A66ContratadaUsuario_ContratadaCod = T000F20_A66ContratadaUsuario_ContratadaCod[0];
            A69ContratadaUsuario_UsuarioCod = T000F20_A69ContratadaUsuario_UsuarioCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A69ContratadaUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext0F16( )
      {
         /* Scan next routine */
         pr_default.readNext(18);
         RcdFound16 = 0;
         if ( (pr_default.getStatus(18) != 101) )
         {
            RcdFound16 = 1;
            A66ContratadaUsuario_ContratadaCod = T000F20_A66ContratadaUsuario_ContratadaCod[0];
            A69ContratadaUsuario_UsuarioCod = T000F20_A69ContratadaUsuario_UsuarioCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A69ContratadaUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0)));
         }
      }

      protected void ScanEnd0F16( )
      {
         pr_default.close(18);
      }

      protected void AfterConfirm0F16( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0F16( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0F16( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0F16( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0F16( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0F16( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0F16( )
      {
         edtUsuario_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_Nome_Enabled), 5, 0)));
         edtContratadaUsuario_UsuarioPessoaNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratadaUsuario_UsuarioPessoaNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratadaUsuario_UsuarioPessoaNom_Enabled), 5, 0)));
         edtContratadaUsuario_ContratadaPessoaNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratadaUsuario_ContratadaPessoaNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratadaUsuario_ContratadaPessoaNom_Enabled), 5, 0)));
         edtContratadaUsuario_ContratadaPessoaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratadaUsuario_ContratadaPessoaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratadaUsuario_ContratadaPessoaCod_Enabled), 5, 0)));
         edtContratadaUsuario_UsuarioCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratadaUsuario_UsuarioCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratadaUsuario_UsuarioCod_Enabled), 5, 0)));
         edtContratadaUsuario_UsuarioPessoaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratadaUsuario_UsuarioPessoaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratadaUsuario_UsuarioPessoaCod_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues0F0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202052118101656");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratadausuario.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContratadaUsuario_ContratadaCod) + "," + UrlEncode("" +AV8ContratadaUsuario_UsuarioCod)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z66ContratadaUsuario_ContratadaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z66ContratadaUsuario_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z69ContratadaUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z69ContratadaUsuario_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z576ContratadaUsuario_CstUntPrdNrm", StringUtil.LTrim( StringUtil.NToC( Z576ContratadaUsuario_CstUntPrdNrm, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z577ContratadaUsuario_CstUntPrdExt", StringUtil.LTrim( StringUtil.NToC( Z577ContratadaUsuario_CstUntPrdExt, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1518ContratadaUsuario_TmpEstAnl", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1518ContratadaUsuario_TmpEstAnl), 8, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1503ContratadaUsuario_TmpEstExc", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1503ContratadaUsuario_TmpEstExc), 8, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1504ContratadaUsuario_TmpEstCrr", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1504ContratadaUsuario_TmpEstCrr), 8, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV10TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV10TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vCONTRATADAUSUARIO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContratadaUsuario_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATADAUSUARIO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8ContratadaUsuario_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_CSTUNTPRDNRM", StringUtil.LTrim( StringUtil.NToC( A576ContratadaUsuario_CstUntPrdNrm, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_CSTUNTPRDEXT", StringUtil.LTrim( StringUtil.NToC( A577ContratadaUsuario_CstUntPrdExt, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_TMPESTANL", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1518ContratadaUsuario_TmpEstAnl), 8, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_TMPESTEXC", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1503ContratadaUsuario_TmpEstExc), 8, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_TMPESTCRR", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1504ContratadaUsuario_TmpEstCrr), 8, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1228ContratadaUsuario_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATADAUSUARIO_USUARIOEHCONTRATADA", A1018ContratadaUsuario_UsuarioEhContratada);
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATADAUSUARIO_USUARIOATIVO", A1394ContratadaUsuario_UsuarioAtivo);
         GxWebStd.gx_hidden_field( context, "USUARIO_USERGAMGUID", StringUtil.RTrim( A341Usuario_UserGamGuid));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_CONTRATADAPESSOACNPJ", A348ContratadaUsuario_ContratadaPessoaCNPJ);
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_AREATRABALHODES", A1297ContratadaUsuario_AreaTrabalhoDes);
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_USUARIOPESSOADOC", A491ContratadaUsuario_UsuarioPessoaDoc);
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATADAUSUARIO_CONTRATADACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratadaUsuario_ContratadaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATADAUSUARIO_USUARIOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8ContratadaUsuario_UsuarioCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ContratadaUsuario";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A66ContratadaUsuario_ContratadaCod), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A576ContratadaUsuario_CstUntPrdNrm, "ZZZ,ZZZ,ZZZ,ZZ9.99");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A577ContratadaUsuario_CstUntPrdExt, "ZZZ,ZZZ,ZZZ,ZZ9.99");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1518ContratadaUsuario_TmpEstAnl), "ZZZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1503ContratadaUsuario_TmpEstExc), "ZZZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1504ContratadaUsuario_TmpEstCrr), "ZZZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contratadausuario:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("contratadausuario:[SendSecurityCheck value for]"+"ContratadaUsuario_ContratadaCod:"+context.localUtil.Format( (decimal)(A66ContratadaUsuario_ContratadaCod), "ZZZZZ9"));
         GXUtil.WriteLog("contratadausuario:[SendSecurityCheck value for]"+"ContratadaUsuario_CstUntPrdNrm:"+context.localUtil.Format( A576ContratadaUsuario_CstUntPrdNrm, "ZZZ,ZZZ,ZZZ,ZZ9.99"));
         GXUtil.WriteLog("contratadausuario:[SendSecurityCheck value for]"+"ContratadaUsuario_CstUntPrdExt:"+context.localUtil.Format( A577ContratadaUsuario_CstUntPrdExt, "ZZZ,ZZZ,ZZZ,ZZ9.99"));
         GXUtil.WriteLog("contratadausuario:[SendSecurityCheck value for]"+"ContratadaUsuario_TmpEstAnl:"+context.localUtil.Format( (decimal)(A1518ContratadaUsuario_TmpEstAnl), "ZZZZZZZ9"));
         GXUtil.WriteLog("contratadausuario:[SendSecurityCheck value for]"+"ContratadaUsuario_TmpEstExc:"+context.localUtil.Format( (decimal)(A1503ContratadaUsuario_TmpEstExc), "ZZZZZZZ9"));
         GXUtil.WriteLog("contratadausuario:[SendSecurityCheck value for]"+"ContratadaUsuario_TmpEstCrr:"+context.localUtil.Format( (decimal)(A1504ContratadaUsuario_TmpEstCrr), "ZZZZZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contratadausuario.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContratadaUsuario_ContratadaCod) + "," + UrlEncode("" +AV8ContratadaUsuario_UsuarioCod) ;
      }

      public override String GetPgmname( )
      {
         return "ContratadaUsuario" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contratada Usuario" ;
      }

      protected void InitializeNonKey0F16( )
      {
         A70ContratadaUsuario_UsuarioPessoaCod = 0;
         n70ContratadaUsuario_UsuarioPessoaCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A70ContratadaUsuario_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A70ContratadaUsuario_UsuarioPessoaCod), 6, 0)));
         A71ContratadaUsuario_UsuarioPessoaNom = "";
         n71ContratadaUsuario_UsuarioPessoaNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A71ContratadaUsuario_UsuarioPessoaNom", A71ContratadaUsuario_UsuarioPessoaNom);
         A491ContratadaUsuario_UsuarioPessoaDoc = "";
         n491ContratadaUsuario_UsuarioPessoaDoc = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A491ContratadaUsuario_UsuarioPessoaDoc", A491ContratadaUsuario_UsuarioPessoaDoc);
         A1018ContratadaUsuario_UsuarioEhContratada = false;
         n1018ContratadaUsuario_UsuarioEhContratada = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1018ContratadaUsuario_UsuarioEhContratada", A1018ContratadaUsuario_UsuarioEhContratada);
         A1394ContratadaUsuario_UsuarioAtivo = false;
         n1394ContratadaUsuario_UsuarioAtivo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1394ContratadaUsuario_UsuarioAtivo", A1394ContratadaUsuario_UsuarioAtivo);
         A2Usuario_Nome = "";
         n2Usuario_Nome = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2Usuario_Nome", A2Usuario_Nome);
         A341Usuario_UserGamGuid = "";
         n341Usuario_UserGamGuid = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A341Usuario_UserGamGuid", A341Usuario_UserGamGuid);
         A576ContratadaUsuario_CstUntPrdNrm = 0;
         n576ContratadaUsuario_CstUntPrdNrm = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A576ContratadaUsuario_CstUntPrdNrm", StringUtil.LTrim( StringUtil.Str( A576ContratadaUsuario_CstUntPrdNrm, 18, 5)));
         A577ContratadaUsuario_CstUntPrdExt = 0;
         n577ContratadaUsuario_CstUntPrdExt = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A577ContratadaUsuario_CstUntPrdExt", StringUtil.LTrim( StringUtil.Str( A577ContratadaUsuario_CstUntPrdExt, 18, 5)));
         A1518ContratadaUsuario_TmpEstAnl = 0;
         n1518ContratadaUsuario_TmpEstAnl = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1518ContratadaUsuario_TmpEstAnl", StringUtil.LTrim( StringUtil.Str( (decimal)(A1518ContratadaUsuario_TmpEstAnl), 8, 0)));
         A1503ContratadaUsuario_TmpEstExc = 0;
         n1503ContratadaUsuario_TmpEstExc = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1503ContratadaUsuario_TmpEstExc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1503ContratadaUsuario_TmpEstExc), 8, 0)));
         A1504ContratadaUsuario_TmpEstCrr = 0;
         n1504ContratadaUsuario_TmpEstCrr = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1504ContratadaUsuario_TmpEstCrr", StringUtil.LTrim( StringUtil.Str( (decimal)(A1504ContratadaUsuario_TmpEstCrr), 8, 0)));
         Z576ContratadaUsuario_CstUntPrdNrm = 0;
         Z577ContratadaUsuario_CstUntPrdExt = 0;
         Z1518ContratadaUsuario_TmpEstAnl = 0;
         Z1503ContratadaUsuario_TmpEstExc = 0;
         Z1504ContratadaUsuario_TmpEstCrr = 0;
      }

      protected void InitAll0F16( )
      {
         A66ContratadaUsuario_ContratadaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A66ContratadaUsuario_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0)));
         A69ContratadaUsuario_UsuarioCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A69ContratadaUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0)));
         InitializeNonKey0F16( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202052118101672");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contratadausuario.js", "?202052118101672");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockusuario_nome_Internalname = "TEXTBLOCKUSUARIO_NOME";
         edtUsuario_Nome_Internalname = "USUARIO_NOME";
         lblTextblockcontratadausuario_usuariopessoanom_Internalname = "TEXTBLOCKCONTRATADAUSUARIO_USUARIOPESSOANOM";
         edtContratadaUsuario_UsuarioPessoaNom_Internalname = "CONTRATADAUSUARIO_USUARIOPESSOANOM";
         lblTextblockcontratadausuario_contratadapessoanom_Internalname = "TEXTBLOCKCONTRATADAUSUARIO_CONTRATADAPESSOANOM";
         edtContratadaUsuario_ContratadaPessoaNom_Internalname = "CONTRATADAUSUARIO_CONTRATADAPESSOANOM";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtContratadaUsuario_ContratadaPessoaCod_Internalname = "CONTRATADAUSUARIO_CONTRATADAPESSOACOD";
         edtContratadaUsuario_UsuarioCod_Internalname = "CONTRATADAUSUARIO_USUARIOCOD";
         edtContratadaUsuario_UsuarioPessoaCod_Internalname = "CONTRATADAUSUARIO_USUARIOPESSOACOD";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Usu�rio da Contratada";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Contratada Usuario";
         edtContratadaUsuario_ContratadaPessoaNom_Jsonclick = "";
         edtContratadaUsuario_ContratadaPessoaNom_Enabled = 0;
         edtContratadaUsuario_UsuarioPessoaNom_Jsonclick = "";
         edtContratadaUsuario_UsuarioPessoaNom_Enabled = 0;
         edtUsuario_Nome_Jsonclick = "";
         edtUsuario_Nome_Enabled = 0;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtContratadaUsuario_UsuarioPessoaCod_Jsonclick = "";
         edtContratadaUsuario_UsuarioPessoaCod_Enabled = 0;
         edtContratadaUsuario_UsuarioPessoaCod_Visible = 1;
         edtContratadaUsuario_UsuarioCod_Jsonclick = "";
         edtContratadaUsuario_UsuarioCod_Enabled = 1;
         edtContratadaUsuario_UsuarioCod_Visible = 1;
         edtContratadaUsuario_ContratadaPessoaCod_Jsonclick = "";
         edtContratadaUsuario_ContratadaPessoaCod_Enabled = 0;
         edtContratadaUsuario_ContratadaPessoaCod_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public void Valid_Contratadausuario_usuariocod( int GX_Parm1 ,
                                                      int GX_Parm2 ,
                                                      bool GX_Parm3 ,
                                                      bool GX_Parm4 ,
                                                      String GX_Parm5 ,
                                                      String GX_Parm6 ,
                                                      String GX_Parm7 ,
                                                      String GX_Parm8 )
      {
         A69ContratadaUsuario_UsuarioCod = GX_Parm1;
         A70ContratadaUsuario_UsuarioPessoaCod = GX_Parm2;
         n70ContratadaUsuario_UsuarioPessoaCod = false;
         A1018ContratadaUsuario_UsuarioEhContratada = GX_Parm3;
         n1018ContratadaUsuario_UsuarioEhContratada = false;
         A1394ContratadaUsuario_UsuarioAtivo = GX_Parm4;
         n1394ContratadaUsuario_UsuarioAtivo = false;
         A2Usuario_Nome = GX_Parm5;
         n2Usuario_Nome = false;
         A341Usuario_UserGamGuid = GX_Parm6;
         n341Usuario_UserGamGuid = false;
         A71ContratadaUsuario_UsuarioPessoaNom = GX_Parm7;
         n71ContratadaUsuario_UsuarioPessoaNom = false;
         A491ContratadaUsuario_UsuarioPessoaDoc = GX_Parm8;
         n491ContratadaUsuario_UsuarioPessoaDoc = false;
         /* Using cursor T000F18 */
         pr_default.execute(16, new Object[] {A69ContratadaUsuario_UsuarioCod});
         if ( (pr_default.getStatus(16) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contratada Usuario_Usuario'.", "ForeignKeyNotFound", 1, "CONTRATADAUSUARIO_USUARIOCOD");
            AnyError = 1;
            GX_FocusControl = edtContratadaUsuario_UsuarioCod_Internalname;
         }
         A1018ContratadaUsuario_UsuarioEhContratada = T000F18_A1018ContratadaUsuario_UsuarioEhContratada[0];
         n1018ContratadaUsuario_UsuarioEhContratada = T000F18_n1018ContratadaUsuario_UsuarioEhContratada[0];
         A1394ContratadaUsuario_UsuarioAtivo = T000F18_A1394ContratadaUsuario_UsuarioAtivo[0];
         n1394ContratadaUsuario_UsuarioAtivo = T000F18_n1394ContratadaUsuario_UsuarioAtivo[0];
         A2Usuario_Nome = T000F18_A2Usuario_Nome[0];
         n2Usuario_Nome = T000F18_n2Usuario_Nome[0];
         A341Usuario_UserGamGuid = T000F18_A341Usuario_UserGamGuid[0];
         n341Usuario_UserGamGuid = T000F18_n341Usuario_UserGamGuid[0];
         A70ContratadaUsuario_UsuarioPessoaCod = T000F18_A70ContratadaUsuario_UsuarioPessoaCod[0];
         n70ContratadaUsuario_UsuarioPessoaCod = T000F18_n70ContratadaUsuario_UsuarioPessoaCod[0];
         pr_default.close(16);
         /* Using cursor T000F19 */
         pr_default.execute(17, new Object[] {n70ContratadaUsuario_UsuarioPessoaCod, A70ContratadaUsuario_UsuarioPessoaCod});
         if ( (pr_default.getStatus(17) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A71ContratadaUsuario_UsuarioPessoaNom = T000F19_A71ContratadaUsuario_UsuarioPessoaNom[0];
         n71ContratadaUsuario_UsuarioPessoaNom = T000F19_n71ContratadaUsuario_UsuarioPessoaNom[0];
         A491ContratadaUsuario_UsuarioPessoaDoc = T000F19_A491ContratadaUsuario_UsuarioPessoaDoc[0];
         n491ContratadaUsuario_UsuarioPessoaDoc = T000F19_n491ContratadaUsuario_UsuarioPessoaDoc[0];
         pr_default.close(17);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1018ContratadaUsuario_UsuarioEhContratada = false;
            n1018ContratadaUsuario_UsuarioEhContratada = false;
            A1394ContratadaUsuario_UsuarioAtivo = false;
            n1394ContratadaUsuario_UsuarioAtivo = false;
            A2Usuario_Nome = "";
            n2Usuario_Nome = false;
            A341Usuario_UserGamGuid = "";
            n341Usuario_UserGamGuid = false;
            A70ContratadaUsuario_UsuarioPessoaCod = 0;
            n70ContratadaUsuario_UsuarioPessoaCod = false;
            A71ContratadaUsuario_UsuarioPessoaNom = "";
            n71ContratadaUsuario_UsuarioPessoaNom = false;
            A491ContratadaUsuario_UsuarioPessoaDoc = "";
            n491ContratadaUsuario_UsuarioPessoaDoc = false;
         }
         isValidOutput.Add(A1018ContratadaUsuario_UsuarioEhContratada);
         isValidOutput.Add(A1394ContratadaUsuario_UsuarioAtivo);
         isValidOutput.Add(StringUtil.RTrim( A2Usuario_Nome));
         isValidOutput.Add(StringUtil.RTrim( A341Usuario_UserGamGuid));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A70ContratadaUsuario_UsuarioPessoaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A71ContratadaUsuario_UsuarioPessoaNom));
         isValidOutput.Add(A491ContratadaUsuario_UsuarioPessoaDoc);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7ContratadaUsuario_ContratadaCod',fld:'vCONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV8ContratadaUsuario_UsuarioCod',fld:'vCONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E120F2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV8ContratadaUsuario_UsuarioCod',fld:'vCONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV10TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[{av:'AV8ContratadaUsuario_UsuarioCod',fld:'vCONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(16);
         pr_default.close(17);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         TempTags = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockusuario_nome_Jsonclick = "";
         A2Usuario_Nome = "";
         lblTextblockcontratadausuario_usuariopessoanom_Jsonclick = "";
         A71ContratadaUsuario_UsuarioPessoaNom = "";
         lblTextblockcontratadausuario_contratadapessoanom_Jsonclick = "";
         A68ContratadaUsuario_ContratadaPessoaNom = "";
         A341Usuario_UserGamGuid = "";
         A348ContratadaUsuario_ContratadaPessoaCNPJ = "";
         A1297ContratadaUsuario_AreaTrabalhoDes = "";
         A491ContratadaUsuario_UsuarioPessoaDoc = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode16 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV10TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11WebSession = context.GetSession();
         Z68ContratadaUsuario_ContratadaPessoaNom = "";
         Z348ContratadaUsuario_ContratadaPessoaCNPJ = "";
         Z1297ContratadaUsuario_AreaTrabalhoDes = "";
         Z2Usuario_Nome = "";
         Z341Usuario_UserGamGuid = "";
         Z71ContratadaUsuario_UsuarioPessoaNom = "";
         Z491ContratadaUsuario_UsuarioPessoaDoc = "";
         T000F4_A67ContratadaUsuario_ContratadaPessoaCod = new int[1] ;
         T000F4_n67ContratadaUsuario_ContratadaPessoaCod = new bool[] {false} ;
         T000F4_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         T000F4_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         T000F6_A68ContratadaUsuario_ContratadaPessoaNom = new String[] {""} ;
         T000F6_n68ContratadaUsuario_ContratadaPessoaNom = new bool[] {false} ;
         T000F6_A348ContratadaUsuario_ContratadaPessoaCNPJ = new String[] {""} ;
         T000F6_n348ContratadaUsuario_ContratadaPessoaCNPJ = new bool[] {false} ;
         T000F7_A1297ContratadaUsuario_AreaTrabalhoDes = new String[] {""} ;
         T000F7_n1297ContratadaUsuario_AreaTrabalhoDes = new bool[] {false} ;
         T000F5_A1018ContratadaUsuario_UsuarioEhContratada = new bool[] {false} ;
         T000F5_n1018ContratadaUsuario_UsuarioEhContratada = new bool[] {false} ;
         T000F5_A1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         T000F5_n1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         T000F5_A2Usuario_Nome = new String[] {""} ;
         T000F5_n2Usuario_Nome = new bool[] {false} ;
         T000F5_A341Usuario_UserGamGuid = new String[] {""} ;
         T000F5_n341Usuario_UserGamGuid = new bool[] {false} ;
         T000F5_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         T000F5_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         T000F8_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         T000F8_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         T000F8_A491ContratadaUsuario_UsuarioPessoaDoc = new String[] {""} ;
         T000F8_n491ContratadaUsuario_UsuarioPessoaDoc = new bool[] {false} ;
         T000F9_A1297ContratadaUsuario_AreaTrabalhoDes = new String[] {""} ;
         T000F9_n1297ContratadaUsuario_AreaTrabalhoDes = new bool[] {false} ;
         T000F9_A68ContratadaUsuario_ContratadaPessoaNom = new String[] {""} ;
         T000F9_n68ContratadaUsuario_ContratadaPessoaNom = new bool[] {false} ;
         T000F9_A348ContratadaUsuario_ContratadaPessoaCNPJ = new String[] {""} ;
         T000F9_n348ContratadaUsuario_ContratadaPessoaCNPJ = new bool[] {false} ;
         T000F9_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         T000F9_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         T000F9_A491ContratadaUsuario_UsuarioPessoaDoc = new String[] {""} ;
         T000F9_n491ContratadaUsuario_UsuarioPessoaDoc = new bool[] {false} ;
         T000F9_A1018ContratadaUsuario_UsuarioEhContratada = new bool[] {false} ;
         T000F9_n1018ContratadaUsuario_UsuarioEhContratada = new bool[] {false} ;
         T000F9_A1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         T000F9_n1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         T000F9_A2Usuario_Nome = new String[] {""} ;
         T000F9_n2Usuario_Nome = new bool[] {false} ;
         T000F9_A341Usuario_UserGamGuid = new String[] {""} ;
         T000F9_n341Usuario_UserGamGuid = new bool[] {false} ;
         T000F9_A576ContratadaUsuario_CstUntPrdNrm = new decimal[1] ;
         T000F9_n576ContratadaUsuario_CstUntPrdNrm = new bool[] {false} ;
         T000F9_A577ContratadaUsuario_CstUntPrdExt = new decimal[1] ;
         T000F9_n577ContratadaUsuario_CstUntPrdExt = new bool[] {false} ;
         T000F9_A1518ContratadaUsuario_TmpEstAnl = new int[1] ;
         T000F9_n1518ContratadaUsuario_TmpEstAnl = new bool[] {false} ;
         T000F9_A1503ContratadaUsuario_TmpEstExc = new int[1] ;
         T000F9_n1503ContratadaUsuario_TmpEstExc = new bool[] {false} ;
         T000F9_A1504ContratadaUsuario_TmpEstCrr = new int[1] ;
         T000F9_n1504ContratadaUsuario_TmpEstCrr = new bool[] {false} ;
         T000F9_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         T000F9_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         T000F9_A67ContratadaUsuario_ContratadaPessoaCod = new int[1] ;
         T000F9_n67ContratadaUsuario_ContratadaPessoaCod = new bool[] {false} ;
         T000F9_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         T000F9_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         T000F9_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         T000F9_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         T000F10_A1018ContratadaUsuario_UsuarioEhContratada = new bool[] {false} ;
         T000F10_n1018ContratadaUsuario_UsuarioEhContratada = new bool[] {false} ;
         T000F10_A1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         T000F10_n1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         T000F10_A2Usuario_Nome = new String[] {""} ;
         T000F10_n2Usuario_Nome = new bool[] {false} ;
         T000F10_A341Usuario_UserGamGuid = new String[] {""} ;
         T000F10_n341Usuario_UserGamGuid = new bool[] {false} ;
         T000F10_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         T000F10_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         T000F11_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         T000F11_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         T000F11_A491ContratadaUsuario_UsuarioPessoaDoc = new String[] {""} ;
         T000F11_n491ContratadaUsuario_UsuarioPessoaDoc = new bool[] {false} ;
         T000F12_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         T000F12_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         T000F3_A576ContratadaUsuario_CstUntPrdNrm = new decimal[1] ;
         T000F3_n576ContratadaUsuario_CstUntPrdNrm = new bool[] {false} ;
         T000F3_A577ContratadaUsuario_CstUntPrdExt = new decimal[1] ;
         T000F3_n577ContratadaUsuario_CstUntPrdExt = new bool[] {false} ;
         T000F3_A1518ContratadaUsuario_TmpEstAnl = new int[1] ;
         T000F3_n1518ContratadaUsuario_TmpEstAnl = new bool[] {false} ;
         T000F3_A1503ContratadaUsuario_TmpEstExc = new int[1] ;
         T000F3_n1503ContratadaUsuario_TmpEstExc = new bool[] {false} ;
         T000F3_A1504ContratadaUsuario_TmpEstCrr = new int[1] ;
         T000F3_n1504ContratadaUsuario_TmpEstCrr = new bool[] {false} ;
         T000F3_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         T000F3_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         T000F13_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         T000F13_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         T000F14_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         T000F14_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         T000F2_A576ContratadaUsuario_CstUntPrdNrm = new decimal[1] ;
         T000F2_n576ContratadaUsuario_CstUntPrdNrm = new bool[] {false} ;
         T000F2_A577ContratadaUsuario_CstUntPrdExt = new decimal[1] ;
         T000F2_n577ContratadaUsuario_CstUntPrdExt = new bool[] {false} ;
         T000F2_A1518ContratadaUsuario_TmpEstAnl = new int[1] ;
         T000F2_n1518ContratadaUsuario_TmpEstAnl = new bool[] {false} ;
         T000F2_A1503ContratadaUsuario_TmpEstExc = new int[1] ;
         T000F2_n1503ContratadaUsuario_TmpEstExc = new bool[] {false} ;
         T000F2_A1504ContratadaUsuario_TmpEstCrr = new int[1] ;
         T000F2_n1504ContratadaUsuario_TmpEstCrr = new bool[] {false} ;
         T000F2_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         T000F2_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         T000F18_A1018ContratadaUsuario_UsuarioEhContratada = new bool[] {false} ;
         T000F18_n1018ContratadaUsuario_UsuarioEhContratada = new bool[] {false} ;
         T000F18_A1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         T000F18_n1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         T000F18_A2Usuario_Nome = new String[] {""} ;
         T000F18_n2Usuario_Nome = new bool[] {false} ;
         T000F18_A341Usuario_UserGamGuid = new String[] {""} ;
         T000F18_n341Usuario_UserGamGuid = new bool[] {false} ;
         T000F18_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         T000F18_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         T000F19_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         T000F19_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         T000F19_A491ContratadaUsuario_UsuarioPessoaDoc = new String[] {""} ;
         T000F19_n491ContratadaUsuario_UsuarioPessoaDoc = new bool[] {false} ;
         T000F20_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         T000F20_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratadausuario__default(),
            new Object[][] {
                new Object[] {
               T000F2_A576ContratadaUsuario_CstUntPrdNrm, T000F2_n576ContratadaUsuario_CstUntPrdNrm, T000F2_A577ContratadaUsuario_CstUntPrdExt, T000F2_n577ContratadaUsuario_CstUntPrdExt, T000F2_A1518ContratadaUsuario_TmpEstAnl, T000F2_n1518ContratadaUsuario_TmpEstAnl, T000F2_A1503ContratadaUsuario_TmpEstExc, T000F2_n1503ContratadaUsuario_TmpEstExc, T000F2_A1504ContratadaUsuario_TmpEstCrr, T000F2_n1504ContratadaUsuario_TmpEstCrr,
               T000F2_A66ContratadaUsuario_ContratadaCod, T000F2_A69ContratadaUsuario_UsuarioCod
               }
               , new Object[] {
               T000F3_A576ContratadaUsuario_CstUntPrdNrm, T000F3_n576ContratadaUsuario_CstUntPrdNrm, T000F3_A577ContratadaUsuario_CstUntPrdExt, T000F3_n577ContratadaUsuario_CstUntPrdExt, T000F3_A1518ContratadaUsuario_TmpEstAnl, T000F3_n1518ContratadaUsuario_TmpEstAnl, T000F3_A1503ContratadaUsuario_TmpEstExc, T000F3_n1503ContratadaUsuario_TmpEstExc, T000F3_A1504ContratadaUsuario_TmpEstCrr, T000F3_n1504ContratadaUsuario_TmpEstCrr,
               T000F3_A66ContratadaUsuario_ContratadaCod, T000F3_A69ContratadaUsuario_UsuarioCod
               }
               , new Object[] {
               T000F4_A67ContratadaUsuario_ContratadaPessoaCod, T000F4_n67ContratadaUsuario_ContratadaPessoaCod, T000F4_A1228ContratadaUsuario_AreaTrabalhoCod, T000F4_n1228ContratadaUsuario_AreaTrabalhoCod
               }
               , new Object[] {
               T000F5_A1018ContratadaUsuario_UsuarioEhContratada, T000F5_n1018ContratadaUsuario_UsuarioEhContratada, T000F5_A1394ContratadaUsuario_UsuarioAtivo, T000F5_n1394ContratadaUsuario_UsuarioAtivo, T000F5_A2Usuario_Nome, T000F5_n2Usuario_Nome, T000F5_A341Usuario_UserGamGuid, T000F5_n341Usuario_UserGamGuid, T000F5_A70ContratadaUsuario_UsuarioPessoaCod, T000F5_n70ContratadaUsuario_UsuarioPessoaCod
               }
               , new Object[] {
               T000F6_A68ContratadaUsuario_ContratadaPessoaNom, T000F6_n68ContratadaUsuario_ContratadaPessoaNom, T000F6_A348ContratadaUsuario_ContratadaPessoaCNPJ, T000F6_n348ContratadaUsuario_ContratadaPessoaCNPJ
               }
               , new Object[] {
               T000F7_A1297ContratadaUsuario_AreaTrabalhoDes, T000F7_n1297ContratadaUsuario_AreaTrabalhoDes
               }
               , new Object[] {
               T000F8_A71ContratadaUsuario_UsuarioPessoaNom, T000F8_n71ContratadaUsuario_UsuarioPessoaNom, T000F8_A491ContratadaUsuario_UsuarioPessoaDoc, T000F8_n491ContratadaUsuario_UsuarioPessoaDoc
               }
               , new Object[] {
               T000F9_A1297ContratadaUsuario_AreaTrabalhoDes, T000F9_n1297ContratadaUsuario_AreaTrabalhoDes, T000F9_A68ContratadaUsuario_ContratadaPessoaNom, T000F9_n68ContratadaUsuario_ContratadaPessoaNom, T000F9_A348ContratadaUsuario_ContratadaPessoaCNPJ, T000F9_n348ContratadaUsuario_ContratadaPessoaCNPJ, T000F9_A71ContratadaUsuario_UsuarioPessoaNom, T000F9_n71ContratadaUsuario_UsuarioPessoaNom, T000F9_A491ContratadaUsuario_UsuarioPessoaDoc, T000F9_n491ContratadaUsuario_UsuarioPessoaDoc,
               T000F9_A1018ContratadaUsuario_UsuarioEhContratada, T000F9_n1018ContratadaUsuario_UsuarioEhContratada, T000F9_A1394ContratadaUsuario_UsuarioAtivo, T000F9_n1394ContratadaUsuario_UsuarioAtivo, T000F9_A2Usuario_Nome, T000F9_n2Usuario_Nome, T000F9_A341Usuario_UserGamGuid, T000F9_n341Usuario_UserGamGuid, T000F9_A576ContratadaUsuario_CstUntPrdNrm, T000F9_n576ContratadaUsuario_CstUntPrdNrm,
               T000F9_A577ContratadaUsuario_CstUntPrdExt, T000F9_n577ContratadaUsuario_CstUntPrdExt, T000F9_A1518ContratadaUsuario_TmpEstAnl, T000F9_n1518ContratadaUsuario_TmpEstAnl, T000F9_A1503ContratadaUsuario_TmpEstExc, T000F9_n1503ContratadaUsuario_TmpEstExc, T000F9_A1504ContratadaUsuario_TmpEstCrr, T000F9_n1504ContratadaUsuario_TmpEstCrr, T000F9_A66ContratadaUsuario_ContratadaCod, T000F9_A69ContratadaUsuario_UsuarioCod,
               T000F9_A67ContratadaUsuario_ContratadaPessoaCod, T000F9_n67ContratadaUsuario_ContratadaPessoaCod, T000F9_A1228ContratadaUsuario_AreaTrabalhoCod, T000F9_n1228ContratadaUsuario_AreaTrabalhoCod, T000F9_A70ContratadaUsuario_UsuarioPessoaCod, T000F9_n70ContratadaUsuario_UsuarioPessoaCod
               }
               , new Object[] {
               T000F10_A1018ContratadaUsuario_UsuarioEhContratada, T000F10_n1018ContratadaUsuario_UsuarioEhContratada, T000F10_A1394ContratadaUsuario_UsuarioAtivo, T000F10_n1394ContratadaUsuario_UsuarioAtivo, T000F10_A2Usuario_Nome, T000F10_n2Usuario_Nome, T000F10_A341Usuario_UserGamGuid, T000F10_n341Usuario_UserGamGuid, T000F10_A70ContratadaUsuario_UsuarioPessoaCod, T000F10_n70ContratadaUsuario_UsuarioPessoaCod
               }
               , new Object[] {
               T000F11_A71ContratadaUsuario_UsuarioPessoaNom, T000F11_n71ContratadaUsuario_UsuarioPessoaNom, T000F11_A491ContratadaUsuario_UsuarioPessoaDoc, T000F11_n491ContratadaUsuario_UsuarioPessoaDoc
               }
               , new Object[] {
               T000F12_A66ContratadaUsuario_ContratadaCod, T000F12_A69ContratadaUsuario_UsuarioCod
               }
               , new Object[] {
               T000F13_A66ContratadaUsuario_ContratadaCod, T000F13_A69ContratadaUsuario_UsuarioCod
               }
               , new Object[] {
               T000F14_A66ContratadaUsuario_ContratadaCod, T000F14_A69ContratadaUsuario_UsuarioCod
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000F18_A1018ContratadaUsuario_UsuarioEhContratada, T000F18_n1018ContratadaUsuario_UsuarioEhContratada, T000F18_A1394ContratadaUsuario_UsuarioAtivo, T000F18_n1394ContratadaUsuario_UsuarioAtivo, T000F18_A2Usuario_Nome, T000F18_n2Usuario_Nome, T000F18_A341Usuario_UserGamGuid, T000F18_n341Usuario_UserGamGuid, T000F18_A70ContratadaUsuario_UsuarioPessoaCod, T000F18_n70ContratadaUsuario_UsuarioPessoaCod
               }
               , new Object[] {
               T000F19_A71ContratadaUsuario_UsuarioPessoaNom, T000F19_n71ContratadaUsuario_UsuarioPessoaNom, T000F19_A491ContratadaUsuario_UsuarioPessoaDoc, T000F19_n491ContratadaUsuario_UsuarioPessoaDoc
               }
               , new Object[] {
               T000F20_A66ContratadaUsuario_ContratadaCod, T000F20_A69ContratadaUsuario_UsuarioCod
               }
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short RcdFound16 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7ContratadaUsuario_ContratadaCod ;
      private int wcpOAV8ContratadaUsuario_UsuarioCod ;
      private int Z66ContratadaUsuario_ContratadaCod ;
      private int Z69ContratadaUsuario_UsuarioCod ;
      private int Z1518ContratadaUsuario_TmpEstAnl ;
      private int Z1503ContratadaUsuario_TmpEstExc ;
      private int Z1504ContratadaUsuario_TmpEstCrr ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A70ContratadaUsuario_UsuarioPessoaCod ;
      private int AV7ContratadaUsuario_ContratadaCod ;
      private int AV8ContratadaUsuario_UsuarioCod ;
      private int trnEnded ;
      private int A67ContratadaUsuario_ContratadaPessoaCod ;
      private int edtContratadaUsuario_ContratadaPessoaCod_Enabled ;
      private int edtContratadaUsuario_ContratadaPessoaCod_Visible ;
      private int edtContratadaUsuario_UsuarioCod_Visible ;
      private int edtContratadaUsuario_UsuarioCod_Enabled ;
      private int edtContratadaUsuario_UsuarioPessoaCod_Enabled ;
      private int edtContratadaUsuario_UsuarioPessoaCod_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtUsuario_Nome_Enabled ;
      private int edtContratadaUsuario_UsuarioPessoaNom_Enabled ;
      private int edtContratadaUsuario_ContratadaPessoaNom_Enabled ;
      private int A1518ContratadaUsuario_TmpEstAnl ;
      private int A1503ContratadaUsuario_TmpEstExc ;
      private int A1504ContratadaUsuario_TmpEstCrr ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A1228ContratadaUsuario_AreaTrabalhoCod ;
      private int Z67ContratadaUsuario_ContratadaPessoaCod ;
      private int Z1228ContratadaUsuario_AreaTrabalhoCod ;
      private int Z70ContratadaUsuario_UsuarioPessoaCod ;
      private int idxLst ;
      private decimal Z576ContratadaUsuario_CstUntPrdNrm ;
      private decimal Z577ContratadaUsuario_CstUntPrdExt ;
      private decimal A576ContratadaUsuario_CstUntPrdNrm ;
      private decimal A577ContratadaUsuario_CstUntPrdExt ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtContratadaUsuario_UsuarioCod_Internalname ;
      private String edtContratadaUsuario_ContratadaPessoaCod_Internalname ;
      private String edtContratadaUsuario_ContratadaPessoaCod_Jsonclick ;
      private String TempTags ;
      private String edtContratadaUsuario_UsuarioCod_Jsonclick ;
      private String edtContratadaUsuario_UsuarioPessoaCod_Internalname ;
      private String edtContratadaUsuario_UsuarioPessoaCod_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockusuario_nome_Internalname ;
      private String lblTextblockusuario_nome_Jsonclick ;
      private String edtUsuario_Nome_Internalname ;
      private String A2Usuario_Nome ;
      private String edtUsuario_Nome_Jsonclick ;
      private String lblTextblockcontratadausuario_usuariopessoanom_Internalname ;
      private String lblTextblockcontratadausuario_usuariopessoanom_Jsonclick ;
      private String edtContratadaUsuario_UsuarioPessoaNom_Internalname ;
      private String A71ContratadaUsuario_UsuarioPessoaNom ;
      private String edtContratadaUsuario_UsuarioPessoaNom_Jsonclick ;
      private String lblTextblockcontratadausuario_contratadapessoanom_Internalname ;
      private String lblTextblockcontratadausuario_contratadapessoanom_Jsonclick ;
      private String edtContratadaUsuario_ContratadaPessoaNom_Internalname ;
      private String A68ContratadaUsuario_ContratadaPessoaNom ;
      private String edtContratadaUsuario_ContratadaPessoaNom_Jsonclick ;
      private String A341Usuario_UserGamGuid ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode16 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z68ContratadaUsuario_ContratadaPessoaNom ;
      private String Z2Usuario_Nome ;
      private String Z341Usuario_UserGamGuid ;
      private String Z71ContratadaUsuario_UsuarioPessoaNom ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private bool entryPointCalled ;
      private bool n70ContratadaUsuario_UsuarioPessoaCod ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n2Usuario_Nome ;
      private bool n71ContratadaUsuario_UsuarioPessoaNom ;
      private bool n68ContratadaUsuario_ContratadaPessoaNom ;
      private bool n67ContratadaUsuario_ContratadaPessoaCod ;
      private bool n576ContratadaUsuario_CstUntPrdNrm ;
      private bool n577ContratadaUsuario_CstUntPrdExt ;
      private bool n1518ContratadaUsuario_TmpEstAnl ;
      private bool n1503ContratadaUsuario_TmpEstExc ;
      private bool n1504ContratadaUsuario_TmpEstCrr ;
      private bool n1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool A1018ContratadaUsuario_UsuarioEhContratada ;
      private bool n1018ContratadaUsuario_UsuarioEhContratada ;
      private bool A1394ContratadaUsuario_UsuarioAtivo ;
      private bool n1394ContratadaUsuario_UsuarioAtivo ;
      private bool n341Usuario_UserGamGuid ;
      private bool n348ContratadaUsuario_ContratadaPessoaCNPJ ;
      private bool n1297ContratadaUsuario_AreaTrabalhoDes ;
      private bool n491ContratadaUsuario_UsuarioPessoaDoc ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool Z1018ContratadaUsuario_UsuarioEhContratada ;
      private bool Z1394ContratadaUsuario_UsuarioAtivo ;
      private String A348ContratadaUsuario_ContratadaPessoaCNPJ ;
      private String A1297ContratadaUsuario_AreaTrabalhoDes ;
      private String A491ContratadaUsuario_UsuarioPessoaDoc ;
      private String Z348ContratadaUsuario_ContratadaPessoaCNPJ ;
      private String Z1297ContratadaUsuario_AreaTrabalhoDes ;
      private String Z491ContratadaUsuario_UsuarioPessoaDoc ;
      private IGxSession AV11WebSession ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] T000F4_A67ContratadaUsuario_ContratadaPessoaCod ;
      private bool[] T000F4_n67ContratadaUsuario_ContratadaPessoaCod ;
      private int[] T000F4_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] T000F4_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private String[] T000F6_A68ContratadaUsuario_ContratadaPessoaNom ;
      private bool[] T000F6_n68ContratadaUsuario_ContratadaPessoaNom ;
      private String[] T000F6_A348ContratadaUsuario_ContratadaPessoaCNPJ ;
      private bool[] T000F6_n348ContratadaUsuario_ContratadaPessoaCNPJ ;
      private String[] T000F7_A1297ContratadaUsuario_AreaTrabalhoDes ;
      private bool[] T000F7_n1297ContratadaUsuario_AreaTrabalhoDes ;
      private bool[] T000F5_A1018ContratadaUsuario_UsuarioEhContratada ;
      private bool[] T000F5_n1018ContratadaUsuario_UsuarioEhContratada ;
      private bool[] T000F5_A1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] T000F5_n1394ContratadaUsuario_UsuarioAtivo ;
      private String[] T000F5_A2Usuario_Nome ;
      private bool[] T000F5_n2Usuario_Nome ;
      private String[] T000F5_A341Usuario_UserGamGuid ;
      private bool[] T000F5_n341Usuario_UserGamGuid ;
      private int[] T000F5_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] T000F5_n70ContratadaUsuario_UsuarioPessoaCod ;
      private String[] T000F8_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] T000F8_n71ContratadaUsuario_UsuarioPessoaNom ;
      private String[] T000F8_A491ContratadaUsuario_UsuarioPessoaDoc ;
      private bool[] T000F8_n491ContratadaUsuario_UsuarioPessoaDoc ;
      private String[] T000F9_A1297ContratadaUsuario_AreaTrabalhoDes ;
      private bool[] T000F9_n1297ContratadaUsuario_AreaTrabalhoDes ;
      private String[] T000F9_A68ContratadaUsuario_ContratadaPessoaNom ;
      private bool[] T000F9_n68ContratadaUsuario_ContratadaPessoaNom ;
      private String[] T000F9_A348ContratadaUsuario_ContratadaPessoaCNPJ ;
      private bool[] T000F9_n348ContratadaUsuario_ContratadaPessoaCNPJ ;
      private String[] T000F9_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] T000F9_n71ContratadaUsuario_UsuarioPessoaNom ;
      private String[] T000F9_A491ContratadaUsuario_UsuarioPessoaDoc ;
      private bool[] T000F9_n491ContratadaUsuario_UsuarioPessoaDoc ;
      private bool[] T000F9_A1018ContratadaUsuario_UsuarioEhContratada ;
      private bool[] T000F9_n1018ContratadaUsuario_UsuarioEhContratada ;
      private bool[] T000F9_A1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] T000F9_n1394ContratadaUsuario_UsuarioAtivo ;
      private String[] T000F9_A2Usuario_Nome ;
      private bool[] T000F9_n2Usuario_Nome ;
      private String[] T000F9_A341Usuario_UserGamGuid ;
      private bool[] T000F9_n341Usuario_UserGamGuid ;
      private decimal[] T000F9_A576ContratadaUsuario_CstUntPrdNrm ;
      private bool[] T000F9_n576ContratadaUsuario_CstUntPrdNrm ;
      private decimal[] T000F9_A577ContratadaUsuario_CstUntPrdExt ;
      private bool[] T000F9_n577ContratadaUsuario_CstUntPrdExt ;
      private int[] T000F9_A1518ContratadaUsuario_TmpEstAnl ;
      private bool[] T000F9_n1518ContratadaUsuario_TmpEstAnl ;
      private int[] T000F9_A1503ContratadaUsuario_TmpEstExc ;
      private bool[] T000F9_n1503ContratadaUsuario_TmpEstExc ;
      private int[] T000F9_A1504ContratadaUsuario_TmpEstCrr ;
      private bool[] T000F9_n1504ContratadaUsuario_TmpEstCrr ;
      private int[] T000F9_A66ContratadaUsuario_ContratadaCod ;
      private int[] T000F9_A69ContratadaUsuario_UsuarioCod ;
      private int[] T000F9_A67ContratadaUsuario_ContratadaPessoaCod ;
      private bool[] T000F9_n67ContratadaUsuario_ContratadaPessoaCod ;
      private int[] T000F9_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] T000F9_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private int[] T000F9_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] T000F9_n70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] T000F10_A1018ContratadaUsuario_UsuarioEhContratada ;
      private bool[] T000F10_n1018ContratadaUsuario_UsuarioEhContratada ;
      private bool[] T000F10_A1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] T000F10_n1394ContratadaUsuario_UsuarioAtivo ;
      private String[] T000F10_A2Usuario_Nome ;
      private bool[] T000F10_n2Usuario_Nome ;
      private String[] T000F10_A341Usuario_UserGamGuid ;
      private bool[] T000F10_n341Usuario_UserGamGuid ;
      private int[] T000F10_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] T000F10_n70ContratadaUsuario_UsuarioPessoaCod ;
      private String[] T000F11_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] T000F11_n71ContratadaUsuario_UsuarioPessoaNom ;
      private String[] T000F11_A491ContratadaUsuario_UsuarioPessoaDoc ;
      private bool[] T000F11_n491ContratadaUsuario_UsuarioPessoaDoc ;
      private int[] T000F12_A66ContratadaUsuario_ContratadaCod ;
      private int[] T000F12_A69ContratadaUsuario_UsuarioCod ;
      private decimal[] T000F3_A576ContratadaUsuario_CstUntPrdNrm ;
      private bool[] T000F3_n576ContratadaUsuario_CstUntPrdNrm ;
      private decimal[] T000F3_A577ContratadaUsuario_CstUntPrdExt ;
      private bool[] T000F3_n577ContratadaUsuario_CstUntPrdExt ;
      private int[] T000F3_A1518ContratadaUsuario_TmpEstAnl ;
      private bool[] T000F3_n1518ContratadaUsuario_TmpEstAnl ;
      private int[] T000F3_A1503ContratadaUsuario_TmpEstExc ;
      private bool[] T000F3_n1503ContratadaUsuario_TmpEstExc ;
      private int[] T000F3_A1504ContratadaUsuario_TmpEstCrr ;
      private bool[] T000F3_n1504ContratadaUsuario_TmpEstCrr ;
      private int[] T000F3_A66ContratadaUsuario_ContratadaCod ;
      private int[] T000F3_A69ContratadaUsuario_UsuarioCod ;
      private int[] T000F13_A66ContratadaUsuario_ContratadaCod ;
      private int[] T000F13_A69ContratadaUsuario_UsuarioCod ;
      private int[] T000F14_A66ContratadaUsuario_ContratadaCod ;
      private int[] T000F14_A69ContratadaUsuario_UsuarioCod ;
      private decimal[] T000F2_A576ContratadaUsuario_CstUntPrdNrm ;
      private bool[] T000F2_n576ContratadaUsuario_CstUntPrdNrm ;
      private decimal[] T000F2_A577ContratadaUsuario_CstUntPrdExt ;
      private bool[] T000F2_n577ContratadaUsuario_CstUntPrdExt ;
      private int[] T000F2_A1518ContratadaUsuario_TmpEstAnl ;
      private bool[] T000F2_n1518ContratadaUsuario_TmpEstAnl ;
      private int[] T000F2_A1503ContratadaUsuario_TmpEstExc ;
      private bool[] T000F2_n1503ContratadaUsuario_TmpEstExc ;
      private int[] T000F2_A1504ContratadaUsuario_TmpEstCrr ;
      private bool[] T000F2_n1504ContratadaUsuario_TmpEstCrr ;
      private int[] T000F2_A66ContratadaUsuario_ContratadaCod ;
      private int[] T000F2_A69ContratadaUsuario_UsuarioCod ;
      private bool[] T000F18_A1018ContratadaUsuario_UsuarioEhContratada ;
      private bool[] T000F18_n1018ContratadaUsuario_UsuarioEhContratada ;
      private bool[] T000F18_A1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] T000F18_n1394ContratadaUsuario_UsuarioAtivo ;
      private String[] T000F18_A2Usuario_Nome ;
      private bool[] T000F18_n2Usuario_Nome ;
      private String[] T000F18_A341Usuario_UserGamGuid ;
      private bool[] T000F18_n341Usuario_UserGamGuid ;
      private int[] T000F18_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] T000F18_n70ContratadaUsuario_UsuarioPessoaCod ;
      private String[] T000F19_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] T000F19_n71ContratadaUsuario_UsuarioPessoaNom ;
      private String[] T000F19_A491ContratadaUsuario_UsuarioPessoaDoc ;
      private bool[] T000F19_n491ContratadaUsuario_UsuarioPessoaDoc ;
      private int[] T000F20_A66ContratadaUsuario_ContratadaCod ;
      private int[] T000F20_A69ContratadaUsuario_UsuarioCod ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV10TrnContext ;
   }

   public class contratadausuario__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new UpdateCursor(def[13])
         ,new UpdateCursor(def[14])
         ,new UpdateCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT000F4 ;
          prmT000F4 = new Object[] {
          new Object[] {"@ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000F6 ;
          prmT000F6 = new Object[] {
          new Object[] {"@ContratadaUsuario_ContratadaPessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000F7 ;
          prmT000F7 = new Object[] {
          new Object[] {"@ContratadaUsuario_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000F9 ;
          prmT000F9 = new Object[] {
          new Object[] {"@ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000F5 ;
          prmT000F5 = new Object[] {
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000F8 ;
          prmT000F8 = new Object[] {
          new Object[] {"@ContratadaUsuario_UsuarioPessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000F10 ;
          prmT000F10 = new Object[] {
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000F11 ;
          prmT000F11 = new Object[] {
          new Object[] {"@ContratadaUsuario_UsuarioPessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000F12 ;
          prmT000F12 = new Object[] {
          new Object[] {"@ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000F3 ;
          prmT000F3 = new Object[] {
          new Object[] {"@ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000F13 ;
          prmT000F13 = new Object[] {
          new Object[] {"@ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000F14 ;
          prmT000F14 = new Object[] {
          new Object[] {"@ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000F2 ;
          prmT000F2 = new Object[] {
          new Object[] {"@ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000F15 ;
          prmT000F15 = new Object[] {
          new Object[] {"@ContratadaUsuario_CstUntPrdNrm",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContratadaUsuario_CstUntPrdExt",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContratadaUsuario_TmpEstAnl",SqlDbType.Int,8,0} ,
          new Object[] {"@ContratadaUsuario_TmpEstExc",SqlDbType.Int,8,0} ,
          new Object[] {"@ContratadaUsuario_TmpEstCrr",SqlDbType.Int,8,0} ,
          new Object[] {"@ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000F16 ;
          prmT000F16 = new Object[] {
          new Object[] {"@ContratadaUsuario_CstUntPrdNrm",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContratadaUsuario_CstUntPrdExt",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContratadaUsuario_TmpEstAnl",SqlDbType.Int,8,0} ,
          new Object[] {"@ContratadaUsuario_TmpEstExc",SqlDbType.Int,8,0} ,
          new Object[] {"@ContratadaUsuario_TmpEstCrr",SqlDbType.Int,8,0} ,
          new Object[] {"@ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000F17 ;
          prmT000F17 = new Object[] {
          new Object[] {"@ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000F20 ;
          prmT000F20 = new Object[] {
          } ;
          Object[] prmT000F18 ;
          prmT000F18 = new Object[] {
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000F19 ;
          prmT000F19 = new Object[] {
          new Object[] {"@ContratadaUsuario_UsuarioPessoaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T000F2", "SELECT [ContratadaUsuario_CstUntPrdNrm], [ContratadaUsuario_CstUntPrdExt], [ContratadaUsuario_TmpEstAnl], [ContratadaUsuario_TmpEstExc], [ContratadaUsuario_TmpEstCrr], [ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, [ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod FROM [ContratadaUsuario] WITH (UPDLOCK) WHERE [ContratadaUsuario_ContratadaCod] = @ContratadaUsuario_ContratadaCod AND [ContratadaUsuario_UsuarioCod] = @ContratadaUsuario_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000F2,1,0,true,false )
             ,new CursorDef("T000F3", "SELECT [ContratadaUsuario_CstUntPrdNrm], [ContratadaUsuario_CstUntPrdExt], [ContratadaUsuario_TmpEstAnl], [ContratadaUsuario_TmpEstExc], [ContratadaUsuario_TmpEstCrr], [ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, [ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod FROM [ContratadaUsuario] WITH (NOLOCK) WHERE [ContratadaUsuario_ContratadaCod] = @ContratadaUsuario_ContratadaCod AND [ContratadaUsuario_UsuarioCod] = @ContratadaUsuario_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000F3,1,0,true,false )
             ,new CursorDef("T000F4", "SELECT [Contratada_PessoaCod] AS ContratadaUsuario_ContratadaPessoaCod, [Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContratadaUsuario_ContratadaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000F4,1,0,true,false )
             ,new CursorDef("T000F5", "SELECT [Usuario_EhContratada] AS ContratadaUsuario_UsuarioEhContratada, [Usuario_Ativo] AS ContratadaUsuario_UsuarioAtivo, [Usuario_Nome], [Usuario_UserGamGuid], [Usuario_PessoaCod] AS ContratadaUsuario_UsuarioPessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContratadaUsuario_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000F5,1,0,true,false )
             ,new CursorDef("T000F6", "SELECT [Pessoa_Nome] AS ContratadaUsuario_ContratadaPessoaNom, [Pessoa_Docto] AS ContratadaUsuario_ContratadaPessoaCNPJ FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContratadaUsuario_ContratadaPessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000F6,1,0,true,false )
             ,new CursorDef("T000F7", "SELECT [AreaTrabalho_Descricao] AS ContratadaUsuario_AreaTrabalhoDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @ContratadaUsuario_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000F7,1,0,true,false )
             ,new CursorDef("T000F8", "SELECT [Pessoa_Nome] AS ContratadaUsuario_UsuarioPesso, [Pessoa_Docto] AS ContratadaUsuario_UsuarioPesso FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContratadaUsuario_UsuarioPessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000F8,1,0,true,false )
             ,new CursorDef("T000F9", "SELECT T4.[AreaTrabalho_Descricao] AS ContratadaUsuario_AreaTrabalhoDes, T3.[Pessoa_Nome] AS ContratadaUsuario_ContratadaPessoaNom, T3.[Pessoa_Docto] AS ContratadaUsuario_ContratadaPessoaCNPJ, T6.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPesso, T6.[Pessoa_Docto] AS ContratadaUsuario_UsuarioPesso, T5.[Usuario_EhContratada] AS ContratadaUsuario_UsuarioEhContratada, T5.[Usuario_Ativo] AS ContratadaUsuario_UsuarioAtivo, T5.[Usuario_Nome], T5.[Usuario_UserGamGuid], TM1.[ContratadaUsuario_CstUntPrdNrm], TM1.[ContratadaUsuario_CstUntPrdExt], TM1.[ContratadaUsuario_TmpEstAnl], TM1.[ContratadaUsuario_TmpEstExc], TM1.[ContratadaUsuario_TmpEstCrr], TM1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, TM1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T2.[Contratada_PessoaCod] AS ContratadaUsuario_ContratadaPessoaCod, T2.[Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod, T5.[Usuario_PessoaCod] AS ContratadaUsuario_UsuarioPessoaCod FROM ((((([ContratadaUsuario] TM1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = TM1.[ContratadaUsuario_ContratadaCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratada_PessoaCod]) LEFT JOIN [AreaTrabalho] T4 WITH (NOLOCK) ON T4.[AreaTrabalho_Codigo] = T2.[Contratada_AreaTrabalhoCod]) INNER JOIN [Usuario] T5 WITH (NOLOCK) ON T5.[Usuario_Codigo] = TM1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T6 WITH (NOLOCK) ON T6.[Pessoa_Codigo] = T5.[Usuario_PessoaCod]) WHERE TM1.[ContratadaUsuario_ContratadaCod] = @ContratadaUsuario_ContratadaCod and TM1.[ContratadaUsuario_UsuarioCod] = @ContratadaUsuario_UsuarioCod ORDER BY TM1.[ContratadaUsuario_ContratadaCod], TM1.[ContratadaUsuario_UsuarioCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000F9,100,0,true,false )
             ,new CursorDef("T000F10", "SELECT [Usuario_EhContratada] AS ContratadaUsuario_UsuarioEhContratada, [Usuario_Ativo] AS ContratadaUsuario_UsuarioAtivo, [Usuario_Nome], [Usuario_UserGamGuid], [Usuario_PessoaCod] AS ContratadaUsuario_UsuarioPessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContratadaUsuario_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000F10,1,0,true,false )
             ,new CursorDef("T000F11", "SELECT [Pessoa_Nome] AS ContratadaUsuario_UsuarioPesso, [Pessoa_Docto] AS ContratadaUsuario_UsuarioPesso FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContratadaUsuario_UsuarioPessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000F11,1,0,true,false )
             ,new CursorDef("T000F12", "SELECT [ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, [ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod FROM [ContratadaUsuario] WITH (NOLOCK) WHERE [ContratadaUsuario_ContratadaCod] = @ContratadaUsuario_ContratadaCod AND [ContratadaUsuario_UsuarioCod] = @ContratadaUsuario_UsuarioCod  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000F12,1,0,true,false )
             ,new CursorDef("T000F13", "SELECT TOP 1 [ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, [ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod FROM [ContratadaUsuario] WITH (NOLOCK) WHERE ( [ContratadaUsuario_ContratadaCod] > @ContratadaUsuario_ContratadaCod or [ContratadaUsuario_ContratadaCod] = @ContratadaUsuario_ContratadaCod and [ContratadaUsuario_UsuarioCod] > @ContratadaUsuario_UsuarioCod) ORDER BY [ContratadaUsuario_ContratadaCod], [ContratadaUsuario_UsuarioCod]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000F13,1,0,true,true )
             ,new CursorDef("T000F14", "SELECT TOP 1 [ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, [ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod FROM [ContratadaUsuario] WITH (NOLOCK) WHERE ( [ContratadaUsuario_ContratadaCod] < @ContratadaUsuario_ContratadaCod or [ContratadaUsuario_ContratadaCod] = @ContratadaUsuario_ContratadaCod and [ContratadaUsuario_UsuarioCod] < @ContratadaUsuario_UsuarioCod) ORDER BY [ContratadaUsuario_ContratadaCod] DESC, [ContratadaUsuario_UsuarioCod] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000F14,1,0,true,true )
             ,new CursorDef("T000F15", "INSERT INTO [ContratadaUsuario]([ContratadaUsuario_CstUntPrdNrm], [ContratadaUsuario_CstUntPrdExt], [ContratadaUsuario_TmpEstAnl], [ContratadaUsuario_TmpEstExc], [ContratadaUsuario_TmpEstCrr], [ContratadaUsuario_ContratadaCod], [ContratadaUsuario_UsuarioCod]) VALUES(@ContratadaUsuario_CstUntPrdNrm, @ContratadaUsuario_CstUntPrdExt, @ContratadaUsuario_TmpEstAnl, @ContratadaUsuario_TmpEstExc, @ContratadaUsuario_TmpEstCrr, @ContratadaUsuario_ContratadaCod, @ContratadaUsuario_UsuarioCod)", GxErrorMask.GX_NOMASK,prmT000F15)
             ,new CursorDef("T000F16", "UPDATE [ContratadaUsuario] SET [ContratadaUsuario_CstUntPrdNrm]=@ContratadaUsuario_CstUntPrdNrm, [ContratadaUsuario_CstUntPrdExt]=@ContratadaUsuario_CstUntPrdExt, [ContratadaUsuario_TmpEstAnl]=@ContratadaUsuario_TmpEstAnl, [ContratadaUsuario_TmpEstExc]=@ContratadaUsuario_TmpEstExc, [ContratadaUsuario_TmpEstCrr]=@ContratadaUsuario_TmpEstCrr  WHERE [ContratadaUsuario_ContratadaCod] = @ContratadaUsuario_ContratadaCod AND [ContratadaUsuario_UsuarioCod] = @ContratadaUsuario_UsuarioCod", GxErrorMask.GX_NOMASK,prmT000F16)
             ,new CursorDef("T000F17", "DELETE FROM [ContratadaUsuario]  WHERE [ContratadaUsuario_ContratadaCod] = @ContratadaUsuario_ContratadaCod AND [ContratadaUsuario_UsuarioCod] = @ContratadaUsuario_UsuarioCod", GxErrorMask.GX_NOMASK,prmT000F17)
             ,new CursorDef("T000F18", "SELECT [Usuario_EhContratada] AS ContratadaUsuario_UsuarioEhContratada, [Usuario_Ativo] AS ContratadaUsuario_UsuarioAtivo, [Usuario_Nome], [Usuario_UserGamGuid], [Usuario_PessoaCod] AS ContratadaUsuario_UsuarioPessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContratadaUsuario_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000F18,1,0,true,false )
             ,new CursorDef("T000F19", "SELECT [Pessoa_Nome] AS ContratadaUsuario_UsuarioPesso, [Pessoa_Docto] AS ContratadaUsuario_UsuarioPesso FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContratadaUsuario_UsuarioPessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000F19,1,0,true,false )
             ,new CursorDef("T000F20", "SELECT [ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, [ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod FROM [ContratadaUsuario] WITH (NOLOCK) ORDER BY [ContratadaUsuario_ContratadaCod], [ContratadaUsuario_UsuarioCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000F20,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((decimal[]) buf[2])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((decimal[]) buf[2])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 3 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 40) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((bool[]) buf[10])[0] = rslt.getBool(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((bool[]) buf[12])[0] = rslt.getBool(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((String[]) buf[14])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((String[]) buf[16])[0] = rslt.getString(9, 40) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((decimal[]) buf[18])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((decimal[]) buf[20])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((int[]) buf[22])[0] = rslt.getInt(12) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(12);
                ((int[]) buf[24])[0] = rslt.getInt(13) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(13);
                ((int[]) buf[26])[0] = rslt.getInt(14) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(14);
                ((int[]) buf[28])[0] = rslt.getInt(15) ;
                ((int[]) buf[29])[0] = rslt.getInt(16) ;
                ((int[]) buf[30])[0] = rslt.getInt(17) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(17);
                ((int[]) buf[32])[0] = rslt.getInt(18) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(18);
                ((int[]) buf[34])[0] = rslt.getInt(19) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(19);
                return;
             case 8 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 40) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 16 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 40) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                return;
             case 17 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (decimal)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[9]);
                }
                stmt.SetParameter(6, (int)parms[10]);
                stmt.SetParameter(7, (int)parms[11]);
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (decimal)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[9]);
                }
                stmt.SetParameter(6, (int)parms[10]);
                stmt.SetParameter(7, (int)parms[11]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
