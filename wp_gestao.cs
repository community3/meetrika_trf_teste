/*
               File: WP_Gestao
        Description: Gest�o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:25:49.73
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_gestao : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_gestao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_gestao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavContagemresultado_servico = new GXCombobox();
         cmbavContagemresultado_responsavel = new GXCombobox();
         chkavSelected1 = new GXCheckbox();
         chkavSelected2 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_SERVICO") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_SERVICOL42( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridresumo") == 0 )
            {
               nRC_GXsfl_39 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_39_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_39_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGridresumo_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Gridresumo") == 0 )
            {
               subGridresumo_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A472ContagemResultado_DataEntrega = context.localUtil.ParseDateParm( GetNextPar( ));
               n472ContagemResultado_DataEntrega = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A472ContagemResultado_DataEntrega", context.localUtil.Format(A472ContagemResultado_DataEntrega, "99/99/99"));
               A484ContagemResultado_StatusDmn = GetNextPar( );
               n484ContagemResultado_StatusDmn = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A484ContagemResultado_StatusDmn", A484ContagemResultado_StatusDmn);
               A490ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n490ContagemResultado_ContratadaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A490ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0)));
               AV10ServerDate = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10ServerDate", context.localUtil.Format(AV10ServerDate, "99/99/99"));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6Contratadas);
               AV36ContagemResultado_Responsavel = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ContagemResultado_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36ContagemResultado_Responsavel), 6, 0)));
               A890ContagemResultado_Responsavel = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n890ContagemResultado_Responsavel = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A890ContagemResultado_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A890ContagemResultado_Responsavel), 6, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV21Responsaveis);
               A531ContagemResultado_StatusUltCnt = (short)(NumberUtil.Val( GetNextPar( ), "."));
               n531ContagemResultado_StatusUltCnt = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A531ContagemResultado_StatusUltCnt", StringUtil.LTrim( StringUtil.Str( (decimal)(A531ContagemResultado_StatusUltCnt), 2, 0)));
               A456ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
               A602ContagemResultado_OSVinculada = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n602ContagemResultado_OSVinculada = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A602ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(A602ContagemResultado_OSVinculada), 6, 0)));
               A499ContagemResultado_ContratadaPessoaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n499ContagemResultado_ContratadaPessoaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A499ContagemResultado_ContratadaPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A499ContagemResultado_ContratadaPessoaCod), 6, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV5WWPContext);
               AV9Amarelo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Amarelo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Amarelo), 8, 0)));
               A1313ContagemResultado_ResponsavelPessNome = GetNextPar( );
               n1313ContagemResultado_ResponsavelPessNome = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1313ContagemResultado_ResponsavelPessNome", A1313ContagemResultado_ResponsavelPessNome);
               AV39Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavNome_Internalname, AV39Nome);
               AV32ContagemResultado_Servico = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ContagemResultado_Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32ContagemResultado_Servico), 6, 0)));
               AV33ContagemResultado_Demanda = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContagemResultado_Demanda", AV33ContagemResultado_Demanda);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGridresumo_refresh( subGridresumo_Rows, A472ContagemResultado_DataEntrega, A484ContagemResultado_StatusDmn, A490ContagemResultado_ContratadaCod, AV10ServerDate, AV6Contratadas, AV36ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, AV21Responsaveis, A531ContagemResultado_StatusUltCnt, A456ContagemResultado_Codigo, A602ContagemResultado_OSVinculada, A499ContagemResultado_ContratadaPessoaCod, AV5WWPContext, AV9Amarelo, A1313ContagemResultado_ResponsavelPessNome, AV39Nome, AV32ContagemResultado_Servico, AV33ContagemResultado_Demanda) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridvermelho") == 0 )
            {
               nRC_GXsfl_53 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_53_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_53_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGridvermelho_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Gridvermelho") == 0 )
            {
               subGridvermelho_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A472ContagemResultado_DataEntrega = context.localUtil.ParseDateParm( GetNextPar( ));
               n472ContagemResultado_DataEntrega = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A472ContagemResultado_DataEntrega", context.localUtil.Format(A472ContagemResultado_DataEntrega, "99/99/99"));
               A484ContagemResultado_StatusDmn = GetNextPar( );
               n484ContagemResultado_StatusDmn = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A484ContagemResultado_StatusDmn", A484ContagemResultado_StatusDmn);
               A490ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n490ContagemResultado_ContratadaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A490ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0)));
               AV10ServerDate = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10ServerDate", context.localUtil.Format(AV10ServerDate, "99/99/99"));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6Contratadas);
               AV36ContagemResultado_Responsavel = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ContagemResultado_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36ContagemResultado_Responsavel), 6, 0)));
               A890ContagemResultado_Responsavel = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n890ContagemResultado_Responsavel = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A890ContagemResultado_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A890ContagemResultado_Responsavel), 6, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV21Responsaveis);
               A531ContagemResultado_StatusUltCnt = (short)(NumberUtil.Val( GetNextPar( ), "."));
               n531ContagemResultado_StatusUltCnt = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A531ContagemResultado_StatusUltCnt", StringUtil.LTrim( StringUtil.Str( (decimal)(A531ContagemResultado_StatusUltCnt), 2, 0)));
               A456ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
               A602ContagemResultado_OSVinculada = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n602ContagemResultado_OSVinculada = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A602ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(A602ContagemResultado_OSVinculada), 6, 0)));
               A499ContagemResultado_ContratadaPessoaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n499ContagemResultado_ContratadaPessoaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A499ContagemResultado_ContratadaPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A499ContagemResultado_ContratadaPessoaCod), 6, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV5WWPContext);
               AV9Amarelo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Amarelo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Amarelo), 8, 0)));
               A53Contratada_AreaTrabalhoDes = GetNextPar( );
               n53Contratada_AreaTrabalhoDes = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A53Contratada_AreaTrabalhoDes", A53Contratada_AreaTrabalhoDes);
               A501ContagemResultado_OsFsOsFm = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A501ContagemResultado_OsFsOsFm", A501ContagemResultado_OsFsOsFm);
               A801ContagemResultado_ServicoSigla = GetNextPar( );
               n801ContagemResultado_ServicoSigla = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A801ContagemResultado_ServicoSigla", A801ContagemResultado_ServicoSigla);
               A1313ContagemResultado_ResponsavelPessNome = GetNextPar( );
               n1313ContagemResultado_ResponsavelPessNome = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1313ContagemResultado_ResponsavelPessNome", A1313ContagemResultado_ResponsavelPessNome);
               AV7Vermelho = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Vermelho", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Vermelho), 8, 0)));
               AV39Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavNome_Internalname, AV39Nome);
               AV32ContagemResultado_Servico = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ContagemResultado_Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32ContagemResultado_Servico), 6, 0)));
               AV33ContagemResultado_Demanda = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContagemResultado_Demanda", AV33ContagemResultado_Demanda);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGridvermelho_refresh( subGridvermelho_Rows, A472ContagemResultado_DataEntrega, A484ContagemResultado_StatusDmn, A490ContagemResultado_ContratadaCod, AV10ServerDate, AV6Contratadas, AV36ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, AV21Responsaveis, A531ContagemResultado_StatusUltCnt, A456ContagemResultado_Codigo, A602ContagemResultado_OSVinculada, A499ContagemResultado_ContratadaPessoaCod, AV5WWPContext, AV9Amarelo, A53Contratada_AreaTrabalhoDes, A501ContagemResultado_OsFsOsFm, A801ContagemResultado_ServicoSigla, A1313ContagemResultado_ResponsavelPessNome, AV7Vermelho, AV39Nome, AV32ContagemResultado_Servico, AV33ContagemResultado_Demanda) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridlaranja") == 0 )
            {
               nRC_GXsfl_71 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_71_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_71_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGridlaranja_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Gridlaranja") == 0 )
            {
               subGridlaranja_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A472ContagemResultado_DataEntrega = context.localUtil.ParseDateParm( GetNextPar( ));
               n472ContagemResultado_DataEntrega = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A472ContagemResultado_DataEntrega", context.localUtil.Format(A472ContagemResultado_DataEntrega, "99/99/99"));
               A484ContagemResultado_StatusDmn = GetNextPar( );
               n484ContagemResultado_StatusDmn = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A484ContagemResultado_StatusDmn", A484ContagemResultado_StatusDmn);
               A490ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n490ContagemResultado_ContratadaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A490ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0)));
               AV10ServerDate = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10ServerDate", context.localUtil.Format(AV10ServerDate, "99/99/99"));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6Contratadas);
               AV36ContagemResultado_Responsavel = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ContagemResultado_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36ContagemResultado_Responsavel), 6, 0)));
               A890ContagemResultado_Responsavel = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n890ContagemResultado_Responsavel = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A890ContagemResultado_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A890ContagemResultado_Responsavel), 6, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV21Responsaveis);
               A531ContagemResultado_StatusUltCnt = (short)(NumberUtil.Val( GetNextPar( ), "."));
               n531ContagemResultado_StatusUltCnt = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A531ContagemResultado_StatusUltCnt", StringUtil.LTrim( StringUtil.Str( (decimal)(A531ContagemResultado_StatusUltCnt), 2, 0)));
               A456ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
               A602ContagemResultado_OSVinculada = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n602ContagemResultado_OSVinculada = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A602ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(A602ContagemResultado_OSVinculada), 6, 0)));
               A499ContagemResultado_ContratadaPessoaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n499ContagemResultado_ContratadaPessoaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A499ContagemResultado_ContratadaPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A499ContagemResultado_ContratadaPessoaCod), 6, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV5WWPContext);
               AV9Amarelo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Amarelo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Amarelo), 8, 0)));
               A53Contratada_AreaTrabalhoDes = GetNextPar( );
               n53Contratada_AreaTrabalhoDes = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A53Contratada_AreaTrabalhoDes", A53Contratada_AreaTrabalhoDes);
               A501ContagemResultado_OsFsOsFm = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A501ContagemResultado_OsFsOsFm", A501ContagemResultado_OsFsOsFm);
               A801ContagemResultado_ServicoSigla = GetNextPar( );
               n801ContagemResultado_ServicoSigla = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A801ContagemResultado_ServicoSigla", A801ContagemResultado_ServicoSigla);
               A1313ContagemResultado_ResponsavelPessNome = GetNextPar( );
               n1313ContagemResultado_ResponsavelPessNome = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1313ContagemResultado_ResponsavelPessNome", A1313ContagemResultado_ResponsavelPessNome);
               AV8Laranja = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Laranja", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Laranja), 8, 0)));
               AV39Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavNome_Internalname, AV39Nome);
               AV32ContagemResultado_Servico = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ContagemResultado_Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32ContagemResultado_Servico), 6, 0)));
               AV33ContagemResultado_Demanda = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContagemResultado_Demanda", AV33ContagemResultado_Demanda);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGridlaranja_refresh( subGridlaranja_Rows, A472ContagemResultado_DataEntrega, A484ContagemResultado_StatusDmn, A490ContagemResultado_ContratadaCod, AV10ServerDate, AV6Contratadas, AV36ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, AV21Responsaveis, A531ContagemResultado_StatusUltCnt, A456ContagemResultado_Codigo, A602ContagemResultado_OSVinculada, A499ContagemResultado_ContratadaPessoaCod, AV5WWPContext, AV9Amarelo, A53Contratada_AreaTrabalhoDes, A501ContagemResultado_OsFsOsFm, A801ContagemResultado_ServicoSigla, A1313ContagemResultado_ResponsavelPessNome, AV8Laranja, AV39Nome, AV32ContagemResultado_Servico, AV33ContagemResultado_Demanda) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAL42( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTL42( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206216254991");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Shared/yahoo.js", "");
         context.AddJavascriptSource("DVelop/Shared/dom.js", "");
         context.AddJavascriptSource("DVelop/Shared/event.js", "");
         context.AddJavascriptSource("DVelop/Shared/dragdrop.js", "");
         context.AddJavascriptSource("DVelop/Shared/container.js", "");
         context.AddJavascriptSource("DVelop/ConfirmPanel/ConfirmPanelRender.js", "");
         context.AddJavascriptSource("Window/InNewWindowRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_gestao.aspx") +"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_39", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_39), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_53", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_53), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_71", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_71), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DATAENTREGA", context.localUtil.DToC( A472ContagemResultado_DataEntrega, 0, "/"));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_STATUSDMN", StringUtil.RTrim( A484ContagemResultado_StatusDmn));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vSERVERDATE", context.localUtil.DToC( AV10ServerDate, 0, "/"));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATADAS", AV6Contratadas);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATADAS", AV6Contratadas);
         }
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_RESPONSAVEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(A890ContagemResultado_Responsavel), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vRESPONSAVEIS", AV21Responsaveis);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vRESPONSAVEIS", AV21Responsaveis);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV5WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV5WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "vAMARELO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9Amarelo), 8, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_RESPONSAVELPESSNOME", StringUtil.RTrim( A1313ContagemResultado_ResponsavelPessNome));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_AREATRABALHODES", A53Contratada_AreaTrabalhoDes);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_OSFSOSFM", A501ContagemResultado_OsFsOsFm);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_SERVICOSIGLA", StringUtil.RTrim( A801ContagemResultado_ServicoSigla));
         GxWebStd.gx_hidden_field( context, "vLARANJA", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8Laranja), 8, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vVERMELHO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Vermelho), 8, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSELECIONADAS2", AV53Selecionadas2);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSELECIONADAS2", AV53Selecionadas2);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vPODEATRIBUIRDMN", AV31PodeAtribuirDmn);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSELECIONADAS1", AV50Selecionadas1);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSELECIONADAS1", AV50Selecionadas1);
         }
         GxWebStd.gx_hidden_field( context, "vCODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOUNT1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV51Count1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOUNT2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV52Count2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRIDRESUMO_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDRESUMO_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRIDVERMELHO_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDVERMELHO_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRIDLARANJA_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDLARANJA_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRIDRESUMO_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDRESUMO_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRIDVERMELHO_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDVERMELHO_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRIDLARANJA_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDLARANJA_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_STATUSULTCNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(A531ContagemResultado_StatusUltCnt), 2, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_OSVINCULADA", StringUtil.LTrim( StringUtil.NToC( (decimal)(A602ContagemResultado_OSVinculada), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTRATADAPESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A499ContagemResultado_ContratadaPessoaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRIDRESUMO_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridresumo_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDVERMELHO_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridvermelho_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDLARANJA_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridlaranja_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PNLRESUMO_Width", StringUtil.RTrim( Dvpanel_pnlresumo_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PNLRESUMO_Cls", StringUtil.RTrim( Dvpanel_pnlresumo_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PNLRESUMO_Title", StringUtil.RTrim( Dvpanel_pnlresumo_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PNLRESUMO_Collapsible", StringUtil.BoolToStr( Dvpanel_pnlresumo_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PNLRESUMO_Collapsed", StringUtil.BoolToStr( Dvpanel_pnlresumo_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PNLRESUMO_Autowidth", StringUtil.BoolToStr( Dvpanel_pnlresumo_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PNLRESUMO_Autoheight", StringUtil.BoolToStr( Dvpanel_pnlresumo_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PNLRESUMO_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_pnlresumo_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PNLRESUMO_Iconposition", StringUtil.RTrim( Dvpanel_pnlresumo_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PNLRESUMO_Autoscroll", StringUtil.BoolToStr( Dvpanel_pnlresumo_Autoscroll));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PNLVERMELHO_Width", StringUtil.RTrim( Dvpanel_pnlvermelho_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PNLVERMELHO_Cls", StringUtil.RTrim( Dvpanel_pnlvermelho_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PNLVERMELHO_Title", StringUtil.RTrim( Dvpanel_pnlvermelho_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PNLVERMELHO_Collapsible", StringUtil.BoolToStr( Dvpanel_pnlvermelho_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PNLVERMELHO_Collapsed", StringUtil.BoolToStr( Dvpanel_pnlvermelho_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PNLVERMELHO_Autowidth", StringUtil.BoolToStr( Dvpanel_pnlvermelho_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PNLVERMELHO_Autoheight", StringUtil.BoolToStr( Dvpanel_pnlvermelho_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PNLVERMELHO_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_pnlvermelho_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PNLVERMELHO_Iconposition", StringUtil.RTrim( Dvpanel_pnlvermelho_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PNLVERMELHO_Autoscroll", StringUtil.BoolToStr( Dvpanel_pnlvermelho_Autoscroll));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PNLLARANJA_Width", StringUtil.RTrim( Dvpanel_pnllaranja_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PNLLARANJA_Cls", StringUtil.RTrim( Dvpanel_pnllaranja_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PNLLARANJA_Title", StringUtil.RTrim( Dvpanel_pnllaranja_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PNLLARANJA_Collapsible", StringUtil.BoolToStr( Dvpanel_pnllaranja_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PNLLARANJA_Collapsed", StringUtil.BoolToStr( Dvpanel_pnllaranja_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PNLLARANJA_Autowidth", StringUtil.BoolToStr( Dvpanel_pnllaranja_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PNLLARANJA_Autoheight", StringUtil.BoolToStr( Dvpanel_pnllaranja_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PNLLARANJA_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_pnllaranja_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PNLLARANJA_Iconposition", StringUtil.RTrim( Dvpanel_pnllaranja_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PNLLARANJA_Autoscroll", StringUtil.BoolToStr( Dvpanel_pnllaranja_Autoscroll));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PNLAMARELO_Width", StringUtil.RTrim( Dvpanel_pnlamarelo_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PNLAMARELO_Cls", StringUtil.RTrim( Dvpanel_pnlamarelo_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PNLAMARELO_Title", StringUtil.RTrim( Dvpanel_pnlamarelo_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PNLAMARELO_Collapsible", StringUtil.BoolToStr( Dvpanel_pnlamarelo_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PNLAMARELO_Collapsed", StringUtil.BoolToStr( Dvpanel_pnlamarelo_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PNLAMARELO_Autowidth", StringUtil.BoolToStr( Dvpanel_pnlamarelo_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PNLAMARELO_Autoheight", StringUtil.BoolToStr( Dvpanel_pnlamarelo_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PNLAMARELO_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_pnlamarelo_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PNLAMARELO_Iconposition", StringUtil.RTrim( Dvpanel_pnlamarelo_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PNLAMARELO_Autoscroll", StringUtil.BoolToStr( Dvpanel_pnlamarelo_Autoscroll));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Width", StringUtil.RTrim( Confirmpanel_Width));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Height", StringUtil.RTrim( Confirmpanel_Height));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Title", StringUtil.RTrim( Confirmpanel_Title));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Icon", StringUtil.RTrim( Confirmpanel_Icon));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Confirmtext", StringUtil.RTrim( Confirmpanel_Confirmtext));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Buttonyestext", StringUtil.RTrim( Confirmpanel_Buttonyestext));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Buttonnotext", StringUtil.RTrim( Confirmpanel_Buttonnotext));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Buttoncanceltext", StringUtil.RTrim( Confirmpanel_Buttoncanceltext));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Confirmtype", StringUtil.RTrim( Confirmpanel_Confirmtype));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Draggeable", StringUtil.BoolToStr( Confirmpanel_Draggeable));
         GxWebStd.gx_hidden_field( context, "INNEWWINDOW_Target", StringUtil.RTrim( Innewwindow_Target));
         GxWebStd.gx_hidden_field( context, "vWWPCONTEXT_Contratada_codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV5WWPContext.gxTpr_Contratada_codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "vWWPCONTEXT_Contratada_pessoacod", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV5WWPContext.gxTpr_Contratada_pessoacod), 6, 0, ".", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "</form>") ;
         }
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEL42( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTL42( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_gestao.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WP_Gestao" ;
      }

      public override String GetPgmdesc( )
      {
         return "Gest�o" ;
      }

      protected void WBL40( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_L42( true) ;
         }
         else
         {
            wb_table1_2_L42( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_L42e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTL42( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Gest�o", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPL40( ) ;
      }

      protected void WSL42( )
      {
         STARTL42( ) ;
         EVTL42( ) ;
      }

      protected void EVTL42( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOATUALIZAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11L42 */
                              E11L42 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOLIMPARFILTROS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12L42 */
                              E12L42 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 15), "GRIDRESUMO.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_39_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_39_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_39_idx), 4, 0)), 4, "0");
                              SubsflControlProps_392( ) ;
                              AV39Nome = StringUtil.Upper( cgiGet( edtavNome_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavNome_Internalname, AV39Nome);
                              AV45Quantidade = (short)(context.localUtil.CToN( cgiGet( edtavQuantidade_Internalname), ",", "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQuantidade_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV45Quantidade), 3, 0)));
                              AV46Atraso = (short)(context.localUtil.CToN( cgiGet( edtavAtraso_Internalname), ",", "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAtraso_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV46Atraso), 4, 0)));
                              AV44Glosa = context.localUtil.CToN( cgiGet( edtavGlosa_Internalname), ",", ".");
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavGlosa_Internalname, StringUtil.LTrim( StringUtil.Str( AV44Glosa, 18, 5)));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E13L42 */
                                    E13L42 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRIDRESUMO.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E14L42 */
                                    E14L42 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E15L42 */
                                    E15L42 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                           else if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 17), "GRIDVERMELHO.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 13), "'DOATRIBUIR1'") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 16), "VSELECTED1.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 16), "VSELECTED1.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 13), "'DOATRIBUIR1'") == 0 ) )
                           {
                              nGXsfl_53_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_53_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_53_idx), 4, 0)), 4, "0");
                              SubsflControlProps_536( ) ;
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavCodigo1_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCodigo1_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCODIGO1");
                                 GX_FocusControl = edtavCodigo1_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV25Codigo1 = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCodigo1_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV25Codigo1), 6, 0)));
                              }
                              else
                              {
                                 AV25Codigo1 = (int)(context.localUtil.CToN( cgiGet( edtavCodigo1_Internalname), ",", "."));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCodigo1_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV25Codigo1), 6, 0)));
                              }
                              if ( context.localUtil.VCDateTime( cgiGet( edtavVcontagemresultado_dataentrega_Internalname), 0, 0) == 0 )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"VContagem Resultado_Data Entrega"}), 1, "vVCONTAGEMRESULTADO_DATAENTREGA");
                                 GX_FocusControl = edtavVcontagemresultado_dataentrega_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV16VContagemResultado_DataEntrega = (DateTime)(DateTime.MinValue);
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavVcontagemresultado_dataentrega_Internalname, context.localUtil.Format(AV16VContagemResultado_DataEntrega, "99/99/99"));
                              }
                              else
                              {
                                 AV16VContagemResultado_DataEntrega = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavVcontagemresultado_dataentrega_Internalname), 0));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavVcontagemresultado_dataentrega_Internalname, context.localUtil.Format(AV16VContagemResultado_DataEntrega, "99/99/99"));
                              }
                              AV14VAreaTrabalho_Descricao = StringUtil.Upper( cgiGet( edtavVareatrabalho_descricao_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavVareatrabalho_descricao_Internalname, AV14VAreaTrabalho_Descricao);
                              AV15VContagemResultado_OsFsOsFm = cgiGet( edtavVcontagemresultado_osfsosfm_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavVcontagemresultado_osfsosfm_Internalname, AV15VContagemResultado_OsFsOsFm);
                              AV35VServico = StringUtil.Upper( cgiGet( edtavVservico_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavVservico_Internalname, AV35VServico);
                              AV20VResponsavelNome = StringUtil.Upper( cgiGet( edtavVresponsavelnome_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavVresponsavelnome_Internalname, AV20VResponsavelNome);
                              AV26Selected1 = StringUtil.StrToBool( cgiGet( chkavSelected1_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavSelected1_Internalname, AV26Selected1);
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavVatraso_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavVatraso_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vVATRASO");
                                 GX_FocusControl = edtavVatraso_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV37VAtraso = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavVatraso_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV37VAtraso), 4, 0)));
                              }
                              else
                              {
                                 AV37VAtraso = (short)(context.localUtil.CToN( cgiGet( edtavVatraso_Internalname), ",", "."));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavVatraso_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV37VAtraso), 4, 0)));
                              }
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavVglosa_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavVglosa_Internalname), ",", ".") > 999999999999.99999m ) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vVGLOSA");
                                 GX_FocusControl = edtavVglosa_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV38VGlosa = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavVglosa_Internalname, StringUtil.LTrim( StringUtil.Str( AV38VGlosa, 18, 5)));
                              }
                              else
                              {
                                 AV38VGlosa = context.localUtil.CToN( cgiGet( edtavVglosa_Internalname), ",", ".");
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavVglosa_Internalname, StringUtil.LTrim( StringUtil.Str( AV38VGlosa, 18, 5)));
                              }
                              AV29Atribuir1 = cgiGet( edtavAtribuir1_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAtribuir1_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Atribuir1)) ? AV60Atribuir1_GXI : context.convertURL( context.PathToRelativeUrl( AV29Atribuir1))));
                              AV30Historico1 = cgiGet( edtavHistorico1_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavHistorico1_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV30Historico1)) ? AV61Historico1_GXI : context.convertURL( context.PathToRelativeUrl( AV30Historico1))));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "GRIDVERMELHO.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E16L46 */
                                    E16L46 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "'DOATRIBUIR1'") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E17L42 */
                                    E17L42 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VSELECTED1.CLICK") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E18L42 */
                                    E18L42 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                           else if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 16), "GRIDLARANJA.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 13), "'DOATRIBUIR2'") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 16), "VSELECTED2.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 16), "VSELECTED2.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 13), "'DOATRIBUIR2'") == 0 ) )
                           {
                              nGXsfl_71_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_71_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_71_idx), 4, 0)), 4, "0");
                              SubsflControlProps_714( ) ;
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavCodigo2_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCodigo2_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCODIGO2");
                                 GX_FocusControl = edtavCodigo2_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV23Codigo2 = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCodigo2_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV23Codigo2), 6, 0)));
                              }
                              else
                              {
                                 AV23Codigo2 = (int)(context.localUtil.CToN( cgiGet( edtavCodigo2_Internalname), ",", "."));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCodigo2_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV23Codigo2), 6, 0)));
                              }
                              AV11LAreaTrabalho_Descricao = StringUtil.Upper( cgiGet( edtavLareatrabalho_descricao_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavLareatrabalho_descricao_Internalname, AV11LAreaTrabalho_Descricao);
                              AV12LContagemResultado_OsFsOsFm = cgiGet( edtavLcontagemresultado_osfsosfm_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavLcontagemresultado_osfsosfm_Internalname, AV12LContagemResultado_OsFsOsFm);
                              AV34LServico = StringUtil.Upper( cgiGet( edtavLservico_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavLservico_Internalname, AV34LServico);
                              AV19LResponsavelNome = StringUtil.Upper( cgiGet( edtavLresponsavelnome_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavLresponsavelnome_Internalname, AV19LResponsavelNome);
                              AV24Selected2 = StringUtil.StrToBool( cgiGet( chkavSelected2_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavSelected2_Internalname, AV24Selected2);
                              AV27Atribuir2 = cgiGet( edtavAtribuir2_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAtribuir2_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV27Atribuir2)) ? AV57Atribuir2_GXI : context.convertURL( context.PathToRelativeUrl( AV27Atribuir2))));
                              AV28Historico2 = cgiGet( edtavHistorico2_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavHistorico2_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Historico2)) ? AV58Historico2_GXI : context.convertURL( context.PathToRelativeUrl( AV28Historico2))));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "GRIDLARANJA.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E19L44 */
                                    E19L44 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "'DOATRIBUIR2'") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E20L42 */
                                    E20L42 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VSELECTED2.CLICK") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E21L42 */
                                    E21L42 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEL42( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAL42( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavContagemresultado_servico.Name = "vCONTAGEMRESULTADO_SERVICO";
            dynavContagemresultado_servico.WebTags = "";
            cmbavContagemresultado_responsavel.Name = "vCONTAGEMRESULTADO_RESPONSAVEL";
            cmbavContagemresultado_responsavel.WebTags = "";
            cmbavContagemresultado_responsavel.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "(Respons�vel)", 0);
            if ( cmbavContagemresultado_responsavel.ItemCount > 0 )
            {
               AV36ContagemResultado_Responsavel = (int)(NumberUtil.Val( cmbavContagemresultado_responsavel.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV36ContagemResultado_Responsavel), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ContagemResultado_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36ContagemResultado_Responsavel), 6, 0)));
            }
            GXCCtl = "vSELECTED1_" + sGXsfl_53_idx;
            chkavSelected1.Name = GXCCtl;
            chkavSelected1.WebTags = "";
            chkavSelected1.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavSelected1_Internalname, "TitleCaption", chkavSelected1.Caption);
            chkavSelected1.CheckedValue = "false";
            GXCCtl = "vSELECTED2_" + sGXsfl_71_idx;
            chkavSelected2.Name = GXCCtl;
            chkavSelected2.WebTags = "";
            chkavSelected2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavSelected2_Internalname, "TitleCaption", chkavSelected2.Caption);
            chkavSelected2.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavContagemresultado_demanda_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvCONTAGEMRESULTADO_SERVICOL42( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_SERVICO_dataL42( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_SERVICO_htmlL42( )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_SERVICO_dataL42( ) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_servico.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_servico.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_servico.ItemCount > 0 )
         {
            AV32ContagemResultado_Servico = (int)(NumberUtil.Val( dynavContagemresultado_servico.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV32ContagemResultado_Servico), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ContagemResultado_Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32ContagemResultado_Servico), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_SERVICO_dataL42( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Servi�o)");
         /* Using cursor H00L42 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00L42_A155Servico_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00L42_A605Servico_Sigla[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void gxnrGridresumo_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_392( ) ;
         while ( nGXsfl_39_idx <= nRC_GXsfl_39 )
         {
            sendrow_392( ) ;
            nGXsfl_39_idx = (short)(((subGridresumo_Islastpage==1)&&(nGXsfl_39_idx+1>subGridresumo_Recordsperpage( )) ? 1 : nGXsfl_39_idx+1));
            sGXsfl_39_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_39_idx), 4, 0)), 4, "0");
            SubsflControlProps_392( ) ;
         }
         context.GX_webresponse.AddString(GridresumoContainer.ToJavascriptSource());
         /* End function gxnrGridresumo_newrow */
      }

      protected void gxnrGridlaranja_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_714( ) ;
         while ( nGXsfl_71_idx <= nRC_GXsfl_71 )
         {
            sendrow_714( ) ;
            nGXsfl_71_idx = (short)(((subGridlaranja_Islastpage==1)&&(nGXsfl_71_idx+1>subGridlaranja_Recordsperpage( )) ? 1 : nGXsfl_71_idx+1));
            sGXsfl_71_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_71_idx), 4, 0)), 4, "0");
            SubsflControlProps_714( ) ;
         }
         context.GX_webresponse.AddString(GridlaranjaContainer.ToJavascriptSource());
         /* End function gxnrGridlaranja_newrow */
      }

      protected void gxnrGridvermelho_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_536( ) ;
         while ( nGXsfl_53_idx <= nRC_GXsfl_53 )
         {
            sendrow_536( ) ;
            nGXsfl_53_idx = (short)(((subGridvermelho_Islastpage==1)&&(nGXsfl_53_idx+1>subGridvermelho_Recordsperpage( )) ? 1 : nGXsfl_53_idx+1));
            sGXsfl_53_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_53_idx), 4, 0)), 4, "0");
            SubsflControlProps_536( ) ;
         }
         context.GX_webresponse.AddString(GridvermelhoContainer.ToJavascriptSource());
         /* End function gxnrGridvermelho_newrow */
      }

      protected void gxgrGridresumo_refresh( int subGridresumo_Rows ,
                                             DateTime A472ContagemResultado_DataEntrega ,
                                             String A484ContagemResultado_StatusDmn ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             DateTime AV10ServerDate ,
                                             IGxCollection AV6Contratadas ,
                                             int AV36ContagemResultado_Responsavel ,
                                             int A890ContagemResultado_Responsavel ,
                                             IGxCollection AV21Responsaveis ,
                                             short A531ContagemResultado_StatusUltCnt ,
                                             int A456ContagemResultado_Codigo ,
                                             int A602ContagemResultado_OSVinculada ,
                                             int A499ContagemResultado_ContratadaPessoaCod ,
                                             wwpbaseobjects.SdtWWPContext AV5WWPContext ,
                                             int AV9Amarelo ,
                                             String A1313ContagemResultado_ResponsavelPessNome ,
                                             String AV39Nome ,
                                             int AV32ContagemResultado_Servico ,
                                             String AV33ContagemResultado_Demanda )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRIDRESUMO_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridresumo_Rows), 6, 0, ".", "")));
         GRIDRESUMO_nCurrentRecord = 0;
         RFL42( ) ;
         /* End function gxgrGridresumo_refresh */
      }

      protected void gxgrGridvermelho_refresh( int subGridvermelho_Rows ,
                                               DateTime A472ContagemResultado_DataEntrega ,
                                               String A484ContagemResultado_StatusDmn ,
                                               int A490ContagemResultado_ContratadaCod ,
                                               DateTime AV10ServerDate ,
                                               IGxCollection AV6Contratadas ,
                                               int AV36ContagemResultado_Responsavel ,
                                               int A890ContagemResultado_Responsavel ,
                                               IGxCollection AV21Responsaveis ,
                                               short A531ContagemResultado_StatusUltCnt ,
                                               int A456ContagemResultado_Codigo ,
                                               int A602ContagemResultado_OSVinculada ,
                                               int A499ContagemResultado_ContratadaPessoaCod ,
                                               wwpbaseobjects.SdtWWPContext AV5WWPContext ,
                                               int AV9Amarelo ,
                                               String A53Contratada_AreaTrabalhoDes ,
                                               String A501ContagemResultado_OsFsOsFm ,
                                               String A801ContagemResultado_ServicoSigla ,
                                               String A1313ContagemResultado_ResponsavelPessNome ,
                                               int AV7Vermelho ,
                                               String AV39Nome ,
                                               int AV32ContagemResultado_Servico ,
                                               String AV33ContagemResultado_Demanda )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRIDVERMELHO_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridvermelho_Rows), 6, 0, ".", "")));
         /* Execute user event: E15L42 */
         E15L42 ();
         GRIDVERMELHO_nCurrentRecord = 0;
         RFL46( ) ;
         /* End function gxgrGridvermelho_refresh */
      }

      protected void gxgrGridlaranja_refresh( int subGridlaranja_Rows ,
                                              DateTime A472ContagemResultado_DataEntrega ,
                                              String A484ContagemResultado_StatusDmn ,
                                              int A490ContagemResultado_ContratadaCod ,
                                              DateTime AV10ServerDate ,
                                              IGxCollection AV6Contratadas ,
                                              int AV36ContagemResultado_Responsavel ,
                                              int A890ContagemResultado_Responsavel ,
                                              IGxCollection AV21Responsaveis ,
                                              short A531ContagemResultado_StatusUltCnt ,
                                              int A456ContagemResultado_Codigo ,
                                              int A602ContagemResultado_OSVinculada ,
                                              int A499ContagemResultado_ContratadaPessoaCod ,
                                              wwpbaseobjects.SdtWWPContext AV5WWPContext ,
                                              int AV9Amarelo ,
                                              String A53Contratada_AreaTrabalhoDes ,
                                              String A501ContagemResultado_OsFsOsFm ,
                                              String A801ContagemResultado_ServicoSigla ,
                                              String A1313ContagemResultado_ResponsavelPessNome ,
                                              int AV8Laranja ,
                                              String AV39Nome ,
                                              int AV32ContagemResultado_Servico ,
                                              String AV33ContagemResultado_Demanda )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRIDLARANJA_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridlaranja_Rows), 6, 0, ".", "")));
         /* Execute user event: E15L42 */
         E15L42 ();
         GRIDLARANJA_nCurrentRecord = 0;
         RFL44( ) ;
         /* End function gxgrGridlaranja_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavContagemresultado_servico.ItemCount > 0 )
         {
            AV32ContagemResultado_Servico = (int)(NumberUtil.Val( dynavContagemresultado_servico.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV32ContagemResultado_Servico), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ContagemResultado_Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32ContagemResultado_Servico), 6, 0)));
         }
         if ( cmbavContagemresultado_responsavel.ItemCount > 0 )
         {
            AV36ContagemResultado_Responsavel = (int)(NumberUtil.Val( cmbavContagemresultado_responsavel.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV36ContagemResultado_Responsavel), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ContagemResultado_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36ContagemResultado_Responsavel), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFL42( ) ;
         RFL46( ) ;
         RFL44( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavNome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNome_Enabled), 5, 0)));
         edtavQuantidade_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQuantidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQuantidade_Enabled), 5, 0)));
         edtavAtraso_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAtraso_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAtraso_Enabled), 5, 0)));
         edtavGlosa_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGlosa_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGlosa_Enabled), 5, 0)));
         edtavCodigo1_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCodigo1_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCodigo1_Enabled), 5, 0)));
         edtavVcontagemresultado_dataentrega_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavVcontagemresultado_dataentrega_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVcontagemresultado_dataentrega_Enabled), 5, 0)));
         edtavVareatrabalho_descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavVareatrabalho_descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVareatrabalho_descricao_Enabled), 5, 0)));
         edtavVcontagemresultado_osfsosfm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavVcontagemresultado_osfsosfm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVcontagemresultado_osfsosfm_Enabled), 5, 0)));
         edtavVservico_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavVservico_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVservico_Enabled), 5, 0)));
         edtavVresponsavelnome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavVresponsavelnome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVresponsavelnome_Enabled), 5, 0)));
         edtavVatraso_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavVatraso_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVatraso_Enabled), 5, 0)));
         edtavVglosa_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavVglosa_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVglosa_Enabled), 5, 0)));
         edtavCodigo2_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCodigo2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCodigo2_Enabled), 5, 0)));
         edtavLareatrabalho_descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLareatrabalho_descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLareatrabalho_descricao_Enabled), 5, 0)));
         edtavLcontagemresultado_osfsosfm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLcontagemresultado_osfsosfm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLcontagemresultado_osfsosfm_Enabled), 5, 0)));
         edtavLservico_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLservico_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLservico_Enabled), 5, 0)));
         edtavLresponsavelnome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLresponsavelnome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLresponsavelnome_Enabled), 5, 0)));
      }

      protected void RFL42( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridresumoContainer.ClearRows();
         }
         wbStart = 39;
         /* Execute user event: E15L42 */
         E15L42 ();
         nGXsfl_39_idx = 1;
         sGXsfl_39_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_39_idx), 4, 0)), 4, "0");
         SubsflControlProps_392( ) ;
         nGXsfl_39_Refreshing = 1;
         GridresumoContainer.AddObjectProperty("GridName", "Gridresumo");
         GridresumoContainer.AddObjectProperty("CmpContext", "");
         GridresumoContainer.AddObjectProperty("InMasterPage", "false");
         GridresumoContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridresumoContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridresumoContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridresumoContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridresumo_Backcolorstyle), 1, 0, ".", "")));
         GridresumoContainer.PageSize = subGridresumo_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_392( ) ;
            /* Execute user event: E14L42 */
            E14L42 ();
            if ( ( GRIDRESUMO_nCurrentRecord > 0 ) && ( GRIDRESUMO_nGridOutOfScope == 0 ) && ( nGXsfl_39_idx == 1 ) )
            {
               GRIDRESUMO_nCurrentRecord = 0;
               GRIDRESUMO_nGridOutOfScope = 1;
               subgridresumo_firstpage( ) ;
               /* Execute user event: E14L42 */
               E14L42 ();
            }
            wbEnd = 39;
            WBL40( ) ;
         }
         nGXsfl_39_Refreshing = 0;
      }

      protected void RFL44( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridlaranjaContainer.ClearRows();
         }
         wbStart = 71;
         nGXsfl_71_idx = 1;
         sGXsfl_71_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_71_idx), 4, 0)), 4, "0");
         SubsflControlProps_714( ) ;
         nGXsfl_71_Refreshing = 1;
         GridlaranjaContainer.AddObjectProperty("GridName", "Gridlaranja");
         GridlaranjaContainer.AddObjectProperty("CmpContext", "");
         GridlaranjaContainer.AddObjectProperty("InMasterPage", "false");
         GridlaranjaContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridlaranjaContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridlaranjaContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridlaranjaContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridlaranja_Backcolorstyle), 1, 0, ".", "")));
         GridlaranjaContainer.PageSize = subGridlaranja_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_714( ) ;
            /* Execute user event: E19L44 */
            E19L44 ();
            if ( ( GRIDLARANJA_nCurrentRecord > 0 ) && ( GRIDLARANJA_nGridOutOfScope == 0 ) && ( nGXsfl_71_idx == 1 ) )
            {
               GRIDLARANJA_nCurrentRecord = 0;
               GRIDLARANJA_nGridOutOfScope = 1;
               subgridlaranja_firstpage( ) ;
               /* Execute user event: E19L44 */
               E19L44 ();
            }
            wbEnd = 71;
            WBL40( ) ;
         }
         nGXsfl_71_Refreshing = 0;
      }

      protected void RFL46( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridvermelhoContainer.ClearRows();
         }
         wbStart = 53;
         nGXsfl_53_idx = 1;
         sGXsfl_53_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_53_idx), 4, 0)), 4, "0");
         SubsflControlProps_536( ) ;
         nGXsfl_53_Refreshing = 1;
         GridvermelhoContainer.AddObjectProperty("GridName", "Gridvermelho");
         GridvermelhoContainer.AddObjectProperty("CmpContext", "");
         GridvermelhoContainer.AddObjectProperty("InMasterPage", "false");
         GridvermelhoContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridvermelhoContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridvermelhoContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridvermelhoContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridvermelho_Backcolorstyle), 1, 0, ".", "")));
         GridvermelhoContainer.PageSize = subGridvermelho_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_536( ) ;
            /* Execute user event: E16L46 */
            E16L46 ();
            if ( ( GRIDVERMELHO_nCurrentRecord > 0 ) && ( GRIDVERMELHO_nGridOutOfScope == 0 ) && ( nGXsfl_53_idx == 1 ) )
            {
               GRIDVERMELHO_nCurrentRecord = 0;
               GRIDVERMELHO_nGridOutOfScope = 1;
               subgridvermelho_firstpage( ) ;
               /* Execute user event: E16L46 */
               E16L46 ();
            }
            wbEnd = 53;
            WBL40( ) ;
         }
         nGXsfl_53_Refreshing = 0;
      }

      protected int subGridresumo_Pagecount( )
      {
         GRIDRESUMO_nRecordCount = subGridresumo_Recordcount( );
         if ( ((int)((GRIDRESUMO_nRecordCount) % (subGridresumo_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRIDRESUMO_nRecordCount/ (decimal)(subGridresumo_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRIDRESUMO_nRecordCount/ (decimal)(subGridresumo_Recordsperpage( ))))+1) ;
      }

      protected int subGridresumo_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGridresumo_Recordsperpage( )
      {
         if ( subGridresumo_Rows > 0 )
         {
            return subGridresumo_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGridresumo_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected short subgridresumo_firstpage( )
      {
         GRIDRESUMO_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRIDRESUMO_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDRESUMO_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridresumo_refresh( subGridresumo_Rows, A472ContagemResultado_DataEntrega, A484ContagemResultado_StatusDmn, A490ContagemResultado_ContratadaCod, AV10ServerDate, AV6Contratadas, AV36ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, AV21Responsaveis, A531ContagemResultado_StatusUltCnt, A456ContagemResultado_Codigo, A602ContagemResultado_OSVinculada, A499ContagemResultado_ContratadaPessoaCod, AV5WWPContext, AV9Amarelo, A1313ContagemResultado_ResponsavelPessNome, AV39Nome, AV32ContagemResultado_Servico, AV33ContagemResultado_Demanda) ;
         }
         return 0 ;
      }

      protected short subgridresumo_nextpage( )
      {
         if ( GRIDRESUMO_nEOF == 0 )
         {
            GRIDRESUMO_nFirstRecordOnPage = (long)(GRIDRESUMO_nFirstRecordOnPage+subGridresumo_Recordsperpage( ));
         }
         GxWebStd.gx_hidden_field( context, "GRIDRESUMO_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDRESUMO_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridresumo_refresh( subGridresumo_Rows, A472ContagemResultado_DataEntrega, A484ContagemResultado_StatusDmn, A490ContagemResultado_ContratadaCod, AV10ServerDate, AV6Contratadas, AV36ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, AV21Responsaveis, A531ContagemResultado_StatusUltCnt, A456ContagemResultado_Codigo, A602ContagemResultado_OSVinculada, A499ContagemResultado_ContratadaPessoaCod, AV5WWPContext, AV9Amarelo, A1313ContagemResultado_ResponsavelPessNome, AV39Nome, AV32ContagemResultado_Servico, AV33ContagemResultado_Demanda) ;
         }
         return (short)(((GRIDRESUMO_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgridresumo_previouspage( )
      {
         if ( GRIDRESUMO_nFirstRecordOnPage >= subGridresumo_Recordsperpage( ) )
         {
            GRIDRESUMO_nFirstRecordOnPage = (long)(GRIDRESUMO_nFirstRecordOnPage-subGridresumo_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRIDRESUMO_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDRESUMO_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridresumo_refresh( subGridresumo_Rows, A472ContagemResultado_DataEntrega, A484ContagemResultado_StatusDmn, A490ContagemResultado_ContratadaCod, AV10ServerDate, AV6Contratadas, AV36ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, AV21Responsaveis, A531ContagemResultado_StatusUltCnt, A456ContagemResultado_Codigo, A602ContagemResultado_OSVinculada, A499ContagemResultado_ContratadaPessoaCod, AV5WWPContext, AV9Amarelo, A1313ContagemResultado_ResponsavelPessNome, AV39Nome, AV32ContagemResultado_Servico, AV33ContagemResultado_Demanda) ;
         }
         return 0 ;
      }

      protected short subgridresumo_lastpage( )
      {
         subGridresumo_Islastpage = 1;
         if ( isFullAjaxMode( ) )
         {
            gxgrGridresumo_refresh( subGridresumo_Rows, A472ContagemResultado_DataEntrega, A484ContagemResultado_StatusDmn, A490ContagemResultado_ContratadaCod, AV10ServerDate, AV6Contratadas, AV36ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, AV21Responsaveis, A531ContagemResultado_StatusUltCnt, A456ContagemResultado_Codigo, A602ContagemResultado_OSVinculada, A499ContagemResultado_ContratadaPessoaCod, AV5WWPContext, AV9Amarelo, A1313ContagemResultado_ResponsavelPessNome, AV39Nome, AV32ContagemResultado_Servico, AV33ContagemResultado_Demanda) ;
         }
         return 0 ;
      }

      protected int subgridresumo_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRIDRESUMO_nFirstRecordOnPage = (long)(subGridresumo_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRIDRESUMO_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRIDRESUMO_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDRESUMO_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridresumo_refresh( subGridresumo_Rows, A472ContagemResultado_DataEntrega, A484ContagemResultado_StatusDmn, A490ContagemResultado_ContratadaCod, AV10ServerDate, AV6Contratadas, AV36ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, AV21Responsaveis, A531ContagemResultado_StatusUltCnt, A456ContagemResultado_Codigo, A602ContagemResultado_OSVinculada, A499ContagemResultado_ContratadaPessoaCod, AV5WWPContext, AV9Amarelo, A1313ContagemResultado_ResponsavelPessNome, AV39Nome, AV32ContagemResultado_Servico, AV33ContagemResultado_Demanda) ;
         }
         return (int)(0) ;
      }

      protected int subGridlaranja_Pagecount( )
      {
         GRIDLARANJA_nRecordCount = subGridlaranja_Recordcount( );
         if ( ((int)((GRIDLARANJA_nRecordCount) % (subGridlaranja_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRIDLARANJA_nRecordCount/ (decimal)(subGridlaranja_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRIDLARANJA_nRecordCount/ (decimal)(subGridlaranja_Recordsperpage( ))))+1) ;
      }

      protected int subGridlaranja_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGridlaranja_Recordsperpage( )
      {
         if ( subGridlaranja_Rows > 0 )
         {
            return subGridlaranja_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGridlaranja_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected short subgridlaranja_firstpage( )
      {
         GRIDLARANJA_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRIDLARANJA_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDLARANJA_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridlaranja_refresh( subGridlaranja_Rows, A472ContagemResultado_DataEntrega, A484ContagemResultado_StatusDmn, A490ContagemResultado_ContratadaCod, AV10ServerDate, AV6Contratadas, AV36ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, AV21Responsaveis, A531ContagemResultado_StatusUltCnt, A456ContagemResultado_Codigo, A602ContagemResultado_OSVinculada, A499ContagemResultado_ContratadaPessoaCod, AV5WWPContext, AV9Amarelo, A53Contratada_AreaTrabalhoDes, A501ContagemResultado_OsFsOsFm, A801ContagemResultado_ServicoSigla, A1313ContagemResultado_ResponsavelPessNome, AV8Laranja, AV39Nome, AV32ContagemResultado_Servico, AV33ContagemResultado_Demanda) ;
         }
         return 0 ;
      }

      protected short subgridlaranja_nextpage( )
      {
         if ( GRIDLARANJA_nEOF == 0 )
         {
            GRIDLARANJA_nFirstRecordOnPage = (long)(GRIDLARANJA_nFirstRecordOnPage+subGridlaranja_Recordsperpage( ));
         }
         GxWebStd.gx_hidden_field( context, "GRIDLARANJA_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDLARANJA_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridlaranja_refresh( subGridlaranja_Rows, A472ContagemResultado_DataEntrega, A484ContagemResultado_StatusDmn, A490ContagemResultado_ContratadaCod, AV10ServerDate, AV6Contratadas, AV36ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, AV21Responsaveis, A531ContagemResultado_StatusUltCnt, A456ContagemResultado_Codigo, A602ContagemResultado_OSVinculada, A499ContagemResultado_ContratadaPessoaCod, AV5WWPContext, AV9Amarelo, A53Contratada_AreaTrabalhoDes, A501ContagemResultado_OsFsOsFm, A801ContagemResultado_ServicoSigla, A1313ContagemResultado_ResponsavelPessNome, AV8Laranja, AV39Nome, AV32ContagemResultado_Servico, AV33ContagemResultado_Demanda) ;
         }
         return (short)(((GRIDLARANJA_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgridlaranja_previouspage( )
      {
         if ( GRIDLARANJA_nFirstRecordOnPage >= subGridlaranja_Recordsperpage( ) )
         {
            GRIDLARANJA_nFirstRecordOnPage = (long)(GRIDLARANJA_nFirstRecordOnPage-subGridlaranja_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRIDLARANJA_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDLARANJA_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridlaranja_refresh( subGridlaranja_Rows, A472ContagemResultado_DataEntrega, A484ContagemResultado_StatusDmn, A490ContagemResultado_ContratadaCod, AV10ServerDate, AV6Contratadas, AV36ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, AV21Responsaveis, A531ContagemResultado_StatusUltCnt, A456ContagemResultado_Codigo, A602ContagemResultado_OSVinculada, A499ContagemResultado_ContratadaPessoaCod, AV5WWPContext, AV9Amarelo, A53Contratada_AreaTrabalhoDes, A501ContagemResultado_OsFsOsFm, A801ContagemResultado_ServicoSigla, A1313ContagemResultado_ResponsavelPessNome, AV8Laranja, AV39Nome, AV32ContagemResultado_Servico, AV33ContagemResultado_Demanda) ;
         }
         return 0 ;
      }

      protected short subgridlaranja_lastpage( )
      {
         subGridlaranja_Islastpage = 1;
         if ( isFullAjaxMode( ) )
         {
            gxgrGridlaranja_refresh( subGridlaranja_Rows, A472ContagemResultado_DataEntrega, A484ContagemResultado_StatusDmn, A490ContagemResultado_ContratadaCod, AV10ServerDate, AV6Contratadas, AV36ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, AV21Responsaveis, A531ContagemResultado_StatusUltCnt, A456ContagemResultado_Codigo, A602ContagemResultado_OSVinculada, A499ContagemResultado_ContratadaPessoaCod, AV5WWPContext, AV9Amarelo, A53Contratada_AreaTrabalhoDes, A501ContagemResultado_OsFsOsFm, A801ContagemResultado_ServicoSigla, A1313ContagemResultado_ResponsavelPessNome, AV8Laranja, AV39Nome, AV32ContagemResultado_Servico, AV33ContagemResultado_Demanda) ;
         }
         return 0 ;
      }

      protected int subgridlaranja_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRIDLARANJA_nFirstRecordOnPage = (long)(subGridlaranja_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRIDLARANJA_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRIDLARANJA_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDLARANJA_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridlaranja_refresh( subGridlaranja_Rows, A472ContagemResultado_DataEntrega, A484ContagemResultado_StatusDmn, A490ContagemResultado_ContratadaCod, AV10ServerDate, AV6Contratadas, AV36ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, AV21Responsaveis, A531ContagemResultado_StatusUltCnt, A456ContagemResultado_Codigo, A602ContagemResultado_OSVinculada, A499ContagemResultado_ContratadaPessoaCod, AV5WWPContext, AV9Amarelo, A53Contratada_AreaTrabalhoDes, A501ContagemResultado_OsFsOsFm, A801ContagemResultado_ServicoSigla, A1313ContagemResultado_ResponsavelPessNome, AV8Laranja, AV39Nome, AV32ContagemResultado_Servico, AV33ContagemResultado_Demanda) ;
         }
         return (int)(0) ;
      }

      protected int subGridvermelho_Pagecount( )
      {
         GRIDVERMELHO_nRecordCount = subGridvermelho_Recordcount( );
         if ( ((int)((GRIDVERMELHO_nRecordCount) % (subGridvermelho_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRIDVERMELHO_nRecordCount/ (decimal)(subGridvermelho_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRIDVERMELHO_nRecordCount/ (decimal)(subGridvermelho_Recordsperpage( ))))+1) ;
      }

      protected int subGridvermelho_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGridvermelho_Recordsperpage( )
      {
         if ( subGridvermelho_Rows > 0 )
         {
            return subGridvermelho_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGridvermelho_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected short subgridvermelho_firstpage( )
      {
         GRIDVERMELHO_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRIDVERMELHO_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDVERMELHO_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridvermelho_refresh( subGridvermelho_Rows, A472ContagemResultado_DataEntrega, A484ContagemResultado_StatusDmn, A490ContagemResultado_ContratadaCod, AV10ServerDate, AV6Contratadas, AV36ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, AV21Responsaveis, A531ContagemResultado_StatusUltCnt, A456ContagemResultado_Codigo, A602ContagemResultado_OSVinculada, A499ContagemResultado_ContratadaPessoaCod, AV5WWPContext, AV9Amarelo, A53Contratada_AreaTrabalhoDes, A501ContagemResultado_OsFsOsFm, A801ContagemResultado_ServicoSigla, A1313ContagemResultado_ResponsavelPessNome, AV7Vermelho, AV39Nome, AV32ContagemResultado_Servico, AV33ContagemResultado_Demanda) ;
         }
         return 0 ;
      }

      protected short subgridvermelho_nextpage( )
      {
         if ( GRIDVERMELHO_nEOF == 0 )
         {
            GRIDVERMELHO_nFirstRecordOnPage = (long)(GRIDVERMELHO_nFirstRecordOnPage+subGridvermelho_Recordsperpage( ));
         }
         GxWebStd.gx_hidden_field( context, "GRIDVERMELHO_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDVERMELHO_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridvermelho_refresh( subGridvermelho_Rows, A472ContagemResultado_DataEntrega, A484ContagemResultado_StatusDmn, A490ContagemResultado_ContratadaCod, AV10ServerDate, AV6Contratadas, AV36ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, AV21Responsaveis, A531ContagemResultado_StatusUltCnt, A456ContagemResultado_Codigo, A602ContagemResultado_OSVinculada, A499ContagemResultado_ContratadaPessoaCod, AV5WWPContext, AV9Amarelo, A53Contratada_AreaTrabalhoDes, A501ContagemResultado_OsFsOsFm, A801ContagemResultado_ServicoSigla, A1313ContagemResultado_ResponsavelPessNome, AV7Vermelho, AV39Nome, AV32ContagemResultado_Servico, AV33ContagemResultado_Demanda) ;
         }
         return (short)(((GRIDVERMELHO_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgridvermelho_previouspage( )
      {
         if ( GRIDVERMELHO_nFirstRecordOnPage >= subGridvermelho_Recordsperpage( ) )
         {
            GRIDVERMELHO_nFirstRecordOnPage = (long)(GRIDVERMELHO_nFirstRecordOnPage-subGridvermelho_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRIDVERMELHO_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDVERMELHO_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridvermelho_refresh( subGridvermelho_Rows, A472ContagemResultado_DataEntrega, A484ContagemResultado_StatusDmn, A490ContagemResultado_ContratadaCod, AV10ServerDate, AV6Contratadas, AV36ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, AV21Responsaveis, A531ContagemResultado_StatusUltCnt, A456ContagemResultado_Codigo, A602ContagemResultado_OSVinculada, A499ContagemResultado_ContratadaPessoaCod, AV5WWPContext, AV9Amarelo, A53Contratada_AreaTrabalhoDes, A501ContagemResultado_OsFsOsFm, A801ContagemResultado_ServicoSigla, A1313ContagemResultado_ResponsavelPessNome, AV7Vermelho, AV39Nome, AV32ContagemResultado_Servico, AV33ContagemResultado_Demanda) ;
         }
         return 0 ;
      }

      protected short subgridvermelho_lastpage( )
      {
         subGridvermelho_Islastpage = 1;
         if ( isFullAjaxMode( ) )
         {
            gxgrGridvermelho_refresh( subGridvermelho_Rows, A472ContagemResultado_DataEntrega, A484ContagemResultado_StatusDmn, A490ContagemResultado_ContratadaCod, AV10ServerDate, AV6Contratadas, AV36ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, AV21Responsaveis, A531ContagemResultado_StatusUltCnt, A456ContagemResultado_Codigo, A602ContagemResultado_OSVinculada, A499ContagemResultado_ContratadaPessoaCod, AV5WWPContext, AV9Amarelo, A53Contratada_AreaTrabalhoDes, A501ContagemResultado_OsFsOsFm, A801ContagemResultado_ServicoSigla, A1313ContagemResultado_ResponsavelPessNome, AV7Vermelho, AV39Nome, AV32ContagemResultado_Servico, AV33ContagemResultado_Demanda) ;
         }
         return 0 ;
      }

      protected int subgridvermelho_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRIDVERMELHO_nFirstRecordOnPage = (long)(subGridvermelho_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRIDVERMELHO_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRIDVERMELHO_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDVERMELHO_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridvermelho_refresh( subGridvermelho_Rows, A472ContagemResultado_DataEntrega, A484ContagemResultado_StatusDmn, A490ContagemResultado_ContratadaCod, AV10ServerDate, AV6Contratadas, AV36ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, AV21Responsaveis, A531ContagemResultado_StatusUltCnt, A456ContagemResultado_Codigo, A602ContagemResultado_OSVinculada, A499ContagemResultado_ContratadaPessoaCod, AV5WWPContext, AV9Amarelo, A53Contratada_AreaTrabalhoDes, A501ContagemResultado_OsFsOsFm, A801ContagemResultado_ServicoSigla, A1313ContagemResultado_ResponsavelPessNome, AV7Vermelho, AV39Nome, AV32ContagemResultado_Servico, AV33ContagemResultado_Demanda) ;
         }
         return (int)(0) ;
      }

      protected void STRUPL40( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavNome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNome_Enabled), 5, 0)));
         edtavQuantidade_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQuantidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQuantidade_Enabled), 5, 0)));
         edtavAtraso_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAtraso_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAtraso_Enabled), 5, 0)));
         edtavGlosa_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGlosa_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGlosa_Enabled), 5, 0)));
         edtavCodigo1_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCodigo1_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCodigo1_Enabled), 5, 0)));
         edtavVcontagemresultado_dataentrega_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavVcontagemresultado_dataentrega_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVcontagemresultado_dataentrega_Enabled), 5, 0)));
         edtavVareatrabalho_descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavVareatrabalho_descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVareatrabalho_descricao_Enabled), 5, 0)));
         edtavVcontagemresultado_osfsosfm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavVcontagemresultado_osfsosfm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVcontagemresultado_osfsosfm_Enabled), 5, 0)));
         edtavVservico_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavVservico_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVservico_Enabled), 5, 0)));
         edtavVresponsavelnome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavVresponsavelnome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVresponsavelnome_Enabled), 5, 0)));
         edtavVatraso_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavVatraso_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVatraso_Enabled), 5, 0)));
         edtavVglosa_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavVglosa_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVglosa_Enabled), 5, 0)));
         edtavCodigo2_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCodigo2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCodigo2_Enabled), 5, 0)));
         edtavLareatrabalho_descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLareatrabalho_descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLareatrabalho_descricao_Enabled), 5, 0)));
         edtavLcontagemresultado_osfsosfm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLcontagemresultado_osfsosfm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLcontagemresultado_osfsosfm_Enabled), 5, 0)));
         edtavLservico_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLservico_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLservico_Enabled), 5, 0)));
         edtavLresponsavelnome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLresponsavelnome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLresponsavelnome_Enabled), 5, 0)));
         GXVvCONTAGEMRESULTADO_SERVICO_htmlL42( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E13L42 */
         E13L42 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV33ContagemResultado_Demanda = StringUtil.Upper( cgiGet( edtavContagemresultado_demanda_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContagemResultado_Demanda", AV33ContagemResultado_Demanda);
            dynavContagemresultado_servico.Name = dynavContagemresultado_servico_Internalname;
            dynavContagemresultado_servico.CurrentValue = cgiGet( dynavContagemresultado_servico_Internalname);
            AV32ContagemResultado_Servico = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_servico_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ContagemResultado_Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32ContagemResultado_Servico), 6, 0)));
            cmbavContagemresultado_responsavel.Name = cmbavContagemresultado_responsavel_Internalname;
            cmbavContagemresultado_responsavel.CurrentValue = cgiGet( cmbavContagemresultado_responsavel_Internalname);
            AV36ContagemResultado_Responsavel = (int)(NumberUtil.Val( cgiGet( cmbavContagemresultado_responsavel_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ContagemResultado_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36ContagemResultado_Responsavel), 6, 0)));
            /* Read saved values. */
            nRC_GXsfl_39 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_39"), ",", "."));
            nRC_GXsfl_53 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_53"), ",", "."));
            nRC_GXsfl_71 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_71"), ",", "."));
            AV22Codigo = (int)(context.localUtil.CToN( cgiGet( "vCODIGO"), ",", "."));
            GRIDRESUMO_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRIDRESUMO_nFirstRecordOnPage"), ",", "."));
            GRIDVERMELHO_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRIDVERMELHO_nFirstRecordOnPage"), ",", "."));
            GRIDLARANJA_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRIDLARANJA_nFirstRecordOnPage"), ",", "."));
            GRIDRESUMO_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRIDRESUMO_nEOF"), ",", "."));
            GRIDVERMELHO_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRIDVERMELHO_nEOF"), ",", "."));
            GRIDLARANJA_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRIDLARANJA_nEOF"), ",", "."));
            subGridresumo_Rows = (int)(context.localUtil.CToN( cgiGet( "GRIDRESUMO_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRIDRESUMO_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridresumo_Rows), 6, 0, ".", "")));
            subGridvermelho_Rows = (int)(context.localUtil.CToN( cgiGet( "GRIDVERMELHO_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRIDVERMELHO_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridvermelho_Rows), 6, 0, ".", "")));
            subGridlaranja_Rows = (int)(context.localUtil.CToN( cgiGet( "GRIDLARANJA_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRIDLARANJA_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridlaranja_Rows), 6, 0, ".", "")));
            Dvpanel_pnlresumo_Width = cgiGet( "DVPANEL_PNLRESUMO_Width");
            Dvpanel_pnlresumo_Cls = cgiGet( "DVPANEL_PNLRESUMO_Cls");
            Dvpanel_pnlresumo_Title = cgiGet( "DVPANEL_PNLRESUMO_Title");
            Dvpanel_pnlresumo_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_PNLRESUMO_Collapsible"));
            Dvpanel_pnlresumo_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_PNLRESUMO_Collapsed"));
            Dvpanel_pnlresumo_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_PNLRESUMO_Autowidth"));
            Dvpanel_pnlresumo_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_PNLRESUMO_Autoheight"));
            Dvpanel_pnlresumo_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_PNLRESUMO_Showcollapseicon"));
            Dvpanel_pnlresumo_Iconposition = cgiGet( "DVPANEL_PNLRESUMO_Iconposition");
            Dvpanel_pnlresumo_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_PNLRESUMO_Autoscroll"));
            Dvpanel_pnlvermelho_Width = cgiGet( "DVPANEL_PNLVERMELHO_Width");
            Dvpanel_pnlvermelho_Cls = cgiGet( "DVPANEL_PNLVERMELHO_Cls");
            Dvpanel_pnlvermelho_Title = cgiGet( "DVPANEL_PNLVERMELHO_Title");
            Dvpanel_pnlvermelho_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_PNLVERMELHO_Collapsible"));
            Dvpanel_pnlvermelho_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_PNLVERMELHO_Collapsed"));
            Dvpanel_pnlvermelho_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_PNLVERMELHO_Autowidth"));
            Dvpanel_pnlvermelho_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_PNLVERMELHO_Autoheight"));
            Dvpanel_pnlvermelho_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_PNLVERMELHO_Showcollapseicon"));
            Dvpanel_pnlvermelho_Iconposition = cgiGet( "DVPANEL_PNLVERMELHO_Iconposition");
            Dvpanel_pnlvermelho_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_PNLVERMELHO_Autoscroll"));
            Dvpanel_pnllaranja_Width = cgiGet( "DVPANEL_PNLLARANJA_Width");
            Dvpanel_pnllaranja_Cls = cgiGet( "DVPANEL_PNLLARANJA_Cls");
            Dvpanel_pnllaranja_Title = cgiGet( "DVPANEL_PNLLARANJA_Title");
            Dvpanel_pnllaranja_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_PNLLARANJA_Collapsible"));
            Dvpanel_pnllaranja_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_PNLLARANJA_Collapsed"));
            Dvpanel_pnllaranja_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_PNLLARANJA_Autowidth"));
            Dvpanel_pnllaranja_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_PNLLARANJA_Autoheight"));
            Dvpanel_pnllaranja_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_PNLLARANJA_Showcollapseicon"));
            Dvpanel_pnllaranja_Iconposition = cgiGet( "DVPANEL_PNLLARANJA_Iconposition");
            Dvpanel_pnllaranja_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_PNLLARANJA_Autoscroll"));
            Dvpanel_pnlamarelo_Width = cgiGet( "DVPANEL_PNLAMARELO_Width");
            Dvpanel_pnlamarelo_Cls = cgiGet( "DVPANEL_PNLAMARELO_Cls");
            Dvpanel_pnlamarelo_Title = cgiGet( "DVPANEL_PNLAMARELO_Title");
            Dvpanel_pnlamarelo_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_PNLAMARELO_Collapsible"));
            Dvpanel_pnlamarelo_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_PNLAMARELO_Collapsed"));
            Dvpanel_pnlamarelo_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_PNLAMARELO_Autowidth"));
            Dvpanel_pnlamarelo_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_PNLAMARELO_Autoheight"));
            Dvpanel_pnlamarelo_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_PNLAMARELO_Showcollapseicon"));
            Dvpanel_pnlamarelo_Iconposition = cgiGet( "DVPANEL_PNLAMARELO_Iconposition");
            Dvpanel_pnlamarelo_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_PNLAMARELO_Autoscroll"));
            Confirmpanel_Width = cgiGet( "CONFIRMPANEL_Width");
            Confirmpanel_Height = cgiGet( "CONFIRMPANEL_Height");
            Confirmpanel_Title = cgiGet( "CONFIRMPANEL_Title");
            Confirmpanel_Icon = cgiGet( "CONFIRMPANEL_Icon");
            Confirmpanel_Confirmtext = cgiGet( "CONFIRMPANEL_Confirmtext");
            Confirmpanel_Buttonyestext = cgiGet( "CONFIRMPANEL_Buttonyestext");
            Confirmpanel_Buttonnotext = cgiGet( "CONFIRMPANEL_Buttonnotext");
            Confirmpanel_Buttoncanceltext = cgiGet( "CONFIRMPANEL_Buttoncanceltext");
            Confirmpanel_Confirmtype = cgiGet( "CONFIRMPANEL_Confirmtype");
            Confirmpanel_Draggeable = StringUtil.StrToBool( cgiGet( "CONFIRMPANEL_Draggeable"));
            Innewwindow_Target = cgiGet( "INNEWWINDOW_Target");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            /* Check if conditions changed and reset current page numbers */
            /* Check if conditions changed and reset current page numbers */
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E13L42 */
         E13L42 ();
         if (returnInSub) return;
      }

      protected void E13L42( )
      {
         /* Start Routine */
         subGridlaranja_Rows = 0;
         GxWebStd.gx_hidden_field( context, "GRIDLARANJA_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridlaranja_Rows), 6, 0, ".", "")));
         subGridvermelho_Rows = 0;
         GxWebStd.gx_hidden_field( context, "GRIDVERMELHO_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridvermelho_Rows), 6, 0, ".", "")));
         subGridresumo_Rows = 0;
         GxWebStd.gx_hidden_field( context, "GRIDRESUMO_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridresumo_Rows), 6, 0, ".", "")));
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV5WWPContext) ;
         subgridlaranja_gotopage( 1) ;
         subgridvermelho_gotopage( 1) ;
         subgridresumo_gotopage( 1) ;
         AV10ServerDate = DateTimeUtil.ServerDate( context, "DEFAULT");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10ServerDate", context.localUtil.Format(AV10ServerDate, "99/99/99"));
         AV31PodeAtribuirDmn = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31PodeAtribuirDmn", AV31PodeAtribuirDmn);
         /* Using cursor H00L43 */
         pr_default.execute(1, new Object[] {AV5WWPContext.gxTpr_Userid});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A1078ContratoGestor_ContratoCod = H00L43_A1078ContratoGestor_ContratoCod[0];
            A1079ContratoGestor_UsuarioCod = H00L43_A1079ContratoGestor_UsuarioCod[0];
            A1136ContratoGestor_ContratadaCod = H00L43_A1136ContratoGestor_ContratadaCod[0];
            n1136ContratoGestor_ContratadaCod = H00L43_n1136ContratoGestor_ContratadaCod[0];
            A1136ContratoGestor_ContratadaCod = H00L43_A1136ContratoGestor_ContratadaCod[0];
            n1136ContratoGestor_ContratadaCod = H00L43_n1136ContratoGestor_ContratadaCod[0];
            AV6Contratadas.Add(A1136ContratoGestor_ContratadaCod, 0);
            pr_default.readNext(1);
         }
         pr_default.close(1);
         if ( AV6Contratadas.Count == 0 )
         {
            this.executeUsercontrolMethod("", false, "CONFIRMPANELContainer", "Confirm", "", new Object[] {});
         }
         else
         {
            /* Execute user subroutine: 'CARREGARESPONSAVEIS' */
            S112 ();
            if (returnInSub) return;
         }
      }

      private void E14L42( )
      {
         /* Gridresumo_Load Routine */
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A490ContagemResultado_ContratadaCod ,
                                              AV6Contratadas ,
                                              A890ContagemResultado_Responsavel ,
                                              AV21Responsaveis ,
                                              AV36ContagemResultado_Responsavel ,
                                              AV32ContagemResultado_Servico ,
                                              AV33ContagemResultado_Demanda ,
                                              A601ContagemResultado_Servico ,
                                              A457ContagemResultado_Demanda ,
                                              A493ContagemResultado_DemandaFM ,
                                              A472ContagemResultado_DataEntrega ,
                                              AV10ServerDate ,
                                              A484ContagemResultado_StatusDmn ,
                                              AV5WWPContext.gxTpr_Contratada_codigo ,
                                              A531ContagemResultado_StatusUltCnt ,
                                              A456ContagemResultado_Codigo ,
                                              A602ContagemResultado_OSVinculada ,
                                              A499ContagemResultado_ContratadaPessoaCod ,
                                              AV5WWPContext.gxTpr_Contratada_pessoacod ,
                                              A1227ContagemResultado_PrazoInicialDias ,
                                              A1237ContagemResultado_PrazoMaisDias },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.SHORT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT,
                                              TypeConstants.BOOLEAN
                                              }
         });
         lV33ContagemResultado_Demanda = StringUtil.Concat( StringUtil.RTrim( AV33ContagemResultado_Demanda), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContagemResultado_Demanda", AV33ContagemResultado_Demanda);
         lV33ContagemResultado_Demanda = StringUtil.Concat( StringUtil.RTrim( AV33ContagemResultado_Demanda), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContagemResultado_Demanda", AV33ContagemResultado_Demanda);
         /* Using cursor H00L45 */
         pr_default.execute(2, new Object[] {AV10ServerDate, AV32ContagemResultado_Servico, AV36ContagemResultado_Responsavel, lV33ContagemResultado_Demanda, lV33ContagemResultado_Demanda});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKL48 = false;
            A1312ContagemResultado_ResponsavelPessCod = H00L45_A1312ContagemResultado_ResponsavelPessCod[0];
            n1312ContagemResultado_ResponsavelPessCod = H00L45_n1312ContagemResultado_ResponsavelPessCod[0];
            A1553ContagemResultado_CntSrvCod = H00L45_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = H00L45_n1553ContagemResultado_CntSrvCod[0];
            A1313ContagemResultado_ResponsavelPessNome = H00L45_A1313ContagemResultado_ResponsavelPessNome[0];
            n1313ContagemResultado_ResponsavelPessNome = H00L45_n1313ContagemResultado_ResponsavelPessNome[0];
            A1237ContagemResultado_PrazoMaisDias = H00L45_A1237ContagemResultado_PrazoMaisDias[0];
            n1237ContagemResultado_PrazoMaisDias = H00L45_n1237ContagemResultado_PrazoMaisDias[0];
            A1227ContagemResultado_PrazoInicialDias = H00L45_A1227ContagemResultado_PrazoInicialDias[0];
            n1227ContagemResultado_PrazoInicialDias = H00L45_n1227ContagemResultado_PrazoInicialDias[0];
            A601ContagemResultado_Servico = H00L45_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00L45_n601ContagemResultado_Servico[0];
            A890ContagemResultado_Responsavel = H00L45_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = H00L45_n890ContagemResultado_Responsavel[0];
            A493ContagemResultado_DemandaFM = H00L45_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = H00L45_n493ContagemResultado_DemandaFM[0];
            A457ContagemResultado_Demanda = H00L45_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = H00L45_n457ContagemResultado_Demanda[0];
            A490ContagemResultado_ContratadaCod = H00L45_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00L45_n490ContagemResultado_ContratadaCod[0];
            A484ContagemResultado_StatusDmn = H00L45_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00L45_n484ContagemResultado_StatusDmn[0];
            A472ContagemResultado_DataEntrega = H00L45_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = H00L45_n472ContagemResultado_DataEntrega[0];
            A499ContagemResultado_ContratadaPessoaCod = H00L45_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = H00L45_n499ContagemResultado_ContratadaPessoaCod[0];
            A602ContagemResultado_OSVinculada = H00L45_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = H00L45_n602ContagemResultado_OSVinculada[0];
            A456ContagemResultado_Codigo = H00L45_A456ContagemResultado_Codigo[0];
            A531ContagemResultado_StatusUltCnt = H00L45_A531ContagemResultado_StatusUltCnt[0];
            n531ContagemResultado_StatusUltCnt = H00L45_n531ContagemResultado_StatusUltCnt[0];
            A601ContagemResultado_Servico = H00L45_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00L45_n601ContagemResultado_Servico[0];
            A1312ContagemResultado_ResponsavelPessCod = H00L45_A1312ContagemResultado_ResponsavelPessCod[0];
            n1312ContagemResultado_ResponsavelPessCod = H00L45_n1312ContagemResultado_ResponsavelPessCod[0];
            A1313ContagemResultado_ResponsavelPessNome = H00L45_A1313ContagemResultado_ResponsavelPessNome[0];
            n1313ContagemResultado_ResponsavelPessNome = H00L45_n1313ContagemResultado_ResponsavelPessNome[0];
            A499ContagemResultado_ContratadaPessoaCod = H00L45_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = H00L45_n499ContagemResultado_ContratadaPessoaCod[0];
            A531ContagemResultado_StatusUltCnt = H00L45_A531ContagemResultado_StatusUltCnt[0];
            n531ContagemResultado_StatusUltCnt = H00L45_n531ContagemResultado_StatusUltCnt[0];
            if ( ( AV5WWPContext.gxTpr_Contratada_codigo <= 0 ) || ( H00L45_n531ContagemResultado_StatusUltCnt[0] || ( A531ContagemResultado_StatusUltCnt != 7 ) || ( ( A531ContagemResultado_StatusUltCnt == 7 ) && ( new prc_qtdtrtmdvrg(context).executeUdp(  A456ContagemResultado_Codigo,  A602ContagemResultado_OSVinculada,  A499ContagemResultado_ContratadaPessoaCod,  AV5WWPContext.gxTpr_Contratada_pessoacod) == 0 ) ) ) )
            {
               AV45Quantidade = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQuantidade_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV45Quantidade), 3, 0)));
               AV43TotalGls = 0;
               AV46Atraso = (short)(DateTimeUtil.DDiff(AV10ServerDate,A472ContagemResultado_DataEntrega));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAtraso_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV46Atraso), 4, 0)));
               AV39Nome = A1313ContagemResultado_ResponsavelPessNome;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavNome_Internalname, AV39Nome);
               /* Execute user subroutine: 'REDUZNOME' */
               S125 ();
               if ( returnInSub )
               {
                  pr_default.close(2);
                  returnInSub = true;
                  if (true) return;
               }
               while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(H00L45_A1313ContagemResultado_ResponsavelPessNome[0], A1313ContagemResultado_ResponsavelPessNome) == 0 ) )
               {
                  BRKL48 = false;
                  A1312ContagemResultado_ResponsavelPessCod = H00L45_A1312ContagemResultado_ResponsavelPessCod[0];
                  n1312ContagemResultado_ResponsavelPessCod = H00L45_n1312ContagemResultado_ResponsavelPessCod[0];
                  A1553ContagemResultado_CntSrvCod = H00L45_A1553ContagemResultado_CntSrvCod[0];
                  n1553ContagemResultado_CntSrvCod = H00L45_n1553ContagemResultado_CntSrvCod[0];
                  A1237ContagemResultado_PrazoMaisDias = H00L45_A1237ContagemResultado_PrazoMaisDias[0];
                  n1237ContagemResultado_PrazoMaisDias = H00L45_n1237ContagemResultado_PrazoMaisDias[0];
                  A1227ContagemResultado_PrazoInicialDias = H00L45_A1227ContagemResultado_PrazoInicialDias[0];
                  n1227ContagemResultado_PrazoInicialDias = H00L45_n1227ContagemResultado_PrazoInicialDias[0];
                  A601ContagemResultado_Servico = H00L45_A601ContagemResultado_Servico[0];
                  n601ContagemResultado_Servico = H00L45_n601ContagemResultado_Servico[0];
                  A890ContagemResultado_Responsavel = H00L45_A890ContagemResultado_Responsavel[0];
                  n890ContagemResultado_Responsavel = H00L45_n890ContagemResultado_Responsavel[0];
                  A493ContagemResultado_DemandaFM = H00L45_A493ContagemResultado_DemandaFM[0];
                  n493ContagemResultado_DemandaFM = H00L45_n493ContagemResultado_DemandaFM[0];
                  A457ContagemResultado_Demanda = H00L45_A457ContagemResultado_Demanda[0];
                  n457ContagemResultado_Demanda = H00L45_n457ContagemResultado_Demanda[0];
                  A490ContagemResultado_ContratadaCod = H00L45_A490ContagemResultado_ContratadaCod[0];
                  n490ContagemResultado_ContratadaCod = H00L45_n490ContagemResultado_ContratadaCod[0];
                  A484ContagemResultado_StatusDmn = H00L45_A484ContagemResultado_StatusDmn[0];
                  n484ContagemResultado_StatusDmn = H00L45_n484ContagemResultado_StatusDmn[0];
                  A472ContagemResultado_DataEntrega = H00L45_A472ContagemResultado_DataEntrega[0];
                  n472ContagemResultado_DataEntrega = H00L45_n472ContagemResultado_DataEntrega[0];
                  A602ContagemResultado_OSVinculada = H00L45_A602ContagemResultado_OSVinculada[0];
                  n602ContagemResultado_OSVinculada = H00L45_n602ContagemResultado_OSVinculada[0];
                  A456ContagemResultado_Codigo = H00L45_A456ContagemResultado_Codigo[0];
                  A601ContagemResultado_Servico = H00L45_A601ContagemResultado_Servico[0];
                  n601ContagemResultado_Servico = H00L45_n601ContagemResultado_Servico[0];
                  A1312ContagemResultado_ResponsavelPessCod = H00L45_A1312ContagemResultado_ResponsavelPessCod[0];
                  n1312ContagemResultado_ResponsavelPessCod = H00L45_n1312ContagemResultado_ResponsavelPessCod[0];
                  if ( A1227ContagemResultado_PrazoInicialDias + A1237ContagemResultado_PrazoMaisDias > 0 )
                  {
                     if ( (AV6Contratadas.IndexOf(A490ContagemResultado_ContratadaCod)>0) )
                     {
                        AV45Quantidade = (short)(AV45Quantidade+1);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQuantidade_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV45Quantidade), 3, 0)));
                     }
                  }
                  BRKL48 = true;
                  pr_default.readNext(2);
               }
               AV44Glosa = AV43TotalGls;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavGlosa_Internalname, StringUtil.LTrim( StringUtil.Str( AV44Glosa, 18, 5)));
               /* Load Method */
               if ( wbStart != -1 )
               {
                  wbStart = 39;
               }
               if ( ( subGridresumo_Islastpage == 1 ) || ( subGridresumo_Rows == 0 ) || ( ( GRIDRESUMO_nCurrentRecord >= GRIDRESUMO_nFirstRecordOnPage ) && ( GRIDRESUMO_nCurrentRecord < GRIDRESUMO_nFirstRecordOnPage + subGridresumo_Recordsperpage( ) ) ) )
               {
                  sendrow_392( ) ;
                  GRIDRESUMO_nEOF = 1;
                  GxWebStd.gx_hidden_field( context, "GRIDRESUMO_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDRESUMO_nEOF), 1, 0, ".", "")));
                  if ( ( subGridresumo_Islastpage == 1 ) && ( ((int)((GRIDRESUMO_nCurrentRecord) % (subGridresumo_Recordsperpage( )))) == 0 ) )
                  {
                     GRIDRESUMO_nFirstRecordOnPage = GRIDRESUMO_nCurrentRecord;
                  }
               }
               if ( GRIDRESUMO_nCurrentRecord >= GRIDRESUMO_nFirstRecordOnPage + subGridresumo_Recordsperpage( ) )
               {
                  GRIDRESUMO_nEOF = 0;
                  GxWebStd.gx_hidden_field( context, "GRIDRESUMO_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDRESUMO_nEOF), 1, 0, ".", "")));
               }
               GRIDRESUMO_nCurrentRecord = (long)(GRIDRESUMO_nCurrentRecord+1);
               if ( isFullAjaxMode( ) && ( nGXsfl_39_Refreshing == 0 ) )
               {
                  context.DoAjaxLoad(39, GridresumoRow);
               }
            }
            if ( ! BRKL48 )
            {
               BRKL48 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      protected void E20L42( )
      {
         /* 'DoAtribuir2' Routine */
         if ( AV53Selecionadas2.IndexOf(AV23Codigo2) == 0 )
         {
            AV53Selecionadas2.Add(AV23Codigo2, 0);
         }
         AV49WebSession.Set("Codigos", AV53Selecionadas2.ToXml(false, true, "Collection", ""));
         AV49WebSession.Set("Caller", "Gestao");
         context.PopUp(formatLink("wp_tramitacao.aspx") + "?" + UrlEncode("" +AV23Codigo2) + "," + UrlEncode(StringUtil.BoolToStr(AV31PodeAtribuirDmn)) + "," + UrlEncode(StringUtil.BoolToStr(false)) + "," + UrlEncode(StringUtil.RTrim(AV19LResponsavelNome)), new Object[] {"AV19LResponsavelNome"});
         if ( AV53Selecionadas2.Count > 1 )
         {
            context.DoAjaxRefresh();
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV53Selecionadas2", AV53Selecionadas2);
      }

      protected void E17L42( )
      {
         /* 'DoAtribuir1' Routine */
         if ( AV50Selecionadas1.IndexOf(AV25Codigo1) == 0 )
         {
            AV50Selecionadas1.Add(AV25Codigo1, 0);
         }
         AV49WebSession.Set("Codigos", AV50Selecionadas1.ToXml(false, true, "Collection", ""));
         AV49WebSession.Set("Caller", "Gestao");
         context.PopUp(formatLink("wp_tramitacao.aspx") + "?" + UrlEncode("" +AV25Codigo1) + "," + UrlEncode(StringUtil.BoolToStr(AV31PodeAtribuirDmn)) + "," + UrlEncode(StringUtil.BoolToStr(false)) + "," + UrlEncode(StringUtil.RTrim(AV20VResponsavelNome)), new Object[] {"AV20VResponsavelNome"});
         if ( AV50Selecionadas1.Count > 1 )
         {
            context.DoAjaxRefresh();
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV50Selecionadas1", AV50Selecionadas1);
      }

      protected void E12L42( )
      {
         /* 'DoLimparFiltros' Routine */
         AV33ContagemResultado_Demanda = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContagemResultado_Demanda", AV33ContagemResultado_Demanda);
         AV36ContagemResultado_Responsavel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ContagemResultado_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36ContagemResultado_Responsavel), 6, 0)));
         AV32ContagemResultado_Servico = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ContagemResultado_Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32ContagemResultado_Servico), 6, 0)));
         cmbavContagemresultado_responsavel.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV36ContagemResultado_Responsavel), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_responsavel_Internalname, "Values", cmbavContagemresultado_responsavel.ToJavascriptSource());
         dynavContagemresultado_servico.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV32ContagemResultado_Servico), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_servico_Internalname, "Values", dynavContagemresultado_servico.ToJavascriptSource());
      }

      protected void E11L42( )
      {
         /* 'DoAtualizar' Routine */
         context.DoAjaxRefresh();
      }

      protected void E15L42( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV7Vermelho = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Vermelho", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Vermelho), 8, 0)));
         AV8Laranja = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Laranja", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Laranja), 8, 0)));
         AV9Amarelo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Amarelo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Amarelo), 8, 0)));
         /* Execute user subroutine: 'TOTAIS' */
         S142 ();
         if (returnInSub) return;
      }

      protected void E18L42( )
      {
         /* Selected1_Click Routine */
         if ( AV26Selected1 )
         {
            AV50Selecionadas1.Add(AV25Codigo1, 0);
            AV51Count1 = (short)(AV51Count1+1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51Count1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51Count1), 4, 0)));
         }
         else
         {
            AV50Selecionadas1.RemoveItem(AV50Selecionadas1.IndexOf(AV25Codigo1));
            AV51Count1 = (short)(AV51Count1-1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51Count1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51Count1), 4, 0)));
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV50Selecionadas1", AV50Selecionadas1);
      }

      protected void E21L42( )
      {
         /* Selected2_Click Routine */
         if ( AV24Selected2 )
         {
            AV53Selecionadas2.Add(AV23Codigo2, 0);
            AV52Count2 = (short)(AV52Count2+1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52Count2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52Count2), 4, 0)));
         }
         else
         {
            AV53Selecionadas2.RemoveItem(AV53Selecionadas2.IndexOf(AV23Codigo2));
            AV52Count2 = (short)(AV52Count2-1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52Count2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52Count2), 4, 0)));
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV53Selecionadas2", AV53Selecionadas2);
      }

      protected void S142( )
      {
         /* 'TOTAIS' Routine */
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              A490ContagemResultado_ContratadaCod ,
                                              AV6Contratadas ,
                                              A890ContagemResultado_Responsavel ,
                                              AV21Responsaveis ,
                                              AV36ContagemResultado_Responsavel ,
                                              A472ContagemResultado_DataEntrega ,
                                              AV10ServerDate ,
                                              A484ContagemResultado_StatusDmn ,
                                              AV5WWPContext.gxTpr_Contratada_codigo ,
                                              A531ContagemResultado_StatusUltCnt ,
                                              A456ContagemResultado_Codigo ,
                                              A602ContagemResultado_OSVinculada ,
                                              A499ContagemResultado_ContratadaPessoaCod ,
                                              AV5WWPContext.gxTpr_Contratada_pessoacod },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         /* Using cursor H00L47 */
         pr_default.execute(3, new Object[] {AV10ServerDate, AV10ServerDate, AV10ServerDate});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A499ContagemResultado_ContratadaPessoaCod = H00L47_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = H00L47_n499ContagemResultado_ContratadaPessoaCod[0];
            A602ContagemResultado_OSVinculada = H00L47_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = H00L47_n602ContagemResultado_OSVinculada[0];
            A456ContagemResultado_Codigo = H00L47_A456ContagemResultado_Codigo[0];
            A890ContagemResultado_Responsavel = H00L47_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = H00L47_n890ContagemResultado_Responsavel[0];
            A490ContagemResultado_ContratadaCod = H00L47_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00L47_n490ContagemResultado_ContratadaCod[0];
            A484ContagemResultado_StatusDmn = H00L47_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00L47_n484ContagemResultado_StatusDmn[0];
            A472ContagemResultado_DataEntrega = H00L47_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = H00L47_n472ContagemResultado_DataEntrega[0];
            A531ContagemResultado_StatusUltCnt = H00L47_A531ContagemResultado_StatusUltCnt[0];
            n531ContagemResultado_StatusUltCnt = H00L47_n531ContagemResultado_StatusUltCnt[0];
            A531ContagemResultado_StatusUltCnt = H00L47_A531ContagemResultado_StatusUltCnt[0];
            n531ContagemResultado_StatusUltCnt = H00L47_n531ContagemResultado_StatusUltCnt[0];
            A499ContagemResultado_ContratadaPessoaCod = H00L47_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = H00L47_n499ContagemResultado_ContratadaPessoaCod[0];
            if ( ( AV5WWPContext.gxTpr_Contratada_codigo <= 0 ) || ( H00L47_n531ContagemResultado_StatusUltCnt[0] || ( A531ContagemResultado_StatusUltCnt != 7 ) || ( ( A531ContagemResultado_StatusUltCnt == 7 ) && ( new prc_qtdtrtmdvrg(context).executeUdp(  A456ContagemResultado_Codigo,  A602ContagemResultado_OSVinculada,  A499ContagemResultado_ContratadaPessoaCod,  AV5WWPContext.gxTpr_Contratada_pessoacod) == 0 ) ) ) )
            {
               AV9Amarelo = (int)(AV9Amarelo+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Amarelo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Amarelo), 8, 0)));
            }
            pr_default.readNext(3);
         }
         pr_default.close(3);
         Dvpanel_pnlamarelo_Title = "Na semana ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV9Amarelo), 8, 0))+")";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvpanel_pnlamarelo_Internalname, "Title", Dvpanel_pnlamarelo_Title);
      }

      protected void S152( )
      {
         /* 'VERHISTORICO' Routine */
         Innewwindow_Target = formatLink("viewcontagemresultado.aspx") + "?" + UrlEncode("" +AV22Codigo) + "," + UrlEncode(StringUtil.RTrim("LogResponsavel"));
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Innewwindow_Internalname, "Target", Innewwindow_Target);
         this.executeUsercontrolMethod("", false, "INNEWWINDOWContainer", "OpenWindow", "", new Object[] {});
      }

      protected void S112( )
      {
         /* 'CARREGARESPONSAVEIS' Routine */
         pr_default.dynParam(4, new Object[]{ new Object[]{
                                              A490ContagemResultado_ContratadaCod ,
                                              AV6Contratadas ,
                                              A1229ContagemResultado_ContratadaDoResponsavel ,
                                              A484ContagemResultado_StatusDmn ,
                                              AV5WWPContext.gxTpr_Contratada_codigo ,
                                              A531ContagemResultado_StatusUltCnt ,
                                              A456ContagemResultado_Codigo ,
                                              A602ContagemResultado_OSVinculada ,
                                              A499ContagemResultado_ContratadaPessoaCod ,
                                              AV5WWPContext.gxTpr_Contratada_pessoacod ,
                                              A472ContagemResultado_DataEntrega ,
                                              AV10ServerDate },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE
                                              }
         });
         /* Using cursor H00L49 */
         pr_default.execute(4, new Object[] {AV10ServerDate});
         while ( (pr_default.getStatus(4) != 101) )
         {
            A499ContagemResultado_ContratadaPessoaCod = H00L49_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = H00L49_n499ContagemResultado_ContratadaPessoaCod[0];
            A602ContagemResultado_OSVinculada = H00L49_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = H00L49_n602ContagemResultado_OSVinculada[0];
            A456ContagemResultado_Codigo = H00L49_A456ContagemResultado_Codigo[0];
            A490ContagemResultado_ContratadaCod = H00L49_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00L49_n490ContagemResultado_ContratadaCod[0];
            A484ContagemResultado_StatusDmn = H00L49_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00L49_n484ContagemResultado_StatusDmn[0];
            A472ContagemResultado_DataEntrega = H00L49_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = H00L49_n472ContagemResultado_DataEntrega[0];
            A531ContagemResultado_StatusUltCnt = H00L49_A531ContagemResultado_StatusUltCnt[0];
            n531ContagemResultado_StatusUltCnt = H00L49_n531ContagemResultado_StatusUltCnt[0];
            A52Contratada_AreaTrabalhoCod = H00L49_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00L49_n52Contratada_AreaTrabalhoCod[0];
            A890ContagemResultado_Responsavel = H00L49_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = H00L49_n890ContagemResultado_Responsavel[0];
            A531ContagemResultado_StatusUltCnt = H00L49_A531ContagemResultado_StatusUltCnt[0];
            n531ContagemResultado_StatusUltCnt = H00L49_n531ContagemResultado_StatusUltCnt[0];
            A499ContagemResultado_ContratadaPessoaCod = H00L49_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = H00L49_n499ContagemResultado_ContratadaPessoaCod[0];
            A52Contratada_AreaTrabalhoCod = H00L49_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00L49_n52Contratada_AreaTrabalhoCod[0];
            if ( ( AV5WWPContext.gxTpr_Contratada_codigo <= 0 ) || ( H00L49_n531ContagemResultado_StatusUltCnt[0] || ( A531ContagemResultado_StatusUltCnt != 7 ) || ( ( A531ContagemResultado_StatusUltCnt == 7 ) && ( new prc_qtdtrtmdvrg(context).executeUdp(  A456ContagemResultado_Codigo,  A602ContagemResultado_OSVinculada,  A499ContagemResultado_ContratadaPessoaCod,  AV5WWPContext.gxTpr_Contratada_pessoacod) == 0 ) ) ) )
            {
               GXt_int1 = A1229ContagemResultado_ContratadaDoResponsavel;
               new prc_contratadadousuario(context ).execute( ref  A890ContagemResultado_Responsavel, ref  A52Contratada_AreaTrabalhoCod, out  GXt_int1) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A890ContagemResultado_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A890ContagemResultado_Responsavel), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
               A1229ContagemResultado_ContratadaDoResponsavel = GXt_int1;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1229ContagemResultado_ContratadaDoResponsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A1229ContagemResultado_ContratadaDoResponsavel), 6, 0)));
               if ( (AV6Contratadas.IndexOf(A1229ContagemResultado_ContratadaDoResponsavel)>0) )
               {
                  AV21Responsaveis.Add(A890ContagemResultado_Responsavel, 0);
               }
            }
            pr_default.readNext(4);
         }
         pr_default.close(4);
         pr_default.dynParam(5, new Object[]{ new Object[]{
                                              A1Usuario_Codigo ,
                                              AV21Responsaveis },
                                              new int[] {
                                              TypeConstants.INT
                                              }
         });
         /* Using cursor H00L410 */
         pr_default.execute(5);
         while ( (pr_default.getStatus(5) != 101) )
         {
            A57Usuario_PessoaCod = H00L410_A57Usuario_PessoaCod[0];
            A1Usuario_Codigo = H00L410_A1Usuario_Codigo[0];
            A58Usuario_PessoaNom = H00L410_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = H00L410_n58Usuario_PessoaNom[0];
            A2Usuario_Nome = H00L410_A2Usuario_Nome[0];
            n2Usuario_Nome = H00L410_n2Usuario_Nome[0];
            A58Usuario_PessoaNom = H00L410_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = H00L410_n58Usuario_PessoaNom[0];
            cmbavContagemresultado_responsavel.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)), A58Usuario_PessoaNom, 0);
            pr_default.readNext(5);
         }
         pr_default.close(5);
      }

      protected void S125( )
      {
         /* 'REDUZNOME' Routine */
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV39Nome)) )
         {
            AV39Nome = "Sem atribuir";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavNome_Internalname, AV39Nome);
         }
         else
         {
            AV39Nome = StringUtil.Trim( AV39Nome);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavNome_Internalname, AV39Nome);
            AV40p = (short)(StringUtil.StringSearch( AV39Nome, " ", 1)-1);
            AV47f = (short)(StringUtil.StringSearchRev( AV39Nome, " ", -1)+1);
            if ( StringUtil.StringSearch( AV39Nome, " ", AV40p+2) < StringUtil.Len( AV39Nome) )
            {
               AV39Nome = StringUtil.Substring( AV39Nome, 1, AV40p) + " " + StringUtil.Substring( AV39Nome, AV47f, StringUtil.Len( AV39Nome));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavNome_Internalname, AV39Nome);
            }
         }
      }

      protected void S137( )
      {
         /* 'GLOSA' Routine */
      }

      private void E19L44( )
      {
         /* Gridlaranja_Load Routine */
         AV27Atribuir2 = context.GetImagePath( "1f7b56a1-d360-4404-8eeb-b316a206d100", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAtribuir2_Internalname, AV27Atribuir2);
         AV57Atribuir2_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "1f7b56a1-d360-4404-8eeb-b316a206d100", "", context.GetTheme( )));
         edtavAtribuir2_Tooltiptext = "";
         AV28Historico2 = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavHistorico2_Internalname, AV28Historico2);
         AV58Historico2_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
         edtavHistorico2_Tooltiptext = "Ver hist�rico";
         pr_default.dynParam(6, new Object[]{ new Object[]{
                                              A490ContagemResultado_ContratadaCod ,
                                              AV6Contratadas ,
                                              A890ContagemResultado_Responsavel ,
                                              AV21Responsaveis ,
                                              AV36ContagemResultado_Responsavel ,
                                              AV32ContagemResultado_Servico ,
                                              AV33ContagemResultado_Demanda ,
                                              A601ContagemResultado_Servico ,
                                              A457ContagemResultado_Demanda ,
                                              A493ContagemResultado_DemandaFM ,
                                              A484ContagemResultado_StatusDmn ,
                                              AV5WWPContext.gxTpr_Contratada_codigo ,
                                              A531ContagemResultado_StatusUltCnt ,
                                              A456ContagemResultado_Codigo ,
                                              A602ContagemResultado_OSVinculada ,
                                              A499ContagemResultado_ContratadaPessoaCod ,
                                              AV5WWPContext.gxTpr_Contratada_pessoacod ,
                                              A1227ContagemResultado_PrazoInicialDias ,
                                              A1237ContagemResultado_PrazoMaisDias ,
                                              AV10ServerDate ,
                                              A472ContagemResultado_DataEntrega },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.BOOLEAN
                                              }
         });
         lV33ContagemResultado_Demanda = StringUtil.Concat( StringUtil.RTrim( AV33ContagemResultado_Demanda), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContagemResultado_Demanda", AV33ContagemResultado_Demanda);
         lV33ContagemResultado_Demanda = StringUtil.Concat( StringUtil.RTrim( AV33ContagemResultado_Demanda), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContagemResultado_Demanda", AV33ContagemResultado_Demanda);
         /* Using cursor H00L412 */
         pr_default.execute(6, new Object[] {AV10ServerDate, AV32ContagemResultado_Servico, AV36ContagemResultado_Responsavel, lV33ContagemResultado_Demanda, lV33ContagemResultado_Demanda});
         while ( (pr_default.getStatus(6) != 101) )
         {
            A1312ContagemResultado_ResponsavelPessCod = H00L412_A1312ContagemResultado_ResponsavelPessCod[0];
            n1312ContagemResultado_ResponsavelPessCod = H00L412_n1312ContagemResultado_ResponsavelPessCod[0];
            A52Contratada_AreaTrabalhoCod = H00L412_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00L412_n52Contratada_AreaTrabalhoCod[0];
            A1553ContagemResultado_CntSrvCod = H00L412_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = H00L412_n1553ContagemResultado_CntSrvCod[0];
            A472ContagemResultado_DataEntrega = H00L412_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = H00L412_n472ContagemResultado_DataEntrega[0];
            A1237ContagemResultado_PrazoMaisDias = H00L412_A1237ContagemResultado_PrazoMaisDias[0];
            n1237ContagemResultado_PrazoMaisDias = H00L412_n1237ContagemResultado_PrazoMaisDias[0];
            A1227ContagemResultado_PrazoInicialDias = H00L412_A1227ContagemResultado_PrazoInicialDias[0];
            n1227ContagemResultado_PrazoInicialDias = H00L412_n1227ContagemResultado_PrazoInicialDias[0];
            A601ContagemResultado_Servico = H00L412_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00L412_n601ContagemResultado_Servico[0];
            A499ContagemResultado_ContratadaPessoaCod = H00L412_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = H00L412_n499ContagemResultado_ContratadaPessoaCod[0];
            A602ContagemResultado_OSVinculada = H00L412_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = H00L412_n602ContagemResultado_OSVinculada[0];
            A456ContagemResultado_Codigo = H00L412_A456ContagemResultado_Codigo[0];
            A890ContagemResultado_Responsavel = H00L412_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = H00L412_n890ContagemResultado_Responsavel[0];
            A490ContagemResultado_ContratadaCod = H00L412_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00L412_n490ContagemResultado_ContratadaCod[0];
            A484ContagemResultado_StatusDmn = H00L412_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00L412_n484ContagemResultado_StatusDmn[0];
            A53Contratada_AreaTrabalhoDes = H00L412_A53Contratada_AreaTrabalhoDes[0];
            n53Contratada_AreaTrabalhoDes = H00L412_n53Contratada_AreaTrabalhoDes[0];
            A801ContagemResultado_ServicoSigla = H00L412_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = H00L412_n801ContagemResultado_ServicoSigla[0];
            A1313ContagemResultado_ResponsavelPessNome = H00L412_A1313ContagemResultado_ResponsavelPessNome[0];
            n1313ContagemResultado_ResponsavelPessNome = H00L412_n1313ContagemResultado_ResponsavelPessNome[0];
            A531ContagemResultado_StatusUltCnt = H00L412_A531ContagemResultado_StatusUltCnt[0];
            n531ContagemResultado_StatusUltCnt = H00L412_n531ContagemResultado_StatusUltCnt[0];
            A493ContagemResultado_DemandaFM = H00L412_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = H00L412_n493ContagemResultado_DemandaFM[0];
            A457ContagemResultado_Demanda = H00L412_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = H00L412_n457ContagemResultado_Demanda[0];
            A601ContagemResultado_Servico = H00L412_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00L412_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = H00L412_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = H00L412_n801ContagemResultado_ServicoSigla[0];
            A531ContagemResultado_StatusUltCnt = H00L412_A531ContagemResultado_StatusUltCnt[0];
            n531ContagemResultado_StatusUltCnt = H00L412_n531ContagemResultado_StatusUltCnt[0];
            A1312ContagemResultado_ResponsavelPessCod = H00L412_A1312ContagemResultado_ResponsavelPessCod[0];
            n1312ContagemResultado_ResponsavelPessCod = H00L412_n1312ContagemResultado_ResponsavelPessCod[0];
            A1313ContagemResultado_ResponsavelPessNome = H00L412_A1313ContagemResultado_ResponsavelPessNome[0];
            n1313ContagemResultado_ResponsavelPessNome = H00L412_n1313ContagemResultado_ResponsavelPessNome[0];
            A52Contratada_AreaTrabalhoCod = H00L412_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00L412_n52Contratada_AreaTrabalhoCod[0];
            A499ContagemResultado_ContratadaPessoaCod = H00L412_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = H00L412_n499ContagemResultado_ContratadaPessoaCod[0];
            A53Contratada_AreaTrabalhoDes = H00L412_A53Contratada_AreaTrabalhoDes[0];
            n53Contratada_AreaTrabalhoDes = H00L412_n53Contratada_AreaTrabalhoDes[0];
            if ( ( AV5WWPContext.gxTpr_Contratada_codigo <= 0 ) || ( H00L412_n531ContagemResultado_StatusUltCnt[0] || ( A531ContagemResultado_StatusUltCnt != 7 ) || ( ( A531ContagemResultado_StatusUltCnt == 7 ) && ( new prc_qtdtrtmdvrg(context).executeUdp(  A456ContagemResultado_Codigo,  A602ContagemResultado_OSVinculada,  A499ContagemResultado_ContratadaPessoaCod,  AV5WWPContext.gxTpr_Contratada_pessoacod) == 0 ) ) ) )
            {
               A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A501ContagemResultado_OsFsOsFm", A501ContagemResultado_OsFsOsFm);
               AV23Codigo2 = A456ContagemResultado_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCodigo2_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV23Codigo2), 6, 0)));
               AV11LAreaTrabalho_Descricao = A53Contratada_AreaTrabalhoDes;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavLareatrabalho_descricao_Internalname, AV11LAreaTrabalho_Descricao);
               AV12LContagemResultado_OsFsOsFm = A501ContagemResultado_OsFsOsFm;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavLcontagemresultado_osfsosfm_Internalname, AV12LContagemResultado_OsFsOsFm);
               AV34LServico = A801ContagemResultado_ServicoSigla;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavLservico_Internalname, AV34LServico);
               AV39Nome = A1313ContagemResultado_ResponsavelPessNome;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavNome_Internalname, AV39Nome);
               AV8Laranja = (int)(AV8Laranja+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Laranja", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Laranja), 8, 0)));
               /* Execute user subroutine: 'REDUZNOME' */
               S125 ();
               if ( returnInSub )
               {
                  pr_default.close(6);
                  returnInSub = true;
                  if (true) return;
               }
               AV19LResponsavelNome = AV39Nome;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavLresponsavelnome_Internalname, AV19LResponsavelNome);
               /* Load Method */
               if ( wbStart != -1 )
               {
                  wbStart = 71;
               }
               if ( ( subGridlaranja_Islastpage == 1 ) || ( subGridlaranja_Rows == 0 ) || ( ( GRIDLARANJA_nCurrentRecord >= GRIDLARANJA_nFirstRecordOnPage ) && ( GRIDLARANJA_nCurrentRecord < GRIDLARANJA_nFirstRecordOnPage + subGridlaranja_Recordsperpage( ) ) ) )
               {
                  sendrow_714( ) ;
                  GRIDLARANJA_nEOF = 1;
                  GxWebStd.gx_hidden_field( context, "GRIDLARANJA_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDLARANJA_nEOF), 1, 0, ".", "")));
                  if ( ( subGridlaranja_Islastpage == 1 ) && ( ((int)((GRIDLARANJA_nCurrentRecord) % (subGridlaranja_Recordsperpage( )))) == 0 ) )
                  {
                     GRIDLARANJA_nFirstRecordOnPage = GRIDLARANJA_nCurrentRecord;
                  }
               }
               if ( GRIDLARANJA_nCurrentRecord >= GRIDLARANJA_nFirstRecordOnPage + subGridlaranja_Recordsperpage( ) )
               {
                  GRIDLARANJA_nEOF = 0;
                  GxWebStd.gx_hidden_field( context, "GRIDLARANJA_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDLARANJA_nEOF), 1, 0, ".", "")));
               }
               GRIDLARANJA_nCurrentRecord = (long)(GRIDLARANJA_nCurrentRecord+1);
               if ( isFullAjaxMode( ) && ( nGXsfl_71_Refreshing == 0 ) )
               {
                  context.DoAjaxLoad(71, GridlaranjaRow);
               }
            }
            pr_default.readNext(6);
         }
         pr_default.close(6);
         Dvpanel_pnllaranja_Title = "Para hoje ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV8Laranja), 8, 0))+")";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvpanel_pnllaranja_Internalname, "Title", Dvpanel_pnllaranja_Title);
      }

      private void E16L46( )
      {
         /* Gridvermelho_Load Routine */
         AV29Atribuir1 = context.GetImagePath( "1f7b56a1-d360-4404-8eeb-b316a206d100", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAtribuir1_Internalname, AV29Atribuir1);
         AV60Atribuir1_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "1f7b56a1-d360-4404-8eeb-b316a206d100", "", context.GetTheme( )));
         edtavAtribuir1_Tooltiptext = "";
         AV30Historico1 = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavHistorico1_Internalname, AV30Historico1);
         AV61Historico1_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
         edtavHistorico1_Tooltiptext = "Ver hist�rico";
         pr_default.dynParam(7, new Object[]{ new Object[]{
                                              A490ContagemResultado_ContratadaCod ,
                                              AV6Contratadas ,
                                              A890ContagemResultado_Responsavel ,
                                              AV21Responsaveis ,
                                              AV36ContagemResultado_Responsavel ,
                                              AV32ContagemResultado_Servico ,
                                              AV33ContagemResultado_Demanda ,
                                              A601ContagemResultado_Servico ,
                                              A457ContagemResultado_Demanda ,
                                              A493ContagemResultado_DemandaFM ,
                                              A484ContagemResultado_StatusDmn ,
                                              AV5WWPContext.gxTpr_Contratada_codigo ,
                                              A531ContagemResultado_StatusUltCnt ,
                                              A456ContagemResultado_Codigo ,
                                              A602ContagemResultado_OSVinculada ,
                                              A499ContagemResultado_ContratadaPessoaCod ,
                                              AV5WWPContext.gxTpr_Contratada_pessoacod ,
                                              A1227ContagemResultado_PrazoInicialDias ,
                                              A1237ContagemResultado_PrazoMaisDias ,
                                              A472ContagemResultado_DataEntrega ,
                                              AV10ServerDate },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN,
                                              TypeConstants.DATE
                                              }
         });
         lV33ContagemResultado_Demanda = StringUtil.Concat( StringUtil.RTrim( AV33ContagemResultado_Demanda), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContagemResultado_Demanda", AV33ContagemResultado_Demanda);
         lV33ContagemResultado_Demanda = StringUtil.Concat( StringUtil.RTrim( AV33ContagemResultado_Demanda), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContagemResultado_Demanda", AV33ContagemResultado_Demanda);
         /* Using cursor H00L414 */
         pr_default.execute(7, new Object[] {AV10ServerDate, AV32ContagemResultado_Servico, AV36ContagemResultado_Responsavel, lV33ContagemResultado_Demanda, lV33ContagemResultado_Demanda});
         while ( (pr_default.getStatus(7) != 101) )
         {
            A1312ContagemResultado_ResponsavelPessCod = H00L414_A1312ContagemResultado_ResponsavelPessCod[0];
            n1312ContagemResultado_ResponsavelPessCod = H00L414_n1312ContagemResultado_ResponsavelPessCod[0];
            A52Contratada_AreaTrabalhoCod = H00L414_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00L414_n52Contratada_AreaTrabalhoCod[0];
            A1553ContagemResultado_CntSrvCod = H00L414_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = H00L414_n1553ContagemResultado_CntSrvCod[0];
            A1237ContagemResultado_PrazoMaisDias = H00L414_A1237ContagemResultado_PrazoMaisDias[0];
            n1237ContagemResultado_PrazoMaisDias = H00L414_n1237ContagemResultado_PrazoMaisDias[0];
            A1227ContagemResultado_PrazoInicialDias = H00L414_A1227ContagemResultado_PrazoInicialDias[0];
            n1227ContagemResultado_PrazoInicialDias = H00L414_n1227ContagemResultado_PrazoInicialDias[0];
            A601ContagemResultado_Servico = H00L414_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00L414_n601ContagemResultado_Servico[0];
            A499ContagemResultado_ContratadaPessoaCod = H00L414_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = H00L414_n499ContagemResultado_ContratadaPessoaCod[0];
            A602ContagemResultado_OSVinculada = H00L414_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = H00L414_n602ContagemResultado_OSVinculada[0];
            A456ContagemResultado_Codigo = H00L414_A456ContagemResultado_Codigo[0];
            A890ContagemResultado_Responsavel = H00L414_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = H00L414_n890ContagemResultado_Responsavel[0];
            A490ContagemResultado_ContratadaCod = H00L414_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00L414_n490ContagemResultado_ContratadaCod[0];
            A484ContagemResultado_StatusDmn = H00L414_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00L414_n484ContagemResultado_StatusDmn[0];
            A472ContagemResultado_DataEntrega = H00L414_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = H00L414_n472ContagemResultado_DataEntrega[0];
            A53Contratada_AreaTrabalhoDes = H00L414_A53Contratada_AreaTrabalhoDes[0];
            n53Contratada_AreaTrabalhoDes = H00L414_n53Contratada_AreaTrabalhoDes[0];
            A801ContagemResultado_ServicoSigla = H00L414_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = H00L414_n801ContagemResultado_ServicoSigla[0];
            A1313ContagemResultado_ResponsavelPessNome = H00L414_A1313ContagemResultado_ResponsavelPessNome[0];
            n1313ContagemResultado_ResponsavelPessNome = H00L414_n1313ContagemResultado_ResponsavelPessNome[0];
            A531ContagemResultado_StatusUltCnt = H00L414_A531ContagemResultado_StatusUltCnt[0];
            n531ContagemResultado_StatusUltCnt = H00L414_n531ContagemResultado_StatusUltCnt[0];
            A493ContagemResultado_DemandaFM = H00L414_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = H00L414_n493ContagemResultado_DemandaFM[0];
            A457ContagemResultado_Demanda = H00L414_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = H00L414_n457ContagemResultado_Demanda[0];
            A601ContagemResultado_Servico = H00L414_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00L414_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = H00L414_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = H00L414_n801ContagemResultado_ServicoSigla[0];
            A531ContagemResultado_StatusUltCnt = H00L414_A531ContagemResultado_StatusUltCnt[0];
            n531ContagemResultado_StatusUltCnt = H00L414_n531ContagemResultado_StatusUltCnt[0];
            A1312ContagemResultado_ResponsavelPessCod = H00L414_A1312ContagemResultado_ResponsavelPessCod[0];
            n1312ContagemResultado_ResponsavelPessCod = H00L414_n1312ContagemResultado_ResponsavelPessCod[0];
            A1313ContagemResultado_ResponsavelPessNome = H00L414_A1313ContagemResultado_ResponsavelPessNome[0];
            n1313ContagemResultado_ResponsavelPessNome = H00L414_n1313ContagemResultado_ResponsavelPessNome[0];
            A52Contratada_AreaTrabalhoCod = H00L414_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00L414_n52Contratada_AreaTrabalhoCod[0];
            A499ContagemResultado_ContratadaPessoaCod = H00L414_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = H00L414_n499ContagemResultado_ContratadaPessoaCod[0];
            A53Contratada_AreaTrabalhoDes = H00L414_A53Contratada_AreaTrabalhoDes[0];
            n53Contratada_AreaTrabalhoDes = H00L414_n53Contratada_AreaTrabalhoDes[0];
            if ( ( AV5WWPContext.gxTpr_Contratada_codigo <= 0 ) || ( H00L414_n531ContagemResultado_StatusUltCnt[0] || ( A531ContagemResultado_StatusUltCnt != 7 ) || ( ( A531ContagemResultado_StatusUltCnt == 7 ) && ( new prc_qtdtrtmdvrg(context).executeUdp(  A456ContagemResultado_Codigo,  A602ContagemResultado_OSVinculada,  A499ContagemResultado_ContratadaPessoaCod,  AV5WWPContext.gxTpr_Contratada_pessoacod) == 0 ) ) ) )
            {
               A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A501ContagemResultado_OsFsOsFm", A501ContagemResultado_OsFsOsFm);
               AV25Codigo1 = A456ContagemResultado_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCodigo1_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV25Codigo1), 6, 0)));
               AV14VAreaTrabalho_Descricao = A53Contratada_AreaTrabalhoDes;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavVareatrabalho_descricao_Internalname, AV14VAreaTrabalho_Descricao);
               AV15VContagemResultado_OsFsOsFm = A501ContagemResultado_OsFsOsFm;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavVcontagemresultado_osfsosfm_Internalname, AV15VContagemResultado_OsFsOsFm);
               AV16VContagemResultado_DataEntrega = A472ContagemResultado_DataEntrega;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavVcontagemresultado_dataentrega_Internalname, context.localUtil.Format(AV16VContagemResultado_DataEntrega, "99/99/99"));
               AV35VServico = A801ContagemResultado_ServicoSigla;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavVservico_Internalname, AV35VServico);
               AV39Nome = A1313ContagemResultado_ResponsavelPessNome;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavNome_Internalname, AV39Nome);
               AV37VAtraso = (short)(DateTimeUtil.DDiff(AV10ServerDate,A472ContagemResultado_DataEntrega));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavVatraso_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV37VAtraso), 4, 0)));
               /* Execute user subroutine: 'REDUZNOME' */
               S125 ();
               if ( returnInSub )
               {
                  pr_default.close(7);
                  returnInSub = true;
                  if (true) return;
               }
               AV20VResponsavelNome = AV39Nome;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavVresponsavelnome_Internalname, AV20VResponsavelNome);
               AV41Contratada = A490ContagemResultado_ContratadaCod;
               /* Execute user subroutine: 'GLOSA' */
               S137 ();
               if ( returnInSub )
               {
                  pr_default.close(7);
                  returnInSub = true;
                  if (true) return;
               }
               AV7Vermelho = (int)(AV7Vermelho+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Vermelho", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Vermelho), 8, 0)));
               /* Load Method */
               if ( wbStart != -1 )
               {
                  wbStart = 53;
               }
               if ( ( subGridvermelho_Islastpage == 1 ) || ( subGridvermelho_Rows == 0 ) || ( ( GRIDVERMELHO_nCurrentRecord >= GRIDVERMELHO_nFirstRecordOnPage ) && ( GRIDVERMELHO_nCurrentRecord < GRIDVERMELHO_nFirstRecordOnPage + subGridvermelho_Recordsperpage( ) ) ) )
               {
                  sendrow_536( ) ;
                  GRIDVERMELHO_nEOF = 1;
                  GxWebStd.gx_hidden_field( context, "GRIDVERMELHO_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDVERMELHO_nEOF), 1, 0, ".", "")));
                  if ( ( subGridvermelho_Islastpage == 1 ) && ( ((int)((GRIDVERMELHO_nCurrentRecord) % (subGridvermelho_Recordsperpage( )))) == 0 ) )
                  {
                     GRIDVERMELHO_nFirstRecordOnPage = GRIDVERMELHO_nCurrentRecord;
                  }
               }
               if ( GRIDVERMELHO_nCurrentRecord >= GRIDVERMELHO_nFirstRecordOnPage + subGridvermelho_Recordsperpage( ) )
               {
                  GRIDVERMELHO_nEOF = 0;
                  GxWebStd.gx_hidden_field( context, "GRIDVERMELHO_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDVERMELHO_nEOF), 1, 0, ".", "")));
               }
               GRIDVERMELHO_nCurrentRecord = (long)(GRIDVERMELHO_nCurrentRecord+1);
               if ( isFullAjaxMode( ) && ( nGXsfl_53_Refreshing == 0 ) )
               {
                  context.DoAjaxLoad(53, GridvermelhoRow);
               }
            }
            pr_default.readNext(7);
         }
         pr_default.close(7);
         Dvpanel_pnlvermelho_Title = "Atrasado ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV7Vermelho), 8, 0))+")";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvpanel_pnlvermelho_Internalname, "Title", Dvpanel_pnlvermelho_Title);
      }

      protected void wb_table1_2_L42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_L42( true) ;
         }
         else
         {
            wb_table2_8_L42( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_L42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_88_L42( true) ;
         }
         else
         {
            wb_table3_88_L42( false) ;
         }
         return  ;
      }

      protected void wb_table3_88_L42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_L42e( true) ;
         }
         else
         {
            wb_table1_2_L42e( false) ;
         }
      }

      protected void wb_table3_88_L42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUsrtable_Internalname, tblUsrtable_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"CONFIRMPANELContainer"+"\"></div>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"INNEWWINDOWContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_88_L42e( true) ;
         }
         else
         {
            wb_table3_88_L42e( false) ;
         }
      }

      protected void wb_table2_8_L42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 20, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_11_L42( true) ;
         }
         else
         {
            wb_table4_11_L42( false) ;
         }
         return  ;
      }

      protected void wb_table4_11_L42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_17_L42( true) ;
         }
         else
         {
            wb_table5_17_L42( false) ;
         }
         return  ;
      }

      protected void wb_table5_17_L42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_31_L42( true) ;
         }
         else
         {
            wb_table6_31_L42( false) ;
         }
         return  ;
      }

      protected void wb_table6_31_L42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_L42e( true) ;
         }
         else
         {
            wb_table2_8_L42e( false) ;
         }
      }

      protected void wb_table6_31_L42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable2_Internalname, tblUnnamedtable2_Internalname, "", "Table", 0, "", "", 10, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_PNLRESUMOContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_PNLRESUMOContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table7_36_L42( true) ;
         }
         else
         {
            wb_table7_36_L42( false) ;
         }
         return  ;
      }

      protected void wb_table7_36_L42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_PNLVERMELHOContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_PNLVERMELHOContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table8_50_L42( true) ;
         }
         else
         {
            wb_table8_50_L42( false) ;
         }
         return  ;
      }

      protected void wb_table8_50_L42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_PNLLARANJAContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_PNLLARANJAContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table9_68_L42( true) ;
         }
         else
         {
            wb_table9_68_L42( false) ;
         }
         return  ;
      }

      protected void wb_table9_68_L42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_PNLAMARELOContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_PNLAMARELOContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table10_83_L42( true) ;
         }
         else
         {
            wb_table10_83_L42( false) ;
         }
         return  ;
      }

      protected void wb_table10_83_L42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_31_L42e( true) ;
         }
         else
         {
            wb_table6_31_L42e( false) ;
         }
      }

      protected void wb_table10_83_L42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblPnlamarelo_Internalname, tblPnlamarelo_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_83_L42e( true) ;
         }
         else
         {
            wb_table10_83_L42e( false) ;
         }
      }

      protected void wb_table9_68_L42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblPnllaranja_Internalname, tblPnllaranja_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridlaranjaContainer.SetWrapped(nGXWrapped);
            if ( GridlaranjaContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridlaranjaContainer"+"DivS\" data-gxgridid=\"71\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGridlaranja_Internalname, subGridlaranja_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGridlaranja_Backcolorstyle == 0 )
               {
                  subGridlaranja_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGridlaranja_Class) > 0 )
                  {
                     subGridlaranja_Linesclass = subGridlaranja_Class+"Title";
                  }
               }
               else
               {
                  subGridlaranja_Titlebackstyle = 1;
                  if ( subGridlaranja_Backcolorstyle == 1 )
                  {
                     subGridlaranja_Titlebackcolor = subGridlaranja_Allbackcolor;
                     if ( StringUtil.Len( subGridlaranja_Class) > 0 )
                     {
                        subGridlaranja_Linesclass = subGridlaranja_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGridlaranja_Class) > 0 )
                     {
                        subGridlaranja_Linesclass = subGridlaranja_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridlaranja_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridlaranja_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "�rea") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridlaranja_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "OS FS/FM") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridlaranja_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Servi�o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridlaranja_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Respons�vel") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridlaranja_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(15), 4, 0))+"px"+" class=\""+subGridlaranja_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(23), 4, 0))+"px"+" class=\""+subGridlaranja_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridlaranjaContainer.AddObjectProperty("GridName", "Gridlaranja");
            }
            else
            {
               GridlaranjaContainer.AddObjectProperty("GridName", "Gridlaranja");
               GridlaranjaContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridlaranjaContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridlaranjaContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridlaranjaContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridlaranja_Backcolorstyle), 1, 0, ".", "")));
               GridlaranjaContainer.AddObjectProperty("CmpContext", "");
               GridlaranjaContainer.AddObjectProperty("InMasterPage", "false");
               GridlaranjaColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridlaranjaColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV23Codigo2), 6, 0, ".", "")));
               GridlaranjaColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCodigo2_Enabled), 5, 0, ".", "")));
               GridlaranjaContainer.AddColumnProperties(GridlaranjaColumn);
               GridlaranjaColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridlaranjaColumn.AddObjectProperty("Value", AV11LAreaTrabalho_Descricao);
               GridlaranjaColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavLareatrabalho_descricao_Enabled), 5, 0, ".", "")));
               GridlaranjaContainer.AddColumnProperties(GridlaranjaColumn);
               GridlaranjaColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridlaranjaColumn.AddObjectProperty("Value", AV12LContagemResultado_OsFsOsFm);
               GridlaranjaColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavLcontagemresultado_osfsosfm_Enabled), 5, 0, ".", "")));
               GridlaranjaContainer.AddColumnProperties(GridlaranjaColumn);
               GridlaranjaColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridlaranjaColumn.AddObjectProperty("Value", StringUtil.RTrim( AV34LServico));
               GridlaranjaColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavLservico_Enabled), 5, 0, ".", "")));
               GridlaranjaContainer.AddColumnProperties(GridlaranjaColumn);
               GridlaranjaColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridlaranjaColumn.AddObjectProperty("Value", StringUtil.RTrim( AV19LResponsavelNome));
               GridlaranjaColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavLresponsavelnome_Enabled), 5, 0, ".", "")));
               GridlaranjaContainer.AddColumnProperties(GridlaranjaColumn);
               GridlaranjaColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridlaranjaColumn.AddObjectProperty("Value", StringUtil.BoolToStr( AV24Selected2));
               GridlaranjaContainer.AddColumnProperties(GridlaranjaColumn);
               GridlaranjaColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridlaranjaColumn.AddObjectProperty("Value", context.convertURL( AV27Atribuir2));
               GridlaranjaColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavAtribuir2_Tooltiptext));
               GridlaranjaContainer.AddColumnProperties(GridlaranjaColumn);
               GridlaranjaColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridlaranjaColumn.AddObjectProperty("Value", context.convertURL( AV28Historico2));
               GridlaranjaColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavHistorico2_Tooltiptext));
               GridlaranjaContainer.AddColumnProperties(GridlaranjaColumn);
               GridlaranjaContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridlaranja_Allowselection), 1, 0, ".", "")));
               GridlaranjaContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridlaranja_Selectioncolor), 9, 0, ".", "")));
               GridlaranjaContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridlaranja_Allowhovering), 1, 0, ".", "")));
               GridlaranjaContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridlaranja_Hoveringcolor), 9, 0, ".", "")));
               GridlaranjaContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridlaranja_Allowcollapsing), 1, 0, ".", "")));
               GridlaranjaContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridlaranja_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 71 )
         {
            wbEnd = 0;
            nRC_GXsfl_71 = (short)(nGXsfl_71_idx-1);
            if ( GridlaranjaContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridlaranjaContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Gridlaranja", GridlaranjaContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridlaranjaContainerData", GridlaranjaContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridlaranjaContainerData"+"V", GridlaranjaContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridlaranjaContainerData"+"V"+"\" value='"+GridlaranjaContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_68_L42e( true) ;
         }
         else
         {
            wb_table9_68_L42e( false) ;
         }
      }

      protected void wb_table8_50_L42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblPnlvermelho_Internalname, tblPnlvermelho_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridvermelhoContainer.SetWrapped(nGXWrapped);
            if ( GridvermelhoContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridvermelhoContainer"+"DivS\" data-gxgridid=\"53\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGridvermelho_Internalname, subGridvermelho_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGridvermelho_Backcolorstyle == 0 )
               {
                  subGridvermelho_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGridvermelho_Class) > 0 )
                  {
                     subGridvermelho_Linesclass = subGridvermelho_Class+"Title";
                  }
               }
               else
               {
                  subGridvermelho_Titlebackstyle = 1;
                  if ( subGridvermelho_Backcolorstyle == 1 )
                  {
                     subGridvermelho_Titlebackcolor = subGridvermelho_Allbackcolor;
                     if ( StringUtil.Len( subGridvermelho_Class) > 0 )
                     {
                        subGridvermelho_Linesclass = subGridvermelho_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGridvermelho_Class) > 0 )
                     {
                        subGridvermelho_Linesclass = subGridvermelho_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridvermelho_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridvermelho_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Previs�o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridvermelho_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "�rea") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridvermelho_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "OS FS/FM") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridvermelho_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Servi�o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridvermelho_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Respons�vel") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridvermelho_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridvermelho_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Atraso") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridvermelho_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Glosa") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(15), 4, 0))+"px"+" class=\""+subGridvermelho_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(23), 4, 0))+"px"+" class=\""+subGridvermelho_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridvermelhoContainer.AddObjectProperty("GridName", "Gridvermelho");
            }
            else
            {
               GridvermelhoContainer.AddObjectProperty("GridName", "Gridvermelho");
               GridvermelhoContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridvermelhoContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridvermelhoContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridvermelhoContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridvermelho_Backcolorstyle), 1, 0, ".", "")));
               GridvermelhoContainer.AddObjectProperty("CmpContext", "");
               GridvermelhoContainer.AddObjectProperty("InMasterPage", "false");
               GridvermelhoColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridvermelhoColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV25Codigo1), 6, 0, ".", "")));
               GridvermelhoColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCodigo1_Enabled), 5, 0, ".", "")));
               GridvermelhoContainer.AddColumnProperties(GridvermelhoColumn);
               GridvermelhoColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridvermelhoColumn.AddObjectProperty("Value", context.localUtil.Format(AV16VContagemResultado_DataEntrega, "99/99/99"));
               GridvermelhoColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavVcontagemresultado_dataentrega_Enabled), 5, 0, ".", "")));
               GridvermelhoContainer.AddColumnProperties(GridvermelhoColumn);
               GridvermelhoColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridvermelhoColumn.AddObjectProperty("Value", AV14VAreaTrabalho_Descricao);
               GridvermelhoColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavVareatrabalho_descricao_Enabled), 5, 0, ".", "")));
               GridvermelhoContainer.AddColumnProperties(GridvermelhoColumn);
               GridvermelhoColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridvermelhoColumn.AddObjectProperty("Value", AV15VContagemResultado_OsFsOsFm);
               GridvermelhoColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavVcontagemresultado_osfsosfm_Enabled), 5, 0, ".", "")));
               GridvermelhoContainer.AddColumnProperties(GridvermelhoColumn);
               GridvermelhoColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridvermelhoColumn.AddObjectProperty("Value", StringUtil.RTrim( AV35VServico));
               GridvermelhoColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavVservico_Enabled), 5, 0, ".", "")));
               GridvermelhoContainer.AddColumnProperties(GridvermelhoColumn);
               GridvermelhoColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridvermelhoColumn.AddObjectProperty("Value", StringUtil.RTrim( AV20VResponsavelNome));
               GridvermelhoColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavVresponsavelnome_Enabled), 5, 0, ".", "")));
               GridvermelhoContainer.AddColumnProperties(GridvermelhoColumn);
               GridvermelhoColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridvermelhoColumn.AddObjectProperty("Value", StringUtil.BoolToStr( AV26Selected1));
               GridvermelhoContainer.AddColumnProperties(GridvermelhoColumn);
               GridvermelhoColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridvermelhoColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV37VAtraso), 4, 0, ".", "")));
               GridvermelhoColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavVatraso_Enabled), 5, 0, ".", "")));
               GridvermelhoContainer.AddColumnProperties(GridvermelhoColumn);
               GridvermelhoColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridvermelhoColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV38VGlosa, 18, 5, ".", "")));
               GridvermelhoColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavVglosa_Enabled), 5, 0, ".", "")));
               GridvermelhoContainer.AddColumnProperties(GridvermelhoColumn);
               GridvermelhoColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridvermelhoColumn.AddObjectProperty("Value", context.convertURL( AV29Atribuir1));
               GridvermelhoColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavAtribuir1_Tooltiptext));
               GridvermelhoContainer.AddColumnProperties(GridvermelhoColumn);
               GridvermelhoColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridvermelhoColumn.AddObjectProperty("Value", context.convertURL( AV30Historico1));
               GridvermelhoColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavHistorico1_Tooltiptext));
               GridvermelhoContainer.AddColumnProperties(GridvermelhoColumn);
               GridvermelhoContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridvermelho_Allowselection), 1, 0, ".", "")));
               GridvermelhoContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridvermelho_Selectioncolor), 9, 0, ".", "")));
               GridvermelhoContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridvermelho_Allowhovering), 1, 0, ".", "")));
               GridvermelhoContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridvermelho_Hoveringcolor), 9, 0, ".", "")));
               GridvermelhoContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridvermelho_Allowcollapsing), 1, 0, ".", "")));
               GridvermelhoContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridvermelho_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 53 )
         {
            wbEnd = 0;
            nRC_GXsfl_53 = (short)(nGXsfl_53_idx-1);
            if ( GridvermelhoContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridvermelhoContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Gridvermelho", GridvermelhoContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridvermelhoContainerData", GridvermelhoContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridvermelhoContainerData"+"V", GridvermelhoContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridvermelhoContainerData"+"V"+"\" value='"+GridvermelhoContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_50_L42e( true) ;
         }
         else
         {
            wb_table8_50_L42e( false) ;
         }
      }

      protected void wb_table7_36_L42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblPnlresumo_Internalname, tblPnlresumo_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridresumoContainer.SetWrapped(nGXWrapped);
            if ( GridresumoContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridresumoContainer"+"DivS\" data-gxgridid=\"39\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGridresumo_Internalname, subGridresumo_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGridresumo_Backcolorstyle == 0 )
               {
                  subGridresumo_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGridresumo_Class) > 0 )
                  {
                     subGridresumo_Linesclass = subGridresumo_Class+"Title";
                  }
               }
               else
               {
                  subGridresumo_Titlebackstyle = 1;
                  if ( subGridresumo_Backcolorstyle == 1 )
                  {
                     subGridresumo_Titlebackcolor = subGridresumo_Allbackcolor;
                     if ( StringUtil.Len( subGridresumo_Class) > 0 )
                     {
                        subGridresumo_Linesclass = subGridresumo_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGridresumo_Class) > 0 )
                     {
                        subGridresumo_Linesclass = subGridresumo_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridresumo_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Respons�vel") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridresumo_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Quantidade") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridresumo_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Atraso") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridresumo_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Glosa") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridresumoContainer.AddObjectProperty("GridName", "Gridresumo");
            }
            else
            {
               GridresumoContainer.AddObjectProperty("GridName", "Gridresumo");
               GridresumoContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridresumoContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridresumoContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridresumoContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridresumo_Backcolorstyle), 1, 0, ".", "")));
               GridresumoContainer.AddObjectProperty("CmpContext", "");
               GridresumoContainer.AddObjectProperty("InMasterPage", "false");
               GridresumoColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridresumoColumn.AddObjectProperty("Value", StringUtil.RTrim( AV39Nome));
               GridresumoColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavNome_Enabled), 5, 0, ".", "")));
               GridresumoContainer.AddColumnProperties(GridresumoColumn);
               GridresumoColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridresumoColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV45Quantidade), 3, 0, ".", "")));
               GridresumoColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavQuantidade_Enabled), 5, 0, ".", "")));
               GridresumoContainer.AddColumnProperties(GridresumoColumn);
               GridresumoColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridresumoColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV46Atraso), 4, 0, ".", "")));
               GridresumoColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavAtraso_Enabled), 5, 0, ".", "")));
               GridresumoContainer.AddColumnProperties(GridresumoColumn);
               GridresumoColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridresumoColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV44Glosa, 18, 5, ".", "")));
               GridresumoColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavGlosa_Enabled), 5, 0, ".", "")));
               GridresumoContainer.AddColumnProperties(GridresumoColumn);
               GridresumoContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridresumo_Allowselection), 1, 0, ".", "")));
               GridresumoContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridresumo_Selectioncolor), 9, 0, ".", "")));
               GridresumoContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridresumo_Allowhovering), 1, 0, ".", "")));
               GridresumoContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridresumo_Hoveringcolor), 9, 0, ".", "")));
               GridresumoContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridresumo_Allowcollapsing), 1, 0, ".", "")));
               GridresumoContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridresumo_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 39 )
         {
            wbEnd = 0;
            nRC_GXsfl_39 = (short)(nGXsfl_39_idx-1);
            if ( GridresumoContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridresumoContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Gridresumo", GridresumoContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridresumoContainerData", GridresumoContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridresumoContainerData"+"V", GridresumoContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridresumoContainerData"+"V"+"\" value='"+GridresumoContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_36_L42e( true) ;
         }
         else
         {
            wb_table7_36_L42e( false) ;
         }
      }

      protected void wb_table5_17_L42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 10, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgLimparfiltros_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar Filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgLimparfiltros_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOLIMPARFILTROS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_Gestao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'" + sGXsfl_39_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_demanda_Internalname, AV33ContagemResultado_Demanda, StringUtil.RTrim( context.localUtil.Format( AV33ContagemResultado_Demanda, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,22);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "No. OS", edtavContagemresultado_demanda_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_Gestao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_39_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_servico, dynavContagemresultado_servico_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV32ContagemResultado_Servico), 6, 0)), 1, dynavContagemresultado_servico_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_WP_Gestao.htm");
            dynavContagemresultado_servico.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV32ContagemResultado_Servico), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_servico_Internalname, "Values", (String)(dynavContagemresultado_servico.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'" + sGXsfl_39_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContagemresultado_responsavel, cmbavContagemresultado_responsavel_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV36ContagemResultado_Responsavel), 6, 0)), 1, cmbavContagemresultado_responsavel_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,26);\"", "", true, "HLP_WP_Gestao.htm");
            cmbavContagemresultado_responsavel.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV36ContagemResultado_Responsavel), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_responsavel_Internalname, "Values", (String)(cmbavContagemresultado_responsavel.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnatualizar_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(39), 2, 0)+","+"null"+");", "Procurar", bttBtnatualizar_Jsonclick, 5, "Procurar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOATUALIZAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_Gestao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_17_L42e( true) ;
         }
         else
         {
            wb_table5_17_L42e( false) ;
         }
      }

      protected void wb_table4_11_L42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 20, 20, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktitle_Internalname, "Gest�o de OS", "", "", lblTextblocktitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_Gestao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_11_L42e( true) ;
         }
         else
         {
            wb_table4_11_L42e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAL42( ) ;
         WSL42( ) ;
         WEL42( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20206216255322");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
            context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
            context.AddJavascriptSource("wp_gestao.js", "?20206216255322");
            context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
            context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
            context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
            context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
            context.AddJavascriptSource("DVelop/Shared/yahoo.js", "");
            context.AddJavascriptSource("DVelop/Shared/dom.js", "");
            context.AddJavascriptSource("DVelop/Shared/event.js", "");
            context.AddJavascriptSource("DVelop/Shared/dragdrop.js", "");
            context.AddJavascriptSource("DVelop/Shared/container.js", "");
            context.AddJavascriptSource("DVelop/ConfirmPanel/ConfirmPanelRender.js", "");
            context.AddJavascriptSource("Window/InNewWindowRender.js", "");
         }
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_392( )
      {
         edtavNome_Internalname = "vNOME_"+sGXsfl_39_idx;
         edtavQuantidade_Internalname = "vQUANTIDADE_"+sGXsfl_39_idx;
         edtavAtraso_Internalname = "vATRASO_"+sGXsfl_39_idx;
         edtavGlosa_Internalname = "vGLOSA_"+sGXsfl_39_idx;
      }

      protected void SubsflControlProps_fel_392( )
      {
         edtavNome_Internalname = "vNOME_"+sGXsfl_39_fel_idx;
         edtavQuantidade_Internalname = "vQUANTIDADE_"+sGXsfl_39_fel_idx;
         edtavAtraso_Internalname = "vATRASO_"+sGXsfl_39_fel_idx;
         edtavGlosa_Internalname = "vGLOSA_"+sGXsfl_39_fel_idx;
      }

      protected void sendrow_392( )
      {
         SubsflControlProps_392( ) ;
         WBL40( ) ;
         if ( ( subGridresumo_Rows * 1 == 0 ) || ( nGXsfl_39_idx <= subGridresumo_Recordsperpage( ) * 1 ) )
         {
            GridresumoRow = GXWebRow.GetNew(context,GridresumoContainer);
            if ( subGridresumo_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGridresumo_Backstyle = 0;
               if ( StringUtil.StrCmp(subGridresumo_Class, "") != 0 )
               {
                  subGridresumo_Linesclass = subGridresumo_Class+"Odd";
               }
            }
            else if ( subGridresumo_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGridresumo_Backstyle = 0;
               subGridresumo_Backcolor = subGridresumo_Allbackcolor;
               if ( StringUtil.StrCmp(subGridresumo_Class, "") != 0 )
               {
                  subGridresumo_Linesclass = subGridresumo_Class+"Uniform";
               }
            }
            else if ( subGridresumo_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGridresumo_Backstyle = 1;
               if ( StringUtil.StrCmp(subGridresumo_Class, "") != 0 )
               {
                  subGridresumo_Linesclass = subGridresumo_Class+"Odd";
               }
               subGridresumo_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGridresumo_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGridresumo_Backstyle = 1;
               if ( ((int)((nGXsfl_39_idx) % (2))) == 0 )
               {
                  subGridresumo_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGridresumo_Class, "") != 0 )
                  {
                     subGridresumo_Linesclass = subGridresumo_Class+"Even";
                  }
               }
               else
               {
                  subGridresumo_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGridresumo_Class, "") != 0 )
                  {
                     subGridresumo_Linesclass = subGridresumo_Class+"Odd";
                  }
               }
            }
            if ( GridresumoContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGridresumo_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_39_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridresumoContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridresumoRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavNome_Internalname,StringUtil.RTrim( AV39Nome),StringUtil.RTrim( context.localUtil.Format( AV39Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavNome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavNome_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)39,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridresumoContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridresumoRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavQuantidade_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV45Quantidade), 3, 0, ",", "")),((edtavQuantidade_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV45Quantidade), "ZZ9")) : context.localUtil.Format( (decimal)(AV45Quantidade), "ZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavQuantidade_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavQuantidade_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)3,(short)0,(short)0,(short)39,(short)1,(short)-1,(short)0,(bool)true,(String)"Quantidade",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridresumoContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridresumoRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavAtraso_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV46Atraso), 4, 0, ",", "")),((edtavAtraso_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV46Atraso), "ZZZ9")) : context.localUtil.Format( (decimal)(AV46Atraso), "ZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavAtraso_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavAtraso_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)39,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridresumoContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridresumoRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavGlosa_Internalname,StringUtil.LTrim( StringUtil.NToC( AV44Glosa, 18, 5, ",", "")),((edtavGlosa_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV44Glosa, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( AV44Glosa, "ZZZ,ZZZ,ZZZ,ZZ9.99")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavGlosa_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavGlosa_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)39,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
            GridresumoContainer.AddRow(GridresumoRow);
            nGXsfl_39_idx = (short)(((subGridresumo_Islastpage==1)&&(nGXsfl_39_idx+1>subGridresumo_Recordsperpage( )) ? 1 : nGXsfl_39_idx+1));
            sGXsfl_39_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_39_idx), 4, 0)), 4, "0");
            SubsflControlProps_392( ) ;
         }
         /* End function sendrow_392 */
      }

      protected void SubsflControlProps_536( )
      {
         edtavCodigo1_Internalname = "vCODIGO1_"+sGXsfl_53_idx;
         edtavVcontagemresultado_dataentrega_Internalname = "vVCONTAGEMRESULTADO_DATAENTREGA_"+sGXsfl_53_idx;
         edtavVareatrabalho_descricao_Internalname = "vVAREATRABALHO_DESCRICAO_"+sGXsfl_53_idx;
         edtavVcontagemresultado_osfsosfm_Internalname = "vVCONTAGEMRESULTADO_OSFSOSFM_"+sGXsfl_53_idx;
         edtavVservico_Internalname = "vVSERVICO_"+sGXsfl_53_idx;
         edtavVresponsavelnome_Internalname = "vVRESPONSAVELNOME_"+sGXsfl_53_idx;
         chkavSelected1_Internalname = "vSELECTED1_"+sGXsfl_53_idx;
         edtavVatraso_Internalname = "vVATRASO_"+sGXsfl_53_idx;
         edtavVglosa_Internalname = "vVGLOSA_"+sGXsfl_53_idx;
         edtavAtribuir1_Internalname = "vATRIBUIR1_"+sGXsfl_53_idx;
         edtavHistorico1_Internalname = "vHISTORICO1_"+sGXsfl_53_idx;
      }

      protected void SubsflControlProps_fel_536( )
      {
         edtavCodigo1_Internalname = "vCODIGO1_"+sGXsfl_53_fel_idx;
         edtavVcontagemresultado_dataentrega_Internalname = "vVCONTAGEMRESULTADO_DATAENTREGA_"+sGXsfl_53_fel_idx;
         edtavVareatrabalho_descricao_Internalname = "vVAREATRABALHO_DESCRICAO_"+sGXsfl_53_fel_idx;
         edtavVcontagemresultado_osfsosfm_Internalname = "vVCONTAGEMRESULTADO_OSFSOSFM_"+sGXsfl_53_fel_idx;
         edtavVservico_Internalname = "vVSERVICO_"+sGXsfl_53_fel_idx;
         edtavVresponsavelnome_Internalname = "vVRESPONSAVELNOME_"+sGXsfl_53_fel_idx;
         chkavSelected1_Internalname = "vSELECTED1_"+sGXsfl_53_fel_idx;
         edtavVatraso_Internalname = "vVATRASO_"+sGXsfl_53_fel_idx;
         edtavVglosa_Internalname = "vVGLOSA_"+sGXsfl_53_fel_idx;
         edtavAtribuir1_Internalname = "vATRIBUIR1_"+sGXsfl_53_fel_idx;
         edtavHistorico1_Internalname = "vHISTORICO1_"+sGXsfl_53_fel_idx;
      }

      protected void sendrow_536( )
      {
         SubsflControlProps_536( ) ;
         WBL40( ) ;
         if ( ( subGridvermelho_Rows * 1 == 0 ) || ( nGXsfl_53_idx <= subGridvermelho_Recordsperpage( ) * 1 ) )
         {
            GridvermelhoRow = GXWebRow.GetNew(context,GridvermelhoContainer);
            if ( subGridvermelho_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGridvermelho_Backstyle = 0;
               if ( StringUtil.StrCmp(subGridvermelho_Class, "") != 0 )
               {
                  subGridvermelho_Linesclass = subGridvermelho_Class+"Odd";
               }
            }
            else if ( subGridvermelho_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGridvermelho_Backstyle = 0;
               subGridvermelho_Backcolor = subGridvermelho_Allbackcolor;
               if ( StringUtil.StrCmp(subGridvermelho_Class, "") != 0 )
               {
                  subGridvermelho_Linesclass = subGridvermelho_Class+"Uniform";
               }
            }
            else if ( subGridvermelho_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGridvermelho_Backstyle = 1;
               if ( StringUtil.StrCmp(subGridvermelho_Class, "") != 0 )
               {
                  subGridvermelho_Linesclass = subGridvermelho_Class+"Odd";
               }
               subGridvermelho_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGridvermelho_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGridvermelho_Backstyle = 1;
               if ( ((int)((nGXsfl_53_idx) % (2))) == 0 )
               {
                  subGridvermelho_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGridvermelho_Class, "") != 0 )
                  {
                     subGridvermelho_Linesclass = subGridvermelho_Class+"Even";
                  }
               }
               else
               {
                  subGridvermelho_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGridvermelho_Class, "") != 0 )
                  {
                     subGridvermelho_Linesclass = subGridvermelho_Class+"Odd";
                  }
               }
            }
            if ( GridvermelhoContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGridvermelho_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_53_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridvermelhoContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavCodigo1_Enabled!=0)&&(edtavCodigo1_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 54,'',false,'"+sGXsfl_53_idx+"',53)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridvermelhoRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCodigo1_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV25Codigo1), 6, 0, ",", "")),((edtavCodigo1_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV25Codigo1), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV25Codigo1), "ZZZZZ9")),TempTags+((edtavCodigo1_Enabled!=0)&&(edtavCodigo1_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavCodigo1_Enabled!=0)&&(edtavCodigo1_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,54);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCodigo1_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavCodigo1_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)53,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridvermelhoContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavVcontagemresultado_dataentrega_Enabled!=0)&&(edtavVcontagemresultado_dataentrega_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 55,'',false,'"+sGXsfl_53_idx+"',53)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridvermelhoRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavVcontagemresultado_dataentrega_Internalname,context.localUtil.Format(AV16VContagemResultado_DataEntrega, "99/99/99"),context.localUtil.Format( AV16VContagemResultado_DataEntrega, "99/99/99"),TempTags+((edtavVcontagemresultado_dataentrega_Enabled!=0)&&(edtavVcontagemresultado_dataentrega_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavVcontagemresultado_dataentrega_Enabled!=0)&&(edtavVcontagemresultado_dataentrega_Visible!=0) ? " onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,55);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavVcontagemresultado_dataentrega_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavVcontagemresultado_dataentrega_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)53,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridvermelhoContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavVareatrabalho_descricao_Enabled!=0)&&(edtavVareatrabalho_descricao_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 56,'',false,'"+sGXsfl_53_idx+"',53)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridvermelhoRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavVareatrabalho_descricao_Internalname,(String)AV14VAreaTrabalho_Descricao,StringUtil.RTrim( context.localUtil.Format( AV14VAreaTrabalho_Descricao, "@!")),TempTags+((edtavVareatrabalho_descricao_Enabled!=0)&&(edtavVareatrabalho_descricao_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavVareatrabalho_descricao_Enabled!=0)&&(edtavVareatrabalho_descricao_Visible!=0) ? " onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,56);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavVareatrabalho_descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavVareatrabalho_descricao_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)53,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridvermelhoContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavVcontagemresultado_osfsosfm_Enabled!=0)&&(edtavVcontagemresultado_osfsosfm_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 57,'',false,'"+sGXsfl_53_idx+"',53)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridvermelhoRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavVcontagemresultado_osfsosfm_Internalname,(String)AV15VContagemResultado_OsFsOsFm,(String)"",TempTags+((edtavVcontagemresultado_osfsosfm_Enabled!=0)&&(edtavVcontagemresultado_osfsosfm_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavVcontagemresultado_osfsosfm_Enabled!=0)&&(edtavVcontagemresultado_osfsosfm_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,57);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavVcontagemresultado_osfsosfm_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavVcontagemresultado_osfsosfm_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)30,(short)0,(short)0,(short)53,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridvermelhoContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavVservico_Enabled!=0)&&(edtavVservico_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 58,'',false,'"+sGXsfl_53_idx+"',53)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridvermelhoRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavVservico_Internalname,StringUtil.RTrim( AV35VServico),StringUtil.RTrim( context.localUtil.Format( AV35VServico, "@!")),TempTags+((edtavVservico_Enabled!=0)&&(edtavVservico_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavVservico_Enabled!=0)&&(edtavVservico_Visible!=0) ? " onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,58);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavVservico_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavVservico_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)53,(short)1,(short)-1,(short)-1,(bool)true,(String)"Sigla",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridvermelhoContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavVresponsavelnome_Enabled!=0)&&(edtavVresponsavelnome_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 59,'',false,'"+sGXsfl_53_idx+"',53)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridvermelhoRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavVresponsavelnome_Internalname,StringUtil.RTrim( AV20VResponsavelNome),StringUtil.RTrim( context.localUtil.Format( AV20VResponsavelNome, "@!")),TempTags+((edtavVresponsavelnome_Enabled!=0)&&(edtavVresponsavelnome_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavVresponsavelnome_Enabled!=0)&&(edtavVresponsavelnome_Visible!=0) ? " onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,59);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavVresponsavelnome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavVresponsavelnome_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)53,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridvermelhoContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            TempTags = " " + ((chkavSelected1.Enabled!=0)&&(chkavSelected1.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 60,'',false,'"+sGXsfl_53_idx+"',53)\"" : " ");
            ClassString = "AttSemBordaCheckBox";
            StyleString = "";
            GridvermelhoRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkavSelected1_Internalname,StringUtil.BoolToStr( AV26Selected1),(String)"",(String)"",(short)-1,(short)1,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",TempTags+((chkavSelected1.Enabled!=0)&&(chkavSelected1.Visible!=0) ? " onclick=\"gx.fn.checkboxClick(60, this, 'true', 'false');gx.ajax.executeCliEvent('e18l42_client',this, event);gx.evt.onchange(this);\" " : " ")+((chkavSelected1.Enabled!=0)&&(chkavSelected1.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,60);\"" : " ")});
            /* Subfile cell */
            if ( GridvermelhoContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavVatraso_Enabled!=0)&&(edtavVatraso_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 61,'',false,'"+sGXsfl_53_idx+"',53)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridvermelhoRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavVatraso_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV37VAtraso), 4, 0, ",", "")),((edtavVatraso_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV37VAtraso), "ZZZ9")) : context.localUtil.Format( (decimal)(AV37VAtraso), "ZZZ9")),TempTags+((edtavVatraso_Enabled!=0)&&(edtavVatraso_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavVatraso_Enabled!=0)&&(edtavVatraso_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,61);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavVatraso_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavVatraso_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)53,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridvermelhoContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavVglosa_Enabled!=0)&&(edtavVglosa_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 62,'',false,'"+sGXsfl_53_idx+"',53)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridvermelhoRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavVglosa_Internalname,StringUtil.LTrim( StringUtil.NToC( AV38VGlosa, 18, 5, ",", "")),((edtavVglosa_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV38VGlosa, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( AV38VGlosa, "ZZZ,ZZZ,ZZZ,ZZ9.99")),TempTags+((edtavVglosa_Enabled!=0)&&(edtavVglosa_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavVglosa_Enabled!=0)&&(edtavVglosa_Visible!=0) ? " onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,62);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavVglosa_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavVglosa_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)53,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridvermelhoContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavAtribuir1_Enabled!=0)&&(edtavAtribuir1_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 63,'',false,'',53)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV29Atribuir1_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Atribuir1))&&String.IsNullOrEmpty(StringUtil.RTrim( AV60Atribuir1_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Atribuir1)));
            GridvermelhoRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavAtribuir1_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Atribuir1)) ? AV60Atribuir1_GXI : context.PathToRelativeUrl( AV29Atribuir1)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavAtribuir1_Tooltiptext,(short)0,(short)1,(short)15,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavAtribuir1_Jsonclick,"'"+""+"'"+",false,"+"'"+"E\\'DOATRIBUIR1\\'."+sGXsfl_53_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV29Atribuir1_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridvermelhoContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavHistorico1_Enabled!=0)&&(edtavHistorico1_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 64,'',false,'',53)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV30Historico1_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV30Historico1))&&String.IsNullOrEmpty(StringUtil.RTrim( AV61Historico1_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV30Historico1)));
            GridvermelhoRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavHistorico1_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV30Historico1)) ? AV61Historico1_GXI : context.PathToRelativeUrl( AV30Historico1)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavHistorico1_Tooltiptext,(short)0,(short)1,(short)23,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)7,(String)edtavHistorico1_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+"e22l46_client"+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV30Historico1_IsBlob,(bool)false});
            GXCCtl = "CONTAGEMRESULTADO_DEMANDA_" + sGXsfl_53_idx;
            GxWebStd.gx_hidden_field( context, GXCCtl, A457ContagemResultado_Demanda);
            GXCCtl = "CONTAGEMRESULTADO_DEMANDAFM_" + sGXsfl_53_idx;
            GxWebStd.gx_hidden_field( context, GXCCtl, A493ContagemResultado_DemandaFM);
            GXCCtl = "CONTAGEMRESULTADO_OSFSOSFM_" + sGXsfl_53_idx;
            GxWebStd.gx_hidden_field( context, GXCCtl, A501ContagemResultado_OsFsOsFm);
            GXCCtl = "CONTAGEMRESULTADO_STATUSULTCNT_" + sGXsfl_53_idx;
            GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(A531ContagemResultado_StatusUltCnt), 2, 0, ",", "")));
            GXCCtl = "CONTAGEMRESULTADO_CODIGO_" + sGXsfl_53_idx;
            GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")));
            GXCCtl = "CONTAGEMRESULTADO_OSVINCULADA_" + sGXsfl_53_idx;
            GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(A602ContagemResultado_OSVinculada), 6, 0, ",", "")));
            GXCCtl = "CONTAGEMRESULTADO_CONTRATADAPESSOACOD_" + sGXsfl_53_idx;
            GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(A499ContagemResultado_ContratadaPessoaCod), 6, 0, ",", "")));
            GridvermelhoContainer.AddRow(GridvermelhoRow);
            nGXsfl_53_idx = (short)(((subGridvermelho_Islastpage==1)&&(nGXsfl_53_idx+1>subGridvermelho_Recordsperpage( )) ? 1 : nGXsfl_53_idx+1));
            sGXsfl_53_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_53_idx), 4, 0)), 4, "0");
            SubsflControlProps_536( ) ;
         }
         /* End function sendrow_536 */
      }

      protected void SubsflControlProps_714( )
      {
         edtavCodigo2_Internalname = "vCODIGO2_"+sGXsfl_71_idx;
         edtavLareatrabalho_descricao_Internalname = "vLAREATRABALHO_DESCRICAO_"+sGXsfl_71_idx;
         edtavLcontagemresultado_osfsosfm_Internalname = "vLCONTAGEMRESULTADO_OSFSOSFM_"+sGXsfl_71_idx;
         edtavLservico_Internalname = "vLSERVICO_"+sGXsfl_71_idx;
         edtavLresponsavelnome_Internalname = "vLRESPONSAVELNOME_"+sGXsfl_71_idx;
         chkavSelected2_Internalname = "vSELECTED2_"+sGXsfl_71_idx;
         edtavAtribuir2_Internalname = "vATRIBUIR2_"+sGXsfl_71_idx;
         edtavHistorico2_Internalname = "vHISTORICO2_"+sGXsfl_71_idx;
      }

      protected void SubsflControlProps_fel_714( )
      {
         edtavCodigo2_Internalname = "vCODIGO2_"+sGXsfl_71_fel_idx;
         edtavLareatrabalho_descricao_Internalname = "vLAREATRABALHO_DESCRICAO_"+sGXsfl_71_fel_idx;
         edtavLcontagemresultado_osfsosfm_Internalname = "vLCONTAGEMRESULTADO_OSFSOSFM_"+sGXsfl_71_fel_idx;
         edtavLservico_Internalname = "vLSERVICO_"+sGXsfl_71_fel_idx;
         edtavLresponsavelnome_Internalname = "vLRESPONSAVELNOME_"+sGXsfl_71_fel_idx;
         chkavSelected2_Internalname = "vSELECTED2_"+sGXsfl_71_fel_idx;
         edtavAtribuir2_Internalname = "vATRIBUIR2_"+sGXsfl_71_fel_idx;
         edtavHistorico2_Internalname = "vHISTORICO2_"+sGXsfl_71_fel_idx;
      }

      protected void sendrow_714( )
      {
         SubsflControlProps_714( ) ;
         WBL40( ) ;
         if ( ( subGridlaranja_Rows * 1 == 0 ) || ( nGXsfl_71_idx <= subGridlaranja_Recordsperpage( ) * 1 ) )
         {
            GridlaranjaRow = GXWebRow.GetNew(context,GridlaranjaContainer);
            if ( subGridlaranja_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGridlaranja_Backstyle = 0;
               if ( StringUtil.StrCmp(subGridlaranja_Class, "") != 0 )
               {
                  subGridlaranja_Linesclass = subGridlaranja_Class+"Odd";
               }
            }
            else if ( subGridlaranja_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGridlaranja_Backstyle = 0;
               subGridlaranja_Backcolor = subGridlaranja_Allbackcolor;
               if ( StringUtil.StrCmp(subGridlaranja_Class, "") != 0 )
               {
                  subGridlaranja_Linesclass = subGridlaranja_Class+"Uniform";
               }
            }
            else if ( subGridlaranja_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGridlaranja_Backstyle = 1;
               if ( StringUtil.StrCmp(subGridlaranja_Class, "") != 0 )
               {
                  subGridlaranja_Linesclass = subGridlaranja_Class+"Odd";
               }
               subGridlaranja_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGridlaranja_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGridlaranja_Backstyle = 1;
               if ( ((int)((nGXsfl_71_idx) % (2))) == 0 )
               {
                  subGridlaranja_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGridlaranja_Class, "") != 0 )
                  {
                     subGridlaranja_Linesclass = subGridlaranja_Class+"Even";
                  }
               }
               else
               {
                  subGridlaranja_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGridlaranja_Class, "") != 0 )
                  {
                     subGridlaranja_Linesclass = subGridlaranja_Class+"Odd";
                  }
               }
            }
            if ( GridlaranjaContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGridlaranja_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_71_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridlaranjaContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavCodigo2_Enabled!=0)&&(edtavCodigo2_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 72,'',false,'"+sGXsfl_71_idx+"',71)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridlaranjaRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCodigo2_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV23Codigo2), 6, 0, ",", "")),((edtavCodigo2_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV23Codigo2), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV23Codigo2), "ZZZZZ9")),TempTags+((edtavCodigo2_Enabled!=0)&&(edtavCodigo2_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavCodigo2_Enabled!=0)&&(edtavCodigo2_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,72);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCodigo2_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavCodigo2_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)71,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridlaranjaContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavLareatrabalho_descricao_Enabled!=0)&&(edtavLareatrabalho_descricao_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 73,'',false,'"+sGXsfl_71_idx+"',71)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridlaranjaRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavLareatrabalho_descricao_Internalname,(String)AV11LAreaTrabalho_Descricao,StringUtil.RTrim( context.localUtil.Format( AV11LAreaTrabalho_Descricao, "@!")),TempTags+((edtavLareatrabalho_descricao_Enabled!=0)&&(edtavLareatrabalho_descricao_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavLareatrabalho_descricao_Enabled!=0)&&(edtavLareatrabalho_descricao_Visible!=0) ? " onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,73);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavLareatrabalho_descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavLareatrabalho_descricao_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)71,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridlaranjaContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavLcontagemresultado_osfsosfm_Enabled!=0)&&(edtavLcontagemresultado_osfsosfm_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 74,'',false,'"+sGXsfl_71_idx+"',71)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridlaranjaRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavLcontagemresultado_osfsosfm_Internalname,(String)AV12LContagemResultado_OsFsOsFm,(String)"",TempTags+((edtavLcontagemresultado_osfsosfm_Enabled!=0)&&(edtavLcontagemresultado_osfsosfm_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavLcontagemresultado_osfsosfm_Enabled!=0)&&(edtavLcontagemresultado_osfsosfm_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,74);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavLcontagemresultado_osfsosfm_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavLcontagemresultado_osfsosfm_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)30,(short)0,(short)0,(short)71,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridlaranjaContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavLservico_Enabled!=0)&&(edtavLservico_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 75,'',false,'"+sGXsfl_71_idx+"',71)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridlaranjaRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavLservico_Internalname,StringUtil.RTrim( AV34LServico),StringUtil.RTrim( context.localUtil.Format( AV34LServico, "@!")),TempTags+((edtavLservico_Enabled!=0)&&(edtavLservico_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavLservico_Enabled!=0)&&(edtavLservico_Visible!=0) ? " onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,75);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavLservico_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavLservico_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)71,(short)1,(short)-1,(short)-1,(bool)true,(String)"Sigla",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridlaranjaContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavLresponsavelnome_Enabled!=0)&&(edtavLresponsavelnome_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 76,'',false,'"+sGXsfl_71_idx+"',71)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridlaranjaRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavLresponsavelnome_Internalname,StringUtil.RTrim( AV19LResponsavelNome),StringUtil.RTrim( context.localUtil.Format( AV19LResponsavelNome, "@!")),TempTags+((edtavLresponsavelnome_Enabled!=0)&&(edtavLresponsavelnome_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavLresponsavelnome_Enabled!=0)&&(edtavLresponsavelnome_Visible!=0) ? " onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,76);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavLresponsavelnome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavLresponsavelnome_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)71,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridlaranjaContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            TempTags = " " + ((chkavSelected2.Enabled!=0)&&(chkavSelected2.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 77,'',false,'"+sGXsfl_71_idx+"',71)\"" : " ");
            ClassString = "AttSemBordaCheckBox";
            StyleString = "";
            GridlaranjaRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkavSelected2_Internalname,StringUtil.BoolToStr( AV24Selected2),(String)"",(String)"",(short)-1,(short)1,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",TempTags+((chkavSelected2.Enabled!=0)&&(chkavSelected2.Visible!=0) ? " onclick=\"gx.fn.checkboxClick(77, this, 'true', 'false');gx.ajax.executeCliEvent('e21l42_client',this, event);gx.evt.onchange(this);\" " : " ")+((chkavSelected2.Enabled!=0)&&(chkavSelected2.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,77);\"" : " ")});
            /* Subfile cell */
            if ( GridlaranjaContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavAtribuir2_Enabled!=0)&&(edtavAtribuir2_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 78,'',false,'',71)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV27Atribuir2_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV27Atribuir2))&&String.IsNullOrEmpty(StringUtil.RTrim( AV57Atribuir2_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV27Atribuir2)));
            GridlaranjaRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavAtribuir2_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV27Atribuir2)) ? AV57Atribuir2_GXI : context.PathToRelativeUrl( AV27Atribuir2)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavAtribuir2_Tooltiptext,(short)0,(short)1,(short)15,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavAtribuir2_Jsonclick,"'"+""+"'"+",false,"+"'"+"E\\'DOATRIBUIR2\\'."+sGXsfl_71_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV27Atribuir2_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridlaranjaContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavHistorico2_Enabled!=0)&&(edtavHistorico2_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 79,'',false,'',71)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV28Historico2_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Historico2))&&String.IsNullOrEmpty(StringUtil.RTrim( AV58Historico2_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Historico2)));
            GridlaranjaRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavHistorico2_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Historico2)) ? AV58Historico2_GXI : context.PathToRelativeUrl( AV28Historico2)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavHistorico2_Tooltiptext,(short)0,(short)1,(short)23,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)7,(String)edtavHistorico2_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+"e23l44_client"+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV28Historico2_IsBlob,(bool)false});
            GXCCtl = "CONTAGEMRESULTADO_DEMANDA_" + sGXsfl_71_idx;
            GxWebStd.gx_hidden_field( context, GXCCtl, A457ContagemResultado_Demanda);
            GXCCtl = "CONTAGEMRESULTADO_DEMANDAFM_" + sGXsfl_71_idx;
            GxWebStd.gx_hidden_field( context, GXCCtl, A493ContagemResultado_DemandaFM);
            GXCCtl = "CONTAGEMRESULTADO_OSFSOSFM_" + sGXsfl_71_idx;
            GxWebStd.gx_hidden_field( context, GXCCtl, A501ContagemResultado_OsFsOsFm);
            GXCCtl = "CONTAGEMRESULTADO_STATUSULTCNT_" + sGXsfl_71_idx;
            GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(A531ContagemResultado_StatusUltCnt), 2, 0, ",", "")));
            GXCCtl = "CONTAGEMRESULTADO_CODIGO_" + sGXsfl_71_idx;
            GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")));
            GXCCtl = "CONTAGEMRESULTADO_OSVINCULADA_" + sGXsfl_71_idx;
            GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(A602ContagemResultado_OSVinculada), 6, 0, ",", "")));
            GXCCtl = "CONTAGEMRESULTADO_CONTRATADAPESSOACOD_" + sGXsfl_71_idx;
            GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(A499ContagemResultado_ContratadaPessoaCod), 6, 0, ",", "")));
            GridlaranjaContainer.AddRow(GridlaranjaRow);
            nGXsfl_71_idx = (short)(((subGridlaranja_Islastpage==1)&&(nGXsfl_71_idx+1>subGridlaranja_Recordsperpage( )) ? 1 : nGXsfl_71_idx+1));
            sGXsfl_71_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_71_idx), 4, 0)), 4, "0");
            SubsflControlProps_714( ) ;
         }
         /* End function sendrow_714 */
      }

      protected void init_default_properties( )
      {
         lblTextblocktitle_Internalname = "TEXTBLOCKTITLE";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         imgLimparfiltros_Internalname = "LIMPARFILTROS";
         edtavContagemresultado_demanda_Internalname = "vCONTAGEMRESULTADO_DEMANDA";
         dynavContagemresultado_servico_Internalname = "vCONTAGEMRESULTADO_SERVICO";
         cmbavContagemresultado_responsavel_Internalname = "vCONTAGEMRESULTADO_RESPONSAVEL";
         bttBtnatualizar_Internalname = "BTNATUALIZAR";
         tblTablefilters_Internalname = "TABLEFILTERS";
         edtavNome_Internalname = "vNOME";
         edtavQuantidade_Internalname = "vQUANTIDADE";
         edtavAtraso_Internalname = "vATRASO";
         edtavGlosa_Internalname = "vGLOSA";
         tblPnlresumo_Internalname = "PNLRESUMO";
         Dvpanel_pnlresumo_Internalname = "DVPANEL_PNLRESUMO";
         edtavCodigo1_Internalname = "vCODIGO1";
         edtavVcontagemresultado_dataentrega_Internalname = "vVCONTAGEMRESULTADO_DATAENTREGA";
         edtavVareatrabalho_descricao_Internalname = "vVAREATRABALHO_DESCRICAO";
         edtavVcontagemresultado_osfsosfm_Internalname = "vVCONTAGEMRESULTADO_OSFSOSFM";
         edtavVservico_Internalname = "vVSERVICO";
         edtavVresponsavelnome_Internalname = "vVRESPONSAVELNOME";
         chkavSelected1_Internalname = "vSELECTED1";
         edtavVatraso_Internalname = "vVATRASO";
         edtavVglosa_Internalname = "vVGLOSA";
         edtavAtribuir1_Internalname = "vATRIBUIR1";
         edtavHistorico1_Internalname = "vHISTORICO1";
         tblPnlvermelho_Internalname = "PNLVERMELHO";
         Dvpanel_pnlvermelho_Internalname = "DVPANEL_PNLVERMELHO";
         edtavCodigo2_Internalname = "vCODIGO2";
         edtavLareatrabalho_descricao_Internalname = "vLAREATRABALHO_DESCRICAO";
         edtavLcontagemresultado_osfsosfm_Internalname = "vLCONTAGEMRESULTADO_OSFSOSFM";
         edtavLservico_Internalname = "vLSERVICO";
         edtavLresponsavelnome_Internalname = "vLRESPONSAVELNOME";
         chkavSelected2_Internalname = "vSELECTED2";
         edtavAtribuir2_Internalname = "vATRIBUIR2";
         edtavHistorico2_Internalname = "vHISTORICO2";
         tblPnllaranja_Internalname = "PNLLARANJA";
         Dvpanel_pnllaranja_Internalname = "DVPANEL_PNLLARANJA";
         tblPnlamarelo_Internalname = "PNLAMARELO";
         Dvpanel_pnlamarelo_Internalname = "DVPANEL_PNLAMARELO";
         tblUnnamedtable2_Internalname = "UNNAMEDTABLE2";
         tblTablecontent_Internalname = "TABLECONTENT";
         Confirmpanel_Internalname = "CONFIRMPANEL";
         Innewwindow_Internalname = "INNEWWINDOW";
         tblUsrtable_Internalname = "USRTABLE";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
         subGridresumo_Internalname = "GRIDRESUMO";
         subGridvermelho_Internalname = "GRIDVERMELHO";
         subGridlaranja_Internalname = "GRIDLARANJA";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavHistorico2_Jsonclick = "";
         edtavHistorico2_Visible = -1;
         edtavHistorico2_Enabled = 1;
         edtavAtribuir2_Jsonclick = "";
         edtavAtribuir2_Visible = -1;
         edtavAtribuir2_Enabled = 1;
         chkavSelected2.Visible = -1;
         chkavSelected2.Enabled = 1;
         edtavLresponsavelnome_Jsonclick = "";
         edtavLresponsavelnome_Visible = -1;
         edtavLservico_Jsonclick = "";
         edtavLservico_Visible = -1;
         edtavLcontagemresultado_osfsosfm_Jsonclick = "";
         edtavLcontagemresultado_osfsosfm_Visible = -1;
         edtavLareatrabalho_descricao_Jsonclick = "";
         edtavLareatrabalho_descricao_Visible = -1;
         edtavCodigo2_Jsonclick = "";
         edtavCodigo2_Visible = 0;
         edtavHistorico1_Jsonclick = "";
         edtavHistorico1_Visible = -1;
         edtavHistorico1_Enabled = 1;
         edtavAtribuir1_Jsonclick = "";
         edtavAtribuir1_Visible = -1;
         edtavAtribuir1_Enabled = 1;
         edtavVglosa_Jsonclick = "";
         edtavVglosa_Visible = -1;
         edtavVatraso_Jsonclick = "";
         edtavVatraso_Visible = -1;
         chkavSelected1.Visible = -1;
         chkavSelected1.Enabled = 1;
         edtavVresponsavelnome_Jsonclick = "";
         edtavVresponsavelnome_Visible = -1;
         edtavVservico_Jsonclick = "";
         edtavVservico_Visible = -1;
         edtavVcontagemresultado_osfsosfm_Jsonclick = "";
         edtavVcontagemresultado_osfsosfm_Visible = -1;
         edtavVareatrabalho_descricao_Jsonclick = "";
         edtavVareatrabalho_descricao_Visible = -1;
         edtavVcontagemresultado_dataentrega_Jsonclick = "";
         edtavVcontagemresultado_dataentrega_Visible = -1;
         edtavCodigo1_Jsonclick = "";
         edtavCodigo1_Visible = 0;
         edtavGlosa_Jsonclick = "";
         edtavAtraso_Jsonclick = "";
         edtavQuantidade_Jsonclick = "";
         edtavNome_Jsonclick = "";
         cmbavContagemresultado_responsavel_Jsonclick = "";
         dynavContagemresultado_servico_Jsonclick = "";
         edtavContagemresultado_demanda_Jsonclick = "";
         subGridresumo_Allowcollapsing = 0;
         subGridresumo_Allowselection = 0;
         edtavGlosa_Enabled = 0;
         edtavAtraso_Enabled = 0;
         edtavQuantidade_Enabled = 0;
         edtavNome_Enabled = 0;
         subGridresumo_Class = "WorkWithBorder WorkWith";
         subGridvermelho_Allowcollapsing = 0;
         subGridvermelho_Allowselection = 0;
         edtavHistorico1_Tooltiptext = "Ver hist�rico";
         edtavAtribuir1_Tooltiptext = "";
         edtavVglosa_Enabled = 1;
         edtavVatraso_Enabled = 1;
         edtavVresponsavelnome_Enabled = 1;
         edtavVservico_Enabled = 1;
         edtavVcontagemresultado_osfsosfm_Enabled = 1;
         edtavVareatrabalho_descricao_Enabled = 1;
         edtavVcontagemresultado_dataentrega_Enabled = 1;
         edtavCodigo1_Enabled = 1;
         subGridvermelho_Class = "WorkWithBorder WorkWith";
         subGridlaranja_Allowcollapsing = 0;
         subGridlaranja_Allowselection = 0;
         edtavHistorico2_Tooltiptext = "Ver hist�rico";
         edtavAtribuir2_Tooltiptext = "";
         edtavLresponsavelnome_Enabled = 1;
         edtavLservico_Enabled = 1;
         edtavLcontagemresultado_osfsosfm_Enabled = 1;
         edtavLareatrabalho_descricao_Enabled = 1;
         edtavCodigo2_Enabled = 1;
         subGridlaranja_Class = "WorkWithBorder WorkWith";
         subGridvermelho_Backcolorstyle = 3;
         subGridlaranja_Backcolorstyle = 3;
         subGridresumo_Backcolorstyle = 3;
         chkavSelected2.Caption = "";
         chkavSelected1.Caption = "";
         Innewwindow_Target = "";
         Confirmpanel_Draggeable = Convert.ToBoolean( 0);
         Confirmpanel_Confirmtype = "0";
         Confirmpanel_Buttoncanceltext = "Fechar";
         Confirmpanel_Buttonnotext = "N�o";
         Confirmpanel_Buttonyestext = "Fechar";
         Confirmpanel_Confirmtext = "Seu usu�rio n�o tem a gest�o de nenhum contrato.";
         Confirmpanel_Icon = "2";
         Confirmpanel_Title = "Aten��o";
         Confirmpanel_Height = "50";
         Dvpanel_pnlamarelo_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_pnlamarelo_Iconposition = "left";
         Dvpanel_pnlamarelo_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_pnlamarelo_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_pnlamarelo_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_pnlamarelo_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_pnlamarelo_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_pnlamarelo_Title = "Na semana";
         Dvpanel_pnlamarelo_Cls = "GXUI-DVelop-Panel";
         Dvpanel_pnlamarelo_Width = "100%";
         Dvpanel_pnllaranja_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_pnllaranja_Iconposition = "left";
         Dvpanel_pnllaranja_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_pnllaranja_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_pnllaranja_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_pnllaranja_Collapsed = Convert.ToBoolean( 1);
         Dvpanel_pnllaranja_Collapsible = Convert.ToBoolean( -1);
         Dvpanel_pnllaranja_Title = "Para hoje";
         Dvpanel_pnllaranja_Cls = "GXUI-DVelop-Panel";
         Dvpanel_pnllaranja_Width = "100%";
         Dvpanel_pnlvermelho_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_pnlvermelho_Iconposition = "left";
         Dvpanel_pnlvermelho_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_pnlvermelho_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_pnlvermelho_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_pnlvermelho_Collapsed = Convert.ToBoolean( 1);
         Dvpanel_pnlvermelho_Collapsible = Convert.ToBoolean( -1);
         Dvpanel_pnlvermelho_Title = "Atrasado";
         Dvpanel_pnlvermelho_Cls = "GXUI-DVelop-Panel";
         Dvpanel_pnlvermelho_Width = "100%";
         Dvpanel_pnlresumo_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_pnlresumo_Iconposition = "left";
         Dvpanel_pnlresumo_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_pnlresumo_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_pnlresumo_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_pnlresumo_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_pnlresumo_Collapsible = Convert.ToBoolean( -1);
         Dvpanel_pnlresumo_Title = "Resumo do Atrasado";
         Dvpanel_pnlresumo_Cls = "GXUI-DVelop-Panel";
         Dvpanel_pnlresumo_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Gest�o";
         subGridlaranja_Rows = 0;
         subGridvermelho_Rows = 0;
         subGridresumo_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRIDRESUMO_nFirstRecordOnPage',nv:0},{av:'GRIDRESUMO_nEOF',nv:0},{av:'subGridresumo_Rows',nv:0},{av:'GRIDLARANJA_nFirstRecordOnPage',nv:0},{av:'GRIDLARANJA_nEOF',nv:0},{av:'subGridlaranja_Rows',nv:0},{av:'AV8Laranja',fld:'vLARANJA',pic:'ZZZZZZZ9',nv:0},{av:'GRIDVERMELHO_nFirstRecordOnPage',nv:0},{av:'GRIDVERMELHO_nEOF',nv:0},{av:'subGridvermelho_Rows',nv:0},{av:'A53Contratada_AreaTrabalhoDes',fld:'CONTRATADA_AREATRABALHODES',pic:'@!',nv:''},{av:'A501ContagemResultado_OsFsOsFm',fld:'CONTAGEMRESULTADO_OSFSOSFM',pic:'',nv:''},{av:'A801ContagemResultado_ServicoSigla',fld:'CONTAGEMRESULTADO_SERVICOSIGLA',pic:'@!',nv:''},{av:'A1313ContagemResultado_ResponsavelPessNome',fld:'CONTAGEMRESULTADO_RESPONSAVELPESSNOME',pic:'@!',nv:''},{av:'AV7Vermelho',fld:'vVERMELHO',pic:'ZZZZZZZ9',nv:0},{av:'AV39Nome',fld:'vNOME',pic:'@!',nv:''},{av:'AV32ContagemResultado_Servico',fld:'vCONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV33ContagemResultado_Demanda',fld:'vCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'A472ContagemResultado_DataEntrega',fld:'CONTAGEMRESULTADO_DATAENTREGA',pic:'',nv:''},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV10ServerDate',fld:'vSERVERDATE',pic:'',nv:''},{av:'AV6Contratadas',fld:'vCONTRATADAS',pic:'',nv:null},{av:'AV36ContagemResultado_Responsavel',fld:'vCONTAGEMRESULTADO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'A890ContagemResultado_Responsavel',fld:'CONTAGEMRESULTADO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV21Responsaveis',fld:'vRESPONSAVEIS',pic:'',nv:null},{av:'A531ContagemResultado_StatusUltCnt',fld:'CONTAGEMRESULTADO_STATUSULTCNT',pic:'Z9',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'A499ContagemResultado_ContratadaPessoaCod',fld:'CONTAGEMRESULTADO_CONTRATADAPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV5WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV9Amarelo',fld:'vAMARELO',pic:'ZZZZZZZ9',nv:0}],oparms:[{av:'AV7Vermelho',fld:'vVERMELHO',pic:'ZZZZZZZ9',nv:0},{av:'AV8Laranja',fld:'vLARANJA',pic:'ZZZZZZZ9',nv:0},{av:'AV9Amarelo',fld:'vAMARELO',pic:'ZZZZZZZ9',nv:0},{av:'Dvpanel_pnlamarelo_Title',ctrl:'DVPANEL_PNLAMARELO',prop:'Title'}]}");
         setEventMetadata("GRIDLARANJA.LOAD","{handler:'E19L44',iparms:[{av:'A472ContagemResultado_DataEntrega',fld:'CONTAGEMRESULTADO_DATAENTREGA',pic:'',nv:''},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV10ServerDate',fld:'vSERVERDATE',pic:'',nv:''},{av:'AV6Contratadas',fld:'vCONTRATADAS',pic:'',nv:null},{av:'AV36ContagemResultado_Responsavel',fld:'vCONTAGEMRESULTADO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'A890ContagemResultado_Responsavel',fld:'CONTAGEMRESULTADO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV21Responsaveis',fld:'vRESPONSAVEIS',pic:'',nv:null},{av:'A531ContagemResultado_StatusUltCnt',fld:'CONTAGEMRESULTADO_STATUSULTCNT',pic:'Z9',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'A499ContagemResultado_ContratadaPessoaCod',fld:'CONTAGEMRESULTADO_CONTRATADAPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV5WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A53Contratada_AreaTrabalhoDes',fld:'CONTRATADA_AREATRABALHODES',pic:'@!',nv:''},{av:'A501ContagemResultado_OsFsOsFm',fld:'CONTAGEMRESULTADO_OSFSOSFM',pic:'',nv:''},{av:'A801ContagemResultado_ServicoSigla',fld:'CONTAGEMRESULTADO_SERVICOSIGLA',pic:'@!',nv:''},{av:'A1313ContagemResultado_ResponsavelPessNome',fld:'CONTAGEMRESULTADO_RESPONSAVELPESSNOME',pic:'@!',nv:''},{av:'AV8Laranja',fld:'vLARANJA',pic:'ZZZZZZZ9',nv:0},{av:'AV39Nome',fld:'vNOME',pic:'@!',nv:''},{av:'AV32ContagemResultado_Servico',fld:'vCONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV33ContagemResultado_Demanda',fld:'vCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''}],oparms:[{av:'AV27Atribuir2',fld:'vATRIBUIR2',pic:'',nv:''},{av:'edtavAtribuir2_Tooltiptext',ctrl:'vATRIBUIR2',prop:'Tooltiptext'},{av:'AV28Historico2',fld:'vHISTORICO2',pic:'',nv:''},{av:'edtavHistorico2_Tooltiptext',ctrl:'vHISTORICO2',prop:'Tooltiptext'},{av:'AV23Codigo2',fld:'vCODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV11LAreaTrabalho_Descricao',fld:'vLAREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'AV12LContagemResultado_OsFsOsFm',fld:'vLCONTAGEMRESULTADO_OSFSOSFM',pic:'',nv:''},{av:'AV34LServico',fld:'vLSERVICO',pic:'@!',nv:''},{av:'AV39Nome',fld:'vNOME',pic:'@!',nv:''},{av:'AV8Laranja',fld:'vLARANJA',pic:'ZZZZZZZ9',nv:0},{av:'AV19LResponsavelNome',fld:'vLRESPONSAVELNOME',pic:'@!',nv:''},{av:'Dvpanel_pnllaranja_Title',ctrl:'DVPANEL_PNLLARANJA',prop:'Title'}]}");
         setEventMetadata("GRIDVERMELHO.LOAD","{handler:'E16L46',iparms:[{av:'A472ContagemResultado_DataEntrega',fld:'CONTAGEMRESULTADO_DATAENTREGA',pic:'',nv:''},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A890ContagemResultado_Responsavel',fld:'CONTAGEMRESULTADO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV10ServerDate',fld:'vSERVERDATE',pic:'',nv:''},{av:'AV6Contratadas',fld:'vCONTRATADAS',pic:'',nv:null},{av:'AV36ContagemResultado_Responsavel',fld:'vCONTAGEMRESULTADO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV21Responsaveis',fld:'vRESPONSAVEIS',pic:'',nv:null},{av:'A531ContagemResultado_StatusUltCnt',fld:'CONTAGEMRESULTADO_STATUSULTCNT',pic:'Z9',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'A499ContagemResultado_ContratadaPessoaCod',fld:'CONTAGEMRESULTADO_CONTRATADAPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV5WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A53Contratada_AreaTrabalhoDes',fld:'CONTRATADA_AREATRABALHODES',pic:'@!',nv:''},{av:'A501ContagemResultado_OsFsOsFm',fld:'CONTAGEMRESULTADO_OSFSOSFM',pic:'',nv:''},{av:'A801ContagemResultado_ServicoSigla',fld:'CONTAGEMRESULTADO_SERVICOSIGLA',pic:'@!',nv:''},{av:'A1313ContagemResultado_ResponsavelPessNome',fld:'CONTAGEMRESULTADO_RESPONSAVELPESSNOME',pic:'@!',nv:''},{av:'AV7Vermelho',fld:'vVERMELHO',pic:'ZZZZZZZ9',nv:0},{av:'AV39Nome',fld:'vNOME',pic:'@!',nv:''},{av:'AV32ContagemResultado_Servico',fld:'vCONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV33ContagemResultado_Demanda',fld:'vCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''}],oparms:[{av:'AV29Atribuir1',fld:'vATRIBUIR1',pic:'',nv:''},{av:'edtavAtribuir1_Tooltiptext',ctrl:'vATRIBUIR1',prop:'Tooltiptext'},{av:'AV30Historico1',fld:'vHISTORICO1',pic:'',nv:''},{av:'edtavHistorico1_Tooltiptext',ctrl:'vHISTORICO1',prop:'Tooltiptext'},{av:'AV25Codigo1',fld:'vCODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV14VAreaTrabalho_Descricao',fld:'vVAREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'AV15VContagemResultado_OsFsOsFm',fld:'vVCONTAGEMRESULTADO_OSFSOSFM',pic:'',nv:''},{av:'AV16VContagemResultado_DataEntrega',fld:'vVCONTAGEMRESULTADO_DATAENTREGA',pic:'',nv:''},{av:'AV35VServico',fld:'vVSERVICO',pic:'@!',nv:''},{av:'AV39Nome',fld:'vNOME',pic:'@!',nv:''},{av:'AV37VAtraso',fld:'vVATRASO',pic:'ZZZ9',nv:0},{av:'AV20VResponsavelNome',fld:'vVRESPONSAVELNOME',pic:'@!',nv:''},{av:'AV7Vermelho',fld:'vVERMELHO',pic:'ZZZZZZZ9',nv:0},{av:'Dvpanel_pnlvermelho_Title',ctrl:'DVPANEL_PNLVERMELHO',prop:'Title'}]}");
         setEventMetadata("GRIDRESUMO.LOAD","{handler:'E14L42',iparms:[{av:'A1313ContagemResultado_ResponsavelPessNome',fld:'CONTAGEMRESULTADO_RESPONSAVELPESSNOME',pic:'@!',nv:''},{av:'A472ContagemResultado_DataEntrega',fld:'CONTAGEMRESULTADO_DATAENTREGA',pic:'',nv:''},{av:'AV10ServerDate',fld:'vSERVERDATE',pic:'',nv:''},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV6Contratadas',fld:'vCONTRATADAS',pic:'',nv:null},{av:'AV36ContagemResultado_Responsavel',fld:'vCONTAGEMRESULTADO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'A890ContagemResultado_Responsavel',fld:'CONTAGEMRESULTADO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV21Responsaveis',fld:'vRESPONSAVEIS',pic:'',nv:null},{av:'A531ContagemResultado_StatusUltCnt',fld:'CONTAGEMRESULTADO_STATUSULTCNT',pic:'Z9',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'A499ContagemResultado_ContratadaPessoaCod',fld:'CONTAGEMRESULTADO_CONTRATADAPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV5WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV39Nome',fld:'vNOME',pic:'@!',nv:''},{av:'AV32ContagemResultado_Servico',fld:'vCONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV33ContagemResultado_Demanda',fld:'vCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''}],oparms:[{av:'AV45Quantidade',fld:'vQUANTIDADE',pic:'ZZ9',nv:0},{av:'AV46Atraso',fld:'vATRASO',pic:'ZZZ9',nv:0},{av:'AV39Nome',fld:'vNOME',pic:'@!',nv:''},{av:'AV44Glosa',fld:'vGLOSA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0}]}");
         setEventMetadata("'DOATRIBUIR2'","{handler:'E20L42',iparms:[{av:'GRIDRESUMO_nFirstRecordOnPage',nv:0},{av:'GRIDRESUMO_nEOF',nv:0},{av:'subGridresumo_Rows',nv:0},{av:'A472ContagemResultado_DataEntrega',fld:'CONTAGEMRESULTADO_DATAENTREGA',pic:'',nv:''},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV10ServerDate',fld:'vSERVERDATE',pic:'',nv:''},{av:'AV6Contratadas',fld:'vCONTRATADAS',pic:'',nv:null},{av:'AV36ContagemResultado_Responsavel',fld:'vCONTAGEMRESULTADO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'A890ContagemResultado_Responsavel',fld:'CONTAGEMRESULTADO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV21Responsaveis',fld:'vRESPONSAVEIS',pic:'',nv:null},{av:'A531ContagemResultado_StatusUltCnt',fld:'CONTAGEMRESULTADO_STATUSULTCNT',pic:'Z9',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'A499ContagemResultado_ContratadaPessoaCod',fld:'CONTAGEMRESULTADO_CONTRATADAPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV5WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV9Amarelo',fld:'vAMARELO',pic:'ZZZZZZZ9',nv:0},{av:'A1313ContagemResultado_ResponsavelPessNome',fld:'CONTAGEMRESULTADO_RESPONSAVELPESSNOME',pic:'@!',nv:''},{av:'AV39Nome',fld:'vNOME',pic:'@!',nv:''},{av:'AV32ContagemResultado_Servico',fld:'vCONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV33ContagemResultado_Demanda',fld:'vCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV53Selecionadas2',fld:'vSELECIONADAS2',pic:'',nv:null},{av:'AV23Codigo2',fld:'vCODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV31PodeAtribuirDmn',fld:'vPODEATRIBUIRDMN',pic:'',nv:false},{av:'AV19LResponsavelNome',fld:'vLRESPONSAVELNOME',pic:'@!',nv:''}],oparms:[{av:'AV53Selecionadas2',fld:'vSELECIONADAS2',pic:'',nv:null},{av:'AV19LResponsavelNome',fld:'vLRESPONSAVELNOME',pic:'@!',nv:''}]}");
         setEventMetadata("'DOATRIBUIR1'","{handler:'E17L42',iparms:[{av:'GRIDRESUMO_nFirstRecordOnPage',nv:0},{av:'GRIDRESUMO_nEOF',nv:0},{av:'subGridresumo_Rows',nv:0},{av:'A472ContagemResultado_DataEntrega',fld:'CONTAGEMRESULTADO_DATAENTREGA',pic:'',nv:''},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV10ServerDate',fld:'vSERVERDATE',pic:'',nv:''},{av:'AV6Contratadas',fld:'vCONTRATADAS',pic:'',nv:null},{av:'AV36ContagemResultado_Responsavel',fld:'vCONTAGEMRESULTADO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'A890ContagemResultado_Responsavel',fld:'CONTAGEMRESULTADO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV21Responsaveis',fld:'vRESPONSAVEIS',pic:'',nv:null},{av:'A531ContagemResultado_StatusUltCnt',fld:'CONTAGEMRESULTADO_STATUSULTCNT',pic:'Z9',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'A499ContagemResultado_ContratadaPessoaCod',fld:'CONTAGEMRESULTADO_CONTRATADAPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV5WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV9Amarelo',fld:'vAMARELO',pic:'ZZZZZZZ9',nv:0},{av:'A1313ContagemResultado_ResponsavelPessNome',fld:'CONTAGEMRESULTADO_RESPONSAVELPESSNOME',pic:'@!',nv:''},{av:'AV39Nome',fld:'vNOME',pic:'@!',nv:''},{av:'AV32ContagemResultado_Servico',fld:'vCONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV33ContagemResultado_Demanda',fld:'vCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV50Selecionadas1',fld:'vSELECIONADAS1',pic:'',nv:null},{av:'AV25Codigo1',fld:'vCODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV31PodeAtribuirDmn',fld:'vPODEATRIBUIRDMN',pic:'',nv:false},{av:'AV20VResponsavelNome',fld:'vVRESPONSAVELNOME',pic:'@!',nv:''}],oparms:[{av:'AV50Selecionadas1',fld:'vSELECIONADAS1',pic:'',nv:null},{av:'AV20VResponsavelNome',fld:'vVRESPONSAVELNOME',pic:'@!',nv:''}]}");
         setEventMetadata("'DOLIMPARFILTROS'","{handler:'E12L42',iparms:[],oparms:[{av:'AV33ContagemResultado_Demanda',fld:'vCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV36ContagemResultado_Responsavel',fld:'vCONTAGEMRESULTADO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV32ContagemResultado_Servico',fld:'vCONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DOATUALIZAR'","{handler:'E11L42',iparms:[{av:'GRIDRESUMO_nFirstRecordOnPage',nv:0},{av:'GRIDRESUMO_nEOF',nv:0},{av:'subGridresumo_Rows',nv:0},{av:'A472ContagemResultado_DataEntrega',fld:'CONTAGEMRESULTADO_DATAENTREGA',pic:'',nv:''},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV10ServerDate',fld:'vSERVERDATE',pic:'',nv:''},{av:'AV6Contratadas',fld:'vCONTRATADAS',pic:'',nv:null},{av:'AV36ContagemResultado_Responsavel',fld:'vCONTAGEMRESULTADO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'A890ContagemResultado_Responsavel',fld:'CONTAGEMRESULTADO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV21Responsaveis',fld:'vRESPONSAVEIS',pic:'',nv:null},{av:'A531ContagemResultado_StatusUltCnt',fld:'CONTAGEMRESULTADO_STATUSULTCNT',pic:'Z9',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'A499ContagemResultado_ContratadaPessoaCod',fld:'CONTAGEMRESULTADO_CONTRATADAPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV5WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV9Amarelo',fld:'vAMARELO',pic:'ZZZZZZZ9',nv:0},{av:'A1313ContagemResultado_ResponsavelPessNome',fld:'CONTAGEMRESULTADO_RESPONSAVELPESSNOME',pic:'@!',nv:''},{av:'AV39Nome',fld:'vNOME',pic:'@!',nv:''},{av:'AV32ContagemResultado_Servico',fld:'vCONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV33ContagemResultado_Demanda',fld:'vCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''}],oparms:[]}");
         setEventMetadata("VHISTORICO1.CLICK","{handler:'E22L46',iparms:[{av:'AV25Codigo1',fld:'vCODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV22Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV22Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'Innewwindow_Target',ctrl:'INNEWWINDOW',prop:'Target'}]}");
         setEventMetadata("VHISTORICO2.CLICK","{handler:'E23L44',iparms:[{av:'AV23Codigo2',fld:'vCODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV22Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV22Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'Innewwindow_Target',ctrl:'INNEWWINDOW',prop:'Target'}]}");
         setEventMetadata("VSELECTED1.CLICK","{handler:'E18L42',iparms:[{av:'AV26Selected1',fld:'vSELECTED1',pic:'',nv:false},{av:'AV25Codigo1',fld:'vCODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV50Selecionadas1',fld:'vSELECIONADAS1',pic:'',nv:null},{av:'AV51Count1',fld:'vCOUNT1',pic:'ZZZ9',nv:0}],oparms:[{av:'AV50Selecionadas1',fld:'vSELECIONADAS1',pic:'',nv:null},{av:'AV51Count1',fld:'vCOUNT1',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("VSELECTED2.CLICK","{handler:'E21L42',iparms:[{av:'AV24Selected2',fld:'vSELECTED2',pic:'',nv:false},{av:'AV23Codigo2',fld:'vCODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV53Selecionadas2',fld:'vSELECIONADAS2',pic:'',nv:null},{av:'AV52Count2',fld:'vCOUNT2',pic:'ZZZ9',nv:0}],oparms:[{av:'AV53Selecionadas2',fld:'vSELECIONADAS2',pic:'',nv:null},{av:'AV52Count2',fld:'vCOUNT2',pic:'ZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV5WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         A484ContagemResultado_StatusDmn = "";
         AV10ServerDate = DateTime.MinValue;
         AV6Contratadas = new GxSimpleCollection();
         AV21Responsaveis = new GxSimpleCollection();
         A1313ContagemResultado_ResponsavelPessNome = "";
         AV39Nome = "";
         AV33ContagemResultado_Demanda = "";
         GXKey = "";
         A53Contratada_AreaTrabalhoDes = "";
         A501ContagemResultado_OsFsOsFm = "";
         A801ContagemResultado_ServicoSigla = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV53Selecionadas2 = new GxSimpleCollection();
         AV50Selecionadas1 = new GxSimpleCollection();
         Confirmpanel_Width = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV16VContagemResultado_DataEntrega = DateTime.MinValue;
         AV14VAreaTrabalho_Descricao = "";
         AV15VContagemResultado_OsFsOsFm = "";
         AV35VServico = "";
         AV20VResponsavelNome = "";
         AV29Atribuir1 = "";
         AV60Atribuir1_GXI = "";
         AV30Historico1 = "";
         AV61Historico1_GXI = "";
         AV11LAreaTrabalho_Descricao = "";
         AV12LContagemResultado_OsFsOsFm = "";
         AV34LServico = "";
         AV19LResponsavelNome = "";
         AV27Atribuir2 = "";
         AV57Atribuir2_GXI = "";
         AV28Historico2 = "";
         AV58Historico2_GXI = "";
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00L42_A155Servico_Codigo = new int[1] ;
         H00L42_A605Servico_Sigla = new String[] {""} ;
         GridresumoContainer = new GXWebGrid( context);
         GridlaranjaContainer = new GXWebGrid( context);
         GridvermelhoContainer = new GXWebGrid( context);
         H00L43_A1078ContratoGestor_ContratoCod = new int[1] ;
         H00L43_A1079ContratoGestor_UsuarioCod = new int[1] ;
         H00L43_A1136ContratoGestor_ContratadaCod = new int[1] ;
         H00L43_n1136ContratoGestor_ContratadaCod = new bool[] {false} ;
         lV33ContagemResultado_Demanda = "";
         A457ContagemResultado_Demanda = "";
         A493ContagemResultado_DemandaFM = "";
         H00L45_A1312ContagemResultado_ResponsavelPessCod = new int[1] ;
         H00L45_n1312ContagemResultado_ResponsavelPessCod = new bool[] {false} ;
         H00L45_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00L45_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00L45_A1313ContagemResultado_ResponsavelPessNome = new String[] {""} ;
         H00L45_n1313ContagemResultado_ResponsavelPessNome = new bool[] {false} ;
         H00L45_A1237ContagemResultado_PrazoMaisDias = new short[1] ;
         H00L45_n1237ContagemResultado_PrazoMaisDias = new bool[] {false} ;
         H00L45_A1227ContagemResultado_PrazoInicialDias = new short[1] ;
         H00L45_n1227ContagemResultado_PrazoInicialDias = new bool[] {false} ;
         H00L45_A601ContagemResultado_Servico = new int[1] ;
         H00L45_n601ContagemResultado_Servico = new bool[] {false} ;
         H00L45_A890ContagemResultado_Responsavel = new int[1] ;
         H00L45_n890ContagemResultado_Responsavel = new bool[] {false} ;
         H00L45_A493ContagemResultado_DemandaFM = new String[] {""} ;
         H00L45_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         H00L45_A457ContagemResultado_Demanda = new String[] {""} ;
         H00L45_n457ContagemResultado_Demanda = new bool[] {false} ;
         H00L45_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00L45_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00L45_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00L45_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00L45_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         H00L45_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         H00L45_A499ContagemResultado_ContratadaPessoaCod = new int[1] ;
         H00L45_n499ContagemResultado_ContratadaPessoaCod = new bool[] {false} ;
         H00L45_A602ContagemResultado_OSVinculada = new int[1] ;
         H00L45_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         H00L45_A456ContagemResultado_Codigo = new int[1] ;
         H00L45_A531ContagemResultado_StatusUltCnt = new short[1] ;
         H00L45_n531ContagemResultado_StatusUltCnt = new bool[] {false} ;
         GridresumoRow = new GXWebRow();
         AV49WebSession = context.GetSession();
         H00L47_A499ContagemResultado_ContratadaPessoaCod = new int[1] ;
         H00L47_n499ContagemResultado_ContratadaPessoaCod = new bool[] {false} ;
         H00L47_A602ContagemResultado_OSVinculada = new int[1] ;
         H00L47_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         H00L47_A456ContagemResultado_Codigo = new int[1] ;
         H00L47_A890ContagemResultado_Responsavel = new int[1] ;
         H00L47_n890ContagemResultado_Responsavel = new bool[] {false} ;
         H00L47_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00L47_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00L47_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00L47_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00L47_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         H00L47_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         H00L47_A531ContagemResultado_StatusUltCnt = new short[1] ;
         H00L47_n531ContagemResultado_StatusUltCnt = new bool[] {false} ;
         H00L49_A499ContagemResultado_ContratadaPessoaCod = new int[1] ;
         H00L49_n499ContagemResultado_ContratadaPessoaCod = new bool[] {false} ;
         H00L49_A602ContagemResultado_OSVinculada = new int[1] ;
         H00L49_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         H00L49_A456ContagemResultado_Codigo = new int[1] ;
         H00L49_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00L49_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00L49_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00L49_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00L49_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         H00L49_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         H00L49_A531ContagemResultado_StatusUltCnt = new short[1] ;
         H00L49_n531ContagemResultado_StatusUltCnt = new bool[] {false} ;
         H00L49_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00L49_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00L49_A890ContagemResultado_Responsavel = new int[1] ;
         H00L49_n890ContagemResultado_Responsavel = new bool[] {false} ;
         H00L410_A57Usuario_PessoaCod = new int[1] ;
         H00L410_A1Usuario_Codigo = new int[1] ;
         H00L410_A58Usuario_PessoaNom = new String[] {""} ;
         H00L410_n58Usuario_PessoaNom = new bool[] {false} ;
         H00L410_A2Usuario_Nome = new String[] {""} ;
         H00L410_n2Usuario_Nome = new bool[] {false} ;
         A58Usuario_PessoaNom = "";
         A2Usuario_Nome = "";
         H00L412_A1312ContagemResultado_ResponsavelPessCod = new int[1] ;
         H00L412_n1312ContagemResultado_ResponsavelPessCod = new bool[] {false} ;
         H00L412_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00L412_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00L412_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00L412_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00L412_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         H00L412_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         H00L412_A1237ContagemResultado_PrazoMaisDias = new short[1] ;
         H00L412_n1237ContagemResultado_PrazoMaisDias = new bool[] {false} ;
         H00L412_A1227ContagemResultado_PrazoInicialDias = new short[1] ;
         H00L412_n1227ContagemResultado_PrazoInicialDias = new bool[] {false} ;
         H00L412_A601ContagemResultado_Servico = new int[1] ;
         H00L412_n601ContagemResultado_Servico = new bool[] {false} ;
         H00L412_A499ContagemResultado_ContratadaPessoaCod = new int[1] ;
         H00L412_n499ContagemResultado_ContratadaPessoaCod = new bool[] {false} ;
         H00L412_A602ContagemResultado_OSVinculada = new int[1] ;
         H00L412_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         H00L412_A456ContagemResultado_Codigo = new int[1] ;
         H00L412_A890ContagemResultado_Responsavel = new int[1] ;
         H00L412_n890ContagemResultado_Responsavel = new bool[] {false} ;
         H00L412_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00L412_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00L412_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00L412_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00L412_A53Contratada_AreaTrabalhoDes = new String[] {""} ;
         H00L412_n53Contratada_AreaTrabalhoDes = new bool[] {false} ;
         H00L412_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         H00L412_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         H00L412_A1313ContagemResultado_ResponsavelPessNome = new String[] {""} ;
         H00L412_n1313ContagemResultado_ResponsavelPessNome = new bool[] {false} ;
         H00L412_A531ContagemResultado_StatusUltCnt = new short[1] ;
         H00L412_n531ContagemResultado_StatusUltCnt = new bool[] {false} ;
         H00L412_A493ContagemResultado_DemandaFM = new String[] {""} ;
         H00L412_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         H00L412_A457ContagemResultado_Demanda = new String[] {""} ;
         H00L412_n457ContagemResultado_Demanda = new bool[] {false} ;
         GridlaranjaRow = new GXWebRow();
         H00L414_A1312ContagemResultado_ResponsavelPessCod = new int[1] ;
         H00L414_n1312ContagemResultado_ResponsavelPessCod = new bool[] {false} ;
         H00L414_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00L414_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00L414_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00L414_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00L414_A1237ContagemResultado_PrazoMaisDias = new short[1] ;
         H00L414_n1237ContagemResultado_PrazoMaisDias = new bool[] {false} ;
         H00L414_A1227ContagemResultado_PrazoInicialDias = new short[1] ;
         H00L414_n1227ContagemResultado_PrazoInicialDias = new bool[] {false} ;
         H00L414_A601ContagemResultado_Servico = new int[1] ;
         H00L414_n601ContagemResultado_Servico = new bool[] {false} ;
         H00L414_A499ContagemResultado_ContratadaPessoaCod = new int[1] ;
         H00L414_n499ContagemResultado_ContratadaPessoaCod = new bool[] {false} ;
         H00L414_A602ContagemResultado_OSVinculada = new int[1] ;
         H00L414_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         H00L414_A456ContagemResultado_Codigo = new int[1] ;
         H00L414_A890ContagemResultado_Responsavel = new int[1] ;
         H00L414_n890ContagemResultado_Responsavel = new bool[] {false} ;
         H00L414_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00L414_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00L414_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00L414_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00L414_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         H00L414_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         H00L414_A53Contratada_AreaTrabalhoDes = new String[] {""} ;
         H00L414_n53Contratada_AreaTrabalhoDes = new bool[] {false} ;
         H00L414_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         H00L414_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         H00L414_A1313ContagemResultado_ResponsavelPessNome = new String[] {""} ;
         H00L414_n1313ContagemResultado_ResponsavelPessNome = new bool[] {false} ;
         H00L414_A531ContagemResultado_StatusUltCnt = new short[1] ;
         H00L414_n531ContagemResultado_StatusUltCnt = new bool[] {false} ;
         H00L414_A493ContagemResultado_DemandaFM = new String[] {""} ;
         H00L414_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         H00L414_A457ContagemResultado_Demanda = new String[] {""} ;
         H00L414_n457ContagemResultado_Demanda = new bool[] {false} ;
         GridvermelhoRow = new GXWebRow();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         subGridlaranja_Linesclass = "";
         GridlaranjaColumn = new GXWebColumn();
         subGridvermelho_Linesclass = "";
         GridvermelhoColumn = new GXWebColumn();
         subGridresumo_Linesclass = "";
         GridresumoColumn = new GXWebColumn();
         TempTags = "";
         imgLimparfiltros_Jsonclick = "";
         bttBtnatualizar_Jsonclick = "";
         lblTextblocktitle_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_gestao__default(),
            new Object[][] {
                new Object[] {
               H00L42_A155Servico_Codigo, H00L42_A605Servico_Sigla
               }
               , new Object[] {
               H00L43_A1078ContratoGestor_ContratoCod, H00L43_A1079ContratoGestor_UsuarioCod, H00L43_A1136ContratoGestor_ContratadaCod, H00L43_n1136ContratoGestor_ContratadaCod
               }
               , new Object[] {
               H00L45_A1312ContagemResultado_ResponsavelPessCod, H00L45_n1312ContagemResultado_ResponsavelPessCod, H00L45_A1553ContagemResultado_CntSrvCod, H00L45_n1553ContagemResultado_CntSrvCod, H00L45_A1313ContagemResultado_ResponsavelPessNome, H00L45_n1313ContagemResultado_ResponsavelPessNome, H00L45_A1237ContagemResultado_PrazoMaisDias, H00L45_n1237ContagemResultado_PrazoMaisDias, H00L45_A1227ContagemResultado_PrazoInicialDias, H00L45_n1227ContagemResultado_PrazoInicialDias,
               H00L45_A601ContagemResultado_Servico, H00L45_n601ContagemResultado_Servico, H00L45_A890ContagemResultado_Responsavel, H00L45_n890ContagemResultado_Responsavel, H00L45_A493ContagemResultado_DemandaFM, H00L45_n493ContagemResultado_DemandaFM, H00L45_A457ContagemResultado_Demanda, H00L45_n457ContagemResultado_Demanda, H00L45_A490ContagemResultado_ContratadaCod, H00L45_n490ContagemResultado_ContratadaCod,
               H00L45_A484ContagemResultado_StatusDmn, H00L45_n484ContagemResultado_StatusDmn, H00L45_A472ContagemResultado_DataEntrega, H00L45_n472ContagemResultado_DataEntrega, H00L45_A499ContagemResultado_ContratadaPessoaCod, H00L45_n499ContagemResultado_ContratadaPessoaCod, H00L45_A602ContagemResultado_OSVinculada, H00L45_n602ContagemResultado_OSVinculada, H00L45_A456ContagemResultado_Codigo, H00L45_A531ContagemResultado_StatusUltCnt,
               H00L45_n531ContagemResultado_StatusUltCnt
               }
               , new Object[] {
               H00L47_A499ContagemResultado_ContratadaPessoaCod, H00L47_n499ContagemResultado_ContratadaPessoaCod, H00L47_A602ContagemResultado_OSVinculada, H00L47_n602ContagemResultado_OSVinculada, H00L47_A456ContagemResultado_Codigo, H00L47_A890ContagemResultado_Responsavel, H00L47_n890ContagemResultado_Responsavel, H00L47_A490ContagemResultado_ContratadaCod, H00L47_n490ContagemResultado_ContratadaCod, H00L47_A484ContagemResultado_StatusDmn,
               H00L47_n484ContagemResultado_StatusDmn, H00L47_A472ContagemResultado_DataEntrega, H00L47_n472ContagemResultado_DataEntrega, H00L47_A531ContagemResultado_StatusUltCnt, H00L47_n531ContagemResultado_StatusUltCnt
               }
               , new Object[] {
               H00L49_A499ContagemResultado_ContratadaPessoaCod, H00L49_n499ContagemResultado_ContratadaPessoaCod, H00L49_A602ContagemResultado_OSVinculada, H00L49_n602ContagemResultado_OSVinculada, H00L49_A456ContagemResultado_Codigo, H00L49_A490ContagemResultado_ContratadaCod, H00L49_n490ContagemResultado_ContratadaCod, H00L49_A484ContagemResultado_StatusDmn, H00L49_n484ContagemResultado_StatusDmn, H00L49_A472ContagemResultado_DataEntrega,
               H00L49_n472ContagemResultado_DataEntrega, H00L49_A531ContagemResultado_StatusUltCnt, H00L49_n531ContagemResultado_StatusUltCnt, H00L49_A52Contratada_AreaTrabalhoCod, H00L49_n52Contratada_AreaTrabalhoCod, H00L49_A890ContagemResultado_Responsavel, H00L49_n890ContagemResultado_Responsavel
               }
               , new Object[] {
               H00L410_A57Usuario_PessoaCod, H00L410_A1Usuario_Codigo, H00L410_A58Usuario_PessoaNom, H00L410_n58Usuario_PessoaNom, H00L410_A2Usuario_Nome, H00L410_n2Usuario_Nome
               }
               , new Object[] {
               H00L412_A1312ContagemResultado_ResponsavelPessCod, H00L412_n1312ContagemResultado_ResponsavelPessCod, H00L412_A52Contratada_AreaTrabalhoCod, H00L412_n52Contratada_AreaTrabalhoCod, H00L412_A1553ContagemResultado_CntSrvCod, H00L412_n1553ContagemResultado_CntSrvCod, H00L412_A472ContagemResultado_DataEntrega, H00L412_n472ContagemResultado_DataEntrega, H00L412_A1237ContagemResultado_PrazoMaisDias, H00L412_n1237ContagemResultado_PrazoMaisDias,
               H00L412_A1227ContagemResultado_PrazoInicialDias, H00L412_n1227ContagemResultado_PrazoInicialDias, H00L412_A601ContagemResultado_Servico, H00L412_n601ContagemResultado_Servico, H00L412_A499ContagemResultado_ContratadaPessoaCod, H00L412_n499ContagemResultado_ContratadaPessoaCod, H00L412_A602ContagemResultado_OSVinculada, H00L412_n602ContagemResultado_OSVinculada, H00L412_A456ContagemResultado_Codigo, H00L412_A890ContagemResultado_Responsavel,
               H00L412_n890ContagemResultado_Responsavel, H00L412_A490ContagemResultado_ContratadaCod, H00L412_n490ContagemResultado_ContratadaCod, H00L412_A484ContagemResultado_StatusDmn, H00L412_n484ContagemResultado_StatusDmn, H00L412_A53Contratada_AreaTrabalhoDes, H00L412_n53Contratada_AreaTrabalhoDes, H00L412_A801ContagemResultado_ServicoSigla, H00L412_n801ContagemResultado_ServicoSigla, H00L412_A1313ContagemResultado_ResponsavelPessNome,
               H00L412_n1313ContagemResultado_ResponsavelPessNome, H00L412_A531ContagemResultado_StatusUltCnt, H00L412_n531ContagemResultado_StatusUltCnt, H00L412_A493ContagemResultado_DemandaFM, H00L412_n493ContagemResultado_DemandaFM, H00L412_A457ContagemResultado_Demanda, H00L412_n457ContagemResultado_Demanda
               }
               , new Object[] {
               H00L414_A1312ContagemResultado_ResponsavelPessCod, H00L414_n1312ContagemResultado_ResponsavelPessCod, H00L414_A52Contratada_AreaTrabalhoCod, H00L414_n52Contratada_AreaTrabalhoCod, H00L414_A1553ContagemResultado_CntSrvCod, H00L414_n1553ContagemResultado_CntSrvCod, H00L414_A1237ContagemResultado_PrazoMaisDias, H00L414_n1237ContagemResultado_PrazoMaisDias, H00L414_A1227ContagemResultado_PrazoInicialDias, H00L414_n1227ContagemResultado_PrazoInicialDias,
               H00L414_A601ContagemResultado_Servico, H00L414_n601ContagemResultado_Servico, H00L414_A499ContagemResultado_ContratadaPessoaCod, H00L414_n499ContagemResultado_ContratadaPessoaCod, H00L414_A602ContagemResultado_OSVinculada, H00L414_n602ContagemResultado_OSVinculada, H00L414_A456ContagemResultado_Codigo, H00L414_A890ContagemResultado_Responsavel, H00L414_n890ContagemResultado_Responsavel, H00L414_A490ContagemResultado_ContratadaCod,
               H00L414_n490ContagemResultado_ContratadaCod, H00L414_A484ContagemResultado_StatusDmn, H00L414_n484ContagemResultado_StatusDmn, H00L414_A472ContagemResultado_DataEntrega, H00L414_n472ContagemResultado_DataEntrega, H00L414_A53Contratada_AreaTrabalhoDes, H00L414_n53Contratada_AreaTrabalhoDes, H00L414_A801ContagemResultado_ServicoSigla, H00L414_n801ContagemResultado_ServicoSigla, H00L414_A1313ContagemResultado_ResponsavelPessNome,
               H00L414_n1313ContagemResultado_ResponsavelPessNome, H00L414_A531ContagemResultado_StatusUltCnt, H00L414_n531ContagemResultado_StatusUltCnt, H00L414_A493ContagemResultado_DemandaFM, H00L414_n493ContagemResultado_DemandaFM, H00L414_A457ContagemResultado_Demanda, H00L414_n457ContagemResultado_Demanda
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavNome_Enabled = 0;
         edtavQuantidade_Enabled = 0;
         edtavAtraso_Enabled = 0;
         edtavGlosa_Enabled = 0;
         edtavCodigo1_Enabled = 0;
         edtavVcontagemresultado_dataentrega_Enabled = 0;
         edtavVareatrabalho_descricao_Enabled = 0;
         edtavVcontagemresultado_osfsosfm_Enabled = 0;
         edtavVservico_Enabled = 0;
         edtavVresponsavelnome_Enabled = 0;
         edtavVatraso_Enabled = 0;
         edtavVglosa_Enabled = 0;
         edtavCodigo2_Enabled = 0;
         edtavLareatrabalho_descricao_Enabled = 0;
         edtavLcontagemresultado_osfsosfm_Enabled = 0;
         edtavLservico_Enabled = 0;
         edtavLresponsavelnome_Enabled = 0;
      }

      private short nRcdExists_8 ;
      private short nIsMod_8 ;
      private short nRcdExists_9 ;
      private short nIsMod_9 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_39 ;
      private short nGXsfl_39_idx=1 ;
      private short A531ContagemResultado_StatusUltCnt ;
      private short nRC_GXsfl_53 ;
      private short nGXsfl_53_idx=1 ;
      private short nRC_GXsfl_71 ;
      private short nGXsfl_71_idx=1 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short nGXWrapped ;
      private short GRIDRESUMO_nEOF ;
      private short GRIDVERMELHO_nEOF ;
      private short GRIDLARANJA_nEOF ;
      private short AV51Count1 ;
      private short AV52Count2 ;
      private short wbEnd ;
      private short wbStart ;
      private short AV45Quantidade ;
      private short AV46Atraso ;
      private short AV37VAtraso ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_39_Refreshing=0 ;
      private short subGridresumo_Backcolorstyle ;
      private short nGXsfl_71_Refreshing=0 ;
      private short subGridlaranja_Backcolorstyle ;
      private short nGXsfl_53_Refreshing=0 ;
      private short subGridvermelho_Backcolorstyle ;
      private short A1227ContagemResultado_PrazoInicialDias ;
      private short A1237ContagemResultado_PrazoMaisDias ;
      private short AV40p ;
      private short AV47f ;
      private short subGridlaranja_Titlebackstyle ;
      private short subGridlaranja_Allowselection ;
      private short subGridlaranja_Allowhovering ;
      private short subGridlaranja_Allowcollapsing ;
      private short subGridlaranja_Collapsed ;
      private short subGridvermelho_Titlebackstyle ;
      private short subGridvermelho_Allowselection ;
      private short subGridvermelho_Allowhovering ;
      private short subGridvermelho_Allowcollapsing ;
      private short subGridvermelho_Collapsed ;
      private short subGridresumo_Titlebackstyle ;
      private short subGridresumo_Allowselection ;
      private short subGridresumo_Allowhovering ;
      private short subGridresumo_Allowcollapsing ;
      private short subGridresumo_Collapsed ;
      private short subGridresumo_Backstyle ;
      private short subGridvermelho_Backstyle ;
      private short subGridlaranja_Backstyle ;
      private int subGridresumo_Rows ;
      private int A490ContagemResultado_ContratadaCod ;
      private int AV36ContagemResultado_Responsavel ;
      private int A890ContagemResultado_Responsavel ;
      private int A456ContagemResultado_Codigo ;
      private int A602ContagemResultado_OSVinculada ;
      private int A499ContagemResultado_ContratadaPessoaCod ;
      private int AV9Amarelo ;
      private int AV32ContagemResultado_Servico ;
      private int subGridvermelho_Rows ;
      private int AV7Vermelho ;
      private int subGridlaranja_Rows ;
      private int AV8Laranja ;
      private int AV22Codigo ;
      private int AV25Codigo1 ;
      private int AV23Codigo2 ;
      private int gxdynajaxindex ;
      private int subGridresumo_Islastpage ;
      private int subGridlaranja_Islastpage ;
      private int subGridvermelho_Islastpage ;
      private int edtavNome_Enabled ;
      private int edtavQuantidade_Enabled ;
      private int edtavAtraso_Enabled ;
      private int edtavGlosa_Enabled ;
      private int edtavCodigo1_Enabled ;
      private int edtavVcontagemresultado_dataentrega_Enabled ;
      private int edtavVareatrabalho_descricao_Enabled ;
      private int edtavVcontagemresultado_osfsosfm_Enabled ;
      private int edtavVservico_Enabled ;
      private int edtavVresponsavelnome_Enabled ;
      private int edtavVatraso_Enabled ;
      private int edtavVglosa_Enabled ;
      private int edtavCodigo2_Enabled ;
      private int edtavLareatrabalho_descricao_Enabled ;
      private int edtavLcontagemresultado_osfsosfm_Enabled ;
      private int edtavLservico_Enabled ;
      private int edtavLresponsavelnome_Enabled ;
      private int GRIDRESUMO_nGridOutOfScope ;
      private int GRIDLARANJA_nGridOutOfScope ;
      private int GRIDVERMELHO_nGridOutOfScope ;
      private int A1078ContratoGestor_ContratoCod ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int A1136ContratoGestor_ContratadaCod ;
      private int AV5WWPContext_gxTpr_Contratada_codigo ;
      private int AV5WWPContext_gxTpr_Contratada_pessoacod ;
      private int A601ContagemResultado_Servico ;
      private int A1312ContagemResultado_ResponsavelPessCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A1229ContagemResultado_ContratadaDoResponsavel ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int GXt_int1 ;
      private int A1Usuario_Codigo ;
      private int A57Usuario_PessoaCod ;
      private int AV41Contratada ;
      private int subGridlaranja_Titlebackcolor ;
      private int subGridlaranja_Allbackcolor ;
      private int subGridlaranja_Selectioncolor ;
      private int subGridlaranja_Hoveringcolor ;
      private int subGridvermelho_Titlebackcolor ;
      private int subGridvermelho_Allbackcolor ;
      private int subGridvermelho_Selectioncolor ;
      private int subGridvermelho_Hoveringcolor ;
      private int subGridresumo_Titlebackcolor ;
      private int subGridresumo_Allbackcolor ;
      private int subGridresumo_Selectioncolor ;
      private int subGridresumo_Hoveringcolor ;
      private int idxLst ;
      private int subGridresumo_Backcolor ;
      private int subGridvermelho_Backcolor ;
      private int edtavCodigo1_Visible ;
      private int edtavVcontagemresultado_dataentrega_Visible ;
      private int edtavVareatrabalho_descricao_Visible ;
      private int edtavVcontagemresultado_osfsosfm_Visible ;
      private int edtavVservico_Visible ;
      private int edtavVresponsavelnome_Visible ;
      private int edtavVatraso_Visible ;
      private int edtavVglosa_Visible ;
      private int edtavAtribuir1_Enabled ;
      private int edtavAtribuir1_Visible ;
      private int edtavHistorico1_Enabled ;
      private int edtavHistorico1_Visible ;
      private int subGridlaranja_Backcolor ;
      private int edtavCodigo2_Visible ;
      private int edtavLareatrabalho_descricao_Visible ;
      private int edtavLcontagemresultado_osfsosfm_Visible ;
      private int edtavLservico_Visible ;
      private int edtavLresponsavelnome_Visible ;
      private int edtavAtribuir2_Enabled ;
      private int edtavAtribuir2_Visible ;
      private int edtavHistorico2_Enabled ;
      private int edtavHistorico2_Visible ;
      private long GRIDRESUMO_nFirstRecordOnPage ;
      private long GRIDVERMELHO_nFirstRecordOnPage ;
      private long GRIDLARANJA_nFirstRecordOnPage ;
      private long GRIDRESUMO_nCurrentRecord ;
      private long GRIDVERMELHO_nCurrentRecord ;
      private long GRIDLARANJA_nCurrentRecord ;
      private long GRIDRESUMO_nRecordCount ;
      private long GRIDLARANJA_nRecordCount ;
      private long GRIDVERMELHO_nRecordCount ;
      private decimal AV44Glosa ;
      private decimal AV38VGlosa ;
      private decimal AV43TotalGls ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_39_idx="0001" ;
      private String A484ContagemResultado_StatusDmn ;
      private String A1313ContagemResultado_ResponsavelPessNome ;
      private String AV39Nome ;
      private String edtavNome_Internalname ;
      private String GXKey ;
      private String sGXsfl_53_idx="0001" ;
      private String A801ContagemResultado_ServicoSigla ;
      private String sGXsfl_71_idx="0001" ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_pnlresumo_Width ;
      private String Dvpanel_pnlresumo_Cls ;
      private String Dvpanel_pnlresumo_Title ;
      private String Dvpanel_pnlresumo_Iconposition ;
      private String Dvpanel_pnlvermelho_Width ;
      private String Dvpanel_pnlvermelho_Cls ;
      private String Dvpanel_pnlvermelho_Title ;
      private String Dvpanel_pnlvermelho_Iconposition ;
      private String Dvpanel_pnllaranja_Width ;
      private String Dvpanel_pnllaranja_Cls ;
      private String Dvpanel_pnllaranja_Title ;
      private String Dvpanel_pnllaranja_Iconposition ;
      private String Dvpanel_pnlamarelo_Width ;
      private String Dvpanel_pnlamarelo_Cls ;
      private String Dvpanel_pnlamarelo_Title ;
      private String Dvpanel_pnlamarelo_Iconposition ;
      private String Confirmpanel_Width ;
      private String Confirmpanel_Height ;
      private String Confirmpanel_Title ;
      private String Confirmpanel_Icon ;
      private String Confirmpanel_Confirmtext ;
      private String Confirmpanel_Buttonyestext ;
      private String Confirmpanel_Buttonnotext ;
      private String Confirmpanel_Buttoncanceltext ;
      private String Confirmpanel_Confirmtype ;
      private String Innewwindow_Target ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavQuantidade_Internalname ;
      private String edtavAtraso_Internalname ;
      private String edtavGlosa_Internalname ;
      private String edtavCodigo1_Internalname ;
      private String edtavVcontagemresultado_dataentrega_Internalname ;
      private String edtavVareatrabalho_descricao_Internalname ;
      private String edtavVcontagemresultado_osfsosfm_Internalname ;
      private String AV35VServico ;
      private String edtavVservico_Internalname ;
      private String AV20VResponsavelNome ;
      private String edtavVresponsavelnome_Internalname ;
      private String chkavSelected1_Internalname ;
      private String edtavVatraso_Internalname ;
      private String edtavVglosa_Internalname ;
      private String edtavAtribuir1_Internalname ;
      private String edtavHistorico1_Internalname ;
      private String edtavCodigo2_Internalname ;
      private String edtavLareatrabalho_descricao_Internalname ;
      private String edtavLcontagemresultado_osfsosfm_Internalname ;
      private String AV34LServico ;
      private String edtavLservico_Internalname ;
      private String AV19LResponsavelNome ;
      private String edtavLresponsavelnome_Internalname ;
      private String chkavSelected2_Internalname ;
      private String edtavAtribuir2_Internalname ;
      private String edtavHistorico2_Internalname ;
      private String GXCCtl ;
      private String edtavContagemresultado_demanda_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String dynavContagemresultado_servico_Internalname ;
      private String cmbavContagemresultado_responsavel_Internalname ;
      private String Dvpanel_pnlamarelo_Internalname ;
      private String Innewwindow_Internalname ;
      private String A58Usuario_PessoaNom ;
      private String A2Usuario_Nome ;
      private String edtavAtribuir2_Tooltiptext ;
      private String edtavHistorico2_Tooltiptext ;
      private String Dvpanel_pnllaranja_Internalname ;
      private String edtavAtribuir1_Tooltiptext ;
      private String edtavHistorico1_Tooltiptext ;
      private String Dvpanel_pnlvermelho_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblUsrtable_Internalname ;
      private String tblTablecontent_Internalname ;
      private String tblUnnamedtable2_Internalname ;
      private String tblPnlamarelo_Internalname ;
      private String tblPnllaranja_Internalname ;
      private String subGridlaranja_Internalname ;
      private String subGridlaranja_Class ;
      private String subGridlaranja_Linesclass ;
      private String tblPnlvermelho_Internalname ;
      private String subGridvermelho_Internalname ;
      private String subGridvermelho_Class ;
      private String subGridvermelho_Linesclass ;
      private String tblPnlresumo_Internalname ;
      private String subGridresumo_Internalname ;
      private String subGridresumo_Class ;
      private String subGridresumo_Linesclass ;
      private String tblTablefilters_Internalname ;
      private String TempTags ;
      private String imgLimparfiltros_Internalname ;
      private String imgLimparfiltros_Jsonclick ;
      private String edtavContagemresultado_demanda_Jsonclick ;
      private String dynavContagemresultado_servico_Jsonclick ;
      private String cmbavContagemresultado_responsavel_Jsonclick ;
      private String bttBtnatualizar_Internalname ;
      private String bttBtnatualizar_Jsonclick ;
      private String tblUnnamedtable1_Internalname ;
      private String lblTextblocktitle_Internalname ;
      private String lblTextblocktitle_Jsonclick ;
      private String sGXsfl_39_fel_idx="0001" ;
      private String ROClassString ;
      private String edtavNome_Jsonclick ;
      private String edtavQuantidade_Jsonclick ;
      private String edtavAtraso_Jsonclick ;
      private String edtavGlosa_Jsonclick ;
      private String sGXsfl_53_fel_idx="0001" ;
      private String edtavCodigo1_Jsonclick ;
      private String edtavVcontagemresultado_dataentrega_Jsonclick ;
      private String edtavVareatrabalho_descricao_Jsonclick ;
      private String edtavVcontagemresultado_osfsosfm_Jsonclick ;
      private String edtavVservico_Jsonclick ;
      private String edtavVresponsavelnome_Jsonclick ;
      private String edtavVatraso_Jsonclick ;
      private String edtavVglosa_Jsonclick ;
      private String edtavAtribuir1_Jsonclick ;
      private String edtavHistorico1_Jsonclick ;
      private String sGXsfl_71_fel_idx="0001" ;
      private String edtavCodigo2_Jsonclick ;
      private String edtavLareatrabalho_descricao_Jsonclick ;
      private String edtavLcontagemresultado_osfsosfm_Jsonclick ;
      private String edtavLservico_Jsonclick ;
      private String edtavLresponsavelnome_Jsonclick ;
      private String edtavAtribuir2_Jsonclick ;
      private String edtavHistorico2_Jsonclick ;
      private String Dvpanel_pnlresumo_Internalname ;
      private String Confirmpanel_Internalname ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private DateTime AV10ServerDate ;
      private DateTime AV16VContagemResultado_DataEntrega ;
      private bool entryPointCalled ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n890ContagemResultado_Responsavel ;
      private bool n531ContagemResultado_StatusUltCnt ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n499ContagemResultado_ContratadaPessoaCod ;
      private bool n1313ContagemResultado_ResponsavelPessNome ;
      private bool n53Contratada_AreaTrabalhoDes ;
      private bool n801ContagemResultado_ServicoSigla ;
      private bool toggleJsOutput ;
      private bool AV31PodeAtribuirDmn ;
      private bool Dvpanel_pnlresumo_Collapsible ;
      private bool Dvpanel_pnlresumo_Collapsed ;
      private bool Dvpanel_pnlresumo_Autowidth ;
      private bool Dvpanel_pnlresumo_Autoheight ;
      private bool Dvpanel_pnlresumo_Showcollapseicon ;
      private bool Dvpanel_pnlresumo_Autoscroll ;
      private bool Dvpanel_pnlvermelho_Collapsible ;
      private bool Dvpanel_pnlvermelho_Collapsed ;
      private bool Dvpanel_pnlvermelho_Autowidth ;
      private bool Dvpanel_pnlvermelho_Autoheight ;
      private bool Dvpanel_pnlvermelho_Showcollapseicon ;
      private bool Dvpanel_pnlvermelho_Autoscroll ;
      private bool Dvpanel_pnllaranja_Collapsible ;
      private bool Dvpanel_pnllaranja_Collapsed ;
      private bool Dvpanel_pnllaranja_Autowidth ;
      private bool Dvpanel_pnllaranja_Autoheight ;
      private bool Dvpanel_pnllaranja_Showcollapseicon ;
      private bool Dvpanel_pnllaranja_Autoscroll ;
      private bool Dvpanel_pnlamarelo_Collapsible ;
      private bool Dvpanel_pnlamarelo_Collapsed ;
      private bool Dvpanel_pnlamarelo_Autowidth ;
      private bool Dvpanel_pnlamarelo_Autoheight ;
      private bool Dvpanel_pnlamarelo_Showcollapseicon ;
      private bool Dvpanel_pnlamarelo_Autoscroll ;
      private bool Confirmpanel_Draggeable ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV26Selected1 ;
      private bool AV24Selected2 ;
      private bool returnInSub ;
      private bool n1136ContratoGestor_ContratadaCod ;
      private bool BRKL48 ;
      private bool n1312ContagemResultado_ResponsavelPessCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1237ContagemResultado_PrazoMaisDias ;
      private bool n1227ContagemResultado_PrazoInicialDias ;
      private bool n601ContagemResultado_Servico ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n457ContagemResultado_Demanda ;
      private bool gx_refresh_fired ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n58Usuario_PessoaNom ;
      private bool n2Usuario_Nome ;
      private bool AV29Atribuir1_IsBlob ;
      private bool AV30Historico1_IsBlob ;
      private bool AV27Atribuir2_IsBlob ;
      private bool AV28Historico2_IsBlob ;
      private String AV33ContagemResultado_Demanda ;
      private String A53Contratada_AreaTrabalhoDes ;
      private String A501ContagemResultado_OsFsOsFm ;
      private String AV14VAreaTrabalho_Descricao ;
      private String AV15VContagemResultado_OsFsOsFm ;
      private String AV60Atribuir1_GXI ;
      private String AV61Historico1_GXI ;
      private String AV11LAreaTrabalho_Descricao ;
      private String AV12LContagemResultado_OsFsOsFm ;
      private String AV57Atribuir2_GXI ;
      private String AV58Historico2_GXI ;
      private String lV33ContagemResultado_Demanda ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String AV29Atribuir1 ;
      private String AV30Historico1 ;
      private String AV27Atribuir2 ;
      private String AV28Historico2 ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GridresumoContainer ;
      private GXWebGrid GridlaranjaContainer ;
      private GXWebGrid GridvermelhoContainer ;
      private GXWebRow GridresumoRow ;
      private GXWebRow GridlaranjaRow ;
      private GXWebRow GridvermelhoRow ;
      private GXWebColumn GridlaranjaColumn ;
      private GXWebColumn GridvermelhoColumn ;
      private GXWebColumn GridresumoColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavContagemresultado_servico ;
      private GXCombobox cmbavContagemresultado_responsavel ;
      private GXCheckbox chkavSelected1 ;
      private GXCheckbox chkavSelected2 ;
      private IDataStoreProvider pr_default ;
      private int[] H00L42_A155Servico_Codigo ;
      private String[] H00L42_A605Servico_Sigla ;
      private int[] H00L43_A1078ContratoGestor_ContratoCod ;
      private int[] H00L43_A1079ContratoGestor_UsuarioCod ;
      private int[] H00L43_A1136ContratoGestor_ContratadaCod ;
      private bool[] H00L43_n1136ContratoGestor_ContratadaCod ;
      private int[] H00L45_A1312ContagemResultado_ResponsavelPessCod ;
      private bool[] H00L45_n1312ContagemResultado_ResponsavelPessCod ;
      private int[] H00L45_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00L45_n1553ContagemResultado_CntSrvCod ;
      private String[] H00L45_A1313ContagemResultado_ResponsavelPessNome ;
      private bool[] H00L45_n1313ContagemResultado_ResponsavelPessNome ;
      private short[] H00L45_A1237ContagemResultado_PrazoMaisDias ;
      private bool[] H00L45_n1237ContagemResultado_PrazoMaisDias ;
      private short[] H00L45_A1227ContagemResultado_PrazoInicialDias ;
      private bool[] H00L45_n1227ContagemResultado_PrazoInicialDias ;
      private int[] H00L45_A601ContagemResultado_Servico ;
      private bool[] H00L45_n601ContagemResultado_Servico ;
      private int[] H00L45_A890ContagemResultado_Responsavel ;
      private bool[] H00L45_n890ContagemResultado_Responsavel ;
      private String[] H00L45_A493ContagemResultado_DemandaFM ;
      private bool[] H00L45_n493ContagemResultado_DemandaFM ;
      private String[] H00L45_A457ContagemResultado_Demanda ;
      private bool[] H00L45_n457ContagemResultado_Demanda ;
      private int[] H00L45_A490ContagemResultado_ContratadaCod ;
      private bool[] H00L45_n490ContagemResultado_ContratadaCod ;
      private String[] H00L45_A484ContagemResultado_StatusDmn ;
      private bool[] H00L45_n484ContagemResultado_StatusDmn ;
      private DateTime[] H00L45_A472ContagemResultado_DataEntrega ;
      private bool[] H00L45_n472ContagemResultado_DataEntrega ;
      private int[] H00L45_A499ContagemResultado_ContratadaPessoaCod ;
      private bool[] H00L45_n499ContagemResultado_ContratadaPessoaCod ;
      private int[] H00L45_A602ContagemResultado_OSVinculada ;
      private bool[] H00L45_n602ContagemResultado_OSVinculada ;
      private int[] H00L45_A456ContagemResultado_Codigo ;
      private short[] H00L45_A531ContagemResultado_StatusUltCnt ;
      private bool[] H00L45_n531ContagemResultado_StatusUltCnt ;
      private int[] H00L47_A499ContagemResultado_ContratadaPessoaCod ;
      private bool[] H00L47_n499ContagemResultado_ContratadaPessoaCod ;
      private int[] H00L47_A602ContagemResultado_OSVinculada ;
      private bool[] H00L47_n602ContagemResultado_OSVinculada ;
      private int[] H00L47_A456ContagemResultado_Codigo ;
      private int[] H00L47_A890ContagemResultado_Responsavel ;
      private bool[] H00L47_n890ContagemResultado_Responsavel ;
      private int[] H00L47_A490ContagemResultado_ContratadaCod ;
      private bool[] H00L47_n490ContagemResultado_ContratadaCod ;
      private String[] H00L47_A484ContagemResultado_StatusDmn ;
      private bool[] H00L47_n484ContagemResultado_StatusDmn ;
      private DateTime[] H00L47_A472ContagemResultado_DataEntrega ;
      private bool[] H00L47_n472ContagemResultado_DataEntrega ;
      private short[] H00L47_A531ContagemResultado_StatusUltCnt ;
      private bool[] H00L47_n531ContagemResultado_StatusUltCnt ;
      private int[] H00L49_A499ContagemResultado_ContratadaPessoaCod ;
      private bool[] H00L49_n499ContagemResultado_ContratadaPessoaCod ;
      private int[] H00L49_A602ContagemResultado_OSVinculada ;
      private bool[] H00L49_n602ContagemResultado_OSVinculada ;
      private int[] H00L49_A456ContagemResultado_Codigo ;
      private int[] H00L49_A490ContagemResultado_ContratadaCod ;
      private bool[] H00L49_n490ContagemResultado_ContratadaCod ;
      private String[] H00L49_A484ContagemResultado_StatusDmn ;
      private bool[] H00L49_n484ContagemResultado_StatusDmn ;
      private DateTime[] H00L49_A472ContagemResultado_DataEntrega ;
      private bool[] H00L49_n472ContagemResultado_DataEntrega ;
      private short[] H00L49_A531ContagemResultado_StatusUltCnt ;
      private bool[] H00L49_n531ContagemResultado_StatusUltCnt ;
      private int[] H00L49_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00L49_n52Contratada_AreaTrabalhoCod ;
      private int[] H00L49_A890ContagemResultado_Responsavel ;
      private bool[] H00L49_n890ContagemResultado_Responsavel ;
      private int[] H00L410_A57Usuario_PessoaCod ;
      private int[] H00L410_A1Usuario_Codigo ;
      private String[] H00L410_A58Usuario_PessoaNom ;
      private bool[] H00L410_n58Usuario_PessoaNom ;
      private String[] H00L410_A2Usuario_Nome ;
      private bool[] H00L410_n2Usuario_Nome ;
      private int[] H00L412_A1312ContagemResultado_ResponsavelPessCod ;
      private bool[] H00L412_n1312ContagemResultado_ResponsavelPessCod ;
      private int[] H00L412_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00L412_n52Contratada_AreaTrabalhoCod ;
      private int[] H00L412_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00L412_n1553ContagemResultado_CntSrvCod ;
      private DateTime[] H00L412_A472ContagemResultado_DataEntrega ;
      private bool[] H00L412_n472ContagemResultado_DataEntrega ;
      private short[] H00L412_A1237ContagemResultado_PrazoMaisDias ;
      private bool[] H00L412_n1237ContagemResultado_PrazoMaisDias ;
      private short[] H00L412_A1227ContagemResultado_PrazoInicialDias ;
      private bool[] H00L412_n1227ContagemResultado_PrazoInicialDias ;
      private int[] H00L412_A601ContagemResultado_Servico ;
      private bool[] H00L412_n601ContagemResultado_Servico ;
      private int[] H00L412_A499ContagemResultado_ContratadaPessoaCod ;
      private bool[] H00L412_n499ContagemResultado_ContratadaPessoaCod ;
      private int[] H00L412_A602ContagemResultado_OSVinculada ;
      private bool[] H00L412_n602ContagemResultado_OSVinculada ;
      private int[] H00L412_A456ContagemResultado_Codigo ;
      private int[] H00L412_A890ContagemResultado_Responsavel ;
      private bool[] H00L412_n890ContagemResultado_Responsavel ;
      private int[] H00L412_A490ContagemResultado_ContratadaCod ;
      private bool[] H00L412_n490ContagemResultado_ContratadaCod ;
      private String[] H00L412_A484ContagemResultado_StatusDmn ;
      private bool[] H00L412_n484ContagemResultado_StatusDmn ;
      private String[] H00L412_A53Contratada_AreaTrabalhoDes ;
      private bool[] H00L412_n53Contratada_AreaTrabalhoDes ;
      private String[] H00L412_A801ContagemResultado_ServicoSigla ;
      private bool[] H00L412_n801ContagemResultado_ServicoSigla ;
      private String[] H00L412_A1313ContagemResultado_ResponsavelPessNome ;
      private bool[] H00L412_n1313ContagemResultado_ResponsavelPessNome ;
      private short[] H00L412_A531ContagemResultado_StatusUltCnt ;
      private bool[] H00L412_n531ContagemResultado_StatusUltCnt ;
      private String[] H00L412_A493ContagemResultado_DemandaFM ;
      private bool[] H00L412_n493ContagemResultado_DemandaFM ;
      private String[] H00L412_A457ContagemResultado_Demanda ;
      private bool[] H00L412_n457ContagemResultado_Demanda ;
      private int[] H00L414_A1312ContagemResultado_ResponsavelPessCod ;
      private bool[] H00L414_n1312ContagemResultado_ResponsavelPessCod ;
      private int[] H00L414_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00L414_n52Contratada_AreaTrabalhoCod ;
      private int[] H00L414_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00L414_n1553ContagemResultado_CntSrvCod ;
      private short[] H00L414_A1237ContagemResultado_PrazoMaisDias ;
      private bool[] H00L414_n1237ContagemResultado_PrazoMaisDias ;
      private short[] H00L414_A1227ContagemResultado_PrazoInicialDias ;
      private bool[] H00L414_n1227ContagemResultado_PrazoInicialDias ;
      private int[] H00L414_A601ContagemResultado_Servico ;
      private bool[] H00L414_n601ContagemResultado_Servico ;
      private int[] H00L414_A499ContagemResultado_ContratadaPessoaCod ;
      private bool[] H00L414_n499ContagemResultado_ContratadaPessoaCod ;
      private int[] H00L414_A602ContagemResultado_OSVinculada ;
      private bool[] H00L414_n602ContagemResultado_OSVinculada ;
      private int[] H00L414_A456ContagemResultado_Codigo ;
      private int[] H00L414_A890ContagemResultado_Responsavel ;
      private bool[] H00L414_n890ContagemResultado_Responsavel ;
      private int[] H00L414_A490ContagemResultado_ContratadaCod ;
      private bool[] H00L414_n490ContagemResultado_ContratadaCod ;
      private String[] H00L414_A484ContagemResultado_StatusDmn ;
      private bool[] H00L414_n484ContagemResultado_StatusDmn ;
      private DateTime[] H00L414_A472ContagemResultado_DataEntrega ;
      private bool[] H00L414_n472ContagemResultado_DataEntrega ;
      private String[] H00L414_A53Contratada_AreaTrabalhoDes ;
      private bool[] H00L414_n53Contratada_AreaTrabalhoDes ;
      private String[] H00L414_A801ContagemResultado_ServicoSigla ;
      private bool[] H00L414_n801ContagemResultado_ServicoSigla ;
      private String[] H00L414_A1313ContagemResultado_ResponsavelPessNome ;
      private bool[] H00L414_n1313ContagemResultado_ResponsavelPessNome ;
      private short[] H00L414_A531ContagemResultado_StatusUltCnt ;
      private bool[] H00L414_n531ContagemResultado_StatusUltCnt ;
      private String[] H00L414_A493ContagemResultado_DemandaFM ;
      private bool[] H00L414_n493ContagemResultado_DemandaFM ;
      private String[] H00L414_A457ContagemResultado_Demanda ;
      private bool[] H00L414_n457ContagemResultado_Demanda ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private IGxSession AV49WebSession ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV6Contratadas ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV21Responsaveis ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV53Selecionadas2 ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV50Selecionadas1 ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV5WWPContext ;
   }

   public class wp_gestao__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00L45( IGxContext context ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             IGxCollection AV6Contratadas ,
                                             int A890ContagemResultado_Responsavel ,
                                             IGxCollection AV21Responsaveis ,
                                             int AV36ContagemResultado_Responsavel ,
                                             int AV32ContagemResultado_Servico ,
                                             String AV33ContagemResultado_Demanda ,
                                             int A601ContagemResultado_Servico ,
                                             String A457ContagemResultado_Demanda ,
                                             String A493ContagemResultado_DemandaFM ,
                                             DateTime A472ContagemResultado_DataEntrega ,
                                             DateTime AV10ServerDate ,
                                             String A484ContagemResultado_StatusDmn ,
                                             int AV5WWPContext_gxTpr_Contratada_codigo ,
                                             short A531ContagemResultado_StatusUltCnt ,
                                             int A456ContagemResultado_Codigo ,
                                             int A602ContagemResultado_OSVinculada ,
                                             int A499ContagemResultado_ContratadaPessoaCod ,
                                             int AV5WWPContext_gxTpr_Contratada_pessoacod ,
                                             short A1227ContagemResultado_PrazoInicialDias ,
                                             short A1237ContagemResultado_PrazoMaisDias )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [5] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         scmdbuf = "SELECT T3.[Usuario_PessoaCod] AS ContagemResultado_ResponsavelPessCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T4.[Pessoa_Nome] AS ContagemResultado_ResponsavelPessNome, T1.[ContagemResultado_PrazoMaisDias], T1.[ContagemResultado_PrazoInicialDias], T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_Responsavel] AS ContagemResultado_Responsavel, T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCo, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DataEntrega], T5.[Contratada_PessoaCod] AS ContagemResultado_ContratadaPessoaCod, T1.[ContagemResultado_OSVinculada], T1.[ContagemResultado_Codigo], COALESCE( T6.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt FROM ((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[ContagemResultado_Responsavel]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) LEFT JOIN [Contratada] T5 WITH (NOLOCK) ON T5.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN (SELECT MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T6 ON T6.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContagemResultado_DataEntrega] < @AV10ServerDate)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_StatusDmn] = 'E' or T1.[ContagemResultado_StatusDmn] = 'A' or T1.[ContagemResultado_StatusDmn] = 'D')";
         scmdbuf = scmdbuf + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV6Contratadas, "T1.[ContagemResultado_ContratadaCod] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_PrazoInicialDias] + T1.[ContagemResultado_PrazoMaisDias] > 0)";
         if ( (0==AV36ContagemResultado_Responsavel) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Responsavel] IS NULL or (T1.[ContagemResultado_Responsavel] = convert(int, 0)) or " + new GxDbmsUtils( new GxSqlServer()).ValueList(AV21Responsaveis, "T1.[ContagemResultado_Responsavel] IN (", ")") + ")";
         }
         if ( ! (0==AV32ContagemResultado_Servico) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV32ContagemResultado_Servico)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ! (0==AV36ContagemResultado_Responsavel) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Responsavel] = @AV36ContagemResultado_Responsavel)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33ContagemResultado_Demanda)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV33ContagemResultado_Demanda + '%' or T1.[ContagemResultado_DemandaFM] like @lV33ContagemResultado_Demanda + '%')";
         }
         else
         {
            GXv_int2[3] = 1;
            GXv_int2[4] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T4.[Pessoa_Nome], T1.[ContagemResultado_DataEntrega], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_ContratadaCod]";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00L47( IGxContext context ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             IGxCollection AV6Contratadas ,
                                             int A890ContagemResultado_Responsavel ,
                                             IGxCollection AV21Responsaveis ,
                                             int AV36ContagemResultado_Responsavel ,
                                             DateTime A472ContagemResultado_DataEntrega ,
                                             DateTime AV10ServerDate ,
                                             String A484ContagemResultado_StatusDmn ,
                                             int AV5WWPContext_gxTpr_Contratada_codigo ,
                                             short A531ContagemResultado_StatusUltCnt ,
                                             int A456ContagemResultado_Codigo ,
                                             int A602ContagemResultado_OSVinculada ,
                                             int A499ContagemResultado_ContratadaPessoaCod ,
                                             int AV5WWPContext_gxTpr_Contratada_pessoacod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [3] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT T3.[Contratada_PessoaCod] AS ContagemResultado_ContratadaPessoaCod, T1.[ContagemResultado_OSVinculada], T1.[ContagemResultado_Codigo], T1.[ContagemResultado_Responsavel] AS ContagemResultado_Responsavel, T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCo, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DataEntrega], COALESCE( T2.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T2 ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContagemResultado_DataEntrega] > @AV10ServerDate)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_DataEntrega] > @AV10ServerDate)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_DataEntrega] < DATEADD( ss,86400 * ( 7), @AV10ServerDate))";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_StatusDmn] = 'E' or T1.[ContagemResultado_StatusDmn] = 'A' or T1.[ContagemResultado_StatusDmn] = 'D')";
         scmdbuf = scmdbuf + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV6Contratadas, "T1.[ContagemResultado_ContratadaCod] IN (", ")") + ")";
         if ( (0==AV36ContagemResultado_Responsavel) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Responsavel] IS NULL or (T1.[ContagemResultado_Responsavel] = convert(int, 0)) or " + new GxDbmsUtils( new GxSqlServer()).ValueList(AV21Responsaveis, "T1.[ContagemResultado_Responsavel] IN (", ")") + ")";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_DataEntrega], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_ContratadaCod]";
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      protected Object[] conditional_H00L49( IGxContext context ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             IGxCollection AV6Contratadas ,
                                             int A1229ContagemResultado_ContratadaDoResponsavel ,
                                             String A484ContagemResultado_StatusDmn ,
                                             int AV5WWPContext_gxTpr_Contratada_codigo ,
                                             short A531ContagemResultado_StatusUltCnt ,
                                             int A456ContagemResultado_Codigo ,
                                             int A602ContagemResultado_OSVinculada ,
                                             int A499ContagemResultado_ContratadaPessoaCod ,
                                             int AV5WWPContext_gxTpr_Contratada_pessoacod ,
                                             DateTime A472ContagemResultado_DataEntrega ,
                                             DateTime AV10ServerDate )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int6 ;
         GXv_int6 = new short [1] ;
         Object[] GXv_Object7 ;
         GXv_Object7 = new Object [2] ;
         scmdbuf = "SELECT T3.[Contratada_PessoaCod] AS ContagemResultado_ContratadaPessoaCod, T1.[ContagemResultado_OSVinculada], T1.[ContagemResultado_Codigo], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCo, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DataEntrega], COALESCE( T2.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt, T3.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T1.[ContagemResultado_Responsavel] AS ContagemResultado_Responsavel FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T2 ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContagemResultado_StatusDmn] = 'S' or T1.[ContagemResultado_StatusDmn] = 'E' or T1.[ContagemResultado_StatusDmn] = 'A' or T1.[ContagemResultado_StatusDmn] = 'D')";
         scmdbuf = scmdbuf + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV6Contratadas, "T1.[ContagemResultado_ContratadaCod] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_DataEntrega] <= @AV10ServerDate)";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_DataEntrega], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_ContratadaCod], T1.[ContagemResultado_Responsavel]";
         GXv_Object7[0] = scmdbuf;
         GXv_Object7[1] = GXv_int6;
         return GXv_Object7 ;
      }

      protected Object[] conditional_H00L410( IGxContext context ,
                                              int A1Usuario_Codigo ,
                                              IGxCollection AV21Responsaveis )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object8 ;
         GXv_Object8 = new Object [2] ;
         scmdbuf = "SELECT T1.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Usuario_Codigo], T2.[Pessoa_Nome] AS Usuario_PessoaNom, T1.[Usuario_Nome] FROM ([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV21Responsaveis, "T1.[Usuario_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Usuario_Nome]";
         GXv_Object8[0] = scmdbuf;
         return GXv_Object8 ;
      }

      protected Object[] conditional_H00L412( IGxContext context ,
                                              int A490ContagemResultado_ContratadaCod ,
                                              IGxCollection AV6Contratadas ,
                                              int A890ContagemResultado_Responsavel ,
                                              IGxCollection AV21Responsaveis ,
                                              int AV36ContagemResultado_Responsavel ,
                                              int AV32ContagemResultado_Servico ,
                                              String AV33ContagemResultado_Demanda ,
                                              int A601ContagemResultado_Servico ,
                                              String A457ContagemResultado_Demanda ,
                                              String A493ContagemResultado_DemandaFM ,
                                              String A484ContagemResultado_StatusDmn ,
                                              int AV5WWPContext_gxTpr_Contratada_codigo ,
                                              short A531ContagemResultado_StatusUltCnt ,
                                              int A456ContagemResultado_Codigo ,
                                              int A602ContagemResultado_OSVinculada ,
                                              int A499ContagemResultado_ContratadaPessoaCod ,
                                              int AV5WWPContext_gxTpr_Contratada_pessoacod ,
                                              short A1227ContagemResultado_PrazoInicialDias ,
                                              short A1237ContagemResultado_PrazoMaisDias ,
                                              DateTime AV10ServerDate ,
                                              DateTime A472ContagemResultado_DataEntrega )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int10 ;
         GXv_int10 = new short [5] ;
         Object[] GXv_Object11 ;
         GXv_Object11 = new Object [2] ;
         scmdbuf = "SELECT T5.[Usuario_PessoaCod] AS ContagemResultado_ResponsavelPessCod, T7.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_DataEntrega], T1.[ContagemResultado_PrazoMaisDias], T1.[ContagemResultado_PrazoInicialDias], T2.[Servico_Codigo] AS ContagemResultado_Servico, T7.[Contratada_PessoaCod] AS ContagemResultado_ContratadaPessoaCod, T1.[ContagemResultado_OSVinculada], T1.[ContagemResultado_Codigo], T1.[ContagemResultado_Responsavel] AS ContagemResultado_Responsavel, T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCo, T1.[ContagemResultado_StatusDmn], T8.[AreaTrabalho_Descricao] AS Contratada_AreaTrabalhoDes, T3.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T6.[Pessoa_Nome] AS ContagemResultado_ResponsavelPessNome, COALESCE( T4.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt, T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Demanda] FROM ((((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN (SELECT MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T4 ON T4.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Usuario] T5 WITH (NOLOCK) ON T5.[Usuario_Codigo] = T1.[ContagemResultado_Responsavel]) LEFT JOIN [Pessoa] T6 WITH (NOLOCK) ON T6.[Pessoa_Codigo] = T5.[Usuario_PessoaCod]) LEFT JOIN [Contratada] T7 WITH (NOLOCK) ON T7.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod])";
         scmdbuf = scmdbuf + " LEFT JOIN [AreaTrabalho] T8 WITH (NOLOCK) ON T8.[AreaTrabalho_Codigo] = T7.[Contratada_AreaTrabalhoCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContagemResultado_DataEntrega] = @AV10ServerDate)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_StatusDmn] = 'S' or T1.[ContagemResultado_StatusDmn] = 'E' or T1.[ContagemResultado_StatusDmn] = 'A' or T1.[ContagemResultado_StatusDmn] = 'D')";
         scmdbuf = scmdbuf + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV6Contratadas, "T1.[ContagemResultado_ContratadaCod] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_PrazoInicialDias] + T1.[ContagemResultado_PrazoMaisDias] > 0)";
         if ( (0==AV36ContagemResultado_Responsavel) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Responsavel] IS NULL or (T1.[ContagemResultado_Responsavel] = convert(int, 0)) or " + new GxDbmsUtils( new GxSqlServer()).ValueList(AV21Responsaveis, "T1.[ContagemResultado_Responsavel] IN (", ")") + ")";
         }
         if ( ! (0==AV32ContagemResultado_Servico) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV32ContagemResultado_Servico)";
         }
         else
         {
            GXv_int10[1] = 1;
         }
         if ( ! (0==AV36ContagemResultado_Responsavel) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Responsavel] = @AV36ContagemResultado_Responsavel)";
         }
         else
         {
            GXv_int10[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33ContagemResultado_Demanda)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV33ContagemResultado_Demanda + '%' or T1.[ContagemResultado_DemandaFM] like @lV33ContagemResultado_Demanda + '%')";
         }
         else
         {
            GXv_int10[3] = 1;
            GXv_int10[4] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_DataEntrega], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_ContratadaCod]";
         GXv_Object11[0] = scmdbuf;
         GXv_Object11[1] = GXv_int10;
         return GXv_Object11 ;
      }

      protected Object[] conditional_H00L414( IGxContext context ,
                                              int A490ContagemResultado_ContratadaCod ,
                                              IGxCollection AV6Contratadas ,
                                              int A890ContagemResultado_Responsavel ,
                                              IGxCollection AV21Responsaveis ,
                                              int AV36ContagemResultado_Responsavel ,
                                              int AV32ContagemResultado_Servico ,
                                              String AV33ContagemResultado_Demanda ,
                                              int A601ContagemResultado_Servico ,
                                              String A457ContagemResultado_Demanda ,
                                              String A493ContagemResultado_DemandaFM ,
                                              String A484ContagemResultado_StatusDmn ,
                                              int AV5WWPContext_gxTpr_Contratada_codigo ,
                                              short A531ContagemResultado_StatusUltCnt ,
                                              int A456ContagemResultado_Codigo ,
                                              int A602ContagemResultado_OSVinculada ,
                                              int A499ContagemResultado_ContratadaPessoaCod ,
                                              int AV5WWPContext_gxTpr_Contratada_pessoacod ,
                                              short A1227ContagemResultado_PrazoInicialDias ,
                                              short A1237ContagemResultado_PrazoMaisDias ,
                                              DateTime A472ContagemResultado_DataEntrega ,
                                              DateTime AV10ServerDate )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int12 ;
         GXv_int12 = new short [5] ;
         Object[] GXv_Object13 ;
         GXv_Object13 = new Object [2] ;
         scmdbuf = "SELECT T5.[Usuario_PessoaCod] AS ContagemResultado_ResponsavelPessCod, T7.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_PrazoMaisDias], T1.[ContagemResultado_PrazoInicialDias], T2.[Servico_Codigo] AS ContagemResultado_Servico, T7.[Contratada_PessoaCod] AS ContagemResultado_ContratadaPessoaCod, T1.[ContagemResultado_OSVinculada], T1.[ContagemResultado_Codigo], T1.[ContagemResultado_Responsavel] AS ContagemResultado_Responsavel, T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCo, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DataEntrega], T8.[AreaTrabalho_Descricao] AS Contratada_AreaTrabalhoDes, T3.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T6.[Pessoa_Nome] AS ContagemResultado_ResponsavelPessNome, COALESCE( T4.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt, T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Demanda] FROM ((((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN (SELECT MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T4 ON T4.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Usuario] T5 WITH (NOLOCK) ON T5.[Usuario_Codigo] = T1.[ContagemResultado_Responsavel]) LEFT JOIN [Pessoa] T6 WITH (NOLOCK) ON T6.[Pessoa_Codigo] = T5.[Usuario_PessoaCod]) LEFT JOIN [Contratada] T7 WITH (NOLOCK) ON T7.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod])";
         scmdbuf = scmdbuf + " LEFT JOIN [AreaTrabalho] T8 WITH (NOLOCK) ON T8.[AreaTrabalho_Codigo] = T7.[Contratada_AreaTrabalhoCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContagemResultado_StatusDmn] = 'E' or T1.[ContagemResultado_StatusDmn] = 'A' or T1.[ContagemResultado_StatusDmn] = 'D')";
         scmdbuf = scmdbuf + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV6Contratadas, "T1.[ContagemResultado_ContratadaCod] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_PrazoInicialDias] + T1.[ContagemResultado_PrazoMaisDias] > 0)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_DataEntrega] < @AV10ServerDate)";
         if ( (0==AV36ContagemResultado_Responsavel) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Responsavel] IS NULL or (T1.[ContagemResultado_Responsavel] = convert(int, 0)) or " + new GxDbmsUtils( new GxSqlServer()).ValueList(AV21Responsaveis, "T1.[ContagemResultado_Responsavel] IN (", ")") + ")";
         }
         if ( ! (0==AV32ContagemResultado_Servico) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV32ContagemResultado_Servico)";
         }
         else
         {
            GXv_int12[1] = 1;
         }
         if ( ! (0==AV36ContagemResultado_Responsavel) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Responsavel] = @AV36ContagemResultado_Responsavel)";
         }
         else
         {
            GXv_int12[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33ContagemResultado_Demanda)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV33ContagemResultado_Demanda + '%' or T1.[ContagemResultado_DemandaFM] like @lV33ContagemResultado_Demanda + '%')";
         }
         else
         {
            GXv_int12[3] = 1;
            GXv_int12[4] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_DataEntrega], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_ContratadaCod], T1.[ContagemResultado_Responsavel]";
         GXv_Object13[0] = scmdbuf;
         GXv_Object13[1] = GXv_int12;
         return GXv_Object13 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 2 :
                     return conditional_H00L45(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] , (String)dynConstraints[6] , (int)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (DateTime)dynConstraints[10] , (DateTime)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (short)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (short)dynConstraints[19] , (short)dynConstraints[20] );
               case 3 :
                     return conditional_H00L47(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (int)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (short)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] );
               case 4 :
                     return conditional_H00L49(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (String)dynConstraints[3] , (int)dynConstraints[4] , (short)dynConstraints[5] , (int)dynConstraints[6] , (int)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] , (DateTime)dynConstraints[10] , (DateTime)dynConstraints[11] );
               case 5 :
                     return conditional_H00L410(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] );
               case 6 :
                     return conditional_H00L412(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] , (String)dynConstraints[6] , (int)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (short)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (short)dynConstraints[17] , (short)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] );
               case 7 :
                     return conditional_H00L414(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] , (String)dynConstraints[6] , (int)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (short)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (short)dynConstraints[17] , (short)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00L42 ;
          prmH00L42 = new Object[] {
          } ;
          Object[] prmH00L43 ;
          prmH00L43 = new Object[] {
          new Object[] {"@AV5WWPContext__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH00L45 ;
          prmH00L45 = new Object[] {
          new Object[] {"@AV10ServerDate",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV32ContagemResultado_Servico",SqlDbType.Int,6,0} ,
          new Object[] {"@AV36ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@lV33ContagemResultado_Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV33ContagemResultado_Demanda",SqlDbType.VarChar,30,0}
          } ;
          Object[] prmH00L47 ;
          prmH00L47 = new Object[] {
          new Object[] {"@AV10ServerDate",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV10ServerDate",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV10ServerDate",SqlDbType.DateTime,8,0}
          } ;
          Object[] prmH00L49 ;
          prmH00L49 = new Object[] {
          new Object[] {"@AV10ServerDate",SqlDbType.DateTime,8,0}
          } ;
          Object[] prmH00L410 ;
          prmH00L410 = new Object[] {
          } ;
          Object[] prmH00L412 ;
          prmH00L412 = new Object[] {
          new Object[] {"@AV10ServerDate",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV32ContagemResultado_Servico",SqlDbType.Int,6,0} ,
          new Object[] {"@AV36ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@lV33ContagemResultado_Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV33ContagemResultado_Demanda",SqlDbType.VarChar,30,0}
          } ;
          Object[] prmH00L414 ;
          prmH00L414 = new Object[] {
          new Object[] {"@AV10ServerDate",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV32ContagemResultado_Servico",SqlDbType.Int,6,0} ,
          new Object[] {"@AV36ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@lV33ContagemResultado_Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV33ContagemResultado_Demanda",SqlDbType.VarChar,30,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00L42", "SELECT [Servico_Codigo], [Servico_Sigla] FROM [Servico] WITH (NOLOCK) ORDER BY [Servico_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00L42,0,0,true,false )
             ,new CursorDef("H00L43", "SELECT T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T1.[ContratoGestor_UsuarioCod], T2.[Contratada_Codigo] AS ContratoGestor_ContratadaCod FROM ([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) WHERE T1.[ContratoGestor_UsuarioCod] = @AV5WWPContext__Userid ORDER BY T1.[ContratoGestor_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00L43,100,0,false,false )
             ,new CursorDef("H00L45", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00L45,100,0,true,false )
             ,new CursorDef("H00L47", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00L47,100,0,true,false )
             ,new CursorDef("H00L49", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00L49,100,0,true,false )
             ,new CursorDef("H00L410", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00L410,100,0,false,false )
             ,new CursorDef("H00L412", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00L412,100,0,true,false )
             ,new CursorDef("H00L414", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00L414,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((short[]) buf[6])[0] = rslt.getShort(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((short[]) buf[8])[0] = rslt.getShort(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((String[]) buf[14])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((String[]) buf[16])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((int[]) buf[18])[0] = rslt.getInt(10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((String[]) buf[20])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[22])[0] = rslt.getGXDate(12) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(12);
                ((int[]) buf[24])[0] = rslt.getInt(13) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(13);
                ((int[]) buf[26])[0] = rslt.getInt(14) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(14);
                ((int[]) buf[28])[0] = rslt.getInt(15) ;
                ((short[]) buf[29])[0] = rslt.getShort(16) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(16);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[11])[0] = rslt.getGXDate(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((short[]) buf[13])[0] = rslt.getShort(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[9])[0] = rslt.getGXDate(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((short[]) buf[11])[0] = rslt.getShort(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((short[]) buf[8])[0] = rslt.getShort(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((short[]) buf[10])[0] = rslt.getShort(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((int[]) buf[14])[0] = rslt.getInt(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((int[]) buf[16])[0] = rslt.getInt(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((int[]) buf[18])[0] = rslt.getInt(10) ;
                ((int[]) buf[19])[0] = rslt.getInt(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((int[]) buf[21])[0] = rslt.getInt(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((String[]) buf[23])[0] = rslt.getString(13, 1) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(13);
                ((String[]) buf[25])[0] = rslt.getVarchar(14) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(14);
                ((String[]) buf[27])[0] = rslt.getString(15, 15) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(15);
                ((String[]) buf[29])[0] = rslt.getString(16, 100) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(16);
                ((short[]) buf[31])[0] = rslt.getShort(17) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(17);
                ((String[]) buf[33])[0] = rslt.getVarchar(18) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(18);
                ((String[]) buf[35])[0] = rslt.getVarchar(19) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(19);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((short[]) buf[6])[0] = rslt.getShort(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((short[]) buf[8])[0] = rslt.getShort(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((int[]) buf[14])[0] = rslt.getInt(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((int[]) buf[16])[0] = rslt.getInt(9) ;
                ((int[]) buf[17])[0] = rslt.getInt(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((int[]) buf[19])[0] = rslt.getInt(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((String[]) buf[21])[0] = rslt.getString(12, 1) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((DateTime[]) buf[23])[0] = rslt.getGXDate(13) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(13);
                ((String[]) buf[25])[0] = rslt.getVarchar(14) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(14);
                ((String[]) buf[27])[0] = rslt.getString(15, 15) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(15);
                ((String[]) buf[29])[0] = rslt.getString(16, 100) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(16);
                ((short[]) buf[31])[0] = rslt.getShort(17) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(17);
                ((String[]) buf[33])[0] = rslt.getVarchar(18) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(18);
                ((String[]) buf[35])[0] = rslt.getVarchar(19) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(19);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 1 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[5]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[3]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[4]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[5]);
                }
                return;
             case 4 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[1]);
                }
                return;
             case 6 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[5]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                return;
             case 7 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[5]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                return;
       }
    }

 }

}
