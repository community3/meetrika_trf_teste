/*
               File: PRC_1aEntregaDaOS
        Description: Data da primeira entrega da OS
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:51:50.81
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_1aentregadaos : GXProcedure
   {
      public prc_1aentregadaos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_1aentregadaos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_LogResponsavel_DemandaCod ,
                           out DateTime aP1_DateTime )
      {
         this.A892LogResponsavel_DemandaCod = aP0_LogResponsavel_DemandaCod;
         this.AV8DateTime = DateTime.MinValue ;
         initialize();
         executePrivate();
         aP0_LogResponsavel_DemandaCod=this.A892LogResponsavel_DemandaCod;
         aP1_DateTime=this.AV8DateTime;
      }

      public DateTime executeUdp( ref int aP0_LogResponsavel_DemandaCod )
      {
         this.A892LogResponsavel_DemandaCod = aP0_LogResponsavel_DemandaCod;
         this.AV8DateTime = DateTime.MinValue ;
         initialize();
         executePrivate();
         aP0_LogResponsavel_DemandaCod=this.A892LogResponsavel_DemandaCod;
         aP1_DateTime=this.AV8DateTime;
         return AV8DateTime ;
      }

      public void executeSubmit( ref int aP0_LogResponsavel_DemandaCod ,
                                 out DateTime aP1_DateTime )
      {
         prc_1aentregadaos objprc_1aentregadaos;
         objprc_1aentregadaos = new prc_1aentregadaos();
         objprc_1aentregadaos.A892LogResponsavel_DemandaCod = aP0_LogResponsavel_DemandaCod;
         objprc_1aentregadaos.AV8DateTime = DateTime.MinValue ;
         objprc_1aentregadaos.context.SetSubmitInitialConfig(context);
         objprc_1aentregadaos.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_1aentregadaos);
         aP0_LogResponsavel_DemandaCod=this.A892LogResponsavel_DemandaCod;
         aP1_DateTime=this.AV8DateTime;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_1aentregadaos)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV9Acoes = "DVH";
         /* Using cursor P008I2 */
         pr_default.execute(0, new Object[] {n892LogResponsavel_DemandaCod, A892LogResponsavel_DemandaCod, AV9Acoes});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A894LogResponsavel_Acao = P008I2_A894LogResponsavel_Acao[0];
            A893LogResponsavel_DataHora = P008I2_A893LogResponsavel_DataHora[0];
            A1797LogResponsavel_Codigo = P008I2_A1797LogResponsavel_Codigo[0];
            AV8DateTime = A893LogResponsavel_DataHora;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV9Acoes = "";
         scmdbuf = "";
         P008I2_A892LogResponsavel_DemandaCod = new int[1] ;
         P008I2_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         P008I2_A894LogResponsavel_Acao = new String[] {""} ;
         P008I2_A893LogResponsavel_DataHora = new DateTime[] {DateTime.MinValue} ;
         P008I2_A1797LogResponsavel_Codigo = new long[1] ;
         A894LogResponsavel_Acao = "";
         A893LogResponsavel_DataHora = (DateTime)(DateTime.MinValue);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_1aentregadaos__default(),
            new Object[][] {
                new Object[] {
               P008I2_A892LogResponsavel_DemandaCod, P008I2_n892LogResponsavel_DemandaCod, P008I2_A894LogResponsavel_Acao, P008I2_A893LogResponsavel_DataHora, P008I2_A1797LogResponsavel_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A892LogResponsavel_DemandaCod ;
      private long A1797LogResponsavel_Codigo ;
      private String AV9Acoes ;
      private String scmdbuf ;
      private String A894LogResponsavel_Acao ;
      private DateTime AV8DateTime ;
      private DateTime A893LogResponsavel_DataHora ;
      private bool n892LogResponsavel_DemandaCod ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_LogResponsavel_DemandaCod ;
      private IDataStoreProvider pr_default ;
      private int[] P008I2_A892LogResponsavel_DemandaCod ;
      private bool[] P008I2_n892LogResponsavel_DemandaCod ;
      private String[] P008I2_A894LogResponsavel_Acao ;
      private DateTime[] P008I2_A893LogResponsavel_DataHora ;
      private long[] P008I2_A1797LogResponsavel_Codigo ;
      private DateTime aP1_DateTime ;
   }

   public class prc_1aentregadaos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP008I2 ;
          prmP008I2 = new Object[] {
          new Object[] {"@LogResponsavel_DemandaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9Acoes",SqlDbType.Char,5,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P008I2", "SELECT [LogResponsavel_DemandaCod], [LogResponsavel_Acao], [LogResponsavel_DataHora], [LogResponsavel_Codigo] FROM [LogResponsavel] WITH (NOLOCK) WHERE ([LogResponsavel_DemandaCod] = @LogResponsavel_DemandaCod) AND ((CHARINDEX(RTRIM([LogResponsavel_Acao]), @AV9Acoes)) > 0) ORDER BY [LogResponsavel_DemandaCod], [LogResponsavel_DataHora] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008I2,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 20) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(3) ;
                ((long[]) buf[4])[0] = rslt.getLong(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (String)parms[2]);
                return;
       }
    }

 }

}
