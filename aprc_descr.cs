/*
               File: PRC_descr
        Description: PRC_descr
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:56:19.96
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class aprc_descr : GXProcedure
   {
      public aprc_descr( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public aprc_descr( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( )
      {
         initialize();
         executePrivate();
      }

      public void executeSubmit( )
      {
         aprc_descr objaprc_descr;
         objaprc_descr = new aprc_descr();
         objaprc_descr.context.SetSubmitInitialConfig(context);
         objaprc_descr.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objaprc_descr);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((aprc_descr)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         new geralog(context ).execute( ref  AV14Pgmdesc) ;
         AV9senha = Crypto.Decrypt64( "BxJy52La7+7NQJCzUDwVWA==", "879A0A6B8D4A8B78D99E549ED9B5D086");
         new geralog(context ).execute( ref  AV9senha) ;
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV14Pgmdesc = "";
         AV9senha = "";
         AV14Pgmdesc = "PRC_descr";
         /* GeneXus formulas. */
         AV14Pgmdesc = "PRC_descr";
         context.Gx_err = 0;
      }

      private String AV14Pgmdesc ;
      private String AV9senha ;
   }

}
