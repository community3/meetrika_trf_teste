/*
               File: PRC_FAPadrao
        Description: Fator de Ajuste Padr�o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:51:29.11
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_fapadrao : GXProcedure
   {
      public prc_fapadrao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_fapadrao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( out decimal aP0_ParametrosSistema_FatorAjuste )
      {
         this.AV8ParametrosSistema_FatorAjuste = 0 ;
         initialize();
         executePrivate();
         aP0_ParametrosSistema_FatorAjuste=this.AV8ParametrosSistema_FatorAjuste;
      }

      public decimal executeUdp( )
      {
         this.AV8ParametrosSistema_FatorAjuste = 0 ;
         initialize();
         executePrivate();
         aP0_ParametrosSistema_FatorAjuste=this.AV8ParametrosSistema_FatorAjuste;
         return AV8ParametrosSistema_FatorAjuste ;
      }

      public void executeSubmit( out decimal aP0_ParametrosSistema_FatorAjuste )
      {
         prc_fapadrao objprc_fapadrao;
         objprc_fapadrao = new prc_fapadrao();
         objprc_fapadrao.AV8ParametrosSistema_FatorAjuste = 0 ;
         objprc_fapadrao.context.SetSubmitInitialConfig(context);
         objprc_fapadrao.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_fapadrao);
         aP0_ParametrosSistema_FatorAjuste=this.AV8ParametrosSistema_FatorAjuste;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_fapadrao)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P005B2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            A330ParametrosSistema_Codigo = P005B2_A330ParametrosSistema_Codigo[0];
            A708ParametrosSistema_FatorAjuste = P005B2_A708ParametrosSistema_FatorAjuste[0];
            n708ParametrosSistema_FatorAjuste = P005B2_n708ParametrosSistema_FatorAjuste[0];
            OV8ParametrosSistema_FatorAjuste = AV8ParametrosSistema_FatorAjuste;
            AV8ParametrosSistema_FatorAjuste = A708ParametrosSistema_FatorAjuste;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P005B2_A330ParametrosSistema_Codigo = new int[1] ;
         P005B2_A708ParametrosSistema_FatorAjuste = new decimal[1] ;
         P005B2_n708ParametrosSistema_FatorAjuste = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_fapadrao__default(),
            new Object[][] {
                new Object[] {
               P005B2_A330ParametrosSistema_Codigo, P005B2_A708ParametrosSistema_FatorAjuste, P005B2_n708ParametrosSistema_FatorAjuste
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A330ParametrosSistema_Codigo ;
      private decimal AV8ParametrosSistema_FatorAjuste ;
      private decimal A708ParametrosSistema_FatorAjuste ;
      private decimal OV8ParametrosSistema_FatorAjuste ;
      private String scmdbuf ;
      private bool n708ParametrosSistema_FatorAjuste ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P005B2_A330ParametrosSistema_Codigo ;
      private decimal[] P005B2_A708ParametrosSistema_FatorAjuste ;
      private bool[] P005B2_n708ParametrosSistema_FatorAjuste ;
      private decimal aP0_ParametrosSistema_FatorAjuste ;
   }

   public class prc_fapadrao__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP005B2 ;
          prmP005B2 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("P005B2", "SELECT TOP 1 [ParametrosSistema_Codigo], [ParametrosSistema_FatorAjuste] FROM [ParametrosSistema] WITH (NOLOCK) WHERE [ParametrosSistema_Codigo] = 1 ORDER BY [ParametrosSistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005B2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
