/*
               File: ContagemResultadoContagemResultadoContagensWC
        Description: Contagem Resultado Contagem Resultado Contagens WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 5:19:15.97
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemresultadocontagemresultadocontagenswc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contagemresultadocontagemresultadocontagenswc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contagemresultadocontagemresultadocontagenswc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo )
      {
         this.AV7ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbContagemResultado_StatusCnt = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7ContagemResultado_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_8 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_8_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_8_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV14OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
                  AV15OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
                  AV7ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0)));
                  AV97Pgmname = GetNextPar( );
                  A456ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A1553ContagemResultado_CntSrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n1553ContagemResultado_CntSrvCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1553ContagemResultado_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0)));
                  A160ContratoServicos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
                  A2112ContratoServicosUnidConversao_Sigla = GetNextPar( );
                  n2112ContratoServicosUnidConversao_Sigla = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2112ContratoServicosUnidConversao_Sigla", A2112ContratoServicosUnidConversao_Sigla);
                  A2114ContratoServicosUnidConversao_Conversao = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2114ContratoServicosUnidConversao_Conversao", StringUtil.LTrim( StringUtil.Str( A2114ContratoServicosUnidConversao_Conversao, 14, 5)));
                  A473ContagemResultado_DataCnt = context.localUtil.ParseDateParm( GetNextPar( ));
                  A511ContagemResultado_HoraCnt = GetNextPar( );
                  A517ContagemResultado_Ultima = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A517ContagemResultado_Ultima", A517ContagemResultado_Ultima);
                  A574ContagemResultado_PFFinal = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A574ContagemResultado_PFFinal", StringUtil.LTrim( StringUtil.Str( A574ContagemResultado_PFFinal, 14, 5)));
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
                  AV89ContratoServicosUnidConversao_Sigla = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV89ContratoServicosUnidConversao_Sigla", AV89ContratoServicosUnidConversao_Sigla);
                  AV90ContratoServicosUnidConversao_Conversao = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV90ContratoServicosUnidConversao_Conversao", StringUtil.LTrim( StringUtil.Str( AV90ContratoServicosUnidConversao_Conversao, 14, 5)));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV7ContagemResultado_Codigo, AV97Pgmname, A456ContagemResultado_Codigo, A1553ContagemResultado_CntSrvCod, A160ContratoServicos_Codigo, A2112ContratoServicosUnidConversao_Sigla, A2114ContratoServicosUnidConversao_Conversao, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt, A517ContagemResultado_Ultima, A574ContagemResultado_PFFinal, AV6WWPContext, AV89ContratoServicosUnidConversao_Sigla, AV90ContratoServicosUnidConversao_Conversao, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PABF2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV97Pgmname = "ContagemResultadoContagemResultadoContagensWC";
               context.Gx_err = 0;
               edtavContagemresultado_pffinal_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemresultado_pffinal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_pffinal_Enabled), 5, 0)));
               edtavValorpfxunidadeconversao_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavValorpfxunidadeconversao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavValorpfxunidadeconversao_Enabled), 5, 0)));
               WSBF2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contagem Resultado Contagem Resultado Contagens WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020625191615");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contagemresultadocontagemresultadocontagenswc.aspx") + "?" + UrlEncode("" +AV7ContagemResultado_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV15OrderedDsc));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_8", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_8), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV85GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV86GridPageCount), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV97Pgmname));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_CNTSRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A160ContratoServicos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOSUNIDCONVERSAO_SIGLA", StringUtil.RTrim( A2112ContratoServicosUnidConversao_Sigla));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOSUNIDCONVERSAO_CONVERSAO", StringUtil.LTrim( StringUtil.NToC( A2114ContratoServicosUnidConversao_Conversao, 14, 5, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_ULTIMA", A517ContagemResultado_Ultima);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTRATOSERVICOSUNIDCONVERSAO_SIGLA", StringUtil.RTrim( AV89ContratoServicosUnidConversao_Sigla));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTRATOSERVICOSUNIDCONVERSAO_CONVERSAO", StringUtil.LTrim( StringUtil.NToC( AV90ContratoServicosUnidConversao_Conversao, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_PFFINAL", StringUtil.LTrim( StringUtil.NToC( A574ContagemResultado_PFFinal, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormBF2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contagemresultadocontagemresultadocontagenswc.js", "?2020625191645");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContagemResultadoContagemResultadoContagensWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contagem Resultado Contagem Resultado Contagens WC" ;
      }

      protected void WBBF0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contagemresultadocontagemresultadocontagenswc.aspx");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_BF2( true) ;
         }
         else
         {
            wb_table1_2_BF2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_BF2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrderedby_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14OrderedBy), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV14OrderedBy), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,29);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrderedby_Jsonclick, 0, "Attribute", "", "", "", edtavOrderedby_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoContagemResultadoContagensWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV15OrderedDsc), StringUtil.BoolToStr( AV15OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,30);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_ContagemResultadoContagemResultadoContagensWC.htm");
         }
         wbLoad = true;
      }

      protected void STARTBF2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contagem Resultado Contagem Resultado Contagens WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPBF0( ) ;
            }
         }
      }

      protected void WSBF2( )
      {
         STARTBF2( ) ;
         EVTBF2( ) ;
      }

      protected void EVTBF2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPBF0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPBF0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11BF2 */
                                    E11BF2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPBF0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavOrderedby_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPBF0( ) ;
                              }
                              nGXsfl_8_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
                              SubsflControlProps_82( ) ;
                              AV32Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV32Update)) ? AV95Update_GXI : context.convertURL( context.PathToRelativeUrl( AV32Update))));
                              AV33Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV33Delete)) ? AV96Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV33Delete))));
                              A456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagemResultado_Codigo_Internalname), ",", "."));
                              A473ContagemResultado_DataCnt = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContagemResultado_DataCnt_Internalname), 0));
                              A511ContagemResultado_HoraCnt = cgiGet( edtContagemResultado_HoraCnt_Internalname);
                              A458ContagemResultado_PFBFS = context.localUtil.CToN( cgiGet( edtContagemResultado_PFBFS_Internalname), ",", ".");
                              n458ContagemResultado_PFBFS = false;
                              A459ContagemResultado_PFLFS = context.localUtil.CToN( cgiGet( edtContagemResultado_PFLFS_Internalname), ",", ".");
                              n459ContagemResultado_PFLFS = false;
                              A460ContagemResultado_PFBFM = context.localUtil.CToN( cgiGet( edtContagemResultado_PFBFM_Internalname), ",", ".");
                              n460ContagemResultado_PFBFM = false;
                              A461ContagemResultado_PFLFM = context.localUtil.CToN( cgiGet( edtContagemResultado_PFLFM_Internalname), ",", ".");
                              n461ContagemResultado_PFLFM = false;
                              A800ContagemResultado_Deflator = context.localUtil.CToN( cgiGet( edtContagemResultado_Deflator_Internalname), ",", ".");
                              n800ContagemResultado_Deflator = false;
                              AV35ContagemResultado_PFFinal = cgiGet( edtavContagemresultado_pffinal_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavContagemresultado_pffinal_Internalname, AV35ContagemResultado_PFFinal);
                              AV91ValorPFxUnidadeConversao = context.localUtil.CToN( cgiGet( edtavValorpfxunidadeconversao_Internalname), ",", ".");
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavValorpfxunidadeconversao_Internalname, StringUtil.LTrim( StringUtil.Str( AV91ValorPFxUnidadeConversao, 14, 5)));
                              A462ContagemResultado_Divergencia = context.localUtil.CToN( cgiGet( edtContagemResultado_Divergencia_Internalname), ",", ".");
                              cmbContagemResultado_StatusCnt.Name = cmbContagemResultado_StatusCnt_Internalname;
                              cmbContagemResultado_StatusCnt.CurrentValue = cgiGet( cmbContagemResultado_StatusCnt_Internalname);
                              A483ContagemResultado_StatusCnt = (short)(NumberUtil.Val( cgiGet( cmbContagemResultado_StatusCnt_Internalname), "."));
                              A474ContagemResultado_ContadorFMNom = StringUtil.Upper( cgiGet( edtContagemResultado_ContadorFMNom_Internalname));
                              n474ContagemResultado_ContadorFMNom = false;
                              A853ContagemResultado_NomePla = cgiGet( edtContagemResultado_NomePla_Internalname);
                              n853ContagemResultado_NomePla = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E12BF2 */
                                          E12BF2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E13BF2 */
                                          E13BF2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E14BF2 */
                                          E14BF2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPBF0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEBF2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormBF2( ) ;
            }
         }
      }

      protected void PABF2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            GXCCtl = "CONTAGEMRESULTADO_STATUSCNT_" + sGXsfl_8_idx;
            cmbContagemResultado_StatusCnt.Name = GXCCtl;
            cmbContagemResultado_StatusCnt.WebTags = "";
            cmbContagemResultado_StatusCnt.addItem("1", "Inicial", 0);
            cmbContagemResultado_StatusCnt.addItem("2", "Auditoria", 0);
            cmbContagemResultado_StatusCnt.addItem("3", "Negociac�o", 0);
            cmbContagemResultado_StatusCnt.addItem("4", "N�o Aprovada", 0);
            cmbContagemResultado_StatusCnt.addItem("5", "Entregue", 0);
            cmbContagemResultado_StatusCnt.addItem("6", "Pend�ncias", 0);
            cmbContagemResultado_StatusCnt.addItem("7", "Diverg�ncia", 0);
            cmbContagemResultado_StatusCnt.addItem("8", "Aprovada", 0);
            if ( cmbContagemResultado_StatusCnt.ItemCount > 0 )
            {
               A483ContagemResultado_StatusCnt = (short)(NumberUtil.Val( cmbContagemResultado_StatusCnt.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A483ContagemResultado_StatusCnt), 2, 0))), "."));
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_82( ) ;
         while ( nGXsfl_8_idx <= nRC_GXsfl_8 )
         {
            sendrow_82( ) ;
            nGXsfl_8_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_8_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_8_idx+1));
            sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
            SubsflControlProps_82( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV14OrderedBy ,
                                       bool AV15OrderedDsc ,
                                       int AV7ContagemResultado_Codigo ,
                                       String AV97Pgmname ,
                                       int A456ContagemResultado_Codigo ,
                                       int A1553ContagemResultado_CntSrvCod ,
                                       int A160ContratoServicos_Codigo ,
                                       String A2112ContratoServicosUnidConversao_Sigla ,
                                       decimal A2114ContratoServicosUnidConversao_Conversao ,
                                       DateTime A473ContagemResultado_DataCnt ,
                                       String A511ContagemResultado_HoraCnt ,
                                       bool A517ContagemResultado_Ultima ,
                                       decimal A574ContagemResultado_PFFinal ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       String AV89ContratoServicosUnidConversao_Sigla ,
                                       decimal AV90ContratoServicosUnidConversao_Conversao ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFBF2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_DATACNT", GetSecureSignedToken( sPrefix, A473ContagemResultado_DataCnt));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_DATACNT", context.localUtil.Format(A473ContagemResultado_DataCnt, "99/99/99"));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_HORACNT", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A511ContagemResultado_HoraCnt, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_HORACNT", StringUtil.RTrim( A511ContagemResultado_HoraCnt));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_PFBFS", GetSecureSignedToken( sPrefix, context.localUtil.Format( A458ContagemResultado_PFBFS, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_PFBFS", StringUtil.LTrim( StringUtil.NToC( A458ContagemResultado_PFBFS, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_PFLFS", GetSecureSignedToken( sPrefix, context.localUtil.Format( A459ContagemResultado_PFLFS, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_PFLFS", StringUtil.LTrim( StringUtil.NToC( A459ContagemResultado_PFLFS, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_PFBFM", GetSecureSignedToken( sPrefix, context.localUtil.Format( A460ContagemResultado_PFBFM, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_PFBFM", StringUtil.LTrim( StringUtil.NToC( A460ContagemResultado_PFBFM, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_PFLFM", GetSecureSignedToken( sPrefix, context.localUtil.Format( A461ContagemResultado_PFLFM, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_PFLFM", StringUtil.LTrim( StringUtil.NToC( A461ContagemResultado_PFLFM, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_DEFLATOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( A800ContagemResultado_Deflator, "Z9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_DEFLATOR", StringUtil.LTrim( StringUtil.NToC( A800ContagemResultado_Deflator, 6, 3, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_DIVERGENCIA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A462ContagemResultado_Divergencia, "ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_DIVERGENCIA", StringUtil.LTrim( StringUtil.NToC( A462ContagemResultado_Divergencia, 6, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_STATUSCNT", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A483ContagemResultado_StatusCnt), "Z9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_STATUSCNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(A483ContagemResultado_StatusCnt), 2, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_NOMEPLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A853ContagemResultado_NomePla, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_NOMEPLA", StringUtil.RTrim( A853ContagemResultado_NomePla));
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFBF2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV97Pgmname = "ContagemResultadoContagemResultadoContagensWC";
         context.Gx_err = 0;
         edtavContagemresultado_pffinal_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemresultado_pffinal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_pffinal_Enabled), 5, 0)));
         edtavValorpfxunidadeconversao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavValorpfxunidadeconversao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavValorpfxunidadeconversao_Enabled), 5, 0)));
      }

      protected void RFBF2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 8;
         /* Execute user event: E13BF2 */
         E13BF2 ();
         nGXsfl_8_idx = 1;
         sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
         SubsflControlProps_82( ) ;
         nGXsfl_8_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_82( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV14OrderedBy ,
                                                 AV15OrderedDsc ,
                                                 A456ContagemResultado_Codigo ,
                                                 AV7ContagemResultado_Codigo },
                                                 new int[] {
                                                 TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            /* Using cursor H00BF2 */
            pr_default.execute(0, new Object[] {AV7ContagemResultado_Codigo, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_8_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A470ContagemResultado_ContadorFMCod = H00BF2_A470ContagemResultado_ContadorFMCod[0];
               A479ContagemResultado_CrFMPessoaCod = H00BF2_A479ContagemResultado_CrFMPessoaCod[0];
               n479ContagemResultado_CrFMPessoaCod = H00BF2_n479ContagemResultado_CrFMPessoaCod[0];
               A517ContagemResultado_Ultima = H00BF2_A517ContagemResultado_Ultima[0];
               A853ContagemResultado_NomePla = H00BF2_A853ContagemResultado_NomePla[0];
               n853ContagemResultado_NomePla = H00BF2_n853ContagemResultado_NomePla[0];
               A474ContagemResultado_ContadorFMNom = H00BF2_A474ContagemResultado_ContadorFMNom[0];
               n474ContagemResultado_ContadorFMNom = H00BF2_n474ContagemResultado_ContadorFMNom[0];
               A483ContagemResultado_StatusCnt = H00BF2_A483ContagemResultado_StatusCnt[0];
               A462ContagemResultado_Divergencia = H00BF2_A462ContagemResultado_Divergencia[0];
               A800ContagemResultado_Deflator = H00BF2_A800ContagemResultado_Deflator[0];
               n800ContagemResultado_Deflator = H00BF2_n800ContagemResultado_Deflator[0];
               A461ContagemResultado_PFLFM = H00BF2_A461ContagemResultado_PFLFM[0];
               n461ContagemResultado_PFLFM = H00BF2_n461ContagemResultado_PFLFM[0];
               A460ContagemResultado_PFBFM = H00BF2_A460ContagemResultado_PFBFM[0];
               n460ContagemResultado_PFBFM = H00BF2_n460ContagemResultado_PFBFM[0];
               A459ContagemResultado_PFLFS = H00BF2_A459ContagemResultado_PFLFS[0];
               n459ContagemResultado_PFLFS = H00BF2_n459ContagemResultado_PFLFS[0];
               A458ContagemResultado_PFBFS = H00BF2_A458ContagemResultado_PFBFS[0];
               n458ContagemResultado_PFBFS = H00BF2_n458ContagemResultado_PFBFS[0];
               A511ContagemResultado_HoraCnt = H00BF2_A511ContagemResultado_HoraCnt[0];
               A473ContagemResultado_DataCnt = H00BF2_A473ContagemResultado_DataCnt[0];
               A456ContagemResultado_Codigo = H00BF2_A456ContagemResultado_Codigo[0];
               A479ContagemResultado_CrFMPessoaCod = H00BF2_A479ContagemResultado_CrFMPessoaCod[0];
               n479ContagemResultado_CrFMPessoaCod = H00BF2_n479ContagemResultado_CrFMPessoaCod[0];
               A474ContagemResultado_ContadorFMNom = H00BF2_A474ContagemResultado_ContadorFMNom[0];
               n474ContagemResultado_ContadorFMNom = H00BF2_n474ContagemResultado_ContadorFMNom[0];
               GXt_decimal1 = A574ContagemResultado_PFFinal;
               new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
               A574ContagemResultado_PFFinal = GXt_decimal1;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A574ContagemResultado_PFFinal", StringUtil.LTrim( StringUtil.Str( A574ContagemResultado_PFFinal, 14, 5)));
               /* Execute user event: E14BF2 */
               E14BF2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 8;
            WBBF0( ) ;
         }
         nGXsfl_8_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV14OrderedBy ,
                                              AV15OrderedDsc ,
                                              A456ContagemResultado_Codigo ,
                                              AV7ContagemResultado_Codigo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         /* Using cursor H00BF3 */
         pr_default.execute(1, new Object[] {AV7ContagemResultado_Codigo});
         GRID_nRecordCount = H00BF3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV7ContagemResultado_Codigo, AV97Pgmname, A456ContagemResultado_Codigo, A1553ContagemResultado_CntSrvCod, A160ContratoServicos_Codigo, A2112ContratoServicosUnidConversao_Sigla, A2114ContratoServicosUnidConversao_Conversao, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt, A517ContagemResultado_Ultima, A574ContagemResultado_PFFinal, AV6WWPContext, AV89ContratoServicosUnidConversao_Sigla, AV90ContratoServicosUnidConversao_Conversao, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV7ContagemResultado_Codigo, AV97Pgmname, A456ContagemResultado_Codigo, A1553ContagemResultado_CntSrvCod, A160ContratoServicos_Codigo, A2112ContratoServicosUnidConversao_Sigla, A2114ContratoServicosUnidConversao_Conversao, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt, A517ContagemResultado_Ultima, A574ContagemResultado_PFFinal, AV6WWPContext, AV89ContratoServicosUnidConversao_Sigla, AV90ContratoServicosUnidConversao_Conversao, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV7ContagemResultado_Codigo, AV97Pgmname, A456ContagemResultado_Codigo, A1553ContagemResultado_CntSrvCod, A160ContratoServicos_Codigo, A2112ContratoServicosUnidConversao_Sigla, A2114ContratoServicosUnidConversao_Conversao, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt, A517ContagemResultado_Ultima, A574ContagemResultado_PFFinal, AV6WWPContext, AV89ContratoServicosUnidConversao_Sigla, AV90ContratoServicosUnidConversao_Conversao, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV7ContagemResultado_Codigo, AV97Pgmname, A456ContagemResultado_Codigo, A1553ContagemResultado_CntSrvCod, A160ContratoServicos_Codigo, A2112ContratoServicosUnidConversao_Sigla, A2114ContratoServicosUnidConversao_Conversao, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt, A517ContagemResultado_Ultima, A574ContagemResultado_PFFinal, AV6WWPContext, AV89ContratoServicosUnidConversao_Sigla, AV90ContratoServicosUnidConversao_Conversao, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV7ContagemResultado_Codigo, AV97Pgmname, A456ContagemResultado_Codigo, A1553ContagemResultado_CntSrvCod, A160ContratoServicos_Codigo, A2112ContratoServicosUnidConversao_Sigla, A2114ContratoServicosUnidConversao_Conversao, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt, A517ContagemResultado_Ultima, A574ContagemResultado_PFFinal, AV6WWPContext, AV89ContratoServicosUnidConversao_Sigla, AV90ContratoServicosUnidConversao_Conversao, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPBF0( )
      {
         /* Before Start, stand alone formulas. */
         AV97Pgmname = "ContagemResultadoContagemResultadoContagensWC";
         context.Gx_err = 0;
         edtavContagemresultado_pffinal_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemresultado_pffinal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_pffinal_Enabled), 5, 0)));
         edtavValorpfxunidadeconversao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavValorpfxunidadeconversao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavValorpfxunidadeconversao_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E12BF2 */
         E12BF2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vORDEREDBY");
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV14OrderedBy = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            }
            else
            {
               AV14OrderedBy = (short)(context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            }
            AV15OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            /* Read saved values. */
            nRC_GXsfl_8 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_8"), ",", "."));
            AV85GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV86GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV7ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7ContagemResultado_Codigo"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E12BF2 */
         E12BF2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E12BF2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrderedby_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrderedby_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrderedby_Visible), 5, 0)));
         if ( AV14OrderedBy < 1 )
         {
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
      }

      protected void E13BF2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContagemResultado_DataCnt_Titleformat = 2;
         edtContagemResultado_DataCnt_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 1);' >%5</span>", ((AV14OrderedBy==1) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, edtavOrderedby_Internalname, "Data", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultado_DataCnt_Internalname, "Title", edtContagemResultado_DataCnt_Title);
         edtContagemResultado_HoraCnt_Titleformat = 2;
         edtContagemResultado_HoraCnt_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 2);' >%5</span>", ((AV14OrderedBy==2) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, edtavOrderedby_Internalname, "Hora", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultado_HoraCnt_Internalname, "Title", edtContagemResultado_HoraCnt_Title);
         edtContagemResultado_PFBFS_Titleformat = 2;
         edtContagemResultado_PFBFS_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 3);' >%5</span>", ((AV14OrderedBy==3) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, edtavOrderedby_Internalname, "PFB FS", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultado_PFBFS_Internalname, "Title", edtContagemResultado_PFBFS_Title);
         edtContagemResultado_PFLFS_Titleformat = 2;
         edtContagemResultado_PFLFS_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 4);' >%5</span>", ((AV14OrderedBy==4) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, edtavOrderedby_Internalname, "PFL FS", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultado_PFLFS_Internalname, "Title", edtContagemResultado_PFLFS_Title);
         edtContagemResultado_PFBFM_Titleformat = 2;
         edtContagemResultado_PFBFM_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 5);' >%5</span>", ((AV14OrderedBy==5) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, edtavOrderedby_Internalname, "PFB FM", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultado_PFBFM_Internalname, "Title", edtContagemResultado_PFBFM_Title);
         edtContagemResultado_PFLFM_Titleformat = 2;
         edtContagemResultado_PFLFM_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 6);' >%5</span>", ((AV14OrderedBy==6) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, edtavOrderedby_Internalname, "PFL FM", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultado_PFLFM_Internalname, "Title", edtContagemResultado_PFLFM_Title);
         edtContagemResultado_Deflator_Titleformat = 2;
         edtContagemResultado_Deflator_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 7);' >%5</span>", ((AV14OrderedBy==7) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, edtavOrderedby_Internalname, "Deflator", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultado_Deflator_Internalname, "Title", edtContagemResultado_Deflator_Title);
         edtContagemResultado_Divergencia_Titleformat = 2;
         edtContagemResultado_Divergencia_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 8);' >%5</span>", ((AV14OrderedBy==8) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, edtavOrderedby_Internalname, "Diverg�ncia", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultado_Divergencia_Internalname, "Title", edtContagemResultado_Divergencia_Title);
         cmbContagemResultado_StatusCnt_Titleformat = 2;
         cmbContagemResultado_StatusCnt.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 9);' >%5</span>", ((AV14OrderedBy==9) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, edtavOrderedby_Internalname, "Status", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContagemResultado_StatusCnt_Internalname, "Title", cmbContagemResultado_StatusCnt.Title.Text);
         edtContagemResultado_ContadorFMNom_Titleformat = 2;
         edtContagemResultado_ContadorFMNom_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 10);' >%5</span>", ((AV14OrderedBy==10) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, edtavOrderedby_Internalname, "Contador FM", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultado_ContadorFMNom_Internalname, "Title", edtContagemResultado_ContadorFMNom_Title);
         edtContagemResultado_NomePla_Titleformat = 2;
         edtContagemResultado_NomePla_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 11);' >%5</span>", ((AV14OrderedBy==11) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, edtavOrderedby_Internalname, "Planilha", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultado_NomePla_Internalname, "Title", edtContagemResultado_NomePla_Title);
         AV85GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV85GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV85GridCurrentPage), 10, 0)));
         AV86GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV86GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV86GridPageCount), 10, 0)));
         edtContagemResultado_ContadorFMNom_Visible = (!AV6WWPContext.gxTpr_Userehcontratante ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultado_ContadorFMNom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_ContadorFMNom_Visible), 5, 0)));
         edtavUpdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUpdate_Visible), 5, 0)));
         edtavDelete_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDelete_Visible), 5, 0)));
         /* Execute user subroutine: 'BUSCA.VALOR.UNIDADE.CONVERSAO' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV6WWPContext", AV6WWPContext);
      }

      protected void E11BF2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV84PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV84PageToGo) ;
         }
      }

      private void E14BF2( )
      {
         /* Grid_Load Routine */
         AV32Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV32Update);
         AV95Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("contagemresultadocontagens.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A456ContagemResultado_Codigo) + "," + UrlEncode(DateTimeUtil.FormatDateParm(A473ContagemResultado_DataCnt)) + "," + UrlEncode(StringUtil.RTrim(A511ContagemResultado_HoraCnt));
         AV33Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV33Delete);
         AV96Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("contagemresultadocontagens.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A456ContagemResultado_Codigo) + "," + UrlEncode(DateTimeUtil.FormatDateParm(A473ContagemResultado_DataCnt)) + "," + UrlEncode(StringUtil.RTrim(A511ContagemResultado_HoraCnt));
         edtContagemResultado_DataCnt_Link = formatLink("viewcontagemresultadocontagens.aspx") + "?" + UrlEncode("" +A456ContagemResultado_Codigo) + "," + UrlEncode(DateTimeUtil.FormatDateParm(A473ContagemResultado_DataCnt)) + "," + UrlEncode(StringUtil.RTrim(A511ContagemResultado_HoraCnt)) + "," + UrlEncode(StringUtil.RTrim(""));
         if ( A517ContagemResultado_Ultima )
         {
            AV35ContagemResultado_PFFinal = StringUtil.Str( A574ContagemResultado_PFFinal, 14, 3);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavContagemresultado_pffinal_Internalname, AV35ContagemResultado_PFFinal);
            edtavUpdate_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam ? 1 : 0);
            edtavDelete_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam ? 1 : 0);
         }
         else
         {
            AV35ContagemResultado_PFFinal = "";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavContagemresultado_pffinal_Internalname, AV35ContagemResultado_PFFinal);
            edtavUpdate_Visible = 0;
            edtavDelete_Visible = 0;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89ContratoServicosUnidConversao_Sigla)) )
         {
            edtavValorpfxunidadeconversao_Visible = 1;
            edtavValorpfxunidadeconversao_Title = AV89ContratoServicosUnidConversao_Sigla;
            AV91ValorPFxUnidadeConversao = (decimal)((A574ContagemResultado_PFFinal*AV90ContratoServicosUnidConversao_Conversao));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavValorpfxunidadeconversao_Internalname, StringUtil.LTrim( StringUtil.Str( AV91ValorPFxUnidadeConversao, 14, 5)));
         }
         else
         {
            edtavValorpfxunidadeconversao_Visible = 0;
            edtavValorpfxunidadeconversao_Title = "";
            AV91ValorPFxUnidadeConversao = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavValorpfxunidadeconversao_Internalname, StringUtil.LTrim( StringUtil.Str( AV91ValorPFxUnidadeConversao, 14, 5)));
         }
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 8;
         }
         sendrow_82( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_8_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(8, GridRow);
         }
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV34Session.Get(AV97Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV97Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV34Session.Get(AV97Pgmname+"GridState"), "");
         }
         AV14OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         AV15OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
      }

      protected void S132( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV34Session.Get(AV97Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV14OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV15OrderedDsc;
         new wwpbaseobjects.savegridstate(context ).execute(  AV97Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV97Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "ContagemResultadoContagens";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "ContagemResultado_Codigo";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV34Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void S142( )
      {
         /* 'BUSCA.VALOR.UNIDADE.CONVERSAO' Routine */
         AV90ContratoServicosUnidConversao_Conversao = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV90ContratoServicosUnidConversao_Conversao", StringUtil.LTrim( StringUtil.Str( AV90ContratoServicosUnidConversao_Conversao, 14, 5)));
         AV89ContratoServicosUnidConversao_Sigla = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV89ContratoServicosUnidConversao_Sigla", AV89ContratoServicosUnidConversao_Sigla);
         /* Using cursor H00BF4 */
         pr_default.execute(2, new Object[] {AV7ContagemResultado_Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A456ContagemResultado_Codigo = H00BF4_A456ContagemResultado_Codigo[0];
            A1553ContagemResultado_CntSrvCod = H00BF4_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = H00BF4_n1553ContagemResultado_CntSrvCod[0];
            AV92ContagemResultado_CntSrvCod = A1553ContagemResultado_CntSrvCod;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV92ContagemResultado_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV92ContagemResultado_CntSrvCod), 6, 0)));
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(2);
         /* Using cursor H00BF5 */
         pr_default.execute(3, new Object[] {AV92ContagemResultado_CntSrvCod});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A2110ContratoServicosUnidConversao_Codigo = H00BF5_A2110ContratoServicosUnidConversao_Codigo[0];
            A160ContratoServicos_Codigo = H00BF5_A160ContratoServicos_Codigo[0];
            A2112ContratoServicosUnidConversao_Sigla = H00BF5_A2112ContratoServicosUnidConversao_Sigla[0];
            n2112ContratoServicosUnidConversao_Sigla = H00BF5_n2112ContratoServicosUnidConversao_Sigla[0];
            A2114ContratoServicosUnidConversao_Conversao = H00BF5_A2114ContratoServicosUnidConversao_Conversao[0];
            A2112ContratoServicosUnidConversao_Sigla = H00BF5_A2112ContratoServicosUnidConversao_Sigla[0];
            n2112ContratoServicosUnidConversao_Sigla = H00BF5_n2112ContratoServicosUnidConversao_Sigla[0];
            AV89ContratoServicosUnidConversao_Sigla = A2112ContratoServicosUnidConversao_Sigla;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV89ContratoServicosUnidConversao_Sigla", AV89ContratoServicosUnidConversao_Sigla);
            AV90ContratoServicosUnidConversao_Conversao = A2114ContratoServicosUnidConversao_Conversao;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV90ContratoServicosUnidConversao_Conversao", StringUtil.LTrim( StringUtil.Str( AV90ContratoServicosUnidConversao_Conversao, 14, 5)));
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(3);
         }
         pr_default.close(3);
      }

      protected void wb_table1_2_BF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_BF2( true) ;
         }
         else
         {
            wb_table2_5_BF2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_BF2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_BF2e( true) ;
         }
         else
         {
            wb_table1_2_BF2e( false) ;
         }
      }

      protected void wb_table2_5_BF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"8\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Contagem Resultado_Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultado_DataCnt_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultado_DataCnt_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultado_DataCnt_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultado_HoraCnt_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultado_HoraCnt_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultado_HoraCnt_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(94), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultado_PFBFS_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultado_PFBFS_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultado_PFBFS_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(94), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultado_PFLFS_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultado_PFLFS_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultado_PFLFS_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(94), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultado_PFBFM_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultado_PFBFM_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultado_PFBFM_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(94), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultado_PFLFM_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultado_PFLFM_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultado_PFLFM_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(50), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultado_Deflator_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultado_Deflator_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultado_Deflator_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "PF Final") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavValorpfxunidadeconversao_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( edtavValorpfxunidadeconversao_Title) ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultado_Divergencia_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultado_Divergencia_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultado_Divergencia_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContagemResultado_StatusCnt_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContagemResultado_StatusCnt.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContagemResultado_StatusCnt.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtContagemResultado_ContadorFMNom_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               if ( edtContagemResultado_ContadorFMNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultado_ContadorFMNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultado_ContadorFMNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultado_NomePla_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultado_NomePla_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultado_NomePla_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV32Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV33Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A473ContagemResultado_DataCnt, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultado_DataCnt_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_DataCnt_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContagemResultado_DataCnt_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A511ContagemResultado_HoraCnt));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultado_HoraCnt_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_HoraCnt_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A458ContagemResultado_PFBFS, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultado_PFBFS_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_PFBFS_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A459ContagemResultado_PFLFS, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultado_PFLFS_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_PFLFS_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A460ContagemResultado_PFBFM, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultado_PFBFM_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_PFBFM_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A461ContagemResultado_PFLFM, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultado_PFLFM_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_PFLFM_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A800ContagemResultado_Deflator, 6, 3, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultado_Deflator_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_Deflator_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV35ContagemResultado_PFFinal));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContagemresultado_pffinal_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV91ValorPFxUnidadeConversao, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtavValorpfxunidadeconversao_Title));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavValorpfxunidadeconversao_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavValorpfxunidadeconversao_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A462ContagemResultado_Divergencia, 6, 2, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultado_Divergencia_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_Divergencia_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A483ContagemResultado_StatusCnt), 2, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContagemResultado_StatusCnt.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContagemResultado_StatusCnt_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A474ContagemResultado_ContadorFMNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultado_ContadorFMNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_ContadorFMNom_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_ContadorFMNom_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A853ContagemResultado_NomePla));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultado_NomePla_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_NomePla_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 8 )
         {
            wbEnd = 0;
            nRC_GXsfl_8 = (short)(nGXsfl_8_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_BF2e( true) ;
         }
         else
         {
            wb_table2_5_BF2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7ContagemResultado_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PABF2( ) ;
         WSBF2( ) ;
         WEBF2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7ContagemResultado_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PABF2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contagemresultadocontagemresultadocontagenswc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PABF2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7ContagemResultado_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0)));
         }
         wcpOAV7ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7ContagemResultado_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7ContagemResultado_Codigo != wcpOAV7ContagemResultado_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOAV7ContagemResultado_Codigo = AV7ContagemResultado_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7ContagemResultado_Codigo = cgiGet( sPrefix+"AV7ContagemResultado_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV7ContagemResultado_Codigo) > 0 )
         {
            AV7ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7ContagemResultado_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0)));
         }
         else
         {
            AV7ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7ContagemResultado_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PABF2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSBF2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSBF2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7ContagemResultado_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContagemResultado_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7ContagemResultado_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7ContagemResultado_Codigo_CTRL", StringUtil.RTrim( sCtrlAV7ContagemResultado_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEBF2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202062519184");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contagemresultadocontagemresultadocontagenswc.js", "?202062519184");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_82( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_8_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_8_idx;
         edtContagemResultado_Codigo_Internalname = sPrefix+"CONTAGEMRESULTADO_CODIGO_"+sGXsfl_8_idx;
         edtContagemResultado_DataCnt_Internalname = sPrefix+"CONTAGEMRESULTADO_DATACNT_"+sGXsfl_8_idx;
         edtContagemResultado_HoraCnt_Internalname = sPrefix+"CONTAGEMRESULTADO_HORACNT_"+sGXsfl_8_idx;
         edtContagemResultado_PFBFS_Internalname = sPrefix+"CONTAGEMRESULTADO_PFBFS_"+sGXsfl_8_idx;
         edtContagemResultado_PFLFS_Internalname = sPrefix+"CONTAGEMRESULTADO_PFLFS_"+sGXsfl_8_idx;
         edtContagemResultado_PFBFM_Internalname = sPrefix+"CONTAGEMRESULTADO_PFBFM_"+sGXsfl_8_idx;
         edtContagemResultado_PFLFM_Internalname = sPrefix+"CONTAGEMRESULTADO_PFLFM_"+sGXsfl_8_idx;
         edtContagemResultado_Deflator_Internalname = sPrefix+"CONTAGEMRESULTADO_DEFLATOR_"+sGXsfl_8_idx;
         edtavContagemresultado_pffinal_Internalname = sPrefix+"vCONTAGEMRESULTADO_PFFINAL_"+sGXsfl_8_idx;
         edtavValorpfxunidadeconversao_Internalname = sPrefix+"vVALORPFXUNIDADECONVERSAO_"+sGXsfl_8_idx;
         edtContagemResultado_Divergencia_Internalname = sPrefix+"CONTAGEMRESULTADO_DIVERGENCIA_"+sGXsfl_8_idx;
         cmbContagemResultado_StatusCnt_Internalname = sPrefix+"CONTAGEMRESULTADO_STATUSCNT_"+sGXsfl_8_idx;
         edtContagemResultado_ContadorFMNom_Internalname = sPrefix+"CONTAGEMRESULTADO_CONTADORFMNOM_"+sGXsfl_8_idx;
         edtContagemResultado_NomePla_Internalname = sPrefix+"CONTAGEMRESULTADO_NOMEPLA_"+sGXsfl_8_idx;
      }

      protected void SubsflControlProps_fel_82( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_8_fel_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_8_fel_idx;
         edtContagemResultado_Codigo_Internalname = sPrefix+"CONTAGEMRESULTADO_CODIGO_"+sGXsfl_8_fel_idx;
         edtContagemResultado_DataCnt_Internalname = sPrefix+"CONTAGEMRESULTADO_DATACNT_"+sGXsfl_8_fel_idx;
         edtContagemResultado_HoraCnt_Internalname = sPrefix+"CONTAGEMRESULTADO_HORACNT_"+sGXsfl_8_fel_idx;
         edtContagemResultado_PFBFS_Internalname = sPrefix+"CONTAGEMRESULTADO_PFBFS_"+sGXsfl_8_fel_idx;
         edtContagemResultado_PFLFS_Internalname = sPrefix+"CONTAGEMRESULTADO_PFLFS_"+sGXsfl_8_fel_idx;
         edtContagemResultado_PFBFM_Internalname = sPrefix+"CONTAGEMRESULTADO_PFBFM_"+sGXsfl_8_fel_idx;
         edtContagemResultado_PFLFM_Internalname = sPrefix+"CONTAGEMRESULTADO_PFLFM_"+sGXsfl_8_fel_idx;
         edtContagemResultado_Deflator_Internalname = sPrefix+"CONTAGEMRESULTADO_DEFLATOR_"+sGXsfl_8_fel_idx;
         edtavContagemresultado_pffinal_Internalname = sPrefix+"vCONTAGEMRESULTADO_PFFINAL_"+sGXsfl_8_fel_idx;
         edtavValorpfxunidadeconversao_Internalname = sPrefix+"vVALORPFXUNIDADECONVERSAO_"+sGXsfl_8_fel_idx;
         edtContagemResultado_Divergencia_Internalname = sPrefix+"CONTAGEMRESULTADO_DIVERGENCIA_"+sGXsfl_8_fel_idx;
         cmbContagemResultado_StatusCnt_Internalname = sPrefix+"CONTAGEMRESULTADO_STATUSCNT_"+sGXsfl_8_fel_idx;
         edtContagemResultado_ContadorFMNom_Internalname = sPrefix+"CONTAGEMRESULTADO_CONTADORFMNOM_"+sGXsfl_8_fel_idx;
         edtContagemResultado_NomePla_Internalname = sPrefix+"CONTAGEMRESULTADO_NOMEPLA_"+sGXsfl_8_fel_idx;
      }

      protected void sendrow_82( )
      {
         SubsflControlProps_82( ) ;
         WBBF0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_8_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_8_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_8_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV32Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV32Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV95Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV32Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV32Update)) ? AV95Update_GXI : context.PathToRelativeUrl( AV32Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavUpdate_Visible,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV32Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV33Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV33Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV96Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV33Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV33Delete)) ? AV96Delete_GXI : context.PathToRelativeUrl( AV33Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDelete_Visible,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV33Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_DataCnt_Internalname,context.localUtil.Format(A473ContagemResultado_DataCnt, "99/99/99"),context.localUtil.Format( A473ContagemResultado_DataCnt, "99/99/99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtContagemResultado_DataCnt_Link,(String)"",(String)"",(String)"",(String)edtContagemResultado_DataCnt_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_HoraCnt_Internalname,StringUtil.RTrim( A511ContagemResultado_HoraCnt),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_HoraCnt_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)5,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_PFBFS_Internalname,StringUtil.LTrim( StringUtil.NToC( A458ContagemResultado_PFBFS, 14, 5, ",", "")),context.localUtil.Format( A458ContagemResultado_PFBFS, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_PFBFS_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)94,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_PFLFS_Internalname,StringUtil.LTrim( StringUtil.NToC( A459ContagemResultado_PFLFS, 14, 5, ",", "")),context.localUtil.Format( A459ContagemResultado_PFLFS, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_PFLFS_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)94,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_PFBFM_Internalname,StringUtil.LTrim( StringUtil.NToC( A460ContagemResultado_PFBFM, 14, 5, ",", "")),context.localUtil.Format( A460ContagemResultado_PFBFM, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_PFBFM_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)94,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_PFLFM_Internalname,StringUtil.LTrim( StringUtil.NToC( A461ContagemResultado_PFLFM, 14, 5, ",", "")),context.localUtil.Format( A461ContagemResultado_PFLFM, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_PFLFM_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)94,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_Deflator_Internalname,StringUtil.LTrim( StringUtil.NToC( A800ContagemResultado_Deflator, 6, 3, ",", "")),context.localUtil.Format( A800ContagemResultado_Deflator, "Z9.999"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_Deflator_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)50,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"Deflator",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavContagemresultado_pffinal_Internalname,StringUtil.RTrim( AV35ContagemResultado_PFFinal),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavContagemresultado_pffinal_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavContagemresultado_pffinal_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)12,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+((edtavValorpfxunidadeconversao_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavValorpfxunidadeconversao_Internalname,StringUtil.LTrim( StringUtil.NToC( AV91ValorPFxUnidadeConversao, 14, 5, ",", "")),((edtavValorpfxunidadeconversao_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV91ValorPFxUnidadeConversao, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV91ValorPFxUnidadeConversao, "ZZ,ZZZ,ZZ9.999")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavValorpfxunidadeconversao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(int)edtavValorpfxunidadeconversao_Visible,(int)edtavValorpfxunidadeconversao_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_Divergencia_Internalname,StringUtil.LTrim( StringUtil.NToC( A462ContagemResultado_Divergencia, 6, 2, ",", "")),context.localUtil.Format( A462ContagemResultado_Divergencia, "ZZ9.99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_Divergencia_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"Percentual",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_8_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTAGEMRESULTADO_STATUSCNT_" + sGXsfl_8_idx;
               cmbContagemResultado_StatusCnt.Name = GXCCtl;
               cmbContagemResultado_StatusCnt.WebTags = "";
               cmbContagemResultado_StatusCnt.addItem("1", "Inicial", 0);
               cmbContagemResultado_StatusCnt.addItem("2", "Auditoria", 0);
               cmbContagemResultado_StatusCnt.addItem("3", "Negociac�o", 0);
               cmbContagemResultado_StatusCnt.addItem("4", "N�o Aprovada", 0);
               cmbContagemResultado_StatusCnt.addItem("5", "Entregue", 0);
               cmbContagemResultado_StatusCnt.addItem("6", "Pend�ncias", 0);
               cmbContagemResultado_StatusCnt.addItem("7", "Diverg�ncia", 0);
               cmbContagemResultado_StatusCnt.addItem("8", "Aprovada", 0);
               if ( cmbContagemResultado_StatusCnt.ItemCount > 0 )
               {
                  A483ContagemResultado_StatusCnt = (short)(NumberUtil.Val( cmbContagemResultado_StatusCnt.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A483ContagemResultado_StatusCnt), 2, 0))), "."));
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContagemResultado_StatusCnt,(String)cmbContagemResultado_StatusCnt_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A483ContagemResultado_StatusCnt), 2, 0)),(short)1,(String)cmbContagemResultado_StatusCnt_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContagemResultado_StatusCnt.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A483ContagemResultado_StatusCnt), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContagemResultado_StatusCnt_Internalname, "Values", (String)(cmbContagemResultado_StatusCnt.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+((edtContagemResultado_ContadorFMNom_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_ContadorFMNom_Internalname,StringUtil.RTrim( A474ContagemResultado_ContadorFMNom),StringUtil.RTrim( context.localUtil.Format( A474ContagemResultado_ContadorFMNom, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_ContadorFMNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(int)edtContagemResultado_ContadorFMNom_Visible,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_NomePla_Internalname,StringUtil.RTrim( A853ContagemResultado_NomePla),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_NomePla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)-1,(bool)true,(String)"NomeArq",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_CODIGO"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_DATACNT"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, A473ContagemResultado_DataCnt));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_HORACNT"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( A511ContagemResultado_HoraCnt, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_PFBFS"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, context.localUtil.Format( A458ContagemResultado_PFBFS, "ZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_PFLFS"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, context.localUtil.Format( A459ContagemResultado_PFLFS, "ZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_PFBFM"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, context.localUtil.Format( A460ContagemResultado_PFBFM, "ZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_PFLFM"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, context.localUtil.Format( A461ContagemResultado_PFLFM, "ZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_DEFLATOR"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, context.localUtil.Format( A800ContagemResultado_Deflator, "Z9.999")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_DIVERGENCIA"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, context.localUtil.Format( A462ContagemResultado_Divergencia, "ZZ9.99")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_STATUSCNT"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, context.localUtil.Format( (decimal)(A483ContagemResultado_StatusCnt), "Z9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_NOMEPLA"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( A853ContagemResultado_NomePla, ""))));
            GridContainer.AddRow(GridRow);
            nGXsfl_8_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_8_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_8_idx+1));
            sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
            SubsflControlProps_82( ) ;
         }
         /* End function sendrow_82 */
      }

      protected void init_default_properties( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE";
         edtavDelete_Internalname = sPrefix+"vDELETE";
         edtContagemResultado_Codigo_Internalname = sPrefix+"CONTAGEMRESULTADO_CODIGO";
         edtContagemResultado_DataCnt_Internalname = sPrefix+"CONTAGEMRESULTADO_DATACNT";
         edtContagemResultado_HoraCnt_Internalname = sPrefix+"CONTAGEMRESULTADO_HORACNT";
         edtContagemResultado_PFBFS_Internalname = sPrefix+"CONTAGEMRESULTADO_PFBFS";
         edtContagemResultado_PFLFS_Internalname = sPrefix+"CONTAGEMRESULTADO_PFLFS";
         edtContagemResultado_PFBFM_Internalname = sPrefix+"CONTAGEMRESULTADO_PFBFM";
         edtContagemResultado_PFLFM_Internalname = sPrefix+"CONTAGEMRESULTADO_PFLFM";
         edtContagemResultado_Deflator_Internalname = sPrefix+"CONTAGEMRESULTADO_DEFLATOR";
         edtavContagemresultado_pffinal_Internalname = sPrefix+"vCONTAGEMRESULTADO_PFFINAL";
         edtavValorpfxunidadeconversao_Internalname = sPrefix+"vVALORPFXUNIDADECONVERSAO";
         edtContagemResultado_Divergencia_Internalname = sPrefix+"CONTAGEMRESULTADO_DIVERGENCIA";
         cmbContagemResultado_StatusCnt_Internalname = sPrefix+"CONTAGEMRESULTADO_STATUSCNT";
         edtContagemResultado_ContadorFMNom_Internalname = sPrefix+"CONTAGEMRESULTADO_CONTADORFMNOM";
         edtContagemResultado_NomePla_Internalname = sPrefix+"CONTAGEMRESULTADO_NOMEPLA";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         edtavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtContagemResultado_NomePla_Jsonclick = "";
         edtContagemResultado_ContadorFMNom_Jsonclick = "";
         cmbContagemResultado_StatusCnt_Jsonclick = "";
         edtContagemResultado_Divergencia_Jsonclick = "";
         edtavValorpfxunidadeconversao_Jsonclick = "";
         edtavContagemresultado_pffinal_Jsonclick = "";
         edtContagemResultado_Deflator_Jsonclick = "";
         edtContagemResultado_PFLFM_Jsonclick = "";
         edtContagemResultado_PFBFM_Jsonclick = "";
         edtContagemResultado_PFLFS_Jsonclick = "";
         edtContagemResultado_PFBFS_Jsonclick = "";
         edtContagemResultado_HoraCnt_Jsonclick = "";
         edtContagemResultado_DataCnt_Jsonclick = "";
         edtContagemResultado_Codigo_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavValorpfxunidadeconversao_Enabled = 0;
         edtavContagemresultado_pffinal_Enabled = 0;
         edtContagemResultado_DataCnt_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtContagemResultado_NomePla_Titleformat = 0;
         edtContagemResultado_ContadorFMNom_Titleformat = 0;
         cmbContagemResultado_StatusCnt_Titleformat = 0;
         edtContagemResultado_Divergencia_Titleformat = 0;
         edtavValorpfxunidadeconversao_Title = "Valor Convertido";
         edtavValorpfxunidadeconversao_Visible = -1;
         edtContagemResultado_Deflator_Titleformat = 0;
         edtContagemResultado_PFLFM_Titleformat = 0;
         edtContagemResultado_PFBFM_Titleformat = 0;
         edtContagemResultado_PFLFS_Titleformat = 0;
         edtContagemResultado_PFBFS_Titleformat = 0;
         edtContagemResultado_HoraCnt_Titleformat = 0;
         edtContagemResultado_DataCnt_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavDelete_Visible = -1;
         edtavUpdate_Visible = -1;
         edtContagemResultado_ContadorFMNom_Visible = -1;
         edtContagemResultado_NomePla_Title = "Planilha";
         edtContagemResultado_ContadorFMNom_Title = "Contador FM";
         cmbContagemResultado_StatusCnt.Title.Text = "Status";
         edtContagemResultado_Divergencia_Title = "Diverg�ncia";
         edtContagemResultado_Deflator_Title = "Deflator";
         edtContagemResultado_PFLFM_Title = "PFL FM";
         edtContagemResultado_PFBFM_Title = "PFB FM";
         edtContagemResultado_PFLFS_Title = "PFL FS";
         edtContagemResultado_PFBFS_Title = "PFB FS";
         edtContagemResultado_HoraCnt_Title = "Hora";
         edtContagemResultado_DataCnt_Title = "Data";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         edtavOrderedby_Jsonclick = "";
         edtavOrderedby_Visible = 1;
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A473ContagemResultado_DataCnt',fld:'CONTAGEMRESULTADO_DATACNT',pic:'',hsh:true,nv:''},{av:'A511ContagemResultado_HoraCnt',fld:'CONTAGEMRESULTADO_HORACNT',pic:'',hsh:true,nv:''},{av:'A517ContagemResultado_Ultima',fld:'CONTAGEMRESULTADO_ULTIMA',pic:'',nv:false},{av:'A574ContagemResultado_PFFinal',fld:'CONTAGEMRESULTADO_PFFINAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV89ContratoServicosUnidConversao_Sigla',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_SIGLA',pic:'@!',nv:''},{av:'AV90ContratoServicosUnidConversao_Conversao',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_CONVERSAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'sPrefix',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV97Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV7ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A2112ContratoServicosUnidConversao_Sigla',fld:'CONTRATOSERVICOSUNIDCONVERSAO_SIGLA',pic:'@!',nv:''},{av:'A2114ContratoServicosUnidConversao_Conversao',fld:'CONTRATOSERVICOSUNIDCONVERSAO_CONVERSAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtContagemResultado_DataCnt_Titleformat',ctrl:'CONTAGEMRESULTADO_DATACNT',prop:'Titleformat'},{av:'edtContagemResultado_DataCnt_Title',ctrl:'CONTAGEMRESULTADO_DATACNT',prop:'Title'},{av:'edtContagemResultado_HoraCnt_Titleformat',ctrl:'CONTAGEMRESULTADO_HORACNT',prop:'Titleformat'},{av:'edtContagemResultado_HoraCnt_Title',ctrl:'CONTAGEMRESULTADO_HORACNT',prop:'Title'},{av:'edtContagemResultado_PFBFS_Titleformat',ctrl:'CONTAGEMRESULTADO_PFBFS',prop:'Titleformat'},{av:'edtContagemResultado_PFBFS_Title',ctrl:'CONTAGEMRESULTADO_PFBFS',prop:'Title'},{av:'edtContagemResultado_PFLFS_Titleformat',ctrl:'CONTAGEMRESULTADO_PFLFS',prop:'Titleformat'},{av:'edtContagemResultado_PFLFS_Title',ctrl:'CONTAGEMRESULTADO_PFLFS',prop:'Title'},{av:'edtContagemResultado_PFBFM_Titleformat',ctrl:'CONTAGEMRESULTADO_PFBFM',prop:'Titleformat'},{av:'edtContagemResultado_PFBFM_Title',ctrl:'CONTAGEMRESULTADO_PFBFM',prop:'Title'},{av:'edtContagemResultado_PFLFM_Titleformat',ctrl:'CONTAGEMRESULTADO_PFLFM',prop:'Titleformat'},{av:'edtContagemResultado_PFLFM_Title',ctrl:'CONTAGEMRESULTADO_PFLFM',prop:'Title'},{av:'edtContagemResultado_Deflator_Titleformat',ctrl:'CONTAGEMRESULTADO_DEFLATOR',prop:'Titleformat'},{av:'edtContagemResultado_Deflator_Title',ctrl:'CONTAGEMRESULTADO_DEFLATOR',prop:'Title'},{av:'edtContagemResultado_Divergencia_Titleformat',ctrl:'CONTAGEMRESULTADO_DIVERGENCIA',prop:'Titleformat'},{av:'edtContagemResultado_Divergencia_Title',ctrl:'CONTAGEMRESULTADO_DIVERGENCIA',prop:'Title'},{av:'cmbContagemResultado_StatusCnt'},{av:'edtContagemResultado_ContadorFMNom_Titleformat',ctrl:'CONTAGEMRESULTADO_CONTADORFMNOM',prop:'Titleformat'},{av:'edtContagemResultado_ContadorFMNom_Title',ctrl:'CONTAGEMRESULTADO_CONTADORFMNOM',prop:'Title'},{av:'edtContagemResultado_NomePla_Titleformat',ctrl:'CONTAGEMRESULTADO_NOMEPLA',prop:'Titleformat'},{av:'edtContagemResultado_NomePla_Title',ctrl:'CONTAGEMRESULTADO_NOMEPLA',prop:'Title'},{av:'AV85GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV86GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'edtContagemResultado_ContadorFMNom_Visible',ctrl:'CONTAGEMRESULTADO_CONTADORFMNOM',prop:'Visible'},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'AV90ContratoServicosUnidConversao_Conversao',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_CONVERSAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV89ContratoServicosUnidConversao_Sigla',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_SIGLA',pic:'@!',nv:''},{av:'AV92ContagemResultado_CntSrvCod',fld:'vCONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11BF2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV7ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV97Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A2112ContratoServicosUnidConversao_Sigla',fld:'CONTRATOSERVICOSUNIDCONVERSAO_SIGLA',pic:'@!',nv:''},{av:'A2114ContratoServicosUnidConversao_Conversao',fld:'CONTRATOSERVICOSUNIDCONVERSAO_CONVERSAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A473ContagemResultado_DataCnt',fld:'CONTAGEMRESULTADO_DATACNT',pic:'',hsh:true,nv:''},{av:'A511ContagemResultado_HoraCnt',fld:'CONTAGEMRESULTADO_HORACNT',pic:'',hsh:true,nv:''},{av:'A517ContagemResultado_Ultima',fld:'CONTAGEMRESULTADO_ULTIMA',pic:'',nv:false},{av:'A574ContagemResultado_PFFinal',fld:'CONTAGEMRESULTADO_PFFINAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV89ContratoServicosUnidConversao_Sigla',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_SIGLA',pic:'@!',nv:''},{av:'AV90ContratoServicosUnidConversao_Conversao',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_CONVERSAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("GRID.LOAD","{handler:'E14BF2',iparms:[{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A473ContagemResultado_DataCnt',fld:'CONTAGEMRESULTADO_DATACNT',pic:'',hsh:true,nv:''},{av:'A511ContagemResultado_HoraCnt',fld:'CONTAGEMRESULTADO_HORACNT',pic:'',hsh:true,nv:''},{av:'A517ContagemResultado_Ultima',fld:'CONTAGEMRESULTADO_ULTIMA',pic:'',nv:false},{av:'A574ContagemResultado_PFFinal',fld:'CONTAGEMRESULTADO_PFFINAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV89ContratoServicosUnidConversao_Sigla',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_SIGLA',pic:'@!',nv:''},{av:'AV90ContratoServicosUnidConversao_Conversao',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_CONVERSAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}],oparms:[{av:'AV32Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV33Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'edtContagemResultado_DataCnt_Link',ctrl:'CONTAGEMRESULTADO_DATACNT',prop:'Link'},{av:'AV35ContagemResultado_PFFinal',fld:'vCONTAGEMRESULTADO_PFFINAL',pic:'',nv:''},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'edtavValorpfxunidadeconversao_Visible',ctrl:'vVALORPFXUNIDADECONVERSAO',prop:'Visible'},{av:'edtavValorpfxunidadeconversao_Title',ctrl:'vVALORPFXUNIDADECONVERSAO',prop:'Title'},{av:'AV91ValorPFxUnidadeConversao',fld:'vVALORPFXUNIDADECONVERSAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV97Pgmname = "";
         A2112ContratoServicosUnidConversao_Sigla = "";
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A511ContagemResultado_HoraCnt = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV89ContratoServicosUnidConversao_Sigla = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         TempTags = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV32Update = "";
         AV95Update_GXI = "";
         AV33Delete = "";
         AV96Delete_GXI = "";
         AV35ContagemResultado_PFFinal = "";
         A474ContagemResultado_ContadorFMNom = "";
         A853ContagemResultado_NomePla = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         H00BF2_A470ContagemResultado_ContadorFMCod = new int[1] ;
         H00BF2_A479ContagemResultado_CrFMPessoaCod = new int[1] ;
         H00BF2_n479ContagemResultado_CrFMPessoaCod = new bool[] {false} ;
         H00BF2_A517ContagemResultado_Ultima = new bool[] {false} ;
         H00BF2_A853ContagemResultado_NomePla = new String[] {""} ;
         H00BF2_n853ContagemResultado_NomePla = new bool[] {false} ;
         H00BF2_A474ContagemResultado_ContadorFMNom = new String[] {""} ;
         H00BF2_n474ContagemResultado_ContadorFMNom = new bool[] {false} ;
         H00BF2_A483ContagemResultado_StatusCnt = new short[1] ;
         H00BF2_A462ContagemResultado_Divergencia = new decimal[1] ;
         H00BF2_A800ContagemResultado_Deflator = new decimal[1] ;
         H00BF2_n800ContagemResultado_Deflator = new bool[] {false} ;
         H00BF2_A461ContagemResultado_PFLFM = new decimal[1] ;
         H00BF2_n461ContagemResultado_PFLFM = new bool[] {false} ;
         H00BF2_A460ContagemResultado_PFBFM = new decimal[1] ;
         H00BF2_n460ContagemResultado_PFBFM = new bool[] {false} ;
         H00BF2_A459ContagemResultado_PFLFS = new decimal[1] ;
         H00BF2_n459ContagemResultado_PFLFS = new bool[] {false} ;
         H00BF2_A458ContagemResultado_PFBFS = new decimal[1] ;
         H00BF2_n458ContagemResultado_PFBFS = new bool[] {false} ;
         H00BF2_A511ContagemResultado_HoraCnt = new String[] {""} ;
         H00BF2_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         H00BF2_A456ContagemResultado_Codigo = new int[1] ;
         H00BF3_AGRID_nRecordCount = new long[1] ;
         GridRow = new GXWebRow();
         AV34Session = context.GetSession();
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         H00BF4_A456ContagemResultado_Codigo = new int[1] ;
         H00BF4_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00BF4_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00BF5_A2110ContratoServicosUnidConversao_Codigo = new int[1] ;
         H00BF5_A160ContratoServicos_Codigo = new int[1] ;
         H00BF5_A2112ContratoServicosUnidConversao_Sigla = new String[] {""} ;
         H00BF5_n2112ContratoServicosUnidConversao_Sigla = new bool[] {false} ;
         H00BF5_A2114ContratoServicosUnidConversao_Conversao = new decimal[1] ;
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7ContagemResultado_Codigo = "";
         ClassString = "";
         StyleString = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemresultadocontagemresultadocontagenswc__default(),
            new Object[][] {
                new Object[] {
               H00BF2_A470ContagemResultado_ContadorFMCod, H00BF2_A479ContagemResultado_CrFMPessoaCod, H00BF2_n479ContagemResultado_CrFMPessoaCod, H00BF2_A517ContagemResultado_Ultima, H00BF2_A853ContagemResultado_NomePla, H00BF2_n853ContagemResultado_NomePla, H00BF2_A474ContagemResultado_ContadorFMNom, H00BF2_n474ContagemResultado_ContadorFMNom, H00BF2_A483ContagemResultado_StatusCnt, H00BF2_A462ContagemResultado_Divergencia,
               H00BF2_A800ContagemResultado_Deflator, H00BF2_n800ContagemResultado_Deflator, H00BF2_A461ContagemResultado_PFLFM, H00BF2_n461ContagemResultado_PFLFM, H00BF2_A460ContagemResultado_PFBFM, H00BF2_n460ContagemResultado_PFBFM, H00BF2_A459ContagemResultado_PFLFS, H00BF2_n459ContagemResultado_PFLFS, H00BF2_A458ContagemResultado_PFBFS, H00BF2_n458ContagemResultado_PFBFS,
               H00BF2_A511ContagemResultado_HoraCnt, H00BF2_A473ContagemResultado_DataCnt, H00BF2_A456ContagemResultado_Codigo
               }
               , new Object[] {
               H00BF3_AGRID_nRecordCount
               }
               , new Object[] {
               H00BF4_A456ContagemResultado_Codigo, H00BF4_A1553ContagemResultado_CntSrvCod, H00BF4_n1553ContagemResultado_CntSrvCod
               }
               , new Object[] {
               H00BF5_A2110ContratoServicosUnidConversao_Codigo, H00BF5_A160ContratoServicos_Codigo, H00BF5_A2112ContratoServicosUnidConversao_Sigla, H00BF5_n2112ContratoServicosUnidConversao_Sigla, H00BF5_A2114ContratoServicosUnidConversao_Conversao
               }
            }
         );
         AV97Pgmname = "ContagemResultadoContagemResultadoContagensWC";
         /* GeneXus formulas. */
         AV97Pgmname = "ContagemResultadoContagemResultadoContagensWC";
         context.Gx_err = 0;
         edtavContagemresultado_pffinal_Enabled = 0;
         edtavValorpfxunidadeconversao_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_8 ;
      private short nGXsfl_8_idx=1 ;
      private short AV14OrderedBy ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short A483ContagemResultado_StatusCnt ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_8_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContagemResultado_DataCnt_Titleformat ;
      private short edtContagemResultado_HoraCnt_Titleformat ;
      private short edtContagemResultado_PFBFS_Titleformat ;
      private short edtContagemResultado_PFLFS_Titleformat ;
      private short edtContagemResultado_PFBFM_Titleformat ;
      private short edtContagemResultado_PFLFM_Titleformat ;
      private short edtContagemResultado_Deflator_Titleformat ;
      private short edtContagemResultado_Divergencia_Titleformat ;
      private short cmbContagemResultado_StatusCnt_Titleformat ;
      private short edtContagemResultado_ContadorFMNom_Titleformat ;
      private short edtContagemResultado_NomePla_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7ContagemResultado_Codigo ;
      private int wcpOAV7ContagemResultado_Codigo ;
      private int subGrid_Rows ;
      private int A456ContagemResultado_Codigo ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A160ContratoServicos_Codigo ;
      private int edtavContagemresultado_pffinal_Enabled ;
      private int edtavValorpfxunidadeconversao_Enabled ;
      private int Gridpaginationbar_Pagestoshow ;
      private int edtavOrderedby_Visible ;
      private int edtavOrdereddsc_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int A470ContagemResultado_ContadorFMCod ;
      private int A479ContagemResultado_CrFMPessoaCod ;
      private int edtContagemResultado_ContadorFMNom_Visible ;
      private int edtavUpdate_Visible ;
      private int edtavDelete_Visible ;
      private int AV84PageToGo ;
      private int edtavValorpfxunidadeconversao_Visible ;
      private int AV92ContagemResultado_CntSrvCod ;
      private int A2110ContratoServicosUnidConversao_Codigo ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV85GridCurrentPage ;
      private long AV86GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal A2114ContratoServicosUnidConversao_Conversao ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal AV90ContratoServicosUnidConversao_Conversao ;
      private decimal A458ContagemResultado_PFBFS ;
      private decimal A459ContagemResultado_PFLFS ;
      private decimal A460ContagemResultado_PFBFM ;
      private decimal A461ContagemResultado_PFLFM ;
      private decimal A800ContagemResultado_Deflator ;
      private decimal AV91ValorPFxUnidadeConversao ;
      private decimal A462ContagemResultado_Divergencia ;
      private decimal GXt_decimal1 ;
      private String Gridpaginationbar_Selectedpage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_8_idx="0001" ;
      private String AV97Pgmname ;
      private String A2112ContratoServicosUnidConversao_Sigla ;
      private String A511ContagemResultado_HoraCnt ;
      private String AV89ContratoServicosUnidConversao_Sigla ;
      private String GXKey ;
      private String edtavContagemresultado_pffinal_Internalname ;
      private String edtavValorpfxunidadeconversao_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String GX_FocusControl ;
      private String TempTags ;
      private String edtavOrderedby_Internalname ;
      private String edtavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtContagemResultado_Codigo_Internalname ;
      private String edtContagemResultado_DataCnt_Internalname ;
      private String edtContagemResultado_HoraCnt_Internalname ;
      private String edtContagemResultado_PFBFS_Internalname ;
      private String edtContagemResultado_PFLFS_Internalname ;
      private String edtContagemResultado_PFBFM_Internalname ;
      private String edtContagemResultado_PFLFM_Internalname ;
      private String edtContagemResultado_Deflator_Internalname ;
      private String AV35ContagemResultado_PFFinal ;
      private String edtContagemResultado_Divergencia_Internalname ;
      private String cmbContagemResultado_StatusCnt_Internalname ;
      private String A474ContagemResultado_ContadorFMNom ;
      private String edtContagemResultado_ContadorFMNom_Internalname ;
      private String A853ContagemResultado_NomePla ;
      private String edtContagemResultado_NomePla_Internalname ;
      private String GXCCtl ;
      private String scmdbuf ;
      private String edtContagemResultado_DataCnt_Title ;
      private String edtContagemResultado_HoraCnt_Title ;
      private String edtContagemResultado_PFBFS_Title ;
      private String edtContagemResultado_PFLFS_Title ;
      private String edtContagemResultado_PFBFM_Title ;
      private String edtContagemResultado_PFLFM_Title ;
      private String edtContagemResultado_Deflator_Title ;
      private String edtContagemResultado_Divergencia_Title ;
      private String edtContagemResultado_ContadorFMNom_Title ;
      private String edtContagemResultado_NomePla_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtContagemResultado_DataCnt_Link ;
      private String edtavValorpfxunidadeconversao_Title ;
      private String sStyleString ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String sCtrlAV7ContagemResultado_Codigo ;
      private String sGXsfl_8_fel_idx="0001" ;
      private String ClassString ;
      private String StyleString ;
      private String ROClassString ;
      private String edtContagemResultado_Codigo_Jsonclick ;
      private String edtContagemResultado_DataCnt_Jsonclick ;
      private String edtContagemResultado_HoraCnt_Jsonclick ;
      private String edtContagemResultado_PFBFS_Jsonclick ;
      private String edtContagemResultado_PFLFS_Jsonclick ;
      private String edtContagemResultado_PFBFM_Jsonclick ;
      private String edtContagemResultado_PFLFM_Jsonclick ;
      private String edtContagemResultado_Deflator_Jsonclick ;
      private String edtavContagemresultado_pffinal_Jsonclick ;
      private String edtavValorpfxunidadeconversao_Jsonclick ;
      private String edtContagemResultado_Divergencia_Jsonclick ;
      private String cmbContagemResultado_StatusCnt_Jsonclick ;
      private String edtContagemResultado_ContadorFMNom_Jsonclick ;
      private String edtContagemResultado_NomePla_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime A473ContagemResultado_DataCnt ;
      private bool entryPointCalled ;
      private bool AV15OrderedDsc ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n2112ContratoServicosUnidConversao_Sigla ;
      private bool A517ContagemResultado_Ultima ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n458ContagemResultado_PFBFS ;
      private bool n459ContagemResultado_PFLFS ;
      private bool n460ContagemResultado_PFBFM ;
      private bool n461ContagemResultado_PFLFM ;
      private bool n800ContagemResultado_Deflator ;
      private bool n474ContagemResultado_ContadorFMNom ;
      private bool n853ContagemResultado_NomePla ;
      private bool n479ContagemResultado_CrFMPessoaCod ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV32Update_IsBlob ;
      private bool AV33Delete_IsBlob ;
      private String AV95Update_GXI ;
      private String AV96Delete_GXI ;
      private String AV32Update ;
      private String AV33Delete ;
      private IGxSession AV34Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbContagemResultado_StatusCnt ;
      private IDataStoreProvider pr_default ;
      private int[] H00BF2_A470ContagemResultado_ContadorFMCod ;
      private int[] H00BF2_A479ContagemResultado_CrFMPessoaCod ;
      private bool[] H00BF2_n479ContagemResultado_CrFMPessoaCod ;
      private bool[] H00BF2_A517ContagemResultado_Ultima ;
      private String[] H00BF2_A853ContagemResultado_NomePla ;
      private bool[] H00BF2_n853ContagemResultado_NomePla ;
      private String[] H00BF2_A474ContagemResultado_ContadorFMNom ;
      private bool[] H00BF2_n474ContagemResultado_ContadorFMNom ;
      private short[] H00BF2_A483ContagemResultado_StatusCnt ;
      private decimal[] H00BF2_A462ContagemResultado_Divergencia ;
      private decimal[] H00BF2_A800ContagemResultado_Deflator ;
      private bool[] H00BF2_n800ContagemResultado_Deflator ;
      private decimal[] H00BF2_A461ContagemResultado_PFLFM ;
      private bool[] H00BF2_n461ContagemResultado_PFLFM ;
      private decimal[] H00BF2_A460ContagemResultado_PFBFM ;
      private bool[] H00BF2_n460ContagemResultado_PFBFM ;
      private decimal[] H00BF2_A459ContagemResultado_PFLFS ;
      private bool[] H00BF2_n459ContagemResultado_PFLFS ;
      private decimal[] H00BF2_A458ContagemResultado_PFBFS ;
      private bool[] H00BF2_n458ContagemResultado_PFBFS ;
      private String[] H00BF2_A511ContagemResultado_HoraCnt ;
      private DateTime[] H00BF2_A473ContagemResultado_DataCnt ;
      private int[] H00BF2_A456ContagemResultado_Codigo ;
      private long[] H00BF3_AGRID_nRecordCount ;
      private int[] H00BF4_A456ContagemResultado_Codigo ;
      private int[] H00BF4_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00BF4_n1553ContagemResultado_CntSrvCod ;
      private int[] H00BF5_A2110ContratoServicosUnidConversao_Codigo ;
      private int[] H00BF5_A160ContratoServicos_Codigo ;
      private String[] H00BF5_A2112ContratoServicosUnidConversao_Sigla ;
      private bool[] H00BF5_n2112ContratoServicosUnidConversao_Sigla ;
      private decimal[] H00BF5_A2114ContratoServicosUnidConversao_Conversao ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
   }

   public class contagemresultadocontagemresultadocontagenswc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00BF2( IGxContext context ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A456ContagemResultado_Codigo ,
                                             int AV7ContagemResultado_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [6] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[ContagemResultado_ContadorFMCod] AS ContagemResultado_ContadorFMCod, T2.[Usuario_PessoaCod] AS ContagemResultado_CrFMPessoaCod, T1.[ContagemResultado_Ultima], T1.[ContagemResultado_NomePla], T3.[Pessoa_Nome] AS ContagemResultado_ContadorFMNom, T1.[ContagemResultado_StatusCnt], T1.[ContagemResultado_Divergencia], T1.[ContagemResultado_Deflator], T1.[ContagemResultado_PFLFM], T1.[ContagemResultado_PFBFM], T1.[ContagemResultado_PFLFS], T1.[ContagemResultado_PFBFS], T1.[ContagemResultado_HoraCnt], T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_Codigo]";
         sFromString = " FROM (([ContagemResultadoContagens] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContagemResultado_ContadorFMCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[ContagemResultado_Codigo] = @AV7ContagemResultado_Codigo)";
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Codigo], T1.[ContagemResultado_DataCnt]";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Codigo] DESC, T1.[ContagemResultado_DataCnt] DESC";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Codigo], T1.[ContagemResultado_HoraCnt]";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Codigo] DESC, T1.[ContagemResultado_HoraCnt] DESC";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Codigo], T1.[ContagemResultado_PFBFS]";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Codigo] DESC, T1.[ContagemResultado_PFBFS] DESC";
         }
         else if ( ( AV14OrderedBy == 4 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Codigo], T1.[ContagemResultado_PFLFS]";
         }
         else if ( ( AV14OrderedBy == 4 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Codigo] DESC, T1.[ContagemResultado_PFLFS] DESC";
         }
         else if ( ( AV14OrderedBy == 5 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Codigo], T1.[ContagemResultado_PFBFM]";
         }
         else if ( ( AV14OrderedBy == 5 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Codigo] DESC, T1.[ContagemResultado_PFBFM] DESC";
         }
         else if ( ( AV14OrderedBy == 6 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Codigo], T1.[ContagemResultado_PFLFM]";
         }
         else if ( ( AV14OrderedBy == 6 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Codigo] DESC, T1.[ContagemResultado_PFLFM] DESC";
         }
         else if ( ( AV14OrderedBy == 7 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Codigo], T1.[ContagemResultado_Deflator]";
         }
         else if ( ( AV14OrderedBy == 7 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Codigo] DESC, T1.[ContagemResultado_Deflator] DESC";
         }
         else if ( ( AV14OrderedBy == 8 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Codigo], T1.[ContagemResultado_Divergencia]";
         }
         else if ( ( AV14OrderedBy == 8 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Codigo] DESC, T1.[ContagemResultado_Divergencia] DESC";
         }
         else if ( ( AV14OrderedBy == 9 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Codigo], T1.[ContagemResultado_StatusCnt]";
         }
         else if ( ( AV14OrderedBy == 9 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Codigo] DESC, T1.[ContagemResultado_StatusCnt] DESC";
         }
         else if ( ( AV14OrderedBy == 10 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Codigo], T3.[Pessoa_Nome]";
         }
         else if ( ( AV14OrderedBy == 10 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Codigo] DESC, T3.[Pessoa_Nome] DESC";
         }
         else if ( ( AV14OrderedBy == 11 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Codigo], T1.[ContagemResultado_NomePla]";
         }
         else if ( ( AV14OrderedBy == 11 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Codigo] DESC, T1.[ContagemResultado_NomePla] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Codigo], T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_HoraCnt]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00BF3( IGxContext context ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A456ContagemResultado_Codigo ,
                                             int AV7ContagemResultado_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [1] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM (([ContagemResultadoContagens] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContagemResultado_ContadorFMCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContagemResultado_Codigo] = @AV7ContagemResultado_Codigo)";
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 4 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 4 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 5 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 5 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 6 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 6 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 7 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 7 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 8 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 8 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 9 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 9 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 10 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 10 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 11 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 11 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00BF2(context, (short)dynConstraints[0] , (bool)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] );
               case 1 :
                     return conditional_H00BF3(context, (short)dynConstraints[0] , (bool)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00BF4 ;
          prmH00BF4 = new Object[] {
          new Object[] {"@AV7ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BF5 ;
          prmH00BF5 = new Object[] {
          new Object[] {"@AV92ContagemResultado_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BF2 ;
          prmH00BF2 = new Object[] {
          new Object[] {"@AV7ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00BF3 ;
          prmH00BF3 = new Object[] {
          new Object[] {"@AV7ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00BF2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BF2,11,0,true,false )
             ,new CursorDef("H00BF3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BF3,1,0,true,false )
             ,new CursorDef("H00BF4", "SELECT [ContagemResultado_Codigo], [ContagemResultado_CntSrvCod] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @AV7ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BF4,1,0,false,true )
             ,new CursorDef("H00BF5", "SELECT TOP 1 T1.[ContratoServicosUnidConversao_Codigo] AS ContratoServicosUnidConversao_Codigo, T1.[ContratoServicos_Codigo], T2.[UnidadeMedicao_Sigla] AS ContratoServicosUnidConversao_Sigla, T1.[ContratoServicosUnidConversao_Conversao] FROM ([ContratoServicosUnidConversao] T1 WITH (NOLOCK) INNER JOIN [UnidadeMedicao] T2 WITH (NOLOCK) ON T2.[UnidadeMedicao_Codigo] = T1.[ContratoServicosUnidConversao_Codigo]) WHERE T1.[ContratoServicos_Codigo] = @AV92ContagemResultado_CntSrvCod ORDER BY T1.[ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BF5,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((short[]) buf[8])[0] = rslt.getShort(6) ;
                ((decimal[]) buf[9])[0] = rslt.getDecimal(7) ;
                ((decimal[]) buf[10])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((decimal[]) buf[14])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((decimal[]) buf[16])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((decimal[]) buf[18])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((String[]) buf[20])[0] = rslt.getString(13, 5) ;
                ((DateTime[]) buf[21])[0] = rslt.getGXDate(14) ;
                ((int[]) buf[22])[0] = rslt.getInt(15) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[1]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
