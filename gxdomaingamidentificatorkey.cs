/*
               File: GAMIdentificatorKey
        Description: GAMIdentificatorKey
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 21:56:34.91
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gxdomaingamidentificatorkey
   {
      private static Hashtable domain = new Hashtable();
      static gxdomaingamidentificatorkey ()
      {
         domain["Repository"] = "Repository";
         domain["SecurityPolicy"] = "Security Policy";
         domain["Role"] = "Role";
         domain["AppType"] = "Application Type";
         domain["AppTypeResource"] = "Application Type Resource";
         domain["AppTypeAction"] = "Application Type Action";
         domain["Application"] = "Application";
         domain["AppEnv"] = "Application Environment";
         domain["AppMenu"] = "Application Menu";
      }

      public static string getDescription( IGxContext context ,
                                           String key )
      {
         string rtkey ;
         rtkey = StringUtil.Trim( (String)(key));
         return (string)domain[rtkey] ;
      }

      public static GxSimpleCollection getValues( )
      {
         GxSimpleCollection value = new GxSimpleCollection();
         ArrayList aKeys = new ArrayList(domain.Keys);
         aKeys.Sort();
         foreach (String key in aKeys)
         {
            value.Add(key);
         }
         return value;
      }

   }

}
