/*
               File: WP_ConsultaContadoresGrf
        Description:
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:21:20.29
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_consultacontadoresgrf : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_consultacontadoresgrf( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_consultacontadoresgrf( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_AreaTrabalho_Codigo ,
                           int aP1_ContagemResultado_ContadorFMCod ,
                           short aP2_Filtro_Ano ,
                           long aP3_Filtro_Mes ,
                           String aP4_Graficar ,
                           int aP5_Servico_Codigo ,
                           ref bool aP6_UserEhContratada ,
                           ref bool aP7_UserEhContratante ,
                           ref DateTime aP8_DataCnt ,
                           ref int aP9_Contratada_PessoaCod )
      {
         this.AV13AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV5ContagemResultado_ContadorFMCod = aP1_ContagemResultado_ContadorFMCod;
         this.AV6Filtro_Ano = aP2_Filtro_Ano;
         this.AV7Filtro_Mes = aP3_Filtro_Mes;
         this.AV31Graficar = aP4_Graficar;
         this.AV32Servico_Codigo = aP5_Servico_Codigo;
         this.AV33UserEhContratada = aP6_UserEhContratada;
         this.AV35UserEhContratante = aP7_UserEhContratante;
         this.AV41DataCnt = aP8_DataCnt;
         this.AV38Contratada_PessoaCod = aP9_Contratada_PessoaCod;
         executePrivate();
         aP6_UserEhContratada=this.AV33UserEhContratada;
         aP7_UserEhContratante=this.AV35UserEhContratante;
         aP8_DataCnt=this.AV41DataCnt;
         aP9_Contratada_PessoaCod=this.AV38Contratada_PessoaCod;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV13AreaTrabalho_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13AreaTrabalho_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vAREATRABALHO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV13AreaTrabalho_Codigo), "ZZZZZ9")));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV5ContagemResultado_ContadorFMCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5ContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5ContagemResultado_ContadorFMCod), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_CONTADORFMCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV5ContagemResultado_ContadorFMCod), "ZZZZZ9")));
                  AV6Filtro_Ano = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Filtro_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6Filtro_Ano), 4, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vFILTRO_ANO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV6Filtro_Ano), "ZZZ9")));
                  AV7Filtro_Mes = (long)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Filtro_Mes", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Filtro_Mes), 10, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vFILTRO_MES", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Filtro_Mes), "ZZZZZZZZZ9")));
                  AV31Graficar = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Graficar", AV31Graficar);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vGRAFICAR", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV31Graficar, ""))));
                  AV32Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32Servico_Codigo), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSERVICO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV32Servico_Codigo), "ZZZZZ9")));
                  AV33UserEhContratada = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33UserEhContratada", AV33UserEhContratada);
                  AV35UserEhContratante = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35UserEhContratante", AV35UserEhContratante);
                  AV41DataCnt = context.localUtil.ParseDateParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41DataCnt", context.localUtil.Format(AV41DataCnt, "99/99/99"));
                  AV38Contratada_PessoaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Contratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Contratada_PessoaCod), 6, 0)));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PABV2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTBV2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206216212031");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("GxChart/gxChart.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_consultacontadoresgrf.aspx") + "?" + UrlEncode("" +AV13AreaTrabalho_Codigo) + "," + UrlEncode("" +AV5ContagemResultado_ContadorFMCod) + "," + UrlEncode("" +AV6Filtro_Ano) + "," + UrlEncode("" +AV7Filtro_Mes) + "," + UrlEncode(StringUtil.RTrim(AV31Graficar)) + "," + UrlEncode("" +AV32Servico_Codigo) + "," + UrlEncode(StringUtil.BoolToStr(AV33UserEhContratada)) + "," + UrlEncode(StringUtil.BoolToStr(AV35UserEhContratante)) + "," + UrlEncode(DateTimeUtil.FormatDateParm(AV41DataCnt)) + "," + UrlEncode("" +AV38Contratada_PessoaCod)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGXCHARTDATA", GxChartData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGXCHARTDATA", GxChartData);
         }
         GxWebStd.gx_hidden_field( context, "vAREATRABALHO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13AreaTrabalho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_CONTADORFMCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV5ContagemResultado_ContadorFMCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vFILTRO_ANO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV6Filtro_Ano), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vFILTRO_MES", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Filtro_Mes), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRAFICAR", StringUtil.RTrim( AV31Graficar));
         GxWebStd.gx_hidden_field( context, "vSERVICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32Servico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "vUSEREHCONTRATADA", AV33UserEhContratada);
         GxWebStd.gx_boolean_hidden_field( context, "vUSEREHCONTRATANTE", AV35UserEhContratante);
         GxWebStd.gx_hidden_field( context, "vDATACNT", context.localUtil.DToC( AV41DataCnt, 0, "/"));
         GxWebStd.gx_hidden_field( context, "vCONTRATADA_PESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38Contratada_PessoaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vAREATRABALHO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV13AreaTrabalho_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_CONTADORFMCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV5ContagemResultado_ContadorFMCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vFILTRO_ANO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV6Filtro_Ano), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vFILTRO_MES", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Filtro_Mes), "ZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vGRAFICAR", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV31Graficar, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vSERVICO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV32Servico_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vAREATRABALHO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV13AreaTrabalho_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_CONTADORFMCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV5ContagemResultado_ContadorFMCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vFILTRO_ANO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV6Filtro_Ano), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vFILTRO_MES", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Filtro_Mes), "ZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vGRAFICAR", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV31Graficar, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vSERVICO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV32Servico_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "GXCHARTCONTROL1_Title", StringUtil.RTrim( Gxchartcontrol1_Title));
         GxWebStd.gx_hidden_field( context, "GXCHARTCONTROL1_Width", StringUtil.RTrim( Gxchartcontrol1_Width));
         GxWebStd.gx_hidden_field( context, "GXCHARTCONTROL1_Height", StringUtil.RTrim( Gxchartcontrol1_Height));
         GxWebStd.gx_hidden_field( context, "GXCHARTCONTROL1_Charttype", StringUtil.RTrim( Gxchartcontrol1_Charttype));
         GxWebStd.gx_hidden_field( context, "GXCHARTCONTROL1_X_axistitle", StringUtil.RTrim( Gxchartcontrol1_X_axistitle));
         GxWebStd.gx_hidden_field( context, "GXCHARTCONTROL1_Y_axistitle", StringUtil.RTrim( Gxchartcontrol1_Y_axistitle));
         GxWebStd.gx_hidden_field( context, "GXCHARTCONTROL1_Backgroundcolor1", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gxchartcontrol1_Backgroundcolor1), 9, 0, ".", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "</form>") ;
         }
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEBV2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTBV2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_consultacontadoresgrf.aspx") + "?" + UrlEncode("" +AV13AreaTrabalho_Codigo) + "," + UrlEncode("" +AV5ContagemResultado_ContadorFMCod) + "," + UrlEncode("" +AV6Filtro_Ano) + "," + UrlEncode("" +AV7Filtro_Mes) + "," + UrlEncode(StringUtil.RTrim(AV31Graficar)) + "," + UrlEncode("" +AV32Servico_Codigo) + "," + UrlEncode(StringUtil.BoolToStr(AV33UserEhContratada)) + "," + UrlEncode(StringUtil.BoolToStr(AV35UserEhContratante)) + "," + UrlEncode(DateTimeUtil.FormatDateParm(AV41DataCnt)) + "," + UrlEncode("" +AV38Contratada_PessoaCod) ;
      }

      public override String GetPgmname( )
      {
         return "WP_ConsultaContadoresGrf" ;
      }

      public override String GetPgmdesc( )
      {
         return "" ;
      }

      protected void WBBV0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_BV2( true) ;
         }
         else
         {
            wb_table1_2_BV2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_BV2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTBV2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPBV0( ) ;
      }

      protected void WSBV2( )
      {
         STARTBV2( ) ;
         EVTBV2( ) ;
      }

      protected void EVTBV2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11BV2 */
                              E11BV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12BV2 */
                              E12BV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEBV2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PABV2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFBV2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      protected void RFBV2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E12BV2 */
            E12BV2 ();
            WBBV0( ) ;
         }
      }

      protected void STRUPBV0( )
      {
         /* Before Start, stand alone formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11BV2 */
         E11BV2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vGXCHARTDATA"), GxChartData);
            /* Read variables values. */
            /* Read saved values. */
            Gxchartcontrol1_Title = cgiGet( "GXCHARTCONTROL1_Title");
            Gxchartcontrol1_Width = cgiGet( "GXCHARTCONTROL1_Width");
            Gxchartcontrol1_Height = cgiGet( "GXCHARTCONTROL1_Height");
            Gxchartcontrol1_Charttype = cgiGet( "GXCHARTCONTROL1_Charttype");
            Gxchartcontrol1_X_axistitle = cgiGet( "GXCHARTCONTROL1_X_axistitle");
            Gxchartcontrol1_Y_axistitle = cgiGet( "GXCHARTCONTROL1_Y_axistitle");
            Gxchartcontrol1_Backgroundcolor1 = (int)(context.localUtil.CToN( cgiGet( "GXCHARTCONTROL1_Backgroundcolor1"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11BV2 */
         E11BV2 ();
         if (returnInSub) return;
      }

      protected void E11BV2( )
      {
         /* Start Routine */
         Gxchartcontrol1_Title = "Consulta de Contadores Ano "+StringUtil.Str( (decimal)(AV6Filtro_Ano), 4, 0);
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Gxchartcontrol1_Internalname, "Title", Gxchartcontrol1_Title);
         if ( (0==AV7Filtro_Mes) )
         {
            GxChartData.gxTpr_Categories.Add("Janeiro", 0);
            GxChartData.gxTpr_Categories.Add("Fevereiro", 0);
            GxChartData.gxTpr_Categories.Add("Mar�o", 0);
            GxChartData.gxTpr_Categories.Add("Abril", 0);
            GxChartData.gxTpr_Categories.Add("Maio", 0);
            GxChartData.gxTpr_Categories.Add("Junho", 0);
            GxChartData.gxTpr_Categories.Add("Julho", 0);
            GxChartData.gxTpr_Categories.Add("Agosto", 0);
            GxChartData.gxTpr_Categories.Add("Setembro", 0);
            GxChartData.gxTpr_Categories.Add("Outubro", 0);
            GxChartData.gxTpr_Categories.Add("Novembro", 0);
            GxChartData.gxTpr_Categories.Add("Dezembro", 0);
         }
         else
         {
            GxChartData.gxTpr_Categories.Add(DateTimeUtil.CMonth( Gx_date, "por"), 0);
         }
         AV36Contratadas.FromXml(AV37websession.Get("Contratadas"), "Collection");
         AV37websession.Remove("Contratadas");
         if ( StringUtil.StrCmp(AV31Graficar, "DMN") == 0 )
         {
            Gxchartcontrol1_X_axistitle = "Demandas";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Gxchartcontrol1_Internalname, "X_AxisTitle", Gxchartcontrol1_X_axistitle);
         }
         else if ( StringUtil.StrCmp(AV31Graficar, "PFB") == 0 )
         {
            Gxchartcontrol1_X_axistitle = "Pontos de Fun��o Bruto";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Gxchartcontrol1_Internalname, "X_AxisTitle", Gxchartcontrol1_X_axistitle);
         }
         /* Execute user subroutine: 'GRAFICAR' */
         S112 ();
         if (returnInSub) return;
      }

      protected void S112( )
      {
         /* 'GRAFICAR' Routine */
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A490ContagemResultado_ContratadaCod ,
                                              AV36Contratadas ,
                                              AV35UserEhContratante ,
                                              AV40ContagemResultado_Data ,
                                              AV7Filtro_Mes ,
                                              AV41DataCnt ,
                                              AV32Servico_Codigo ,
                                              A999ContagemResultado_CrFMEhContratada ,
                                              A1000ContagemResultado_CrFMEhContratante ,
                                              A473ContagemResultado_DataCnt ,
                                              AV6Filtro_Ano ,
                                              A601ContagemResultado_Servico ,
                                              A484ContagemResultado_StatusDmn ,
                                              A517ContagemResultado_Ultima ,
                                              A470ContagemResultado_ContadorFMCod ,
                                              AV5ContagemResultado_ContadorFMCod },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.LONG, TypeConstants.DATE, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN,
                                              TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         /* Using cursor H00BV2 */
         pr_default.execute(0, new Object[] {AV5ContagemResultado_ContadorFMCod, AV6Filtro_Ano, AV7Filtro_Mes, AV41DataCnt, AV32Servico_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKBV3 = false;
            A456ContagemResultado_Codigo = H00BV2_A456ContagemResultado_Codigo[0];
            A1553ContagemResultado_CntSrvCod = H00BV2_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = H00BV2_n1553ContagemResultado_CntSrvCod[0];
            A479ContagemResultado_CrFMPessoaCod = H00BV2_A479ContagemResultado_CrFMPessoaCod[0];
            n479ContagemResultado_CrFMPessoaCod = H00BV2_n479ContagemResultado_CrFMPessoaCod[0];
            A470ContagemResultado_ContadorFMCod = H00BV2_A470ContagemResultado_ContadorFMCod[0];
            A474ContagemResultado_ContadorFMNom = H00BV2_A474ContagemResultado_ContadorFMNom[0];
            n474ContagemResultado_ContadorFMNom = H00BV2_n474ContagemResultado_ContadorFMNom[0];
            A473ContagemResultado_DataCnt = H00BV2_A473ContagemResultado_DataCnt[0];
            A458ContagemResultado_PFBFS = H00BV2_A458ContagemResultado_PFBFS[0];
            n458ContagemResultado_PFBFS = H00BV2_n458ContagemResultado_PFBFS[0];
            A460ContagemResultado_PFBFM = H00BV2_A460ContagemResultado_PFBFM[0];
            n460ContagemResultado_PFBFM = H00BV2_n460ContagemResultado_PFBFM[0];
            A468ContagemResultado_NaoCnfDmnCod = H00BV2_A468ContagemResultado_NaoCnfDmnCod[0];
            n468ContagemResultado_NaoCnfDmnCod = H00BV2_n468ContagemResultado_NaoCnfDmnCod[0];
            A469ContagemResultado_NaoCnfCntCod = H00BV2_A469ContagemResultado_NaoCnfCntCod[0];
            n469ContagemResultado_NaoCnfCntCod = H00BV2_n469ContagemResultado_NaoCnfCntCod[0];
            A462ContagemResultado_Divergencia = H00BV2_A462ContagemResultado_Divergencia[0];
            A490ContagemResultado_ContratadaCod = H00BV2_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00BV2_n490ContagemResultado_ContratadaCod[0];
            A484ContagemResultado_StatusDmn = H00BV2_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00BV2_n484ContagemResultado_StatusDmn[0];
            A517ContagemResultado_Ultima = H00BV2_A517ContagemResultado_Ultima[0];
            A601ContagemResultado_Servico = H00BV2_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00BV2_n601ContagemResultado_Servico[0];
            A1000ContagemResultado_CrFMEhContratante = H00BV2_A1000ContagemResultado_CrFMEhContratante[0];
            n1000ContagemResultado_CrFMEhContratante = H00BV2_n1000ContagemResultado_CrFMEhContratante[0];
            A999ContagemResultado_CrFMEhContratada = H00BV2_A999ContagemResultado_CrFMEhContratada[0];
            n999ContagemResultado_CrFMEhContratada = H00BV2_n999ContagemResultado_CrFMEhContratada[0];
            A1553ContagemResultado_CntSrvCod = H00BV2_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = H00BV2_n1553ContagemResultado_CntSrvCod[0];
            A468ContagemResultado_NaoCnfDmnCod = H00BV2_A468ContagemResultado_NaoCnfDmnCod[0];
            n468ContagemResultado_NaoCnfDmnCod = H00BV2_n468ContagemResultado_NaoCnfDmnCod[0];
            A490ContagemResultado_ContratadaCod = H00BV2_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00BV2_n490ContagemResultado_ContratadaCod[0];
            A484ContagemResultado_StatusDmn = H00BV2_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00BV2_n484ContagemResultado_StatusDmn[0];
            A601ContagemResultado_Servico = H00BV2_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00BV2_n601ContagemResultado_Servico[0];
            A479ContagemResultado_CrFMPessoaCod = H00BV2_A479ContagemResultado_CrFMPessoaCod[0];
            n479ContagemResultado_CrFMPessoaCod = H00BV2_n479ContagemResultado_CrFMPessoaCod[0];
            A1000ContagemResultado_CrFMEhContratante = H00BV2_A1000ContagemResultado_CrFMEhContratante[0];
            n1000ContagemResultado_CrFMEhContratante = H00BV2_n1000ContagemResultado_CrFMEhContratante[0];
            A999ContagemResultado_CrFMEhContratada = H00BV2_A999ContagemResultado_CrFMEhContratada[0];
            n999ContagemResultado_CrFMEhContratada = H00BV2_n999ContagemResultado_CrFMEhContratada[0];
            A474ContagemResultado_ContadorFMNom = H00BV2_A474ContagemResultado_ContadorFMNom[0];
            n474ContagemResultado_ContadorFMNom = H00BV2_n474ContagemResultado_ContadorFMNom[0];
            GxChartSerie = new SdtGxChart_Serie(context);
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV16mQtdeDmn[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV17mPFBFM[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV18mPFBFS[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV19mCTPF[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV20mRTPF[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV21mPendencias[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV22mDivergencias[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(H00BV2_A474ContagemResultado_ContadorFMNom[0], A474ContagemResultado_ContadorFMNom) == 0 ) )
            {
               BRKBV3 = false;
               A456ContagemResultado_Codigo = H00BV2_A456ContagemResultado_Codigo[0];
               A479ContagemResultado_CrFMPessoaCod = H00BV2_A479ContagemResultado_CrFMPessoaCod[0];
               n479ContagemResultado_CrFMPessoaCod = H00BV2_n479ContagemResultado_CrFMPessoaCod[0];
               A470ContagemResultado_ContadorFMCod = H00BV2_A470ContagemResultado_ContadorFMCod[0];
               A473ContagemResultado_DataCnt = H00BV2_A473ContagemResultado_DataCnt[0];
               A458ContagemResultado_PFBFS = H00BV2_A458ContagemResultado_PFBFS[0];
               n458ContagemResultado_PFBFS = H00BV2_n458ContagemResultado_PFBFS[0];
               A460ContagemResultado_PFBFM = H00BV2_A460ContagemResultado_PFBFM[0];
               n460ContagemResultado_PFBFM = H00BV2_n460ContagemResultado_PFBFM[0];
               A468ContagemResultado_NaoCnfDmnCod = H00BV2_A468ContagemResultado_NaoCnfDmnCod[0];
               n468ContagemResultado_NaoCnfDmnCod = H00BV2_n468ContagemResultado_NaoCnfDmnCod[0];
               A469ContagemResultado_NaoCnfCntCod = H00BV2_A469ContagemResultado_NaoCnfCntCod[0];
               n469ContagemResultado_NaoCnfCntCod = H00BV2_n469ContagemResultado_NaoCnfCntCod[0];
               A462ContagemResultado_Divergencia = H00BV2_A462ContagemResultado_Divergencia[0];
               A490ContagemResultado_ContratadaCod = H00BV2_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = H00BV2_n490ContagemResultado_ContratadaCod[0];
               A484ContagemResultado_StatusDmn = H00BV2_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = H00BV2_n484ContagemResultado_StatusDmn[0];
               A517ContagemResultado_Ultima = H00BV2_A517ContagemResultado_Ultima[0];
               A468ContagemResultado_NaoCnfDmnCod = H00BV2_A468ContagemResultado_NaoCnfDmnCod[0];
               n468ContagemResultado_NaoCnfDmnCod = H00BV2_n468ContagemResultado_NaoCnfDmnCod[0];
               A490ContagemResultado_ContratadaCod = H00BV2_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = H00BV2_n490ContagemResultado_ContratadaCod[0];
               A484ContagemResultado_StatusDmn = H00BV2_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = H00BV2_n484ContagemResultado_StatusDmn[0];
               A479ContagemResultado_CrFMPessoaCod = H00BV2_A479ContagemResultado_CrFMPessoaCod[0];
               n479ContagemResultado_CrFMPessoaCod = H00BV2_n479ContagemResultado_CrFMPessoaCod[0];
               if ( (AV36Contratadas.IndexOf(A490ContagemResultado_ContratadaCod)>0) )
               {
                  AV10i = (short)(DateTimeUtil.Month( A473ContagemResultado_DataCnt));
                  AV16mQtdeDmn[AV10i-1] = (short)(AV16mQtdeDmn[AV10i-1]+1);
                  AV18mPFBFS[AV10i-1] = (decimal)(AV18mPFBFS[AV10i-1]+A458ContagemResultado_PFBFS);
                  AV17mPFBFM[AV10i-1] = (decimal)(AV17mPFBFM[AV10i-1]+A460ContagemResultado_PFBFM);
                  AV19mCTPF[AV10i-1] = (decimal)(AV19mCTPF[AV10i-1]+(A460ContagemResultado_PFBFM*AV15CUPF));
                  AV20mRTPF[AV10i-1] = (decimal)(AV20mRTPF[AV10i-1]+(A460ContagemResultado_PFBFM*AV15CUPF));
                  if ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "A") == 0 )
                  {
                     if ( ! (0==A469ContagemResultado_NaoCnfCntCod) || ! (0==A468ContagemResultado_NaoCnfDmnCod) )
                     {
                        AV21mPendencias[AV10i-1] = (short)(AV21mPendencias[AV10i-1]+1);
                     }
                     if ( A462ContagemResultado_Divergencia > AV14Contrato_IndiceDivergencia )
                     {
                        AV22mDivergencias[AV10i-1] = (short)(AV22mDivergencias[AV10i-1]+1);
                     }
                  }
               }
               BRKBV3 = true;
               pr_default.readNext(0);
            }
            if ( (0==AV7Filtro_Mes) )
            {
               AV10i = 1;
               while ( AV10i <= 12 )
               {
                  if ( StringUtil.StrCmp(AV31Graficar, "DMN") == 0 )
                  {
                     GxChartSerie.gxTpr_Values.Add((decimal)(AV16mQtdeDmn[AV10i-1]), 0);
                  }
                  else if ( StringUtil.StrCmp(AV31Graficar, "PFB") == 0 )
                  {
                     GxChartSerie.gxTpr_Values.Add(AV17mPFBFM[AV10i-1], 0);
                  }
                  AV10i = (short)(AV10i+1);
               }
            }
            else
            {
               if ( StringUtil.StrCmp(AV31Graficar, "DMN") == 0 )
               {
                  GxChartSerie.gxTpr_Values.Add((decimal)(AV16mQtdeDmn[(int)(AV7Filtro_Mes)-1]), 0);
               }
               else if ( StringUtil.StrCmp(AV31Graficar, "PFB") == 0 )
               {
                  GxChartSerie.gxTpr_Values.Add(AV17mPFBFM[(int)(AV7Filtro_Mes)-1], 0);
               }
            }
            AV10i = (short)(GxChartData.gxTpr_Series.Count+1);
            GxChartSerie.gxTpr_Name = StringUtil.Trim( StringUtil.Str( (decimal)(AV10i), 4, 0))+" - "+A474ContagemResultado_ContadorFMNom;
            GxChartData.gxTpr_Series.Add(GxChartSerie, 0);
            if ( ! BRKBV3 )
            {
               BRKBV3 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void nextLoad( )
      {
      }

      protected void E12BV2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_BV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " height: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GXCHARTCONTROL1Container"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_BV2e( true) ;
         }
         else
         {
            wb_table1_2_BV2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV13AreaTrabalho_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13AreaTrabalho_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vAREATRABALHO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV13AreaTrabalho_Codigo), "ZZZZZ9")));
         AV5ContagemResultado_ContadorFMCod = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5ContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5ContagemResultado_ContadorFMCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_CONTADORFMCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV5ContagemResultado_ContadorFMCod), "ZZZZZ9")));
         AV6Filtro_Ano = Convert.ToInt16(getParm(obj,2));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Filtro_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6Filtro_Ano), 4, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vFILTRO_ANO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV6Filtro_Ano), "ZZZ9")));
         AV7Filtro_Mes = Convert.ToInt64(getParm(obj,3));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Filtro_Mes", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Filtro_Mes), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vFILTRO_MES", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Filtro_Mes), "ZZZZZZZZZ9")));
         AV31Graficar = (String)getParm(obj,4);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Graficar", AV31Graficar);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vGRAFICAR", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV31Graficar, ""))));
         AV32Servico_Codigo = Convert.ToInt32(getParm(obj,5));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32Servico_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSERVICO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV32Servico_Codigo), "ZZZZZ9")));
         AV33UserEhContratada = (bool)getParm(obj,6);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33UserEhContratada", AV33UserEhContratada);
         AV35UserEhContratante = (bool)getParm(obj,7);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35UserEhContratante", AV35UserEhContratante);
         AV41DataCnt = (DateTime)getParm(obj,8);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41DataCnt", context.localUtil.Format(AV41DataCnt, "99/99/99"));
         AV38Contratada_PessoaCod = Convert.ToInt32(getParm(obj,9));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Contratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Contratada_PessoaCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PABV2( ) ;
         WSBV2( ) ;
         WEBV2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20206216212059");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
            context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
            context.AddJavascriptSource("wp_consultacontadoresgrf.js", "?20206216212059");
            context.AddJavascriptSource("GxChart/gxChart.js", "");
         }
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         Gxchartcontrol1_Internalname = "GXCHARTCONTROL1";
         tblTable1_Internalname = "TABLE1";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Gxchartcontrol1_Backgroundcolor1 = (int)(0xD3D3D3);
         Gxchartcontrol1_Y_axistitle = "Meses";
         Gxchartcontrol1_X_axistitle = "Demandas";
         Gxchartcontrol1_Charttype = "BAR";
         Gxchartcontrol1_Height = "500";
         Gxchartcontrol1_Width = "900";
         Gxchartcontrol1_Title = "Consulta de Contadores";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "";
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV31Graficar = "";
         wcpOAV41DataCnt = DateTime.MinValue;
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GxChartData = new SdtGxChart(context);
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         Gx_date = DateTime.MinValue;
         AV36Contratadas = new GxSimpleCollection();
         AV37websession = context.GetSession();
         scmdbuf = "";
         AV40ContagemResultado_Data = DateTime.MinValue;
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A484ContagemResultado_StatusDmn = "";
         H00BV2_A511ContagemResultado_HoraCnt = new String[] {""} ;
         H00BV2_A456ContagemResultado_Codigo = new int[1] ;
         H00BV2_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00BV2_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00BV2_A479ContagemResultado_CrFMPessoaCod = new int[1] ;
         H00BV2_n479ContagemResultado_CrFMPessoaCod = new bool[] {false} ;
         H00BV2_A470ContagemResultado_ContadorFMCod = new int[1] ;
         H00BV2_A474ContagemResultado_ContadorFMNom = new String[] {""} ;
         H00BV2_n474ContagemResultado_ContadorFMNom = new bool[] {false} ;
         H00BV2_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         H00BV2_A458ContagemResultado_PFBFS = new decimal[1] ;
         H00BV2_n458ContagemResultado_PFBFS = new bool[] {false} ;
         H00BV2_A460ContagemResultado_PFBFM = new decimal[1] ;
         H00BV2_n460ContagemResultado_PFBFM = new bool[] {false} ;
         H00BV2_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         H00BV2_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         H00BV2_A469ContagemResultado_NaoCnfCntCod = new int[1] ;
         H00BV2_n469ContagemResultado_NaoCnfCntCod = new bool[] {false} ;
         H00BV2_A462ContagemResultado_Divergencia = new decimal[1] ;
         H00BV2_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00BV2_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00BV2_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00BV2_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00BV2_A517ContagemResultado_Ultima = new bool[] {false} ;
         H00BV2_A601ContagemResultado_Servico = new int[1] ;
         H00BV2_n601ContagemResultado_Servico = new bool[] {false} ;
         H00BV2_A1000ContagemResultado_CrFMEhContratante = new bool[] {false} ;
         H00BV2_n1000ContagemResultado_CrFMEhContratante = new bool[] {false} ;
         H00BV2_A999ContagemResultado_CrFMEhContratada = new bool[] {false} ;
         H00BV2_n999ContagemResultado_CrFMEhContratada = new bool[] {false} ;
         A474ContagemResultado_ContadorFMNom = "";
         GxChartSerie = new SdtGxChart_Serie(context);
         AV16mQtdeDmn = new short [12] ;
         AV17mPFBFM = new decimal [12] ;
         AV18mPFBFS = new decimal [12] ;
         AV19mCTPF = new decimal [12] ;
         AV20mRTPF = new decimal [12] ;
         AV21mPendencias = new short [12] ;
         AV22mDivergencias = new short [12] ;
         AV14Contrato_IndiceDivergencia = (decimal)(10);
         sStyleString = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_consultacontadoresgrf__default(),
            new Object[][] {
                new Object[] {
               H00BV2_A511ContagemResultado_HoraCnt, H00BV2_A456ContagemResultado_Codigo, H00BV2_A1553ContagemResultado_CntSrvCod, H00BV2_n1553ContagemResultado_CntSrvCod, H00BV2_A479ContagemResultado_CrFMPessoaCod, H00BV2_n479ContagemResultado_CrFMPessoaCod, H00BV2_A470ContagemResultado_ContadorFMCod, H00BV2_A474ContagemResultado_ContadorFMNom, H00BV2_n474ContagemResultado_ContadorFMNom, H00BV2_A473ContagemResultado_DataCnt,
               H00BV2_A458ContagemResultado_PFBFS, H00BV2_n458ContagemResultado_PFBFS, H00BV2_A460ContagemResultado_PFBFM, H00BV2_n460ContagemResultado_PFBFM, H00BV2_A468ContagemResultado_NaoCnfDmnCod, H00BV2_n468ContagemResultado_NaoCnfDmnCod, H00BV2_A469ContagemResultado_NaoCnfCntCod, H00BV2_n469ContagemResultado_NaoCnfCntCod, H00BV2_A462ContagemResultado_Divergencia, H00BV2_A490ContagemResultado_ContratadaCod,
               H00BV2_n490ContagemResultado_ContratadaCod, H00BV2_A484ContagemResultado_StatusDmn, H00BV2_n484ContagemResultado_StatusDmn, H00BV2_A517ContagemResultado_Ultima, H00BV2_A601ContagemResultado_Servico, H00BV2_n601ContagemResultado_Servico, H00BV2_A1000ContagemResultado_CrFMEhContratante, H00BV2_n1000ContagemResultado_CrFMEhContratante, H00BV2_A999ContagemResultado_CrFMEhContratada, H00BV2_n999ContagemResultado_CrFMEhContratada
               }
            }
         );
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      private short AV6Filtro_Ano ;
      private short wcpOAV6Filtro_Ano ;
      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short nGXWrapped ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short [] AV16mQtdeDmn ;
      private short [] AV21mPendencias ;
      private short [] AV22mDivergencias ;
      private short AV10i ;
      private int AV13AreaTrabalho_Codigo ;
      private int AV5ContagemResultado_ContadorFMCod ;
      private int AV32Servico_Codigo ;
      private int AV38Contratada_PessoaCod ;
      private int wcpOAV13AreaTrabalho_Codigo ;
      private int wcpOAV5ContagemResultado_ContadorFMCod ;
      private int wcpOAV32Servico_Codigo ;
      private int wcpOAV38Contratada_PessoaCod ;
      private int Gxchartcontrol1_Backgroundcolor1 ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A601ContagemResultado_Servico ;
      private int A470ContagemResultado_ContadorFMCod ;
      private int A456ContagemResultado_Codigo ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A479ContagemResultado_CrFMPessoaCod ;
      private int A468ContagemResultado_NaoCnfDmnCod ;
      private int A469ContagemResultado_NaoCnfCntCod ;
      private int GX_I ;
      private int idxLst ;
      private long AV7Filtro_Mes ;
      private long wcpOAV7Filtro_Mes ;
      private decimal A458ContagemResultado_PFBFS ;
      private decimal A460ContagemResultado_PFBFM ;
      private decimal A462ContagemResultado_Divergencia ;
      private decimal [] AV17mPFBFM ;
      private decimal [] AV18mPFBFS ;
      private decimal [] AV19mCTPF ;
      private decimal [] AV20mRTPF ;
      private decimal AV15CUPF ;
      private decimal AV14Contrato_IndiceDivergencia ;
      private String AV31Graficar ;
      private String wcpOAV31Graficar ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gxchartcontrol1_Title ;
      private String Gxchartcontrol1_Width ;
      private String Gxchartcontrol1_Height ;
      private String Gxchartcontrol1_Charttype ;
      private String Gxchartcontrol1_X_axistitle ;
      private String Gxchartcontrol1_Y_axistitle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Gxchartcontrol1_Internalname ;
      private String scmdbuf ;
      private String A484ContagemResultado_StatusDmn ;
      private String A474ContagemResultado_ContadorFMNom ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private DateTime AV41DataCnt ;
      private DateTime wcpOAV41DataCnt ;
      private DateTime Gx_date ;
      private DateTime AV40ContagemResultado_Data ;
      private DateTime A473ContagemResultado_DataCnt ;
      private bool AV33UserEhContratada ;
      private bool AV35UserEhContratante ;
      private bool wcpOAV33UserEhContratada ;
      private bool wcpOAV35UserEhContratante ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool A999ContagemResultado_CrFMEhContratada ;
      private bool A1000ContagemResultado_CrFMEhContratante ;
      private bool A517ContagemResultado_Ultima ;
      private bool BRKBV3 ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n479ContagemResultado_CrFMPessoaCod ;
      private bool n474ContagemResultado_ContadorFMNom ;
      private bool n458ContagemResultado_PFBFS ;
      private bool n460ContagemResultado_PFBFM ;
      private bool n468ContagemResultado_NaoCnfDmnCod ;
      private bool n469ContagemResultado_NaoCnfCntCod ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n601ContagemResultado_Servico ;
      private bool n1000ContagemResultado_CrFMEhContratante ;
      private bool n999ContagemResultado_CrFMEhContratada ;
      private IGxSession AV37websession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private bool aP6_UserEhContratada ;
      private bool aP7_UserEhContratante ;
      private DateTime aP8_DataCnt ;
      private int aP9_Contratada_PessoaCod ;
      private IDataStoreProvider pr_default ;
      private String[] H00BV2_A511ContagemResultado_HoraCnt ;
      private int[] H00BV2_A456ContagemResultado_Codigo ;
      private int[] H00BV2_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00BV2_n1553ContagemResultado_CntSrvCod ;
      private int[] H00BV2_A479ContagemResultado_CrFMPessoaCod ;
      private bool[] H00BV2_n479ContagemResultado_CrFMPessoaCod ;
      private int[] H00BV2_A470ContagemResultado_ContadorFMCod ;
      private String[] H00BV2_A474ContagemResultado_ContadorFMNom ;
      private bool[] H00BV2_n474ContagemResultado_ContadorFMNom ;
      private DateTime[] H00BV2_A473ContagemResultado_DataCnt ;
      private decimal[] H00BV2_A458ContagemResultado_PFBFS ;
      private bool[] H00BV2_n458ContagemResultado_PFBFS ;
      private decimal[] H00BV2_A460ContagemResultado_PFBFM ;
      private bool[] H00BV2_n460ContagemResultado_PFBFM ;
      private int[] H00BV2_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] H00BV2_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] H00BV2_A469ContagemResultado_NaoCnfCntCod ;
      private bool[] H00BV2_n469ContagemResultado_NaoCnfCntCod ;
      private decimal[] H00BV2_A462ContagemResultado_Divergencia ;
      private int[] H00BV2_A490ContagemResultado_ContratadaCod ;
      private bool[] H00BV2_n490ContagemResultado_ContratadaCod ;
      private String[] H00BV2_A484ContagemResultado_StatusDmn ;
      private bool[] H00BV2_n484ContagemResultado_StatusDmn ;
      private bool[] H00BV2_A517ContagemResultado_Ultima ;
      private int[] H00BV2_A601ContagemResultado_Servico ;
      private bool[] H00BV2_n601ContagemResultado_Servico ;
      private bool[] H00BV2_A1000ContagemResultado_CrFMEhContratante ;
      private bool[] H00BV2_n1000ContagemResultado_CrFMEhContratante ;
      private bool[] H00BV2_A999ContagemResultado_CrFMEhContratada ;
      private bool[] H00BV2_n999ContagemResultado_CrFMEhContratada ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV36Contratadas ;
      private GXWebForm Form ;
      private SdtGxChart GxChartData ;
      private SdtGxChart_Serie GxChartSerie ;
   }

   public class wp_consultacontadoresgrf__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00BV2( IGxContext context ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             IGxCollection AV36Contratadas ,
                                             bool AV35UserEhContratante ,
                                             DateTime AV40ContagemResultado_Data ,
                                             long AV7Filtro_Mes ,
                                             DateTime AV41DataCnt ,
                                             int AV32Servico_Codigo ,
                                             bool A999ContagemResultado_CrFMEhContratada ,
                                             bool A1000ContagemResultado_CrFMEhContratante ,
                                             DateTime A473ContagemResultado_DataCnt ,
                                             short AV6Filtro_Ano ,
                                             int A601ContagemResultado_Servico ,
                                             String A484ContagemResultado_StatusDmn ,
                                             bool A517ContagemResultado_Ultima ,
                                             int A470ContagemResultado_ContadorFMCod ,
                                             int AV5ContagemResultado_ContadorFMCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [5] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_HoraCnt], T1.[ContagemResultado_Codigo], T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T4.[Usuario_PessoaCod] AS ContagemResultado_CrFMPessoaCod, T1.[ContagemResultado_ContadorFMCod] AS ContagemResultado_ContadorFMCod, T5.[Pessoa_Nome] AS ContagemResultado_ContadorFMNom, T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_PFBFS], T1.[ContagemResultado_PFBFM], T2.[ContagemResultado_NaoCnfDmnCod], T1.[ContagemResultado_NaoCnfCntCod], T1.[ContagemResultado_Divergencia], T2.[ContagemResultado_ContratadaCod], T2.[ContagemResultado_StatusDmn], T1.[ContagemResultado_Ultima], T3.[Servico_Codigo] AS ContagemResultado_Servico, T4.[Usuario_EhContratante] AS ContagemResultado_CrFMEhContratante, T4.[Usuario_EhContratada] AS ContagemResultado_CrFMEhContratada FROM (((([ContagemResultadoContagens] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) INNER JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = T1.[ContagemResultado_ContadorFMCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Usuario_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T2.[ContagemResultado_StatusDmn] <> 'X')";
         scmdbuf = scmdbuf + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV36Contratadas, "T2.[ContagemResultado_ContratadaCod] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_Ultima] = 1)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_ContadorFMCod] = @AV5ContagemResultado_ContadorFMCod)";
         if ( ! AV35UserEhContratante )
         {
            sWhereString = sWhereString + " and (T4.[Usuario_EhContratada] = 1)";
         }
         if ( AV35UserEhContratante )
         {
            sWhereString = sWhereString + " and (T4.[Usuario_EhContratante] = 1)";
         }
         if ( (DateTime.MinValue==AV40ContagemResultado_Data) )
         {
            sWhereString = sWhereString + " and (YEAR(T1.[ContagemResultado_DataCnt]) = @AV6Filtro_Ano)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ! (0==AV7Filtro_Mes) && (DateTime.MinValue==AV40ContagemResultado_Data) )
         {
            sWhereString = sWhereString + " and (MONTH(T1.[ContagemResultado_DataCnt]) = @AV7Filtro_Mes)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ! (DateTime.MinValue==AV41DataCnt) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataCnt] = @AV41DataCnt)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! (0==AV32Servico_Codigo) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Codigo] = @AV32Servico_Codigo)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T5.[Pessoa_Nome], T2.[ContagemResultado_StatusDmn], T2.[ContagemResultado_ContratadaCod]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00BV2(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (bool)dynConstraints[2] , (DateTime)dynConstraints[3] , (long)dynConstraints[4] , (DateTime)dynConstraints[5] , (int)dynConstraints[6] , (bool)dynConstraints[7] , (bool)dynConstraints[8] , (DateTime)dynConstraints[9] , (short)dynConstraints[10] , (int)dynConstraints[11] , (String)dynConstraints[12] , (bool)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00BV2 ;
          prmH00BV2 = new Object[] {
          new Object[] {"@AV5ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6Filtro_Ano",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV7Filtro_Mes",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV41DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV32Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00BV2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BV2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 5) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((String[]) buf[7])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[9])[0] = rslt.getGXDate(7) ;
                ((decimal[]) buf[10])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((int[]) buf[14])[0] = rslt.getInt(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((int[]) buf[16])[0] = rslt.getInt(11) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((decimal[]) buf[18])[0] = rslt.getDecimal(12) ;
                ((int[]) buf[19])[0] = rslt.getInt(13) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(13);
                ((String[]) buf[21])[0] = rslt.getString(14, 1) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(14);
                ((bool[]) buf[23])[0] = rslt.getBool(15) ;
                ((int[]) buf[24])[0] = rslt.getInt(16) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(16);
                ((bool[]) buf[26])[0] = rslt.getBool(17) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(17);
                ((bool[]) buf[28])[0] = rslt.getBool(18) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(18);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[6]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[7]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[8]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                return;
       }
    }

 }

}
