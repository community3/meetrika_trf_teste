/*
               File: GetPromptContratoUnidadesFilterData
        Description: Get Prompt Contrato Unidades Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:54:24.40
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptcontratounidadesfilterdata : GXProcedure
   {
      public getpromptcontratounidadesfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptcontratounidadesfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV22DDOName = aP0_DDOName;
         this.AV20SearchTxt = aP1_SearchTxt;
         this.AV21SearchTxtTo = aP2_SearchTxtTo;
         this.AV26OptionsJson = "" ;
         this.AV29OptionsDescJson = "" ;
         this.AV31OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV26OptionsJson;
         aP4_OptionsDescJson=this.AV29OptionsDescJson;
         aP5_OptionIndexesJson=this.AV31OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV22DDOName = aP0_DDOName;
         this.AV20SearchTxt = aP1_SearchTxt;
         this.AV21SearchTxtTo = aP2_SearchTxtTo;
         this.AV26OptionsJson = "" ;
         this.AV29OptionsDescJson = "" ;
         this.AV31OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV26OptionsJson;
         aP4_OptionsDescJson=this.AV29OptionsDescJson;
         aP5_OptionIndexesJson=this.AV31OptionIndexesJson;
         return AV31OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptcontratounidadesfilterdata objgetpromptcontratounidadesfilterdata;
         objgetpromptcontratounidadesfilterdata = new getpromptcontratounidadesfilterdata();
         objgetpromptcontratounidadesfilterdata.AV22DDOName = aP0_DDOName;
         objgetpromptcontratounidadesfilterdata.AV20SearchTxt = aP1_SearchTxt;
         objgetpromptcontratounidadesfilterdata.AV21SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptcontratounidadesfilterdata.AV26OptionsJson = "" ;
         objgetpromptcontratounidadesfilterdata.AV29OptionsDescJson = "" ;
         objgetpromptcontratounidadesfilterdata.AV31OptionIndexesJson = "" ;
         objgetpromptcontratounidadesfilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptcontratounidadesfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptcontratounidadesfilterdata);
         aP3_OptionsJson=this.AV26OptionsJson;
         aP4_OptionsDescJson=this.AV29OptionsDescJson;
         aP5_OptionIndexesJson=this.AV31OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptcontratounidadesfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV25Options = (IGxCollection)(new GxSimpleCollection());
         AV28OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV30OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV22DDOName), "DDO_CONTRATOUNIDADES_UNDMEDNOM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOUNIDADES_UNDMEDNOMOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV22DDOName), "DDO_CONTRATOUNIDADES_UNDMEDSIGLA") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOUNIDADES_UNDMEDSIGLAOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV26OptionsJson = AV25Options.ToJSonString(false);
         AV29OptionsDescJson = AV28OptionsDesc.ToJSonString(false);
         AV31OptionIndexesJson = AV30OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV33Session.Get("PromptContratoUnidadesGridState"), "") == 0 )
         {
            AV35GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptContratoUnidadesGridState"), "");
         }
         else
         {
            AV35GridState.FromXml(AV33Session.Get("PromptContratoUnidadesGridState"), "");
         }
         AV40GXV1 = 1;
         while ( AV40GXV1 <= AV35GridState.gxTpr_Filtervalues.Count )
         {
            AV36GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV35GridState.gxTpr_Filtervalues.Item(AV40GXV1));
            if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFCONTRATOUNIDADES_CONTRATOCOD") == 0 )
            {
               AV10TFContratoUnidades_ContratoCod = (int)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Value, "."));
               AV11TFContratoUnidades_ContratoCod_To = (int)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFCONTRATOUNIDADES_UNDMEDCOD") == 0 )
            {
               AV12TFContratoUnidades_UndMedCod = (int)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Value, "."));
               AV13TFContratoUnidades_UndMedCod_To = (int)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFCONTRATOUNIDADES_UNDMEDNOM") == 0 )
            {
               AV14TFContratoUnidades_UndMedNom = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFCONTRATOUNIDADES_UNDMEDNOM_SEL") == 0 )
            {
               AV15TFContratoUnidades_UndMedNom_Sel = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFCONTRATOUNIDADES_UNDMEDSIGLA") == 0 )
            {
               AV16TFContratoUnidades_UndMedSigla = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFCONTRATOUNIDADES_UNDMEDSIGLA_SEL") == 0 )
            {
               AV17TFContratoUnidades_UndMedSigla_Sel = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFCONTRATOUNIDADES_PRODUTIVIDADE") == 0 )
            {
               AV18TFContratoUnidades_Produtividade = NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Value, ".");
               AV19TFContratoUnidades_Produtividade_To = NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Valueto, ".");
            }
            AV40GXV1 = (int)(AV40GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATOUNIDADES_UNDMEDNOMOPTIONS' Routine */
         AV14TFContratoUnidades_UndMedNom = AV20SearchTxt;
         AV15TFContratoUnidades_UndMedNom_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV10TFContratoUnidades_ContratoCod ,
                                              AV11TFContratoUnidades_ContratoCod_To ,
                                              AV12TFContratoUnidades_UndMedCod ,
                                              AV13TFContratoUnidades_UndMedCod_To ,
                                              AV15TFContratoUnidades_UndMedNom_Sel ,
                                              AV14TFContratoUnidades_UndMedNom ,
                                              AV17TFContratoUnidades_UndMedSigla_Sel ,
                                              AV16TFContratoUnidades_UndMedSigla ,
                                              AV18TFContratoUnidades_Produtividade ,
                                              AV19TFContratoUnidades_Produtividade_To ,
                                              A1207ContratoUnidades_ContratoCod ,
                                              A1204ContratoUnidades_UndMedCod ,
                                              A1205ContratoUnidades_UndMedNom ,
                                              A1206ContratoUnidades_UndMedSigla ,
                                              A1208ContratoUnidades_Produtividade },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN
                                              }
         });
         lV14TFContratoUnidades_UndMedNom = StringUtil.PadR( StringUtil.RTrim( AV14TFContratoUnidades_UndMedNom), 50, "%");
         lV16TFContratoUnidades_UndMedSigla = StringUtil.PadR( StringUtil.RTrim( AV16TFContratoUnidades_UndMedSigla), 15, "%");
         /* Using cursor P00Q82 */
         pr_default.execute(0, new Object[] {AV10TFContratoUnidades_ContratoCod, AV11TFContratoUnidades_ContratoCod_To, AV12TFContratoUnidades_UndMedCod, AV13TFContratoUnidades_UndMedCod_To, lV14TFContratoUnidades_UndMedNom, AV15TFContratoUnidades_UndMedNom_Sel, lV16TFContratoUnidades_UndMedSigla, AV17TFContratoUnidades_UndMedSigla_Sel, AV18TFContratoUnidades_Produtividade, AV19TFContratoUnidades_Produtividade_To});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKQ82 = false;
            A1204ContratoUnidades_UndMedCod = P00Q82_A1204ContratoUnidades_UndMedCod[0];
            A1208ContratoUnidades_Produtividade = P00Q82_A1208ContratoUnidades_Produtividade[0];
            n1208ContratoUnidades_Produtividade = P00Q82_n1208ContratoUnidades_Produtividade[0];
            A1206ContratoUnidades_UndMedSigla = P00Q82_A1206ContratoUnidades_UndMedSigla[0];
            n1206ContratoUnidades_UndMedSigla = P00Q82_n1206ContratoUnidades_UndMedSigla[0];
            A1205ContratoUnidades_UndMedNom = P00Q82_A1205ContratoUnidades_UndMedNom[0];
            n1205ContratoUnidades_UndMedNom = P00Q82_n1205ContratoUnidades_UndMedNom[0];
            A1207ContratoUnidades_ContratoCod = P00Q82_A1207ContratoUnidades_ContratoCod[0];
            A1206ContratoUnidades_UndMedSigla = P00Q82_A1206ContratoUnidades_UndMedSigla[0];
            n1206ContratoUnidades_UndMedSigla = P00Q82_n1206ContratoUnidades_UndMedSigla[0];
            A1205ContratoUnidades_UndMedNom = P00Q82_A1205ContratoUnidades_UndMedNom[0];
            n1205ContratoUnidades_UndMedNom = P00Q82_n1205ContratoUnidades_UndMedNom[0];
            AV32count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00Q82_A1204ContratoUnidades_UndMedCod[0] == A1204ContratoUnidades_UndMedCod ) )
            {
               BRKQ82 = false;
               A1207ContratoUnidades_ContratoCod = P00Q82_A1207ContratoUnidades_ContratoCod[0];
               AV32count = (long)(AV32count+1);
               BRKQ82 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1205ContratoUnidades_UndMedNom)) )
            {
               AV24Option = A1205ContratoUnidades_UndMedNom;
               AV23InsertIndex = 1;
               while ( ( AV23InsertIndex <= AV25Options.Count ) && ( StringUtil.StrCmp(((String)AV25Options.Item(AV23InsertIndex)), AV24Option) < 0 ) )
               {
                  AV23InsertIndex = (int)(AV23InsertIndex+1);
               }
               AV25Options.Add(AV24Option, AV23InsertIndex);
               AV30OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV32count), "Z,ZZZ,ZZZ,ZZ9")), AV23InsertIndex);
            }
            if ( AV25Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKQ82 )
            {
               BRKQ82 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTRATOUNIDADES_UNDMEDSIGLAOPTIONS' Routine */
         AV16TFContratoUnidades_UndMedSigla = AV20SearchTxt;
         AV17TFContratoUnidades_UndMedSigla_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV10TFContratoUnidades_ContratoCod ,
                                              AV11TFContratoUnidades_ContratoCod_To ,
                                              AV12TFContratoUnidades_UndMedCod ,
                                              AV13TFContratoUnidades_UndMedCod_To ,
                                              AV15TFContratoUnidades_UndMedNom_Sel ,
                                              AV14TFContratoUnidades_UndMedNom ,
                                              AV17TFContratoUnidades_UndMedSigla_Sel ,
                                              AV16TFContratoUnidades_UndMedSigla ,
                                              AV18TFContratoUnidades_Produtividade ,
                                              AV19TFContratoUnidades_Produtividade_To ,
                                              A1207ContratoUnidades_ContratoCod ,
                                              A1204ContratoUnidades_UndMedCod ,
                                              A1205ContratoUnidades_UndMedNom ,
                                              A1206ContratoUnidades_UndMedSigla ,
                                              A1208ContratoUnidades_Produtividade },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN
                                              }
         });
         lV14TFContratoUnidades_UndMedNom = StringUtil.PadR( StringUtil.RTrim( AV14TFContratoUnidades_UndMedNom), 50, "%");
         lV16TFContratoUnidades_UndMedSigla = StringUtil.PadR( StringUtil.RTrim( AV16TFContratoUnidades_UndMedSigla), 15, "%");
         /* Using cursor P00Q83 */
         pr_default.execute(1, new Object[] {AV10TFContratoUnidades_ContratoCod, AV11TFContratoUnidades_ContratoCod_To, AV12TFContratoUnidades_UndMedCod, AV13TFContratoUnidades_UndMedCod_To, lV14TFContratoUnidades_UndMedNom, AV15TFContratoUnidades_UndMedNom_Sel, lV16TFContratoUnidades_UndMedSigla, AV17TFContratoUnidades_UndMedSigla_Sel, AV18TFContratoUnidades_Produtividade, AV19TFContratoUnidades_Produtividade_To});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKQ84 = false;
            A1206ContratoUnidades_UndMedSigla = P00Q83_A1206ContratoUnidades_UndMedSigla[0];
            n1206ContratoUnidades_UndMedSigla = P00Q83_n1206ContratoUnidades_UndMedSigla[0];
            A1208ContratoUnidades_Produtividade = P00Q83_A1208ContratoUnidades_Produtividade[0];
            n1208ContratoUnidades_Produtividade = P00Q83_n1208ContratoUnidades_Produtividade[0];
            A1205ContratoUnidades_UndMedNom = P00Q83_A1205ContratoUnidades_UndMedNom[0];
            n1205ContratoUnidades_UndMedNom = P00Q83_n1205ContratoUnidades_UndMedNom[0];
            A1204ContratoUnidades_UndMedCod = P00Q83_A1204ContratoUnidades_UndMedCod[0];
            A1207ContratoUnidades_ContratoCod = P00Q83_A1207ContratoUnidades_ContratoCod[0];
            A1206ContratoUnidades_UndMedSigla = P00Q83_A1206ContratoUnidades_UndMedSigla[0];
            n1206ContratoUnidades_UndMedSigla = P00Q83_n1206ContratoUnidades_UndMedSigla[0];
            A1205ContratoUnidades_UndMedNom = P00Q83_A1205ContratoUnidades_UndMedNom[0];
            n1205ContratoUnidades_UndMedNom = P00Q83_n1205ContratoUnidades_UndMedNom[0];
            AV32count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00Q83_A1206ContratoUnidades_UndMedSigla[0], A1206ContratoUnidades_UndMedSigla) == 0 ) )
            {
               BRKQ84 = false;
               A1204ContratoUnidades_UndMedCod = P00Q83_A1204ContratoUnidades_UndMedCod[0];
               A1207ContratoUnidades_ContratoCod = P00Q83_A1207ContratoUnidades_ContratoCod[0];
               AV32count = (long)(AV32count+1);
               BRKQ84 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1206ContratoUnidades_UndMedSigla)) )
            {
               AV24Option = A1206ContratoUnidades_UndMedSigla;
               AV25Options.Add(AV24Option, 0);
               AV30OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV32count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV25Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKQ84 )
            {
               BRKQ84 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV25Options = new GxSimpleCollection();
         AV28OptionsDesc = new GxSimpleCollection();
         AV30OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV33Session = context.GetSession();
         AV35GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV36GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV14TFContratoUnidades_UndMedNom = "";
         AV15TFContratoUnidades_UndMedNom_Sel = "";
         AV16TFContratoUnidades_UndMedSigla = "";
         AV17TFContratoUnidades_UndMedSigla_Sel = "";
         scmdbuf = "";
         lV14TFContratoUnidades_UndMedNom = "";
         lV16TFContratoUnidades_UndMedSigla = "";
         A1205ContratoUnidades_UndMedNom = "";
         A1206ContratoUnidades_UndMedSigla = "";
         P00Q82_A1204ContratoUnidades_UndMedCod = new int[1] ;
         P00Q82_A1208ContratoUnidades_Produtividade = new decimal[1] ;
         P00Q82_n1208ContratoUnidades_Produtividade = new bool[] {false} ;
         P00Q82_A1206ContratoUnidades_UndMedSigla = new String[] {""} ;
         P00Q82_n1206ContratoUnidades_UndMedSigla = new bool[] {false} ;
         P00Q82_A1205ContratoUnidades_UndMedNom = new String[] {""} ;
         P00Q82_n1205ContratoUnidades_UndMedNom = new bool[] {false} ;
         P00Q82_A1207ContratoUnidades_ContratoCod = new int[1] ;
         AV24Option = "";
         P00Q83_A1206ContratoUnidades_UndMedSigla = new String[] {""} ;
         P00Q83_n1206ContratoUnidades_UndMedSigla = new bool[] {false} ;
         P00Q83_A1208ContratoUnidades_Produtividade = new decimal[1] ;
         P00Q83_n1208ContratoUnidades_Produtividade = new bool[] {false} ;
         P00Q83_A1205ContratoUnidades_UndMedNom = new String[] {""} ;
         P00Q83_n1205ContratoUnidades_UndMedNom = new bool[] {false} ;
         P00Q83_A1204ContratoUnidades_UndMedCod = new int[1] ;
         P00Q83_A1207ContratoUnidades_ContratoCod = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptcontratounidadesfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00Q82_A1204ContratoUnidades_UndMedCod, P00Q82_A1208ContratoUnidades_Produtividade, P00Q82_n1208ContratoUnidades_Produtividade, P00Q82_A1206ContratoUnidades_UndMedSigla, P00Q82_n1206ContratoUnidades_UndMedSigla, P00Q82_A1205ContratoUnidades_UndMedNom, P00Q82_n1205ContratoUnidades_UndMedNom, P00Q82_A1207ContratoUnidades_ContratoCod
               }
               , new Object[] {
               P00Q83_A1206ContratoUnidades_UndMedSigla, P00Q83_n1206ContratoUnidades_UndMedSigla, P00Q83_A1208ContratoUnidades_Produtividade, P00Q83_n1208ContratoUnidades_Produtividade, P00Q83_A1205ContratoUnidades_UndMedNom, P00Q83_n1205ContratoUnidades_UndMedNom, P00Q83_A1204ContratoUnidades_UndMedCod, P00Q83_A1207ContratoUnidades_ContratoCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV40GXV1 ;
      private int AV10TFContratoUnidades_ContratoCod ;
      private int AV11TFContratoUnidades_ContratoCod_To ;
      private int AV12TFContratoUnidades_UndMedCod ;
      private int AV13TFContratoUnidades_UndMedCod_To ;
      private int A1207ContratoUnidades_ContratoCod ;
      private int A1204ContratoUnidades_UndMedCod ;
      private int AV23InsertIndex ;
      private long AV32count ;
      private decimal AV18TFContratoUnidades_Produtividade ;
      private decimal AV19TFContratoUnidades_Produtividade_To ;
      private decimal A1208ContratoUnidades_Produtividade ;
      private String AV14TFContratoUnidades_UndMedNom ;
      private String AV15TFContratoUnidades_UndMedNom_Sel ;
      private String AV16TFContratoUnidades_UndMedSigla ;
      private String AV17TFContratoUnidades_UndMedSigla_Sel ;
      private String scmdbuf ;
      private String lV14TFContratoUnidades_UndMedNom ;
      private String lV16TFContratoUnidades_UndMedSigla ;
      private String A1205ContratoUnidades_UndMedNom ;
      private String A1206ContratoUnidades_UndMedSigla ;
      private bool returnInSub ;
      private bool BRKQ82 ;
      private bool n1208ContratoUnidades_Produtividade ;
      private bool n1206ContratoUnidades_UndMedSigla ;
      private bool n1205ContratoUnidades_UndMedNom ;
      private bool BRKQ84 ;
      private String AV31OptionIndexesJson ;
      private String AV26OptionsJson ;
      private String AV29OptionsDescJson ;
      private String AV22DDOName ;
      private String AV20SearchTxt ;
      private String AV21SearchTxtTo ;
      private String AV24Option ;
      private IGxSession AV33Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00Q82_A1204ContratoUnidades_UndMedCod ;
      private decimal[] P00Q82_A1208ContratoUnidades_Produtividade ;
      private bool[] P00Q82_n1208ContratoUnidades_Produtividade ;
      private String[] P00Q82_A1206ContratoUnidades_UndMedSigla ;
      private bool[] P00Q82_n1206ContratoUnidades_UndMedSigla ;
      private String[] P00Q82_A1205ContratoUnidades_UndMedNom ;
      private bool[] P00Q82_n1205ContratoUnidades_UndMedNom ;
      private int[] P00Q82_A1207ContratoUnidades_ContratoCod ;
      private String[] P00Q83_A1206ContratoUnidades_UndMedSigla ;
      private bool[] P00Q83_n1206ContratoUnidades_UndMedSigla ;
      private decimal[] P00Q83_A1208ContratoUnidades_Produtividade ;
      private bool[] P00Q83_n1208ContratoUnidades_Produtividade ;
      private String[] P00Q83_A1205ContratoUnidades_UndMedNom ;
      private bool[] P00Q83_n1205ContratoUnidades_UndMedNom ;
      private int[] P00Q83_A1204ContratoUnidades_UndMedCod ;
      private int[] P00Q83_A1207ContratoUnidades_ContratoCod ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV25Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV28OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV30OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV35GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV36GridStateFilterValue ;
   }

   public class getpromptcontratounidadesfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00Q82( IGxContext context ,
                                             int AV10TFContratoUnidades_ContratoCod ,
                                             int AV11TFContratoUnidades_ContratoCod_To ,
                                             int AV12TFContratoUnidades_UndMedCod ,
                                             int AV13TFContratoUnidades_UndMedCod_To ,
                                             String AV15TFContratoUnidades_UndMedNom_Sel ,
                                             String AV14TFContratoUnidades_UndMedNom ,
                                             String AV17TFContratoUnidades_UndMedSigla_Sel ,
                                             String AV16TFContratoUnidades_UndMedSigla ,
                                             decimal AV18TFContratoUnidades_Produtividade ,
                                             decimal AV19TFContratoUnidades_Produtividade_To ,
                                             int A1207ContratoUnidades_ContratoCod ,
                                             int A1204ContratoUnidades_UndMedCod ,
                                             String A1205ContratoUnidades_UndMedNom ,
                                             String A1206ContratoUnidades_UndMedSigla ,
                                             decimal A1208ContratoUnidades_Produtividade )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [10] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoUnidades_UndMedCod] AS ContratoUnidades_UndMedCod, T1.[ContratoUnidades_Produtividade], T2.[UnidadeMedicao_Sigla] AS ContratoUnidades_UndMedSigla, T2.[UnidadeMedicao_Nome] AS ContratoUnidades_UndMedNom, T1.[ContratoUnidades_ContratoCod] FROM ([ContratoUnidades] T1 WITH (NOLOCK) INNER JOIN [UnidadeMedicao] T2 WITH (NOLOCK) ON T2.[UnidadeMedicao_Codigo] = T1.[ContratoUnidades_UndMedCod])";
         if ( ! (0==AV10TFContratoUnidades_ContratoCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoUnidades_ContratoCod] >= @AV10TFContratoUnidades_ContratoCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoUnidades_ContratoCod] >= @AV10TFContratoUnidades_ContratoCod)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ! (0==AV11TFContratoUnidades_ContratoCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoUnidades_ContratoCod] <= @AV11TFContratoUnidades_ContratoCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoUnidades_ContratoCod] <= @AV11TFContratoUnidades_ContratoCod_To)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ! (0==AV12TFContratoUnidades_UndMedCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoUnidades_UndMedCod] >= @AV12TFContratoUnidades_UndMedCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoUnidades_UndMedCod] >= @AV12TFContratoUnidades_UndMedCod)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ! (0==AV13TFContratoUnidades_UndMedCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoUnidades_UndMedCod] <= @AV13TFContratoUnidades_UndMedCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoUnidades_UndMedCod] <= @AV13TFContratoUnidades_UndMedCod_To)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoUnidades_UndMedNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContratoUnidades_UndMedNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] like @lV14TFContratoUnidades_UndMedNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeMedicao_Nome] like @lV14TFContratoUnidades_UndMedNom)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoUnidades_UndMedNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] = @AV15TFContratoUnidades_UndMedNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeMedicao_Nome] = @AV15TFContratoUnidades_UndMedNom_Sel)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContratoUnidades_UndMedSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFContratoUnidades_UndMedSigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Sigla] like @lV16TFContratoUnidades_UndMedSigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeMedicao_Sigla] like @lV16TFContratoUnidades_UndMedSigla)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContratoUnidades_UndMedSigla_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Sigla] = @AV17TFContratoUnidades_UndMedSigla_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeMedicao_Sigla] = @AV17TFContratoUnidades_UndMedSigla_Sel)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV18TFContratoUnidades_Produtividade) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoUnidades_Produtividade] >= @AV18TFContratoUnidades_Produtividade)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoUnidades_Produtividade] >= @AV18TFContratoUnidades_Produtividade)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV19TFContratoUnidades_Produtividade_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoUnidades_Produtividade] <= @AV19TFContratoUnidades_Produtividade_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoUnidades_Produtividade] <= @AV19TFContratoUnidades_Produtividade_To)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoUnidades_UndMedCod]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00Q83( IGxContext context ,
                                             int AV10TFContratoUnidades_ContratoCod ,
                                             int AV11TFContratoUnidades_ContratoCod_To ,
                                             int AV12TFContratoUnidades_UndMedCod ,
                                             int AV13TFContratoUnidades_UndMedCod_To ,
                                             String AV15TFContratoUnidades_UndMedNom_Sel ,
                                             String AV14TFContratoUnidades_UndMedNom ,
                                             String AV17TFContratoUnidades_UndMedSigla_Sel ,
                                             String AV16TFContratoUnidades_UndMedSigla ,
                                             decimal AV18TFContratoUnidades_Produtividade ,
                                             decimal AV19TFContratoUnidades_Produtividade_To ,
                                             int A1207ContratoUnidades_ContratoCod ,
                                             int A1204ContratoUnidades_UndMedCod ,
                                             String A1205ContratoUnidades_UndMedNom ,
                                             String A1206ContratoUnidades_UndMedSigla ,
                                             decimal A1208ContratoUnidades_Produtividade )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [10] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T2.[UnidadeMedicao_Sigla] AS ContratoUnidades_UndMedSigla, T1.[ContratoUnidades_Produtividade], T2.[UnidadeMedicao_Nome] AS ContratoUnidades_UndMedNom, T1.[ContratoUnidades_UndMedCod] AS ContratoUnidades_UndMedCod, T1.[ContratoUnidades_ContratoCod] FROM ([ContratoUnidades] T1 WITH (NOLOCK) INNER JOIN [UnidadeMedicao] T2 WITH (NOLOCK) ON T2.[UnidadeMedicao_Codigo] = T1.[ContratoUnidades_UndMedCod])";
         if ( ! (0==AV10TFContratoUnidades_ContratoCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoUnidades_ContratoCod] >= @AV10TFContratoUnidades_ContratoCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoUnidades_ContratoCod] >= @AV10TFContratoUnidades_ContratoCod)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ! (0==AV11TFContratoUnidades_ContratoCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoUnidades_ContratoCod] <= @AV11TFContratoUnidades_ContratoCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoUnidades_ContratoCod] <= @AV11TFContratoUnidades_ContratoCod_To)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ! (0==AV12TFContratoUnidades_UndMedCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoUnidades_UndMedCod] >= @AV12TFContratoUnidades_UndMedCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoUnidades_UndMedCod] >= @AV12TFContratoUnidades_UndMedCod)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ! (0==AV13TFContratoUnidades_UndMedCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoUnidades_UndMedCod] <= @AV13TFContratoUnidades_UndMedCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoUnidades_UndMedCod] <= @AV13TFContratoUnidades_UndMedCod_To)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoUnidades_UndMedNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContratoUnidades_UndMedNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] like @lV14TFContratoUnidades_UndMedNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeMedicao_Nome] like @lV14TFContratoUnidades_UndMedNom)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoUnidades_UndMedNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] = @AV15TFContratoUnidades_UndMedNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeMedicao_Nome] = @AV15TFContratoUnidades_UndMedNom_Sel)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContratoUnidades_UndMedSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFContratoUnidades_UndMedSigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Sigla] like @lV16TFContratoUnidades_UndMedSigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeMedicao_Sigla] like @lV16TFContratoUnidades_UndMedSigla)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContratoUnidades_UndMedSigla_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Sigla] = @AV17TFContratoUnidades_UndMedSigla_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeMedicao_Sigla] = @AV17TFContratoUnidades_UndMedSigla_Sel)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV18TFContratoUnidades_Produtividade) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoUnidades_Produtividade] >= @AV18TFContratoUnidades_Produtividade)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoUnidades_Produtividade] >= @AV18TFContratoUnidades_Produtividade)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV19TFContratoUnidades_Produtividade_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoUnidades_Produtividade] <= @AV19TFContratoUnidades_Produtividade_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoUnidades_Produtividade] <= @AV19TFContratoUnidades_Produtividade_To)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T2.[UnidadeMedicao_Sigla]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00Q82(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (decimal)dynConstraints[8] , (decimal)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (decimal)dynConstraints[14] );
               case 1 :
                     return conditional_P00Q83(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (decimal)dynConstraints[8] , (decimal)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (decimal)dynConstraints[14] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00Q82 ;
          prmP00Q82 = new Object[] {
          new Object[] {"@AV10TFContratoUnidades_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFContratoUnidades_ContratoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFContratoUnidades_UndMedCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFContratoUnidades_UndMedCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV14TFContratoUnidades_UndMedNom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV15TFContratoUnidades_UndMedNom_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV16TFContratoUnidades_UndMedSigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV17TFContratoUnidades_UndMedSigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV18TFContratoUnidades_Produtividade",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV19TFContratoUnidades_Produtividade_To",SqlDbType.Decimal,14,5}
          } ;
          Object[] prmP00Q83 ;
          prmP00Q83 = new Object[] {
          new Object[] {"@AV10TFContratoUnidades_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFContratoUnidades_ContratoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFContratoUnidades_UndMedCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFContratoUnidades_UndMedCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV14TFContratoUnidades_UndMedNom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV15TFContratoUnidades_UndMedNom_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV16TFContratoUnidades_UndMedSigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV17TFContratoUnidades_UndMedSigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV18TFContratoUnidades_Produtividade",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV19TFContratoUnidades_Produtividade_To",SqlDbType.Decimal,14,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00Q82", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00Q82,100,0,true,false )
             ,new CursorDef("P00Q83", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00Q83,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((decimal[]) buf[2])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[12]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[18]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[19]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[12]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[18]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[19]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptcontratounidadesfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptcontratounidadesfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptcontratounidadesfilterdata") )
          {
             return  ;
          }
          getpromptcontratounidadesfilterdata worker = new getpromptcontratounidadesfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
