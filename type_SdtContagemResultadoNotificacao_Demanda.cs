/*
               File: type_SdtContagemResultadoNotificacao_Demanda
        Description: Notificações da Contagem
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:17:53.86
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "ContagemResultadoNotificacao.Demanda" )]
   [XmlType(TypeName =  "ContagemResultadoNotificacao.Demanda" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtContagemResultadoNotificacao_Demanda : GxSilentTrnSdt, IGxSilentTrnGridItem
   {
      public SdtContagemResultadoNotificacao_Demanda( )
      {
         /* Constructor for serialization */
         gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demanda = "";
         gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demandafm = "";
         gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servicosigla = "";
         gxTv_SdtContagemResultadoNotificacao_Demanda_Mode = "";
         gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demanda_Z = "";
         gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demandafm_Z = "";
         gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servicosigla_Z = "";
      }

      public SdtContagemResultadoNotificacao_Demanda( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "Demanda");
         metadata.Set("BT", "ContagemResultadoNotificacaoDemanda");
         metadata.Set("PK", "[ \"ContagemResultado_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"ContagemResultadoNotificacao_Codigo\" ],\"FKMap\":[  ] },{ \"FK\":[ \"ContagemResultado_Codigo\" ],\"FKMap\":[  ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Modified" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_demanda_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_demandafm_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_servico_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_servicosigla_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_prazoinicialdias_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_demanda_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_demandafm_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_servico_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_servicosigla_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_prazoinicialdias_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtContagemResultadoNotificacao_Demanda deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtContagemResultadoNotificacao_Demanda)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtContagemResultadoNotificacao_Demanda obj ;
         obj = this;
         obj.gxTpr_Contagemresultado_codigo = deserialized.gxTpr_Contagemresultado_codigo;
         obj.gxTpr_Contagemresultado_demanda = deserialized.gxTpr_Contagemresultado_demanda;
         obj.gxTpr_Contagemresultado_demandafm = deserialized.gxTpr_Contagemresultado_demandafm;
         obj.gxTpr_Contagemresultado_servico = deserialized.gxTpr_Contagemresultado_servico;
         obj.gxTpr_Contagemresultado_servicosigla = deserialized.gxTpr_Contagemresultado_servicosigla;
         obj.gxTpr_Contagemresultado_prazoinicialdias = deserialized.gxTpr_Contagemresultado_prazoinicialdias;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Modified = deserialized.gxTpr_Modified;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Contagemresultado_codigo_Z = deserialized.gxTpr_Contagemresultado_codigo_Z;
         obj.gxTpr_Contagemresultado_demanda_Z = deserialized.gxTpr_Contagemresultado_demanda_Z;
         obj.gxTpr_Contagemresultado_demandafm_Z = deserialized.gxTpr_Contagemresultado_demandafm_Z;
         obj.gxTpr_Contagemresultado_servico_Z = deserialized.gxTpr_Contagemresultado_servico_Z;
         obj.gxTpr_Contagemresultado_servicosigla_Z = deserialized.gxTpr_Contagemresultado_servicosigla_Z;
         obj.gxTpr_Contagemresultado_prazoinicialdias_Z = deserialized.gxTpr_Contagemresultado_prazoinicialdias_Z;
         obj.gxTpr_Contagemresultado_demanda_N = deserialized.gxTpr_Contagemresultado_demanda_N;
         obj.gxTpr_Contagemresultado_demandafm_N = deserialized.gxTpr_Contagemresultado_demandafm_N;
         obj.gxTpr_Contagemresultado_servico_N = deserialized.gxTpr_Contagemresultado_servico_N;
         obj.gxTpr_Contagemresultado_servicosigla_N = deserialized.gxTpr_Contagemresultado_servicosigla_N;
         obj.gxTpr_Contagemresultado_prazoinicialdias_N = deserialized.gxTpr_Contagemresultado_prazoinicialdias_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Codigo") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Demanda") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demanda = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DemandaFM") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demandafm = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Servico") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servico = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ServicoSigla") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servicosigla = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PrazoInicialDias") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_prazoinicialdias = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Demanda_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Modified") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Demanda_Modified = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Demanda_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Codigo_Z") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Demanda_Z") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demanda_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DemandaFM_Z") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demandafm_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Servico_Z") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servico_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ServicoSigla_Z") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servicosigla_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PrazoInicialDias_Z") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_prazoinicialdias_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Demanda_N") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demanda_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DemandaFM_N") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demandafm_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Servico_N") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servico_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ServicoSigla_N") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servicosigla_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PrazoInicialDias_N") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_prazoinicialdias_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "ContagemResultadoNotificacao.Demanda";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("ContagemResultado_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_Demanda", StringUtil.RTrim( gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demanda));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_DemandaFM", StringUtil.RTrim( gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demandafm));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_Servico", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servico), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_ServicoSigla", StringUtil.RTrim( gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servicosigla));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_PrazoInicialDias", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_prazoinicialdias), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtContagemResultadoNotificacao_Demanda_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Modified", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Demanda_Modified), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Demanda_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_Demanda_Z", StringUtil.RTrim( gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demanda_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_DemandaFM_Z", StringUtil.RTrim( gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demandafm_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_Servico_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servico_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_ServicoSigla_Z", StringUtil.RTrim( gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servicosigla_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_PrazoInicialDias_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_prazoinicialdias_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_Demanda_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demanda_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_DemandaFM_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demandafm_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_Servico_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servico_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_ServicoSigla_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servicosigla_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_PrazoInicialDias_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_prazoinicialdias_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("ContagemResultado_Codigo", gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_codigo, false);
         AddObjectProperty("ContagemResultado_Demanda", gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demanda, false);
         AddObjectProperty("ContagemResultado_DemandaFM", gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demandafm, false);
         AddObjectProperty("ContagemResultado_Servico", gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servico, false);
         AddObjectProperty("ContagemResultado_ServicoSigla", gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servicosigla, false);
         AddObjectProperty("ContagemResultado_PrazoInicialDias", gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_prazoinicialdias, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtContagemResultadoNotificacao_Demanda_Mode, false);
            AddObjectProperty("Modified", gxTv_SdtContagemResultadoNotificacao_Demanda_Modified, false);
            AddObjectProperty("Initialized", gxTv_SdtContagemResultadoNotificacao_Demanda_Initialized, false);
            AddObjectProperty("ContagemResultado_Codigo_Z", gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_codigo_Z, false);
            AddObjectProperty("ContagemResultado_Demanda_Z", gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demanda_Z, false);
            AddObjectProperty("ContagemResultado_DemandaFM_Z", gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demandafm_Z, false);
            AddObjectProperty("ContagemResultado_Servico_Z", gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servico_Z, false);
            AddObjectProperty("ContagemResultado_ServicoSigla_Z", gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servicosigla_Z, false);
            AddObjectProperty("ContagemResultado_PrazoInicialDias_Z", gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_prazoinicialdias_Z, false);
            AddObjectProperty("ContagemResultado_Demanda_N", gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demanda_N, false);
            AddObjectProperty("ContagemResultado_DemandaFM_N", gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demandafm_N, false);
            AddObjectProperty("ContagemResultado_Servico_N", gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servico_N, false);
            AddObjectProperty("ContagemResultado_ServicoSigla_N", gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servicosigla_N, false);
            AddObjectProperty("ContagemResultado_PrazoInicialDias_N", gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_prazoinicialdias_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Codigo" )]
      [  XmlElement( ElementName = "ContagemResultado_Codigo"   )]
      public int gxTpr_Contagemresultado_codigo
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_codigo ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_codigo = (int)(value);
            gxTv_SdtContagemResultadoNotificacao_Demanda_Modified = 1;
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_Demanda" )]
      [  XmlElement( ElementName = "ContagemResultado_Demanda"   )]
      public String gxTpr_Contagemresultado_demanda
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demanda ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demanda_N = 0;
            gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demanda = (String)(value);
            gxTv_SdtContagemResultadoNotificacao_Demanda_Modified = 1;
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demanda_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demanda_N = 1;
         gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demanda = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demanda_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_DemandaFM" )]
      [  XmlElement( ElementName = "ContagemResultado_DemandaFM"   )]
      public String gxTpr_Contagemresultado_demandafm
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demandafm ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demandafm_N = 0;
            gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demandafm = (String)(value);
            gxTv_SdtContagemResultadoNotificacao_Demanda_Modified = 1;
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demandafm_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demandafm_N = 1;
         gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demandafm = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demandafm_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Servico" )]
      [  XmlElement( ElementName = "ContagemResultado_Servico"   )]
      public int gxTpr_Contagemresultado_servico
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servico ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servico_N = 0;
            gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servico = (int)(value);
            gxTv_SdtContagemResultadoNotificacao_Demanda_Modified = 1;
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servico_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servico_N = 1;
         gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servico = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servico_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_ServicoSigla" )]
      [  XmlElement( ElementName = "ContagemResultado_ServicoSigla"   )]
      public String gxTpr_Contagemresultado_servicosigla
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servicosigla ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servicosigla_N = 0;
            gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servicosigla = (String)(value);
            gxTv_SdtContagemResultadoNotificacao_Demanda_Modified = 1;
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servicosigla_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servicosigla_N = 1;
         gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servicosigla = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servicosigla_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_PrazoInicialDias" )]
      [  XmlElement( ElementName = "ContagemResultado_PrazoInicialDias"   )]
      public short gxTpr_Contagemresultado_prazoinicialdias
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_prazoinicialdias ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_prazoinicialdias_N = 0;
            gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_prazoinicialdias = (short)(value);
            gxTv_SdtContagemResultadoNotificacao_Demanda_Modified = 1;
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_prazoinicialdias_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_prazoinicialdias_N = 1;
         gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_prazoinicialdias = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_prazoinicialdias_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Demanda_Mode ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Demanda_Mode = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Demanda_Mode_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Demanda_Mode = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Demanda_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Modified" )]
      [  XmlElement( ElementName = "Modified"   )]
      public short gxTpr_Modified
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Demanda_Modified ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Demanda_Modified = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Demanda_Modified_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Demanda_Modified = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Demanda_Modified_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Demanda_Initialized ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Demanda_Initialized = (short)(value);
            gxTv_SdtContagemResultadoNotificacao_Demanda_Modified = 1;
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Demanda_Initialized_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Demanda_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Demanda_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Codigo_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_Codigo_Z"   )]
      public int gxTpr_Contagemresultado_codigo_Z
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_codigo_Z ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_codigo_Z = (int)(value);
            gxTv_SdtContagemResultadoNotificacao_Demanda_Modified = 1;
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_codigo_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Demanda_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_Demanda_Z"   )]
      public String gxTpr_Contagemresultado_demanda_Z
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demanda_Z ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demanda_Z = (String)(value);
            gxTv_SdtContagemResultadoNotificacao_Demanda_Modified = 1;
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demanda_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demanda_Z = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demanda_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_DemandaFM_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_DemandaFM_Z"   )]
      public String gxTpr_Contagemresultado_demandafm_Z
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demandafm_Z ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demandafm_Z = (String)(value);
            gxTv_SdtContagemResultadoNotificacao_Demanda_Modified = 1;
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demandafm_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demandafm_Z = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demandafm_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Servico_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_Servico_Z"   )]
      public int gxTpr_Contagemresultado_servico_Z
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servico_Z ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servico_Z = (int)(value);
            gxTv_SdtContagemResultadoNotificacao_Demanda_Modified = 1;
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servico_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servico_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servico_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_ServicoSigla_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_ServicoSigla_Z"   )]
      public String gxTpr_Contagemresultado_servicosigla_Z
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servicosigla_Z ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servicosigla_Z = (String)(value);
            gxTv_SdtContagemResultadoNotificacao_Demanda_Modified = 1;
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servicosigla_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servicosigla_Z = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servicosigla_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_PrazoInicialDias_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_PrazoInicialDias_Z"   )]
      public short gxTpr_Contagemresultado_prazoinicialdias_Z
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_prazoinicialdias_Z ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_prazoinicialdias_Z = (short)(value);
            gxTv_SdtContagemResultadoNotificacao_Demanda_Modified = 1;
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_prazoinicialdias_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_prazoinicialdias_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_prazoinicialdias_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Demanda_N" )]
      [  XmlElement( ElementName = "ContagemResultado_Demanda_N"   )]
      public short gxTpr_Contagemresultado_demanda_N
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demanda_N ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demanda_N = (short)(value);
            gxTv_SdtContagemResultadoNotificacao_Demanda_Modified = 1;
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demanda_N_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demanda_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demanda_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_DemandaFM_N" )]
      [  XmlElement( ElementName = "ContagemResultado_DemandaFM_N"   )]
      public short gxTpr_Contagemresultado_demandafm_N
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demandafm_N ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demandafm_N = (short)(value);
            gxTv_SdtContagemResultadoNotificacao_Demanda_Modified = 1;
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demandafm_N_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demandafm_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demandafm_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Servico_N" )]
      [  XmlElement( ElementName = "ContagemResultado_Servico_N"   )]
      public short gxTpr_Contagemresultado_servico_N
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servico_N ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servico_N = (short)(value);
            gxTv_SdtContagemResultadoNotificacao_Demanda_Modified = 1;
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servico_N_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servico_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servico_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_ServicoSigla_N" )]
      [  XmlElement( ElementName = "ContagemResultado_ServicoSigla_N"   )]
      public short gxTpr_Contagemresultado_servicosigla_N
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servicosigla_N ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servicosigla_N = (short)(value);
            gxTv_SdtContagemResultadoNotificacao_Demanda_Modified = 1;
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servicosigla_N_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servicosigla_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servicosigla_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_PrazoInicialDias_N" )]
      [  XmlElement( ElementName = "ContagemResultado_PrazoInicialDias_N"   )]
      public short gxTpr_Contagemresultado_prazoinicialdias_N
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_prazoinicialdias_N ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_prazoinicialdias_N = (short)(value);
            gxTv_SdtContagemResultadoNotificacao_Demanda_Modified = 1;
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_prazoinicialdias_N_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_prazoinicialdias_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_prazoinicialdias_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demanda = "";
         gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demandafm = "";
         gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servicosigla = "";
         gxTv_SdtContagemResultadoNotificacao_Demanda_Mode = "";
         gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demanda_Z = "";
         gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demandafm_Z = "";
         gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servicosigla_Z = "";
         sTagName = "";
         return  ;
      }

      private short gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_prazoinicialdias ;
      private short gxTv_SdtContagemResultadoNotificacao_Demanda_Modified ;
      private short gxTv_SdtContagemResultadoNotificacao_Demanda_Initialized ;
      private short gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_prazoinicialdias_Z ;
      private short gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demanda_N ;
      private short gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demandafm_N ;
      private short gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servico_N ;
      private short gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servicosigla_N ;
      private short gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_prazoinicialdias_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_codigo ;
      private int gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servico ;
      private int gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_codigo_Z ;
      private int gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servico_Z ;
      private String gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servicosigla ;
      private String gxTv_SdtContagemResultadoNotificacao_Demanda_Mode ;
      private String gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_servicosigla_Z ;
      private String sTagName ;
      private String gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demanda ;
      private String gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demandafm ;
      private String gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demanda_Z ;
      private String gxTv_SdtContagemResultadoNotificacao_Demanda_Contagemresultado_demandafm_Z ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"ContagemResultadoNotificacao.Demanda", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtContagemResultadoNotificacao_Demanda_RESTInterface : GxGenericCollectionItem<SdtContagemResultadoNotificacao_Demanda>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContagemResultadoNotificacao_Demanda_RESTInterface( ) : base()
      {
      }

      public SdtContagemResultadoNotificacao_Demanda_RESTInterface( SdtContagemResultadoNotificacao_Demanda psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ContagemResultado_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultado_codigo
      {
         get {
            return sdt.gxTpr_Contagemresultado_codigo ;
         }

         set {
            sdt.gxTpr_Contagemresultado_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_Demanda" , Order = 1 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultado_demanda
      {
         get {
            return sdt.gxTpr_Contagemresultado_demanda ;
         }

         set {
            sdt.gxTpr_Contagemresultado_demanda = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_DemandaFM" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultado_demandafm
      {
         get {
            return sdt.gxTpr_Contagemresultado_demandafm ;
         }

         set {
            sdt.gxTpr_Contagemresultado_demandafm = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_Servico" , Order = 3 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultado_servico
      {
         get {
            return sdt.gxTpr_Contagemresultado_servico ;
         }

         set {
            sdt.gxTpr_Contagemresultado_servico = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_ServicoSigla" , Order = 4 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultado_servicosigla
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_servicosigla) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_servicosigla = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_PrazoInicialDias" , Order = 5 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contagemresultado_prazoinicialdias
      {
         get {
            return sdt.gxTpr_Contagemresultado_prazoinicialdias ;
         }

         set {
            sdt.gxTpr_Contagemresultado_prazoinicialdias = (short)(value.HasValue ? value.Value : 0);
         }

      }

      public SdtContagemResultadoNotificacao_Demanda sdt
      {
         get {
            return (SdtContagemResultadoNotificacao_Demanda)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtContagemResultadoNotificacao_Demanda() ;
         }
      }

   }

}
