/*
               File: TabelaAtributosWC
        Description: Tabela Atributos WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:59:49.84
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class tabelaatributoswc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public tabelaatributoswc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public tabelaatributoswc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Atributos_TabelaCod ,
                           int aP1_Sistema_Codigo )
      {
         this.AV58Atributos_TabelaCod = aP0_Atributos_TabelaCod;
         this.AV59Sistema_Codigo = aP1_Sistema_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavAtributos_tipodados1 = new GXCombobox();
         chkavAtributos_ativo1 = new GXCheckbox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavAtributos_tipodados2 = new GXCombobox();
         chkavAtributos_ativo2 = new GXCheckbox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         cmbavAtributos_tipodados3 = new GXCombobox();
         chkavAtributos_ativo3 = new GXCheckbox();
         cmbAtributos_TipoDados = new GXCombobox();
         chkAtributos_Ativo = new GXCheckbox();
         chkAtributos_PK = new GXCheckbox();
         chkAtributos_FK = new GXCheckbox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV58Atributos_TabelaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV58Atributos_TabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58Atributos_TabelaCod), 6, 0)));
                  AV59Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV59Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59Sistema_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV58Atributos_TabelaCod,(int)AV59Sistema_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_89 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_89_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_89_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV14OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
                  AV15OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
                  AV16DynamicFiltersSelector1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
                  AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
                  AV18Atributos_Nome1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Atributos_Nome1", AV18Atributos_Nome1);
                  AV52Atributos_TipoDados1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52Atributos_TipoDados1", AV52Atributos_TipoDados1);
                  AV53Atributos_Ativo1 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53Atributos_Ativo1", AV53Atributos_Ativo1);
                  AV20DynamicFiltersSelector2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
                  AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
                  AV22Atributos_Nome2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22Atributos_Nome2", AV22Atributos_Nome2);
                  AV54Atributos_TipoDados2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV54Atributos_TipoDados2", AV54Atributos_TipoDados2);
                  AV55Atributos_Ativo2 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV55Atributos_Ativo2", AV55Atributos_Ativo2);
                  AV24DynamicFiltersSelector3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
                  AV25DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
                  AV26Atributos_Nome3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26Atributos_Nome3", AV26Atributos_Nome3);
                  AV56Atributos_TipoDados3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56Atributos_TipoDados3", AV56Atributos_TipoDados3);
                  AV57Atributos_Ativo3 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57Atributos_Ativo3", AV57Atributos_Ativo3);
                  AV19DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
                  AV23DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
                  AV58Atributos_TabelaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV58Atributos_TabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58Atributos_TabelaCod), 6, 0)));
                  AV87Pgmname = GetNextPar( );
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV11GridState);
                  AV28DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28DynamicFiltersIgnoreFirst", AV28DynamicFiltersIgnoreFirst);
                  AV27DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
                  A176Atributos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV59Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV59Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59Sistema_Codigo), 6, 0)));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Atributos_Nome1, AV52Atributos_TipoDados1, AV53Atributos_Ativo1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Atributos_Nome2, AV54Atributos_TipoDados2, AV55Atributos_Ativo2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26Atributos_Nome3, AV56Atributos_TipoDados3, AV57Atributos_Ativo3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV58Atributos_TabelaCod, AV87Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, AV6WWPContext, A176Atributos_Codigo, AV59Sistema_Codigo, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA4A2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV87Pgmname = "TabelaAtributosWC";
               context.Gx_err = 0;
               WS4A2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Tabela Atributos WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042822595018");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("tabelaatributoswc.aspx") + "?" + UrlEncode("" +AV58Atributos_TabelaCod) + "," + UrlEncode("" +AV59Sistema_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV15OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1", AV16DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vATRIBUTOS_NOME1", StringUtil.RTrim( AV18Atributos_Nome1));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vATRIBUTOS_TIPODADOS1", StringUtil.RTrim( AV52Atributos_TipoDados1));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vATRIBUTOS_ATIVO1", StringUtil.BoolToStr( AV53Atributos_Ativo1));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2", AV20DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vATRIBUTOS_NOME2", StringUtil.RTrim( AV22Atributos_Nome2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vATRIBUTOS_TIPODADOS2", StringUtil.RTrim( AV54Atributos_TipoDados2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vATRIBUTOS_ATIVO2", StringUtil.BoolToStr( AV55Atributos_Ativo2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3", AV24DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV25DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vATRIBUTOS_NOME3", StringUtil.RTrim( AV26Atributos_Nome3));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vATRIBUTOS_TIPODADOS3", StringUtil.RTrim( AV56Atributos_TipoDados3));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vATRIBUTOS_ATIVO3", StringUtil.BoolToStr( AV57Atributos_Ativo3));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV19DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV23DynamicFiltersEnabled3));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_89", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_89), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV79GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV80GridPageCount), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV58Atributos_TabelaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV58Atributos_TabelaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV59Sistema_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV59Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vATRIBUTOS_TABELACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV58Atributos_TabelaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV87Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vGRIDSTATE", AV11GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vGRIDSTATE", AV11GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSIGNOREFIRST", AV28DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSREMOVING", AV27DynamicFiltersRemoving);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"vSISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV59Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm4A2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("tabelaatributoswc.js", "?202042822595051");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "TabelaAtributosWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Tabela Atributos WC" ;
      }

      protected void WB4A0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "tabelaatributoswc.aspx");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
            }
            wb_table1_2_4A2( true) ;
         }
         else
         {
            wb_table1_2_4A2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_4A2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtAtributos_TabelaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A356Atributos_TabelaCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A356Atributos_TabelaCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAtributos_TabelaCod_Jsonclick, 0, "Attribute", "", "", "", edtAtributos_TabelaCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_TabelaAtributosWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'" + sPrefix + "',false,'" + sGXsfl_89_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV19DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(108, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,108);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'" + sPrefix + "',false,'" + sGXsfl_89_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV23DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(109, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,109);\"");
         }
         wbLoad = true;
      }

      protected void START4A2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Tabela Atributos WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP4A0( ) ;
            }
         }
      }

      protected void WS4A2( )
      {
         START4A2( ) ;
         EVT4A2( ) ;
      }

      protected void EVT4A2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP4A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP4A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E114A2 */
                                    E114A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP4A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E124A2 */
                                    E124A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP4A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E134A2 */
                                    E134A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP4A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E144A2 */
                                    E144A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP4A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E154A2 */
                                    E154A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP4A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E164A2 */
                                    E164A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP4A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E174A2 */
                                    E174A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP4A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E184A2 */
                                    E184A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP4A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E194A2 */
                                    E194A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP4A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E204A2 */
                                    E204A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP4A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E214A2 */
                                    E214A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP4A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E224A2 */
                                    E224A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP4A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = cmbavOrderedby_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP4A0( ) ;
                              }
                              nGXsfl_89_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_89_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_89_idx), 4, 0)), 4, "0");
                              SubsflControlProps_892( ) ;
                              AV30Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV30Update)) ? AV84Update_GXI : context.convertURL( context.PathToRelativeUrl( AV30Update))));
                              AV29Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV85Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV29Delete))));
                              A176Atributos_Codigo = (int)(context.localUtil.CToN( cgiGet( edtAtributos_Codigo_Internalname), ",", "."));
                              A177Atributos_Nome = StringUtil.Upper( cgiGet( edtAtributos_Nome_Internalname));
                              cmbAtributos_TipoDados.Name = cmbAtributos_TipoDados_Internalname;
                              cmbAtributos_TipoDados.CurrentValue = cgiGet( cmbAtributos_TipoDados_Internalname);
                              A178Atributos_TipoDados = cgiGet( cmbAtributos_TipoDados_Internalname);
                              n178Atributos_TipoDados = false;
                              A179Atributos_Descricao = cgiGet( edtAtributos_Descricao_Internalname);
                              n179Atributos_Descricao = false;
                              A180Atributos_Ativo = StringUtil.StrToBool( cgiGet( chkAtributos_Ativo_Internalname));
                              AV81Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV81Display)) ? AV86Display_GXI : context.convertURL( context.PathToRelativeUrl( AV81Display))));
                              A390Atributos_Detalhes = cgiGet( edtAtributos_Detalhes_Internalname);
                              n390Atributos_Detalhes = false;
                              A400Atributos_PK = StringUtil.StrToBool( cgiGet( chkAtributos_PK_Internalname));
                              n400Atributos_PK = false;
                              A401Atributos_FK = StringUtil.StrToBool( cgiGet( chkAtributos_FK_Internalname));
                              n401Atributos_FK = false;
                              A190Tabela_SistemaCod = (int)(context.localUtil.CToN( cgiGet( edtTabela_SistemaCod_Internalname), ",", "."));
                              n190Tabela_SistemaCod = false;
                              A747Atributos_MelhoraCod = (int)(context.localUtil.CToN( cgiGet( edtAtributos_MelhoraCod_Internalname), ",", "."));
                              n747Atributos_MelhoraCod = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E234A2 */
                                          E234A2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E244A2 */
                                          E244A2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E254A2 */
                                          E254A2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV17DynamicFiltersOperator1 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Atributos_nome1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vATRIBUTOS_NOME1"), AV18Atributos_Nome1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Atributos_tipodados1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vATRIBUTOS_TIPODADOS1"), AV52Atributos_TipoDados1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Atributos_ativo1 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vATRIBUTOS_ATIVO1")) != AV53Atributos_Ativo1 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV21DynamicFiltersOperator2 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Atributos_nome2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vATRIBUTOS_NOME2"), AV22Atributos_Nome2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Atributos_tipodados2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vATRIBUTOS_TIPODADOS2"), AV54Atributos_TipoDados2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Atributos_ativo2 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vATRIBUTOS_ATIVO2")) != AV55Atributos_Ativo2 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3"), AV24DynamicFiltersSelector3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV25DynamicFiltersOperator3 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Atributos_nome3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vATRIBUTOS_NOME3"), AV26Atributos_Nome3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Atributos_tipodados3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vATRIBUTOS_TIPODADOS3"), AV56Atributos_TipoDados3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Atributos_ativo3 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vATRIBUTOS_ATIVO3")) != AV57Atributos_Ativo3 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED3")) != AV23DynamicFiltersEnabled3 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUP4A0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE4A2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm4A2( ) ;
            }
         }
      }

      protected void PA4A2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("ATRIBUTOS_NOME", "Nome do Atributo", 0);
            cmbavDynamicfiltersselector1.addItem("ATRIBUTOS_TIPODADOS", "Tipo de Dados", 0);
            cmbavDynamicfiltersselector1.addItem("ATRIBUTOS_ATIVO", "Ativo", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            cmbavDynamicfiltersoperator1.addItem("2", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("3", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
            }
            cmbavAtributos_tipodados1.Name = "vATRIBUTOS_TIPODADOS1";
            cmbavAtributos_tipodados1.WebTags = "";
            cmbavAtributos_tipodados1.addItem("", "Todos", 0);
            cmbavAtributos_tipodados1.addItem("N", "Numeric", 0);
            cmbavAtributos_tipodados1.addItem("C", "Character", 0);
            cmbavAtributos_tipodados1.addItem("VC", "Varchar", 0);
            cmbavAtributos_tipodados1.addItem("D", "Date", 0);
            cmbavAtributos_tipodados1.addItem("DT", "Date Time", 0);
            cmbavAtributos_tipodados1.addItem("Bool", "Boolean", 0);
            cmbavAtributos_tipodados1.addItem("Blob", "Blob", 0);
            cmbavAtributos_tipodados1.addItem("Outr", "Outros", 0);
            if ( cmbavAtributos_tipodados1.ItemCount > 0 )
            {
               AV52Atributos_TipoDados1 = cmbavAtributos_tipodados1.getValidValue(AV52Atributos_TipoDados1);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52Atributos_TipoDados1", AV52Atributos_TipoDados1);
            }
            chkavAtributos_ativo1.Name = "vATRIBUTOS_ATIVO1";
            chkavAtributos_ativo1.WebTags = "";
            chkavAtributos_ativo1.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavAtributos_ativo1_Internalname, "TitleCaption", chkavAtributos_ativo1.Caption);
            chkavAtributos_ativo1.CheckedValue = "false";
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("ATRIBUTOS_NOME", "Nome do Atributo", 0);
            cmbavDynamicfiltersselector2.addItem("ATRIBUTOS_TIPODADOS", "Tipo de Dados", 0);
            cmbavDynamicfiltersselector2.addItem("ATRIBUTOS_ATIVO", "Ativo", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            cmbavDynamicfiltersoperator2.addItem("2", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("3", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
            }
            cmbavAtributos_tipodados2.Name = "vATRIBUTOS_TIPODADOS2";
            cmbavAtributos_tipodados2.WebTags = "";
            cmbavAtributos_tipodados2.addItem("", "Todos", 0);
            cmbavAtributos_tipodados2.addItem("N", "Numeric", 0);
            cmbavAtributos_tipodados2.addItem("C", "Character", 0);
            cmbavAtributos_tipodados2.addItem("VC", "Varchar", 0);
            cmbavAtributos_tipodados2.addItem("D", "Date", 0);
            cmbavAtributos_tipodados2.addItem("DT", "Date Time", 0);
            cmbavAtributos_tipodados2.addItem("Bool", "Boolean", 0);
            cmbavAtributos_tipodados2.addItem("Blob", "Blob", 0);
            cmbavAtributos_tipodados2.addItem("Outr", "Outros", 0);
            if ( cmbavAtributos_tipodados2.ItemCount > 0 )
            {
               AV54Atributos_TipoDados2 = cmbavAtributos_tipodados2.getValidValue(AV54Atributos_TipoDados2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV54Atributos_TipoDados2", AV54Atributos_TipoDados2);
            }
            chkavAtributos_ativo2.Name = "vATRIBUTOS_ATIVO2";
            chkavAtributos_ativo2.WebTags = "";
            chkavAtributos_ativo2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavAtributos_ativo2_Internalname, "TitleCaption", chkavAtributos_ativo2.Caption);
            chkavAtributos_ativo2.CheckedValue = "false";
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("ATRIBUTOS_NOME", "Nome do Atributo", 0);
            cmbavDynamicfiltersselector3.addItem("ATRIBUTOS_TIPODADOS", "Tipo de Dados", 0);
            cmbavDynamicfiltersselector3.addItem("ATRIBUTOS_ATIVO", "Ativo", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV24DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV24DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            cmbavDynamicfiltersoperator3.addItem("2", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("3", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV25DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
            }
            cmbavAtributos_tipodados3.Name = "vATRIBUTOS_TIPODADOS3";
            cmbavAtributos_tipodados3.WebTags = "";
            cmbavAtributos_tipodados3.addItem("", "Todos", 0);
            cmbavAtributos_tipodados3.addItem("N", "Numeric", 0);
            cmbavAtributos_tipodados3.addItem("C", "Character", 0);
            cmbavAtributos_tipodados3.addItem("VC", "Varchar", 0);
            cmbavAtributos_tipodados3.addItem("D", "Date", 0);
            cmbavAtributos_tipodados3.addItem("DT", "Date Time", 0);
            cmbavAtributos_tipodados3.addItem("Bool", "Boolean", 0);
            cmbavAtributos_tipodados3.addItem("Blob", "Blob", 0);
            cmbavAtributos_tipodados3.addItem("Outr", "Outros", 0);
            if ( cmbavAtributos_tipodados3.ItemCount > 0 )
            {
               AV56Atributos_TipoDados3 = cmbavAtributos_tipodados3.getValidValue(AV56Atributos_TipoDados3);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56Atributos_TipoDados3", AV56Atributos_TipoDados3);
            }
            chkavAtributos_ativo3.Name = "vATRIBUTOS_ATIVO3";
            chkavAtributos_ativo3.WebTags = "";
            chkavAtributos_ativo3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavAtributos_ativo3_Internalname, "TitleCaption", chkavAtributos_ativo3.Caption);
            chkavAtributos_ativo3.CheckedValue = "false";
            GXCCtl = "ATRIBUTOS_TIPODADOS_" + sGXsfl_89_idx;
            cmbAtributos_TipoDados.Name = GXCCtl;
            cmbAtributos_TipoDados.WebTags = "";
            cmbAtributos_TipoDados.addItem("", "Desconhecido", 0);
            cmbAtributos_TipoDados.addItem("N", "Numeric", 0);
            cmbAtributos_TipoDados.addItem("C", "Character", 0);
            cmbAtributos_TipoDados.addItem("VC", "Varchar", 0);
            cmbAtributos_TipoDados.addItem("D", "Date", 0);
            cmbAtributos_TipoDados.addItem("DT", "Date Time", 0);
            cmbAtributos_TipoDados.addItem("Bool", "Boolean", 0);
            cmbAtributos_TipoDados.addItem("Blob", "Blob", 0);
            cmbAtributos_TipoDados.addItem("Outr", "Outros", 0);
            if ( cmbAtributos_TipoDados.ItemCount > 0 )
            {
               A178Atributos_TipoDados = cmbAtributos_TipoDados.getValidValue(A178Atributos_TipoDados);
               n178Atributos_TipoDados = false;
            }
            GXCCtl = "ATRIBUTOS_ATIVO_" + sGXsfl_89_idx;
            chkAtributos_Ativo.Name = GXCCtl;
            chkAtributos_Ativo.WebTags = "";
            chkAtributos_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkAtributos_Ativo_Internalname, "TitleCaption", chkAtributos_Ativo.Caption);
            chkAtributos_Ativo.CheckedValue = "false";
            GXCCtl = "ATRIBUTOS_PK_" + sGXsfl_89_idx;
            chkAtributos_PK.Name = GXCCtl;
            chkAtributos_PK.WebTags = "";
            chkAtributos_PK.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkAtributos_PK_Internalname, "TitleCaption", chkAtributos_PK.Caption);
            chkAtributos_PK.CheckedValue = "false";
            GXCCtl = "ATRIBUTOS_FK_" + sGXsfl_89_idx;
            chkAtributos_FK.Name = GXCCtl;
            chkAtributos_FK.WebTags = "";
            chkAtributos_FK.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkAtributos_FK_Internalname, "TitleCaption", chkAtributos_FK.Caption);
            chkAtributos_FK.CheckedValue = "false";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_892( ) ;
         while ( nGXsfl_89_idx <= nRC_GXsfl_89 )
         {
            sendrow_892( ) ;
            nGXsfl_89_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_89_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_89_idx+1));
            sGXsfl_89_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_89_idx), 4, 0)), 4, "0");
            SubsflControlProps_892( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV14OrderedBy ,
                                       bool AV15OrderedDsc ,
                                       String AV16DynamicFiltersSelector1 ,
                                       short AV17DynamicFiltersOperator1 ,
                                       String AV18Atributos_Nome1 ,
                                       String AV52Atributos_TipoDados1 ,
                                       bool AV53Atributos_Ativo1 ,
                                       String AV20DynamicFiltersSelector2 ,
                                       short AV21DynamicFiltersOperator2 ,
                                       String AV22Atributos_Nome2 ,
                                       String AV54Atributos_TipoDados2 ,
                                       bool AV55Atributos_Ativo2 ,
                                       String AV24DynamicFiltersSelector3 ,
                                       short AV25DynamicFiltersOperator3 ,
                                       String AV26Atributos_Nome3 ,
                                       String AV56Atributos_TipoDados3 ,
                                       bool AV57Atributos_Ativo3 ,
                                       bool AV19DynamicFiltersEnabled2 ,
                                       bool AV23DynamicFiltersEnabled3 ,
                                       int AV58Atributos_TabelaCod ,
                                       String AV87Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV11GridState ,
                                       bool AV28DynamicFiltersIgnoreFirst ,
                                       bool AV27DynamicFiltersRemoving ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       int A176Atributos_Codigo ,
                                       int AV59Sistema_Codigo ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF4A2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ATRIBUTOS_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A176Atributos_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"ATRIBUTOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A176Atributos_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ATRIBUTOS_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A177Atributos_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"ATRIBUTOS_NOME", StringUtil.RTrim( A177Atributos_Nome));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ATRIBUTOS_TIPODADOS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A178Atributos_TipoDados, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"ATRIBUTOS_TIPODADOS", StringUtil.RTrim( A178Atributos_TipoDados));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ATRIBUTOS_DESCRICAO", GetSecureSignedToken( sPrefix, A179Atributos_Descricao));
         GxWebStd.gx_hidden_field( context, sPrefix+"ATRIBUTOS_DESCRICAO", A179Atributos_Descricao);
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ATRIBUTOS_ATIVO", GetSecureSignedToken( sPrefix, A180Atributos_Ativo));
         GxWebStd.gx_hidden_field( context, sPrefix+"ATRIBUTOS_ATIVO", StringUtil.BoolToStr( A180Atributos_Ativo));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ATRIBUTOS_DETALHES", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A390Atributos_Detalhes, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"ATRIBUTOS_DETALHES", StringUtil.RTrim( A390Atributos_Detalhes));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ATRIBUTOS_PK", GetSecureSignedToken( sPrefix, A400Atributos_PK));
         GxWebStd.gx_hidden_field( context, sPrefix+"ATRIBUTOS_PK", StringUtil.BoolToStr( A400Atributos_PK));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ATRIBUTOS_FK", GetSecureSignedToken( sPrefix, A401Atributos_FK));
         GxWebStd.gx_hidden_field( context, sPrefix+"ATRIBUTOS_FK", StringUtil.BoolToStr( A401Atributos_FK));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ATRIBUTOS_MELHORACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A747Atributos_MelhoraCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"ATRIBUTOS_MELHORACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A747Atributos_MelhoraCod), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavAtributos_tipodados1.ItemCount > 0 )
         {
            AV52Atributos_TipoDados1 = cmbavAtributos_tipodados1.getValidValue(AV52Atributos_TipoDados1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52Atributos_TipoDados1", AV52Atributos_TipoDados1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavAtributos_tipodados2.ItemCount > 0 )
         {
            AV54Atributos_TipoDados2 = cmbavAtributos_tipodados2.getValidValue(AV54Atributos_TipoDados2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV54Atributos_TipoDados2", AV54Atributos_TipoDados2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV24DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV24DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV25DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
         }
         if ( cmbavAtributos_tipodados3.ItemCount > 0 )
         {
            AV56Atributos_TipoDados3 = cmbavAtributos_tipodados3.getValidValue(AV56Atributos_TipoDados3);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56Atributos_TipoDados3", AV56Atributos_TipoDados3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF4A2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV87Pgmname = "TabelaAtributosWC";
         context.Gx_err = 0;
      }

      protected void RF4A2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 89;
         /* Execute user event: E244A2 */
         E244A2 ();
         nGXsfl_89_idx = 1;
         sGXsfl_89_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_89_idx), 4, 0)), 4, "0");
         SubsflControlProps_892( ) ;
         nGXsfl_89_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_892( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV16DynamicFiltersSelector1 ,
                                                 AV17DynamicFiltersOperator1 ,
                                                 AV18Atributos_Nome1 ,
                                                 AV52Atributos_TipoDados1 ,
                                                 AV53Atributos_Ativo1 ,
                                                 AV19DynamicFiltersEnabled2 ,
                                                 AV20DynamicFiltersSelector2 ,
                                                 AV21DynamicFiltersOperator2 ,
                                                 AV22Atributos_Nome2 ,
                                                 AV54Atributos_TipoDados2 ,
                                                 AV55Atributos_Ativo2 ,
                                                 AV23DynamicFiltersEnabled3 ,
                                                 AV24DynamicFiltersSelector3 ,
                                                 AV25DynamicFiltersOperator3 ,
                                                 AV26Atributos_Nome3 ,
                                                 AV56Atributos_TipoDados3 ,
                                                 AV57Atributos_Ativo3 ,
                                                 A177Atributos_Nome ,
                                                 A178Atributos_TipoDados ,
                                                 A180Atributos_Ativo ,
                                                 AV14OrderedBy ,
                                                 AV15OrderedDsc ,
                                                 A356Atributos_TabelaCod ,
                                                 AV58Atributos_TabelaCod },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            lV18Atributos_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV18Atributos_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Atributos_Nome1", AV18Atributos_Nome1);
            lV18Atributos_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV18Atributos_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Atributos_Nome1", AV18Atributos_Nome1);
            lV52Atributos_TipoDados1 = StringUtil.PadR( StringUtil.RTrim( AV52Atributos_TipoDados1), 4, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52Atributos_TipoDados1", AV52Atributos_TipoDados1);
            lV52Atributos_TipoDados1 = StringUtil.PadR( StringUtil.RTrim( AV52Atributos_TipoDados1), 4, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52Atributos_TipoDados1", AV52Atributos_TipoDados1);
            lV22Atributos_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV22Atributos_Nome2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22Atributos_Nome2", AV22Atributos_Nome2);
            lV22Atributos_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV22Atributos_Nome2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22Atributos_Nome2", AV22Atributos_Nome2);
            lV54Atributos_TipoDados2 = StringUtil.PadR( StringUtil.RTrim( AV54Atributos_TipoDados2), 4, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV54Atributos_TipoDados2", AV54Atributos_TipoDados2);
            lV54Atributos_TipoDados2 = StringUtil.PadR( StringUtil.RTrim( AV54Atributos_TipoDados2), 4, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV54Atributos_TipoDados2", AV54Atributos_TipoDados2);
            lV26Atributos_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV26Atributos_Nome3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26Atributos_Nome3", AV26Atributos_Nome3);
            lV26Atributos_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV26Atributos_Nome3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26Atributos_Nome3", AV26Atributos_Nome3);
            lV56Atributos_TipoDados3 = StringUtil.PadR( StringUtil.RTrim( AV56Atributos_TipoDados3), 4, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56Atributos_TipoDados3", AV56Atributos_TipoDados3);
            lV56Atributos_TipoDados3 = StringUtil.PadR( StringUtil.RTrim( AV56Atributos_TipoDados3), 4, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56Atributos_TipoDados3", AV56Atributos_TipoDados3);
            /* Using cursor H004A2 */
            pr_default.execute(0, new Object[] {AV58Atributos_TabelaCod, lV18Atributos_Nome1, lV18Atributos_Nome1, AV52Atributos_TipoDados1, AV52Atributos_TipoDados1, lV52Atributos_TipoDados1, lV52Atributos_TipoDados1, AV53Atributos_Ativo1, lV22Atributos_Nome2, lV22Atributos_Nome2, AV54Atributos_TipoDados2, AV54Atributos_TipoDados2, lV54Atributos_TipoDados2, lV54Atributos_TipoDados2, AV55Atributos_Ativo2, lV26Atributos_Nome3, lV26Atributos_Nome3, AV56Atributos_TipoDados3, AV56Atributos_TipoDados3, lV56Atributos_TipoDados3, lV56Atributos_TipoDados3, AV57Atributos_Ativo3, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_89_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A356Atributos_TabelaCod = H004A2_A356Atributos_TabelaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A356Atributos_TabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A356Atributos_TabelaCod), 6, 0)));
               A747Atributos_MelhoraCod = H004A2_A747Atributos_MelhoraCod[0];
               n747Atributos_MelhoraCod = H004A2_n747Atributos_MelhoraCod[0];
               A190Tabela_SistemaCod = H004A2_A190Tabela_SistemaCod[0];
               n190Tabela_SistemaCod = H004A2_n190Tabela_SistemaCod[0];
               A401Atributos_FK = H004A2_A401Atributos_FK[0];
               n401Atributos_FK = H004A2_n401Atributos_FK[0];
               A400Atributos_PK = H004A2_A400Atributos_PK[0];
               n400Atributos_PK = H004A2_n400Atributos_PK[0];
               A390Atributos_Detalhes = H004A2_A390Atributos_Detalhes[0];
               n390Atributos_Detalhes = H004A2_n390Atributos_Detalhes[0];
               A180Atributos_Ativo = H004A2_A180Atributos_Ativo[0];
               A179Atributos_Descricao = H004A2_A179Atributos_Descricao[0];
               n179Atributos_Descricao = H004A2_n179Atributos_Descricao[0];
               A178Atributos_TipoDados = H004A2_A178Atributos_TipoDados[0];
               n178Atributos_TipoDados = H004A2_n178Atributos_TipoDados[0];
               A177Atributos_Nome = H004A2_A177Atributos_Nome[0];
               A176Atributos_Codigo = H004A2_A176Atributos_Codigo[0];
               A190Tabela_SistemaCod = H004A2_A190Tabela_SistemaCod[0];
               n190Tabela_SistemaCod = H004A2_n190Tabela_SistemaCod[0];
               /* Execute user event: E254A2 */
               E254A2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 89;
            WB4A0( ) ;
         }
         nGXsfl_89_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV16DynamicFiltersSelector1 ,
                                              AV17DynamicFiltersOperator1 ,
                                              AV18Atributos_Nome1 ,
                                              AV52Atributos_TipoDados1 ,
                                              AV53Atributos_Ativo1 ,
                                              AV19DynamicFiltersEnabled2 ,
                                              AV20DynamicFiltersSelector2 ,
                                              AV21DynamicFiltersOperator2 ,
                                              AV22Atributos_Nome2 ,
                                              AV54Atributos_TipoDados2 ,
                                              AV55Atributos_Ativo2 ,
                                              AV23DynamicFiltersEnabled3 ,
                                              AV24DynamicFiltersSelector3 ,
                                              AV25DynamicFiltersOperator3 ,
                                              AV26Atributos_Nome3 ,
                                              AV56Atributos_TipoDados3 ,
                                              AV57Atributos_Ativo3 ,
                                              A177Atributos_Nome ,
                                              A178Atributos_TipoDados ,
                                              A180Atributos_Ativo ,
                                              AV14OrderedBy ,
                                              AV15OrderedDsc ,
                                              A356Atributos_TabelaCod ,
                                              AV58Atributos_TabelaCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV18Atributos_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV18Atributos_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Atributos_Nome1", AV18Atributos_Nome1);
         lV18Atributos_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV18Atributos_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Atributos_Nome1", AV18Atributos_Nome1);
         lV52Atributos_TipoDados1 = StringUtil.PadR( StringUtil.RTrim( AV52Atributos_TipoDados1), 4, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52Atributos_TipoDados1", AV52Atributos_TipoDados1);
         lV52Atributos_TipoDados1 = StringUtil.PadR( StringUtil.RTrim( AV52Atributos_TipoDados1), 4, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52Atributos_TipoDados1", AV52Atributos_TipoDados1);
         lV22Atributos_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV22Atributos_Nome2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22Atributos_Nome2", AV22Atributos_Nome2);
         lV22Atributos_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV22Atributos_Nome2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22Atributos_Nome2", AV22Atributos_Nome2);
         lV54Atributos_TipoDados2 = StringUtil.PadR( StringUtil.RTrim( AV54Atributos_TipoDados2), 4, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV54Atributos_TipoDados2", AV54Atributos_TipoDados2);
         lV54Atributos_TipoDados2 = StringUtil.PadR( StringUtil.RTrim( AV54Atributos_TipoDados2), 4, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV54Atributos_TipoDados2", AV54Atributos_TipoDados2);
         lV26Atributos_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV26Atributos_Nome3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26Atributos_Nome3", AV26Atributos_Nome3);
         lV26Atributos_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV26Atributos_Nome3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26Atributos_Nome3", AV26Atributos_Nome3);
         lV56Atributos_TipoDados3 = StringUtil.PadR( StringUtil.RTrim( AV56Atributos_TipoDados3), 4, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56Atributos_TipoDados3", AV56Atributos_TipoDados3);
         lV56Atributos_TipoDados3 = StringUtil.PadR( StringUtil.RTrim( AV56Atributos_TipoDados3), 4, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56Atributos_TipoDados3", AV56Atributos_TipoDados3);
         /* Using cursor H004A3 */
         pr_default.execute(1, new Object[] {AV58Atributos_TabelaCod, AV58Atributos_TabelaCod, lV18Atributos_Nome1, lV18Atributos_Nome1, AV52Atributos_TipoDados1, AV52Atributos_TipoDados1, lV52Atributos_TipoDados1, lV52Atributos_TipoDados1, AV53Atributos_Ativo1, lV22Atributos_Nome2, lV22Atributos_Nome2, AV54Atributos_TipoDados2, AV54Atributos_TipoDados2, lV54Atributos_TipoDados2, lV54Atributos_TipoDados2, AV55Atributos_Ativo2, lV26Atributos_Nome3, lV26Atributos_Nome3, AV56Atributos_TipoDados3, AV56Atributos_TipoDados3, lV56Atributos_TipoDados3, lV56Atributos_TipoDados3, AV57Atributos_Ativo3});
         GRID_nRecordCount = H004A3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Atributos_Nome1, AV52Atributos_TipoDados1, AV53Atributos_Ativo1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Atributos_Nome2, AV54Atributos_TipoDados2, AV55Atributos_Ativo2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26Atributos_Nome3, AV56Atributos_TipoDados3, AV57Atributos_Ativo3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV58Atributos_TabelaCod, AV87Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, AV6WWPContext, A176Atributos_Codigo, AV59Sistema_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Atributos_Nome1, AV52Atributos_TipoDados1, AV53Atributos_Ativo1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Atributos_Nome2, AV54Atributos_TipoDados2, AV55Atributos_Ativo2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26Atributos_Nome3, AV56Atributos_TipoDados3, AV57Atributos_Ativo3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV58Atributos_TabelaCod, AV87Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, AV6WWPContext, A176Atributos_Codigo, AV59Sistema_Codigo, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Atributos_Nome1, AV52Atributos_TipoDados1, AV53Atributos_Ativo1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Atributos_Nome2, AV54Atributos_TipoDados2, AV55Atributos_Ativo2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26Atributos_Nome3, AV56Atributos_TipoDados3, AV57Atributos_Ativo3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV58Atributos_TabelaCod, AV87Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, AV6WWPContext, A176Atributos_Codigo, AV59Sistema_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Atributos_Nome1, AV52Atributos_TipoDados1, AV53Atributos_Ativo1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Atributos_Nome2, AV54Atributos_TipoDados2, AV55Atributos_Ativo2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26Atributos_Nome3, AV56Atributos_TipoDados3, AV57Atributos_Ativo3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV58Atributos_TabelaCod, AV87Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, AV6WWPContext, A176Atributos_Codigo, AV59Sistema_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Atributos_Nome1, AV52Atributos_TipoDados1, AV53Atributos_Ativo1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Atributos_Nome2, AV54Atributos_TipoDados2, AV55Atributos_Ativo2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26Atributos_Nome3, AV56Atributos_TipoDados3, AV57Atributos_Ativo3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV58Atributos_TabelaCod, AV87Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, AV6WWPContext, A176Atributos_Codigo, AV59Sistema_Codigo, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUP4A0( )
      {
         /* Before Start, stand alone formulas. */
         AV87Pgmname = "TabelaAtributosWC";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E234A2 */
         E234A2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV14OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV16DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
            AV18Atributos_Nome1 = StringUtil.Upper( cgiGet( edtavAtributos_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Atributos_Nome1", AV18Atributos_Nome1);
            cmbavAtributos_tipodados1.Name = cmbavAtributos_tipodados1_Internalname;
            cmbavAtributos_tipodados1.CurrentValue = cgiGet( cmbavAtributos_tipodados1_Internalname);
            AV52Atributos_TipoDados1 = cgiGet( cmbavAtributos_tipodados1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52Atributos_TipoDados1", AV52Atributos_TipoDados1);
            AV53Atributos_Ativo1 = StringUtil.StrToBool( cgiGet( chkavAtributos_ativo1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53Atributos_Ativo1", AV53Atributos_Ativo1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV20DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
            AV22Atributos_Nome2 = StringUtil.Upper( cgiGet( edtavAtributos_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22Atributos_Nome2", AV22Atributos_Nome2);
            cmbavAtributos_tipodados2.Name = cmbavAtributos_tipodados2_Internalname;
            cmbavAtributos_tipodados2.CurrentValue = cgiGet( cmbavAtributos_tipodados2_Internalname);
            AV54Atributos_TipoDados2 = cgiGet( cmbavAtributos_tipodados2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV54Atributos_TipoDados2", AV54Atributos_TipoDados2);
            AV55Atributos_Ativo2 = StringUtil.StrToBool( cgiGet( chkavAtributos_ativo2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV55Atributos_Ativo2", AV55Atributos_Ativo2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV24DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV25DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
            AV26Atributos_Nome3 = StringUtil.Upper( cgiGet( edtavAtributos_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26Atributos_Nome3", AV26Atributos_Nome3);
            cmbavAtributos_tipodados3.Name = cmbavAtributos_tipodados3_Internalname;
            cmbavAtributos_tipodados3.CurrentValue = cgiGet( cmbavAtributos_tipodados3_Internalname);
            AV56Atributos_TipoDados3 = cgiGet( cmbavAtributos_tipodados3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56Atributos_TipoDados3", AV56Atributos_TipoDados3);
            AV57Atributos_Ativo3 = StringUtil.StrToBool( cgiGet( chkavAtributos_ativo3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57Atributos_Ativo3", AV57Atributos_Ativo3);
            A356Atributos_TabelaCod = (int)(context.localUtil.CToN( cgiGet( edtAtributos_TabelaCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A356Atributos_TabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A356Atributos_TabelaCod), 6, 0)));
            AV19DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
            AV23DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
            /* Read saved values. */
            nRC_GXsfl_89 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_89"), ",", "."));
            AV79GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV80GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV58Atributos_TabelaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV58Atributos_TabelaCod"), ",", "."));
            wcpOAV59Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV59Sistema_Codigo"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV17DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vATRIBUTOS_NOME1"), AV18Atributos_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vATRIBUTOS_TIPODADOS1"), AV52Atributos_TipoDados1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vATRIBUTOS_ATIVO1")) != AV53Atributos_Ativo1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV21DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vATRIBUTOS_NOME2"), AV22Atributos_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vATRIBUTOS_TIPODADOS2"), AV54Atributos_TipoDados2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vATRIBUTOS_ATIVO2")) != AV55Atributos_Ativo2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3"), AV24DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV25DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vATRIBUTOS_NOME3"), AV26Atributos_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vATRIBUTOS_TIPODADOS3"), AV56Atributos_TipoDados3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vATRIBUTOS_ATIVO3")) != AV57Atributos_Ativo3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED3")) != AV23DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E234A2 */
         E234A2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E234A2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV52Atributos_TipoDados1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52Atributos_TipoDados1", AV52Atributos_TipoDados1);
         AV16DynamicFiltersSelector1 = "ATRIBUTOS_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV54Atributos_TipoDados2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV54Atributos_TipoDados2", AV54Atributos_TipoDados2);
         AV20DynamicFiltersSelector2 = "ATRIBUTOS_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV56Atributos_TipoDados3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56Atributos_TipoDados3", AV56Atributos_TipoDados3);
         AV24DynamicFiltersSelector3 = "ATRIBUTOS_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtAtributos_TabelaCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtAtributos_TabelaCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAtributos_TabelaCod_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Nome do Atributo", 0);
         cmbavOrderedby.addItem("2", "Tipo de Dados", 0);
         cmbavOrderedby.addItem("3", "Descri��o", 0);
         cmbavOrderedby.addItem("4", "Ativo", 0);
         cmbavOrderedby.addItem("5", "Detalhes", 0);
         cmbavOrderedby.addItem("6", "PK", 0);
         cmbavOrderedby.addItem("7", "FK", 0);
         cmbavOrderedby.addItem("8", "Sistema", 0);
         cmbavOrderedby.addItem("9", "a melhorar", 0);
         if ( AV14OrderedBy < 1 )
         {
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
      }

      protected void E244A2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         cmbavDynamicfiltersoperator1.removeAllItems();
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ATRIBUTOS_NOME") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            cmbavDynamicfiltersoperator1.addItem("2", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("3", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ATRIBUTOS_TIPODADOS") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Igual", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Diferente", 0);
            cmbavDynamicfiltersoperator1.addItem("2", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("3", "Cont�m", 0);
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            cmbavDynamicfiltersoperator2.removeAllItems();
            if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ATRIBUTOS_NOME") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
               cmbavDynamicfiltersoperator2.addItem("2", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("3", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ATRIBUTOS_TIPODADOS") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Igual", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Diferente", 0);
               cmbavDynamicfiltersoperator2.addItem("2", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("3", "Cont�m", 0);
            }
            if ( AV23DynamicFiltersEnabled3 )
            {
               cmbavDynamicfiltersoperator3.removeAllItems();
               if ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ATRIBUTOS_NOME") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
                  cmbavDynamicfiltersoperator3.addItem("2", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("3", "Cont�m", 0);
               }
               else if ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ATRIBUTOS_TIPODADOS") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Igual", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Diferente", 0);
                  cmbavDynamicfiltersoperator3.addItem("2", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("3", "Cont�m", 0);
               }
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtAtributos_Nome_Titleformat = 2;
         edtAtributos_Nome_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 1);' >%5</span>", ((AV14OrderedBy==1) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Nome", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtAtributos_Nome_Internalname, "Title", edtAtributos_Nome_Title);
         cmbAtributos_TipoDados_Titleformat = 2;
         cmbAtributos_TipoDados.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 2);' >%5</span>", ((AV14OrderedBy==2) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Tipo de Dados", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbAtributos_TipoDados_Internalname, "Title", cmbAtributos_TipoDados.Title.Text);
         edtAtributos_Descricao_Titleformat = 2;
         edtAtributos_Descricao_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 3);' >%5</span>", ((AV14OrderedBy==3) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Descri��o", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtAtributos_Descricao_Internalname, "Title", edtAtributos_Descricao_Title);
         chkAtributos_Ativo_Titleformat = 2;
         chkAtributos_Ativo.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 4);' >%5</span>", ((AV14OrderedBy==4) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Ativo", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkAtributos_Ativo_Internalname, "Title", chkAtributos_Ativo.Title.Text);
         edtAtributos_Detalhes_Titleformat = 2;
         edtAtributos_Detalhes_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 5);' >%5</span>", ((AV14OrderedBy==5) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Detalhes", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtAtributos_Detalhes_Internalname, "Title", edtAtributos_Detalhes_Title);
         chkAtributos_PK_Titleformat = 2;
         chkAtributos_PK.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 6);' >%5</span>", ((AV14OrderedBy==6) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "PK", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkAtributos_PK_Internalname, "Title", chkAtributos_PK.Title.Text);
         chkAtributos_FK_Titleformat = 2;
         chkAtributos_FK.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 7);' >%5</span>", ((AV14OrderedBy==7) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "FK", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkAtributos_FK_Internalname, "Title", chkAtributos_FK.Title.Text);
         edtTabela_SistemaCod_Titleformat = 2;
         edtTabela_SistemaCod_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 8);' >%5</span>", ((AV14OrderedBy==8) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Sistema", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtTabela_SistemaCod_Internalname, "Title", edtTabela_SistemaCod_Title);
         edtAtributos_MelhoraCod_Titleformat = 2;
         edtAtributos_MelhoraCod_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 9);' >%5</span>", ((AV14OrderedBy==9) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "a melhorar", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtAtributos_MelhoraCod_Internalname, "Title", edtAtributos_MelhoraCod_Title);
         AV79GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV79GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV79GridCurrentPage), 10, 0)));
         AV80GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV80GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV80GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV6WWPContext", AV6WWPContext);
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
      }

      protected void E114A2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV78PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV78PageToGo) ;
         }
      }

      private void E254A2( )
      {
         /* Grid_Load Routine */
         edtavUpdate_Tooltiptext = "Modifica";
         if ( AV6WWPContext.gxTpr_Update )
         {
            edtavUpdate_Link = formatLink("atributos.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A176Atributos_Codigo) + "," + UrlEncode("" +AV59Sistema_Codigo);
            AV30Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV30Update);
            AV84Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            edtavUpdate_Enabled = 1;
         }
         else
         {
            edtavUpdate_Link = "";
            AV30Update = context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV30Update);
            AV84Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( )));
            edtavUpdate_Enabled = 0;
         }
         edtavDelete_Tooltiptext = "Eliminar";
         if ( AV6WWPContext.gxTpr_Delete )
         {
            edtavDelete_Link = formatLink("atributos.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A176Atributos_Codigo) + "," + UrlEncode("" +AV59Sistema_Codigo);
            AV29Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV29Delete);
            AV85Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavDelete_Enabled = 1;
         }
         else
         {
            edtavDelete_Link = "";
            AV29Delete = context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV29Delete);
            AV85Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( )));
            edtavDelete_Enabled = 0;
         }
         edtavDisplay_Tooltiptext = "Mostrar";
         if ( AV6WWPContext.gxTpr_Display )
         {
            edtavDisplay_Link = formatLink("viewatributos.aspx") + "?" + UrlEncode("" +A176Atributos_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
            AV81Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDisplay_Internalname, AV81Display);
            AV86Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
            edtavDisplay_Enabled = 1;
         }
         else
         {
            edtavDisplay_Link = "";
            AV81Display = context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDisplay_Internalname, AV81Display);
            AV86Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( )));
            edtavDisplay_Enabled = 0;
         }
         edtAtributos_Nome_Link = formatLink("viewatributos.aspx") + "?" + UrlEncode("" +A176Atributos_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 89;
         }
         sendrow_892( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_89_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(89, GridRow);
         }
      }

      protected void E124A2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E184A2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV19DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Atributos_Nome1, AV52Atributos_TipoDados1, AV53Atributos_Ativo1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Atributos_Nome2, AV54Atributos_TipoDados2, AV55Atributos_Ativo2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26Atributos_Nome3, AV56Atributos_TipoDados3, AV57Atributos_Ativo3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV58Atributos_TabelaCod, AV87Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, AV6WWPContext, A176Atributos_Codigo, AV59Sistema_Codigo, sPrefix) ;
      }

      protected void E134A2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV27DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         AV28DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28DynamicFiltersIgnoreFirst", AV28DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV27DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         AV28DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28DynamicFiltersIgnoreFirst", AV28DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Atributos_Nome1, AV52Atributos_TipoDados1, AV53Atributos_Ativo1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Atributos_Nome2, AV54Atributos_TipoDados2, AV55Atributos_Ativo2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26Atributos_Nome3, AV56Atributos_TipoDados3, AV57Atributos_Ativo3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV58Atributos_TabelaCod, AV87Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, AV6WWPContext, A176Atributos_Codigo, AV59Sistema_Codigo, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavAtributos_tipodados1.CurrentValue = StringUtil.RTrim( AV52Atributos_TipoDados1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavAtributos_tipodados1_Internalname, "Values", cmbavAtributos_tipodados1.ToJavascriptSource());
         cmbavAtributos_tipodados2.CurrentValue = StringUtil.RTrim( AV54Atributos_TipoDados2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavAtributos_tipodados2_Internalname, "Values", cmbavAtributos_tipodados2.ToJavascriptSource());
         cmbavAtributos_tipodados3.CurrentValue = StringUtil.RTrim( AV56Atributos_TipoDados3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavAtributos_tipodados3_Internalname, "Values", cmbavAtributos_tipodados3.ToJavascriptSource());
      }

      protected void E194A2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         AV17DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E204A2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV23DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Atributos_Nome1, AV52Atributos_TipoDados1, AV53Atributos_Ativo1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Atributos_Nome2, AV54Atributos_TipoDados2, AV55Atributos_Ativo2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26Atributos_Nome3, AV56Atributos_TipoDados3, AV57Atributos_Ativo3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV58Atributos_TabelaCod, AV87Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, AV6WWPContext, A176Atributos_Codigo, AV59Sistema_Codigo, sPrefix) ;
      }

      protected void E144A2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV27DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV27DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Atributos_Nome1, AV52Atributos_TipoDados1, AV53Atributos_Ativo1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Atributos_Nome2, AV54Atributos_TipoDados2, AV55Atributos_Ativo2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26Atributos_Nome3, AV56Atributos_TipoDados3, AV57Atributos_Ativo3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV58Atributos_TabelaCod, AV87Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, AV6WWPContext, A176Atributos_Codigo, AV59Sistema_Codigo, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavAtributos_tipodados1.CurrentValue = StringUtil.RTrim( AV52Atributos_TipoDados1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavAtributos_tipodados1_Internalname, "Values", cmbavAtributos_tipodados1.ToJavascriptSource());
         cmbavAtributos_tipodados2.CurrentValue = StringUtil.RTrim( AV54Atributos_TipoDados2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavAtributos_tipodados2_Internalname, "Values", cmbavAtributos_tipodados2.ToJavascriptSource());
         cmbavAtributos_tipodados3.CurrentValue = StringUtil.RTrim( AV56Atributos_TipoDados3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavAtributos_tipodados3_Internalname, "Values", cmbavAtributos_tipodados3.ToJavascriptSource());
      }

      protected void E214A2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         AV21DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E154A2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV27DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         AV23DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV27DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Atributos_Nome1, AV52Atributos_TipoDados1, AV53Atributos_Ativo1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Atributos_Nome2, AV54Atributos_TipoDados2, AV55Atributos_Ativo2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26Atributos_Nome3, AV56Atributos_TipoDados3, AV57Atributos_Ativo3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV58Atributos_TabelaCod, AV87Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, AV6WWPContext, A176Atributos_Codigo, AV59Sistema_Codigo, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavAtributos_tipodados1.CurrentValue = StringUtil.RTrim( AV52Atributos_TipoDados1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavAtributos_tipodados1_Internalname, "Values", cmbavAtributos_tipodados1.ToJavascriptSource());
         cmbavAtributos_tipodados2.CurrentValue = StringUtil.RTrim( AV54Atributos_TipoDados2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavAtributos_tipodados2_Internalname, "Values", cmbavAtributos_tipodados2.ToJavascriptSource());
         cmbavAtributos_tipodados3.CurrentValue = StringUtil.RTrim( AV56Atributos_TipoDados3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavAtributos_tipodados3_Internalname, "Values", cmbavAtributos_tipodados3.ToJavascriptSource());
      }

      protected void E224A2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         AV25DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E164A2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefreshCmp(sPrefix);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavAtributos_tipodados1.CurrentValue = StringUtil.RTrim( AV52Atributos_TipoDados1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavAtributos_tipodados1_Internalname, "Values", cmbavAtributos_tipodados1.ToJavascriptSource());
         cmbavAtributos_tipodados2.CurrentValue = StringUtil.RTrim( AV54Atributos_TipoDados2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavAtributos_tipodados2_Internalname, "Values", cmbavAtributos_tipodados2.ToJavascriptSource());
         cmbavAtributos_tipodados3.CurrentValue = StringUtil.RTrim( AV56Atributos_TipoDados3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavAtributos_tipodados3_Internalname, "Values", cmbavAtributos_tipodados3.ToJavascriptSource());
      }

      protected void E174A2( )
      {
         /* 'DoInsert' Routine */
         new prc_newatributo(context ).execute(  AV58Atributos_TabelaCod,  AV59Sistema_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV58Atributos_TabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58Atributos_TabelaCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV59Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59Sistema_Codigo), 6, 0)));
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavAtributos_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAtributos_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAtributos_nome1_Visible), 5, 0)));
         cmbavAtributos_tipodados1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavAtributos_tipodados1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavAtributos_tipodados1.Visible), 5, 0)));
         chkavAtributos_ativo1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavAtributos_ativo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavAtributos_ativo1.Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ATRIBUTOS_NOME") == 0 )
         {
            edtavAtributos_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAtributos_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAtributos_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ATRIBUTOS_TIPODADOS") == 0 )
         {
            cmbavAtributos_tipodados1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavAtributos_tipodados1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavAtributos_tipodados1.Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ATRIBUTOS_ATIVO") == 0 )
         {
            chkavAtributos_ativo1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavAtributos_ativo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavAtributos_ativo1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavAtributos_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAtributos_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAtributos_nome2_Visible), 5, 0)));
         cmbavAtributos_tipodados2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavAtributos_tipodados2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavAtributos_tipodados2.Visible), 5, 0)));
         chkavAtributos_ativo2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavAtributos_ativo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavAtributos_ativo2.Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ATRIBUTOS_NOME") == 0 )
         {
            edtavAtributos_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAtributos_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAtributos_nome2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ATRIBUTOS_TIPODADOS") == 0 )
         {
            cmbavAtributos_tipodados2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavAtributos_tipodados2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavAtributos_tipodados2.Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ATRIBUTOS_ATIVO") == 0 )
         {
            chkavAtributos_ativo2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavAtributos_ativo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavAtributos_ativo2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavAtributos_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAtributos_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAtributos_nome3_Visible), 5, 0)));
         cmbavAtributos_tipodados3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavAtributos_tipodados3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavAtributos_tipodados3.Visible), 5, 0)));
         chkavAtributos_ativo3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavAtributos_ativo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavAtributos_ativo3.Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ATRIBUTOS_NOME") == 0 )
         {
            edtavAtributos_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAtributos_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAtributos_nome3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ATRIBUTOS_TIPODADOS") == 0 )
         {
            cmbavAtributos_tipodados3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavAtributos_tipodados3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavAtributos_tipodados3.Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ATRIBUTOS_ATIVO") == 0 )
         {
            chkavAtributos_ativo3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavAtributos_ativo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavAtributos_ativo3.Visible), 5, 0)));
         }
      }

      protected void S182( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         AV20DynamicFiltersSelector2 = "ATRIBUTOS_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         AV21DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         AV22Atributos_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22Atributos_Nome2", AV22Atributos_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
         AV24DynamicFiltersSelector3 = "ATRIBUTOS_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
         AV25DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
         AV26Atributos_Nome3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26Atributos_Nome3", AV26Atributos_Nome3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'CLEANFILTERS' Routine */
         AV16DynamicFiltersSelector1 = "ATRIBUTOS_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         AV17DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         AV18Atributos_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Atributos_Nome1", AV18Atributos_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV31Session.Get(AV87Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV87Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV31Session.Get(AV87Pgmname+"GridState"), "");
         }
         AV14OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         AV15OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S192( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(1));
            AV16DynamicFiltersSelector1 = AV13GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ATRIBUTOS_NOME") == 0 )
            {
               AV17DynamicFiltersOperator1 = AV13GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV18Atributos_Nome1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Atributos_Nome1", AV18Atributos_Nome1);
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ATRIBUTOS_TIPODADOS") == 0 )
            {
               AV17DynamicFiltersOperator1 = AV13GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV52Atributos_TipoDados1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52Atributos_TipoDados1", AV52Atributos_TipoDados1);
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ATRIBUTOS_ATIVO") == 0 )
            {
               AV53Atributos_Ativo1 = BooleanUtil.Val( AV13GridStateDynamicFilter.gxTpr_Value);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53Atributos_Ativo1", AV53Atributos_Ativo1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV19DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(2));
               AV20DynamicFiltersSelector2 = AV13GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ATRIBUTOS_NOME") == 0 )
               {
                  AV21DynamicFiltersOperator2 = AV13GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
                  AV22Atributos_Nome2 = AV13GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22Atributos_Nome2", AV22Atributos_Nome2);
               }
               else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ATRIBUTOS_TIPODADOS") == 0 )
               {
                  AV21DynamicFiltersOperator2 = AV13GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
                  AV54Atributos_TipoDados2 = AV13GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV54Atributos_TipoDados2", AV54Atributos_TipoDados2);
               }
               else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ATRIBUTOS_ATIVO") == 0 )
               {
                  AV55Atributos_Ativo2 = BooleanUtil.Val( AV13GridStateDynamicFilter.gxTpr_Value);
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV55Atributos_Ativo2", AV55Atributos_Ativo2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV23DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
                  AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(3));
                  AV24DynamicFiltersSelector3 = AV13GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ATRIBUTOS_NOME") == 0 )
                  {
                     AV25DynamicFiltersOperator3 = AV13GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
                     AV26Atributos_Nome3 = AV13GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26Atributos_Nome3", AV26Atributos_Nome3);
                  }
                  else if ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ATRIBUTOS_TIPODADOS") == 0 )
                  {
                     AV25DynamicFiltersOperator3 = AV13GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
                     AV56Atributos_TipoDados3 = AV13GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56Atributos_TipoDados3", AV56Atributos_TipoDados3);
                  }
                  else if ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ATRIBUTOS_ATIVO") == 0 )
                  {
                     AV57Atributos_Ativo3 = BooleanUtil.Val( AV13GridStateDynamicFilter.gxTpr_Value);
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57Atributos_Ativo3", AV57Atributos_Ativo3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV27DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV31Session.Get(AV87Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV14OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV15OrderedDsc;
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV87Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S172( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV28DynamicFiltersIgnoreFirst )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV16DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ATRIBUTOS_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Atributos_Nome1)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV18Atributos_Nome1;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV17DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ATRIBUTOS_TIPODADOS") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV52Atributos_TipoDados1)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV52Atributos_TipoDados1;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV17DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ATRIBUTOS_ATIVO") == 0 ) && ! (false==AV53Atributos_Ativo1) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = StringUtil.BoolToStr( AV53Atributos_Ativo1);
            }
            if ( AV27DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV20DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ATRIBUTOS_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV22Atributos_Nome2)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV22Atributos_Nome2;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV21DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ATRIBUTOS_TIPODADOS") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV54Atributos_TipoDados2)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV54Atributos_TipoDados2;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV21DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ATRIBUTOS_ATIVO") == 0 ) && ! (false==AV55Atributos_Ativo2) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = StringUtil.BoolToStr( AV55Atributos_Ativo2);
            }
            if ( AV27DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV23DynamicFiltersEnabled3 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV24DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ATRIBUTOS_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV26Atributos_Nome3)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV26Atributos_Nome3;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV25DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ATRIBUTOS_TIPODADOS") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV56Atributos_TipoDados3)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV56Atributos_TipoDados3;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV25DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ATRIBUTOS_ATIVO") == 0 ) && ! (false==AV57Atributos_Ativo3) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = StringUtil.BoolToStr( AV57Atributos_Ativo3);
            }
            if ( AV27DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV87Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "Atributos";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "Atributos_TabelaCod";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV58Atributos_TabelaCod), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV31Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_4A2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='TableSearchCell'>") ;
            wb_table2_8_4A2( true) ;
         }
         else
         {
            wb_table2_8_4A2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_4A2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_86_4A2( true) ;
         }
         else
         {
            wb_table3_86_4A2( false) ;
         }
         return  ;
      }

      protected void wb_table3_86_4A2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_4A2e( true) ;
         }
         else
         {
            wb_table1_2_4A2e( false) ;
         }
      }

      protected void wb_table3_86_4A2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"89\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAtributos_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtAtributos_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAtributos_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbAtributos_TipoDados_Titleformat == 0 )
               {
                  context.SendWebValue( cmbAtributos_TipoDados.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbAtributos_TipoDados.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAtributos_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtAtributos_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAtributos_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkAtributos_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( chkAtributos_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkAtributos_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAtributos_Detalhes_Titleformat == 0 )
               {
                  context.SendWebValue( edtAtributos_Detalhes_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAtributos_Detalhes_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkAtributos_PK_Titleformat == 0 )
               {
                  context.SendWebValue( chkAtributos_PK.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkAtributos_PK.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkAtributos_FK_Titleformat == 0 )
               {
                  context.SendWebValue( chkAtributos_FK.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkAtributos_FK.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtTabela_SistemaCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtTabela_SistemaCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtTabela_SistemaCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAtributos_MelhoraCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtAtributos_MelhoraCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAtributos_MelhoraCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV30Update));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Delete));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A176Atributos_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A177Atributos_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAtributos_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAtributos_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtAtributos_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A178Atributos_TipoDados));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbAtributos_TipoDados.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbAtributos_TipoDados_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A179Atributos_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAtributos_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAtributos_Descricao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A180Atributos_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkAtributos_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkAtributos_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV81Display));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDisplay_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A390Atributos_Detalhes));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAtributos_Detalhes_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAtributos_Detalhes_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A400Atributos_PK));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkAtributos_PK.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkAtributos_PK_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A401Atributos_FK));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkAtributos_FK.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkAtributos_FK_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A190Tabela_SistemaCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtTabela_SistemaCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTabela_SistemaCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A747Atributos_MelhoraCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAtributos_MelhoraCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAtributos_MelhoraCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 89 )
         {
            wbEnd = 0;
            nRC_GXsfl_89 = (short)(nGXsfl_89_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_86_4A2e( true) ;
         }
         else
         {
            wb_table3_86_4A2e( false) ;
         }
      }

      protected void wb_table2_8_4A2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_11_4A2( true) ;
         }
         else
         {
            wb_table4_11_4A2( false) ;
         }
         return  ;
      }

      protected void wb_table4_11_4A2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_TabelaAtributosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'" + sPrefix + "',false,'" + sGXsfl_89_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_TabelaAtributosWC.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'" + sPrefix + "',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV15OrderedDsc), StringUtil.BoolToStr( AV15OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_TabelaAtributosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_21_4A2( true) ;
         }
         else
         {
            wb_table5_21_4A2( false) ;
         }
         return  ;
      }

      protected void wb_table5_21_4A2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_4A2e( true) ;
         }
         else
         {
            wb_table2_8_4A2e( false) ;
         }
      }

      protected void wb_table5_21_4A2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_TabelaAtributosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_26_4A2( true) ;
         }
         else
         {
            wb_table6_26_4A2( false) ;
         }
         return  ;
      }

      protected void wb_table6_26_4A2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_TabelaAtributosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_21_4A2e( true) ;
         }
         else
         {
            wb_table5_21_4A2e( false) ;
         }
      }

      protected void wb_table6_26_4A2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_TabelaAtributosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'" + sPrefix + "',false,'" + sGXsfl_89_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV16DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_TabelaAtributosWC.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_TabelaAtributosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_35_4A2( true) ;
         }
         else
         {
            wb_table7_35_4A2( false) ;
         }
         return  ;
      }

      protected void wb_table7_35_4A2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_TabelaAtributosWC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_TabelaAtributosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_TabelaAtributosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'" + sPrefix + "',false,'" + sGXsfl_89_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV20DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", "", true, "HLP_TabelaAtributosWC.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_TabelaAtributosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_54_4A2( true) ;
         }
         else
         {
            wb_table8_54_4A2( false) ;
         }
         return  ;
      }

      protected void wb_table8_54_4A2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_TabelaAtributosWC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_TabelaAtributosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_TabelaAtributosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'" + sPrefix + "',false,'" + sGXsfl_89_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV24DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,69);\"", "", true, "HLP_TabelaAtributosWC.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_TabelaAtributosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_73_4A2( true) ;
         }
         else
         {
            wb_table9_73_4A2( false) ;
         }
         return  ;
      }

      protected void wb_table9_73_4A2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_TabelaAtributosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_26_4A2e( true) ;
         }
         else
         {
            wb_table6_26_4A2e( false) ;
         }
      }

      protected void wb_table9_73_4A2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'" + sPrefix + "',false,'" + sGXsfl_89_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,76);\"", "", true, "HLP_TabelaAtributosWC.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'" + sPrefix + "',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAtributos_nome3_Internalname, StringUtil.RTrim( AV26Atributos_Nome3), StringUtil.RTrim( context.localUtil.Format( AV26Atributos_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,78);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAtributos_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavAtributos_nome3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_TabelaAtributosWC.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'" + sPrefix + "',false,'" + sGXsfl_89_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavAtributos_tipodados3, cmbavAtributos_tipodados3_Internalname, StringUtil.RTrim( AV56Atributos_TipoDados3), 1, cmbavAtributos_tipodados3_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", cmbavAtributos_tipodados3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,79);\"", "", true, "HLP_TabelaAtributosWC.htm");
            cmbavAtributos_tipodados3.CurrentValue = StringUtil.RTrim( AV56Atributos_TipoDados3);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavAtributos_tipodados3_Internalname, "Values", (String)(cmbavAtributos_tipodados3.ToJavascriptSource()));
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'" + sPrefix + "',false,'" + sGXsfl_89_idx + "',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavAtributos_ativo3_Internalname, StringUtil.BoolToStr( AV57Atributos_Ativo3), "", "", chkavAtributos_ativo3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(80, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,80);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_73_4A2e( true) ;
         }
         else
         {
            wb_table9_73_4A2e( false) ;
         }
      }

      protected void wb_table8_54_4A2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'" + sPrefix + "',false,'" + sGXsfl_89_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,57);\"", "", true, "HLP_TabelaAtributosWC.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'" + sPrefix + "',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAtributos_nome2_Internalname, StringUtil.RTrim( AV22Atributos_Nome2), StringUtil.RTrim( context.localUtil.Format( AV22Atributos_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,59);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAtributos_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavAtributos_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_TabelaAtributosWC.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'" + sPrefix + "',false,'" + sGXsfl_89_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavAtributos_tipodados2, cmbavAtributos_tipodados2_Internalname, StringUtil.RTrim( AV54Atributos_TipoDados2), 1, cmbavAtributos_tipodados2_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", cmbavAtributos_tipodados2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,60);\"", "", true, "HLP_TabelaAtributosWC.htm");
            cmbavAtributos_tipodados2.CurrentValue = StringUtil.RTrim( AV54Atributos_TipoDados2);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavAtributos_tipodados2_Internalname, "Values", (String)(cmbavAtributos_tipodados2.ToJavascriptSource()));
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'" + sPrefix + "',false,'" + sGXsfl_89_idx + "',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavAtributos_ativo2_Internalname, StringUtil.BoolToStr( AV55Atributos_Ativo2), "", "", chkavAtributos_ativo2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(61, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,61);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_54_4A2e( true) ;
         }
         else
         {
            wb_table8_54_4A2e( false) ;
         }
      }

      protected void wb_table7_35_4A2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'" + sPrefix + "',false,'" + sGXsfl_89_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"", "", true, "HLP_TabelaAtributosWC.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'" + sPrefix + "',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAtributos_nome1_Internalname, StringUtil.RTrim( AV18Atributos_Nome1), StringUtil.RTrim( context.localUtil.Format( AV18Atributos_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,40);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAtributos_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavAtributos_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_TabelaAtributosWC.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'" + sPrefix + "',false,'" + sGXsfl_89_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavAtributos_tipodados1, cmbavAtributos_tipodados1_Internalname, StringUtil.RTrim( AV52Atributos_TipoDados1), 1, cmbavAtributos_tipodados1_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", cmbavAtributos_tipodados1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,41);\"", "", true, "HLP_TabelaAtributosWC.htm");
            cmbavAtributos_tipodados1.CurrentValue = StringUtil.RTrim( AV52Atributos_TipoDados1);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavAtributos_tipodados1_Internalname, "Values", (String)(cmbavAtributos_tipodados1.ToJavascriptSource()));
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'" + sPrefix + "',false,'" + sGXsfl_89_idx + "',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavAtributos_ativo1_Internalname, StringUtil.BoolToStr( AV53Atributos_Ativo1), "", "", chkavAtributos_ativo1.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(42, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_35_4A2e( true) ;
         }
         else
         {
            wb_table7_35_4A2e( false) ;
         }
      }

      protected void wb_table4_11_4A2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_TabelaAtributosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_11_4A2e( true) ;
         }
         else
         {
            wb_table4_11_4A2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV58Atributos_TabelaCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV58Atributos_TabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58Atributos_TabelaCod), 6, 0)));
         AV59Sistema_Codigo = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV59Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59Sistema_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA4A2( ) ;
         WS4A2( ) ;
         WE4A2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV58Atributos_TabelaCod = (String)((String)getParm(obj,0));
         sCtrlAV59Sistema_Codigo = (String)((String)getParm(obj,1));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA4A2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "tabelaatributoswc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA4A2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV58Atributos_TabelaCod = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV58Atributos_TabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58Atributos_TabelaCod), 6, 0)));
            AV59Sistema_Codigo = Convert.ToInt32(getParm(obj,3));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV59Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59Sistema_Codigo), 6, 0)));
         }
         wcpOAV58Atributos_TabelaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV58Atributos_TabelaCod"), ",", "."));
         wcpOAV59Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV59Sistema_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV58Atributos_TabelaCod != wcpOAV58Atributos_TabelaCod ) || ( AV59Sistema_Codigo != wcpOAV59Sistema_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOAV58Atributos_TabelaCod = AV58Atributos_TabelaCod;
         wcpOAV59Sistema_Codigo = AV59Sistema_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV58Atributos_TabelaCod = cgiGet( sPrefix+"AV58Atributos_TabelaCod_CTRL");
         if ( StringUtil.Len( sCtrlAV58Atributos_TabelaCod) > 0 )
         {
            AV58Atributos_TabelaCod = (int)(context.localUtil.CToN( cgiGet( sCtrlAV58Atributos_TabelaCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV58Atributos_TabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58Atributos_TabelaCod), 6, 0)));
         }
         else
         {
            AV58Atributos_TabelaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV58Atributos_TabelaCod_PARM"), ",", "."));
         }
         sCtrlAV59Sistema_Codigo = cgiGet( sPrefix+"AV59Sistema_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV59Sistema_Codigo) > 0 )
         {
            AV59Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV59Sistema_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV59Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59Sistema_Codigo), 6, 0)));
         }
         else
         {
            AV59Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV59Sistema_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA4A2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS4A2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS4A2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV58Atributos_TabelaCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV58Atributos_TabelaCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV58Atributos_TabelaCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV58Atributos_TabelaCod_CTRL", StringUtil.RTrim( sCtrlAV58Atributos_TabelaCod));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"AV59Sistema_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV59Sistema_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV59Sistema_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV59Sistema_Codigo_CTRL", StringUtil.RTrim( sCtrlAV59Sistema_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE4A2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202042822595419");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("tabelaatributoswc.js", "?202042822595420");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_892( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_89_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_89_idx;
         edtAtributos_Codigo_Internalname = sPrefix+"ATRIBUTOS_CODIGO_"+sGXsfl_89_idx;
         edtAtributos_Nome_Internalname = sPrefix+"ATRIBUTOS_NOME_"+sGXsfl_89_idx;
         cmbAtributos_TipoDados_Internalname = sPrefix+"ATRIBUTOS_TIPODADOS_"+sGXsfl_89_idx;
         edtAtributos_Descricao_Internalname = sPrefix+"ATRIBUTOS_DESCRICAO_"+sGXsfl_89_idx;
         chkAtributos_Ativo_Internalname = sPrefix+"ATRIBUTOS_ATIVO_"+sGXsfl_89_idx;
         edtavDisplay_Internalname = sPrefix+"vDISPLAY_"+sGXsfl_89_idx;
         edtAtributos_Detalhes_Internalname = sPrefix+"ATRIBUTOS_DETALHES_"+sGXsfl_89_idx;
         chkAtributos_PK_Internalname = sPrefix+"ATRIBUTOS_PK_"+sGXsfl_89_idx;
         chkAtributos_FK_Internalname = sPrefix+"ATRIBUTOS_FK_"+sGXsfl_89_idx;
         edtTabela_SistemaCod_Internalname = sPrefix+"TABELA_SISTEMACOD_"+sGXsfl_89_idx;
         edtAtributos_MelhoraCod_Internalname = sPrefix+"ATRIBUTOS_MELHORACOD_"+sGXsfl_89_idx;
      }

      protected void SubsflControlProps_fel_892( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_89_fel_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_89_fel_idx;
         edtAtributos_Codigo_Internalname = sPrefix+"ATRIBUTOS_CODIGO_"+sGXsfl_89_fel_idx;
         edtAtributos_Nome_Internalname = sPrefix+"ATRIBUTOS_NOME_"+sGXsfl_89_fel_idx;
         cmbAtributos_TipoDados_Internalname = sPrefix+"ATRIBUTOS_TIPODADOS_"+sGXsfl_89_fel_idx;
         edtAtributos_Descricao_Internalname = sPrefix+"ATRIBUTOS_DESCRICAO_"+sGXsfl_89_fel_idx;
         chkAtributos_Ativo_Internalname = sPrefix+"ATRIBUTOS_ATIVO_"+sGXsfl_89_fel_idx;
         edtavDisplay_Internalname = sPrefix+"vDISPLAY_"+sGXsfl_89_fel_idx;
         edtAtributos_Detalhes_Internalname = sPrefix+"ATRIBUTOS_DETALHES_"+sGXsfl_89_fel_idx;
         chkAtributos_PK_Internalname = sPrefix+"ATRIBUTOS_PK_"+sGXsfl_89_fel_idx;
         chkAtributos_FK_Internalname = sPrefix+"ATRIBUTOS_FK_"+sGXsfl_89_fel_idx;
         edtTabela_SistemaCod_Internalname = sPrefix+"TABELA_SISTEMACOD_"+sGXsfl_89_fel_idx;
         edtAtributos_MelhoraCod_Internalname = sPrefix+"ATRIBUTOS_MELHORACOD_"+sGXsfl_89_fel_idx;
      }

      protected void sendrow_892( )
      {
         SubsflControlProps_892( ) ;
         WB4A0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_89_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_89_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_89_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV30Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV30Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV84Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV30Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV30Update)) ? AV84Update_GXI : context.PathToRelativeUrl( AV30Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavUpdate_Enabled,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV30Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV29Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV85Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV85Delete_GXI : context.PathToRelativeUrl( AV29Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDelete_Enabled,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV29Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAtributos_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A176Atributos_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A176Atributos_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAtributos_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)89,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAtributos_Nome_Internalname,StringUtil.RTrim( A177Atributos_Nome),StringUtil.RTrim( context.localUtil.Format( A177Atributos_Nome, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtAtributos_Nome_Link,(String)"",(String)"",(String)"",(String)edtAtributos_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)89,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_89_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "ATRIBUTOS_TIPODADOS_" + sGXsfl_89_idx;
               cmbAtributos_TipoDados.Name = GXCCtl;
               cmbAtributos_TipoDados.WebTags = "";
               cmbAtributos_TipoDados.addItem("", "Desconhecido", 0);
               cmbAtributos_TipoDados.addItem("N", "Numeric", 0);
               cmbAtributos_TipoDados.addItem("C", "Character", 0);
               cmbAtributos_TipoDados.addItem("VC", "Varchar", 0);
               cmbAtributos_TipoDados.addItem("D", "Date", 0);
               cmbAtributos_TipoDados.addItem("DT", "Date Time", 0);
               cmbAtributos_TipoDados.addItem("Bool", "Boolean", 0);
               cmbAtributos_TipoDados.addItem("Blob", "Blob", 0);
               cmbAtributos_TipoDados.addItem("Outr", "Outros", 0);
               if ( cmbAtributos_TipoDados.ItemCount > 0 )
               {
                  A178Atributos_TipoDados = cmbAtributos_TipoDados.getValidValue(A178Atributos_TipoDados);
                  n178Atributos_TipoDados = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbAtributos_TipoDados,(String)cmbAtributos_TipoDados_Internalname,StringUtil.RTrim( A178Atributos_TipoDados),(short)1,(String)cmbAtributos_TipoDados_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbAtributos_TipoDados.CurrentValue = StringUtil.RTrim( A178Atributos_TipoDados);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbAtributos_TipoDados_Internalname, "Values", (String)(cmbAtributos_TipoDados.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAtributos_Descricao_Internalname,(String)A179Atributos_Descricao,(String)A179Atributos_Descricao,(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAtributos_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)0,(short)89,(short)1,(short)0,(short)-1,(bool)true,(String)"DescricaoLonga",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkAtributos_Ativo_Internalname,StringUtil.BoolToStr( A180Atributos_Ativo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV81Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV81Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV86Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV81Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV81Display)) ? AV86Display_GXI : context.PathToRelativeUrl( AV81Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDisplay_Enabled,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV81Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAtributos_Detalhes_Internalname,StringUtil.RTrim( A390Atributos_Detalhes),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAtributos_Detalhes_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)10,(short)0,(short)0,(short)89,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkAtributos_PK_Internalname,StringUtil.BoolToStr( A400Atributos_PK),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkAtributos_FK_Internalname,StringUtil.BoolToStr( A401Atributos_FK),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTabela_SistemaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A190Tabela_SistemaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A190Tabela_SistemaCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTabela_SistemaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)89,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAtributos_MelhoraCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A747Atributos_MelhoraCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A747Atributos_MelhoraCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAtributos_MelhoraCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)89,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ATRIBUTOS_CODIGO"+"_"+sGXsfl_89_idx, GetSecureSignedToken( sPrefix+sGXsfl_89_idx, context.localUtil.Format( (decimal)(A176Atributos_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ATRIBUTOS_NOME"+"_"+sGXsfl_89_idx, GetSecureSignedToken( sPrefix+sGXsfl_89_idx, StringUtil.RTrim( context.localUtil.Format( A177Atributos_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ATRIBUTOS_TIPODADOS"+"_"+sGXsfl_89_idx, GetSecureSignedToken( sPrefix+sGXsfl_89_idx, StringUtil.RTrim( context.localUtil.Format( A178Atributos_TipoDados, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ATRIBUTOS_DESCRICAO"+"_"+sGXsfl_89_idx, GetSecureSignedToken( sPrefix+sGXsfl_89_idx, A179Atributos_Descricao));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ATRIBUTOS_ATIVO"+"_"+sGXsfl_89_idx, GetSecureSignedToken( sPrefix+sGXsfl_89_idx, A180Atributos_Ativo));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ATRIBUTOS_DETALHES"+"_"+sGXsfl_89_idx, GetSecureSignedToken( sPrefix+sGXsfl_89_idx, StringUtil.RTrim( context.localUtil.Format( A390Atributos_Detalhes, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ATRIBUTOS_PK"+"_"+sGXsfl_89_idx, GetSecureSignedToken( sPrefix+sGXsfl_89_idx, A400Atributos_PK));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ATRIBUTOS_FK"+"_"+sGXsfl_89_idx, GetSecureSignedToken( sPrefix+sGXsfl_89_idx, A401Atributos_FK));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ATRIBUTOS_MELHORACOD"+"_"+sGXsfl_89_idx, GetSecureSignedToken( sPrefix+sGXsfl_89_idx, context.localUtil.Format( (decimal)(A747Atributos_MelhoraCod), "ZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_89_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_89_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_89_idx+1));
            sGXsfl_89_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_89_idx), 4, 0)), 4, "0");
            SubsflControlProps_892( ) ;
         }
         /* End function sendrow_892 */
      }

      protected void init_default_properties( )
      {
         imgInsert_Internalname = sPrefix+"INSERT";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         lblOrderedtext_Internalname = sPrefix+"ORDEREDTEXT";
         cmbavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         imgCleanfilters_Internalname = sPrefix+"CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = sPrefix+"DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = sPrefix+"vDYNAMICFILTERSOPERATOR1";
         edtavAtributos_nome1_Internalname = sPrefix+"vATRIBUTOS_NOME1";
         cmbavAtributos_tipodados1_Internalname = sPrefix+"vATRIBUTOS_TIPODADOS1";
         chkavAtributos_ativo1_Internalname = sPrefix+"vATRIBUTOS_ATIVO1";
         tblTablemergeddynamicfilters1_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = sPrefix+"ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = sPrefix+"REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = sPrefix+"DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = sPrefix+"vDYNAMICFILTERSOPERATOR2";
         edtavAtributos_nome2_Internalname = sPrefix+"vATRIBUTOS_NOME2";
         cmbavAtributos_tipodados2_Internalname = sPrefix+"vATRIBUTOS_TIPODADOS2";
         chkavAtributos_ativo2_Internalname = sPrefix+"vATRIBUTOS_ATIVO2";
         tblTablemergeddynamicfilters2_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = sPrefix+"ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = sPrefix+"REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = sPrefix+"DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = sPrefix+"vDYNAMICFILTERSOPERATOR3";
         edtavAtributos_nome3_Internalname = sPrefix+"vATRIBUTOS_NOME3";
         cmbavAtributos_tipodados3_Internalname = sPrefix+"vATRIBUTOS_TIPODADOS3";
         chkavAtributos_ativo3_Internalname = sPrefix+"vATRIBUTOS_ATIVO3";
         tblTablemergeddynamicfilters3_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = sPrefix+"REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = sPrefix+"TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = sPrefix+"JSDYNAMICFILTERS";
         tblTablefilters_Internalname = sPrefix+"TABLEFILTERS";
         tblTablesearch_Internalname = sPrefix+"TABLESEARCH";
         edtavUpdate_Internalname = sPrefix+"vUPDATE";
         edtavDelete_Internalname = sPrefix+"vDELETE";
         edtAtributos_Codigo_Internalname = sPrefix+"ATRIBUTOS_CODIGO";
         edtAtributos_Nome_Internalname = sPrefix+"ATRIBUTOS_NOME";
         cmbAtributos_TipoDados_Internalname = sPrefix+"ATRIBUTOS_TIPODADOS";
         edtAtributos_Descricao_Internalname = sPrefix+"ATRIBUTOS_DESCRICAO";
         chkAtributos_Ativo_Internalname = sPrefix+"ATRIBUTOS_ATIVO";
         edtavDisplay_Internalname = sPrefix+"vDISPLAY";
         edtAtributos_Detalhes_Internalname = sPrefix+"ATRIBUTOS_DETALHES";
         chkAtributos_PK_Internalname = sPrefix+"ATRIBUTOS_PK";
         chkAtributos_FK_Internalname = sPrefix+"ATRIBUTOS_FK";
         edtTabela_SistemaCod_Internalname = sPrefix+"TABELA_SISTEMACOD";
         edtAtributos_MelhoraCod_Internalname = sPrefix+"ATRIBUTOS_MELHORACOD";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         edtAtributos_TabelaCod_Internalname = sPrefix+"ATRIBUTOS_TABELACOD";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = sPrefix+"vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = sPrefix+"vDYNAMICFILTERSENABLED3";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtAtributos_MelhoraCod_Jsonclick = "";
         edtTabela_SistemaCod_Jsonclick = "";
         edtAtributos_Detalhes_Jsonclick = "";
         edtAtributos_Descricao_Jsonclick = "";
         cmbAtributos_TipoDados_Jsonclick = "";
         edtAtributos_Nome_Jsonclick = "";
         edtAtributos_Codigo_Jsonclick = "";
         cmbavAtributos_tipodados1_Jsonclick = "";
         edtavAtributos_nome1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         cmbavAtributos_tipodados2_Jsonclick = "";
         edtavAtributos_nome2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         cmbavAtributos_tipodados3_Jsonclick = "";
         edtavAtributos_nome3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavDisplay_Enabled = 1;
         edtAtributos_Nome_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavDelete_Enabled = 1;
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtavUpdate_Enabled = 1;
         edtAtributos_MelhoraCod_Titleformat = 0;
         edtTabela_SistemaCod_Titleformat = 0;
         chkAtributos_FK_Titleformat = 0;
         chkAtributos_PK_Titleformat = 0;
         edtAtributos_Detalhes_Titleformat = 0;
         chkAtributos_Ativo_Titleformat = 0;
         edtAtributos_Descricao_Titleformat = 0;
         cmbAtributos_TipoDados_Titleformat = 0;
         edtAtributos_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         chkavAtributos_ativo3.Visible = 1;
         cmbavAtributos_tipodados3.Visible = 1;
         edtavAtributos_nome3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         chkavAtributos_ativo2.Visible = 1;
         cmbavAtributos_tipodados2.Visible = 1;
         edtavAtributos_nome2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         chkavAtributos_ativo1.Visible = 1;
         cmbavAtributos_tipodados1.Visible = 1;
         edtavAtributos_nome1_Visible = 1;
         edtAtributos_MelhoraCod_Title = "a melhorar";
         edtTabela_SistemaCod_Title = "Sistema";
         chkAtributos_FK.Title.Text = "FK";
         chkAtributos_PK.Title.Text = "PK";
         edtAtributos_Detalhes_Title = "Detalhes";
         chkAtributos_Ativo.Title.Text = "Ativo";
         edtAtributos_Descricao_Title = "Descri��o";
         cmbAtributos_TipoDados.Title.Text = "Tipo de Dados";
         edtAtributos_Nome_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkAtributos_FK.Caption = "";
         chkAtributos_PK.Caption = "";
         chkAtributos_Ativo.Caption = "";
         chkavAtributos_ativo3.Caption = "";
         chkavAtributos_ativo2.Caption = "";
         chkavAtributos_ativo1.Caption = "";
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         edtAtributos_TabelaCod_Jsonclick = "";
         edtAtributos_TabelaCod_Visible = 1;
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV58Atributos_TabelaCod',fld:'vATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV59Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV87Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV18Atributos_Nome1',fld:'vATRIBUTOS_NOME1',pic:'@!',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV52Atributos_TipoDados1',fld:'vATRIBUTOS_TIPODADOS1',pic:'',nv:''},{av:'AV53Atributos_Ativo1',fld:'vATRIBUTOS_ATIVO1',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22Atributos_Nome2',fld:'vATRIBUTOS_NOME2',pic:'@!',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV54Atributos_TipoDados2',fld:'vATRIBUTOS_TIPODADOS2',pic:'',nv:''},{av:'AV55Atributos_Ativo2',fld:'vATRIBUTOS_ATIVO2',pic:'',nv:false},{av:'AV26Atributos_Nome3',fld:'vATRIBUTOS_NOME3',pic:'@!',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV56Atributos_TipoDados3',fld:'vATRIBUTOS_TIPODADOS3',pic:'',nv:''},{av:'AV57Atributos_Ativo3',fld:'vATRIBUTOS_ATIVO3',pic:'',nv:false}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtAtributos_Nome_Titleformat',ctrl:'ATRIBUTOS_NOME',prop:'Titleformat'},{av:'edtAtributos_Nome_Title',ctrl:'ATRIBUTOS_NOME',prop:'Title'},{av:'cmbAtributos_TipoDados'},{av:'edtAtributos_Descricao_Titleformat',ctrl:'ATRIBUTOS_DESCRICAO',prop:'Titleformat'},{av:'edtAtributos_Descricao_Title',ctrl:'ATRIBUTOS_DESCRICAO',prop:'Title'},{av:'chkAtributos_Ativo_Titleformat',ctrl:'ATRIBUTOS_ATIVO',prop:'Titleformat'},{av:'chkAtributos_Ativo.Title.Text',ctrl:'ATRIBUTOS_ATIVO',prop:'Title'},{av:'edtAtributos_Detalhes_Titleformat',ctrl:'ATRIBUTOS_DETALHES',prop:'Titleformat'},{av:'edtAtributos_Detalhes_Title',ctrl:'ATRIBUTOS_DETALHES',prop:'Title'},{av:'chkAtributos_PK_Titleformat',ctrl:'ATRIBUTOS_PK',prop:'Titleformat'},{av:'chkAtributos_PK.Title.Text',ctrl:'ATRIBUTOS_PK',prop:'Title'},{av:'chkAtributos_FK_Titleformat',ctrl:'ATRIBUTOS_FK',prop:'Titleformat'},{av:'chkAtributos_FK.Title.Text',ctrl:'ATRIBUTOS_FK',prop:'Title'},{av:'edtTabela_SistemaCod_Titleformat',ctrl:'TABELA_SISTEMACOD',prop:'Titleformat'},{av:'edtTabela_SistemaCod_Title',ctrl:'TABELA_SISTEMACOD',prop:'Title'},{av:'edtAtributos_MelhoraCod_Titleformat',ctrl:'ATRIBUTOS_MELHORACOD',prop:'Titleformat'},{av:'edtAtributos_MelhoraCod_Title',ctrl:'ATRIBUTOS_MELHORACOD',prop:'Title'},{av:'AV79GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV80GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E114A2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Atributos_Nome1',fld:'vATRIBUTOS_NOME1',pic:'@!',nv:''},{av:'AV52Atributos_TipoDados1',fld:'vATRIBUTOS_TIPODADOS1',pic:'',nv:''},{av:'AV53Atributos_Ativo1',fld:'vATRIBUTOS_ATIVO1',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Atributos_Nome2',fld:'vATRIBUTOS_NOME2',pic:'@!',nv:''},{av:'AV54Atributos_TipoDados2',fld:'vATRIBUTOS_TIPODADOS2',pic:'',nv:''},{av:'AV55Atributos_Ativo2',fld:'vATRIBUTOS_ATIVO2',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26Atributos_Nome3',fld:'vATRIBUTOS_NOME3',pic:'@!',nv:''},{av:'AV56Atributos_TipoDados3',fld:'vATRIBUTOS_TIPODADOS3',pic:'',nv:''},{av:'AV57Atributos_Ativo3',fld:'vATRIBUTOS_ATIVO3',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV58Atributos_TabelaCod',fld:'vATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV87Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV59Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("GRID.LOAD","{handler:'E254A2',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV59Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV30Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Enabled',ctrl:'vUPDATE',prop:'Enabled'},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV29Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Enabled',ctrl:'vDELETE',prop:'Enabled'},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'AV81Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Enabled',ctrl:'vDISPLAY',prop:'Enabled'},{av:'edtAtributos_Nome_Link',ctrl:'ATRIBUTOS_NOME',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E124A2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Atributos_Nome1',fld:'vATRIBUTOS_NOME1',pic:'@!',nv:''},{av:'AV52Atributos_TipoDados1',fld:'vATRIBUTOS_TIPODADOS1',pic:'',nv:''},{av:'AV53Atributos_Ativo1',fld:'vATRIBUTOS_ATIVO1',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Atributos_Nome2',fld:'vATRIBUTOS_NOME2',pic:'@!',nv:''},{av:'AV54Atributos_TipoDados2',fld:'vATRIBUTOS_TIPODADOS2',pic:'',nv:''},{av:'AV55Atributos_Ativo2',fld:'vATRIBUTOS_ATIVO2',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26Atributos_Nome3',fld:'vATRIBUTOS_NOME3',pic:'@!',nv:''},{av:'AV56Atributos_TipoDados3',fld:'vATRIBUTOS_TIPODADOS3',pic:'',nv:''},{av:'AV57Atributos_Ativo3',fld:'vATRIBUTOS_ATIVO3',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV58Atributos_TabelaCod',fld:'vATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV87Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV59Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E184A2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Atributos_Nome1',fld:'vATRIBUTOS_NOME1',pic:'@!',nv:''},{av:'AV52Atributos_TipoDados1',fld:'vATRIBUTOS_TIPODADOS1',pic:'',nv:''},{av:'AV53Atributos_Ativo1',fld:'vATRIBUTOS_ATIVO1',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Atributos_Nome2',fld:'vATRIBUTOS_NOME2',pic:'@!',nv:''},{av:'AV54Atributos_TipoDados2',fld:'vATRIBUTOS_TIPODADOS2',pic:'',nv:''},{av:'AV55Atributos_Ativo2',fld:'vATRIBUTOS_ATIVO2',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26Atributos_Nome3',fld:'vATRIBUTOS_NOME3',pic:'@!',nv:''},{av:'AV56Atributos_TipoDados3',fld:'vATRIBUTOS_TIPODADOS3',pic:'',nv:''},{av:'AV57Atributos_Ativo3',fld:'vATRIBUTOS_ATIVO3',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV58Atributos_TabelaCod',fld:'vATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV87Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV59Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E134A2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Atributos_Nome1',fld:'vATRIBUTOS_NOME1',pic:'@!',nv:''},{av:'AV52Atributos_TipoDados1',fld:'vATRIBUTOS_TIPODADOS1',pic:'',nv:''},{av:'AV53Atributos_Ativo1',fld:'vATRIBUTOS_ATIVO1',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Atributos_Nome2',fld:'vATRIBUTOS_NOME2',pic:'@!',nv:''},{av:'AV54Atributos_TipoDados2',fld:'vATRIBUTOS_TIPODADOS2',pic:'',nv:''},{av:'AV55Atributos_Ativo2',fld:'vATRIBUTOS_ATIVO2',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26Atributos_Nome3',fld:'vATRIBUTOS_NOME3',pic:'@!',nv:''},{av:'AV56Atributos_TipoDados3',fld:'vATRIBUTOS_TIPODADOS3',pic:'',nv:''},{av:'AV57Atributos_Ativo3',fld:'vATRIBUTOS_ATIVO3',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV58Atributos_TabelaCod',fld:'vATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV87Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV59Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Atributos_Nome2',fld:'vATRIBUTOS_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26Atributos_Nome3',fld:'vATRIBUTOS_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Atributos_Nome1',fld:'vATRIBUTOS_NOME1',pic:'@!',nv:''},{av:'AV52Atributos_TipoDados1',fld:'vATRIBUTOS_TIPODADOS1',pic:'',nv:''},{av:'AV53Atributos_Ativo1',fld:'vATRIBUTOS_ATIVO1',pic:'',nv:false},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV54Atributos_TipoDados2',fld:'vATRIBUTOS_TIPODADOS2',pic:'',nv:''},{av:'AV55Atributos_Ativo2',fld:'vATRIBUTOS_ATIVO2',pic:'',nv:false},{av:'AV56Atributos_TipoDados3',fld:'vATRIBUTOS_TIPODADOS3',pic:'',nv:''},{av:'AV57Atributos_Ativo3',fld:'vATRIBUTOS_ATIVO3',pic:'',nv:false},{av:'edtavAtributos_nome2_Visible',ctrl:'vATRIBUTOS_NOME2',prop:'Visible'},{av:'cmbavAtributos_tipodados2'},{av:'chkavAtributos_ativo2.Visible',ctrl:'vATRIBUTOS_ATIVO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavAtributos_nome3_Visible',ctrl:'vATRIBUTOS_NOME3',prop:'Visible'},{av:'cmbavAtributos_tipodados3'},{av:'chkavAtributos_ativo3.Visible',ctrl:'vATRIBUTOS_ATIVO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavAtributos_nome1_Visible',ctrl:'vATRIBUTOS_NOME1',prop:'Visible'},{av:'cmbavAtributos_tipodados1'},{av:'chkavAtributos_ativo1.Visible',ctrl:'vATRIBUTOS_ATIVO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E194A2',iparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'edtavAtributos_nome1_Visible',ctrl:'vATRIBUTOS_NOME1',prop:'Visible'},{av:'cmbavAtributos_tipodados1'},{av:'chkavAtributos_ativo1.Visible',ctrl:'vATRIBUTOS_ATIVO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E204A2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Atributos_Nome1',fld:'vATRIBUTOS_NOME1',pic:'@!',nv:''},{av:'AV52Atributos_TipoDados1',fld:'vATRIBUTOS_TIPODADOS1',pic:'',nv:''},{av:'AV53Atributos_Ativo1',fld:'vATRIBUTOS_ATIVO1',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Atributos_Nome2',fld:'vATRIBUTOS_NOME2',pic:'@!',nv:''},{av:'AV54Atributos_TipoDados2',fld:'vATRIBUTOS_TIPODADOS2',pic:'',nv:''},{av:'AV55Atributos_Ativo2',fld:'vATRIBUTOS_ATIVO2',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26Atributos_Nome3',fld:'vATRIBUTOS_NOME3',pic:'@!',nv:''},{av:'AV56Atributos_TipoDados3',fld:'vATRIBUTOS_TIPODADOS3',pic:'',nv:''},{av:'AV57Atributos_Ativo3',fld:'vATRIBUTOS_ATIVO3',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV58Atributos_TabelaCod',fld:'vATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV87Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV59Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E144A2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Atributos_Nome1',fld:'vATRIBUTOS_NOME1',pic:'@!',nv:''},{av:'AV52Atributos_TipoDados1',fld:'vATRIBUTOS_TIPODADOS1',pic:'',nv:''},{av:'AV53Atributos_Ativo1',fld:'vATRIBUTOS_ATIVO1',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Atributos_Nome2',fld:'vATRIBUTOS_NOME2',pic:'@!',nv:''},{av:'AV54Atributos_TipoDados2',fld:'vATRIBUTOS_TIPODADOS2',pic:'',nv:''},{av:'AV55Atributos_Ativo2',fld:'vATRIBUTOS_ATIVO2',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26Atributos_Nome3',fld:'vATRIBUTOS_NOME3',pic:'@!',nv:''},{av:'AV56Atributos_TipoDados3',fld:'vATRIBUTOS_TIPODADOS3',pic:'',nv:''},{av:'AV57Atributos_Ativo3',fld:'vATRIBUTOS_ATIVO3',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV58Atributos_TabelaCod',fld:'vATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV87Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV59Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Atributos_Nome2',fld:'vATRIBUTOS_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26Atributos_Nome3',fld:'vATRIBUTOS_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Atributos_Nome1',fld:'vATRIBUTOS_NOME1',pic:'@!',nv:''},{av:'AV52Atributos_TipoDados1',fld:'vATRIBUTOS_TIPODADOS1',pic:'',nv:''},{av:'AV53Atributos_Ativo1',fld:'vATRIBUTOS_ATIVO1',pic:'',nv:false},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV54Atributos_TipoDados2',fld:'vATRIBUTOS_TIPODADOS2',pic:'',nv:''},{av:'AV55Atributos_Ativo2',fld:'vATRIBUTOS_ATIVO2',pic:'',nv:false},{av:'AV56Atributos_TipoDados3',fld:'vATRIBUTOS_TIPODADOS3',pic:'',nv:''},{av:'AV57Atributos_Ativo3',fld:'vATRIBUTOS_ATIVO3',pic:'',nv:false},{av:'edtavAtributos_nome2_Visible',ctrl:'vATRIBUTOS_NOME2',prop:'Visible'},{av:'cmbavAtributos_tipodados2'},{av:'chkavAtributos_ativo2.Visible',ctrl:'vATRIBUTOS_ATIVO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavAtributos_nome3_Visible',ctrl:'vATRIBUTOS_NOME3',prop:'Visible'},{av:'cmbavAtributos_tipodados3'},{av:'chkavAtributos_ativo3.Visible',ctrl:'vATRIBUTOS_ATIVO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavAtributos_nome1_Visible',ctrl:'vATRIBUTOS_NOME1',prop:'Visible'},{av:'cmbavAtributos_tipodados1'},{av:'chkavAtributos_ativo1.Visible',ctrl:'vATRIBUTOS_ATIVO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E214A2',iparms:[{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'edtavAtributos_nome2_Visible',ctrl:'vATRIBUTOS_NOME2',prop:'Visible'},{av:'cmbavAtributos_tipodados2'},{av:'chkavAtributos_ativo2.Visible',ctrl:'vATRIBUTOS_ATIVO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E154A2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Atributos_Nome1',fld:'vATRIBUTOS_NOME1',pic:'@!',nv:''},{av:'AV52Atributos_TipoDados1',fld:'vATRIBUTOS_TIPODADOS1',pic:'',nv:''},{av:'AV53Atributos_Ativo1',fld:'vATRIBUTOS_ATIVO1',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Atributos_Nome2',fld:'vATRIBUTOS_NOME2',pic:'@!',nv:''},{av:'AV54Atributos_TipoDados2',fld:'vATRIBUTOS_TIPODADOS2',pic:'',nv:''},{av:'AV55Atributos_Ativo2',fld:'vATRIBUTOS_ATIVO2',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26Atributos_Nome3',fld:'vATRIBUTOS_NOME3',pic:'@!',nv:''},{av:'AV56Atributos_TipoDados3',fld:'vATRIBUTOS_TIPODADOS3',pic:'',nv:''},{av:'AV57Atributos_Ativo3',fld:'vATRIBUTOS_ATIVO3',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV58Atributos_TabelaCod',fld:'vATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV87Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV59Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Atributos_Nome2',fld:'vATRIBUTOS_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26Atributos_Nome3',fld:'vATRIBUTOS_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Atributos_Nome1',fld:'vATRIBUTOS_NOME1',pic:'@!',nv:''},{av:'AV52Atributos_TipoDados1',fld:'vATRIBUTOS_TIPODADOS1',pic:'',nv:''},{av:'AV53Atributos_Ativo1',fld:'vATRIBUTOS_ATIVO1',pic:'',nv:false},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV54Atributos_TipoDados2',fld:'vATRIBUTOS_TIPODADOS2',pic:'',nv:''},{av:'AV55Atributos_Ativo2',fld:'vATRIBUTOS_ATIVO2',pic:'',nv:false},{av:'AV56Atributos_TipoDados3',fld:'vATRIBUTOS_TIPODADOS3',pic:'',nv:''},{av:'AV57Atributos_Ativo3',fld:'vATRIBUTOS_ATIVO3',pic:'',nv:false},{av:'edtavAtributos_nome2_Visible',ctrl:'vATRIBUTOS_NOME2',prop:'Visible'},{av:'cmbavAtributos_tipodados2'},{av:'chkavAtributos_ativo2.Visible',ctrl:'vATRIBUTOS_ATIVO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavAtributos_nome3_Visible',ctrl:'vATRIBUTOS_NOME3',prop:'Visible'},{av:'cmbavAtributos_tipodados3'},{av:'chkavAtributos_ativo3.Visible',ctrl:'vATRIBUTOS_ATIVO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavAtributos_nome1_Visible',ctrl:'vATRIBUTOS_NOME1',prop:'Visible'},{av:'cmbavAtributos_tipodados1'},{av:'chkavAtributos_ativo1.Visible',ctrl:'vATRIBUTOS_ATIVO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E224A2',iparms:[{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtavAtributos_nome3_Visible',ctrl:'vATRIBUTOS_NOME3',prop:'Visible'},{av:'cmbavAtributos_tipodados3'},{av:'chkavAtributos_ativo3.Visible',ctrl:'vATRIBUTOS_ATIVO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E164A2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Atributos_Nome1',fld:'vATRIBUTOS_NOME1',pic:'@!',nv:''},{av:'AV52Atributos_TipoDados1',fld:'vATRIBUTOS_TIPODADOS1',pic:'',nv:''},{av:'AV53Atributos_Ativo1',fld:'vATRIBUTOS_ATIVO1',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Atributos_Nome2',fld:'vATRIBUTOS_NOME2',pic:'@!',nv:''},{av:'AV54Atributos_TipoDados2',fld:'vATRIBUTOS_TIPODADOS2',pic:'',nv:''},{av:'AV55Atributos_Ativo2',fld:'vATRIBUTOS_ATIVO2',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26Atributos_Nome3',fld:'vATRIBUTOS_NOME3',pic:'@!',nv:''},{av:'AV56Atributos_TipoDados3',fld:'vATRIBUTOS_TIPODADOS3',pic:'',nv:''},{av:'AV57Atributos_Ativo3',fld:'vATRIBUTOS_ATIVO3',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV58Atributos_TabelaCod',fld:'vATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV87Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV59Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Atributos_Nome1',fld:'vATRIBUTOS_NOME1',pic:'@!',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavAtributos_nome1_Visible',ctrl:'vATRIBUTOS_NOME1',prop:'Visible'},{av:'cmbavAtributos_tipodados1'},{av:'chkavAtributos_ativo1.Visible',ctrl:'vATRIBUTOS_ATIVO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Atributos_Nome2',fld:'vATRIBUTOS_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26Atributos_Nome3',fld:'vATRIBUTOS_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV52Atributos_TipoDados1',fld:'vATRIBUTOS_TIPODADOS1',pic:'',nv:''},{av:'AV53Atributos_Ativo1',fld:'vATRIBUTOS_ATIVO1',pic:'',nv:false},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV54Atributos_TipoDados2',fld:'vATRIBUTOS_TIPODADOS2',pic:'',nv:''},{av:'AV55Atributos_Ativo2',fld:'vATRIBUTOS_ATIVO2',pic:'',nv:false},{av:'AV56Atributos_TipoDados3',fld:'vATRIBUTOS_TIPODADOS3',pic:'',nv:''},{av:'AV57Atributos_Ativo3',fld:'vATRIBUTOS_ATIVO3',pic:'',nv:false},{av:'edtavAtributos_nome2_Visible',ctrl:'vATRIBUTOS_NOME2',prop:'Visible'},{av:'cmbavAtributos_tipodados2'},{av:'chkavAtributos_ativo2.Visible',ctrl:'vATRIBUTOS_ATIVO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavAtributos_nome3_Visible',ctrl:'vATRIBUTOS_NOME3',prop:'Visible'},{av:'cmbavAtributos_tipodados3'},{av:'chkavAtributos_ativo3.Visible',ctrl:'vATRIBUTOS_ATIVO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E174A2',iparms:[{av:'AV58Atributos_TabelaCod',fld:'vATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV59Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV16DynamicFiltersSelector1 = "";
         AV18Atributos_Nome1 = "";
         AV52Atributos_TipoDados1 = "";
         AV53Atributos_Ativo1 = true;
         AV20DynamicFiltersSelector2 = "";
         AV22Atributos_Nome2 = "";
         AV54Atributos_TipoDados2 = "";
         AV55Atributos_Ativo2 = true;
         AV24DynamicFiltersSelector3 = "";
         AV26Atributos_Nome3 = "";
         AV56Atributos_TipoDados3 = "";
         AV57Atributos_Ativo3 = true;
         AV87Pgmname = "";
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV30Update = "";
         AV84Update_GXI = "";
         AV29Delete = "";
         AV85Delete_GXI = "";
         A177Atributos_Nome = "";
         A178Atributos_TipoDados = "";
         A179Atributos_Descricao = "";
         AV81Display = "";
         AV86Display_GXI = "";
         A390Atributos_Detalhes = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV18Atributos_Nome1 = "";
         lV52Atributos_TipoDados1 = "";
         lV22Atributos_Nome2 = "";
         lV54Atributos_TipoDados2 = "";
         lV26Atributos_Nome3 = "";
         lV56Atributos_TipoDados3 = "";
         H004A2_A356Atributos_TabelaCod = new int[1] ;
         H004A2_A747Atributos_MelhoraCod = new int[1] ;
         H004A2_n747Atributos_MelhoraCod = new bool[] {false} ;
         H004A2_A190Tabela_SistemaCod = new int[1] ;
         H004A2_n190Tabela_SistemaCod = new bool[] {false} ;
         H004A2_A401Atributos_FK = new bool[] {false} ;
         H004A2_n401Atributos_FK = new bool[] {false} ;
         H004A2_A400Atributos_PK = new bool[] {false} ;
         H004A2_n400Atributos_PK = new bool[] {false} ;
         H004A2_A390Atributos_Detalhes = new String[] {""} ;
         H004A2_n390Atributos_Detalhes = new bool[] {false} ;
         H004A2_A180Atributos_Ativo = new bool[] {false} ;
         H004A2_A179Atributos_Descricao = new String[] {""} ;
         H004A2_n179Atributos_Descricao = new bool[] {false} ;
         H004A2_A178Atributos_TipoDados = new String[] {""} ;
         H004A2_n178Atributos_TipoDados = new bool[] {false} ;
         H004A2_A177Atributos_Nome = new String[] {""} ;
         H004A2_A176Atributos_Codigo = new int[1] ;
         H004A3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GridRow = new GXWebRow();
         AV31Session = context.GetSession();
         AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV58Atributos_TabelaCod = "";
         sCtrlAV59Sistema_Codigo = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.tabelaatributoswc__default(),
            new Object[][] {
                new Object[] {
               H004A2_A356Atributos_TabelaCod, H004A2_A747Atributos_MelhoraCod, H004A2_n747Atributos_MelhoraCod, H004A2_A190Tabela_SistemaCod, H004A2_n190Tabela_SistemaCod, H004A2_A401Atributos_FK, H004A2_n401Atributos_FK, H004A2_A400Atributos_PK, H004A2_n400Atributos_PK, H004A2_A390Atributos_Detalhes,
               H004A2_n390Atributos_Detalhes, H004A2_A180Atributos_Ativo, H004A2_A179Atributos_Descricao, H004A2_n179Atributos_Descricao, H004A2_A178Atributos_TipoDados, H004A2_n178Atributos_TipoDados, H004A2_A177Atributos_Nome, H004A2_A176Atributos_Codigo
               }
               , new Object[] {
               H004A3_AGRID_nRecordCount
               }
            }
         );
         AV87Pgmname = "TabelaAtributosWC";
         /* GeneXus formulas. */
         AV87Pgmname = "TabelaAtributosWC";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_89 ;
      private short nGXsfl_89_idx=1 ;
      private short AV14OrderedBy ;
      private short AV17DynamicFiltersOperator1 ;
      private short AV21DynamicFiltersOperator2 ;
      private short AV25DynamicFiltersOperator3 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_89_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtAtributos_Nome_Titleformat ;
      private short cmbAtributos_TipoDados_Titleformat ;
      private short edtAtributos_Descricao_Titleformat ;
      private short chkAtributos_Ativo_Titleformat ;
      private short edtAtributos_Detalhes_Titleformat ;
      private short chkAtributos_PK_Titleformat ;
      private short chkAtributos_FK_Titleformat ;
      private short edtTabela_SistemaCod_Titleformat ;
      private short edtAtributos_MelhoraCod_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV58Atributos_TabelaCod ;
      private int AV59Sistema_Codigo ;
      private int wcpOAV58Atributos_TabelaCod ;
      private int wcpOAV59Sistema_Codigo ;
      private int subGrid_Rows ;
      private int A176Atributos_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int A356Atributos_TabelaCod ;
      private int edtAtributos_TabelaCod_Visible ;
      private int A190Tabela_SistemaCod ;
      private int A747Atributos_MelhoraCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV78PageToGo ;
      private int edtavUpdate_Enabled ;
      private int edtavDelete_Enabled ;
      private int edtavDisplay_Enabled ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavAtributos_nome1_Visible ;
      private int edtavAtributos_nome2_Visible ;
      private int edtavAtributos_nome3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV79GridCurrentPage ;
      private long AV80GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_89_idx="0001" ;
      private String AV18Atributos_Nome1 ;
      private String AV52Atributos_TipoDados1 ;
      private String AV22Atributos_Nome2 ;
      private String AV54Atributos_TipoDados2 ;
      private String AV26Atributos_Nome3 ;
      private String AV56Atributos_TipoDados3 ;
      private String AV87Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String GX_FocusControl ;
      private String edtAtributos_TabelaCod_Internalname ;
      private String edtAtributos_TabelaCod_Jsonclick ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String cmbavOrderedby_Internalname ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtAtributos_Codigo_Internalname ;
      private String A177Atributos_Nome ;
      private String edtAtributos_Nome_Internalname ;
      private String cmbAtributos_TipoDados_Internalname ;
      private String A178Atributos_TipoDados ;
      private String edtAtributos_Descricao_Internalname ;
      private String chkAtributos_Ativo_Internalname ;
      private String edtavDisplay_Internalname ;
      private String A390Atributos_Detalhes ;
      private String edtAtributos_Detalhes_Internalname ;
      private String chkAtributos_PK_Internalname ;
      private String chkAtributos_FK_Internalname ;
      private String edtTabela_SistemaCod_Internalname ;
      private String edtAtributos_MelhoraCod_Internalname ;
      private String chkavAtributos_ativo1_Internalname ;
      private String chkavAtributos_ativo2_Internalname ;
      private String chkavAtributos_ativo3_Internalname ;
      private String GXCCtl ;
      private String scmdbuf ;
      private String lV18Atributos_Nome1 ;
      private String lV52Atributos_TipoDados1 ;
      private String lV22Atributos_Nome2 ;
      private String lV54Atributos_TipoDados2 ;
      private String lV26Atributos_Nome3 ;
      private String lV56Atributos_TipoDados3 ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavAtributos_nome1_Internalname ;
      private String cmbavAtributos_tipodados1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavAtributos_nome2_Internalname ;
      private String cmbavAtributos_tipodados2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavAtributos_nome3_Internalname ;
      private String cmbavAtributos_tipodados3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtAtributos_Nome_Title ;
      private String edtAtributos_Descricao_Title ;
      private String edtAtributos_Detalhes_Title ;
      private String edtTabela_SistemaCod_Title ;
      private String edtAtributos_MelhoraCod_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String edtAtributos_Nome_Link ;
      private String sStyleString ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavAtributos_nome3_Jsonclick ;
      private String cmbavAtributos_tipodados3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavAtributos_nome2_Jsonclick ;
      private String cmbavAtributos_tipodados2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavAtributos_nome1_Jsonclick ;
      private String cmbavAtributos_tipodados1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sCtrlAV58Atributos_TabelaCod ;
      private String sCtrlAV59Sistema_Codigo ;
      private String sGXsfl_89_fel_idx="0001" ;
      private String ROClassString ;
      private String edtAtributos_Codigo_Jsonclick ;
      private String edtAtributos_Nome_Jsonclick ;
      private String cmbAtributos_TipoDados_Jsonclick ;
      private String edtAtributos_Descricao_Jsonclick ;
      private String edtAtributos_Detalhes_Jsonclick ;
      private String edtTabela_SistemaCod_Jsonclick ;
      private String edtAtributos_MelhoraCod_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV15OrderedDsc ;
      private bool AV53Atributos_Ativo1 ;
      private bool AV55Atributos_Ativo2 ;
      private bool AV57Atributos_Ativo3 ;
      private bool AV19DynamicFiltersEnabled2 ;
      private bool AV23DynamicFiltersEnabled3 ;
      private bool AV28DynamicFiltersIgnoreFirst ;
      private bool AV27DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n178Atributos_TipoDados ;
      private bool n179Atributos_Descricao ;
      private bool A180Atributos_Ativo ;
      private bool n390Atributos_Detalhes ;
      private bool A400Atributos_PK ;
      private bool n400Atributos_PK ;
      private bool A401Atributos_FK ;
      private bool n401Atributos_FK ;
      private bool n190Tabela_SistemaCod ;
      private bool n747Atributos_MelhoraCod ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV30Update_IsBlob ;
      private bool AV29Delete_IsBlob ;
      private bool AV81Display_IsBlob ;
      private String A179Atributos_Descricao ;
      private String AV16DynamicFiltersSelector1 ;
      private String AV20DynamicFiltersSelector2 ;
      private String AV24DynamicFiltersSelector3 ;
      private String AV84Update_GXI ;
      private String AV85Delete_GXI ;
      private String AV86Display_GXI ;
      private String AV30Update ;
      private String AV29Delete ;
      private String AV81Display ;
      private IGxSession AV31Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavAtributos_tipodados1 ;
      private GXCheckbox chkavAtributos_ativo1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavAtributos_tipodados2 ;
      private GXCheckbox chkavAtributos_ativo2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCombobox cmbavAtributos_tipodados3 ;
      private GXCheckbox chkavAtributos_ativo3 ;
      private GXCombobox cmbAtributos_TipoDados ;
      private GXCheckbox chkAtributos_Ativo ;
      private GXCheckbox chkAtributos_PK ;
      private GXCheckbox chkAtributos_FK ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H004A2_A356Atributos_TabelaCod ;
      private int[] H004A2_A747Atributos_MelhoraCod ;
      private bool[] H004A2_n747Atributos_MelhoraCod ;
      private int[] H004A2_A190Tabela_SistemaCod ;
      private bool[] H004A2_n190Tabela_SistemaCod ;
      private bool[] H004A2_A401Atributos_FK ;
      private bool[] H004A2_n401Atributos_FK ;
      private bool[] H004A2_A400Atributos_PK ;
      private bool[] H004A2_n400Atributos_PK ;
      private String[] H004A2_A390Atributos_Detalhes ;
      private bool[] H004A2_n390Atributos_Detalhes ;
      private bool[] H004A2_A180Atributos_Ativo ;
      private String[] H004A2_A179Atributos_Descricao ;
      private bool[] H004A2_n179Atributos_Descricao ;
      private String[] H004A2_A178Atributos_TipoDados ;
      private bool[] H004A2_n178Atributos_TipoDados ;
      private String[] H004A2_A177Atributos_Nome ;
      private int[] H004A2_A176Atributos_Codigo ;
      private long[] H004A3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV13GridStateDynamicFilter ;
   }

   public class tabelaatributoswc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H004A2( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             short AV17DynamicFiltersOperator1 ,
                                             String AV18Atributos_Nome1 ,
                                             String AV52Atributos_TipoDados1 ,
                                             bool AV53Atributos_Ativo1 ,
                                             bool AV19DynamicFiltersEnabled2 ,
                                             String AV20DynamicFiltersSelector2 ,
                                             short AV21DynamicFiltersOperator2 ,
                                             String AV22Atributos_Nome2 ,
                                             String AV54Atributos_TipoDados2 ,
                                             bool AV55Atributos_Ativo2 ,
                                             bool AV23DynamicFiltersEnabled3 ,
                                             String AV24DynamicFiltersSelector3 ,
                                             short AV25DynamicFiltersOperator3 ,
                                             String AV26Atributos_Nome3 ,
                                             String AV56Atributos_TipoDados3 ,
                                             bool AV57Atributos_Ativo3 ,
                                             String A177Atributos_Nome ,
                                             String A178Atributos_TipoDados ,
                                             bool A180Atributos_Ativo ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A356Atributos_TabelaCod ,
                                             int AV58Atributos_TabelaCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [27] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[Atributos_TabelaCod] AS Atributos_TabelaCod, T1.[Atributos_MelhoraCod], T2.[Tabela_SistemaCod], T1.[Atributos_FK], T1.[Atributos_PK], T1.[Atributos_Detalhes], T1.[Atributos_Ativo], T1.[Atributos_Descricao], T1.[Atributos_TipoDados], T1.[Atributos_Nome], T1.[Atributos_Codigo]";
         sFromString = " FROM ([Atributos] T1 WITH (NOLOCK) INNER JOIN [Tabela] T2 WITH (NOLOCK) ON T2.[Tabela_Codigo] = T1.[Atributos_TabelaCod])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[Atributos_TabelaCod] = @AV58Atributos_TabelaCod)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ATRIBUTOS_NOME") == 0 ) && ( ( AV17DynamicFiltersOperator1 == 0 ) || ( AV17DynamicFiltersOperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Atributos_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_Nome] like @lV18Atributos_Nome1)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ATRIBUTOS_NOME") == 0 ) && ( ( AV17DynamicFiltersOperator1 == 1 ) || ( AV17DynamicFiltersOperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Atributos_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_Nome] like '%' + @lV18Atributos_Nome1)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ATRIBUTOS_TIPODADOS") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52Atributos_TipoDados1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_TipoDados] = @AV52Atributos_TipoDados1)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ATRIBUTOS_TIPODADOS") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52Atributos_TipoDados1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_TipoDados] <> @AV52Atributos_TipoDados1)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ATRIBUTOS_TIPODADOS") == 0 ) && ( AV17DynamicFiltersOperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52Atributos_TipoDados1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_TipoDados] like @lV52Atributos_TipoDados1)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ATRIBUTOS_TIPODADOS") == 0 ) && ( AV17DynamicFiltersOperator1 == 3 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52Atributos_TipoDados1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_TipoDados] like '%' + @lV52Atributos_TipoDados1)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ATRIBUTOS_ATIVO") == 0 ) && ( ! (false==AV53Atributos_Ativo1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_Ativo] = @AV53Atributos_Ativo1)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ATRIBUTOS_NOME") == 0 ) && ( ( AV21DynamicFiltersOperator2 == 0 ) || ( AV21DynamicFiltersOperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22Atributos_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_Nome] like @lV22Atributos_Nome2)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ATRIBUTOS_NOME") == 0 ) && ( ( AV21DynamicFiltersOperator2 == 1 ) || ( AV21DynamicFiltersOperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22Atributos_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_Nome] like '%' + @lV22Atributos_Nome2)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ATRIBUTOS_TIPODADOS") == 0 ) && ( AV21DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54Atributos_TipoDados2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_TipoDados] = @AV54Atributos_TipoDados2)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ATRIBUTOS_TIPODADOS") == 0 ) && ( AV21DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54Atributos_TipoDados2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_TipoDados] <> @AV54Atributos_TipoDados2)";
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ATRIBUTOS_TIPODADOS") == 0 ) && ( AV21DynamicFiltersOperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54Atributos_TipoDados2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_TipoDados] like @lV54Atributos_TipoDados2)";
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ATRIBUTOS_TIPODADOS") == 0 ) && ( AV21DynamicFiltersOperator2 == 3 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54Atributos_TipoDados2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_TipoDados] like '%' + @lV54Atributos_TipoDados2)";
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ATRIBUTOS_ATIVO") == 0 ) && ( ! (false==AV55Atributos_Ativo2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_Ativo] = @AV55Atributos_Ativo2)";
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ATRIBUTOS_NOME") == 0 ) && ( ( AV25DynamicFiltersOperator3 == 0 ) || ( AV25DynamicFiltersOperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26Atributos_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_Nome] like @lV26Atributos_Nome3)";
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ATRIBUTOS_NOME") == 0 ) && ( ( AV25DynamicFiltersOperator3 == 1 ) || ( AV25DynamicFiltersOperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26Atributos_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_Nome] like '%' + @lV26Atributos_Nome3)";
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ATRIBUTOS_TIPODADOS") == 0 ) && ( AV25DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56Atributos_TipoDados3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_TipoDados] = @AV56Atributos_TipoDados3)";
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ATRIBUTOS_TIPODADOS") == 0 ) && ( AV25DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56Atributos_TipoDados3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_TipoDados] <> @AV56Atributos_TipoDados3)";
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ATRIBUTOS_TIPODADOS") == 0 ) && ( AV25DynamicFiltersOperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56Atributos_TipoDados3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_TipoDados] like @lV56Atributos_TipoDados3)";
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ATRIBUTOS_TIPODADOS") == 0 ) && ( AV25DynamicFiltersOperator3 == 3 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56Atributos_TipoDados3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_TipoDados] like '%' + @lV56Atributos_TipoDados3)";
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ATRIBUTOS_ATIVO") == 0 ) && ( ! (false==AV57Atributos_Ativo3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_Ativo] = @AV57Atributos_Ativo3)";
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Atributos_TabelaCod], T1.[Atributos_Nome]";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Atributos_TabelaCod] DESC, T1.[Atributos_Nome] DESC";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Atributos_TabelaCod], T1.[Atributos_TipoDados]";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Atributos_TabelaCod] DESC, T1.[Atributos_TipoDados] DESC";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Atributos_TabelaCod], T1.[Atributos_Descricao]";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Atributos_TabelaCod] DESC, T1.[Atributos_Descricao] DESC";
         }
         else if ( ( AV14OrderedBy == 4 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Atributos_TabelaCod], T1.[Atributos_Ativo]";
         }
         else if ( ( AV14OrderedBy == 4 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Atributos_TabelaCod] DESC, T1.[Atributos_Ativo] DESC";
         }
         else if ( ( AV14OrderedBy == 5 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Atributos_TabelaCod], T1.[Atributos_Detalhes]";
         }
         else if ( ( AV14OrderedBy == 5 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Atributos_TabelaCod] DESC, T1.[Atributos_Detalhes] DESC";
         }
         else if ( ( AV14OrderedBy == 6 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Atributos_TabelaCod], T1.[Atributos_PK]";
         }
         else if ( ( AV14OrderedBy == 6 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Atributos_TabelaCod] DESC, T1.[Atributos_PK] DESC";
         }
         else if ( ( AV14OrderedBy == 7 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Atributos_TabelaCod], T1.[Atributos_FK]";
         }
         else if ( ( AV14OrderedBy == 7 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Atributos_TabelaCod] DESC, T1.[Atributos_FK] DESC";
         }
         else if ( ( AV14OrderedBy == 8 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Atributos_TabelaCod], T2.[Tabela_SistemaCod]";
         }
         else if ( ( AV14OrderedBy == 8 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Atributos_TabelaCod] DESC, T2.[Tabela_SistemaCod] DESC";
         }
         else if ( ( AV14OrderedBy == 9 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Atributos_TabelaCod], T1.[Atributos_MelhoraCod]";
         }
         else if ( ( AV14OrderedBy == 9 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Atributos_TabelaCod] DESC, T1.[Atributos_MelhoraCod] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Atributos_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_H004A3( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             short AV17DynamicFiltersOperator1 ,
                                             String AV18Atributos_Nome1 ,
                                             String AV52Atributos_TipoDados1 ,
                                             bool AV53Atributos_Ativo1 ,
                                             bool AV19DynamicFiltersEnabled2 ,
                                             String AV20DynamicFiltersSelector2 ,
                                             short AV21DynamicFiltersOperator2 ,
                                             String AV22Atributos_Nome2 ,
                                             String AV54Atributos_TipoDados2 ,
                                             bool AV55Atributos_Ativo2 ,
                                             bool AV23DynamicFiltersEnabled3 ,
                                             String AV24DynamicFiltersSelector3 ,
                                             short AV25DynamicFiltersOperator3 ,
                                             String AV26Atributos_Nome3 ,
                                             String AV56Atributos_TipoDados3 ,
                                             bool AV57Atributos_Ativo3 ,
                                             String A177Atributos_Nome ,
                                             String A178Atributos_TipoDados ,
                                             bool A180Atributos_Ativo ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A356Atributos_TabelaCod ,
                                             int AV58Atributos_TabelaCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [23] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([Atributos] T1 WITH (NOLOCK) INNER JOIN [Tabela] T2 WITH (NOLOCK) ON T2.[Tabela_Codigo] = T1.[Atributos_TabelaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Atributos_TabelaCod] = @AV58Atributos_TabelaCod)";
         scmdbuf = scmdbuf + " and (T1.[Atributos_TabelaCod] = @AV58Atributos_TabelaCod)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ATRIBUTOS_NOME") == 0 ) && ( ( AV17DynamicFiltersOperator1 == 0 ) || ( AV17DynamicFiltersOperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Atributos_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_Nome] like @lV18Atributos_Nome1)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ATRIBUTOS_NOME") == 0 ) && ( ( AV17DynamicFiltersOperator1 == 1 ) || ( AV17DynamicFiltersOperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Atributos_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_Nome] like '%' + @lV18Atributos_Nome1)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ATRIBUTOS_TIPODADOS") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52Atributos_TipoDados1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_TipoDados] = @AV52Atributos_TipoDados1)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ATRIBUTOS_TIPODADOS") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52Atributos_TipoDados1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_TipoDados] <> @AV52Atributos_TipoDados1)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ATRIBUTOS_TIPODADOS") == 0 ) && ( AV17DynamicFiltersOperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52Atributos_TipoDados1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_TipoDados] like @lV52Atributos_TipoDados1)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ATRIBUTOS_TIPODADOS") == 0 ) && ( AV17DynamicFiltersOperator1 == 3 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52Atributos_TipoDados1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_TipoDados] like '%' + @lV52Atributos_TipoDados1)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ATRIBUTOS_ATIVO") == 0 ) && ( ! (false==AV53Atributos_Ativo1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_Ativo] = @AV53Atributos_Ativo1)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ATRIBUTOS_NOME") == 0 ) && ( ( AV21DynamicFiltersOperator2 == 0 ) || ( AV21DynamicFiltersOperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22Atributos_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_Nome] like @lV22Atributos_Nome2)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ATRIBUTOS_NOME") == 0 ) && ( ( AV21DynamicFiltersOperator2 == 1 ) || ( AV21DynamicFiltersOperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22Atributos_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_Nome] like '%' + @lV22Atributos_Nome2)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ATRIBUTOS_TIPODADOS") == 0 ) && ( AV21DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54Atributos_TipoDados2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_TipoDados] = @AV54Atributos_TipoDados2)";
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ATRIBUTOS_TIPODADOS") == 0 ) && ( AV21DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54Atributos_TipoDados2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_TipoDados] <> @AV54Atributos_TipoDados2)";
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ATRIBUTOS_TIPODADOS") == 0 ) && ( AV21DynamicFiltersOperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54Atributos_TipoDados2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_TipoDados] like @lV54Atributos_TipoDados2)";
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ATRIBUTOS_TIPODADOS") == 0 ) && ( AV21DynamicFiltersOperator2 == 3 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54Atributos_TipoDados2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_TipoDados] like '%' + @lV54Atributos_TipoDados2)";
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ATRIBUTOS_ATIVO") == 0 ) && ( ! (false==AV55Atributos_Ativo2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_Ativo] = @AV55Atributos_Ativo2)";
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ATRIBUTOS_NOME") == 0 ) && ( ( AV25DynamicFiltersOperator3 == 0 ) || ( AV25DynamicFiltersOperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26Atributos_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_Nome] like @lV26Atributos_Nome3)";
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ATRIBUTOS_NOME") == 0 ) && ( ( AV25DynamicFiltersOperator3 == 1 ) || ( AV25DynamicFiltersOperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26Atributos_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_Nome] like '%' + @lV26Atributos_Nome3)";
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ATRIBUTOS_TIPODADOS") == 0 ) && ( AV25DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56Atributos_TipoDados3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_TipoDados] = @AV56Atributos_TipoDados3)";
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ATRIBUTOS_TIPODADOS") == 0 ) && ( AV25DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56Atributos_TipoDados3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_TipoDados] <> @AV56Atributos_TipoDados3)";
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ATRIBUTOS_TIPODADOS") == 0 ) && ( AV25DynamicFiltersOperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56Atributos_TipoDados3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_TipoDados] like @lV56Atributos_TipoDados3)";
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ATRIBUTOS_TIPODADOS") == 0 ) && ( AV25DynamicFiltersOperator3 == 3 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56Atributos_TipoDados3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_TipoDados] like '%' + @lV56Atributos_TipoDados3)";
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ATRIBUTOS_ATIVO") == 0 ) && ( ! (false==AV57Atributos_Ativo3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_Ativo] = @AV57Atributos_Ativo3)";
         }
         else
         {
            GXv_int3[22] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 4 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 4 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 5 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 5 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 6 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 6 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 7 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 7 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 8 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 8 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 9 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 9 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H004A2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (bool)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (bool)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (bool)dynConstraints[19] , (short)dynConstraints[20] , (bool)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] );
               case 1 :
                     return conditional_H004A3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (bool)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (bool)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (bool)dynConstraints[19] , (short)dynConstraints[20] , (bool)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH004A2 ;
          prmH004A2 = new Object[] {
          new Object[] {"@AV58Atributos_TabelaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV18Atributos_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV18Atributos_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV52Atributos_TipoDados1",SqlDbType.Char,4,0} ,
          new Object[] {"@AV52Atributos_TipoDados1",SqlDbType.Char,4,0} ,
          new Object[] {"@lV52Atributos_TipoDados1",SqlDbType.Char,4,0} ,
          new Object[] {"@lV52Atributos_TipoDados1",SqlDbType.Char,4,0} ,
          new Object[] {"@AV53Atributos_Ativo1",SqlDbType.Bit,4,0} ,
          new Object[] {"@lV22Atributos_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV22Atributos_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV54Atributos_TipoDados2",SqlDbType.Char,4,0} ,
          new Object[] {"@AV54Atributos_TipoDados2",SqlDbType.Char,4,0} ,
          new Object[] {"@lV54Atributos_TipoDados2",SqlDbType.Char,4,0} ,
          new Object[] {"@lV54Atributos_TipoDados2",SqlDbType.Char,4,0} ,
          new Object[] {"@AV55Atributos_Ativo2",SqlDbType.Bit,4,0} ,
          new Object[] {"@lV26Atributos_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV26Atributos_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV56Atributos_TipoDados3",SqlDbType.Char,4,0} ,
          new Object[] {"@AV56Atributos_TipoDados3",SqlDbType.Char,4,0} ,
          new Object[] {"@lV56Atributos_TipoDados3",SqlDbType.Char,4,0} ,
          new Object[] {"@lV56Atributos_TipoDados3",SqlDbType.Char,4,0} ,
          new Object[] {"@AV57Atributos_Ativo3",SqlDbType.Bit,4,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH004A3 ;
          prmH004A3 = new Object[] {
          new Object[] {"@AV58Atributos_TabelaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV58Atributos_TabelaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV18Atributos_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV18Atributos_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV52Atributos_TipoDados1",SqlDbType.Char,4,0} ,
          new Object[] {"@AV52Atributos_TipoDados1",SqlDbType.Char,4,0} ,
          new Object[] {"@lV52Atributos_TipoDados1",SqlDbType.Char,4,0} ,
          new Object[] {"@lV52Atributos_TipoDados1",SqlDbType.Char,4,0} ,
          new Object[] {"@AV53Atributos_Ativo1",SqlDbType.Bit,4,0} ,
          new Object[] {"@lV22Atributos_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV22Atributos_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV54Atributos_TipoDados2",SqlDbType.Char,4,0} ,
          new Object[] {"@AV54Atributos_TipoDados2",SqlDbType.Char,4,0} ,
          new Object[] {"@lV54Atributos_TipoDados2",SqlDbType.Char,4,0} ,
          new Object[] {"@lV54Atributos_TipoDados2",SqlDbType.Char,4,0} ,
          new Object[] {"@AV55Atributos_Ativo2",SqlDbType.Bit,4,0} ,
          new Object[] {"@lV26Atributos_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV26Atributos_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV56Atributos_TipoDados3",SqlDbType.Char,4,0} ,
          new Object[] {"@AV56Atributos_TipoDados3",SqlDbType.Char,4,0} ,
          new Object[] {"@lV56Atributos_TipoDados3",SqlDbType.Char,4,0} ,
          new Object[] {"@lV56Atributos_TipoDados3",SqlDbType.Char,4,0} ,
          new Object[] {"@AV57Atributos_Ativo3",SqlDbType.Bit,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H004A2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH004A2,11,0,true,false )
             ,new CursorDef("H004A3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH004A3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((bool[]) buf[5])[0] = rslt.getBool(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((bool[]) buf[7])[0] = rslt.getBool(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 10) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((bool[]) buf[11])[0] = rslt.getBool(7) ;
                ((String[]) buf[12])[0] = rslt.getLongVarchar(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getString(9, 4) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getString(10, 50) ;
                ((int[]) buf[17])[0] = rslt.getInt(11) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[34]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[41]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[48]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[50]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[51]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[52]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[53]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[31]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[38]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[45]);
                }
                return;
       }
    }

 }

}
