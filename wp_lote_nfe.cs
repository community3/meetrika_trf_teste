/*
               File: WP_Lote_NFe
        Description: NFe
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:21:29.36
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_lote_nfe : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_lote_nfe( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_lote_nfe( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Lote_Codigo ,
                           ref bool aP1_Confirmado )
      {
         this.AV22Lote_Codigo = aP0_Lote_Codigo;
         this.AV13Confirmado = aP1_Confirmado;
         executePrivate();
         aP0_Lote_Codigo=this.AV22Lote_Codigo;
         aP1_Confirmado=this.AV13Confirmado;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV22Lote_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Lote_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Lote_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV13Confirmado = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Confirmado", AV13Confirmado);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PADV2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTDV2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206216212938");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_lote_nfe.aspx") + "?" + UrlEncode("" +AV22Lote_Codigo) + "," + UrlEncode(StringUtil.BoolToStr(AV13Confirmado))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vLOTE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22Lote_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_LOTEACEITECOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A597ContagemResultado_LoteAceiteCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCODIGOS", AV16Codigos);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCODIGOS", AV16Codigos);
         }
         GxWebStd.gx_hidden_field( context, "vLOTE_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18Lote_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vLOTE_NOME", StringUtil.RTrim( AV21Lote_Nome));
         GxWebStd.gx_boolean_hidden_field( context, "vCONFIRMADO", AV13Confirmado);
         GXCCtlgxBlob = "vLOTE_NFEARQ" + "_gxBlob";
         GxWebStd.gx_hidden_field( context, GXCCtlgxBlob, AV11Lote_NFeArq);
         GxWebStd.gx_hidden_field( context, "vLOTE_NFEARQ_Filetype", StringUtil.RTrim( edtavLote_nfearq_Filetype));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEDV2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTDV2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_lote_nfe.aspx") + "?" + UrlEncode("" +AV22Lote_Codigo) + "," + UrlEncode(StringUtil.BoolToStr(AV13Confirmado)) ;
      }

      public override String GetPgmname( )
      {
         return "WP_Lote_NFe" ;
      }

      public override String GetPgmdesc( )
      {
         return "NFe" ;
      }

      protected void WBDV0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_DV2( true) ;
         }
         else
         {
            wb_table1_2_DV2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_DV2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFilename_Internalname, StringUtil.RTrim( AV15FileName), StringUtil.RTrim( context.localUtil.Format( AV15FileName, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,35);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFilename_Jsonclick, 0, "Attribute", "", "", "", edtavFilename_Visible, 1, 0, "text", "", 80, "chr", 2, "px", 255, 0, 0, 0, 1, -1, 0, true, "", "left", true, "HLP_WP_Lote_NFe.htm");
         }
         wbLoad = true;
      }

      protected void STARTDV2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "NFe", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPDV0( ) ;
      }

      protected void WSDV2( )
      {
         STARTDV2( ) ;
         EVTDV2( ) ;
      }

      protected void EVTDV2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11DV2 */
                              E11DV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'CONFIRMAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12DV2 */
                              E12DV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'CANCELAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13DV2 */
                              E13DV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14DV2 */
                              E14DV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEDV2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PADV2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavLote_nfe_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFDV2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFDV2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E14DV2 */
            E14DV2 ();
            WBDV0( ) ;
         }
      }

      protected void STRUPDV0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11DV2 */
         E11DV2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtavLote_nfe_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavLote_nfe_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vLOTE_NFE");
               GX_FocusControl = edtavLote_nfe_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV5Lote_NFe = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Lote_NFe", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Lote_NFe), 6, 0)));
            }
            else
            {
               AV5Lote_NFe = (int)(context.localUtil.CToN( cgiGet( edtavLote_nfe_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Lote_NFe", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Lote_NFe), 6, 0)));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavLote_datanfe_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data emis�o"}), 1, "vLOTE_DATANFE");
               GX_FocusControl = edtavLote_datanfe_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV10Lote_DataNFe = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Lote_DataNFe", context.localUtil.Format(AV10Lote_DataNFe, "99/99/99"));
            }
            else
            {
               AV10Lote_DataNFe = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavLote_datanfe_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Lote_DataNFe", context.localUtil.Format(AV10Lote_DataNFe, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavLote_nfedataprotocolo_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data do protocolo"}), 1, "vLOTE_NFEDATAPROTOCOLO");
               GX_FocusControl = edtavLote_nfedataprotocolo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV14Lote_NFeDataProtocolo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Lote_NFeDataProtocolo", context.localUtil.Format(AV14Lote_NFeDataProtocolo, "99/99/99"));
            }
            else
            {
               AV14Lote_NFeDataProtocolo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavLote_nfedataprotocolo_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Lote_NFeDataProtocolo", context.localUtil.Format(AV14Lote_NFeDataProtocolo, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavLote_datapagamento_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Previs�o de Pagamento"}), 1, "vLOTE_DATAPAGAMENTO");
               GX_FocusControl = edtavLote_datapagamento_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV12Lote_DataPagamento = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Lote_DataPagamento", context.localUtil.Format(AV12Lote_DataPagamento, "99/99/99"));
            }
            else
            {
               AV12Lote_DataPagamento = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavLote_datapagamento_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Lote_DataPagamento", context.localUtil.Format(AV12Lote_DataPagamento, "99/99/99"));
            }
            AV11Lote_NFeArq = cgiGet( edtavLote_nfearq_Internalname);
            AV15FileName = cgiGet( edtavFilename_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15FileName", AV15FileName);
            /* Read saved values. */
            edtavLote_nfearq_Filetype = cgiGet( "vLOTE_NFEARQ_Filetype");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11Lote_NFeArq)) )
            {
               GXCCtlgxBlob = "vLOTE_NFEARQ" + "_gxBlob";
               AV11Lote_NFeArq = cgiGet( GXCCtlgxBlob);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11DV2 */
         E11DV2 ();
         if (returnInSub) return;
      }

      protected void E11DV2( )
      {
         /* Start Routine */
         Form.Jscriptsrc.Add("http://code.jquery.com/jquery-latest.js\">") ;
         Form.Headerrawhtml = "<script type=\"text/javascript\">";
         Form.Headerrawhtml = Form.Headerrawhtml+"$(document).ready(function(){";
         Form.Headerrawhtml = Form.Headerrawhtml+"$(\"#vLOTE_NFEARQ\").blur(function(){";
         Form.Headerrawhtml = Form.Headerrawhtml+"var nome = $(\"#vLOTE_NFEARQ\").val();";
         Form.Headerrawhtml = Form.Headerrawhtml+"$(\"#vFILENAME\").val(nome);";
         Form.Headerrawhtml = Form.Headerrawhtml+"});";
         Form.Headerrawhtml = Form.Headerrawhtml+"});";
         Form.Headerrawhtml = Form.Headerrawhtml+"</script>";
         edtavFilename_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFilename_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFilename_Visible), 5, 0)));
         AV10Lote_DataNFe = DateTimeUtil.ResetTime(DateTimeUtil.ServerNow( context, "DEFAULT"));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Lote_DataNFe", context.localUtil.Format(AV10Lote_DataNFe, "99/99/99"));
         AV14Lote_NFeDataProtocolo = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Lote_NFeDataProtocolo", context.localUtil.Format(AV14Lote_NFeDataProtocolo, "99/99/99"));
         /* Using cursor H00DV2 */
         pr_default.execute(0, new Object[] {AV22Lote_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A596Lote_Codigo = H00DV2_A596Lote_Codigo[0];
            A855AreaTrabalho_DiasParaPagar = H00DV2_A855AreaTrabalho_DiasParaPagar[0];
            n855AreaTrabalho_DiasParaPagar = H00DV2_n855AreaTrabalho_DiasParaPagar[0];
            A595Lote_AreaTrabalhoCod = H00DV2_A595Lote_AreaTrabalhoCod[0];
            A563Lote_Nome = H00DV2_A563Lote_Nome[0];
            A855AreaTrabalho_DiasParaPagar = H00DV2_A855AreaTrabalho_DiasParaPagar[0];
            n855AreaTrabalho_DiasParaPagar = H00DV2_n855AreaTrabalho_DiasParaPagar[0];
            GXt_dtime1 = DateTimeUtil.ResetTime( AV10Lote_DataNFe ) ;
            AV12Lote_DataPagamento = DateTimeUtil.ResetTime(DateTimeUtil.TAdd( GXt_dtime1, 86400*(A855AreaTrabalho_DiasParaPagar)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Lote_DataPagamento", context.localUtil.Format(AV12Lote_DataPagamento, "99/99/99"));
            AV18Lote_AreaTrabalhoCod = A595Lote_AreaTrabalhoCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Lote_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18Lote_AreaTrabalhoCod), 6, 0)));
            AV21Lote_Nome = A563Lote_Nome;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Lote_Nome", AV21Lote_Nome);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         Form.Caption = "NFe do lote "+AV21Lote_Nome;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
      }

      protected void E12DV2( )
      {
         /* 'Confirmar' Routine */
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11Lote_NFeArq)) )
         {
            GX_msglist.addItem("Arquivo da NFe � obrigat�rio!");
         }
         else if ( (0==AV5Lote_NFe) )
         {
            GX_msglist.addItem("N�mero da NFe � obrigat�rio!");
         }
         else if ( (DateTime.MinValue==AV10Lote_DataNFe) )
         {
            GX_msglist.addItem("Data de emiss�o da NFe incorreta!");
         }
         else if ( (DateTime.MinValue==AV12Lote_DataPagamento) )
         {
            GX_msglist.addItem("Data prevista de pagamento incorreta!");
         }
         else if ( (DateTime.MinValue==AV14Lote_NFeDataProtocolo) )
         {
            GX_msglist.addItem("Data prevista de protocolo incorreta!");
         }
         else
         {
            AV13Confirmado = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Confirmado", AV13Confirmado);
            AV15FileName = StringUtil.Substring( AV15FileName, StringUtil.StringSearchRev( AV15FileName, "\\", -1)+1, 255);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15FileName", AV15FileName);
            AV15FileName = StringUtil.Substring( AV15FileName, 1, StringUtil.StringSearchRev( AV15FileName, ".", -1)-1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15FileName", AV15FileName);
            /* Execute user subroutine: 'UPDATE' */
            S112 ();
            if (returnInSub) return;
            context.setWebReturnParms(new Object[] {(int)AV22Lote_Codigo,(bool)AV13Confirmado});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV16Codigos", AV16Codigos);
      }

      protected void E13DV2( )
      {
         /* 'Cancelar' Routine */
         context.setWebReturnParms(new Object[] {(int)AV22Lote_Codigo,(bool)AV13Confirmado});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void S112( )
      {
         /* 'UPDATE' Routine */
         new prc_updlotenfe(context ).execute( ref  AV22Lote_Codigo,  AV5Lote_NFe,  AV10Lote_DataNFe,  AV11Lote_NFeArq,  AV15FileName,  edtavLote_nfearq_Filetype,  AV12Lote_DataPagamento,  AV14Lote_NFeDataProtocolo) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Lote_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Lote_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Lote_NFe", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Lote_NFe), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Lote_DataNFe", context.localUtil.Format(AV10Lote_DataNFe, "99/99/99"));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Lote_NFeArq", AV11Lote_NFeArq);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15FileName", AV15FileName);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_nfearq_Internalname, "Filetype", edtavLote_nfearq_Filetype);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Lote_DataPagamento", context.localUtil.Format(AV12Lote_DataPagamento, "99/99/99"));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Lote_NFeDataProtocolo", context.localUtil.Format(AV14Lote_NFeDataProtocolo, "99/99/99"));
         /* Execute user subroutine: 'STATUS' */
         S122 ();
         if (returnInSub) return;
      }

      protected void S122( )
      {
         /* 'STATUS' Routine */
         /* Using cursor H00DV3 */
         pr_default.execute(1, new Object[] {AV22Lote_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A597ContagemResultado_LoteAceiteCod = H00DV3_A597ContagemResultado_LoteAceiteCod[0];
            n597ContagemResultado_LoteAceiteCod = H00DV3_n597ContagemResultado_LoteAceiteCod[0];
            A456ContagemResultado_Codigo = H00DV3_A456ContagemResultado_Codigo[0];
            AV16Codigos.Add(A456ContagemResultado_Codigo, 0);
            pr_default.readNext(1);
         }
         pr_default.close(1);
         AV17WebSession.Set("SdtCodigos", AV16Codigos.ToXml(false, true, "Collection", ""));
         new prc_agruparparafaturamento(context ).execute(  AV18Lote_AreaTrabalhoCod,  5,  "",  "P",  AV21Lote_Nome) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Lote_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18Lote_AreaTrabalhoCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Lote_Nome", AV21Lote_Nome);
      }

      protected void nextLoad( )
      {
      }

      protected void E14DV2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_DV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(400), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCellLeft'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock2_Internalname, "N�mero NFe:", "", "", lblTextblock2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Lote_NFe.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLote_nfe_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV5Lote_NFe), 6, 0, ",", "")), ((edtavLote_nfe_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV5Lote_NFe), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV5Lote_NFe), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,10);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_nfe_Jsonclick, 0, "Attribute", "", "", "", 1, edtavLote_nfe_Enabled, 0, "text", "", 54, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_Lote_NFe.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCellLeft'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock3_Internalname, "Data de emiss�o:", "", "", lblTextblock3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Lote_NFe.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavLote_datanfe_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavLote_datanfe_Internalname, context.localUtil.Format(AV10Lote_DataNFe, "99/99/99"), context.localUtil.Format( AV10Lote_DataNFe, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,15);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_datanfe_Jsonclick, 0, "Attribute", "", "", "", 1, edtavLote_datanfe_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WP_Lote_NFe.htm");
            GxWebStd.gx_bitmap( context, edtavLote_datanfe_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtavLote_datanfe_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_Lote_NFe.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCellLeft'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock5_Internalname, "Data do protocolo:", "", "", lblTextblock5_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Lote_NFe.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavLote_nfedataprotocolo_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavLote_nfedataprotocolo_Internalname, context.localUtil.Format(AV14Lote_NFeDataProtocolo, "99/99/99"), context.localUtil.Format( AV14Lote_NFeDataProtocolo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,20);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_nfedataprotocolo_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WP_Lote_NFe.htm");
            GxWebStd.gx_bitmap( context, edtavLote_nfedataprotocolo_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_Lote_NFe.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCellLeft'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock6_Internalname, "Data prevista de pagamento:", "", "", lblTextblock6_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Lote_NFe.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavLote_datapagamento_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavLote_datapagamento_Internalname, context.localUtil.Format(AV12Lote_DataPagamento, "99/99/99"), context.localUtil.Format( AV12Lote_DataPagamento, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,25);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_datapagamento_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_Lote_NFe.htm");
            GxWebStd.gx_bitmap( context, edtavLote_datapagamento_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_Lote_NFe.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCellLeft'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock4_Internalname, "Arquivo:", "", "", lblTextblock4_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Lote_NFe.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            ClassString = "Image";
            StyleString = "";
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'',false,'',0)\"";
            edtavLote_nfearq_Filetype = "tmp";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_nfearq_Internalname, "Filetype", edtavLote_nfearq_Filetype);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11Lote_NFeArq)) )
            {
               gxblobfileaux.Source = AV11Lote_NFeArq;
               if ( ! gxblobfileaux.HasExtension() || ( StringUtil.StrCmp(edtavLote_nfearq_Filetype, "tmp") != 0 ) )
               {
                  gxblobfileaux.SetExtension(StringUtil.Trim( edtavLote_nfearq_Filetype));
               }
               if ( gxblobfileaux.ErrCode == 0 )
               {
                  AV11Lote_NFeArq = gxblobfileaux.GetAbsoluteName();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_nfearq_Internalname, "URL", context.PathToRelativeUrl( AV11Lote_NFeArq));
                  edtavLote_nfearq_Filetype = gxblobfileaux.GetExtension();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_nfearq_Internalname, "Filetype", edtavLote_nfearq_Filetype);
               }
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_nfearq_Internalname, "URL", context.PathToRelativeUrl( AV11Lote_NFeArq));
            }
            GxWebStd.gx_blob_field( context, edtavLote_nfearq_Internalname, StringUtil.RTrim( AV11Lote_NFeArq), context.PathToRelativeUrl( AV11Lote_NFeArq), (String.IsNullOrEmpty(StringUtil.RTrim( edtavLote_nfearq_Contenttype)) ? context.GetContentType( (String.IsNullOrEmpty(StringUtil.RTrim( edtavLote_nfearq_Filetype)) ? AV11Lote_NFeArq : edtavLote_nfearq_Filetype)) : edtavLote_nfearq_Contenttype), false, "", edtavLote_nfearq_Parameters, 1, edtavLote_nfearq_Enabled, 1, "", "", 0, -1, 250, "px", 60, "px", 0, 0, 0, edtavLote_nfearq_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", StyleString, ClassString, "", ""+TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,30);\"", "", "", "HLP_WP_Lote_NFe.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\" colspan=\"2\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;vertical-align:middle;height:100px")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton1_Internalname, "", "Confirmar", bttButton1_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'CONFIRMAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_Lote_NFe.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton2_Internalname, "", "Cancelar", bttButton2_Jsonclick, 5, "Cancelar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'CANCELAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_Lote_NFe.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_DV2e( true) ;
         }
         else
         {
            wb_table1_2_DV2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV22Lote_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Lote_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Lote_Codigo), 6, 0)));
         AV13Confirmado = (bool)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Confirmado", AV13Confirmado);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PADV2( ) ;
         WSDV2( ) ;
         WEDV2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20206216212960");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_lote_nfe.js", "?20206216212960");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblock2_Internalname = "TEXTBLOCK2";
         edtavLote_nfe_Internalname = "vLOTE_NFE";
         lblTextblock3_Internalname = "TEXTBLOCK3";
         edtavLote_datanfe_Internalname = "vLOTE_DATANFE";
         lblTextblock5_Internalname = "TEXTBLOCK5";
         edtavLote_nfedataprotocolo_Internalname = "vLOTE_NFEDATAPROTOCOLO";
         lblTextblock6_Internalname = "TEXTBLOCK6";
         edtavLote_datapagamento_Internalname = "vLOTE_DATAPAGAMENTO";
         lblTextblock4_Internalname = "TEXTBLOCK4";
         edtavLote_nfearq_Internalname = "vLOTE_NFEARQ";
         bttButton1_Internalname = "BUTTON1";
         bttButton2_Internalname = "BUTTON2";
         tblTable1_Internalname = "TABLE1";
         edtavFilename_Internalname = "vFILENAME";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavLote_nfearq_Jsonclick = "";
         edtavLote_nfearq_Parameters = "";
         edtavLote_nfearq_Contenttype = "";
         edtavLote_nfearq_Enabled = 1;
         edtavLote_datapagamento_Jsonclick = "";
         edtavLote_nfedataprotocolo_Jsonclick = "";
         edtavLote_datanfe_Jsonclick = "";
         edtavLote_datanfe_Enabled = 1;
         edtavLote_nfe_Jsonclick = "";
         edtavLote_nfe_Enabled = 1;
         edtavFilename_Jsonclick = "";
         edtavFilename_Visible = 1;
         edtavLote_nfearq_Filetype = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "NFe";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'CONFIRMAR'","{handler:'E12DV2',iparms:[{av:'AV11Lote_NFeArq',fld:'vLOTE_NFEARQ',pic:'',nv:''},{av:'AV5Lote_NFe',fld:'vLOTE_NFE',pic:'ZZZZZ9',nv:0},{av:'AV10Lote_DataNFe',fld:'vLOTE_DATANFE',pic:'',nv:''},{av:'AV12Lote_DataPagamento',fld:'vLOTE_DATAPAGAMENTO',pic:'',nv:''},{av:'AV14Lote_NFeDataProtocolo',fld:'vLOTE_NFEDATAPROTOCOLO',pic:'',nv:''},{av:'AV15FileName',fld:'vFILENAME',pic:'',nv:''},{av:'AV22Lote_Codigo',fld:'vLOTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'edtavLote_nfearq_Filetype',ctrl:'vLOTE_NFEARQ',prop:'Filetype'},{av:'A597ContagemResultado_LoteAceiteCod',fld:'CONTAGEMRESULTADO_LOTEACEITECOD',pic:'ZZZZZ9',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV16Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV18Lote_AreaTrabalhoCod',fld:'vLOTE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV21Lote_Nome',fld:'vLOTE_NOME',pic:'@!',nv:''}],oparms:[{av:'AV13Confirmado',fld:'vCONFIRMADO',pic:'',nv:false},{av:'AV15FileName',fld:'vFILENAME',pic:'',nv:''},{av:'AV22Lote_Codigo',fld:'vLOTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV16Codigos',fld:'vCODIGOS',pic:'',nv:null}]}");
         setEventMetadata("'CANCELAR'","{handler:'E13DV2',iparms:[{av:'AV13Confirmado',fld:'vCONFIRMADO',pic:'',nv:false},{av:'AV22Lote_Codigo',fld:'vLOTE_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV16Codigos = new GxSimpleCollection();
         AV21Lote_Nome = "";
         GXCCtlgxBlob = "";
         AV11Lote_NFeArq = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         AV15FileName = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV10Lote_DataNFe = DateTime.MinValue;
         AV14Lote_NFeDataProtocolo = DateTime.MinValue;
         AV12Lote_DataPagamento = DateTime.MinValue;
         scmdbuf = "";
         H00DV2_A596Lote_Codigo = new int[1] ;
         H00DV2_A855AreaTrabalho_DiasParaPagar = new short[1] ;
         H00DV2_n855AreaTrabalho_DiasParaPagar = new bool[] {false} ;
         H00DV2_A595Lote_AreaTrabalhoCod = new int[1] ;
         H00DV2_A563Lote_Nome = new String[] {""} ;
         A563Lote_Nome = "";
         GXt_dtime1 = (DateTime)(DateTime.MinValue);
         H00DV3_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         H00DV3_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         H00DV3_A456ContagemResultado_Codigo = new int[1] ;
         AV17WebSession = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblTextblock2_Jsonclick = "";
         lblTextblock3_Jsonclick = "";
         lblTextblock5_Jsonclick = "";
         lblTextblock6_Jsonclick = "";
         lblTextblock4_Jsonclick = "";
         gxblobfileaux = new GxFile(context.GetPhysicalPath());
         bttButton1_Jsonclick = "";
         bttButton2_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_lote_nfe__default(),
            new Object[][] {
                new Object[] {
               H00DV2_A596Lote_Codigo, H00DV2_A855AreaTrabalho_DiasParaPagar, H00DV2_n855AreaTrabalho_DiasParaPagar, H00DV2_A595Lote_AreaTrabalhoCod, H00DV2_A563Lote_Nome
               }
               , new Object[] {
               H00DV3_A597ContagemResultado_LoteAceiteCod, H00DV3_n597ContagemResultado_LoteAceiteCod, H00DV3_A456ContagemResultado_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short A855AreaTrabalho_DiasParaPagar ;
      private short nGXWrapped ;
      private int AV22Lote_Codigo ;
      private int wcpOAV22Lote_Codigo ;
      private int A597ContagemResultado_LoteAceiteCod ;
      private int A456ContagemResultado_Codigo ;
      private int AV18Lote_AreaTrabalhoCod ;
      private int edtavFilename_Visible ;
      private int AV5Lote_NFe ;
      private int A596Lote_Codigo ;
      private int A595Lote_AreaTrabalhoCod ;
      private int edtavLote_nfe_Enabled ;
      private int edtavLote_datanfe_Enabled ;
      private int edtavLote_nfearq_Enabled ;
      private int idxLst ;
      private String edtavLote_nfearq_Filetype ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String AV21Lote_Nome ;
      private String GXCCtlgxBlob ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String edtavFilename_Internalname ;
      private String AV15FileName ;
      private String edtavFilename_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavLote_nfe_Internalname ;
      private String edtavLote_datanfe_Internalname ;
      private String edtavLote_nfedataprotocolo_Internalname ;
      private String edtavLote_datapagamento_Internalname ;
      private String edtavLote_nfearq_Internalname ;
      private String scmdbuf ;
      private String A563Lote_Nome ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String lblTextblock2_Internalname ;
      private String lblTextblock2_Jsonclick ;
      private String edtavLote_nfe_Jsonclick ;
      private String lblTextblock3_Internalname ;
      private String lblTextblock3_Jsonclick ;
      private String edtavLote_datanfe_Jsonclick ;
      private String lblTextblock5_Internalname ;
      private String lblTextblock5_Jsonclick ;
      private String edtavLote_nfedataprotocolo_Jsonclick ;
      private String lblTextblock6_Internalname ;
      private String lblTextblock6_Jsonclick ;
      private String edtavLote_datapagamento_Jsonclick ;
      private String lblTextblock4_Internalname ;
      private String lblTextblock4_Jsonclick ;
      private String edtavLote_nfearq_Contenttype ;
      private String edtavLote_nfearq_Parameters ;
      private String edtavLote_nfearq_Jsonclick ;
      private String bttButton1_Internalname ;
      private String bttButton1_Jsonclick ;
      private String bttButton2_Internalname ;
      private String bttButton2_Jsonclick ;
      private DateTime GXt_dtime1 ;
      private DateTime AV10Lote_DataNFe ;
      private DateTime AV14Lote_NFeDataProtocolo ;
      private DateTime AV12Lote_DataPagamento ;
      private bool AV13Confirmado ;
      private bool wcpOAV13Confirmado ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool n855AreaTrabalho_DiasParaPagar ;
      private bool n597ContagemResultado_LoteAceiteCod ;
      private String AV11Lote_NFeArq ;
      private GxFile gxblobfileaux ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Lote_Codigo ;
      private bool aP1_Confirmado ;
      private IDataStoreProvider pr_default ;
      private int[] H00DV2_A596Lote_Codigo ;
      private short[] H00DV2_A855AreaTrabalho_DiasParaPagar ;
      private bool[] H00DV2_n855AreaTrabalho_DiasParaPagar ;
      private int[] H00DV2_A595Lote_AreaTrabalhoCod ;
      private String[] H00DV2_A563Lote_Nome ;
      private int[] H00DV3_A597ContagemResultado_LoteAceiteCod ;
      private bool[] H00DV3_n597ContagemResultado_LoteAceiteCod ;
      private int[] H00DV3_A456ContagemResultado_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private IGxSession AV17WebSession ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV16Codigos ;
      private GXWebForm Form ;
   }

   public class wp_lote_nfe__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00DV2 ;
          prmH00DV2 = new Object[] {
          new Object[] {"@AV22Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00DV3 ;
          prmH00DV3 = new Object[] {
          new Object[] {"@AV22Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00DV2", "SELECT TOP 1 T1.[Lote_Codigo], T2.[AreaTrabalho_DiasParaPagar], T1.[Lote_AreaTrabalhoCod] AS Lote_AreaTrabalhoCod, T1.[Lote_Nome] FROM ([Lote] T1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = T1.[Lote_AreaTrabalhoCod]) WHERE T1.[Lote_Codigo] = @AV22Lote_Codigo ORDER BY T1.[Lote_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DV2,1,0,false,true )
             ,new CursorDef("H00DV3", "SELECT [ContagemResultado_LoteAceiteCod], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_LoteAceiteCod] = @AV22Lote_Codigo ORDER BY [ContagemResultado_LoteAceiteCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DV3,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 50) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
