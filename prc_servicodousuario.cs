/*
               File: PRC_ServicoDoUsuario
        Description: Servico Do Usuario
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:12:47.30
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_servicodousuario : GXProcedure
   {
      public prc_servicodousuario( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_servicodousuario( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_UsuarioServicos_ServicoCod ,
                           ref int aP1_UsuarioServicos_UsuarioCod ,
                           int aP2_ContagemResultado_Codigo ,
                           int aP3_Contratada_Codigo ,
                           out int aP4_Servico_Codigo )
      {
         this.A829UsuarioServicos_ServicoCod = aP0_UsuarioServicos_ServicoCod;
         this.A828UsuarioServicos_UsuarioCod = aP1_UsuarioServicos_UsuarioCod;
         this.AV8ContagemResultado_Codigo = aP2_ContagemResultado_Codigo;
         this.AV9Contratada_Codigo = aP3_Contratada_Codigo;
         this.AV10Servico_Codigo = 0 ;
         initialize();
         executePrivate();
         aP0_UsuarioServicos_ServicoCod=this.A829UsuarioServicos_ServicoCod;
         aP1_UsuarioServicos_UsuarioCod=this.A828UsuarioServicos_UsuarioCod;
         aP4_Servico_Codigo=this.AV10Servico_Codigo;
      }

      public int executeUdp( ref int aP0_UsuarioServicos_ServicoCod ,
                             ref int aP1_UsuarioServicos_UsuarioCod ,
                             int aP2_ContagemResultado_Codigo ,
                             int aP3_Contratada_Codigo )
      {
         this.A829UsuarioServicos_ServicoCod = aP0_UsuarioServicos_ServicoCod;
         this.A828UsuarioServicos_UsuarioCod = aP1_UsuarioServicos_UsuarioCod;
         this.AV8ContagemResultado_Codigo = aP2_ContagemResultado_Codigo;
         this.AV9Contratada_Codigo = aP3_Contratada_Codigo;
         this.AV10Servico_Codigo = 0 ;
         initialize();
         executePrivate();
         aP0_UsuarioServicos_ServicoCod=this.A829UsuarioServicos_ServicoCod;
         aP1_UsuarioServicos_UsuarioCod=this.A828UsuarioServicos_UsuarioCod;
         aP4_Servico_Codigo=this.AV10Servico_Codigo;
         return AV10Servico_Codigo ;
      }

      public void executeSubmit( ref int aP0_UsuarioServicos_ServicoCod ,
                                 ref int aP1_UsuarioServicos_UsuarioCod ,
                                 int aP2_ContagemResultado_Codigo ,
                                 int aP3_Contratada_Codigo ,
                                 out int aP4_Servico_Codigo )
      {
         prc_servicodousuario objprc_servicodousuario;
         objprc_servicodousuario = new prc_servicodousuario();
         objprc_servicodousuario.A829UsuarioServicos_ServicoCod = aP0_UsuarioServicos_ServicoCod;
         objprc_servicodousuario.A828UsuarioServicos_UsuarioCod = aP1_UsuarioServicos_UsuarioCod;
         objprc_servicodousuario.AV8ContagemResultado_Codigo = aP2_ContagemResultado_Codigo;
         objprc_servicodousuario.AV9Contratada_Codigo = aP3_Contratada_Codigo;
         objprc_servicodousuario.AV10Servico_Codigo = 0 ;
         objprc_servicodousuario.context.SetSubmitInitialConfig(context);
         objprc_servicodousuario.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_servicodousuario);
         aP0_UsuarioServicos_ServicoCod=this.A829UsuarioServicos_ServicoCod;
         aP1_UsuarioServicos_UsuarioCod=this.A828UsuarioServicos_UsuarioCod;
         aP4_Servico_Codigo=this.AV10Servico_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_servicodousuario)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( (0==AV8ContagemResultado_Codigo) )
         {
            /* Execute user subroutine: 'SERVICOEHDOUSUARIOECONTRATADO' */
            S111 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else
         {
            AV13GXLvl5 = 0;
            /* Using cursor P009Q2 */
            pr_default.execute(0, new Object[] {AV8ContagemResultado_Codigo, A829UsuarioServicos_ServicoCod});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A1553ContagemResultado_CntSrvCod = P009Q2_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P009Q2_n1553ContagemResultado_CntSrvCod[0];
               A601ContagemResultado_Servico = P009Q2_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P009Q2_n601ContagemResultado_Servico[0];
               A456ContagemResultado_Codigo = P009Q2_A456ContagemResultado_Codigo[0];
               A601ContagemResultado_Servico = P009Q2_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P009Q2_n601ContagemResultado_Servico[0];
               AV13GXLvl5 = 1;
               AV10Servico_Codigo = A829UsuarioServicos_ServicoCod;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            if ( AV13GXLvl5 == 0 )
            {
               /* Execute user subroutine: 'SERVICOEHDOUSUARIOECONTRATADO' */
               S111 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
            }
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'SERVICOEHDOUSUARIOECONTRATADO' Routine */
         /* Using cursor P009Q3 */
         pr_default.execute(1, new Object[] {A828UsuarioServicos_UsuarioCod, A829UsuarioServicos_ServicoCod});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A832UsuarioServicos_ServicoAtivo = P009Q3_A832UsuarioServicos_ServicoAtivo[0];
            n832UsuarioServicos_ServicoAtivo = P009Q3_n832UsuarioServicos_ServicoAtivo[0];
            A832UsuarioServicos_ServicoAtivo = P009Q3_A832UsuarioServicos_ServicoAtivo[0];
            n832UsuarioServicos_ServicoAtivo = P009Q3_n832UsuarioServicos_ServicoAtivo[0];
            if ( (0==AV9Contratada_Codigo) )
            {
               AV10Servico_Codigo = A829UsuarioServicos_ServicoCod;
            }
            else
            {
               /* Using cursor P009Q4 */
               pr_default.execute(2, new Object[] {A829UsuarioServicos_ServicoCod, AV9Contratada_Codigo});
               while ( (pr_default.getStatus(2) != 101) )
               {
                  A74Contrato_Codigo = P009Q4_A74Contrato_Codigo[0];
                  A155Servico_Codigo = P009Q4_A155Servico_Codigo[0];
                  A92Contrato_Ativo = P009Q4_A92Contrato_Ativo[0];
                  A39Contratada_Codigo = P009Q4_A39Contratada_Codigo[0];
                  A160ContratoServicos_Codigo = P009Q4_A160ContratoServicos_Codigo[0];
                  A92Contrato_Ativo = P009Q4_A92Contrato_Ativo[0];
                  A39Contratada_Codigo = P009Q4_A39Contratada_Codigo[0];
                  AV10Servico_Codigo = A829UsuarioServicos_ServicoCod;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  pr_default.readNext(2);
               }
               pr_default.close(2);
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P009Q2_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P009Q2_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P009Q2_A601ContagemResultado_Servico = new int[1] ;
         P009Q2_n601ContagemResultado_Servico = new bool[] {false} ;
         P009Q2_A456ContagemResultado_Codigo = new int[1] ;
         P009Q3_A828UsuarioServicos_UsuarioCod = new int[1] ;
         P009Q3_A829UsuarioServicos_ServicoCod = new int[1] ;
         P009Q3_A832UsuarioServicos_ServicoAtivo = new bool[] {false} ;
         P009Q3_n832UsuarioServicos_ServicoAtivo = new bool[] {false} ;
         P009Q4_A74Contrato_Codigo = new int[1] ;
         P009Q4_A155Servico_Codigo = new int[1] ;
         P009Q4_A92Contrato_Ativo = new bool[] {false} ;
         P009Q4_A39Contratada_Codigo = new int[1] ;
         P009Q4_A160ContratoServicos_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_servicodousuario__default(),
            new Object[][] {
                new Object[] {
               P009Q2_A1553ContagemResultado_CntSrvCod, P009Q2_n1553ContagemResultado_CntSrvCod, P009Q2_A601ContagemResultado_Servico, P009Q2_n601ContagemResultado_Servico, P009Q2_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P009Q3_A828UsuarioServicos_UsuarioCod, P009Q3_A829UsuarioServicos_ServicoCod, P009Q3_A832UsuarioServicos_ServicoAtivo, P009Q3_n832UsuarioServicos_ServicoAtivo
               }
               , new Object[] {
               P009Q4_A74Contrato_Codigo, P009Q4_A155Servico_Codigo, P009Q4_A92Contrato_Ativo, P009Q4_A39Contratada_Codigo, P009Q4_A160ContratoServicos_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV13GXLvl5 ;
      private int A829UsuarioServicos_ServicoCod ;
      private int A828UsuarioServicos_UsuarioCod ;
      private int AV8ContagemResultado_Codigo ;
      private int AV9Contratada_Codigo ;
      private int AV10Servico_Codigo ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A601ContagemResultado_Servico ;
      private int A456ContagemResultado_Codigo ;
      private int A74Contrato_Codigo ;
      private int A155Servico_Codigo ;
      private int A39Contratada_Codigo ;
      private int A160ContratoServicos_Codigo ;
      private String scmdbuf ;
      private bool returnInSub ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n601ContagemResultado_Servico ;
      private bool A832UsuarioServicos_ServicoAtivo ;
      private bool n832UsuarioServicos_ServicoAtivo ;
      private bool A92Contrato_Ativo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_UsuarioServicos_ServicoCod ;
      private int aP1_UsuarioServicos_UsuarioCod ;
      private IDataStoreProvider pr_default ;
      private int[] P009Q2_A1553ContagemResultado_CntSrvCod ;
      private bool[] P009Q2_n1553ContagemResultado_CntSrvCod ;
      private int[] P009Q2_A601ContagemResultado_Servico ;
      private bool[] P009Q2_n601ContagemResultado_Servico ;
      private int[] P009Q2_A456ContagemResultado_Codigo ;
      private int[] P009Q3_A828UsuarioServicos_UsuarioCod ;
      private int[] P009Q3_A829UsuarioServicos_ServicoCod ;
      private bool[] P009Q3_A832UsuarioServicos_ServicoAtivo ;
      private bool[] P009Q3_n832UsuarioServicos_ServicoAtivo ;
      private int[] P009Q4_A74Contrato_Codigo ;
      private int[] P009Q4_A155Servico_Codigo ;
      private bool[] P009Q4_A92Contrato_Ativo ;
      private int[] P009Q4_A39Contratada_Codigo ;
      private int[] P009Q4_A160ContratoServicos_Codigo ;
      private int aP4_Servico_Codigo ;
   }

   public class prc_servicodousuario__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP009Q2 ;
          prmP009Q2 = new Object[] {
          new Object[] {"@AV8ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@UsuarioServicos_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009Q3 ;
          prmP009Q3 = new Object[] {
          new Object[] {"@UsuarioServicos_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@UsuarioServicos_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009Q4 ;
          prmP009Q4 = new Object[] {
          new Object[] {"@UsuarioServicos_ServicoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P009Q2", "SELECT TOP 1 T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_Codigo] FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) WHERE (T1.[ContagemResultado_Codigo] = @AV8ContagemResultado_Codigo) AND (T2.[Servico_Codigo] = @UsuarioServicos_ServicoCod) ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009Q2,1,0,false,true )
             ,new CursorDef("P009Q3", "SELECT TOP 1 T1.[UsuarioServicos_UsuarioCod], T1.[UsuarioServicos_ServicoCod] AS UsuarioServicos_ServicoCod, T2.[Servico_Ativo] AS UsuarioServicos_ServicoAtivo FROM ([UsuarioServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[UsuarioServicos_ServicoCod]) WHERE (T1.[UsuarioServicos_UsuarioCod] = @UsuarioServicos_UsuarioCod and T1.[UsuarioServicos_ServicoCod] = @UsuarioServicos_ServicoCod) AND (T2.[Servico_Ativo] = 1) ORDER BY T1.[UsuarioServicos_UsuarioCod], T1.[UsuarioServicos_ServicoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009Q3,1,0,true,true )
             ,new CursorDef("P009Q4", "SELECT TOP 1 T1.[Contrato_Codigo], T1.[Servico_Codigo], T2.[Contrato_Ativo], T2.[Contratada_Codigo], T1.[ContratoServicos_Codigo] FROM ([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE (T1.[Servico_Codigo] = @UsuarioServicos_ServicoCod) AND (T2.[Contrato_Ativo] = 1) AND (T2.[Contratada_Codigo] = @AV9Contratada_Codigo) ORDER BY T1.[Servico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009Q4,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
