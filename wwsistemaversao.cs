/*
               File: WWSistemaVersao
        Description:  Versionamento
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 13:38:13.30
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwsistemaversao : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwsistemaversao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwsistemaversao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_88 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_88_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_88_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17SistemaVersao_Id1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17SistemaVersao_Id1", AV17SistemaVersao_Id1);
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
               AV21SistemaVersao_Id2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21SistemaVersao_Id2", AV21SistemaVersao_Id2);
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
               AV25SistemaVersao_Id3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25SistemaVersao_Id3", AV25SistemaVersao_Id3);
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV38TFSistemaVersao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFSistemaVersao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFSistemaVersao_Codigo), 6, 0)));
               AV39TFSistemaVersao_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFSistemaVersao_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFSistemaVersao_Codigo_To), 6, 0)));
               AV42TFSistemaVersao_Id = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFSistemaVersao_Id", AV42TFSistemaVersao_Id);
               AV43TFSistemaVersao_Id_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFSistemaVersao_Id_Sel", AV43TFSistemaVersao_Id_Sel);
               AV46TFSistemaVersao_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFSistemaVersao_Descricao", AV46TFSistemaVersao_Descricao);
               AV47TFSistemaVersao_Descricao_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFSistemaVersao_Descricao_Sel", AV47TFSistemaVersao_Descricao_Sel);
               AV50TFSistemaVersao_Evolucao = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFSistemaVersao_Evolucao", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50TFSistemaVersao_Evolucao), 4, 0)));
               AV51TFSistemaVersao_Evolucao_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFSistemaVersao_Evolucao_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFSistemaVersao_Evolucao_To), 4, 0)));
               AV54TFSistemaVersao_Melhoria = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFSistemaVersao_Melhoria", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54TFSistemaVersao_Melhoria), 4, 0)));
               AV55TFSistemaVersao_Melhoria_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFSistemaVersao_Melhoria_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFSistemaVersao_Melhoria_To), 4, 0)));
               AV58TFSistemaVersao_Correcao = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFSistemaVersao_Correcao", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58TFSistemaVersao_Correcao), 8, 0)));
               AV59TFSistemaVersao_Correcao_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFSistemaVersao_Correcao_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59TFSistemaVersao_Correcao_To), 8, 0)));
               AV62TFSistemaVersao_Data = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFSistemaVersao_Data", context.localUtil.TToC( AV62TFSistemaVersao_Data, 8, 5, 0, 3, "/", ":", " "));
               AV63TFSistemaVersao_Data_To = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFSistemaVersao_Data_To", context.localUtil.TToC( AV63TFSistemaVersao_Data_To, 8, 5, 0, 3, "/", ":", " "));
               AV31ManageFiltersExecutionStep = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV31ManageFiltersExecutionStep), 1, 0));
               AV40ddo_SistemaVersao_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ddo_SistemaVersao_CodigoTitleControlIdToReplace", AV40ddo_SistemaVersao_CodigoTitleControlIdToReplace);
               AV44ddo_SistemaVersao_IdTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ddo_SistemaVersao_IdTitleControlIdToReplace", AV44ddo_SistemaVersao_IdTitleControlIdToReplace);
               AV48ddo_SistemaVersao_DescricaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ddo_SistemaVersao_DescricaoTitleControlIdToReplace", AV48ddo_SistemaVersao_DescricaoTitleControlIdToReplace);
               AV52ddo_SistemaVersao_EvolucaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ddo_SistemaVersao_EvolucaoTitleControlIdToReplace", AV52ddo_SistemaVersao_EvolucaoTitleControlIdToReplace);
               AV56ddo_SistemaVersao_MelhoriaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ddo_SistemaVersao_MelhoriaTitleControlIdToReplace", AV56ddo_SistemaVersao_MelhoriaTitleControlIdToReplace);
               AV60ddo_SistemaVersao_CorrecaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60ddo_SistemaVersao_CorrecaoTitleControlIdToReplace", AV60ddo_SistemaVersao_CorrecaoTitleControlIdToReplace);
               AV66ddo_SistemaVersao_DataTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ddo_SistemaVersao_DataTitleControlIdToReplace", AV66ddo_SistemaVersao_DataTitleControlIdToReplace);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               AV101Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               A1859SistemaVersao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV71SistemaVersao_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71SistemaVersao_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV71SistemaVersao_SistemaCod), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17SistemaVersao_Id1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21SistemaVersao_Id2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25SistemaVersao_Id3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV38TFSistemaVersao_Codigo, AV39TFSistemaVersao_Codigo_To, AV42TFSistemaVersao_Id, AV43TFSistemaVersao_Id_Sel, AV46TFSistemaVersao_Descricao, AV47TFSistemaVersao_Descricao_Sel, AV50TFSistemaVersao_Evolucao, AV51TFSistemaVersao_Evolucao_To, AV54TFSistemaVersao_Melhoria, AV55TFSistemaVersao_Melhoria_To, AV58TFSistemaVersao_Correcao, AV59TFSistemaVersao_Correcao_To, AV62TFSistemaVersao_Data, AV63TFSistemaVersao_Data_To, AV31ManageFiltersExecutionStep, AV40ddo_SistemaVersao_CodigoTitleControlIdToReplace, AV44ddo_SistemaVersao_IdTitleControlIdToReplace, AV48ddo_SistemaVersao_DescricaoTitleControlIdToReplace, AV52ddo_SistemaVersao_EvolucaoTitleControlIdToReplace, AV56ddo_SistemaVersao_MelhoriaTitleControlIdToReplace, AV60ddo_SistemaVersao_CorrecaoTitleControlIdToReplace, AV66ddo_SistemaVersao_DataTitleControlIdToReplace, AV6WWPContext, AV101Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1859SistemaVersao_Codigo, AV71SistemaVersao_SistemaCod) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAOE2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTOE2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202051813381469");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwsistemaversao.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vSISTEMAVERSAO_ID1", StringUtil.RTrim( AV17SistemaVersao_Id1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vSISTEMAVERSAO_ID2", StringUtil.RTrim( AV21SistemaVersao_Id2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vSISTEMAVERSAO_ID3", StringUtil.RTrim( AV25SistemaVersao_Id3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSISTEMAVERSAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38TFSistemaVersao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSISTEMAVERSAO_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV39TFSistemaVersao_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSISTEMAVERSAO_ID", StringUtil.RTrim( AV42TFSistemaVersao_Id));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSISTEMAVERSAO_ID_SEL", StringUtil.RTrim( AV43TFSistemaVersao_Id_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSISTEMAVERSAO_DESCRICAO", AV46TFSistemaVersao_Descricao);
         GxWebStd.gx_hidden_field( context, "GXH_vTFSISTEMAVERSAO_DESCRICAO_SEL", AV47TFSistemaVersao_Descricao_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFSISTEMAVERSAO_EVOLUCAO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV50TFSistemaVersao_Evolucao), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSISTEMAVERSAO_EVOLUCAO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV51TFSistemaVersao_Evolucao_To), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSISTEMAVERSAO_MELHORIA", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV54TFSistemaVersao_Melhoria), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSISTEMAVERSAO_MELHORIA_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV55TFSistemaVersao_Melhoria_To), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSISTEMAVERSAO_CORRECAO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV58TFSistemaVersao_Correcao), 8, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSISTEMAVERSAO_CORRECAO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV59TFSistemaVersao_Correcao_To), 8, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSISTEMAVERSAO_DATA", context.localUtil.TToC( AV62TFSistemaVersao_Data, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSISTEMAVERSAO_DATA_TO", context.localUtil.TToC( AV63TFSistemaVersao_Data_To, 10, 8, 0, 3, "/", ":", " "));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_88", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_88), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMANAGEFILTERSDATA", AV35ManageFiltersData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMANAGEFILTERSDATA", AV35ManageFiltersData);
         }
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV69GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV70GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV67DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV67DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSISTEMAVERSAO_CODIGOTITLEFILTERDATA", AV37SistemaVersao_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSISTEMAVERSAO_CODIGOTITLEFILTERDATA", AV37SistemaVersao_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSISTEMAVERSAO_IDTITLEFILTERDATA", AV41SistemaVersao_IdTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSISTEMAVERSAO_IDTITLEFILTERDATA", AV41SistemaVersao_IdTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSISTEMAVERSAO_DESCRICAOTITLEFILTERDATA", AV45SistemaVersao_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSISTEMAVERSAO_DESCRICAOTITLEFILTERDATA", AV45SistemaVersao_DescricaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSISTEMAVERSAO_EVOLUCAOTITLEFILTERDATA", AV49SistemaVersao_EvolucaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSISTEMAVERSAO_EVOLUCAOTITLEFILTERDATA", AV49SistemaVersao_EvolucaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSISTEMAVERSAO_MELHORIATITLEFILTERDATA", AV53SistemaVersao_MelhoriaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSISTEMAVERSAO_MELHORIATITLEFILTERDATA", AV53SistemaVersao_MelhoriaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSISTEMAVERSAO_CORRECAOTITLEFILTERDATA", AV57SistemaVersao_CorrecaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSISTEMAVERSAO_CORRECAOTITLEFILTERDATA", AV57SistemaVersao_CorrecaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSISTEMAVERSAO_DATATITLEFILTERDATA", AV61SistemaVersao_DataTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSISTEMAVERSAO_DATATITLEFILTERDATA", AV61SistemaVersao_DataTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV101Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vSISTEMAVERSAO_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV71SistemaVersao_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Icon", StringUtil.RTrim( Ddo_managefilters_Icon));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Caption", StringUtil.RTrim( Ddo_managefilters_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Tooltip", StringUtil.RTrim( Ddo_managefilters_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Cls", StringUtil.RTrim( Ddo_managefilters_Cls));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CODIGO_Caption", StringUtil.RTrim( Ddo_sistemaversao_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CODIGO_Tooltip", StringUtil.RTrim( Ddo_sistemaversao_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CODIGO_Cls", StringUtil.RTrim( Ddo_sistemaversao_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_sistemaversao_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_sistemaversao_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_sistemaversao_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_sistemaversao_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_sistemaversao_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_sistemaversao_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_sistemaversao_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_sistemaversao_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CODIGO_Filtertype", StringUtil.RTrim( Ddo_sistemaversao_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_sistemaversao_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_sistemaversao_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CODIGO_Sortasc", StringUtil.RTrim( Ddo_sistemaversao_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_sistemaversao_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_sistemaversao_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_sistemaversao_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_sistemaversao_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_sistemaversao_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Caption", StringUtil.RTrim( Ddo_sistemaversao_id_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Tooltip", StringUtil.RTrim( Ddo_sistemaversao_id_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Cls", StringUtil.RTrim( Ddo_sistemaversao_id_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Filteredtext_set", StringUtil.RTrim( Ddo_sistemaversao_id_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Selectedvalue_set", StringUtil.RTrim( Ddo_sistemaversao_id_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Dropdownoptionstype", StringUtil.RTrim( Ddo_sistemaversao_id_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_sistemaversao_id_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Includesortasc", StringUtil.BoolToStr( Ddo_sistemaversao_id_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Includesortdsc", StringUtil.BoolToStr( Ddo_sistemaversao_id_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Sortedstatus", StringUtil.RTrim( Ddo_sistemaversao_id_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Includefilter", StringUtil.BoolToStr( Ddo_sistemaversao_id_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Filtertype", StringUtil.RTrim( Ddo_sistemaversao_id_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Filterisrange", StringUtil.BoolToStr( Ddo_sistemaversao_id_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Includedatalist", StringUtil.BoolToStr( Ddo_sistemaversao_id_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Datalisttype", StringUtil.RTrim( Ddo_sistemaversao_id_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Datalistproc", StringUtil.RTrim( Ddo_sistemaversao_id_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_sistemaversao_id_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Sortasc", StringUtil.RTrim( Ddo_sistemaversao_id_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Sortdsc", StringUtil.RTrim( Ddo_sistemaversao_id_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Loadingdata", StringUtil.RTrim( Ddo_sistemaversao_id_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Cleanfilter", StringUtil.RTrim( Ddo_sistemaversao_id_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Noresultsfound", StringUtil.RTrim( Ddo_sistemaversao_id_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Searchbuttontext", StringUtil.RTrim( Ddo_sistemaversao_id_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DESCRICAO_Caption", StringUtil.RTrim( Ddo_sistemaversao_descricao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_sistemaversao_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DESCRICAO_Cls", StringUtil.RTrim( Ddo_sistemaversao_descricao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_sistemaversao_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_sistemaversao_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_sistemaversao_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_sistemaversao_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_sistemaversao_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_sistemaversao_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_sistemaversao_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_sistemaversao_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_sistemaversao_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_sistemaversao_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_sistemaversao_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_sistemaversao_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_sistemaversao_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_sistemaversao_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_sistemaversao_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_sistemaversao_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_sistemaversao_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_sistemaversao_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_sistemaversao_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_sistemaversao_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_EVOLUCAO_Caption", StringUtil.RTrim( Ddo_sistemaversao_evolucao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_EVOLUCAO_Tooltip", StringUtil.RTrim( Ddo_sistemaversao_evolucao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_EVOLUCAO_Cls", StringUtil.RTrim( Ddo_sistemaversao_evolucao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_EVOLUCAO_Filteredtext_set", StringUtil.RTrim( Ddo_sistemaversao_evolucao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_EVOLUCAO_Filteredtextto_set", StringUtil.RTrim( Ddo_sistemaversao_evolucao_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_EVOLUCAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_sistemaversao_evolucao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_EVOLUCAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_sistemaversao_evolucao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_EVOLUCAO_Includesortasc", StringUtil.BoolToStr( Ddo_sistemaversao_evolucao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_EVOLUCAO_Includesortdsc", StringUtil.BoolToStr( Ddo_sistemaversao_evolucao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_EVOLUCAO_Sortedstatus", StringUtil.RTrim( Ddo_sistemaversao_evolucao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_EVOLUCAO_Includefilter", StringUtil.BoolToStr( Ddo_sistemaversao_evolucao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_EVOLUCAO_Filtertype", StringUtil.RTrim( Ddo_sistemaversao_evolucao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_EVOLUCAO_Filterisrange", StringUtil.BoolToStr( Ddo_sistemaversao_evolucao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_EVOLUCAO_Includedatalist", StringUtil.BoolToStr( Ddo_sistemaversao_evolucao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_EVOLUCAO_Sortasc", StringUtil.RTrim( Ddo_sistemaversao_evolucao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_EVOLUCAO_Sortdsc", StringUtil.RTrim( Ddo_sistemaversao_evolucao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_EVOLUCAO_Cleanfilter", StringUtil.RTrim( Ddo_sistemaversao_evolucao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_EVOLUCAO_Rangefilterfrom", StringUtil.RTrim( Ddo_sistemaversao_evolucao_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_EVOLUCAO_Rangefilterto", StringUtil.RTrim( Ddo_sistemaversao_evolucao_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_EVOLUCAO_Searchbuttontext", StringUtil.RTrim( Ddo_sistemaversao_evolucao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_MELHORIA_Caption", StringUtil.RTrim( Ddo_sistemaversao_melhoria_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_MELHORIA_Tooltip", StringUtil.RTrim( Ddo_sistemaversao_melhoria_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_MELHORIA_Cls", StringUtil.RTrim( Ddo_sistemaversao_melhoria_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_MELHORIA_Filteredtext_set", StringUtil.RTrim( Ddo_sistemaversao_melhoria_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_MELHORIA_Filteredtextto_set", StringUtil.RTrim( Ddo_sistemaversao_melhoria_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_MELHORIA_Dropdownoptionstype", StringUtil.RTrim( Ddo_sistemaversao_melhoria_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_MELHORIA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_sistemaversao_melhoria_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_MELHORIA_Includesortasc", StringUtil.BoolToStr( Ddo_sistemaversao_melhoria_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_MELHORIA_Includesortdsc", StringUtil.BoolToStr( Ddo_sistemaversao_melhoria_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_MELHORIA_Sortedstatus", StringUtil.RTrim( Ddo_sistemaversao_melhoria_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_MELHORIA_Includefilter", StringUtil.BoolToStr( Ddo_sistemaversao_melhoria_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_MELHORIA_Filtertype", StringUtil.RTrim( Ddo_sistemaversao_melhoria_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_MELHORIA_Filterisrange", StringUtil.BoolToStr( Ddo_sistemaversao_melhoria_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_MELHORIA_Includedatalist", StringUtil.BoolToStr( Ddo_sistemaversao_melhoria_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_MELHORIA_Sortasc", StringUtil.RTrim( Ddo_sistemaversao_melhoria_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_MELHORIA_Sortdsc", StringUtil.RTrim( Ddo_sistemaversao_melhoria_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_MELHORIA_Cleanfilter", StringUtil.RTrim( Ddo_sistemaversao_melhoria_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_MELHORIA_Rangefilterfrom", StringUtil.RTrim( Ddo_sistemaversao_melhoria_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_MELHORIA_Rangefilterto", StringUtil.RTrim( Ddo_sistemaversao_melhoria_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_MELHORIA_Searchbuttontext", StringUtil.RTrim( Ddo_sistemaversao_melhoria_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CORRECAO_Caption", StringUtil.RTrim( Ddo_sistemaversao_correcao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CORRECAO_Tooltip", StringUtil.RTrim( Ddo_sistemaversao_correcao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CORRECAO_Cls", StringUtil.RTrim( Ddo_sistemaversao_correcao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CORRECAO_Filteredtext_set", StringUtil.RTrim( Ddo_sistemaversao_correcao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CORRECAO_Filteredtextto_set", StringUtil.RTrim( Ddo_sistemaversao_correcao_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CORRECAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_sistemaversao_correcao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CORRECAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_sistemaversao_correcao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CORRECAO_Includesortasc", StringUtil.BoolToStr( Ddo_sistemaversao_correcao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CORRECAO_Includesortdsc", StringUtil.BoolToStr( Ddo_sistemaversao_correcao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CORRECAO_Sortedstatus", StringUtil.RTrim( Ddo_sistemaversao_correcao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CORRECAO_Includefilter", StringUtil.BoolToStr( Ddo_sistemaversao_correcao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CORRECAO_Filtertype", StringUtil.RTrim( Ddo_sistemaversao_correcao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CORRECAO_Filterisrange", StringUtil.BoolToStr( Ddo_sistemaversao_correcao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CORRECAO_Includedatalist", StringUtil.BoolToStr( Ddo_sistemaversao_correcao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CORRECAO_Sortasc", StringUtil.RTrim( Ddo_sistemaversao_correcao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CORRECAO_Sortdsc", StringUtil.RTrim( Ddo_sistemaversao_correcao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CORRECAO_Cleanfilter", StringUtil.RTrim( Ddo_sistemaversao_correcao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CORRECAO_Rangefilterfrom", StringUtil.RTrim( Ddo_sistemaversao_correcao_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CORRECAO_Rangefilterto", StringUtil.RTrim( Ddo_sistemaversao_correcao_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CORRECAO_Searchbuttontext", StringUtil.RTrim( Ddo_sistemaversao_correcao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DATA_Caption", StringUtil.RTrim( Ddo_sistemaversao_data_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DATA_Tooltip", StringUtil.RTrim( Ddo_sistemaversao_data_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DATA_Cls", StringUtil.RTrim( Ddo_sistemaversao_data_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DATA_Filteredtext_set", StringUtil.RTrim( Ddo_sistemaversao_data_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DATA_Filteredtextto_set", StringUtil.RTrim( Ddo_sistemaversao_data_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DATA_Dropdownoptionstype", StringUtil.RTrim( Ddo_sistemaversao_data_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DATA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_sistemaversao_data_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DATA_Includesortasc", StringUtil.BoolToStr( Ddo_sistemaversao_data_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DATA_Includesortdsc", StringUtil.BoolToStr( Ddo_sistemaversao_data_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DATA_Sortedstatus", StringUtil.RTrim( Ddo_sistemaversao_data_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DATA_Includefilter", StringUtil.BoolToStr( Ddo_sistemaversao_data_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DATA_Filtertype", StringUtil.RTrim( Ddo_sistemaversao_data_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DATA_Filterisrange", StringUtil.BoolToStr( Ddo_sistemaversao_data_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DATA_Includedatalist", StringUtil.BoolToStr( Ddo_sistemaversao_data_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DATA_Sortasc", StringUtil.RTrim( Ddo_sistemaversao_data_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DATA_Sortdsc", StringUtil.RTrim( Ddo_sistemaversao_data_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DATA_Cleanfilter", StringUtil.RTrim( Ddo_sistemaversao_data_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DATA_Rangefilterfrom", StringUtil.RTrim( Ddo_sistemaversao_data_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DATA_Rangefilterto", StringUtil.RTrim( Ddo_sistemaversao_data_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DATA_Searchbuttontext", StringUtil.RTrim( Ddo_sistemaversao_data_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_sistemaversao_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_sistemaversao_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_sistemaversao_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Activeeventkey", StringUtil.RTrim( Ddo_sistemaversao_id_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Filteredtext_get", StringUtil.RTrim( Ddo_sistemaversao_id_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Selectedvalue_get", StringUtil.RTrim( Ddo_sistemaversao_id_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_sistemaversao_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_sistemaversao_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_sistemaversao_descricao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_EVOLUCAO_Activeeventkey", StringUtil.RTrim( Ddo_sistemaversao_evolucao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_EVOLUCAO_Filteredtext_get", StringUtil.RTrim( Ddo_sistemaversao_evolucao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_EVOLUCAO_Filteredtextto_get", StringUtil.RTrim( Ddo_sistemaversao_evolucao_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_MELHORIA_Activeeventkey", StringUtil.RTrim( Ddo_sistemaversao_melhoria_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_MELHORIA_Filteredtext_get", StringUtil.RTrim( Ddo_sistemaversao_melhoria_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_MELHORIA_Filteredtextto_get", StringUtil.RTrim( Ddo_sistemaversao_melhoria_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CORRECAO_Activeeventkey", StringUtil.RTrim( Ddo_sistemaversao_correcao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CORRECAO_Filteredtext_get", StringUtil.RTrim( Ddo_sistemaversao_correcao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_CORRECAO_Filteredtextto_get", StringUtil.RTrim( Ddo_sistemaversao_correcao_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DATA_Activeeventkey", StringUtil.RTrim( Ddo_sistemaversao_data_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DATA_Filteredtext_get", StringUtil.RTrim( Ddo_sistemaversao_data_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_DATA_Filteredtextto_get", StringUtil.RTrim( Ddo_sistemaversao_data_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Activeeventkey", StringUtil.RTrim( Ddo_managefilters_Activeeventkey));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEOE2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTOE2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwsistemaversao.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWSistemaVersao" ;
      }

      public override String GetPgmdesc( )
      {
         return " Versionamento" ;
      }

      protected void WBOE0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_OE2( true) ;
         }
         else
         {
            wb_table1_2_OE2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_OE2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(102, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,102);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(103, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,103);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavManagefiltersexecutionstep_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31ManageFiltersExecutionStep), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV31ManageFiltersExecutionStep), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,104);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavManagefiltersexecutionstep_Jsonclick, 0, "Attribute", "", "", "", edtavManagefiltersexecutionstep_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSistemaVersao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsistemaversao_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38TFSistemaVersao_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV38TFSistemaVersao_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,105);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsistemaversao_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfsistemaversao_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSistemaVersao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsistemaversao_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV39TFSistemaVersao_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV39TFSistemaVersao_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,106);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsistemaversao_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfsistemaversao_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSistemaVersao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsistemaversao_id_Internalname, StringUtil.RTrim( AV42TFSistemaVersao_Id), StringUtil.RTrim( context.localUtil.Format( AV42TFSistemaVersao_Id, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,107);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsistemaversao_id_Jsonclick, 0, "Attribute", "", "", "", edtavTfsistemaversao_id_Visible, 1, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWSistemaVersao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsistemaversao_id_sel_Internalname, StringUtil.RTrim( AV43TFSistemaVersao_Id_Sel), StringUtil.RTrim( context.localUtil.Format( AV43TFSistemaVersao_Id_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,108);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsistemaversao_id_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfsistemaversao_id_sel_Visible, 1, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWSistemaVersao.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfsistemaversao_descricao_Internalname, AV46TFSistemaVersao_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,109);\"", 0, edtavTfsistemaversao_descricao_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWSistemaVersao.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfsistemaversao_descricao_sel_Internalname, AV47TFSistemaVersao_Descricao_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,110);\"", 0, edtavTfsistemaversao_descricao_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWSistemaVersao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsistemaversao_evolucao_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV50TFSistemaVersao_Evolucao), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV50TFSistemaVersao_Evolucao), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,111);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsistemaversao_evolucao_Jsonclick, 0, "Attribute", "", "", "", edtavTfsistemaversao_evolucao_Visible, 1, 0, "text", "", 80, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSistemaVersao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsistemaversao_evolucao_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV51TFSistemaVersao_Evolucao_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV51TFSistemaVersao_Evolucao_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,112);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsistemaversao_evolucao_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfsistemaversao_evolucao_to_Visible, 1, 0, "text", "", 80, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSistemaVersao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsistemaversao_melhoria_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV54TFSistemaVersao_Melhoria), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV54TFSistemaVersao_Melhoria), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,113);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsistemaversao_melhoria_Jsonclick, 0, "Attribute", "", "", "", edtavTfsistemaversao_melhoria_Visible, 1, 0, "text", "", 80, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSistemaVersao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsistemaversao_melhoria_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV55TFSistemaVersao_Melhoria_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV55TFSistemaVersao_Melhoria_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,114);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsistemaversao_melhoria_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfsistemaversao_melhoria_to_Visible, 1, 0, "text", "", 80, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSistemaVersao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsistemaversao_correcao_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV58TFSistemaVersao_Correcao), 8, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV58TFSistemaVersao_Correcao), "ZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,115);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsistemaversao_correcao_Jsonclick, 0, "Attribute", "", "", "", edtavTfsistemaversao_correcao_Visible, 1, 0, "text", "", 80, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSistemaVersao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsistemaversao_correcao_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV59TFSistemaVersao_Correcao_To), 8, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV59TFSistemaVersao_Correcao_To), "ZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,116);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsistemaversao_correcao_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfsistemaversao_correcao_to_Visible, 1, 0, "text", "", 80, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSistemaVersao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'" + sGXsfl_88_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfsistemaversao_data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfsistemaversao_data_Internalname, context.localUtil.TToC( AV62TFSistemaVersao_Data, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV62TFSistemaVersao_Data, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,117);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsistemaversao_data_Jsonclick, 0, "Attribute", "", "", "", edtavTfsistemaversao_data_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSistemaVersao.htm");
            GxWebStd.gx_bitmap( context, edtavTfsistemaversao_data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfsistemaversao_data_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWSistemaVersao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'" + sGXsfl_88_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfsistemaversao_data_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfsistemaversao_data_to_Internalname, context.localUtil.TToC( AV63TFSistemaVersao_Data_To, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV63TFSistemaVersao_Data_To, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,118);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsistemaversao_data_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfsistemaversao_data_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSistemaVersao.htm");
            GxWebStd.gx_bitmap( context, edtavTfsistemaversao_data_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfsistemaversao_data_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWSistemaVersao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_sistemaversao_dataauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 120,'',false,'" + sGXsfl_88_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_sistemaversao_dataauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_sistemaversao_dataauxdate_Internalname, context.localUtil.Format(AV64DDO_SistemaVersao_DataAuxDate, "99/99/99"), context.localUtil.Format( AV64DDO_SistemaVersao_DataAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,120);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_sistemaversao_dataauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSistemaVersao.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_sistemaversao_dataauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWSistemaVersao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'',false,'" + sGXsfl_88_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_sistemaversao_dataauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_sistemaversao_dataauxdateto_Internalname, context.localUtil.Format(AV65DDO_SistemaVersao_DataAuxDateTo, "99/99/99"), context.localUtil.Format( AV65DDO_SistemaVersao_DataAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,121);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_sistemaversao_dataauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSistemaVersao.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_sistemaversao_dataauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWSistemaVersao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SISTEMAVERSAO_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 123,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_sistemaversao_codigotitlecontrolidtoreplace_Internalname, AV40ddo_SistemaVersao_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,123);\"", 0, edtavDdo_sistemaversao_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWSistemaVersao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SISTEMAVERSAO_IDContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 125,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_sistemaversao_idtitlecontrolidtoreplace_Internalname, AV44ddo_SistemaVersao_IdTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,125);\"", 0, edtavDdo_sistemaversao_idtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWSistemaVersao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SISTEMAVERSAO_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 127,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_sistemaversao_descricaotitlecontrolidtoreplace_Internalname, AV48ddo_SistemaVersao_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,127);\"", 0, edtavDdo_sistemaversao_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWSistemaVersao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SISTEMAVERSAO_EVOLUCAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 129,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_sistemaversao_evolucaotitlecontrolidtoreplace_Internalname, AV52ddo_SistemaVersao_EvolucaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,129);\"", 0, edtavDdo_sistemaversao_evolucaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWSistemaVersao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SISTEMAVERSAO_MELHORIAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 131,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_sistemaversao_melhoriatitlecontrolidtoreplace_Internalname, AV56ddo_SistemaVersao_MelhoriaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,131);\"", 0, edtavDdo_sistemaversao_melhoriatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWSistemaVersao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SISTEMAVERSAO_CORRECAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 133,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_sistemaversao_correcaotitlecontrolidtoreplace_Internalname, AV60ddo_SistemaVersao_CorrecaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,133);\"", 0, edtavDdo_sistemaversao_correcaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWSistemaVersao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SISTEMAVERSAO_DATAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 135,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_sistemaversao_datatitlecontrolidtoreplace_Internalname, AV66ddo_SistemaVersao_DataTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,135);\"", 0, edtavDdo_sistemaversao_datatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWSistemaVersao.htm");
         }
         wbLoad = true;
      }

      protected void STARTOE2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Versionamento", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPOE0( ) ;
      }

      protected void WSOE2( )
      {
         STARTOE2( ) ;
         EVTOE2( ) ;
      }

      protected void EVTOE2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_MANAGEFILTERS.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11OE2 */
                              E11OE2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12OE2 */
                              E12OE2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SISTEMAVERSAO_CODIGO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13OE2 */
                              E13OE2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SISTEMAVERSAO_ID.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14OE2 */
                              E14OE2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SISTEMAVERSAO_DESCRICAO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15OE2 */
                              E15OE2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SISTEMAVERSAO_EVOLUCAO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16OE2 */
                              E16OE2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SISTEMAVERSAO_MELHORIA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17OE2 */
                              E17OE2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SISTEMAVERSAO_CORRECAO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18OE2 */
                              E18OE2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SISTEMAVERSAO_DATA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19OE2 */
                              E19OE2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20OE2 */
                              E20OE2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21OE2 */
                              E21OE2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22OE2 */
                              E22OE2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23OE2 */
                              E23OE2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E24OE2 */
                              E24OE2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E25OE2 */
                              E25OE2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E26OE2 */
                              E26OE2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E27OE2 */
                              E27OE2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E28OE2 */
                              E28OE2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E29OE2 */
                              E29OE2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_88_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_88_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_88_idx), 4, 0)), 4, "0");
                              SubsflControlProps_882( ) ;
                              AV28Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV99Update_GXI : context.convertURL( context.PathToRelativeUrl( AV28Update))));
                              AV29Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV100Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV29Delete))));
                              A1859SistemaVersao_Codigo = (int)(context.localUtil.CToN( cgiGet( edtSistemaVersao_Codigo_Internalname), ",", "."));
                              A1860SistemaVersao_Id = cgiGet( edtSistemaVersao_Id_Internalname);
                              A1861SistemaVersao_Descricao = cgiGet( edtSistemaVersao_Descricao_Internalname);
                              A1862SistemaVersao_Evolucao = (short)(context.localUtil.CToN( cgiGet( edtSistemaVersao_Evolucao_Internalname), ",", "."));
                              n1862SistemaVersao_Evolucao = false;
                              A1863SistemaVersao_Melhoria = (short)(context.localUtil.CToN( cgiGet( edtSistemaVersao_Melhoria_Internalname), ",", "."));
                              n1863SistemaVersao_Melhoria = false;
                              A1864SistemaVersao_Correcao = (int)(context.localUtil.CToN( cgiGet( edtSistemaVersao_Correcao_Internalname), ",", "."));
                              n1864SistemaVersao_Correcao = false;
                              A1865SistemaVersao_Data = context.localUtil.CToT( cgiGet( edtSistemaVersao_Data_Internalname), 0);
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E30OE2 */
                                    E30OE2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E31OE2 */
                                    E31OE2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E32OE2 */
                                    E32OE2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Sistemaversao_id1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vSISTEMAVERSAO_ID1"), AV17SistemaVersao_Id1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Sistemaversao_id2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vSISTEMAVERSAO_ID2"), AV21SistemaVersao_Id2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Sistemaversao_id3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vSISTEMAVERSAO_ID3"), AV25SistemaVersao_Id3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfsistemaversao_codigo Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSISTEMAVERSAO_CODIGO"), ",", ".") != Convert.ToDecimal( AV38TFSistemaVersao_Codigo )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfsistemaversao_codigo_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSISTEMAVERSAO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV39TFSistemaVersao_Codigo_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfsistemaversao_id Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSISTEMAVERSAO_ID"), AV42TFSistemaVersao_Id) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfsistemaversao_id_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSISTEMAVERSAO_ID_SEL"), AV43TFSistemaVersao_Id_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfsistemaversao_descricao Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSISTEMAVERSAO_DESCRICAO"), AV46TFSistemaVersao_Descricao) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfsistemaversao_descricao_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSISTEMAVERSAO_DESCRICAO_SEL"), AV47TFSistemaVersao_Descricao_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfsistemaversao_evolucao Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSISTEMAVERSAO_EVOLUCAO"), ",", ".") != Convert.ToDecimal( AV50TFSistemaVersao_Evolucao )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfsistemaversao_evolucao_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSISTEMAVERSAO_EVOLUCAO_TO"), ",", ".") != Convert.ToDecimal( AV51TFSistemaVersao_Evolucao_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfsistemaversao_melhoria Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSISTEMAVERSAO_MELHORIA"), ",", ".") != Convert.ToDecimal( AV54TFSistemaVersao_Melhoria )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfsistemaversao_melhoria_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSISTEMAVERSAO_MELHORIA_TO"), ",", ".") != Convert.ToDecimal( AV55TFSistemaVersao_Melhoria_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfsistemaversao_correcao Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSISTEMAVERSAO_CORRECAO"), ",", ".") != Convert.ToDecimal( AV58TFSistemaVersao_Correcao )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfsistemaversao_correcao_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSISTEMAVERSAO_CORRECAO_TO"), ",", ".") != Convert.ToDecimal( AV59TFSistemaVersao_Correcao_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfsistemaversao_data Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFSISTEMAVERSAO_DATA"), 0) != AV62TFSistemaVersao_Data )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfsistemaversao_data_to Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFSISTEMAVERSAO_DATA_TO"), 0) != AV63TFSistemaVersao_Data_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEOE2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAOE2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("SISTEMAVERSAO_ID", "Vers�o", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("SISTEMAVERSAO_ID", "Vers�o", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("SISTEMAVERSAO_ID", "Vers�o", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_882( ) ;
         while ( nGXsfl_88_idx <= nRC_GXsfl_88 )
         {
            sendrow_882( ) ;
            nGXsfl_88_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_88_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_88_idx+1));
            sGXsfl_88_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_88_idx), 4, 0)), 4, "0");
            SubsflControlProps_882( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       String AV17SistemaVersao_Id1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       short AV20DynamicFiltersOperator2 ,
                                       String AV21SistemaVersao_Id2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       short AV24DynamicFiltersOperator3 ,
                                       String AV25SistemaVersao_Id3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       int AV38TFSistemaVersao_Codigo ,
                                       int AV39TFSistemaVersao_Codigo_To ,
                                       String AV42TFSistemaVersao_Id ,
                                       String AV43TFSistemaVersao_Id_Sel ,
                                       String AV46TFSistemaVersao_Descricao ,
                                       String AV47TFSistemaVersao_Descricao_Sel ,
                                       short AV50TFSistemaVersao_Evolucao ,
                                       short AV51TFSistemaVersao_Evolucao_To ,
                                       short AV54TFSistemaVersao_Melhoria ,
                                       short AV55TFSistemaVersao_Melhoria_To ,
                                       int AV58TFSistemaVersao_Correcao ,
                                       int AV59TFSistemaVersao_Correcao_To ,
                                       DateTime AV62TFSistemaVersao_Data ,
                                       DateTime AV63TFSistemaVersao_Data_To ,
                                       short AV31ManageFiltersExecutionStep ,
                                       String AV40ddo_SistemaVersao_CodigoTitleControlIdToReplace ,
                                       String AV44ddo_SistemaVersao_IdTitleControlIdToReplace ,
                                       String AV48ddo_SistemaVersao_DescricaoTitleControlIdToReplace ,
                                       String AV52ddo_SistemaVersao_EvolucaoTitleControlIdToReplace ,
                                       String AV56ddo_SistemaVersao_MelhoriaTitleControlIdToReplace ,
                                       String AV60ddo_SistemaVersao_CorrecaoTitleControlIdToReplace ,
                                       String AV66ddo_SistemaVersao_DataTitleControlIdToReplace ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       String AV101Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving ,
                                       int A1859SistemaVersao_Codigo ,
                                       int AV71SistemaVersao_SistemaCod )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFOE2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_SISTEMAVERSAO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1859SistemaVersao_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "SISTEMAVERSAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1859SistemaVersao_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_SISTEMAVERSAO_ID", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1860SistemaVersao_Id, ""))));
         GxWebStd.gx_hidden_field( context, "SISTEMAVERSAO_ID", StringUtil.RTrim( A1860SistemaVersao_Id));
         GxWebStd.gx_hidden_field( context, "gxhash_SISTEMAVERSAO_DESCRICAO", GetSecureSignedToken( "", A1861SistemaVersao_Descricao));
         GxWebStd.gx_hidden_field( context, "SISTEMAVERSAO_DESCRICAO", A1861SistemaVersao_Descricao);
         GxWebStd.gx_hidden_field( context, "gxhash_SISTEMAVERSAO_EVOLUCAO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1862SistemaVersao_Evolucao), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "SISTEMAVERSAO_EVOLUCAO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1862SistemaVersao_Evolucao), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_SISTEMAVERSAO_MELHORIA", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1863SistemaVersao_Melhoria), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "SISTEMAVERSAO_MELHORIA", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1863SistemaVersao_Melhoria), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_SISTEMAVERSAO_CORRECAO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1864SistemaVersao_Correcao), "ZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "SISTEMAVERSAO_CORRECAO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1864SistemaVersao_Correcao), 8, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_SISTEMAVERSAO_DATA", GetSecureSignedToken( "", context.localUtil.Format( A1865SistemaVersao_Data, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, "SISTEMAVERSAO_DATA", context.localUtil.TToC( A1865SistemaVersao_Data, 10, 8, 0, 3, "/", ":", " "));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFOE2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV101Pgmname = "WWSistemaVersao";
         context.Gx_err = 0;
      }

      protected void RFOE2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 88;
         /* Execute user event: E31OE2 */
         E31OE2 ();
         nGXsfl_88_idx = 1;
         sGXsfl_88_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_88_idx), 4, 0)), 4, "0");
         SubsflControlProps_882( ) ;
         nGXsfl_88_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_882( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV74WWSistemaVersaoDS_1_Dynamicfiltersselector1 ,
                                                 AV75WWSistemaVersaoDS_2_Dynamicfiltersoperator1 ,
                                                 AV76WWSistemaVersaoDS_3_Sistemaversao_id1 ,
                                                 AV77WWSistemaVersaoDS_4_Dynamicfiltersenabled2 ,
                                                 AV78WWSistemaVersaoDS_5_Dynamicfiltersselector2 ,
                                                 AV79WWSistemaVersaoDS_6_Dynamicfiltersoperator2 ,
                                                 AV80WWSistemaVersaoDS_7_Sistemaversao_id2 ,
                                                 AV81WWSistemaVersaoDS_8_Dynamicfiltersenabled3 ,
                                                 AV82WWSistemaVersaoDS_9_Dynamicfiltersselector3 ,
                                                 AV83WWSistemaVersaoDS_10_Dynamicfiltersoperator3 ,
                                                 AV84WWSistemaVersaoDS_11_Sistemaversao_id3 ,
                                                 AV85WWSistemaVersaoDS_12_Tfsistemaversao_codigo ,
                                                 AV86WWSistemaVersaoDS_13_Tfsistemaversao_codigo_to ,
                                                 AV88WWSistemaVersaoDS_15_Tfsistemaversao_id_sel ,
                                                 AV87WWSistemaVersaoDS_14_Tfsistemaversao_id ,
                                                 AV90WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel ,
                                                 AV89WWSistemaVersaoDS_16_Tfsistemaversao_descricao ,
                                                 AV91WWSistemaVersaoDS_18_Tfsistemaversao_evolucao ,
                                                 AV92WWSistemaVersaoDS_19_Tfsistemaversao_evolucao_to ,
                                                 AV93WWSistemaVersaoDS_20_Tfsistemaversao_melhoria ,
                                                 AV94WWSistemaVersaoDS_21_Tfsistemaversao_melhoria_to ,
                                                 AV95WWSistemaVersaoDS_22_Tfsistemaversao_correcao ,
                                                 AV96WWSistemaVersaoDS_23_Tfsistemaversao_correcao_to ,
                                                 AV97WWSistemaVersaoDS_24_Tfsistemaversao_data ,
                                                 AV98WWSistemaVersaoDS_25_Tfsistemaversao_data_to ,
                                                 A1860SistemaVersao_Id ,
                                                 A1859SistemaVersao_Codigo ,
                                                 A1861SistemaVersao_Descricao ,
                                                 A1862SistemaVersao_Evolucao ,
                                                 A1863SistemaVersao_Melhoria ,
                                                 A1864SistemaVersao_Correcao ,
                                                 A1865SistemaVersao_Data ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                                 TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT,
                                                 TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN,
                                                 TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV76WWSistemaVersaoDS_3_Sistemaversao_id1 = StringUtil.PadR( StringUtil.RTrim( AV76WWSistemaVersaoDS_3_Sistemaversao_id1), 20, "%");
            lV76WWSistemaVersaoDS_3_Sistemaversao_id1 = StringUtil.PadR( StringUtil.RTrim( AV76WWSistemaVersaoDS_3_Sistemaversao_id1), 20, "%");
            lV80WWSistemaVersaoDS_7_Sistemaversao_id2 = StringUtil.PadR( StringUtil.RTrim( AV80WWSistemaVersaoDS_7_Sistemaversao_id2), 20, "%");
            lV80WWSistemaVersaoDS_7_Sistemaversao_id2 = StringUtil.PadR( StringUtil.RTrim( AV80WWSistemaVersaoDS_7_Sistemaversao_id2), 20, "%");
            lV84WWSistemaVersaoDS_11_Sistemaversao_id3 = StringUtil.PadR( StringUtil.RTrim( AV84WWSistemaVersaoDS_11_Sistemaversao_id3), 20, "%");
            lV84WWSistemaVersaoDS_11_Sistemaversao_id3 = StringUtil.PadR( StringUtil.RTrim( AV84WWSistemaVersaoDS_11_Sistemaversao_id3), 20, "%");
            lV87WWSistemaVersaoDS_14_Tfsistemaversao_id = StringUtil.PadR( StringUtil.RTrim( AV87WWSistemaVersaoDS_14_Tfsistemaversao_id), 20, "%");
            lV89WWSistemaVersaoDS_16_Tfsistemaversao_descricao = StringUtil.Concat( StringUtil.RTrim( AV89WWSistemaVersaoDS_16_Tfsistemaversao_descricao), "%", "");
            /* Using cursor H00OE2 */
            pr_default.execute(0, new Object[] {lV76WWSistemaVersaoDS_3_Sistemaversao_id1, lV76WWSistemaVersaoDS_3_Sistemaversao_id1, lV80WWSistemaVersaoDS_7_Sistemaversao_id2, lV80WWSistemaVersaoDS_7_Sistemaversao_id2, lV84WWSistemaVersaoDS_11_Sistemaversao_id3, lV84WWSistemaVersaoDS_11_Sistemaversao_id3, AV85WWSistemaVersaoDS_12_Tfsistemaversao_codigo, AV86WWSistemaVersaoDS_13_Tfsistemaversao_codigo_to, lV87WWSistemaVersaoDS_14_Tfsistemaversao_id, AV88WWSistemaVersaoDS_15_Tfsistemaversao_id_sel, lV89WWSistemaVersaoDS_16_Tfsistemaversao_descricao, AV90WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel, AV91WWSistemaVersaoDS_18_Tfsistemaversao_evolucao, AV92WWSistemaVersaoDS_19_Tfsistemaversao_evolucao_to, AV93WWSistemaVersaoDS_20_Tfsistemaversao_melhoria, AV94WWSistemaVersaoDS_21_Tfsistemaversao_melhoria_to, AV95WWSistemaVersaoDS_22_Tfsistemaversao_correcao, AV96WWSistemaVersaoDS_23_Tfsistemaversao_correcao_to, AV97WWSistemaVersaoDS_24_Tfsistemaversao_data, AV98WWSistemaVersaoDS_25_Tfsistemaversao_data_to, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_88_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1865SistemaVersao_Data = H00OE2_A1865SistemaVersao_Data[0];
               A1864SistemaVersao_Correcao = H00OE2_A1864SistemaVersao_Correcao[0];
               n1864SistemaVersao_Correcao = H00OE2_n1864SistemaVersao_Correcao[0];
               A1863SistemaVersao_Melhoria = H00OE2_A1863SistemaVersao_Melhoria[0];
               n1863SistemaVersao_Melhoria = H00OE2_n1863SistemaVersao_Melhoria[0];
               A1862SistemaVersao_Evolucao = H00OE2_A1862SistemaVersao_Evolucao[0];
               n1862SistemaVersao_Evolucao = H00OE2_n1862SistemaVersao_Evolucao[0];
               A1861SistemaVersao_Descricao = H00OE2_A1861SistemaVersao_Descricao[0];
               A1860SistemaVersao_Id = H00OE2_A1860SistemaVersao_Id[0];
               A1859SistemaVersao_Codigo = H00OE2_A1859SistemaVersao_Codigo[0];
               /* Execute user event: E32OE2 */
               E32OE2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 88;
            WBOE0( ) ;
         }
         nGXsfl_88_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV74WWSistemaVersaoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV75WWSistemaVersaoDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV76WWSistemaVersaoDS_3_Sistemaversao_id1 = AV17SistemaVersao_Id1;
         AV77WWSistemaVersaoDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV78WWSistemaVersaoDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV79WWSistemaVersaoDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV80WWSistemaVersaoDS_7_Sistemaversao_id2 = AV21SistemaVersao_Id2;
         AV81WWSistemaVersaoDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV82WWSistemaVersaoDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV83WWSistemaVersaoDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV84WWSistemaVersaoDS_11_Sistemaversao_id3 = AV25SistemaVersao_Id3;
         AV85WWSistemaVersaoDS_12_Tfsistemaversao_codigo = AV38TFSistemaVersao_Codigo;
         AV86WWSistemaVersaoDS_13_Tfsistemaversao_codigo_to = AV39TFSistemaVersao_Codigo_To;
         AV87WWSistemaVersaoDS_14_Tfsistemaversao_id = AV42TFSistemaVersao_Id;
         AV88WWSistemaVersaoDS_15_Tfsistemaversao_id_sel = AV43TFSistemaVersao_Id_Sel;
         AV89WWSistemaVersaoDS_16_Tfsistemaversao_descricao = AV46TFSistemaVersao_Descricao;
         AV90WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel = AV47TFSistemaVersao_Descricao_Sel;
         AV91WWSistemaVersaoDS_18_Tfsistemaversao_evolucao = AV50TFSistemaVersao_Evolucao;
         AV92WWSistemaVersaoDS_19_Tfsistemaversao_evolucao_to = AV51TFSistemaVersao_Evolucao_To;
         AV93WWSistemaVersaoDS_20_Tfsistemaversao_melhoria = AV54TFSistemaVersao_Melhoria;
         AV94WWSistemaVersaoDS_21_Tfsistemaversao_melhoria_to = AV55TFSistemaVersao_Melhoria_To;
         AV95WWSistemaVersaoDS_22_Tfsistemaversao_correcao = AV58TFSistemaVersao_Correcao;
         AV96WWSistemaVersaoDS_23_Tfsistemaversao_correcao_to = AV59TFSistemaVersao_Correcao_To;
         AV97WWSistemaVersaoDS_24_Tfsistemaversao_data = AV62TFSistemaVersao_Data;
         AV98WWSistemaVersaoDS_25_Tfsistemaversao_data_to = AV63TFSistemaVersao_Data_To;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV74WWSistemaVersaoDS_1_Dynamicfiltersselector1 ,
                                              AV75WWSistemaVersaoDS_2_Dynamicfiltersoperator1 ,
                                              AV76WWSistemaVersaoDS_3_Sistemaversao_id1 ,
                                              AV77WWSistemaVersaoDS_4_Dynamicfiltersenabled2 ,
                                              AV78WWSistemaVersaoDS_5_Dynamicfiltersselector2 ,
                                              AV79WWSistemaVersaoDS_6_Dynamicfiltersoperator2 ,
                                              AV80WWSistemaVersaoDS_7_Sistemaversao_id2 ,
                                              AV81WWSistemaVersaoDS_8_Dynamicfiltersenabled3 ,
                                              AV82WWSistemaVersaoDS_9_Dynamicfiltersselector3 ,
                                              AV83WWSistemaVersaoDS_10_Dynamicfiltersoperator3 ,
                                              AV84WWSistemaVersaoDS_11_Sistemaversao_id3 ,
                                              AV85WWSistemaVersaoDS_12_Tfsistemaversao_codigo ,
                                              AV86WWSistemaVersaoDS_13_Tfsistemaversao_codigo_to ,
                                              AV88WWSistemaVersaoDS_15_Tfsistemaversao_id_sel ,
                                              AV87WWSistemaVersaoDS_14_Tfsistemaversao_id ,
                                              AV90WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel ,
                                              AV89WWSistemaVersaoDS_16_Tfsistemaversao_descricao ,
                                              AV91WWSistemaVersaoDS_18_Tfsistemaversao_evolucao ,
                                              AV92WWSistemaVersaoDS_19_Tfsistemaversao_evolucao_to ,
                                              AV93WWSistemaVersaoDS_20_Tfsistemaversao_melhoria ,
                                              AV94WWSistemaVersaoDS_21_Tfsistemaversao_melhoria_to ,
                                              AV95WWSistemaVersaoDS_22_Tfsistemaversao_correcao ,
                                              AV96WWSistemaVersaoDS_23_Tfsistemaversao_correcao_to ,
                                              AV97WWSistemaVersaoDS_24_Tfsistemaversao_data ,
                                              AV98WWSistemaVersaoDS_25_Tfsistemaversao_data_to ,
                                              A1860SistemaVersao_Id ,
                                              A1859SistemaVersao_Codigo ,
                                              A1861SistemaVersao_Descricao ,
                                              A1862SistemaVersao_Evolucao ,
                                              A1863SistemaVersao_Melhoria ,
                                              A1864SistemaVersao_Correcao ,
                                              A1865SistemaVersao_Data ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN,
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV76WWSistemaVersaoDS_3_Sistemaversao_id1 = StringUtil.PadR( StringUtil.RTrim( AV76WWSistemaVersaoDS_3_Sistemaversao_id1), 20, "%");
         lV76WWSistemaVersaoDS_3_Sistemaversao_id1 = StringUtil.PadR( StringUtil.RTrim( AV76WWSistemaVersaoDS_3_Sistemaversao_id1), 20, "%");
         lV80WWSistemaVersaoDS_7_Sistemaversao_id2 = StringUtil.PadR( StringUtil.RTrim( AV80WWSistemaVersaoDS_7_Sistemaversao_id2), 20, "%");
         lV80WWSistemaVersaoDS_7_Sistemaversao_id2 = StringUtil.PadR( StringUtil.RTrim( AV80WWSistemaVersaoDS_7_Sistemaversao_id2), 20, "%");
         lV84WWSistemaVersaoDS_11_Sistemaversao_id3 = StringUtil.PadR( StringUtil.RTrim( AV84WWSistemaVersaoDS_11_Sistemaversao_id3), 20, "%");
         lV84WWSistemaVersaoDS_11_Sistemaversao_id3 = StringUtil.PadR( StringUtil.RTrim( AV84WWSistemaVersaoDS_11_Sistemaversao_id3), 20, "%");
         lV87WWSistemaVersaoDS_14_Tfsistemaversao_id = StringUtil.PadR( StringUtil.RTrim( AV87WWSistemaVersaoDS_14_Tfsistemaversao_id), 20, "%");
         lV89WWSistemaVersaoDS_16_Tfsistemaversao_descricao = StringUtil.Concat( StringUtil.RTrim( AV89WWSistemaVersaoDS_16_Tfsistemaversao_descricao), "%", "");
         /* Using cursor H00OE3 */
         pr_default.execute(1, new Object[] {lV76WWSistemaVersaoDS_3_Sistemaversao_id1, lV76WWSistemaVersaoDS_3_Sistemaversao_id1, lV80WWSistemaVersaoDS_7_Sistemaversao_id2, lV80WWSistemaVersaoDS_7_Sistemaversao_id2, lV84WWSistemaVersaoDS_11_Sistemaversao_id3, lV84WWSistemaVersaoDS_11_Sistemaversao_id3, AV85WWSistemaVersaoDS_12_Tfsistemaversao_codigo, AV86WWSistemaVersaoDS_13_Tfsistemaversao_codigo_to, lV87WWSistemaVersaoDS_14_Tfsistemaversao_id, AV88WWSistemaVersaoDS_15_Tfsistemaversao_id_sel, lV89WWSistemaVersaoDS_16_Tfsistemaversao_descricao, AV90WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel, AV91WWSistemaVersaoDS_18_Tfsistemaversao_evolucao, AV92WWSistemaVersaoDS_19_Tfsistemaversao_evolucao_to, AV93WWSistemaVersaoDS_20_Tfsistemaversao_melhoria, AV94WWSistemaVersaoDS_21_Tfsistemaversao_melhoria_to, AV95WWSistemaVersaoDS_22_Tfsistemaversao_correcao, AV96WWSistemaVersaoDS_23_Tfsistemaversao_correcao_to, AV97WWSistemaVersaoDS_24_Tfsistemaversao_data, AV98WWSistemaVersaoDS_25_Tfsistemaversao_data_to});
         GRID_nRecordCount = H00OE3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV74WWSistemaVersaoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV75WWSistemaVersaoDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV76WWSistemaVersaoDS_3_Sistemaversao_id1 = AV17SistemaVersao_Id1;
         AV77WWSistemaVersaoDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV78WWSistemaVersaoDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV79WWSistemaVersaoDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV80WWSistemaVersaoDS_7_Sistemaversao_id2 = AV21SistemaVersao_Id2;
         AV81WWSistemaVersaoDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV82WWSistemaVersaoDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV83WWSistemaVersaoDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV84WWSistemaVersaoDS_11_Sistemaversao_id3 = AV25SistemaVersao_Id3;
         AV85WWSistemaVersaoDS_12_Tfsistemaversao_codigo = AV38TFSistemaVersao_Codigo;
         AV86WWSistemaVersaoDS_13_Tfsistemaversao_codigo_to = AV39TFSistemaVersao_Codigo_To;
         AV87WWSistemaVersaoDS_14_Tfsistemaversao_id = AV42TFSistemaVersao_Id;
         AV88WWSistemaVersaoDS_15_Tfsistemaversao_id_sel = AV43TFSistemaVersao_Id_Sel;
         AV89WWSistemaVersaoDS_16_Tfsistemaversao_descricao = AV46TFSistemaVersao_Descricao;
         AV90WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel = AV47TFSistemaVersao_Descricao_Sel;
         AV91WWSistemaVersaoDS_18_Tfsistemaversao_evolucao = AV50TFSistemaVersao_Evolucao;
         AV92WWSistemaVersaoDS_19_Tfsistemaversao_evolucao_to = AV51TFSistemaVersao_Evolucao_To;
         AV93WWSistemaVersaoDS_20_Tfsistemaversao_melhoria = AV54TFSistemaVersao_Melhoria;
         AV94WWSistemaVersaoDS_21_Tfsistemaversao_melhoria_to = AV55TFSistemaVersao_Melhoria_To;
         AV95WWSistemaVersaoDS_22_Tfsistemaversao_correcao = AV58TFSistemaVersao_Correcao;
         AV96WWSistemaVersaoDS_23_Tfsistemaversao_correcao_to = AV59TFSistemaVersao_Correcao_To;
         AV97WWSistemaVersaoDS_24_Tfsistemaversao_data = AV62TFSistemaVersao_Data;
         AV98WWSistemaVersaoDS_25_Tfsistemaversao_data_to = AV63TFSistemaVersao_Data_To;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17SistemaVersao_Id1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21SistemaVersao_Id2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25SistemaVersao_Id3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV38TFSistemaVersao_Codigo, AV39TFSistemaVersao_Codigo_To, AV42TFSistemaVersao_Id, AV43TFSistemaVersao_Id_Sel, AV46TFSistemaVersao_Descricao, AV47TFSistemaVersao_Descricao_Sel, AV50TFSistemaVersao_Evolucao, AV51TFSistemaVersao_Evolucao_To, AV54TFSistemaVersao_Melhoria, AV55TFSistemaVersao_Melhoria_To, AV58TFSistemaVersao_Correcao, AV59TFSistemaVersao_Correcao_To, AV62TFSistemaVersao_Data, AV63TFSistemaVersao_Data_To, AV31ManageFiltersExecutionStep, AV40ddo_SistemaVersao_CodigoTitleControlIdToReplace, AV44ddo_SistemaVersao_IdTitleControlIdToReplace, AV48ddo_SistemaVersao_DescricaoTitleControlIdToReplace, AV52ddo_SistemaVersao_EvolucaoTitleControlIdToReplace, AV56ddo_SistemaVersao_MelhoriaTitleControlIdToReplace, AV60ddo_SistemaVersao_CorrecaoTitleControlIdToReplace, AV66ddo_SistemaVersao_DataTitleControlIdToReplace, AV6WWPContext, AV101Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1859SistemaVersao_Codigo, AV71SistemaVersao_SistemaCod) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV74WWSistemaVersaoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV75WWSistemaVersaoDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV76WWSistemaVersaoDS_3_Sistemaversao_id1 = AV17SistemaVersao_Id1;
         AV77WWSistemaVersaoDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV78WWSistemaVersaoDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV79WWSistemaVersaoDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV80WWSistemaVersaoDS_7_Sistemaversao_id2 = AV21SistemaVersao_Id2;
         AV81WWSistemaVersaoDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV82WWSistemaVersaoDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV83WWSistemaVersaoDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV84WWSistemaVersaoDS_11_Sistemaversao_id3 = AV25SistemaVersao_Id3;
         AV85WWSistemaVersaoDS_12_Tfsistemaversao_codigo = AV38TFSistemaVersao_Codigo;
         AV86WWSistemaVersaoDS_13_Tfsistemaversao_codigo_to = AV39TFSistemaVersao_Codigo_To;
         AV87WWSistemaVersaoDS_14_Tfsistemaversao_id = AV42TFSistemaVersao_Id;
         AV88WWSistemaVersaoDS_15_Tfsistemaversao_id_sel = AV43TFSistemaVersao_Id_Sel;
         AV89WWSistemaVersaoDS_16_Tfsistemaversao_descricao = AV46TFSistemaVersao_Descricao;
         AV90WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel = AV47TFSistemaVersao_Descricao_Sel;
         AV91WWSistemaVersaoDS_18_Tfsistemaversao_evolucao = AV50TFSistemaVersao_Evolucao;
         AV92WWSistemaVersaoDS_19_Tfsistemaversao_evolucao_to = AV51TFSistemaVersao_Evolucao_To;
         AV93WWSistemaVersaoDS_20_Tfsistemaversao_melhoria = AV54TFSistemaVersao_Melhoria;
         AV94WWSistemaVersaoDS_21_Tfsistemaversao_melhoria_to = AV55TFSistemaVersao_Melhoria_To;
         AV95WWSistemaVersaoDS_22_Tfsistemaversao_correcao = AV58TFSistemaVersao_Correcao;
         AV96WWSistemaVersaoDS_23_Tfsistemaversao_correcao_to = AV59TFSistemaVersao_Correcao_To;
         AV97WWSistemaVersaoDS_24_Tfsistemaversao_data = AV62TFSistemaVersao_Data;
         AV98WWSistemaVersaoDS_25_Tfsistemaversao_data_to = AV63TFSistemaVersao_Data_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17SistemaVersao_Id1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21SistemaVersao_Id2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25SistemaVersao_Id3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV38TFSistemaVersao_Codigo, AV39TFSistemaVersao_Codigo_To, AV42TFSistemaVersao_Id, AV43TFSistemaVersao_Id_Sel, AV46TFSistemaVersao_Descricao, AV47TFSistemaVersao_Descricao_Sel, AV50TFSistemaVersao_Evolucao, AV51TFSistemaVersao_Evolucao_To, AV54TFSistemaVersao_Melhoria, AV55TFSistemaVersao_Melhoria_To, AV58TFSistemaVersao_Correcao, AV59TFSistemaVersao_Correcao_To, AV62TFSistemaVersao_Data, AV63TFSistemaVersao_Data_To, AV31ManageFiltersExecutionStep, AV40ddo_SistemaVersao_CodigoTitleControlIdToReplace, AV44ddo_SistemaVersao_IdTitleControlIdToReplace, AV48ddo_SistemaVersao_DescricaoTitleControlIdToReplace, AV52ddo_SistemaVersao_EvolucaoTitleControlIdToReplace, AV56ddo_SistemaVersao_MelhoriaTitleControlIdToReplace, AV60ddo_SistemaVersao_CorrecaoTitleControlIdToReplace, AV66ddo_SistemaVersao_DataTitleControlIdToReplace, AV6WWPContext, AV101Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1859SistemaVersao_Codigo, AV71SistemaVersao_SistemaCod) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV74WWSistemaVersaoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV75WWSistemaVersaoDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV76WWSistemaVersaoDS_3_Sistemaversao_id1 = AV17SistemaVersao_Id1;
         AV77WWSistemaVersaoDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV78WWSistemaVersaoDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV79WWSistemaVersaoDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV80WWSistemaVersaoDS_7_Sistemaversao_id2 = AV21SistemaVersao_Id2;
         AV81WWSistemaVersaoDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV82WWSistemaVersaoDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV83WWSistemaVersaoDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV84WWSistemaVersaoDS_11_Sistemaversao_id3 = AV25SistemaVersao_Id3;
         AV85WWSistemaVersaoDS_12_Tfsistemaversao_codigo = AV38TFSistemaVersao_Codigo;
         AV86WWSistemaVersaoDS_13_Tfsistemaversao_codigo_to = AV39TFSistemaVersao_Codigo_To;
         AV87WWSistemaVersaoDS_14_Tfsistemaversao_id = AV42TFSistemaVersao_Id;
         AV88WWSistemaVersaoDS_15_Tfsistemaversao_id_sel = AV43TFSistemaVersao_Id_Sel;
         AV89WWSistemaVersaoDS_16_Tfsistemaversao_descricao = AV46TFSistemaVersao_Descricao;
         AV90WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel = AV47TFSistemaVersao_Descricao_Sel;
         AV91WWSistemaVersaoDS_18_Tfsistemaversao_evolucao = AV50TFSistemaVersao_Evolucao;
         AV92WWSistemaVersaoDS_19_Tfsistemaversao_evolucao_to = AV51TFSistemaVersao_Evolucao_To;
         AV93WWSistemaVersaoDS_20_Tfsistemaversao_melhoria = AV54TFSistemaVersao_Melhoria;
         AV94WWSistemaVersaoDS_21_Tfsistemaversao_melhoria_to = AV55TFSistemaVersao_Melhoria_To;
         AV95WWSistemaVersaoDS_22_Tfsistemaversao_correcao = AV58TFSistemaVersao_Correcao;
         AV96WWSistemaVersaoDS_23_Tfsistemaversao_correcao_to = AV59TFSistemaVersao_Correcao_To;
         AV97WWSistemaVersaoDS_24_Tfsistemaversao_data = AV62TFSistemaVersao_Data;
         AV98WWSistemaVersaoDS_25_Tfsistemaversao_data_to = AV63TFSistemaVersao_Data_To;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17SistemaVersao_Id1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21SistemaVersao_Id2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25SistemaVersao_Id3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV38TFSistemaVersao_Codigo, AV39TFSistemaVersao_Codigo_To, AV42TFSistemaVersao_Id, AV43TFSistemaVersao_Id_Sel, AV46TFSistemaVersao_Descricao, AV47TFSistemaVersao_Descricao_Sel, AV50TFSistemaVersao_Evolucao, AV51TFSistemaVersao_Evolucao_To, AV54TFSistemaVersao_Melhoria, AV55TFSistemaVersao_Melhoria_To, AV58TFSistemaVersao_Correcao, AV59TFSistemaVersao_Correcao_To, AV62TFSistemaVersao_Data, AV63TFSistemaVersao_Data_To, AV31ManageFiltersExecutionStep, AV40ddo_SistemaVersao_CodigoTitleControlIdToReplace, AV44ddo_SistemaVersao_IdTitleControlIdToReplace, AV48ddo_SistemaVersao_DescricaoTitleControlIdToReplace, AV52ddo_SistemaVersao_EvolucaoTitleControlIdToReplace, AV56ddo_SistemaVersao_MelhoriaTitleControlIdToReplace, AV60ddo_SistemaVersao_CorrecaoTitleControlIdToReplace, AV66ddo_SistemaVersao_DataTitleControlIdToReplace, AV6WWPContext, AV101Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1859SistemaVersao_Codigo, AV71SistemaVersao_SistemaCod) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV74WWSistemaVersaoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV75WWSistemaVersaoDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV76WWSistemaVersaoDS_3_Sistemaversao_id1 = AV17SistemaVersao_Id1;
         AV77WWSistemaVersaoDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV78WWSistemaVersaoDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV79WWSistemaVersaoDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV80WWSistemaVersaoDS_7_Sistemaversao_id2 = AV21SistemaVersao_Id2;
         AV81WWSistemaVersaoDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV82WWSistemaVersaoDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV83WWSistemaVersaoDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV84WWSistemaVersaoDS_11_Sistemaversao_id3 = AV25SistemaVersao_Id3;
         AV85WWSistemaVersaoDS_12_Tfsistemaversao_codigo = AV38TFSistemaVersao_Codigo;
         AV86WWSistemaVersaoDS_13_Tfsistemaversao_codigo_to = AV39TFSistemaVersao_Codigo_To;
         AV87WWSistemaVersaoDS_14_Tfsistemaversao_id = AV42TFSistemaVersao_Id;
         AV88WWSistemaVersaoDS_15_Tfsistemaversao_id_sel = AV43TFSistemaVersao_Id_Sel;
         AV89WWSistemaVersaoDS_16_Tfsistemaversao_descricao = AV46TFSistemaVersao_Descricao;
         AV90WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel = AV47TFSistemaVersao_Descricao_Sel;
         AV91WWSistemaVersaoDS_18_Tfsistemaversao_evolucao = AV50TFSistemaVersao_Evolucao;
         AV92WWSistemaVersaoDS_19_Tfsistemaversao_evolucao_to = AV51TFSistemaVersao_Evolucao_To;
         AV93WWSistemaVersaoDS_20_Tfsistemaversao_melhoria = AV54TFSistemaVersao_Melhoria;
         AV94WWSistemaVersaoDS_21_Tfsistemaversao_melhoria_to = AV55TFSistemaVersao_Melhoria_To;
         AV95WWSistemaVersaoDS_22_Tfsistemaversao_correcao = AV58TFSistemaVersao_Correcao;
         AV96WWSistemaVersaoDS_23_Tfsistemaversao_correcao_to = AV59TFSistemaVersao_Correcao_To;
         AV97WWSistemaVersaoDS_24_Tfsistemaversao_data = AV62TFSistemaVersao_Data;
         AV98WWSistemaVersaoDS_25_Tfsistemaversao_data_to = AV63TFSistemaVersao_Data_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17SistemaVersao_Id1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21SistemaVersao_Id2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25SistemaVersao_Id3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV38TFSistemaVersao_Codigo, AV39TFSistemaVersao_Codigo_To, AV42TFSistemaVersao_Id, AV43TFSistemaVersao_Id_Sel, AV46TFSistemaVersao_Descricao, AV47TFSistemaVersao_Descricao_Sel, AV50TFSistemaVersao_Evolucao, AV51TFSistemaVersao_Evolucao_To, AV54TFSistemaVersao_Melhoria, AV55TFSistemaVersao_Melhoria_To, AV58TFSistemaVersao_Correcao, AV59TFSistemaVersao_Correcao_To, AV62TFSistemaVersao_Data, AV63TFSistemaVersao_Data_To, AV31ManageFiltersExecutionStep, AV40ddo_SistemaVersao_CodigoTitleControlIdToReplace, AV44ddo_SistemaVersao_IdTitleControlIdToReplace, AV48ddo_SistemaVersao_DescricaoTitleControlIdToReplace, AV52ddo_SistemaVersao_EvolucaoTitleControlIdToReplace, AV56ddo_SistemaVersao_MelhoriaTitleControlIdToReplace, AV60ddo_SistemaVersao_CorrecaoTitleControlIdToReplace, AV66ddo_SistemaVersao_DataTitleControlIdToReplace, AV6WWPContext, AV101Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1859SistemaVersao_Codigo, AV71SistemaVersao_SistemaCod) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV74WWSistemaVersaoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV75WWSistemaVersaoDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV76WWSistemaVersaoDS_3_Sistemaversao_id1 = AV17SistemaVersao_Id1;
         AV77WWSistemaVersaoDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV78WWSistemaVersaoDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV79WWSistemaVersaoDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV80WWSistemaVersaoDS_7_Sistemaversao_id2 = AV21SistemaVersao_Id2;
         AV81WWSistemaVersaoDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV82WWSistemaVersaoDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV83WWSistemaVersaoDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV84WWSistemaVersaoDS_11_Sistemaversao_id3 = AV25SistemaVersao_Id3;
         AV85WWSistemaVersaoDS_12_Tfsistemaversao_codigo = AV38TFSistemaVersao_Codigo;
         AV86WWSistemaVersaoDS_13_Tfsistemaversao_codigo_to = AV39TFSistemaVersao_Codigo_To;
         AV87WWSistemaVersaoDS_14_Tfsistemaversao_id = AV42TFSistemaVersao_Id;
         AV88WWSistemaVersaoDS_15_Tfsistemaversao_id_sel = AV43TFSistemaVersao_Id_Sel;
         AV89WWSistemaVersaoDS_16_Tfsistemaversao_descricao = AV46TFSistemaVersao_Descricao;
         AV90WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel = AV47TFSistemaVersao_Descricao_Sel;
         AV91WWSistemaVersaoDS_18_Tfsistemaversao_evolucao = AV50TFSistemaVersao_Evolucao;
         AV92WWSistemaVersaoDS_19_Tfsistemaversao_evolucao_to = AV51TFSistemaVersao_Evolucao_To;
         AV93WWSistemaVersaoDS_20_Tfsistemaversao_melhoria = AV54TFSistemaVersao_Melhoria;
         AV94WWSistemaVersaoDS_21_Tfsistemaversao_melhoria_to = AV55TFSistemaVersao_Melhoria_To;
         AV95WWSistemaVersaoDS_22_Tfsistemaversao_correcao = AV58TFSistemaVersao_Correcao;
         AV96WWSistemaVersaoDS_23_Tfsistemaversao_correcao_to = AV59TFSistemaVersao_Correcao_To;
         AV97WWSistemaVersaoDS_24_Tfsistemaversao_data = AV62TFSistemaVersao_Data;
         AV98WWSistemaVersaoDS_25_Tfsistemaversao_data_to = AV63TFSistemaVersao_Data_To;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17SistemaVersao_Id1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21SistemaVersao_Id2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25SistemaVersao_Id3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV38TFSistemaVersao_Codigo, AV39TFSistemaVersao_Codigo_To, AV42TFSistemaVersao_Id, AV43TFSistemaVersao_Id_Sel, AV46TFSistemaVersao_Descricao, AV47TFSistemaVersao_Descricao_Sel, AV50TFSistemaVersao_Evolucao, AV51TFSistemaVersao_Evolucao_To, AV54TFSistemaVersao_Melhoria, AV55TFSistemaVersao_Melhoria_To, AV58TFSistemaVersao_Correcao, AV59TFSistemaVersao_Correcao_To, AV62TFSistemaVersao_Data, AV63TFSistemaVersao_Data_To, AV31ManageFiltersExecutionStep, AV40ddo_SistemaVersao_CodigoTitleControlIdToReplace, AV44ddo_SistemaVersao_IdTitleControlIdToReplace, AV48ddo_SistemaVersao_DescricaoTitleControlIdToReplace, AV52ddo_SistemaVersao_EvolucaoTitleControlIdToReplace, AV56ddo_SistemaVersao_MelhoriaTitleControlIdToReplace, AV60ddo_SistemaVersao_CorrecaoTitleControlIdToReplace, AV66ddo_SistemaVersao_DataTitleControlIdToReplace, AV6WWPContext, AV101Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1859SistemaVersao_Codigo, AV71SistemaVersao_SistemaCod) ;
         }
         return (int)(0) ;
      }

      protected void STRUPOE0( )
      {
         /* Before Start, stand alone formulas. */
         AV101Pgmname = "WWSistemaVersao";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E30OE2 */
         E30OE2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vMANAGEFILTERSDATA"), AV35ManageFiltersData);
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV67DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vSISTEMAVERSAO_CODIGOTITLEFILTERDATA"), AV37SistemaVersao_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSISTEMAVERSAO_IDTITLEFILTERDATA"), AV41SistemaVersao_IdTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSISTEMAVERSAO_DESCRICAOTITLEFILTERDATA"), AV45SistemaVersao_DescricaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSISTEMAVERSAO_EVOLUCAOTITLEFILTERDATA"), AV49SistemaVersao_EvolucaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSISTEMAVERSAO_MELHORIATITLEFILTERDATA"), AV53SistemaVersao_MelhoriaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSISTEMAVERSAO_CORRECAOTITLEFILTERDATA"), AV57SistemaVersao_CorrecaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSISTEMAVERSAO_DATATITLEFILTERDATA"), AV61SistemaVersao_DataTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            AV17SistemaVersao_Id1 = cgiGet( edtavSistemaversao_id1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17SistemaVersao_Id1", AV17SistemaVersao_Id1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            AV21SistemaVersao_Id2 = cgiGet( edtavSistemaversao_id2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21SistemaVersao_Id2", AV21SistemaVersao_Id2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            AV25SistemaVersao_Id3 = cgiGet( edtavSistemaversao_id3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25SistemaVersao_Id3", AV25SistemaVersao_Id3);
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vMANAGEFILTERSEXECUTIONSTEP");
               GX_FocusControl = edtavManagefiltersexecutionstep_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV31ManageFiltersExecutionStep = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV31ManageFiltersExecutionStep), 1, 0));
            }
            else
            {
               AV31ManageFiltersExecutionStep = (short)(context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV31ManageFiltersExecutionStep), 1, 0));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsistemaversao_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsistemaversao_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSISTEMAVERSAO_CODIGO");
               GX_FocusControl = edtavTfsistemaversao_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV38TFSistemaVersao_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFSistemaVersao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFSistemaVersao_Codigo), 6, 0)));
            }
            else
            {
               AV38TFSistemaVersao_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfsistemaversao_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFSistemaVersao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFSistemaVersao_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsistemaversao_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsistemaversao_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSISTEMAVERSAO_CODIGO_TO");
               GX_FocusControl = edtavTfsistemaversao_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV39TFSistemaVersao_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFSistemaVersao_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFSistemaVersao_Codigo_To), 6, 0)));
            }
            else
            {
               AV39TFSistemaVersao_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfsistemaversao_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFSistemaVersao_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFSistemaVersao_Codigo_To), 6, 0)));
            }
            AV42TFSistemaVersao_Id = cgiGet( edtavTfsistemaversao_id_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFSistemaVersao_Id", AV42TFSistemaVersao_Id);
            AV43TFSistemaVersao_Id_Sel = cgiGet( edtavTfsistemaversao_id_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFSistemaVersao_Id_Sel", AV43TFSistemaVersao_Id_Sel);
            AV46TFSistemaVersao_Descricao = cgiGet( edtavTfsistemaversao_descricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFSistemaVersao_Descricao", AV46TFSistemaVersao_Descricao);
            AV47TFSistemaVersao_Descricao_Sel = cgiGet( edtavTfsistemaversao_descricao_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFSistemaVersao_Descricao_Sel", AV47TFSistemaVersao_Descricao_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsistemaversao_evolucao_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsistemaversao_evolucao_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSISTEMAVERSAO_EVOLUCAO");
               GX_FocusControl = edtavTfsistemaversao_evolucao_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV50TFSistemaVersao_Evolucao = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFSistemaVersao_Evolucao", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50TFSistemaVersao_Evolucao), 4, 0)));
            }
            else
            {
               AV50TFSistemaVersao_Evolucao = (short)(context.localUtil.CToN( cgiGet( edtavTfsistemaversao_evolucao_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFSistemaVersao_Evolucao", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50TFSistemaVersao_Evolucao), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsistemaversao_evolucao_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsistemaversao_evolucao_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSISTEMAVERSAO_EVOLUCAO_TO");
               GX_FocusControl = edtavTfsistemaversao_evolucao_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV51TFSistemaVersao_Evolucao_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFSistemaVersao_Evolucao_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFSistemaVersao_Evolucao_To), 4, 0)));
            }
            else
            {
               AV51TFSistemaVersao_Evolucao_To = (short)(context.localUtil.CToN( cgiGet( edtavTfsistemaversao_evolucao_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFSistemaVersao_Evolucao_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFSistemaVersao_Evolucao_To), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsistemaversao_melhoria_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsistemaversao_melhoria_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSISTEMAVERSAO_MELHORIA");
               GX_FocusControl = edtavTfsistemaversao_melhoria_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV54TFSistemaVersao_Melhoria = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFSistemaVersao_Melhoria", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54TFSistemaVersao_Melhoria), 4, 0)));
            }
            else
            {
               AV54TFSistemaVersao_Melhoria = (short)(context.localUtil.CToN( cgiGet( edtavTfsistemaversao_melhoria_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFSistemaVersao_Melhoria", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54TFSistemaVersao_Melhoria), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsistemaversao_melhoria_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsistemaversao_melhoria_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSISTEMAVERSAO_MELHORIA_TO");
               GX_FocusControl = edtavTfsistemaversao_melhoria_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV55TFSistemaVersao_Melhoria_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFSistemaVersao_Melhoria_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFSistemaVersao_Melhoria_To), 4, 0)));
            }
            else
            {
               AV55TFSistemaVersao_Melhoria_To = (short)(context.localUtil.CToN( cgiGet( edtavTfsistemaversao_melhoria_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFSistemaVersao_Melhoria_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFSistemaVersao_Melhoria_To), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsistemaversao_correcao_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsistemaversao_correcao_Internalname), ",", ".") > Convert.ToDecimal( 99999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSISTEMAVERSAO_CORRECAO");
               GX_FocusControl = edtavTfsistemaversao_correcao_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV58TFSistemaVersao_Correcao = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFSistemaVersao_Correcao", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58TFSistemaVersao_Correcao), 8, 0)));
            }
            else
            {
               AV58TFSistemaVersao_Correcao = (int)(context.localUtil.CToN( cgiGet( edtavTfsistemaversao_correcao_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFSistemaVersao_Correcao", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58TFSistemaVersao_Correcao), 8, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsistemaversao_correcao_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsistemaversao_correcao_to_Internalname), ",", ".") > Convert.ToDecimal( 99999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSISTEMAVERSAO_CORRECAO_TO");
               GX_FocusControl = edtavTfsistemaversao_correcao_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV59TFSistemaVersao_Correcao_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFSistemaVersao_Correcao_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59TFSistemaVersao_Correcao_To), 8, 0)));
            }
            else
            {
               AV59TFSistemaVersao_Correcao_To = (int)(context.localUtil.CToN( cgiGet( edtavTfsistemaversao_correcao_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFSistemaVersao_Correcao_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59TFSistemaVersao_Correcao_To), 8, 0)));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfsistemaversao_data_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFSistema Versao_Data"}), 1, "vTFSISTEMAVERSAO_DATA");
               GX_FocusControl = edtavTfsistemaversao_data_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV62TFSistemaVersao_Data = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFSistemaVersao_Data", context.localUtil.TToC( AV62TFSistemaVersao_Data, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV62TFSistemaVersao_Data = context.localUtil.CToT( cgiGet( edtavTfsistemaversao_data_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFSistemaVersao_Data", context.localUtil.TToC( AV62TFSistemaVersao_Data, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfsistemaversao_data_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFSistema Versao_Data_To"}), 1, "vTFSISTEMAVERSAO_DATA_TO");
               GX_FocusControl = edtavTfsistemaversao_data_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV63TFSistemaVersao_Data_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFSistemaVersao_Data_To", context.localUtil.TToC( AV63TFSistemaVersao_Data_To, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV63TFSistemaVersao_Data_To = context.localUtil.CToT( cgiGet( edtavTfsistemaversao_data_to_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFSistemaVersao_Data_To", context.localUtil.TToC( AV63TFSistemaVersao_Data_To, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_sistemaversao_dataauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Sistema Versao_Data Aux Date"}), 1, "vDDO_SISTEMAVERSAO_DATAAUXDATE");
               GX_FocusControl = edtavDdo_sistemaversao_dataauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV64DDO_SistemaVersao_DataAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64DDO_SistemaVersao_DataAuxDate", context.localUtil.Format(AV64DDO_SistemaVersao_DataAuxDate, "99/99/99"));
            }
            else
            {
               AV64DDO_SistemaVersao_DataAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_sistemaversao_dataauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64DDO_SistemaVersao_DataAuxDate", context.localUtil.Format(AV64DDO_SistemaVersao_DataAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_sistemaversao_dataauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Sistema Versao_Data Aux Date To"}), 1, "vDDO_SISTEMAVERSAO_DATAAUXDATETO");
               GX_FocusControl = edtavDdo_sistemaversao_dataauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV65DDO_SistemaVersao_DataAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65DDO_SistemaVersao_DataAuxDateTo", context.localUtil.Format(AV65DDO_SistemaVersao_DataAuxDateTo, "99/99/99"));
            }
            else
            {
               AV65DDO_SistemaVersao_DataAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_sistemaversao_dataauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65DDO_SistemaVersao_DataAuxDateTo", context.localUtil.Format(AV65DDO_SistemaVersao_DataAuxDateTo, "99/99/99"));
            }
            AV40ddo_SistemaVersao_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_sistemaversao_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ddo_SistemaVersao_CodigoTitleControlIdToReplace", AV40ddo_SistemaVersao_CodigoTitleControlIdToReplace);
            AV44ddo_SistemaVersao_IdTitleControlIdToReplace = cgiGet( edtavDdo_sistemaversao_idtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ddo_SistemaVersao_IdTitleControlIdToReplace", AV44ddo_SistemaVersao_IdTitleControlIdToReplace);
            AV48ddo_SistemaVersao_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_sistemaversao_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ddo_SistemaVersao_DescricaoTitleControlIdToReplace", AV48ddo_SistemaVersao_DescricaoTitleControlIdToReplace);
            AV52ddo_SistemaVersao_EvolucaoTitleControlIdToReplace = cgiGet( edtavDdo_sistemaversao_evolucaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ddo_SistemaVersao_EvolucaoTitleControlIdToReplace", AV52ddo_SistemaVersao_EvolucaoTitleControlIdToReplace);
            AV56ddo_SistemaVersao_MelhoriaTitleControlIdToReplace = cgiGet( edtavDdo_sistemaversao_melhoriatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ddo_SistemaVersao_MelhoriaTitleControlIdToReplace", AV56ddo_SistemaVersao_MelhoriaTitleControlIdToReplace);
            AV60ddo_SistemaVersao_CorrecaoTitleControlIdToReplace = cgiGet( edtavDdo_sistemaversao_correcaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60ddo_SistemaVersao_CorrecaoTitleControlIdToReplace", AV60ddo_SistemaVersao_CorrecaoTitleControlIdToReplace);
            AV66ddo_SistemaVersao_DataTitleControlIdToReplace = cgiGet( edtavDdo_sistemaversao_datatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ddo_SistemaVersao_DataTitleControlIdToReplace", AV66ddo_SistemaVersao_DataTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_88 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_88"), ",", "."));
            AV69GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV70GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Ddo_managefilters_Icon = cgiGet( "DDO_MANAGEFILTERS_Icon");
            Ddo_managefilters_Caption = cgiGet( "DDO_MANAGEFILTERS_Caption");
            Ddo_managefilters_Tooltip = cgiGet( "DDO_MANAGEFILTERS_Tooltip");
            Ddo_managefilters_Cls = cgiGet( "DDO_MANAGEFILTERS_Cls");
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_sistemaversao_codigo_Caption = cgiGet( "DDO_SISTEMAVERSAO_CODIGO_Caption");
            Ddo_sistemaversao_codigo_Tooltip = cgiGet( "DDO_SISTEMAVERSAO_CODIGO_Tooltip");
            Ddo_sistemaversao_codigo_Cls = cgiGet( "DDO_SISTEMAVERSAO_CODIGO_Cls");
            Ddo_sistemaversao_codigo_Filteredtext_set = cgiGet( "DDO_SISTEMAVERSAO_CODIGO_Filteredtext_set");
            Ddo_sistemaversao_codigo_Filteredtextto_set = cgiGet( "DDO_SISTEMAVERSAO_CODIGO_Filteredtextto_set");
            Ddo_sistemaversao_codigo_Dropdownoptionstype = cgiGet( "DDO_SISTEMAVERSAO_CODIGO_Dropdownoptionstype");
            Ddo_sistemaversao_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_SISTEMAVERSAO_CODIGO_Titlecontrolidtoreplace");
            Ddo_sistemaversao_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SISTEMAVERSAO_CODIGO_Includesortasc"));
            Ddo_sistemaversao_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SISTEMAVERSAO_CODIGO_Includesortdsc"));
            Ddo_sistemaversao_codigo_Sortedstatus = cgiGet( "DDO_SISTEMAVERSAO_CODIGO_Sortedstatus");
            Ddo_sistemaversao_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SISTEMAVERSAO_CODIGO_Includefilter"));
            Ddo_sistemaversao_codigo_Filtertype = cgiGet( "DDO_SISTEMAVERSAO_CODIGO_Filtertype");
            Ddo_sistemaversao_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SISTEMAVERSAO_CODIGO_Filterisrange"));
            Ddo_sistemaversao_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SISTEMAVERSAO_CODIGO_Includedatalist"));
            Ddo_sistemaversao_codigo_Sortasc = cgiGet( "DDO_SISTEMAVERSAO_CODIGO_Sortasc");
            Ddo_sistemaversao_codigo_Sortdsc = cgiGet( "DDO_SISTEMAVERSAO_CODIGO_Sortdsc");
            Ddo_sistemaversao_codigo_Cleanfilter = cgiGet( "DDO_SISTEMAVERSAO_CODIGO_Cleanfilter");
            Ddo_sistemaversao_codigo_Rangefilterfrom = cgiGet( "DDO_SISTEMAVERSAO_CODIGO_Rangefilterfrom");
            Ddo_sistemaversao_codigo_Rangefilterto = cgiGet( "DDO_SISTEMAVERSAO_CODIGO_Rangefilterto");
            Ddo_sistemaversao_codigo_Searchbuttontext = cgiGet( "DDO_SISTEMAVERSAO_CODIGO_Searchbuttontext");
            Ddo_sistemaversao_id_Caption = cgiGet( "DDO_SISTEMAVERSAO_ID_Caption");
            Ddo_sistemaversao_id_Tooltip = cgiGet( "DDO_SISTEMAVERSAO_ID_Tooltip");
            Ddo_sistemaversao_id_Cls = cgiGet( "DDO_SISTEMAVERSAO_ID_Cls");
            Ddo_sistemaversao_id_Filteredtext_set = cgiGet( "DDO_SISTEMAVERSAO_ID_Filteredtext_set");
            Ddo_sistemaversao_id_Selectedvalue_set = cgiGet( "DDO_SISTEMAVERSAO_ID_Selectedvalue_set");
            Ddo_sistemaversao_id_Dropdownoptionstype = cgiGet( "DDO_SISTEMAVERSAO_ID_Dropdownoptionstype");
            Ddo_sistemaversao_id_Titlecontrolidtoreplace = cgiGet( "DDO_SISTEMAVERSAO_ID_Titlecontrolidtoreplace");
            Ddo_sistemaversao_id_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SISTEMAVERSAO_ID_Includesortasc"));
            Ddo_sistemaversao_id_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SISTEMAVERSAO_ID_Includesortdsc"));
            Ddo_sistemaversao_id_Sortedstatus = cgiGet( "DDO_SISTEMAVERSAO_ID_Sortedstatus");
            Ddo_sistemaversao_id_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SISTEMAVERSAO_ID_Includefilter"));
            Ddo_sistemaversao_id_Filtertype = cgiGet( "DDO_SISTEMAVERSAO_ID_Filtertype");
            Ddo_sistemaversao_id_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SISTEMAVERSAO_ID_Filterisrange"));
            Ddo_sistemaversao_id_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SISTEMAVERSAO_ID_Includedatalist"));
            Ddo_sistemaversao_id_Datalisttype = cgiGet( "DDO_SISTEMAVERSAO_ID_Datalisttype");
            Ddo_sistemaversao_id_Datalistproc = cgiGet( "DDO_SISTEMAVERSAO_ID_Datalistproc");
            Ddo_sistemaversao_id_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_SISTEMAVERSAO_ID_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_sistemaversao_id_Sortasc = cgiGet( "DDO_SISTEMAVERSAO_ID_Sortasc");
            Ddo_sistemaversao_id_Sortdsc = cgiGet( "DDO_SISTEMAVERSAO_ID_Sortdsc");
            Ddo_sistemaversao_id_Loadingdata = cgiGet( "DDO_SISTEMAVERSAO_ID_Loadingdata");
            Ddo_sistemaversao_id_Cleanfilter = cgiGet( "DDO_SISTEMAVERSAO_ID_Cleanfilter");
            Ddo_sistemaversao_id_Noresultsfound = cgiGet( "DDO_SISTEMAVERSAO_ID_Noresultsfound");
            Ddo_sistemaversao_id_Searchbuttontext = cgiGet( "DDO_SISTEMAVERSAO_ID_Searchbuttontext");
            Ddo_sistemaversao_descricao_Caption = cgiGet( "DDO_SISTEMAVERSAO_DESCRICAO_Caption");
            Ddo_sistemaversao_descricao_Tooltip = cgiGet( "DDO_SISTEMAVERSAO_DESCRICAO_Tooltip");
            Ddo_sistemaversao_descricao_Cls = cgiGet( "DDO_SISTEMAVERSAO_DESCRICAO_Cls");
            Ddo_sistemaversao_descricao_Filteredtext_set = cgiGet( "DDO_SISTEMAVERSAO_DESCRICAO_Filteredtext_set");
            Ddo_sistemaversao_descricao_Selectedvalue_set = cgiGet( "DDO_SISTEMAVERSAO_DESCRICAO_Selectedvalue_set");
            Ddo_sistemaversao_descricao_Dropdownoptionstype = cgiGet( "DDO_SISTEMAVERSAO_DESCRICAO_Dropdownoptionstype");
            Ddo_sistemaversao_descricao_Titlecontrolidtoreplace = cgiGet( "DDO_SISTEMAVERSAO_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_sistemaversao_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SISTEMAVERSAO_DESCRICAO_Includesortasc"));
            Ddo_sistemaversao_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SISTEMAVERSAO_DESCRICAO_Includesortdsc"));
            Ddo_sistemaversao_descricao_Sortedstatus = cgiGet( "DDO_SISTEMAVERSAO_DESCRICAO_Sortedstatus");
            Ddo_sistemaversao_descricao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SISTEMAVERSAO_DESCRICAO_Includefilter"));
            Ddo_sistemaversao_descricao_Filtertype = cgiGet( "DDO_SISTEMAVERSAO_DESCRICAO_Filtertype");
            Ddo_sistemaversao_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SISTEMAVERSAO_DESCRICAO_Filterisrange"));
            Ddo_sistemaversao_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SISTEMAVERSAO_DESCRICAO_Includedatalist"));
            Ddo_sistemaversao_descricao_Datalisttype = cgiGet( "DDO_SISTEMAVERSAO_DESCRICAO_Datalisttype");
            Ddo_sistemaversao_descricao_Datalistproc = cgiGet( "DDO_SISTEMAVERSAO_DESCRICAO_Datalistproc");
            Ddo_sistemaversao_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_SISTEMAVERSAO_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_sistemaversao_descricao_Sortasc = cgiGet( "DDO_SISTEMAVERSAO_DESCRICAO_Sortasc");
            Ddo_sistemaversao_descricao_Sortdsc = cgiGet( "DDO_SISTEMAVERSAO_DESCRICAO_Sortdsc");
            Ddo_sistemaversao_descricao_Loadingdata = cgiGet( "DDO_SISTEMAVERSAO_DESCRICAO_Loadingdata");
            Ddo_sistemaversao_descricao_Cleanfilter = cgiGet( "DDO_SISTEMAVERSAO_DESCRICAO_Cleanfilter");
            Ddo_sistemaversao_descricao_Noresultsfound = cgiGet( "DDO_SISTEMAVERSAO_DESCRICAO_Noresultsfound");
            Ddo_sistemaversao_descricao_Searchbuttontext = cgiGet( "DDO_SISTEMAVERSAO_DESCRICAO_Searchbuttontext");
            Ddo_sistemaversao_evolucao_Caption = cgiGet( "DDO_SISTEMAVERSAO_EVOLUCAO_Caption");
            Ddo_sistemaversao_evolucao_Tooltip = cgiGet( "DDO_SISTEMAVERSAO_EVOLUCAO_Tooltip");
            Ddo_sistemaversao_evolucao_Cls = cgiGet( "DDO_SISTEMAVERSAO_EVOLUCAO_Cls");
            Ddo_sistemaversao_evolucao_Filteredtext_set = cgiGet( "DDO_SISTEMAVERSAO_EVOLUCAO_Filteredtext_set");
            Ddo_sistemaversao_evolucao_Filteredtextto_set = cgiGet( "DDO_SISTEMAVERSAO_EVOLUCAO_Filteredtextto_set");
            Ddo_sistemaversao_evolucao_Dropdownoptionstype = cgiGet( "DDO_SISTEMAVERSAO_EVOLUCAO_Dropdownoptionstype");
            Ddo_sistemaversao_evolucao_Titlecontrolidtoreplace = cgiGet( "DDO_SISTEMAVERSAO_EVOLUCAO_Titlecontrolidtoreplace");
            Ddo_sistemaversao_evolucao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SISTEMAVERSAO_EVOLUCAO_Includesortasc"));
            Ddo_sistemaversao_evolucao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SISTEMAVERSAO_EVOLUCAO_Includesortdsc"));
            Ddo_sistemaversao_evolucao_Sortedstatus = cgiGet( "DDO_SISTEMAVERSAO_EVOLUCAO_Sortedstatus");
            Ddo_sistemaversao_evolucao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SISTEMAVERSAO_EVOLUCAO_Includefilter"));
            Ddo_sistemaversao_evolucao_Filtertype = cgiGet( "DDO_SISTEMAVERSAO_EVOLUCAO_Filtertype");
            Ddo_sistemaversao_evolucao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SISTEMAVERSAO_EVOLUCAO_Filterisrange"));
            Ddo_sistemaversao_evolucao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SISTEMAVERSAO_EVOLUCAO_Includedatalist"));
            Ddo_sistemaversao_evolucao_Sortasc = cgiGet( "DDO_SISTEMAVERSAO_EVOLUCAO_Sortasc");
            Ddo_sistemaversao_evolucao_Sortdsc = cgiGet( "DDO_SISTEMAVERSAO_EVOLUCAO_Sortdsc");
            Ddo_sistemaversao_evolucao_Cleanfilter = cgiGet( "DDO_SISTEMAVERSAO_EVOLUCAO_Cleanfilter");
            Ddo_sistemaversao_evolucao_Rangefilterfrom = cgiGet( "DDO_SISTEMAVERSAO_EVOLUCAO_Rangefilterfrom");
            Ddo_sistemaversao_evolucao_Rangefilterto = cgiGet( "DDO_SISTEMAVERSAO_EVOLUCAO_Rangefilterto");
            Ddo_sistemaversao_evolucao_Searchbuttontext = cgiGet( "DDO_SISTEMAVERSAO_EVOLUCAO_Searchbuttontext");
            Ddo_sistemaversao_melhoria_Caption = cgiGet( "DDO_SISTEMAVERSAO_MELHORIA_Caption");
            Ddo_sistemaversao_melhoria_Tooltip = cgiGet( "DDO_SISTEMAVERSAO_MELHORIA_Tooltip");
            Ddo_sistemaversao_melhoria_Cls = cgiGet( "DDO_SISTEMAVERSAO_MELHORIA_Cls");
            Ddo_sistemaversao_melhoria_Filteredtext_set = cgiGet( "DDO_SISTEMAVERSAO_MELHORIA_Filteredtext_set");
            Ddo_sistemaversao_melhoria_Filteredtextto_set = cgiGet( "DDO_SISTEMAVERSAO_MELHORIA_Filteredtextto_set");
            Ddo_sistemaversao_melhoria_Dropdownoptionstype = cgiGet( "DDO_SISTEMAVERSAO_MELHORIA_Dropdownoptionstype");
            Ddo_sistemaversao_melhoria_Titlecontrolidtoreplace = cgiGet( "DDO_SISTEMAVERSAO_MELHORIA_Titlecontrolidtoreplace");
            Ddo_sistemaversao_melhoria_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SISTEMAVERSAO_MELHORIA_Includesortasc"));
            Ddo_sistemaversao_melhoria_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SISTEMAVERSAO_MELHORIA_Includesortdsc"));
            Ddo_sistemaversao_melhoria_Sortedstatus = cgiGet( "DDO_SISTEMAVERSAO_MELHORIA_Sortedstatus");
            Ddo_sistemaversao_melhoria_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SISTEMAVERSAO_MELHORIA_Includefilter"));
            Ddo_sistemaversao_melhoria_Filtertype = cgiGet( "DDO_SISTEMAVERSAO_MELHORIA_Filtertype");
            Ddo_sistemaversao_melhoria_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SISTEMAVERSAO_MELHORIA_Filterisrange"));
            Ddo_sistemaversao_melhoria_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SISTEMAVERSAO_MELHORIA_Includedatalist"));
            Ddo_sistemaversao_melhoria_Sortasc = cgiGet( "DDO_SISTEMAVERSAO_MELHORIA_Sortasc");
            Ddo_sistemaversao_melhoria_Sortdsc = cgiGet( "DDO_SISTEMAVERSAO_MELHORIA_Sortdsc");
            Ddo_sistemaversao_melhoria_Cleanfilter = cgiGet( "DDO_SISTEMAVERSAO_MELHORIA_Cleanfilter");
            Ddo_sistemaversao_melhoria_Rangefilterfrom = cgiGet( "DDO_SISTEMAVERSAO_MELHORIA_Rangefilterfrom");
            Ddo_sistemaversao_melhoria_Rangefilterto = cgiGet( "DDO_SISTEMAVERSAO_MELHORIA_Rangefilterto");
            Ddo_sistemaversao_melhoria_Searchbuttontext = cgiGet( "DDO_SISTEMAVERSAO_MELHORIA_Searchbuttontext");
            Ddo_sistemaversao_correcao_Caption = cgiGet( "DDO_SISTEMAVERSAO_CORRECAO_Caption");
            Ddo_sistemaversao_correcao_Tooltip = cgiGet( "DDO_SISTEMAVERSAO_CORRECAO_Tooltip");
            Ddo_sistemaversao_correcao_Cls = cgiGet( "DDO_SISTEMAVERSAO_CORRECAO_Cls");
            Ddo_sistemaversao_correcao_Filteredtext_set = cgiGet( "DDO_SISTEMAVERSAO_CORRECAO_Filteredtext_set");
            Ddo_sistemaversao_correcao_Filteredtextto_set = cgiGet( "DDO_SISTEMAVERSAO_CORRECAO_Filteredtextto_set");
            Ddo_sistemaversao_correcao_Dropdownoptionstype = cgiGet( "DDO_SISTEMAVERSAO_CORRECAO_Dropdownoptionstype");
            Ddo_sistemaversao_correcao_Titlecontrolidtoreplace = cgiGet( "DDO_SISTEMAVERSAO_CORRECAO_Titlecontrolidtoreplace");
            Ddo_sistemaversao_correcao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SISTEMAVERSAO_CORRECAO_Includesortasc"));
            Ddo_sistemaversao_correcao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SISTEMAVERSAO_CORRECAO_Includesortdsc"));
            Ddo_sistemaversao_correcao_Sortedstatus = cgiGet( "DDO_SISTEMAVERSAO_CORRECAO_Sortedstatus");
            Ddo_sistemaversao_correcao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SISTEMAVERSAO_CORRECAO_Includefilter"));
            Ddo_sistemaversao_correcao_Filtertype = cgiGet( "DDO_SISTEMAVERSAO_CORRECAO_Filtertype");
            Ddo_sistemaversao_correcao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SISTEMAVERSAO_CORRECAO_Filterisrange"));
            Ddo_sistemaversao_correcao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SISTEMAVERSAO_CORRECAO_Includedatalist"));
            Ddo_sistemaversao_correcao_Sortasc = cgiGet( "DDO_SISTEMAVERSAO_CORRECAO_Sortasc");
            Ddo_sistemaversao_correcao_Sortdsc = cgiGet( "DDO_SISTEMAVERSAO_CORRECAO_Sortdsc");
            Ddo_sistemaversao_correcao_Cleanfilter = cgiGet( "DDO_SISTEMAVERSAO_CORRECAO_Cleanfilter");
            Ddo_sistemaversao_correcao_Rangefilterfrom = cgiGet( "DDO_SISTEMAVERSAO_CORRECAO_Rangefilterfrom");
            Ddo_sistemaversao_correcao_Rangefilterto = cgiGet( "DDO_SISTEMAVERSAO_CORRECAO_Rangefilterto");
            Ddo_sistemaversao_correcao_Searchbuttontext = cgiGet( "DDO_SISTEMAVERSAO_CORRECAO_Searchbuttontext");
            Ddo_sistemaversao_data_Caption = cgiGet( "DDO_SISTEMAVERSAO_DATA_Caption");
            Ddo_sistemaversao_data_Tooltip = cgiGet( "DDO_SISTEMAVERSAO_DATA_Tooltip");
            Ddo_sistemaversao_data_Cls = cgiGet( "DDO_SISTEMAVERSAO_DATA_Cls");
            Ddo_sistemaversao_data_Filteredtext_set = cgiGet( "DDO_SISTEMAVERSAO_DATA_Filteredtext_set");
            Ddo_sistemaversao_data_Filteredtextto_set = cgiGet( "DDO_SISTEMAVERSAO_DATA_Filteredtextto_set");
            Ddo_sistemaversao_data_Dropdownoptionstype = cgiGet( "DDO_SISTEMAVERSAO_DATA_Dropdownoptionstype");
            Ddo_sistemaversao_data_Titlecontrolidtoreplace = cgiGet( "DDO_SISTEMAVERSAO_DATA_Titlecontrolidtoreplace");
            Ddo_sistemaversao_data_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SISTEMAVERSAO_DATA_Includesortasc"));
            Ddo_sistemaversao_data_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SISTEMAVERSAO_DATA_Includesortdsc"));
            Ddo_sistemaversao_data_Sortedstatus = cgiGet( "DDO_SISTEMAVERSAO_DATA_Sortedstatus");
            Ddo_sistemaversao_data_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SISTEMAVERSAO_DATA_Includefilter"));
            Ddo_sistemaversao_data_Filtertype = cgiGet( "DDO_SISTEMAVERSAO_DATA_Filtertype");
            Ddo_sistemaversao_data_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SISTEMAVERSAO_DATA_Filterisrange"));
            Ddo_sistemaversao_data_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SISTEMAVERSAO_DATA_Includedatalist"));
            Ddo_sistemaversao_data_Sortasc = cgiGet( "DDO_SISTEMAVERSAO_DATA_Sortasc");
            Ddo_sistemaversao_data_Sortdsc = cgiGet( "DDO_SISTEMAVERSAO_DATA_Sortdsc");
            Ddo_sistemaversao_data_Cleanfilter = cgiGet( "DDO_SISTEMAVERSAO_DATA_Cleanfilter");
            Ddo_sistemaversao_data_Rangefilterfrom = cgiGet( "DDO_SISTEMAVERSAO_DATA_Rangefilterfrom");
            Ddo_sistemaversao_data_Rangefilterto = cgiGet( "DDO_SISTEMAVERSAO_DATA_Rangefilterto");
            Ddo_sistemaversao_data_Searchbuttontext = cgiGet( "DDO_SISTEMAVERSAO_DATA_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_sistemaversao_codigo_Activeeventkey = cgiGet( "DDO_SISTEMAVERSAO_CODIGO_Activeeventkey");
            Ddo_sistemaversao_codigo_Filteredtext_get = cgiGet( "DDO_SISTEMAVERSAO_CODIGO_Filteredtext_get");
            Ddo_sistemaversao_codigo_Filteredtextto_get = cgiGet( "DDO_SISTEMAVERSAO_CODIGO_Filteredtextto_get");
            Ddo_sistemaversao_id_Activeeventkey = cgiGet( "DDO_SISTEMAVERSAO_ID_Activeeventkey");
            Ddo_sistemaversao_id_Filteredtext_get = cgiGet( "DDO_SISTEMAVERSAO_ID_Filteredtext_get");
            Ddo_sistemaversao_id_Selectedvalue_get = cgiGet( "DDO_SISTEMAVERSAO_ID_Selectedvalue_get");
            Ddo_sistemaversao_descricao_Activeeventkey = cgiGet( "DDO_SISTEMAVERSAO_DESCRICAO_Activeeventkey");
            Ddo_sistemaversao_descricao_Filteredtext_get = cgiGet( "DDO_SISTEMAVERSAO_DESCRICAO_Filteredtext_get");
            Ddo_sistemaversao_descricao_Selectedvalue_get = cgiGet( "DDO_SISTEMAVERSAO_DESCRICAO_Selectedvalue_get");
            Ddo_sistemaversao_evolucao_Activeeventkey = cgiGet( "DDO_SISTEMAVERSAO_EVOLUCAO_Activeeventkey");
            Ddo_sistemaversao_evolucao_Filteredtext_get = cgiGet( "DDO_SISTEMAVERSAO_EVOLUCAO_Filteredtext_get");
            Ddo_sistemaversao_evolucao_Filteredtextto_get = cgiGet( "DDO_SISTEMAVERSAO_EVOLUCAO_Filteredtextto_get");
            Ddo_sistemaversao_melhoria_Activeeventkey = cgiGet( "DDO_SISTEMAVERSAO_MELHORIA_Activeeventkey");
            Ddo_sistemaversao_melhoria_Filteredtext_get = cgiGet( "DDO_SISTEMAVERSAO_MELHORIA_Filteredtext_get");
            Ddo_sistemaversao_melhoria_Filteredtextto_get = cgiGet( "DDO_SISTEMAVERSAO_MELHORIA_Filteredtextto_get");
            Ddo_sistemaversao_correcao_Activeeventkey = cgiGet( "DDO_SISTEMAVERSAO_CORRECAO_Activeeventkey");
            Ddo_sistemaversao_correcao_Filteredtext_get = cgiGet( "DDO_SISTEMAVERSAO_CORRECAO_Filteredtext_get");
            Ddo_sistemaversao_correcao_Filteredtextto_get = cgiGet( "DDO_SISTEMAVERSAO_CORRECAO_Filteredtextto_get");
            Ddo_sistemaversao_data_Activeeventkey = cgiGet( "DDO_SISTEMAVERSAO_DATA_Activeeventkey");
            Ddo_sistemaversao_data_Filteredtext_get = cgiGet( "DDO_SISTEMAVERSAO_DATA_Filteredtext_get");
            Ddo_sistemaversao_data_Filteredtextto_get = cgiGet( "DDO_SISTEMAVERSAO_DATA_Filteredtextto_get");
            Ddo_managefilters_Activeeventkey = cgiGet( "DDO_MANAGEFILTERS_Activeeventkey");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSISTEMAVERSAO_ID1"), AV17SistemaVersao_Id1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSISTEMAVERSAO_ID2"), AV21SistemaVersao_Id2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSISTEMAVERSAO_ID3"), AV25SistemaVersao_Id3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSISTEMAVERSAO_CODIGO"), ",", ".") != Convert.ToDecimal( AV38TFSistemaVersao_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSISTEMAVERSAO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV39TFSistemaVersao_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSISTEMAVERSAO_ID"), AV42TFSistemaVersao_Id) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSISTEMAVERSAO_ID_SEL"), AV43TFSistemaVersao_Id_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSISTEMAVERSAO_DESCRICAO"), AV46TFSistemaVersao_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSISTEMAVERSAO_DESCRICAO_SEL"), AV47TFSistemaVersao_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSISTEMAVERSAO_EVOLUCAO"), ",", ".") != Convert.ToDecimal( AV50TFSistemaVersao_Evolucao )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSISTEMAVERSAO_EVOLUCAO_TO"), ",", ".") != Convert.ToDecimal( AV51TFSistemaVersao_Evolucao_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSISTEMAVERSAO_MELHORIA"), ",", ".") != Convert.ToDecimal( AV54TFSistemaVersao_Melhoria )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSISTEMAVERSAO_MELHORIA_TO"), ",", ".") != Convert.ToDecimal( AV55TFSistemaVersao_Melhoria_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSISTEMAVERSAO_CORRECAO"), ",", ".") != Convert.ToDecimal( AV58TFSistemaVersao_Correcao )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSISTEMAVERSAO_CORRECAO_TO"), ",", ".") != Convert.ToDecimal( AV59TFSistemaVersao_Correcao_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFSISTEMAVERSAO_DATA"), 0) != AV62TFSistemaVersao_Data )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFSISTEMAVERSAO_DATA_TO"), 0) != AV63TFSistemaVersao_Data_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E30OE2 */
         E30OE2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E30OE2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         if ( StringUtil.StrCmp(AV7HTTPRequest.Method, "GET") == 0 )
         {
            /* Execute user subroutine: 'LOADSAVEDFILTERS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            edtavManagefiltersexecutionstep_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavManagefiltersexecutionstep_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavManagefiltersexecutionstep_Visible), 5, 0)));
         }
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "SISTEMAVERSAO_ID";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "SISTEMAVERSAO_ID";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "SISTEMAVERSAO_ID";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfsistemaversao_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsistemaversao_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsistemaversao_codigo_Visible), 5, 0)));
         edtavTfsistemaversao_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsistemaversao_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsistemaversao_codigo_to_Visible), 5, 0)));
         edtavTfsistemaversao_id_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsistemaversao_id_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsistemaversao_id_Visible), 5, 0)));
         edtavTfsistemaversao_id_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsistemaversao_id_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsistemaversao_id_sel_Visible), 5, 0)));
         edtavTfsistemaversao_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsistemaversao_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsistemaversao_descricao_Visible), 5, 0)));
         edtavTfsistemaversao_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsistemaversao_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsistemaversao_descricao_sel_Visible), 5, 0)));
         edtavTfsistemaversao_evolucao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsistemaversao_evolucao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsistemaversao_evolucao_Visible), 5, 0)));
         edtavTfsistemaversao_evolucao_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsistemaversao_evolucao_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsistemaversao_evolucao_to_Visible), 5, 0)));
         edtavTfsistemaversao_melhoria_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsistemaversao_melhoria_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsistemaversao_melhoria_Visible), 5, 0)));
         edtavTfsistemaversao_melhoria_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsistemaversao_melhoria_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsistemaversao_melhoria_to_Visible), 5, 0)));
         edtavTfsistemaversao_correcao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsistemaversao_correcao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsistemaversao_correcao_Visible), 5, 0)));
         edtavTfsistemaversao_correcao_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsistemaversao_correcao_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsistemaversao_correcao_to_Visible), 5, 0)));
         edtavTfsistemaversao_data_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsistemaversao_data_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsistemaversao_data_Visible), 5, 0)));
         edtavTfsistemaversao_data_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsistemaversao_data_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsistemaversao_data_to_Visible), 5, 0)));
         Ddo_sistemaversao_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_SistemaVersao_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_codigo_Internalname, "TitleControlIdToReplace", Ddo_sistemaversao_codigo_Titlecontrolidtoreplace);
         AV40ddo_SistemaVersao_CodigoTitleControlIdToReplace = Ddo_sistemaversao_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ddo_SistemaVersao_CodigoTitleControlIdToReplace", AV40ddo_SistemaVersao_CodigoTitleControlIdToReplace);
         edtavDdo_sistemaversao_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_sistemaversao_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_sistemaversao_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_sistemaversao_id_Titlecontrolidtoreplace = subGrid_Internalname+"_SistemaVersao_Id";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_id_Internalname, "TitleControlIdToReplace", Ddo_sistemaversao_id_Titlecontrolidtoreplace);
         AV44ddo_SistemaVersao_IdTitleControlIdToReplace = Ddo_sistemaversao_id_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ddo_SistemaVersao_IdTitleControlIdToReplace", AV44ddo_SistemaVersao_IdTitleControlIdToReplace);
         edtavDdo_sistemaversao_idtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_sistemaversao_idtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_sistemaversao_idtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_sistemaversao_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_SistemaVersao_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_descricao_Internalname, "TitleControlIdToReplace", Ddo_sistemaversao_descricao_Titlecontrolidtoreplace);
         AV48ddo_SistemaVersao_DescricaoTitleControlIdToReplace = Ddo_sistemaversao_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ddo_SistemaVersao_DescricaoTitleControlIdToReplace", AV48ddo_SistemaVersao_DescricaoTitleControlIdToReplace);
         edtavDdo_sistemaversao_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_sistemaversao_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_sistemaversao_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_sistemaversao_evolucao_Titlecontrolidtoreplace = subGrid_Internalname+"_SistemaVersao_Evolucao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_evolucao_Internalname, "TitleControlIdToReplace", Ddo_sistemaversao_evolucao_Titlecontrolidtoreplace);
         AV52ddo_SistemaVersao_EvolucaoTitleControlIdToReplace = Ddo_sistemaversao_evolucao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ddo_SistemaVersao_EvolucaoTitleControlIdToReplace", AV52ddo_SistemaVersao_EvolucaoTitleControlIdToReplace);
         edtavDdo_sistemaversao_evolucaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_sistemaversao_evolucaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_sistemaversao_evolucaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_sistemaversao_melhoria_Titlecontrolidtoreplace = subGrid_Internalname+"_SistemaVersao_Melhoria";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_melhoria_Internalname, "TitleControlIdToReplace", Ddo_sistemaversao_melhoria_Titlecontrolidtoreplace);
         AV56ddo_SistemaVersao_MelhoriaTitleControlIdToReplace = Ddo_sistemaversao_melhoria_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ddo_SistemaVersao_MelhoriaTitleControlIdToReplace", AV56ddo_SistemaVersao_MelhoriaTitleControlIdToReplace);
         edtavDdo_sistemaversao_melhoriatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_sistemaversao_melhoriatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_sistemaversao_melhoriatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_sistemaversao_correcao_Titlecontrolidtoreplace = subGrid_Internalname+"_SistemaVersao_Correcao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_correcao_Internalname, "TitleControlIdToReplace", Ddo_sistemaversao_correcao_Titlecontrolidtoreplace);
         AV60ddo_SistemaVersao_CorrecaoTitleControlIdToReplace = Ddo_sistemaversao_correcao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60ddo_SistemaVersao_CorrecaoTitleControlIdToReplace", AV60ddo_SistemaVersao_CorrecaoTitleControlIdToReplace);
         edtavDdo_sistemaversao_correcaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_sistemaversao_correcaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_sistemaversao_correcaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_sistemaversao_data_Titlecontrolidtoreplace = subGrid_Internalname+"_SistemaVersao_Data";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_data_Internalname, "TitleControlIdToReplace", Ddo_sistemaversao_data_Titlecontrolidtoreplace);
         AV66ddo_SistemaVersao_DataTitleControlIdToReplace = Ddo_sistemaversao_data_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ddo_SistemaVersao_DataTitleControlIdToReplace", AV66ddo_SistemaVersao_DataTitleControlIdToReplace);
         edtavDdo_sistemaversao_datatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_sistemaversao_datatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_sistemaversao_datatitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Versionamento";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Vers�o", 0);
         cmbavOrderedby.addItem("2", "Vers�o", 0);
         cmbavOrderedby.addItem("3", "Descri��o", 0);
         cmbavOrderedby.addItem("4", "Evolu��o", 0);
         cmbavOrderedby.addItem("5", "Melhoria", 0);
         cmbavOrderedby.addItem("6", "Corre��o", 0);
         cmbavOrderedby.addItem("7", "Em", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         Ddo_managefilters_Icon = context.convertURL( (String)(context.GetImagePath( "5efb9e2b-46db-43f4-8f74-dd3f5818d30e", "", context.GetTheme( ))));
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_managefilters_Internalname, "Icon", Ddo_managefilters_Icon);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV67DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV67DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E31OE2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV37SistemaVersao_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV41SistemaVersao_IdTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV45SistemaVersao_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV49SistemaVersao_EvolucaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV53SistemaVersao_MelhoriaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV57SistemaVersao_CorrecaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV61SistemaVersao_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'CHECKSECURITYFORACTIONS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV31ManageFiltersExecutionStep == 1 )
         {
            AV31ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV31ManageFiltersExecutionStep), 1, 0));
         }
         else if ( AV31ManageFiltersExecutionStep == 2 )
         {
            AV31ManageFiltersExecutionStep = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV31ManageFiltersExecutionStep), 1, 0));
            /* Execute user subroutine: 'LOADSAVEDFILTERS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtSistemaVersao_Codigo_Titleformat = 2;
         edtSistemaVersao_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Vers�o", AV40ddo_SistemaVersao_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistemaVersao_Codigo_Internalname, "Title", edtSistemaVersao_Codigo_Title);
         edtSistemaVersao_Id_Titleformat = 2;
         edtSistemaVersao_Id_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Vers�o", AV44ddo_SistemaVersao_IdTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistemaVersao_Id_Internalname, "Title", edtSistemaVersao_Id_Title);
         edtSistemaVersao_Descricao_Titleformat = 2;
         edtSistemaVersao_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Descri��o", AV48ddo_SistemaVersao_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistemaVersao_Descricao_Internalname, "Title", edtSistemaVersao_Descricao_Title);
         edtSistemaVersao_Evolucao_Titleformat = 2;
         edtSistemaVersao_Evolucao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Evolu��o", AV52ddo_SistemaVersao_EvolucaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistemaVersao_Evolucao_Internalname, "Title", edtSistemaVersao_Evolucao_Title);
         edtSistemaVersao_Melhoria_Titleformat = 2;
         edtSistemaVersao_Melhoria_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Melhoria", AV56ddo_SistemaVersao_MelhoriaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistemaVersao_Melhoria_Internalname, "Title", edtSistemaVersao_Melhoria_Title);
         edtSistemaVersao_Correcao_Titleformat = 2;
         edtSistemaVersao_Correcao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Corre��o", AV60ddo_SistemaVersao_CorrecaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistemaVersao_Correcao_Internalname, "Title", edtSistemaVersao_Correcao_Title);
         edtSistemaVersao_Data_Titleformat = 2;
         edtSistemaVersao_Data_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Em", AV66ddo_SistemaVersao_DataTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistemaVersao_Data_Internalname, "Title", edtSistemaVersao_Data_Title);
         AV69GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV69GridCurrentPage), 10, 0)));
         AV70GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV70GridPageCount), 10, 0)));
         AV74WWSistemaVersaoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV75WWSistemaVersaoDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV76WWSistemaVersaoDS_3_Sistemaversao_id1 = AV17SistemaVersao_Id1;
         AV77WWSistemaVersaoDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV78WWSistemaVersaoDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV79WWSistemaVersaoDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV80WWSistemaVersaoDS_7_Sistemaversao_id2 = AV21SistemaVersao_Id2;
         AV81WWSistemaVersaoDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV82WWSistemaVersaoDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV83WWSistemaVersaoDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV84WWSistemaVersaoDS_11_Sistemaversao_id3 = AV25SistemaVersao_Id3;
         AV85WWSistemaVersaoDS_12_Tfsistemaversao_codigo = AV38TFSistemaVersao_Codigo;
         AV86WWSistemaVersaoDS_13_Tfsistemaversao_codigo_to = AV39TFSistemaVersao_Codigo_To;
         AV87WWSistemaVersaoDS_14_Tfsistemaversao_id = AV42TFSistemaVersao_Id;
         AV88WWSistemaVersaoDS_15_Tfsistemaversao_id_sel = AV43TFSistemaVersao_Id_Sel;
         AV89WWSistemaVersaoDS_16_Tfsistemaversao_descricao = AV46TFSistemaVersao_Descricao;
         AV90WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel = AV47TFSistemaVersao_Descricao_Sel;
         AV91WWSistemaVersaoDS_18_Tfsistemaversao_evolucao = AV50TFSistemaVersao_Evolucao;
         AV92WWSistemaVersaoDS_19_Tfsistemaversao_evolucao_to = AV51TFSistemaVersao_Evolucao_To;
         AV93WWSistemaVersaoDS_20_Tfsistemaversao_melhoria = AV54TFSistemaVersao_Melhoria;
         AV94WWSistemaVersaoDS_21_Tfsistemaversao_melhoria_to = AV55TFSistemaVersao_Melhoria_To;
         AV95WWSistemaVersaoDS_22_Tfsistemaversao_correcao = AV58TFSistemaVersao_Correcao;
         AV96WWSistemaVersaoDS_23_Tfsistemaversao_correcao_to = AV59TFSistemaVersao_Correcao_To;
         AV97WWSistemaVersaoDS_24_Tfsistemaversao_data = AV62TFSistemaVersao_Data;
         AV98WWSistemaVersaoDS_25_Tfsistemaversao_data_to = AV63TFSistemaVersao_Data_To;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV37SistemaVersao_CodigoTitleFilterData", AV37SistemaVersao_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV41SistemaVersao_IdTitleFilterData", AV41SistemaVersao_IdTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV45SistemaVersao_DescricaoTitleFilterData", AV45SistemaVersao_DescricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV49SistemaVersao_EvolucaoTitleFilterData", AV49SistemaVersao_EvolucaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV53SistemaVersao_MelhoriaTitleFilterData", AV53SistemaVersao_MelhoriaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV57SistemaVersao_CorrecaoTitleFilterData", AV57SistemaVersao_CorrecaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV61SistemaVersao_DataTitleFilterData", AV61SistemaVersao_DataTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35ManageFiltersData", AV35ManageFiltersData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E12OE2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV68PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV68PageToGo) ;
         }
      }

      protected void E13OE2( )
      {
         /* Ddo_sistemaversao_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_sistemaversao_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_sistemaversao_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_codigo_Internalname, "SortedStatus", Ddo_sistemaversao_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_sistemaversao_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_sistemaversao_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_codigo_Internalname, "SortedStatus", Ddo_sistemaversao_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_sistemaversao_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV38TFSistemaVersao_Codigo = (int)(NumberUtil.Val( Ddo_sistemaversao_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFSistemaVersao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFSistemaVersao_Codigo), 6, 0)));
            AV39TFSistemaVersao_Codigo_To = (int)(NumberUtil.Val( Ddo_sistemaversao_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFSistemaVersao_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFSistemaVersao_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14OE2( )
      {
         /* Ddo_sistemaversao_id_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_sistemaversao_id_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_sistemaversao_id_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_id_Internalname, "SortedStatus", Ddo_sistemaversao_id_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_sistemaversao_id_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_sistemaversao_id_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_id_Internalname, "SortedStatus", Ddo_sistemaversao_id_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_sistemaversao_id_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV42TFSistemaVersao_Id = Ddo_sistemaversao_id_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFSistemaVersao_Id", AV42TFSistemaVersao_Id);
            AV43TFSistemaVersao_Id_Sel = Ddo_sistemaversao_id_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFSistemaVersao_Id_Sel", AV43TFSistemaVersao_Id_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E15OE2( )
      {
         /* Ddo_sistemaversao_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_sistemaversao_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_sistemaversao_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_descricao_Internalname, "SortedStatus", Ddo_sistemaversao_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_sistemaversao_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_sistemaversao_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_descricao_Internalname, "SortedStatus", Ddo_sistemaversao_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_sistemaversao_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV46TFSistemaVersao_Descricao = Ddo_sistemaversao_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFSistemaVersao_Descricao", AV46TFSistemaVersao_Descricao);
            AV47TFSistemaVersao_Descricao_Sel = Ddo_sistemaversao_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFSistemaVersao_Descricao_Sel", AV47TFSistemaVersao_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E16OE2( )
      {
         /* Ddo_sistemaversao_evolucao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_sistemaversao_evolucao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_sistemaversao_evolucao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_evolucao_Internalname, "SortedStatus", Ddo_sistemaversao_evolucao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_sistemaversao_evolucao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_sistemaversao_evolucao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_evolucao_Internalname, "SortedStatus", Ddo_sistemaversao_evolucao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_sistemaversao_evolucao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV50TFSistemaVersao_Evolucao = (short)(NumberUtil.Val( Ddo_sistemaversao_evolucao_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFSistemaVersao_Evolucao", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50TFSistemaVersao_Evolucao), 4, 0)));
            AV51TFSistemaVersao_Evolucao_To = (short)(NumberUtil.Val( Ddo_sistemaversao_evolucao_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFSistemaVersao_Evolucao_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFSistemaVersao_Evolucao_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E17OE2( )
      {
         /* Ddo_sistemaversao_melhoria_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_sistemaversao_melhoria_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_sistemaversao_melhoria_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_melhoria_Internalname, "SortedStatus", Ddo_sistemaversao_melhoria_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_sistemaversao_melhoria_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_sistemaversao_melhoria_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_melhoria_Internalname, "SortedStatus", Ddo_sistemaversao_melhoria_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_sistemaversao_melhoria_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV54TFSistemaVersao_Melhoria = (short)(NumberUtil.Val( Ddo_sistemaversao_melhoria_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFSistemaVersao_Melhoria", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54TFSistemaVersao_Melhoria), 4, 0)));
            AV55TFSistemaVersao_Melhoria_To = (short)(NumberUtil.Val( Ddo_sistemaversao_melhoria_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFSistemaVersao_Melhoria_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFSistemaVersao_Melhoria_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E18OE2( )
      {
         /* Ddo_sistemaversao_correcao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_sistemaversao_correcao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_sistemaversao_correcao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_correcao_Internalname, "SortedStatus", Ddo_sistemaversao_correcao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_sistemaversao_correcao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_sistemaversao_correcao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_correcao_Internalname, "SortedStatus", Ddo_sistemaversao_correcao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_sistemaversao_correcao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV58TFSistemaVersao_Correcao = (int)(NumberUtil.Val( Ddo_sistemaversao_correcao_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFSistemaVersao_Correcao", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58TFSistemaVersao_Correcao), 8, 0)));
            AV59TFSistemaVersao_Correcao_To = (int)(NumberUtil.Val( Ddo_sistemaversao_correcao_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFSistemaVersao_Correcao_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59TFSistemaVersao_Correcao_To), 8, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E19OE2( )
      {
         /* Ddo_sistemaversao_data_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_sistemaversao_data_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_sistemaversao_data_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_data_Internalname, "SortedStatus", Ddo_sistemaversao_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_sistemaversao_data_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_sistemaversao_data_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_data_Internalname, "SortedStatus", Ddo_sistemaversao_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_sistemaversao_data_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV62TFSistemaVersao_Data = context.localUtil.CToT( Ddo_sistemaversao_data_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFSistemaVersao_Data", context.localUtil.TToC( AV62TFSistemaVersao_Data, 8, 5, 0, 3, "/", ":", " "));
            AV63TFSistemaVersao_Data_To = context.localUtil.CToT( Ddo_sistemaversao_data_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFSistemaVersao_Data_To", context.localUtil.TToC( AV63TFSistemaVersao_Data_To, 8, 5, 0, 3, "/", ":", " "));
            if ( ! (DateTime.MinValue==AV63TFSistemaVersao_Data_To) )
            {
               AV63TFSistemaVersao_Data_To = context.localUtil.YMDHMSToT( (short)(DateTimeUtil.Year( AV63TFSistemaVersao_Data_To)), (short)(DateTimeUtil.Month( AV63TFSistemaVersao_Data_To)), (short)(DateTimeUtil.Day( AV63TFSistemaVersao_Data_To)), 23, 59, 59);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFSistemaVersao_Data_To", context.localUtil.TToC( AV63TFSistemaVersao_Data_To, 8, 5, 0, 3, "/", ":", " "));
            }
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E32OE2( )
      {
         /* Grid_Load Routine */
         edtavUpdate_Tooltiptext = "Modifica";
         if ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam )
         {
            edtavUpdate_Link = formatLink("sistemaversao.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A1859SistemaVersao_Codigo) + "," + UrlEncode("" +AV71SistemaVersao_SistemaCod);
            AV28Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV28Update);
            AV99Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            edtavUpdate_Enabled = 1;
         }
         else
         {
            edtavUpdate_Link = "";
            AV28Update = context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV28Update);
            AV99Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( )));
            edtavUpdate_Enabled = 0;
         }
         edtavDelete_Tooltiptext = "Eliminar";
         if ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam )
         {
            edtavDelete_Link = formatLink("sistemaversao.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A1859SistemaVersao_Codigo) + "," + UrlEncode("" +AV71SistemaVersao_SistemaCod);
            AV29Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV29Delete);
            AV100Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavDelete_Enabled = 1;
         }
         else
         {
            edtavDelete_Link = "";
            AV29Delete = context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV29Delete);
            AV100Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( )));
            edtavDelete_Enabled = 0;
         }
         edtSistemaVersao_Id_Link = formatLink("viewsistemaversao.aspx") + "?" + UrlEncode("" +A1859SistemaVersao_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 88;
         }
         sendrow_882( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_88_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(88, GridRow);
         }
      }

      protected void E20OE2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E25OE2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E21OE2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17SistemaVersao_Id1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21SistemaVersao_Id2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25SistemaVersao_Id3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV38TFSistemaVersao_Codigo, AV39TFSistemaVersao_Codigo_To, AV42TFSistemaVersao_Id, AV43TFSistemaVersao_Id_Sel, AV46TFSistemaVersao_Descricao, AV47TFSistemaVersao_Descricao_Sel, AV50TFSistemaVersao_Evolucao, AV51TFSistemaVersao_Evolucao_To, AV54TFSistemaVersao_Melhoria, AV55TFSistemaVersao_Melhoria_To, AV58TFSistemaVersao_Correcao, AV59TFSistemaVersao_Correcao_To, AV62TFSistemaVersao_Data, AV63TFSistemaVersao_Data_To, AV31ManageFiltersExecutionStep, AV40ddo_SistemaVersao_CodigoTitleControlIdToReplace, AV44ddo_SistemaVersao_IdTitleControlIdToReplace, AV48ddo_SistemaVersao_DescricaoTitleControlIdToReplace, AV52ddo_SistemaVersao_EvolucaoTitleControlIdToReplace, AV56ddo_SistemaVersao_MelhoriaTitleControlIdToReplace, AV60ddo_SistemaVersao_CorrecaoTitleControlIdToReplace, AV66ddo_SistemaVersao_DataTitleControlIdToReplace, AV6WWPContext, AV101Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1859SistemaVersao_Codigo, AV71SistemaVersao_SistemaCod) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E26OE2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E27OE2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E22OE2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17SistemaVersao_Id1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21SistemaVersao_Id2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25SistemaVersao_Id3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV38TFSistemaVersao_Codigo, AV39TFSistemaVersao_Codigo_To, AV42TFSistemaVersao_Id, AV43TFSistemaVersao_Id_Sel, AV46TFSistemaVersao_Descricao, AV47TFSistemaVersao_Descricao_Sel, AV50TFSistemaVersao_Evolucao, AV51TFSistemaVersao_Evolucao_To, AV54TFSistemaVersao_Melhoria, AV55TFSistemaVersao_Melhoria_To, AV58TFSistemaVersao_Correcao, AV59TFSistemaVersao_Correcao_To, AV62TFSistemaVersao_Data, AV63TFSistemaVersao_Data_To, AV31ManageFiltersExecutionStep, AV40ddo_SistemaVersao_CodigoTitleControlIdToReplace, AV44ddo_SistemaVersao_IdTitleControlIdToReplace, AV48ddo_SistemaVersao_DescricaoTitleControlIdToReplace, AV52ddo_SistemaVersao_EvolucaoTitleControlIdToReplace, AV56ddo_SistemaVersao_MelhoriaTitleControlIdToReplace, AV60ddo_SistemaVersao_CorrecaoTitleControlIdToReplace, AV66ddo_SistemaVersao_DataTitleControlIdToReplace, AV6WWPContext, AV101Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1859SistemaVersao_Codigo, AV71SistemaVersao_SistemaCod) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E28OE2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E23OE2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17SistemaVersao_Id1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21SistemaVersao_Id2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25SistemaVersao_Id3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV38TFSistemaVersao_Codigo, AV39TFSistemaVersao_Codigo_To, AV42TFSistemaVersao_Id, AV43TFSistemaVersao_Id_Sel, AV46TFSistemaVersao_Descricao, AV47TFSistemaVersao_Descricao_Sel, AV50TFSistemaVersao_Evolucao, AV51TFSistemaVersao_Evolucao_To, AV54TFSistemaVersao_Melhoria, AV55TFSistemaVersao_Melhoria_To, AV58TFSistemaVersao_Correcao, AV59TFSistemaVersao_Correcao_To, AV62TFSistemaVersao_Data, AV63TFSistemaVersao_Data_To, AV31ManageFiltersExecutionStep, AV40ddo_SistemaVersao_CodigoTitleControlIdToReplace, AV44ddo_SistemaVersao_IdTitleControlIdToReplace, AV48ddo_SistemaVersao_DescricaoTitleControlIdToReplace, AV52ddo_SistemaVersao_EvolucaoTitleControlIdToReplace, AV56ddo_SistemaVersao_MelhoriaTitleControlIdToReplace, AV60ddo_SistemaVersao_CorrecaoTitleControlIdToReplace, AV66ddo_SistemaVersao_DataTitleControlIdToReplace, AV6WWPContext, AV101Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1859SistemaVersao_Codigo, AV71SistemaVersao_SistemaCod) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E29OE2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11OE2( )
      {
         /* Ddo_managefilters_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Clean#>") == 0 )
         {
            /* Execute user subroutine: 'CLEANFILTERS' */
            S242 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Save#>") == 0 )
         {
            /* Execute user subroutine: 'SAVEGRIDSTATE' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            context.PopUp(formatLink("wwpbaseobjects.savefilteras.aspx") + "?" + UrlEncode(StringUtil.RTrim("WWSistemaVersaoFilters")) + "," + UrlEncode(StringUtil.RTrim(AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3"))), new Object[] {});
            AV31ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV31ManageFiltersExecutionStep), 1, 0));
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Manage#>") == 0 )
         {
            context.PopUp(formatLink("wwpbaseobjects.managefilters.aspx") + "?" + UrlEncode(StringUtil.RTrim("WWSistemaVersaoFilters")), new Object[] {});
            AV31ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV31ManageFiltersExecutionStep), 1, 0));
            context.DoAjaxRefresh();
         }
         else
         {
            GXt_char2 = AV32ManageFiltersXml;
            new wwpbaseobjects.getfilterbyname(context ).execute(  "WWSistemaVersaoFilters",  Ddo_managefilters_Activeeventkey, out  GXt_char2) ;
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_managefilters_Internalname, "ActiveEventKey", Ddo_managefilters_Activeeventkey);
            AV32ManageFiltersXml = GXt_char2;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV32ManageFiltersXml)) )
            {
               GX_msglist.addItem("O filtro selecionado n�o existe mais.");
            }
            else
            {
               /* Execute user subroutine: 'CLEANFILTERS' */
               S242 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               new wwpbaseobjects.savegridstate(context ).execute(  AV101Pgmname+"GridState",  AV32ManageFiltersXml) ;
               AV10GridState.FromXml(AV32ManageFiltersXml, "");
               AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
               S172 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
               S252 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
               S232 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               subgrid_firstpage( ) ;
               context.DoAjaxRefresh();
            }
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E24OE2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("sistemaversao.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +AV71SistemaVersao_SistemaCod);
         context.wjLocDisableFrm = 1;
      }

      protected void S202( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_sistemaversao_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_codigo_Internalname, "SortedStatus", Ddo_sistemaversao_codigo_Sortedstatus);
         Ddo_sistemaversao_id_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_id_Internalname, "SortedStatus", Ddo_sistemaversao_id_Sortedstatus);
         Ddo_sistemaversao_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_descricao_Internalname, "SortedStatus", Ddo_sistemaversao_descricao_Sortedstatus);
         Ddo_sistemaversao_evolucao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_evolucao_Internalname, "SortedStatus", Ddo_sistemaversao_evolucao_Sortedstatus);
         Ddo_sistemaversao_melhoria_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_melhoria_Internalname, "SortedStatus", Ddo_sistemaversao_melhoria_Sortedstatus);
         Ddo_sistemaversao_correcao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_correcao_Internalname, "SortedStatus", Ddo_sistemaversao_correcao_Sortedstatus);
         Ddo_sistemaversao_data_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_data_Internalname, "SortedStatus", Ddo_sistemaversao_data_Sortedstatus);
      }

      protected void S172( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_sistemaversao_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_codigo_Internalname, "SortedStatus", Ddo_sistemaversao_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 1 )
         {
            Ddo_sistemaversao_id_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_id_Internalname, "SortedStatus", Ddo_sistemaversao_id_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_sistemaversao_descricao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_descricao_Internalname, "SortedStatus", Ddo_sistemaversao_descricao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_sistemaversao_evolucao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_evolucao_Internalname, "SortedStatus", Ddo_sistemaversao_evolucao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_sistemaversao_melhoria_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_melhoria_Internalname, "SortedStatus", Ddo_sistemaversao_melhoria_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_sistemaversao_correcao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_correcao_Internalname, "SortedStatus", Ddo_sistemaversao_correcao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 7 )
         {
            Ddo_sistemaversao_data_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_data_Internalname, "SortedStatus", Ddo_sistemaversao_data_Sortedstatus);
         }
      }

      protected void S182( )
      {
         /* 'CHECKSECURITYFORACTIONS' Routine */
         if ( ! ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam ) )
         {
            imgInsert_Link = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Link", imgInsert_Link);
            imgInsert_Bitmap = context.GetImagePath( "13d28f37-c579-4dd9-a404-ba51ab71d1e3", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Bitmap", context.convertURL( context.PathToRelativeUrl( imgInsert_Bitmap)));
            imgInsert_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Enabled), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavSistemaversao_id1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistemaversao_id1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistemaversao_id1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SISTEMAVERSAO_ID") == 0 )
         {
            edtavSistemaversao_id1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistemaversao_id1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistemaversao_id1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavSistemaversao_id2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistemaversao_id2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistemaversao_id2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SISTEMAVERSAO_ID") == 0 )
         {
            edtavSistemaversao_id2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistemaversao_id2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistemaversao_id2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S142( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavSistemaversao_id3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistemaversao_id3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistemaversao_id3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SISTEMAVERSAO_ID") == 0 )
         {
            edtavSistemaversao_id3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistemaversao_id3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistemaversao_id3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S222( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "SISTEMAVERSAO_ID";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV20DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         AV21SistemaVersao_Id2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21SistemaVersao_Id2", AV21SistemaVersao_Id2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "SISTEMAVERSAO_ID";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV24DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         AV25SistemaVersao_Id3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25SistemaVersao_Id3", AV25SistemaVersao_Id3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S112( )
      {
         /* 'LOADSAVEDFILTERS' Routine */
         AV35ManageFiltersData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV36ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV36ManageFiltersDataItem.gxTpr_Title = "Limpar filtros";
         AV36ManageFiltersDataItem.gxTpr_Eventkey = "<#Clean#>";
         AV36ManageFiltersDataItem.gxTpr_Isdivider = false;
         AV36ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "63d2ae92-4e43-4a70-af61-0943e39ea422", "", context.GetTheme( ))));
         AV36ManageFiltersDataItem.gxTpr_Jsonclickevent = "WWPDynFilterHideAll(3)";
         AV35ManageFiltersData.Add(AV36ManageFiltersDataItem, 0);
         AV36ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV36ManageFiltersDataItem.gxTpr_Title = "Salvar filtro como...";
         AV36ManageFiltersDataItem.gxTpr_Eventkey = "<#Save#>";
         AV36ManageFiltersDataItem.gxTpr_Isdivider = false;
         AV36ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "6eee63e8-73c7-4738-beee-f98e3a8d2841", "", context.GetTheme( ))));
         AV35ManageFiltersData.Add(AV36ManageFiltersDataItem, 0);
         AV36ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV36ManageFiltersDataItem.gxTpr_Isdivider = true;
         AV35ManageFiltersData.Add(AV36ManageFiltersDataItem, 0);
         AV33ManageFiltersItems.FromXml(new wwpbaseobjects.loadmanagefiltersstate(context).executeUdp(  "WWSistemaVersaoFilters"), "");
         AV102GXV1 = 1;
         while ( AV102GXV1 <= AV33ManageFiltersItems.Count )
         {
            AV34ManageFiltersItem = ((wwpbaseobjects.SdtGridStateCollection_Item)AV33ManageFiltersItems.Item(AV102GXV1));
            AV36ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV36ManageFiltersDataItem.gxTpr_Title = AV34ManageFiltersItem.gxTpr_Title;
            AV36ManageFiltersDataItem.gxTpr_Eventkey = AV34ManageFiltersItem.gxTpr_Title;
            AV36ManageFiltersDataItem.gxTpr_Isdivider = false;
            AV36ManageFiltersDataItem.gxTpr_Jsonclickevent = "WWPDynFilterHideAll(3)";
            AV35ManageFiltersData.Add(AV36ManageFiltersDataItem, 0);
            if ( AV35ManageFiltersData.Count == 13 )
            {
               if (true) break;
            }
            AV102GXV1 = (int)(AV102GXV1+1);
         }
         if ( AV35ManageFiltersData.Count > 3 )
         {
            AV36ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV36ManageFiltersDataItem.gxTpr_Isdivider = true;
            AV35ManageFiltersData.Add(AV36ManageFiltersDataItem, 0);
            AV36ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV36ManageFiltersDataItem.gxTpr_Title = "Gerenciar filtros";
            AV36ManageFiltersDataItem.gxTpr_Eventkey = "<#Manage#>";
            AV36ManageFiltersDataItem.gxTpr_Isdivider = false;
            AV36ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "653f6166-5d82-407a-af84-19e0dde65efd", "", context.GetTheme( ))));
            AV36ManageFiltersDataItem.gxTpr_Jsonclickevent = "";
            AV35ManageFiltersData.Add(AV36ManageFiltersDataItem, 0);
         }
      }

      protected void S242( )
      {
         /* 'CLEANFILTERS' Routine */
         AV38TFSistemaVersao_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFSistemaVersao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFSistemaVersao_Codigo), 6, 0)));
         Ddo_sistemaversao_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_codigo_Internalname, "FilteredText_set", Ddo_sistemaversao_codigo_Filteredtext_set);
         AV39TFSistemaVersao_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFSistemaVersao_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFSistemaVersao_Codigo_To), 6, 0)));
         Ddo_sistemaversao_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_codigo_Internalname, "FilteredTextTo_set", Ddo_sistemaversao_codigo_Filteredtextto_set);
         AV42TFSistemaVersao_Id = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFSistemaVersao_Id", AV42TFSistemaVersao_Id);
         Ddo_sistemaversao_id_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_id_Internalname, "FilteredText_set", Ddo_sistemaversao_id_Filteredtext_set);
         AV43TFSistemaVersao_Id_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFSistemaVersao_Id_Sel", AV43TFSistemaVersao_Id_Sel);
         Ddo_sistemaversao_id_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_id_Internalname, "SelectedValue_set", Ddo_sistemaversao_id_Selectedvalue_set);
         AV46TFSistemaVersao_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFSistemaVersao_Descricao", AV46TFSistemaVersao_Descricao);
         Ddo_sistemaversao_descricao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_descricao_Internalname, "FilteredText_set", Ddo_sistemaversao_descricao_Filteredtext_set);
         AV47TFSistemaVersao_Descricao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFSistemaVersao_Descricao_Sel", AV47TFSistemaVersao_Descricao_Sel);
         Ddo_sistemaversao_descricao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_descricao_Internalname, "SelectedValue_set", Ddo_sistemaversao_descricao_Selectedvalue_set);
         AV50TFSistemaVersao_Evolucao = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFSistemaVersao_Evolucao", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50TFSistemaVersao_Evolucao), 4, 0)));
         Ddo_sistemaversao_evolucao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_evolucao_Internalname, "FilteredText_set", Ddo_sistemaversao_evolucao_Filteredtext_set);
         AV51TFSistemaVersao_Evolucao_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFSistemaVersao_Evolucao_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFSistemaVersao_Evolucao_To), 4, 0)));
         Ddo_sistemaversao_evolucao_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_evolucao_Internalname, "FilteredTextTo_set", Ddo_sistemaversao_evolucao_Filteredtextto_set);
         AV54TFSistemaVersao_Melhoria = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFSistemaVersao_Melhoria", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54TFSistemaVersao_Melhoria), 4, 0)));
         Ddo_sistemaversao_melhoria_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_melhoria_Internalname, "FilteredText_set", Ddo_sistemaversao_melhoria_Filteredtext_set);
         AV55TFSistemaVersao_Melhoria_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFSistemaVersao_Melhoria_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFSistemaVersao_Melhoria_To), 4, 0)));
         Ddo_sistemaversao_melhoria_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_melhoria_Internalname, "FilteredTextTo_set", Ddo_sistemaversao_melhoria_Filteredtextto_set);
         AV58TFSistemaVersao_Correcao = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFSistemaVersao_Correcao", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58TFSistemaVersao_Correcao), 8, 0)));
         Ddo_sistemaversao_correcao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_correcao_Internalname, "FilteredText_set", Ddo_sistemaversao_correcao_Filteredtext_set);
         AV59TFSistemaVersao_Correcao_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFSistemaVersao_Correcao_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59TFSistemaVersao_Correcao_To), 8, 0)));
         Ddo_sistemaversao_correcao_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_correcao_Internalname, "FilteredTextTo_set", Ddo_sistemaversao_correcao_Filteredtextto_set);
         AV62TFSistemaVersao_Data = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFSistemaVersao_Data", context.localUtil.TToC( AV62TFSistemaVersao_Data, 8, 5, 0, 3, "/", ":", " "));
         Ddo_sistemaversao_data_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_data_Internalname, "FilteredText_set", Ddo_sistemaversao_data_Filteredtext_set);
         AV63TFSistemaVersao_Data_To = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFSistemaVersao_Data_To", context.localUtil.TToC( AV63TFSistemaVersao_Data_To, 8, 5, 0, 3, "/", ":", " "));
         Ddo_sistemaversao_data_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_data_Internalname, "FilteredTextTo_set", Ddo_sistemaversao_data_Filteredtextto_set);
         AV15DynamicFiltersSelector1 = "SISTEMAVERSAO_ID";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         AV17SistemaVersao_Id1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17SistemaVersao_Id1", AV17SistemaVersao_Id1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S162( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV30Session.Get(AV101Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV101Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV30Session.Get(AV101Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S252 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S252( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV103GXV2 = 1;
         while ( AV103GXV2 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV103GXV2));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSISTEMAVERSAO_CODIGO") == 0 )
            {
               AV38TFSistemaVersao_Codigo = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFSistemaVersao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFSistemaVersao_Codigo), 6, 0)));
               AV39TFSistemaVersao_Codigo_To = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFSistemaVersao_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFSistemaVersao_Codigo_To), 6, 0)));
               if ( ! (0==AV38TFSistemaVersao_Codigo) )
               {
                  Ddo_sistemaversao_codigo_Filteredtext_set = StringUtil.Str( (decimal)(AV38TFSistemaVersao_Codigo), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_codigo_Internalname, "FilteredText_set", Ddo_sistemaversao_codigo_Filteredtext_set);
               }
               if ( ! (0==AV39TFSistemaVersao_Codigo_To) )
               {
                  Ddo_sistemaversao_codigo_Filteredtextto_set = StringUtil.Str( (decimal)(AV39TFSistemaVersao_Codigo_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_codigo_Internalname, "FilteredTextTo_set", Ddo_sistemaversao_codigo_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSISTEMAVERSAO_ID") == 0 )
            {
               AV42TFSistemaVersao_Id = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFSistemaVersao_Id", AV42TFSistemaVersao_Id);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42TFSistemaVersao_Id)) )
               {
                  Ddo_sistemaversao_id_Filteredtext_set = AV42TFSistemaVersao_Id;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_id_Internalname, "FilteredText_set", Ddo_sistemaversao_id_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSISTEMAVERSAO_ID_SEL") == 0 )
            {
               AV43TFSistemaVersao_Id_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFSistemaVersao_Id_Sel", AV43TFSistemaVersao_Id_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFSistemaVersao_Id_Sel)) )
               {
                  Ddo_sistemaversao_id_Selectedvalue_set = AV43TFSistemaVersao_Id_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_id_Internalname, "SelectedValue_set", Ddo_sistemaversao_id_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSISTEMAVERSAO_DESCRICAO") == 0 )
            {
               AV46TFSistemaVersao_Descricao = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFSistemaVersao_Descricao", AV46TFSistemaVersao_Descricao);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46TFSistemaVersao_Descricao)) )
               {
                  Ddo_sistemaversao_descricao_Filteredtext_set = AV46TFSistemaVersao_Descricao;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_descricao_Internalname, "FilteredText_set", Ddo_sistemaversao_descricao_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSISTEMAVERSAO_DESCRICAO_SEL") == 0 )
            {
               AV47TFSistemaVersao_Descricao_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFSistemaVersao_Descricao_Sel", AV47TFSistemaVersao_Descricao_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47TFSistemaVersao_Descricao_Sel)) )
               {
                  Ddo_sistemaversao_descricao_Selectedvalue_set = AV47TFSistemaVersao_Descricao_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_descricao_Internalname, "SelectedValue_set", Ddo_sistemaversao_descricao_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSISTEMAVERSAO_EVOLUCAO") == 0 )
            {
               AV50TFSistemaVersao_Evolucao = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFSistemaVersao_Evolucao", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50TFSistemaVersao_Evolucao), 4, 0)));
               AV51TFSistemaVersao_Evolucao_To = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFSistemaVersao_Evolucao_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFSistemaVersao_Evolucao_To), 4, 0)));
               if ( ! (0==AV50TFSistemaVersao_Evolucao) )
               {
                  Ddo_sistemaversao_evolucao_Filteredtext_set = StringUtil.Str( (decimal)(AV50TFSistemaVersao_Evolucao), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_evolucao_Internalname, "FilteredText_set", Ddo_sistemaversao_evolucao_Filteredtext_set);
               }
               if ( ! (0==AV51TFSistemaVersao_Evolucao_To) )
               {
                  Ddo_sistemaversao_evolucao_Filteredtextto_set = StringUtil.Str( (decimal)(AV51TFSistemaVersao_Evolucao_To), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_evolucao_Internalname, "FilteredTextTo_set", Ddo_sistemaversao_evolucao_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSISTEMAVERSAO_MELHORIA") == 0 )
            {
               AV54TFSistemaVersao_Melhoria = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFSistemaVersao_Melhoria", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54TFSistemaVersao_Melhoria), 4, 0)));
               AV55TFSistemaVersao_Melhoria_To = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFSistemaVersao_Melhoria_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFSistemaVersao_Melhoria_To), 4, 0)));
               if ( ! (0==AV54TFSistemaVersao_Melhoria) )
               {
                  Ddo_sistemaversao_melhoria_Filteredtext_set = StringUtil.Str( (decimal)(AV54TFSistemaVersao_Melhoria), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_melhoria_Internalname, "FilteredText_set", Ddo_sistemaversao_melhoria_Filteredtext_set);
               }
               if ( ! (0==AV55TFSistemaVersao_Melhoria_To) )
               {
                  Ddo_sistemaversao_melhoria_Filteredtextto_set = StringUtil.Str( (decimal)(AV55TFSistemaVersao_Melhoria_To), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_melhoria_Internalname, "FilteredTextTo_set", Ddo_sistemaversao_melhoria_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSISTEMAVERSAO_CORRECAO") == 0 )
            {
               AV58TFSistemaVersao_Correcao = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFSistemaVersao_Correcao", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58TFSistemaVersao_Correcao), 8, 0)));
               AV59TFSistemaVersao_Correcao_To = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFSistemaVersao_Correcao_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59TFSistemaVersao_Correcao_To), 8, 0)));
               if ( ! (0==AV58TFSistemaVersao_Correcao) )
               {
                  Ddo_sistemaversao_correcao_Filteredtext_set = StringUtil.Str( (decimal)(AV58TFSistemaVersao_Correcao), 8, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_correcao_Internalname, "FilteredText_set", Ddo_sistemaversao_correcao_Filteredtext_set);
               }
               if ( ! (0==AV59TFSistemaVersao_Correcao_To) )
               {
                  Ddo_sistemaversao_correcao_Filteredtextto_set = StringUtil.Str( (decimal)(AV59TFSistemaVersao_Correcao_To), 8, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_correcao_Internalname, "FilteredTextTo_set", Ddo_sistemaversao_correcao_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSISTEMAVERSAO_DATA") == 0 )
            {
               AV62TFSistemaVersao_Data = context.localUtil.CToT( AV11GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFSistemaVersao_Data", context.localUtil.TToC( AV62TFSistemaVersao_Data, 8, 5, 0, 3, "/", ":", " "));
               AV63TFSistemaVersao_Data_To = context.localUtil.CToT( AV11GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFSistemaVersao_Data_To", context.localUtil.TToC( AV63TFSistemaVersao_Data_To, 8, 5, 0, 3, "/", ":", " "));
               if ( ! (DateTime.MinValue==AV62TFSistemaVersao_Data) )
               {
                  AV64DDO_SistemaVersao_DataAuxDate = DateTimeUtil.ResetTime(AV62TFSistemaVersao_Data);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64DDO_SistemaVersao_DataAuxDate", context.localUtil.Format(AV64DDO_SistemaVersao_DataAuxDate, "99/99/99"));
                  Ddo_sistemaversao_data_Filteredtext_set = context.localUtil.DToC( AV64DDO_SistemaVersao_DataAuxDate, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_data_Internalname, "FilteredText_set", Ddo_sistemaversao_data_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV63TFSistemaVersao_Data_To) )
               {
                  AV65DDO_SistemaVersao_DataAuxDateTo = DateTimeUtil.ResetTime(AV63TFSistemaVersao_Data_To);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65DDO_SistemaVersao_DataAuxDateTo", context.localUtil.Format(AV65DDO_SistemaVersao_DataAuxDateTo, "99/99/99"));
                  Ddo_sistemaversao_data_Filteredtextto_set = context.localUtil.DToC( AV65DDO_SistemaVersao_DataAuxDateTo, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_data_Internalname, "FilteredTextTo_set", Ddo_sistemaversao_data_Filteredtextto_set);
               }
            }
            AV103GXV2 = (int)(AV103GXV2+1);
         }
      }

      protected void S232( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SISTEMAVERSAO_ID") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17SistemaVersao_Id1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17SistemaVersao_Id1", AV17SistemaVersao_Id1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SISTEMAVERSAO_ID") == 0 )
               {
                  AV20DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
                  AV21SistemaVersao_Id2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21SistemaVersao_Id2", AV21SistemaVersao_Id2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S132 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SISTEMAVERSAO_ID") == 0 )
                  {
                     AV24DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
                     AV25SistemaVersao_Id3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25SistemaVersao_Id3", AV25SistemaVersao_Id3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S142 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S192( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV30Session.Get(AV101Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (0==AV38TFSistemaVersao_Codigo) && (0==AV39TFSistemaVersao_Codigo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSISTEMAVERSAO_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV38TFSistemaVersao_Codigo), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV39TFSistemaVersao_Codigo_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42TFSistemaVersao_Id)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSISTEMAVERSAO_ID";
            AV11GridStateFilterValue.gxTpr_Value = AV42TFSistemaVersao_Id;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFSistemaVersao_Id_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSISTEMAVERSAO_ID_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV43TFSistemaVersao_Id_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46TFSistemaVersao_Descricao)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSISTEMAVERSAO_DESCRICAO";
            AV11GridStateFilterValue.gxTpr_Value = AV46TFSistemaVersao_Descricao;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47TFSistemaVersao_Descricao_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSISTEMAVERSAO_DESCRICAO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV47TFSistemaVersao_Descricao_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV50TFSistemaVersao_Evolucao) && (0==AV51TFSistemaVersao_Evolucao_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSISTEMAVERSAO_EVOLUCAO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV50TFSistemaVersao_Evolucao), 4, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV51TFSistemaVersao_Evolucao_To), 4, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV54TFSistemaVersao_Melhoria) && (0==AV55TFSistemaVersao_Melhoria_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSISTEMAVERSAO_MELHORIA";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV54TFSistemaVersao_Melhoria), 4, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV55TFSistemaVersao_Melhoria_To), 4, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV58TFSistemaVersao_Correcao) && (0==AV59TFSistemaVersao_Correcao_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSISTEMAVERSAO_CORRECAO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV58TFSistemaVersao_Correcao), 8, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV59TFSistemaVersao_Correcao_To), 8, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV62TFSistemaVersao_Data) && (DateTime.MinValue==AV63TFSistemaVersao_Data_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSISTEMAVERSAO_DATA";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.TToC( AV62TFSistemaVersao_Data, 8, 5, 0, 3, "/", ":", " ");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.TToC( AV63TFSistemaVersao_Data_To, 8, 5, 0, 3, "/", ":", " ");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV101Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S212( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SISTEMAVERSAO_ID") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17SistemaVersao_Id1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17SistemaVersao_Id1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SISTEMAVERSAO_ID") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV21SistemaVersao_Id2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV21SistemaVersao_Id2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV20DynamicFiltersOperator2;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SISTEMAVERSAO_ID") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25SistemaVersao_Id3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV25SistemaVersao_Id3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV24DynamicFiltersOperator3;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S152( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV101Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "SistemaVersao";
         AV30Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_OE2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_OE2( true) ;
         }
         else
         {
            wb_table2_8_OE2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_OE2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_82_OE2( true) ;
         }
         else
         {
            wb_table3_82_OE2( false) ;
         }
         return  ;
      }

      protected void wb_table3_82_OE2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_OE2e( true) ;
         }
         else
         {
            wb_table1_2_OE2e( false) ;
         }
      }

      protected void wb_table3_82_OE2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "TableGridHeader", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_85_OE2( true) ;
         }
         else
         {
            wb_table4_85_OE2( false) ;
         }
         return  ;
      }

      protected void wb_table4_85_OE2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_82_OE2e( true) ;
         }
         else
         {
            wb_table3_82_OE2e( false) ;
         }
      }

      protected void wb_table4_85_OE2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"88\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSistemaVersao_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtSistemaVersao_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSistemaVersao_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSistemaVersao_Id_Titleformat == 0 )
               {
                  context.SendWebValue( edtSistemaVersao_Id_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSistemaVersao_Id_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSistemaVersao_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtSistemaVersao_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSistemaVersao_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(55), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSistemaVersao_Evolucao_Titleformat == 0 )
               {
                  context.SendWebValue( edtSistemaVersao_Evolucao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSistemaVersao_Evolucao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(57), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSistemaVersao_Melhoria_Titleformat == 0 )
               {
                  context.SendWebValue( edtSistemaVersao_Melhoria_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSistemaVersao_Melhoria_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(58), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSistemaVersao_Correcao_Titleformat == 0 )
               {
                  context.SendWebValue( edtSistemaVersao_Correcao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSistemaVersao_Correcao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSistemaVersao_Data_Titleformat == 0 )
               {
                  context.SendWebValue( edtSistemaVersao_Data_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSistemaVersao_Data_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Update));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Delete));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1859SistemaVersao_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSistemaVersao_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSistemaVersao_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1860SistemaVersao_Id));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSistemaVersao_Id_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSistemaVersao_Id_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtSistemaVersao_Id_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A1861SistemaVersao_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSistemaVersao_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSistemaVersao_Descricao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1862SistemaVersao_Evolucao), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSistemaVersao_Evolucao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSistemaVersao_Evolucao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1863SistemaVersao_Melhoria), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSistemaVersao_Melhoria_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSistemaVersao_Melhoria_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1864SistemaVersao_Correcao), 8, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSistemaVersao_Correcao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSistemaVersao_Correcao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A1865SistemaVersao_Data, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSistemaVersao_Data_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSistemaVersao_Data_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 88 )
         {
            wbEnd = 0;
            nRC_GXsfl_88 = (short)(nGXsfl_88_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_85_OE2e( true) ;
         }
         else
         {
            wb_table4_85_OE2e( false) ;
         }
      }

      protected void wb_table2_8_OE2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table5_11_OE2( true) ;
         }
         else
         {
            wb_table5_11_OE2( false) ;
         }
         return  ;
      }

      protected void wb_table5_11_OE2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_23_OE2( true) ;
         }
         else
         {
            wb_table6_23_OE2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_OE2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_OE2e( true) ;
         }
         else
         {
            wb_table2_8_OE2e( false) ;
         }
      }

      protected void wb_table6_23_OE2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "TableFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_MANAGEFILTERSContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_OE2( true) ;
         }
         else
         {
            wb_table7_28_OE2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_OE2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWSistemaVersao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_OE2e( true) ;
         }
         else
         {
            wb_table6_23_OE2e( false) ;
         }
      }

      protected void wb_table7_28_OE2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWSistemaVersao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WWSistemaVersao.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWSistemaVersao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_37_OE2( true) ;
         }
         else
         {
            wb_table8_37_OE2( false) ;
         }
         return  ;
      }

      protected void wb_table8_37_OE2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWSistemaVersao.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWSistemaVersao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWSistemaVersao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", "", true, "HLP_WWSistemaVersao.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWSistemaVersao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_54_OE2( true) ;
         }
         else
         {
            wb_table9_54_OE2( false) ;
         }
         return  ;
      }

      protected void wb_table9_54_OE2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWSistemaVersao.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWSistemaVersao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWSistemaVersao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,67);\"", "", true, "HLP_WWSistemaVersao.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWSistemaVersao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_71_OE2( true) ;
         }
         else
         {
            wb_table10_71_OE2( false) ;
         }
         return  ;
      }

      protected void wb_table10_71_OE2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWSistemaVersao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_OE2e( true) ;
         }
         else
         {
            wb_table7_28_OE2e( false) ;
         }
      }

      protected void wb_table10_71_OE2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", "", true, "HLP_WWSistemaVersao.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSistemaversao_id3_Internalname, StringUtil.RTrim( AV25SistemaVersao_Id3), StringUtil.RTrim( context.localUtil.Format( AV25SistemaVersao_Id3, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,76);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSistemaversao_id3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavSistemaversao_id3_Visible, 1, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWSistemaVersao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_71_OE2e( true) ;
         }
         else
         {
            wb_table10_71_OE2e( false) ;
         }
      }

      protected void wb_table9_54_OE2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,57);\"", "", true, "HLP_WWSistemaVersao.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSistemaversao_id2_Internalname, StringUtil.RTrim( AV21SistemaVersao_Id2), StringUtil.RTrim( context.localUtil.Format( AV21SistemaVersao_Id2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSistemaversao_id2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavSistemaversao_id2_Visible, 1, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWSistemaVersao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_54_OE2e( true) ;
         }
         else
         {
            wb_table9_54_OE2e( false) ;
         }
      }

      protected void wb_table8_37_OE2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", "", true, "HLP_WWSistemaVersao.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSistemaversao_id1_Internalname, StringUtil.RTrim( AV17SistemaVersao_Id1), StringUtil.RTrim( context.localUtil.Format( AV17SistemaVersao_Id1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSistemaversao_id1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavSistemaversao_id1_Visible, 1, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWSistemaVersao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_37_OE2e( true) ;
         }
         else
         {
            wb_table8_37_OE2e( false) ;
         }
      }

      protected void wb_table5_11_OE2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblSistemaversaotitle_Internalname, "Versionamento", "", "", lblSistemaversaotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWSistemaVersao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, imgInsert_Bitmap, "", "", "", context.GetTheme( ), 1, imgInsert_Enabled, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWSistemaVersao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWSistemaVersao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWSistemaVersao.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWSistemaVersao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_11_OE2e( true) ;
         }
         else
         {
            wb_table5_11_OE2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAOE2( ) ;
         WSOE2( ) ;
         WEOE2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202051813383763");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwsistemaversao.js", "?202051813383764");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_882( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_88_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_88_idx;
         edtSistemaVersao_Codigo_Internalname = "SISTEMAVERSAO_CODIGO_"+sGXsfl_88_idx;
         edtSistemaVersao_Id_Internalname = "SISTEMAVERSAO_ID_"+sGXsfl_88_idx;
         edtSistemaVersao_Descricao_Internalname = "SISTEMAVERSAO_DESCRICAO_"+sGXsfl_88_idx;
         edtSistemaVersao_Evolucao_Internalname = "SISTEMAVERSAO_EVOLUCAO_"+sGXsfl_88_idx;
         edtSistemaVersao_Melhoria_Internalname = "SISTEMAVERSAO_MELHORIA_"+sGXsfl_88_idx;
         edtSistemaVersao_Correcao_Internalname = "SISTEMAVERSAO_CORRECAO_"+sGXsfl_88_idx;
         edtSistemaVersao_Data_Internalname = "SISTEMAVERSAO_DATA_"+sGXsfl_88_idx;
      }

      protected void SubsflControlProps_fel_882( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_88_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_88_fel_idx;
         edtSistemaVersao_Codigo_Internalname = "SISTEMAVERSAO_CODIGO_"+sGXsfl_88_fel_idx;
         edtSistemaVersao_Id_Internalname = "SISTEMAVERSAO_ID_"+sGXsfl_88_fel_idx;
         edtSistemaVersao_Descricao_Internalname = "SISTEMAVERSAO_DESCRICAO_"+sGXsfl_88_fel_idx;
         edtSistemaVersao_Evolucao_Internalname = "SISTEMAVERSAO_EVOLUCAO_"+sGXsfl_88_fel_idx;
         edtSistemaVersao_Melhoria_Internalname = "SISTEMAVERSAO_MELHORIA_"+sGXsfl_88_fel_idx;
         edtSistemaVersao_Correcao_Internalname = "SISTEMAVERSAO_CORRECAO_"+sGXsfl_88_fel_idx;
         edtSistemaVersao_Data_Internalname = "SISTEMAVERSAO_DATA_"+sGXsfl_88_fel_idx;
      }

      protected void sendrow_882( )
      {
         SubsflControlProps_882( ) ;
         WBOE0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_88_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_88_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_88_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV28Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV99Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV99Update_GXI : context.PathToRelativeUrl( AV28Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavUpdate_Enabled,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV28Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV29Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV100Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV100Delete_GXI : context.PathToRelativeUrl( AV29Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDelete_Enabled,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV29Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSistemaVersao_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1859SistemaVersao_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1859SistemaVersao_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSistemaVersao_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)88,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSistemaVersao_Id_Internalname,StringUtil.RTrim( A1860SistemaVersao_Id),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtSistemaVersao_Id_Link,(String)"",(String)"",(String)"",(String)edtSistemaVersao_Id_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)20,(short)0,(short)0,(short)88,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSistemaVersao_Descricao_Internalname,(String)A1861SistemaVersao_Descricao,(String)A1861SistemaVersao_Descricao,(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSistemaVersao_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)0,(short)88,(short)1,(short)0,(short)-1,(bool)true,(String)"DescricaoLonga",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSistemaVersao_Evolucao_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1862SistemaVersao_Evolucao), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A1862SistemaVersao_Evolucao), "ZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSistemaVersao_Evolucao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)55,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)88,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSistemaVersao_Melhoria_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1863SistemaVersao_Melhoria), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A1863SistemaVersao_Melhoria), "ZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSistemaVersao_Melhoria_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)57,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)88,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSistemaVersao_Correcao_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1864SistemaVersao_Correcao), 8, 0, ",", "")),context.localUtil.Format( (decimal)(A1864SistemaVersao_Correcao), "ZZZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSistemaVersao_Correcao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)58,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)88,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSistemaVersao_Data_Internalname,context.localUtil.TToC( A1865SistemaVersao_Data, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A1865SistemaVersao_Data, "99/99/99 99:99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSistemaVersao_Data_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)88,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_SISTEMAVERSAO_CODIGO"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, context.localUtil.Format( (decimal)(A1859SistemaVersao_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_SISTEMAVERSAO_ID"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, StringUtil.RTrim( context.localUtil.Format( A1860SistemaVersao_Id, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_SISTEMAVERSAO_DESCRICAO"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, A1861SistemaVersao_Descricao));
            GxWebStd.gx_hidden_field( context, "gxhash_SISTEMAVERSAO_EVOLUCAO"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, context.localUtil.Format( (decimal)(A1862SistemaVersao_Evolucao), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_SISTEMAVERSAO_MELHORIA"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, context.localUtil.Format( (decimal)(A1863SistemaVersao_Melhoria), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_SISTEMAVERSAO_CORRECAO"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, context.localUtil.Format( (decimal)(A1864SistemaVersao_Correcao), "ZZZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_SISTEMAVERSAO_DATA"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, context.localUtil.Format( A1865SistemaVersao_Data, "99/99/99 99:99")));
            GridContainer.AddRow(GridRow);
            nGXsfl_88_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_88_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_88_idx+1));
            sGXsfl_88_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_88_idx), 4, 0)), 4, "0");
            SubsflControlProps_882( ) ;
         }
         /* End function sendrow_882 */
      }

      protected void init_default_properties( )
      {
         lblSistemaversaotitle_Internalname = "SISTEMAVERSAOTITLE";
         imgInsert_Internalname = "INSERT";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         tblTableactions_Internalname = "TABLEACTIONS";
         Ddo_managefilters_Internalname = "DDO_MANAGEFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavSistemaversao_id1_Internalname = "vSISTEMAVERSAO_ID1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavSistemaversao_id2_Internalname = "vSISTEMAVERSAO_ID2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavSistemaversao_id3_Internalname = "vSISTEMAVERSAO_ID3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtSistemaVersao_Codigo_Internalname = "SISTEMAVERSAO_CODIGO";
         edtSistemaVersao_Id_Internalname = "SISTEMAVERSAO_ID";
         edtSistemaVersao_Descricao_Internalname = "SISTEMAVERSAO_DESCRICAO";
         edtSistemaVersao_Evolucao_Internalname = "SISTEMAVERSAO_EVOLUCAO";
         edtSistemaVersao_Melhoria_Internalname = "SISTEMAVERSAO_MELHORIA";
         edtSistemaVersao_Correcao_Internalname = "SISTEMAVERSAO_CORRECAO";
         edtSistemaVersao_Data_Internalname = "SISTEMAVERSAO_DATA";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavManagefiltersexecutionstep_Internalname = "vMANAGEFILTERSEXECUTIONSTEP";
         edtavTfsistemaversao_codigo_Internalname = "vTFSISTEMAVERSAO_CODIGO";
         edtavTfsistemaversao_codigo_to_Internalname = "vTFSISTEMAVERSAO_CODIGO_TO";
         edtavTfsistemaversao_id_Internalname = "vTFSISTEMAVERSAO_ID";
         edtavTfsistemaversao_id_sel_Internalname = "vTFSISTEMAVERSAO_ID_SEL";
         edtavTfsistemaversao_descricao_Internalname = "vTFSISTEMAVERSAO_DESCRICAO";
         edtavTfsistemaversao_descricao_sel_Internalname = "vTFSISTEMAVERSAO_DESCRICAO_SEL";
         edtavTfsistemaversao_evolucao_Internalname = "vTFSISTEMAVERSAO_EVOLUCAO";
         edtavTfsistemaversao_evolucao_to_Internalname = "vTFSISTEMAVERSAO_EVOLUCAO_TO";
         edtavTfsistemaversao_melhoria_Internalname = "vTFSISTEMAVERSAO_MELHORIA";
         edtavTfsistemaversao_melhoria_to_Internalname = "vTFSISTEMAVERSAO_MELHORIA_TO";
         edtavTfsistemaversao_correcao_Internalname = "vTFSISTEMAVERSAO_CORRECAO";
         edtavTfsistemaversao_correcao_to_Internalname = "vTFSISTEMAVERSAO_CORRECAO_TO";
         edtavTfsistemaversao_data_Internalname = "vTFSISTEMAVERSAO_DATA";
         edtavTfsistemaversao_data_to_Internalname = "vTFSISTEMAVERSAO_DATA_TO";
         edtavDdo_sistemaversao_dataauxdate_Internalname = "vDDO_SISTEMAVERSAO_DATAAUXDATE";
         edtavDdo_sistemaversao_dataauxdateto_Internalname = "vDDO_SISTEMAVERSAO_DATAAUXDATETO";
         divDdo_sistemaversao_dataauxdates_Internalname = "DDO_SISTEMAVERSAO_DATAAUXDATES";
         Ddo_sistemaversao_codigo_Internalname = "DDO_SISTEMAVERSAO_CODIGO";
         edtavDdo_sistemaversao_codigotitlecontrolidtoreplace_Internalname = "vDDO_SISTEMAVERSAO_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_sistemaversao_id_Internalname = "DDO_SISTEMAVERSAO_ID";
         edtavDdo_sistemaversao_idtitlecontrolidtoreplace_Internalname = "vDDO_SISTEMAVERSAO_IDTITLECONTROLIDTOREPLACE";
         Ddo_sistemaversao_descricao_Internalname = "DDO_SISTEMAVERSAO_DESCRICAO";
         edtavDdo_sistemaversao_descricaotitlecontrolidtoreplace_Internalname = "vDDO_SISTEMAVERSAO_DESCRICAOTITLECONTROLIDTOREPLACE";
         Ddo_sistemaversao_evolucao_Internalname = "DDO_SISTEMAVERSAO_EVOLUCAO";
         edtavDdo_sistemaversao_evolucaotitlecontrolidtoreplace_Internalname = "vDDO_SISTEMAVERSAO_EVOLUCAOTITLECONTROLIDTOREPLACE";
         Ddo_sistemaversao_melhoria_Internalname = "DDO_SISTEMAVERSAO_MELHORIA";
         edtavDdo_sistemaversao_melhoriatitlecontrolidtoreplace_Internalname = "vDDO_SISTEMAVERSAO_MELHORIATITLECONTROLIDTOREPLACE";
         Ddo_sistemaversao_correcao_Internalname = "DDO_SISTEMAVERSAO_CORRECAO";
         edtavDdo_sistemaversao_correcaotitlecontrolidtoreplace_Internalname = "vDDO_SISTEMAVERSAO_CORRECAOTITLECONTROLIDTOREPLACE";
         Ddo_sistemaversao_data_Internalname = "DDO_SISTEMAVERSAO_DATA";
         edtavDdo_sistemaversao_datatitlecontrolidtoreplace_Internalname = "vDDO_SISTEMAVERSAO_DATATITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtSistemaVersao_Data_Jsonclick = "";
         edtSistemaVersao_Correcao_Jsonclick = "";
         edtSistemaVersao_Melhoria_Jsonclick = "";
         edtSistemaVersao_Evolucao_Jsonclick = "";
         edtSistemaVersao_Descricao_Jsonclick = "";
         edtSistemaVersao_Id_Jsonclick = "";
         edtSistemaVersao_Codigo_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         imgInsert_Enabled = 1;
         imgInsert_Bitmap = (String)(context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )));
         edtavSistemaversao_id1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavSistemaversao_id2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavSistemaversao_id3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtSistemaVersao_Id_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavDelete_Enabled = 1;
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtavUpdate_Enabled = 1;
         edtSistemaVersao_Data_Titleformat = 0;
         edtSistemaVersao_Correcao_Titleformat = 0;
         edtSistemaVersao_Melhoria_Titleformat = 0;
         edtSistemaVersao_Evolucao_Titleformat = 0;
         edtSistemaVersao_Descricao_Titleformat = 0;
         edtSistemaVersao_Id_Titleformat = 0;
         edtSistemaVersao_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavSistemaversao_id3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavSistemaversao_id2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavSistemaversao_id1_Visible = 1;
         edtSistemaVersao_Data_Title = "Em";
         edtSistemaVersao_Correcao_Title = "Corre��o";
         edtSistemaVersao_Melhoria_Title = "Melhoria";
         edtSistemaVersao_Evolucao_Title = "Evolu��o";
         edtSistemaVersao_Descricao_Title = "Descri��o";
         edtSistemaVersao_Id_Title = "Vers�o";
         edtSistemaVersao_Codigo_Title = "Vers�o";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_sistemaversao_datatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_sistemaversao_correcaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_sistemaversao_melhoriatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_sistemaversao_evolucaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_sistemaversao_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_sistemaversao_idtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_sistemaversao_codigotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_sistemaversao_dataauxdateto_Jsonclick = "";
         edtavDdo_sistemaversao_dataauxdate_Jsonclick = "";
         edtavTfsistemaversao_data_to_Jsonclick = "";
         edtavTfsistemaversao_data_to_Visible = 1;
         edtavTfsistemaversao_data_Jsonclick = "";
         edtavTfsistemaversao_data_Visible = 1;
         edtavTfsistemaversao_correcao_to_Jsonclick = "";
         edtavTfsistemaversao_correcao_to_Visible = 1;
         edtavTfsistemaversao_correcao_Jsonclick = "";
         edtavTfsistemaversao_correcao_Visible = 1;
         edtavTfsistemaversao_melhoria_to_Jsonclick = "";
         edtavTfsistemaversao_melhoria_to_Visible = 1;
         edtavTfsistemaversao_melhoria_Jsonclick = "";
         edtavTfsistemaversao_melhoria_Visible = 1;
         edtavTfsistemaversao_evolucao_to_Jsonclick = "";
         edtavTfsistemaversao_evolucao_to_Visible = 1;
         edtavTfsistemaversao_evolucao_Jsonclick = "";
         edtavTfsistemaversao_evolucao_Visible = 1;
         edtavTfsistemaversao_descricao_sel_Visible = 1;
         edtavTfsistemaversao_descricao_Visible = 1;
         edtavTfsistemaversao_id_sel_Jsonclick = "";
         edtavTfsistemaversao_id_sel_Visible = 1;
         edtavTfsistemaversao_id_Jsonclick = "";
         edtavTfsistemaversao_id_Visible = 1;
         edtavTfsistemaversao_codigo_to_Jsonclick = "";
         edtavTfsistemaversao_codigo_to_Visible = 1;
         edtavTfsistemaversao_codigo_Jsonclick = "";
         edtavTfsistemaversao_codigo_Visible = 1;
         edtavManagefiltersexecutionstep_Jsonclick = "";
         edtavManagefiltersexecutionstep_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_sistemaversao_data_Searchbuttontext = "Pesquisar";
         Ddo_sistemaversao_data_Rangefilterto = "At�";
         Ddo_sistemaversao_data_Rangefilterfrom = "Desde";
         Ddo_sistemaversao_data_Cleanfilter = "Limpar pesquisa";
         Ddo_sistemaversao_data_Sortdsc = "Ordenar de Z � A";
         Ddo_sistemaversao_data_Sortasc = "Ordenar de A � Z";
         Ddo_sistemaversao_data_Includedatalist = Convert.ToBoolean( 0);
         Ddo_sistemaversao_data_Filterisrange = Convert.ToBoolean( -1);
         Ddo_sistemaversao_data_Filtertype = "Date";
         Ddo_sistemaversao_data_Includefilter = Convert.ToBoolean( -1);
         Ddo_sistemaversao_data_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_sistemaversao_data_Includesortasc = Convert.ToBoolean( -1);
         Ddo_sistemaversao_data_Titlecontrolidtoreplace = "";
         Ddo_sistemaversao_data_Dropdownoptionstype = "GridTitleSettings";
         Ddo_sistemaversao_data_Cls = "ColumnSettings";
         Ddo_sistemaversao_data_Tooltip = "Op��es";
         Ddo_sistemaversao_data_Caption = "";
         Ddo_sistemaversao_correcao_Searchbuttontext = "Pesquisar";
         Ddo_sistemaversao_correcao_Rangefilterto = "At�";
         Ddo_sistemaversao_correcao_Rangefilterfrom = "Desde";
         Ddo_sistemaversao_correcao_Cleanfilter = "Limpar pesquisa";
         Ddo_sistemaversao_correcao_Sortdsc = "Ordenar de Z � A";
         Ddo_sistemaversao_correcao_Sortasc = "Ordenar de A � Z";
         Ddo_sistemaversao_correcao_Includedatalist = Convert.ToBoolean( 0);
         Ddo_sistemaversao_correcao_Filterisrange = Convert.ToBoolean( -1);
         Ddo_sistemaversao_correcao_Filtertype = "Numeric";
         Ddo_sistemaversao_correcao_Includefilter = Convert.ToBoolean( -1);
         Ddo_sistemaversao_correcao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_sistemaversao_correcao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_sistemaversao_correcao_Titlecontrolidtoreplace = "";
         Ddo_sistemaversao_correcao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_sistemaversao_correcao_Cls = "ColumnSettings";
         Ddo_sistemaversao_correcao_Tooltip = "Op��es";
         Ddo_sistemaversao_correcao_Caption = "";
         Ddo_sistemaversao_melhoria_Searchbuttontext = "Pesquisar";
         Ddo_sistemaversao_melhoria_Rangefilterto = "At�";
         Ddo_sistemaversao_melhoria_Rangefilterfrom = "Desde";
         Ddo_sistemaversao_melhoria_Cleanfilter = "Limpar pesquisa";
         Ddo_sistemaversao_melhoria_Sortdsc = "Ordenar de Z � A";
         Ddo_sistemaversao_melhoria_Sortasc = "Ordenar de A � Z";
         Ddo_sistemaversao_melhoria_Includedatalist = Convert.ToBoolean( 0);
         Ddo_sistemaversao_melhoria_Filterisrange = Convert.ToBoolean( -1);
         Ddo_sistemaversao_melhoria_Filtertype = "Numeric";
         Ddo_sistemaversao_melhoria_Includefilter = Convert.ToBoolean( -1);
         Ddo_sistemaversao_melhoria_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_sistemaversao_melhoria_Includesortasc = Convert.ToBoolean( -1);
         Ddo_sistemaversao_melhoria_Titlecontrolidtoreplace = "";
         Ddo_sistemaversao_melhoria_Dropdownoptionstype = "GridTitleSettings";
         Ddo_sistemaversao_melhoria_Cls = "ColumnSettings";
         Ddo_sistemaversao_melhoria_Tooltip = "Op��es";
         Ddo_sistemaversao_melhoria_Caption = "";
         Ddo_sistemaversao_evolucao_Searchbuttontext = "Pesquisar";
         Ddo_sistemaversao_evolucao_Rangefilterto = "At�";
         Ddo_sistemaversao_evolucao_Rangefilterfrom = "Desde";
         Ddo_sistemaversao_evolucao_Cleanfilter = "Limpar pesquisa";
         Ddo_sistemaversao_evolucao_Sortdsc = "Ordenar de Z � A";
         Ddo_sistemaversao_evolucao_Sortasc = "Ordenar de A � Z";
         Ddo_sistemaversao_evolucao_Includedatalist = Convert.ToBoolean( 0);
         Ddo_sistemaversao_evolucao_Filterisrange = Convert.ToBoolean( -1);
         Ddo_sistemaversao_evolucao_Filtertype = "Numeric";
         Ddo_sistemaversao_evolucao_Includefilter = Convert.ToBoolean( -1);
         Ddo_sistemaversao_evolucao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_sistemaversao_evolucao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_sistemaversao_evolucao_Titlecontrolidtoreplace = "";
         Ddo_sistemaversao_evolucao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_sistemaversao_evolucao_Cls = "ColumnSettings";
         Ddo_sistemaversao_evolucao_Tooltip = "Op��es";
         Ddo_sistemaversao_evolucao_Caption = "";
         Ddo_sistemaversao_descricao_Searchbuttontext = "Pesquisar";
         Ddo_sistemaversao_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_sistemaversao_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_sistemaversao_descricao_Loadingdata = "Carregando dados...";
         Ddo_sistemaversao_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_sistemaversao_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_sistemaversao_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_sistemaversao_descricao_Datalistproc = "GetWWSistemaVersaoFilterData";
         Ddo_sistemaversao_descricao_Datalisttype = "Dynamic";
         Ddo_sistemaversao_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_sistemaversao_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_sistemaversao_descricao_Filtertype = "Character";
         Ddo_sistemaversao_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_sistemaversao_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_sistemaversao_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_sistemaversao_descricao_Titlecontrolidtoreplace = "";
         Ddo_sistemaversao_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_sistemaversao_descricao_Cls = "ColumnSettings";
         Ddo_sistemaversao_descricao_Tooltip = "Op��es";
         Ddo_sistemaversao_descricao_Caption = "";
         Ddo_sistemaversao_id_Searchbuttontext = "Pesquisar";
         Ddo_sistemaversao_id_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_sistemaversao_id_Cleanfilter = "Limpar pesquisa";
         Ddo_sistemaversao_id_Loadingdata = "Carregando dados...";
         Ddo_sistemaversao_id_Sortdsc = "Ordenar de Z � A";
         Ddo_sistemaversao_id_Sortasc = "Ordenar de A � Z";
         Ddo_sistemaversao_id_Datalistupdateminimumcharacters = 0;
         Ddo_sistemaversao_id_Datalistproc = "GetWWSistemaVersaoFilterData";
         Ddo_sistemaversao_id_Datalisttype = "Dynamic";
         Ddo_sistemaversao_id_Includedatalist = Convert.ToBoolean( -1);
         Ddo_sistemaversao_id_Filterisrange = Convert.ToBoolean( 0);
         Ddo_sistemaversao_id_Filtertype = "Character";
         Ddo_sistemaversao_id_Includefilter = Convert.ToBoolean( -1);
         Ddo_sistemaversao_id_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_sistemaversao_id_Includesortasc = Convert.ToBoolean( -1);
         Ddo_sistemaversao_id_Titlecontrolidtoreplace = "";
         Ddo_sistemaversao_id_Dropdownoptionstype = "GridTitleSettings";
         Ddo_sistemaversao_id_Cls = "ColumnSettings";
         Ddo_sistemaversao_id_Tooltip = "Op��es";
         Ddo_sistemaversao_id_Caption = "";
         Ddo_sistemaversao_codigo_Searchbuttontext = "Pesquisar";
         Ddo_sistemaversao_codigo_Rangefilterto = "At�";
         Ddo_sistemaversao_codigo_Rangefilterfrom = "Desde";
         Ddo_sistemaversao_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_sistemaversao_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_sistemaversao_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_sistemaversao_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_sistemaversao_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_sistemaversao_codigo_Filtertype = "Numeric";
         Ddo_sistemaversao_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_sistemaversao_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_sistemaversao_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_sistemaversao_codigo_Titlecontrolidtoreplace = "";
         Ddo_sistemaversao_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_sistemaversao_codigo_Cls = "ColumnSettings";
         Ddo_sistemaversao_codigo_Tooltip = "Op��es";
         Ddo_sistemaversao_codigo_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Ddo_managefilters_Cls = "ManageFilters";
         Ddo_managefilters_Tooltip = "Gerenciar filtros";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Versionamento";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A1859SistemaVersao_Codigo',fld:'SISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV71SistemaVersao_SistemaCod',fld:'vSISTEMAVERSAO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV31ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV40ddo_SistemaVersao_CodigoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_SistemaVersao_IdTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_IDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_SistemaVersao_DescricaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SistemaVersao_EvolucaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_EVOLUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SistemaVersao_MelhoriaTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_MELHORIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SistemaVersao_CorrecaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_CORRECAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_SistemaVersao_DataTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SistemaVersao_Id1',fld:'vSISTEMAVERSAO_ID1',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SistemaVersao_Id2',fld:'vSISTEMAVERSAO_ID2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SistemaVersao_Id3',fld:'vSISTEMAVERSAO_ID3',pic:'',nv:''},{av:'AV38TFSistemaVersao_Codigo',fld:'vTFSISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSistemaVersao_Codigo_To',fld:'vTFSISTEMAVERSAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSistemaVersao_Id',fld:'vTFSISTEMAVERSAO_ID',pic:'',nv:''},{av:'AV43TFSistemaVersao_Id_Sel',fld:'vTFSISTEMAVERSAO_ID_SEL',pic:'',nv:''},{av:'AV46TFSistemaVersao_Descricao',fld:'vTFSISTEMAVERSAO_DESCRICAO',pic:'',nv:''},{av:'AV47TFSistemaVersao_Descricao_Sel',fld:'vTFSISTEMAVERSAO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV50TFSistemaVersao_Evolucao',fld:'vTFSISTEMAVERSAO_EVOLUCAO',pic:'ZZZ9',nv:0},{av:'AV51TFSistemaVersao_Evolucao_To',fld:'vTFSISTEMAVERSAO_EVOLUCAO_TO',pic:'ZZZ9',nv:0},{av:'AV54TFSistemaVersao_Melhoria',fld:'vTFSISTEMAVERSAO_MELHORIA',pic:'ZZZ9',nv:0},{av:'AV55TFSistemaVersao_Melhoria_To',fld:'vTFSISTEMAVERSAO_MELHORIA_TO',pic:'ZZZ9',nv:0},{av:'AV58TFSistemaVersao_Correcao',fld:'vTFSISTEMAVERSAO_CORRECAO',pic:'ZZZZZZZ9',nv:0},{av:'AV59TFSistemaVersao_Correcao_To',fld:'vTFSISTEMAVERSAO_CORRECAO_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV62TFSistemaVersao_Data',fld:'vTFSISTEMAVERSAO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV63TFSistemaVersao_Data_To',fld:'vTFSISTEMAVERSAO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV101Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV37SistemaVersao_CodigoTitleFilterData',fld:'vSISTEMAVERSAO_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV41SistemaVersao_IdTitleFilterData',fld:'vSISTEMAVERSAO_IDTITLEFILTERDATA',pic:'',nv:null},{av:'AV45SistemaVersao_DescricaoTitleFilterData',fld:'vSISTEMAVERSAO_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV49SistemaVersao_EvolucaoTitleFilterData',fld:'vSISTEMAVERSAO_EVOLUCAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV53SistemaVersao_MelhoriaTitleFilterData',fld:'vSISTEMAVERSAO_MELHORIATITLEFILTERDATA',pic:'',nv:null},{av:'AV57SistemaVersao_CorrecaoTitleFilterData',fld:'vSISTEMAVERSAO_CORRECAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV61SistemaVersao_DataTitleFilterData',fld:'vSISTEMAVERSAO_DATATITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV31ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'edtSistemaVersao_Codigo_Titleformat',ctrl:'SISTEMAVERSAO_CODIGO',prop:'Titleformat'},{av:'edtSistemaVersao_Codigo_Title',ctrl:'SISTEMAVERSAO_CODIGO',prop:'Title'},{av:'edtSistemaVersao_Id_Titleformat',ctrl:'SISTEMAVERSAO_ID',prop:'Titleformat'},{av:'edtSistemaVersao_Id_Title',ctrl:'SISTEMAVERSAO_ID',prop:'Title'},{av:'edtSistemaVersao_Descricao_Titleformat',ctrl:'SISTEMAVERSAO_DESCRICAO',prop:'Titleformat'},{av:'edtSistemaVersao_Descricao_Title',ctrl:'SISTEMAVERSAO_DESCRICAO',prop:'Title'},{av:'edtSistemaVersao_Evolucao_Titleformat',ctrl:'SISTEMAVERSAO_EVOLUCAO',prop:'Titleformat'},{av:'edtSistemaVersao_Evolucao_Title',ctrl:'SISTEMAVERSAO_EVOLUCAO',prop:'Title'},{av:'edtSistemaVersao_Melhoria_Titleformat',ctrl:'SISTEMAVERSAO_MELHORIA',prop:'Titleformat'},{av:'edtSistemaVersao_Melhoria_Title',ctrl:'SISTEMAVERSAO_MELHORIA',prop:'Title'},{av:'edtSistemaVersao_Correcao_Titleformat',ctrl:'SISTEMAVERSAO_CORRECAO',prop:'Titleformat'},{av:'edtSistemaVersao_Correcao_Title',ctrl:'SISTEMAVERSAO_CORRECAO',prop:'Title'},{av:'edtSistemaVersao_Data_Titleformat',ctrl:'SISTEMAVERSAO_DATA',prop:'Titleformat'},{av:'edtSistemaVersao_Data_Title',ctrl:'SISTEMAVERSAO_DATA',prop:'Title'},{av:'AV69GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV70GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'imgInsert_Link',ctrl:'INSERT',prop:'Link'},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'},{av:'AV35ManageFiltersData',fld:'vMANAGEFILTERSDATA',pic:'',nv:null},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E12OE2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SistemaVersao_Id1',fld:'vSISTEMAVERSAO_ID1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SistemaVersao_Id2',fld:'vSISTEMAVERSAO_ID2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SistemaVersao_Id3',fld:'vSISTEMAVERSAO_ID3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV38TFSistemaVersao_Codigo',fld:'vTFSISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSistemaVersao_Codigo_To',fld:'vTFSISTEMAVERSAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSistemaVersao_Id',fld:'vTFSISTEMAVERSAO_ID',pic:'',nv:''},{av:'AV43TFSistemaVersao_Id_Sel',fld:'vTFSISTEMAVERSAO_ID_SEL',pic:'',nv:''},{av:'AV46TFSistemaVersao_Descricao',fld:'vTFSISTEMAVERSAO_DESCRICAO',pic:'',nv:''},{av:'AV47TFSistemaVersao_Descricao_Sel',fld:'vTFSISTEMAVERSAO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV50TFSistemaVersao_Evolucao',fld:'vTFSISTEMAVERSAO_EVOLUCAO',pic:'ZZZ9',nv:0},{av:'AV51TFSistemaVersao_Evolucao_To',fld:'vTFSISTEMAVERSAO_EVOLUCAO_TO',pic:'ZZZ9',nv:0},{av:'AV54TFSistemaVersao_Melhoria',fld:'vTFSISTEMAVERSAO_MELHORIA',pic:'ZZZ9',nv:0},{av:'AV55TFSistemaVersao_Melhoria_To',fld:'vTFSISTEMAVERSAO_MELHORIA_TO',pic:'ZZZ9',nv:0},{av:'AV58TFSistemaVersao_Correcao',fld:'vTFSISTEMAVERSAO_CORRECAO',pic:'ZZZZZZZ9',nv:0},{av:'AV59TFSistemaVersao_Correcao_To',fld:'vTFSISTEMAVERSAO_CORRECAO_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV62TFSistemaVersao_Data',fld:'vTFSISTEMAVERSAO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV63TFSistemaVersao_Data_To',fld:'vTFSISTEMAVERSAO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV31ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV40ddo_SistemaVersao_CodigoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_SistemaVersao_IdTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_IDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_SistemaVersao_DescricaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SistemaVersao_EvolucaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_EVOLUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SistemaVersao_MelhoriaTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_MELHORIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SistemaVersao_CorrecaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_CORRECAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_SistemaVersao_DataTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV101Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1859SistemaVersao_Codigo',fld:'SISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV71SistemaVersao_SistemaCod',fld:'vSISTEMAVERSAO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_SISTEMAVERSAO_CODIGO.ONOPTIONCLICKED","{handler:'E13OE2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SistemaVersao_Id1',fld:'vSISTEMAVERSAO_ID1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SistemaVersao_Id2',fld:'vSISTEMAVERSAO_ID2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SistemaVersao_Id3',fld:'vSISTEMAVERSAO_ID3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV38TFSistemaVersao_Codigo',fld:'vTFSISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSistemaVersao_Codigo_To',fld:'vTFSISTEMAVERSAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSistemaVersao_Id',fld:'vTFSISTEMAVERSAO_ID',pic:'',nv:''},{av:'AV43TFSistemaVersao_Id_Sel',fld:'vTFSISTEMAVERSAO_ID_SEL',pic:'',nv:''},{av:'AV46TFSistemaVersao_Descricao',fld:'vTFSISTEMAVERSAO_DESCRICAO',pic:'',nv:''},{av:'AV47TFSistemaVersao_Descricao_Sel',fld:'vTFSISTEMAVERSAO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV50TFSistemaVersao_Evolucao',fld:'vTFSISTEMAVERSAO_EVOLUCAO',pic:'ZZZ9',nv:0},{av:'AV51TFSistemaVersao_Evolucao_To',fld:'vTFSISTEMAVERSAO_EVOLUCAO_TO',pic:'ZZZ9',nv:0},{av:'AV54TFSistemaVersao_Melhoria',fld:'vTFSISTEMAVERSAO_MELHORIA',pic:'ZZZ9',nv:0},{av:'AV55TFSistemaVersao_Melhoria_To',fld:'vTFSISTEMAVERSAO_MELHORIA_TO',pic:'ZZZ9',nv:0},{av:'AV58TFSistemaVersao_Correcao',fld:'vTFSISTEMAVERSAO_CORRECAO',pic:'ZZZZZZZ9',nv:0},{av:'AV59TFSistemaVersao_Correcao_To',fld:'vTFSISTEMAVERSAO_CORRECAO_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV62TFSistemaVersao_Data',fld:'vTFSISTEMAVERSAO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV63TFSistemaVersao_Data_To',fld:'vTFSISTEMAVERSAO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV31ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV40ddo_SistemaVersao_CodigoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_SistemaVersao_IdTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_IDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_SistemaVersao_DescricaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SistemaVersao_EvolucaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_EVOLUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SistemaVersao_MelhoriaTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_MELHORIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SistemaVersao_CorrecaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_CORRECAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_SistemaVersao_DataTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV101Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1859SistemaVersao_Codigo',fld:'SISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV71SistemaVersao_SistemaCod',fld:'vSISTEMAVERSAO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_sistemaversao_codigo_Activeeventkey',ctrl:'DDO_SISTEMAVERSAO_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_sistemaversao_codigo_Filteredtext_get',ctrl:'DDO_SISTEMAVERSAO_CODIGO',prop:'FilteredText_get'},{av:'Ddo_sistemaversao_codigo_Filteredtextto_get',ctrl:'DDO_SISTEMAVERSAO_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_sistemaversao_codigo_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_CODIGO',prop:'SortedStatus'},{av:'AV38TFSistemaVersao_Codigo',fld:'vTFSISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSistemaVersao_Codigo_To',fld:'vTFSISTEMAVERSAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_sistemaversao_id_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_ID',prop:'SortedStatus'},{av:'Ddo_sistemaversao_descricao_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_sistemaversao_evolucao_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_EVOLUCAO',prop:'SortedStatus'},{av:'Ddo_sistemaversao_melhoria_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_MELHORIA',prop:'SortedStatus'},{av:'Ddo_sistemaversao_correcao_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_CORRECAO',prop:'SortedStatus'},{av:'Ddo_sistemaversao_data_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_DATA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SISTEMAVERSAO_ID.ONOPTIONCLICKED","{handler:'E14OE2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SistemaVersao_Id1',fld:'vSISTEMAVERSAO_ID1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SistemaVersao_Id2',fld:'vSISTEMAVERSAO_ID2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SistemaVersao_Id3',fld:'vSISTEMAVERSAO_ID3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV38TFSistemaVersao_Codigo',fld:'vTFSISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSistemaVersao_Codigo_To',fld:'vTFSISTEMAVERSAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSistemaVersao_Id',fld:'vTFSISTEMAVERSAO_ID',pic:'',nv:''},{av:'AV43TFSistemaVersao_Id_Sel',fld:'vTFSISTEMAVERSAO_ID_SEL',pic:'',nv:''},{av:'AV46TFSistemaVersao_Descricao',fld:'vTFSISTEMAVERSAO_DESCRICAO',pic:'',nv:''},{av:'AV47TFSistemaVersao_Descricao_Sel',fld:'vTFSISTEMAVERSAO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV50TFSistemaVersao_Evolucao',fld:'vTFSISTEMAVERSAO_EVOLUCAO',pic:'ZZZ9',nv:0},{av:'AV51TFSistemaVersao_Evolucao_To',fld:'vTFSISTEMAVERSAO_EVOLUCAO_TO',pic:'ZZZ9',nv:0},{av:'AV54TFSistemaVersao_Melhoria',fld:'vTFSISTEMAVERSAO_MELHORIA',pic:'ZZZ9',nv:0},{av:'AV55TFSistemaVersao_Melhoria_To',fld:'vTFSISTEMAVERSAO_MELHORIA_TO',pic:'ZZZ9',nv:0},{av:'AV58TFSistemaVersao_Correcao',fld:'vTFSISTEMAVERSAO_CORRECAO',pic:'ZZZZZZZ9',nv:0},{av:'AV59TFSistemaVersao_Correcao_To',fld:'vTFSISTEMAVERSAO_CORRECAO_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV62TFSistemaVersao_Data',fld:'vTFSISTEMAVERSAO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV63TFSistemaVersao_Data_To',fld:'vTFSISTEMAVERSAO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV31ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV40ddo_SistemaVersao_CodigoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_SistemaVersao_IdTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_IDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_SistemaVersao_DescricaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SistemaVersao_EvolucaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_EVOLUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SistemaVersao_MelhoriaTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_MELHORIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SistemaVersao_CorrecaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_CORRECAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_SistemaVersao_DataTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV101Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1859SistemaVersao_Codigo',fld:'SISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV71SistemaVersao_SistemaCod',fld:'vSISTEMAVERSAO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_sistemaversao_id_Activeeventkey',ctrl:'DDO_SISTEMAVERSAO_ID',prop:'ActiveEventKey'},{av:'Ddo_sistemaversao_id_Filteredtext_get',ctrl:'DDO_SISTEMAVERSAO_ID',prop:'FilteredText_get'},{av:'Ddo_sistemaversao_id_Selectedvalue_get',ctrl:'DDO_SISTEMAVERSAO_ID',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_sistemaversao_id_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_ID',prop:'SortedStatus'},{av:'AV42TFSistemaVersao_Id',fld:'vTFSISTEMAVERSAO_ID',pic:'',nv:''},{av:'AV43TFSistemaVersao_Id_Sel',fld:'vTFSISTEMAVERSAO_ID_SEL',pic:'',nv:''},{av:'Ddo_sistemaversao_codigo_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_CODIGO',prop:'SortedStatus'},{av:'Ddo_sistemaversao_descricao_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_sistemaversao_evolucao_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_EVOLUCAO',prop:'SortedStatus'},{av:'Ddo_sistemaversao_melhoria_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_MELHORIA',prop:'SortedStatus'},{av:'Ddo_sistemaversao_correcao_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_CORRECAO',prop:'SortedStatus'},{av:'Ddo_sistemaversao_data_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_DATA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SISTEMAVERSAO_DESCRICAO.ONOPTIONCLICKED","{handler:'E15OE2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SistemaVersao_Id1',fld:'vSISTEMAVERSAO_ID1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SistemaVersao_Id2',fld:'vSISTEMAVERSAO_ID2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SistemaVersao_Id3',fld:'vSISTEMAVERSAO_ID3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV38TFSistemaVersao_Codigo',fld:'vTFSISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSistemaVersao_Codigo_To',fld:'vTFSISTEMAVERSAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSistemaVersao_Id',fld:'vTFSISTEMAVERSAO_ID',pic:'',nv:''},{av:'AV43TFSistemaVersao_Id_Sel',fld:'vTFSISTEMAVERSAO_ID_SEL',pic:'',nv:''},{av:'AV46TFSistemaVersao_Descricao',fld:'vTFSISTEMAVERSAO_DESCRICAO',pic:'',nv:''},{av:'AV47TFSistemaVersao_Descricao_Sel',fld:'vTFSISTEMAVERSAO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV50TFSistemaVersao_Evolucao',fld:'vTFSISTEMAVERSAO_EVOLUCAO',pic:'ZZZ9',nv:0},{av:'AV51TFSistemaVersao_Evolucao_To',fld:'vTFSISTEMAVERSAO_EVOLUCAO_TO',pic:'ZZZ9',nv:0},{av:'AV54TFSistemaVersao_Melhoria',fld:'vTFSISTEMAVERSAO_MELHORIA',pic:'ZZZ9',nv:0},{av:'AV55TFSistemaVersao_Melhoria_To',fld:'vTFSISTEMAVERSAO_MELHORIA_TO',pic:'ZZZ9',nv:0},{av:'AV58TFSistemaVersao_Correcao',fld:'vTFSISTEMAVERSAO_CORRECAO',pic:'ZZZZZZZ9',nv:0},{av:'AV59TFSistemaVersao_Correcao_To',fld:'vTFSISTEMAVERSAO_CORRECAO_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV62TFSistemaVersao_Data',fld:'vTFSISTEMAVERSAO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV63TFSistemaVersao_Data_To',fld:'vTFSISTEMAVERSAO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV31ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV40ddo_SistemaVersao_CodigoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_SistemaVersao_IdTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_IDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_SistemaVersao_DescricaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SistemaVersao_EvolucaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_EVOLUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SistemaVersao_MelhoriaTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_MELHORIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SistemaVersao_CorrecaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_CORRECAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_SistemaVersao_DataTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV101Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1859SistemaVersao_Codigo',fld:'SISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV71SistemaVersao_SistemaCod',fld:'vSISTEMAVERSAO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_sistemaversao_descricao_Activeeventkey',ctrl:'DDO_SISTEMAVERSAO_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_sistemaversao_descricao_Filteredtext_get',ctrl:'DDO_SISTEMAVERSAO_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_sistemaversao_descricao_Selectedvalue_get',ctrl:'DDO_SISTEMAVERSAO_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_sistemaversao_descricao_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_DESCRICAO',prop:'SortedStatus'},{av:'AV46TFSistemaVersao_Descricao',fld:'vTFSISTEMAVERSAO_DESCRICAO',pic:'',nv:''},{av:'AV47TFSistemaVersao_Descricao_Sel',fld:'vTFSISTEMAVERSAO_DESCRICAO_SEL',pic:'',nv:''},{av:'Ddo_sistemaversao_codigo_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_CODIGO',prop:'SortedStatus'},{av:'Ddo_sistemaversao_id_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_ID',prop:'SortedStatus'},{av:'Ddo_sistemaversao_evolucao_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_EVOLUCAO',prop:'SortedStatus'},{av:'Ddo_sistemaversao_melhoria_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_MELHORIA',prop:'SortedStatus'},{av:'Ddo_sistemaversao_correcao_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_CORRECAO',prop:'SortedStatus'},{av:'Ddo_sistemaversao_data_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_DATA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SISTEMAVERSAO_EVOLUCAO.ONOPTIONCLICKED","{handler:'E16OE2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SistemaVersao_Id1',fld:'vSISTEMAVERSAO_ID1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SistemaVersao_Id2',fld:'vSISTEMAVERSAO_ID2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SistemaVersao_Id3',fld:'vSISTEMAVERSAO_ID3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV38TFSistemaVersao_Codigo',fld:'vTFSISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSistemaVersao_Codigo_To',fld:'vTFSISTEMAVERSAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSistemaVersao_Id',fld:'vTFSISTEMAVERSAO_ID',pic:'',nv:''},{av:'AV43TFSistemaVersao_Id_Sel',fld:'vTFSISTEMAVERSAO_ID_SEL',pic:'',nv:''},{av:'AV46TFSistemaVersao_Descricao',fld:'vTFSISTEMAVERSAO_DESCRICAO',pic:'',nv:''},{av:'AV47TFSistemaVersao_Descricao_Sel',fld:'vTFSISTEMAVERSAO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV50TFSistemaVersao_Evolucao',fld:'vTFSISTEMAVERSAO_EVOLUCAO',pic:'ZZZ9',nv:0},{av:'AV51TFSistemaVersao_Evolucao_To',fld:'vTFSISTEMAVERSAO_EVOLUCAO_TO',pic:'ZZZ9',nv:0},{av:'AV54TFSistemaVersao_Melhoria',fld:'vTFSISTEMAVERSAO_MELHORIA',pic:'ZZZ9',nv:0},{av:'AV55TFSistemaVersao_Melhoria_To',fld:'vTFSISTEMAVERSAO_MELHORIA_TO',pic:'ZZZ9',nv:0},{av:'AV58TFSistemaVersao_Correcao',fld:'vTFSISTEMAVERSAO_CORRECAO',pic:'ZZZZZZZ9',nv:0},{av:'AV59TFSistemaVersao_Correcao_To',fld:'vTFSISTEMAVERSAO_CORRECAO_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV62TFSistemaVersao_Data',fld:'vTFSISTEMAVERSAO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV63TFSistemaVersao_Data_To',fld:'vTFSISTEMAVERSAO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV31ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV40ddo_SistemaVersao_CodigoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_SistemaVersao_IdTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_IDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_SistemaVersao_DescricaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SistemaVersao_EvolucaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_EVOLUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SistemaVersao_MelhoriaTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_MELHORIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SistemaVersao_CorrecaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_CORRECAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_SistemaVersao_DataTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV101Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1859SistemaVersao_Codigo',fld:'SISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV71SistemaVersao_SistemaCod',fld:'vSISTEMAVERSAO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_sistemaversao_evolucao_Activeeventkey',ctrl:'DDO_SISTEMAVERSAO_EVOLUCAO',prop:'ActiveEventKey'},{av:'Ddo_sistemaversao_evolucao_Filteredtext_get',ctrl:'DDO_SISTEMAVERSAO_EVOLUCAO',prop:'FilteredText_get'},{av:'Ddo_sistemaversao_evolucao_Filteredtextto_get',ctrl:'DDO_SISTEMAVERSAO_EVOLUCAO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_sistemaversao_evolucao_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_EVOLUCAO',prop:'SortedStatus'},{av:'AV50TFSistemaVersao_Evolucao',fld:'vTFSISTEMAVERSAO_EVOLUCAO',pic:'ZZZ9',nv:0},{av:'AV51TFSistemaVersao_Evolucao_To',fld:'vTFSISTEMAVERSAO_EVOLUCAO_TO',pic:'ZZZ9',nv:0},{av:'Ddo_sistemaversao_codigo_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_CODIGO',prop:'SortedStatus'},{av:'Ddo_sistemaversao_id_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_ID',prop:'SortedStatus'},{av:'Ddo_sistemaversao_descricao_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_sistemaversao_melhoria_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_MELHORIA',prop:'SortedStatus'},{av:'Ddo_sistemaversao_correcao_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_CORRECAO',prop:'SortedStatus'},{av:'Ddo_sistemaversao_data_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_DATA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SISTEMAVERSAO_MELHORIA.ONOPTIONCLICKED","{handler:'E17OE2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SistemaVersao_Id1',fld:'vSISTEMAVERSAO_ID1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SistemaVersao_Id2',fld:'vSISTEMAVERSAO_ID2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SistemaVersao_Id3',fld:'vSISTEMAVERSAO_ID3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV38TFSistemaVersao_Codigo',fld:'vTFSISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSistemaVersao_Codigo_To',fld:'vTFSISTEMAVERSAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSistemaVersao_Id',fld:'vTFSISTEMAVERSAO_ID',pic:'',nv:''},{av:'AV43TFSistemaVersao_Id_Sel',fld:'vTFSISTEMAVERSAO_ID_SEL',pic:'',nv:''},{av:'AV46TFSistemaVersao_Descricao',fld:'vTFSISTEMAVERSAO_DESCRICAO',pic:'',nv:''},{av:'AV47TFSistemaVersao_Descricao_Sel',fld:'vTFSISTEMAVERSAO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV50TFSistemaVersao_Evolucao',fld:'vTFSISTEMAVERSAO_EVOLUCAO',pic:'ZZZ9',nv:0},{av:'AV51TFSistemaVersao_Evolucao_To',fld:'vTFSISTEMAVERSAO_EVOLUCAO_TO',pic:'ZZZ9',nv:0},{av:'AV54TFSistemaVersao_Melhoria',fld:'vTFSISTEMAVERSAO_MELHORIA',pic:'ZZZ9',nv:0},{av:'AV55TFSistemaVersao_Melhoria_To',fld:'vTFSISTEMAVERSAO_MELHORIA_TO',pic:'ZZZ9',nv:0},{av:'AV58TFSistemaVersao_Correcao',fld:'vTFSISTEMAVERSAO_CORRECAO',pic:'ZZZZZZZ9',nv:0},{av:'AV59TFSistemaVersao_Correcao_To',fld:'vTFSISTEMAVERSAO_CORRECAO_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV62TFSistemaVersao_Data',fld:'vTFSISTEMAVERSAO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV63TFSistemaVersao_Data_To',fld:'vTFSISTEMAVERSAO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV31ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV40ddo_SistemaVersao_CodigoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_SistemaVersao_IdTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_IDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_SistemaVersao_DescricaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SistemaVersao_EvolucaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_EVOLUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SistemaVersao_MelhoriaTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_MELHORIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SistemaVersao_CorrecaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_CORRECAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_SistemaVersao_DataTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV101Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1859SistemaVersao_Codigo',fld:'SISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV71SistemaVersao_SistemaCod',fld:'vSISTEMAVERSAO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_sistemaversao_melhoria_Activeeventkey',ctrl:'DDO_SISTEMAVERSAO_MELHORIA',prop:'ActiveEventKey'},{av:'Ddo_sistemaversao_melhoria_Filteredtext_get',ctrl:'DDO_SISTEMAVERSAO_MELHORIA',prop:'FilteredText_get'},{av:'Ddo_sistemaversao_melhoria_Filteredtextto_get',ctrl:'DDO_SISTEMAVERSAO_MELHORIA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_sistemaversao_melhoria_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_MELHORIA',prop:'SortedStatus'},{av:'AV54TFSistemaVersao_Melhoria',fld:'vTFSISTEMAVERSAO_MELHORIA',pic:'ZZZ9',nv:0},{av:'AV55TFSistemaVersao_Melhoria_To',fld:'vTFSISTEMAVERSAO_MELHORIA_TO',pic:'ZZZ9',nv:0},{av:'Ddo_sistemaversao_codigo_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_CODIGO',prop:'SortedStatus'},{av:'Ddo_sistemaversao_id_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_ID',prop:'SortedStatus'},{av:'Ddo_sistemaversao_descricao_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_sistemaversao_evolucao_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_EVOLUCAO',prop:'SortedStatus'},{av:'Ddo_sistemaversao_correcao_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_CORRECAO',prop:'SortedStatus'},{av:'Ddo_sistemaversao_data_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_DATA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SISTEMAVERSAO_CORRECAO.ONOPTIONCLICKED","{handler:'E18OE2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SistemaVersao_Id1',fld:'vSISTEMAVERSAO_ID1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SistemaVersao_Id2',fld:'vSISTEMAVERSAO_ID2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SistemaVersao_Id3',fld:'vSISTEMAVERSAO_ID3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV38TFSistemaVersao_Codigo',fld:'vTFSISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSistemaVersao_Codigo_To',fld:'vTFSISTEMAVERSAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSistemaVersao_Id',fld:'vTFSISTEMAVERSAO_ID',pic:'',nv:''},{av:'AV43TFSistemaVersao_Id_Sel',fld:'vTFSISTEMAVERSAO_ID_SEL',pic:'',nv:''},{av:'AV46TFSistemaVersao_Descricao',fld:'vTFSISTEMAVERSAO_DESCRICAO',pic:'',nv:''},{av:'AV47TFSistemaVersao_Descricao_Sel',fld:'vTFSISTEMAVERSAO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV50TFSistemaVersao_Evolucao',fld:'vTFSISTEMAVERSAO_EVOLUCAO',pic:'ZZZ9',nv:0},{av:'AV51TFSistemaVersao_Evolucao_To',fld:'vTFSISTEMAVERSAO_EVOLUCAO_TO',pic:'ZZZ9',nv:0},{av:'AV54TFSistemaVersao_Melhoria',fld:'vTFSISTEMAVERSAO_MELHORIA',pic:'ZZZ9',nv:0},{av:'AV55TFSistemaVersao_Melhoria_To',fld:'vTFSISTEMAVERSAO_MELHORIA_TO',pic:'ZZZ9',nv:0},{av:'AV58TFSistemaVersao_Correcao',fld:'vTFSISTEMAVERSAO_CORRECAO',pic:'ZZZZZZZ9',nv:0},{av:'AV59TFSistemaVersao_Correcao_To',fld:'vTFSISTEMAVERSAO_CORRECAO_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV62TFSistemaVersao_Data',fld:'vTFSISTEMAVERSAO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV63TFSistemaVersao_Data_To',fld:'vTFSISTEMAVERSAO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV31ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV40ddo_SistemaVersao_CodigoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_SistemaVersao_IdTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_IDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_SistemaVersao_DescricaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SistemaVersao_EvolucaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_EVOLUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SistemaVersao_MelhoriaTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_MELHORIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SistemaVersao_CorrecaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_CORRECAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_SistemaVersao_DataTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV101Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1859SistemaVersao_Codigo',fld:'SISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV71SistemaVersao_SistemaCod',fld:'vSISTEMAVERSAO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_sistemaversao_correcao_Activeeventkey',ctrl:'DDO_SISTEMAVERSAO_CORRECAO',prop:'ActiveEventKey'},{av:'Ddo_sistemaversao_correcao_Filteredtext_get',ctrl:'DDO_SISTEMAVERSAO_CORRECAO',prop:'FilteredText_get'},{av:'Ddo_sistemaversao_correcao_Filteredtextto_get',ctrl:'DDO_SISTEMAVERSAO_CORRECAO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_sistemaversao_correcao_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_CORRECAO',prop:'SortedStatus'},{av:'AV58TFSistemaVersao_Correcao',fld:'vTFSISTEMAVERSAO_CORRECAO',pic:'ZZZZZZZ9',nv:0},{av:'AV59TFSistemaVersao_Correcao_To',fld:'vTFSISTEMAVERSAO_CORRECAO_TO',pic:'ZZZZZZZ9',nv:0},{av:'Ddo_sistemaversao_codigo_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_CODIGO',prop:'SortedStatus'},{av:'Ddo_sistemaversao_id_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_ID',prop:'SortedStatus'},{av:'Ddo_sistemaversao_descricao_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_sistemaversao_evolucao_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_EVOLUCAO',prop:'SortedStatus'},{av:'Ddo_sistemaversao_melhoria_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_MELHORIA',prop:'SortedStatus'},{av:'Ddo_sistemaversao_data_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_DATA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SISTEMAVERSAO_DATA.ONOPTIONCLICKED","{handler:'E19OE2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SistemaVersao_Id1',fld:'vSISTEMAVERSAO_ID1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SistemaVersao_Id2',fld:'vSISTEMAVERSAO_ID2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SistemaVersao_Id3',fld:'vSISTEMAVERSAO_ID3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV38TFSistemaVersao_Codigo',fld:'vTFSISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSistemaVersao_Codigo_To',fld:'vTFSISTEMAVERSAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSistemaVersao_Id',fld:'vTFSISTEMAVERSAO_ID',pic:'',nv:''},{av:'AV43TFSistemaVersao_Id_Sel',fld:'vTFSISTEMAVERSAO_ID_SEL',pic:'',nv:''},{av:'AV46TFSistemaVersao_Descricao',fld:'vTFSISTEMAVERSAO_DESCRICAO',pic:'',nv:''},{av:'AV47TFSistemaVersao_Descricao_Sel',fld:'vTFSISTEMAVERSAO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV50TFSistemaVersao_Evolucao',fld:'vTFSISTEMAVERSAO_EVOLUCAO',pic:'ZZZ9',nv:0},{av:'AV51TFSistemaVersao_Evolucao_To',fld:'vTFSISTEMAVERSAO_EVOLUCAO_TO',pic:'ZZZ9',nv:0},{av:'AV54TFSistemaVersao_Melhoria',fld:'vTFSISTEMAVERSAO_MELHORIA',pic:'ZZZ9',nv:0},{av:'AV55TFSistemaVersao_Melhoria_To',fld:'vTFSISTEMAVERSAO_MELHORIA_TO',pic:'ZZZ9',nv:0},{av:'AV58TFSistemaVersao_Correcao',fld:'vTFSISTEMAVERSAO_CORRECAO',pic:'ZZZZZZZ9',nv:0},{av:'AV59TFSistemaVersao_Correcao_To',fld:'vTFSISTEMAVERSAO_CORRECAO_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV62TFSistemaVersao_Data',fld:'vTFSISTEMAVERSAO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV63TFSistemaVersao_Data_To',fld:'vTFSISTEMAVERSAO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV31ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV40ddo_SistemaVersao_CodigoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_SistemaVersao_IdTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_IDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_SistemaVersao_DescricaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SistemaVersao_EvolucaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_EVOLUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SistemaVersao_MelhoriaTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_MELHORIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SistemaVersao_CorrecaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_CORRECAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_SistemaVersao_DataTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV101Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1859SistemaVersao_Codigo',fld:'SISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV71SistemaVersao_SistemaCod',fld:'vSISTEMAVERSAO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_sistemaversao_data_Activeeventkey',ctrl:'DDO_SISTEMAVERSAO_DATA',prop:'ActiveEventKey'},{av:'Ddo_sistemaversao_data_Filteredtext_get',ctrl:'DDO_SISTEMAVERSAO_DATA',prop:'FilteredText_get'},{av:'Ddo_sistemaversao_data_Filteredtextto_get',ctrl:'DDO_SISTEMAVERSAO_DATA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_sistemaversao_data_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_DATA',prop:'SortedStatus'},{av:'AV62TFSistemaVersao_Data',fld:'vTFSISTEMAVERSAO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV63TFSistemaVersao_Data_To',fld:'vTFSISTEMAVERSAO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_sistemaversao_codigo_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_CODIGO',prop:'SortedStatus'},{av:'Ddo_sistemaversao_id_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_ID',prop:'SortedStatus'},{av:'Ddo_sistemaversao_descricao_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_sistemaversao_evolucao_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_EVOLUCAO',prop:'SortedStatus'},{av:'Ddo_sistemaversao_melhoria_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_MELHORIA',prop:'SortedStatus'},{av:'Ddo_sistemaversao_correcao_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_CORRECAO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E32OE2',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1859SistemaVersao_Codigo',fld:'SISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV71SistemaVersao_SistemaCod',fld:'vSISTEMAVERSAO_SISTEMACOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV28Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Enabled',ctrl:'vUPDATE',prop:'Enabled'},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV29Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Enabled',ctrl:'vDELETE',prop:'Enabled'},{av:'edtSistemaVersao_Id_Link',ctrl:'SISTEMAVERSAO_ID',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E20OE2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SistemaVersao_Id1',fld:'vSISTEMAVERSAO_ID1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SistemaVersao_Id2',fld:'vSISTEMAVERSAO_ID2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SistemaVersao_Id3',fld:'vSISTEMAVERSAO_ID3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV38TFSistemaVersao_Codigo',fld:'vTFSISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSistemaVersao_Codigo_To',fld:'vTFSISTEMAVERSAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSistemaVersao_Id',fld:'vTFSISTEMAVERSAO_ID',pic:'',nv:''},{av:'AV43TFSistemaVersao_Id_Sel',fld:'vTFSISTEMAVERSAO_ID_SEL',pic:'',nv:''},{av:'AV46TFSistemaVersao_Descricao',fld:'vTFSISTEMAVERSAO_DESCRICAO',pic:'',nv:''},{av:'AV47TFSistemaVersao_Descricao_Sel',fld:'vTFSISTEMAVERSAO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV50TFSistemaVersao_Evolucao',fld:'vTFSISTEMAVERSAO_EVOLUCAO',pic:'ZZZ9',nv:0},{av:'AV51TFSistemaVersao_Evolucao_To',fld:'vTFSISTEMAVERSAO_EVOLUCAO_TO',pic:'ZZZ9',nv:0},{av:'AV54TFSistemaVersao_Melhoria',fld:'vTFSISTEMAVERSAO_MELHORIA',pic:'ZZZ9',nv:0},{av:'AV55TFSistemaVersao_Melhoria_To',fld:'vTFSISTEMAVERSAO_MELHORIA_TO',pic:'ZZZ9',nv:0},{av:'AV58TFSistemaVersao_Correcao',fld:'vTFSISTEMAVERSAO_CORRECAO',pic:'ZZZZZZZ9',nv:0},{av:'AV59TFSistemaVersao_Correcao_To',fld:'vTFSISTEMAVERSAO_CORRECAO_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV62TFSistemaVersao_Data',fld:'vTFSISTEMAVERSAO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV63TFSistemaVersao_Data_To',fld:'vTFSISTEMAVERSAO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV31ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV40ddo_SistemaVersao_CodigoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_SistemaVersao_IdTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_IDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_SistemaVersao_DescricaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SistemaVersao_EvolucaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_EVOLUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SistemaVersao_MelhoriaTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_MELHORIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SistemaVersao_CorrecaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_CORRECAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_SistemaVersao_DataTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV101Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1859SistemaVersao_Codigo',fld:'SISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV71SistemaVersao_SistemaCod',fld:'vSISTEMAVERSAO_SISTEMACOD',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E25OE2',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E21OE2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SistemaVersao_Id1',fld:'vSISTEMAVERSAO_ID1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SistemaVersao_Id2',fld:'vSISTEMAVERSAO_ID2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SistemaVersao_Id3',fld:'vSISTEMAVERSAO_ID3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV38TFSistemaVersao_Codigo',fld:'vTFSISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSistemaVersao_Codigo_To',fld:'vTFSISTEMAVERSAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSistemaVersao_Id',fld:'vTFSISTEMAVERSAO_ID',pic:'',nv:''},{av:'AV43TFSistemaVersao_Id_Sel',fld:'vTFSISTEMAVERSAO_ID_SEL',pic:'',nv:''},{av:'AV46TFSistemaVersao_Descricao',fld:'vTFSISTEMAVERSAO_DESCRICAO',pic:'',nv:''},{av:'AV47TFSistemaVersao_Descricao_Sel',fld:'vTFSISTEMAVERSAO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV50TFSistemaVersao_Evolucao',fld:'vTFSISTEMAVERSAO_EVOLUCAO',pic:'ZZZ9',nv:0},{av:'AV51TFSistemaVersao_Evolucao_To',fld:'vTFSISTEMAVERSAO_EVOLUCAO_TO',pic:'ZZZ9',nv:0},{av:'AV54TFSistemaVersao_Melhoria',fld:'vTFSISTEMAVERSAO_MELHORIA',pic:'ZZZ9',nv:0},{av:'AV55TFSistemaVersao_Melhoria_To',fld:'vTFSISTEMAVERSAO_MELHORIA_TO',pic:'ZZZ9',nv:0},{av:'AV58TFSistemaVersao_Correcao',fld:'vTFSISTEMAVERSAO_CORRECAO',pic:'ZZZZZZZ9',nv:0},{av:'AV59TFSistemaVersao_Correcao_To',fld:'vTFSISTEMAVERSAO_CORRECAO_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV62TFSistemaVersao_Data',fld:'vTFSISTEMAVERSAO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV63TFSistemaVersao_Data_To',fld:'vTFSISTEMAVERSAO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV31ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV40ddo_SistemaVersao_CodigoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_SistemaVersao_IdTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_IDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_SistemaVersao_DescricaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SistemaVersao_EvolucaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_EVOLUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SistemaVersao_MelhoriaTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_MELHORIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SistemaVersao_CorrecaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_CORRECAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_SistemaVersao_DataTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV101Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1859SistemaVersao_Codigo',fld:'SISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV71SistemaVersao_SistemaCod',fld:'vSISTEMAVERSAO_SISTEMACOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SistemaVersao_Id2',fld:'vSISTEMAVERSAO_ID2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SistemaVersao_Id3',fld:'vSISTEMAVERSAO_ID3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SistemaVersao_Id1',fld:'vSISTEMAVERSAO_ID1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavSistemaversao_id2_Visible',ctrl:'vSISTEMAVERSAO_ID2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavSistemaversao_id3_Visible',ctrl:'vSISTEMAVERSAO_ID3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavSistemaversao_id1_Visible',ctrl:'vSISTEMAVERSAO_ID1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E26OE2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavSistemaversao_id1_Visible',ctrl:'vSISTEMAVERSAO_ID1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E27OE2',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E22OE2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SistemaVersao_Id1',fld:'vSISTEMAVERSAO_ID1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SistemaVersao_Id2',fld:'vSISTEMAVERSAO_ID2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SistemaVersao_Id3',fld:'vSISTEMAVERSAO_ID3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV38TFSistemaVersao_Codigo',fld:'vTFSISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSistemaVersao_Codigo_To',fld:'vTFSISTEMAVERSAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSistemaVersao_Id',fld:'vTFSISTEMAVERSAO_ID',pic:'',nv:''},{av:'AV43TFSistemaVersao_Id_Sel',fld:'vTFSISTEMAVERSAO_ID_SEL',pic:'',nv:''},{av:'AV46TFSistemaVersao_Descricao',fld:'vTFSISTEMAVERSAO_DESCRICAO',pic:'',nv:''},{av:'AV47TFSistemaVersao_Descricao_Sel',fld:'vTFSISTEMAVERSAO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV50TFSistemaVersao_Evolucao',fld:'vTFSISTEMAVERSAO_EVOLUCAO',pic:'ZZZ9',nv:0},{av:'AV51TFSistemaVersao_Evolucao_To',fld:'vTFSISTEMAVERSAO_EVOLUCAO_TO',pic:'ZZZ9',nv:0},{av:'AV54TFSistemaVersao_Melhoria',fld:'vTFSISTEMAVERSAO_MELHORIA',pic:'ZZZ9',nv:0},{av:'AV55TFSistemaVersao_Melhoria_To',fld:'vTFSISTEMAVERSAO_MELHORIA_TO',pic:'ZZZ9',nv:0},{av:'AV58TFSistemaVersao_Correcao',fld:'vTFSISTEMAVERSAO_CORRECAO',pic:'ZZZZZZZ9',nv:0},{av:'AV59TFSistemaVersao_Correcao_To',fld:'vTFSISTEMAVERSAO_CORRECAO_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV62TFSistemaVersao_Data',fld:'vTFSISTEMAVERSAO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV63TFSistemaVersao_Data_To',fld:'vTFSISTEMAVERSAO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV31ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV40ddo_SistemaVersao_CodigoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_SistemaVersao_IdTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_IDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_SistemaVersao_DescricaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SistemaVersao_EvolucaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_EVOLUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SistemaVersao_MelhoriaTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_MELHORIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SistemaVersao_CorrecaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_CORRECAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_SistemaVersao_DataTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV101Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1859SistemaVersao_Codigo',fld:'SISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV71SistemaVersao_SistemaCod',fld:'vSISTEMAVERSAO_SISTEMACOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SistemaVersao_Id2',fld:'vSISTEMAVERSAO_ID2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SistemaVersao_Id3',fld:'vSISTEMAVERSAO_ID3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SistemaVersao_Id1',fld:'vSISTEMAVERSAO_ID1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavSistemaversao_id2_Visible',ctrl:'vSISTEMAVERSAO_ID2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavSistemaversao_id3_Visible',ctrl:'vSISTEMAVERSAO_ID3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavSistemaversao_id1_Visible',ctrl:'vSISTEMAVERSAO_ID1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E28OE2',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavSistemaversao_id2_Visible',ctrl:'vSISTEMAVERSAO_ID2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E23OE2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SistemaVersao_Id1',fld:'vSISTEMAVERSAO_ID1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SistemaVersao_Id2',fld:'vSISTEMAVERSAO_ID2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SistemaVersao_Id3',fld:'vSISTEMAVERSAO_ID3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV38TFSistemaVersao_Codigo',fld:'vTFSISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSistemaVersao_Codigo_To',fld:'vTFSISTEMAVERSAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSistemaVersao_Id',fld:'vTFSISTEMAVERSAO_ID',pic:'',nv:''},{av:'AV43TFSistemaVersao_Id_Sel',fld:'vTFSISTEMAVERSAO_ID_SEL',pic:'',nv:''},{av:'AV46TFSistemaVersao_Descricao',fld:'vTFSISTEMAVERSAO_DESCRICAO',pic:'',nv:''},{av:'AV47TFSistemaVersao_Descricao_Sel',fld:'vTFSISTEMAVERSAO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV50TFSistemaVersao_Evolucao',fld:'vTFSISTEMAVERSAO_EVOLUCAO',pic:'ZZZ9',nv:0},{av:'AV51TFSistemaVersao_Evolucao_To',fld:'vTFSISTEMAVERSAO_EVOLUCAO_TO',pic:'ZZZ9',nv:0},{av:'AV54TFSistemaVersao_Melhoria',fld:'vTFSISTEMAVERSAO_MELHORIA',pic:'ZZZ9',nv:0},{av:'AV55TFSistemaVersao_Melhoria_To',fld:'vTFSISTEMAVERSAO_MELHORIA_TO',pic:'ZZZ9',nv:0},{av:'AV58TFSistemaVersao_Correcao',fld:'vTFSISTEMAVERSAO_CORRECAO',pic:'ZZZZZZZ9',nv:0},{av:'AV59TFSistemaVersao_Correcao_To',fld:'vTFSISTEMAVERSAO_CORRECAO_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV62TFSistemaVersao_Data',fld:'vTFSISTEMAVERSAO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV63TFSistemaVersao_Data_To',fld:'vTFSISTEMAVERSAO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV31ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV40ddo_SistemaVersao_CodigoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_SistemaVersao_IdTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_IDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_SistemaVersao_DescricaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SistemaVersao_EvolucaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_EVOLUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SistemaVersao_MelhoriaTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_MELHORIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SistemaVersao_CorrecaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_CORRECAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_SistemaVersao_DataTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV101Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1859SistemaVersao_Codigo',fld:'SISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV71SistemaVersao_SistemaCod',fld:'vSISTEMAVERSAO_SISTEMACOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SistemaVersao_Id2',fld:'vSISTEMAVERSAO_ID2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SistemaVersao_Id3',fld:'vSISTEMAVERSAO_ID3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SistemaVersao_Id1',fld:'vSISTEMAVERSAO_ID1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavSistemaversao_id2_Visible',ctrl:'vSISTEMAVERSAO_ID2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavSistemaversao_id3_Visible',ctrl:'vSISTEMAVERSAO_ID3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavSistemaversao_id1_Visible',ctrl:'vSISTEMAVERSAO_ID1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E29OE2',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavSistemaversao_id3_Visible',ctrl:'vSISTEMAVERSAO_ID3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("DDO_MANAGEFILTERS.ONOPTIONCLICKED","{handler:'E11OE2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SistemaVersao_Id1',fld:'vSISTEMAVERSAO_ID1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SistemaVersao_Id2',fld:'vSISTEMAVERSAO_ID2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SistemaVersao_Id3',fld:'vSISTEMAVERSAO_ID3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV38TFSistemaVersao_Codigo',fld:'vTFSISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSistemaVersao_Codigo_To',fld:'vTFSISTEMAVERSAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSistemaVersao_Id',fld:'vTFSISTEMAVERSAO_ID',pic:'',nv:''},{av:'AV43TFSistemaVersao_Id_Sel',fld:'vTFSISTEMAVERSAO_ID_SEL',pic:'',nv:''},{av:'AV46TFSistemaVersao_Descricao',fld:'vTFSISTEMAVERSAO_DESCRICAO',pic:'',nv:''},{av:'AV47TFSistemaVersao_Descricao_Sel',fld:'vTFSISTEMAVERSAO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV50TFSistemaVersao_Evolucao',fld:'vTFSISTEMAVERSAO_EVOLUCAO',pic:'ZZZ9',nv:0},{av:'AV51TFSistemaVersao_Evolucao_To',fld:'vTFSISTEMAVERSAO_EVOLUCAO_TO',pic:'ZZZ9',nv:0},{av:'AV54TFSistemaVersao_Melhoria',fld:'vTFSISTEMAVERSAO_MELHORIA',pic:'ZZZ9',nv:0},{av:'AV55TFSistemaVersao_Melhoria_To',fld:'vTFSISTEMAVERSAO_MELHORIA_TO',pic:'ZZZ9',nv:0},{av:'AV58TFSistemaVersao_Correcao',fld:'vTFSISTEMAVERSAO_CORRECAO',pic:'ZZZZZZZ9',nv:0},{av:'AV59TFSistemaVersao_Correcao_To',fld:'vTFSISTEMAVERSAO_CORRECAO_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV62TFSistemaVersao_Data',fld:'vTFSISTEMAVERSAO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV63TFSistemaVersao_Data_To',fld:'vTFSISTEMAVERSAO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV31ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV40ddo_SistemaVersao_CodigoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_SistemaVersao_IdTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_IDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_SistemaVersao_DescricaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SistemaVersao_EvolucaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_EVOLUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SistemaVersao_MelhoriaTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_MELHORIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SistemaVersao_CorrecaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_CORRECAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_SistemaVersao_DataTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV101Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1859SistemaVersao_Codigo',fld:'SISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV71SistemaVersao_SistemaCod',fld:'vSISTEMAVERSAO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_managefilters_Activeeventkey',ctrl:'DDO_MANAGEFILTERS',prop:'ActiveEventKey'}],oparms:[{av:'AV31ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV38TFSistemaVersao_Codigo',fld:'vTFSISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_sistemaversao_codigo_Filteredtext_set',ctrl:'DDO_SISTEMAVERSAO_CODIGO',prop:'FilteredText_set'},{av:'AV39TFSistemaVersao_Codigo_To',fld:'vTFSISTEMAVERSAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_sistemaversao_codigo_Filteredtextto_set',ctrl:'DDO_SISTEMAVERSAO_CODIGO',prop:'FilteredTextTo_set'},{av:'AV42TFSistemaVersao_Id',fld:'vTFSISTEMAVERSAO_ID',pic:'',nv:''},{av:'Ddo_sistemaversao_id_Filteredtext_set',ctrl:'DDO_SISTEMAVERSAO_ID',prop:'FilteredText_set'},{av:'AV43TFSistemaVersao_Id_Sel',fld:'vTFSISTEMAVERSAO_ID_SEL',pic:'',nv:''},{av:'Ddo_sistemaversao_id_Selectedvalue_set',ctrl:'DDO_SISTEMAVERSAO_ID',prop:'SelectedValue_set'},{av:'AV46TFSistemaVersao_Descricao',fld:'vTFSISTEMAVERSAO_DESCRICAO',pic:'',nv:''},{av:'Ddo_sistemaversao_descricao_Filteredtext_set',ctrl:'DDO_SISTEMAVERSAO_DESCRICAO',prop:'FilteredText_set'},{av:'AV47TFSistemaVersao_Descricao_Sel',fld:'vTFSISTEMAVERSAO_DESCRICAO_SEL',pic:'',nv:''},{av:'Ddo_sistemaversao_descricao_Selectedvalue_set',ctrl:'DDO_SISTEMAVERSAO_DESCRICAO',prop:'SelectedValue_set'},{av:'AV50TFSistemaVersao_Evolucao',fld:'vTFSISTEMAVERSAO_EVOLUCAO',pic:'ZZZ9',nv:0},{av:'Ddo_sistemaversao_evolucao_Filteredtext_set',ctrl:'DDO_SISTEMAVERSAO_EVOLUCAO',prop:'FilteredText_set'},{av:'AV51TFSistemaVersao_Evolucao_To',fld:'vTFSISTEMAVERSAO_EVOLUCAO_TO',pic:'ZZZ9',nv:0},{av:'Ddo_sistemaversao_evolucao_Filteredtextto_set',ctrl:'DDO_SISTEMAVERSAO_EVOLUCAO',prop:'FilteredTextTo_set'},{av:'AV54TFSistemaVersao_Melhoria',fld:'vTFSISTEMAVERSAO_MELHORIA',pic:'ZZZ9',nv:0},{av:'Ddo_sistemaversao_melhoria_Filteredtext_set',ctrl:'DDO_SISTEMAVERSAO_MELHORIA',prop:'FilteredText_set'},{av:'AV55TFSistemaVersao_Melhoria_To',fld:'vTFSISTEMAVERSAO_MELHORIA_TO',pic:'ZZZ9',nv:0},{av:'Ddo_sistemaversao_melhoria_Filteredtextto_set',ctrl:'DDO_SISTEMAVERSAO_MELHORIA',prop:'FilteredTextTo_set'},{av:'AV58TFSistemaVersao_Correcao',fld:'vTFSISTEMAVERSAO_CORRECAO',pic:'ZZZZZZZ9',nv:0},{av:'Ddo_sistemaversao_correcao_Filteredtext_set',ctrl:'DDO_SISTEMAVERSAO_CORRECAO',prop:'FilteredText_set'},{av:'AV59TFSistemaVersao_Correcao_To',fld:'vTFSISTEMAVERSAO_CORRECAO_TO',pic:'ZZZZZZZ9',nv:0},{av:'Ddo_sistemaversao_correcao_Filteredtextto_set',ctrl:'DDO_SISTEMAVERSAO_CORRECAO',prop:'FilteredTextTo_set'},{av:'AV62TFSistemaVersao_Data',fld:'vTFSISTEMAVERSAO_DATA',pic:'99/99/99 99:99',nv:''},{av:'Ddo_sistemaversao_data_Filteredtext_set',ctrl:'DDO_SISTEMAVERSAO_DATA',prop:'FilteredText_set'},{av:'AV63TFSistemaVersao_Data_To',fld:'vTFSISTEMAVERSAO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_sistemaversao_data_Filteredtextto_set',ctrl:'DDO_SISTEMAVERSAO_DATA',prop:'FilteredTextTo_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SistemaVersao_Id1',fld:'vSISTEMAVERSAO_ID1',pic:'',nv:''},{av:'Ddo_sistemaversao_data_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_DATA',prop:'SortedStatus'},{av:'Ddo_sistemaversao_correcao_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_CORRECAO',prop:'SortedStatus'},{av:'Ddo_sistemaversao_melhoria_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_MELHORIA',prop:'SortedStatus'},{av:'Ddo_sistemaversao_evolucao_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_EVOLUCAO',prop:'SortedStatus'},{av:'Ddo_sistemaversao_descricao_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_sistemaversao_id_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_ID',prop:'SortedStatus'},{av:'Ddo_sistemaversao_codigo_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_CODIGO',prop:'SortedStatus'},{av:'AV64DDO_SistemaVersao_DataAuxDate',fld:'vDDO_SISTEMAVERSAO_DATAAUXDATE',pic:'',nv:''},{av:'AV65DDO_SistemaVersao_DataAuxDateTo',fld:'vDDO_SISTEMAVERSAO_DATAAUXDATETO',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SistemaVersao_Id2',fld:'vSISTEMAVERSAO_ID2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SistemaVersao_Id3',fld:'vSISTEMAVERSAO_ID3',pic:'',nv:''},{av:'edtavSistemaversao_id1_Visible',ctrl:'vSISTEMAVERSAO_ID1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'edtavSistemaversao_id2_Visible',ctrl:'vSISTEMAVERSAO_ID2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavSistemaversao_id3_Visible',ctrl:'vSISTEMAVERSAO_ID3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E24OE2',iparms:[{av:'A1859SistemaVersao_Codigo',fld:'SISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV71SistemaVersao_SistemaCod',fld:'vSISTEMAVERSAO_SISTEMACOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV71SistemaVersao_SistemaCod',fld:'vSISTEMAVERSAO_SISTEMACOD',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_sistemaversao_codigo_Activeeventkey = "";
         Ddo_sistemaversao_codigo_Filteredtext_get = "";
         Ddo_sistemaversao_codigo_Filteredtextto_get = "";
         Ddo_sistemaversao_id_Activeeventkey = "";
         Ddo_sistemaversao_id_Filteredtext_get = "";
         Ddo_sistemaversao_id_Selectedvalue_get = "";
         Ddo_sistemaversao_descricao_Activeeventkey = "";
         Ddo_sistemaversao_descricao_Filteredtext_get = "";
         Ddo_sistemaversao_descricao_Selectedvalue_get = "";
         Ddo_sistemaversao_evolucao_Activeeventkey = "";
         Ddo_sistemaversao_evolucao_Filteredtext_get = "";
         Ddo_sistemaversao_evolucao_Filteredtextto_get = "";
         Ddo_sistemaversao_melhoria_Activeeventkey = "";
         Ddo_sistemaversao_melhoria_Filteredtext_get = "";
         Ddo_sistemaversao_melhoria_Filteredtextto_get = "";
         Ddo_sistemaversao_correcao_Activeeventkey = "";
         Ddo_sistemaversao_correcao_Filteredtext_get = "";
         Ddo_sistemaversao_correcao_Filteredtextto_get = "";
         Ddo_sistemaversao_data_Activeeventkey = "";
         Ddo_sistemaversao_data_Filteredtext_get = "";
         Ddo_sistemaversao_data_Filteredtextto_get = "";
         Ddo_managefilters_Activeeventkey = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17SistemaVersao_Id1 = "";
         AV19DynamicFiltersSelector2 = "";
         AV21SistemaVersao_Id2 = "";
         AV23DynamicFiltersSelector3 = "";
         AV25SistemaVersao_Id3 = "";
         AV42TFSistemaVersao_Id = "";
         AV43TFSistemaVersao_Id_Sel = "";
         AV46TFSistemaVersao_Descricao = "";
         AV47TFSistemaVersao_Descricao_Sel = "";
         AV62TFSistemaVersao_Data = (DateTime)(DateTime.MinValue);
         AV63TFSistemaVersao_Data_To = (DateTime)(DateTime.MinValue);
         AV40ddo_SistemaVersao_CodigoTitleControlIdToReplace = "";
         AV44ddo_SistemaVersao_IdTitleControlIdToReplace = "";
         AV48ddo_SistemaVersao_DescricaoTitleControlIdToReplace = "";
         AV52ddo_SistemaVersao_EvolucaoTitleControlIdToReplace = "";
         AV56ddo_SistemaVersao_MelhoriaTitleControlIdToReplace = "";
         AV60ddo_SistemaVersao_CorrecaoTitleControlIdToReplace = "";
         AV66ddo_SistemaVersao_DataTitleControlIdToReplace = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV101Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV35ManageFiltersData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV67DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV37SistemaVersao_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV41SistemaVersao_IdTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV45SistemaVersao_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV49SistemaVersao_EvolucaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV53SistemaVersao_MelhoriaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV57SistemaVersao_CorrecaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV61SistemaVersao_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_managefilters_Icon = "";
         Ddo_managefilters_Caption = "";
         Ddo_sistemaversao_codigo_Filteredtext_set = "";
         Ddo_sistemaversao_codigo_Filteredtextto_set = "";
         Ddo_sistemaversao_codigo_Sortedstatus = "";
         Ddo_sistemaversao_id_Filteredtext_set = "";
         Ddo_sistemaversao_id_Selectedvalue_set = "";
         Ddo_sistemaversao_id_Sortedstatus = "";
         Ddo_sistemaversao_descricao_Filteredtext_set = "";
         Ddo_sistemaversao_descricao_Selectedvalue_set = "";
         Ddo_sistemaversao_descricao_Sortedstatus = "";
         Ddo_sistemaversao_evolucao_Filteredtext_set = "";
         Ddo_sistemaversao_evolucao_Filteredtextto_set = "";
         Ddo_sistemaversao_evolucao_Sortedstatus = "";
         Ddo_sistemaversao_melhoria_Filteredtext_set = "";
         Ddo_sistemaversao_melhoria_Filteredtextto_set = "";
         Ddo_sistemaversao_melhoria_Sortedstatus = "";
         Ddo_sistemaversao_correcao_Filteredtext_set = "";
         Ddo_sistemaversao_correcao_Filteredtextto_set = "";
         Ddo_sistemaversao_correcao_Sortedstatus = "";
         Ddo_sistemaversao_data_Filteredtext_set = "";
         Ddo_sistemaversao_data_Filteredtextto_set = "";
         Ddo_sistemaversao_data_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV64DDO_SistemaVersao_DataAuxDate = DateTime.MinValue;
         AV65DDO_SistemaVersao_DataAuxDateTo = DateTime.MinValue;
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Update = "";
         AV99Update_GXI = "";
         AV29Delete = "";
         AV100Delete_GXI = "";
         A1860SistemaVersao_Id = "";
         A1861SistemaVersao_Descricao = "";
         A1865SistemaVersao_Data = (DateTime)(DateTime.MinValue);
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV76WWSistemaVersaoDS_3_Sistemaversao_id1 = "";
         lV80WWSistemaVersaoDS_7_Sistemaversao_id2 = "";
         lV84WWSistemaVersaoDS_11_Sistemaversao_id3 = "";
         lV87WWSistemaVersaoDS_14_Tfsistemaversao_id = "";
         lV89WWSistemaVersaoDS_16_Tfsistemaversao_descricao = "";
         AV74WWSistemaVersaoDS_1_Dynamicfiltersselector1 = "";
         AV76WWSistemaVersaoDS_3_Sistemaversao_id1 = "";
         AV78WWSistemaVersaoDS_5_Dynamicfiltersselector2 = "";
         AV80WWSistemaVersaoDS_7_Sistemaversao_id2 = "";
         AV82WWSistemaVersaoDS_9_Dynamicfiltersselector3 = "";
         AV84WWSistemaVersaoDS_11_Sistemaversao_id3 = "";
         AV88WWSistemaVersaoDS_15_Tfsistemaversao_id_sel = "";
         AV87WWSistemaVersaoDS_14_Tfsistemaversao_id = "";
         AV90WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel = "";
         AV89WWSistemaVersaoDS_16_Tfsistemaversao_descricao = "";
         AV97WWSistemaVersaoDS_24_Tfsistemaversao_data = (DateTime)(DateTime.MinValue);
         AV98WWSistemaVersaoDS_25_Tfsistemaversao_data_to = (DateTime)(DateTime.MinValue);
         H00OE2_A1865SistemaVersao_Data = new DateTime[] {DateTime.MinValue} ;
         H00OE2_A1864SistemaVersao_Correcao = new int[1] ;
         H00OE2_n1864SistemaVersao_Correcao = new bool[] {false} ;
         H00OE2_A1863SistemaVersao_Melhoria = new short[1] ;
         H00OE2_n1863SistemaVersao_Melhoria = new bool[] {false} ;
         H00OE2_A1862SistemaVersao_Evolucao = new short[1] ;
         H00OE2_n1862SistemaVersao_Evolucao = new bool[] {false} ;
         H00OE2_A1861SistemaVersao_Descricao = new String[] {""} ;
         H00OE2_A1860SistemaVersao_Id = new String[] {""} ;
         H00OE2_A1859SistemaVersao_Codigo = new int[1] ;
         H00OE3_AGRID_nRecordCount = new long[1] ;
         AV7HTTPRequest = new GxHttpRequest( context);
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         GridRow = new GXWebRow();
         AV32ManageFiltersXml = "";
         GXt_char2 = "";
         imgInsert_Link = "";
         AV36ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV33ManageFiltersItems = new GxObjectCollection( context, "GridStateCollection.Item", "", "wwpbaseobjects.SdtGridStateCollection_Item", "GeneXus.Programs");
         AV34ManageFiltersItem = new wwpbaseobjects.SdtGridStateCollection_Item(context);
         AV30Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblSistemaversaotitle_Jsonclick = "";
         imgInsert_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwsistemaversao__default(),
            new Object[][] {
                new Object[] {
               H00OE2_A1865SistemaVersao_Data, H00OE2_A1864SistemaVersao_Correcao, H00OE2_n1864SistemaVersao_Correcao, H00OE2_A1863SistemaVersao_Melhoria, H00OE2_n1863SistemaVersao_Melhoria, H00OE2_A1862SistemaVersao_Evolucao, H00OE2_n1862SistemaVersao_Evolucao, H00OE2_A1861SistemaVersao_Descricao, H00OE2_A1860SistemaVersao_Id, H00OE2_A1859SistemaVersao_Codigo
               }
               , new Object[] {
               H00OE3_AGRID_nRecordCount
               }
            }
         );
         AV101Pgmname = "WWSistemaVersao";
         /* GeneXus formulas. */
         AV101Pgmname = "WWSistemaVersao";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_88 ;
      private short nGXsfl_88_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV20DynamicFiltersOperator2 ;
      private short AV24DynamicFiltersOperator3 ;
      private short AV50TFSistemaVersao_Evolucao ;
      private short AV51TFSistemaVersao_Evolucao_To ;
      private short AV54TFSistemaVersao_Melhoria ;
      private short AV55TFSistemaVersao_Melhoria_To ;
      private short AV31ManageFiltersExecutionStep ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A1862SistemaVersao_Evolucao ;
      private short A1863SistemaVersao_Melhoria ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_88_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV75WWSistemaVersaoDS_2_Dynamicfiltersoperator1 ;
      private short AV79WWSistemaVersaoDS_6_Dynamicfiltersoperator2 ;
      private short AV83WWSistemaVersaoDS_10_Dynamicfiltersoperator3 ;
      private short AV91WWSistemaVersaoDS_18_Tfsistemaversao_evolucao ;
      private short AV92WWSistemaVersaoDS_19_Tfsistemaversao_evolucao_to ;
      private short AV93WWSistemaVersaoDS_20_Tfsistemaversao_melhoria ;
      private short AV94WWSistemaVersaoDS_21_Tfsistemaversao_melhoria_to ;
      private short edtSistemaVersao_Codigo_Titleformat ;
      private short edtSistemaVersao_Id_Titleformat ;
      private short edtSistemaVersao_Descricao_Titleformat ;
      private short edtSistemaVersao_Evolucao_Titleformat ;
      private short edtSistemaVersao_Melhoria_Titleformat ;
      private short edtSistemaVersao_Correcao_Titleformat ;
      private short edtSistemaVersao_Data_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV38TFSistemaVersao_Codigo ;
      private int AV39TFSistemaVersao_Codigo_To ;
      private int AV58TFSistemaVersao_Correcao ;
      private int AV59TFSistemaVersao_Correcao_To ;
      private int A1859SistemaVersao_Codigo ;
      private int AV71SistemaVersao_SistemaCod ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_sistemaversao_id_Datalistupdateminimumcharacters ;
      private int Ddo_sistemaversao_descricao_Datalistupdateminimumcharacters ;
      private int edtavManagefiltersexecutionstep_Visible ;
      private int edtavTfsistemaversao_codigo_Visible ;
      private int edtavTfsistemaversao_codigo_to_Visible ;
      private int edtavTfsistemaversao_id_Visible ;
      private int edtavTfsistemaversao_id_sel_Visible ;
      private int edtavTfsistemaversao_descricao_Visible ;
      private int edtavTfsistemaversao_descricao_sel_Visible ;
      private int edtavTfsistemaversao_evolucao_Visible ;
      private int edtavTfsistemaversao_evolucao_to_Visible ;
      private int edtavTfsistemaversao_melhoria_Visible ;
      private int edtavTfsistemaversao_melhoria_to_Visible ;
      private int edtavTfsistemaversao_correcao_Visible ;
      private int edtavTfsistemaversao_correcao_to_Visible ;
      private int edtavTfsistemaversao_data_Visible ;
      private int edtavTfsistemaversao_data_to_Visible ;
      private int edtavDdo_sistemaversao_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_sistemaversao_idtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_sistemaversao_descricaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_sistemaversao_evolucaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_sistemaversao_melhoriatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_sistemaversao_correcaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_sistemaversao_datatitlecontrolidtoreplace_Visible ;
      private int A1864SistemaVersao_Correcao ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV85WWSistemaVersaoDS_12_Tfsistemaversao_codigo ;
      private int AV86WWSistemaVersaoDS_13_Tfsistemaversao_codigo_to ;
      private int AV95WWSistemaVersaoDS_22_Tfsistemaversao_correcao ;
      private int AV96WWSistemaVersaoDS_23_Tfsistemaversao_correcao_to ;
      private int edtavOrdereddsc_Visible ;
      private int AV68PageToGo ;
      private int edtavUpdate_Enabled ;
      private int edtavDelete_Enabled ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int imgInsert_Enabled ;
      private int edtavSistemaversao_id1_Visible ;
      private int edtavSistemaversao_id2_Visible ;
      private int edtavSistemaversao_id3_Visible ;
      private int AV102GXV1 ;
      private int AV103GXV2 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV69GridCurrentPage ;
      private long AV70GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_sistemaversao_codigo_Activeeventkey ;
      private String Ddo_sistemaversao_codigo_Filteredtext_get ;
      private String Ddo_sistemaversao_codigo_Filteredtextto_get ;
      private String Ddo_sistemaversao_id_Activeeventkey ;
      private String Ddo_sistemaversao_id_Filteredtext_get ;
      private String Ddo_sistemaversao_id_Selectedvalue_get ;
      private String Ddo_sistemaversao_descricao_Activeeventkey ;
      private String Ddo_sistemaversao_descricao_Filteredtext_get ;
      private String Ddo_sistemaversao_descricao_Selectedvalue_get ;
      private String Ddo_sistemaversao_evolucao_Activeeventkey ;
      private String Ddo_sistemaversao_evolucao_Filteredtext_get ;
      private String Ddo_sistemaversao_evolucao_Filteredtextto_get ;
      private String Ddo_sistemaversao_melhoria_Activeeventkey ;
      private String Ddo_sistemaversao_melhoria_Filteredtext_get ;
      private String Ddo_sistemaversao_melhoria_Filteredtextto_get ;
      private String Ddo_sistemaversao_correcao_Activeeventkey ;
      private String Ddo_sistemaversao_correcao_Filteredtext_get ;
      private String Ddo_sistemaversao_correcao_Filteredtextto_get ;
      private String Ddo_sistemaversao_data_Activeeventkey ;
      private String Ddo_sistemaversao_data_Filteredtext_get ;
      private String Ddo_sistemaversao_data_Filteredtextto_get ;
      private String Ddo_managefilters_Activeeventkey ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_88_idx="0001" ;
      private String AV17SistemaVersao_Id1 ;
      private String AV21SistemaVersao_Id2 ;
      private String AV25SistemaVersao_Id3 ;
      private String AV42TFSistemaVersao_Id ;
      private String AV43TFSistemaVersao_Id_Sel ;
      private String AV101Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Ddo_managefilters_Icon ;
      private String Ddo_managefilters_Caption ;
      private String Ddo_managefilters_Tooltip ;
      private String Ddo_managefilters_Cls ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_sistemaversao_codigo_Caption ;
      private String Ddo_sistemaversao_codigo_Tooltip ;
      private String Ddo_sistemaversao_codigo_Cls ;
      private String Ddo_sistemaversao_codigo_Filteredtext_set ;
      private String Ddo_sistemaversao_codigo_Filteredtextto_set ;
      private String Ddo_sistemaversao_codigo_Dropdownoptionstype ;
      private String Ddo_sistemaversao_codigo_Titlecontrolidtoreplace ;
      private String Ddo_sistemaversao_codigo_Sortedstatus ;
      private String Ddo_sistemaversao_codigo_Filtertype ;
      private String Ddo_sistemaversao_codigo_Sortasc ;
      private String Ddo_sistemaversao_codigo_Sortdsc ;
      private String Ddo_sistemaversao_codigo_Cleanfilter ;
      private String Ddo_sistemaversao_codigo_Rangefilterfrom ;
      private String Ddo_sistemaversao_codigo_Rangefilterto ;
      private String Ddo_sistemaversao_codigo_Searchbuttontext ;
      private String Ddo_sistemaversao_id_Caption ;
      private String Ddo_sistemaversao_id_Tooltip ;
      private String Ddo_sistemaversao_id_Cls ;
      private String Ddo_sistemaversao_id_Filteredtext_set ;
      private String Ddo_sistemaversao_id_Selectedvalue_set ;
      private String Ddo_sistemaversao_id_Dropdownoptionstype ;
      private String Ddo_sistemaversao_id_Titlecontrolidtoreplace ;
      private String Ddo_sistemaversao_id_Sortedstatus ;
      private String Ddo_sistemaversao_id_Filtertype ;
      private String Ddo_sistemaversao_id_Datalisttype ;
      private String Ddo_sistemaversao_id_Datalistproc ;
      private String Ddo_sistemaversao_id_Sortasc ;
      private String Ddo_sistemaversao_id_Sortdsc ;
      private String Ddo_sistemaversao_id_Loadingdata ;
      private String Ddo_sistemaversao_id_Cleanfilter ;
      private String Ddo_sistemaversao_id_Noresultsfound ;
      private String Ddo_sistemaversao_id_Searchbuttontext ;
      private String Ddo_sistemaversao_descricao_Caption ;
      private String Ddo_sistemaversao_descricao_Tooltip ;
      private String Ddo_sistemaversao_descricao_Cls ;
      private String Ddo_sistemaversao_descricao_Filteredtext_set ;
      private String Ddo_sistemaversao_descricao_Selectedvalue_set ;
      private String Ddo_sistemaversao_descricao_Dropdownoptionstype ;
      private String Ddo_sistemaversao_descricao_Titlecontrolidtoreplace ;
      private String Ddo_sistemaversao_descricao_Sortedstatus ;
      private String Ddo_sistemaversao_descricao_Filtertype ;
      private String Ddo_sistemaversao_descricao_Datalisttype ;
      private String Ddo_sistemaversao_descricao_Datalistproc ;
      private String Ddo_sistemaversao_descricao_Sortasc ;
      private String Ddo_sistemaversao_descricao_Sortdsc ;
      private String Ddo_sistemaversao_descricao_Loadingdata ;
      private String Ddo_sistemaversao_descricao_Cleanfilter ;
      private String Ddo_sistemaversao_descricao_Noresultsfound ;
      private String Ddo_sistemaversao_descricao_Searchbuttontext ;
      private String Ddo_sistemaversao_evolucao_Caption ;
      private String Ddo_sistemaversao_evolucao_Tooltip ;
      private String Ddo_sistemaversao_evolucao_Cls ;
      private String Ddo_sistemaversao_evolucao_Filteredtext_set ;
      private String Ddo_sistemaversao_evolucao_Filteredtextto_set ;
      private String Ddo_sistemaversao_evolucao_Dropdownoptionstype ;
      private String Ddo_sistemaversao_evolucao_Titlecontrolidtoreplace ;
      private String Ddo_sistemaversao_evolucao_Sortedstatus ;
      private String Ddo_sistemaversao_evolucao_Filtertype ;
      private String Ddo_sistemaversao_evolucao_Sortasc ;
      private String Ddo_sistemaversao_evolucao_Sortdsc ;
      private String Ddo_sistemaversao_evolucao_Cleanfilter ;
      private String Ddo_sistemaversao_evolucao_Rangefilterfrom ;
      private String Ddo_sistemaversao_evolucao_Rangefilterto ;
      private String Ddo_sistemaversao_evolucao_Searchbuttontext ;
      private String Ddo_sistemaversao_melhoria_Caption ;
      private String Ddo_sistemaversao_melhoria_Tooltip ;
      private String Ddo_sistemaversao_melhoria_Cls ;
      private String Ddo_sistemaversao_melhoria_Filteredtext_set ;
      private String Ddo_sistemaversao_melhoria_Filteredtextto_set ;
      private String Ddo_sistemaversao_melhoria_Dropdownoptionstype ;
      private String Ddo_sistemaversao_melhoria_Titlecontrolidtoreplace ;
      private String Ddo_sistemaversao_melhoria_Sortedstatus ;
      private String Ddo_sistemaversao_melhoria_Filtertype ;
      private String Ddo_sistemaversao_melhoria_Sortasc ;
      private String Ddo_sistemaversao_melhoria_Sortdsc ;
      private String Ddo_sistemaversao_melhoria_Cleanfilter ;
      private String Ddo_sistemaversao_melhoria_Rangefilterfrom ;
      private String Ddo_sistemaversao_melhoria_Rangefilterto ;
      private String Ddo_sistemaversao_melhoria_Searchbuttontext ;
      private String Ddo_sistemaversao_correcao_Caption ;
      private String Ddo_sistemaversao_correcao_Tooltip ;
      private String Ddo_sistemaversao_correcao_Cls ;
      private String Ddo_sistemaversao_correcao_Filteredtext_set ;
      private String Ddo_sistemaversao_correcao_Filteredtextto_set ;
      private String Ddo_sistemaversao_correcao_Dropdownoptionstype ;
      private String Ddo_sistemaversao_correcao_Titlecontrolidtoreplace ;
      private String Ddo_sistemaversao_correcao_Sortedstatus ;
      private String Ddo_sistemaversao_correcao_Filtertype ;
      private String Ddo_sistemaversao_correcao_Sortasc ;
      private String Ddo_sistemaversao_correcao_Sortdsc ;
      private String Ddo_sistemaversao_correcao_Cleanfilter ;
      private String Ddo_sistemaversao_correcao_Rangefilterfrom ;
      private String Ddo_sistemaversao_correcao_Rangefilterto ;
      private String Ddo_sistemaversao_correcao_Searchbuttontext ;
      private String Ddo_sistemaversao_data_Caption ;
      private String Ddo_sistemaversao_data_Tooltip ;
      private String Ddo_sistemaversao_data_Cls ;
      private String Ddo_sistemaversao_data_Filteredtext_set ;
      private String Ddo_sistemaversao_data_Filteredtextto_set ;
      private String Ddo_sistemaversao_data_Dropdownoptionstype ;
      private String Ddo_sistemaversao_data_Titlecontrolidtoreplace ;
      private String Ddo_sistemaversao_data_Sortedstatus ;
      private String Ddo_sistemaversao_data_Filtertype ;
      private String Ddo_sistemaversao_data_Sortasc ;
      private String Ddo_sistemaversao_data_Sortdsc ;
      private String Ddo_sistemaversao_data_Cleanfilter ;
      private String Ddo_sistemaversao_data_Rangefilterfrom ;
      private String Ddo_sistemaversao_data_Rangefilterto ;
      private String Ddo_sistemaversao_data_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavManagefiltersexecutionstep_Internalname ;
      private String edtavManagefiltersexecutionstep_Jsonclick ;
      private String edtavTfsistemaversao_codigo_Internalname ;
      private String edtavTfsistemaversao_codigo_Jsonclick ;
      private String edtavTfsistemaversao_codigo_to_Internalname ;
      private String edtavTfsistemaversao_codigo_to_Jsonclick ;
      private String edtavTfsistemaversao_id_Internalname ;
      private String edtavTfsistemaversao_id_Jsonclick ;
      private String edtavTfsistemaversao_id_sel_Internalname ;
      private String edtavTfsistemaversao_id_sel_Jsonclick ;
      private String edtavTfsistemaversao_descricao_Internalname ;
      private String edtavTfsistemaversao_descricao_sel_Internalname ;
      private String edtavTfsistemaversao_evolucao_Internalname ;
      private String edtavTfsistemaversao_evolucao_Jsonclick ;
      private String edtavTfsistemaversao_evolucao_to_Internalname ;
      private String edtavTfsistemaversao_evolucao_to_Jsonclick ;
      private String edtavTfsistemaversao_melhoria_Internalname ;
      private String edtavTfsistemaversao_melhoria_Jsonclick ;
      private String edtavTfsistemaversao_melhoria_to_Internalname ;
      private String edtavTfsistemaversao_melhoria_to_Jsonclick ;
      private String edtavTfsistemaversao_correcao_Internalname ;
      private String edtavTfsistemaversao_correcao_Jsonclick ;
      private String edtavTfsistemaversao_correcao_to_Internalname ;
      private String edtavTfsistemaversao_correcao_to_Jsonclick ;
      private String edtavTfsistemaversao_data_Internalname ;
      private String edtavTfsistemaversao_data_Jsonclick ;
      private String edtavTfsistemaversao_data_to_Internalname ;
      private String edtavTfsistemaversao_data_to_Jsonclick ;
      private String divDdo_sistemaversao_dataauxdates_Internalname ;
      private String edtavDdo_sistemaversao_dataauxdate_Internalname ;
      private String edtavDdo_sistemaversao_dataauxdate_Jsonclick ;
      private String edtavDdo_sistemaversao_dataauxdateto_Internalname ;
      private String edtavDdo_sistemaversao_dataauxdateto_Jsonclick ;
      private String edtavDdo_sistemaversao_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_sistemaversao_idtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_sistemaversao_descricaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_sistemaversao_evolucaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_sistemaversao_melhoriatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_sistemaversao_correcaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_sistemaversao_datatitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtSistemaVersao_Codigo_Internalname ;
      private String A1860SistemaVersao_Id ;
      private String edtSistemaVersao_Id_Internalname ;
      private String edtSistemaVersao_Descricao_Internalname ;
      private String edtSistemaVersao_Evolucao_Internalname ;
      private String edtSistemaVersao_Melhoria_Internalname ;
      private String edtSistemaVersao_Correcao_Internalname ;
      private String edtSistemaVersao_Data_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV76WWSistemaVersaoDS_3_Sistemaversao_id1 ;
      private String lV80WWSistemaVersaoDS_7_Sistemaversao_id2 ;
      private String lV84WWSistemaVersaoDS_11_Sistemaversao_id3 ;
      private String lV87WWSistemaVersaoDS_14_Tfsistemaversao_id ;
      private String AV76WWSistemaVersaoDS_3_Sistemaversao_id1 ;
      private String AV80WWSistemaVersaoDS_7_Sistemaversao_id2 ;
      private String AV84WWSistemaVersaoDS_11_Sistemaversao_id3 ;
      private String AV88WWSistemaVersaoDS_15_Tfsistemaversao_id_sel ;
      private String AV87WWSistemaVersaoDS_14_Tfsistemaversao_id ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavSistemaversao_id1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavSistemaversao_id2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavSistemaversao_id3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_sistemaversao_codigo_Internalname ;
      private String Ddo_sistemaversao_id_Internalname ;
      private String Ddo_sistemaversao_descricao_Internalname ;
      private String Ddo_sistemaversao_evolucao_Internalname ;
      private String Ddo_sistemaversao_melhoria_Internalname ;
      private String Ddo_sistemaversao_correcao_Internalname ;
      private String Ddo_sistemaversao_data_Internalname ;
      private String Ddo_managefilters_Internalname ;
      private String edtSistemaVersao_Codigo_Title ;
      private String edtSistemaVersao_Id_Title ;
      private String edtSistemaVersao_Descricao_Title ;
      private String edtSistemaVersao_Evolucao_Title ;
      private String edtSistemaVersao_Melhoria_Title ;
      private String edtSistemaVersao_Correcao_Title ;
      private String edtSistemaVersao_Data_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtSistemaVersao_Id_Link ;
      private String GXt_char2 ;
      private String imgInsert_Link ;
      private String imgInsert_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavSistemaversao_id3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavSistemaversao_id2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavSistemaversao_id1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String lblSistemaversaotitle_Internalname ;
      private String lblSistemaversaotitle_Jsonclick ;
      private String imgInsert_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String sGXsfl_88_fel_idx="0001" ;
      private String ROClassString ;
      private String edtSistemaVersao_Codigo_Jsonclick ;
      private String edtSistemaVersao_Id_Jsonclick ;
      private String edtSistemaVersao_Descricao_Jsonclick ;
      private String edtSistemaVersao_Evolucao_Jsonclick ;
      private String edtSistemaVersao_Melhoria_Jsonclick ;
      private String edtSistemaVersao_Correcao_Jsonclick ;
      private String edtSistemaVersao_Data_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV62TFSistemaVersao_Data ;
      private DateTime AV63TFSistemaVersao_Data_To ;
      private DateTime A1865SistemaVersao_Data ;
      private DateTime AV97WWSistemaVersaoDS_24_Tfsistemaversao_data ;
      private DateTime AV98WWSistemaVersaoDS_25_Tfsistemaversao_data_to ;
      private DateTime AV64DDO_SistemaVersao_DataAuxDate ;
      private DateTime AV65DDO_SistemaVersao_DataAuxDateTo ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_sistemaversao_codigo_Includesortasc ;
      private bool Ddo_sistemaversao_codigo_Includesortdsc ;
      private bool Ddo_sistemaversao_codigo_Includefilter ;
      private bool Ddo_sistemaversao_codigo_Filterisrange ;
      private bool Ddo_sistemaversao_codigo_Includedatalist ;
      private bool Ddo_sistemaversao_id_Includesortasc ;
      private bool Ddo_sistemaversao_id_Includesortdsc ;
      private bool Ddo_sistemaversao_id_Includefilter ;
      private bool Ddo_sistemaversao_id_Filterisrange ;
      private bool Ddo_sistemaversao_id_Includedatalist ;
      private bool Ddo_sistemaversao_descricao_Includesortasc ;
      private bool Ddo_sistemaversao_descricao_Includesortdsc ;
      private bool Ddo_sistemaversao_descricao_Includefilter ;
      private bool Ddo_sistemaversao_descricao_Filterisrange ;
      private bool Ddo_sistemaversao_descricao_Includedatalist ;
      private bool Ddo_sistemaversao_evolucao_Includesortasc ;
      private bool Ddo_sistemaversao_evolucao_Includesortdsc ;
      private bool Ddo_sistemaversao_evolucao_Includefilter ;
      private bool Ddo_sistemaversao_evolucao_Filterisrange ;
      private bool Ddo_sistemaversao_evolucao_Includedatalist ;
      private bool Ddo_sistemaversao_melhoria_Includesortasc ;
      private bool Ddo_sistemaversao_melhoria_Includesortdsc ;
      private bool Ddo_sistemaversao_melhoria_Includefilter ;
      private bool Ddo_sistemaversao_melhoria_Filterisrange ;
      private bool Ddo_sistemaversao_melhoria_Includedatalist ;
      private bool Ddo_sistemaversao_correcao_Includesortasc ;
      private bool Ddo_sistemaversao_correcao_Includesortdsc ;
      private bool Ddo_sistemaversao_correcao_Includefilter ;
      private bool Ddo_sistemaversao_correcao_Filterisrange ;
      private bool Ddo_sistemaversao_correcao_Includedatalist ;
      private bool Ddo_sistemaversao_data_Includesortasc ;
      private bool Ddo_sistemaversao_data_Includesortdsc ;
      private bool Ddo_sistemaversao_data_Includefilter ;
      private bool Ddo_sistemaversao_data_Filterisrange ;
      private bool Ddo_sistemaversao_data_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1862SistemaVersao_Evolucao ;
      private bool n1863SistemaVersao_Melhoria ;
      private bool n1864SistemaVersao_Correcao ;
      private bool AV77WWSistemaVersaoDS_4_Dynamicfiltersenabled2 ;
      private bool AV81WWSistemaVersaoDS_8_Dynamicfiltersenabled3 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Update_IsBlob ;
      private bool AV29Delete_IsBlob ;
      private String A1861SistemaVersao_Descricao ;
      private String AV32ManageFiltersXml ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV46TFSistemaVersao_Descricao ;
      private String AV47TFSistemaVersao_Descricao_Sel ;
      private String AV40ddo_SistemaVersao_CodigoTitleControlIdToReplace ;
      private String AV44ddo_SistemaVersao_IdTitleControlIdToReplace ;
      private String AV48ddo_SistemaVersao_DescricaoTitleControlIdToReplace ;
      private String AV52ddo_SistemaVersao_EvolucaoTitleControlIdToReplace ;
      private String AV56ddo_SistemaVersao_MelhoriaTitleControlIdToReplace ;
      private String AV60ddo_SistemaVersao_CorrecaoTitleControlIdToReplace ;
      private String AV66ddo_SistemaVersao_DataTitleControlIdToReplace ;
      private String AV99Update_GXI ;
      private String AV100Delete_GXI ;
      private String lV89WWSistemaVersaoDS_16_Tfsistemaversao_descricao ;
      private String AV74WWSistemaVersaoDS_1_Dynamicfiltersselector1 ;
      private String AV78WWSistemaVersaoDS_5_Dynamicfiltersselector2 ;
      private String AV82WWSistemaVersaoDS_9_Dynamicfiltersselector3 ;
      private String AV90WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel ;
      private String AV89WWSistemaVersaoDS_16_Tfsistemaversao_descricao ;
      private String AV28Update ;
      private String AV29Delete ;
      private String imgInsert_Bitmap ;
      private IGxSession AV30Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private DateTime[] H00OE2_A1865SistemaVersao_Data ;
      private int[] H00OE2_A1864SistemaVersao_Correcao ;
      private bool[] H00OE2_n1864SistemaVersao_Correcao ;
      private short[] H00OE2_A1863SistemaVersao_Melhoria ;
      private bool[] H00OE2_n1863SistemaVersao_Melhoria ;
      private short[] H00OE2_A1862SistemaVersao_Evolucao ;
      private bool[] H00OE2_n1862SistemaVersao_Evolucao ;
      private String[] H00OE2_A1861SistemaVersao_Descricao ;
      private String[] H00OE2_A1860SistemaVersao_Id ;
      private int[] H00OE2_A1859SistemaVersao_Codigo ;
      private long[] H00OE3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtGridStateCollection_Item ))]
      private IGxCollection AV33ManageFiltersItems ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV35ManageFiltersData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV37SistemaVersao_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV41SistemaVersao_IdTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV45SistemaVersao_DescricaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV49SistemaVersao_EvolucaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV53SistemaVersao_MelhoriaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV57SistemaVersao_CorrecaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV61SistemaVersao_DataTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtGridStateCollection_Item AV34ManageFiltersItem ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item AV36ManageFiltersDataItem ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV67DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwsistemaversao__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00OE2( IGxContext context ,
                                             String AV74WWSistemaVersaoDS_1_Dynamicfiltersselector1 ,
                                             short AV75WWSistemaVersaoDS_2_Dynamicfiltersoperator1 ,
                                             String AV76WWSistemaVersaoDS_3_Sistemaversao_id1 ,
                                             bool AV77WWSistemaVersaoDS_4_Dynamicfiltersenabled2 ,
                                             String AV78WWSistemaVersaoDS_5_Dynamicfiltersselector2 ,
                                             short AV79WWSistemaVersaoDS_6_Dynamicfiltersoperator2 ,
                                             String AV80WWSistemaVersaoDS_7_Sistemaversao_id2 ,
                                             bool AV81WWSistemaVersaoDS_8_Dynamicfiltersenabled3 ,
                                             String AV82WWSistemaVersaoDS_9_Dynamicfiltersselector3 ,
                                             short AV83WWSistemaVersaoDS_10_Dynamicfiltersoperator3 ,
                                             String AV84WWSistemaVersaoDS_11_Sistemaversao_id3 ,
                                             int AV85WWSistemaVersaoDS_12_Tfsistemaversao_codigo ,
                                             int AV86WWSistemaVersaoDS_13_Tfsistemaversao_codigo_to ,
                                             String AV88WWSistemaVersaoDS_15_Tfsistemaversao_id_sel ,
                                             String AV87WWSistemaVersaoDS_14_Tfsistemaversao_id ,
                                             String AV90WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel ,
                                             String AV89WWSistemaVersaoDS_16_Tfsistemaversao_descricao ,
                                             short AV91WWSistemaVersaoDS_18_Tfsistemaversao_evolucao ,
                                             short AV92WWSistemaVersaoDS_19_Tfsistemaversao_evolucao_to ,
                                             short AV93WWSistemaVersaoDS_20_Tfsistemaversao_melhoria ,
                                             short AV94WWSistemaVersaoDS_21_Tfsistemaversao_melhoria_to ,
                                             int AV95WWSistemaVersaoDS_22_Tfsistemaversao_correcao ,
                                             int AV96WWSistemaVersaoDS_23_Tfsistemaversao_correcao_to ,
                                             DateTime AV97WWSistemaVersaoDS_24_Tfsistemaversao_data ,
                                             DateTime AV98WWSistemaVersaoDS_25_Tfsistemaversao_data_to ,
                                             String A1860SistemaVersao_Id ,
                                             int A1859SistemaVersao_Codigo ,
                                             String A1861SistemaVersao_Descricao ,
                                             short A1862SistemaVersao_Evolucao ,
                                             short A1863SistemaVersao_Melhoria ,
                                             int A1864SistemaVersao_Correcao ,
                                             DateTime A1865SistemaVersao_Data ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [25] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [SistemaVersao_Data], [SistemaVersao_Correcao], [SistemaVersao_Melhoria], [SistemaVersao_Evolucao], [SistemaVersao_Descricao], [SistemaVersao_Id], [SistemaVersao_Codigo]";
         sFromString = " FROM [SistemaVersao] WITH (NOLOCK)";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV74WWSistemaVersaoDS_1_Dynamicfiltersselector1, "SISTEMAVERSAO_ID") == 0 ) && ( AV75WWSistemaVersaoDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76WWSistemaVersaoDS_3_Sistemaversao_id1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Id] like @lV76WWSistemaVersaoDS_3_Sistemaversao_id1)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Id] like @lV76WWSistemaVersaoDS_3_Sistemaversao_id1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV74WWSistemaVersaoDS_1_Dynamicfiltersselector1, "SISTEMAVERSAO_ID") == 0 ) && ( AV75WWSistemaVersaoDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76WWSistemaVersaoDS_3_Sistemaversao_id1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Id] like '%' + @lV76WWSistemaVersaoDS_3_Sistemaversao_id1)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Id] like '%' + @lV76WWSistemaVersaoDS_3_Sistemaversao_id1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV77WWSistemaVersaoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV78WWSistemaVersaoDS_5_Dynamicfiltersselector2, "SISTEMAVERSAO_ID") == 0 ) && ( AV79WWSistemaVersaoDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWSistemaVersaoDS_7_Sistemaversao_id2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Id] like @lV80WWSistemaVersaoDS_7_Sistemaversao_id2)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Id] like @lV80WWSistemaVersaoDS_7_Sistemaversao_id2)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV77WWSistemaVersaoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV78WWSistemaVersaoDS_5_Dynamicfiltersselector2, "SISTEMAVERSAO_ID") == 0 ) && ( AV79WWSistemaVersaoDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWSistemaVersaoDS_7_Sistemaversao_id2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Id] like '%' + @lV80WWSistemaVersaoDS_7_Sistemaversao_id2)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Id] like '%' + @lV80WWSistemaVersaoDS_7_Sistemaversao_id2)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV81WWSistemaVersaoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWSistemaVersaoDS_9_Dynamicfiltersselector3, "SISTEMAVERSAO_ID") == 0 ) && ( AV83WWSistemaVersaoDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWSistemaVersaoDS_11_Sistemaversao_id3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Id] like @lV84WWSistemaVersaoDS_11_Sistemaversao_id3)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Id] like @lV84WWSistemaVersaoDS_11_Sistemaversao_id3)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV81WWSistemaVersaoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWSistemaVersaoDS_9_Dynamicfiltersselector3, "SISTEMAVERSAO_ID") == 0 ) && ( AV83WWSistemaVersaoDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWSistemaVersaoDS_11_Sistemaversao_id3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Id] like '%' + @lV84WWSistemaVersaoDS_11_Sistemaversao_id3)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Id] like '%' + @lV84WWSistemaVersaoDS_11_Sistemaversao_id3)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! (0==AV85WWSistemaVersaoDS_12_Tfsistemaversao_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Codigo] >= @AV85WWSistemaVersaoDS_12_Tfsistemaversao_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Codigo] >= @AV85WWSistemaVersaoDS_12_Tfsistemaversao_codigo)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! (0==AV86WWSistemaVersaoDS_13_Tfsistemaversao_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Codigo] <= @AV86WWSistemaVersaoDS_13_Tfsistemaversao_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Codigo] <= @AV86WWSistemaVersaoDS_13_Tfsistemaversao_codigo_to)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV88WWSistemaVersaoDS_15_Tfsistemaversao_id_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWSistemaVersaoDS_14_Tfsistemaversao_id)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Id] like @lV87WWSistemaVersaoDS_14_Tfsistemaversao_id)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Id] like @lV87WWSistemaVersaoDS_14_Tfsistemaversao_id)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWSistemaVersaoDS_15_Tfsistemaversao_id_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Id] = @AV88WWSistemaVersaoDS_15_Tfsistemaversao_id_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Id] = @AV88WWSistemaVersaoDS_15_Tfsistemaversao_id_sel)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV90WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWSistemaVersaoDS_16_Tfsistemaversao_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Descricao] like @lV89WWSistemaVersaoDS_16_Tfsistemaversao_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Descricao] like @lV89WWSistemaVersaoDS_16_Tfsistemaversao_descricao)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Descricao] = @AV90WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Descricao] = @AV90WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! (0==AV91WWSistemaVersaoDS_18_Tfsistemaversao_evolucao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Evolucao] >= @AV91WWSistemaVersaoDS_18_Tfsistemaversao_evolucao)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Evolucao] >= @AV91WWSistemaVersaoDS_18_Tfsistemaversao_evolucao)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ! (0==AV92WWSistemaVersaoDS_19_Tfsistemaversao_evolucao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Evolucao] <= @AV92WWSistemaVersaoDS_19_Tfsistemaversao_evolucao_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Evolucao] <= @AV92WWSistemaVersaoDS_19_Tfsistemaversao_evolucao_to)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ! (0==AV93WWSistemaVersaoDS_20_Tfsistemaversao_melhoria) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Melhoria] >= @AV93WWSistemaVersaoDS_20_Tfsistemaversao_melhoria)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Melhoria] >= @AV93WWSistemaVersaoDS_20_Tfsistemaversao_melhoria)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ! (0==AV94WWSistemaVersaoDS_21_Tfsistemaversao_melhoria_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Melhoria] <= @AV94WWSistemaVersaoDS_21_Tfsistemaversao_melhoria_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Melhoria] <= @AV94WWSistemaVersaoDS_21_Tfsistemaversao_melhoria_to)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( ! (0==AV95WWSistemaVersaoDS_22_Tfsistemaversao_correcao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Correcao] >= @AV95WWSistemaVersaoDS_22_Tfsistemaversao_correcao)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Correcao] >= @AV95WWSistemaVersaoDS_22_Tfsistemaversao_correcao)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( ! (0==AV96WWSistemaVersaoDS_23_Tfsistemaversao_correcao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Correcao] <= @AV96WWSistemaVersaoDS_23_Tfsistemaversao_correcao_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Correcao] <= @AV96WWSistemaVersaoDS_23_Tfsistemaversao_correcao_to)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( ! (DateTime.MinValue==AV97WWSistemaVersaoDS_24_Tfsistemaversao_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Data] >= @AV97WWSistemaVersaoDS_24_Tfsistemaversao_data)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Data] >= @AV97WWSistemaVersaoDS_24_Tfsistemaversao_data)";
            }
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( ! (DateTime.MinValue==AV98WWSistemaVersaoDS_25_Tfsistemaversao_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Data] <= @AV98WWSistemaVersaoDS_25_Tfsistemaversao_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Data] <= @AV98WWSistemaVersaoDS_25_Tfsistemaversao_data_to)";
            }
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [SistemaVersao_Id]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [SistemaVersao_Id] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [SistemaVersao_Codigo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [SistemaVersao_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [SistemaVersao_Descricao]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [SistemaVersao_Descricao] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [SistemaVersao_Evolucao]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [SistemaVersao_Evolucao] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [SistemaVersao_Melhoria]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [SistemaVersao_Melhoria] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [SistemaVersao_Correcao]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [SistemaVersao_Correcao] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [SistemaVersao_Data]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [SistemaVersao_Data] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [SistemaVersao_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_H00OE3( IGxContext context ,
                                             String AV74WWSistemaVersaoDS_1_Dynamicfiltersselector1 ,
                                             short AV75WWSistemaVersaoDS_2_Dynamicfiltersoperator1 ,
                                             String AV76WWSistemaVersaoDS_3_Sistemaversao_id1 ,
                                             bool AV77WWSistemaVersaoDS_4_Dynamicfiltersenabled2 ,
                                             String AV78WWSistemaVersaoDS_5_Dynamicfiltersselector2 ,
                                             short AV79WWSistemaVersaoDS_6_Dynamicfiltersoperator2 ,
                                             String AV80WWSistemaVersaoDS_7_Sistemaversao_id2 ,
                                             bool AV81WWSistemaVersaoDS_8_Dynamicfiltersenabled3 ,
                                             String AV82WWSistemaVersaoDS_9_Dynamicfiltersselector3 ,
                                             short AV83WWSistemaVersaoDS_10_Dynamicfiltersoperator3 ,
                                             String AV84WWSistemaVersaoDS_11_Sistemaversao_id3 ,
                                             int AV85WWSistemaVersaoDS_12_Tfsistemaversao_codigo ,
                                             int AV86WWSistemaVersaoDS_13_Tfsistemaversao_codigo_to ,
                                             String AV88WWSistemaVersaoDS_15_Tfsistemaversao_id_sel ,
                                             String AV87WWSistemaVersaoDS_14_Tfsistemaversao_id ,
                                             String AV90WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel ,
                                             String AV89WWSistemaVersaoDS_16_Tfsistemaversao_descricao ,
                                             short AV91WWSistemaVersaoDS_18_Tfsistemaversao_evolucao ,
                                             short AV92WWSistemaVersaoDS_19_Tfsistemaversao_evolucao_to ,
                                             short AV93WWSistemaVersaoDS_20_Tfsistemaversao_melhoria ,
                                             short AV94WWSistemaVersaoDS_21_Tfsistemaversao_melhoria_to ,
                                             int AV95WWSistemaVersaoDS_22_Tfsistemaversao_correcao ,
                                             int AV96WWSistemaVersaoDS_23_Tfsistemaversao_correcao_to ,
                                             DateTime AV97WWSistemaVersaoDS_24_Tfsistemaversao_data ,
                                             DateTime AV98WWSistemaVersaoDS_25_Tfsistemaversao_data_to ,
                                             String A1860SistemaVersao_Id ,
                                             int A1859SistemaVersao_Codigo ,
                                             String A1861SistemaVersao_Descricao ,
                                             short A1862SistemaVersao_Evolucao ,
                                             short A1863SistemaVersao_Melhoria ,
                                             int A1864SistemaVersao_Correcao ,
                                             DateTime A1865SistemaVersao_Data ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [20] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [SistemaVersao] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV74WWSistemaVersaoDS_1_Dynamicfiltersselector1, "SISTEMAVERSAO_ID") == 0 ) && ( AV75WWSistemaVersaoDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76WWSistemaVersaoDS_3_Sistemaversao_id1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Id] like @lV76WWSistemaVersaoDS_3_Sistemaversao_id1)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Id] like @lV76WWSistemaVersaoDS_3_Sistemaversao_id1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV74WWSistemaVersaoDS_1_Dynamicfiltersselector1, "SISTEMAVERSAO_ID") == 0 ) && ( AV75WWSistemaVersaoDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76WWSistemaVersaoDS_3_Sistemaversao_id1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Id] like '%' + @lV76WWSistemaVersaoDS_3_Sistemaversao_id1)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Id] like '%' + @lV76WWSistemaVersaoDS_3_Sistemaversao_id1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( AV77WWSistemaVersaoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV78WWSistemaVersaoDS_5_Dynamicfiltersselector2, "SISTEMAVERSAO_ID") == 0 ) && ( AV79WWSistemaVersaoDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWSistemaVersaoDS_7_Sistemaversao_id2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Id] like @lV80WWSistemaVersaoDS_7_Sistemaversao_id2)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Id] like @lV80WWSistemaVersaoDS_7_Sistemaversao_id2)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( AV77WWSistemaVersaoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV78WWSistemaVersaoDS_5_Dynamicfiltersselector2, "SISTEMAVERSAO_ID") == 0 ) && ( AV79WWSistemaVersaoDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWSistemaVersaoDS_7_Sistemaversao_id2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Id] like '%' + @lV80WWSistemaVersaoDS_7_Sistemaversao_id2)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Id] like '%' + @lV80WWSistemaVersaoDS_7_Sistemaversao_id2)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV81WWSistemaVersaoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWSistemaVersaoDS_9_Dynamicfiltersselector3, "SISTEMAVERSAO_ID") == 0 ) && ( AV83WWSistemaVersaoDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWSistemaVersaoDS_11_Sistemaversao_id3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Id] like @lV84WWSistemaVersaoDS_11_Sistemaversao_id3)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Id] like @lV84WWSistemaVersaoDS_11_Sistemaversao_id3)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV81WWSistemaVersaoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWSistemaVersaoDS_9_Dynamicfiltersselector3, "SISTEMAVERSAO_ID") == 0 ) && ( AV83WWSistemaVersaoDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWSistemaVersaoDS_11_Sistemaversao_id3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Id] like '%' + @lV84WWSistemaVersaoDS_11_Sistemaversao_id3)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Id] like '%' + @lV84WWSistemaVersaoDS_11_Sistemaversao_id3)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( ! (0==AV85WWSistemaVersaoDS_12_Tfsistemaversao_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Codigo] >= @AV85WWSistemaVersaoDS_12_Tfsistemaversao_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Codigo] >= @AV85WWSistemaVersaoDS_12_Tfsistemaversao_codigo)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( ! (0==AV86WWSistemaVersaoDS_13_Tfsistemaversao_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Codigo] <= @AV86WWSistemaVersaoDS_13_Tfsistemaversao_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Codigo] <= @AV86WWSistemaVersaoDS_13_Tfsistemaversao_codigo_to)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV88WWSistemaVersaoDS_15_Tfsistemaversao_id_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWSistemaVersaoDS_14_Tfsistemaversao_id)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Id] like @lV87WWSistemaVersaoDS_14_Tfsistemaversao_id)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Id] like @lV87WWSistemaVersaoDS_14_Tfsistemaversao_id)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWSistemaVersaoDS_15_Tfsistemaversao_id_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Id] = @AV88WWSistemaVersaoDS_15_Tfsistemaversao_id_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Id] = @AV88WWSistemaVersaoDS_15_Tfsistemaversao_id_sel)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV90WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWSistemaVersaoDS_16_Tfsistemaversao_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Descricao] like @lV89WWSistemaVersaoDS_16_Tfsistemaversao_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Descricao] like @lV89WWSistemaVersaoDS_16_Tfsistemaversao_descricao)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Descricao] = @AV90WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Descricao] = @AV90WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( ! (0==AV91WWSistemaVersaoDS_18_Tfsistemaversao_evolucao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Evolucao] >= @AV91WWSistemaVersaoDS_18_Tfsistemaversao_evolucao)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Evolucao] >= @AV91WWSistemaVersaoDS_18_Tfsistemaversao_evolucao)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( ! (0==AV92WWSistemaVersaoDS_19_Tfsistemaversao_evolucao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Evolucao] <= @AV92WWSistemaVersaoDS_19_Tfsistemaversao_evolucao_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Evolucao] <= @AV92WWSistemaVersaoDS_19_Tfsistemaversao_evolucao_to)";
            }
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( ! (0==AV93WWSistemaVersaoDS_20_Tfsistemaversao_melhoria) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Melhoria] >= @AV93WWSistemaVersaoDS_20_Tfsistemaversao_melhoria)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Melhoria] >= @AV93WWSistemaVersaoDS_20_Tfsistemaversao_melhoria)";
            }
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( ! (0==AV94WWSistemaVersaoDS_21_Tfsistemaversao_melhoria_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Melhoria] <= @AV94WWSistemaVersaoDS_21_Tfsistemaversao_melhoria_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Melhoria] <= @AV94WWSistemaVersaoDS_21_Tfsistemaversao_melhoria_to)";
            }
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( ! (0==AV95WWSistemaVersaoDS_22_Tfsistemaversao_correcao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Correcao] >= @AV95WWSistemaVersaoDS_22_Tfsistemaversao_correcao)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Correcao] >= @AV95WWSistemaVersaoDS_22_Tfsistemaversao_correcao)";
            }
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( ! (0==AV96WWSistemaVersaoDS_23_Tfsistemaversao_correcao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Correcao] <= @AV96WWSistemaVersaoDS_23_Tfsistemaversao_correcao_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Correcao] <= @AV96WWSistemaVersaoDS_23_Tfsistemaversao_correcao_to)";
            }
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( ! (DateTime.MinValue==AV97WWSistemaVersaoDS_24_Tfsistemaversao_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Data] >= @AV97WWSistemaVersaoDS_24_Tfsistemaversao_data)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Data] >= @AV97WWSistemaVersaoDS_24_Tfsistemaversao_data)";
            }
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( ! (DateTime.MinValue==AV98WWSistemaVersaoDS_25_Tfsistemaversao_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Data] <= @AV98WWSistemaVersaoDS_25_Tfsistemaversao_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Data] <= @AV98WWSistemaVersaoDS_25_Tfsistemaversao_data_to)";
            }
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00OE2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (short)dynConstraints[17] , (short)dynConstraints[18] , (short)dynConstraints[19] , (short)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (DateTime)dynConstraints[23] , (DateTime)dynConstraints[24] , (String)dynConstraints[25] , (int)dynConstraints[26] , (String)dynConstraints[27] , (short)dynConstraints[28] , (short)dynConstraints[29] , (int)dynConstraints[30] , (DateTime)dynConstraints[31] , (short)dynConstraints[32] , (bool)dynConstraints[33] );
               case 1 :
                     return conditional_H00OE3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (short)dynConstraints[17] , (short)dynConstraints[18] , (short)dynConstraints[19] , (short)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (DateTime)dynConstraints[23] , (DateTime)dynConstraints[24] , (String)dynConstraints[25] , (int)dynConstraints[26] , (String)dynConstraints[27] , (short)dynConstraints[28] , (short)dynConstraints[29] , (int)dynConstraints[30] , (DateTime)dynConstraints[31] , (short)dynConstraints[32] , (bool)dynConstraints[33] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00OE2 ;
          prmH00OE2 = new Object[] {
          new Object[] {"@lV76WWSistemaVersaoDS_3_Sistemaversao_id1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV76WWSistemaVersaoDS_3_Sistemaversao_id1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV80WWSistemaVersaoDS_7_Sistemaversao_id2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV80WWSistemaVersaoDS_7_Sistemaversao_id2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV84WWSistemaVersaoDS_11_Sistemaversao_id3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV84WWSistemaVersaoDS_11_Sistemaversao_id3",SqlDbType.Char,20,0} ,
          new Object[] {"@AV85WWSistemaVersaoDS_12_Tfsistemaversao_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV86WWSistemaVersaoDS_13_Tfsistemaversao_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV87WWSistemaVersaoDS_14_Tfsistemaversao_id",SqlDbType.Char,20,0} ,
          new Object[] {"@AV88WWSistemaVersaoDS_15_Tfsistemaversao_id_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV89WWSistemaVersaoDS_16_Tfsistemaversao_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV90WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV91WWSistemaVersaoDS_18_Tfsistemaversao_evolucao",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV92WWSistemaVersaoDS_19_Tfsistemaversao_evolucao_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV93WWSistemaVersaoDS_20_Tfsistemaversao_melhoria",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV94WWSistemaVersaoDS_21_Tfsistemaversao_melhoria_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV95WWSistemaVersaoDS_22_Tfsistemaversao_correcao",SqlDbType.Int,8,0} ,
          new Object[] {"@AV96WWSistemaVersaoDS_23_Tfsistemaversao_correcao_to",SqlDbType.Int,8,0} ,
          new Object[] {"@AV97WWSistemaVersaoDS_24_Tfsistemaversao_data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV98WWSistemaVersaoDS_25_Tfsistemaversao_data_to",SqlDbType.DateTime,8,5} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00OE3 ;
          prmH00OE3 = new Object[] {
          new Object[] {"@lV76WWSistemaVersaoDS_3_Sistemaversao_id1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV76WWSistemaVersaoDS_3_Sistemaversao_id1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV80WWSistemaVersaoDS_7_Sistemaversao_id2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV80WWSistemaVersaoDS_7_Sistemaversao_id2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV84WWSistemaVersaoDS_11_Sistemaversao_id3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV84WWSistemaVersaoDS_11_Sistemaversao_id3",SqlDbType.Char,20,0} ,
          new Object[] {"@AV85WWSistemaVersaoDS_12_Tfsistemaversao_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV86WWSistemaVersaoDS_13_Tfsistemaversao_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV87WWSistemaVersaoDS_14_Tfsistemaversao_id",SqlDbType.Char,20,0} ,
          new Object[] {"@AV88WWSistemaVersaoDS_15_Tfsistemaversao_id_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV89WWSistemaVersaoDS_16_Tfsistemaversao_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV90WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV91WWSistemaVersaoDS_18_Tfsistemaversao_evolucao",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV92WWSistemaVersaoDS_19_Tfsistemaversao_evolucao_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV93WWSistemaVersaoDS_20_Tfsistemaversao_melhoria",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV94WWSistemaVersaoDS_21_Tfsistemaversao_melhoria_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV95WWSistemaVersaoDS_22_Tfsistemaversao_correcao",SqlDbType.Int,8,0} ,
          new Object[] {"@AV96WWSistemaVersaoDS_23_Tfsistemaversao_correcao_to",SqlDbType.Int,8,0} ,
          new Object[] {"@AV97WWSistemaVersaoDS_24_Tfsistemaversao_data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV98WWSistemaVersaoDS_25_Tfsistemaversao_data_to",SqlDbType.DateTime,8,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00OE2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00OE2,11,0,true,false )
             ,new CursorDef("H00OE3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00OE3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDateTime(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((short[]) buf[5])[0] = rslt.getShort(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(5) ;
                ((String[]) buf[8])[0] = rslt.getString(6, 20) ;
                ((int[]) buf[9])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[37]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[38]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[39]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[40]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[42]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[43]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[44]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[48]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[32]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[33]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[34]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[35]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[39]);
                }
                return;
       }
    }

 }

}
