/*
               File: type_SdtSistemaDePara
        Description: De Para
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:7:39.60
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SistemaDePara" )]
   [XmlType(TypeName =  "SistemaDePara" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtSistemaDePara : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSistemaDePara( )
      {
         /* Constructor for serialization */
         gxTv_SdtSistemaDePara_Sistemadepara_origem = "";
         gxTv_SdtSistemaDePara_Sistemadepara_origemdsc = "";
         gxTv_SdtSistemaDePara_Mode = "";
         gxTv_SdtSistemaDePara_Sistemadepara_origem_Z = "";
      }

      public SdtSistemaDePara( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV127Sistema_Codigo ,
                        int AV1460SistemaDePara_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV127Sistema_Codigo,(int)AV1460SistemaDePara_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"Sistema_Codigo", typeof(int)}, new Object[]{"SistemaDePara_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "SistemaDePara");
         metadata.Set("BT", "SistemaDePara");
         metadata.Set("PK", "[ \"Sistema_Codigo\",\"SistemaDePara_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"Sistema_Codigo\" ],\"FKMap\":[  ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistemadepara_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistemadepara_origem_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistemadepara_origemid_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistemadepara_origemdsc_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSistemaDePara deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSistemaDePara)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSistemaDePara obj ;
         obj = this;
         obj.gxTpr_Sistema_codigo = deserialized.gxTpr_Sistema_codigo;
         obj.gxTpr_Sistemadepara_codigo = deserialized.gxTpr_Sistemadepara_codigo;
         obj.gxTpr_Sistemadepara_origem = deserialized.gxTpr_Sistemadepara_origem;
         obj.gxTpr_Sistemadepara_origemid = deserialized.gxTpr_Sistemadepara_origemid;
         obj.gxTpr_Sistemadepara_origemdsc = deserialized.gxTpr_Sistemadepara_origemdsc;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Sistema_codigo_Z = deserialized.gxTpr_Sistema_codigo_Z;
         obj.gxTpr_Sistemadepara_codigo_Z = deserialized.gxTpr_Sistemadepara_codigo_Z;
         obj.gxTpr_Sistemadepara_origem_Z = deserialized.gxTpr_Sistemadepara_origem_Z;
         obj.gxTpr_Sistemadepara_origemid_Z = deserialized.gxTpr_Sistemadepara_origemid_Z;
         obj.gxTpr_Sistemadepara_origemdsc_N = deserialized.gxTpr_Sistemadepara_origemdsc_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Codigo") )
               {
                  gxTv_SdtSistemaDePara_Sistema_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SistemaDePara_Codigo") )
               {
                  gxTv_SdtSistemaDePara_Sistemadepara_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SistemaDePara_Origem") )
               {
                  gxTv_SdtSistemaDePara_Sistemadepara_origem = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SistemaDePara_OrigemId") )
               {
                  gxTv_SdtSistemaDePara_Sistemadepara_origemid = (long)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SistemaDePara_OrigemDsc") )
               {
                  gxTv_SdtSistemaDePara_Sistemadepara_origemdsc = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtSistemaDePara_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtSistemaDePara_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Codigo_Z") )
               {
                  gxTv_SdtSistemaDePara_Sistema_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SistemaDePara_Codigo_Z") )
               {
                  gxTv_SdtSistemaDePara_Sistemadepara_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SistemaDePara_Origem_Z") )
               {
                  gxTv_SdtSistemaDePara_Sistemadepara_origem_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SistemaDePara_OrigemId_Z") )
               {
                  gxTv_SdtSistemaDePara_Sistemadepara_origemid_Z = (long)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SistemaDePara_OrigemDsc_N") )
               {
                  gxTv_SdtSistemaDePara_Sistemadepara_origemdsc_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SistemaDePara";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Sistema_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistemaDePara_Sistema_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("SistemaDePara_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistemaDePara_Sistemadepara_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("SistemaDePara_Origem", StringUtil.RTrim( gxTv_SdtSistemaDePara_Sistemadepara_origem));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("SistemaDePara_OrigemId", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistemaDePara_Sistemadepara_origemid), 18, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("SistemaDePara_OrigemDsc", StringUtil.RTrim( gxTv_SdtSistemaDePara_Sistemadepara_origemdsc));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtSistemaDePara_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistemaDePara_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistemaDePara_Sistema_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("SistemaDePara_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistemaDePara_Sistemadepara_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("SistemaDePara_Origem_Z", StringUtil.RTrim( gxTv_SdtSistemaDePara_Sistemadepara_origem_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("SistemaDePara_OrigemId_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistemaDePara_Sistemadepara_origemid_Z), 18, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("SistemaDePara_OrigemDsc_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistemaDePara_Sistemadepara_origemdsc_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Sistema_Codigo", gxTv_SdtSistemaDePara_Sistema_codigo, false);
         AddObjectProperty("SistemaDePara_Codigo", gxTv_SdtSistemaDePara_Sistemadepara_codigo, false);
         AddObjectProperty("SistemaDePara_Origem", gxTv_SdtSistemaDePara_Sistemadepara_origem, false);
         AddObjectProperty("SistemaDePara_OrigemId", StringUtil.LTrim( StringUtil.Str( (decimal)(gxTv_SdtSistemaDePara_Sistemadepara_origemid), 18, 0)), false);
         AddObjectProperty("SistemaDePara_OrigemDsc", gxTv_SdtSistemaDePara_Sistemadepara_origemdsc, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtSistemaDePara_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtSistemaDePara_Initialized, false);
            AddObjectProperty("Sistema_Codigo_Z", gxTv_SdtSistemaDePara_Sistema_codigo_Z, false);
            AddObjectProperty("SistemaDePara_Codigo_Z", gxTv_SdtSistemaDePara_Sistemadepara_codigo_Z, false);
            AddObjectProperty("SistemaDePara_Origem_Z", gxTv_SdtSistemaDePara_Sistemadepara_origem_Z, false);
            AddObjectProperty("SistemaDePara_OrigemId_Z", StringUtil.LTrim( StringUtil.Str( (decimal)(gxTv_SdtSistemaDePara_Sistemadepara_origemid_Z), 18, 0)), false);
            AddObjectProperty("SistemaDePara_OrigemDsc_N", gxTv_SdtSistemaDePara_Sistemadepara_origemdsc_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Sistema_Codigo" )]
      [  XmlElement( ElementName = "Sistema_Codigo"   )]
      public int gxTpr_Sistema_codigo
      {
         get {
            return gxTv_SdtSistemaDePara_Sistema_codigo ;
         }

         set {
            if ( gxTv_SdtSistemaDePara_Sistema_codigo != value )
            {
               gxTv_SdtSistemaDePara_Mode = "INS";
               this.gxTv_SdtSistemaDePara_Sistema_codigo_Z_SetNull( );
               this.gxTv_SdtSistemaDePara_Sistemadepara_codigo_Z_SetNull( );
               this.gxTv_SdtSistemaDePara_Sistemadepara_origem_Z_SetNull( );
               this.gxTv_SdtSistemaDePara_Sistemadepara_origemid_Z_SetNull( );
            }
            gxTv_SdtSistemaDePara_Sistema_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "SistemaDePara_Codigo" )]
      [  XmlElement( ElementName = "SistemaDePara_Codigo"   )]
      public int gxTpr_Sistemadepara_codigo
      {
         get {
            return gxTv_SdtSistemaDePara_Sistemadepara_codigo ;
         }

         set {
            if ( gxTv_SdtSistemaDePara_Sistemadepara_codigo != value )
            {
               gxTv_SdtSistemaDePara_Mode = "INS";
               this.gxTv_SdtSistemaDePara_Sistema_codigo_Z_SetNull( );
               this.gxTv_SdtSistemaDePara_Sistemadepara_codigo_Z_SetNull( );
               this.gxTv_SdtSistemaDePara_Sistemadepara_origem_Z_SetNull( );
               this.gxTv_SdtSistemaDePara_Sistemadepara_origemid_Z_SetNull( );
            }
            gxTv_SdtSistemaDePara_Sistemadepara_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "SistemaDePara_Origem" )]
      [  XmlElement( ElementName = "SistemaDePara_Origem"   )]
      public String gxTpr_Sistemadepara_origem
      {
         get {
            return gxTv_SdtSistemaDePara_Sistemadepara_origem ;
         }

         set {
            gxTv_SdtSistemaDePara_Sistemadepara_origem = (String)(value);
         }

      }

      [  SoapElement( ElementName = "SistemaDePara_OrigemId" )]
      [  XmlElement( ElementName = "SistemaDePara_OrigemId"   )]
      public long gxTpr_Sistemadepara_origemid
      {
         get {
            return gxTv_SdtSistemaDePara_Sistemadepara_origemid ;
         }

         set {
            gxTv_SdtSistemaDePara_Sistemadepara_origemid = (long)(value);
         }

      }

      [  SoapElement( ElementName = "SistemaDePara_OrigemDsc" )]
      [  XmlElement( ElementName = "SistemaDePara_OrigemDsc"   )]
      public String gxTpr_Sistemadepara_origemdsc
      {
         get {
            return gxTv_SdtSistemaDePara_Sistemadepara_origemdsc ;
         }

         set {
            gxTv_SdtSistemaDePara_Sistemadepara_origemdsc_N = 0;
            gxTv_SdtSistemaDePara_Sistemadepara_origemdsc = (String)(value);
         }

      }

      public void gxTv_SdtSistemaDePara_Sistemadepara_origemdsc_SetNull( )
      {
         gxTv_SdtSistemaDePara_Sistemadepara_origemdsc_N = 1;
         gxTv_SdtSistemaDePara_Sistemadepara_origemdsc = "";
         return  ;
      }

      public bool gxTv_SdtSistemaDePara_Sistemadepara_origemdsc_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtSistemaDePara_Mode ;
         }

         set {
            gxTv_SdtSistemaDePara_Mode = (String)(value);
         }

      }

      public void gxTv_SdtSistemaDePara_Mode_SetNull( )
      {
         gxTv_SdtSistemaDePara_Mode = "";
         return  ;
      }

      public bool gxTv_SdtSistemaDePara_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtSistemaDePara_Initialized ;
         }

         set {
            gxTv_SdtSistemaDePara_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtSistemaDePara_Initialized_SetNull( )
      {
         gxTv_SdtSistemaDePara_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtSistemaDePara_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_Codigo_Z" )]
      [  XmlElement( ElementName = "Sistema_Codigo_Z"   )]
      public int gxTpr_Sistema_codigo_Z
      {
         get {
            return gxTv_SdtSistemaDePara_Sistema_codigo_Z ;
         }

         set {
            gxTv_SdtSistemaDePara_Sistema_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtSistemaDePara_Sistema_codigo_Z_SetNull( )
      {
         gxTv_SdtSistemaDePara_Sistema_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSistemaDePara_Sistema_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "SistemaDePara_Codigo_Z" )]
      [  XmlElement( ElementName = "SistemaDePara_Codigo_Z"   )]
      public int gxTpr_Sistemadepara_codigo_Z
      {
         get {
            return gxTv_SdtSistemaDePara_Sistemadepara_codigo_Z ;
         }

         set {
            gxTv_SdtSistemaDePara_Sistemadepara_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtSistemaDePara_Sistemadepara_codigo_Z_SetNull( )
      {
         gxTv_SdtSistemaDePara_Sistemadepara_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSistemaDePara_Sistemadepara_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "SistemaDePara_Origem_Z" )]
      [  XmlElement( ElementName = "SistemaDePara_Origem_Z"   )]
      public String gxTpr_Sistemadepara_origem_Z
      {
         get {
            return gxTv_SdtSistemaDePara_Sistemadepara_origem_Z ;
         }

         set {
            gxTv_SdtSistemaDePara_Sistemadepara_origem_Z = (String)(value);
         }

      }

      public void gxTv_SdtSistemaDePara_Sistemadepara_origem_Z_SetNull( )
      {
         gxTv_SdtSistemaDePara_Sistemadepara_origem_Z = "";
         return  ;
      }

      public bool gxTv_SdtSistemaDePara_Sistemadepara_origem_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "SistemaDePara_OrigemId_Z" )]
      [  XmlElement( ElementName = "SistemaDePara_OrigemId_Z"   )]
      public long gxTpr_Sistemadepara_origemid_Z
      {
         get {
            return gxTv_SdtSistemaDePara_Sistemadepara_origemid_Z ;
         }

         set {
            gxTv_SdtSistemaDePara_Sistemadepara_origemid_Z = (long)(value);
         }

      }

      public void gxTv_SdtSistemaDePara_Sistemadepara_origemid_Z_SetNull( )
      {
         gxTv_SdtSistemaDePara_Sistemadepara_origemid_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSistemaDePara_Sistemadepara_origemid_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "SistemaDePara_OrigemDsc_N" )]
      [  XmlElement( ElementName = "SistemaDePara_OrigemDsc_N"   )]
      public short gxTpr_Sistemadepara_origemdsc_N
      {
         get {
            return gxTv_SdtSistemaDePara_Sistemadepara_origemdsc_N ;
         }

         set {
            gxTv_SdtSistemaDePara_Sistemadepara_origemdsc_N = (short)(value);
         }

      }

      public void gxTv_SdtSistemaDePara_Sistemadepara_origemdsc_N_SetNull( )
      {
         gxTv_SdtSistemaDePara_Sistemadepara_origemdsc_N = 0;
         return  ;
      }

      public bool gxTv_SdtSistemaDePara_Sistemadepara_origemdsc_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtSistemaDePara_Sistemadepara_origem = "";
         gxTv_SdtSistemaDePara_Sistemadepara_origemdsc = "";
         gxTv_SdtSistemaDePara_Mode = "";
         gxTv_SdtSistemaDePara_Sistemadepara_origem_Z = "";
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "sistemadepara", "GeneXus.Programs.sistemadepara_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtSistemaDePara_Initialized ;
      private short gxTv_SdtSistemaDePara_Sistemadepara_origemdsc_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtSistemaDePara_Sistema_codigo ;
      private int gxTv_SdtSistemaDePara_Sistemadepara_codigo ;
      private int gxTv_SdtSistemaDePara_Sistema_codigo_Z ;
      private int gxTv_SdtSistemaDePara_Sistemadepara_codigo_Z ;
      private long gxTv_SdtSistemaDePara_Sistemadepara_origemid ;
      private long gxTv_SdtSistemaDePara_Sistemadepara_origemid_Z ;
      private String gxTv_SdtSistemaDePara_Sistemadepara_origem ;
      private String gxTv_SdtSistemaDePara_Mode ;
      private String gxTv_SdtSistemaDePara_Sistemadepara_origem_Z ;
      private String sTagName ;
      private String gxTv_SdtSistemaDePara_Sistemadepara_origemdsc ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"SistemaDePara", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtSistemaDePara_RESTInterface : GxGenericCollectionItem<SdtSistemaDePara>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSistemaDePara_RESTInterface( ) : base()
      {
      }

      public SdtSistemaDePara_RESTInterface( SdtSistemaDePara psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Sistema_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Sistema_codigo
      {
         get {
            return sdt.gxTpr_Sistema_codigo ;
         }

         set {
            sdt.gxTpr_Sistema_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "SistemaDePara_Codigo" , Order = 1 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Sistemadepara_codigo
      {
         get {
            return sdt.gxTpr_Sistemadepara_codigo ;
         }

         set {
            sdt.gxTpr_Sistemadepara_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "SistemaDePara_Origem" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Sistemadepara_origem
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Sistemadepara_origem) ;
         }

         set {
            sdt.gxTpr_Sistemadepara_origem = (String)(value);
         }

      }

      [DataMember( Name = "SistemaDePara_OrigemId" , Order = 3 )]
      [GxSeudo()]
      public String gxTpr_Sistemadepara_origemid
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Sistemadepara_origemid), 18, 0)) ;
         }

         set {
            sdt.gxTpr_Sistemadepara_origemid = (long)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "SistemaDePara_OrigemDsc" , Order = 4 )]
      public String gxTpr_Sistemadepara_origemdsc
      {
         get {
            return sdt.gxTpr_Sistemadepara_origemdsc ;
         }

         set {
            sdt.gxTpr_Sistemadepara_origemdsc = (String)(value);
         }

      }

      public SdtSistemaDePara sdt
      {
         get {
            return (SdtSistemaDePara)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSistemaDePara() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 12 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
