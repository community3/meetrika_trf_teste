/*
               File: PRC_SSOrigem
        Description: SS Origem
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:13:53.19
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_ssorigem : GXProcedure
   {
      public prc_ssorigem( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_ssorigem( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_Codigo ,
                           out int aP1_Codigo )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV8Codigo = 0 ;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP1_Codigo=this.AV8Codigo;
      }

      public int executeUdp( ref int aP0_ContagemResultado_Codigo )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV8Codigo = 0 ;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP1_Codigo=this.AV8Codigo;
         return AV8Codigo ;
      }

      public void executeSubmit( ref int aP0_ContagemResultado_Codigo ,
                                 out int aP1_Codigo )
      {
         prc_ssorigem objprc_ssorigem;
         objprc_ssorigem = new prc_ssorigem();
         objprc_ssorigem.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_ssorigem.AV8Codigo = 0 ;
         objprc_ssorigem.context.SetSubmitInitialConfig(context);
         objprc_ssorigem.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_ssorigem);
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP1_Codigo=this.AV8Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_ssorigem)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00T82 */
         pr_default.execute(0, new Object[] {A456ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A602ContagemResultado_OSVinculada = P00T82_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P00T82_n602ContagemResultado_OSVinculada[0];
            A1765ContagemResultado_CodSrvSSVnc = P00T82_A1765ContagemResultado_CodSrvSSVnc[0];
            n1765ContagemResultado_CodSrvSSVnc = P00T82_n1765ContagemResultado_CodSrvSSVnc[0];
            A1765ContagemResultado_CodSrvSSVnc = P00T82_A1765ContagemResultado_CodSrvSSVnc[0];
            n1765ContagemResultado_CodSrvSSVnc = P00T82_n1765ContagemResultado_CodSrvSSVnc[0];
            if ( A602ContagemResultado_OSVinculada > 0 )
            {
               if ( A1765ContagemResultado_CodSrvSSVnc > 0 )
               {
                  AV8Codigo = A602ContagemResultado_OSVinculada;
               }
               else
               {
                  GXt_int1 = AV8Codigo;
                  new prc_ssorigem(context ).execute( ref  A602ContagemResultado_OSVinculada, out  GXt_int1) ;
                  AV8Codigo = GXt_int1;
               }
            }
            else
            {
               AV8Codigo = 0;
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00T82_A456ContagemResultado_Codigo = new int[1] ;
         P00T82_A602ContagemResultado_OSVinculada = new int[1] ;
         P00T82_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P00T82_A1765ContagemResultado_CodSrvSSVnc = new int[1] ;
         P00T82_n1765ContagemResultado_CodSrvSSVnc = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_ssorigem__default(),
            new Object[][] {
                new Object[] {
               P00T82_A456ContagemResultado_Codigo, P00T82_A602ContagemResultado_OSVinculada, P00T82_n602ContagemResultado_OSVinculada, P00T82_A1765ContagemResultado_CodSrvSSVnc, P00T82_n1765ContagemResultado_CodSrvSSVnc
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A456ContagemResultado_Codigo ;
      private int AV8Codigo ;
      private int A602ContagemResultado_OSVinculada ;
      private int A1765ContagemResultado_CodSrvSSVnc ;
      private int GXt_int1 ;
      private String scmdbuf ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n1765ContagemResultado_CodSrvSSVnc ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00T82_A456ContagemResultado_Codigo ;
      private int[] P00T82_A602ContagemResultado_OSVinculada ;
      private bool[] P00T82_n602ContagemResultado_OSVinculada ;
      private int[] P00T82_A1765ContagemResultado_CodSrvSSVnc ;
      private bool[] P00T82_n1765ContagemResultado_CodSrvSSVnc ;
      private int aP1_Codigo ;
   }

   public class prc_ssorigem__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00T82 ;
          prmP00T82 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00T82", "SELECT TOP 1 T1.[ContagemResultado_Codigo], T1.[ContagemResultado_OSVinculada] AS ContagemResultado_OSVinculada, T2.[ContagemResultado_ServicoSS] AS ContagemResultado_CodSrvSSVnc FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_OSVinculada]) WHERE T1.[ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00T82,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
