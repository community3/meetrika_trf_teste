/*
               File: GetPromptServicoGrupoFilterData
        Description: Get Prompt Servico Grupo Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:55:14.65
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptservicogrupofilterdata : GXProcedure
   {
      public getpromptservicogrupofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptservicogrupofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV15DDOName = aP0_DDOName;
         this.AV13SearchTxt = aP1_SearchTxt;
         this.AV14SearchTxtTo = aP2_SearchTxtTo;
         this.AV19OptionsJson = "" ;
         this.AV22OptionsDescJson = "" ;
         this.AV24OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV19OptionsJson;
         aP4_OptionsDescJson=this.AV22OptionsDescJson;
         aP5_OptionIndexesJson=this.AV24OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV15DDOName = aP0_DDOName;
         this.AV13SearchTxt = aP1_SearchTxt;
         this.AV14SearchTxtTo = aP2_SearchTxtTo;
         this.AV19OptionsJson = "" ;
         this.AV22OptionsDescJson = "" ;
         this.AV24OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV19OptionsJson;
         aP4_OptionsDescJson=this.AV22OptionsDescJson;
         aP5_OptionIndexesJson=this.AV24OptionIndexesJson;
         return AV24OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptservicogrupofilterdata objgetpromptservicogrupofilterdata;
         objgetpromptservicogrupofilterdata = new getpromptservicogrupofilterdata();
         objgetpromptservicogrupofilterdata.AV15DDOName = aP0_DDOName;
         objgetpromptservicogrupofilterdata.AV13SearchTxt = aP1_SearchTxt;
         objgetpromptservicogrupofilterdata.AV14SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptservicogrupofilterdata.AV19OptionsJson = "" ;
         objgetpromptservicogrupofilterdata.AV22OptionsDescJson = "" ;
         objgetpromptservicogrupofilterdata.AV24OptionIndexesJson = "" ;
         objgetpromptservicogrupofilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptservicogrupofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptservicogrupofilterdata);
         aP3_OptionsJson=this.AV19OptionsJson;
         aP4_OptionsDescJson=this.AV22OptionsDescJson;
         aP5_OptionIndexesJson=this.AV24OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptservicogrupofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV18Options = (IGxCollection)(new GxSimpleCollection());
         AV21OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV23OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV15DDOName), "DDO_SERVICOGRUPO_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADSERVICOGRUPO_DESCRICAOOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV19OptionsJson = AV18Options.ToJSonString(false);
         AV22OptionsDescJson = AV21OptionsDesc.ToJSonString(false);
         AV24OptionIndexesJson = AV23OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV26Session.Get("PromptServicoGrupoGridState"), "") == 0 )
         {
            AV28GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptServicoGrupoGridState"), "");
         }
         else
         {
            AV28GridState.FromXml(AV26Session.Get("PromptServicoGrupoGridState"), "");
         }
         AV45GXV1 = 1;
         while ( AV45GXV1 <= AV28GridState.gxTpr_Filtervalues.Count )
         {
            AV29GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV28GridState.gxTpr_Filtervalues.Item(AV45GXV1));
            if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "SERVICOGRUPO_ATIVO") == 0 )
            {
               AV31ServicoGrupo_Ativo = BooleanUtil.Val( AV29GridStateFilterValue.gxTpr_Value);
            }
            else if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "TFSERVICOGRUPO_DESCRICAO") == 0 )
            {
               AV10TFServicoGrupo_Descricao = AV29GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "TFSERVICOGRUPO_DESCRICAO_SEL") == 0 )
            {
               AV11TFServicoGrupo_Descricao_Sel = AV29GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "TFSERVICOGRUPO_ATIVO_SEL") == 0 )
            {
               AV12TFServicoGrupo_Ativo_Sel = (short)(NumberUtil.Val( AV29GridStateFilterValue.gxTpr_Value, "."));
            }
            AV45GXV1 = (int)(AV45GXV1+1);
         }
         if ( AV28GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV30GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV28GridState.gxTpr_Dynamicfilters.Item(1));
            AV32DynamicFiltersSelector1 = AV30GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "SERVICOGRUPO_DESCRICAO") == 0 )
            {
               AV33DynamicFiltersOperator1 = AV30GridStateDynamicFilter.gxTpr_Operator;
               AV34ServicoGrupo_Descricao1 = AV30GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV28GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV35DynamicFiltersEnabled2 = true;
               AV30GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV28GridState.gxTpr_Dynamicfilters.Item(2));
               AV36DynamicFiltersSelector2 = AV30GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV36DynamicFiltersSelector2, "SERVICOGRUPO_DESCRICAO") == 0 )
               {
                  AV37DynamicFiltersOperator2 = AV30GridStateDynamicFilter.gxTpr_Operator;
                  AV38ServicoGrupo_Descricao2 = AV30GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV28GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV39DynamicFiltersEnabled3 = true;
                  AV30GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV28GridState.gxTpr_Dynamicfilters.Item(3));
                  AV40DynamicFiltersSelector3 = AV30GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "SERVICOGRUPO_DESCRICAO") == 0 )
                  {
                     AV41DynamicFiltersOperator3 = AV30GridStateDynamicFilter.gxTpr_Operator;
                     AV42ServicoGrupo_Descricao3 = AV30GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADSERVICOGRUPO_DESCRICAOOPTIONS' Routine */
         AV10TFServicoGrupo_Descricao = AV13SearchTxt;
         AV11TFServicoGrupo_Descricao_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV32DynamicFiltersSelector1 ,
                                              AV33DynamicFiltersOperator1 ,
                                              AV34ServicoGrupo_Descricao1 ,
                                              AV35DynamicFiltersEnabled2 ,
                                              AV36DynamicFiltersSelector2 ,
                                              AV37DynamicFiltersOperator2 ,
                                              AV38ServicoGrupo_Descricao2 ,
                                              AV39DynamicFiltersEnabled3 ,
                                              AV40DynamicFiltersSelector3 ,
                                              AV41DynamicFiltersOperator3 ,
                                              AV42ServicoGrupo_Descricao3 ,
                                              AV11TFServicoGrupo_Descricao_Sel ,
                                              AV10TFServicoGrupo_Descricao ,
                                              AV12TFServicoGrupo_Ativo_Sel ,
                                              A158ServicoGrupo_Descricao ,
                                              A159ServicoGrupo_Ativo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV34ServicoGrupo_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV34ServicoGrupo_Descricao1), "%", "");
         lV34ServicoGrupo_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV34ServicoGrupo_Descricao1), "%", "");
         lV38ServicoGrupo_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV38ServicoGrupo_Descricao2), "%", "");
         lV38ServicoGrupo_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV38ServicoGrupo_Descricao2), "%", "");
         lV42ServicoGrupo_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV42ServicoGrupo_Descricao3), "%", "");
         lV42ServicoGrupo_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV42ServicoGrupo_Descricao3), "%", "");
         lV10TFServicoGrupo_Descricao = StringUtil.Concat( StringUtil.RTrim( AV10TFServicoGrupo_Descricao), "%", "");
         /* Using cursor P00UL2 */
         pr_default.execute(0, new Object[] {lV34ServicoGrupo_Descricao1, lV34ServicoGrupo_Descricao1, lV38ServicoGrupo_Descricao2, lV38ServicoGrupo_Descricao2, lV42ServicoGrupo_Descricao3, lV42ServicoGrupo_Descricao3, lV10TFServicoGrupo_Descricao, AV11TFServicoGrupo_Descricao_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKUL2 = false;
            A159ServicoGrupo_Ativo = P00UL2_A159ServicoGrupo_Ativo[0];
            A158ServicoGrupo_Descricao = P00UL2_A158ServicoGrupo_Descricao[0];
            A157ServicoGrupo_Codigo = P00UL2_A157ServicoGrupo_Codigo[0];
            AV25count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00UL2_A158ServicoGrupo_Descricao[0], A158ServicoGrupo_Descricao) == 0 ) )
            {
               BRKUL2 = false;
               A157ServicoGrupo_Codigo = P00UL2_A157ServicoGrupo_Codigo[0];
               AV25count = (long)(AV25count+1);
               BRKUL2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A158ServicoGrupo_Descricao)) )
            {
               AV17Option = A158ServicoGrupo_Descricao;
               AV18Options.Add(AV17Option, 0);
               AV23OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV25count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV18Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUL2 )
            {
               BRKUL2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV18Options = new GxSimpleCollection();
         AV21OptionsDesc = new GxSimpleCollection();
         AV23OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV26Session = context.GetSession();
         AV28GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV29GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV31ServicoGrupo_Ativo = true;
         AV10TFServicoGrupo_Descricao = "";
         AV11TFServicoGrupo_Descricao_Sel = "";
         AV30GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV32DynamicFiltersSelector1 = "";
         AV34ServicoGrupo_Descricao1 = "";
         AV36DynamicFiltersSelector2 = "";
         AV38ServicoGrupo_Descricao2 = "";
         AV40DynamicFiltersSelector3 = "";
         AV42ServicoGrupo_Descricao3 = "";
         scmdbuf = "";
         lV34ServicoGrupo_Descricao1 = "";
         lV38ServicoGrupo_Descricao2 = "";
         lV42ServicoGrupo_Descricao3 = "";
         lV10TFServicoGrupo_Descricao = "";
         A158ServicoGrupo_Descricao = "";
         P00UL2_A159ServicoGrupo_Ativo = new bool[] {false} ;
         P00UL2_A158ServicoGrupo_Descricao = new String[] {""} ;
         P00UL2_A157ServicoGrupo_Codigo = new int[1] ;
         AV17Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptservicogrupofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00UL2_A159ServicoGrupo_Ativo, P00UL2_A158ServicoGrupo_Descricao, P00UL2_A157ServicoGrupo_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV12TFServicoGrupo_Ativo_Sel ;
      private short AV33DynamicFiltersOperator1 ;
      private short AV37DynamicFiltersOperator2 ;
      private short AV41DynamicFiltersOperator3 ;
      private int AV45GXV1 ;
      private int A157ServicoGrupo_Codigo ;
      private long AV25count ;
      private String scmdbuf ;
      private bool returnInSub ;
      private bool AV31ServicoGrupo_Ativo ;
      private bool AV35DynamicFiltersEnabled2 ;
      private bool AV39DynamicFiltersEnabled3 ;
      private bool A159ServicoGrupo_Ativo ;
      private bool BRKUL2 ;
      private String AV24OptionIndexesJson ;
      private String AV19OptionsJson ;
      private String AV22OptionsDescJson ;
      private String AV15DDOName ;
      private String AV13SearchTxt ;
      private String AV14SearchTxtTo ;
      private String AV10TFServicoGrupo_Descricao ;
      private String AV11TFServicoGrupo_Descricao_Sel ;
      private String AV32DynamicFiltersSelector1 ;
      private String AV34ServicoGrupo_Descricao1 ;
      private String AV36DynamicFiltersSelector2 ;
      private String AV38ServicoGrupo_Descricao2 ;
      private String AV40DynamicFiltersSelector3 ;
      private String AV42ServicoGrupo_Descricao3 ;
      private String lV34ServicoGrupo_Descricao1 ;
      private String lV38ServicoGrupo_Descricao2 ;
      private String lV42ServicoGrupo_Descricao3 ;
      private String lV10TFServicoGrupo_Descricao ;
      private String A158ServicoGrupo_Descricao ;
      private String AV17Option ;
      private IGxSession AV26Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private bool[] P00UL2_A159ServicoGrupo_Ativo ;
      private String[] P00UL2_A158ServicoGrupo_Descricao ;
      private int[] P00UL2_A157ServicoGrupo_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV18Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV28GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV29GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV30GridStateDynamicFilter ;
   }

   public class getpromptservicogrupofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00UL2( IGxContext context ,
                                             String AV32DynamicFiltersSelector1 ,
                                             short AV33DynamicFiltersOperator1 ,
                                             String AV34ServicoGrupo_Descricao1 ,
                                             bool AV35DynamicFiltersEnabled2 ,
                                             String AV36DynamicFiltersSelector2 ,
                                             short AV37DynamicFiltersOperator2 ,
                                             String AV38ServicoGrupo_Descricao2 ,
                                             bool AV39DynamicFiltersEnabled3 ,
                                             String AV40DynamicFiltersSelector3 ,
                                             short AV41DynamicFiltersOperator3 ,
                                             String AV42ServicoGrupo_Descricao3 ,
                                             String AV11TFServicoGrupo_Descricao_Sel ,
                                             String AV10TFServicoGrupo_Descricao ,
                                             short AV12TFServicoGrupo_Ativo_Sel ,
                                             String A158ServicoGrupo_Descricao ,
                                             bool A159ServicoGrupo_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [8] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [ServicoGrupo_Ativo], [ServicoGrupo_Descricao], [ServicoGrupo_Codigo] FROM [ServicoGrupo] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([ServicoGrupo_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "SERVICOGRUPO_DESCRICAO") == 0 ) && ( AV33DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34ServicoGrupo_Descricao1)) ) )
         {
            sWhereString = sWhereString + " and ([ServicoGrupo_Descricao] like '%' + @lV34ServicoGrupo_Descricao1 + '%')";
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "SERVICOGRUPO_DESCRICAO") == 0 ) && ( AV33DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34ServicoGrupo_Descricao1)) ) )
         {
            sWhereString = sWhereString + " and ([ServicoGrupo_Descricao] like '%' + @lV34ServicoGrupo_Descricao1 + '%')";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV35DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV36DynamicFiltersSelector2, "SERVICOGRUPO_DESCRICAO") == 0 ) && ( AV37DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38ServicoGrupo_Descricao2)) ) )
         {
            sWhereString = sWhereString + " and ([ServicoGrupo_Descricao] like '%' + @lV38ServicoGrupo_Descricao2 + '%')";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV35DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV36DynamicFiltersSelector2, "SERVICOGRUPO_DESCRICAO") == 0 ) && ( AV37DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38ServicoGrupo_Descricao2)) ) )
         {
            sWhereString = sWhereString + " and ([ServicoGrupo_Descricao] like '%' + @lV38ServicoGrupo_Descricao2 + '%')";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV39DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "SERVICOGRUPO_DESCRICAO") == 0 ) && ( AV41DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42ServicoGrupo_Descricao3)) ) )
         {
            sWhereString = sWhereString + " and ([ServicoGrupo_Descricao] like '%' + @lV42ServicoGrupo_Descricao3 + '%')";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV39DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "SERVICOGRUPO_DESCRICAO") == 0 ) && ( AV41DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42ServicoGrupo_Descricao3)) ) )
         {
            sWhereString = sWhereString + " and ([ServicoGrupo_Descricao] like '%' + @lV42ServicoGrupo_Descricao3 + '%')";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFServicoGrupo_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFServicoGrupo_Descricao)) ) )
         {
            sWhereString = sWhereString + " and ([ServicoGrupo_Descricao] like @lV10TFServicoGrupo_Descricao)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFServicoGrupo_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and ([ServicoGrupo_Descricao] = @AV11TFServicoGrupo_Descricao_Sel)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV12TFServicoGrupo_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and ([ServicoGrupo_Ativo] = 1)";
         }
         if ( AV12TFServicoGrupo_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and ([ServicoGrupo_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [ServicoGrupo_Descricao]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00UL2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (bool)dynConstraints[15] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00UL2 ;
          prmP00UL2 = new Object[] {
          new Object[] {"@lV34ServicoGrupo_Descricao1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV34ServicoGrupo_Descricao1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV38ServicoGrupo_Descricao2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV38ServicoGrupo_Descricao2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV42ServicoGrupo_Descricao3",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV42ServicoGrupo_Descricao3",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV10TFServicoGrupo_Descricao",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV11TFServicoGrupo_Descricao_Sel",SqlDbType.VarChar,100,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00UL2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UL2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptservicogrupofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptservicogrupofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptservicogrupofilterdata") )
          {
             return  ;
          }
          getpromptservicogrupofilterdata worker = new getpromptservicogrupofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
