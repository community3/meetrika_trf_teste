/*
               File: type_SdtSDT_RedmineUsers
        Description: SDT_RedmineUsers
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:30:7.2
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "users" )]
   [XmlType(TypeName =  "users" , Namespace = "" )]
   [System.Xml.Serialization.XmlInclude( typeof( SdtSDT_RedmineUsers_user ))]
   [Serializable]
   public class SdtSDT_RedmineUsers : GxUserType
   {
      public SdtSDT_RedmineUsers( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_RedmineUsers_Type = "";
      }

      public SdtSDT_RedmineUsers( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_RedmineUsers deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType());
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_RedmineUsers)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_RedmineUsers obj ;
         obj = this;
         obj.gxTpr_Total_count = deserialized.gxTpr_Total_count;
         obj.gxTpr_Offset = deserialized.gxTpr_Offset;
         obj.gxTpr_Limit = deserialized.gxTpr_Limit;
         obj.gxTpr_Type = deserialized.gxTpr_Type;
         obj.gxTpr_Users = deserialized.gxTpr_Users;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         if ( oReader.ExistsAttribute("total_count") == 1 )
         {
            gxTv_SdtSDT_RedmineUsers_Total_count = (short)(NumberUtil.Val( oReader.GetAttributeByName("total_count"), "."));
            if ( GXSoapError > 0 )
            {
               readOk = 1;
            }
         }
         if ( oReader.ExistsAttribute("offset") == 1 )
         {
            gxTv_SdtSDT_RedmineUsers_Offset = (short)(NumberUtil.Val( oReader.GetAttributeByName("offset"), "."));
            if ( GXSoapError > 0 )
            {
               readOk = 1;
            }
         }
         if ( oReader.ExistsAttribute("limit") == 1 )
         {
            gxTv_SdtSDT_RedmineUsers_Limit = (short)(NumberUtil.Val( oReader.GetAttributeByName("limit"), "."));
            if ( GXSoapError > 0 )
            {
               readOk = 1;
            }
         }
         if ( oReader.ExistsAttribute("type") == 1 )
         {
            gxTv_SdtSDT_RedmineUsers_Type = oReader.GetAttributeByName("type");
            if ( GXSoapError > 0 )
            {
               readOk = 1;
            }
         }
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            if ( gxTv_SdtSDT_RedmineUsers_Users != null )
            {
               gxTv_SdtSDT_RedmineUsers_Users.ClearCollection();
            }
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp(oReader.LocalName, "user") == 0 )
               {
                  if ( gxTv_SdtSDT_RedmineUsers_Users == null )
                  {
                     gxTv_SdtSDT_RedmineUsers_Users = new GxObjectCollection( context, "SDT_RedmineUsers.user", "", "SdtSDT_RedmineUsers_user", "GeneXus.Programs");
                  }
                  GXSoapError = gxTv_SdtSDT_RedmineUsers_Users.readxmlcollection(oReader, "", "user");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "users";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteAttribute("total_count", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_RedmineUsers_Total_count), 2, 0)));
         oWriter.WriteAttribute("offset", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_RedmineUsers_Offset), 2, 0)));
         oWriter.WriteAttribute("limit", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_RedmineUsers_Limit), 2, 0)));
         oWriter.WriteAttribute("type", StringUtil.RTrim( gxTv_SdtSDT_RedmineUsers_Type));
         if ( gxTv_SdtSDT_RedmineUsers_Users != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "";
            }
            else
            {
               sNameSpace1 = "";
            }
            gxTv_SdtSDT_RedmineUsers_Users.writexmlcollection(oWriter, "", sNameSpace1, "user", sNameSpace1);
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("total_count", gxTv_SdtSDT_RedmineUsers_Total_count, false);
         AddObjectProperty("offset", gxTv_SdtSDT_RedmineUsers_Offset, false);
         AddObjectProperty("limit", gxTv_SdtSDT_RedmineUsers_Limit, false);
         AddObjectProperty("type", gxTv_SdtSDT_RedmineUsers_Type, false);
         if ( gxTv_SdtSDT_RedmineUsers_Users != null )
         {
            AddObjectProperty("users", gxTv_SdtSDT_RedmineUsers_Users, false);
         }
         return  ;
      }

      [SoapAttribute( AttributeName = "total_count" )]
      [XmlAttribute( AttributeName = "total_count" )]
      public short gxTpr_Total_count
      {
         get {
            return gxTv_SdtSDT_RedmineUsers_Total_count ;
         }

         set {
            gxTv_SdtSDT_RedmineUsers_Total_count = (short)(value);
         }

      }

      [SoapAttribute( AttributeName = "offset" )]
      [XmlAttribute( AttributeName = "offset" )]
      public short gxTpr_Offset
      {
         get {
            return gxTv_SdtSDT_RedmineUsers_Offset ;
         }

         set {
            gxTv_SdtSDT_RedmineUsers_Offset = (short)(value);
         }

      }

      [SoapAttribute( AttributeName = "limit" )]
      [XmlAttribute( AttributeName = "limit" )]
      public short gxTpr_Limit
      {
         get {
            return gxTv_SdtSDT_RedmineUsers_Limit ;
         }

         set {
            gxTv_SdtSDT_RedmineUsers_Limit = (short)(value);
         }

      }

      [SoapAttribute( AttributeName = "type" )]
      [XmlAttribute( AttributeName = "type" )]
      public String gxTpr_Type
      {
         get {
            return gxTv_SdtSDT_RedmineUsers_Type ;
         }

         set {
            gxTv_SdtSDT_RedmineUsers_Type = (String)(value);
         }

      }

      public class gxTv_SdtSDT_RedmineUsers_Users_SdtSDT_RedmineUsers_user_80compatibility:SdtSDT_RedmineUsers_user {}
      [  SoapElement( ElementName = "user" )]
      [  XmlElement( ElementName = "user" , Namespace = "" , Type= typeof( SdtSDT_RedmineUsers_user ))]
      public GxObjectCollection gxTpr_Users_GxObjectCollection
      {
         get {
            if ( gxTv_SdtSDT_RedmineUsers_Users == null )
            {
               gxTv_SdtSDT_RedmineUsers_Users = new GxObjectCollection( context, "SDT_RedmineUsers.user", "", "SdtSDT_RedmineUsers_user", "GeneXus.Programs");
            }
            return (GxObjectCollection)gxTv_SdtSDT_RedmineUsers_Users ;
         }

         set {
            if ( gxTv_SdtSDT_RedmineUsers_Users == null )
            {
               gxTv_SdtSDT_RedmineUsers_Users = new GxObjectCollection( context, "SDT_RedmineUsers.user", "", "SdtSDT_RedmineUsers_user", "GeneXus.Programs");
            }
            gxTv_SdtSDT_RedmineUsers_Users = (GxObjectCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public IGxCollection gxTpr_Users
      {
         get {
            if ( gxTv_SdtSDT_RedmineUsers_Users == null )
            {
               gxTv_SdtSDT_RedmineUsers_Users = new GxObjectCollection( context, "SDT_RedmineUsers.user", "", "SdtSDT_RedmineUsers_user", "GeneXus.Programs");
            }
            return gxTv_SdtSDT_RedmineUsers_Users ;
         }

         set {
            gxTv_SdtSDT_RedmineUsers_Users = value;
         }

      }

      public void gxTv_SdtSDT_RedmineUsers_Users_SetNull( )
      {
         gxTv_SdtSDT_RedmineUsers_Users = null;
         return  ;
      }

      public bool gxTv_SdtSDT_RedmineUsers_Users_IsNull( )
      {
         if ( gxTv_SdtSDT_RedmineUsers_Users == null )
         {
            return true ;
         }
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtSDT_RedmineUsers_Type = "";
         sTagName = "";
         return  ;
      }

      protected short gxTv_SdtSDT_RedmineUsers_Total_count ;
      protected short gxTv_SdtSDT_RedmineUsers_Offset ;
      protected short gxTv_SdtSDT_RedmineUsers_Limit ;
      protected short readOk ;
      protected short nOutParmCount ;
      protected String gxTv_SdtSDT_RedmineUsers_Type ;
      protected String sTagName ;
      [ObjectCollection(ItemType=typeof( SdtSDT_RedmineUsers_user ))]
      protected IGxCollection gxTv_SdtSDT_RedmineUsers_Users=null ;
   }

   [DataContract(Name = @"SDT_RedmineUsers", Namespace = "")]
   public class SdtSDT_RedmineUsers_RESTInterface : GxGenericCollectionItem<SdtSDT_RedmineUsers>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_RedmineUsers_RESTInterface( ) : base()
      {
      }

      public SdtSDT_RedmineUsers_RESTInterface( SdtSDT_RedmineUsers psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "total_count" , Order = 0 )]
      public Nullable<short> gxTpr_Total_count
      {
         get {
            return sdt.gxTpr_Total_count ;
         }

         set {
            sdt.gxTpr_Total_count = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "offset" , Order = 1 )]
      public Nullable<short> gxTpr_Offset
      {
         get {
            return sdt.gxTpr_Offset ;
         }

         set {
            sdt.gxTpr_Offset = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "limit" , Order = 2 )]
      public Nullable<short> gxTpr_Limit
      {
         get {
            return sdt.gxTpr_Limit ;
         }

         set {
            sdt.gxTpr_Limit = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "type" , Order = 3 )]
      public String gxTpr_Type
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Type) ;
         }

         set {
            sdt.gxTpr_Type = (String)(value);
         }

      }

      [DataMember( Name = "users" , Order = 4 )]
      public GxGenericCollection<SdtSDT_RedmineUsers_user_RESTInterface> gxTpr_Users
      {
         get {
            return new GxGenericCollection<SdtSDT_RedmineUsers_user_RESTInterface>(sdt.gxTpr_Users) ;
         }

         set {
            value.LoadCollection(sdt.gxTpr_Users);
         }

      }

      public SdtSDT_RedmineUsers sdt
      {
         get {
            return (SdtSDT_RedmineUsers)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_RedmineUsers() ;
         }
      }

   }

}
