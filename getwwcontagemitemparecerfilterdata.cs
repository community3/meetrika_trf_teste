/*
               File: GetWWContagemItemParecerFilterData
        Description: Get WWContagem Item Parecer Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:4:35.5
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwcontagemitemparecerfilterdata : GXProcedure
   {
      public getwwcontagemitemparecerfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwcontagemitemparecerfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV26DDOName = aP0_DDOName;
         this.AV24SearchTxt = aP1_SearchTxt;
         this.AV25SearchTxtTo = aP2_SearchTxtTo;
         this.AV30OptionsJson = "" ;
         this.AV33OptionsDescJson = "" ;
         this.AV35OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV30OptionsJson;
         aP4_OptionsDescJson=this.AV33OptionsDescJson;
         aP5_OptionIndexesJson=this.AV35OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV26DDOName = aP0_DDOName;
         this.AV24SearchTxt = aP1_SearchTxt;
         this.AV25SearchTxtTo = aP2_SearchTxtTo;
         this.AV30OptionsJson = "" ;
         this.AV33OptionsDescJson = "" ;
         this.AV35OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV30OptionsJson;
         aP4_OptionsDescJson=this.AV33OptionsDescJson;
         aP5_OptionIndexesJson=this.AV35OptionIndexesJson;
         return AV35OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwcontagemitemparecerfilterdata objgetwwcontagemitemparecerfilterdata;
         objgetwwcontagemitemparecerfilterdata = new getwwcontagemitemparecerfilterdata();
         objgetwwcontagemitemparecerfilterdata.AV26DDOName = aP0_DDOName;
         objgetwwcontagemitemparecerfilterdata.AV24SearchTxt = aP1_SearchTxt;
         objgetwwcontagemitemparecerfilterdata.AV25SearchTxtTo = aP2_SearchTxtTo;
         objgetwwcontagemitemparecerfilterdata.AV30OptionsJson = "" ;
         objgetwwcontagemitemparecerfilterdata.AV33OptionsDescJson = "" ;
         objgetwwcontagemitemparecerfilterdata.AV35OptionIndexesJson = "" ;
         objgetwwcontagemitemparecerfilterdata.context.SetSubmitInitialConfig(context);
         objgetwwcontagemitemparecerfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwcontagemitemparecerfilterdata);
         aP3_OptionsJson=this.AV30OptionsJson;
         aP4_OptionsDescJson=this.AV33OptionsDescJson;
         aP5_OptionIndexesJson=this.AV35OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwcontagemitemparecerfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV29Options = (IGxCollection)(new GxSimpleCollection());
         AV32OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV34OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV26DDOName), "DDO_CONTAGEMITEMPARECER_COMENTARIO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMITEMPARECER_COMENTARIOOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV26DDOName), "DDO_CONTAGEMITEMPARECER_USUARIOPESSOANOM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMITEMPARECER_USUARIOPESSOANOMOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV30OptionsJson = AV29Options.ToJSonString(false);
         AV33OptionsDescJson = AV32OptionsDesc.ToJSonString(false);
         AV35OptionIndexesJson = AV34OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV37Session.Get("WWContagemItemParecerGridState"), "") == 0 )
         {
            AV39GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWContagemItemParecerGridState"), "");
         }
         else
         {
            AV39GridState.FromXml(AV37Session.Get("WWContagemItemParecerGridState"), "");
         }
         AV58GXV1 = 1;
         while ( AV58GXV1 <= AV39GridState.gxTpr_Filtervalues.Count )
         {
            AV40GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV39GridState.gxTpr_Filtervalues.Item(AV58GXV1));
            if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTAGEMITEMPARECER_CODIGO") == 0 )
            {
               AV10TFContagemItemParecer_Codigo = (int)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Value, "."));
               AV11TFContagemItemParecer_Codigo_To = (int)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTAGEMITEM_LANCAMENTO") == 0 )
            {
               AV12TFContagemItem_Lancamento = (int)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Value, "."));
               AV13TFContagemItem_Lancamento_To = (int)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTAGEMITEMPARECER_COMENTARIO") == 0 )
            {
               AV14TFContagemItemParecer_Comentario = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTAGEMITEMPARECER_COMENTARIO_SEL") == 0 )
            {
               AV15TFContagemItemParecer_Comentario_Sel = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTAGEMITEMPARECER_DATA") == 0 )
            {
               AV16TFContagemItemParecer_Data = context.localUtil.CToT( AV40GridStateFilterValue.gxTpr_Value, 2);
               AV17TFContagemItemParecer_Data_To = context.localUtil.CToT( AV40GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTAGEMITEMPARECER_USUARIOCOD") == 0 )
            {
               AV18TFContagemItemParecer_UsuarioCod = (int)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Value, "."));
               AV19TFContagemItemParecer_UsuarioCod_To = (int)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTAGEMITEMPARECER_USUARIOPESSOACOD") == 0 )
            {
               AV20TFContagemItemParecer_UsuarioPessoaCod = (int)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Value, "."));
               AV21TFContagemItemParecer_UsuarioPessoaCod_To = (int)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTAGEMITEMPARECER_USUARIOPESSOANOM") == 0 )
            {
               AV22TFContagemItemParecer_UsuarioPessoaNom = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTAGEMITEMPARECER_USUARIOPESSOANOM_SEL") == 0 )
            {
               AV23TFContagemItemParecer_UsuarioPessoaNom_Sel = AV40GridStateFilterValue.gxTpr_Value;
            }
            AV58GXV1 = (int)(AV58GXV1+1);
         }
         if ( AV39GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV41GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV39GridState.gxTpr_Dynamicfilters.Item(1));
            AV42DynamicFiltersSelector1 = AV41GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV42DynamicFiltersSelector1, "CONTAGEMITEMPARECER_COMENTARIO") == 0 )
            {
               AV43DynamicFiltersOperator1 = AV41GridStateDynamicFilter.gxTpr_Operator;
               AV44ContagemItemParecer_Comentario1 = AV41GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV42DynamicFiltersSelector1, "CONTAGEMITEMPARECER_USUARIOPESSOANOM") == 0 )
            {
               AV43DynamicFiltersOperator1 = AV41GridStateDynamicFilter.gxTpr_Operator;
               AV45ContagemItemParecer_UsuarioPessoaNom1 = AV41GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV39GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV46DynamicFiltersEnabled2 = true;
               AV41GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV39GridState.gxTpr_Dynamicfilters.Item(2));
               AV47DynamicFiltersSelector2 = AV41GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV47DynamicFiltersSelector2, "CONTAGEMITEMPARECER_COMENTARIO") == 0 )
               {
                  AV48DynamicFiltersOperator2 = AV41GridStateDynamicFilter.gxTpr_Operator;
                  AV49ContagemItemParecer_Comentario2 = AV41GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV47DynamicFiltersSelector2, "CONTAGEMITEMPARECER_USUARIOPESSOANOM") == 0 )
               {
                  AV48DynamicFiltersOperator2 = AV41GridStateDynamicFilter.gxTpr_Operator;
                  AV50ContagemItemParecer_UsuarioPessoaNom2 = AV41GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV39GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV51DynamicFiltersEnabled3 = true;
                  AV41GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV39GridState.gxTpr_Dynamicfilters.Item(3));
                  AV52DynamicFiltersSelector3 = AV41GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV52DynamicFiltersSelector3, "CONTAGEMITEMPARECER_COMENTARIO") == 0 )
                  {
                     AV53DynamicFiltersOperator3 = AV41GridStateDynamicFilter.gxTpr_Operator;
                     AV54ContagemItemParecer_Comentario3 = AV41GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV52DynamicFiltersSelector3, "CONTAGEMITEMPARECER_USUARIOPESSOANOM") == 0 )
                  {
                     AV53DynamicFiltersOperator3 = AV41GridStateDynamicFilter.gxTpr_Operator;
                     AV55ContagemItemParecer_UsuarioPessoaNom3 = AV41GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTAGEMITEMPARECER_COMENTARIOOPTIONS' Routine */
         AV14TFContagemItemParecer_Comentario = AV24SearchTxt;
         AV15TFContagemItemParecer_Comentario_Sel = "";
         AV60WWContagemItemParecerDS_1_Dynamicfiltersselector1 = AV42DynamicFiltersSelector1;
         AV61WWContagemItemParecerDS_2_Dynamicfiltersoperator1 = AV43DynamicFiltersOperator1;
         AV62WWContagemItemParecerDS_3_Contagemitemparecer_comentario1 = AV44ContagemItemParecer_Comentario1;
         AV63WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1 = AV45ContagemItemParecer_UsuarioPessoaNom1;
         AV64WWContagemItemParecerDS_5_Dynamicfiltersenabled2 = AV46DynamicFiltersEnabled2;
         AV65WWContagemItemParecerDS_6_Dynamicfiltersselector2 = AV47DynamicFiltersSelector2;
         AV66WWContagemItemParecerDS_7_Dynamicfiltersoperator2 = AV48DynamicFiltersOperator2;
         AV67WWContagemItemParecerDS_8_Contagemitemparecer_comentario2 = AV49ContagemItemParecer_Comentario2;
         AV68WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2 = AV50ContagemItemParecer_UsuarioPessoaNom2;
         AV69WWContagemItemParecerDS_10_Dynamicfiltersenabled3 = AV51DynamicFiltersEnabled3;
         AV70WWContagemItemParecerDS_11_Dynamicfiltersselector3 = AV52DynamicFiltersSelector3;
         AV71WWContagemItemParecerDS_12_Dynamicfiltersoperator3 = AV53DynamicFiltersOperator3;
         AV72WWContagemItemParecerDS_13_Contagemitemparecer_comentario3 = AV54ContagemItemParecer_Comentario3;
         AV73WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3 = AV55ContagemItemParecer_UsuarioPessoaNom3;
         AV74WWContagemItemParecerDS_15_Tfcontagemitemparecer_codigo = AV10TFContagemItemParecer_Codigo;
         AV75WWContagemItemParecerDS_16_Tfcontagemitemparecer_codigo_to = AV11TFContagemItemParecer_Codigo_To;
         AV76WWContagemItemParecerDS_17_Tfcontagemitem_lancamento = AV12TFContagemItem_Lancamento;
         AV77WWContagemItemParecerDS_18_Tfcontagemitem_lancamento_to = AV13TFContagemItem_Lancamento_To;
         AV78WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario = AV14TFContagemItemParecer_Comentario;
         AV79WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel = AV15TFContagemItemParecer_Comentario_Sel;
         AV80WWContagemItemParecerDS_21_Tfcontagemitemparecer_data = AV16TFContagemItemParecer_Data;
         AV81WWContagemItemParecerDS_22_Tfcontagemitemparecer_data_to = AV17TFContagemItemParecer_Data_To;
         AV82WWContagemItemParecerDS_23_Tfcontagemitemparecer_usuariocod = AV18TFContagemItemParecer_UsuarioCod;
         AV83WWContagemItemParecerDS_24_Tfcontagemitemparecer_usuariocod_to = AV19TFContagemItemParecer_UsuarioCod_To;
         AV84WWContagemItemParecerDS_25_Tfcontagemitemparecer_usuariopessoacod = AV20TFContagemItemParecer_UsuarioPessoaCod;
         AV85WWContagemItemParecerDS_26_Tfcontagemitemparecer_usuariopessoacod_to = AV21TFContagemItemParecer_UsuarioPessoaCod_To;
         AV86WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom = AV22TFContagemItemParecer_UsuarioPessoaNom;
         AV87WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel = AV23TFContagemItemParecer_UsuarioPessoaNom_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV60WWContagemItemParecerDS_1_Dynamicfiltersselector1 ,
                                              AV61WWContagemItemParecerDS_2_Dynamicfiltersoperator1 ,
                                              AV62WWContagemItemParecerDS_3_Contagemitemparecer_comentario1 ,
                                              AV63WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1 ,
                                              AV64WWContagemItemParecerDS_5_Dynamicfiltersenabled2 ,
                                              AV65WWContagemItemParecerDS_6_Dynamicfiltersselector2 ,
                                              AV66WWContagemItemParecerDS_7_Dynamicfiltersoperator2 ,
                                              AV67WWContagemItemParecerDS_8_Contagemitemparecer_comentario2 ,
                                              AV68WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2 ,
                                              AV69WWContagemItemParecerDS_10_Dynamicfiltersenabled3 ,
                                              AV70WWContagemItemParecerDS_11_Dynamicfiltersselector3 ,
                                              AV71WWContagemItemParecerDS_12_Dynamicfiltersoperator3 ,
                                              AV72WWContagemItemParecerDS_13_Contagemitemparecer_comentario3 ,
                                              AV73WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3 ,
                                              AV74WWContagemItemParecerDS_15_Tfcontagemitemparecer_codigo ,
                                              AV75WWContagemItemParecerDS_16_Tfcontagemitemparecer_codigo_to ,
                                              AV76WWContagemItemParecerDS_17_Tfcontagemitem_lancamento ,
                                              AV77WWContagemItemParecerDS_18_Tfcontagemitem_lancamento_to ,
                                              AV79WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel ,
                                              AV78WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario ,
                                              AV80WWContagemItemParecerDS_21_Tfcontagemitemparecer_data ,
                                              AV81WWContagemItemParecerDS_22_Tfcontagemitemparecer_data_to ,
                                              AV82WWContagemItemParecerDS_23_Tfcontagemitemparecer_usuariocod ,
                                              AV83WWContagemItemParecerDS_24_Tfcontagemitemparecer_usuariocod_to ,
                                              AV84WWContagemItemParecerDS_25_Tfcontagemitemparecer_usuariopessoacod ,
                                              AV85WWContagemItemParecerDS_26_Tfcontagemitemparecer_usuariopessoacod_to ,
                                              AV87WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel ,
                                              AV86WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom ,
                                              A244ContagemItemParecer_Comentario ,
                                              A248ContagemItemParecer_UsuarioPessoaNom ,
                                              A243ContagemItemParecer_Codigo ,
                                              A224ContagemItem_Lancamento ,
                                              A245ContagemItemParecer_Data ,
                                              A246ContagemItemParecer_UsuarioCod ,
                                              A247ContagemItemParecer_UsuarioPessoaCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         lV62WWContagemItemParecerDS_3_Contagemitemparecer_comentario1 = StringUtil.Concat( StringUtil.RTrim( AV62WWContagemItemParecerDS_3_Contagemitemparecer_comentario1), "%", "");
         lV62WWContagemItemParecerDS_3_Contagemitemparecer_comentario1 = StringUtil.Concat( StringUtil.RTrim( AV62WWContagemItemParecerDS_3_Contagemitemparecer_comentario1), "%", "");
         lV63WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV63WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1), 100, "%");
         lV63WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV63WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1), 100, "%");
         lV67WWContagemItemParecerDS_8_Contagemitemparecer_comentario2 = StringUtil.Concat( StringUtil.RTrim( AV67WWContagemItemParecerDS_8_Contagemitemparecer_comentario2), "%", "");
         lV67WWContagemItemParecerDS_8_Contagemitemparecer_comentario2 = StringUtil.Concat( StringUtil.RTrim( AV67WWContagemItemParecerDS_8_Contagemitemparecer_comentario2), "%", "");
         lV68WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV68WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2), 100, "%");
         lV68WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV68WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2), 100, "%");
         lV72WWContagemItemParecerDS_13_Contagemitemparecer_comentario3 = StringUtil.Concat( StringUtil.RTrim( AV72WWContagemItemParecerDS_13_Contagemitemparecer_comentario3), "%", "");
         lV72WWContagemItemParecerDS_13_Contagemitemparecer_comentario3 = StringUtil.Concat( StringUtil.RTrim( AV72WWContagemItemParecerDS_13_Contagemitemparecer_comentario3), "%", "");
         lV73WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3 = StringUtil.PadR( StringUtil.RTrim( AV73WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3), 100, "%");
         lV73WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3 = StringUtil.PadR( StringUtil.RTrim( AV73WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3), 100, "%");
         lV78WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario = StringUtil.Concat( StringUtil.RTrim( AV78WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario), "%", "");
         lV86WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom = StringUtil.PadR( StringUtil.RTrim( AV86WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom), 100, "%");
         /* Using cursor P00IF2 */
         pr_default.execute(0, new Object[] {lV62WWContagemItemParecerDS_3_Contagemitemparecer_comentario1, lV62WWContagemItemParecerDS_3_Contagemitemparecer_comentario1, lV63WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1, lV63WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1, lV67WWContagemItemParecerDS_8_Contagemitemparecer_comentario2, lV67WWContagemItemParecerDS_8_Contagemitemparecer_comentario2, lV68WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2, lV68WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2, lV72WWContagemItemParecerDS_13_Contagemitemparecer_comentario3, lV72WWContagemItemParecerDS_13_Contagemitemparecer_comentario3, lV73WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3, lV73WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3, AV74WWContagemItemParecerDS_15_Tfcontagemitemparecer_codigo, AV75WWContagemItemParecerDS_16_Tfcontagemitemparecer_codigo_to, AV76WWContagemItemParecerDS_17_Tfcontagemitem_lancamento, AV77WWContagemItemParecerDS_18_Tfcontagemitem_lancamento_to, lV78WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario, AV79WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel, AV80WWContagemItemParecerDS_21_Tfcontagemitemparecer_data, AV81WWContagemItemParecerDS_22_Tfcontagemitemparecer_data_to, AV82WWContagemItemParecerDS_23_Tfcontagemitemparecer_usuariocod, AV83WWContagemItemParecerDS_24_Tfcontagemitemparecer_usuariocod_to, AV84WWContagemItemParecerDS_25_Tfcontagemitemparecer_usuariopessoacod, AV85WWContagemItemParecerDS_26_Tfcontagemitemparecer_usuariopessoacod_to, lV86WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom, AV87WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKIF2 = false;
            A244ContagemItemParecer_Comentario = P00IF2_A244ContagemItemParecer_Comentario[0];
            A247ContagemItemParecer_UsuarioPessoaCod = P00IF2_A247ContagemItemParecer_UsuarioPessoaCod[0];
            n247ContagemItemParecer_UsuarioPessoaCod = P00IF2_n247ContagemItemParecer_UsuarioPessoaCod[0];
            A246ContagemItemParecer_UsuarioCod = P00IF2_A246ContagemItemParecer_UsuarioCod[0];
            A245ContagemItemParecer_Data = P00IF2_A245ContagemItemParecer_Data[0];
            A224ContagemItem_Lancamento = P00IF2_A224ContagemItem_Lancamento[0];
            A243ContagemItemParecer_Codigo = P00IF2_A243ContagemItemParecer_Codigo[0];
            A248ContagemItemParecer_UsuarioPessoaNom = P00IF2_A248ContagemItemParecer_UsuarioPessoaNom[0];
            n248ContagemItemParecer_UsuarioPessoaNom = P00IF2_n248ContagemItemParecer_UsuarioPessoaNom[0];
            A247ContagemItemParecer_UsuarioPessoaCod = P00IF2_A247ContagemItemParecer_UsuarioPessoaCod[0];
            n247ContagemItemParecer_UsuarioPessoaCod = P00IF2_n247ContagemItemParecer_UsuarioPessoaCod[0];
            A248ContagemItemParecer_UsuarioPessoaNom = P00IF2_A248ContagemItemParecer_UsuarioPessoaNom[0];
            n248ContagemItemParecer_UsuarioPessoaNom = P00IF2_n248ContagemItemParecer_UsuarioPessoaNom[0];
            AV36count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00IF2_A244ContagemItemParecer_Comentario[0], A244ContagemItemParecer_Comentario) == 0 ) )
            {
               BRKIF2 = false;
               A243ContagemItemParecer_Codigo = P00IF2_A243ContagemItemParecer_Codigo[0];
               AV36count = (long)(AV36count+1);
               BRKIF2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A244ContagemItemParecer_Comentario)) )
            {
               AV28Option = A244ContagemItemParecer_Comentario;
               AV29Options.Add(AV28Option, 0);
               AV34OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV36count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV29Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKIF2 )
            {
               BRKIF2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTAGEMITEMPARECER_USUARIOPESSOANOMOPTIONS' Routine */
         AV22TFContagemItemParecer_UsuarioPessoaNom = AV24SearchTxt;
         AV23TFContagemItemParecer_UsuarioPessoaNom_Sel = "";
         AV60WWContagemItemParecerDS_1_Dynamicfiltersselector1 = AV42DynamicFiltersSelector1;
         AV61WWContagemItemParecerDS_2_Dynamicfiltersoperator1 = AV43DynamicFiltersOperator1;
         AV62WWContagemItemParecerDS_3_Contagemitemparecer_comentario1 = AV44ContagemItemParecer_Comentario1;
         AV63WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1 = AV45ContagemItemParecer_UsuarioPessoaNom1;
         AV64WWContagemItemParecerDS_5_Dynamicfiltersenabled2 = AV46DynamicFiltersEnabled2;
         AV65WWContagemItemParecerDS_6_Dynamicfiltersselector2 = AV47DynamicFiltersSelector2;
         AV66WWContagemItemParecerDS_7_Dynamicfiltersoperator2 = AV48DynamicFiltersOperator2;
         AV67WWContagemItemParecerDS_8_Contagemitemparecer_comentario2 = AV49ContagemItemParecer_Comentario2;
         AV68WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2 = AV50ContagemItemParecer_UsuarioPessoaNom2;
         AV69WWContagemItemParecerDS_10_Dynamicfiltersenabled3 = AV51DynamicFiltersEnabled3;
         AV70WWContagemItemParecerDS_11_Dynamicfiltersselector3 = AV52DynamicFiltersSelector3;
         AV71WWContagemItemParecerDS_12_Dynamicfiltersoperator3 = AV53DynamicFiltersOperator3;
         AV72WWContagemItemParecerDS_13_Contagemitemparecer_comentario3 = AV54ContagemItemParecer_Comentario3;
         AV73WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3 = AV55ContagemItemParecer_UsuarioPessoaNom3;
         AV74WWContagemItemParecerDS_15_Tfcontagemitemparecer_codigo = AV10TFContagemItemParecer_Codigo;
         AV75WWContagemItemParecerDS_16_Tfcontagemitemparecer_codigo_to = AV11TFContagemItemParecer_Codigo_To;
         AV76WWContagemItemParecerDS_17_Tfcontagemitem_lancamento = AV12TFContagemItem_Lancamento;
         AV77WWContagemItemParecerDS_18_Tfcontagemitem_lancamento_to = AV13TFContagemItem_Lancamento_To;
         AV78WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario = AV14TFContagemItemParecer_Comentario;
         AV79WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel = AV15TFContagemItemParecer_Comentario_Sel;
         AV80WWContagemItemParecerDS_21_Tfcontagemitemparecer_data = AV16TFContagemItemParecer_Data;
         AV81WWContagemItemParecerDS_22_Tfcontagemitemparecer_data_to = AV17TFContagemItemParecer_Data_To;
         AV82WWContagemItemParecerDS_23_Tfcontagemitemparecer_usuariocod = AV18TFContagemItemParecer_UsuarioCod;
         AV83WWContagemItemParecerDS_24_Tfcontagemitemparecer_usuariocod_to = AV19TFContagemItemParecer_UsuarioCod_To;
         AV84WWContagemItemParecerDS_25_Tfcontagemitemparecer_usuariopessoacod = AV20TFContagemItemParecer_UsuarioPessoaCod;
         AV85WWContagemItemParecerDS_26_Tfcontagemitemparecer_usuariopessoacod_to = AV21TFContagemItemParecer_UsuarioPessoaCod_To;
         AV86WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom = AV22TFContagemItemParecer_UsuarioPessoaNom;
         AV87WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel = AV23TFContagemItemParecer_UsuarioPessoaNom_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV60WWContagemItemParecerDS_1_Dynamicfiltersselector1 ,
                                              AV61WWContagemItemParecerDS_2_Dynamicfiltersoperator1 ,
                                              AV62WWContagemItemParecerDS_3_Contagemitemparecer_comentario1 ,
                                              AV63WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1 ,
                                              AV64WWContagemItemParecerDS_5_Dynamicfiltersenabled2 ,
                                              AV65WWContagemItemParecerDS_6_Dynamicfiltersselector2 ,
                                              AV66WWContagemItemParecerDS_7_Dynamicfiltersoperator2 ,
                                              AV67WWContagemItemParecerDS_8_Contagemitemparecer_comentario2 ,
                                              AV68WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2 ,
                                              AV69WWContagemItemParecerDS_10_Dynamicfiltersenabled3 ,
                                              AV70WWContagemItemParecerDS_11_Dynamicfiltersselector3 ,
                                              AV71WWContagemItemParecerDS_12_Dynamicfiltersoperator3 ,
                                              AV72WWContagemItemParecerDS_13_Contagemitemparecer_comentario3 ,
                                              AV73WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3 ,
                                              AV74WWContagemItemParecerDS_15_Tfcontagemitemparecer_codigo ,
                                              AV75WWContagemItemParecerDS_16_Tfcontagemitemparecer_codigo_to ,
                                              AV76WWContagemItemParecerDS_17_Tfcontagemitem_lancamento ,
                                              AV77WWContagemItemParecerDS_18_Tfcontagemitem_lancamento_to ,
                                              AV79WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel ,
                                              AV78WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario ,
                                              AV80WWContagemItemParecerDS_21_Tfcontagemitemparecer_data ,
                                              AV81WWContagemItemParecerDS_22_Tfcontagemitemparecer_data_to ,
                                              AV82WWContagemItemParecerDS_23_Tfcontagemitemparecer_usuariocod ,
                                              AV83WWContagemItemParecerDS_24_Tfcontagemitemparecer_usuariocod_to ,
                                              AV84WWContagemItemParecerDS_25_Tfcontagemitemparecer_usuariopessoacod ,
                                              AV85WWContagemItemParecerDS_26_Tfcontagemitemparecer_usuariopessoacod_to ,
                                              AV87WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel ,
                                              AV86WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom ,
                                              A244ContagemItemParecer_Comentario ,
                                              A248ContagemItemParecer_UsuarioPessoaNom ,
                                              A243ContagemItemParecer_Codigo ,
                                              A224ContagemItem_Lancamento ,
                                              A245ContagemItemParecer_Data ,
                                              A246ContagemItemParecer_UsuarioCod ,
                                              A247ContagemItemParecer_UsuarioPessoaCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         lV62WWContagemItemParecerDS_3_Contagemitemparecer_comentario1 = StringUtil.Concat( StringUtil.RTrim( AV62WWContagemItemParecerDS_3_Contagemitemparecer_comentario1), "%", "");
         lV62WWContagemItemParecerDS_3_Contagemitemparecer_comentario1 = StringUtil.Concat( StringUtil.RTrim( AV62WWContagemItemParecerDS_3_Contagemitemparecer_comentario1), "%", "");
         lV63WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV63WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1), 100, "%");
         lV63WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV63WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1), 100, "%");
         lV67WWContagemItemParecerDS_8_Contagemitemparecer_comentario2 = StringUtil.Concat( StringUtil.RTrim( AV67WWContagemItemParecerDS_8_Contagemitemparecer_comentario2), "%", "");
         lV67WWContagemItemParecerDS_8_Contagemitemparecer_comentario2 = StringUtil.Concat( StringUtil.RTrim( AV67WWContagemItemParecerDS_8_Contagemitemparecer_comentario2), "%", "");
         lV68WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV68WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2), 100, "%");
         lV68WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV68WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2), 100, "%");
         lV72WWContagemItemParecerDS_13_Contagemitemparecer_comentario3 = StringUtil.Concat( StringUtil.RTrim( AV72WWContagemItemParecerDS_13_Contagemitemparecer_comentario3), "%", "");
         lV72WWContagemItemParecerDS_13_Contagemitemparecer_comentario3 = StringUtil.Concat( StringUtil.RTrim( AV72WWContagemItemParecerDS_13_Contagemitemparecer_comentario3), "%", "");
         lV73WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3 = StringUtil.PadR( StringUtil.RTrim( AV73WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3), 100, "%");
         lV73WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3 = StringUtil.PadR( StringUtil.RTrim( AV73WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3), 100, "%");
         lV78WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario = StringUtil.Concat( StringUtil.RTrim( AV78WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario), "%", "");
         lV86WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom = StringUtil.PadR( StringUtil.RTrim( AV86WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom), 100, "%");
         /* Using cursor P00IF3 */
         pr_default.execute(1, new Object[] {lV62WWContagemItemParecerDS_3_Contagemitemparecer_comentario1, lV62WWContagemItemParecerDS_3_Contagemitemparecer_comentario1, lV63WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1, lV63WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1, lV67WWContagemItemParecerDS_8_Contagemitemparecer_comentario2, lV67WWContagemItemParecerDS_8_Contagemitemparecer_comentario2, lV68WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2, lV68WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2, lV72WWContagemItemParecerDS_13_Contagemitemparecer_comentario3, lV72WWContagemItemParecerDS_13_Contagemitemparecer_comentario3, lV73WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3, lV73WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3, AV74WWContagemItemParecerDS_15_Tfcontagemitemparecer_codigo, AV75WWContagemItemParecerDS_16_Tfcontagemitemparecer_codigo_to, AV76WWContagemItemParecerDS_17_Tfcontagemitem_lancamento, AV77WWContagemItemParecerDS_18_Tfcontagemitem_lancamento_to, lV78WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario, AV79WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel, AV80WWContagemItemParecerDS_21_Tfcontagemitemparecer_data, AV81WWContagemItemParecerDS_22_Tfcontagemitemparecer_data_to, AV82WWContagemItemParecerDS_23_Tfcontagemitemparecer_usuariocod, AV83WWContagemItemParecerDS_24_Tfcontagemitemparecer_usuariocod_to, AV84WWContagemItemParecerDS_25_Tfcontagemitemparecer_usuariopessoacod, AV85WWContagemItemParecerDS_26_Tfcontagemitemparecer_usuariopessoacod_to, lV86WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom, AV87WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKIF4 = false;
            A248ContagemItemParecer_UsuarioPessoaNom = P00IF3_A248ContagemItemParecer_UsuarioPessoaNom[0];
            n248ContagemItemParecer_UsuarioPessoaNom = P00IF3_n248ContagemItemParecer_UsuarioPessoaNom[0];
            A247ContagemItemParecer_UsuarioPessoaCod = P00IF3_A247ContagemItemParecer_UsuarioPessoaCod[0];
            n247ContagemItemParecer_UsuarioPessoaCod = P00IF3_n247ContagemItemParecer_UsuarioPessoaCod[0];
            A246ContagemItemParecer_UsuarioCod = P00IF3_A246ContagemItemParecer_UsuarioCod[0];
            A245ContagemItemParecer_Data = P00IF3_A245ContagemItemParecer_Data[0];
            A224ContagemItem_Lancamento = P00IF3_A224ContagemItem_Lancamento[0];
            A243ContagemItemParecer_Codigo = P00IF3_A243ContagemItemParecer_Codigo[0];
            A244ContagemItemParecer_Comentario = P00IF3_A244ContagemItemParecer_Comentario[0];
            A247ContagemItemParecer_UsuarioPessoaCod = P00IF3_A247ContagemItemParecer_UsuarioPessoaCod[0];
            n247ContagemItemParecer_UsuarioPessoaCod = P00IF3_n247ContagemItemParecer_UsuarioPessoaCod[0];
            A248ContagemItemParecer_UsuarioPessoaNom = P00IF3_A248ContagemItemParecer_UsuarioPessoaNom[0];
            n248ContagemItemParecer_UsuarioPessoaNom = P00IF3_n248ContagemItemParecer_UsuarioPessoaNom[0];
            AV36count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00IF3_A248ContagemItemParecer_UsuarioPessoaNom[0], A248ContagemItemParecer_UsuarioPessoaNom) == 0 ) )
            {
               BRKIF4 = false;
               A247ContagemItemParecer_UsuarioPessoaCod = P00IF3_A247ContagemItemParecer_UsuarioPessoaCod[0];
               n247ContagemItemParecer_UsuarioPessoaCod = P00IF3_n247ContagemItemParecer_UsuarioPessoaCod[0];
               A246ContagemItemParecer_UsuarioCod = P00IF3_A246ContagemItemParecer_UsuarioCod[0];
               A243ContagemItemParecer_Codigo = P00IF3_A243ContagemItemParecer_Codigo[0];
               A247ContagemItemParecer_UsuarioPessoaCod = P00IF3_A247ContagemItemParecer_UsuarioPessoaCod[0];
               n247ContagemItemParecer_UsuarioPessoaCod = P00IF3_n247ContagemItemParecer_UsuarioPessoaCod[0];
               AV36count = (long)(AV36count+1);
               BRKIF4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A248ContagemItemParecer_UsuarioPessoaNom)) )
            {
               AV28Option = A248ContagemItemParecer_UsuarioPessoaNom;
               AV29Options.Add(AV28Option, 0);
               AV34OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV36count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV29Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKIF4 )
            {
               BRKIF4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV29Options = new GxSimpleCollection();
         AV32OptionsDesc = new GxSimpleCollection();
         AV34OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV37Session = context.GetSession();
         AV39GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV40GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV14TFContagemItemParecer_Comentario = "";
         AV15TFContagemItemParecer_Comentario_Sel = "";
         AV16TFContagemItemParecer_Data = (DateTime)(DateTime.MinValue);
         AV17TFContagemItemParecer_Data_To = (DateTime)(DateTime.MinValue);
         AV22TFContagemItemParecer_UsuarioPessoaNom = "";
         AV23TFContagemItemParecer_UsuarioPessoaNom_Sel = "";
         AV41GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV42DynamicFiltersSelector1 = "";
         AV44ContagemItemParecer_Comentario1 = "";
         AV45ContagemItemParecer_UsuarioPessoaNom1 = "";
         AV47DynamicFiltersSelector2 = "";
         AV49ContagemItemParecer_Comentario2 = "";
         AV50ContagemItemParecer_UsuarioPessoaNom2 = "";
         AV52DynamicFiltersSelector3 = "";
         AV54ContagemItemParecer_Comentario3 = "";
         AV55ContagemItemParecer_UsuarioPessoaNom3 = "";
         AV60WWContagemItemParecerDS_1_Dynamicfiltersselector1 = "";
         AV62WWContagemItemParecerDS_3_Contagemitemparecer_comentario1 = "";
         AV63WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1 = "";
         AV65WWContagemItemParecerDS_6_Dynamicfiltersselector2 = "";
         AV67WWContagemItemParecerDS_8_Contagemitemparecer_comentario2 = "";
         AV68WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2 = "";
         AV70WWContagemItemParecerDS_11_Dynamicfiltersselector3 = "";
         AV72WWContagemItemParecerDS_13_Contagemitemparecer_comentario3 = "";
         AV73WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3 = "";
         AV78WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario = "";
         AV79WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel = "";
         AV80WWContagemItemParecerDS_21_Tfcontagemitemparecer_data = (DateTime)(DateTime.MinValue);
         AV81WWContagemItemParecerDS_22_Tfcontagemitemparecer_data_to = (DateTime)(DateTime.MinValue);
         AV86WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom = "";
         AV87WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel = "";
         scmdbuf = "";
         lV62WWContagemItemParecerDS_3_Contagemitemparecer_comentario1 = "";
         lV63WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1 = "";
         lV67WWContagemItemParecerDS_8_Contagemitemparecer_comentario2 = "";
         lV68WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2 = "";
         lV72WWContagemItemParecerDS_13_Contagemitemparecer_comentario3 = "";
         lV73WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3 = "";
         lV78WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario = "";
         lV86WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom = "";
         A244ContagemItemParecer_Comentario = "";
         A248ContagemItemParecer_UsuarioPessoaNom = "";
         A245ContagemItemParecer_Data = (DateTime)(DateTime.MinValue);
         P00IF2_A244ContagemItemParecer_Comentario = new String[] {""} ;
         P00IF2_A247ContagemItemParecer_UsuarioPessoaCod = new int[1] ;
         P00IF2_n247ContagemItemParecer_UsuarioPessoaCod = new bool[] {false} ;
         P00IF2_A246ContagemItemParecer_UsuarioCod = new int[1] ;
         P00IF2_A245ContagemItemParecer_Data = new DateTime[] {DateTime.MinValue} ;
         P00IF2_A224ContagemItem_Lancamento = new int[1] ;
         P00IF2_A243ContagemItemParecer_Codigo = new int[1] ;
         P00IF2_A248ContagemItemParecer_UsuarioPessoaNom = new String[] {""} ;
         P00IF2_n248ContagemItemParecer_UsuarioPessoaNom = new bool[] {false} ;
         AV28Option = "";
         P00IF3_A248ContagemItemParecer_UsuarioPessoaNom = new String[] {""} ;
         P00IF3_n248ContagemItemParecer_UsuarioPessoaNom = new bool[] {false} ;
         P00IF3_A247ContagemItemParecer_UsuarioPessoaCod = new int[1] ;
         P00IF3_n247ContagemItemParecer_UsuarioPessoaCod = new bool[] {false} ;
         P00IF3_A246ContagemItemParecer_UsuarioCod = new int[1] ;
         P00IF3_A245ContagemItemParecer_Data = new DateTime[] {DateTime.MinValue} ;
         P00IF3_A224ContagemItem_Lancamento = new int[1] ;
         P00IF3_A243ContagemItemParecer_Codigo = new int[1] ;
         P00IF3_A244ContagemItemParecer_Comentario = new String[] {""} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwcontagemitemparecerfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00IF2_A244ContagemItemParecer_Comentario, P00IF2_A247ContagemItemParecer_UsuarioPessoaCod, P00IF2_n247ContagemItemParecer_UsuarioPessoaCod, P00IF2_A246ContagemItemParecer_UsuarioCod, P00IF2_A245ContagemItemParecer_Data, P00IF2_A224ContagemItem_Lancamento, P00IF2_A243ContagemItemParecer_Codigo, P00IF2_A248ContagemItemParecer_UsuarioPessoaNom, P00IF2_n248ContagemItemParecer_UsuarioPessoaNom
               }
               , new Object[] {
               P00IF3_A248ContagemItemParecer_UsuarioPessoaNom, P00IF3_n248ContagemItemParecer_UsuarioPessoaNom, P00IF3_A247ContagemItemParecer_UsuarioPessoaCod, P00IF3_n247ContagemItemParecer_UsuarioPessoaCod, P00IF3_A246ContagemItemParecer_UsuarioCod, P00IF3_A245ContagemItemParecer_Data, P00IF3_A224ContagemItem_Lancamento, P00IF3_A243ContagemItemParecer_Codigo, P00IF3_A244ContagemItemParecer_Comentario
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV43DynamicFiltersOperator1 ;
      private short AV48DynamicFiltersOperator2 ;
      private short AV53DynamicFiltersOperator3 ;
      private short AV61WWContagemItemParecerDS_2_Dynamicfiltersoperator1 ;
      private short AV66WWContagemItemParecerDS_7_Dynamicfiltersoperator2 ;
      private short AV71WWContagemItemParecerDS_12_Dynamicfiltersoperator3 ;
      private int AV58GXV1 ;
      private int AV10TFContagemItemParecer_Codigo ;
      private int AV11TFContagemItemParecer_Codigo_To ;
      private int AV12TFContagemItem_Lancamento ;
      private int AV13TFContagemItem_Lancamento_To ;
      private int AV18TFContagemItemParecer_UsuarioCod ;
      private int AV19TFContagemItemParecer_UsuarioCod_To ;
      private int AV20TFContagemItemParecer_UsuarioPessoaCod ;
      private int AV21TFContagemItemParecer_UsuarioPessoaCod_To ;
      private int AV74WWContagemItemParecerDS_15_Tfcontagemitemparecer_codigo ;
      private int AV75WWContagemItemParecerDS_16_Tfcontagemitemparecer_codigo_to ;
      private int AV76WWContagemItemParecerDS_17_Tfcontagemitem_lancamento ;
      private int AV77WWContagemItemParecerDS_18_Tfcontagemitem_lancamento_to ;
      private int AV82WWContagemItemParecerDS_23_Tfcontagemitemparecer_usuariocod ;
      private int AV83WWContagemItemParecerDS_24_Tfcontagemitemparecer_usuariocod_to ;
      private int AV84WWContagemItemParecerDS_25_Tfcontagemitemparecer_usuariopessoacod ;
      private int AV85WWContagemItemParecerDS_26_Tfcontagemitemparecer_usuariopessoacod_to ;
      private int A243ContagemItemParecer_Codigo ;
      private int A224ContagemItem_Lancamento ;
      private int A246ContagemItemParecer_UsuarioCod ;
      private int A247ContagemItemParecer_UsuarioPessoaCod ;
      private long AV36count ;
      private String AV22TFContagemItemParecer_UsuarioPessoaNom ;
      private String AV23TFContagemItemParecer_UsuarioPessoaNom_Sel ;
      private String AV45ContagemItemParecer_UsuarioPessoaNom1 ;
      private String AV50ContagemItemParecer_UsuarioPessoaNom2 ;
      private String AV55ContagemItemParecer_UsuarioPessoaNom3 ;
      private String AV63WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1 ;
      private String AV68WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2 ;
      private String AV73WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3 ;
      private String AV86WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom ;
      private String AV87WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel ;
      private String scmdbuf ;
      private String lV63WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1 ;
      private String lV68WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2 ;
      private String lV73WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3 ;
      private String lV86WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom ;
      private String A248ContagemItemParecer_UsuarioPessoaNom ;
      private DateTime AV16TFContagemItemParecer_Data ;
      private DateTime AV17TFContagemItemParecer_Data_To ;
      private DateTime AV80WWContagemItemParecerDS_21_Tfcontagemitemparecer_data ;
      private DateTime AV81WWContagemItemParecerDS_22_Tfcontagemitemparecer_data_to ;
      private DateTime A245ContagemItemParecer_Data ;
      private bool returnInSub ;
      private bool AV46DynamicFiltersEnabled2 ;
      private bool AV51DynamicFiltersEnabled3 ;
      private bool AV64WWContagemItemParecerDS_5_Dynamicfiltersenabled2 ;
      private bool AV69WWContagemItemParecerDS_10_Dynamicfiltersenabled3 ;
      private bool BRKIF2 ;
      private bool n247ContagemItemParecer_UsuarioPessoaCod ;
      private bool n248ContagemItemParecer_UsuarioPessoaNom ;
      private bool BRKIF4 ;
      private String AV35OptionIndexesJson ;
      private String AV30OptionsJson ;
      private String AV33OptionsDescJson ;
      private String AV44ContagemItemParecer_Comentario1 ;
      private String AV49ContagemItemParecer_Comentario2 ;
      private String AV54ContagemItemParecer_Comentario3 ;
      private String AV62WWContagemItemParecerDS_3_Contagemitemparecer_comentario1 ;
      private String AV67WWContagemItemParecerDS_8_Contagemitemparecer_comentario2 ;
      private String AV72WWContagemItemParecerDS_13_Contagemitemparecer_comentario3 ;
      private String lV62WWContagemItemParecerDS_3_Contagemitemparecer_comentario1 ;
      private String lV67WWContagemItemParecerDS_8_Contagemitemparecer_comentario2 ;
      private String lV72WWContagemItemParecerDS_13_Contagemitemparecer_comentario3 ;
      private String A244ContagemItemParecer_Comentario ;
      private String AV26DDOName ;
      private String AV24SearchTxt ;
      private String AV25SearchTxtTo ;
      private String AV14TFContagemItemParecer_Comentario ;
      private String AV15TFContagemItemParecer_Comentario_Sel ;
      private String AV42DynamicFiltersSelector1 ;
      private String AV47DynamicFiltersSelector2 ;
      private String AV52DynamicFiltersSelector3 ;
      private String AV60WWContagemItemParecerDS_1_Dynamicfiltersselector1 ;
      private String AV65WWContagemItemParecerDS_6_Dynamicfiltersselector2 ;
      private String AV70WWContagemItemParecerDS_11_Dynamicfiltersselector3 ;
      private String AV78WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario ;
      private String AV79WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel ;
      private String lV78WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario ;
      private String AV28Option ;
      private IGxSession AV37Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00IF2_A244ContagemItemParecer_Comentario ;
      private int[] P00IF2_A247ContagemItemParecer_UsuarioPessoaCod ;
      private bool[] P00IF2_n247ContagemItemParecer_UsuarioPessoaCod ;
      private int[] P00IF2_A246ContagemItemParecer_UsuarioCod ;
      private DateTime[] P00IF2_A245ContagemItemParecer_Data ;
      private int[] P00IF2_A224ContagemItem_Lancamento ;
      private int[] P00IF2_A243ContagemItemParecer_Codigo ;
      private String[] P00IF2_A248ContagemItemParecer_UsuarioPessoaNom ;
      private bool[] P00IF2_n248ContagemItemParecer_UsuarioPessoaNom ;
      private String[] P00IF3_A248ContagemItemParecer_UsuarioPessoaNom ;
      private bool[] P00IF3_n248ContagemItemParecer_UsuarioPessoaNom ;
      private int[] P00IF3_A247ContagemItemParecer_UsuarioPessoaCod ;
      private bool[] P00IF3_n247ContagemItemParecer_UsuarioPessoaCod ;
      private int[] P00IF3_A246ContagemItemParecer_UsuarioCod ;
      private DateTime[] P00IF3_A245ContagemItemParecer_Data ;
      private int[] P00IF3_A224ContagemItem_Lancamento ;
      private int[] P00IF3_A243ContagemItemParecer_Codigo ;
      private String[] P00IF3_A244ContagemItemParecer_Comentario ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV29Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV32OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV34OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV39GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV40GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV41GridStateDynamicFilter ;
   }

   public class getwwcontagemitemparecerfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00IF2( IGxContext context ,
                                             String AV60WWContagemItemParecerDS_1_Dynamicfiltersselector1 ,
                                             short AV61WWContagemItemParecerDS_2_Dynamicfiltersoperator1 ,
                                             String AV62WWContagemItemParecerDS_3_Contagemitemparecer_comentario1 ,
                                             String AV63WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1 ,
                                             bool AV64WWContagemItemParecerDS_5_Dynamicfiltersenabled2 ,
                                             String AV65WWContagemItemParecerDS_6_Dynamicfiltersselector2 ,
                                             short AV66WWContagemItemParecerDS_7_Dynamicfiltersoperator2 ,
                                             String AV67WWContagemItemParecerDS_8_Contagemitemparecer_comentario2 ,
                                             String AV68WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2 ,
                                             bool AV69WWContagemItemParecerDS_10_Dynamicfiltersenabled3 ,
                                             String AV70WWContagemItemParecerDS_11_Dynamicfiltersselector3 ,
                                             short AV71WWContagemItemParecerDS_12_Dynamicfiltersoperator3 ,
                                             String AV72WWContagemItemParecerDS_13_Contagemitemparecer_comentario3 ,
                                             String AV73WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3 ,
                                             int AV74WWContagemItemParecerDS_15_Tfcontagemitemparecer_codigo ,
                                             int AV75WWContagemItemParecerDS_16_Tfcontagemitemparecer_codigo_to ,
                                             int AV76WWContagemItemParecerDS_17_Tfcontagemitem_lancamento ,
                                             int AV77WWContagemItemParecerDS_18_Tfcontagemitem_lancamento_to ,
                                             String AV79WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel ,
                                             String AV78WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario ,
                                             DateTime AV80WWContagemItemParecerDS_21_Tfcontagemitemparecer_data ,
                                             DateTime AV81WWContagemItemParecerDS_22_Tfcontagemitemparecer_data_to ,
                                             int AV82WWContagemItemParecerDS_23_Tfcontagemitemparecer_usuariocod ,
                                             int AV83WWContagemItemParecerDS_24_Tfcontagemitemparecer_usuariocod_to ,
                                             int AV84WWContagemItemParecerDS_25_Tfcontagemitemparecer_usuariopessoacod ,
                                             int AV85WWContagemItemParecerDS_26_Tfcontagemitemparecer_usuariopessoacod_to ,
                                             String AV87WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel ,
                                             String AV86WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom ,
                                             String A244ContagemItemParecer_Comentario ,
                                             String A248ContagemItemParecer_UsuarioPessoaNom ,
                                             int A243ContagemItemParecer_Codigo ,
                                             int A224ContagemItem_Lancamento ,
                                             DateTime A245ContagemItemParecer_Data ,
                                             int A246ContagemItemParecer_UsuarioCod ,
                                             int A247ContagemItemParecer_UsuarioPessoaCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [26] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemItemParecer_Comentario], T2.[Usuario_PessoaCod] AS ContagemItemParecer_UsuarioPessoaCod, T1.[ContagemItemParecer_UsuarioCod] AS ContagemItemParecer_UsuarioCod, T1.[ContagemItemParecer_Data], T1.[ContagemItem_Lancamento], T1.[ContagemItemParecer_Codigo], T3.[Pessoa_Nome] AS ContagemItemParecer_UsuarioPessoaNom FROM (([ContagemItemParecer] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContagemItemParecer_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV60WWContagemItemParecerDS_1_Dynamicfiltersselector1, "CONTAGEMITEMPARECER_COMENTARIO") == 0 ) && ( ( AV61WWContagemItemParecerDS_2_Dynamicfiltersoperator1 == 0 ) || ( AV61WWContagemItemParecerDS_2_Dynamicfiltersoperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWContagemItemParecerDS_3_Contagemitemparecer_comentario1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Comentario] like @lV62WWContagemItemParecerDS_3_Contagemitemparecer_comentario1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Comentario] like @lV62WWContagemItemParecerDS_3_Contagemitemparecer_comentario1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV60WWContagemItemParecerDS_1_Dynamicfiltersselector1, "CONTAGEMITEMPARECER_COMENTARIO") == 0 ) && ( ( AV61WWContagemItemParecerDS_2_Dynamicfiltersoperator1 == 1 ) || ( AV61WWContagemItemParecerDS_2_Dynamicfiltersoperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWContagemItemParecerDS_3_Contagemitemparecer_comentario1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Comentario] like '%' + @lV62WWContagemItemParecerDS_3_Contagemitemparecer_comentario1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Comentario] like '%' + @lV62WWContagemItemParecerDS_3_Contagemitemparecer_comentario1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV60WWContagemItemParecerDS_1_Dynamicfiltersselector1, "CONTAGEMITEMPARECER_USUARIOPESSOANOM") == 0 ) && ( ( AV61WWContagemItemParecerDS_2_Dynamicfiltersoperator1 == 0 ) || ( AV61WWContagemItemParecerDS_2_Dynamicfiltersoperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV63WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV63WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV60WWContagemItemParecerDS_1_Dynamicfiltersselector1, "CONTAGEMITEMPARECER_USUARIOPESSOANOM") == 0 ) && ( ( AV61WWContagemItemParecerDS_2_Dynamicfiltersoperator1 == 1 ) || ( AV61WWContagemItemParecerDS_2_Dynamicfiltersoperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV63WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV63WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV64WWContagemItemParecerDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWContagemItemParecerDS_6_Dynamicfiltersselector2, "CONTAGEMITEMPARECER_COMENTARIO") == 0 ) && ( ( AV66WWContagemItemParecerDS_7_Dynamicfiltersoperator2 == 0 ) || ( AV66WWContagemItemParecerDS_7_Dynamicfiltersoperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContagemItemParecerDS_8_Contagemitemparecer_comentario2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Comentario] like @lV67WWContagemItemParecerDS_8_Contagemitemparecer_comentario2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Comentario] like @lV67WWContagemItemParecerDS_8_Contagemitemparecer_comentario2)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV64WWContagemItemParecerDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWContagemItemParecerDS_6_Dynamicfiltersselector2, "CONTAGEMITEMPARECER_COMENTARIO") == 0 ) && ( ( AV66WWContagemItemParecerDS_7_Dynamicfiltersoperator2 == 1 ) || ( AV66WWContagemItemParecerDS_7_Dynamicfiltersoperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContagemItemParecerDS_8_Contagemitemparecer_comentario2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Comentario] like '%' + @lV67WWContagemItemParecerDS_8_Contagemitemparecer_comentario2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Comentario] like '%' + @lV67WWContagemItemParecerDS_8_Contagemitemparecer_comentario2)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV64WWContagemItemParecerDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWContagemItemParecerDS_6_Dynamicfiltersselector2, "CONTAGEMITEMPARECER_USUARIOPESSOANOM") == 0 ) && ( ( AV66WWContagemItemParecerDS_7_Dynamicfiltersoperator2 == 0 ) || ( AV66WWContagemItemParecerDS_7_Dynamicfiltersoperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV68WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV68WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV64WWContagemItemParecerDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWContagemItemParecerDS_6_Dynamicfiltersselector2, "CONTAGEMITEMPARECER_USUARIOPESSOANOM") == 0 ) && ( ( AV66WWContagemItemParecerDS_7_Dynamicfiltersoperator2 == 1 ) || ( AV66WWContagemItemParecerDS_7_Dynamicfiltersoperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV68WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV68WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV69WWContagemItemParecerDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV70WWContagemItemParecerDS_11_Dynamicfiltersselector3, "CONTAGEMITEMPARECER_COMENTARIO") == 0 ) && ( ( AV71WWContagemItemParecerDS_12_Dynamicfiltersoperator3 == 0 ) || ( AV71WWContagemItemParecerDS_12_Dynamicfiltersoperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContagemItemParecerDS_13_Contagemitemparecer_comentario3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Comentario] like @lV72WWContagemItemParecerDS_13_Contagemitemparecer_comentario3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Comentario] like @lV72WWContagemItemParecerDS_13_Contagemitemparecer_comentario3)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV69WWContagemItemParecerDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV70WWContagemItemParecerDS_11_Dynamicfiltersselector3, "CONTAGEMITEMPARECER_COMENTARIO") == 0 ) && ( ( AV71WWContagemItemParecerDS_12_Dynamicfiltersoperator3 == 1 ) || ( AV71WWContagemItemParecerDS_12_Dynamicfiltersoperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContagemItemParecerDS_13_Contagemitemparecer_comentario3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Comentario] like '%' + @lV72WWContagemItemParecerDS_13_Contagemitemparecer_comentario3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Comentario] like '%' + @lV72WWContagemItemParecerDS_13_Contagemitemparecer_comentario3)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV69WWContagemItemParecerDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV70WWContagemItemParecerDS_11_Dynamicfiltersselector3, "CONTAGEMITEMPARECER_USUARIOPESSOANOM") == 0 ) && ( ( AV71WWContagemItemParecerDS_12_Dynamicfiltersoperator3 == 0 ) || ( AV71WWContagemItemParecerDS_12_Dynamicfiltersoperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV73WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV73WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV69WWContagemItemParecerDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV70WWContagemItemParecerDS_11_Dynamicfiltersselector3, "CONTAGEMITEMPARECER_USUARIOPESSOANOM") == 0 ) && ( ( AV71WWContagemItemParecerDS_12_Dynamicfiltersoperator3 == 1 ) || ( AV71WWContagemItemParecerDS_12_Dynamicfiltersoperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV73WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV73WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! (0==AV74WWContagemItemParecerDS_15_Tfcontagemitemparecer_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Codigo] >= @AV74WWContagemItemParecerDS_15_Tfcontagemitemparecer_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Codigo] >= @AV74WWContagemItemParecerDS_15_Tfcontagemitemparecer_codigo)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! (0==AV75WWContagemItemParecerDS_16_Tfcontagemitemparecer_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Codigo] <= @AV75WWContagemItemParecerDS_16_Tfcontagemitemparecer_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Codigo] <= @AV75WWContagemItemParecerDS_16_Tfcontagemitemparecer_codigo_to)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ! (0==AV76WWContagemItemParecerDS_17_Tfcontagemitem_lancamento) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItem_Lancamento] >= @AV76WWContagemItemParecerDS_17_Tfcontagemitem_lancamento)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItem_Lancamento] >= @AV76WWContagemItemParecerDS_17_Tfcontagemitem_lancamento)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! (0==AV77WWContagemItemParecerDS_18_Tfcontagemitem_lancamento_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItem_Lancamento] <= @AV77WWContagemItemParecerDS_18_Tfcontagemitem_lancamento_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItem_Lancamento] <= @AV77WWContagemItemParecerDS_18_Tfcontagemitem_lancamento_to)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV79WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Comentario] like @lV78WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Comentario] like @lV78WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Comentario] = @AV79WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Comentario] = @AV79WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( ! (DateTime.MinValue==AV80WWContagemItemParecerDS_21_Tfcontagemitemparecer_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Data] >= @AV80WWContagemItemParecerDS_21_Tfcontagemitemparecer_data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Data] >= @AV80WWContagemItemParecerDS_21_Tfcontagemitemparecer_data)";
            }
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( ! (DateTime.MinValue==AV81WWContagemItemParecerDS_22_Tfcontagemitemparecer_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Data] <= @AV81WWContagemItemParecerDS_22_Tfcontagemitemparecer_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Data] <= @AV81WWContagemItemParecerDS_22_Tfcontagemitemparecer_data_to)";
            }
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( ! (0==AV82WWContagemItemParecerDS_23_Tfcontagemitemparecer_usuariocod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_UsuarioCod] >= @AV82WWContagemItemParecerDS_23_Tfcontagemitemparecer_usuariocod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_UsuarioCod] >= @AV82WWContagemItemParecerDS_23_Tfcontagemitemparecer_usuariocod)";
            }
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( ! (0==AV83WWContagemItemParecerDS_24_Tfcontagemitemparecer_usuariocod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_UsuarioCod] <= @AV83WWContagemItemParecerDS_24_Tfcontagemitemparecer_usuariocod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_UsuarioCod] <= @AV83WWContagemItemParecerDS_24_Tfcontagemitemparecer_usuariocod_to)";
            }
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( ! (0==AV84WWContagemItemParecerDS_25_Tfcontagemitemparecer_usuariopessoacod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_PessoaCod] >= @AV84WWContagemItemParecerDS_25_Tfcontagemitemparecer_usuariopessoacod)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_PessoaCod] >= @AV84WWContagemItemParecerDS_25_Tfcontagemitemparecer_usuariopessoacod)";
            }
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( ! (0==AV85WWContagemItemParecerDS_26_Tfcontagemitemparecer_usuariopessoacod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_PessoaCod] <= @AV85WWContagemItemParecerDS_26_Tfcontagemitemparecer_usuariopessoacod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_PessoaCod] <= @AV85WWContagemItemParecerDS_26_Tfcontagemitemparecer_usuariopessoacod_to)";
            }
         }
         else
         {
            GXv_int1[23] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV87WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV86WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV86WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom)";
            }
         }
         else
         {
            GXv_int1[24] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV87WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] = @AV87WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel)";
            }
         }
         else
         {
            GXv_int1[25] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemItemParecer_Comentario]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00IF3( IGxContext context ,
                                             String AV60WWContagemItemParecerDS_1_Dynamicfiltersselector1 ,
                                             short AV61WWContagemItemParecerDS_2_Dynamicfiltersoperator1 ,
                                             String AV62WWContagemItemParecerDS_3_Contagemitemparecer_comentario1 ,
                                             String AV63WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1 ,
                                             bool AV64WWContagemItemParecerDS_5_Dynamicfiltersenabled2 ,
                                             String AV65WWContagemItemParecerDS_6_Dynamicfiltersselector2 ,
                                             short AV66WWContagemItemParecerDS_7_Dynamicfiltersoperator2 ,
                                             String AV67WWContagemItemParecerDS_8_Contagemitemparecer_comentario2 ,
                                             String AV68WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2 ,
                                             bool AV69WWContagemItemParecerDS_10_Dynamicfiltersenabled3 ,
                                             String AV70WWContagemItemParecerDS_11_Dynamicfiltersselector3 ,
                                             short AV71WWContagemItemParecerDS_12_Dynamicfiltersoperator3 ,
                                             String AV72WWContagemItemParecerDS_13_Contagemitemparecer_comentario3 ,
                                             String AV73WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3 ,
                                             int AV74WWContagemItemParecerDS_15_Tfcontagemitemparecer_codigo ,
                                             int AV75WWContagemItemParecerDS_16_Tfcontagemitemparecer_codigo_to ,
                                             int AV76WWContagemItemParecerDS_17_Tfcontagemitem_lancamento ,
                                             int AV77WWContagemItemParecerDS_18_Tfcontagemitem_lancamento_to ,
                                             String AV79WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel ,
                                             String AV78WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario ,
                                             DateTime AV80WWContagemItemParecerDS_21_Tfcontagemitemparecer_data ,
                                             DateTime AV81WWContagemItemParecerDS_22_Tfcontagemitemparecer_data_to ,
                                             int AV82WWContagemItemParecerDS_23_Tfcontagemitemparecer_usuariocod ,
                                             int AV83WWContagemItemParecerDS_24_Tfcontagemitemparecer_usuariocod_to ,
                                             int AV84WWContagemItemParecerDS_25_Tfcontagemitemparecer_usuariopessoacod ,
                                             int AV85WWContagemItemParecerDS_26_Tfcontagemitemparecer_usuariopessoacod_to ,
                                             String AV87WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel ,
                                             String AV86WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom ,
                                             String A244ContagemItemParecer_Comentario ,
                                             String A248ContagemItemParecer_UsuarioPessoaNom ,
                                             int A243ContagemItemParecer_Codigo ,
                                             int A224ContagemItem_Lancamento ,
                                             DateTime A245ContagemItemParecer_Data ,
                                             int A246ContagemItemParecer_UsuarioCod ,
                                             int A247ContagemItemParecer_UsuarioPessoaCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [26] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T3.[Pessoa_Nome] AS ContagemItemParecer_UsuarioPessoaNom, T2.[Usuario_PessoaCod] AS ContagemItemParecer_UsuarioPessoaCod, T1.[ContagemItemParecer_UsuarioCod] AS ContagemItemParecer_UsuarioCod, T1.[ContagemItemParecer_Data], T1.[ContagemItem_Lancamento], T1.[ContagemItemParecer_Codigo], T1.[ContagemItemParecer_Comentario] FROM (([ContagemItemParecer] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContagemItemParecer_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV60WWContagemItemParecerDS_1_Dynamicfiltersselector1, "CONTAGEMITEMPARECER_COMENTARIO") == 0 ) && ( ( AV61WWContagemItemParecerDS_2_Dynamicfiltersoperator1 == 0 ) || ( AV61WWContagemItemParecerDS_2_Dynamicfiltersoperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWContagemItemParecerDS_3_Contagemitemparecer_comentario1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Comentario] like @lV62WWContagemItemParecerDS_3_Contagemitemparecer_comentario1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Comentario] like @lV62WWContagemItemParecerDS_3_Contagemitemparecer_comentario1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV60WWContagemItemParecerDS_1_Dynamicfiltersselector1, "CONTAGEMITEMPARECER_COMENTARIO") == 0 ) && ( ( AV61WWContagemItemParecerDS_2_Dynamicfiltersoperator1 == 1 ) || ( AV61WWContagemItemParecerDS_2_Dynamicfiltersoperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWContagemItemParecerDS_3_Contagemitemparecer_comentario1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Comentario] like '%' + @lV62WWContagemItemParecerDS_3_Contagemitemparecer_comentario1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Comentario] like '%' + @lV62WWContagemItemParecerDS_3_Contagemitemparecer_comentario1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV60WWContagemItemParecerDS_1_Dynamicfiltersselector1, "CONTAGEMITEMPARECER_USUARIOPESSOANOM") == 0 ) && ( ( AV61WWContagemItemParecerDS_2_Dynamicfiltersoperator1 == 0 ) || ( AV61WWContagemItemParecerDS_2_Dynamicfiltersoperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV63WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV63WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV60WWContagemItemParecerDS_1_Dynamicfiltersselector1, "CONTAGEMITEMPARECER_USUARIOPESSOANOM") == 0 ) && ( ( AV61WWContagemItemParecerDS_2_Dynamicfiltersoperator1 == 1 ) || ( AV61WWContagemItemParecerDS_2_Dynamicfiltersoperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV63WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV63WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV64WWContagemItemParecerDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWContagemItemParecerDS_6_Dynamicfiltersselector2, "CONTAGEMITEMPARECER_COMENTARIO") == 0 ) && ( ( AV66WWContagemItemParecerDS_7_Dynamicfiltersoperator2 == 0 ) || ( AV66WWContagemItemParecerDS_7_Dynamicfiltersoperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContagemItemParecerDS_8_Contagemitemparecer_comentario2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Comentario] like @lV67WWContagemItemParecerDS_8_Contagemitemparecer_comentario2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Comentario] like @lV67WWContagemItemParecerDS_8_Contagemitemparecer_comentario2)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV64WWContagemItemParecerDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWContagemItemParecerDS_6_Dynamicfiltersselector2, "CONTAGEMITEMPARECER_COMENTARIO") == 0 ) && ( ( AV66WWContagemItemParecerDS_7_Dynamicfiltersoperator2 == 1 ) || ( AV66WWContagemItemParecerDS_7_Dynamicfiltersoperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContagemItemParecerDS_8_Contagemitemparecer_comentario2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Comentario] like '%' + @lV67WWContagemItemParecerDS_8_Contagemitemparecer_comentario2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Comentario] like '%' + @lV67WWContagemItemParecerDS_8_Contagemitemparecer_comentario2)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV64WWContagemItemParecerDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWContagemItemParecerDS_6_Dynamicfiltersselector2, "CONTAGEMITEMPARECER_USUARIOPESSOANOM") == 0 ) && ( ( AV66WWContagemItemParecerDS_7_Dynamicfiltersoperator2 == 0 ) || ( AV66WWContagemItemParecerDS_7_Dynamicfiltersoperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV68WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV68WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV64WWContagemItemParecerDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWContagemItemParecerDS_6_Dynamicfiltersselector2, "CONTAGEMITEMPARECER_USUARIOPESSOANOM") == 0 ) && ( ( AV66WWContagemItemParecerDS_7_Dynamicfiltersoperator2 == 1 ) || ( AV66WWContagemItemParecerDS_7_Dynamicfiltersoperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV68WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV68WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV69WWContagemItemParecerDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV70WWContagemItemParecerDS_11_Dynamicfiltersselector3, "CONTAGEMITEMPARECER_COMENTARIO") == 0 ) && ( ( AV71WWContagemItemParecerDS_12_Dynamicfiltersoperator3 == 0 ) || ( AV71WWContagemItemParecerDS_12_Dynamicfiltersoperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContagemItemParecerDS_13_Contagemitemparecer_comentario3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Comentario] like @lV72WWContagemItemParecerDS_13_Contagemitemparecer_comentario3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Comentario] like @lV72WWContagemItemParecerDS_13_Contagemitemparecer_comentario3)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV69WWContagemItemParecerDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV70WWContagemItemParecerDS_11_Dynamicfiltersselector3, "CONTAGEMITEMPARECER_COMENTARIO") == 0 ) && ( ( AV71WWContagemItemParecerDS_12_Dynamicfiltersoperator3 == 1 ) || ( AV71WWContagemItemParecerDS_12_Dynamicfiltersoperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContagemItemParecerDS_13_Contagemitemparecer_comentario3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Comentario] like '%' + @lV72WWContagemItemParecerDS_13_Contagemitemparecer_comentario3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Comentario] like '%' + @lV72WWContagemItemParecerDS_13_Contagemitemparecer_comentario3)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV69WWContagemItemParecerDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV70WWContagemItemParecerDS_11_Dynamicfiltersselector3, "CONTAGEMITEMPARECER_USUARIOPESSOANOM") == 0 ) && ( ( AV71WWContagemItemParecerDS_12_Dynamicfiltersoperator3 == 0 ) || ( AV71WWContagemItemParecerDS_12_Dynamicfiltersoperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV73WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV73WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV69WWContagemItemParecerDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV70WWContagemItemParecerDS_11_Dynamicfiltersselector3, "CONTAGEMITEMPARECER_USUARIOPESSOANOM") == 0 ) && ( ( AV71WWContagemItemParecerDS_12_Dynamicfiltersoperator3 == 1 ) || ( AV71WWContagemItemParecerDS_12_Dynamicfiltersoperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV73WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV73WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! (0==AV74WWContagemItemParecerDS_15_Tfcontagemitemparecer_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Codigo] >= @AV74WWContagemItemParecerDS_15_Tfcontagemitemparecer_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Codigo] >= @AV74WWContagemItemParecerDS_15_Tfcontagemitemparecer_codigo)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ! (0==AV75WWContagemItemParecerDS_16_Tfcontagemitemparecer_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Codigo] <= @AV75WWContagemItemParecerDS_16_Tfcontagemitemparecer_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Codigo] <= @AV75WWContagemItemParecerDS_16_Tfcontagemitemparecer_codigo_to)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ! (0==AV76WWContagemItemParecerDS_17_Tfcontagemitem_lancamento) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItem_Lancamento] >= @AV76WWContagemItemParecerDS_17_Tfcontagemitem_lancamento)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItem_Lancamento] >= @AV76WWContagemItemParecerDS_17_Tfcontagemitem_lancamento)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ! (0==AV77WWContagemItemParecerDS_18_Tfcontagemitem_lancamento_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItem_Lancamento] <= @AV77WWContagemItemParecerDS_18_Tfcontagemitem_lancamento_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItem_Lancamento] <= @AV77WWContagemItemParecerDS_18_Tfcontagemitem_lancamento_to)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV79WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Comentario] like @lV78WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Comentario] like @lV78WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Comentario] = @AV79WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Comentario] = @AV79WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( ! (DateTime.MinValue==AV80WWContagemItemParecerDS_21_Tfcontagemitemparecer_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Data] >= @AV80WWContagemItemParecerDS_21_Tfcontagemitemparecer_data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Data] >= @AV80WWContagemItemParecerDS_21_Tfcontagemitemparecer_data)";
            }
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( ! (DateTime.MinValue==AV81WWContagemItemParecerDS_22_Tfcontagemitemparecer_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_Data] <= @AV81WWContagemItemParecerDS_22_Tfcontagemitemparecer_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_Data] <= @AV81WWContagemItemParecerDS_22_Tfcontagemitemparecer_data_to)";
            }
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( ! (0==AV82WWContagemItemParecerDS_23_Tfcontagemitemparecer_usuariocod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_UsuarioCod] >= @AV82WWContagemItemParecerDS_23_Tfcontagemitemparecer_usuariocod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_UsuarioCod] >= @AV82WWContagemItemParecerDS_23_Tfcontagemitemparecer_usuariocod)";
            }
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( ! (0==AV83WWContagemItemParecerDS_24_Tfcontagemitemparecer_usuariocod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItemParecer_UsuarioCod] <= @AV83WWContagemItemParecerDS_24_Tfcontagemitemparecer_usuariocod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItemParecer_UsuarioCod] <= @AV83WWContagemItemParecerDS_24_Tfcontagemitemparecer_usuariocod_to)";
            }
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( ! (0==AV84WWContagemItemParecerDS_25_Tfcontagemitemparecer_usuariopessoacod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_PessoaCod] >= @AV84WWContagemItemParecerDS_25_Tfcontagemitemparecer_usuariopessoacod)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_PessoaCod] >= @AV84WWContagemItemParecerDS_25_Tfcontagemitemparecer_usuariopessoacod)";
            }
         }
         else
         {
            GXv_int3[22] = 1;
         }
         if ( ! (0==AV85WWContagemItemParecerDS_26_Tfcontagemitemparecer_usuariopessoacod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_PessoaCod] <= @AV85WWContagemItemParecerDS_26_Tfcontagemitemparecer_usuariopessoacod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_PessoaCod] <= @AV85WWContagemItemParecerDS_26_Tfcontagemitemparecer_usuariopessoacod_to)";
            }
         }
         else
         {
            GXv_int3[23] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV87WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV86WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV86WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom)";
            }
         }
         else
         {
            GXv_int3[24] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV87WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] = @AV87WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel)";
            }
         }
         else
         {
            GXv_int3[25] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T3.[Pessoa_Nome]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00IF2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (DateTime)dynConstraints[20] , (DateTime)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] , (int)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (int)dynConstraints[31] , (DateTime)dynConstraints[32] , (int)dynConstraints[33] , (int)dynConstraints[34] );
               case 1 :
                     return conditional_P00IF3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (DateTime)dynConstraints[20] , (DateTime)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] , (int)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (int)dynConstraints[31] , (DateTime)dynConstraints[32] , (int)dynConstraints[33] , (int)dynConstraints[34] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00IF2 ;
          prmP00IF2 = new Object[] {
          new Object[] {"@lV62WWContagemItemParecerDS_3_Contagemitemparecer_comentario1",SqlDbType.VarChar,5000,0} ,
          new Object[] {"@lV62WWContagemItemParecerDS_3_Contagemitemparecer_comentario1",SqlDbType.VarChar,5000,0} ,
          new Object[] {"@lV63WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV63WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV67WWContagemItemParecerDS_8_Contagemitemparecer_comentario2",SqlDbType.VarChar,5000,0} ,
          new Object[] {"@lV67WWContagemItemParecerDS_8_Contagemitemparecer_comentario2",SqlDbType.VarChar,5000,0} ,
          new Object[] {"@lV68WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV68WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV72WWContagemItemParecerDS_13_Contagemitemparecer_comentario3",SqlDbType.VarChar,5000,0} ,
          new Object[] {"@lV72WWContagemItemParecerDS_13_Contagemitemparecer_comentario3",SqlDbType.VarChar,5000,0} ,
          new Object[] {"@lV73WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV73WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV74WWContagemItemParecerDS_15_Tfcontagemitemparecer_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV75WWContagemItemParecerDS_16_Tfcontagemitemparecer_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV76WWContagemItemParecerDS_17_Tfcontagemitem_lancamento",SqlDbType.Int,6,0} ,
          new Object[] {"@AV77WWContagemItemParecerDS_18_Tfcontagemitem_lancamento_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV78WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV79WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV80WWContagemItemParecerDS_21_Tfcontagemitemparecer_data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV81WWContagemItemParecerDS_22_Tfcontagemitemparecer_data_to",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV82WWContagemItemParecerDS_23_Tfcontagemitemparecer_usuariocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV83WWContagemItemParecerDS_24_Tfcontagemitemparecer_usuariocod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV84WWContagemItemParecerDS_25_Tfcontagemitemparecer_usuariopessoacod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV85WWContagemItemParecerDS_26_Tfcontagemitemparecer_usuariopessoacod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV86WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV87WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel",SqlDbType.Char,100,0}
          } ;
          Object[] prmP00IF3 ;
          prmP00IF3 = new Object[] {
          new Object[] {"@lV62WWContagemItemParecerDS_3_Contagemitemparecer_comentario1",SqlDbType.VarChar,5000,0} ,
          new Object[] {"@lV62WWContagemItemParecerDS_3_Contagemitemparecer_comentario1",SqlDbType.VarChar,5000,0} ,
          new Object[] {"@lV63WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV63WWContagemItemParecerDS_4_Contagemitemparecer_usuariopessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV67WWContagemItemParecerDS_8_Contagemitemparecer_comentario2",SqlDbType.VarChar,5000,0} ,
          new Object[] {"@lV67WWContagemItemParecerDS_8_Contagemitemparecer_comentario2",SqlDbType.VarChar,5000,0} ,
          new Object[] {"@lV68WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV68WWContagemItemParecerDS_9_Contagemitemparecer_usuariopessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV72WWContagemItemParecerDS_13_Contagemitemparecer_comentario3",SqlDbType.VarChar,5000,0} ,
          new Object[] {"@lV72WWContagemItemParecerDS_13_Contagemitemparecer_comentario3",SqlDbType.VarChar,5000,0} ,
          new Object[] {"@lV73WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV73WWContagemItemParecerDS_14_Contagemitemparecer_usuariopessoanom3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV74WWContagemItemParecerDS_15_Tfcontagemitemparecer_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV75WWContagemItemParecerDS_16_Tfcontagemitemparecer_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV76WWContagemItemParecerDS_17_Tfcontagemitem_lancamento",SqlDbType.Int,6,0} ,
          new Object[] {"@AV77WWContagemItemParecerDS_18_Tfcontagemitem_lancamento_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV78WWContagemItemParecerDS_19_Tfcontagemitemparecer_comentario",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV79WWContagemItemParecerDS_20_Tfcontagemitemparecer_comentario_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV80WWContagemItemParecerDS_21_Tfcontagemitemparecer_data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV81WWContagemItemParecerDS_22_Tfcontagemitemparecer_data_to",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV82WWContagemItemParecerDS_23_Tfcontagemitemparecer_usuariocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV83WWContagemItemParecerDS_24_Tfcontagemitemparecer_usuariocod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV84WWContagemItemParecerDS_25_Tfcontagemitemparecer_usuariopessoacod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV85WWContagemItemParecerDS_26_Tfcontagemitemparecer_usuariopessoacod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV86WWContagemItemParecerDS_27_Tfcontagemitemparecer_usuariopessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV87WWContagemItemParecerDS_28_Tfcontagemitemparecer_usuariopessoanom_sel",SqlDbType.Char,100,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00IF2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00IF2,100,0,true,false )
             ,new CursorDef("P00IF3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00IF3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDateTime(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                ((String[]) buf[7])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(4) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((String[]) buf[8])[0] = rslt.getLongVarchar(7) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[44]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[45]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[48]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[44]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[45]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[48]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwcontagemitemparecerfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwcontagemitemparecerfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwcontagemitemparecerfilterdata") )
          {
             return  ;
          }
          getwwcontagemitemparecerfilterdata worker = new getwwcontagemitemparecerfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
