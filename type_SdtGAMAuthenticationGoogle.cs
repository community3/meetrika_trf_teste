/*
               File: type_SdtGAMAuthenticationGoogle
        Description: GAMAuthenticationGoogle
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:29:42.77
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMAuthenticationGoogle : GxUserType, IGxExternalObject
   {
      public SdtGAMAuthenticationGoogle( )
      {
         initialize();
      }

      public SdtGAMAuthenticationGoogle( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMAuthenticationGoogle_externalReference == null )
         {
            GAMAuthenticationGoogle_externalReference = new Artech.Security.GAMAuthenticationGoogle(context);
         }
         returntostring = "";
         returntostring = (String)(GAMAuthenticationGoogle_externalReference.ToString());
         return returntostring ;
      }

      public String gxTpr_Clientid
      {
         get {
            if ( GAMAuthenticationGoogle_externalReference == null )
            {
               GAMAuthenticationGoogle_externalReference = new Artech.Security.GAMAuthenticationGoogle(context);
            }
            return GAMAuthenticationGoogle_externalReference.ClientId ;
         }

         set {
            if ( GAMAuthenticationGoogle_externalReference == null )
            {
               GAMAuthenticationGoogle_externalReference = new Artech.Security.GAMAuthenticationGoogle(context);
            }
            GAMAuthenticationGoogle_externalReference.ClientId = value;
         }

      }

      public String gxTpr_Clientsecret
      {
         get {
            if ( GAMAuthenticationGoogle_externalReference == null )
            {
               GAMAuthenticationGoogle_externalReference = new Artech.Security.GAMAuthenticationGoogle(context);
            }
            return GAMAuthenticationGoogle_externalReference.ClientSecret ;
         }

         set {
            if ( GAMAuthenticationGoogle_externalReference == null )
            {
               GAMAuthenticationGoogle_externalReference = new Artech.Security.GAMAuthenticationGoogle(context);
            }
            GAMAuthenticationGoogle_externalReference.ClientSecret = value;
         }

      }

      public String gxTpr_Siteurl
      {
         get {
            if ( GAMAuthenticationGoogle_externalReference == null )
            {
               GAMAuthenticationGoogle_externalReference = new Artech.Security.GAMAuthenticationGoogle(context);
            }
            return GAMAuthenticationGoogle_externalReference.SiteURL ;
         }

         set {
            if ( GAMAuthenticationGoogle_externalReference == null )
            {
               GAMAuthenticationGoogle_externalReference = new Artech.Security.GAMAuthenticationGoogle(context);
            }
            GAMAuthenticationGoogle_externalReference.SiteURL = value;
         }

      }

      public String gxTpr_Additionalscope
      {
         get {
            if ( GAMAuthenticationGoogle_externalReference == null )
            {
               GAMAuthenticationGoogle_externalReference = new Artech.Security.GAMAuthenticationGoogle(context);
            }
            return GAMAuthenticationGoogle_externalReference.AdditionalScope ;
         }

         set {
            if ( GAMAuthenticationGoogle_externalReference == null )
            {
               GAMAuthenticationGoogle_externalReference = new Artech.Security.GAMAuthenticationGoogle(context);
            }
            GAMAuthenticationGoogle_externalReference.AdditionalScope = value;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMAuthenticationGoogle_externalReference == null )
            {
               GAMAuthenticationGoogle_externalReference = new Artech.Security.GAMAuthenticationGoogle(context);
            }
            return GAMAuthenticationGoogle_externalReference ;
         }

         set {
            GAMAuthenticationGoogle_externalReference = (Artech.Security.GAMAuthenticationGoogle)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMAuthenticationGoogle GAMAuthenticationGoogle_externalReference=null ;
   }

}
