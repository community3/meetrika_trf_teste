/*
               File: PDFpermission
        Description: PDFpermission
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 21:56:35.9
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gxdomainpdfpermission
   {
      private static Hashtable domain = new Hashtable();
      static gxdomainpdfpermission ()
      {
         domain[(int)2052] = "Allow Printing";
         domain[(int)8] = "Allow Modify Contents";
         domain[(int)16] = "Allow Copy";
         domain[(int)32] = "Allow Modify Annotations";
         domain[(int)256] = "Allow Fill In";
         domain[(int)512] = "Allow Screen Readers";
         domain[(int)1024] = "Allow Assembly";
         domain[(int)4] = "Allow Degraded Printing";
      }

      public static string getDescription( IGxContext context ,
                                           int key )
      {
         return (string)domain[key] ;
      }

      public static GxSimpleCollection getValues( )
      {
         GxSimpleCollection value = new GxSimpleCollection();
         ArrayList aKeys = new ArrayList(domain.Keys);
         aKeys.Sort();
         foreach (int key in aKeys)
         {
            value.Add(key);
         }
         return value;
      }

   }

}
