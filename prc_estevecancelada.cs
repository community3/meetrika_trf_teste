/*
               File: PRC_EsteveCancelada
        Description: Esteve Cancelada
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 12:43:33.76
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_estevecancelada : GXProcedure
   {
      public prc_estevecancelada( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_estevecancelada( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_LogResponsavel_DemandaCod ,
                           out bool aP1_EsteveCancelada )
      {
         this.A892LogResponsavel_DemandaCod = aP0_LogResponsavel_DemandaCod;
         this.AV8EsteveCancelada = false ;
         initialize();
         executePrivate();
         aP0_LogResponsavel_DemandaCod=this.A892LogResponsavel_DemandaCod;
         aP1_EsteveCancelada=this.AV8EsteveCancelada;
      }

      public bool executeUdp( ref int aP0_LogResponsavel_DemandaCod )
      {
         this.A892LogResponsavel_DemandaCod = aP0_LogResponsavel_DemandaCod;
         this.AV8EsteveCancelada = false ;
         initialize();
         executePrivate();
         aP0_LogResponsavel_DemandaCod=this.A892LogResponsavel_DemandaCod;
         aP1_EsteveCancelada=this.AV8EsteveCancelada;
         return AV8EsteveCancelada ;
      }

      public void executeSubmit( ref int aP0_LogResponsavel_DemandaCod ,
                                 out bool aP1_EsteveCancelada )
      {
         prc_estevecancelada objprc_estevecancelada;
         objprc_estevecancelada = new prc_estevecancelada();
         objprc_estevecancelada.A892LogResponsavel_DemandaCod = aP0_LogResponsavel_DemandaCod;
         objprc_estevecancelada.AV8EsteveCancelada = false ;
         objprc_estevecancelada.context.SetSubmitInitialConfig(context);
         objprc_estevecancelada.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_estevecancelada);
         aP0_LogResponsavel_DemandaCod=this.A892LogResponsavel_DemandaCod;
         aP1_EsteveCancelada=this.AV8EsteveCancelada;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_estevecancelada)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00DH2 */
         pr_default.execute(0, new Object[] {n892LogResponsavel_DemandaCod, A892LogResponsavel_DemandaCod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1234LogResponsavel_NovoStatus = P00DH2_A1234LogResponsavel_NovoStatus[0];
            n1234LogResponsavel_NovoStatus = P00DH2_n1234LogResponsavel_NovoStatus[0];
            A1797LogResponsavel_Codigo = P00DH2_A1797LogResponsavel_Codigo[0];
            AV8EsteveCancelada = true;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00DH2_A892LogResponsavel_DemandaCod = new int[1] ;
         P00DH2_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         P00DH2_A1234LogResponsavel_NovoStatus = new String[] {""} ;
         P00DH2_n1234LogResponsavel_NovoStatus = new bool[] {false} ;
         P00DH2_A1797LogResponsavel_Codigo = new long[1] ;
         A1234LogResponsavel_NovoStatus = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_estevecancelada__default(),
            new Object[][] {
                new Object[] {
               P00DH2_A892LogResponsavel_DemandaCod, P00DH2_n892LogResponsavel_DemandaCod, P00DH2_A1234LogResponsavel_NovoStatus, P00DH2_n1234LogResponsavel_NovoStatus, P00DH2_A1797LogResponsavel_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A892LogResponsavel_DemandaCod ;
      private long A1797LogResponsavel_Codigo ;
      private String scmdbuf ;
      private String A1234LogResponsavel_NovoStatus ;
      private bool AV8EsteveCancelada ;
      private bool n892LogResponsavel_DemandaCod ;
      private bool n1234LogResponsavel_NovoStatus ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_LogResponsavel_DemandaCod ;
      private IDataStoreProvider pr_default ;
      private int[] P00DH2_A892LogResponsavel_DemandaCod ;
      private bool[] P00DH2_n892LogResponsavel_DemandaCod ;
      private String[] P00DH2_A1234LogResponsavel_NovoStatus ;
      private bool[] P00DH2_n1234LogResponsavel_NovoStatus ;
      private long[] P00DH2_A1797LogResponsavel_Codigo ;
      private bool aP1_EsteveCancelada ;
   }

   public class prc_estevecancelada__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00DH2 ;
          prmP00DH2 = new Object[] {
          new Object[] {"@LogResponsavel_DemandaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00DH2", "SELECT TOP 1 [LogResponsavel_DemandaCod], [LogResponsavel_NovoStatus], [LogResponsavel_Codigo] FROM [LogResponsavel] WITH (NOLOCK) WHERE ([LogResponsavel_DemandaCod] = @LogResponsavel_DemandaCod) AND ([LogResponsavel_NovoStatus] = 'X') ORDER BY [LogResponsavel_DemandaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00DH2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((long[]) buf[4])[0] = rslt.getLong(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
