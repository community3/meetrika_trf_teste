/*
               File: PRC_ValidaOSFM
        Description: Valida OS FM
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:51:35.30
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_validaosfm : GXProcedure
   {
      public prc_validaosfm( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_validaosfm( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_AreaTrabalho_Codigo ,
                           out bool aP1_AreaTrabalho_ValidaOSFM )
      {
         this.A5AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV8AreaTrabalho_ValidaOSFM = false ;
         initialize();
         executePrivate();
         aP0_AreaTrabalho_Codigo=this.A5AreaTrabalho_Codigo;
         aP1_AreaTrabalho_ValidaOSFM=this.AV8AreaTrabalho_ValidaOSFM;
      }

      public bool executeUdp( ref int aP0_AreaTrabalho_Codigo )
      {
         this.A5AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV8AreaTrabalho_ValidaOSFM = false ;
         initialize();
         executePrivate();
         aP0_AreaTrabalho_Codigo=this.A5AreaTrabalho_Codigo;
         aP1_AreaTrabalho_ValidaOSFM=this.AV8AreaTrabalho_ValidaOSFM;
         return AV8AreaTrabalho_ValidaOSFM ;
      }

      public void executeSubmit( ref int aP0_AreaTrabalho_Codigo ,
                                 out bool aP1_AreaTrabalho_ValidaOSFM )
      {
         prc_validaosfm objprc_validaosfm;
         objprc_validaosfm = new prc_validaosfm();
         objprc_validaosfm.A5AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         objprc_validaosfm.AV8AreaTrabalho_ValidaOSFM = false ;
         objprc_validaosfm.context.SetSubmitInitialConfig(context);
         objprc_validaosfm.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_validaosfm);
         aP0_AreaTrabalho_Codigo=this.A5AreaTrabalho_Codigo;
         aP1_AreaTrabalho_ValidaOSFM=this.AV8AreaTrabalho_ValidaOSFM;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_validaosfm)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P005U2 */
         pr_default.execute(0, new Object[] {A5AreaTrabalho_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A834AreaTrabalho_ValidaOSFM = P005U2_A834AreaTrabalho_ValidaOSFM[0];
            n834AreaTrabalho_ValidaOSFM = P005U2_n834AreaTrabalho_ValidaOSFM[0];
            AV8AreaTrabalho_ValidaOSFM = A834AreaTrabalho_ValidaOSFM;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P005U2_A5AreaTrabalho_Codigo = new int[1] ;
         P005U2_A834AreaTrabalho_ValidaOSFM = new bool[] {false} ;
         P005U2_n834AreaTrabalho_ValidaOSFM = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_validaosfm__default(),
            new Object[][] {
                new Object[] {
               P005U2_A5AreaTrabalho_Codigo, P005U2_A834AreaTrabalho_ValidaOSFM, P005U2_n834AreaTrabalho_ValidaOSFM
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A5AreaTrabalho_Codigo ;
      private String scmdbuf ;
      private bool AV8AreaTrabalho_ValidaOSFM ;
      private bool A834AreaTrabalho_ValidaOSFM ;
      private bool n834AreaTrabalho_ValidaOSFM ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_AreaTrabalho_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P005U2_A5AreaTrabalho_Codigo ;
      private bool[] P005U2_A834AreaTrabalho_ValidaOSFM ;
      private bool[] P005U2_n834AreaTrabalho_ValidaOSFM ;
      private bool aP1_AreaTrabalho_ValidaOSFM ;
   }

   public class prc_validaosfm__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP005U2 ;
          prmP005U2 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P005U2", "SELECT TOP 1 [AreaTrabalho_Codigo], [AreaTrabalho_ValidaOSFM] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo ORDER BY [AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005U2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
