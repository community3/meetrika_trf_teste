/*
               File: PRC_GetDadosPrioridade
        Description: Get Dados Prioridade
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:52:18.57
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_getdadosprioridade : GXProcedure
   {
      public prc_getdadosprioridade( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_getdadosprioridade( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContratoServicosPrioridade_Codigo ,
                           ref decimal aP1_PercPrazo ,
                           ref decimal aP2_PercValorB )
      {
         this.A1336ContratoServicosPrioridade_Codigo = aP0_ContratoServicosPrioridade_Codigo;
         this.AV8PercPrazo = aP1_PercPrazo;
         this.AV9PercValorB = aP2_PercValorB;
         initialize();
         executePrivate();
         aP0_ContratoServicosPrioridade_Codigo=this.A1336ContratoServicosPrioridade_Codigo;
         aP1_PercPrazo=this.AV8PercPrazo;
         aP2_PercValorB=this.AV9PercValorB;
      }

      public decimal executeUdp( ref int aP0_ContratoServicosPrioridade_Codigo ,
                                 ref decimal aP1_PercPrazo )
      {
         this.A1336ContratoServicosPrioridade_Codigo = aP0_ContratoServicosPrioridade_Codigo;
         this.AV8PercPrazo = aP1_PercPrazo;
         this.AV9PercValorB = aP2_PercValorB;
         initialize();
         executePrivate();
         aP0_ContratoServicosPrioridade_Codigo=this.A1336ContratoServicosPrioridade_Codigo;
         aP1_PercPrazo=this.AV8PercPrazo;
         aP2_PercValorB=this.AV9PercValorB;
         return AV9PercValorB ;
      }

      public void executeSubmit( ref int aP0_ContratoServicosPrioridade_Codigo ,
                                 ref decimal aP1_PercPrazo ,
                                 ref decimal aP2_PercValorB )
      {
         prc_getdadosprioridade objprc_getdadosprioridade;
         objprc_getdadosprioridade = new prc_getdadosprioridade();
         objprc_getdadosprioridade.A1336ContratoServicosPrioridade_Codigo = aP0_ContratoServicosPrioridade_Codigo;
         objprc_getdadosprioridade.AV8PercPrazo = aP1_PercPrazo;
         objprc_getdadosprioridade.AV9PercValorB = aP2_PercValorB;
         objprc_getdadosprioridade.context.SetSubmitInitialConfig(context);
         objprc_getdadosprioridade.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_getdadosprioridade);
         aP0_ContratoServicosPrioridade_Codigo=this.A1336ContratoServicosPrioridade_Codigo;
         aP1_PercPrazo=this.AV8PercPrazo;
         aP2_PercValorB=this.AV9PercValorB;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_getdadosprioridade)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00AF2 */
         pr_default.execute(0, new Object[] {A1336ContratoServicosPrioridade_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1339ContratoServicosPrioridade_PercPrazo = P00AF2_A1339ContratoServicosPrioridade_PercPrazo[0];
            n1339ContratoServicosPrioridade_PercPrazo = P00AF2_n1339ContratoServicosPrioridade_PercPrazo[0];
            A1338ContratoServicosPrioridade_PercValorB = P00AF2_A1338ContratoServicosPrioridade_PercValorB[0];
            n1338ContratoServicosPrioridade_PercValorB = P00AF2_n1338ContratoServicosPrioridade_PercValorB[0];
            AV8PercPrazo = A1339ContratoServicosPrioridade_PercPrazo;
            AV9PercValorB = A1338ContratoServicosPrioridade_PercValorB;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00AF2_A1336ContratoServicosPrioridade_Codigo = new int[1] ;
         P00AF2_A1339ContratoServicosPrioridade_PercPrazo = new decimal[1] ;
         P00AF2_n1339ContratoServicosPrioridade_PercPrazo = new bool[] {false} ;
         P00AF2_A1338ContratoServicosPrioridade_PercValorB = new decimal[1] ;
         P00AF2_n1338ContratoServicosPrioridade_PercValorB = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_getdadosprioridade__default(),
            new Object[][] {
                new Object[] {
               P00AF2_A1336ContratoServicosPrioridade_Codigo, P00AF2_A1339ContratoServicosPrioridade_PercPrazo, P00AF2_n1339ContratoServicosPrioridade_PercPrazo, P00AF2_A1338ContratoServicosPrioridade_PercValorB, P00AF2_n1338ContratoServicosPrioridade_PercValorB
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A1336ContratoServicosPrioridade_Codigo ;
      private decimal AV8PercPrazo ;
      private decimal AV9PercValorB ;
      private decimal A1339ContratoServicosPrioridade_PercPrazo ;
      private decimal A1338ContratoServicosPrioridade_PercValorB ;
      private String scmdbuf ;
      private bool n1339ContratoServicosPrioridade_PercPrazo ;
      private bool n1338ContratoServicosPrioridade_PercValorB ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContratoServicosPrioridade_Codigo ;
      private decimal aP1_PercPrazo ;
      private decimal aP2_PercValorB ;
      private IDataStoreProvider pr_default ;
      private int[] P00AF2_A1336ContratoServicosPrioridade_Codigo ;
      private decimal[] P00AF2_A1339ContratoServicosPrioridade_PercPrazo ;
      private bool[] P00AF2_n1339ContratoServicosPrioridade_PercPrazo ;
      private decimal[] P00AF2_A1338ContratoServicosPrioridade_PercValorB ;
      private bool[] P00AF2_n1338ContratoServicosPrioridade_PercValorB ;
   }

   public class prc_getdadosprioridade__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00AF2 ;
          prmP00AF2 = new Object[] {
          new Object[] {"@ContratoServicosPrioridade_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00AF2", "SELECT TOP 1 [ContratoServicosPrioridade_Codigo], [ContratoServicosPrioridade_PercPrazo], [ContratoServicosPrioridade_PercValorB] FROM [ContratoServicosPrioridade] WITH (NOLOCK) WHERE [ContratoServicosPrioridade_Codigo] = @ContratoServicosPrioridade_Codigo ORDER BY [ContratoServicosPrioridade_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00AF2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
