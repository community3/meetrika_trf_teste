/*
               File: GetWWCheckListFilterData
        Description: Get WWCheck List Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:54:53.26
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwchecklistfilterdata : GXProcedure
   {
      public getwwchecklistfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwchecklistfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV22DDOName = aP0_DDOName;
         this.AV20SearchTxt = aP1_SearchTxt;
         this.AV21SearchTxtTo = aP2_SearchTxtTo;
         this.AV26OptionsJson = "" ;
         this.AV29OptionsDescJson = "" ;
         this.AV31OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV26OptionsJson;
         aP4_OptionsDescJson=this.AV29OptionsDescJson;
         aP5_OptionIndexesJson=this.AV31OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV22DDOName = aP0_DDOName;
         this.AV20SearchTxt = aP1_SearchTxt;
         this.AV21SearchTxtTo = aP2_SearchTxtTo;
         this.AV26OptionsJson = "" ;
         this.AV29OptionsDescJson = "" ;
         this.AV31OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV26OptionsJson;
         aP4_OptionsDescJson=this.AV29OptionsDescJson;
         aP5_OptionIndexesJson=this.AV31OptionIndexesJson;
         return AV31OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwchecklistfilterdata objgetwwchecklistfilterdata;
         objgetwwchecklistfilterdata = new getwwchecklistfilterdata();
         objgetwwchecklistfilterdata.AV22DDOName = aP0_DDOName;
         objgetwwchecklistfilterdata.AV20SearchTxt = aP1_SearchTxt;
         objgetwwchecklistfilterdata.AV21SearchTxtTo = aP2_SearchTxtTo;
         objgetwwchecklistfilterdata.AV26OptionsJson = "" ;
         objgetwwchecklistfilterdata.AV29OptionsDescJson = "" ;
         objgetwwchecklistfilterdata.AV31OptionIndexesJson = "" ;
         objgetwwchecklistfilterdata.context.SetSubmitInitialConfig(context);
         objgetwwchecklistfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwchecklistfilterdata);
         aP3_OptionsJson=this.AV26OptionsJson;
         aP4_OptionsDescJson=this.AV29OptionsDescJson;
         aP5_OptionIndexesJson=this.AV31OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwchecklistfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV25Options = (IGxCollection)(new GxSimpleCollection());
         AV28OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV30OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV22DDOName), "DDO_CHECKLIST_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADCHECKLIST_DESCRICAOOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV26OptionsJson = AV25Options.ToJSonString(false);
         AV29OptionsDescJson = AV28OptionsDesc.ToJSonString(false);
         AV31OptionIndexesJson = AV30OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV33Session.Get("WWCheckListGridState"), "") == 0 )
         {
            AV35GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWCheckListGridState"), "");
         }
         else
         {
            AV35GridState.FromXml(AV33Session.Get("WWCheckListGridState"), "");
         }
         AV48GXV1 = 1;
         while ( AV48GXV1 <= AV35GridState.gxTpr_Filtervalues.Count )
         {
            AV36GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV35GridState.gxTpr_Filtervalues.Item(AV48GXV1));
            if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFCHECKLIST_CODIGO") == 0 )
            {
               AV10TFCheckList_Codigo = (int)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Value, "."));
               AV11TFCheckList_Codigo_To = (int)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFCHECK_CODIGO") == 0 )
            {
               AV12TFCheck_Codigo = (int)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Value, "."));
               AV13TFCheck_Codigo_To = (int)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFCHECKLIST_DESCRICAO") == 0 )
            {
               AV14TFCheckList_Descricao = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFCHECKLIST_DESCRICAO_SEL") == 0 )
            {
               AV15TFCheckList_Descricao_Sel = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFCHECKLIST_DE_SEL") == 0 )
            {
               AV16TFCheckList_De_SelsJson = AV36GridStateFilterValue.gxTpr_Value;
               AV17TFCheckList_De_Sels.FromJSonString(AV16TFCheckList_De_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFCHECKLIST_OBRIGATORIO_SEL") == 0 )
            {
               AV18TFCheckList_Obrigatorio_Sel = (short)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFCHECKLIST_ATIVO_SEL") == 0 )
            {
               AV19TFCheckList_Ativo_Sel = (short)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Value, "."));
            }
            AV48GXV1 = (int)(AV48GXV1+1);
         }
         if ( AV35GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV37GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV35GridState.gxTpr_Dynamicfilters.Item(1));
            AV38DynamicFiltersSelector1 = AV37GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV38DynamicFiltersSelector1, "CHECKLIST_DE") == 0 )
            {
               AV39CheckList_De1 = (short)(NumberUtil.Val( AV37GridStateDynamicFilter.gxTpr_Value, "."));
            }
            if ( AV35GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV40DynamicFiltersEnabled2 = true;
               AV37GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV35GridState.gxTpr_Dynamicfilters.Item(2));
               AV41DynamicFiltersSelector2 = AV37GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV41DynamicFiltersSelector2, "CHECKLIST_DE") == 0 )
               {
                  AV42CheckList_De2 = (short)(NumberUtil.Val( AV37GridStateDynamicFilter.gxTpr_Value, "."));
               }
               if ( AV35GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV43DynamicFiltersEnabled3 = true;
                  AV37GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV35GridState.gxTpr_Dynamicfilters.Item(3));
                  AV44DynamicFiltersSelector3 = AV37GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV44DynamicFiltersSelector3, "CHECKLIST_DE") == 0 )
                  {
                     AV45CheckList_De3 = (short)(NumberUtil.Val( AV37GridStateDynamicFilter.gxTpr_Value, "."));
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCHECKLIST_DESCRICAOOPTIONS' Routine */
         AV14TFCheckList_Descricao = AV20SearchTxt;
         AV15TFCheckList_Descricao_Sel = "";
         AV50WWCheckListDS_1_Dynamicfiltersselector1 = AV38DynamicFiltersSelector1;
         AV51WWCheckListDS_2_Checklist_de1 = AV39CheckList_De1;
         AV52WWCheckListDS_3_Dynamicfiltersenabled2 = AV40DynamicFiltersEnabled2;
         AV53WWCheckListDS_4_Dynamicfiltersselector2 = AV41DynamicFiltersSelector2;
         AV54WWCheckListDS_5_Checklist_de2 = AV42CheckList_De2;
         AV55WWCheckListDS_6_Dynamicfiltersenabled3 = AV43DynamicFiltersEnabled3;
         AV56WWCheckListDS_7_Dynamicfiltersselector3 = AV44DynamicFiltersSelector3;
         AV57WWCheckListDS_8_Checklist_de3 = AV45CheckList_De3;
         AV58WWCheckListDS_9_Tfchecklist_codigo = AV10TFCheckList_Codigo;
         AV59WWCheckListDS_10_Tfchecklist_codigo_to = AV11TFCheckList_Codigo_To;
         AV60WWCheckListDS_11_Tfcheck_codigo = AV12TFCheck_Codigo;
         AV61WWCheckListDS_12_Tfcheck_codigo_to = AV13TFCheck_Codigo_To;
         AV62WWCheckListDS_13_Tfchecklist_descricao = AV14TFCheckList_Descricao;
         AV63WWCheckListDS_14_Tfchecklist_descricao_sel = AV15TFCheckList_Descricao_Sel;
         AV64WWCheckListDS_15_Tfchecklist_de_sels = AV17TFCheckList_De_Sels;
         AV65WWCheckListDS_16_Tfchecklist_obrigatorio_sel = AV18TFCheckList_Obrigatorio_Sel;
         AV66WWCheckListDS_17_Tfchecklist_ativo_sel = AV19TFCheckList_Ativo_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A1230CheckList_De ,
                                              AV64WWCheckListDS_15_Tfchecklist_de_sels ,
                                              AV50WWCheckListDS_1_Dynamicfiltersselector1 ,
                                              AV51WWCheckListDS_2_Checklist_de1 ,
                                              AV52WWCheckListDS_3_Dynamicfiltersenabled2 ,
                                              AV53WWCheckListDS_4_Dynamicfiltersselector2 ,
                                              AV54WWCheckListDS_5_Checklist_de2 ,
                                              AV55WWCheckListDS_6_Dynamicfiltersenabled3 ,
                                              AV56WWCheckListDS_7_Dynamicfiltersselector3 ,
                                              AV57WWCheckListDS_8_Checklist_de3 ,
                                              AV58WWCheckListDS_9_Tfchecklist_codigo ,
                                              AV59WWCheckListDS_10_Tfchecklist_codigo_to ,
                                              AV60WWCheckListDS_11_Tfcheck_codigo ,
                                              AV61WWCheckListDS_12_Tfcheck_codigo_to ,
                                              AV63WWCheckListDS_14_Tfchecklist_descricao_sel ,
                                              AV62WWCheckListDS_13_Tfchecklist_descricao ,
                                              AV64WWCheckListDS_15_Tfchecklist_de_sels.Count ,
                                              AV65WWCheckListDS_16_Tfchecklist_obrigatorio_sel ,
                                              AV66WWCheckListDS_17_Tfchecklist_ativo_sel ,
                                              A758CheckList_Codigo ,
                                              A1839Check_Codigo ,
                                              A763CheckList_Descricao ,
                                              A1845CheckList_Obrigatorio ,
                                              A1151CheckList_Ativo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                              }
         });
         lV62WWCheckListDS_13_Tfchecklist_descricao = StringUtil.Concat( StringUtil.RTrim( AV62WWCheckListDS_13_Tfchecklist_descricao), "%", "");
         /* Using cursor P00T32 */
         pr_default.execute(0, new Object[] {AV51WWCheckListDS_2_Checklist_de1, AV54WWCheckListDS_5_Checklist_de2, AV57WWCheckListDS_8_Checklist_de3, AV58WWCheckListDS_9_Tfchecklist_codigo, AV59WWCheckListDS_10_Tfchecklist_codigo_to, AV60WWCheckListDS_11_Tfcheck_codigo, AV61WWCheckListDS_12_Tfcheck_codigo_to, lV62WWCheckListDS_13_Tfchecklist_descricao, AV63WWCheckListDS_14_Tfchecklist_descricao_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKT32 = false;
            A763CheckList_Descricao = P00T32_A763CheckList_Descricao[0];
            A1151CheckList_Ativo = P00T32_A1151CheckList_Ativo[0];
            A1845CheckList_Obrigatorio = P00T32_A1845CheckList_Obrigatorio[0];
            n1845CheckList_Obrigatorio = P00T32_n1845CheckList_Obrigatorio[0];
            A1839Check_Codigo = P00T32_A1839Check_Codigo[0];
            n1839Check_Codigo = P00T32_n1839Check_Codigo[0];
            A758CheckList_Codigo = P00T32_A758CheckList_Codigo[0];
            A1230CheckList_De = P00T32_A1230CheckList_De[0];
            n1230CheckList_De = P00T32_n1230CheckList_De[0];
            AV32count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00T32_A763CheckList_Descricao[0], A763CheckList_Descricao) == 0 ) )
            {
               BRKT32 = false;
               A758CheckList_Codigo = P00T32_A758CheckList_Codigo[0];
               AV32count = (long)(AV32count+1);
               BRKT32 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A763CheckList_Descricao)) )
            {
               AV24Option = A763CheckList_Descricao;
               AV25Options.Add(AV24Option, 0);
               AV30OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV32count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV25Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKT32 )
            {
               BRKT32 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV25Options = new GxSimpleCollection();
         AV28OptionsDesc = new GxSimpleCollection();
         AV30OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV33Session = context.GetSession();
         AV35GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV36GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV14TFCheckList_Descricao = "";
         AV15TFCheckList_Descricao_Sel = "";
         AV16TFCheckList_De_SelsJson = "";
         AV17TFCheckList_De_Sels = new GxSimpleCollection();
         AV37GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV38DynamicFiltersSelector1 = "";
         AV41DynamicFiltersSelector2 = "";
         AV44DynamicFiltersSelector3 = "";
         AV50WWCheckListDS_1_Dynamicfiltersselector1 = "";
         AV53WWCheckListDS_4_Dynamicfiltersselector2 = "";
         AV56WWCheckListDS_7_Dynamicfiltersselector3 = "";
         AV62WWCheckListDS_13_Tfchecklist_descricao = "";
         AV63WWCheckListDS_14_Tfchecklist_descricao_sel = "";
         AV64WWCheckListDS_15_Tfchecklist_de_sels = new GxSimpleCollection();
         scmdbuf = "";
         lV62WWCheckListDS_13_Tfchecklist_descricao = "";
         A763CheckList_Descricao = "";
         P00T32_A763CheckList_Descricao = new String[] {""} ;
         P00T32_A1151CheckList_Ativo = new bool[] {false} ;
         P00T32_A1845CheckList_Obrigatorio = new bool[] {false} ;
         P00T32_n1845CheckList_Obrigatorio = new bool[] {false} ;
         P00T32_A1839Check_Codigo = new int[1] ;
         P00T32_n1839Check_Codigo = new bool[] {false} ;
         P00T32_A758CheckList_Codigo = new int[1] ;
         P00T32_A1230CheckList_De = new short[1] ;
         P00T32_n1230CheckList_De = new bool[] {false} ;
         AV24Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwchecklistfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00T32_A763CheckList_Descricao, P00T32_A1151CheckList_Ativo, P00T32_A1845CheckList_Obrigatorio, P00T32_n1845CheckList_Obrigatorio, P00T32_A1839Check_Codigo, P00T32_n1839Check_Codigo, P00T32_A758CheckList_Codigo, P00T32_A1230CheckList_De, P00T32_n1230CheckList_De
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV18TFCheckList_Obrigatorio_Sel ;
      private short AV19TFCheckList_Ativo_Sel ;
      private short AV39CheckList_De1 ;
      private short AV42CheckList_De2 ;
      private short AV45CheckList_De3 ;
      private short AV51WWCheckListDS_2_Checklist_de1 ;
      private short AV54WWCheckListDS_5_Checklist_de2 ;
      private short AV57WWCheckListDS_8_Checklist_de3 ;
      private short AV65WWCheckListDS_16_Tfchecklist_obrigatorio_sel ;
      private short AV66WWCheckListDS_17_Tfchecklist_ativo_sel ;
      private short A1230CheckList_De ;
      private int AV48GXV1 ;
      private int AV10TFCheckList_Codigo ;
      private int AV11TFCheckList_Codigo_To ;
      private int AV12TFCheck_Codigo ;
      private int AV13TFCheck_Codigo_To ;
      private int AV58WWCheckListDS_9_Tfchecklist_codigo ;
      private int AV59WWCheckListDS_10_Tfchecklist_codigo_to ;
      private int AV60WWCheckListDS_11_Tfcheck_codigo ;
      private int AV61WWCheckListDS_12_Tfcheck_codigo_to ;
      private int AV64WWCheckListDS_15_Tfchecklist_de_sels_Count ;
      private int A758CheckList_Codigo ;
      private int A1839Check_Codigo ;
      private long AV32count ;
      private String scmdbuf ;
      private bool returnInSub ;
      private bool AV40DynamicFiltersEnabled2 ;
      private bool AV43DynamicFiltersEnabled3 ;
      private bool AV52WWCheckListDS_3_Dynamicfiltersenabled2 ;
      private bool AV55WWCheckListDS_6_Dynamicfiltersenabled3 ;
      private bool A1845CheckList_Obrigatorio ;
      private bool A1151CheckList_Ativo ;
      private bool BRKT32 ;
      private bool n1845CheckList_Obrigatorio ;
      private bool n1839Check_Codigo ;
      private bool n1230CheckList_De ;
      private String AV31OptionIndexesJson ;
      private String AV26OptionsJson ;
      private String AV29OptionsDescJson ;
      private String AV16TFCheckList_De_SelsJson ;
      private String A763CheckList_Descricao ;
      private String AV22DDOName ;
      private String AV20SearchTxt ;
      private String AV21SearchTxtTo ;
      private String AV14TFCheckList_Descricao ;
      private String AV15TFCheckList_Descricao_Sel ;
      private String AV38DynamicFiltersSelector1 ;
      private String AV41DynamicFiltersSelector2 ;
      private String AV44DynamicFiltersSelector3 ;
      private String AV50WWCheckListDS_1_Dynamicfiltersselector1 ;
      private String AV53WWCheckListDS_4_Dynamicfiltersselector2 ;
      private String AV56WWCheckListDS_7_Dynamicfiltersselector3 ;
      private String AV62WWCheckListDS_13_Tfchecklist_descricao ;
      private String AV63WWCheckListDS_14_Tfchecklist_descricao_sel ;
      private String lV62WWCheckListDS_13_Tfchecklist_descricao ;
      private String AV24Option ;
      private IGxSession AV33Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00T32_A763CheckList_Descricao ;
      private bool[] P00T32_A1151CheckList_Ativo ;
      private bool[] P00T32_A1845CheckList_Obrigatorio ;
      private bool[] P00T32_n1845CheckList_Obrigatorio ;
      private int[] P00T32_A1839Check_Codigo ;
      private bool[] P00T32_n1839Check_Codigo ;
      private int[] P00T32_A758CheckList_Codigo ;
      private short[] P00T32_A1230CheckList_De ;
      private bool[] P00T32_n1230CheckList_De ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV17TFCheckList_De_Sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV64WWCheckListDS_15_Tfchecklist_de_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV25Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV28OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV30OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV35GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV36GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV37GridStateDynamicFilter ;
   }

   public class getwwchecklistfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00T32( IGxContext context ,
                                             short A1230CheckList_De ,
                                             IGxCollection AV64WWCheckListDS_15_Tfchecklist_de_sels ,
                                             String AV50WWCheckListDS_1_Dynamicfiltersselector1 ,
                                             short AV51WWCheckListDS_2_Checklist_de1 ,
                                             bool AV52WWCheckListDS_3_Dynamicfiltersenabled2 ,
                                             String AV53WWCheckListDS_4_Dynamicfiltersselector2 ,
                                             short AV54WWCheckListDS_5_Checklist_de2 ,
                                             bool AV55WWCheckListDS_6_Dynamicfiltersenabled3 ,
                                             String AV56WWCheckListDS_7_Dynamicfiltersselector3 ,
                                             short AV57WWCheckListDS_8_Checklist_de3 ,
                                             int AV58WWCheckListDS_9_Tfchecklist_codigo ,
                                             int AV59WWCheckListDS_10_Tfchecklist_codigo_to ,
                                             int AV60WWCheckListDS_11_Tfcheck_codigo ,
                                             int AV61WWCheckListDS_12_Tfcheck_codigo_to ,
                                             String AV63WWCheckListDS_14_Tfchecklist_descricao_sel ,
                                             String AV62WWCheckListDS_13_Tfchecklist_descricao ,
                                             int AV64WWCheckListDS_15_Tfchecklist_de_sels_Count ,
                                             short AV65WWCheckListDS_16_Tfchecklist_obrigatorio_sel ,
                                             short AV66WWCheckListDS_17_Tfchecklist_ativo_sel ,
                                             int A758CheckList_Codigo ,
                                             int A1839Check_Codigo ,
                                             String A763CheckList_Descricao ,
                                             bool A1845CheckList_Obrigatorio ,
                                             bool A1151CheckList_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [9] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [CheckList_Descricao], [CheckList_Ativo], [CheckList_Obrigatorio], [Check_Codigo], [CheckList_Codigo], [CheckList_De] FROM [CheckList] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV50WWCheckListDS_1_Dynamicfiltersselector1, "CHECKLIST_DE") == 0 ) && ( ! (0==AV51WWCheckListDS_2_Checklist_de1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CheckList_De] = @AV51WWCheckListDS_2_Checklist_de1)";
            }
            else
            {
               sWhereString = sWhereString + " ([CheckList_De] = @AV51WWCheckListDS_2_Checklist_de1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( AV52WWCheckListDS_3_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV53WWCheckListDS_4_Dynamicfiltersselector2, "CHECKLIST_DE") == 0 ) && ( ! (0==AV54WWCheckListDS_5_Checklist_de2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CheckList_De] = @AV54WWCheckListDS_5_Checklist_de2)";
            }
            else
            {
               sWhereString = sWhereString + " ([CheckList_De] = @AV54WWCheckListDS_5_Checklist_de2)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV55WWCheckListDS_6_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV56WWCheckListDS_7_Dynamicfiltersselector3, "CHECKLIST_DE") == 0 ) && ( ! (0==AV57WWCheckListDS_8_Checklist_de3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CheckList_De] = @AV57WWCheckListDS_8_Checklist_de3)";
            }
            else
            {
               sWhereString = sWhereString + " ([CheckList_De] = @AV57WWCheckListDS_8_Checklist_de3)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ! (0==AV58WWCheckListDS_9_Tfchecklist_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CheckList_Codigo] >= @AV58WWCheckListDS_9_Tfchecklist_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([CheckList_Codigo] >= @AV58WWCheckListDS_9_Tfchecklist_codigo)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! (0==AV59WWCheckListDS_10_Tfchecklist_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CheckList_Codigo] <= @AV59WWCheckListDS_10_Tfchecklist_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([CheckList_Codigo] <= @AV59WWCheckListDS_10_Tfchecklist_codigo_to)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! (0==AV60WWCheckListDS_11_Tfcheck_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Check_Codigo] >= @AV60WWCheckListDS_11_Tfcheck_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Check_Codigo] >= @AV60WWCheckListDS_11_Tfcheck_codigo)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! (0==AV61WWCheckListDS_12_Tfcheck_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Check_Codigo] <= @AV61WWCheckListDS_12_Tfcheck_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([Check_Codigo] <= @AV61WWCheckListDS_12_Tfcheck_codigo_to)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV63WWCheckListDS_14_Tfchecklist_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWCheckListDS_13_Tfchecklist_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CheckList_Descricao] like @lV62WWCheckListDS_13_Tfchecklist_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " ([CheckList_Descricao] like @lV62WWCheckListDS_13_Tfchecklist_descricao)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWCheckListDS_14_Tfchecklist_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CheckList_Descricao] = @AV63WWCheckListDS_14_Tfchecklist_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([CheckList_Descricao] = @AV63WWCheckListDS_14_Tfchecklist_descricao_sel)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV64WWCheckListDS_15_Tfchecklist_de_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV64WWCheckListDS_15_Tfchecklist_de_sels, "[CheckList_De] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV64WWCheckListDS_15_Tfchecklist_de_sels, "[CheckList_De] IN (", ")") + ")";
            }
         }
         if ( AV65WWCheckListDS_16_Tfchecklist_obrigatorio_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CheckList_Obrigatorio] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " ([CheckList_Obrigatorio] = 1)";
            }
         }
         if ( AV65WWCheckListDS_16_Tfchecklist_obrigatorio_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CheckList_Obrigatorio] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " ([CheckList_Obrigatorio] = 0)";
            }
         }
         if ( AV66WWCheckListDS_17_Tfchecklist_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CheckList_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " ([CheckList_Ativo] = 1)";
            }
         }
         if ( AV66WWCheckListDS_17_Tfchecklist_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CheckList_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " ([CheckList_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [CheckList_Descricao]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00T32(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (short)dynConstraints[17] , (short)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (bool)dynConstraints[22] , (bool)dynConstraints[23] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00T32 ;
          prmP00T32 = new Object[] {
          new Object[] {"@AV51WWCheckListDS_2_Checklist_de1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV54WWCheckListDS_5_Checklist_de2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV57WWCheckListDS_8_Checklist_de3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV58WWCheckListDS_9_Tfchecklist_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV59WWCheckListDS_10_Tfchecklist_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV60WWCheckListDS_11_Tfcheck_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV61WWCheckListDS_12_Tfcheck_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV62WWCheckListDS_13_Tfchecklist_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV63WWCheckListDS_14_Tfchecklist_descricao_sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00T32", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00T32,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((short[]) buf[7])[0] = rslt.getShort(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[9]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[10]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[11]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[12]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwchecklistfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwchecklistfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwchecklistfilterdata") )
          {
             return  ;
          }
          getwwchecklistfilterdata worker = new getwwchecklistfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
