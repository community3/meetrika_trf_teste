/*
               File: ContagemResultadoChckLstLogWC
        Description: Contagem Resultado Chck Lst Log WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 5:19:14.5
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemresultadochcklstlogwc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contagemresultadochcklstlogwc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contagemresultadochcklstlogwc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultadoChckLstLog_OSCodigo )
      {
         this.AV35ContagemResultadoChckLstLog_OSCodigo = aP0_ContagemResultadoChckLstLog_OSCodigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV35ContagemResultadoChckLstLog_OSCodigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35ContagemResultadoChckLstLog_OSCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35ContagemResultadoChckLstLog_OSCodigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV35ContagemResultadoChckLstLog_OSCodigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_8 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_8_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_8_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
                  AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
                  AV22TFContagemResultadoChckLstLog_DataHora = context.localUtil.ParseDTimeParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFContagemResultadoChckLstLog_DataHora", context.localUtil.TToC( AV22TFContagemResultadoChckLstLog_DataHora, 8, 5, 0, 3, "/", ":", " "));
                  AV23TFContagemResultadoChckLstLog_DataHora_To = context.localUtil.ParseDTimeParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFContagemResultadoChckLstLog_DataHora_To", context.localUtil.TToC( AV23TFContagemResultadoChckLstLog_DataHora_To, 8, 5, 0, 3, "/", ":", " "));
                  AV28TFContagemResultadoChckLstLog_UsuarioNome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28TFContagemResultadoChckLstLog_UsuarioNome", AV28TFContagemResultadoChckLstLog_UsuarioNome);
                  AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel", AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel);
                  AV35ContagemResultadoChckLstLog_OSCodigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35ContagemResultadoChckLstLog_OSCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35ContagemResultadoChckLstLog_OSCodigo), 6, 0)));
                  AV26ddo_ContagemResultadoChckLstLog_DataHoraTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26ddo_ContagemResultadoChckLstLog_DataHoraTitleControlIdToReplace", AV26ddo_ContagemResultadoChckLstLog_DataHoraTitleControlIdToReplace);
                  AV30ddo_ContagemResultadoChckLstLog_UsuarioNomeTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30ddo_ContagemResultadoChckLstLog_UsuarioNomeTitleControlIdToReplace", AV30ddo_ContagemResultadoChckLstLog_UsuarioNomeTitleControlIdToReplace);
                  A602ContagemResultado_OSVinculada = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n602ContagemResultado_OSVinculada = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A602ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(A602ContagemResultado_OSVinculada), 6, 0)));
                  AV17ContagemResultadoChckLstLog_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A456ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
                  AV41Pgmname = GetNextPar( );
                  A811ContagemResultadoChckLstLog_ChckLstCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n811ContagemResultadoChckLstLog_ChckLstCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A811ContagemResultadoChckLstLog_ChckLstCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A811ContagemResultadoChckLstLog_ChckLstCod), 6, 0)));
                  A812ContagemResultadoChckLstLog_ChckLstDes = GetNextPar( );
                  n812ContagemResultadoChckLstLog_ChckLstDes = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A812ContagemResultadoChckLstLog_ChckLstDes", A812ContagemResultadoChckLstLog_ChckLstDes);
                  A824ContagemResultadoChckLstLog_Etapa = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  n824ContagemResultadoChckLstLog_Etapa = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A824ContagemResultadoChckLstLog_Etapa", StringUtil.Str( (decimal)(A824ContagemResultadoChckLstLog_Etapa), 1, 0));
                  A1841Check_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1841Check_Nome", A1841Check_Nome);
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV22TFContagemResultadoChckLstLog_DataHora, AV23TFContagemResultadoChckLstLog_DataHora_To, AV28TFContagemResultadoChckLstLog_UsuarioNome, AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel, AV35ContagemResultadoChckLstLog_OSCodigo, AV26ddo_ContagemResultadoChckLstLog_DataHoraTitleControlIdToReplace, AV30ddo_ContagemResultadoChckLstLog_UsuarioNomeTitleControlIdToReplace, A602ContagemResultado_OSVinculada, AV17ContagemResultadoChckLstLog_Codigo, A456ContagemResultado_Codigo, AV41Pgmname, A811ContagemResultadoChckLstLog_ChckLstCod, A812ContagemResultadoChckLstLog_ChckLstDes, A824ContagemResultadoChckLstLog_Etapa, A1841Check_Nome, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAEN2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV41Pgmname = "ContagemResultadoChckLstLogWC";
               context.Gx_err = 0;
               edtavDescricao_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDescricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDescricao_Enabled), 5, 0)));
               WSEN2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contagem Resultado Chck Lst Log WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020625191420");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("Window/InNewWindowRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contagemresultadochcklstlogwc.aspx") + "?" + UrlEncode("" +AV35ContagemResultadoChckLstLog_OSCodigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA", context.localUtil.TToC( AV22TFContagemResultadoChckLstLog_DataHora, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO", context.localUtil.TToC( AV23TFContagemResultadoChckLstLog_DataHora_To, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME", StringUtil.RTrim( AV28TFContagemResultadoChckLstLog_UsuarioNome));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_SEL", StringUtil.RTrim( AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_8", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_8), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV33GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV34GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV31DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV31DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORATITLEFILTERDATA", AV21ContagemResultadoChckLstLog_DataHoraTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORATITLEFILTERDATA", AV21ContagemResultadoChckLstLog_DataHoraTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMETITLEFILTERDATA", AV27ContagemResultadoChckLstLog_UsuarioNomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMETITLEFILTERDATA", AV27ContagemResultadoChckLstLog_UsuarioNomeTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV35ContagemResultadoChckLstLog_OSCodigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV35ContagemResultadoChckLstLog_OSCodigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTAGEMRESULTADOCHCKLSTLOG_OSCODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35ContagemResultadoChckLstLog_OSCodigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_OSVINCULADA", StringUtil.LTrim( StringUtil.NToC( (decimal)(A602ContagemResultado_OSVinculada), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTAGEMRESULTADOCHCKLSTLOG_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17ContagemResultadoChckLstLog_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV41Pgmname));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A811ContagemResultadoChckLstLog_ChckLstCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTDES", A812ContagemResultadoChckLstLog_ChckLstDes);
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_ETAPA", StringUtil.LTrim( StringUtil.NToC( (decimal)(A824ContagemResultadoChckLstLog_Etapa), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CHECK_NOME", StringUtil.RTrim( A1841Check_Nome));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTAGEMRESULTADO_OSVINCULADA", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV19ContagemResultado_OSVinculada), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADOCHCKLSTLOG_OSCODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1853ContagemResultadoChckLstLog_OSCodigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"INNEWWINDOW_Target", StringUtil.RTrim( Innewwindow_Target));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Caption", StringUtil.RTrim( Ddo_contagemresultadochcklstlog_datahora_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Tooltip", StringUtil.RTrim( Ddo_contagemresultadochcklstlog_datahora_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Cls", StringUtil.RTrim( Ddo_contagemresultadochcklstlog_datahora_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultadochcklstlog_datahora_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Filteredtextto_set", StringUtil.RTrim( Ddo_contagemresultadochcklstlog_datahora_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultadochcklstlog_datahora_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultadochcklstlog_datahora_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultadochcklstlog_datahora_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultadochcklstlog_datahora_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultadochcklstlog_datahora_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultadochcklstlog_datahora_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Filtertype", StringUtil.RTrim( Ddo_contagemresultadochcklstlog_datahora_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultadochcklstlog_datahora_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultadochcklstlog_datahora_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Sortasc", StringUtil.RTrim( Ddo_contagemresultadochcklstlog_datahora_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Sortdsc", StringUtil.RTrim( Ddo_contagemresultadochcklstlog_datahora_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultadochcklstlog_datahora_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Rangefilterfrom", StringUtil.RTrim( Ddo_contagemresultadochcklstlog_datahora_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Rangefilterto", StringUtil.RTrim( Ddo_contagemresultadochcklstlog_datahora_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultadochcklstlog_datahora_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Caption", StringUtil.RTrim( Ddo_contagemresultadochcklstlog_usuarionome_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Tooltip", StringUtil.RTrim( Ddo_contagemresultadochcklstlog_usuarionome_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Cls", StringUtil.RTrim( Ddo_contagemresultadochcklstlog_usuarionome_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultadochcklstlog_usuarionome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Selectedvalue_set", StringUtil.RTrim( Ddo_contagemresultadochcklstlog_usuarionome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultadochcklstlog_usuarionome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultadochcklstlog_usuarionome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultadochcklstlog_usuarionome_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultadochcklstlog_usuarionome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultadochcklstlog_usuarionome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultadochcklstlog_usuarionome_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Filtertype", StringUtil.RTrim( Ddo_contagemresultadochcklstlog_usuarionome_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultadochcklstlog_usuarionome_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultadochcklstlog_usuarionome_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Datalisttype", StringUtil.RTrim( Ddo_contagemresultadochcklstlog_usuarionome_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Datalistproc", StringUtil.RTrim( Ddo_contagemresultadochcklstlog_usuarionome_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contagemresultadochcklstlog_usuarionome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Sortasc", StringUtil.RTrim( Ddo_contagemresultadochcklstlog_usuarionome_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Sortdsc", StringUtil.RTrim( Ddo_contagemresultadochcklstlog_usuarionome_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Loadingdata", StringUtil.RTrim( Ddo_contagemresultadochcklstlog_usuarionome_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultadochcklstlog_usuarionome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Noresultsfound", StringUtil.RTrim( Ddo_contagemresultadochcklstlog_usuarionome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultadochcklstlog_usuarionome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultadochcklstlog_datahora_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultadochcklstlog_datahora_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Filteredtextto_get", StringUtil.RTrim( Ddo_contagemresultadochcklstlog_datahora_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultadochcklstlog_usuarionome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultadochcklstlog_usuarionome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Selectedvalue_get", StringUtil.RTrim( Ddo_contagemresultadochcklstlog_usuarionome_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormEN2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contagemresultadochcklstlogwc.js", "?202062519155");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContagemResultadoChckLstLogWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contagem Resultado Chck Lst Log WC" ;
      }

      protected void WBEN0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contagemresultadochcklstlogwc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("Window/InNewWindowRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_EN2( true) ;
         }
         else
         {
            wb_table1_2_EN2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_EN2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoChckLstLog_OSCodigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1853ContagemResultadoChckLstLog_OSCodigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1853ContagemResultadoChckLstLog_OSCodigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoChckLstLog_OSCodigo_Jsonclick, 0, "Attribute", "", "", "", edtContagemResultadoChckLstLog_OSCodigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoChckLstLogWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrderedby_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV13OrderedBy), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,24);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrderedby_Jsonclick, 0, "Attribute", "", "", "", edtavOrderedby_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoChckLstLogWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,25);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_ContagemResultadoChckLstLogWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontagemresultadochcklstlog_datahora_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadochcklstlog_datahora_Internalname, context.localUtil.TToC( AV22TFContagemResultadoChckLstLog_DataHora, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV22TFContagemResultadoChckLstLog_DataHora, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,26);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadochcklstlog_datahora_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadochcklstlog_datahora_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoChckLstLogWC.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontagemresultadochcklstlog_datahora_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontagemresultadochcklstlog_datahora_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContagemResultadoChckLstLogWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontagemresultadochcklstlog_datahora_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadochcklstlog_datahora_to_Internalname, context.localUtil.TToC( AV23TFContagemResultadoChckLstLog_DataHora_To, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV23TFContagemResultadoChckLstLog_DataHora_To, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,27);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadochcklstlog_datahora_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadochcklstlog_datahora_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoChckLstLogWC.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontagemresultadochcklstlog_datahora_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontagemresultadochcklstlog_datahora_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContagemResultadoChckLstLogWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_contagemresultadochcklstlog_datahoraauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contagemresultadochcklstlog_datahoraauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contagemresultadochcklstlog_datahoraauxdate_Internalname, context.localUtil.Format(AV24DDO_ContagemResultadoChckLstLog_DataHoraAuxDate, "99/99/99"), context.localUtil.Format( AV24DDO_ContagemResultadoChckLstLog_DataHoraAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,29);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contagemresultadochcklstlog_datahoraauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoChckLstLogWC.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contagemresultadochcklstlog_datahoraauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContagemResultadoChckLstLogWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contagemresultadochcklstlog_datahoraauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contagemresultadochcklstlog_datahoraauxdateto_Internalname, context.localUtil.Format(AV25DDO_ContagemResultadoChckLstLog_DataHoraAuxDateTo, "99/99/99"), context.localUtil.Format( AV25DDO_ContagemResultadoChckLstLog_DataHoraAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,30);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contagemresultadochcklstlog_datahoraauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoChckLstLogWC.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contagemresultadochcklstlog_datahoraauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContagemResultadoChckLstLogWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadochcklstlog_usuarionome_Internalname, StringUtil.RTrim( AV28TFContagemResultadoChckLstLog_UsuarioNome), StringUtil.RTrim( context.localUtil.Format( AV28TFContagemResultadoChckLstLog_UsuarioNome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,31);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadochcklstlog_usuarionome_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontagemresultadochcklstlog_usuarionome_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContagemResultadoChckLstLogWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadochcklstlog_usuarionome_sel_Internalname, StringUtil.RTrim( AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel), StringUtil.RTrim( context.localUtil.Format( AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,32);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadochcklstlog_usuarionome_sel_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontagemresultadochcklstlog_usuarionome_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContagemResultadoChckLstLogWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultadochcklstlog_datahoratitlecontrolidtoreplace_Internalname, AV26ddo_ContagemResultadoChckLstLog_DataHoraTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,34);\"", 0, edtavDdo_contagemresultadochcklstlog_datahoratitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContagemResultadoChckLstLogWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultadochcklstlog_usuarionometitlecontrolidtoreplace_Internalname, AV30ddo_ContagemResultadoChckLstLog_UsuarioNomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,36);\"", 0, edtavDdo_contagemresultadochcklstlog_usuarionometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContagemResultadoChckLstLogWC.htm");
         }
         wbLoad = true;
      }

      protected void STARTEN2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contagem Resultado Chck Lst Log WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPEN0( ) ;
            }
         }
      }

      protected void WSEN2( )
      {
         STARTEN2( ) ;
         EVTEN2( ) ;
      }

      protected void EVTEN2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPEN0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPEN0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11EN2 */
                                    E11EN2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPEN0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12EN2 */
                                    E12EN2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPEN0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13EN2 */
                                    E13EN2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPEN0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavDescricao_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPEN0( ) ;
                              }
                              nGXsfl_8_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
                              SubsflControlProps_82( ) ;
                              A814ContagemResultadoChckLstLog_DataHora = context.localUtil.CToT( cgiGet( edtContagemResultadoChckLstLog_DataHora_Internalname), 0);
                              AV16Descricao = cgiGet( edtavDescricao_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDescricao_Internalname, AV16Descricao);
                              A817ContagemResultadoChckLstLog_UsuarioNome = StringUtil.Upper( cgiGet( edtContagemResultadoChckLstLog_UsuarioNome_Internalname));
                              n817ContagemResultadoChckLstLog_UsuarioNome = false;
                              AV18ViewChckLst = cgiGet( edtavViewchcklst_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavViewchcklst_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV18ViewChckLst)) ? AV40Viewchcklst_GXI : context.convertURL( context.PathToRelativeUrl( AV18ViewChckLst))));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavDescricao_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E14EN2 */
                                          E14EN2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavDescricao_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E15EN2 */
                                          E15EN2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavDescricao_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E16EN2 */
                                          E16EN2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontagemresultadochcklstlog_datahora Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA"), 0) != AV22TFContagemResultadoChckLstLog_DataHora )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontagemresultadochcklstlog_datahora_to Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO"), 0) != AV23TFContagemResultadoChckLstLog_DataHora_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontagemresultadochcklstlog_usuarionome Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME"), AV28TFContagemResultadoChckLstLog_UsuarioNome) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontagemresultadochcklstlog_usuarionome_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_SEL"), AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavDescricao_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPEN0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavDescricao_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEEN2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormEN2( ) ;
            }
         }
      }

      protected void PAEN2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_82( ) ;
         while ( nGXsfl_8_idx <= nRC_GXsfl_8 )
         {
            sendrow_82( ) ;
            nGXsfl_8_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_8_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_8_idx+1));
            sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
            SubsflControlProps_82( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       DateTime AV22TFContagemResultadoChckLstLog_DataHora ,
                                       DateTime AV23TFContagemResultadoChckLstLog_DataHora_To ,
                                       String AV28TFContagemResultadoChckLstLog_UsuarioNome ,
                                       String AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel ,
                                       int AV35ContagemResultadoChckLstLog_OSCodigo ,
                                       String AV26ddo_ContagemResultadoChckLstLog_DataHoraTitleControlIdToReplace ,
                                       String AV30ddo_ContagemResultadoChckLstLog_UsuarioNomeTitleControlIdToReplace ,
                                       int A602ContagemResultado_OSVinculada ,
                                       int AV17ContagemResultadoChckLstLog_Codigo ,
                                       int A456ContagemResultado_Codigo ,
                                       String AV41Pgmname ,
                                       int A811ContagemResultadoChckLstLog_ChckLstCod ,
                                       String A812ContagemResultadoChckLstLog_ChckLstDes ,
                                       short A824ContagemResultadoChckLstLog_Etapa ,
                                       String A1841Check_Nome ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFEN2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A814ContagemResultadoChckLstLog_DataHora, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA", context.localUtil.TToC( A814ContagemResultadoChckLstLog_DataHora, 10, 8, 0, 3, "/", ":", " "));
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFEN2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV41Pgmname = "ContagemResultadoChckLstLogWC";
         context.Gx_err = 0;
         edtavDescricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDescricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDescricao_Enabled), 5, 0)));
      }

      protected void RFEN2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 8;
         /* Execute user event: E15EN2 */
         E15EN2 ();
         nGXsfl_8_idx = 1;
         sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
         SubsflControlProps_82( ) ;
         nGXsfl_8_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_82( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV22TFContagemResultadoChckLstLog_DataHora ,
                                                 AV23TFContagemResultadoChckLstLog_DataHora_To ,
                                                 AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel ,
                                                 AV28TFContagemResultadoChckLstLog_UsuarioNome ,
                                                 A814ContagemResultadoChckLstLog_DataHora ,
                                                 A817ContagemResultadoChckLstLog_UsuarioNome ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A1853ContagemResultadoChckLstLog_OSCodigo ,
                                                 AV35ContagemResultadoChckLstLog_OSCodigo },
                                                 new int[] {
                                                 TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT,
                                                 TypeConstants.INT
                                                 }
            });
            lV28TFContagemResultadoChckLstLog_UsuarioNome = StringUtil.PadR( StringUtil.RTrim( AV28TFContagemResultadoChckLstLog_UsuarioNome), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28TFContagemResultadoChckLstLog_UsuarioNome", AV28TFContagemResultadoChckLstLog_UsuarioNome);
            /* Using cursor H00EN2 */
            pr_default.execute(0, new Object[] {AV35ContagemResultadoChckLstLog_OSCodigo, AV22TFContagemResultadoChckLstLog_DataHora, AV23TFContagemResultadoChckLstLog_DataHora_To, lV28TFContagemResultadoChckLstLog_UsuarioNome, AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_8_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A822ContagemResultadoChckLstLog_UsuarioCod = H00EN2_A822ContagemResultadoChckLstLog_UsuarioCod[0];
               A823ContagemResultadoChckLstLog_PessoaCod = H00EN2_A823ContagemResultadoChckLstLog_PessoaCod[0];
               n823ContagemResultadoChckLstLog_PessoaCod = H00EN2_n823ContagemResultadoChckLstLog_PessoaCod[0];
               A1839Check_Codigo = H00EN2_A1839Check_Codigo[0];
               n1839Check_Codigo = H00EN2_n1839Check_Codigo[0];
               A1841Check_Nome = H00EN2_A1841Check_Nome[0];
               A811ContagemResultadoChckLstLog_ChckLstCod = H00EN2_A811ContagemResultadoChckLstLog_ChckLstCod[0];
               n811ContagemResultadoChckLstLog_ChckLstCod = H00EN2_n811ContagemResultadoChckLstLog_ChckLstCod[0];
               A812ContagemResultadoChckLstLog_ChckLstDes = H00EN2_A812ContagemResultadoChckLstLog_ChckLstDes[0];
               n812ContagemResultadoChckLstLog_ChckLstDes = H00EN2_n812ContagemResultadoChckLstLog_ChckLstDes[0];
               A824ContagemResultadoChckLstLog_Etapa = H00EN2_A824ContagemResultadoChckLstLog_Etapa[0];
               n824ContagemResultadoChckLstLog_Etapa = H00EN2_n824ContagemResultadoChckLstLog_Etapa[0];
               A1853ContagemResultadoChckLstLog_OSCodigo = H00EN2_A1853ContagemResultadoChckLstLog_OSCodigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1853ContagemResultadoChckLstLog_OSCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1853ContagemResultadoChckLstLog_OSCodigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADOCHCKLSTLOG_OSCODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1853ContagemResultadoChckLstLog_OSCodigo), "ZZZZZ9")));
               A817ContagemResultadoChckLstLog_UsuarioNome = H00EN2_A817ContagemResultadoChckLstLog_UsuarioNome[0];
               n817ContagemResultadoChckLstLog_UsuarioNome = H00EN2_n817ContagemResultadoChckLstLog_UsuarioNome[0];
               A814ContagemResultadoChckLstLog_DataHora = H00EN2_A814ContagemResultadoChckLstLog_DataHora[0];
               A823ContagemResultadoChckLstLog_PessoaCod = H00EN2_A823ContagemResultadoChckLstLog_PessoaCod[0];
               n823ContagemResultadoChckLstLog_PessoaCod = H00EN2_n823ContagemResultadoChckLstLog_PessoaCod[0];
               A817ContagemResultadoChckLstLog_UsuarioNome = H00EN2_A817ContagemResultadoChckLstLog_UsuarioNome[0];
               n817ContagemResultadoChckLstLog_UsuarioNome = H00EN2_n817ContagemResultadoChckLstLog_UsuarioNome[0];
               A1839Check_Codigo = H00EN2_A1839Check_Codigo[0];
               n1839Check_Codigo = H00EN2_n1839Check_Codigo[0];
               A812ContagemResultadoChckLstLog_ChckLstDes = H00EN2_A812ContagemResultadoChckLstLog_ChckLstDes[0];
               n812ContagemResultadoChckLstLog_ChckLstDes = H00EN2_n812ContagemResultadoChckLstLog_ChckLstDes[0];
               A1841Check_Nome = H00EN2_A1841Check_Nome[0];
               /* Execute user event: E16EN2 */
               E16EN2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 8;
            WBEN0( ) ;
         }
         nGXsfl_8_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV22TFContagemResultadoChckLstLog_DataHora ,
                                              AV23TFContagemResultadoChckLstLog_DataHora_To ,
                                              AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel ,
                                              AV28TFContagemResultadoChckLstLog_UsuarioNome ,
                                              A814ContagemResultadoChckLstLog_DataHora ,
                                              A817ContagemResultadoChckLstLog_UsuarioNome ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              A1853ContagemResultadoChckLstLog_OSCodigo ,
                                              AV35ContagemResultadoChckLstLog_OSCodigo },
                                              new int[] {
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.INT
                                              }
         });
         lV28TFContagemResultadoChckLstLog_UsuarioNome = StringUtil.PadR( StringUtil.RTrim( AV28TFContagemResultadoChckLstLog_UsuarioNome), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28TFContagemResultadoChckLstLog_UsuarioNome", AV28TFContagemResultadoChckLstLog_UsuarioNome);
         /* Using cursor H00EN3 */
         pr_default.execute(1, new Object[] {AV35ContagemResultadoChckLstLog_OSCodigo, AV22TFContagemResultadoChckLstLog_DataHora, AV23TFContagemResultadoChckLstLog_DataHora_To, lV28TFContagemResultadoChckLstLog_UsuarioNome, AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel});
         GRID_nRecordCount = H00EN3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV22TFContagemResultadoChckLstLog_DataHora, AV23TFContagemResultadoChckLstLog_DataHora_To, AV28TFContagemResultadoChckLstLog_UsuarioNome, AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel, AV35ContagemResultadoChckLstLog_OSCodigo, AV26ddo_ContagemResultadoChckLstLog_DataHoraTitleControlIdToReplace, AV30ddo_ContagemResultadoChckLstLog_UsuarioNomeTitleControlIdToReplace, A602ContagemResultado_OSVinculada, AV17ContagemResultadoChckLstLog_Codigo, A456ContagemResultado_Codigo, AV41Pgmname, A811ContagemResultadoChckLstLog_ChckLstCod, A812ContagemResultadoChckLstLog_ChckLstDes, A824ContagemResultadoChckLstLog_Etapa, A1841Check_Nome, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV22TFContagemResultadoChckLstLog_DataHora, AV23TFContagemResultadoChckLstLog_DataHora_To, AV28TFContagemResultadoChckLstLog_UsuarioNome, AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel, AV35ContagemResultadoChckLstLog_OSCodigo, AV26ddo_ContagemResultadoChckLstLog_DataHoraTitleControlIdToReplace, AV30ddo_ContagemResultadoChckLstLog_UsuarioNomeTitleControlIdToReplace, A602ContagemResultado_OSVinculada, AV17ContagemResultadoChckLstLog_Codigo, A456ContagemResultado_Codigo, AV41Pgmname, A811ContagemResultadoChckLstLog_ChckLstCod, A812ContagemResultadoChckLstLog_ChckLstDes, A824ContagemResultadoChckLstLog_Etapa, A1841Check_Nome, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV22TFContagemResultadoChckLstLog_DataHora, AV23TFContagemResultadoChckLstLog_DataHora_To, AV28TFContagemResultadoChckLstLog_UsuarioNome, AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel, AV35ContagemResultadoChckLstLog_OSCodigo, AV26ddo_ContagemResultadoChckLstLog_DataHoraTitleControlIdToReplace, AV30ddo_ContagemResultadoChckLstLog_UsuarioNomeTitleControlIdToReplace, A602ContagemResultado_OSVinculada, AV17ContagemResultadoChckLstLog_Codigo, A456ContagemResultado_Codigo, AV41Pgmname, A811ContagemResultadoChckLstLog_ChckLstCod, A812ContagemResultadoChckLstLog_ChckLstDes, A824ContagemResultadoChckLstLog_Etapa, A1841Check_Nome, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV22TFContagemResultadoChckLstLog_DataHora, AV23TFContagemResultadoChckLstLog_DataHora_To, AV28TFContagemResultadoChckLstLog_UsuarioNome, AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel, AV35ContagemResultadoChckLstLog_OSCodigo, AV26ddo_ContagemResultadoChckLstLog_DataHoraTitleControlIdToReplace, AV30ddo_ContagemResultadoChckLstLog_UsuarioNomeTitleControlIdToReplace, A602ContagemResultado_OSVinculada, AV17ContagemResultadoChckLstLog_Codigo, A456ContagemResultado_Codigo, AV41Pgmname, A811ContagemResultadoChckLstLog_ChckLstCod, A812ContagemResultadoChckLstLog_ChckLstDes, A824ContagemResultadoChckLstLog_Etapa, A1841Check_Nome, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV22TFContagemResultadoChckLstLog_DataHora, AV23TFContagemResultadoChckLstLog_DataHora_To, AV28TFContagemResultadoChckLstLog_UsuarioNome, AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel, AV35ContagemResultadoChckLstLog_OSCodigo, AV26ddo_ContagemResultadoChckLstLog_DataHoraTitleControlIdToReplace, AV30ddo_ContagemResultadoChckLstLog_UsuarioNomeTitleControlIdToReplace, A602ContagemResultado_OSVinculada, AV17ContagemResultadoChckLstLog_Codigo, A456ContagemResultado_Codigo, AV41Pgmname, A811ContagemResultadoChckLstLog_ChckLstCod, A812ContagemResultadoChckLstLog_ChckLstDes, A824ContagemResultadoChckLstLog_Etapa, A1841Check_Nome, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPEN0( )
      {
         /* Before Start, stand alone formulas. */
         AV41Pgmname = "ContagemResultadoChckLstLogWC";
         context.Gx_err = 0;
         edtavDescricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDescricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDescricao_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E14EN2 */
         E14EN2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV31DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORATITLEFILTERDATA"), AV21ContagemResultadoChckLstLog_DataHoraTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMETITLEFILTERDATA"), AV27ContagemResultadoChckLstLog_UsuarioNomeTitleFilterData);
            /* Read variables values. */
            A1853ContagemResultadoChckLstLog_OSCodigo = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoChckLstLog_OSCodigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1853ContagemResultadoChckLstLog_OSCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1853ContagemResultadoChckLstLog_OSCodigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADOCHCKLSTLOG_OSCODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1853ContagemResultadoChckLstLog_OSCodigo), "ZZZZZ9")));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vORDEREDBY");
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV13OrderedBy = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            else
            {
               AV13OrderedBy = (short)(context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontagemresultadochcklstlog_datahora_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFContagem Resultado Chck Lst Log_Data Hora"}), 1, "vTFCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA");
               GX_FocusControl = edtavTfcontagemresultadochcklstlog_datahora_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV22TFContagemResultadoChckLstLog_DataHora = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFContagemResultadoChckLstLog_DataHora", context.localUtil.TToC( AV22TFContagemResultadoChckLstLog_DataHora, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV22TFContagemResultadoChckLstLog_DataHora = context.localUtil.CToT( cgiGet( edtavTfcontagemresultadochcklstlog_datahora_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFContagemResultadoChckLstLog_DataHora", context.localUtil.TToC( AV22TFContagemResultadoChckLstLog_DataHora, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontagemresultadochcklstlog_datahora_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFContagem Resultado Chck Lst Log_Data Hora_To"}), 1, "vTFCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO");
               GX_FocusControl = edtavTfcontagemresultadochcklstlog_datahora_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV23TFContagemResultadoChckLstLog_DataHora_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFContagemResultadoChckLstLog_DataHora_To", context.localUtil.TToC( AV23TFContagemResultadoChckLstLog_DataHora_To, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV23TFContagemResultadoChckLstLog_DataHora_To = context.localUtil.CToT( cgiGet( edtavTfcontagemresultadochcklstlog_datahora_to_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFContagemResultadoChckLstLog_DataHora_To", context.localUtil.TToC( AV23TFContagemResultadoChckLstLog_DataHora_To, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contagemresultadochcklstlog_datahoraauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contagem Resultado Chck Lst Log_Data Hora Aux Date"}), 1, "vDDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAAUXDATE");
               GX_FocusControl = edtavDdo_contagemresultadochcklstlog_datahoraauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV24DDO_ContagemResultadoChckLstLog_DataHoraAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DDO_ContagemResultadoChckLstLog_DataHoraAuxDate", context.localUtil.Format(AV24DDO_ContagemResultadoChckLstLog_DataHoraAuxDate, "99/99/99"));
            }
            else
            {
               AV24DDO_ContagemResultadoChckLstLog_DataHoraAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contagemresultadochcklstlog_datahoraauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DDO_ContagemResultadoChckLstLog_DataHoraAuxDate", context.localUtil.Format(AV24DDO_ContagemResultadoChckLstLog_DataHoraAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contagemresultadochcklstlog_datahoraauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contagem Resultado Chck Lst Log_Data Hora Aux Date To"}), 1, "vDDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAAUXDATETO");
               GX_FocusControl = edtavDdo_contagemresultadochcklstlog_datahoraauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV25DDO_ContagemResultadoChckLstLog_DataHoraAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DDO_ContagemResultadoChckLstLog_DataHoraAuxDateTo", context.localUtil.Format(AV25DDO_ContagemResultadoChckLstLog_DataHoraAuxDateTo, "99/99/99"));
            }
            else
            {
               AV25DDO_ContagemResultadoChckLstLog_DataHoraAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contagemresultadochcklstlog_datahoraauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DDO_ContagemResultadoChckLstLog_DataHoraAuxDateTo", context.localUtil.Format(AV25DDO_ContagemResultadoChckLstLog_DataHoraAuxDateTo, "99/99/99"));
            }
            AV28TFContagemResultadoChckLstLog_UsuarioNome = StringUtil.Upper( cgiGet( edtavTfcontagemresultadochcklstlog_usuarionome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28TFContagemResultadoChckLstLog_UsuarioNome", AV28TFContagemResultadoChckLstLog_UsuarioNome);
            AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel = StringUtil.Upper( cgiGet( edtavTfcontagemresultadochcklstlog_usuarionome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel", AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel);
            AV26ddo_ContagemResultadoChckLstLog_DataHoraTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultadochcklstlog_datahoratitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26ddo_ContagemResultadoChckLstLog_DataHoraTitleControlIdToReplace", AV26ddo_ContagemResultadoChckLstLog_DataHoraTitleControlIdToReplace);
            AV30ddo_ContagemResultadoChckLstLog_UsuarioNomeTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultadochcklstlog_usuarionometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30ddo_ContagemResultadoChckLstLog_UsuarioNomeTitleControlIdToReplace", AV30ddo_ContagemResultadoChckLstLog_UsuarioNomeTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_8 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_8"), ",", "."));
            AV33GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV34GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV35ContagemResultadoChckLstLog_OSCodigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV35ContagemResultadoChckLstLog_OSCodigo"), ",", "."));
            AV19ContagemResultado_OSVinculada = (int)(context.localUtil.CToN( cgiGet( sPrefix+"vCONTAGEMRESULTADO_OSVINCULADA"), ",", "."));
            AV17ContagemResultadoChckLstLog_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"vCONTAGEMRESULTADOCHCKLSTLOG_CODIGO"), ",", "."));
            A824ContagemResultadoChckLstLog_Etapa = (short)(context.localUtil.CToN( cgiGet( sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_ETAPA"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Innewwindow_Target = cgiGet( sPrefix+"INNEWWINDOW_Target");
            Ddo_contagemresultadochcklstlog_datahora_Caption = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Caption");
            Ddo_contagemresultadochcklstlog_datahora_Tooltip = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Tooltip");
            Ddo_contagemresultadochcklstlog_datahora_Cls = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Cls");
            Ddo_contagemresultadochcklstlog_datahora_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Filteredtext_set");
            Ddo_contagemresultadochcklstlog_datahora_Filteredtextto_set = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Filteredtextto_set");
            Ddo_contagemresultadochcklstlog_datahora_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Dropdownoptionstype");
            Ddo_contagemresultadochcklstlog_datahora_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Titlecontrolidtoreplace");
            Ddo_contagemresultadochcklstlog_datahora_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Includesortasc"));
            Ddo_contagemresultadochcklstlog_datahora_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Includesortdsc"));
            Ddo_contagemresultadochcklstlog_datahora_Sortedstatus = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Sortedstatus");
            Ddo_contagemresultadochcklstlog_datahora_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Includefilter"));
            Ddo_contagemresultadochcklstlog_datahora_Filtertype = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Filtertype");
            Ddo_contagemresultadochcklstlog_datahora_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Filterisrange"));
            Ddo_contagemresultadochcklstlog_datahora_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Includedatalist"));
            Ddo_contagemresultadochcklstlog_datahora_Sortasc = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Sortasc");
            Ddo_contagemresultadochcklstlog_datahora_Sortdsc = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Sortdsc");
            Ddo_contagemresultadochcklstlog_datahora_Cleanfilter = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Cleanfilter");
            Ddo_contagemresultadochcklstlog_datahora_Rangefilterfrom = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Rangefilterfrom");
            Ddo_contagemresultadochcklstlog_datahora_Rangefilterto = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Rangefilterto");
            Ddo_contagemresultadochcklstlog_datahora_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Searchbuttontext");
            Ddo_contagemresultadochcklstlog_usuarionome_Caption = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Caption");
            Ddo_contagemresultadochcklstlog_usuarionome_Tooltip = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Tooltip");
            Ddo_contagemresultadochcklstlog_usuarionome_Cls = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Cls");
            Ddo_contagemresultadochcklstlog_usuarionome_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Filteredtext_set");
            Ddo_contagemresultadochcklstlog_usuarionome_Selectedvalue_set = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Selectedvalue_set");
            Ddo_contagemresultadochcklstlog_usuarionome_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Dropdownoptionstype");
            Ddo_contagemresultadochcklstlog_usuarionome_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Titlecontrolidtoreplace");
            Ddo_contagemresultadochcklstlog_usuarionome_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Includesortasc"));
            Ddo_contagemresultadochcklstlog_usuarionome_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Includesortdsc"));
            Ddo_contagemresultadochcklstlog_usuarionome_Sortedstatus = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Sortedstatus");
            Ddo_contagemresultadochcklstlog_usuarionome_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Includefilter"));
            Ddo_contagemresultadochcklstlog_usuarionome_Filtertype = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Filtertype");
            Ddo_contagemresultadochcklstlog_usuarionome_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Filterisrange"));
            Ddo_contagemresultadochcklstlog_usuarionome_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Includedatalist"));
            Ddo_contagemresultadochcklstlog_usuarionome_Datalisttype = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Datalisttype");
            Ddo_contagemresultadochcklstlog_usuarionome_Datalistproc = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Datalistproc");
            Ddo_contagemresultadochcklstlog_usuarionome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contagemresultadochcklstlog_usuarionome_Sortasc = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Sortasc");
            Ddo_contagemresultadochcklstlog_usuarionome_Sortdsc = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Sortdsc");
            Ddo_contagemresultadochcklstlog_usuarionome_Loadingdata = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Loadingdata");
            Ddo_contagemresultadochcklstlog_usuarionome_Cleanfilter = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Cleanfilter");
            Ddo_contagemresultadochcklstlog_usuarionome_Noresultsfound = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Noresultsfound");
            Ddo_contagemresultadochcklstlog_usuarionome_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contagemresultadochcklstlog_datahora_Activeeventkey = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Activeeventkey");
            Ddo_contagemresultadochcklstlog_datahora_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Filteredtext_get");
            Ddo_contagemresultadochcklstlog_datahora_Filteredtextto_get = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_Filteredtextto_get");
            Ddo_contagemresultadochcklstlog_usuarionome_Activeeventkey = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Activeeventkey");
            Ddo_contagemresultadochcklstlog_usuarionome_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Filteredtext_get");
            Ddo_contagemresultadochcklstlog_usuarionome_Selectedvalue_get = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA"), 0) != AV22TFContagemResultadoChckLstLog_DataHora )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO"), 0) != AV23TFContagemResultadoChckLstLog_DataHora_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME"), AV28TFContagemResultadoChckLstLog_UsuarioNome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_SEL"), AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E14EN2 */
         E14EN2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E14EN2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTfcontagemresultadochcklstlog_datahora_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontagemresultadochcklstlog_datahora_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadochcklstlog_datahora_Visible), 5, 0)));
         edtavTfcontagemresultadochcklstlog_datahora_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontagemresultadochcklstlog_datahora_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadochcklstlog_datahora_to_Visible), 5, 0)));
         edtavTfcontagemresultadochcklstlog_usuarionome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontagemresultadochcklstlog_usuarionome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadochcklstlog_usuarionome_Visible), 5, 0)));
         edtavTfcontagemresultadochcklstlog_usuarionome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontagemresultadochcklstlog_usuarionome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadochcklstlog_usuarionome_sel_Visible), 5, 0)));
         Ddo_contagemresultadochcklstlog_datahora_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultadoChckLstLog_DataHora";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadochcklstlog_datahora_Internalname, "TitleControlIdToReplace", Ddo_contagemresultadochcklstlog_datahora_Titlecontrolidtoreplace);
         AV26ddo_ContagemResultadoChckLstLog_DataHoraTitleControlIdToReplace = Ddo_contagemresultadochcklstlog_datahora_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26ddo_ContagemResultadoChckLstLog_DataHoraTitleControlIdToReplace", AV26ddo_ContagemResultadoChckLstLog_DataHoraTitleControlIdToReplace);
         edtavDdo_contagemresultadochcklstlog_datahoratitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contagemresultadochcklstlog_datahoratitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultadochcklstlog_datahoratitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultadochcklstlog_usuarionome_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultadoChckLstLog_UsuarioNome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadochcklstlog_usuarionome_Internalname, "TitleControlIdToReplace", Ddo_contagemresultadochcklstlog_usuarionome_Titlecontrolidtoreplace);
         AV30ddo_ContagemResultadoChckLstLog_UsuarioNomeTitleControlIdToReplace = Ddo_contagemresultadochcklstlog_usuarionome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30ddo_ContagemResultadoChckLstLog_UsuarioNomeTitleControlIdToReplace", AV30ddo_ContagemResultadoChckLstLog_UsuarioNomeTitleControlIdToReplace);
         edtavDdo_contagemresultadochcklstlog_usuarionometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contagemresultadochcklstlog_usuarionometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultadochcklstlog_usuarionometitlecontrolidtoreplace_Visible), 5, 0)));
         edtContagemResultadoChckLstLog_OSCodigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoChckLstLog_OSCodigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoChckLstLog_OSCodigo_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrderedby_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrderedby_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrderedby_Visible), 5, 0)));
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV31DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV31DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E15EN2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV21ContagemResultadoChckLstLog_DataHoraTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV27ContagemResultadoChckLstLog_UsuarioNomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContagemResultadoChckLstLog_DataHora_Titleformat = 2;
         edtContagemResultadoChckLstLog_DataHora_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Data", AV26ddo_ContagemResultadoChckLstLog_DataHoraTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoChckLstLog_DataHora_Internalname, "Title", edtContagemResultadoChckLstLog_DataHora_Title);
         edtContagemResultadoChckLstLog_UsuarioNome_Titleformat = 2;
         edtContagemResultadoChckLstLog_UsuarioNome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Usu�rio", AV30ddo_ContagemResultadoChckLstLog_UsuarioNomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoChckLstLog_UsuarioNome_Internalname, "Title", edtContagemResultadoChckLstLog_UsuarioNome_Title);
         AV33GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33GridCurrentPage), 10, 0)));
         AV34GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34GridPageCount), 10, 0)));
         /* Using cursor H00EN4 */
         pr_default.execute(2, new Object[] {AV17ContagemResultadoChckLstLog_Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A602ContagemResultado_OSVinculada = H00EN4_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = H00EN4_n602ContagemResultado_OSVinculada[0];
            A456ContagemResultado_Codigo = H00EN4_A456ContagemResultado_Codigo[0];
            AV19ContagemResultado_OSVinculada = A456ContagemResultado_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19ContagemResultado_OSVinculada), 6, 0)));
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(2);
         }
         pr_default.close(2);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV21ContagemResultadoChckLstLog_DataHoraTitleFilterData", AV21ContagemResultadoChckLstLog_DataHoraTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV27ContagemResultadoChckLstLog_UsuarioNomeTitleFilterData", AV27ContagemResultadoChckLstLog_UsuarioNomeTitleFilterData);
      }

      protected void E11EN2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV32PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV32PageToGo) ;
         }
      }

      protected void E12EN2( )
      {
         /* Ddo_contagemresultadochcklstlog_datahora_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultadochcklstlog_datahora_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultadochcklstlog_datahora_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadochcklstlog_datahora_Internalname, "SortedStatus", Ddo_contagemresultadochcklstlog_datahora_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadochcklstlog_datahora_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultadochcklstlog_datahora_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadochcklstlog_datahora_Internalname, "SortedStatus", Ddo_contagemresultadochcklstlog_datahora_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadochcklstlog_datahora_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV22TFContagemResultadoChckLstLog_DataHora = context.localUtil.CToT( Ddo_contagemresultadochcklstlog_datahora_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFContagemResultadoChckLstLog_DataHora", context.localUtil.TToC( AV22TFContagemResultadoChckLstLog_DataHora, 8, 5, 0, 3, "/", ":", " "));
            AV23TFContagemResultadoChckLstLog_DataHora_To = context.localUtil.CToT( Ddo_contagemresultadochcklstlog_datahora_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFContagemResultadoChckLstLog_DataHora_To", context.localUtil.TToC( AV23TFContagemResultadoChckLstLog_DataHora_To, 8, 5, 0, 3, "/", ":", " "));
            if ( ! (DateTime.MinValue==AV23TFContagemResultadoChckLstLog_DataHora_To) )
            {
               AV23TFContagemResultadoChckLstLog_DataHora_To = context.localUtil.YMDHMSToT( (short)(DateTimeUtil.Year( AV23TFContagemResultadoChckLstLog_DataHora_To)), (short)(DateTimeUtil.Month( AV23TFContagemResultadoChckLstLog_DataHora_To)), (short)(DateTimeUtil.Day( AV23TFContagemResultadoChckLstLog_DataHora_To)), 23, 59, 59);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFContagemResultadoChckLstLog_DataHora_To", context.localUtil.TToC( AV23TFContagemResultadoChckLstLog_DataHora_To, 8, 5, 0, 3, "/", ":", " "));
            }
            subgrid_firstpage( ) ;
         }
      }

      protected void E13EN2( )
      {
         /* Ddo_contagemresultadochcklstlog_usuarionome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultadochcklstlog_usuarionome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultadochcklstlog_usuarionome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadochcklstlog_usuarionome_Internalname, "SortedStatus", Ddo_contagemresultadochcklstlog_usuarionome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadochcklstlog_usuarionome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultadochcklstlog_usuarionome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadochcklstlog_usuarionome_Internalname, "SortedStatus", Ddo_contagemresultadochcklstlog_usuarionome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadochcklstlog_usuarionome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV28TFContagemResultadoChckLstLog_UsuarioNome = Ddo_contagemresultadochcklstlog_usuarionome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28TFContagemResultadoChckLstLog_UsuarioNome", AV28TFContagemResultadoChckLstLog_UsuarioNome);
            AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel = Ddo_contagemresultadochcklstlog_usuarionome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel", AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel);
            subgrid_firstpage( ) ;
         }
      }

      private void E16EN2( )
      {
         /* Grid_Load Routine */
         if ( A811ContagemResultadoChckLstLog_ChckLstCod > 0 )
         {
            AV16Descricao = A812ContagemResultadoChckLstLog_ChckLstDes + " N�O";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDescricao_Internalname, AV16Descricao);
         }
         else
         {
            if ( A824ContagemResultadoChckLstLog_Etapa == 0 )
            {
               AV16Descricao = "An�lise finalizado";
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDescricao_Internalname, AV16Descricao);
               /* Using cursor H00EN5 */
               pr_default.execute(3, new Object[] {n1839Check_Codigo, A1839Check_Codigo});
               while ( (pr_default.getStatus(3) != 101) )
               {
                  AV16Descricao = A1841Check_Nome + " finalizado";
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDescricao_Internalname, AV16Descricao);
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(3);
            }
            else
            {
               AV16Descricao = "Etapa " + StringUtil.Trim( StringUtil.Str( (decimal)(A824ContagemResultadoChckLstLog_Etapa), 1, 0)) + " finalizada";
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDescricao_Internalname, AV16Descricao);
            }
         }
         AV18ViewChckLst = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavViewchcklst_Internalname, AV18ViewChckLst);
         AV40Viewchcklst_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
         edtavViewchcklst_Tooltiptext = "Visualizar Check List";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 8;
         }
         sendrow_82( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_8_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(8, GridRow);
         }
      }

      protected void S152( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contagemresultadochcklstlog_datahora_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadochcklstlog_datahora_Internalname, "SortedStatus", Ddo_contagemresultadochcklstlog_datahora_Sortedstatus);
         Ddo_contagemresultadochcklstlog_usuarionome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadochcklstlog_usuarionome_Internalname, "SortedStatus", Ddo_contagemresultadochcklstlog_usuarionome_Sortedstatus);
      }

      protected void S132( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_contagemresultadochcklstlog_datahora_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadochcklstlog_datahora_Internalname, "SortedStatus", Ddo_contagemresultadochcklstlog_datahora_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_contagemresultadochcklstlog_usuarionome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadochcklstlog_usuarionome_Internalname, "SortedStatus", Ddo_contagemresultadochcklstlog_usuarionome_Sortedstatus);
         }
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV15Session.Get(AV41Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV41Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV15Session.Get(AV41Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV42GXV1 = 1;
         while ( AV42GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV42GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA") == 0 )
            {
               AV22TFContagemResultadoChckLstLog_DataHora = context.localUtil.CToT( AV12GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFContagemResultadoChckLstLog_DataHora", context.localUtil.TToC( AV22TFContagemResultadoChckLstLog_DataHora, 8, 5, 0, 3, "/", ":", " "));
               AV23TFContagemResultadoChckLstLog_DataHora_To = context.localUtil.CToT( AV12GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFContagemResultadoChckLstLog_DataHora_To", context.localUtil.TToC( AV23TFContagemResultadoChckLstLog_DataHora_To, 8, 5, 0, 3, "/", ":", " "));
               if ( ! (DateTime.MinValue==AV22TFContagemResultadoChckLstLog_DataHora) )
               {
                  AV24DDO_ContagemResultadoChckLstLog_DataHoraAuxDate = DateTimeUtil.ResetTime(AV22TFContagemResultadoChckLstLog_DataHora);
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DDO_ContagemResultadoChckLstLog_DataHoraAuxDate", context.localUtil.Format(AV24DDO_ContagemResultadoChckLstLog_DataHoraAuxDate, "99/99/99"));
                  Ddo_contagemresultadochcklstlog_datahora_Filteredtext_set = context.localUtil.DToC( AV24DDO_ContagemResultadoChckLstLog_DataHoraAuxDate, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadochcklstlog_datahora_Internalname, "FilteredText_set", Ddo_contagemresultadochcklstlog_datahora_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV23TFContagemResultadoChckLstLog_DataHora_To) )
               {
                  AV25DDO_ContagemResultadoChckLstLog_DataHoraAuxDateTo = DateTimeUtil.ResetTime(AV23TFContagemResultadoChckLstLog_DataHora_To);
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DDO_ContagemResultadoChckLstLog_DataHoraAuxDateTo", context.localUtil.Format(AV25DDO_ContagemResultadoChckLstLog_DataHoraAuxDateTo, "99/99/99"));
                  Ddo_contagemresultadochcklstlog_datahora_Filteredtextto_set = context.localUtil.DToC( AV25DDO_ContagemResultadoChckLstLog_DataHoraAuxDateTo, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadochcklstlog_datahora_Internalname, "FilteredTextTo_set", Ddo_contagemresultadochcklstlog_datahora_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME") == 0 )
            {
               AV28TFContagemResultadoChckLstLog_UsuarioNome = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28TFContagemResultadoChckLstLog_UsuarioNome", AV28TFContagemResultadoChckLstLog_UsuarioNome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28TFContagemResultadoChckLstLog_UsuarioNome)) )
               {
                  Ddo_contagemresultadochcklstlog_usuarionome_Filteredtext_set = AV28TFContagemResultadoChckLstLog_UsuarioNome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadochcklstlog_usuarionome_Internalname, "FilteredText_set", Ddo_contagemresultadochcklstlog_usuarionome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_SEL") == 0 )
            {
               AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel", AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel)) )
               {
                  Ddo_contagemresultadochcklstlog_usuarionome_Selectedvalue_set = AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadochcklstlog_usuarionome_Internalname, "SelectedValue_set", Ddo_contagemresultadochcklstlog_usuarionome_Selectedvalue_set);
               }
            }
            AV42GXV1 = (int)(AV42GXV1+1);
         }
      }

      protected void S142( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV15Session.Get(AV41Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (DateTime.MinValue==AV22TFContagemResultadoChckLstLog_DataHora) && (DateTime.MinValue==AV23TFContagemResultadoChckLstLog_DataHora_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA";
            AV12GridStateFilterValue.gxTpr_Value = context.localUtil.TToC( AV22TFContagemResultadoChckLstLog_DataHora, 8, 5, 0, 3, "/", ":", " ");
            AV12GridStateFilterValue.gxTpr_Valueto = context.localUtil.TToC( AV23TFContagemResultadoChckLstLog_DataHora_To, 8, 5, 0, 3, "/", ":", " ");
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28TFContagemResultadoChckLstLog_UsuarioNome)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME";
            AV12GridStateFilterValue.gxTpr_Value = AV28TFContagemResultadoChckLstLog_UsuarioNome;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV35ContagemResultadoChckLstLog_OSCodigo) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "PARM_&CONTAGEMRESULTADOCHCKLSTLOG_OSCODIGO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV35ContagemResultadoChckLstLog_OSCodigo), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV41Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV41Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "ContagemResultadoChckLstLog";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "ContagemResultadoChckLstLog_OSCodigo";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV35ContagemResultadoChckLstLog_OSCodigo), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV15Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_EN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table2_5_EN2( true) ;
         }
         else
         {
            wb_table2_5_EN2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_EN2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_18_EN2( true) ;
         }
         else
         {
            wb_table3_18_EN2( false) ;
         }
         return  ;
      }

      protected void wb_table3_18_EN2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_EN2e( true) ;
         }
         else
         {
            wb_table1_2_EN2e( false) ;
         }
      }

      protected void wb_table3_18_EN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTblusertable_Internalname, tblTblusertable_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"INNEWWINDOWContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_18_EN2e( true) ;
         }
         else
         {
            wb_table3_18_EN2e( false) ;
         }
      }

      protected void wb_table2_5_EN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"8\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoChckLstLog_DataHora_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoChckLstLog_DataHora_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoChckLstLog_DataHora_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(410), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Descri��o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoChckLstLog_UsuarioNome_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoChckLstLog_UsuarioNome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoChckLstLog_UsuarioNome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(20), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A814ContagemResultadoChckLstLog_DataHora, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoChckLstLog_DataHora_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoChckLstLog_DataHora_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", AV16Descricao);
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDescricao_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A817ContagemResultadoChckLstLog_UsuarioNome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoChckLstLog_UsuarioNome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoChckLstLog_UsuarioNome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV18ViewChckLst));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavViewchcklst_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 8 )
         {
            wbEnd = 0;
            nRC_GXsfl_8 = (short)(nGXsfl_8_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_EN2e( true) ;
         }
         else
         {
            wb_table2_5_EN2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV35ContagemResultadoChckLstLog_OSCodigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35ContagemResultadoChckLstLog_OSCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35ContagemResultadoChckLstLog_OSCodigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAEN2( ) ;
         WSEN2( ) ;
         WEEN2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV35ContagemResultadoChckLstLog_OSCodigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAEN2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contagemresultadochcklstlogwc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAEN2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV35ContagemResultadoChckLstLog_OSCodigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35ContagemResultadoChckLstLog_OSCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35ContagemResultadoChckLstLog_OSCodigo), 6, 0)));
         }
         wcpOAV35ContagemResultadoChckLstLog_OSCodigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV35ContagemResultadoChckLstLog_OSCodigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV35ContagemResultadoChckLstLog_OSCodigo != wcpOAV35ContagemResultadoChckLstLog_OSCodigo ) ) )
         {
            setjustcreated();
         }
         wcpOAV35ContagemResultadoChckLstLog_OSCodigo = AV35ContagemResultadoChckLstLog_OSCodigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV35ContagemResultadoChckLstLog_OSCodigo = cgiGet( sPrefix+"AV35ContagemResultadoChckLstLog_OSCodigo_CTRL");
         if ( StringUtil.Len( sCtrlAV35ContagemResultadoChckLstLog_OSCodigo) > 0 )
         {
            AV35ContagemResultadoChckLstLog_OSCodigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV35ContagemResultadoChckLstLog_OSCodigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35ContagemResultadoChckLstLog_OSCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35ContagemResultadoChckLstLog_OSCodigo), 6, 0)));
         }
         else
         {
            AV35ContagemResultadoChckLstLog_OSCodigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV35ContagemResultadoChckLstLog_OSCodigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAEN2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSEN2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSEN2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV35ContagemResultadoChckLstLog_OSCodigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35ContagemResultadoChckLstLog_OSCodigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV35ContagemResultadoChckLstLog_OSCodigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV35ContagemResultadoChckLstLog_OSCodigo_CTRL", StringUtil.RTrim( sCtrlAV35ContagemResultadoChckLstLog_OSCodigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEEN2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020625191764");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contagemresultadochcklstlogwc.js", "?2020625191764");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("Window/InNewWindowRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_82( )
      {
         edtContagemResultadoChckLstLog_DataHora_Internalname = sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_"+sGXsfl_8_idx;
         edtavDescricao_Internalname = sPrefix+"vDESCRICAO_"+sGXsfl_8_idx;
         edtContagemResultadoChckLstLog_UsuarioNome_Internalname = sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_"+sGXsfl_8_idx;
         edtavViewchcklst_Internalname = sPrefix+"vVIEWCHCKLST_"+sGXsfl_8_idx;
      }

      protected void SubsflControlProps_fel_82( )
      {
         edtContagemResultadoChckLstLog_DataHora_Internalname = sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_"+sGXsfl_8_fel_idx;
         edtavDescricao_Internalname = sPrefix+"vDESCRICAO_"+sGXsfl_8_fel_idx;
         edtContagemResultadoChckLstLog_UsuarioNome_Internalname = sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_"+sGXsfl_8_fel_idx;
         edtavViewchcklst_Internalname = sPrefix+"vVIEWCHCKLST_"+sGXsfl_8_fel_idx;
      }

      protected void sendrow_82( )
      {
         SubsflControlProps_82( ) ;
         WBEN0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_8_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_8_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_8_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoChckLstLog_DataHora_Internalname,context.localUtil.TToC( A814ContagemResultadoChckLstLog_DataHora, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A814ContagemResultadoChckLstLog_DataHora, "99/99/99 99:99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoChckLstLog_DataHora_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"DataHora",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavDescricao_Enabled!=0)&&(edtavDescricao_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 10,'"+sPrefix+"',false,'"+sGXsfl_8_idx+"',8)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavDescricao_Internalname,(String)AV16Descricao,(String)AV16Descricao,TempTags+((edtavDescricao_Enabled!=0)&&(edtavDescricao_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavDescricao_Enabled!=0)&&(edtavDescricao_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,10);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavDescricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavDescricao_Enabled,(short)0,(String)"text",(String)"",(short)410,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)0,(short)8,(short)1,(short)0,(short)-1,(bool)true,(String)"",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoChckLstLog_UsuarioNome_Internalname,StringUtil.RTrim( A817ContagemResultadoChckLstLog_UsuarioNome),StringUtil.RTrim( context.localUtil.Format( A817ContagemResultadoChckLstLog_UsuarioNome, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoChckLstLog_UsuarioNome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavViewchcklst_Enabled!=0)&&(edtavViewchcklst_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 12,'"+sPrefix+"',false,'',8)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV18ViewChckLst_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV18ViewChckLst))&&String.IsNullOrEmpty(StringUtil.RTrim( AV40Viewchcklst_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV18ViewChckLst)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavViewchcklst_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV18ViewChckLst)) ? AV40Viewchcklst_GXI : context.PathToRelativeUrl( AV18ViewChckLst)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavViewchcklst_Tooltiptext,(short)0,(short)1,(short)20,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)7,(String)edtavViewchcklst_Jsonclick,(String)"'"+sPrefix+"'"+",false,"+"'"+"e17en2_client"+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV18ViewChckLst_IsBlob,(bool)false});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, context.localUtil.Format( A814ContagemResultadoChckLstLog_DataHora, "99/99/99 99:99")));
            GridContainer.AddRow(GridRow);
            nGXsfl_8_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_8_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_8_idx+1));
            sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
            SubsflControlProps_82( ) ;
         }
         /* End function sendrow_82 */
      }

      protected void init_default_properties( )
      {
         edtContagemResultadoChckLstLog_DataHora_Internalname = sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA";
         edtavDescricao_Internalname = sPrefix+"vDESCRICAO";
         edtContagemResultadoChckLstLog_UsuarioNome_Internalname = sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME";
         edtavViewchcklst_Internalname = sPrefix+"vVIEWCHCKLST";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         Innewwindow_Internalname = sPrefix+"INNEWWINDOW";
         tblTblusertable_Internalname = sPrefix+"TBLUSERTABLE";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         edtContagemResultadoChckLstLog_OSCodigo_Internalname = sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_OSCODIGO";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         edtavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         edtavTfcontagemresultadochcklstlog_datahora_Internalname = sPrefix+"vTFCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA";
         edtavTfcontagemresultadochcklstlog_datahora_to_Internalname = sPrefix+"vTFCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO";
         edtavDdo_contagemresultadochcklstlog_datahoraauxdate_Internalname = sPrefix+"vDDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAAUXDATE";
         edtavDdo_contagemresultadochcklstlog_datahoraauxdateto_Internalname = sPrefix+"vDDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAAUXDATETO";
         divDdo_contagemresultadochcklstlog_datahoraauxdates_Internalname = sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAAUXDATES";
         edtavTfcontagemresultadochcklstlog_usuarionome_Internalname = sPrefix+"vTFCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME";
         edtavTfcontagemresultadochcklstlog_usuarionome_sel_Internalname = sPrefix+"vTFCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_SEL";
         Ddo_contagemresultadochcklstlog_datahora_Internalname = sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA";
         edtavDdo_contagemresultadochcklstlog_datahoratitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORATITLECONTROLIDTOREPLACE";
         Ddo_contagemresultadochcklstlog_usuarionome_Internalname = sPrefix+"DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME";
         edtavDdo_contagemresultadochcklstlog_usuarionometitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMETITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtavViewchcklst_Jsonclick = "";
         edtavViewchcklst_Visible = -1;
         edtavViewchcklst_Enabled = 1;
         edtContagemResultadoChckLstLog_UsuarioNome_Jsonclick = "";
         edtavDescricao_Jsonclick = "";
         edtavDescricao_Visible = -1;
         edtContagemResultadoChckLstLog_DataHora_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavViewchcklst_Tooltiptext = "Visualizar Check List";
         edtavDescricao_Enabled = 1;
         edtContagemResultadoChckLstLog_UsuarioNome_Titleformat = 0;
         edtContagemResultadoChckLstLog_DataHora_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtContagemResultadoChckLstLog_UsuarioNome_Title = "Usu�rio";
         edtContagemResultadoChckLstLog_DataHora_Title = "Data";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_contagemresultadochcklstlog_usuarionometitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultadochcklstlog_datahoratitlecontrolidtoreplace_Visible = 1;
         edtavTfcontagemresultadochcklstlog_usuarionome_sel_Jsonclick = "";
         edtavTfcontagemresultadochcklstlog_usuarionome_sel_Visible = 1;
         edtavTfcontagemresultadochcklstlog_usuarionome_Jsonclick = "";
         edtavTfcontagemresultadochcklstlog_usuarionome_Visible = 1;
         edtavDdo_contagemresultadochcklstlog_datahoraauxdateto_Jsonclick = "";
         edtavDdo_contagemresultadochcklstlog_datahoraauxdate_Jsonclick = "";
         edtavTfcontagemresultadochcklstlog_datahora_to_Jsonclick = "";
         edtavTfcontagemresultadochcklstlog_datahora_to_Visible = 1;
         edtavTfcontagemresultadochcklstlog_datahora_Jsonclick = "";
         edtavTfcontagemresultadochcklstlog_datahora_Visible = 1;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         edtavOrderedby_Jsonclick = "";
         edtavOrderedby_Visible = 1;
         edtContagemResultadoChckLstLog_OSCodigo_Jsonclick = "";
         edtContagemResultadoChckLstLog_OSCodigo_Visible = 1;
         Ddo_contagemresultadochcklstlog_usuarionome_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultadochcklstlog_usuarionome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contagemresultadochcklstlog_usuarionome_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultadochcklstlog_usuarionome_Loadingdata = "Carregando dados...";
         Ddo_contagemresultadochcklstlog_usuarionome_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultadochcklstlog_usuarionome_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultadochcklstlog_usuarionome_Datalistupdateminimumcharacters = 0;
         Ddo_contagemresultadochcklstlog_usuarionome_Datalistproc = "GetContagemResultadoChckLstLogWCFilterData";
         Ddo_contagemresultadochcklstlog_usuarionome_Datalisttype = "Dynamic";
         Ddo_contagemresultadochcklstlog_usuarionome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contagemresultadochcklstlog_usuarionome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contagemresultadochcklstlog_usuarionome_Filtertype = "Character";
         Ddo_contagemresultadochcklstlog_usuarionome_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultadochcklstlog_usuarionome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultadochcklstlog_usuarionome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultadochcklstlog_usuarionome_Titlecontrolidtoreplace = "";
         Ddo_contagemresultadochcklstlog_usuarionome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultadochcklstlog_usuarionome_Cls = "ColumnSettings";
         Ddo_contagemresultadochcklstlog_usuarionome_Tooltip = "Op��es";
         Ddo_contagemresultadochcklstlog_usuarionome_Caption = "";
         Ddo_contagemresultadochcklstlog_datahora_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultadochcklstlog_datahora_Rangefilterto = "At�";
         Ddo_contagemresultadochcklstlog_datahora_Rangefilterfrom = "Desde";
         Ddo_contagemresultadochcklstlog_datahora_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultadochcklstlog_datahora_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultadochcklstlog_datahora_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultadochcklstlog_datahora_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagemresultadochcklstlog_datahora_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagemresultadochcklstlog_datahora_Filtertype = "Date";
         Ddo_contagemresultadochcklstlog_datahora_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultadochcklstlog_datahora_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultadochcklstlog_datahora_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultadochcklstlog_datahora_Titlecontrolidtoreplace = "";
         Ddo_contagemresultadochcklstlog_datahora_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultadochcklstlog_datahora_Cls = "ColumnSettings";
         Ddo_contagemresultadochcklstlog_datahora_Tooltip = "Op��es";
         Ddo_contagemresultadochcklstlog_datahora_Caption = "";
         Innewwindow_Target = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A811ContagemResultadoChckLstLog_ChckLstCod',fld:'CONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTCOD',pic:'ZZZZZ9',nv:0},{av:'A812ContagemResultadoChckLstLog_ChckLstDes',fld:'CONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTDES',pic:'',nv:''},{av:'A824ContagemResultadoChckLstLog_Etapa',fld:'CONTAGEMRESULTADOCHCKLSTLOG_ETAPA',pic:'9',nv:0},{av:'A1841Check_Nome',fld:'CHECK_NOME',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'AV26ddo_ContagemResultadoChckLstLog_DataHoraTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV30ddo_ContagemResultadoChckLstLog_UsuarioNomeTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'AV17ContagemResultadoChckLstLog_Codigo',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV41Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV22TFContagemResultadoChckLstLog_DataHora',fld:'vTFCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV23TFContagemResultadoChckLstLog_DataHora_To',fld:'vTFCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV28TFContagemResultadoChckLstLog_UsuarioNome',fld:'vTFCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME',pic:'@!',nv:''},{av:'AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel',fld:'vTFCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_SEL',pic:'@!',nv:''},{av:'AV35ContagemResultadoChckLstLog_OSCodigo',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_OSCODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV21ContagemResultadoChckLstLog_DataHoraTitleFilterData',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORATITLEFILTERDATA',pic:'',nv:null},{av:'AV27ContagemResultadoChckLstLog_UsuarioNomeTitleFilterData',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMETITLEFILTERDATA',pic:'',nv:null},{av:'edtContagemResultadoChckLstLog_DataHora_Titleformat',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA',prop:'Titleformat'},{av:'edtContagemResultadoChckLstLog_DataHora_Title',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA',prop:'Title'},{av:'edtContagemResultadoChckLstLog_UsuarioNome_Titleformat',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME',prop:'Titleformat'},{av:'edtContagemResultadoChckLstLog_UsuarioNome_Title',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME',prop:'Title'},{av:'AV33GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV34GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV19ContagemResultado_OSVinculada',fld:'vCONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11EN2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV22TFContagemResultadoChckLstLog_DataHora',fld:'vTFCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV23TFContagemResultadoChckLstLog_DataHora_To',fld:'vTFCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV28TFContagemResultadoChckLstLog_UsuarioNome',fld:'vTFCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME',pic:'@!',nv:''},{av:'AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel',fld:'vTFCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_SEL',pic:'@!',nv:''},{av:'AV35ContagemResultadoChckLstLog_OSCodigo',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_OSCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV26ddo_ContagemResultadoChckLstLog_DataHoraTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV30ddo_ContagemResultadoChckLstLog_UsuarioNomeTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'AV17ContagemResultadoChckLstLog_Codigo',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV41Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A811ContagemResultadoChckLstLog_ChckLstCod',fld:'CONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTCOD',pic:'ZZZZZ9',nv:0},{av:'A812ContagemResultadoChckLstLog_ChckLstDes',fld:'CONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTDES',pic:'',nv:''},{av:'A824ContagemResultadoChckLstLog_Etapa',fld:'CONTAGEMRESULTADOCHCKLSTLOG_ETAPA',pic:'9',nv:0},{av:'A1841Check_Nome',fld:'CHECK_NOME',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA.ONOPTIONCLICKED","{handler:'E12EN2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV22TFContagemResultadoChckLstLog_DataHora',fld:'vTFCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV23TFContagemResultadoChckLstLog_DataHora_To',fld:'vTFCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV28TFContagemResultadoChckLstLog_UsuarioNome',fld:'vTFCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME',pic:'@!',nv:''},{av:'AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel',fld:'vTFCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_SEL',pic:'@!',nv:''},{av:'AV35ContagemResultadoChckLstLog_OSCodigo',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_OSCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV26ddo_ContagemResultadoChckLstLog_DataHoraTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV30ddo_ContagemResultadoChckLstLog_UsuarioNomeTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'AV17ContagemResultadoChckLstLog_Codigo',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV41Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A811ContagemResultadoChckLstLog_ChckLstCod',fld:'CONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTCOD',pic:'ZZZZZ9',nv:0},{av:'A812ContagemResultadoChckLstLog_ChckLstDes',fld:'CONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTDES',pic:'',nv:''},{av:'A824ContagemResultadoChckLstLog_Etapa',fld:'CONTAGEMRESULTADOCHCKLSTLOG_ETAPA',pic:'9',nv:0},{av:'A1841Check_Nome',fld:'CHECK_NOME',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_contagemresultadochcklstlog_datahora_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA',prop:'ActiveEventKey'},{av:'Ddo_contagemresultadochcklstlog_datahora_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA',prop:'FilteredText_get'},{av:'Ddo_contagemresultadochcklstlog_datahora_Filteredtextto_get',ctrl:'DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultadochcklstlog_datahora_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA',prop:'SortedStatus'},{av:'AV22TFContagemResultadoChckLstLog_DataHora',fld:'vTFCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV23TFContagemResultadoChckLstLog_DataHora_To',fld:'vTFCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_contagemresultadochcklstlog_usuarionome_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME.ONOPTIONCLICKED","{handler:'E13EN2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV22TFContagemResultadoChckLstLog_DataHora',fld:'vTFCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV23TFContagemResultadoChckLstLog_DataHora_To',fld:'vTFCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV28TFContagemResultadoChckLstLog_UsuarioNome',fld:'vTFCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME',pic:'@!',nv:''},{av:'AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel',fld:'vTFCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_SEL',pic:'@!',nv:''},{av:'AV35ContagemResultadoChckLstLog_OSCodigo',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_OSCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV26ddo_ContagemResultadoChckLstLog_DataHoraTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV30ddo_ContagemResultadoChckLstLog_UsuarioNomeTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'AV17ContagemResultadoChckLstLog_Codigo',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV41Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A811ContagemResultadoChckLstLog_ChckLstCod',fld:'CONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTCOD',pic:'ZZZZZ9',nv:0},{av:'A812ContagemResultadoChckLstLog_ChckLstDes',fld:'CONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTDES',pic:'',nv:''},{av:'A824ContagemResultadoChckLstLog_Etapa',fld:'CONTAGEMRESULTADOCHCKLSTLOG_ETAPA',pic:'9',nv:0},{av:'A1841Check_Nome',fld:'CHECK_NOME',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_contagemresultadochcklstlog_usuarionome_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME',prop:'ActiveEventKey'},{av:'Ddo_contagemresultadochcklstlog_usuarionome_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME',prop:'FilteredText_get'},{av:'Ddo_contagemresultadochcklstlog_usuarionome_Selectedvalue_get',ctrl:'DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultadochcklstlog_usuarionome_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME',prop:'SortedStatus'},{av:'AV28TFContagemResultadoChckLstLog_UsuarioNome',fld:'vTFCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME',pic:'@!',nv:''},{av:'AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel',fld:'vTFCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_SEL',pic:'@!',nv:''},{av:'Ddo_contagemresultadochcklstlog_datahora_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E16EN2',iparms:[{av:'A811ContagemResultadoChckLstLog_ChckLstCod',fld:'CONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTCOD',pic:'ZZZZZ9',nv:0},{av:'A812ContagemResultadoChckLstLog_ChckLstDes',fld:'CONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTDES',pic:'',nv:''},{av:'A824ContagemResultadoChckLstLog_Etapa',fld:'CONTAGEMRESULTADOCHCKLSTLOG_ETAPA',pic:'9',nv:0},{av:'A1841Check_Nome',fld:'CHECK_NOME',pic:'@!',nv:''}],oparms:[{av:'AV16Descricao',fld:'vDESCRICAO',pic:'',nv:''},{av:'AV18ViewChckLst',fld:'vVIEWCHCKLST',pic:'',nv:''},{av:'edtavViewchcklst_Tooltiptext',ctrl:'vVIEWCHCKLST',prop:'Tooltiptext'}]}");
         setEventMetadata("VVIEWCHCKLST.CLICK","{handler:'E17EN2',iparms:[{av:'A824ContagemResultadoChckLstLog_Etapa',fld:'CONTAGEMRESULTADOCHCKLSTLOG_ETAPA',pic:'9',nv:0},{av:'AV17ContagemResultadoChckLstLog_Codigo',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV19ContagemResultado_OSVinculada',fld:'vCONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0}],oparms:[{av:'Innewwindow_Target',ctrl:'INNEWWINDOW',prop:'Target'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_contagemresultadochcklstlog_datahora_Activeeventkey = "";
         Ddo_contagemresultadochcklstlog_datahora_Filteredtext_get = "";
         Ddo_contagemresultadochcklstlog_datahora_Filteredtextto_get = "";
         Ddo_contagemresultadochcklstlog_usuarionome_Activeeventkey = "";
         Ddo_contagemresultadochcklstlog_usuarionome_Filteredtext_get = "";
         Ddo_contagemresultadochcklstlog_usuarionome_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV22TFContagemResultadoChckLstLog_DataHora = (DateTime)(DateTime.MinValue);
         AV23TFContagemResultadoChckLstLog_DataHora_To = (DateTime)(DateTime.MinValue);
         AV28TFContagemResultadoChckLstLog_UsuarioNome = "";
         AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel = "";
         AV26ddo_ContagemResultadoChckLstLog_DataHoraTitleControlIdToReplace = "";
         AV30ddo_ContagemResultadoChckLstLog_UsuarioNomeTitleControlIdToReplace = "";
         AV41Pgmname = "";
         A812ContagemResultadoChckLstLog_ChckLstDes = "";
         A1841Check_Nome = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV31DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV21ContagemResultadoChckLstLog_DataHoraTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV27ContagemResultadoChckLstLog_UsuarioNomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_contagemresultadochcklstlog_datahora_Filteredtext_set = "";
         Ddo_contagemresultadochcklstlog_datahora_Filteredtextto_set = "";
         Ddo_contagemresultadochcklstlog_datahora_Sortedstatus = "";
         Ddo_contagemresultadochcklstlog_usuarionome_Filteredtext_set = "";
         Ddo_contagemresultadochcklstlog_usuarionome_Selectedvalue_set = "";
         Ddo_contagemresultadochcklstlog_usuarionome_Sortedstatus = "";
         GX_FocusControl = "";
         TempTags = "";
         AV24DDO_ContagemResultadoChckLstLog_DataHoraAuxDate = DateTime.MinValue;
         AV25DDO_ContagemResultadoChckLstLog_DataHoraAuxDateTo = DateTime.MinValue;
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         A814ContagemResultadoChckLstLog_DataHora = (DateTime)(DateTime.MinValue);
         AV16Descricao = "";
         A817ContagemResultadoChckLstLog_UsuarioNome = "";
         AV18ViewChckLst = "";
         AV40Viewchcklst_GXI = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV28TFContagemResultadoChckLstLog_UsuarioNome = "";
         H00EN2_A820ContagemResultadoChckLstLog_Codigo = new int[1] ;
         H00EN2_A822ContagemResultadoChckLstLog_UsuarioCod = new int[1] ;
         H00EN2_A823ContagemResultadoChckLstLog_PessoaCod = new int[1] ;
         H00EN2_n823ContagemResultadoChckLstLog_PessoaCod = new bool[] {false} ;
         H00EN2_A1839Check_Codigo = new int[1] ;
         H00EN2_n1839Check_Codigo = new bool[] {false} ;
         H00EN2_A1841Check_Nome = new String[] {""} ;
         H00EN2_A811ContagemResultadoChckLstLog_ChckLstCod = new int[1] ;
         H00EN2_n811ContagemResultadoChckLstLog_ChckLstCod = new bool[] {false} ;
         H00EN2_A812ContagemResultadoChckLstLog_ChckLstDes = new String[] {""} ;
         H00EN2_n812ContagemResultadoChckLstLog_ChckLstDes = new bool[] {false} ;
         H00EN2_A824ContagemResultadoChckLstLog_Etapa = new short[1] ;
         H00EN2_n824ContagemResultadoChckLstLog_Etapa = new bool[] {false} ;
         H00EN2_A1853ContagemResultadoChckLstLog_OSCodigo = new int[1] ;
         H00EN2_A817ContagemResultadoChckLstLog_UsuarioNome = new String[] {""} ;
         H00EN2_n817ContagemResultadoChckLstLog_UsuarioNome = new bool[] {false} ;
         H00EN2_A814ContagemResultadoChckLstLog_DataHora = new DateTime[] {DateTime.MinValue} ;
         H00EN3_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         H00EN4_A602ContagemResultado_OSVinculada = new int[1] ;
         H00EN4_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         H00EN4_A456ContagemResultado_Codigo = new int[1] ;
         H00EN5_A1839Check_Codigo = new int[1] ;
         H00EN5_n1839Check_Codigo = new bool[] {false} ;
         GridRow = new GXWebRow();
         AV15Session = context.GetSession();
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV35ContagemResultadoChckLstLog_OSCodigo = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemresultadochcklstlogwc__default(),
            new Object[][] {
                new Object[] {
               H00EN2_A820ContagemResultadoChckLstLog_Codigo, H00EN2_A822ContagemResultadoChckLstLog_UsuarioCod, H00EN2_A823ContagemResultadoChckLstLog_PessoaCod, H00EN2_n823ContagemResultadoChckLstLog_PessoaCod, H00EN2_A1839Check_Codigo, H00EN2_n1839Check_Codigo, H00EN2_A1841Check_Nome, H00EN2_A811ContagemResultadoChckLstLog_ChckLstCod, H00EN2_n811ContagemResultadoChckLstLog_ChckLstCod, H00EN2_A812ContagemResultadoChckLstLog_ChckLstDes,
               H00EN2_n812ContagemResultadoChckLstLog_ChckLstDes, H00EN2_A824ContagemResultadoChckLstLog_Etapa, H00EN2_n824ContagemResultadoChckLstLog_Etapa, H00EN2_A1853ContagemResultadoChckLstLog_OSCodigo, H00EN2_A817ContagemResultadoChckLstLog_UsuarioNome, H00EN2_n817ContagemResultadoChckLstLog_UsuarioNome, H00EN2_A814ContagemResultadoChckLstLog_DataHora
               }
               , new Object[] {
               H00EN3_AGRID_nRecordCount
               }
               , new Object[] {
               H00EN4_A602ContagemResultado_OSVinculada, H00EN4_n602ContagemResultado_OSVinculada, H00EN4_A456ContagemResultado_Codigo
               }
               , new Object[] {
               H00EN5_A1839Check_Codigo
               }
            }
         );
         AV41Pgmname = "ContagemResultadoChckLstLogWC";
         /* GeneXus formulas. */
         AV41Pgmname = "ContagemResultadoChckLstLogWC";
         context.Gx_err = 0;
         edtavDescricao_Enabled = 0;
      }

      private short nRcdExists_4 ;
      private short nIsMod_4 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_8 ;
      private short nGXsfl_8_idx=1 ;
      private short AV13OrderedBy ;
      private short A824ContagemResultadoChckLstLog_Etapa ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_8_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContagemResultadoChckLstLog_DataHora_Titleformat ;
      private short edtContagemResultadoChckLstLog_UsuarioNome_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV35ContagemResultadoChckLstLog_OSCodigo ;
      private int wcpOAV35ContagemResultadoChckLstLog_OSCodigo ;
      private int subGrid_Rows ;
      private int A602ContagemResultado_OSVinculada ;
      private int AV17ContagemResultadoChckLstLog_Codigo ;
      private int A456ContagemResultado_Codigo ;
      private int A811ContagemResultadoChckLstLog_ChckLstCod ;
      private int edtavDescricao_Enabled ;
      private int AV19ContagemResultado_OSVinculada ;
      private int A1853ContagemResultadoChckLstLog_OSCodigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_contagemresultadochcklstlog_usuarionome_Datalistupdateminimumcharacters ;
      private int edtContagemResultadoChckLstLog_OSCodigo_Visible ;
      private int edtavOrderedby_Visible ;
      private int edtavOrdereddsc_Visible ;
      private int edtavTfcontagemresultadochcklstlog_datahora_Visible ;
      private int edtavTfcontagemresultadochcklstlog_datahora_to_Visible ;
      private int edtavTfcontagemresultadochcklstlog_usuarionome_Visible ;
      private int edtavTfcontagemresultadochcklstlog_usuarionome_sel_Visible ;
      private int edtavDdo_contagemresultadochcklstlog_datahoratitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultadochcklstlog_usuarionometitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int A822ContagemResultadoChckLstLog_UsuarioCod ;
      private int A823ContagemResultadoChckLstLog_PessoaCod ;
      private int A1839Check_Codigo ;
      private int AV32PageToGo ;
      private int AV42GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavDescricao_Visible ;
      private int edtavViewchcklst_Enabled ;
      private int edtavViewchcklst_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV33GridCurrentPage ;
      private long AV34GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contagemresultadochcklstlog_datahora_Activeeventkey ;
      private String Ddo_contagemresultadochcklstlog_datahora_Filteredtext_get ;
      private String Ddo_contagemresultadochcklstlog_datahora_Filteredtextto_get ;
      private String Ddo_contagemresultadochcklstlog_usuarionome_Activeeventkey ;
      private String Ddo_contagemresultadochcklstlog_usuarionome_Filteredtext_get ;
      private String Ddo_contagemresultadochcklstlog_usuarionome_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_8_idx="0001" ;
      private String AV28TFContagemResultadoChckLstLog_UsuarioNome ;
      private String AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel ;
      private String AV41Pgmname ;
      private String A1841Check_Nome ;
      private String GXKey ;
      private String edtavDescricao_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Innewwindow_Target ;
      private String Ddo_contagemresultadochcklstlog_datahora_Caption ;
      private String Ddo_contagemresultadochcklstlog_datahora_Tooltip ;
      private String Ddo_contagemresultadochcklstlog_datahora_Cls ;
      private String Ddo_contagemresultadochcklstlog_datahora_Filteredtext_set ;
      private String Ddo_contagemresultadochcklstlog_datahora_Filteredtextto_set ;
      private String Ddo_contagemresultadochcklstlog_datahora_Dropdownoptionstype ;
      private String Ddo_contagemresultadochcklstlog_datahora_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultadochcklstlog_datahora_Sortedstatus ;
      private String Ddo_contagemresultadochcklstlog_datahora_Filtertype ;
      private String Ddo_contagemresultadochcklstlog_datahora_Sortasc ;
      private String Ddo_contagemresultadochcklstlog_datahora_Sortdsc ;
      private String Ddo_contagemresultadochcklstlog_datahora_Cleanfilter ;
      private String Ddo_contagemresultadochcklstlog_datahora_Rangefilterfrom ;
      private String Ddo_contagemresultadochcklstlog_datahora_Rangefilterto ;
      private String Ddo_contagemresultadochcklstlog_datahora_Searchbuttontext ;
      private String Ddo_contagemresultadochcklstlog_usuarionome_Caption ;
      private String Ddo_contagemresultadochcklstlog_usuarionome_Tooltip ;
      private String Ddo_contagemresultadochcklstlog_usuarionome_Cls ;
      private String Ddo_contagemresultadochcklstlog_usuarionome_Filteredtext_set ;
      private String Ddo_contagemresultadochcklstlog_usuarionome_Selectedvalue_set ;
      private String Ddo_contagemresultadochcklstlog_usuarionome_Dropdownoptionstype ;
      private String Ddo_contagemresultadochcklstlog_usuarionome_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultadochcklstlog_usuarionome_Sortedstatus ;
      private String Ddo_contagemresultadochcklstlog_usuarionome_Filtertype ;
      private String Ddo_contagemresultadochcklstlog_usuarionome_Datalisttype ;
      private String Ddo_contagemresultadochcklstlog_usuarionome_Datalistproc ;
      private String Ddo_contagemresultadochcklstlog_usuarionome_Sortasc ;
      private String Ddo_contagemresultadochcklstlog_usuarionome_Sortdsc ;
      private String Ddo_contagemresultadochcklstlog_usuarionome_Loadingdata ;
      private String Ddo_contagemresultadochcklstlog_usuarionome_Cleanfilter ;
      private String Ddo_contagemresultadochcklstlog_usuarionome_Noresultsfound ;
      private String Ddo_contagemresultadochcklstlog_usuarionome_Searchbuttontext ;
      private String GX_FocusControl ;
      private String edtContagemResultadoChckLstLog_OSCodigo_Internalname ;
      private String edtContagemResultadoChckLstLog_OSCodigo_Jsonclick ;
      private String TempTags ;
      private String edtavOrderedby_Internalname ;
      private String edtavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String edtavTfcontagemresultadochcklstlog_datahora_Internalname ;
      private String edtavTfcontagemresultadochcklstlog_datahora_Jsonclick ;
      private String edtavTfcontagemresultadochcklstlog_datahora_to_Internalname ;
      private String edtavTfcontagemresultadochcklstlog_datahora_to_Jsonclick ;
      private String divDdo_contagemresultadochcklstlog_datahoraauxdates_Internalname ;
      private String edtavDdo_contagemresultadochcklstlog_datahoraauxdate_Internalname ;
      private String edtavDdo_contagemresultadochcklstlog_datahoraauxdate_Jsonclick ;
      private String edtavDdo_contagemresultadochcklstlog_datahoraauxdateto_Internalname ;
      private String edtavDdo_contagemresultadochcklstlog_datahoraauxdateto_Jsonclick ;
      private String edtavTfcontagemresultadochcklstlog_usuarionome_Internalname ;
      private String edtavTfcontagemresultadochcklstlog_usuarionome_Jsonclick ;
      private String edtavTfcontagemresultadochcklstlog_usuarionome_sel_Internalname ;
      private String edtavTfcontagemresultadochcklstlog_usuarionome_sel_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_contagemresultadochcklstlog_datahoratitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultadochcklstlog_usuarionometitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtContagemResultadoChckLstLog_DataHora_Internalname ;
      private String A817ContagemResultadoChckLstLog_UsuarioNome ;
      private String edtContagemResultadoChckLstLog_UsuarioNome_Internalname ;
      private String edtavViewchcklst_Internalname ;
      private String scmdbuf ;
      private String lV28TFContagemResultadoChckLstLog_UsuarioNome ;
      private String subGrid_Internalname ;
      private String Ddo_contagemresultadochcklstlog_datahora_Internalname ;
      private String Ddo_contagemresultadochcklstlog_usuarionome_Internalname ;
      private String edtContagemResultadoChckLstLog_DataHora_Title ;
      private String edtContagemResultadoChckLstLog_UsuarioNome_Title ;
      private String edtavViewchcklst_Tooltiptext ;
      private String sStyleString ;
      private String tblTablegridheader_Internalname ;
      private String tblTblusertable_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String sCtrlAV35ContagemResultadoChckLstLog_OSCodigo ;
      private String sGXsfl_8_fel_idx="0001" ;
      private String ROClassString ;
      private String edtContagemResultadoChckLstLog_DataHora_Jsonclick ;
      private String edtavDescricao_Jsonclick ;
      private String edtContagemResultadoChckLstLog_UsuarioNome_Jsonclick ;
      private String edtavViewchcklst_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Innewwindow_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV22TFContagemResultadoChckLstLog_DataHora ;
      private DateTime AV23TFContagemResultadoChckLstLog_DataHora_To ;
      private DateTime A814ContagemResultadoChckLstLog_DataHora ;
      private DateTime AV24DDO_ContagemResultadoChckLstLog_DataHoraAuxDate ;
      private DateTime AV25DDO_ContagemResultadoChckLstLog_DataHoraAuxDateTo ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n811ContagemResultadoChckLstLog_ChckLstCod ;
      private bool n812ContagemResultadoChckLstLog_ChckLstDes ;
      private bool n824ContagemResultadoChckLstLog_Etapa ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contagemresultadochcklstlog_datahora_Includesortasc ;
      private bool Ddo_contagemresultadochcklstlog_datahora_Includesortdsc ;
      private bool Ddo_contagemresultadochcklstlog_datahora_Includefilter ;
      private bool Ddo_contagemresultadochcklstlog_datahora_Filterisrange ;
      private bool Ddo_contagemresultadochcklstlog_datahora_Includedatalist ;
      private bool Ddo_contagemresultadochcklstlog_usuarionome_Includesortasc ;
      private bool Ddo_contagemresultadochcklstlog_usuarionome_Includesortdsc ;
      private bool Ddo_contagemresultadochcklstlog_usuarionome_Includefilter ;
      private bool Ddo_contagemresultadochcklstlog_usuarionome_Filterisrange ;
      private bool Ddo_contagemresultadochcklstlog_usuarionome_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n817ContagemResultadoChckLstLog_UsuarioNome ;
      private bool n823ContagemResultadoChckLstLog_PessoaCod ;
      private bool n1839Check_Codigo ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV18ViewChckLst_IsBlob ;
      private String A812ContagemResultadoChckLstLog_ChckLstDes ;
      private String AV16Descricao ;
      private String AV26ddo_ContagemResultadoChckLstLog_DataHoraTitleControlIdToReplace ;
      private String AV30ddo_ContagemResultadoChckLstLog_UsuarioNomeTitleControlIdToReplace ;
      private String AV40Viewchcklst_GXI ;
      private String AV18ViewChckLst ;
      private IGxSession AV15Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00EN2_A820ContagemResultadoChckLstLog_Codigo ;
      private int[] H00EN2_A822ContagemResultadoChckLstLog_UsuarioCod ;
      private int[] H00EN2_A823ContagemResultadoChckLstLog_PessoaCod ;
      private bool[] H00EN2_n823ContagemResultadoChckLstLog_PessoaCod ;
      private int[] H00EN2_A1839Check_Codigo ;
      private bool[] H00EN2_n1839Check_Codigo ;
      private String[] H00EN2_A1841Check_Nome ;
      private int[] H00EN2_A811ContagemResultadoChckLstLog_ChckLstCod ;
      private bool[] H00EN2_n811ContagemResultadoChckLstLog_ChckLstCod ;
      private String[] H00EN2_A812ContagemResultadoChckLstLog_ChckLstDes ;
      private bool[] H00EN2_n812ContagemResultadoChckLstLog_ChckLstDes ;
      private short[] H00EN2_A824ContagemResultadoChckLstLog_Etapa ;
      private bool[] H00EN2_n824ContagemResultadoChckLstLog_Etapa ;
      private int[] H00EN2_A1853ContagemResultadoChckLstLog_OSCodigo ;
      private String[] H00EN2_A817ContagemResultadoChckLstLog_UsuarioNome ;
      private bool[] H00EN2_n817ContagemResultadoChckLstLog_UsuarioNome ;
      private DateTime[] H00EN2_A814ContagemResultadoChckLstLog_DataHora ;
      private long[] H00EN3_AGRID_nRecordCount ;
      private int[] H00EN4_A602ContagemResultado_OSVinculada ;
      private bool[] H00EN4_n602ContagemResultado_OSVinculada ;
      private int[] H00EN4_A456ContagemResultado_Codigo ;
      private int[] H00EN5_A1839Check_Codigo ;
      private bool[] H00EN5_n1839Check_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV21ContagemResultadoChckLstLog_DataHoraTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV27ContagemResultadoChckLstLog_UsuarioNomeTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV31DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class contagemresultadochcklstlogwc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00EN2( IGxContext context ,
                                             DateTime AV22TFContagemResultadoChckLstLog_DataHora ,
                                             DateTime AV23TFContagemResultadoChckLstLog_DataHora_To ,
                                             String AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel ,
                                             String AV28TFContagemResultadoChckLstLog_UsuarioNome ,
                                             DateTime A814ContagemResultadoChckLstLog_DataHora ,
                                             String A817ContagemResultadoChckLstLog_UsuarioNome ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A1853ContagemResultadoChckLstLog_OSCodigo ,
                                             int AV35ContagemResultadoChckLstLog_OSCodigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [10] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[ContagemResultadoChckLstLog_Codigo], T1.[ContagemResultadoChckLstLog_UsuarioCod] AS ContagemResultadoChckLstLog_UsuarioCod, T2.[Usuario_PessoaCod] AS ContagemResultadoChckLstLog_PessoaCod, T4.[Check_Codigo], T5.[Check_Nome], T1.[ContagemResultadoChckLstLog_ChckLstCod] AS ContagemResultadoChckLstLog_ChckLstCod, T4.[CheckList_Descricao] AS ContagemResultadoChckLstLog_ChckLstDes, T1.[ContagemResultadoChckLstLog_Etapa], T1.[ContagemResultadoChckLstLog_OSCodigo], T3.[Pessoa_Nome] AS ContagemResultadoChckLstLog_UsuarioNome, T1.[ContagemResultadoChckLstLog_DataHora]";
         sFromString = " FROM (((([ContagemResultadoChckLstLog] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContagemResultadoChckLstLog_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) LEFT JOIN [CheckList] T4 WITH (NOLOCK) ON T4.[CheckList_Codigo] = T1.[ContagemResultadoChckLstLog_ChckLstCod]) LEFT JOIN [Check] T5 WITH (NOLOCK) ON T5.[Check_Codigo] = T4.[Check_Codigo])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[ContagemResultadoChckLstLog_OSCodigo] = @AV35ContagemResultadoChckLstLog_OSCodigo)";
         if ( ! (DateTime.MinValue==AV22TFContagemResultadoChckLstLog_DataHora) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultadoChckLstLog_DataHora] >= @AV22TFContagemResultadoChckLstLog_DataHora)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ! (DateTime.MinValue==AV23TFContagemResultadoChckLstLog_DataHora_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultadoChckLstLog_DataHora] <= @AV23TFContagemResultadoChckLstLog_DataHora_To)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28TFContagemResultadoChckLstLog_UsuarioNome)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV28TFContagemResultadoChckLstLog_UsuarioNome)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoChckLstLog_OSCodigo], T1.[ContagemResultadoChckLstLog_DataHora]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoChckLstLog_OSCodigo] DESC, T1.[ContagemResultadoChckLstLog_DataHora] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoChckLstLog_OSCodigo], T3.[Pessoa_Nome]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoChckLstLog_OSCodigo] DESC, T3.[Pessoa_Nome] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoChckLstLog_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00EN3( IGxContext context ,
                                             DateTime AV22TFContagemResultadoChckLstLog_DataHora ,
                                             DateTime AV23TFContagemResultadoChckLstLog_DataHora_To ,
                                             String AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel ,
                                             String AV28TFContagemResultadoChckLstLog_UsuarioNome ,
                                             DateTime A814ContagemResultadoChckLstLog_DataHora ,
                                             String A817ContagemResultadoChckLstLog_UsuarioNome ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A1853ContagemResultadoChckLstLog_OSCodigo ,
                                             int AV35ContagemResultadoChckLstLog_OSCodigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [5] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM (((([ContagemResultadoChckLstLog] T1 WITH (NOLOCK) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[ContagemResultadoChckLstLog_UsuarioCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) LEFT JOIN [CheckList] T2 WITH (NOLOCK) ON T2.[CheckList_Codigo] = T1.[ContagemResultadoChckLstLog_ChckLstCod]) LEFT JOIN [Check] T5 WITH (NOLOCK) ON T5.[Check_Codigo] = T2.[Check_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContagemResultadoChckLstLog_OSCodigo] = @AV35ContagemResultadoChckLstLog_OSCodigo)";
         if ( ! (DateTime.MinValue==AV22TFContagemResultadoChckLstLog_DataHora) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultadoChckLstLog_DataHora] >= @AV22TFContagemResultadoChckLstLog_DataHora)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ! (DateTime.MinValue==AV23TFContagemResultadoChckLstLog_DataHora_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultadoChckLstLog_DataHora] <= @AV23TFContagemResultadoChckLstLog_DataHora_To)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28TFContagemResultadoChckLstLog_UsuarioNome)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV28TFContagemResultadoChckLstLog_UsuarioNome)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00EN2(context, (DateTime)dynConstraints[0] , (DateTime)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (DateTime)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (bool)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] );
               case 1 :
                     return conditional_H00EN3(context, (DateTime)dynConstraints[0] , (DateTime)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (DateTime)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (bool)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00EN4 ;
          prmH00EN4 = new Object[] {
          new Object[] {"@AV17ContagemResultadoChckLstLog_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00EN5 ;
          prmH00EN5 = new Object[] {
          new Object[] {"@Check_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00EN2 ;
          prmH00EN2 = new Object[] {
          new Object[] {"@AV35ContagemResultadoChckLstLog_OSCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV22TFContagemResultadoChckLstLog_DataHora",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV23TFContagemResultadoChckLstLog_DataHora_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV28TFContagemResultadoChckLstLog_UsuarioNome",SqlDbType.Char,100,0} ,
          new Object[] {"@AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00EN3 ;
          prmH00EN3 = new Object[] {
          new Object[] {"@AV35ContagemResultadoChckLstLog_OSCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV22TFContagemResultadoChckLstLog_DataHora",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV23TFContagemResultadoChckLstLog_DataHora_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV28TFContagemResultadoChckLstLog_UsuarioNome",SqlDbType.Char,100,0} ,
          new Object[] {"@AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel",SqlDbType.Char,100,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00EN2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EN2,11,0,true,false )
             ,new CursorDef("H00EN3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EN3,1,0,true,false )
             ,new CursorDef("H00EN4", "SELECT TOP 1 [ContagemResultado_OSVinculada], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_OSVinculada] = @AV17ContagemResultadoChckLstLog_Codigo ORDER BY [ContagemResultado_OSVinculada] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EN4,1,0,false,true )
             ,new CursorDef("H00EN5", "SELECT TOP 1 [Check_Codigo] FROM [Check] WITH (NOLOCK) WHERE [Check_Codigo] = @Check_Codigo ORDER BY [Check_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EN5,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 50) ;
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((short[]) buf[11])[0] = rslt.getShort(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((int[]) buf[13])[0] = rslt.getInt(9) ;
                ((String[]) buf[14])[0] = rslt.getString(10, 100) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((DateTime[]) buf[16])[0] = rslt.getGXDateTime(11) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[11]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[12]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[6]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[7]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
