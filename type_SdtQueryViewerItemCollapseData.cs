/*
               File: type_SdtQueryViewerItemCollapseData
        Description: QueryViewerItemCollapseData
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:29:58.36
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "QueryViewerItemCollapseData" )]
   [XmlType(TypeName =  "QueryViewerItemCollapseData" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtQueryViewerItemCollapseData : GxUserType
   {
      public SdtQueryViewerItemCollapseData( )
      {
         /* Constructor for serialization */
         gxTv_SdtQueryViewerItemCollapseData_Name = "";
         gxTv_SdtQueryViewerItemCollapseData_Value = "";
      }

      public SdtQueryViewerItemCollapseData( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtQueryViewerItemCollapseData deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtQueryViewerItemCollapseData)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtQueryViewerItemCollapseData obj ;
         obj = this;
         obj.gxTpr_Name = deserialized.gxTpr_Name;
         obj.gxTpr_Value = deserialized.gxTpr_Value;
         obj.gxTpr_Expandedvalues = deserialized.gxTpr_Expandedvalues;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Name") )
               {
                  gxTv_SdtQueryViewerItemCollapseData_Name = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Value") )
               {
                  gxTv_SdtQueryViewerItemCollapseData_Value = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ExpandedValues") )
               {
                  if ( gxTv_SdtQueryViewerItemCollapseData_Expandedvalues == null )
                  {
                     gxTv_SdtQueryViewerItemCollapseData_Expandedvalues = new GxSimpleCollection();
                  }
                  if ( oReader.IsSimple == 0 )
                  {
                     GXSoapError = gxTv_SdtQueryViewerItemCollapseData_Expandedvalues.readxmlcollection(oReader, "ExpandedValues", "Item");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "QueryViewerItemCollapseData";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Name", StringUtil.RTrim( gxTv_SdtQueryViewerItemCollapseData_Name));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Value", StringUtil.RTrim( gxTv_SdtQueryViewerItemCollapseData_Value));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( gxTv_SdtQueryViewerItemCollapseData_Expandedvalues != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "GxEv3Up14_MeetrikaVs3";
            }
            else
            {
               sNameSpace1 = "GxEv3Up14_MeetrikaVs3";
            }
            gxTv_SdtQueryViewerItemCollapseData_Expandedvalues.writexmlcollection(oWriter, "ExpandedValues", sNameSpace1, "Item", sNameSpace1);
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Name", gxTv_SdtQueryViewerItemCollapseData_Name, false);
         AddObjectProperty("Value", gxTv_SdtQueryViewerItemCollapseData_Value, false);
         if ( gxTv_SdtQueryViewerItemCollapseData_Expandedvalues != null )
         {
            AddObjectProperty("ExpandedValues", gxTv_SdtQueryViewerItemCollapseData_Expandedvalues, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Name" )]
      [  XmlElement( ElementName = "Name"   )]
      public String gxTpr_Name
      {
         get {
            return gxTv_SdtQueryViewerItemCollapseData_Name ;
         }

         set {
            gxTv_SdtQueryViewerItemCollapseData_Name = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Value" )]
      [  XmlElement( ElementName = "Value"   )]
      public String gxTpr_Value
      {
         get {
            return gxTv_SdtQueryViewerItemCollapseData_Value ;
         }

         set {
            gxTv_SdtQueryViewerItemCollapseData_Value = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ExpandedValues" )]
      [  XmlArray( ElementName = "ExpandedValues"  )]
      [  XmlArrayItemAttribute( Type= typeof( String ), ElementName= "Item"  , IsNullable=false)]
      public GxSimpleCollection gxTpr_Expandedvalues_GxSimpleCollection
      {
         get {
            if ( gxTv_SdtQueryViewerItemCollapseData_Expandedvalues == null )
            {
               gxTv_SdtQueryViewerItemCollapseData_Expandedvalues = new GxSimpleCollection();
            }
            return (GxSimpleCollection)gxTv_SdtQueryViewerItemCollapseData_Expandedvalues ;
         }

         set {
            if ( gxTv_SdtQueryViewerItemCollapseData_Expandedvalues == null )
            {
               gxTv_SdtQueryViewerItemCollapseData_Expandedvalues = new GxSimpleCollection();
            }
            gxTv_SdtQueryViewerItemCollapseData_Expandedvalues = (GxSimpleCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public IGxCollection gxTpr_Expandedvalues
      {
         get {
            if ( gxTv_SdtQueryViewerItemCollapseData_Expandedvalues == null )
            {
               gxTv_SdtQueryViewerItemCollapseData_Expandedvalues = new GxSimpleCollection();
            }
            return gxTv_SdtQueryViewerItemCollapseData_Expandedvalues ;
         }

         set {
            gxTv_SdtQueryViewerItemCollapseData_Expandedvalues = value;
         }

      }

      public void gxTv_SdtQueryViewerItemCollapseData_Expandedvalues_SetNull( )
      {
         gxTv_SdtQueryViewerItemCollapseData_Expandedvalues = null;
         return  ;
      }

      public bool gxTv_SdtQueryViewerItemCollapseData_Expandedvalues_IsNull( )
      {
         if ( gxTv_SdtQueryViewerItemCollapseData_Expandedvalues == null )
         {
            return true ;
         }
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtQueryViewerItemCollapseData_Name = "";
         gxTv_SdtQueryViewerItemCollapseData_Value = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String gxTv_SdtQueryViewerItemCollapseData_Name ;
      protected String gxTv_SdtQueryViewerItemCollapseData_Value ;
      protected String sTagName ;
      [ObjectCollection(ItemType=typeof( String ))]
      protected IGxCollection gxTv_SdtQueryViewerItemCollapseData_Expandedvalues=null ;
   }

   [DataContract(Name = @"QueryViewerItemCollapseData", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtQueryViewerItemCollapseData_RESTInterface : GxGenericCollectionItem<SdtQueryViewerItemCollapseData>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtQueryViewerItemCollapseData_RESTInterface( ) : base()
      {
      }

      public SdtQueryViewerItemCollapseData_RESTInterface( SdtQueryViewerItemCollapseData psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Name" , Order = 0 )]
      public String gxTpr_Name
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Name) ;
         }

         set {
            sdt.gxTpr_Name = (String)(value);
         }

      }

      [DataMember( Name = "Value" , Order = 1 )]
      public String gxTpr_Value
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Value) ;
         }

         set {
            sdt.gxTpr_Value = (String)(value);
         }

      }

      [DataMember( Name = "ExpandedValues" , Order = 2 )]
      public GxSimpleCollection gxTpr_Expandedvalues
      {
         get {
            return (GxSimpleCollection)(sdt.gxTpr_Expandedvalues) ;
         }

         set {
            sdt.gxTpr_Expandedvalues = value;
         }

      }

      public SdtQueryViewerItemCollapseData sdt
      {
         get {
            return (SdtQueryViewerItemCollapseData)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtQueryViewerItemCollapseData() ;
         }
      }

   }

}
