/*
               File: ReferenciaINM
        Description: Referencia INM
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 12:51:20.10
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class referenciainm : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_11") == 0 )
         {
            A712ReferenciaINM_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A712ReferenciaINM_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A712ReferenciaINM_AreaTrabalhoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_11( A712ReferenciaINM_AreaTrabalhoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridanexos") == 0 )
         {
            nRC_GXsfl_29 = (short)(NumberUtil.Val( GetNextPar( ), "."));
            nGXsfl_29_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
            sGXsfl_29_idx = GetNextPar( );
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxnrGridanexos_newrow( ) ;
            return  ;
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7ReferenciaINM_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ReferenciaINM_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vREFERENCIAINM_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ReferenciaINM_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         chkReferenciaINM_Ativo.Name = "REFERENCIAINM_ATIVO";
         chkReferenciaINM_Ativo.WebTags = "";
         chkReferenciaINM_Ativo.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkReferenciaINM_Ativo_Internalname, "TitleCaption", chkReferenciaINM_Ativo.Caption);
         chkReferenciaINM_Ativo.CheckedValue = "false";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Referencia INM", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtReferenciaINM_Descricao_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public referenciainm( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public referenciainm( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_ReferenciaINM_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7ReferenciaINM_Codigo = aP1_ReferenciaINM_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         chkReferenciaINM_Ativo = new GXCheckbox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_2C90( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_2C90e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtReferenciaINM_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A709ReferenciaINM_Codigo), 6, 0, ",", "")), ((edtReferenciaINM_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A709ReferenciaINM_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A709ReferenciaINM_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtReferenciaINM_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtReferenciaINM_Codigo_Visible, edtReferenciaINM_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ReferenciaINM.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_2C90( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblReferenciainmtitle_Internalname, "Referencia INM", "", "", lblReferenciainmtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_ReferenciaINM.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_2C90( true) ;
         }
         return  ;
      }

      protected void wb_table2_8_2C90e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_33_2C90( true) ;
         }
         return  ;
      }

      protected void wb_table3_33_2C90e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_2C90e( true) ;
         }
         else
         {
            wb_table1_2_2C90e( false) ;
         }
      }

      protected void wb_table3_33_2C90( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ReferenciaINM.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ReferenciaINM.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ReferenciaINM.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_33_2C90e( true) ;
         }
         else
         {
            wb_table3_33_2C90e( false) ;
         }
      }

      protected void wb_table2_8_2C90( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_16_2C90( true) ;
         }
         return  ;
      }

      protected void wb_table4_16_2C90e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridanexosContainer.AddObjectProperty("GridName", "Gridanexos");
            GridanexosContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
            GridanexosContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
            GridanexosContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
            GridanexosContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridanexos_Backcolorstyle), 1, 0, ".", "")));
            GridanexosContainer.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridanexos_Visible), 5, 0, ".", "")));
            GridanexosContainer.AddObjectProperty("CmpContext", "");
            GridanexosContainer.AddObjectProperty("InMasterPage", "false");
            GridanexosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridanexosColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2134ReferenciaINMAnexos_Codigo), 6, 0, ".", "")));
            GridanexosColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtReferenciaINMAnexos_Codigo_Enabled), 5, 0, ".", "")));
            GridanexosContainer.AddColumnProperties(GridanexosColumn);
            GridanexosContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridanexos_Allowselection), 1, 0, ".", "")));
            GridanexosContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridanexos_Selectioncolor), 9, 0, ".", "")));
            GridanexosContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridanexos_Allowhovering), 1, 0, ".", "")));
            GridanexosContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridanexos_Hoveringcolor), 9, 0, ".", "")));
            GridanexosContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridanexos_Allowcollapsing), 1, 0, ".", "")));
            GridanexosContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridanexos_Collapsed), 1, 0, ".", "")));
            nGXsfl_29_idx = 0;
            if ( ( nKeyPressed == 1 ) && ( AnyError == 0 ) )
            {
               /* Enter key processing. */
               nBlankRcdCount234 = 5;
               if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
               {
                  /* Display confirmed (stored) records */
                  nRcdExists_234 = 1;
                  ScanStart2C234( ) ;
                  while ( RcdFound234 != 0 )
                  {
                     init_level_properties234( ) ;
                     getByPrimaryKey2C234( ) ;
                     AddRow2C234( ) ;
                     ScanNext2C234( ) ;
                  }
                  ScanEnd2C234( ) ;
                  nBlankRcdCount234 = 5;
               }
            }
            else if ( ( nKeyPressed == 3 ) || ( nKeyPressed == 4 ) || ( ( nKeyPressed == 1 ) && ( AnyError != 0 ) ) )
            {
               /* Button check  or addlines. */
               standaloneNotModal2C234( ) ;
               standaloneModal2C234( ) ;
               sMode234 = Gx_mode;
               while ( nGXsfl_29_idx < nRC_GXsfl_29 )
               {
                  ReadRow2C234( ) ;
                  edtReferenciaINMAnexos_Codigo_Enabled = (int)(context.localUtil.CToN( cgiGet( "REFERENCIAINMANEXOS_CODIGO_"+sGXsfl_29_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtReferenciaINMAnexos_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtReferenciaINMAnexos_Codigo_Enabled), 5, 0)));
                  if ( ( nRcdExists_234 == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") != 0 ) )
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     standaloneModal2C234( ) ;
                  }
                  SendRow2C234( ) ;
               }
               Gx_mode = sMode234;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            }
            else
            {
               /* Get or get-alike key processing. */
               nBlankRcdCount234 = 5;
               nRcdExists_234 = 1;
               if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
               {
                  ScanStart2C234( ) ;
                  while ( RcdFound234 != 0 )
                  {
                     sGXsfl_29_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_29_idx+1), 4, 0)), 4, "0");
                     SubsflControlProps_29234( ) ;
                     init_level_properties234( ) ;
                     standaloneNotModal2C234( ) ;
                     getByPrimaryKey2C234( ) ;
                     standaloneModal2C234( ) ;
                     AddRow2C234( ) ;
                     ScanNext2C234( ) ;
                  }
                  ScanEnd2C234( ) ;
               }
            }
            /* Initialize fields for 'new' records and send them. */
            if ( ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 ) && ( StringUtil.StrCmp(Gx_mode, "DLT") != 0 ) )
            {
               sMode234 = Gx_mode;
               Gx_mode = "INS";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
               sGXsfl_29_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_29_idx+1), 4, 0)), 4, "0");
               SubsflControlProps_29234( ) ;
               InitAll2C234( ) ;
               init_level_properties234( ) ;
               standaloneNotModal2C234( ) ;
               standaloneModal2C234( ) ;
               nRcdExists_234 = 0;
               nIsMod_234 = 0;
               nRcdDeleted_234 = 0;
               nBlankRcdCount234 = (short)(nBlankRcdUsr234+nBlankRcdCount234);
               fRowAdded = 0;
               while ( nBlankRcdCount234 > 0 )
               {
                  AddRow2C234( ) ;
                  if ( ( nKeyPressed == 4 ) && ( fRowAdded == 0 ) )
                  {
                     fRowAdded = 1;
                     GX_FocusControl = edtReferenciaINMAnexos_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  nBlankRcdCount234 = (short)(nBlankRcdCount234-1);
               }
               Gx_mode = sMode234;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            }
            if ( subGridanexos_Visible != 0 )
            {
               sStyleString = "";
            }
            else
            {
               sStyleString = " style=\"display:none;\"";
            }
            context.WriteHtmlText( "<div id=\""+"GridanexosContainer"+"Div\" "+sStyleString+">"+"</div>") ;
            context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Gridanexos", GridanexosContainer);
            if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
            {
               GxWebStd.gx_hidden_field( context, "GridanexosContainerData", GridanexosContainer.ToJavascriptSource());
            }
            if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
            {
               GxWebStd.gx_hidden_field( context, "GridanexosContainerData"+"V", GridanexosContainer.GridValuesHidden());
            }
            else
            {
               context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridanexosContainerData"+"V"+"\" value='"+GridanexosContainer.GridValuesHidden()+"'/>") ;
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_2C90e( true) ;
         }
         else
         {
            wb_table2_8_2C90e( false) ;
         }
      }

      protected void wb_table4_16_2C90( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "Table", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockreferenciainm_descricao_Internalname, "Descri��o", "", "", lblTextblockreferenciainm_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ReferenciaINM.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtReferenciaINM_Descricao_Internalname, A710ReferenciaINM_Descricao, StringUtil.RTrim( context.localUtil.Format( A710ReferenciaINM_Descricao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtReferenciaINM_Descricao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtReferenciaINM_Descricao_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_ReferenciaINM.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockreferenciainm_ativo_Internalname, "Ativo", "", "", lblTextblockreferenciainm_ativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockreferenciainm_ativo_Visible, 1, 0, "HLP_ReferenciaINM.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkReferenciaINM_Ativo_Internalname, StringUtil.BoolToStr( A711ReferenciaINM_Ativo), "", "", chkReferenciaINM_Ativo.Visible, chkReferenciaINM_Ativo.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(26, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,26);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_16_2C90e( true) ;
         }
         else
         {
            wb_table4_16_2C90e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E112C2 */
         E112C2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A710ReferenciaINM_Descricao = StringUtil.Upper( cgiGet( edtReferenciaINM_Descricao_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A710ReferenciaINM_Descricao", A710ReferenciaINM_Descricao);
               A711ReferenciaINM_Ativo = StringUtil.StrToBool( cgiGet( chkReferenciaINM_Ativo_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A711ReferenciaINM_Ativo", A711ReferenciaINM_Ativo);
               A709ReferenciaINM_Codigo = (int)(context.localUtil.CToN( cgiGet( edtReferenciaINM_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A709ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A709ReferenciaINM_Codigo), 6, 0)));
               /* Read saved values. */
               Z709ReferenciaINM_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z709ReferenciaINM_Codigo"), ",", "."));
               Z710ReferenciaINM_Descricao = cgiGet( "Z710ReferenciaINM_Descricao");
               Z711ReferenciaINM_Ativo = StringUtil.StrToBool( cgiGet( "Z711ReferenciaINM_Ativo"));
               Z712ReferenciaINM_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "Z712ReferenciaINM_AreaTrabalhoCod"), ",", "."));
               A712ReferenciaINM_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "Z712ReferenciaINM_AreaTrabalhoCod"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               nRC_GXsfl_29 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_29"), ",", "."));
               N712ReferenciaINM_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "N712ReferenciaINM_AreaTrabalhoCod"), ",", "."));
               AV7ReferenciaINM_Codigo = (int)(context.localUtil.CToN( cgiGet( "vREFERENCIAINM_CODIGO"), ",", "."));
               AV11Insert_ReferenciaINM_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_REFERENCIAINM_AREATRABALHOCOD"), ",", "."));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A712ReferenciaINM_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "REFERENCIAINM_AREATRABALHOCOD"), ",", "."));
               A713ReferenciaINM_AreaTrabalhoDes = cgiGet( "REFERENCIAINM_AREATRABALHODES");
               n713ReferenciaINM_AreaTrabalhoDes = false;
               AV14Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               A2138ReferenciaINMAnexos_Descricao = cgiGet( "REFERENCIAINMANEXOS_DESCRICAO");
               A2139ReferenciaINMAnexos_Arquivo = cgiGet( "REFERENCIAINMANEXOS_ARQUIVO");
               n2139ReferenciaINMAnexos_Arquivo = false;
               n2139ReferenciaINMAnexos_Arquivo = (String.IsNullOrEmpty(StringUtil.RTrim( A2139ReferenciaINMAnexos_Arquivo)) ? true : false);
               A2143ReferenciaINMAnexos_Link = cgiGet( "REFERENCIAINMANEXOS_LINK");
               n2143ReferenciaINMAnexos_Link = false;
               n2143ReferenciaINMAnexos_Link = (String.IsNullOrEmpty(StringUtil.RTrim( A2143ReferenciaINMAnexos_Link)) ? true : false);
               A2142ReferenciaINMAnexos_Data = context.localUtil.CToT( cgiGet( "REFERENCIAINMANEXOS_DATA"), 0);
               n2142ReferenciaINMAnexos_Data = false;
               n2142ReferenciaINMAnexos_Data = ((DateTime.MinValue==A2142ReferenciaINMAnexos_Data) ? true : false);
               A2140ReferenciaINMAnexos_ArquivoTipo = cgiGet( "REFERENCIAINMANEXOS_ARQUIVOTIPO");
               n2140ReferenciaINMAnexos_ArquivoTipo = false;
               n2140ReferenciaINMAnexos_ArquivoTipo = (String.IsNullOrEmpty(StringUtil.RTrim( A2140ReferenciaINMAnexos_ArquivoTipo)) ? true : false);
               A2141ReferenciaINMAnexos_ArquivoNome = cgiGet( "REFERENCIAINMANEXOS_ARQUIVONOME");
               n2141ReferenciaINMAnexos_ArquivoNome = false;
               n2141ReferenciaINMAnexos_ArquivoNome = (String.IsNullOrEmpty(StringUtil.RTrim( A2141ReferenciaINMAnexos_ArquivoNome)) ? true : false);
               A645TipoDocumento_Codigo = (int)(context.localUtil.CToN( cgiGet( "TIPODOCUMENTO_CODIGO"), ",", "."));
               n645TipoDocumento_Codigo = false;
               n645TipoDocumento_Codigo = ((0==A645TipoDocumento_Codigo) ? true : false);
               A646TipoDocumento_Nome = cgiGet( "TIPODOCUMENTO_NOME");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "ReferenciaINM";
               A709ReferenciaINM_Codigo = (int)(context.localUtil.CToN( cgiGet( edtReferenciaINM_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A709ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A709ReferenciaINM_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A709ReferenciaINM_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A712ReferenciaINM_AreaTrabalhoCod), "ZZZZZ9");
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A709ReferenciaINM_Codigo != Z709ReferenciaINM_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("referenciainm:[SecurityCheckFailed value for]"+"ReferenciaINM_Codigo:"+context.localUtil.Format( (decimal)(A709ReferenciaINM_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("referenciainm:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("referenciainm:[SecurityCheckFailed value for]"+"ReferenciaINM_AreaTrabalhoCod:"+context.localUtil.Format( (decimal)(A712ReferenciaINM_AreaTrabalhoCod), "ZZZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               /* Check if conditions changed and reset current page numbers */
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A709ReferenciaINM_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A709ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A709ReferenciaINM_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode90 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode90;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound90 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_2C0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "REFERENCIAINM_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtReferenciaINM_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E112C2 */
                           E112C2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E122C2 */
                           E122C2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E122C2 */
            E122C2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll2C90( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes2C90( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_2C0( )
      {
         BeforeValidate2C90( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls2C90( ) ;
            }
            else
            {
               CheckExtendedTable2C90( ) ;
               CloseExtendedTableCursors2C90( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            /* Save parent mode. */
            sMode90 = Gx_mode;
            CONFIRM_2C234( ) ;
            if ( AnyError == 0 )
            {
               /* Restore parent mode. */
               Gx_mode = sMode90;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
               IsConfirmed = 1;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
            }
            /* Restore parent mode. */
            Gx_mode = sMode90;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
      }

      protected void CONFIRM_2C234( )
      {
         nGXsfl_29_idx = 0;
         while ( nGXsfl_29_idx < nRC_GXsfl_29 )
         {
            ReadRow2C234( ) ;
            if ( ( nRcdExists_234 != 0 ) || ( nIsMod_234 != 0 ) )
            {
               GetKey2C234( ) ;
               if ( ( nRcdExists_234 == 0 ) && ( nRcdDeleted_234 == 0 ) )
               {
                  if ( RcdFound234 == 0 )
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     BeforeValidate2C234( ) ;
                     if ( AnyError == 0 )
                     {
                        CheckExtendedTable2C234( ) ;
                        CloseExtendedTableCursors2C234( ) ;
                        if ( AnyError == 0 )
                        {
                           IsConfirmed = 1;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
                        }
                     }
                  }
                  else
                  {
                     GXCCtl = "REFERENCIAINMANEXOS_CODIGO_" + sGXsfl_29_idx;
                     GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, GXCCtl);
                     AnyError = 1;
                     GX_FocusControl = edtReferenciaINMAnexos_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( RcdFound234 != 0 )
                  {
                     if ( nRcdDeleted_234 != 0 )
                     {
                        Gx_mode = "DLT";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                        getByPrimaryKey2C234( ) ;
                        Load2C234( ) ;
                        BeforeValidate2C234( ) ;
                        if ( AnyError == 0 )
                        {
                           OnDeleteControls2C234( ) ;
                        }
                     }
                     else
                     {
                        if ( nIsMod_234 != 0 )
                        {
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                           BeforeValidate2C234( ) ;
                           if ( AnyError == 0 )
                           {
                              CheckExtendedTable2C234( ) ;
                              CloseExtendedTableCursors2C234( ) ;
                              if ( AnyError == 0 )
                              {
                                 IsConfirmed = 1;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
                              }
                           }
                        }
                     }
                  }
                  else
                  {
                     if ( nRcdDeleted_234 == 0 )
                     {
                        GXCCtl = "REFERENCIAINMANEXOS_CODIGO_" + sGXsfl_29_idx;
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, GXCCtl);
                        AnyError = 1;
                        GX_FocusControl = edtReferenciaINMAnexos_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
            ChangePostValue( edtReferenciaINMAnexos_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A2134ReferenciaINMAnexos_Codigo), 6, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z2134ReferenciaINMAnexos_Codigo_"+sGXsfl_29_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2134ReferenciaINMAnexos_Codigo), 6, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z2142ReferenciaINMAnexos_Data_"+sGXsfl_29_idx, context.localUtil.TToC( Z2142ReferenciaINMAnexos_Data, 10, 8, 0, 0, "/", ":", " ")) ;
            ChangePostValue( "ZT_"+"Z645TipoDocumento_Codigo_"+sGXsfl_29_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z645TipoDocumento_Codigo), 6, 0, ",", ""))) ;
            ChangePostValue( "nRcdDeleted_234_"+sGXsfl_29_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_234), 4, 0, ",", ""))) ;
            ChangePostValue( "nRcdExists_234_"+sGXsfl_29_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_234), 4, 0, ",", ""))) ;
            ChangePostValue( "nIsMod_234_"+sGXsfl_29_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_234), 4, 0, ",", ""))) ;
            if ( nIsMod_234 != 0 )
            {
               ChangePostValue( "REFERENCIAINMANEXOS_CODIGO_"+sGXsfl_29_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtReferenciaINMAnexos_Codigo_Enabled), 5, 0, ".", ""))) ;
            }
         }
         /* Start of After( level) rules */
         /* End of After( level) rules */
      }

      protected void ResetCaption2C0( )
      {
      }

      protected void E112C2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV14Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV15GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            while ( AV15GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV15GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "ReferenciaINM_AreaTrabalhoCod") == 0 )
               {
                  AV11Insert_ReferenciaINM_AreaTrabalhoCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_ReferenciaINM_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_ReferenciaINM_AreaTrabalhoCod), 6, 0)));
               }
               AV15GXV1 = (int)(AV15GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            }
         }
         edtReferenciaINM_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtReferenciaINM_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtReferenciaINM_Codigo_Visible), 5, 0)));
         GX_FocusControl = Dvpanel_tableattributes_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         context.DoAjaxSetFocus(GX_FocusControl);
         subGridanexos_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "GridanexosContainerDiv", "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(subGridanexos_Visible), 5, 0)));
      }

      protected void E122C2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwreferenciainm.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM2C90( short GX_JID )
      {
         if ( ( GX_JID == 10 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z710ReferenciaINM_Descricao = T002C6_A710ReferenciaINM_Descricao[0];
               Z711ReferenciaINM_Ativo = T002C6_A711ReferenciaINM_Ativo[0];
               Z712ReferenciaINM_AreaTrabalhoCod = T002C6_A712ReferenciaINM_AreaTrabalhoCod[0];
            }
            else
            {
               Z710ReferenciaINM_Descricao = A710ReferenciaINM_Descricao;
               Z711ReferenciaINM_Ativo = A711ReferenciaINM_Ativo;
               Z712ReferenciaINM_AreaTrabalhoCod = A712ReferenciaINM_AreaTrabalhoCod;
            }
         }
         if ( GX_JID == -10 )
         {
            Z709ReferenciaINM_Codigo = A709ReferenciaINM_Codigo;
            Z710ReferenciaINM_Descricao = A710ReferenciaINM_Descricao;
            Z711ReferenciaINM_Ativo = A711ReferenciaINM_Ativo;
            Z712ReferenciaINM_AreaTrabalhoCod = A712ReferenciaINM_AreaTrabalhoCod;
            Z713ReferenciaINM_AreaTrabalhoDes = A713ReferenciaINM_AreaTrabalhoDes;
         }
      }

      protected void standaloneNotModal( )
      {
         edtReferenciaINM_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtReferenciaINM_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtReferenciaINM_Codigo_Enabled), 5, 0)));
         AV14Pgmname = "ReferenciaINM";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Pgmname", AV14Pgmname);
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         edtReferenciaINM_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtReferenciaINM_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtReferenciaINM_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7ReferenciaINM_Codigo) )
         {
            A709ReferenciaINM_Codigo = AV7ReferenciaINM_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A709ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A709ReferenciaINM_Codigo), 6, 0)));
         }
      }

      protected void standaloneModal( )
      {
         chkReferenciaINM_Ativo.Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkReferenciaINM_Ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkReferenciaINM_Ativo.Visible), 5, 0)));
         lblTextblockreferenciainm_ativo_Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockreferenciainm_ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockreferenciainm_ativo_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_ReferenciaINM_AreaTrabalhoCod) )
         {
            A712ReferenciaINM_AreaTrabalhoCod = AV11Insert_ReferenciaINM_AreaTrabalhoCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A712ReferenciaINM_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A712ReferenciaINM_AreaTrabalhoCod), 6, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A712ReferenciaINM_AreaTrabalhoCod) && ( Gx_BScreen == 0 ) )
            {
               A712ReferenciaINM_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A712ReferenciaINM_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A712ReferenciaINM_AreaTrabalhoCod), 6, 0)));
            }
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A711ReferenciaINM_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A711ReferenciaINM_Ativo = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A711ReferenciaINM_Ativo", A711ReferenciaINM_Ativo);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T002C7 */
            pr_default.execute(5, new Object[] {A712ReferenciaINM_AreaTrabalhoCod});
            A713ReferenciaINM_AreaTrabalhoDes = T002C7_A713ReferenciaINM_AreaTrabalhoDes[0];
            n713ReferenciaINM_AreaTrabalhoDes = T002C7_n713ReferenciaINM_AreaTrabalhoDes[0];
            pr_default.close(5);
         }
      }

      protected void Load2C90( )
      {
         /* Using cursor T002C8 */
         pr_default.execute(6, new Object[] {A709ReferenciaINM_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound90 = 1;
            A710ReferenciaINM_Descricao = T002C8_A710ReferenciaINM_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A710ReferenciaINM_Descricao", A710ReferenciaINM_Descricao);
            A713ReferenciaINM_AreaTrabalhoDes = T002C8_A713ReferenciaINM_AreaTrabalhoDes[0];
            n713ReferenciaINM_AreaTrabalhoDes = T002C8_n713ReferenciaINM_AreaTrabalhoDes[0];
            A711ReferenciaINM_Ativo = T002C8_A711ReferenciaINM_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A711ReferenciaINM_Ativo", A711ReferenciaINM_Ativo);
            A712ReferenciaINM_AreaTrabalhoCod = T002C8_A712ReferenciaINM_AreaTrabalhoCod[0];
            ZM2C90( -10) ;
         }
         pr_default.close(6);
         OnLoadActions2C90( ) ;
      }

      protected void OnLoadActions2C90( )
      {
      }

      protected void CheckExtendedTable2C90( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         /* Using cursor T002C7 */
         pr_default.execute(5, new Object[] {A712ReferenciaINM_AreaTrabalhoCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Referencia INM_Area Trabalho'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A713ReferenciaINM_AreaTrabalhoDes = T002C7_A713ReferenciaINM_AreaTrabalhoDes[0];
         n713ReferenciaINM_AreaTrabalhoDes = T002C7_n713ReferenciaINM_AreaTrabalhoDes[0];
         pr_default.close(5);
      }

      protected void CloseExtendedTableCursors2C90( )
      {
         pr_default.close(5);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_11( int A712ReferenciaINM_AreaTrabalhoCod )
      {
         /* Using cursor T002C9 */
         pr_default.execute(7, new Object[] {A712ReferenciaINM_AreaTrabalhoCod});
         if ( (pr_default.getStatus(7) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Referencia INM_Area Trabalho'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A713ReferenciaINM_AreaTrabalhoDes = T002C9_A713ReferenciaINM_AreaTrabalhoDes[0];
         n713ReferenciaINM_AreaTrabalhoDes = T002C9_n713ReferenciaINM_AreaTrabalhoDes[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A713ReferenciaINM_AreaTrabalhoDes)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(7) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(7);
      }

      protected void GetKey2C90( )
      {
         /* Using cursor T002C10 */
         pr_default.execute(8, new Object[] {A709ReferenciaINM_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound90 = 1;
         }
         else
         {
            RcdFound90 = 0;
         }
         pr_default.close(8);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T002C6 */
         pr_default.execute(4, new Object[] {A709ReferenciaINM_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            ZM2C90( 10) ;
            RcdFound90 = 1;
            A709ReferenciaINM_Codigo = T002C6_A709ReferenciaINM_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A709ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A709ReferenciaINM_Codigo), 6, 0)));
            A710ReferenciaINM_Descricao = T002C6_A710ReferenciaINM_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A710ReferenciaINM_Descricao", A710ReferenciaINM_Descricao);
            A711ReferenciaINM_Ativo = T002C6_A711ReferenciaINM_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A711ReferenciaINM_Ativo", A711ReferenciaINM_Ativo);
            A712ReferenciaINM_AreaTrabalhoCod = T002C6_A712ReferenciaINM_AreaTrabalhoCod[0];
            Z709ReferenciaINM_Codigo = A709ReferenciaINM_Codigo;
            sMode90 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load2C90( ) ;
            if ( AnyError == 1 )
            {
               RcdFound90 = 0;
               InitializeNonKey2C90( ) ;
            }
            Gx_mode = sMode90;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound90 = 0;
            InitializeNonKey2C90( ) ;
            sMode90 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode90;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(4);
      }

      protected void getEqualNoModal( )
      {
         GetKey2C90( ) ;
         if ( RcdFound90 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound90 = 0;
         /* Using cursor T002C11 */
         pr_default.execute(9, new Object[] {A709ReferenciaINM_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( T002C11_A709ReferenciaINM_Codigo[0] < A709ReferenciaINM_Codigo ) ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( T002C11_A709ReferenciaINM_Codigo[0] > A709ReferenciaINM_Codigo ) ) )
            {
               A709ReferenciaINM_Codigo = T002C11_A709ReferenciaINM_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A709ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A709ReferenciaINM_Codigo), 6, 0)));
               RcdFound90 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void move_previous( )
      {
         RcdFound90 = 0;
         /* Using cursor T002C12 */
         pr_default.execute(10, new Object[] {A709ReferenciaINM_Codigo});
         if ( (pr_default.getStatus(10) != 101) )
         {
            while ( (pr_default.getStatus(10) != 101) && ( ( T002C12_A709ReferenciaINM_Codigo[0] > A709ReferenciaINM_Codigo ) ) )
            {
               pr_default.readNext(10);
            }
            if ( (pr_default.getStatus(10) != 101) && ( ( T002C12_A709ReferenciaINM_Codigo[0] < A709ReferenciaINM_Codigo ) ) )
            {
               A709ReferenciaINM_Codigo = T002C12_A709ReferenciaINM_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A709ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A709ReferenciaINM_Codigo), 6, 0)));
               RcdFound90 = 1;
            }
         }
         pr_default.close(10);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey2C90( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtReferenciaINM_Descricao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert2C90( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound90 == 1 )
            {
               if ( A709ReferenciaINM_Codigo != Z709ReferenciaINM_Codigo )
               {
                  A709ReferenciaINM_Codigo = Z709ReferenciaINM_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A709ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A709ReferenciaINM_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "REFERENCIAINM_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtReferenciaINM_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtReferenciaINM_Descricao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update2C90( ) ;
                  GX_FocusControl = edtReferenciaINM_Descricao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A709ReferenciaINM_Codigo != Z709ReferenciaINM_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtReferenciaINM_Descricao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert2C90( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "REFERENCIAINM_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtReferenciaINM_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtReferenciaINM_Descricao_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert2C90( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A709ReferenciaINM_Codigo != Z709ReferenciaINM_Codigo )
         {
            A709ReferenciaINM_Codigo = Z709ReferenciaINM_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A709ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A709ReferenciaINM_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "REFERENCIAINM_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtReferenciaINM_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtReferenciaINM_Descricao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency2C90( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T002C5 */
            pr_default.execute(3, new Object[] {A709ReferenciaINM_Codigo});
            if ( (pr_default.getStatus(3) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ReferenciaINM"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(3) == 101) || ( StringUtil.StrCmp(Z710ReferenciaINM_Descricao, T002C5_A710ReferenciaINM_Descricao[0]) != 0 ) || ( Z711ReferenciaINM_Ativo != T002C5_A711ReferenciaINM_Ativo[0] ) || ( Z712ReferenciaINM_AreaTrabalhoCod != T002C5_A712ReferenciaINM_AreaTrabalhoCod[0] ) )
            {
               if ( StringUtil.StrCmp(Z710ReferenciaINM_Descricao, T002C5_A710ReferenciaINM_Descricao[0]) != 0 )
               {
                  GXUtil.WriteLog("referenciainm:[seudo value changed for attri]"+"ReferenciaINM_Descricao");
                  GXUtil.WriteLogRaw("Old: ",Z710ReferenciaINM_Descricao);
                  GXUtil.WriteLogRaw("Current: ",T002C5_A710ReferenciaINM_Descricao[0]);
               }
               if ( Z711ReferenciaINM_Ativo != T002C5_A711ReferenciaINM_Ativo[0] )
               {
                  GXUtil.WriteLog("referenciainm:[seudo value changed for attri]"+"ReferenciaINM_Ativo");
                  GXUtil.WriteLogRaw("Old: ",Z711ReferenciaINM_Ativo);
                  GXUtil.WriteLogRaw("Current: ",T002C5_A711ReferenciaINM_Ativo[0]);
               }
               if ( Z712ReferenciaINM_AreaTrabalhoCod != T002C5_A712ReferenciaINM_AreaTrabalhoCod[0] )
               {
                  GXUtil.WriteLog("referenciainm:[seudo value changed for attri]"+"ReferenciaINM_AreaTrabalhoCod");
                  GXUtil.WriteLogRaw("Old: ",Z712ReferenciaINM_AreaTrabalhoCod);
                  GXUtil.WriteLogRaw("Current: ",T002C5_A712ReferenciaINM_AreaTrabalhoCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ReferenciaINM"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2C90( )
      {
         BeforeValidate2C90( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2C90( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2C90( 0) ;
            CheckOptimisticConcurrency2C90( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2C90( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2C90( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002C13 */
                     pr_default.execute(11, new Object[] {A710ReferenciaINM_Descricao, A711ReferenciaINM_Ativo, A712ReferenciaINM_AreaTrabalhoCod});
                     A709ReferenciaINM_Codigo = T002C13_A709ReferenciaINM_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A709ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A709ReferenciaINM_Codigo), 6, 0)));
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("ReferenciaINM") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel2C90( ) ;
                           if ( AnyError == 0 )
                           {
                              /* Save values for previous() function. */
                              GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                              ResetCaption2C0( ) ;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2C90( ) ;
            }
            EndLevel2C90( ) ;
         }
         CloseExtendedTableCursors2C90( ) ;
      }

      protected void Update2C90( )
      {
         BeforeValidate2C90( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2C90( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2C90( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2C90( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2C90( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002C14 */
                     pr_default.execute(12, new Object[] {A710ReferenciaINM_Descricao, A711ReferenciaINM_Ativo, A712ReferenciaINM_AreaTrabalhoCod, A709ReferenciaINM_Codigo});
                     pr_default.close(12);
                     dsDefault.SmartCacheProvider.SetUpdated("ReferenciaINM") ;
                     if ( (pr_default.getStatus(12) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ReferenciaINM"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2C90( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel2C90( ) ;
                           if ( AnyError == 0 )
                           {
                              if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                              {
                                 if ( AnyError == 0 )
                                 {
                                    context.nUserReturn = 1;
                                 }
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2C90( ) ;
         }
         CloseExtendedTableCursors2C90( ) ;
      }

      protected void DeferredUpdate2C90( )
      {
      }

      protected void delete( )
      {
         BeforeValidate2C90( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2C90( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2C90( ) ;
            AfterConfirm2C90( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2C90( ) ;
               if ( AnyError == 0 )
               {
                  ScanStart2C234( ) ;
                  while ( RcdFound234 != 0 )
                  {
                     getByPrimaryKey2C234( ) ;
                     Delete2C234( ) ;
                     ScanNext2C234( ) ;
                  }
                  ScanEnd2C234( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002C15 */
                     pr_default.execute(13, new Object[] {A709ReferenciaINM_Codigo});
                     pr_default.close(13);
                     dsDefault.SmartCacheProvider.SetUpdated("ReferenciaINM") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( delete) rules */
                        /* End of After( delete) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
         sMode90 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel2C90( ) ;
         Gx_mode = sMode90;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls2C90( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T002C16 */
            pr_default.execute(14, new Object[] {A712ReferenciaINM_AreaTrabalhoCod});
            A713ReferenciaINM_AreaTrabalhoDes = T002C16_A713ReferenciaINM_AreaTrabalhoDes[0];
            n713ReferenciaINM_AreaTrabalhoDes = T002C16_n713ReferenciaINM_AreaTrabalhoDes[0];
            pr_default.close(14);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T002C17 */
            pr_default.execute(15, new Object[] {A709ReferenciaINM_Codigo});
            if ( (pr_default.getStatus(15) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Item N�o Mensuravel"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(15);
         }
      }

      protected void ProcessNestedLevel2C234( )
      {
         nGXsfl_29_idx = 0;
         while ( nGXsfl_29_idx < nRC_GXsfl_29 )
         {
            ReadRow2C234( ) ;
            if ( ( nRcdExists_234 != 0 ) || ( nIsMod_234 != 0 ) )
            {
               standaloneNotModal2C234( ) ;
               GetKey2C234( ) ;
               if ( ( nRcdExists_234 == 0 ) && ( nRcdDeleted_234 == 0 ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  Insert2C234( ) ;
               }
               else
               {
                  if ( RcdFound234 != 0 )
                  {
                     if ( ( nRcdDeleted_234 != 0 ) && ( nRcdExists_234 != 0 ) )
                     {
                        Gx_mode = "DLT";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                        Delete2C234( ) ;
                     }
                     else
                     {
                        if ( ( nIsMod_234 != 0 ) && ( nRcdExists_234 != 0 ) )
                        {
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                           Update2C234( ) ;
                        }
                     }
                  }
                  else
                  {
                     if ( nRcdDeleted_234 == 0 )
                     {
                        GXCCtl = "REFERENCIAINMANEXOS_CODIGO_" + sGXsfl_29_idx;
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, GXCCtl);
                        AnyError = 1;
                        GX_FocusControl = edtReferenciaINMAnexos_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
            ChangePostValue( edtReferenciaINMAnexos_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A2134ReferenciaINMAnexos_Codigo), 6, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z2134ReferenciaINMAnexos_Codigo_"+sGXsfl_29_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2134ReferenciaINMAnexos_Codigo), 6, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z2142ReferenciaINMAnexos_Data_"+sGXsfl_29_idx, context.localUtil.TToC( Z2142ReferenciaINMAnexos_Data, 10, 8, 0, 0, "/", ":", " ")) ;
            ChangePostValue( "ZT_"+"Z645TipoDocumento_Codigo_"+sGXsfl_29_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z645TipoDocumento_Codigo), 6, 0, ",", ""))) ;
            ChangePostValue( "nRcdDeleted_234_"+sGXsfl_29_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_234), 4, 0, ",", ""))) ;
            ChangePostValue( "nRcdExists_234_"+sGXsfl_29_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_234), 4, 0, ",", ""))) ;
            ChangePostValue( "nIsMod_234_"+sGXsfl_29_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_234), 4, 0, ",", ""))) ;
            if ( nIsMod_234 != 0 )
            {
               ChangePostValue( "REFERENCIAINMANEXOS_CODIGO_"+sGXsfl_29_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtReferenciaINMAnexos_Codigo_Enabled), 5, 0, ".", ""))) ;
            }
         }
         /* Start of After( level) rules */
         /* End of After( level) rules */
         InitAll2C234( ) ;
         if ( AnyError != 0 )
         {
         }
         nRcdExists_234 = 0;
         nIsMod_234 = 0;
         nRcdDeleted_234 = 0;
      }

      protected void ProcessLevel2C90( )
      {
         /* Save parent mode. */
         sMode90 = Gx_mode;
         ProcessNestedLevel2C234( ) ;
         if ( AnyError != 0 )
         {
         }
         /* Restore parent mode. */
         Gx_mode = sMode90;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         /* ' Update level parameters */
      }

      protected void EndLevel2C90( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(3);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete2C90( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(4);
            pr_default.close(1);
            pr_default.close(0);
            pr_default.close(14);
            pr_default.close(2);
            context.CommitDataStores( "ReferenciaINM");
            if ( AnyError == 0 )
            {
               ConfirmValues2C0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(4);
            pr_default.close(1);
            pr_default.close(0);
            pr_default.close(14);
            pr_default.close(2);
            context.RollbackDataStores( "ReferenciaINM");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart2C90( )
      {
         /* Scan By routine */
         /* Using cursor T002C18 */
         pr_default.execute(16);
         RcdFound90 = 0;
         if ( (pr_default.getStatus(16) != 101) )
         {
            RcdFound90 = 1;
            A709ReferenciaINM_Codigo = T002C18_A709ReferenciaINM_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A709ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A709ReferenciaINM_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext2C90( )
      {
         /* Scan next routine */
         pr_default.readNext(16);
         RcdFound90 = 0;
         if ( (pr_default.getStatus(16) != 101) )
         {
            RcdFound90 = 1;
            A709ReferenciaINM_Codigo = T002C18_A709ReferenciaINM_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A709ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A709ReferenciaINM_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd2C90( )
      {
         pr_default.close(16);
      }

      protected void AfterConfirm2C90( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert2C90( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate2C90( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete2C90( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2C90( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2C90( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes2C90( )
      {
         edtReferenciaINM_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtReferenciaINM_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtReferenciaINM_Descricao_Enabled), 5, 0)));
         chkReferenciaINM_Ativo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkReferenciaINM_Ativo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkReferenciaINM_Ativo.Enabled), 5, 0)));
         edtReferenciaINM_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtReferenciaINM_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtReferenciaINM_Codigo_Enabled), 5, 0)));
      }

      protected void ZM2C234( short GX_JID )
      {
         if ( ( GX_JID == 12 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z2142ReferenciaINMAnexos_Data = T002C3_A2142ReferenciaINMAnexos_Data[0];
               Z645TipoDocumento_Codigo = T002C3_A645TipoDocumento_Codigo[0];
            }
            else
            {
               Z2142ReferenciaINMAnexos_Data = A2142ReferenciaINMAnexos_Data;
               Z645TipoDocumento_Codigo = A645TipoDocumento_Codigo;
            }
         }
         if ( GX_JID == -12 )
         {
            Z709ReferenciaINM_Codigo = A709ReferenciaINM_Codigo;
            Z2134ReferenciaINMAnexos_Codigo = A2134ReferenciaINMAnexos_Codigo;
            Z2138ReferenciaINMAnexos_Descricao = A2138ReferenciaINMAnexos_Descricao;
            Z2139ReferenciaINMAnexos_Arquivo = A2139ReferenciaINMAnexos_Arquivo;
            Z2143ReferenciaINMAnexos_Link = A2143ReferenciaINMAnexos_Link;
            Z2142ReferenciaINMAnexos_Data = A2142ReferenciaINMAnexos_Data;
            Z2140ReferenciaINMAnexos_ArquivoTipo = A2140ReferenciaINMAnexos_ArquivoTipo;
            Z2141ReferenciaINMAnexos_ArquivoNome = A2141ReferenciaINMAnexos_ArquivoNome;
            Z645TipoDocumento_Codigo = A645TipoDocumento_Codigo;
            Z646TipoDocumento_Nome = A646TipoDocumento_Nome;
         }
      }

      protected void standaloneNotModal2C234( )
      {
      }

      protected void standaloneModal2C234( )
      {
         /* Using cursor T002C4 */
         pr_default.execute(2, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (0==A645TipoDocumento_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Tipo de Documentos'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A646TipoDocumento_Nome = T002C4_A646TipoDocumento_Nome[0];
         pr_default.close(2);
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            edtReferenciaINMAnexos_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtReferenciaINMAnexos_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtReferenciaINMAnexos_Codigo_Enabled), 5, 0)));
         }
         else
         {
            edtReferenciaINMAnexos_Codigo_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtReferenciaINMAnexos_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtReferenciaINMAnexos_Codigo_Enabled), 5, 0)));
         }
      }

      protected void Load2C234( )
      {
         /* Using cursor T002C19 */
         pr_default.execute(17, new Object[] {A709ReferenciaINM_Codigo, A2134ReferenciaINMAnexos_Codigo});
         if ( (pr_default.getStatus(17) != 101) )
         {
            RcdFound234 = 1;
            A2138ReferenciaINMAnexos_Descricao = T002C19_A2138ReferenciaINMAnexos_Descricao[0];
            A2143ReferenciaINMAnexos_Link = T002C19_A2143ReferenciaINMAnexos_Link[0];
            n2143ReferenciaINMAnexos_Link = T002C19_n2143ReferenciaINMAnexos_Link[0];
            A2142ReferenciaINMAnexos_Data = T002C19_A2142ReferenciaINMAnexos_Data[0];
            n2142ReferenciaINMAnexos_Data = T002C19_n2142ReferenciaINMAnexos_Data[0];
            A646TipoDocumento_Nome = T002C19_A646TipoDocumento_Nome[0];
            A2140ReferenciaINMAnexos_ArquivoTipo = T002C19_A2140ReferenciaINMAnexos_ArquivoTipo[0];
            n2140ReferenciaINMAnexos_ArquivoTipo = T002C19_n2140ReferenciaINMAnexos_ArquivoTipo[0];
            A2139ReferenciaINMAnexos_Arquivo_Filetype = A2140ReferenciaINMAnexos_ArquivoTipo;
            A2141ReferenciaINMAnexos_ArquivoNome = T002C19_A2141ReferenciaINMAnexos_ArquivoNome[0];
            n2141ReferenciaINMAnexos_ArquivoNome = T002C19_n2141ReferenciaINMAnexos_ArquivoNome[0];
            A2139ReferenciaINMAnexos_Arquivo_Filename = A2141ReferenciaINMAnexos_ArquivoNome;
            A645TipoDocumento_Codigo = T002C19_A645TipoDocumento_Codigo[0];
            n645TipoDocumento_Codigo = T002C19_n645TipoDocumento_Codigo[0];
            A2139ReferenciaINMAnexos_Arquivo = T002C19_A2139ReferenciaINMAnexos_Arquivo[0];
            n2139ReferenciaINMAnexos_Arquivo = T002C19_n2139ReferenciaINMAnexos_Arquivo[0];
            ZM2C234( -12) ;
         }
         pr_default.close(17);
         OnLoadActions2C234( ) ;
      }

      protected void OnLoadActions2C234( )
      {
      }

      protected void CheckExtendedTable2C234( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal2C234( ) ;
      }

      protected void CloseExtendedTableCursors2C234( )
      {
      }

      protected void enableDisable2C234( )
      {
      }

      protected void GetKey2C234( )
      {
         /* Using cursor T002C20 */
         pr_default.execute(18, new Object[] {A709ReferenciaINM_Codigo, A2134ReferenciaINMAnexos_Codigo});
         if ( (pr_default.getStatus(18) != 101) )
         {
            RcdFound234 = 1;
         }
         else
         {
            RcdFound234 = 0;
         }
         pr_default.close(18);
      }

      protected void getByPrimaryKey2C234( )
      {
         /* Using cursor T002C3 */
         pr_default.execute(1, new Object[] {A709ReferenciaINM_Codigo, A2134ReferenciaINMAnexos_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM2C234( 12) ;
            RcdFound234 = 1;
            InitializeNonKey2C234( ) ;
            A2134ReferenciaINMAnexos_Codigo = T002C3_A2134ReferenciaINMAnexos_Codigo[0];
            A2138ReferenciaINMAnexos_Descricao = T002C3_A2138ReferenciaINMAnexos_Descricao[0];
            A2143ReferenciaINMAnexos_Link = T002C3_A2143ReferenciaINMAnexos_Link[0];
            n2143ReferenciaINMAnexos_Link = T002C3_n2143ReferenciaINMAnexos_Link[0];
            A2142ReferenciaINMAnexos_Data = T002C3_A2142ReferenciaINMAnexos_Data[0];
            n2142ReferenciaINMAnexos_Data = T002C3_n2142ReferenciaINMAnexos_Data[0];
            A2140ReferenciaINMAnexos_ArquivoTipo = T002C3_A2140ReferenciaINMAnexos_ArquivoTipo[0];
            n2140ReferenciaINMAnexos_ArquivoTipo = T002C3_n2140ReferenciaINMAnexos_ArquivoTipo[0];
            A2139ReferenciaINMAnexos_Arquivo_Filetype = A2140ReferenciaINMAnexos_ArquivoTipo;
            A2141ReferenciaINMAnexos_ArquivoNome = T002C3_A2141ReferenciaINMAnexos_ArquivoNome[0];
            n2141ReferenciaINMAnexos_ArquivoNome = T002C3_n2141ReferenciaINMAnexos_ArquivoNome[0];
            A2139ReferenciaINMAnexos_Arquivo_Filename = A2141ReferenciaINMAnexos_ArquivoNome;
            A645TipoDocumento_Codigo = T002C3_A645TipoDocumento_Codigo[0];
            n645TipoDocumento_Codigo = T002C3_n645TipoDocumento_Codigo[0];
            A2139ReferenciaINMAnexos_Arquivo = T002C3_A2139ReferenciaINMAnexos_Arquivo[0];
            n2139ReferenciaINMAnexos_Arquivo = T002C3_n2139ReferenciaINMAnexos_Arquivo[0];
            Z709ReferenciaINM_Codigo = A709ReferenciaINM_Codigo;
            Z2134ReferenciaINMAnexos_Codigo = A2134ReferenciaINMAnexos_Codigo;
            sMode234 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load2C234( ) ;
            Gx_mode = sMode234;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound234 = 0;
            InitializeNonKey2C234( ) ;
            sMode234 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal2C234( ) ;
            Gx_mode = sMode234;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            DisableAttributes2C234( ) ;
         }
         pr_default.close(1);
      }

      protected void CheckOptimisticConcurrency2C234( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T002C2 */
            pr_default.execute(0, new Object[] {A709ReferenciaINM_Codigo, A2134ReferenciaINMAnexos_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ReferenciaINMAnexos"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z2142ReferenciaINMAnexos_Data != T002C2_A2142ReferenciaINMAnexos_Data[0] ) || ( Z645TipoDocumento_Codigo != T002C2_A645TipoDocumento_Codigo[0] ) )
            {
               if ( Z2142ReferenciaINMAnexos_Data != T002C2_A2142ReferenciaINMAnexos_Data[0] )
               {
                  GXUtil.WriteLog("referenciainm:[seudo value changed for attri]"+"ReferenciaINMAnexos_Data");
                  GXUtil.WriteLogRaw("Old: ",Z2142ReferenciaINMAnexos_Data);
                  GXUtil.WriteLogRaw("Current: ",T002C2_A2142ReferenciaINMAnexos_Data[0]);
               }
               if ( Z645TipoDocumento_Codigo != T002C2_A645TipoDocumento_Codigo[0] )
               {
                  GXUtil.WriteLog("referenciainm:[seudo value changed for attri]"+"TipoDocumento_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z645TipoDocumento_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T002C2_A645TipoDocumento_Codigo[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ReferenciaINMAnexos"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2C234( )
      {
         BeforeValidate2C234( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2C234( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2C234( 0) ;
            CheckOptimisticConcurrency2C234( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2C234( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2C234( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002C21 */
                     A2140ReferenciaINMAnexos_ArquivoTipo = A2139ReferenciaINMAnexos_Arquivo_Filetype;
                     n2140ReferenciaINMAnexos_ArquivoTipo = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2140ReferenciaINMAnexos_ArquivoTipo", A2140ReferenciaINMAnexos_ArquivoTipo);
                     A2141ReferenciaINMAnexos_ArquivoNome = A2139ReferenciaINMAnexos_Arquivo_Filename;
                     n2141ReferenciaINMAnexos_ArquivoNome = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2141ReferenciaINMAnexos_ArquivoNome", A2141ReferenciaINMAnexos_ArquivoNome);
                     pr_default.execute(19, new Object[] {A709ReferenciaINM_Codigo, A2134ReferenciaINMAnexos_Codigo, A2138ReferenciaINMAnexos_Descricao, n2139ReferenciaINMAnexos_Arquivo, A2139ReferenciaINMAnexos_Arquivo, n2143ReferenciaINMAnexos_Link, A2143ReferenciaINMAnexos_Link, n2142ReferenciaINMAnexos_Data, A2142ReferenciaINMAnexos_Data, n2140ReferenciaINMAnexos_ArquivoTipo, A2140ReferenciaINMAnexos_ArquivoTipo, n2141ReferenciaINMAnexos_ArquivoNome, A2141ReferenciaINMAnexos_ArquivoNome, n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
                     pr_default.close(19);
                     dsDefault.SmartCacheProvider.SetUpdated("ReferenciaINMAnexos") ;
                     if ( (pr_default.getStatus(19) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2C234( ) ;
            }
            EndLevel2C234( ) ;
         }
         CloseExtendedTableCursors2C234( ) ;
      }

      protected void Update2C234( )
      {
         BeforeValidate2C234( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2C234( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2C234( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2C234( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2C234( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002C22 */
                     A2140ReferenciaINMAnexos_ArquivoTipo = A2139ReferenciaINMAnexos_Arquivo_Filetype;
                     n2140ReferenciaINMAnexos_ArquivoTipo = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2140ReferenciaINMAnexos_ArquivoTipo", A2140ReferenciaINMAnexos_ArquivoTipo);
                     A2141ReferenciaINMAnexos_ArquivoNome = A2139ReferenciaINMAnexos_Arquivo_Filename;
                     n2141ReferenciaINMAnexos_ArquivoNome = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2141ReferenciaINMAnexos_ArquivoNome", A2141ReferenciaINMAnexos_ArquivoNome);
                     pr_default.execute(20, new Object[] {A2138ReferenciaINMAnexos_Descricao, n2143ReferenciaINMAnexos_Link, A2143ReferenciaINMAnexos_Link, n2142ReferenciaINMAnexos_Data, A2142ReferenciaINMAnexos_Data, n2140ReferenciaINMAnexos_ArquivoTipo, A2140ReferenciaINMAnexos_ArquivoTipo, n2141ReferenciaINMAnexos_ArquivoNome, A2141ReferenciaINMAnexos_ArquivoNome, n645TipoDocumento_Codigo, A645TipoDocumento_Codigo, A709ReferenciaINM_Codigo, A2134ReferenciaINMAnexos_Codigo});
                     pr_default.close(20);
                     dsDefault.SmartCacheProvider.SetUpdated("ReferenciaINMAnexos") ;
                     if ( (pr_default.getStatus(20) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ReferenciaINMAnexos"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2C234( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey2C234( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2C234( ) ;
         }
         CloseExtendedTableCursors2C234( ) ;
      }

      protected void DeferredUpdate2C234( )
      {
         if ( AnyError == 0 )
         {
            /* Using cursor T002C23 */
            pr_default.execute(21, new Object[] {n2139ReferenciaINMAnexos_Arquivo, A2139ReferenciaINMAnexos_Arquivo, A709ReferenciaINM_Codigo, A2134ReferenciaINMAnexos_Codigo});
            pr_default.close(21);
            dsDefault.SmartCacheProvider.SetUpdated("ReferenciaINMAnexos") ;
         }
      }

      protected void Delete2C234( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         BeforeValidate2C234( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2C234( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2C234( ) ;
            AfterConfirm2C234( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2C234( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T002C24 */
                  pr_default.execute(22, new Object[] {A709ReferenciaINM_Codigo, A2134ReferenciaINMAnexos_Codigo});
                  pr_default.close(22);
                  dsDefault.SmartCacheProvider.SetUpdated("ReferenciaINMAnexos") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode234 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel2C234( ) ;
         Gx_mode = sMode234;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls2C234( )
      {
         standaloneModal2C234( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel2C234( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart2C234( )
      {
         /* Scan By routine */
         /* Using cursor T002C25 */
         pr_default.execute(23, new Object[] {A709ReferenciaINM_Codigo});
         RcdFound234 = 0;
         if ( (pr_default.getStatus(23) != 101) )
         {
            RcdFound234 = 1;
            A2134ReferenciaINMAnexos_Codigo = T002C25_A2134ReferenciaINMAnexos_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext2C234( )
      {
         /* Scan next routine */
         pr_default.readNext(23);
         RcdFound234 = 0;
         if ( (pr_default.getStatus(23) != 101) )
         {
            RcdFound234 = 1;
            A2134ReferenciaINMAnexos_Codigo = T002C25_A2134ReferenciaINMAnexos_Codigo[0];
         }
      }

      protected void ScanEnd2C234( )
      {
         pr_default.close(23);
      }

      protected void AfterConfirm2C234( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert2C234( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate2C234( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete2C234( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2C234( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2C234( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes2C234( )
      {
         edtReferenciaINMAnexos_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtReferenciaINMAnexos_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtReferenciaINMAnexos_Codigo_Enabled), 5, 0)));
      }

      protected void SubsflControlProps_29234( )
      {
         edtReferenciaINMAnexos_Codigo_Internalname = "REFERENCIAINMANEXOS_CODIGO_"+sGXsfl_29_idx;
      }

      protected void SubsflControlProps_fel_29234( )
      {
         edtReferenciaINMAnexos_Codigo_Internalname = "REFERENCIAINMANEXOS_CODIGO_"+sGXsfl_29_fel_idx;
      }

      protected void AddRow2C234( )
      {
         nGXsfl_29_idx = (short)(nGXsfl_29_idx+1);
         sGXsfl_29_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_29_idx), 4, 0)), 4, "0");
         SubsflControlProps_29234( ) ;
         SendRow2C234( ) ;
      }

      protected void SendRow2C234( )
      {
         GridanexosRow = GXWebRow.GetNew(context);
         if ( subGridanexos_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGridanexos_Backstyle = 0;
            if ( StringUtil.StrCmp(subGridanexos_Class, "") != 0 )
            {
               subGridanexos_Linesclass = subGridanexos_Class+"Odd";
            }
         }
         else if ( subGridanexos_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGridanexos_Backstyle = 0;
            subGridanexos_Backcolor = subGridanexos_Allbackcolor;
            if ( StringUtil.StrCmp(subGridanexos_Class, "") != 0 )
            {
               subGridanexos_Linesclass = subGridanexos_Class+"Uniform";
            }
         }
         else if ( subGridanexos_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGridanexos_Backstyle = 1;
            if ( StringUtil.StrCmp(subGridanexos_Class, "") != 0 )
            {
               subGridanexos_Linesclass = subGridanexos_Class+"Odd";
            }
            subGridanexos_Backcolor = (int)(0xFFFFFF);
         }
         else if ( subGridanexos_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGridanexos_Backstyle = 1;
            if ( ((int)((nGXsfl_29_idx) % (2))) == 0 )
            {
               subGridanexos_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGridanexos_Class, "") != 0 )
               {
                  subGridanexos_Linesclass = subGridanexos_Class+"Even";
               }
            }
            else
            {
               subGridanexos_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGridanexos_Class, "") != 0 )
               {
                  subGridanexos_Linesclass = subGridanexos_Class+"Odd";
               }
            }
         }
         /* Subfile cell */
         /* Single line edit */
         TempTags = " gxoch1=\"gx.fn.setControlValue('nIsMod_234_" + sGXsfl_29_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 30,'',false,'" + sGXsfl_29_idx + "',29)\"";
         ROClassString = "BootstrapAttribute";
         GridanexosRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtReferenciaINMAnexos_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A2134ReferenciaINMAnexos_Codigo), 6, 0, ",", "")),StringUtil.LTrim( context.localUtil.Format( (decimal)(A2134ReferenciaINMAnexos_Codigo), "ZZZZZ9")),TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,30);\"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtReferenciaINMAnexos_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtReferenciaINMAnexos_Codigo_Enabled,(short)1,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)29,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
         context.httpAjaxContext.ajax_sending_grid_row(GridanexosRow);
         GXCCtl = "Z2134ReferenciaINMAnexos_Codigo_" + sGXsfl_29_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2134ReferenciaINMAnexos_Codigo), 6, 0, ",", "")));
         GXCCtl = "Z2142ReferenciaINMAnexos_Data_" + sGXsfl_29_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, context.localUtil.TToC( Z2142ReferenciaINMAnexos_Data, 10, 8, 0, 0, "/", ":", " "));
         GXCCtl = "Z645TipoDocumento_Codigo_" + sGXsfl_29_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z645TipoDocumento_Codigo), 6, 0, ",", "")));
         GXCCtl = "nRcdDeleted_234_" + sGXsfl_29_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_234), 4, 0, ",", "")));
         GXCCtl = "nRcdExists_234_" + sGXsfl_29_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_234), 4, 0, ",", "")));
         GXCCtl = "nIsMod_234_" + sGXsfl_29_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_234), 4, 0, ",", "")));
         GXCCtl = "vMODE_" + sGXsfl_29_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.RTrim( Gx_mode));
         GXCCtl = "vTRNCONTEXT_" + sGXsfl_29_idx;
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, GXCCtl, AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(GXCCtl, AV9TrnContext);
         }
         GXCCtl = "vREFERENCIAINM_CODIGO_" + sGXsfl_29_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ReferenciaINM_Codigo), 6, 0, ",", "")));
         GXCCtl = "TIPODOCUMENTO_NOME_" + sGXsfl_29_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.RTrim( A646TipoDocumento_Nome));
         GXCCtl = "REFERENCIAINMANEXOS_ARQUIVO_Filetype_" + sGXsfl_29_idx;
         GXCCtl = "REFERENCIAINMANEXOS_ARQUIVO_Filename_" + sGXsfl_29_idx;
         GxWebStd.gx_hidden_field( context, "REFERENCIAINMANEXOS_CODIGO_"+sGXsfl_29_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtReferenciaINMAnexos_Codigo_Enabled), 5, 0, ".", "")));
         context.httpAjaxContext.ajax_sending_grid_row(null);
         GridanexosContainer.AddRow(GridanexosRow);
      }

      protected void ReadRow2C234( )
      {
         nGXsfl_29_idx = (short)(nGXsfl_29_idx+1);
         sGXsfl_29_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_29_idx), 4, 0)), 4, "0");
         SubsflControlProps_29234( ) ;
         edtReferenciaINMAnexos_Codigo_Enabled = (int)(context.localUtil.CToN( cgiGet( "REFERENCIAINMANEXOS_CODIGO_"+sGXsfl_29_idx+"Enabled"), ",", "."));
         GXCCtl = "REFERENCIAINMANEXOS_ARQUIVO_Filetype_" + sGXsfl_29_idx;
         GXCCtl = "REFERENCIAINMANEXOS_ARQUIVO_Filename_" + sGXsfl_29_idx;
         if ( ( ( context.localUtil.CToN( cgiGet( edtReferenciaINMAnexos_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtReferenciaINMAnexos_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
         {
            GXCCtl = "REFERENCIAINMANEXOS_CODIGO_" + sGXsfl_29_idx;
            GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtReferenciaINMAnexos_Codigo_Internalname;
            wbErr = true;
            A2134ReferenciaINMAnexos_Codigo = 0;
         }
         else
         {
            A2134ReferenciaINMAnexos_Codigo = (int)(context.localUtil.CToN( cgiGet( edtReferenciaINMAnexos_Codigo_Internalname), ",", "."));
         }
         GXCCtl = "Z2134ReferenciaINMAnexos_Codigo_" + sGXsfl_29_idx;
         Z2134ReferenciaINMAnexos_Codigo = (int)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "Z2142ReferenciaINMAnexos_Data_" + sGXsfl_29_idx;
         Z2142ReferenciaINMAnexos_Data = context.localUtil.CToT( cgiGet( GXCCtl), 0);
         n2142ReferenciaINMAnexos_Data = ((DateTime.MinValue==A2142ReferenciaINMAnexos_Data) ? true : false);
         GXCCtl = "Z645TipoDocumento_Codigo_" + sGXsfl_29_idx;
         Z645TipoDocumento_Codigo = (int)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         n645TipoDocumento_Codigo = ((0==A645TipoDocumento_Codigo) ? true : false);
         GXCCtl = "Z2142ReferenciaINMAnexos_Data_" + sGXsfl_29_idx;
         A2142ReferenciaINMAnexos_Data = context.localUtil.CToT( cgiGet( GXCCtl), 0);
         n2142ReferenciaINMAnexos_Data = false;
         n2142ReferenciaINMAnexos_Data = ((DateTime.MinValue==A2142ReferenciaINMAnexos_Data) ? true : false);
         GXCCtl = "Z645TipoDocumento_Codigo_" + sGXsfl_29_idx;
         A645TipoDocumento_Codigo = (int)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         n645TipoDocumento_Codigo = false;
         n645TipoDocumento_Codigo = ((0==A645TipoDocumento_Codigo) ? true : false);
         GXCCtl = "nRcdDeleted_234_" + sGXsfl_29_idx;
         nRcdDeleted_234 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "nRcdExists_234_" + sGXsfl_29_idx;
         nRcdExists_234 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "nIsMod_234_" + sGXsfl_29_idx;
         nIsMod_234 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "TIPODOCUMENTO_NOME_" + sGXsfl_29_idx;
         A646TipoDocumento_Nome = cgiGet( GXCCtl);
      }

      protected void assign_properties_default( )
      {
         defedtReferenciaINMAnexos_Codigo_Enabled = edtReferenciaINMAnexos_Codigo_Enabled;
      }

      protected void ConfirmValues2C0( )
      {
         nGXsfl_29_idx = 0;
         sGXsfl_29_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_29_idx), 4, 0)), 4, "0");
         SubsflControlProps_29234( ) ;
         while ( nGXsfl_29_idx < nRC_GXsfl_29 )
         {
            nGXsfl_29_idx = (short)(nGXsfl_29_idx+1);
            sGXsfl_29_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_29_idx), 4, 0)), 4, "0");
            SubsflControlProps_29234( ) ;
            ChangePostValue( "Z2134ReferenciaINMAnexos_Codigo_"+sGXsfl_29_idx, cgiGet( "ZT_"+"Z2134ReferenciaINMAnexos_Codigo_"+sGXsfl_29_idx)) ;
            DeletePostValue( "ZT_"+"Z2134ReferenciaINMAnexos_Codigo_"+sGXsfl_29_idx) ;
            ChangePostValue( "Z2142ReferenciaINMAnexos_Data_"+sGXsfl_29_idx, cgiGet( "ZT_"+"Z2142ReferenciaINMAnexos_Data_"+sGXsfl_29_idx)) ;
            DeletePostValue( "ZT_"+"Z2142ReferenciaINMAnexos_Data_"+sGXsfl_29_idx) ;
            ChangePostValue( "Z645TipoDocumento_Codigo_"+sGXsfl_29_idx, cgiGet( "ZT_"+"Z645TipoDocumento_Codigo_"+sGXsfl_29_idx)) ;
            DeletePostValue( "ZT_"+"Z645TipoDocumento_Codigo_"+sGXsfl_29_idx) ;
         }
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202051812512253");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("referenciainm.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ReferenciaINM_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z709ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z709ReferenciaINM_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z710ReferenciaINM_Descricao", Z710ReferenciaINM_Descricao);
         GxWebStd.gx_boolean_hidden_field( context, "Z711ReferenciaINM_Ativo", Z711ReferenciaINM_Ativo);
         GxWebStd.gx_hidden_field( context, "Z712ReferenciaINM_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z712ReferenciaINM_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_29", StringUtil.LTrim( StringUtil.NToC( (decimal)(nGXsfl_29_idx), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N712ReferenciaINM_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A712ReferenciaINM_AreaTrabalhoCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vREFERENCIAINM_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ReferenciaINM_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_REFERENCIAINM_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_ReferenciaINM_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "REFERENCIAINM_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A712ReferenciaINM_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "REFERENCIAINM_AREATRABALHODES", A713ReferenciaINM_AreaTrabalhoDes);
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV14Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "REFERENCIAINMANEXOS_DESCRICAO", A2138ReferenciaINMAnexos_Descricao);
         GxWebStd.gx_hidden_field( context, "REFERENCIAINMANEXOS_ARQUIVO", A2139ReferenciaINMAnexos_Arquivo);
         GxWebStd.gx_hidden_field( context, "REFERENCIAINMANEXOS_LINK", A2143ReferenciaINMAnexos_Link);
         GxWebStd.gx_hidden_field( context, "REFERENCIAINMANEXOS_DATA", context.localUtil.TToC( A2142ReferenciaINMAnexos_Data, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "REFERENCIAINMANEXOS_ARQUIVOTIPO", StringUtil.RTrim( A2140ReferenciaINMAnexos_ArquivoTipo));
         GxWebStd.gx_hidden_field( context, "REFERENCIAINMANEXOS_ARQUIVONOME", StringUtil.RTrim( A2141ReferenciaINMAnexos_ArquivoNome));
         GxWebStd.gx_hidden_field( context, "TIPODOCUMENTO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A645TipoDocumento_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "TIPODOCUMENTO_NOME", StringUtil.RTrim( A646TipoDocumento_Nome));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vREFERENCIAINM_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ReferenciaINM_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ReferenciaINM";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A709ReferenciaINM_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A712ReferenciaINM_AreaTrabalhoCod), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("referenciainm:[SendSecurityCheck value for]"+"ReferenciaINM_Codigo:"+context.localUtil.Format( (decimal)(A709ReferenciaINM_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("referenciainm:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("referenciainm:[SendSecurityCheck value for]"+"ReferenciaINM_AreaTrabalhoCod:"+context.localUtil.Format( (decimal)(A712ReferenciaINM_AreaTrabalhoCod), "ZZZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("referenciainm.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ReferenciaINM_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "ReferenciaINM" ;
      }

      public override String GetPgmdesc( )
      {
         return "Referencia INM" ;
      }

      protected void InitializeNonKey2C90( )
      {
         A710ReferenciaINM_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A710ReferenciaINM_Descricao", A710ReferenciaINM_Descricao);
         A713ReferenciaINM_AreaTrabalhoDes = "";
         n713ReferenciaINM_AreaTrabalhoDes = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A713ReferenciaINM_AreaTrabalhoDes", A713ReferenciaINM_AreaTrabalhoDes);
         A712ReferenciaINM_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A712ReferenciaINM_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A712ReferenciaINM_AreaTrabalhoCod), 6, 0)));
         A711ReferenciaINM_Ativo = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A711ReferenciaINM_Ativo", A711ReferenciaINM_Ativo);
         Z710ReferenciaINM_Descricao = "";
         Z711ReferenciaINM_Ativo = false;
         Z712ReferenciaINM_AreaTrabalhoCod = 0;
      }

      protected void InitAll2C90( )
      {
         A709ReferenciaINM_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A709ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A709ReferenciaINM_Codigo), 6, 0)));
         InitializeNonKey2C90( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A712ReferenciaINM_AreaTrabalhoCod = i712ReferenciaINM_AreaTrabalhoCod;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A712ReferenciaINM_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A712ReferenciaINM_AreaTrabalhoCod), 6, 0)));
         A711ReferenciaINM_Ativo = i711ReferenciaINM_Ativo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A711ReferenciaINM_Ativo", A711ReferenciaINM_Ativo);
      }

      protected void InitializeNonKey2C234( )
      {
         A2138ReferenciaINMAnexos_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2138ReferenciaINMAnexos_Descricao", A2138ReferenciaINMAnexos_Descricao);
         A2139ReferenciaINMAnexos_Arquivo = "";
         n2139ReferenciaINMAnexos_Arquivo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2139ReferenciaINMAnexos_Arquivo", A2139ReferenciaINMAnexos_Arquivo);
         A2143ReferenciaINMAnexos_Link = "";
         n2143ReferenciaINMAnexos_Link = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2143ReferenciaINMAnexos_Link", A2143ReferenciaINMAnexos_Link);
         A2142ReferenciaINMAnexos_Data = (DateTime)(DateTime.MinValue);
         n2142ReferenciaINMAnexos_Data = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2142ReferenciaINMAnexos_Data", context.localUtil.TToC( A2142ReferenciaINMAnexos_Data, 8, 5, 0, 3, "/", ":", " "));
         A645TipoDocumento_Codigo = 0;
         n645TipoDocumento_Codigo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A645TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0)));
         A646TipoDocumento_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A646TipoDocumento_Nome", A646TipoDocumento_Nome);
         A2140ReferenciaINMAnexos_ArquivoTipo = "";
         n2140ReferenciaINMAnexos_ArquivoTipo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2140ReferenciaINMAnexos_ArquivoTipo", A2140ReferenciaINMAnexos_ArquivoTipo);
         A2141ReferenciaINMAnexos_ArquivoNome = "";
         n2141ReferenciaINMAnexos_ArquivoNome = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2141ReferenciaINMAnexos_ArquivoNome", A2141ReferenciaINMAnexos_ArquivoNome);
         Z2142ReferenciaINMAnexos_Data = (DateTime)(DateTime.MinValue);
         Z645TipoDocumento_Codigo = 0;
      }

      protected void InitAll2C234( )
      {
         A2134ReferenciaINMAnexos_Codigo = 0;
         InitializeNonKey2C234( ) ;
      }

      protected void StandaloneModalInsert2C234( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202051812512288");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("referenciainm.js", "?202051812512288");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_level_properties234( )
      {
         edtReferenciaINMAnexos_Codigo_Enabled = defedtReferenciaINMAnexos_Codigo_Enabled;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtReferenciaINMAnexos_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtReferenciaINMAnexos_Codigo_Enabled), 5, 0)));
      }

      protected void init_default_properties( )
      {
         lblReferenciainmtitle_Internalname = "REFERENCIAINMTITLE";
         lblTextblockreferenciainm_descricao_Internalname = "TEXTBLOCKREFERENCIAINM_DESCRICAO";
         edtReferenciaINM_Descricao_Internalname = "REFERENCIAINM_DESCRICAO";
         lblTextblockreferenciainm_ativo_Internalname = "TEXTBLOCKREFERENCIAINM_ATIVO";
         chkReferenciaINM_Ativo_Internalname = "REFERENCIAINM_ATIVO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         edtReferenciaINMAnexos_Codigo_Internalname = "REFERENCIAINMANEXOS_CODIGO";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtReferenciaINM_Codigo_Internalname = "REFERENCIAINM_CODIGO";
         Form.Internalname = "FORM";
         subGridanexos_Internalname = "GRIDANEXOS";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Refer�ncia INM";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Referencia INM";
         edtReferenciaINMAnexos_Codigo_Jsonclick = "";
         subGridanexos_Class = "WorkWithBorder WorkWith";
         chkReferenciaINM_Ativo.Enabled = 1;
         chkReferenciaINM_Ativo.Visible = 1;
         lblTextblockreferenciainm_ativo_Visible = 1;
         edtReferenciaINM_Descricao_Jsonclick = "";
         edtReferenciaINM_Descricao_Enabled = 1;
         subGridanexos_Visible = 1;
         subGridanexos_Allowcollapsing = 0;
         subGridanexos_Allowselection = 0;
         edtReferenciaINMAnexos_Codigo_Enabled = 1;
         subGridanexos_Backcolorstyle = 3;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtReferenciaINM_Codigo_Jsonclick = "";
         edtReferenciaINM_Codigo_Enabled = 0;
         edtReferenciaINM_Codigo_Visible = 1;
         chkReferenciaINM_Ativo.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGridanexos_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         Gx_mode = "INS";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         SubsflControlProps_29234( ) ;
         while ( nGXsfl_29_idx <= nRC_GXsfl_29 )
         {
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            standaloneNotModal2C234( ) ;
            standaloneModal2C234( ) ;
            chkReferenciaINM_Ativo.Name = "REFERENCIAINM_ATIVO";
            chkReferenciaINM_Ativo.WebTags = "";
            chkReferenciaINM_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkReferenciaINM_Ativo_Internalname, "TitleCaption", chkReferenciaINM_Ativo.Caption);
            chkReferenciaINM_Ativo.CheckedValue = "false";
            dynload_actions( ) ;
            SendRow2C234( ) ;
            nGXsfl_29_idx = (short)(nGXsfl_29_idx+1);
            sGXsfl_29_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_29_idx), 4, 0)), 4, "0");
            SubsflControlProps_29234( ) ;
         }
         context.GX_webresponse.AddString(GridanexosContainer.ToJavascriptSource());
         /* End function gxnrGridanexos_newrow */
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7ReferenciaINM_Codigo',fld:'vREFERENCIAINM_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E122C2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(4);
         pr_default.close(14);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z710ReferenciaINM_Descricao = "";
         Z2142ReferenciaINMAnexos_Data = (DateTime)(DateTime.MinValue);
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         lblReferenciainmtitle_Jsonclick = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         GridanexosContainer = new GXWebGrid( context);
         GridanexosColumn = new GXWebColumn();
         sMode234 = "";
         lblTextblockreferenciainm_descricao_Jsonclick = "";
         A710ReferenciaINM_Descricao = "";
         lblTextblockreferenciainm_ativo_Jsonclick = "";
         A713ReferenciaINM_AreaTrabalhoDes = "";
         AV14Pgmname = "";
         A2138ReferenciaINMAnexos_Descricao = "";
         A2139ReferenciaINMAnexos_Arquivo = "";
         A2143ReferenciaINMAnexos_Link = "";
         A2142ReferenciaINMAnexos_Data = (DateTime)(DateTime.MinValue);
         A2140ReferenciaINMAnexos_ArquivoTipo = "";
         A2141ReferenciaINMAnexos_ArquivoNome = "";
         A646TipoDocumento_Nome = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode90 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         GXCCtl = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z713ReferenciaINM_AreaTrabalhoDes = "";
         T002C7_A713ReferenciaINM_AreaTrabalhoDes = new String[] {""} ;
         T002C7_n713ReferenciaINM_AreaTrabalhoDes = new bool[] {false} ;
         T002C8_A709ReferenciaINM_Codigo = new int[1] ;
         T002C8_A710ReferenciaINM_Descricao = new String[] {""} ;
         T002C8_A713ReferenciaINM_AreaTrabalhoDes = new String[] {""} ;
         T002C8_n713ReferenciaINM_AreaTrabalhoDes = new bool[] {false} ;
         T002C8_A711ReferenciaINM_Ativo = new bool[] {false} ;
         T002C8_A712ReferenciaINM_AreaTrabalhoCod = new int[1] ;
         T002C9_A713ReferenciaINM_AreaTrabalhoDes = new String[] {""} ;
         T002C9_n713ReferenciaINM_AreaTrabalhoDes = new bool[] {false} ;
         T002C10_A709ReferenciaINM_Codigo = new int[1] ;
         T002C6_A709ReferenciaINM_Codigo = new int[1] ;
         T002C6_A710ReferenciaINM_Descricao = new String[] {""} ;
         T002C6_A711ReferenciaINM_Ativo = new bool[] {false} ;
         T002C6_A712ReferenciaINM_AreaTrabalhoCod = new int[1] ;
         T002C11_A709ReferenciaINM_Codigo = new int[1] ;
         T002C12_A709ReferenciaINM_Codigo = new int[1] ;
         T002C5_A709ReferenciaINM_Codigo = new int[1] ;
         T002C5_A710ReferenciaINM_Descricao = new String[] {""} ;
         T002C5_A711ReferenciaINM_Ativo = new bool[] {false} ;
         T002C5_A712ReferenciaINM_AreaTrabalhoCod = new int[1] ;
         T002C13_A709ReferenciaINM_Codigo = new int[1] ;
         T002C16_A713ReferenciaINM_AreaTrabalhoDes = new String[] {""} ;
         T002C16_n713ReferenciaINM_AreaTrabalhoDes = new bool[] {false} ;
         T002C17_A718ItemNaoMensuravel_AreaTrabalhoCod = new int[1] ;
         T002C17_A715ItemNaoMensuravel_Codigo = new String[] {""} ;
         T002C18_A709ReferenciaINM_Codigo = new int[1] ;
         Z2138ReferenciaINMAnexos_Descricao = "";
         Z2139ReferenciaINMAnexos_Arquivo = "";
         Z2143ReferenciaINMAnexos_Link = "";
         Z2140ReferenciaINMAnexos_ArquivoTipo = "";
         Z2141ReferenciaINMAnexos_ArquivoNome = "";
         Z646TipoDocumento_Nome = "";
         T002C4_A646TipoDocumento_Nome = new String[] {""} ;
         T002C19_A709ReferenciaINM_Codigo = new int[1] ;
         T002C19_A2134ReferenciaINMAnexos_Codigo = new int[1] ;
         T002C19_A2138ReferenciaINMAnexos_Descricao = new String[] {""} ;
         T002C19_A2143ReferenciaINMAnexos_Link = new String[] {""} ;
         T002C19_n2143ReferenciaINMAnexos_Link = new bool[] {false} ;
         T002C19_A2142ReferenciaINMAnexos_Data = new DateTime[] {DateTime.MinValue} ;
         T002C19_n2142ReferenciaINMAnexos_Data = new bool[] {false} ;
         T002C19_A646TipoDocumento_Nome = new String[] {""} ;
         T002C19_A2140ReferenciaINMAnexos_ArquivoTipo = new String[] {""} ;
         T002C19_n2140ReferenciaINMAnexos_ArquivoTipo = new bool[] {false} ;
         T002C19_A2141ReferenciaINMAnexos_ArquivoNome = new String[] {""} ;
         T002C19_n2141ReferenciaINMAnexos_ArquivoNome = new bool[] {false} ;
         T002C19_A645TipoDocumento_Codigo = new int[1] ;
         T002C19_n645TipoDocumento_Codigo = new bool[] {false} ;
         T002C19_A2139ReferenciaINMAnexos_Arquivo = new String[] {""} ;
         T002C19_n2139ReferenciaINMAnexos_Arquivo = new bool[] {false} ;
         A2139ReferenciaINMAnexos_Arquivo_Filetype = "";
         A2139ReferenciaINMAnexos_Arquivo_Filename = "";
         T002C20_A709ReferenciaINM_Codigo = new int[1] ;
         T002C20_A2134ReferenciaINMAnexos_Codigo = new int[1] ;
         T002C3_A709ReferenciaINM_Codigo = new int[1] ;
         T002C3_A2134ReferenciaINMAnexos_Codigo = new int[1] ;
         T002C3_A2138ReferenciaINMAnexos_Descricao = new String[] {""} ;
         T002C3_A2143ReferenciaINMAnexos_Link = new String[] {""} ;
         T002C3_n2143ReferenciaINMAnexos_Link = new bool[] {false} ;
         T002C3_A2142ReferenciaINMAnexos_Data = new DateTime[] {DateTime.MinValue} ;
         T002C3_n2142ReferenciaINMAnexos_Data = new bool[] {false} ;
         T002C3_A2140ReferenciaINMAnexos_ArquivoTipo = new String[] {""} ;
         T002C3_n2140ReferenciaINMAnexos_ArquivoTipo = new bool[] {false} ;
         T002C3_A2141ReferenciaINMAnexos_ArquivoNome = new String[] {""} ;
         T002C3_n2141ReferenciaINMAnexos_ArquivoNome = new bool[] {false} ;
         T002C3_A645TipoDocumento_Codigo = new int[1] ;
         T002C3_n645TipoDocumento_Codigo = new bool[] {false} ;
         T002C3_A2139ReferenciaINMAnexos_Arquivo = new String[] {""} ;
         T002C3_n2139ReferenciaINMAnexos_Arquivo = new bool[] {false} ;
         T002C2_A709ReferenciaINM_Codigo = new int[1] ;
         T002C2_A2134ReferenciaINMAnexos_Codigo = new int[1] ;
         T002C2_A2138ReferenciaINMAnexos_Descricao = new String[] {""} ;
         T002C2_A2143ReferenciaINMAnexos_Link = new String[] {""} ;
         T002C2_n2143ReferenciaINMAnexos_Link = new bool[] {false} ;
         T002C2_A2142ReferenciaINMAnexos_Data = new DateTime[] {DateTime.MinValue} ;
         T002C2_n2142ReferenciaINMAnexos_Data = new bool[] {false} ;
         T002C2_A2140ReferenciaINMAnexos_ArquivoTipo = new String[] {""} ;
         T002C2_n2140ReferenciaINMAnexos_ArquivoTipo = new bool[] {false} ;
         T002C2_A2141ReferenciaINMAnexos_ArquivoNome = new String[] {""} ;
         T002C2_n2141ReferenciaINMAnexos_ArquivoNome = new bool[] {false} ;
         T002C2_A645TipoDocumento_Codigo = new int[1] ;
         T002C2_n645TipoDocumento_Codigo = new bool[] {false} ;
         T002C2_A2139ReferenciaINMAnexos_Arquivo = new String[] {""} ;
         T002C2_n2139ReferenciaINMAnexos_Arquivo = new bool[] {false} ;
         T002C25_A709ReferenciaINM_Codigo = new int[1] ;
         T002C25_A2134ReferenciaINMAnexos_Codigo = new int[1] ;
         GridanexosRow = new GXWebRow();
         subGridanexos_Linesclass = "";
         ROClassString = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.referenciainm__default(),
            new Object[][] {
                new Object[] {
               T002C2_A709ReferenciaINM_Codigo, T002C2_A2134ReferenciaINMAnexos_Codigo, T002C2_A2138ReferenciaINMAnexos_Descricao, T002C2_A2143ReferenciaINMAnexos_Link, T002C2_n2143ReferenciaINMAnexos_Link, T002C2_A2142ReferenciaINMAnexos_Data, T002C2_n2142ReferenciaINMAnexos_Data, T002C2_A2140ReferenciaINMAnexos_ArquivoTipo, T002C2_n2140ReferenciaINMAnexos_ArquivoTipo, T002C2_A2141ReferenciaINMAnexos_ArquivoNome,
               T002C2_n2141ReferenciaINMAnexos_ArquivoNome, T002C2_A645TipoDocumento_Codigo, T002C2_n645TipoDocumento_Codigo, T002C2_A2139ReferenciaINMAnexos_Arquivo, T002C2_n2139ReferenciaINMAnexos_Arquivo
               }
               , new Object[] {
               T002C3_A709ReferenciaINM_Codigo, T002C3_A2134ReferenciaINMAnexos_Codigo, T002C3_A2138ReferenciaINMAnexos_Descricao, T002C3_A2143ReferenciaINMAnexos_Link, T002C3_n2143ReferenciaINMAnexos_Link, T002C3_A2142ReferenciaINMAnexos_Data, T002C3_n2142ReferenciaINMAnexos_Data, T002C3_A2140ReferenciaINMAnexos_ArquivoTipo, T002C3_n2140ReferenciaINMAnexos_ArquivoTipo, T002C3_A2141ReferenciaINMAnexos_ArquivoNome,
               T002C3_n2141ReferenciaINMAnexos_ArquivoNome, T002C3_A645TipoDocumento_Codigo, T002C3_n645TipoDocumento_Codigo, T002C3_A2139ReferenciaINMAnexos_Arquivo, T002C3_n2139ReferenciaINMAnexos_Arquivo
               }
               , new Object[] {
               T002C4_A646TipoDocumento_Nome
               }
               , new Object[] {
               T002C5_A709ReferenciaINM_Codigo, T002C5_A710ReferenciaINM_Descricao, T002C5_A711ReferenciaINM_Ativo, T002C5_A712ReferenciaINM_AreaTrabalhoCod
               }
               , new Object[] {
               T002C6_A709ReferenciaINM_Codigo, T002C6_A710ReferenciaINM_Descricao, T002C6_A711ReferenciaINM_Ativo, T002C6_A712ReferenciaINM_AreaTrabalhoCod
               }
               , new Object[] {
               T002C7_A713ReferenciaINM_AreaTrabalhoDes, T002C7_n713ReferenciaINM_AreaTrabalhoDes
               }
               , new Object[] {
               T002C8_A709ReferenciaINM_Codigo, T002C8_A710ReferenciaINM_Descricao, T002C8_A713ReferenciaINM_AreaTrabalhoDes, T002C8_n713ReferenciaINM_AreaTrabalhoDes, T002C8_A711ReferenciaINM_Ativo, T002C8_A712ReferenciaINM_AreaTrabalhoCod
               }
               , new Object[] {
               T002C9_A713ReferenciaINM_AreaTrabalhoDes, T002C9_n713ReferenciaINM_AreaTrabalhoDes
               }
               , new Object[] {
               T002C10_A709ReferenciaINM_Codigo
               }
               , new Object[] {
               T002C11_A709ReferenciaINM_Codigo
               }
               , new Object[] {
               T002C12_A709ReferenciaINM_Codigo
               }
               , new Object[] {
               T002C13_A709ReferenciaINM_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T002C16_A713ReferenciaINM_AreaTrabalhoDes, T002C16_n713ReferenciaINM_AreaTrabalhoDes
               }
               , new Object[] {
               T002C17_A718ItemNaoMensuravel_AreaTrabalhoCod, T002C17_A715ItemNaoMensuravel_Codigo
               }
               , new Object[] {
               T002C18_A709ReferenciaINM_Codigo
               }
               , new Object[] {
               T002C19_A709ReferenciaINM_Codigo, T002C19_A2134ReferenciaINMAnexos_Codigo, T002C19_A2138ReferenciaINMAnexos_Descricao, T002C19_A2143ReferenciaINMAnexos_Link, T002C19_n2143ReferenciaINMAnexos_Link, T002C19_A2142ReferenciaINMAnexos_Data, T002C19_n2142ReferenciaINMAnexos_Data, T002C19_A646TipoDocumento_Nome, T002C19_A2140ReferenciaINMAnexos_ArquivoTipo, T002C19_n2140ReferenciaINMAnexos_ArquivoTipo,
               T002C19_A2141ReferenciaINMAnexos_ArquivoNome, T002C19_n2141ReferenciaINMAnexos_ArquivoNome, T002C19_A645TipoDocumento_Codigo, T002C19_n645TipoDocumento_Codigo, T002C19_A2139ReferenciaINMAnexos_Arquivo, T002C19_n2139ReferenciaINMAnexos_Arquivo
               }
               , new Object[] {
               T002C20_A709ReferenciaINM_Codigo, T002C20_A2134ReferenciaINMAnexos_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T002C25_A709ReferenciaINM_Codigo, T002C25_A2134ReferenciaINMAnexos_Codigo
               }
            }
         );
         Z711ReferenciaINM_Ativo = true;
         A711ReferenciaINM_Ativo = true;
         i711ReferenciaINM_Ativo = true;
         AV14Pgmname = "ReferenciaINM";
         Z712ReferenciaINM_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         N712ReferenciaINM_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         i712ReferenciaINM_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         A712ReferenciaINM_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
      }

      private short nRC_GXsfl_29 ;
      private short nGXsfl_29_idx=1 ;
      private short nRcdDeleted_234 ;
      private short nRcdExists_234 ;
      private short nIsMod_234 ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short subGridanexos_Backcolorstyle ;
      private short subGridanexos_Allowselection ;
      private short subGridanexos_Allowhovering ;
      private short subGridanexos_Allowcollapsing ;
      private short subGridanexos_Collapsed ;
      private short nBlankRcdCount234 ;
      private short RcdFound234 ;
      private short nBlankRcdUsr234 ;
      private short Gx_BScreen ;
      private short RcdFound90 ;
      private short GX_JID ;
      private short subGridanexos_Backstyle ;
      private short gxajaxcallmode ;
      private int wcpOAV7ReferenciaINM_Codigo ;
      private int Z709ReferenciaINM_Codigo ;
      private int Z712ReferenciaINM_AreaTrabalhoCod ;
      private int N712ReferenciaINM_AreaTrabalhoCod ;
      private int Z2134ReferenciaINMAnexos_Codigo ;
      private int Z645TipoDocumento_Codigo ;
      private int A712ReferenciaINM_AreaTrabalhoCod ;
      private int AV7ReferenciaINM_Codigo ;
      private int trnEnded ;
      private int A709ReferenciaINM_Codigo ;
      private int edtReferenciaINM_Codigo_Enabled ;
      private int edtReferenciaINM_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int subGridanexos_Visible ;
      private int A2134ReferenciaINMAnexos_Codigo ;
      private int edtReferenciaINMAnexos_Codigo_Enabled ;
      private int subGridanexos_Selectioncolor ;
      private int subGridanexos_Hoveringcolor ;
      private int fRowAdded ;
      private int edtReferenciaINM_Descricao_Enabled ;
      private int lblTextblockreferenciainm_ativo_Visible ;
      private int AV11Insert_ReferenciaINM_AreaTrabalhoCod ;
      private int A645TipoDocumento_Codigo ;
      private int AV15GXV1 ;
      private int subGridanexos_Backcolor ;
      private int subGridanexos_Allbackcolor ;
      private int defedtReferenciaINMAnexos_Codigo_Enabled ;
      private int i712ReferenciaINM_AreaTrabalhoCod ;
      private int idxLst ;
      private long GRIDANEXOS_nFirstRecordOnPage ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_29_idx="0001" ;
      private String Gx_mode ;
      private String GXKey ;
      private String chkReferenciaINM_Ativo_Internalname ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtReferenciaINM_Descricao_Internalname ;
      private String edtReferenciaINM_Codigo_Internalname ;
      private String edtReferenciaINM_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String lblReferenciainmtitle_Internalname ;
      private String lblReferenciainmtitle_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String sMode234 ;
      private String edtReferenciaINMAnexos_Codigo_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockreferenciainm_descricao_Internalname ;
      private String lblTextblockreferenciainm_descricao_Jsonclick ;
      private String edtReferenciaINM_Descricao_Jsonclick ;
      private String lblTextblockreferenciainm_ativo_Internalname ;
      private String lblTextblockreferenciainm_ativo_Jsonclick ;
      private String AV14Pgmname ;
      private String A2140ReferenciaINMAnexos_ArquivoTipo ;
      private String A2141ReferenciaINMAnexos_ArquivoNome ;
      private String A646TipoDocumento_Nome ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode90 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String GXCCtl ;
      private String Dvpanel_tableattributes_Internalname ;
      private String Z2140ReferenciaINMAnexos_ArquivoTipo ;
      private String Z2141ReferenciaINMAnexos_ArquivoNome ;
      private String Z646TipoDocumento_Nome ;
      private String A2139ReferenciaINMAnexos_Arquivo_Filetype ;
      private String A2139ReferenciaINMAnexos_Arquivo_Filename ;
      private String sGXsfl_29_fel_idx="0001" ;
      private String subGridanexos_Class ;
      private String subGridanexos_Linesclass ;
      private String ROClassString ;
      private String edtReferenciaINMAnexos_Codigo_Jsonclick ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String subGridanexos_Internalname ;
      private DateTime Z2142ReferenciaINMAnexos_Data ;
      private DateTime A2142ReferenciaINMAnexos_Data ;
      private bool Z711ReferenciaINM_Ativo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool A711ReferenciaINM_Ativo ;
      private bool n713ReferenciaINM_AreaTrabalhoDes ;
      private bool n2139ReferenciaINMAnexos_Arquivo ;
      private bool n2143ReferenciaINMAnexos_Link ;
      private bool n2142ReferenciaINMAnexos_Data ;
      private bool n2140ReferenciaINMAnexos_ArquivoTipo ;
      private bool n2141ReferenciaINMAnexos_ArquivoNome ;
      private bool n645TipoDocumento_Codigo ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool i711ReferenciaINM_Ativo ;
      private String A2138ReferenciaINMAnexos_Descricao ;
      private String A2143ReferenciaINMAnexos_Link ;
      private String Z2138ReferenciaINMAnexos_Descricao ;
      private String Z2143ReferenciaINMAnexos_Link ;
      private String Z710ReferenciaINM_Descricao ;
      private String A710ReferenciaINM_Descricao ;
      private String A713ReferenciaINM_AreaTrabalhoDes ;
      private String Z713ReferenciaINM_AreaTrabalhoDes ;
      private String A2139ReferenciaINMAnexos_Arquivo ;
      private String Z2139ReferenciaINMAnexos_Arquivo ;
      private IGxSession AV10WebSession ;
      private GXWebGrid GridanexosContainer ;
      private GXWebRow GridanexosRow ;
      private GXWebColumn GridanexosColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCheckbox chkReferenciaINM_Ativo ;
      private IDataStoreProvider pr_default ;
      private String[] T002C7_A713ReferenciaINM_AreaTrabalhoDes ;
      private bool[] T002C7_n713ReferenciaINM_AreaTrabalhoDes ;
      private int[] T002C8_A709ReferenciaINM_Codigo ;
      private String[] T002C8_A710ReferenciaINM_Descricao ;
      private String[] T002C8_A713ReferenciaINM_AreaTrabalhoDes ;
      private bool[] T002C8_n713ReferenciaINM_AreaTrabalhoDes ;
      private bool[] T002C8_A711ReferenciaINM_Ativo ;
      private int[] T002C8_A712ReferenciaINM_AreaTrabalhoCod ;
      private String[] T002C9_A713ReferenciaINM_AreaTrabalhoDes ;
      private bool[] T002C9_n713ReferenciaINM_AreaTrabalhoDes ;
      private int[] T002C10_A709ReferenciaINM_Codigo ;
      private int[] T002C6_A709ReferenciaINM_Codigo ;
      private String[] T002C6_A710ReferenciaINM_Descricao ;
      private bool[] T002C6_A711ReferenciaINM_Ativo ;
      private int[] T002C6_A712ReferenciaINM_AreaTrabalhoCod ;
      private int[] T002C11_A709ReferenciaINM_Codigo ;
      private int[] T002C12_A709ReferenciaINM_Codigo ;
      private int[] T002C5_A709ReferenciaINM_Codigo ;
      private String[] T002C5_A710ReferenciaINM_Descricao ;
      private bool[] T002C5_A711ReferenciaINM_Ativo ;
      private int[] T002C5_A712ReferenciaINM_AreaTrabalhoCod ;
      private int[] T002C13_A709ReferenciaINM_Codigo ;
      private String[] T002C16_A713ReferenciaINM_AreaTrabalhoDes ;
      private bool[] T002C16_n713ReferenciaINM_AreaTrabalhoDes ;
      private int[] T002C17_A718ItemNaoMensuravel_AreaTrabalhoCod ;
      private String[] T002C17_A715ItemNaoMensuravel_Codigo ;
      private int[] T002C18_A709ReferenciaINM_Codigo ;
      private String[] T002C4_A646TipoDocumento_Nome ;
      private int[] T002C19_A709ReferenciaINM_Codigo ;
      private int[] T002C19_A2134ReferenciaINMAnexos_Codigo ;
      private String[] T002C19_A2138ReferenciaINMAnexos_Descricao ;
      private String[] T002C19_A2143ReferenciaINMAnexos_Link ;
      private bool[] T002C19_n2143ReferenciaINMAnexos_Link ;
      private DateTime[] T002C19_A2142ReferenciaINMAnexos_Data ;
      private bool[] T002C19_n2142ReferenciaINMAnexos_Data ;
      private String[] T002C19_A646TipoDocumento_Nome ;
      private String[] T002C19_A2140ReferenciaINMAnexos_ArquivoTipo ;
      private bool[] T002C19_n2140ReferenciaINMAnexos_ArquivoTipo ;
      private String[] T002C19_A2141ReferenciaINMAnexos_ArquivoNome ;
      private bool[] T002C19_n2141ReferenciaINMAnexos_ArquivoNome ;
      private int[] T002C19_A645TipoDocumento_Codigo ;
      private bool[] T002C19_n645TipoDocumento_Codigo ;
      private String[] T002C19_A2139ReferenciaINMAnexos_Arquivo ;
      private bool[] T002C19_n2139ReferenciaINMAnexos_Arquivo ;
      private int[] T002C20_A709ReferenciaINM_Codigo ;
      private int[] T002C20_A2134ReferenciaINMAnexos_Codigo ;
      private int[] T002C3_A709ReferenciaINM_Codigo ;
      private int[] T002C3_A2134ReferenciaINMAnexos_Codigo ;
      private String[] T002C3_A2138ReferenciaINMAnexos_Descricao ;
      private String[] T002C3_A2143ReferenciaINMAnexos_Link ;
      private bool[] T002C3_n2143ReferenciaINMAnexos_Link ;
      private DateTime[] T002C3_A2142ReferenciaINMAnexos_Data ;
      private bool[] T002C3_n2142ReferenciaINMAnexos_Data ;
      private String[] T002C3_A2140ReferenciaINMAnexos_ArquivoTipo ;
      private bool[] T002C3_n2140ReferenciaINMAnexos_ArquivoTipo ;
      private String[] T002C3_A2141ReferenciaINMAnexos_ArquivoNome ;
      private bool[] T002C3_n2141ReferenciaINMAnexos_ArquivoNome ;
      private int[] T002C3_A645TipoDocumento_Codigo ;
      private bool[] T002C3_n645TipoDocumento_Codigo ;
      private String[] T002C3_A2139ReferenciaINMAnexos_Arquivo ;
      private bool[] T002C3_n2139ReferenciaINMAnexos_Arquivo ;
      private int[] T002C2_A709ReferenciaINM_Codigo ;
      private int[] T002C2_A2134ReferenciaINMAnexos_Codigo ;
      private String[] T002C2_A2138ReferenciaINMAnexos_Descricao ;
      private String[] T002C2_A2143ReferenciaINMAnexos_Link ;
      private bool[] T002C2_n2143ReferenciaINMAnexos_Link ;
      private DateTime[] T002C2_A2142ReferenciaINMAnexos_Data ;
      private bool[] T002C2_n2142ReferenciaINMAnexos_Data ;
      private String[] T002C2_A2140ReferenciaINMAnexos_ArquivoTipo ;
      private bool[] T002C2_n2140ReferenciaINMAnexos_ArquivoTipo ;
      private String[] T002C2_A2141ReferenciaINMAnexos_ArquivoNome ;
      private bool[] T002C2_n2141ReferenciaINMAnexos_ArquivoNome ;
      private int[] T002C2_A645TipoDocumento_Codigo ;
      private bool[] T002C2_n645TipoDocumento_Codigo ;
      private String[] T002C2_A2139ReferenciaINMAnexos_Arquivo ;
      private bool[] T002C2_n2139ReferenciaINMAnexos_Arquivo ;
      private int[] T002C25_A709ReferenciaINM_Codigo ;
      private int[] T002C25_A2134ReferenciaINMAnexos_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class referenciainm__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new UpdateCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new UpdateCursor(def[19])
         ,new UpdateCursor(def[20])
         ,new UpdateCursor(def[21])
         ,new UpdateCursor(def[22])
         ,new ForEachCursor(def[23])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT002C8 ;
          prmT002C8 = new Object[] {
          new Object[] {"@ReferenciaINM_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002C7 ;
          prmT002C7 = new Object[] {
          new Object[] {"@ReferenciaINM_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002C9 ;
          prmT002C9 = new Object[] {
          new Object[] {"@ReferenciaINM_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002C10 ;
          prmT002C10 = new Object[] {
          new Object[] {"@ReferenciaINM_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002C6 ;
          prmT002C6 = new Object[] {
          new Object[] {"@ReferenciaINM_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002C11 ;
          prmT002C11 = new Object[] {
          new Object[] {"@ReferenciaINM_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002C12 ;
          prmT002C12 = new Object[] {
          new Object[] {"@ReferenciaINM_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002C5 ;
          prmT002C5 = new Object[] {
          new Object[] {"@ReferenciaINM_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002C13 ;
          prmT002C13 = new Object[] {
          new Object[] {"@ReferenciaINM_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ReferenciaINM_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@ReferenciaINM_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002C14 ;
          prmT002C14 = new Object[] {
          new Object[] {"@ReferenciaINM_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ReferenciaINM_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@ReferenciaINM_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ReferenciaINM_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002C15 ;
          prmT002C15 = new Object[] {
          new Object[] {"@ReferenciaINM_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002C16 ;
          prmT002C16 = new Object[] {
          new Object[] {"@ReferenciaINM_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002C17 ;
          prmT002C17 = new Object[] {
          new Object[] {"@ReferenciaINM_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002C18 ;
          prmT002C18 = new Object[] {
          } ;
          Object[] prmT002C4 ;
          prmT002C4 = new Object[] {
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002C19 ;
          prmT002C19 = new Object[] {
          new Object[] {"@ReferenciaINM_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ReferenciaINMAnexos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002C20 ;
          prmT002C20 = new Object[] {
          new Object[] {"@ReferenciaINM_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ReferenciaINMAnexos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002C3 ;
          prmT002C3 = new Object[] {
          new Object[] {"@ReferenciaINM_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ReferenciaINMAnexos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002C2 ;
          prmT002C2 = new Object[] {
          new Object[] {"@ReferenciaINM_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ReferenciaINMAnexos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002C21 ;
          prmT002C21 = new Object[] {
          new Object[] {"@ReferenciaINM_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ReferenciaINMAnexos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ReferenciaINMAnexos_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ReferenciaINMAnexos_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@ReferenciaINMAnexos_Link",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@ReferenciaINMAnexos_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ReferenciaINMAnexos_ArquivoTipo",SqlDbType.Char,10,0} ,
          new Object[] {"@ReferenciaINMAnexos_ArquivoNome",SqlDbType.Char,50,0} ,
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002C22 ;
          prmT002C22 = new Object[] {
          new Object[] {"@ReferenciaINMAnexos_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ReferenciaINMAnexos_Link",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@ReferenciaINMAnexos_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ReferenciaINMAnexos_ArquivoTipo",SqlDbType.Char,10,0} ,
          new Object[] {"@ReferenciaINMAnexos_ArquivoNome",SqlDbType.Char,50,0} ,
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ReferenciaINM_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ReferenciaINMAnexos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002C23 ;
          prmT002C23 = new Object[] {
          new Object[] {"@ReferenciaINMAnexos_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@ReferenciaINM_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ReferenciaINMAnexos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002C24 ;
          prmT002C24 = new Object[] {
          new Object[] {"@ReferenciaINM_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ReferenciaINMAnexos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002C25 ;
          prmT002C25 = new Object[] {
          new Object[] {"@ReferenciaINM_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T002C2", "SELECT [ReferenciaINM_Codigo], [ReferenciaINMAnexos_Codigo], [ReferenciaINMAnexos_Descricao], [ReferenciaINMAnexos_Link], [ReferenciaINMAnexos_Data], [ReferenciaINMAnexos_ArquivoTipo], [ReferenciaINMAnexos_ArquivoNome], [TipoDocumento_Codigo], [ReferenciaINMAnexos_Arquivo] FROM [ReferenciaINMAnexos] WITH (UPDLOCK) WHERE [ReferenciaINM_Codigo] = @ReferenciaINM_Codigo AND [ReferenciaINMAnexos_Codigo] = @ReferenciaINMAnexos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002C2,1,0,true,false )
             ,new CursorDef("T002C3", "SELECT [ReferenciaINM_Codigo], [ReferenciaINMAnexos_Codigo], [ReferenciaINMAnexos_Descricao], [ReferenciaINMAnexos_Link], [ReferenciaINMAnexos_Data], [ReferenciaINMAnexos_ArquivoTipo], [ReferenciaINMAnexos_ArquivoNome], [TipoDocumento_Codigo], [ReferenciaINMAnexos_Arquivo] FROM [ReferenciaINMAnexos] WITH (NOLOCK) WHERE [ReferenciaINM_Codigo] = @ReferenciaINM_Codigo AND [ReferenciaINMAnexos_Codigo] = @ReferenciaINMAnexos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002C3,1,0,true,false )
             ,new CursorDef("T002C4", "SELECT [TipoDocumento_Nome] FROM [TipoDocumento] WITH (NOLOCK) WHERE [TipoDocumento_Codigo] = @TipoDocumento_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002C4,1,0,true,false )
             ,new CursorDef("T002C5", "SELECT [ReferenciaINM_Codigo], [ReferenciaINM_Descricao], [ReferenciaINM_Ativo], [ReferenciaINM_AreaTrabalhoCod] AS ReferenciaINM_AreaTrabalhoCod FROM [ReferenciaINM] WITH (UPDLOCK) WHERE [ReferenciaINM_Codigo] = @ReferenciaINM_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002C5,1,0,true,false )
             ,new CursorDef("T002C6", "SELECT [ReferenciaINM_Codigo], [ReferenciaINM_Descricao], [ReferenciaINM_Ativo], [ReferenciaINM_AreaTrabalhoCod] AS ReferenciaINM_AreaTrabalhoCod FROM [ReferenciaINM] WITH (NOLOCK) WHERE [ReferenciaINM_Codigo] = @ReferenciaINM_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002C6,1,0,true,false )
             ,new CursorDef("T002C7", "SELECT [AreaTrabalho_Descricao] AS ReferenciaINM_AreaTrabalhoDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @ReferenciaINM_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002C7,1,0,true,false )
             ,new CursorDef("T002C8", "SELECT TM1.[ReferenciaINM_Codigo], TM1.[ReferenciaINM_Descricao], T2.[AreaTrabalho_Descricao] AS ReferenciaINM_AreaTrabalhoDes, TM1.[ReferenciaINM_Ativo], TM1.[ReferenciaINM_AreaTrabalhoCod] AS ReferenciaINM_AreaTrabalhoCod FROM ([ReferenciaINM] TM1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = TM1.[ReferenciaINM_AreaTrabalhoCod]) WHERE TM1.[ReferenciaINM_Codigo] = @ReferenciaINM_Codigo ORDER BY TM1.[ReferenciaINM_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002C8,100,0,true,false )
             ,new CursorDef("T002C9", "SELECT [AreaTrabalho_Descricao] AS ReferenciaINM_AreaTrabalhoDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @ReferenciaINM_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002C9,1,0,true,false )
             ,new CursorDef("T002C10", "SELECT [ReferenciaINM_Codigo] FROM [ReferenciaINM] WITH (NOLOCK) WHERE [ReferenciaINM_Codigo] = @ReferenciaINM_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002C10,1,0,true,false )
             ,new CursorDef("T002C11", "SELECT TOP 1 [ReferenciaINM_Codigo] FROM [ReferenciaINM] WITH (NOLOCK) WHERE ( [ReferenciaINM_Codigo] > @ReferenciaINM_Codigo) ORDER BY [ReferenciaINM_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002C11,1,0,true,true )
             ,new CursorDef("T002C12", "SELECT TOP 1 [ReferenciaINM_Codigo] FROM [ReferenciaINM] WITH (NOLOCK) WHERE ( [ReferenciaINM_Codigo] < @ReferenciaINM_Codigo) ORDER BY [ReferenciaINM_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002C12,1,0,true,true )
             ,new CursorDef("T002C13", "INSERT INTO [ReferenciaINM]([ReferenciaINM_Descricao], [ReferenciaINM_Ativo], [ReferenciaINM_AreaTrabalhoCod]) VALUES(@ReferenciaINM_Descricao, @ReferenciaINM_Ativo, @ReferenciaINM_AreaTrabalhoCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT002C13)
             ,new CursorDef("T002C14", "UPDATE [ReferenciaINM] SET [ReferenciaINM_Descricao]=@ReferenciaINM_Descricao, [ReferenciaINM_Ativo]=@ReferenciaINM_Ativo, [ReferenciaINM_AreaTrabalhoCod]=@ReferenciaINM_AreaTrabalhoCod  WHERE [ReferenciaINM_Codigo] = @ReferenciaINM_Codigo", GxErrorMask.GX_NOMASK,prmT002C14)
             ,new CursorDef("T002C15", "DELETE FROM [ReferenciaINM]  WHERE [ReferenciaINM_Codigo] = @ReferenciaINM_Codigo", GxErrorMask.GX_NOMASK,prmT002C15)
             ,new CursorDef("T002C16", "SELECT [AreaTrabalho_Descricao] AS ReferenciaINM_AreaTrabalhoDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @ReferenciaINM_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002C16,1,0,true,false )
             ,new CursorDef("T002C17", "SELECT TOP 1 [ItemNaoMensuravel_AreaTrabalhoCod], [ItemNaoMensuravel_Codigo] FROM [ItemNaoMensuravel] WITH (NOLOCK) WHERE [ReferenciaINM_Codigo] = @ReferenciaINM_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002C17,1,0,true,true )
             ,new CursorDef("T002C18", "SELECT [ReferenciaINM_Codigo] FROM [ReferenciaINM] WITH (NOLOCK) ORDER BY [ReferenciaINM_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002C18,100,0,true,false )
             ,new CursorDef("T002C19", "SELECT T1.[ReferenciaINM_Codigo], T1.[ReferenciaINMAnexos_Codigo], T1.[ReferenciaINMAnexos_Descricao], T1.[ReferenciaINMAnexos_Link], T1.[ReferenciaINMAnexos_Data], T2.[TipoDocumento_Nome], T1.[ReferenciaINMAnexos_ArquivoTipo], T1.[ReferenciaINMAnexos_ArquivoNome], T1.[TipoDocumento_Codigo], T1.[ReferenciaINMAnexos_Arquivo] FROM ([ReferenciaINMAnexos] T1 WITH (NOLOCK) LEFT JOIN [TipoDocumento] T2 WITH (NOLOCK) ON T2.[TipoDocumento_Codigo] = T1.[TipoDocumento_Codigo]) WHERE T1.[ReferenciaINM_Codigo] = @ReferenciaINM_Codigo and T1.[ReferenciaINMAnexos_Codigo] = @ReferenciaINMAnexos_Codigo ORDER BY T1.[ReferenciaINM_Codigo], T1.[ReferenciaINMAnexos_Codigo] ",true, GxErrorMask.GX_NOMASK, false, this,prmT002C19,11,0,true,false )
             ,new CursorDef("T002C20", "SELECT [ReferenciaINM_Codigo], [ReferenciaINMAnexos_Codigo] FROM [ReferenciaINMAnexos] WITH (NOLOCK) WHERE [ReferenciaINM_Codigo] = @ReferenciaINM_Codigo AND [ReferenciaINMAnexos_Codigo] = @ReferenciaINMAnexos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002C20,1,0,true,false )
             ,new CursorDef("T002C21", "INSERT INTO [ReferenciaINMAnexos]([ReferenciaINM_Codigo], [ReferenciaINMAnexos_Codigo], [ReferenciaINMAnexos_Descricao], [ReferenciaINMAnexos_Arquivo], [ReferenciaINMAnexos_Link], [ReferenciaINMAnexos_Data], [ReferenciaINMAnexos_ArquivoTipo], [ReferenciaINMAnexos_ArquivoNome], [TipoDocumento_Codigo]) VALUES(@ReferenciaINM_Codigo, @ReferenciaINMAnexos_Codigo, @ReferenciaINMAnexos_Descricao, @ReferenciaINMAnexos_Arquivo, @ReferenciaINMAnexos_Link, @ReferenciaINMAnexos_Data, @ReferenciaINMAnexos_ArquivoTipo, @ReferenciaINMAnexos_ArquivoNome, @TipoDocumento_Codigo)", GxErrorMask.GX_NOMASK,prmT002C21)
             ,new CursorDef("T002C22", "UPDATE [ReferenciaINMAnexos] SET [ReferenciaINMAnexos_Descricao]=@ReferenciaINMAnexos_Descricao, [ReferenciaINMAnexos_Link]=@ReferenciaINMAnexos_Link, [ReferenciaINMAnexos_Data]=@ReferenciaINMAnexos_Data, [ReferenciaINMAnexos_ArquivoTipo]=@ReferenciaINMAnexos_ArquivoTipo, [ReferenciaINMAnexos_ArquivoNome]=@ReferenciaINMAnexos_ArquivoNome, [TipoDocumento_Codigo]=@TipoDocumento_Codigo  WHERE [ReferenciaINM_Codigo] = @ReferenciaINM_Codigo AND [ReferenciaINMAnexos_Codigo] = @ReferenciaINMAnexos_Codigo", GxErrorMask.GX_NOMASK,prmT002C22)
             ,new CursorDef("T002C23", "UPDATE [ReferenciaINMAnexos] SET [ReferenciaINMAnexos_Arquivo]=@ReferenciaINMAnexos_Arquivo  WHERE [ReferenciaINM_Codigo] = @ReferenciaINM_Codigo AND [ReferenciaINMAnexos_Codigo] = @ReferenciaINMAnexos_Codigo", GxErrorMask.GX_NOMASK,prmT002C23)
             ,new CursorDef("T002C24", "DELETE FROM [ReferenciaINMAnexos]  WHERE [ReferenciaINM_Codigo] = @ReferenciaINM_Codigo AND [ReferenciaINMAnexos_Codigo] = @ReferenciaINMAnexos_Codigo", GxErrorMask.GX_NOMASK,prmT002C24)
             ,new CursorDef("T002C25", "SELECT [ReferenciaINM_Codigo], [ReferenciaINMAnexos_Codigo] FROM [ReferenciaINMAnexos] WITH (NOLOCK) WHERE [ReferenciaINM_Codigo] = @ReferenciaINM_Codigo ORDER BY [ReferenciaINM_Codigo], [ReferenciaINMAnexos_Codigo] ",true, GxErrorMask.GX_NOMASK, false, this,prmT002C25,11,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 10) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getBLOBFile(9, rslt.getString(6, 10), rslt.getString(7, 50)) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 10) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getBLOBFile(9, rslt.getString(6, 10), rslt.getString(7, 50)) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 50) ;
                ((String[]) buf[8])[0] = rslt.getString(7, 10) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((String[]) buf[10])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((int[]) buf[12])[0] = rslt.getInt(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((String[]) buf[14])[0] = rslt.getBLOBFile(10, rslt.getString(7, 10), rslt.getString(8, 50)) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 23 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (bool)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 12 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (bool)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(6, (DateTime)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 8 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[14]);
                }
                return;
             case 20 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(3, (DateTime)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[10]);
                }
                stmt.SetParameter(7, (int)parms[11]);
                stmt.SetParameter(8, (int)parms[12]);
                return;
             case 21 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (int)parms[3]);
                return;
             case 22 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 23 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
