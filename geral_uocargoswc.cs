/*
               File: Geral_UOCargosWC
        Description: Geral_UOCargos WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:1:52.94
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class geral_uocargoswc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public geral_uocargoswc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public geral_uocargoswc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Cargo_UOCod )
      {
         this.AV7Cargo_UOCod = aP0_Cargo_UOCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         chkavCargo_ativo = new GXCheckbox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         dynavGrupocargo_codigo1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         dynavGrupocargo_codigo2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         dynavGrupocargo_codigo3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7Cargo_UOCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Cargo_UOCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Cargo_UOCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7Cargo_UOCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vGRUPOCARGO_CODIGO1") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  GXDLVvGRUPOCARGO_CODIGO1I72( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vGRUPOCARGO_CODIGO2") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  GXDLVvGRUPOCARGO_CODIGO2I72( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vGRUPOCARGO_CODIGO3") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  GXDLVvGRUPOCARGO_CODIGO3I72( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_62 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_62_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_62_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV17DynamicFiltersSelector1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersSelector1", AV17DynamicFiltersSelector1);
                  AV18Cargo_Nome1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Cargo_Nome1", AV18Cargo_Nome1);
                  AV28GrupoCargo_Codigo1 = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28GrupoCargo_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28GrupoCargo_Codigo1), 6, 0)));
                  AV20DynamicFiltersSelector2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
                  AV21Cargo_Nome2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21Cargo_Nome2", AV21Cargo_Nome2);
                  AV29GrupoCargo_Codigo2 = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29GrupoCargo_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29GrupoCargo_Codigo2), 6, 0)));
                  AV23DynamicFiltersSelector3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  AV24Cargo_Nome3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24Cargo_Nome3", AV24Cargo_Nome3);
                  AV30GrupoCargo_Codigo3 = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30GrupoCargo_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30GrupoCargo_Codigo3), 6, 0)));
                  AV19DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
                  AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV14OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
                  AV15OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
                  AV32TFCargo_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32TFCargo_Nome", AV32TFCargo_Nome);
                  AV33TFCargo_Nome_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFCargo_Nome_Sel", AV33TFCargo_Nome_Sel);
                  AV36TFGrupoCargo_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36TFGrupoCargo_Nome", AV36TFGrupoCargo_Nome);
                  AV37TFGrupoCargo_Nome_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37TFGrupoCargo_Nome_Sel", AV37TFGrupoCargo_Nome_Sel);
                  AV7Cargo_UOCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Cargo_UOCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Cargo_UOCod), 6, 0)));
                  AV34ddo_Cargo_NomeTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34ddo_Cargo_NomeTitleControlIdToReplace", AV34ddo_Cargo_NomeTitleControlIdToReplace);
                  AV38ddo_GrupoCargo_NomeTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38ddo_GrupoCargo_NomeTitleControlIdToReplace", AV38ddo_GrupoCargo_NomeTitleControlIdToReplace);
                  AV45Pgmname = GetNextPar( );
                  AV16Cargo_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16Cargo_Ativo", AV16Cargo_Ativo);
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV11GridState);
                  AV26DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DynamicFiltersIgnoreFirst", AV26DynamicFiltersIgnoreFirst);
                  AV25DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersRemoving", AV25DynamicFiltersRemoving);
                  A617Cargo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A615GrupoCargo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A615GrupoCargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A615GrupoCargo_Codigo), 6, 0)));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV17DynamicFiltersSelector1, AV18Cargo_Nome1, AV28GrupoCargo_Codigo1, AV20DynamicFiltersSelector2, AV21Cargo_Nome2, AV29GrupoCargo_Codigo2, AV23DynamicFiltersSelector3, AV24Cargo_Nome3, AV30GrupoCargo_Codigo3, AV19DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV14OrderedBy, AV15OrderedDsc, AV32TFCargo_Nome, AV33TFCargo_Nome_Sel, AV36TFGrupoCargo_Nome, AV37TFGrupoCargo_Nome_Sel, AV7Cargo_UOCod, AV34ddo_Cargo_NomeTitleControlIdToReplace, AV38ddo_GrupoCargo_NomeTitleControlIdToReplace, AV45Pgmname, AV16Cargo_Ativo, AV11GridState, AV26DynamicFiltersIgnoreFirst, AV25DynamicFiltersRemoving, A617Cargo_Codigo, A615GrupoCargo_Codigo, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAI72( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV45Pgmname = "Geral_UOCargosWC";
               context.Gx_err = 0;
               GXVvGRUPOCARGO_CODIGO1_htmlI72( ) ;
               GXVvGRUPOCARGO_CODIGO2_htmlI72( ) ;
               GXVvGRUPOCARGO_CODIGO3_htmlI72( ) ;
               WSI72( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Geral_UOCargos WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205211815325");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("geral_uocargoswc.aspx") + "?" + UrlEncode("" +AV7Cargo_UOCod)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1", AV17DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCARGO_NOME1", AV18Cargo_Nome1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vGRUPOCARGO_CODIGO1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV28GrupoCargo_Codigo1), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2", AV20DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCARGO_NOME2", AV21Cargo_Nome2);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vGRUPOCARGO_CODIGO2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV29GrupoCargo_Codigo2), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCARGO_NOME3", AV24Cargo_Nome3);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vGRUPOCARGO_CODIGO3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV30GrupoCargo_Codigo3), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV19DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV15OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCARGO_NOME", AV32TFCargo_Nome);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCARGO_NOME_SEL", AV33TFCargo_Nome_Sel);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFGRUPOCARGO_NOME", AV36TFGrupoCargo_Nome);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFGRUPOCARGO_NOME_SEL", AV37TFGrupoCargo_Nome_Sel);
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_62", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_62), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV41GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV42GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV39DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV39DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCARGO_NOMETITLEFILTERDATA", AV31Cargo_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCARGO_NOMETITLEFILTERDATA", AV31Cargo_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vGRUPOCARGO_NOMETITLEFILTERDATA", AV35GrupoCargo_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vGRUPOCARGO_NOMETITLEFILTERDATA", AV35GrupoCargo_NomeTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7Cargo_UOCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7Cargo_UOCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCARGO_UOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Cargo_UOCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV45Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vGRIDSTATE", AV11GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vGRIDSTATE", AV11GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSIGNOREFIRST", AV26DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSREMOVING", AV25DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, sPrefix+"GRUPOCARGO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A615GrupoCargo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Caption", StringUtil.RTrim( Ddo_cargo_nome_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Tooltip", StringUtil.RTrim( Ddo_cargo_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Cls", StringUtil.RTrim( Ddo_cargo_nome_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_cargo_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_cargo_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_cargo_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_cargo_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_cargo_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_cargo_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Sortedstatus", StringUtil.RTrim( Ddo_cargo_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_cargo_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Filtertype", StringUtil.RTrim( Ddo_cargo_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_cargo_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_cargo_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Datalisttype", StringUtil.RTrim( Ddo_cargo_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Datalistproc", StringUtil.RTrim( Ddo_cargo_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_cargo_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Sortasc", StringUtil.RTrim( Ddo_cargo_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Sortdsc", StringUtil.RTrim( Ddo_cargo_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Loadingdata", StringUtil.RTrim( Ddo_cargo_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_cargo_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_cargo_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_cargo_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_GRUPOCARGO_NOME_Caption", StringUtil.RTrim( Ddo_grupocargo_nome_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_GRUPOCARGO_NOME_Tooltip", StringUtil.RTrim( Ddo_grupocargo_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_GRUPOCARGO_NOME_Cls", StringUtil.RTrim( Ddo_grupocargo_nome_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_GRUPOCARGO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_grupocargo_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_GRUPOCARGO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_grupocargo_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_GRUPOCARGO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_grupocargo_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_GRUPOCARGO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_grupocargo_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_GRUPOCARGO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_grupocargo_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_GRUPOCARGO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_grupocargo_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_GRUPOCARGO_NOME_Sortedstatus", StringUtil.RTrim( Ddo_grupocargo_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_GRUPOCARGO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_grupocargo_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_GRUPOCARGO_NOME_Filtertype", StringUtil.RTrim( Ddo_grupocargo_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_GRUPOCARGO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_grupocargo_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_GRUPOCARGO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_grupocargo_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_GRUPOCARGO_NOME_Datalisttype", StringUtil.RTrim( Ddo_grupocargo_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_GRUPOCARGO_NOME_Datalistproc", StringUtil.RTrim( Ddo_grupocargo_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_GRUPOCARGO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_grupocargo_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_GRUPOCARGO_NOME_Sortasc", StringUtil.RTrim( Ddo_grupocargo_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_GRUPOCARGO_NOME_Sortdsc", StringUtil.RTrim( Ddo_grupocargo_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_GRUPOCARGO_NOME_Loadingdata", StringUtil.RTrim( Ddo_grupocargo_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_GRUPOCARGO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_grupocargo_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_GRUPOCARGO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_grupocargo_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_GRUPOCARGO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_grupocargo_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_cargo_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_cargo_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_cargo_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_GRUPOCARGO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_grupocargo_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_GRUPOCARGO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_grupocargo_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_GRUPOCARGO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_grupocargo_nome_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormI72( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("geral_uocargoswc.js", "?20205211815418");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "Geral_UOCargosWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Geral_UOCargos WC" ;
      }

      protected void WBI70( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "geral_uocargoswc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_I72( true) ;
         }
         else
         {
            wb_table1_2_I72( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_I72e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtCargo_UOCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1002Cargo_UOCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1002Cargo_UOCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCargo_UOCod_Jsonclick, 0, "Attribute", "", "", "", edtCargo_UOCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Geral_UOCargosWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'" + sPrefix + "',false,'" + sGXsfl_62_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV19DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(71, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,71);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'" + sPrefix + "',false,'" + sGXsfl_62_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(72, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,72);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'" + sPrefix + "',false,'" + sGXsfl_62_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrderedby_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14OrderedBy), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV14OrderedBy), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,73);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrderedby_Jsonclick, 0, "Attribute", "", "", "", edtavOrderedby_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Geral_UOCargosWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'" + sPrefix + "',false,'" + sGXsfl_62_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV15OrderedDsc), StringUtil.BoolToStr( AV15OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_Geral_UOCargosWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'" + sPrefix + "',false,'" + sGXsfl_62_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcargo_nome_Internalname, AV32TFCargo_Nome, StringUtil.RTrim( context.localUtil.Format( AV32TFCargo_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,75);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcargo_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfcargo_nome_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Geral_UOCargosWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'" + sPrefix + "',false,'" + sGXsfl_62_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcargo_nome_sel_Internalname, AV33TFCargo_Nome_Sel, StringUtil.RTrim( context.localUtil.Format( AV33TFCargo_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,76);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcargo_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcargo_nome_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Geral_UOCargosWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'" + sPrefix + "',false,'" + sGXsfl_62_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfgrupocargo_nome_Internalname, AV36TFGrupoCargo_Nome, StringUtil.RTrim( context.localUtil.Format( AV36TFGrupoCargo_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,77);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfgrupocargo_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfgrupocargo_nome_Visible, 1, 0, "text", "", 380, "px", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Geral_UOCargosWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'" + sPrefix + "',false,'" + sGXsfl_62_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfgrupocargo_nome_sel_Internalname, AV37TFGrupoCargo_Nome_Sel, StringUtil.RTrim( context.localUtil.Format( AV37TFGrupoCargo_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,78);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfgrupocargo_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfgrupocargo_nome_sel_Visible, 1, 0, "text", "", 380, "px", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Geral_UOCargosWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CARGO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'" + sPrefix + "',false,'" + sGXsfl_62_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_cargo_nometitlecontrolidtoreplace_Internalname, AV34ddo_Cargo_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,80);\"", 0, edtavDdo_cargo_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_Geral_UOCargosWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_GRUPOCARGO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'" + sPrefix + "',false,'" + sGXsfl_62_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_grupocargo_nometitlecontrolidtoreplace_Internalname, AV38ddo_GrupoCargo_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,82);\"", 0, edtavDdo_grupocargo_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_Geral_UOCargosWC.htm");
         }
         wbLoad = true;
      }

      protected void STARTI72( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Geral_UOCargos WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPI70( ) ;
            }
         }
      }

      protected void WSI72( )
      {
         STARTI72( ) ;
         EVTI72( ) ;
      }

      protected void EVTI72( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPI70( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPI70( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11I72 */
                                    E11I72 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CARGO_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPI70( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12I72 */
                                    E12I72 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_GRUPOCARGO_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPI70( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13I72 */
                                    E13I72 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPI70( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14I72 */
                                    E14I72 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPI70( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15I72 */
                                    E15I72 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPI70( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E16I72 */
                                    E16I72 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPI70( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E17I72 */
                                    E17I72 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPI70( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E18I72 */
                                    E18I72 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPI70( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E19I72 */
                                    E19I72 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPI70( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E20I72 */
                                    E20I72 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPI70( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E21I72 */
                                    E21I72 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPI70( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E22I72 */
                                    E22I72 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPI70( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = chkavCargo_ativo_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPI70( ) ;
                              }
                              nGXsfl_62_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_62_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_62_idx), 4, 0)), 4, "0");
                              SubsflControlProps_622( ) ;
                              A617Cargo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtCargo_Codigo_Internalname), ",", "."));
                              A618Cargo_Nome = StringUtil.Upper( cgiGet( edtCargo_Nome_Internalname));
                              A616GrupoCargo_Nome = StringUtil.Upper( cgiGet( edtGrupoCargo_Nome_Internalname));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = chkavCargo_ativo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E23I72 */
                                          E23I72 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = chkavCargo_ativo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E24I72 */
                                          E24I72 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = chkavCargo_ativo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E25I72 */
                                          E25I72 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Dynamicfiltersselector1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV17DynamicFiltersSelector1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Cargo_nome1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCARGO_NOME1"), AV18Cargo_Nome1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Grupocargo_codigo1 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vGRUPOCARGO_CODIGO1"), ",", ".") != Convert.ToDecimal( AV28GrupoCargo_Codigo1 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Cargo_nome2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCARGO_NOME2"), AV21Cargo_Nome2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Grupocargo_codigo2 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vGRUPOCARGO_CODIGO2"), ",", ".") != Convert.ToDecimal( AV29GrupoCargo_Codigo2 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Cargo_nome3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCARGO_NOME3"), AV24Cargo_Nome3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Grupocargo_codigo3 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vGRUPOCARGO_CODIGO3"), ",", ".") != Convert.ToDecimal( AV30GrupoCargo_Codigo3 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcargo_nome Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCARGO_NOME"), AV32TFCargo_Nome) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcargo_nome_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCARGO_NOME_SEL"), AV33TFCargo_Nome_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfgrupocargo_nome Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFGRUPOCARGO_NOME"), AV36TFGrupoCargo_Nome) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfgrupocargo_nome_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFGRUPOCARGO_NOME_SEL"), AV37TFGrupoCargo_Nome_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = chkavCargo_ativo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPI70( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = chkavCargo_ativo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEI72( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormI72( ) ;
            }
         }
      }

      protected void PAI72( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            chkavCargo_ativo.Name = "vCARGO_ATIVO";
            chkavCargo_ativo.WebTags = "";
            chkavCargo_ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavCargo_ativo_Internalname, "TitleCaption", chkavCargo_ativo.Caption);
            chkavCargo_ativo.CheckedValue = "false";
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CARGO_NOME", "Nome", 0);
            cmbavDynamicfiltersselector1.addItem("GRUPOCARGO_CODIGO", "Grupo", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV17DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV17DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersSelector1", AV17DynamicFiltersSelector1);
            }
            dynavGrupocargo_codigo1.Name = "vGRUPOCARGO_CODIGO1";
            dynavGrupocargo_codigo1.WebTags = "";
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CARGO_NOME", "Nome", 0);
            cmbavDynamicfiltersselector2.addItem("GRUPOCARGO_CODIGO", "Grupo", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            }
            dynavGrupocargo_codigo2.Name = "vGRUPOCARGO_CODIGO2";
            dynavGrupocargo_codigo2.WebTags = "";
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CARGO_NOME", "Nome", 0);
            cmbavDynamicfiltersselector3.addItem("GRUPOCARGO_CODIGO", "Grupo", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            dynavGrupocargo_codigo3.Name = "vGRUPOCARGO_CODIGO3";
            dynavGrupocargo_codigo3.WebTags = "";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = chkavCargo_ativo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvGRUPOCARGO_CODIGO1I72( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvGRUPOCARGO_CODIGO1_dataI72( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvGRUPOCARGO_CODIGO1_htmlI72( )
      {
         int gxdynajaxvalue ;
         GXDLVvGRUPOCARGO_CODIGO1_dataI72( ) ;
         gxdynajaxindex = 1;
         dynavGrupocargo_codigo1.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavGrupocargo_codigo1.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavGrupocargo_codigo1.ItemCount > 0 )
         {
            AV28GrupoCargo_Codigo1 = (int)(NumberUtil.Val( dynavGrupocargo_codigo1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV28GrupoCargo_Codigo1), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28GrupoCargo_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28GrupoCargo_Codigo1), 6, 0)));
         }
      }

      protected void GXDLVvGRUPOCARGO_CODIGO1_dataI72( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00I72 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00I72_A615GrupoCargo_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00I72_A616GrupoCargo_Nome[0]);
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void GXDLVvGRUPOCARGO_CODIGO2I72( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvGRUPOCARGO_CODIGO2_dataI72( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvGRUPOCARGO_CODIGO2_htmlI72( )
      {
         int gxdynajaxvalue ;
         GXDLVvGRUPOCARGO_CODIGO2_dataI72( ) ;
         gxdynajaxindex = 1;
         dynavGrupocargo_codigo2.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavGrupocargo_codigo2.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavGrupocargo_codigo2.ItemCount > 0 )
         {
            AV29GrupoCargo_Codigo2 = (int)(NumberUtil.Val( dynavGrupocargo_codigo2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV29GrupoCargo_Codigo2), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29GrupoCargo_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29GrupoCargo_Codigo2), 6, 0)));
         }
      }

      protected void GXDLVvGRUPOCARGO_CODIGO2_dataI72( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00I73 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00I73_A615GrupoCargo_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00I73_A616GrupoCargo_Nome[0]);
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void GXDLVvGRUPOCARGO_CODIGO3I72( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvGRUPOCARGO_CODIGO3_dataI72( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvGRUPOCARGO_CODIGO3_htmlI72( )
      {
         int gxdynajaxvalue ;
         GXDLVvGRUPOCARGO_CODIGO3_dataI72( ) ;
         gxdynajaxindex = 1;
         dynavGrupocargo_codigo3.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavGrupocargo_codigo3.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavGrupocargo_codigo3.ItemCount > 0 )
         {
            AV30GrupoCargo_Codigo3 = (int)(NumberUtil.Val( dynavGrupocargo_codigo3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV30GrupoCargo_Codigo3), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30GrupoCargo_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30GrupoCargo_Codigo3), 6, 0)));
         }
      }

      protected void GXDLVvGRUPOCARGO_CODIGO3_dataI72( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00I74 */
         pr_default.execute(2);
         while ( (pr_default.getStatus(2) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00I74_A615GrupoCargo_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00I74_A616GrupoCargo_Nome[0]);
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_622( ) ;
         while ( nGXsfl_62_idx <= nRC_GXsfl_62 )
         {
            sendrow_622( ) ;
            nGXsfl_62_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_62_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_62_idx+1));
            sGXsfl_62_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_62_idx), 4, 0)), 4, "0");
            SubsflControlProps_622( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       String AV17DynamicFiltersSelector1 ,
                                       String AV18Cargo_Nome1 ,
                                       int AV28GrupoCargo_Codigo1 ,
                                       String AV20DynamicFiltersSelector2 ,
                                       String AV21Cargo_Nome2 ,
                                       int AV29GrupoCargo_Codigo2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       String AV24Cargo_Nome3 ,
                                       int AV30GrupoCargo_Codigo3 ,
                                       bool AV19DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       short AV14OrderedBy ,
                                       bool AV15OrderedDsc ,
                                       String AV32TFCargo_Nome ,
                                       String AV33TFCargo_Nome_Sel ,
                                       String AV36TFGrupoCargo_Nome ,
                                       String AV37TFGrupoCargo_Nome_Sel ,
                                       int AV7Cargo_UOCod ,
                                       String AV34ddo_Cargo_NomeTitleControlIdToReplace ,
                                       String AV38ddo_GrupoCargo_NomeTitleControlIdToReplace ,
                                       String AV45Pgmname ,
                                       bool AV16Cargo_Ativo ,
                                       wwpbaseobjects.SdtWWPGridState AV11GridState ,
                                       bool AV26DynamicFiltersIgnoreFirst ,
                                       bool AV25DynamicFiltersRemoving ,
                                       int A617Cargo_Codigo ,
                                       int A615GrupoCargo_Codigo ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFI72( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CARGO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A617Cargo_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CARGO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A617Cargo_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CARGO_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A618Cargo_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CARGO_NOME", A618Cargo_Nome);
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV17DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV17DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersSelector1", AV17DynamicFiltersSelector1);
         }
         if ( dynavGrupocargo_codigo1.ItemCount > 0 )
         {
            AV28GrupoCargo_Codigo1 = (int)(NumberUtil.Val( dynavGrupocargo_codigo1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV28GrupoCargo_Codigo1), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28GrupoCargo_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28GrupoCargo_Codigo1), 6, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         }
         if ( dynavGrupocargo_codigo2.ItemCount > 0 )
         {
            AV29GrupoCargo_Codigo2 = (int)(NumberUtil.Val( dynavGrupocargo_codigo2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV29GrupoCargo_Codigo2), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29GrupoCargo_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29GrupoCargo_Codigo2), 6, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
         if ( dynavGrupocargo_codigo3.ItemCount > 0 )
         {
            AV30GrupoCargo_Codigo3 = (int)(NumberUtil.Val( dynavGrupocargo_codigo3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV30GrupoCargo_Codigo3), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30GrupoCargo_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30GrupoCargo_Codigo3), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFI72( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV45Pgmname = "Geral_UOCargosWC";
         context.Gx_err = 0;
      }

      protected void RFI72( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 62;
         /* Execute user event: E24I72 */
         E24I72 ();
         nGXsfl_62_idx = 1;
         sGXsfl_62_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_62_idx), 4, 0)), 4, "0");
         SubsflControlProps_622( ) ;
         nGXsfl_62_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_622( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(3, new Object[]{ new Object[]{
                                                 AV17DynamicFiltersSelector1 ,
                                                 AV18Cargo_Nome1 ,
                                                 AV28GrupoCargo_Codigo1 ,
                                                 AV19DynamicFiltersEnabled2 ,
                                                 AV20DynamicFiltersSelector2 ,
                                                 AV21Cargo_Nome2 ,
                                                 AV29GrupoCargo_Codigo2 ,
                                                 AV22DynamicFiltersEnabled3 ,
                                                 AV23DynamicFiltersSelector3 ,
                                                 AV24Cargo_Nome3 ,
                                                 AV30GrupoCargo_Codigo3 ,
                                                 AV33TFCargo_Nome_Sel ,
                                                 AV32TFCargo_Nome ,
                                                 AV37TFGrupoCargo_Nome_Sel ,
                                                 AV36TFGrupoCargo_Nome ,
                                                 A618Cargo_Nome ,
                                                 A615GrupoCargo_Codigo ,
                                                 A616GrupoCargo_Nome ,
                                                 AV14OrderedBy ,
                                                 AV15OrderedDsc ,
                                                 A1002Cargo_UOCod ,
                                                 AV7Cargo_UOCod ,
                                                 A628Cargo_Ativo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN,
                                                 TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN
                                                 }
            });
            lV18Cargo_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV18Cargo_Nome1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Cargo_Nome1", AV18Cargo_Nome1);
            lV21Cargo_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV21Cargo_Nome2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21Cargo_Nome2", AV21Cargo_Nome2);
            lV24Cargo_Nome3 = StringUtil.Concat( StringUtil.RTrim( AV24Cargo_Nome3), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24Cargo_Nome3", AV24Cargo_Nome3);
            lV32TFCargo_Nome = StringUtil.Concat( StringUtil.RTrim( AV32TFCargo_Nome), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32TFCargo_Nome", AV32TFCargo_Nome);
            lV36TFGrupoCargo_Nome = StringUtil.Concat( StringUtil.RTrim( AV36TFGrupoCargo_Nome), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36TFGrupoCargo_Nome", AV36TFGrupoCargo_Nome);
            /* Using cursor H00I75 */
            pr_default.execute(3, new Object[] {AV7Cargo_UOCod, lV18Cargo_Nome1, AV28GrupoCargo_Codigo1, lV21Cargo_Nome2, AV29GrupoCargo_Codigo2, lV24Cargo_Nome3, AV30GrupoCargo_Codigo3, lV32TFCargo_Nome, AV33TFCargo_Nome_Sel, lV36TFGrupoCargo_Nome, AV37TFGrupoCargo_Nome_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_62_idx = 1;
            while ( ( (pr_default.getStatus(3) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A628Cargo_Ativo = H00I75_A628Cargo_Ativo[0];
               A615GrupoCargo_Codigo = H00I75_A615GrupoCargo_Codigo[0];
               A1002Cargo_UOCod = H00I75_A1002Cargo_UOCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1002Cargo_UOCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1002Cargo_UOCod), 6, 0)));
               n1002Cargo_UOCod = H00I75_n1002Cargo_UOCod[0];
               A616GrupoCargo_Nome = H00I75_A616GrupoCargo_Nome[0];
               A618Cargo_Nome = H00I75_A618Cargo_Nome[0];
               A617Cargo_Codigo = H00I75_A617Cargo_Codigo[0];
               A616GrupoCargo_Nome = H00I75_A616GrupoCargo_Nome[0];
               /* Execute user event: E25I72 */
               E25I72 ();
               pr_default.readNext(3);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(3) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(3);
            wbEnd = 62;
            WBI70( ) ;
         }
         nGXsfl_62_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(4, new Object[]{ new Object[]{
                                              AV17DynamicFiltersSelector1 ,
                                              AV18Cargo_Nome1 ,
                                              AV28GrupoCargo_Codigo1 ,
                                              AV19DynamicFiltersEnabled2 ,
                                              AV20DynamicFiltersSelector2 ,
                                              AV21Cargo_Nome2 ,
                                              AV29GrupoCargo_Codigo2 ,
                                              AV22DynamicFiltersEnabled3 ,
                                              AV23DynamicFiltersSelector3 ,
                                              AV24Cargo_Nome3 ,
                                              AV30GrupoCargo_Codigo3 ,
                                              AV33TFCargo_Nome_Sel ,
                                              AV32TFCargo_Nome ,
                                              AV37TFGrupoCargo_Nome_Sel ,
                                              AV36TFGrupoCargo_Nome ,
                                              A618Cargo_Nome ,
                                              A615GrupoCargo_Codigo ,
                                              A616GrupoCargo_Nome ,
                                              AV14OrderedBy ,
                                              AV15OrderedDsc ,
                                              A1002Cargo_UOCod ,
                                              AV7Cargo_UOCod ,
                                              A628Cargo_Ativo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         lV18Cargo_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV18Cargo_Nome1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Cargo_Nome1", AV18Cargo_Nome1);
         lV21Cargo_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV21Cargo_Nome2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21Cargo_Nome2", AV21Cargo_Nome2);
         lV24Cargo_Nome3 = StringUtil.Concat( StringUtil.RTrim( AV24Cargo_Nome3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24Cargo_Nome3", AV24Cargo_Nome3);
         lV32TFCargo_Nome = StringUtil.Concat( StringUtil.RTrim( AV32TFCargo_Nome), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32TFCargo_Nome", AV32TFCargo_Nome);
         lV36TFGrupoCargo_Nome = StringUtil.Concat( StringUtil.RTrim( AV36TFGrupoCargo_Nome), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36TFGrupoCargo_Nome", AV36TFGrupoCargo_Nome);
         /* Using cursor H00I76 */
         pr_default.execute(4, new Object[] {AV7Cargo_UOCod, lV18Cargo_Nome1, AV28GrupoCargo_Codigo1, lV21Cargo_Nome2, AV29GrupoCargo_Codigo2, lV24Cargo_Nome3, AV30GrupoCargo_Codigo3, lV32TFCargo_Nome, AV33TFCargo_Nome_Sel, lV36TFGrupoCargo_Nome, AV37TFGrupoCargo_Nome_Sel});
         GRID_nRecordCount = H00I76_AGRID_nRecordCount[0];
         pr_default.close(4);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV17DynamicFiltersSelector1, AV18Cargo_Nome1, AV28GrupoCargo_Codigo1, AV20DynamicFiltersSelector2, AV21Cargo_Nome2, AV29GrupoCargo_Codigo2, AV23DynamicFiltersSelector3, AV24Cargo_Nome3, AV30GrupoCargo_Codigo3, AV19DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV14OrderedBy, AV15OrderedDsc, AV32TFCargo_Nome, AV33TFCargo_Nome_Sel, AV36TFGrupoCargo_Nome, AV37TFGrupoCargo_Nome_Sel, AV7Cargo_UOCod, AV34ddo_Cargo_NomeTitleControlIdToReplace, AV38ddo_GrupoCargo_NomeTitleControlIdToReplace, AV45Pgmname, AV16Cargo_Ativo, AV11GridState, AV26DynamicFiltersIgnoreFirst, AV25DynamicFiltersRemoving, A617Cargo_Codigo, A615GrupoCargo_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV17DynamicFiltersSelector1, AV18Cargo_Nome1, AV28GrupoCargo_Codigo1, AV20DynamicFiltersSelector2, AV21Cargo_Nome2, AV29GrupoCargo_Codigo2, AV23DynamicFiltersSelector3, AV24Cargo_Nome3, AV30GrupoCargo_Codigo3, AV19DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV14OrderedBy, AV15OrderedDsc, AV32TFCargo_Nome, AV33TFCargo_Nome_Sel, AV36TFGrupoCargo_Nome, AV37TFGrupoCargo_Nome_Sel, AV7Cargo_UOCod, AV34ddo_Cargo_NomeTitleControlIdToReplace, AV38ddo_GrupoCargo_NomeTitleControlIdToReplace, AV45Pgmname, AV16Cargo_Ativo, AV11GridState, AV26DynamicFiltersIgnoreFirst, AV25DynamicFiltersRemoving, A617Cargo_Codigo, A615GrupoCargo_Codigo, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV17DynamicFiltersSelector1, AV18Cargo_Nome1, AV28GrupoCargo_Codigo1, AV20DynamicFiltersSelector2, AV21Cargo_Nome2, AV29GrupoCargo_Codigo2, AV23DynamicFiltersSelector3, AV24Cargo_Nome3, AV30GrupoCargo_Codigo3, AV19DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV14OrderedBy, AV15OrderedDsc, AV32TFCargo_Nome, AV33TFCargo_Nome_Sel, AV36TFGrupoCargo_Nome, AV37TFGrupoCargo_Nome_Sel, AV7Cargo_UOCod, AV34ddo_Cargo_NomeTitleControlIdToReplace, AV38ddo_GrupoCargo_NomeTitleControlIdToReplace, AV45Pgmname, AV16Cargo_Ativo, AV11GridState, AV26DynamicFiltersIgnoreFirst, AV25DynamicFiltersRemoving, A617Cargo_Codigo, A615GrupoCargo_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV17DynamicFiltersSelector1, AV18Cargo_Nome1, AV28GrupoCargo_Codigo1, AV20DynamicFiltersSelector2, AV21Cargo_Nome2, AV29GrupoCargo_Codigo2, AV23DynamicFiltersSelector3, AV24Cargo_Nome3, AV30GrupoCargo_Codigo3, AV19DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV14OrderedBy, AV15OrderedDsc, AV32TFCargo_Nome, AV33TFCargo_Nome_Sel, AV36TFGrupoCargo_Nome, AV37TFGrupoCargo_Nome_Sel, AV7Cargo_UOCod, AV34ddo_Cargo_NomeTitleControlIdToReplace, AV38ddo_GrupoCargo_NomeTitleControlIdToReplace, AV45Pgmname, AV16Cargo_Ativo, AV11GridState, AV26DynamicFiltersIgnoreFirst, AV25DynamicFiltersRemoving, A617Cargo_Codigo, A615GrupoCargo_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV17DynamicFiltersSelector1, AV18Cargo_Nome1, AV28GrupoCargo_Codigo1, AV20DynamicFiltersSelector2, AV21Cargo_Nome2, AV29GrupoCargo_Codigo2, AV23DynamicFiltersSelector3, AV24Cargo_Nome3, AV30GrupoCargo_Codigo3, AV19DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV14OrderedBy, AV15OrderedDsc, AV32TFCargo_Nome, AV33TFCargo_Nome_Sel, AV36TFGrupoCargo_Nome, AV37TFGrupoCargo_Nome_Sel, AV7Cargo_UOCod, AV34ddo_Cargo_NomeTitleControlIdToReplace, AV38ddo_GrupoCargo_NomeTitleControlIdToReplace, AV45Pgmname, AV16Cargo_Ativo, AV11GridState, AV26DynamicFiltersIgnoreFirst, AV25DynamicFiltersRemoving, A617Cargo_Codigo, A615GrupoCargo_Codigo, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPI70( )
      {
         /* Before Start, stand alone formulas. */
         AV45Pgmname = "Geral_UOCargosWC";
         context.Gx_err = 0;
         GXVvGRUPOCARGO_CODIGO1_htmlI72( ) ;
         GXVvGRUPOCARGO_CODIGO2_htmlI72( ) ;
         GXVvGRUPOCARGO_CODIGO3_htmlI72( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E23I72 */
         E23I72 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV39DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCARGO_NOMETITLEFILTERDATA"), AV31Cargo_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vGRUPOCARGO_NOMETITLEFILTERDATA"), AV35GrupoCargo_NomeTitleFilterData);
            /* Read variables values. */
            AV16Cargo_Ativo = StringUtil.StrToBool( cgiGet( chkavCargo_ativo_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16Cargo_Ativo", AV16Cargo_Ativo);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV17DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersSelector1", AV17DynamicFiltersSelector1);
            AV18Cargo_Nome1 = StringUtil.Upper( cgiGet( edtavCargo_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Cargo_Nome1", AV18Cargo_Nome1);
            dynavGrupocargo_codigo1.Name = dynavGrupocargo_codigo1_Internalname;
            dynavGrupocargo_codigo1.CurrentValue = cgiGet( dynavGrupocargo_codigo1_Internalname);
            AV28GrupoCargo_Codigo1 = (int)(NumberUtil.Val( cgiGet( dynavGrupocargo_codigo1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28GrupoCargo_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28GrupoCargo_Codigo1), 6, 0)));
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV20DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            AV21Cargo_Nome2 = StringUtil.Upper( cgiGet( edtavCargo_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21Cargo_Nome2", AV21Cargo_Nome2);
            dynavGrupocargo_codigo2.Name = dynavGrupocargo_codigo2_Internalname;
            dynavGrupocargo_codigo2.CurrentValue = cgiGet( dynavGrupocargo_codigo2_Internalname);
            AV29GrupoCargo_Codigo2 = (int)(NumberUtil.Val( cgiGet( dynavGrupocargo_codigo2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29GrupoCargo_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29GrupoCargo_Codigo2), 6, 0)));
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            AV24Cargo_Nome3 = StringUtil.Upper( cgiGet( edtavCargo_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24Cargo_Nome3", AV24Cargo_Nome3);
            dynavGrupocargo_codigo3.Name = dynavGrupocargo_codigo3_Internalname;
            dynavGrupocargo_codigo3.CurrentValue = cgiGet( dynavGrupocargo_codigo3_Internalname);
            AV30GrupoCargo_Codigo3 = (int)(NumberUtil.Val( cgiGet( dynavGrupocargo_codigo3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30GrupoCargo_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30GrupoCargo_Codigo3), 6, 0)));
            A1002Cargo_UOCod = (int)(context.localUtil.CToN( cgiGet( edtCargo_UOCod_Internalname), ",", "."));
            n1002Cargo_UOCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1002Cargo_UOCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1002Cargo_UOCod), 6, 0)));
            AV19DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vORDEREDBY");
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV14OrderedBy = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            }
            else
            {
               AV14OrderedBy = (short)(context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            }
            AV15OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            AV32TFCargo_Nome = StringUtil.Upper( cgiGet( edtavTfcargo_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32TFCargo_Nome", AV32TFCargo_Nome);
            AV33TFCargo_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfcargo_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFCargo_Nome_Sel", AV33TFCargo_Nome_Sel);
            AV36TFGrupoCargo_Nome = StringUtil.Upper( cgiGet( edtavTfgrupocargo_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36TFGrupoCargo_Nome", AV36TFGrupoCargo_Nome);
            AV37TFGrupoCargo_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfgrupocargo_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37TFGrupoCargo_Nome_Sel", AV37TFGrupoCargo_Nome_Sel);
            AV34ddo_Cargo_NomeTitleControlIdToReplace = cgiGet( edtavDdo_cargo_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34ddo_Cargo_NomeTitleControlIdToReplace", AV34ddo_Cargo_NomeTitleControlIdToReplace);
            AV38ddo_GrupoCargo_NomeTitleControlIdToReplace = cgiGet( edtavDdo_grupocargo_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38ddo_GrupoCargo_NomeTitleControlIdToReplace", AV38ddo_GrupoCargo_NomeTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_62 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_62"), ",", "."));
            AV41GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV42GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV7Cargo_UOCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Cargo_UOCod"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_cargo_nome_Caption = cgiGet( sPrefix+"DDO_CARGO_NOME_Caption");
            Ddo_cargo_nome_Tooltip = cgiGet( sPrefix+"DDO_CARGO_NOME_Tooltip");
            Ddo_cargo_nome_Cls = cgiGet( sPrefix+"DDO_CARGO_NOME_Cls");
            Ddo_cargo_nome_Filteredtext_set = cgiGet( sPrefix+"DDO_CARGO_NOME_Filteredtext_set");
            Ddo_cargo_nome_Selectedvalue_set = cgiGet( sPrefix+"DDO_CARGO_NOME_Selectedvalue_set");
            Ddo_cargo_nome_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CARGO_NOME_Dropdownoptionstype");
            Ddo_cargo_nome_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CARGO_NOME_Titlecontrolidtoreplace");
            Ddo_cargo_nome_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CARGO_NOME_Includesortasc"));
            Ddo_cargo_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CARGO_NOME_Includesortdsc"));
            Ddo_cargo_nome_Sortedstatus = cgiGet( sPrefix+"DDO_CARGO_NOME_Sortedstatus");
            Ddo_cargo_nome_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CARGO_NOME_Includefilter"));
            Ddo_cargo_nome_Filtertype = cgiGet( sPrefix+"DDO_CARGO_NOME_Filtertype");
            Ddo_cargo_nome_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CARGO_NOME_Filterisrange"));
            Ddo_cargo_nome_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CARGO_NOME_Includedatalist"));
            Ddo_cargo_nome_Datalisttype = cgiGet( sPrefix+"DDO_CARGO_NOME_Datalisttype");
            Ddo_cargo_nome_Datalistproc = cgiGet( sPrefix+"DDO_CARGO_NOME_Datalistproc");
            Ddo_cargo_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_CARGO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_cargo_nome_Sortasc = cgiGet( sPrefix+"DDO_CARGO_NOME_Sortasc");
            Ddo_cargo_nome_Sortdsc = cgiGet( sPrefix+"DDO_CARGO_NOME_Sortdsc");
            Ddo_cargo_nome_Loadingdata = cgiGet( sPrefix+"DDO_CARGO_NOME_Loadingdata");
            Ddo_cargo_nome_Cleanfilter = cgiGet( sPrefix+"DDO_CARGO_NOME_Cleanfilter");
            Ddo_cargo_nome_Noresultsfound = cgiGet( sPrefix+"DDO_CARGO_NOME_Noresultsfound");
            Ddo_cargo_nome_Searchbuttontext = cgiGet( sPrefix+"DDO_CARGO_NOME_Searchbuttontext");
            Ddo_grupocargo_nome_Caption = cgiGet( sPrefix+"DDO_GRUPOCARGO_NOME_Caption");
            Ddo_grupocargo_nome_Tooltip = cgiGet( sPrefix+"DDO_GRUPOCARGO_NOME_Tooltip");
            Ddo_grupocargo_nome_Cls = cgiGet( sPrefix+"DDO_GRUPOCARGO_NOME_Cls");
            Ddo_grupocargo_nome_Filteredtext_set = cgiGet( sPrefix+"DDO_GRUPOCARGO_NOME_Filteredtext_set");
            Ddo_grupocargo_nome_Selectedvalue_set = cgiGet( sPrefix+"DDO_GRUPOCARGO_NOME_Selectedvalue_set");
            Ddo_grupocargo_nome_Dropdownoptionstype = cgiGet( sPrefix+"DDO_GRUPOCARGO_NOME_Dropdownoptionstype");
            Ddo_grupocargo_nome_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_GRUPOCARGO_NOME_Titlecontrolidtoreplace");
            Ddo_grupocargo_nome_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_GRUPOCARGO_NOME_Includesortasc"));
            Ddo_grupocargo_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_GRUPOCARGO_NOME_Includesortdsc"));
            Ddo_grupocargo_nome_Sortedstatus = cgiGet( sPrefix+"DDO_GRUPOCARGO_NOME_Sortedstatus");
            Ddo_grupocargo_nome_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_GRUPOCARGO_NOME_Includefilter"));
            Ddo_grupocargo_nome_Filtertype = cgiGet( sPrefix+"DDO_GRUPOCARGO_NOME_Filtertype");
            Ddo_grupocargo_nome_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_GRUPOCARGO_NOME_Filterisrange"));
            Ddo_grupocargo_nome_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_GRUPOCARGO_NOME_Includedatalist"));
            Ddo_grupocargo_nome_Datalisttype = cgiGet( sPrefix+"DDO_GRUPOCARGO_NOME_Datalisttype");
            Ddo_grupocargo_nome_Datalistproc = cgiGet( sPrefix+"DDO_GRUPOCARGO_NOME_Datalistproc");
            Ddo_grupocargo_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_GRUPOCARGO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_grupocargo_nome_Sortasc = cgiGet( sPrefix+"DDO_GRUPOCARGO_NOME_Sortasc");
            Ddo_grupocargo_nome_Sortdsc = cgiGet( sPrefix+"DDO_GRUPOCARGO_NOME_Sortdsc");
            Ddo_grupocargo_nome_Loadingdata = cgiGet( sPrefix+"DDO_GRUPOCARGO_NOME_Loadingdata");
            Ddo_grupocargo_nome_Cleanfilter = cgiGet( sPrefix+"DDO_GRUPOCARGO_NOME_Cleanfilter");
            Ddo_grupocargo_nome_Noresultsfound = cgiGet( sPrefix+"DDO_GRUPOCARGO_NOME_Noresultsfound");
            Ddo_grupocargo_nome_Searchbuttontext = cgiGet( sPrefix+"DDO_GRUPOCARGO_NOME_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            Ddo_cargo_nome_Activeeventkey = cgiGet( sPrefix+"DDO_CARGO_NOME_Activeeventkey");
            Ddo_cargo_nome_Filteredtext_get = cgiGet( sPrefix+"DDO_CARGO_NOME_Filteredtext_get");
            Ddo_cargo_nome_Selectedvalue_get = cgiGet( sPrefix+"DDO_CARGO_NOME_Selectedvalue_get");
            Ddo_grupocargo_nome_Activeeventkey = cgiGet( sPrefix+"DDO_GRUPOCARGO_NOME_Activeeventkey");
            Ddo_grupocargo_nome_Filteredtext_get = cgiGet( sPrefix+"DDO_GRUPOCARGO_NOME_Filteredtext_get");
            Ddo_grupocargo_nome_Selectedvalue_get = cgiGet( sPrefix+"DDO_GRUPOCARGO_NOME_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV17DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCARGO_NOME1"), AV18Cargo_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vGRUPOCARGO_CODIGO1"), ",", ".") != Convert.ToDecimal( AV28GrupoCargo_Codigo1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCARGO_NOME2"), AV21Cargo_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vGRUPOCARGO_CODIGO2"), ",", ".") != Convert.ToDecimal( AV29GrupoCargo_Codigo2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCARGO_NOME3"), AV24Cargo_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vGRUPOCARGO_CODIGO3"), ",", ".") != Convert.ToDecimal( AV30GrupoCargo_Codigo3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCARGO_NOME"), AV32TFCargo_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCARGO_NOME_SEL"), AV33TFCargo_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFGRUPOCARGO_NOME"), AV36TFGrupoCargo_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFGRUPOCARGO_NOME_SEL"), AV37TFGrupoCargo_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E23I72 */
         E23I72 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E23I72( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV17DynamicFiltersSelector1 = "CARGO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersSelector1", AV17DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV20DynamicFiltersSelector2 = "CARGO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "CARGO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfcargo_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcargo_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcargo_nome_Visible), 5, 0)));
         edtavTfcargo_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcargo_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcargo_nome_sel_Visible), 5, 0)));
         edtavTfgrupocargo_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfgrupocargo_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfgrupocargo_nome_Visible), 5, 0)));
         edtavTfgrupocargo_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfgrupocargo_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfgrupocargo_nome_sel_Visible), 5, 0)));
         Ddo_cargo_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Cargo_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_cargo_nome_Internalname, "TitleControlIdToReplace", Ddo_cargo_nome_Titlecontrolidtoreplace);
         AV34ddo_Cargo_NomeTitleControlIdToReplace = Ddo_cargo_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34ddo_Cargo_NomeTitleControlIdToReplace", AV34ddo_Cargo_NomeTitleControlIdToReplace);
         edtavDdo_cargo_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_cargo_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_cargo_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_grupocargo_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_GrupoCargo_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_grupocargo_nome_Internalname, "TitleControlIdToReplace", Ddo_grupocargo_nome_Titlecontrolidtoreplace);
         AV38ddo_GrupoCargo_NomeTitleControlIdToReplace = Ddo_grupocargo_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38ddo_GrupoCargo_NomeTitleControlIdToReplace", AV38ddo_GrupoCargo_NomeTitleControlIdToReplace);
         edtavDdo_grupocargo_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_grupocargo_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_grupocargo_nometitlecontrolidtoreplace_Visible), 5, 0)));
         edtCargo_UOCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtCargo_UOCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCargo_UOCod_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrderedby_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrderedby_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrderedby_Visible), 5, 0)));
         if ( AV14OrderedBy < 1 )
         {
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV39DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV39DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E24I72( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV31Cargo_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV35GrupoCargo_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtCargo_Nome_Titleformat = 2;
         edtCargo_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV34ddo_Cargo_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtCargo_Nome_Internalname, "Title", edtCargo_Nome_Title);
         edtGrupoCargo_Nome_Titleformat = 2;
         edtGrupoCargo_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Grupo", AV38ddo_GrupoCargo_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtGrupoCargo_Nome_Internalname, "Title", edtGrupoCargo_Nome_Title);
         AV41GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41GridCurrentPage), 10, 0)));
         AV42GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV31Cargo_NomeTitleFilterData", AV31Cargo_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV35GrupoCargo_NomeTitleFilterData", AV35GrupoCargo_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
      }

      protected void E11I72( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV40PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV40PageToGo) ;
         }
      }

      protected void E12I72( )
      {
         /* Ddo_cargo_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_cargo_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_cargo_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_cargo_nome_Internalname, "SortedStatus", Ddo_cargo_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_cargo_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_cargo_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_cargo_nome_Internalname, "SortedStatus", Ddo_cargo_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_cargo_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV32TFCargo_Nome = Ddo_cargo_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32TFCargo_Nome", AV32TFCargo_Nome);
            AV33TFCargo_Nome_Sel = Ddo_cargo_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFCargo_Nome_Sel", AV33TFCargo_Nome_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E13I72( )
      {
         /* Ddo_grupocargo_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_grupocargo_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_grupocargo_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_grupocargo_nome_Internalname, "SortedStatus", Ddo_grupocargo_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_grupocargo_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_grupocargo_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_grupocargo_nome_Internalname, "SortedStatus", Ddo_grupocargo_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_grupocargo_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV36TFGrupoCargo_Nome = Ddo_grupocargo_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36TFGrupoCargo_Nome", AV36TFGrupoCargo_Nome);
            AV37TFGrupoCargo_Nome_Sel = Ddo_grupocargo_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37TFGrupoCargo_Nome_Sel", AV37TFGrupoCargo_Nome_Sel);
            subgrid_firstpage( ) ;
         }
      }

      private void E25I72( )
      {
         /* Grid_Load Routine */
         edtCargo_Nome_Link = formatLink("viewgeral_cargo.aspx") + "?" + UrlEncode("" +A617Cargo_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         edtGrupoCargo_Nome_Link = formatLink("viewgeral_grupo_cargo.aspx") + "?" + UrlEncode("" +A615GrupoCargo_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 62;
         }
         sendrow_622( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_62_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(62, GridRow);
         }
      }

      protected void E18I72( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV19DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E14I72( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV25DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersRemoving", AV25DynamicFiltersRemoving);
         AV26DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DynamicFiltersIgnoreFirst", AV26DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV25DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersRemoving", AV25DynamicFiltersRemoving);
         AV26DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DynamicFiltersIgnoreFirst", AV26DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV17DynamicFiltersSelector1, AV18Cargo_Nome1, AV28GrupoCargo_Codigo1, AV20DynamicFiltersSelector2, AV21Cargo_Nome2, AV29GrupoCargo_Codigo2, AV23DynamicFiltersSelector3, AV24Cargo_Nome3, AV30GrupoCargo_Codigo3, AV19DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV14OrderedBy, AV15OrderedDsc, AV32TFCargo_Nome, AV33TFCargo_Nome_Sel, AV36TFGrupoCargo_Nome, AV37TFGrupoCargo_Nome_Sel, AV7Cargo_UOCod, AV34ddo_Cargo_NomeTitleControlIdToReplace, AV38ddo_GrupoCargo_NomeTitleControlIdToReplace, AV45Pgmname, AV16Cargo_Ativo, AV11GridState, AV26DynamicFiltersIgnoreFirst, AV25DynamicFiltersRemoving, A617Cargo_Codigo, A615GrupoCargo_Codigo, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV17DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         dynavGrupocargo_codigo1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28GrupoCargo_Codigo1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavGrupocargo_codigo1_Internalname, "Values", dynavGrupocargo_codigo1.ToJavascriptSource());
         dynavGrupocargo_codigo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV29GrupoCargo_Codigo2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavGrupocargo_codigo2_Internalname, "Values", dynavGrupocargo_codigo2.ToJavascriptSource());
         dynavGrupocargo_codigo3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV30GrupoCargo_Codigo3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavGrupocargo_codigo3_Internalname, "Values", dynavGrupocargo_codigo3.ToJavascriptSource());
      }

      protected void E19I72( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E20I72( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E15I72( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV25DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersRemoving", AV25DynamicFiltersRemoving);
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV25DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersRemoving", AV25DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV17DynamicFiltersSelector1, AV18Cargo_Nome1, AV28GrupoCargo_Codigo1, AV20DynamicFiltersSelector2, AV21Cargo_Nome2, AV29GrupoCargo_Codigo2, AV23DynamicFiltersSelector3, AV24Cargo_Nome3, AV30GrupoCargo_Codigo3, AV19DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV14OrderedBy, AV15OrderedDsc, AV32TFCargo_Nome, AV33TFCargo_Nome_Sel, AV36TFGrupoCargo_Nome, AV37TFGrupoCargo_Nome_Sel, AV7Cargo_UOCod, AV34ddo_Cargo_NomeTitleControlIdToReplace, AV38ddo_GrupoCargo_NomeTitleControlIdToReplace, AV45Pgmname, AV16Cargo_Ativo, AV11GridState, AV26DynamicFiltersIgnoreFirst, AV25DynamicFiltersRemoving, A617Cargo_Codigo, A615GrupoCargo_Codigo, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV17DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         dynavGrupocargo_codigo1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28GrupoCargo_Codigo1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavGrupocargo_codigo1_Internalname, "Values", dynavGrupocargo_codigo1.ToJavascriptSource());
         dynavGrupocargo_codigo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV29GrupoCargo_Codigo2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavGrupocargo_codigo2_Internalname, "Values", dynavGrupocargo_codigo2.ToJavascriptSource());
         dynavGrupocargo_codigo3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV30GrupoCargo_Codigo3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavGrupocargo_codigo3_Internalname, "Values", dynavGrupocargo_codigo3.ToJavascriptSource());
      }

      protected void E21I72( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E16I72( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV25DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersRemoving", AV25DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV25DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersRemoving", AV25DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV17DynamicFiltersSelector1, AV18Cargo_Nome1, AV28GrupoCargo_Codigo1, AV20DynamicFiltersSelector2, AV21Cargo_Nome2, AV29GrupoCargo_Codigo2, AV23DynamicFiltersSelector3, AV24Cargo_Nome3, AV30GrupoCargo_Codigo3, AV19DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV14OrderedBy, AV15OrderedDsc, AV32TFCargo_Nome, AV33TFCargo_Nome_Sel, AV36TFGrupoCargo_Nome, AV37TFGrupoCargo_Nome_Sel, AV7Cargo_UOCod, AV34ddo_Cargo_NomeTitleControlIdToReplace, AV38ddo_GrupoCargo_NomeTitleControlIdToReplace, AV45Pgmname, AV16Cargo_Ativo, AV11GridState, AV26DynamicFiltersIgnoreFirst, AV25DynamicFiltersRemoving, A617Cargo_Codigo, A615GrupoCargo_Codigo, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV17DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         dynavGrupocargo_codigo1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28GrupoCargo_Codigo1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavGrupocargo_codigo1_Internalname, "Values", dynavGrupocargo_codigo1.ToJavascriptSource());
         dynavGrupocargo_codigo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV29GrupoCargo_Codigo2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavGrupocargo_codigo2_Internalname, "Values", dynavGrupocargo_codigo2.ToJavascriptSource());
         dynavGrupocargo_codigo3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV30GrupoCargo_Codigo3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavGrupocargo_codigo3_Internalname, "Values", dynavGrupocargo_codigo3.ToJavascriptSource());
      }

      protected void E22I72( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E17I72( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefreshCmp(sPrefix);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV17DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         dynavGrupocargo_codigo1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28GrupoCargo_Codigo1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavGrupocargo_codigo1_Internalname, "Values", dynavGrupocargo_codigo1.ToJavascriptSource());
         dynavGrupocargo_codigo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV29GrupoCargo_Codigo2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavGrupocargo_codigo2_Internalname, "Values", dynavGrupocargo_codigo2.ToJavascriptSource());
         dynavGrupocargo_codigo3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV30GrupoCargo_Codigo3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavGrupocargo_codigo3_Internalname, "Values", dynavGrupocargo_codigo3.ToJavascriptSource());
      }

      protected void S182( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_cargo_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_cargo_nome_Internalname, "SortedStatus", Ddo_cargo_nome_Sortedstatus);
         Ddo_grupocargo_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_grupocargo_nome_Internalname, "SortedStatus", Ddo_grupocargo_nome_Sortedstatus);
      }

      protected void S162( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV14OrderedBy == 1 )
         {
            Ddo_cargo_nome_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_cargo_nome_Internalname, "SortedStatus", Ddo_cargo_nome_Sortedstatus);
         }
         else if ( AV14OrderedBy == 2 )
         {
            Ddo_grupocargo_nome_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_grupocargo_nome_Internalname, "SortedStatus", Ddo_grupocargo_nome_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavCargo_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCargo_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCargo_nome1_Visible), 5, 0)));
         dynavGrupocargo_codigo1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavGrupocargo_codigo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavGrupocargo_codigo1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "CARGO_NOME") == 0 )
         {
            edtavCargo_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCargo_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCargo_nome1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "GRUPOCARGO_CODIGO") == 0 )
         {
            dynavGrupocargo_codigo1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavGrupocargo_codigo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavGrupocargo_codigo1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavCargo_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCargo_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCargo_nome2_Visible), 5, 0)));
         dynavGrupocargo_codigo2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavGrupocargo_codigo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavGrupocargo_codigo2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CARGO_NOME") == 0 )
         {
            edtavCargo_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCargo_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCargo_nome2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "GRUPOCARGO_CODIGO") == 0 )
         {
            dynavGrupocargo_codigo2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavGrupocargo_codigo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavGrupocargo_codigo2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavCargo_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCargo_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCargo_nome3_Visible), 5, 0)));
         dynavGrupocargo_codigo3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavGrupocargo_codigo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavGrupocargo_codigo3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CARGO_NOME") == 0 )
         {
            edtavCargo_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCargo_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCargo_nome3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "GRUPOCARGO_CODIGO") == 0 )
         {
            dynavGrupocargo_codigo3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavGrupocargo_codigo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavGrupocargo_codigo3.Visible), 5, 0)));
         }
      }

      protected void S202( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         AV20DynamicFiltersSelector2 = "CARGO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         AV21Cargo_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21Cargo_Nome2", AV21Cargo_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "CARGO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV24Cargo_Nome3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24Cargo_Nome3", AV24Cargo_Nome3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S222( )
      {
         /* 'CLEANFILTERS' Routine */
         AV16Cargo_Ativo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16Cargo_Ativo", AV16Cargo_Ativo);
         AV32TFCargo_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32TFCargo_Nome", AV32TFCargo_Nome);
         Ddo_cargo_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_cargo_nome_Internalname, "FilteredText_set", Ddo_cargo_nome_Filteredtext_set);
         AV33TFCargo_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFCargo_Nome_Sel", AV33TFCargo_Nome_Sel);
         Ddo_cargo_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_cargo_nome_Internalname, "SelectedValue_set", Ddo_cargo_nome_Selectedvalue_set);
         AV36TFGrupoCargo_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36TFGrupoCargo_Nome", AV36TFGrupoCargo_Nome);
         Ddo_grupocargo_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_grupocargo_nome_Internalname, "FilteredText_set", Ddo_grupocargo_nome_Filteredtext_set);
         AV37TFGrupoCargo_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37TFGrupoCargo_Nome_Sel", AV37TFGrupoCargo_Nome_Sel);
         Ddo_grupocargo_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_grupocargo_nome_Internalname, "SelectedValue_set", Ddo_grupocargo_nome_Selectedvalue_set);
         AV17DynamicFiltersSelector1 = "CARGO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersSelector1", AV17DynamicFiltersSelector1);
         AV18Cargo_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Cargo_Nome1", AV18Cargo_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV27Session.Get(AV45Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV45Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV27Session.Get(AV45Pgmname+"GridState"), "");
         }
         AV14OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         AV15OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S232( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV46GXV1 = 1;
         while ( AV46GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV46GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "CARGO_ATIVO") == 0 )
            {
               AV16Cargo_Ativo = BooleanUtil.Val( AV12GridStateFilterValue.gxTpr_Value);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16Cargo_Ativo", AV16Cargo_Ativo);
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCARGO_NOME") == 0 )
            {
               AV32TFCargo_Nome = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32TFCargo_Nome", AV32TFCargo_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32TFCargo_Nome)) )
               {
                  Ddo_cargo_nome_Filteredtext_set = AV32TFCargo_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_cargo_nome_Internalname, "FilteredText_set", Ddo_cargo_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCARGO_NOME_SEL") == 0 )
            {
               AV33TFCargo_Nome_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFCargo_Nome_Sel", AV33TFCargo_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33TFCargo_Nome_Sel)) )
               {
                  Ddo_cargo_nome_Selectedvalue_set = AV33TFCargo_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_cargo_nome_Internalname, "SelectedValue_set", Ddo_cargo_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFGRUPOCARGO_NOME") == 0 )
            {
               AV36TFGrupoCargo_Nome = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36TFGrupoCargo_Nome", AV36TFGrupoCargo_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TFGrupoCargo_Nome)) )
               {
                  Ddo_grupocargo_nome_Filteredtext_set = AV36TFGrupoCargo_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_grupocargo_nome_Internalname, "FilteredText_set", Ddo_grupocargo_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFGRUPOCARGO_NOME_SEL") == 0 )
            {
               AV37TFGrupoCargo_Nome_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37TFGrupoCargo_Nome_Sel", AV37TFGrupoCargo_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37TFGrupoCargo_Nome_Sel)) )
               {
                  Ddo_grupocargo_nome_Selectedvalue_set = AV37TFGrupoCargo_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_grupocargo_nome_Internalname, "SelectedValue_set", Ddo_grupocargo_nome_Selectedvalue_set);
               }
            }
            AV46GXV1 = (int)(AV46GXV1+1);
         }
      }

      protected void S212( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(1));
            AV17DynamicFiltersSelector1 = AV13GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersSelector1", AV17DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "CARGO_NOME") == 0 )
            {
               AV18Cargo_Nome1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Cargo_Nome1", AV18Cargo_Nome1);
            }
            else if ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "GRUPOCARGO_CODIGO") == 0 )
            {
               AV28GrupoCargo_Codigo1 = (int)(NumberUtil.Val( AV13GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28GrupoCargo_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28GrupoCargo_Codigo1), 6, 0)));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV19DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(2));
               AV20DynamicFiltersSelector2 = AV13GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CARGO_NOME") == 0 )
               {
                  AV21Cargo_Nome2 = AV13GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21Cargo_Nome2", AV21Cargo_Nome2);
               }
               else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "GRUPOCARGO_CODIGO") == 0 )
               {
                  AV29GrupoCargo_Codigo2 = (int)(NumberUtil.Val( AV13GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29GrupoCargo_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29GrupoCargo_Codigo2), 6, 0)));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV13GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CARGO_NOME") == 0 )
                  {
                     AV24Cargo_Nome3 = AV13GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24Cargo_Nome3", AV24Cargo_Nome3);
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "GRUPOCARGO_CODIGO") == 0 )
                  {
                     AV30GrupoCargo_Codigo3 = (int)(NumberUtil.Val( AV13GridStateDynamicFilter.gxTpr_Value, "."));
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30GrupoCargo_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30GrupoCargo_Codigo3), 6, 0)));
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV25DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV27Session.Get(AV45Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV14OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV15OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! (false==AV16Cargo_Ativo) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "CARGO_ATIVO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.BoolToStr( AV16Cargo_Ativo);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32TFCargo_Nome)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCARGO_NOME";
            AV12GridStateFilterValue.gxTpr_Value = AV32TFCargo_Nome;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33TFCargo_Nome_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCARGO_NOME_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV33TFCargo_Nome_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TFGrupoCargo_Nome)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFGRUPOCARGO_NOME";
            AV12GridStateFilterValue.gxTpr_Value = AV36TFGrupoCargo_Nome;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37TFGrupoCargo_Nome_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFGRUPOCARGO_NOME_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV37TFGrupoCargo_Nome_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV7Cargo_UOCod) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "PARM_&CARGO_UOCOD";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV7Cargo_UOCod), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV45Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S192( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV26DynamicFiltersIgnoreFirst )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV17DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "CARGO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Cargo_Nome1)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV18Cargo_Nome1;
            }
            else if ( ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "GRUPOCARGO_CODIGO") == 0 ) && ! (0==AV28GrupoCargo_Codigo1) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV28GrupoCargo_Codigo1), 6, 0);
            }
            if ( AV25DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV20DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CARGO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV21Cargo_Nome2)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV21Cargo_Nome2;
            }
            else if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "GRUPOCARGO_CODIGO") == 0 ) && ! (0==AV29GrupoCargo_Codigo2) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV29GrupoCargo_Codigo2), 6, 0);
            }
            if ( AV25DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CARGO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Cargo_Nome3)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV24Cargo_Nome3;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "GRUPOCARGO_CODIGO") == 0 ) && ! (0==AV30GrupoCargo_Codigo3) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV30GrupoCargo_Codigo3), 6, 0);
            }
            if ( AV25DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV45Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "Geral_Cargo";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "Cargo_UOCod";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Cargo_UOCod), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV27Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_I72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table2_5_I72( true) ;
         }
         else
         {
            wb_table2_5_I72( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_I72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_56_I72( true) ;
         }
         else
         {
            wb_table3_56_I72( false) ;
         }
         return  ;
      }

      protected void wb_table3_56_I72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_I72e( true) ;
         }
         else
         {
            wb_table1_2_I72e( false) ;
         }
      }

      protected void wb_table3_56_I72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "TableGridHeader", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_59_I72( true) ;
         }
         else
         {
            wb_table4_59_I72( false) ;
         }
         return  ;
      }

      protected void wb_table4_59_I72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_56_I72e( true) ;
         }
         else
         {
            wb_table3_56_I72e( false) ;
         }
      }

      protected void wb_table4_59_I72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"62\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Cargo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtCargo_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtCargo_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtCargo_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(410), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtGrupoCargo_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtGrupoCargo_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtGrupoCargo_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A617Cargo_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A618Cargo_Nome);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtCargo_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtCargo_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtCargo_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A616GrupoCargo_Nome);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtGrupoCargo_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtGrupoCargo_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtGrupoCargo_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 62 )
         {
            wbEnd = 0;
            nRC_GXsfl_62 = (short)(nGXsfl_62_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_59_I72e( true) ;
         }
         else
         {
            wb_table4_59_I72e( false) ;
         }
      }

      protected void wb_table2_5_I72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 8,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Geral_UOCargosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextcargo_ativo_Internalname, "Ativo?", "", "", lblFiltertextcargo_ativo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Geral_UOCargosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'" + sPrefix + "',false,'" + sGXsfl_62_idx + "',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavCargo_ativo_Internalname, StringUtil.BoolToStr( AV16Cargo_Ativo), "", "", 1, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(12, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,12);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_14_I72( true) ;
         }
         else
         {
            wb_table5_14_I72( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_I72e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_Geral_UOCargosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_I72e( true) ;
         }
         else
         {
            wb_table2_5_I72e( false) ;
         }
      }

      protected void wb_table5_14_I72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_Geral_UOCargosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'" + sPrefix + "',false,'" + sGXsfl_62_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV17DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "", true, "HLP_Geral_UOCargosWC.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV17DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_Geral_UOCargosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'" + sPrefix + "',false,'" + sGXsfl_62_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCargo_nome1_Internalname, AV18Cargo_Nome1, StringUtil.RTrim( context.localUtil.Format( AV18Cargo_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,23);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCargo_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavCargo_nome1_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Geral_UOCargosWC.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'" + sPrefix + "',false,'" + sGXsfl_62_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavGrupocargo_codigo1, dynavGrupocargo_codigo1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV28GrupoCargo_Codigo1), 6, 0)), 1, dynavGrupocargo_codigo1_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", dynavGrupocargo_codigo1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_Geral_UOCargosWC.htm");
            dynavGrupocargo_codigo1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28GrupoCargo_Codigo1), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavGrupocargo_codigo1_Internalname, "Values", (String)(dynavGrupocargo_codigo1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Geral_UOCargosWC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Geral_UOCargosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_Geral_UOCargosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'" + sPrefix + "',false,'" + sGXsfl_62_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV20DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,32);\"", "", true, "HLP_Geral_UOCargosWC.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_Geral_UOCargosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'" + sPrefix + "',false,'" + sGXsfl_62_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCargo_nome2_Internalname, AV21Cargo_Nome2, StringUtil.RTrim( context.localUtil.Format( AV21Cargo_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,36);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCargo_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavCargo_nome2_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Geral_UOCargosWC.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'" + sPrefix + "',false,'" + sGXsfl_62_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavGrupocargo_codigo2, dynavGrupocargo_codigo2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV29GrupoCargo_Codigo2), 6, 0)), 1, dynavGrupocargo_codigo2_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", dynavGrupocargo_codigo2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,37);\"", "", true, "HLP_Geral_UOCargosWC.htm");
            dynavGrupocargo_codigo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV29GrupoCargo_Codigo2), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavGrupocargo_codigo2_Internalname, "Values", (String)(dynavGrupocargo_codigo2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Geral_UOCargosWC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Geral_UOCargosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_Geral_UOCargosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'" + sPrefix + "',false,'" + sGXsfl_62_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,45);\"", "", true, "HLP_Geral_UOCargosWC.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_Geral_UOCargosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'" + sPrefix + "',false,'" + sGXsfl_62_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCargo_nome3_Internalname, AV24Cargo_Nome3, StringUtil.RTrim( context.localUtil.Format( AV24Cargo_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,49);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCargo_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavCargo_nome3_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Geral_UOCargosWC.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'" + sPrefix + "',false,'" + sGXsfl_62_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavGrupocargo_codigo3, dynavGrupocargo_codigo3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV30GrupoCargo_Codigo3), 6, 0)), 1, dynavGrupocargo_codigo3_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", dynavGrupocargo_codigo3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", "", true, "HLP_Geral_UOCargosWC.htm");
            dynavGrupocargo_codigo3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV30GrupoCargo_Codigo3), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavGrupocargo_codigo3_Internalname, "Values", (String)(dynavGrupocargo_codigo3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Geral_UOCargosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_I72e( true) ;
         }
         else
         {
            wb_table5_14_I72e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7Cargo_UOCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Cargo_UOCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Cargo_UOCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAI72( ) ;
         WSI72( ) ;
         WEI72( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7Cargo_UOCod = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAI72( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "geral_uocargoswc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAI72( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7Cargo_UOCod = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Cargo_UOCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Cargo_UOCod), 6, 0)));
         }
         wcpOAV7Cargo_UOCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Cargo_UOCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7Cargo_UOCod != wcpOAV7Cargo_UOCod ) ) )
         {
            setjustcreated();
         }
         wcpOAV7Cargo_UOCod = AV7Cargo_UOCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7Cargo_UOCod = cgiGet( sPrefix+"AV7Cargo_UOCod_CTRL");
         if ( StringUtil.Len( sCtrlAV7Cargo_UOCod) > 0 )
         {
            AV7Cargo_UOCod = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7Cargo_UOCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Cargo_UOCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Cargo_UOCod), 6, 0)));
         }
         else
         {
            AV7Cargo_UOCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7Cargo_UOCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAI72( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSI72( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSI72( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7Cargo_UOCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Cargo_UOCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7Cargo_UOCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7Cargo_UOCod_CTRL", StringUtil.RTrim( sCtrlAV7Cargo_UOCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEI72( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205211815827");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("geral_uocargoswc.js", "?20205211815827");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_622( )
      {
         edtCargo_Codigo_Internalname = sPrefix+"CARGO_CODIGO_"+sGXsfl_62_idx;
         edtCargo_Nome_Internalname = sPrefix+"CARGO_NOME_"+sGXsfl_62_idx;
         edtGrupoCargo_Nome_Internalname = sPrefix+"GRUPOCARGO_NOME_"+sGXsfl_62_idx;
      }

      protected void SubsflControlProps_fel_622( )
      {
         edtCargo_Codigo_Internalname = sPrefix+"CARGO_CODIGO_"+sGXsfl_62_fel_idx;
         edtCargo_Nome_Internalname = sPrefix+"CARGO_NOME_"+sGXsfl_62_fel_idx;
         edtGrupoCargo_Nome_Internalname = sPrefix+"GRUPOCARGO_NOME_"+sGXsfl_62_fel_idx;
      }

      protected void sendrow_622( )
      {
         SubsflControlProps_622( ) ;
         WBI70( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_62_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_62_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_62_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtCargo_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A617Cargo_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A617Cargo_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtCargo_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)62,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtCargo_Nome_Internalname,(String)A618Cargo_Nome,StringUtil.RTrim( context.localUtil.Format( A618Cargo_Nome, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtCargo_Nome_Link,(String)"",(String)"",(String)"",(String)edtCargo_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)80,(short)0,(short)0,(short)62,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtGrupoCargo_Nome_Internalname,(String)A616GrupoCargo_Nome,StringUtil.RTrim( context.localUtil.Format( A616GrupoCargo_Nome, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtGrupoCargo_Nome_Link,(String)"",(String)"",(String)"",(String)edtGrupoCargo_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)410,(String)"px",(short)17,(String)"px",(short)80,(short)0,(short)0,(short)62,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CARGO_CODIGO"+"_"+sGXsfl_62_idx, GetSecureSignedToken( sPrefix+sGXsfl_62_idx, context.localUtil.Format( (decimal)(A617Cargo_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CARGO_NOME"+"_"+sGXsfl_62_idx, GetSecureSignedToken( sPrefix+sGXsfl_62_idx, StringUtil.RTrim( context.localUtil.Format( A618Cargo_Nome, "@!"))));
            GridContainer.AddRow(GridRow);
            nGXsfl_62_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_62_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_62_idx+1));
            sGXsfl_62_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_62_idx), 4, 0)), 4, "0");
            SubsflControlProps_622( ) ;
         }
         /* End function sendrow_622 */
      }

      protected void init_default_properties( )
      {
         imgCleanfilters_Internalname = sPrefix+"CLEANFILTERS";
         lblFiltertextcargo_ativo_Internalname = sPrefix+"FILTERTEXTCARGO_ATIVO";
         chkavCargo_ativo_Internalname = sPrefix+"vCARGO_ATIVO";
         lblDynamicfiltersprefix1_Internalname = sPrefix+"DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE1";
         edtavCargo_nome1_Internalname = sPrefix+"vCARGO_NOME1";
         dynavGrupocargo_codigo1_Internalname = sPrefix+"vGRUPOCARGO_CODIGO1";
         imgAdddynamicfilters1_Internalname = sPrefix+"ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = sPrefix+"REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = sPrefix+"DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE2";
         edtavCargo_nome2_Internalname = sPrefix+"vCARGO_NOME2";
         dynavGrupocargo_codigo2_Internalname = sPrefix+"vGRUPOCARGO_CODIGO2";
         imgAdddynamicfilters2_Internalname = sPrefix+"ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = sPrefix+"REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = sPrefix+"DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE3";
         edtavCargo_nome3_Internalname = sPrefix+"vCARGO_NOME3";
         dynavGrupocargo_codigo3_Internalname = sPrefix+"vGRUPOCARGO_CODIGO3";
         imgRemovedynamicfilters3_Internalname = sPrefix+"REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = sPrefix+"TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = sPrefix+"JSDYNAMICFILTERS";
         tblTablefilters_Internalname = sPrefix+"TABLEFILTERS";
         edtCargo_Codigo_Internalname = sPrefix+"CARGO_CODIGO";
         edtCargo_Nome_Internalname = sPrefix+"CARGO_NOME";
         edtGrupoCargo_Nome_Internalname = sPrefix+"GRUPOCARGO_NOME";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         edtCargo_UOCod_Internalname = sPrefix+"CARGO_UOCOD";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = sPrefix+"vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = sPrefix+"vDYNAMICFILTERSENABLED3";
         edtavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         edtavTfcargo_nome_Internalname = sPrefix+"vTFCARGO_NOME";
         edtavTfcargo_nome_sel_Internalname = sPrefix+"vTFCARGO_NOME_SEL";
         edtavTfgrupocargo_nome_Internalname = sPrefix+"vTFGRUPOCARGO_NOME";
         edtavTfgrupocargo_nome_sel_Internalname = sPrefix+"vTFGRUPOCARGO_NOME_SEL";
         Ddo_cargo_nome_Internalname = sPrefix+"DDO_CARGO_NOME";
         edtavDdo_cargo_nometitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CARGO_NOMETITLECONTROLIDTOREPLACE";
         Ddo_grupocargo_nome_Internalname = sPrefix+"DDO_GRUPOCARGO_NOME";
         edtavDdo_grupocargo_nometitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_GRUPOCARGO_NOMETITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtGrupoCargo_Nome_Jsonclick = "";
         edtCargo_Nome_Jsonclick = "";
         edtCargo_Codigo_Jsonclick = "";
         dynavGrupocargo_codigo3_Jsonclick = "";
         edtavCargo_nome3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         dynavGrupocargo_codigo2_Jsonclick = "";
         edtavCargo_nome2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         dynavGrupocargo_codigo1_Jsonclick = "";
         edtavCargo_nome1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtGrupoCargo_Nome_Link = "";
         edtCargo_Nome_Link = "";
         edtGrupoCargo_Nome_Titleformat = 0;
         edtCargo_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         dynavGrupocargo_codigo3.Visible = 1;
         edtavCargo_nome3_Visible = 1;
         dynavGrupocargo_codigo2.Visible = 1;
         edtavCargo_nome2_Visible = 1;
         dynavGrupocargo_codigo1.Visible = 1;
         edtavCargo_nome1_Visible = 1;
         edtGrupoCargo_Nome_Title = "Grupo";
         edtCargo_Nome_Title = "Nome";
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkavCargo_ativo.Caption = "";
         edtavDdo_grupocargo_nometitlecontrolidtoreplace_Visible = 1;
         edtavDdo_cargo_nometitlecontrolidtoreplace_Visible = 1;
         edtavTfgrupocargo_nome_sel_Jsonclick = "";
         edtavTfgrupocargo_nome_sel_Visible = 1;
         edtavTfgrupocargo_nome_Jsonclick = "";
         edtavTfgrupocargo_nome_Visible = 1;
         edtavTfcargo_nome_sel_Jsonclick = "";
         edtavTfcargo_nome_sel_Visible = 1;
         edtavTfcargo_nome_Jsonclick = "";
         edtavTfcargo_nome_Visible = 1;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         edtavOrderedby_Jsonclick = "";
         edtavOrderedby_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         edtCargo_UOCod_Jsonclick = "";
         edtCargo_UOCod_Visible = 1;
         Ddo_grupocargo_nome_Searchbuttontext = "Pesquisar";
         Ddo_grupocargo_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_grupocargo_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_grupocargo_nome_Loadingdata = "Carregando dados...";
         Ddo_grupocargo_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_grupocargo_nome_Sortasc = "Ordenar de A � Z";
         Ddo_grupocargo_nome_Datalistupdateminimumcharacters = 0;
         Ddo_grupocargo_nome_Datalistproc = "GetGeral_UOCargosWCFilterData";
         Ddo_grupocargo_nome_Datalisttype = "Dynamic";
         Ddo_grupocargo_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_grupocargo_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_grupocargo_nome_Filtertype = "Character";
         Ddo_grupocargo_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_grupocargo_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_grupocargo_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_grupocargo_nome_Titlecontrolidtoreplace = "";
         Ddo_grupocargo_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_grupocargo_nome_Cls = "ColumnSettings";
         Ddo_grupocargo_nome_Tooltip = "Op��es";
         Ddo_grupocargo_nome_Caption = "";
         Ddo_cargo_nome_Searchbuttontext = "Pesquisar";
         Ddo_cargo_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_cargo_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_cargo_nome_Loadingdata = "Carregando dados...";
         Ddo_cargo_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_cargo_nome_Sortasc = "Ordenar de A � Z";
         Ddo_cargo_nome_Datalistupdateminimumcharacters = 0;
         Ddo_cargo_nome_Datalistproc = "GetGeral_UOCargosWCFilterData";
         Ddo_cargo_nome_Datalisttype = "Dynamic";
         Ddo_cargo_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_cargo_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_cargo_nome_Filtertype = "Character";
         Ddo_cargo_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_cargo_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_cargo_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_cargo_nome_Titlecontrolidtoreplace = "";
         Ddo_cargo_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_cargo_nome_Cls = "ColumnSettings";
         Ddo_cargo_nome_Tooltip = "Op��es";
         Ddo_cargo_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A617Cargo_Codigo',fld:'CARGO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A615GrupoCargo_Codigo',fld:'GRUPOCARGO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV34ddo_Cargo_NomeTitleControlIdToReplace',fld:'vDDO_CARGO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_GrupoCargo_NomeTitleControlIdToReplace',fld:'vDDO_GRUPOCARGO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16Cargo_Ativo',fld:'vCARGO_ATIVO',pic:'',nv:false},{av:'AV32TFCargo_Nome',fld:'vTFCARGO_NOME',pic:'@!',nv:''},{av:'AV33TFCargo_Nome_Sel',fld:'vTFCARGO_NOME_SEL',pic:'@!',nv:''},{av:'AV36TFGrupoCargo_Nome',fld:'vTFGRUPOCARGO_NOME',pic:'@!',nv:''},{av:'AV37TFGrupoCargo_Nome_Sel',fld:'vTFGRUPOCARGO_NOME_SEL',pic:'@!',nv:''},{av:'AV7Cargo_UOCod',fld:'vCARGO_UOCOD',pic:'ZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Cargo_Nome1',fld:'vCARGO_NOME1',pic:'@!',nv:''},{av:'AV28GrupoCargo_Codigo1',fld:'vGRUPOCARGO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Cargo_Nome2',fld:'vCARGO_NOME2',pic:'@!',nv:''},{av:'AV29GrupoCargo_Codigo2',fld:'vGRUPOCARGO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24Cargo_Nome3',fld:'vCARGO_NOME3',pic:'@!',nv:''},{av:'AV30GrupoCargo_Codigo3',fld:'vGRUPOCARGO_CODIGO3',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV31Cargo_NomeTitleFilterData',fld:'vCARGO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV35GrupoCargo_NomeTitleFilterData',fld:'vGRUPOCARGO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'edtCargo_Nome_Titleformat',ctrl:'CARGO_NOME',prop:'Titleformat'},{av:'edtCargo_Nome_Title',ctrl:'CARGO_NOME',prop:'Title'},{av:'edtGrupoCargo_Nome_Titleformat',ctrl:'GRUPOCARGO_NOME',prop:'Titleformat'},{av:'edtGrupoCargo_Nome_Title',ctrl:'GRUPOCARGO_NOME',prop:'Title'},{av:'AV41GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV42GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11I72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Cargo_Nome1',fld:'vCARGO_NOME1',pic:'@!',nv:''},{av:'AV28GrupoCargo_Codigo1',fld:'vGRUPOCARGO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Cargo_Nome2',fld:'vCARGO_NOME2',pic:'@!',nv:''},{av:'AV29GrupoCargo_Codigo2',fld:'vGRUPOCARGO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24Cargo_Nome3',fld:'vCARGO_NOME3',pic:'@!',nv:''},{av:'AV30GrupoCargo_Codigo3',fld:'vGRUPOCARGO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV32TFCargo_Nome',fld:'vTFCARGO_NOME',pic:'@!',nv:''},{av:'AV33TFCargo_Nome_Sel',fld:'vTFCARGO_NOME_SEL',pic:'@!',nv:''},{av:'AV36TFGrupoCargo_Nome',fld:'vTFGRUPOCARGO_NOME',pic:'@!',nv:''},{av:'AV37TFGrupoCargo_Nome_Sel',fld:'vTFGRUPOCARGO_NOME_SEL',pic:'@!',nv:''},{av:'AV7Cargo_UOCod',fld:'vCARGO_UOCOD',pic:'ZZZZZ9',nv:0},{av:'AV34ddo_Cargo_NomeTitleControlIdToReplace',fld:'vDDO_CARGO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_GrupoCargo_NomeTitleControlIdToReplace',fld:'vDDO_GRUPOCARGO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV16Cargo_Ativo',fld:'vCARGO_ATIVO',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A617Cargo_Codigo',fld:'CARGO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A615GrupoCargo_Codigo',fld:'GRUPOCARGO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CARGO_NOME.ONOPTIONCLICKED","{handler:'E12I72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Cargo_Nome1',fld:'vCARGO_NOME1',pic:'@!',nv:''},{av:'AV28GrupoCargo_Codigo1',fld:'vGRUPOCARGO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Cargo_Nome2',fld:'vCARGO_NOME2',pic:'@!',nv:''},{av:'AV29GrupoCargo_Codigo2',fld:'vGRUPOCARGO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24Cargo_Nome3',fld:'vCARGO_NOME3',pic:'@!',nv:''},{av:'AV30GrupoCargo_Codigo3',fld:'vGRUPOCARGO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV32TFCargo_Nome',fld:'vTFCARGO_NOME',pic:'@!',nv:''},{av:'AV33TFCargo_Nome_Sel',fld:'vTFCARGO_NOME_SEL',pic:'@!',nv:''},{av:'AV36TFGrupoCargo_Nome',fld:'vTFGRUPOCARGO_NOME',pic:'@!',nv:''},{av:'AV37TFGrupoCargo_Nome_Sel',fld:'vTFGRUPOCARGO_NOME_SEL',pic:'@!',nv:''},{av:'AV7Cargo_UOCod',fld:'vCARGO_UOCOD',pic:'ZZZZZ9',nv:0},{av:'AV34ddo_Cargo_NomeTitleControlIdToReplace',fld:'vDDO_CARGO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_GrupoCargo_NomeTitleControlIdToReplace',fld:'vDDO_GRUPOCARGO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV16Cargo_Ativo',fld:'vCARGO_ATIVO',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A617Cargo_Codigo',fld:'CARGO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A615GrupoCargo_Codigo',fld:'GRUPOCARGO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_cargo_nome_Activeeventkey',ctrl:'DDO_CARGO_NOME',prop:'ActiveEventKey'},{av:'Ddo_cargo_nome_Filteredtext_get',ctrl:'DDO_CARGO_NOME',prop:'FilteredText_get'},{av:'Ddo_cargo_nome_Selectedvalue_get',ctrl:'DDO_CARGO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_cargo_nome_Sortedstatus',ctrl:'DDO_CARGO_NOME',prop:'SortedStatus'},{av:'AV32TFCargo_Nome',fld:'vTFCARGO_NOME',pic:'@!',nv:''},{av:'AV33TFCargo_Nome_Sel',fld:'vTFCARGO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_grupocargo_nome_Sortedstatus',ctrl:'DDO_GRUPOCARGO_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_GRUPOCARGO_NOME.ONOPTIONCLICKED","{handler:'E13I72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Cargo_Nome1',fld:'vCARGO_NOME1',pic:'@!',nv:''},{av:'AV28GrupoCargo_Codigo1',fld:'vGRUPOCARGO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Cargo_Nome2',fld:'vCARGO_NOME2',pic:'@!',nv:''},{av:'AV29GrupoCargo_Codigo2',fld:'vGRUPOCARGO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24Cargo_Nome3',fld:'vCARGO_NOME3',pic:'@!',nv:''},{av:'AV30GrupoCargo_Codigo3',fld:'vGRUPOCARGO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV32TFCargo_Nome',fld:'vTFCARGO_NOME',pic:'@!',nv:''},{av:'AV33TFCargo_Nome_Sel',fld:'vTFCARGO_NOME_SEL',pic:'@!',nv:''},{av:'AV36TFGrupoCargo_Nome',fld:'vTFGRUPOCARGO_NOME',pic:'@!',nv:''},{av:'AV37TFGrupoCargo_Nome_Sel',fld:'vTFGRUPOCARGO_NOME_SEL',pic:'@!',nv:''},{av:'AV7Cargo_UOCod',fld:'vCARGO_UOCOD',pic:'ZZZZZ9',nv:0},{av:'AV34ddo_Cargo_NomeTitleControlIdToReplace',fld:'vDDO_CARGO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_GrupoCargo_NomeTitleControlIdToReplace',fld:'vDDO_GRUPOCARGO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV16Cargo_Ativo',fld:'vCARGO_ATIVO',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A617Cargo_Codigo',fld:'CARGO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A615GrupoCargo_Codigo',fld:'GRUPOCARGO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_grupocargo_nome_Activeeventkey',ctrl:'DDO_GRUPOCARGO_NOME',prop:'ActiveEventKey'},{av:'Ddo_grupocargo_nome_Filteredtext_get',ctrl:'DDO_GRUPOCARGO_NOME',prop:'FilteredText_get'},{av:'Ddo_grupocargo_nome_Selectedvalue_get',ctrl:'DDO_GRUPOCARGO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_grupocargo_nome_Sortedstatus',ctrl:'DDO_GRUPOCARGO_NOME',prop:'SortedStatus'},{av:'AV36TFGrupoCargo_Nome',fld:'vTFGRUPOCARGO_NOME',pic:'@!',nv:''},{av:'AV37TFGrupoCargo_Nome_Sel',fld:'vTFGRUPOCARGO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_cargo_nome_Sortedstatus',ctrl:'DDO_CARGO_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E25I72',iparms:[{av:'A617Cargo_Codigo',fld:'CARGO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A615GrupoCargo_Codigo',fld:'GRUPOCARGO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'edtCargo_Nome_Link',ctrl:'CARGO_NOME',prop:'Link'},{av:'edtGrupoCargo_Nome_Link',ctrl:'GRUPOCARGO_NOME',prop:'Link'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E18I72',iparms:[],oparms:[{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E14I72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Cargo_Nome1',fld:'vCARGO_NOME1',pic:'@!',nv:''},{av:'AV28GrupoCargo_Codigo1',fld:'vGRUPOCARGO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Cargo_Nome2',fld:'vCARGO_NOME2',pic:'@!',nv:''},{av:'AV29GrupoCargo_Codigo2',fld:'vGRUPOCARGO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24Cargo_Nome3',fld:'vCARGO_NOME3',pic:'@!',nv:''},{av:'AV30GrupoCargo_Codigo3',fld:'vGRUPOCARGO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV32TFCargo_Nome',fld:'vTFCARGO_NOME',pic:'@!',nv:''},{av:'AV33TFCargo_Nome_Sel',fld:'vTFCARGO_NOME_SEL',pic:'@!',nv:''},{av:'AV36TFGrupoCargo_Nome',fld:'vTFGRUPOCARGO_NOME',pic:'@!',nv:''},{av:'AV37TFGrupoCargo_Nome_Sel',fld:'vTFGRUPOCARGO_NOME_SEL',pic:'@!',nv:''},{av:'AV7Cargo_UOCod',fld:'vCARGO_UOCOD',pic:'ZZZZZ9',nv:0},{av:'AV34ddo_Cargo_NomeTitleControlIdToReplace',fld:'vDDO_CARGO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_GrupoCargo_NomeTitleControlIdToReplace',fld:'vDDO_GRUPOCARGO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV16Cargo_Ativo',fld:'vCARGO_ATIVO',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A617Cargo_Codigo',fld:'CARGO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A615GrupoCargo_Codigo',fld:'GRUPOCARGO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Cargo_Nome2',fld:'vCARGO_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24Cargo_Nome3',fld:'vCARGO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Cargo_Nome1',fld:'vCARGO_NOME1',pic:'@!',nv:''},{av:'AV28GrupoCargo_Codigo1',fld:'vGRUPOCARGO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV29GrupoCargo_Codigo2',fld:'vGRUPOCARGO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV30GrupoCargo_Codigo3',fld:'vGRUPOCARGO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'edtavCargo_nome2_Visible',ctrl:'vCARGO_NOME2',prop:'Visible'},{av:'dynavGrupocargo_codigo2'},{av:'edtavCargo_nome3_Visible',ctrl:'vCARGO_NOME3',prop:'Visible'},{av:'dynavGrupocargo_codigo3'},{av:'edtavCargo_nome1_Visible',ctrl:'vCARGO_NOME1',prop:'Visible'},{av:'dynavGrupocargo_codigo1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E19I72',iparms:[{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavCargo_nome1_Visible',ctrl:'vCARGO_NOME1',prop:'Visible'},{av:'dynavGrupocargo_codigo1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E20I72',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E15I72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Cargo_Nome1',fld:'vCARGO_NOME1',pic:'@!',nv:''},{av:'AV28GrupoCargo_Codigo1',fld:'vGRUPOCARGO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Cargo_Nome2',fld:'vCARGO_NOME2',pic:'@!',nv:''},{av:'AV29GrupoCargo_Codigo2',fld:'vGRUPOCARGO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24Cargo_Nome3',fld:'vCARGO_NOME3',pic:'@!',nv:''},{av:'AV30GrupoCargo_Codigo3',fld:'vGRUPOCARGO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV32TFCargo_Nome',fld:'vTFCARGO_NOME',pic:'@!',nv:''},{av:'AV33TFCargo_Nome_Sel',fld:'vTFCARGO_NOME_SEL',pic:'@!',nv:''},{av:'AV36TFGrupoCargo_Nome',fld:'vTFGRUPOCARGO_NOME',pic:'@!',nv:''},{av:'AV37TFGrupoCargo_Nome_Sel',fld:'vTFGRUPOCARGO_NOME_SEL',pic:'@!',nv:''},{av:'AV7Cargo_UOCod',fld:'vCARGO_UOCOD',pic:'ZZZZZ9',nv:0},{av:'AV34ddo_Cargo_NomeTitleControlIdToReplace',fld:'vDDO_CARGO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_GrupoCargo_NomeTitleControlIdToReplace',fld:'vDDO_GRUPOCARGO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV16Cargo_Ativo',fld:'vCARGO_ATIVO',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A617Cargo_Codigo',fld:'CARGO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A615GrupoCargo_Codigo',fld:'GRUPOCARGO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Cargo_Nome2',fld:'vCARGO_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24Cargo_Nome3',fld:'vCARGO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Cargo_Nome1',fld:'vCARGO_NOME1',pic:'@!',nv:''},{av:'AV28GrupoCargo_Codigo1',fld:'vGRUPOCARGO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV29GrupoCargo_Codigo2',fld:'vGRUPOCARGO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV30GrupoCargo_Codigo3',fld:'vGRUPOCARGO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'edtavCargo_nome2_Visible',ctrl:'vCARGO_NOME2',prop:'Visible'},{av:'dynavGrupocargo_codigo2'},{av:'edtavCargo_nome3_Visible',ctrl:'vCARGO_NOME3',prop:'Visible'},{av:'dynavGrupocargo_codigo3'},{av:'edtavCargo_nome1_Visible',ctrl:'vCARGO_NOME1',prop:'Visible'},{av:'dynavGrupocargo_codigo1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E21I72',iparms:[{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavCargo_nome2_Visible',ctrl:'vCARGO_NOME2',prop:'Visible'},{av:'dynavGrupocargo_codigo2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E16I72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Cargo_Nome1',fld:'vCARGO_NOME1',pic:'@!',nv:''},{av:'AV28GrupoCargo_Codigo1',fld:'vGRUPOCARGO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Cargo_Nome2',fld:'vCARGO_NOME2',pic:'@!',nv:''},{av:'AV29GrupoCargo_Codigo2',fld:'vGRUPOCARGO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24Cargo_Nome3',fld:'vCARGO_NOME3',pic:'@!',nv:''},{av:'AV30GrupoCargo_Codigo3',fld:'vGRUPOCARGO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV32TFCargo_Nome',fld:'vTFCARGO_NOME',pic:'@!',nv:''},{av:'AV33TFCargo_Nome_Sel',fld:'vTFCARGO_NOME_SEL',pic:'@!',nv:''},{av:'AV36TFGrupoCargo_Nome',fld:'vTFGRUPOCARGO_NOME',pic:'@!',nv:''},{av:'AV37TFGrupoCargo_Nome_Sel',fld:'vTFGRUPOCARGO_NOME_SEL',pic:'@!',nv:''},{av:'AV7Cargo_UOCod',fld:'vCARGO_UOCOD',pic:'ZZZZZ9',nv:0},{av:'AV34ddo_Cargo_NomeTitleControlIdToReplace',fld:'vDDO_CARGO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_GrupoCargo_NomeTitleControlIdToReplace',fld:'vDDO_GRUPOCARGO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV16Cargo_Ativo',fld:'vCARGO_ATIVO',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A617Cargo_Codigo',fld:'CARGO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A615GrupoCargo_Codigo',fld:'GRUPOCARGO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Cargo_Nome2',fld:'vCARGO_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24Cargo_Nome3',fld:'vCARGO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Cargo_Nome1',fld:'vCARGO_NOME1',pic:'@!',nv:''},{av:'AV28GrupoCargo_Codigo1',fld:'vGRUPOCARGO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV29GrupoCargo_Codigo2',fld:'vGRUPOCARGO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV30GrupoCargo_Codigo3',fld:'vGRUPOCARGO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'edtavCargo_nome2_Visible',ctrl:'vCARGO_NOME2',prop:'Visible'},{av:'dynavGrupocargo_codigo2'},{av:'edtavCargo_nome3_Visible',ctrl:'vCARGO_NOME3',prop:'Visible'},{av:'dynavGrupocargo_codigo3'},{av:'edtavCargo_nome1_Visible',ctrl:'vCARGO_NOME1',prop:'Visible'},{av:'dynavGrupocargo_codigo1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E22I72',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavCargo_nome3_Visible',ctrl:'vCARGO_NOME3',prop:'Visible'},{av:'dynavGrupocargo_codigo3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E17I72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Cargo_Nome1',fld:'vCARGO_NOME1',pic:'@!',nv:''},{av:'AV28GrupoCargo_Codigo1',fld:'vGRUPOCARGO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Cargo_Nome2',fld:'vCARGO_NOME2',pic:'@!',nv:''},{av:'AV29GrupoCargo_Codigo2',fld:'vGRUPOCARGO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24Cargo_Nome3',fld:'vCARGO_NOME3',pic:'@!',nv:''},{av:'AV30GrupoCargo_Codigo3',fld:'vGRUPOCARGO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV32TFCargo_Nome',fld:'vTFCARGO_NOME',pic:'@!',nv:''},{av:'AV33TFCargo_Nome_Sel',fld:'vTFCARGO_NOME_SEL',pic:'@!',nv:''},{av:'AV36TFGrupoCargo_Nome',fld:'vTFGRUPOCARGO_NOME',pic:'@!',nv:''},{av:'AV37TFGrupoCargo_Nome_Sel',fld:'vTFGRUPOCARGO_NOME_SEL',pic:'@!',nv:''},{av:'AV7Cargo_UOCod',fld:'vCARGO_UOCOD',pic:'ZZZZZ9',nv:0},{av:'AV34ddo_Cargo_NomeTitleControlIdToReplace',fld:'vDDO_CARGO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_GrupoCargo_NomeTitleControlIdToReplace',fld:'vDDO_GRUPOCARGO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV16Cargo_Ativo',fld:'vCARGO_ATIVO',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A617Cargo_Codigo',fld:'CARGO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A615GrupoCargo_Codigo',fld:'GRUPOCARGO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV16Cargo_Ativo',fld:'vCARGO_ATIVO',pic:'',nv:false},{av:'AV32TFCargo_Nome',fld:'vTFCARGO_NOME',pic:'@!',nv:''},{av:'Ddo_cargo_nome_Filteredtext_set',ctrl:'DDO_CARGO_NOME',prop:'FilteredText_set'},{av:'AV33TFCargo_Nome_Sel',fld:'vTFCARGO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_cargo_nome_Selectedvalue_set',ctrl:'DDO_CARGO_NOME',prop:'SelectedValue_set'},{av:'AV36TFGrupoCargo_Nome',fld:'vTFGRUPOCARGO_NOME',pic:'@!',nv:''},{av:'Ddo_grupocargo_nome_Filteredtext_set',ctrl:'DDO_GRUPOCARGO_NOME',prop:'FilteredText_set'},{av:'AV37TFGrupoCargo_Nome_Sel',fld:'vTFGRUPOCARGO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_grupocargo_nome_Selectedvalue_set',ctrl:'DDO_GRUPOCARGO_NOME',prop:'SelectedValue_set'},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Cargo_Nome1',fld:'vCARGO_NOME1',pic:'@!',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavCargo_nome1_Visible',ctrl:'vCARGO_NOME1',prop:'Visible'},{av:'dynavGrupocargo_codigo1'},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Cargo_Nome2',fld:'vCARGO_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24Cargo_Nome3',fld:'vCARGO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV28GrupoCargo_Codigo1',fld:'vGRUPOCARGO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV29GrupoCargo_Codigo2',fld:'vGRUPOCARGO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV30GrupoCargo_Codigo3',fld:'vGRUPOCARGO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'edtavCargo_nome2_Visible',ctrl:'vCARGO_NOME2',prop:'Visible'},{av:'dynavGrupocargo_codigo2'},{av:'edtavCargo_nome3_Visible',ctrl:'vCARGO_NOME3',prop:'Visible'},{av:'dynavGrupocargo_codigo3'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_cargo_nome_Activeeventkey = "";
         Ddo_cargo_nome_Filteredtext_get = "";
         Ddo_cargo_nome_Selectedvalue_get = "";
         Ddo_grupocargo_nome_Activeeventkey = "";
         Ddo_grupocargo_nome_Filteredtext_get = "";
         Ddo_grupocargo_nome_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV17DynamicFiltersSelector1 = "";
         AV18Cargo_Nome1 = "";
         AV20DynamicFiltersSelector2 = "";
         AV21Cargo_Nome2 = "";
         AV23DynamicFiltersSelector3 = "";
         AV24Cargo_Nome3 = "";
         AV32TFCargo_Nome = "";
         AV33TFCargo_Nome_Sel = "";
         AV36TFGrupoCargo_Nome = "";
         AV37TFGrupoCargo_Nome_Sel = "";
         AV34ddo_Cargo_NomeTitleControlIdToReplace = "";
         AV38ddo_GrupoCargo_NomeTitleControlIdToReplace = "";
         AV45Pgmname = "";
         AV16Cargo_Ativo = true;
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV39DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV31Cargo_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV35GrupoCargo_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_cargo_nome_Filteredtext_set = "";
         Ddo_cargo_nome_Selectedvalue_set = "";
         Ddo_cargo_nome_Sortedstatus = "";
         Ddo_grupocargo_nome_Filteredtext_set = "";
         Ddo_grupocargo_nome_Selectedvalue_set = "";
         Ddo_grupocargo_nome_Sortedstatus = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         A618Cargo_Nome = "";
         A616GrupoCargo_Nome = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00I72_A615GrupoCargo_Codigo = new int[1] ;
         H00I72_A616GrupoCargo_Nome = new String[] {""} ;
         H00I72_A626GrupoCargo_Ativo = new bool[] {false} ;
         H00I73_A615GrupoCargo_Codigo = new int[1] ;
         H00I73_A616GrupoCargo_Nome = new String[] {""} ;
         H00I73_A626GrupoCargo_Ativo = new bool[] {false} ;
         H00I74_A615GrupoCargo_Codigo = new int[1] ;
         H00I74_A616GrupoCargo_Nome = new String[] {""} ;
         H00I74_A626GrupoCargo_Ativo = new bool[] {false} ;
         GridContainer = new GXWebGrid( context);
         lV18Cargo_Nome1 = "";
         lV21Cargo_Nome2 = "";
         lV24Cargo_Nome3 = "";
         lV32TFCargo_Nome = "";
         lV36TFGrupoCargo_Nome = "";
         H00I75_A628Cargo_Ativo = new bool[] {false} ;
         H00I75_A615GrupoCargo_Codigo = new int[1] ;
         H00I75_A1002Cargo_UOCod = new int[1] ;
         H00I75_n1002Cargo_UOCod = new bool[] {false} ;
         H00I75_A616GrupoCargo_Nome = new String[] {""} ;
         H00I75_A618Cargo_Nome = new String[] {""} ;
         H00I75_A617Cargo_Codigo = new int[1] ;
         H00I76_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV27Session = context.GetSession();
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblFiltertextcargo_ativo_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7Cargo_UOCod = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.geral_uocargoswc__default(),
            new Object[][] {
                new Object[] {
               H00I72_A615GrupoCargo_Codigo, H00I72_A616GrupoCargo_Nome, H00I72_A626GrupoCargo_Ativo
               }
               , new Object[] {
               H00I73_A615GrupoCargo_Codigo, H00I73_A616GrupoCargo_Nome, H00I73_A626GrupoCargo_Ativo
               }
               , new Object[] {
               H00I74_A615GrupoCargo_Codigo, H00I74_A616GrupoCargo_Nome, H00I74_A626GrupoCargo_Ativo
               }
               , new Object[] {
               H00I75_A628Cargo_Ativo, H00I75_A615GrupoCargo_Codigo, H00I75_A1002Cargo_UOCod, H00I75_n1002Cargo_UOCod, H00I75_A616GrupoCargo_Nome, H00I75_A618Cargo_Nome, H00I75_A617Cargo_Codigo
               }
               , new Object[] {
               H00I76_AGRID_nRecordCount
               }
            }
         );
         AV45Pgmname = "Geral_UOCargosWC";
         /* GeneXus formulas. */
         AV45Pgmname = "Geral_UOCargosWC";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_62 ;
      private short nGXsfl_62_idx=1 ;
      private short AV14OrderedBy ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_62_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtCargo_Nome_Titleformat ;
      private short edtGrupoCargo_Nome_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7Cargo_UOCod ;
      private int wcpOAV7Cargo_UOCod ;
      private int subGrid_Rows ;
      private int AV28GrupoCargo_Codigo1 ;
      private int AV29GrupoCargo_Codigo2 ;
      private int AV30GrupoCargo_Codigo3 ;
      private int A617Cargo_Codigo ;
      private int A615GrupoCargo_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_cargo_nome_Datalistupdateminimumcharacters ;
      private int Ddo_grupocargo_nome_Datalistupdateminimumcharacters ;
      private int A1002Cargo_UOCod ;
      private int edtCargo_UOCod_Visible ;
      private int edtavOrderedby_Visible ;
      private int edtavOrdereddsc_Visible ;
      private int edtavTfcargo_nome_Visible ;
      private int edtavTfcargo_nome_sel_Visible ;
      private int edtavTfgrupocargo_nome_Visible ;
      private int edtavTfgrupocargo_nome_sel_Visible ;
      private int edtavDdo_cargo_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_grupocargo_nometitlecontrolidtoreplace_Visible ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV40PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavCargo_nome1_Visible ;
      private int edtavCargo_nome2_Visible ;
      private int edtavCargo_nome3_Visible ;
      private int AV46GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV41GridCurrentPage ;
      private long AV42GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_cargo_nome_Activeeventkey ;
      private String Ddo_cargo_nome_Filteredtext_get ;
      private String Ddo_cargo_nome_Selectedvalue_get ;
      private String Ddo_grupocargo_nome_Activeeventkey ;
      private String Ddo_grupocargo_nome_Filteredtext_get ;
      private String Ddo_grupocargo_nome_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_62_idx="0001" ;
      private String AV45Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_cargo_nome_Caption ;
      private String Ddo_cargo_nome_Tooltip ;
      private String Ddo_cargo_nome_Cls ;
      private String Ddo_cargo_nome_Filteredtext_set ;
      private String Ddo_cargo_nome_Selectedvalue_set ;
      private String Ddo_cargo_nome_Dropdownoptionstype ;
      private String Ddo_cargo_nome_Titlecontrolidtoreplace ;
      private String Ddo_cargo_nome_Sortedstatus ;
      private String Ddo_cargo_nome_Filtertype ;
      private String Ddo_cargo_nome_Datalisttype ;
      private String Ddo_cargo_nome_Datalistproc ;
      private String Ddo_cargo_nome_Sortasc ;
      private String Ddo_cargo_nome_Sortdsc ;
      private String Ddo_cargo_nome_Loadingdata ;
      private String Ddo_cargo_nome_Cleanfilter ;
      private String Ddo_cargo_nome_Noresultsfound ;
      private String Ddo_cargo_nome_Searchbuttontext ;
      private String Ddo_grupocargo_nome_Caption ;
      private String Ddo_grupocargo_nome_Tooltip ;
      private String Ddo_grupocargo_nome_Cls ;
      private String Ddo_grupocargo_nome_Filteredtext_set ;
      private String Ddo_grupocargo_nome_Selectedvalue_set ;
      private String Ddo_grupocargo_nome_Dropdownoptionstype ;
      private String Ddo_grupocargo_nome_Titlecontrolidtoreplace ;
      private String Ddo_grupocargo_nome_Sortedstatus ;
      private String Ddo_grupocargo_nome_Filtertype ;
      private String Ddo_grupocargo_nome_Datalisttype ;
      private String Ddo_grupocargo_nome_Datalistproc ;
      private String Ddo_grupocargo_nome_Sortasc ;
      private String Ddo_grupocargo_nome_Sortdsc ;
      private String Ddo_grupocargo_nome_Loadingdata ;
      private String Ddo_grupocargo_nome_Cleanfilter ;
      private String Ddo_grupocargo_nome_Noresultsfound ;
      private String Ddo_grupocargo_nome_Searchbuttontext ;
      private String GX_FocusControl ;
      private String edtCargo_UOCod_Internalname ;
      private String edtCargo_UOCod_Jsonclick ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavOrderedby_Internalname ;
      private String edtavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String edtavTfcargo_nome_Internalname ;
      private String edtavTfcargo_nome_Jsonclick ;
      private String edtavTfcargo_nome_sel_Internalname ;
      private String edtavTfcargo_nome_sel_Jsonclick ;
      private String edtavTfgrupocargo_nome_Internalname ;
      private String edtavTfgrupocargo_nome_Jsonclick ;
      private String edtavTfgrupocargo_nome_sel_Internalname ;
      private String edtavTfgrupocargo_nome_sel_Jsonclick ;
      private String edtavDdo_cargo_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_grupocargo_nometitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String chkavCargo_ativo_Internalname ;
      private String edtCargo_Codigo_Internalname ;
      private String edtCargo_Nome_Internalname ;
      private String edtGrupoCargo_Nome_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavCargo_nome1_Internalname ;
      private String dynavGrupocargo_codigo1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavCargo_nome2_Internalname ;
      private String dynavGrupocargo_codigo2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavCargo_nome3_Internalname ;
      private String dynavGrupocargo_codigo3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_cargo_nome_Internalname ;
      private String Ddo_grupocargo_nome_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtCargo_Nome_Title ;
      private String edtGrupoCargo_Nome_Title ;
      private String edtCargo_Nome_Link ;
      private String edtGrupoCargo_Nome_Link ;
      private String sStyleString ;
      private String tblUnnamedtable1_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablefilters_Internalname ;
      private String lblFiltertextcargo_ativo_Internalname ;
      private String lblFiltertextcargo_ativo_Jsonclick ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavCargo_nome1_Jsonclick ;
      private String dynavGrupocargo_codigo1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavCargo_nome2_Jsonclick ;
      private String dynavGrupocargo_codigo2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavCargo_nome3_Jsonclick ;
      private String dynavGrupocargo_codigo3_Jsonclick ;
      private String sCtrlAV7Cargo_UOCod ;
      private String sGXsfl_62_fel_idx="0001" ;
      private String ROClassString ;
      private String edtCargo_Codigo_Jsonclick ;
      private String edtCargo_Nome_Jsonclick ;
      private String edtGrupoCargo_Nome_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV19DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV15OrderedDsc ;
      private bool AV16Cargo_Ativo ;
      private bool AV26DynamicFiltersIgnoreFirst ;
      private bool AV25DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_cargo_nome_Includesortasc ;
      private bool Ddo_cargo_nome_Includesortdsc ;
      private bool Ddo_cargo_nome_Includefilter ;
      private bool Ddo_cargo_nome_Filterisrange ;
      private bool Ddo_cargo_nome_Includedatalist ;
      private bool Ddo_grupocargo_nome_Includesortasc ;
      private bool Ddo_grupocargo_nome_Includesortdsc ;
      private bool Ddo_grupocargo_nome_Includefilter ;
      private bool Ddo_grupocargo_nome_Filterisrange ;
      private bool Ddo_grupocargo_nome_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool A628Cargo_Ativo ;
      private bool n1002Cargo_UOCod ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private String AV17DynamicFiltersSelector1 ;
      private String AV18Cargo_Nome1 ;
      private String AV20DynamicFiltersSelector2 ;
      private String AV21Cargo_Nome2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV24Cargo_Nome3 ;
      private String AV32TFCargo_Nome ;
      private String AV33TFCargo_Nome_Sel ;
      private String AV36TFGrupoCargo_Nome ;
      private String AV37TFGrupoCargo_Nome_Sel ;
      private String AV34ddo_Cargo_NomeTitleControlIdToReplace ;
      private String AV38ddo_GrupoCargo_NomeTitleControlIdToReplace ;
      private String A618Cargo_Nome ;
      private String A616GrupoCargo_Nome ;
      private String lV18Cargo_Nome1 ;
      private String lV21Cargo_Nome2 ;
      private String lV24Cargo_Nome3 ;
      private String lV32TFCargo_Nome ;
      private String lV36TFGrupoCargo_Nome ;
      private IGxSession AV27Session ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCheckbox chkavCargo_ativo ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox dynavGrupocargo_codigo1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox dynavGrupocargo_codigo2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox dynavGrupocargo_codigo3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H00I72_A615GrupoCargo_Codigo ;
      private String[] H00I72_A616GrupoCargo_Nome ;
      private bool[] H00I72_A626GrupoCargo_Ativo ;
      private int[] H00I73_A615GrupoCargo_Codigo ;
      private String[] H00I73_A616GrupoCargo_Nome ;
      private bool[] H00I73_A626GrupoCargo_Ativo ;
      private int[] H00I74_A615GrupoCargo_Codigo ;
      private String[] H00I74_A616GrupoCargo_Nome ;
      private bool[] H00I74_A626GrupoCargo_Ativo ;
      private bool[] H00I75_A628Cargo_Ativo ;
      private int[] H00I75_A615GrupoCargo_Codigo ;
      private int[] H00I75_A1002Cargo_UOCod ;
      private bool[] H00I75_n1002Cargo_UOCod ;
      private String[] H00I75_A616GrupoCargo_Nome ;
      private String[] H00I75_A618Cargo_Nome ;
      private int[] H00I75_A617Cargo_Codigo ;
      private long[] H00I76_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV31Cargo_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV35GrupoCargo_NomeTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV13GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV39DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class geral_uocargoswc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00I75( IGxContext context ,
                                             String AV17DynamicFiltersSelector1 ,
                                             String AV18Cargo_Nome1 ,
                                             int AV28GrupoCargo_Codigo1 ,
                                             bool AV19DynamicFiltersEnabled2 ,
                                             String AV20DynamicFiltersSelector2 ,
                                             String AV21Cargo_Nome2 ,
                                             int AV29GrupoCargo_Codigo2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             String AV24Cargo_Nome3 ,
                                             int AV30GrupoCargo_Codigo3 ,
                                             String AV33TFCargo_Nome_Sel ,
                                             String AV32TFCargo_Nome ,
                                             String AV37TFGrupoCargo_Nome_Sel ,
                                             String AV36TFGrupoCargo_Nome ,
                                             String A618Cargo_Nome ,
                                             int A615GrupoCargo_Codigo ,
                                             String A616GrupoCargo_Nome ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A1002Cargo_UOCod ,
                                             int AV7Cargo_UOCod ,
                                             bool A628Cargo_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [16] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[Cargo_Ativo], T1.[GrupoCargo_Codigo], T1.[Cargo_UOCod], T2.[GrupoCargo_Nome], T1.[Cargo_Nome], T1.[Cargo_Codigo]";
         sFromString = " FROM ([Geral_Cargo] T1 WITH (NOLOCK) INNER JOIN [Geral_Grupo_Cargo] T2 WITH (NOLOCK) ON T2.[GrupoCargo_Codigo] = T1.[GrupoCargo_Codigo])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[Cargo_UOCod] = @AV7Cargo_UOCod)";
         sWhereString = sWhereString + " and (T1.[Cargo_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "CARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Cargo_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] like '%' + @lV18Cargo_Nome1 + '%')";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "GRUPOCARGO_CODIGO") == 0 ) && ( ! (0==AV28GrupoCargo_Codigo1) ) )
         {
            sWhereString = sWhereString + " and (T1.[GrupoCargo_Codigo] = @AV28GrupoCargo_Codigo1)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21Cargo_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] like '%' + @lV21Cargo_Nome2 + '%')";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "GRUPOCARGO_CODIGO") == 0 ) && ( ! (0==AV29GrupoCargo_Codigo2) ) )
         {
            sWhereString = sWhereString + " and (T1.[GrupoCargo_Codigo] = @AV29GrupoCargo_Codigo2)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Cargo_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] like '%' + @lV24Cargo_Nome3 + '%')";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "GRUPOCARGO_CODIGO") == 0 ) && ( ! (0==AV30GrupoCargo_Codigo3) ) )
         {
            sWhereString = sWhereString + " and (T1.[GrupoCargo_Codigo] = @AV30GrupoCargo_Codigo3)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV33TFCargo_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32TFCargo_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] like @lV32TFCargo_Nome)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33TFCargo_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] = @AV33TFCargo_Nome_Sel)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV37TFGrupoCargo_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TFGrupoCargo_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T2.[GrupoCargo_Nome] like @lV36TFGrupoCargo_Nome)";
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37TFGrupoCargo_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[GrupoCargo_Nome] = @AV37TFGrupoCargo_Nome_Sel)";
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Cargo_UOCod], T1.[Cargo_Nome]";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Cargo_UOCod] DESC, T1.[Cargo_Nome] DESC";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Cargo_UOCod], T2.[GrupoCargo_Nome]";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Cargo_UOCod] DESC, T2.[GrupoCargo_Nome] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Cargo_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00I76( IGxContext context ,
                                             String AV17DynamicFiltersSelector1 ,
                                             String AV18Cargo_Nome1 ,
                                             int AV28GrupoCargo_Codigo1 ,
                                             bool AV19DynamicFiltersEnabled2 ,
                                             String AV20DynamicFiltersSelector2 ,
                                             String AV21Cargo_Nome2 ,
                                             int AV29GrupoCargo_Codigo2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             String AV24Cargo_Nome3 ,
                                             int AV30GrupoCargo_Codigo3 ,
                                             String AV33TFCargo_Nome_Sel ,
                                             String AV32TFCargo_Nome ,
                                             String AV37TFGrupoCargo_Nome_Sel ,
                                             String AV36TFGrupoCargo_Nome ,
                                             String A618Cargo_Nome ,
                                             int A615GrupoCargo_Codigo ,
                                             String A616GrupoCargo_Nome ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A1002Cargo_UOCod ,
                                             int AV7Cargo_UOCod ,
                                             bool A628Cargo_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [11] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([Geral_Cargo] T1 WITH (NOLOCK) INNER JOIN [Geral_Grupo_Cargo] T2 WITH (NOLOCK) ON T2.[GrupoCargo_Codigo] = T1.[GrupoCargo_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[Cargo_UOCod] = @AV7Cargo_UOCod)";
         scmdbuf = scmdbuf + " and (T1.[Cargo_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "CARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Cargo_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] like '%' + @lV18Cargo_Nome1 + '%')";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "GRUPOCARGO_CODIGO") == 0 ) && ( ! (0==AV28GrupoCargo_Codigo1) ) )
         {
            sWhereString = sWhereString + " and (T1.[GrupoCargo_Codigo] = @AV28GrupoCargo_Codigo1)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21Cargo_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] like '%' + @lV21Cargo_Nome2 + '%')";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "GRUPOCARGO_CODIGO") == 0 ) && ( ! (0==AV29GrupoCargo_Codigo2) ) )
         {
            sWhereString = sWhereString + " and (T1.[GrupoCargo_Codigo] = @AV29GrupoCargo_Codigo2)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Cargo_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] like '%' + @lV24Cargo_Nome3 + '%')";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "GRUPOCARGO_CODIGO") == 0 ) && ( ! (0==AV30GrupoCargo_Codigo3) ) )
         {
            sWhereString = sWhereString + " and (T1.[GrupoCargo_Codigo] = @AV30GrupoCargo_Codigo3)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV33TFCargo_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32TFCargo_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] like @lV32TFCargo_Nome)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33TFCargo_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] = @AV33TFCargo_Nome_Sel)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV37TFGrupoCargo_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TFGrupoCargo_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T2.[GrupoCargo_Nome] like @lV36TFGrupoCargo_Nome)";
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37TFGrupoCargo_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[GrupoCargo_Nome] = @AV37TFGrupoCargo_Nome_Sel)";
         }
         else
         {
            GXv_int4[10] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 3 :
                     return conditional_H00I75(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (int)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (int)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (String)dynConstraints[17] , (short)dynConstraints[18] , (bool)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (bool)dynConstraints[22] );
               case 4 :
                     return conditional_H00I76(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (int)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (int)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (String)dynConstraints[17] , (short)dynConstraints[18] , (bool)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (bool)dynConstraints[22] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00I72 ;
          prmH00I72 = new Object[] {
          } ;
          Object[] prmH00I73 ;
          prmH00I73 = new Object[] {
          } ;
          Object[] prmH00I74 ;
          prmH00I74 = new Object[] {
          } ;
          Object[] prmH00I75 ;
          prmH00I75 = new Object[] {
          new Object[] {"@AV7Cargo_UOCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV18Cargo_Nome1",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV28GrupoCargo_Codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV21Cargo_Nome2",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV29GrupoCargo_Codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV24Cargo_Nome3",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV30GrupoCargo_Codigo3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV32TFCargo_Nome",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV33TFCargo_Nome_Sel",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV36TFGrupoCargo_Nome",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV37TFGrupoCargo_Nome_Sel",SqlDbType.VarChar,80,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00I76 ;
          prmH00I76 = new Object[] {
          new Object[] {"@AV7Cargo_UOCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV18Cargo_Nome1",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV28GrupoCargo_Codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV21Cargo_Nome2",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV29GrupoCargo_Codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV24Cargo_Nome3",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV30GrupoCargo_Codigo3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV32TFCargo_Nome",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV33TFCargo_Nome_Sel",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV36TFGrupoCargo_Nome",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV37TFGrupoCargo_Nome_Sel",SqlDbType.VarChar,80,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00I72", "SELECT [GrupoCargo_Codigo], [GrupoCargo_Nome], [GrupoCargo_Ativo] FROM [Geral_Grupo_Cargo] WITH (NOLOCK) WHERE [GrupoCargo_Ativo] = 1 ORDER BY [GrupoCargo_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00I72,0,0,true,false )
             ,new CursorDef("H00I73", "SELECT [GrupoCargo_Codigo], [GrupoCargo_Nome], [GrupoCargo_Ativo] FROM [Geral_Grupo_Cargo] WITH (NOLOCK) WHERE [GrupoCargo_Ativo] = 1 ORDER BY [GrupoCargo_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00I73,0,0,true,false )
             ,new CursorDef("H00I74", "SELECT [GrupoCargo_Codigo], [GrupoCargo_Nome], [GrupoCargo_Ativo] FROM [Geral_Grupo_Cargo] WITH (NOLOCK) WHERE [GrupoCargo_Ativo] = 1 ORDER BY [GrupoCargo_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00I74,0,0,true,false )
             ,new CursorDef("H00I75", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00I75,11,0,true,false )
             ,new CursorDef("H00I76", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00I76,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 3 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                return;
             case 4 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                return;
             case 4 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                return;
       }
    }

 }

}
