/*
               File: PRC_AlteraContrato
        Description: PRC_Altera Contrato
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:13:59.54
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_alteracontrato : GXProcedure
   {
      public prc_alteracontrato( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_alteracontrato( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contrato_Codigo ,
                           int aP1_Servico_Codigo ,
                           int aP2_UserId )
      {
         this.AV10Contrato_Codigo = aP0_Contrato_Codigo;
         this.AV16Servico_Codigo = aP1_Servico_Codigo;
         this.AV12UserId = aP2_UserId;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_Contrato_Codigo ,
                                 int aP1_Servico_Codigo ,
                                 int aP2_UserId )
      {
         prc_alteracontrato objprc_alteracontrato;
         objprc_alteracontrato = new prc_alteracontrato();
         objprc_alteracontrato.AV10Contrato_Codigo = aP0_Contrato_Codigo;
         objprc_alteracontrato.AV16Servico_Codigo = aP1_Servico_Codigo;
         objprc_alteracontrato.AV12UserId = aP2_UserId;
         objprc_alteracontrato.context.SetSubmitInitialConfig(context);
         objprc_alteracontrato.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_alteracontrato);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_alteracontrato)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV8Codigos.FromXml(AV9WebSession.Get("Codigos"), "Collection");
         AV9WebSession.Remove("Codigos");
         AV9WebSession.Set("Momento", "Solicitacao");
         /* Using cursor P00TT2 */
         pr_default.execute(0, new Object[] {AV10Contrato_Codigo, AV16Servico_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A155Servico_Codigo = P00TT2_A155Servico_Codigo[0];
            A74Contrato_Codigo = P00TT2_A74Contrato_Codigo[0];
            A77Contrato_Numero = P00TT2_A77Contrato_Numero[0];
            A160ContratoServicos_Codigo = P00TT2_A160ContratoServicos_Codigo[0];
            A1454ContratoServicos_PrazoTpDias = P00TT2_A1454ContratoServicos_PrazoTpDias[0];
            n1454ContratoServicos_PrazoTpDias = P00TT2_n1454ContratoServicos_PrazoTpDias[0];
            A77Contrato_Numero = P00TT2_A77Contrato_Numero[0];
            AV15Contrato_Numero = A77Contrato_Numero;
            AV17ContratoServicos_Codigo = A160ContratoServicos_Codigo;
            AV18TipoDias = A1454ContratoServicos_PrazoTpDias;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A456ContagemResultado_Codigo ,
                                              AV8Codigos ,
                                              A1603ContagemResultado_CntCod ,
                                              AV10Contrato_Codigo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         /* Using cursor P00TT4 */
         pr_default.execute(1, new Object[] {AV10Contrato_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A1603ContagemResultado_CntCod = P00TT4_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P00TT4_n1603ContagemResultado_CntCod[0];
            A456ContagemResultado_Codigo = P00TT4_A456ContagemResultado_Codigo[0];
            A798ContagemResultado_PFBFSImp = P00TT4_A798ContagemResultado_PFBFSImp[0];
            n798ContagemResultado_PFBFSImp = P00TT4_n798ContagemResultado_PFBFSImp[0];
            A471ContagemResultado_DataDmn = P00TT4_A471ContagemResultado_DataDmn[0];
            A912ContagemResultado_HoraEntrega = P00TT4_A912ContagemResultado_HoraEntrega[0];
            n912ContagemResultado_HoraEntrega = P00TT4_n912ContagemResultado_HoraEntrega[0];
            A1612ContagemResultado_CntNum = P00TT4_A1612ContagemResultado_CntNum[0];
            n1612ContagemResultado_CntNum = P00TT4_n1612ContagemResultado_CntNum[0];
            A1553ContagemResultado_CntSrvCod = P00TT4_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00TT4_n1553ContagemResultado_CntSrvCod[0];
            A1227ContagemResultado_PrazoInicialDias = P00TT4_A1227ContagemResultado_PrazoInicialDias[0];
            n1227ContagemResultado_PrazoInicialDias = P00TT4_n1227ContagemResultado_PrazoInicialDias[0];
            A484ContagemResultado_StatusDmn = P00TT4_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00TT4_n484ContagemResultado_StatusDmn[0];
            A472ContagemResultado_DataEntrega = P00TT4_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = P00TT4_n472ContagemResultado_DataEntrega[0];
            A1597ContagemResultado_CntSrvVlrUndCnt = P00TT4_A1597ContagemResultado_CntSrvVlrUndCnt[0];
            n1597ContagemResultado_CntSrvVlrUndCnt = P00TT4_n1597ContagemResultado_CntSrvVlrUndCnt[0];
            A512ContagemResultado_ValorPF = P00TT4_A512ContagemResultado_ValorPF[0];
            n512ContagemResultado_ValorPF = P00TT4_n512ContagemResultado_ValorPF[0];
            A1616ContagemResultado_CntVlrUndCnt = P00TT4_A1616ContagemResultado_CntVlrUndCnt[0];
            n1616ContagemResultado_CntVlrUndCnt = P00TT4_n1616ContagemResultado_CntVlrUndCnt[0];
            A890ContagemResultado_Responsavel = P00TT4_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = P00TT4_n890ContagemResultado_Responsavel[0];
            A684ContagemResultado_PFBFSUltima = P00TT4_A684ContagemResultado_PFBFSUltima[0];
            n684ContagemResultado_PFBFSUltima = P00TT4_n684ContagemResultado_PFBFSUltima[0];
            A684ContagemResultado_PFBFSUltima = P00TT4_A684ContagemResultado_PFBFSUltima[0];
            n684ContagemResultado_PFBFSUltima = P00TT4_n684ContagemResultado_PFBFSUltima[0];
            A1603ContagemResultado_CntCod = P00TT4_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P00TT4_n1603ContagemResultado_CntCod[0];
            A1597ContagemResultado_CntSrvVlrUndCnt = P00TT4_A1597ContagemResultado_CntSrvVlrUndCnt[0];
            n1597ContagemResultado_CntSrvVlrUndCnt = P00TT4_n1597ContagemResultado_CntSrvVlrUndCnt[0];
            A1612ContagemResultado_CntNum = P00TT4_A1612ContagemResultado_CntNum[0];
            n1612ContagemResultado_CntNum = P00TT4_n1612ContagemResultado_CntNum[0];
            A1616ContagemResultado_CntVlrUndCnt = P00TT4_A1616ContagemResultado_CntVlrUndCnt[0];
            n1616ContagemResultado_CntVlrUndCnt = P00TT4_n1616ContagemResultado_CntVlrUndCnt[0];
            if ( ( A684ContagemResultado_PFBFSUltima > Convert.ToDecimal( 0 )) )
            {
               AV20PFBFS = A684ContagemResultado_PFBFSUltima;
            }
            else
            {
               AV20PFBFS = A798ContagemResultado_PFBFSImp;
            }
            AV13Prazo = DateTimeUtil.ResetTime( A471ContagemResultado_DataDmn ) ;
            /* Execute user subroutine: 'CALCULARPRAZO' */
            S111 ();
            if ( returnInSub )
            {
               pr_default.close(1);
               this.cleanup();
               if (true) return;
            }
            AV13Prazo = DateTimeUtil.TAdd( AV13Prazo, 3600*(DateTimeUtil.Hour( A912ContagemResultado_HoraEntrega)));
            AV13Prazo = DateTimeUtil.TAdd( AV13Prazo, 60*(DateTimeUtil.Minute( A912ContagemResultado_HoraEntrega)));
            AV14Observacao = "Do contrato " + A1612ContagemResultado_CntNum + " pro contrato " + AV15Contrato_Numero;
            A1553ContagemResultado_CntSrvCod = AV17ContratoServicos_Codigo;
            n1553ContagemResultado_CntSrvCod = false;
            if ( A1227ContagemResultado_PrazoInicialDias != AV19Dias )
            {
               AV14Observacao = AV14Observacao + " com prazo alterado.";
               A1227ContagemResultado_PrazoInicialDias = AV19Dias;
               n1227ContagemResultado_PrazoInicialDias = false;
               if ( ! ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "E") == 0 ) )
               {
                  A472ContagemResultado_DataEntrega = DateTimeUtil.ResetTime(AV13Prazo);
                  n472ContagemResultado_DataEntrega = false;
                  A912ContagemResultado_HoraEntrega = DateTimeUtil.ResetDate(AV13Prazo);
                  n912ContagemResultado_HoraEntrega = false;
               }
            }
            else
            {
               AV14Observacao = AV14Observacao + ".";
            }
            if ( ( A1597ContagemResultado_CntSrvVlrUndCnt > Convert.ToDecimal( 0 )) )
            {
               A512ContagemResultado_ValorPF = A1597ContagemResultado_CntSrvVlrUndCnt;
               n512ContagemResultado_ValorPF = false;
            }
            else
            {
               A512ContagemResultado_ValorPF = A1616ContagemResultado_CntVlrUndCnt;
               n512ContagemResultado_ValorPF = false;
            }
            new prc_inslogresponsavel(context ).execute( ref  A456ContagemResultado_Codigo,  0,  "U",  "D",  AV12UserId,  0,  A484ContagemResultado_StatusDmn,  A484ContagemResultado_StatusDmn,  AV14Observacao,  AV13Prazo,  false) ;
            BatchSize = 100;
            pr_default.initializeBatch( 2, BatchSize, this, "Executebatchp00tt5");
            /* Using cursor P00TT5 */
            pr_default.addRecord(2, new Object[] {n912ContagemResultado_HoraEntrega, A912ContagemResultado_HoraEntrega, n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod, n1227ContagemResultado_PrazoInicialDias, A1227ContagemResultado_PrazoInicialDias, n472ContagemResultado_DataEntrega, A472ContagemResultado_DataEntrega, n512ContagemResultado_ValorPF, A512ContagemResultado_ValorPF, A456ContagemResultado_Codigo});
            if ( pr_default.recordCount(2) == pr_default.getBatchSize(2) )
            {
               Executebatchp00tt5( ) ;
            }
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            pr_default.readNext(1);
         }
         if ( pr_default.getBatchSize(2) > 0 )
         {
            Executebatchp00tt5( ) ;
         }
         pr_default.close(1);
         AV9WebSession.Remove("Momento");
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'CALCULARPRAZO' Routine */
         GXt_int1 = AV19Dias;
         new prc_diasparaentrega(context ).execute( ref  AV17ContratoServicos_Codigo,  AV20PFBFS,  0, out  GXt_int1) ;
         AV19Dias = GXt_int1;
         GXt_dtime2 = AV13Prazo;
         new prc_adddiasuteis(context ).execute(  AV13Prazo,  AV19Dias,  AV18TipoDias, out  GXt_dtime2) ;
         AV13Prazo = GXt_dtime2;
      }

      protected void Executebatchp00tt5( )
      {
         /* Using cursor P00TT5 */
         pr_default.executeBatch(2);
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_AlteraContrato");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(2);
      }

      public override void initialize( )
      {
         AV8Codigos = new GxSimpleCollection();
         AV9WebSession = context.GetSession();
         scmdbuf = "";
         P00TT2_A155Servico_Codigo = new int[1] ;
         P00TT2_A74Contrato_Codigo = new int[1] ;
         P00TT2_A77Contrato_Numero = new String[] {""} ;
         P00TT2_A160ContratoServicos_Codigo = new int[1] ;
         P00TT2_A1454ContratoServicos_PrazoTpDias = new String[] {""} ;
         P00TT2_n1454ContratoServicos_PrazoTpDias = new bool[] {false} ;
         A77Contrato_Numero = "";
         A1454ContratoServicos_PrazoTpDias = "";
         AV15Contrato_Numero = "";
         AV18TipoDias = "";
         P00TT4_A1603ContagemResultado_CntCod = new int[1] ;
         P00TT4_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P00TT4_A456ContagemResultado_Codigo = new int[1] ;
         P00TT4_A798ContagemResultado_PFBFSImp = new decimal[1] ;
         P00TT4_n798ContagemResultado_PFBFSImp = new bool[] {false} ;
         P00TT4_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00TT4_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         P00TT4_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         P00TT4_A1612ContagemResultado_CntNum = new String[] {""} ;
         P00TT4_n1612ContagemResultado_CntNum = new bool[] {false} ;
         P00TT4_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00TT4_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00TT4_A1227ContagemResultado_PrazoInicialDias = new short[1] ;
         P00TT4_n1227ContagemResultado_PrazoInicialDias = new bool[] {false} ;
         P00TT4_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00TT4_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00TT4_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P00TT4_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P00TT4_A1597ContagemResultado_CntSrvVlrUndCnt = new decimal[1] ;
         P00TT4_n1597ContagemResultado_CntSrvVlrUndCnt = new bool[] {false} ;
         P00TT4_A512ContagemResultado_ValorPF = new decimal[1] ;
         P00TT4_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P00TT4_A1616ContagemResultado_CntVlrUndCnt = new decimal[1] ;
         P00TT4_n1616ContagemResultado_CntVlrUndCnt = new bool[] {false} ;
         P00TT4_A890ContagemResultado_Responsavel = new int[1] ;
         P00TT4_n890ContagemResultado_Responsavel = new bool[] {false} ;
         P00TT4_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         P00TT4_n684ContagemResultado_PFBFSUltima = new bool[] {false} ;
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A912ContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         A1612ContagemResultado_CntNum = "";
         A484ContagemResultado_StatusDmn = "";
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         AV13Prazo = (DateTime)(DateTime.MinValue);
         AV14Observacao = "";
         P00TT5_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         P00TT5_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         P00TT5_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00TT5_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00TT5_A1227ContagemResultado_PrazoInicialDias = new short[1] ;
         P00TT5_n1227ContagemResultado_PrazoInicialDias = new bool[] {false} ;
         P00TT5_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P00TT5_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P00TT5_A512ContagemResultado_ValorPF = new decimal[1] ;
         P00TT5_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P00TT5_A456ContagemResultado_Codigo = new int[1] ;
         GXt_dtime2 = (DateTime)(DateTime.MinValue);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_alteracontrato__default(),
            new Object[][] {
                new Object[] {
               P00TT2_A155Servico_Codigo, P00TT2_A74Contrato_Codigo, P00TT2_A77Contrato_Numero, P00TT2_A160ContratoServicos_Codigo, P00TT2_A1454ContratoServicos_PrazoTpDias, P00TT2_n1454ContratoServicos_PrazoTpDias
               }
               , new Object[] {
               P00TT4_A1603ContagemResultado_CntCod, P00TT4_n1603ContagemResultado_CntCod, P00TT4_A456ContagemResultado_Codigo, P00TT4_A798ContagemResultado_PFBFSImp, P00TT4_n798ContagemResultado_PFBFSImp, P00TT4_A471ContagemResultado_DataDmn, P00TT4_A912ContagemResultado_HoraEntrega, P00TT4_n912ContagemResultado_HoraEntrega, P00TT4_A1612ContagemResultado_CntNum, P00TT4_n1612ContagemResultado_CntNum,
               P00TT4_A1553ContagemResultado_CntSrvCod, P00TT4_n1553ContagemResultado_CntSrvCod, P00TT4_A1227ContagemResultado_PrazoInicialDias, P00TT4_n1227ContagemResultado_PrazoInicialDias, P00TT4_A484ContagemResultado_StatusDmn, P00TT4_n484ContagemResultado_StatusDmn, P00TT4_A472ContagemResultado_DataEntrega, P00TT4_n472ContagemResultado_DataEntrega, P00TT4_A1597ContagemResultado_CntSrvVlrUndCnt, P00TT4_n1597ContagemResultado_CntSrvVlrUndCnt,
               P00TT4_A512ContagemResultado_ValorPF, P00TT4_n512ContagemResultado_ValorPF, P00TT4_A1616ContagemResultado_CntVlrUndCnt, P00TT4_n1616ContagemResultado_CntVlrUndCnt, P00TT4_A890ContagemResultado_Responsavel, P00TT4_n890ContagemResultado_Responsavel, P00TT4_A684ContagemResultado_PFBFSUltima, P00TT4_n684ContagemResultado_PFBFSUltima
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A1227ContagemResultado_PrazoInicialDias ;
      private short AV19Dias ;
      private short GXt_int1 ;
      private int AV10Contrato_Codigo ;
      private int AV16Servico_Codigo ;
      private int AV12UserId ;
      private int A155Servico_Codigo ;
      private int A74Contrato_Codigo ;
      private int A160ContratoServicos_Codigo ;
      private int AV17ContratoServicos_Codigo ;
      private int A456ContagemResultado_Codigo ;
      private int A1603ContagemResultado_CntCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A890ContagemResultado_Responsavel ;
      private int BatchSize ;
      private decimal A798ContagemResultado_PFBFSImp ;
      private decimal A1597ContagemResultado_CntSrvVlrUndCnt ;
      private decimal A512ContagemResultado_ValorPF ;
      private decimal A1616ContagemResultado_CntVlrUndCnt ;
      private decimal A684ContagemResultado_PFBFSUltima ;
      private decimal AV20PFBFS ;
      private String scmdbuf ;
      private String A77Contrato_Numero ;
      private String A1454ContratoServicos_PrazoTpDias ;
      private String AV15Contrato_Numero ;
      private String AV18TipoDias ;
      private String A1612ContagemResultado_CntNum ;
      private String A484ContagemResultado_StatusDmn ;
      private DateTime A912ContagemResultado_HoraEntrega ;
      private DateTime AV13Prazo ;
      private DateTime GXt_dtime2 ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private bool n1454ContratoServicos_PrazoTpDias ;
      private bool n1603ContagemResultado_CntCod ;
      private bool n798ContagemResultado_PFBFSImp ;
      private bool n912ContagemResultado_HoraEntrega ;
      private bool n1612ContagemResultado_CntNum ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1227ContagemResultado_PrazoInicialDias ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool n1597ContagemResultado_CntSrvVlrUndCnt ;
      private bool n512ContagemResultado_ValorPF ;
      private bool n1616ContagemResultado_CntVlrUndCnt ;
      private bool n890ContagemResultado_Responsavel ;
      private bool n684ContagemResultado_PFBFSUltima ;
      private bool returnInSub ;
      private String AV14Observacao ;
      private IGxSession AV9WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00TT2_A155Servico_Codigo ;
      private int[] P00TT2_A74Contrato_Codigo ;
      private String[] P00TT2_A77Contrato_Numero ;
      private int[] P00TT2_A160ContratoServicos_Codigo ;
      private String[] P00TT2_A1454ContratoServicos_PrazoTpDias ;
      private bool[] P00TT2_n1454ContratoServicos_PrazoTpDias ;
      private int[] P00TT4_A1603ContagemResultado_CntCod ;
      private bool[] P00TT4_n1603ContagemResultado_CntCod ;
      private int[] P00TT4_A456ContagemResultado_Codigo ;
      private decimal[] P00TT4_A798ContagemResultado_PFBFSImp ;
      private bool[] P00TT4_n798ContagemResultado_PFBFSImp ;
      private DateTime[] P00TT4_A471ContagemResultado_DataDmn ;
      private DateTime[] P00TT4_A912ContagemResultado_HoraEntrega ;
      private bool[] P00TT4_n912ContagemResultado_HoraEntrega ;
      private String[] P00TT4_A1612ContagemResultado_CntNum ;
      private bool[] P00TT4_n1612ContagemResultado_CntNum ;
      private int[] P00TT4_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00TT4_n1553ContagemResultado_CntSrvCod ;
      private short[] P00TT4_A1227ContagemResultado_PrazoInicialDias ;
      private bool[] P00TT4_n1227ContagemResultado_PrazoInicialDias ;
      private String[] P00TT4_A484ContagemResultado_StatusDmn ;
      private bool[] P00TT4_n484ContagemResultado_StatusDmn ;
      private DateTime[] P00TT4_A472ContagemResultado_DataEntrega ;
      private bool[] P00TT4_n472ContagemResultado_DataEntrega ;
      private decimal[] P00TT4_A1597ContagemResultado_CntSrvVlrUndCnt ;
      private bool[] P00TT4_n1597ContagemResultado_CntSrvVlrUndCnt ;
      private decimal[] P00TT4_A512ContagemResultado_ValorPF ;
      private bool[] P00TT4_n512ContagemResultado_ValorPF ;
      private decimal[] P00TT4_A1616ContagemResultado_CntVlrUndCnt ;
      private bool[] P00TT4_n1616ContagemResultado_CntVlrUndCnt ;
      private int[] P00TT4_A890ContagemResultado_Responsavel ;
      private bool[] P00TT4_n890ContagemResultado_Responsavel ;
      private decimal[] P00TT4_A684ContagemResultado_PFBFSUltima ;
      private bool[] P00TT4_n684ContagemResultado_PFBFSUltima ;
      private DateTime[] P00TT5_A912ContagemResultado_HoraEntrega ;
      private bool[] P00TT5_n912ContagemResultado_HoraEntrega ;
      private int[] P00TT5_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00TT5_n1553ContagemResultado_CntSrvCod ;
      private short[] P00TT5_A1227ContagemResultado_PrazoInicialDias ;
      private bool[] P00TT5_n1227ContagemResultado_PrazoInicialDias ;
      private DateTime[] P00TT5_A472ContagemResultado_DataEntrega ;
      private bool[] P00TT5_n472ContagemResultado_DataEntrega ;
      private decimal[] P00TT5_A512ContagemResultado_ValorPF ;
      private bool[] P00TT5_n512ContagemResultado_ValorPF ;
      private int[] P00TT5_A456ContagemResultado_Codigo ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV8Codigos ;
   }

   public class prc_alteracontrato__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00TT4( IGxContext context ,
                                             int A456ContagemResultado_Codigo ,
                                             IGxCollection AV8Codigos ,
                                             int A1603ContagemResultado_CntCod ,
                                             int AV10Contrato_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [1] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T3.[Contrato_Codigo] AS ContagemResultado_CntCod, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_PFBFSImp], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_HoraEntrega], T4.[Contrato_Numero] AS ContagemResultado_CntNum, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_PrazoInicialDias], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DataEntrega], T3.[Servico_VlrUnidadeContratada] AS ContagemResultado_CntSrvVlrUndCnt, T1.[ContagemResultado_ValorPF], T4.[Contrato_ValorUnidadeContratacao] AS ContagemResultado_CntVlrUndCnt, T1.[ContagemResultado_Responsavel], COALESCE( T2.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima FROM ((([ContagemResultado] T1 WITH (UPDLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T2 ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contrato] T4 WITH (NOLOCK) ON T4.[Contrato_Codigo] = T3.[Contrato_Codigo])";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV8Codigos, "T1.[ContagemResultado_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and (T3.[Contrato_Codigo] <> @AV10Contrato_Codigo)";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Codigo]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 1 :
                     return conditional_P00TT4(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new BatchUpdateCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00TT2 ;
          prmP00TT2 = new Object[] {
          new Object[] {"@AV10Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV16Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00TT5 ;
          prmP00TT5 = new Object[] {
          new Object[] {"@ContagemResultado_HoraEntrega",SqlDbType.DateTime,0,5} ,
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_PrazoInicialDias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_DataEntrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_ValorPF",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00TT4 ;
          prmP00TT4 = new Object[] {
          new Object[] {"@AV10Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00TT2", "SELECT TOP 1 T1.[Servico_Codigo], T1.[Contrato_Codigo], T2.[Contrato_Numero], T1.[ContratoServicos_Codigo], T1.[ContratoServicos_PrazoTpDias] FROM ([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE (T1.[Contrato_Codigo] = @AV10Contrato_Codigo) AND (T1.[Servico_Codigo] = @AV16Servico_Codigo) ORDER BY T1.[Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00TT2,1,0,false,true )
             ,new CursorDef("P00TT4", "scmdbuf",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00TT4,1,0,true,false )
             ,new CursorDef("P00TT5", "UPDATE [ContagemResultado] SET [ContagemResultado_HoraEntrega]=@ContagemResultado_HoraEntrega, [ContagemResultado_CntSrvCod]=@ContagemResultado_CntSrvCod, [ContagemResultado_PrazoInicialDias]=@ContagemResultado_PrazoInicialDias, [ContagemResultado_DataEntrega]=@ContagemResultado_DataEntrega, [ContagemResultado_ValorPF]=@ContagemResultado_ValorPF  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00TT5)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(4) ;
                ((DateTime[]) buf[6])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 20) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((short[]) buf[12])[0] = rslt.getShort(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getString(9, 1) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[16])[0] = rslt.getGXDate(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((decimal[]) buf[18])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((decimal[]) buf[20])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((decimal[]) buf[22])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((int[]) buf[24])[0] = rslt.getInt(14) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((decimal[]) buf[26])[0] = rslt.getDecimal(15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[1]);
                }
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(3, (short)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(4, (DateTime)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[9]);
                }
                stmt.SetParameter(6, (int)parms[10]);
                return;
       }
    }

 }

}
