/*
               File: type_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato
        Description: SDT_WS_ConsultarAutorizacaoUsuario
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:30:8.28
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_WS_ConsultarAutorizacaoUsuario.AreaTrabalho.Contrato" )]
   [XmlType(TypeName =  "SDT_WS_ConsultarAutorizacaoUsuario.AreaTrabalho.Contrato" , Namespace = "AutorizacaoUsuario" )]
   [Serializable]
   public class SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato : GxUserType
   {
      public SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato_Contrato_numero = "";
      }

      public SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "AutorizacaoUsuario" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato obj ;
         obj = this;
         obj.gxTpr_Contrato_codigo = deserialized.gxTpr_Contrato_codigo;
         obj.gxTpr_Contrato_numero = deserialized.gxTpr_Contrato_numero;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_Codigo") )
               {
                  gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato_Contrato_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contrato_Numero") )
               {
                  gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato_Contrato_numero = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_WS_ConsultarAutorizacaoUsuario.AreaTrabalho.Contrato";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Contrato_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato_Contrato_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "AutorizacaoUsuario") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "AutorizacaoUsuario");
         }
         oWriter.WriteElement("Contrato_Numero", StringUtil.RTrim( gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato_Contrato_numero));
         if ( StringUtil.StrCmp(sNameSpace, "AutorizacaoUsuario") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "AutorizacaoUsuario");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Contrato_Codigo", gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato_Contrato_codigo, false);
         AddObjectProperty("Contrato_Numero", gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato_Contrato_numero, false);
         return  ;
      }

      [  SoapElement( ElementName = "Contrato_Codigo" )]
      [  XmlElement( ElementName = "Contrato_Codigo"   )]
      public int gxTpr_Contrato_codigo
      {
         get {
            return gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato_Contrato_codigo ;
         }

         set {
            gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato_Contrato_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Contrato_Numero" )]
      [  XmlElement( ElementName = "Contrato_Numero"   )]
      public String gxTpr_Contrato_numero
      {
         get {
            return gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato_Contrato_numero ;
         }

         set {
            gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato_Contrato_numero = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato_Contrato_numero = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected int gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato_Contrato_codigo ;
      protected String gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato_Contrato_numero ;
      protected String sTagName ;
   }

   [DataContract(Name = @"SDT_WS_ConsultarAutorizacaoUsuario.AreaTrabalho.Contrato", Namespace = "AutorizacaoUsuario")]
   public class SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato_RESTInterface : GxGenericCollectionItem<SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato_RESTInterface( ) : base()
      {
      }

      public SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato_RESTInterface( SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Contrato_Codigo" , Order = 0 )]
      public Nullable<int> gxTpr_Contrato_codigo
      {
         get {
            return sdt.gxTpr_Contrato_codigo ;
         }

         set {
            sdt.gxTpr_Contrato_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contrato_Numero" , Order = 1 )]
      public String gxTpr_Contrato_numero
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contrato_numero) ;
         }

         set {
            sdt.gxTpr_Contrato_numero = (String)(value);
         }

      }

      public SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato sdt
      {
         get {
            return (SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato() ;
         }
      }

   }

}
