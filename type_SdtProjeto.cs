/*
               File: type_SdtProjeto
        Description: Projeto
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:30:26.7
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "Projeto" )]
   [XmlType(TypeName =  "Projeto" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [System.Xml.Serialization.XmlInclude( typeof( SdtProjeto_Anexos ))]
   [Serializable]
   public class SdtProjeto : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtProjeto( )
      {
         /* Constructor for serialization */
         gxTv_SdtProjeto_Projeto_nome = "";
         gxTv_SdtProjeto_Projeto_sigla = "";
         gxTv_SdtProjeto_Projeto_tecnicacontagem = "";
         gxTv_SdtProjeto_Projeto_introducao = "";
         gxTv_SdtProjeto_Projeto_escopo = "";
         gxTv_SdtProjeto_Projeto_previsao = DateTime.MinValue;
         gxTv_SdtProjeto_Projeto_status = "";
         gxTv_SdtProjeto_Projeto_identificador = "";
         gxTv_SdtProjeto_Projeto_objetivo = "";
         gxTv_SdtProjeto_Projeto_dtinicio = DateTime.MinValue;
         gxTv_SdtProjeto_Projeto_dtfim = DateTime.MinValue;
         gxTv_SdtProjeto_Projeto_areatrabalhodescricao = "";
         gxTv_SdtProjeto_Projeto_gpoobjctrlnome = "";
         gxTv_SdtProjeto_Projeto_sistemanome = "";
         gxTv_SdtProjeto_Projeto_sistemasigla = "";
         gxTv_SdtProjeto_Projeto_modulonome = "";
         gxTv_SdtProjeto_Projeto_modulosigla = "";
         gxTv_SdtProjeto_Mode = "";
         gxTv_SdtProjeto_Projeto_nome_Z = "";
         gxTv_SdtProjeto_Projeto_sigla_Z = "";
         gxTv_SdtProjeto_Projeto_tecnicacontagem_Z = "";
         gxTv_SdtProjeto_Projeto_previsao_Z = DateTime.MinValue;
         gxTv_SdtProjeto_Projeto_status_Z = "";
         gxTv_SdtProjeto_Projeto_identificador_Z = "";
         gxTv_SdtProjeto_Projeto_dtinicio_Z = DateTime.MinValue;
         gxTv_SdtProjeto_Projeto_dtfim_Z = DateTime.MinValue;
         gxTv_SdtProjeto_Projeto_areatrabalhodescricao_Z = "";
         gxTv_SdtProjeto_Projeto_gpoobjctrlnome_Z = "";
         gxTv_SdtProjeto_Projeto_sistemanome_Z = "";
         gxTv_SdtProjeto_Projeto_sistemasigla_Z = "";
         gxTv_SdtProjeto_Projeto_modulonome_Z = "";
         gxTv_SdtProjeto_Projeto_modulosigla_Z = "";
      }

      public SdtProjeto( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV648Projeto_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV648Projeto_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"Projeto_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "Projeto");
         metadata.Set("BT", "Projeto");
         metadata.Set("PK", "[ \"Projeto_Codigo\" ]");
         metadata.Set("PKAssigned", "[ \"Projeto_Codigo\" ]");
         metadata.Set("Levels", "[ \"Anexos\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"AreaTrabalho_Codigo\" ],\"FKMap\":[ \"Projeto_AreaTrabalhoCodigo-AreaTrabalho_Codigo\" ] },{ \"FK\":[ \"GpoObjCtrl_Codigo\" ],\"FKMap\":[ \"Projeto_GpoObjCtrlCodigo-GpoObjCtrl_Codigo\" ] },{ \"FK\":[ \"Modulo_Codigo\" ],\"FKMap\":[ \"Projeto_ModuloCodigo-Modulo_Codigo\" ] },{ \"FK\":[ \"Sistema_Codigo\" ],\"FKMap\":[ \"Projeto_SistemaCodigo-Sistema_Codigo\" ] },{ \"FK\":[ \"TipoProjeto_Codigo\" ],\"FKMap\":[ \"Projeto_TipoProjetoCod-TipoProjeto_Codigo\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Anexos_GxSilentTrnGridCollection" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_nome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_sigla_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_tipoprojetocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_tecnicacontagem_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_previsao_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_gerentecod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_servicocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_custo_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_prazo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_esforco_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_sistemacod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_status_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_fatorescala_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_fatormultiplicador_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_constacocomo_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_incremental_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_identificador_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_dtinicio_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_dtfim_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_prazoprevisto_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_esforcoprevisto_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_custoprevisto_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_areatrabalhocodigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_areatrabalhodescricao_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_gpoobjctrlcodigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_gpoobjctrlnome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_sistemacodigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_sistemanome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_sistemasigla_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_modulocodigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_modulonome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_modulosigla_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_anexosequecial_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_tipoprojetocod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_introducao_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_previsao_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_gerentecod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_servicocod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_esforco_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_fatorescala_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_fatormultiplicador_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_constacocomo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_incremental_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_identificador_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_objetivo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_dtinicio_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_dtfim_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_prazoprevisto_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_esforcoprevisto_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_custoprevisto_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_areatrabalhocodigo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_areatrabalhodescricao_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_gpoobjctrlcodigo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_gpoobjctrlnome_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_sistemacodigo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_sistemanome_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_sistemasigla_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_modulocodigo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_modulonome_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_modulosigla_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_anexosequecial_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtProjeto deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtProjeto)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtProjeto obj ;
         obj = this;
         obj.gxTpr_Projeto_codigo = deserialized.gxTpr_Projeto_codigo;
         obj.gxTpr_Projeto_nome = deserialized.gxTpr_Projeto_nome;
         obj.gxTpr_Projeto_sigla = deserialized.gxTpr_Projeto_sigla;
         obj.gxTpr_Projeto_tipoprojetocod = deserialized.gxTpr_Projeto_tipoprojetocod;
         obj.gxTpr_Projeto_tecnicacontagem = deserialized.gxTpr_Projeto_tecnicacontagem;
         obj.gxTpr_Projeto_introducao = deserialized.gxTpr_Projeto_introducao;
         obj.gxTpr_Projeto_escopo = deserialized.gxTpr_Projeto_escopo;
         obj.gxTpr_Projeto_previsao = deserialized.gxTpr_Projeto_previsao;
         obj.gxTpr_Projeto_gerentecod = deserialized.gxTpr_Projeto_gerentecod;
         obj.gxTpr_Projeto_servicocod = deserialized.gxTpr_Projeto_servicocod;
         obj.gxTpr_Projeto_custo = deserialized.gxTpr_Projeto_custo;
         obj.gxTpr_Projeto_prazo = deserialized.gxTpr_Projeto_prazo;
         obj.gxTpr_Projeto_esforco = deserialized.gxTpr_Projeto_esforco;
         obj.gxTpr_Projeto_sistemacod = deserialized.gxTpr_Projeto_sistemacod;
         obj.gxTpr_Projeto_status = deserialized.gxTpr_Projeto_status;
         obj.gxTpr_Projeto_fatorescala = deserialized.gxTpr_Projeto_fatorescala;
         obj.gxTpr_Projeto_fatormultiplicador = deserialized.gxTpr_Projeto_fatormultiplicador;
         obj.gxTpr_Projeto_constacocomo = deserialized.gxTpr_Projeto_constacocomo;
         obj.gxTpr_Projeto_incremental = deserialized.gxTpr_Projeto_incremental;
         obj.gxTpr_Projeto_identificador = deserialized.gxTpr_Projeto_identificador;
         obj.gxTpr_Projeto_objetivo = deserialized.gxTpr_Projeto_objetivo;
         obj.gxTpr_Projeto_dtinicio = deserialized.gxTpr_Projeto_dtinicio;
         obj.gxTpr_Projeto_dtfim = deserialized.gxTpr_Projeto_dtfim;
         obj.gxTpr_Projeto_prazoprevisto = deserialized.gxTpr_Projeto_prazoprevisto;
         obj.gxTpr_Projeto_esforcoprevisto = deserialized.gxTpr_Projeto_esforcoprevisto;
         obj.gxTpr_Projeto_custoprevisto = deserialized.gxTpr_Projeto_custoprevisto;
         obj.gxTpr_Projeto_areatrabalhocodigo = deserialized.gxTpr_Projeto_areatrabalhocodigo;
         obj.gxTpr_Projeto_areatrabalhodescricao = deserialized.gxTpr_Projeto_areatrabalhodescricao;
         obj.gxTpr_Projeto_gpoobjctrlcodigo = deserialized.gxTpr_Projeto_gpoobjctrlcodigo;
         obj.gxTpr_Projeto_gpoobjctrlnome = deserialized.gxTpr_Projeto_gpoobjctrlnome;
         obj.gxTpr_Projeto_sistemacodigo = deserialized.gxTpr_Projeto_sistemacodigo;
         obj.gxTpr_Projeto_sistemanome = deserialized.gxTpr_Projeto_sistemanome;
         obj.gxTpr_Projeto_sistemasigla = deserialized.gxTpr_Projeto_sistemasigla;
         obj.gxTpr_Projeto_modulocodigo = deserialized.gxTpr_Projeto_modulocodigo;
         obj.gxTpr_Projeto_modulonome = deserialized.gxTpr_Projeto_modulonome;
         obj.gxTpr_Projeto_modulosigla = deserialized.gxTpr_Projeto_modulosigla;
         obj.gxTpr_Projeto_anexosequecial = deserialized.gxTpr_Projeto_anexosequecial;
         obj.gxTpr_Anexos = deserialized.gxTpr_Anexos;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Projeto_codigo_Z = deserialized.gxTpr_Projeto_codigo_Z;
         obj.gxTpr_Projeto_nome_Z = deserialized.gxTpr_Projeto_nome_Z;
         obj.gxTpr_Projeto_sigla_Z = deserialized.gxTpr_Projeto_sigla_Z;
         obj.gxTpr_Projeto_tipoprojetocod_Z = deserialized.gxTpr_Projeto_tipoprojetocod_Z;
         obj.gxTpr_Projeto_tecnicacontagem_Z = deserialized.gxTpr_Projeto_tecnicacontagem_Z;
         obj.gxTpr_Projeto_previsao_Z = deserialized.gxTpr_Projeto_previsao_Z;
         obj.gxTpr_Projeto_gerentecod_Z = deserialized.gxTpr_Projeto_gerentecod_Z;
         obj.gxTpr_Projeto_servicocod_Z = deserialized.gxTpr_Projeto_servicocod_Z;
         obj.gxTpr_Projeto_custo_Z = deserialized.gxTpr_Projeto_custo_Z;
         obj.gxTpr_Projeto_prazo_Z = deserialized.gxTpr_Projeto_prazo_Z;
         obj.gxTpr_Projeto_esforco_Z = deserialized.gxTpr_Projeto_esforco_Z;
         obj.gxTpr_Projeto_sistemacod_Z = deserialized.gxTpr_Projeto_sistemacod_Z;
         obj.gxTpr_Projeto_status_Z = deserialized.gxTpr_Projeto_status_Z;
         obj.gxTpr_Projeto_fatorescala_Z = deserialized.gxTpr_Projeto_fatorescala_Z;
         obj.gxTpr_Projeto_fatormultiplicador_Z = deserialized.gxTpr_Projeto_fatormultiplicador_Z;
         obj.gxTpr_Projeto_constacocomo_Z = deserialized.gxTpr_Projeto_constacocomo_Z;
         obj.gxTpr_Projeto_incremental_Z = deserialized.gxTpr_Projeto_incremental_Z;
         obj.gxTpr_Projeto_identificador_Z = deserialized.gxTpr_Projeto_identificador_Z;
         obj.gxTpr_Projeto_dtinicio_Z = deserialized.gxTpr_Projeto_dtinicio_Z;
         obj.gxTpr_Projeto_dtfim_Z = deserialized.gxTpr_Projeto_dtfim_Z;
         obj.gxTpr_Projeto_prazoprevisto_Z = deserialized.gxTpr_Projeto_prazoprevisto_Z;
         obj.gxTpr_Projeto_esforcoprevisto_Z = deserialized.gxTpr_Projeto_esforcoprevisto_Z;
         obj.gxTpr_Projeto_custoprevisto_Z = deserialized.gxTpr_Projeto_custoprevisto_Z;
         obj.gxTpr_Projeto_areatrabalhocodigo_Z = deserialized.gxTpr_Projeto_areatrabalhocodigo_Z;
         obj.gxTpr_Projeto_areatrabalhodescricao_Z = deserialized.gxTpr_Projeto_areatrabalhodescricao_Z;
         obj.gxTpr_Projeto_gpoobjctrlcodigo_Z = deserialized.gxTpr_Projeto_gpoobjctrlcodigo_Z;
         obj.gxTpr_Projeto_gpoobjctrlnome_Z = deserialized.gxTpr_Projeto_gpoobjctrlnome_Z;
         obj.gxTpr_Projeto_sistemacodigo_Z = deserialized.gxTpr_Projeto_sistemacodigo_Z;
         obj.gxTpr_Projeto_sistemanome_Z = deserialized.gxTpr_Projeto_sistemanome_Z;
         obj.gxTpr_Projeto_sistemasigla_Z = deserialized.gxTpr_Projeto_sistemasigla_Z;
         obj.gxTpr_Projeto_modulocodigo_Z = deserialized.gxTpr_Projeto_modulocodigo_Z;
         obj.gxTpr_Projeto_modulonome_Z = deserialized.gxTpr_Projeto_modulonome_Z;
         obj.gxTpr_Projeto_modulosigla_Z = deserialized.gxTpr_Projeto_modulosigla_Z;
         obj.gxTpr_Projeto_anexosequecial_Z = deserialized.gxTpr_Projeto_anexosequecial_Z;
         obj.gxTpr_Projeto_tipoprojetocod_N = deserialized.gxTpr_Projeto_tipoprojetocod_N;
         obj.gxTpr_Projeto_introducao_N = deserialized.gxTpr_Projeto_introducao_N;
         obj.gxTpr_Projeto_previsao_N = deserialized.gxTpr_Projeto_previsao_N;
         obj.gxTpr_Projeto_gerentecod_N = deserialized.gxTpr_Projeto_gerentecod_N;
         obj.gxTpr_Projeto_servicocod_N = deserialized.gxTpr_Projeto_servicocod_N;
         obj.gxTpr_Projeto_esforco_N = deserialized.gxTpr_Projeto_esforco_N;
         obj.gxTpr_Projeto_fatorescala_N = deserialized.gxTpr_Projeto_fatorescala_N;
         obj.gxTpr_Projeto_fatormultiplicador_N = deserialized.gxTpr_Projeto_fatormultiplicador_N;
         obj.gxTpr_Projeto_constacocomo_N = deserialized.gxTpr_Projeto_constacocomo_N;
         obj.gxTpr_Projeto_incremental_N = deserialized.gxTpr_Projeto_incremental_N;
         obj.gxTpr_Projeto_identificador_N = deserialized.gxTpr_Projeto_identificador_N;
         obj.gxTpr_Projeto_objetivo_N = deserialized.gxTpr_Projeto_objetivo_N;
         obj.gxTpr_Projeto_dtinicio_N = deserialized.gxTpr_Projeto_dtinicio_N;
         obj.gxTpr_Projeto_dtfim_N = deserialized.gxTpr_Projeto_dtfim_N;
         obj.gxTpr_Projeto_prazoprevisto_N = deserialized.gxTpr_Projeto_prazoprevisto_N;
         obj.gxTpr_Projeto_esforcoprevisto_N = deserialized.gxTpr_Projeto_esforcoprevisto_N;
         obj.gxTpr_Projeto_custoprevisto_N = deserialized.gxTpr_Projeto_custoprevisto_N;
         obj.gxTpr_Projeto_areatrabalhocodigo_N = deserialized.gxTpr_Projeto_areatrabalhocodigo_N;
         obj.gxTpr_Projeto_areatrabalhodescricao_N = deserialized.gxTpr_Projeto_areatrabalhodescricao_N;
         obj.gxTpr_Projeto_gpoobjctrlcodigo_N = deserialized.gxTpr_Projeto_gpoobjctrlcodigo_N;
         obj.gxTpr_Projeto_gpoobjctrlnome_N = deserialized.gxTpr_Projeto_gpoobjctrlnome_N;
         obj.gxTpr_Projeto_sistemacodigo_N = deserialized.gxTpr_Projeto_sistemacodigo_N;
         obj.gxTpr_Projeto_sistemanome_N = deserialized.gxTpr_Projeto_sistemanome_N;
         obj.gxTpr_Projeto_sistemasigla_N = deserialized.gxTpr_Projeto_sistemasigla_N;
         obj.gxTpr_Projeto_modulocodigo_N = deserialized.gxTpr_Projeto_modulocodigo_N;
         obj.gxTpr_Projeto_modulonome_N = deserialized.gxTpr_Projeto_modulonome_N;
         obj.gxTpr_Projeto_modulosigla_N = deserialized.gxTpr_Projeto_modulosigla_N;
         obj.gxTpr_Projeto_anexosequecial_N = deserialized.gxTpr_Projeto_anexosequecial_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Codigo") )
               {
                  gxTv_SdtProjeto_Projeto_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Nome") )
               {
                  gxTv_SdtProjeto_Projeto_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Sigla") )
               {
                  gxTv_SdtProjeto_Projeto_sigla = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_TipoProjetoCod") )
               {
                  gxTv_SdtProjeto_Projeto_tipoprojetocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_TecnicaContagem") )
               {
                  gxTv_SdtProjeto_Projeto_tecnicacontagem = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Introducao") )
               {
                  gxTv_SdtProjeto_Projeto_introducao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Escopo") )
               {
                  gxTv_SdtProjeto_Projeto_escopo = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Previsao") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtProjeto_Projeto_previsao = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtProjeto_Projeto_previsao = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_GerenteCod") )
               {
                  gxTv_SdtProjeto_Projeto_gerentecod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_ServicoCod") )
               {
                  gxTv_SdtProjeto_Projeto_servicocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Custo") )
               {
                  gxTv_SdtProjeto_Projeto_custo = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Prazo") )
               {
                  gxTv_SdtProjeto_Projeto_prazo = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Esforco") )
               {
                  gxTv_SdtProjeto_Projeto_esforco = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_SistemaCod") )
               {
                  gxTv_SdtProjeto_Projeto_sistemacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Status") )
               {
                  gxTv_SdtProjeto_Projeto_status = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_FatorEscala") )
               {
                  gxTv_SdtProjeto_Projeto_fatorescala = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_FatorMultiplicador") )
               {
                  gxTv_SdtProjeto_Projeto_fatormultiplicador = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_ConstACocomo") )
               {
                  gxTv_SdtProjeto_Projeto_constacocomo = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Incremental") )
               {
                  gxTv_SdtProjeto_Projeto_incremental = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Identificador") )
               {
                  gxTv_SdtProjeto_Projeto_identificador = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Objetivo") )
               {
                  gxTv_SdtProjeto_Projeto_objetivo = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_DTInicio") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtProjeto_Projeto_dtinicio = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtProjeto_Projeto_dtinicio = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_DTFim") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtProjeto_Projeto_dtfim = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtProjeto_Projeto_dtfim = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_PrazoPrevisto") )
               {
                  gxTv_SdtProjeto_Projeto_prazoprevisto = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_EsforcoPrevisto") )
               {
                  gxTv_SdtProjeto_Projeto_esforcoprevisto = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_CustoPrevisto") )
               {
                  gxTv_SdtProjeto_Projeto_custoprevisto = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_AreaTrabalhoCodigo") )
               {
                  gxTv_SdtProjeto_Projeto_areatrabalhocodigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_AreaTrabalhoDescricao") )
               {
                  gxTv_SdtProjeto_Projeto_areatrabalhodescricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_GpoObjCtrlCodigo") )
               {
                  gxTv_SdtProjeto_Projeto_gpoobjctrlcodigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_GpoObjCtrlNome") )
               {
                  gxTv_SdtProjeto_Projeto_gpoobjctrlnome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_SistemaCodigo") )
               {
                  gxTv_SdtProjeto_Projeto_sistemacodigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_SistemaNome") )
               {
                  gxTv_SdtProjeto_Projeto_sistemanome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_SistemaSigla") )
               {
                  gxTv_SdtProjeto_Projeto_sistemasigla = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_ModuloCodigo") )
               {
                  gxTv_SdtProjeto_Projeto_modulocodigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_ModuloNome") )
               {
                  gxTv_SdtProjeto_Projeto_modulonome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_ModuloSigla") )
               {
                  gxTv_SdtProjeto_Projeto_modulosigla = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_AnexoSequecial") )
               {
                  gxTv_SdtProjeto_Projeto_anexosequecial = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Anexos") )
               {
                  if ( gxTv_SdtProjeto_Anexos == null )
                  {
                     gxTv_SdtProjeto_Anexos = new GxSilentTrnGridCollection( context, "Projeto.Anexos", "GxEv3Up14_MeetrikaVs3", "SdtProjeto_Anexos", "GeneXus.Programs");
                  }
                  if ( ( oReader.IsSimple == 0 ) || ( oReader.AttributeCount > 0 ) )
                  {
                     GXSoapError = gxTv_SdtProjeto_Anexos.readxml(oReader, "Anexos");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtProjeto_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtProjeto_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Codigo_Z") )
               {
                  gxTv_SdtProjeto_Projeto_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Nome_Z") )
               {
                  gxTv_SdtProjeto_Projeto_nome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Sigla_Z") )
               {
                  gxTv_SdtProjeto_Projeto_sigla_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_TipoProjetoCod_Z") )
               {
                  gxTv_SdtProjeto_Projeto_tipoprojetocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_TecnicaContagem_Z") )
               {
                  gxTv_SdtProjeto_Projeto_tecnicacontagem_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Previsao_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtProjeto_Projeto_previsao_Z = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtProjeto_Projeto_previsao_Z = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_GerenteCod_Z") )
               {
                  gxTv_SdtProjeto_Projeto_gerentecod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_ServicoCod_Z") )
               {
                  gxTv_SdtProjeto_Projeto_servicocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Custo_Z") )
               {
                  gxTv_SdtProjeto_Projeto_custo_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Prazo_Z") )
               {
                  gxTv_SdtProjeto_Projeto_prazo_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Esforco_Z") )
               {
                  gxTv_SdtProjeto_Projeto_esforco_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_SistemaCod_Z") )
               {
                  gxTv_SdtProjeto_Projeto_sistemacod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Status_Z") )
               {
                  gxTv_SdtProjeto_Projeto_status_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_FatorEscala_Z") )
               {
                  gxTv_SdtProjeto_Projeto_fatorescala_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_FatorMultiplicador_Z") )
               {
                  gxTv_SdtProjeto_Projeto_fatormultiplicador_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_ConstACocomo_Z") )
               {
                  gxTv_SdtProjeto_Projeto_constacocomo_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Incremental_Z") )
               {
                  gxTv_SdtProjeto_Projeto_incremental_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Identificador_Z") )
               {
                  gxTv_SdtProjeto_Projeto_identificador_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_DTInicio_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtProjeto_Projeto_dtinicio_Z = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtProjeto_Projeto_dtinicio_Z = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_DTFim_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtProjeto_Projeto_dtfim_Z = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtProjeto_Projeto_dtfim_Z = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_PrazoPrevisto_Z") )
               {
                  gxTv_SdtProjeto_Projeto_prazoprevisto_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_EsforcoPrevisto_Z") )
               {
                  gxTv_SdtProjeto_Projeto_esforcoprevisto_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_CustoPrevisto_Z") )
               {
                  gxTv_SdtProjeto_Projeto_custoprevisto_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_AreaTrabalhoCodigo_Z") )
               {
                  gxTv_SdtProjeto_Projeto_areatrabalhocodigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_AreaTrabalhoDescricao_Z") )
               {
                  gxTv_SdtProjeto_Projeto_areatrabalhodescricao_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_GpoObjCtrlCodigo_Z") )
               {
                  gxTv_SdtProjeto_Projeto_gpoobjctrlcodigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_GpoObjCtrlNome_Z") )
               {
                  gxTv_SdtProjeto_Projeto_gpoobjctrlnome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_SistemaCodigo_Z") )
               {
                  gxTv_SdtProjeto_Projeto_sistemacodigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_SistemaNome_Z") )
               {
                  gxTv_SdtProjeto_Projeto_sistemanome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_SistemaSigla_Z") )
               {
                  gxTv_SdtProjeto_Projeto_sistemasigla_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_ModuloCodigo_Z") )
               {
                  gxTv_SdtProjeto_Projeto_modulocodigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_ModuloNome_Z") )
               {
                  gxTv_SdtProjeto_Projeto_modulonome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_ModuloSigla_Z") )
               {
                  gxTv_SdtProjeto_Projeto_modulosigla_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_AnexoSequecial_Z") )
               {
                  gxTv_SdtProjeto_Projeto_anexosequecial_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_TipoProjetoCod_N") )
               {
                  gxTv_SdtProjeto_Projeto_tipoprojetocod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Introducao_N") )
               {
                  gxTv_SdtProjeto_Projeto_introducao_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Previsao_N") )
               {
                  gxTv_SdtProjeto_Projeto_previsao_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_GerenteCod_N") )
               {
                  gxTv_SdtProjeto_Projeto_gerentecod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_ServicoCod_N") )
               {
                  gxTv_SdtProjeto_Projeto_servicocod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Esforco_N") )
               {
                  gxTv_SdtProjeto_Projeto_esforco_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_FatorEscala_N") )
               {
                  gxTv_SdtProjeto_Projeto_fatorescala_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_FatorMultiplicador_N") )
               {
                  gxTv_SdtProjeto_Projeto_fatormultiplicador_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_ConstACocomo_N") )
               {
                  gxTv_SdtProjeto_Projeto_constacocomo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Incremental_N") )
               {
                  gxTv_SdtProjeto_Projeto_incremental_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Identificador_N") )
               {
                  gxTv_SdtProjeto_Projeto_identificador_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Objetivo_N") )
               {
                  gxTv_SdtProjeto_Projeto_objetivo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_DTInicio_N") )
               {
                  gxTv_SdtProjeto_Projeto_dtinicio_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_DTFim_N") )
               {
                  gxTv_SdtProjeto_Projeto_dtfim_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_PrazoPrevisto_N") )
               {
                  gxTv_SdtProjeto_Projeto_prazoprevisto_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_EsforcoPrevisto_N") )
               {
                  gxTv_SdtProjeto_Projeto_esforcoprevisto_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_CustoPrevisto_N") )
               {
                  gxTv_SdtProjeto_Projeto_custoprevisto_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_AreaTrabalhoCodigo_N") )
               {
                  gxTv_SdtProjeto_Projeto_areatrabalhocodigo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_AreaTrabalhoDescricao_N") )
               {
                  gxTv_SdtProjeto_Projeto_areatrabalhodescricao_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_GpoObjCtrlCodigo_N") )
               {
                  gxTv_SdtProjeto_Projeto_gpoobjctrlcodigo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_GpoObjCtrlNome_N") )
               {
                  gxTv_SdtProjeto_Projeto_gpoobjctrlnome_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_SistemaCodigo_N") )
               {
                  gxTv_SdtProjeto_Projeto_sistemacodigo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_SistemaNome_N") )
               {
                  gxTv_SdtProjeto_Projeto_sistemanome_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_SistemaSigla_N") )
               {
                  gxTv_SdtProjeto_Projeto_sistemasigla_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_ModuloCodigo_N") )
               {
                  gxTv_SdtProjeto_Projeto_modulocodigo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_ModuloNome_N") )
               {
                  gxTv_SdtProjeto_Projeto_modulonome_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_ModuloSigla_N") )
               {
                  gxTv_SdtProjeto_Projeto_modulosigla_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_AnexoSequecial_N") )
               {
                  gxTv_SdtProjeto_Projeto_anexosequecial_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "Projeto";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Projeto_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Projeto_Nome", StringUtil.RTrim( gxTv_SdtProjeto_Projeto_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Projeto_Sigla", StringUtil.RTrim( gxTv_SdtProjeto_Projeto_sigla));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Projeto_TipoProjetoCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_tipoprojetocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Projeto_TecnicaContagem", StringUtil.RTrim( gxTv_SdtProjeto_Projeto_tecnicacontagem));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Projeto_Introducao", StringUtil.RTrim( gxTv_SdtProjeto_Projeto_introducao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Projeto_Escopo", StringUtil.RTrim( gxTv_SdtProjeto_Projeto_escopo));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( (DateTime.MinValue==gxTv_SdtProjeto_Projeto_previsao) )
         {
            oWriter.WriteStartElement("Projeto_Previsao");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtProjeto_Projeto_previsao)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtProjeto_Projeto_previsao)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtProjeto_Projeto_previsao)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("Projeto_Previsao", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteElement("Projeto_GerenteCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_gerentecod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Projeto_ServicoCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_servicocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Projeto_Custo", StringUtil.Trim( StringUtil.Str( gxTv_SdtProjeto_Projeto_custo, 12, 2)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Projeto_Prazo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_prazo), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Projeto_Esforco", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_esforco), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Projeto_SistemaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_sistemacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Projeto_Status", StringUtil.RTrim( gxTv_SdtProjeto_Projeto_status));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Projeto_FatorEscala", StringUtil.Trim( StringUtil.Str( gxTv_SdtProjeto_Projeto_fatorescala, 6, 2)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Projeto_FatorMultiplicador", StringUtil.Trim( StringUtil.Str( gxTv_SdtProjeto_Projeto_fatormultiplicador, 6, 2)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Projeto_ConstACocomo", StringUtil.Trim( StringUtil.Str( gxTv_SdtProjeto_Projeto_constacocomo, 6, 2)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Projeto_Incremental", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtProjeto_Projeto_incremental)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Projeto_Identificador", StringUtil.RTrim( gxTv_SdtProjeto_Projeto_identificador));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Projeto_Objetivo", StringUtil.RTrim( gxTv_SdtProjeto_Projeto_objetivo));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( (DateTime.MinValue==gxTv_SdtProjeto_Projeto_dtinicio) )
         {
            oWriter.WriteStartElement("Projeto_DTInicio");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtProjeto_Projeto_dtinicio)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtProjeto_Projeto_dtinicio)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtProjeto_Projeto_dtinicio)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("Projeto_DTInicio", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         if ( (DateTime.MinValue==gxTv_SdtProjeto_Projeto_dtfim) )
         {
            oWriter.WriteStartElement("Projeto_DTFim");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtProjeto_Projeto_dtfim)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtProjeto_Projeto_dtfim)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtProjeto_Projeto_dtfim)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("Projeto_DTFim", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteElement("Projeto_PrazoPrevisto", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_prazoprevisto), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Projeto_EsforcoPrevisto", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_esforcoprevisto), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Projeto_CustoPrevisto", StringUtil.Trim( StringUtil.Str( gxTv_SdtProjeto_Projeto_custoprevisto, 12, 2)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Projeto_AreaTrabalhoCodigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_areatrabalhocodigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Projeto_AreaTrabalhoDescricao", StringUtil.RTrim( gxTv_SdtProjeto_Projeto_areatrabalhodescricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Projeto_GpoObjCtrlCodigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_gpoobjctrlcodigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Projeto_GpoObjCtrlNome", StringUtil.RTrim( gxTv_SdtProjeto_Projeto_gpoobjctrlnome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Projeto_SistemaCodigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_sistemacodigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Projeto_SistemaNome", StringUtil.RTrim( gxTv_SdtProjeto_Projeto_sistemanome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Projeto_SistemaSigla", StringUtil.RTrim( gxTv_SdtProjeto_Projeto_sistemasigla));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Projeto_ModuloCodigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_modulocodigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Projeto_ModuloNome", StringUtil.RTrim( gxTv_SdtProjeto_Projeto_modulonome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Projeto_ModuloSigla", StringUtil.RTrim( gxTv_SdtProjeto_Projeto_modulosigla));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Projeto_AnexoSequecial", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_anexosequecial), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( sIncludeState )
         {
            if ( gxTv_SdtProjeto_Anexos != null )
            {
               String sNameSpace1 ;
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") == 0 )
               {
                  sNameSpace1 = "[*:nosend]" + "GxEv3Up14_MeetrikaVs3";
               }
               else
               {
                  sNameSpace1 = "GxEv3Up14_MeetrikaVs3";
               }
               gxTv_SdtProjeto_Anexos.writexml(oWriter, "Anexos", sNameSpace1);
            }
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtProjeto_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_Nome_Z", StringUtil.RTrim( gxTv_SdtProjeto_Projeto_nome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_Sigla_Z", StringUtil.RTrim( gxTv_SdtProjeto_Projeto_sigla_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_TipoProjetoCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_tipoprojetocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_TecnicaContagem_Z", StringUtil.RTrim( gxTv_SdtProjeto_Projeto_tecnicacontagem_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            if ( (DateTime.MinValue==gxTv_SdtProjeto_Projeto_previsao_Z) )
            {
               oWriter.WriteStartElement("Projeto_Previsao_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtProjeto_Projeto_previsao_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtProjeto_Projeto_previsao_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtProjeto_Projeto_previsao_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("Projeto_Previsao_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
               }
            }
            oWriter.WriteElement("Projeto_GerenteCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_gerentecod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_ServicoCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_servicocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_Custo_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtProjeto_Projeto_custo_Z, 12, 2)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_Prazo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_prazo_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_Esforco_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_esforco_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_SistemaCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_sistemacod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_Status_Z", StringUtil.RTrim( gxTv_SdtProjeto_Projeto_status_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_FatorEscala_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtProjeto_Projeto_fatorescala_Z, 6, 2)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_FatorMultiplicador_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtProjeto_Projeto_fatormultiplicador_Z, 6, 2)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_ConstACocomo_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtProjeto_Projeto_constacocomo_Z, 6, 2)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_Incremental_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtProjeto_Projeto_incremental_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_Identificador_Z", StringUtil.RTrim( gxTv_SdtProjeto_Projeto_identificador_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            if ( (DateTime.MinValue==gxTv_SdtProjeto_Projeto_dtinicio_Z) )
            {
               oWriter.WriteStartElement("Projeto_DTInicio_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtProjeto_Projeto_dtinicio_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtProjeto_Projeto_dtinicio_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtProjeto_Projeto_dtinicio_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("Projeto_DTInicio_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
               }
            }
            if ( (DateTime.MinValue==gxTv_SdtProjeto_Projeto_dtfim_Z) )
            {
               oWriter.WriteStartElement("Projeto_DTFim_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtProjeto_Projeto_dtfim_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtProjeto_Projeto_dtfim_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtProjeto_Projeto_dtfim_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("Projeto_DTFim_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
               }
            }
            oWriter.WriteElement("Projeto_PrazoPrevisto_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_prazoprevisto_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_EsforcoPrevisto_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_esforcoprevisto_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_CustoPrevisto_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtProjeto_Projeto_custoprevisto_Z, 12, 2)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_AreaTrabalhoCodigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_areatrabalhocodigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_AreaTrabalhoDescricao_Z", StringUtil.RTrim( gxTv_SdtProjeto_Projeto_areatrabalhodescricao_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_GpoObjCtrlCodigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_gpoobjctrlcodigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_GpoObjCtrlNome_Z", StringUtil.RTrim( gxTv_SdtProjeto_Projeto_gpoobjctrlnome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_SistemaCodigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_sistemacodigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_SistemaNome_Z", StringUtil.RTrim( gxTv_SdtProjeto_Projeto_sistemanome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_SistemaSigla_Z", StringUtil.RTrim( gxTv_SdtProjeto_Projeto_sistemasigla_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_ModuloCodigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_modulocodigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_ModuloNome_Z", StringUtil.RTrim( gxTv_SdtProjeto_Projeto_modulonome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_ModuloSigla_Z", StringUtil.RTrim( gxTv_SdtProjeto_Projeto_modulosigla_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_AnexoSequecial_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_anexosequecial_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_TipoProjetoCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_tipoprojetocod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_Introducao_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_introducao_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_Previsao_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_previsao_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_GerenteCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_gerentecod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_ServicoCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_servicocod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_Esforco_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_esforco_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_FatorEscala_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_fatorescala_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_FatorMultiplicador_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_fatormultiplicador_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_ConstACocomo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_constacocomo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_Incremental_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_incremental_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_Identificador_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_identificador_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_Objetivo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_objetivo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_DTInicio_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_dtinicio_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_DTFim_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_dtfim_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_PrazoPrevisto_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_prazoprevisto_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_EsforcoPrevisto_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_esforcoprevisto_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_CustoPrevisto_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_custoprevisto_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_AreaTrabalhoCodigo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_areatrabalhocodigo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_AreaTrabalhoDescricao_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_areatrabalhodescricao_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_GpoObjCtrlCodigo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_gpoobjctrlcodigo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_GpoObjCtrlNome_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_gpoobjctrlnome_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_SistemaCodigo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_sistemacodigo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_SistemaNome_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_sistemanome_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_SistemaSigla_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_sistemasigla_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_ModuloCodigo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_modulocodigo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_ModuloNome_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_modulonome_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_ModuloSigla_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_modulosigla_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Projeto_AnexoSequecial_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_anexosequecial_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Projeto_Codigo", gxTv_SdtProjeto_Projeto_codigo, false);
         AddObjectProperty("Projeto_Nome", gxTv_SdtProjeto_Projeto_nome, false);
         AddObjectProperty("Projeto_Sigla", gxTv_SdtProjeto_Projeto_sigla, false);
         AddObjectProperty("Projeto_TipoProjetoCod", gxTv_SdtProjeto_Projeto_tipoprojetocod, false);
         AddObjectProperty("Projeto_TecnicaContagem", gxTv_SdtProjeto_Projeto_tecnicacontagem, false);
         AddObjectProperty("Projeto_Introducao", gxTv_SdtProjeto_Projeto_introducao, false);
         AddObjectProperty("Projeto_Escopo", gxTv_SdtProjeto_Projeto_escopo, false);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtProjeto_Projeto_previsao)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtProjeto_Projeto_previsao)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtProjeto_Projeto_previsao)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("Projeto_Previsao", sDateCnv, false);
         AddObjectProperty("Projeto_GerenteCod", gxTv_SdtProjeto_Projeto_gerentecod, false);
         AddObjectProperty("Projeto_ServicoCod", gxTv_SdtProjeto_Projeto_servicocod, false);
         AddObjectProperty("Projeto_Custo", gxTv_SdtProjeto_Projeto_custo, false);
         AddObjectProperty("Projeto_Prazo", gxTv_SdtProjeto_Projeto_prazo, false);
         AddObjectProperty("Projeto_Esforco", gxTv_SdtProjeto_Projeto_esforco, false);
         AddObjectProperty("Projeto_SistemaCod", gxTv_SdtProjeto_Projeto_sistemacod, false);
         AddObjectProperty("Projeto_Status", gxTv_SdtProjeto_Projeto_status, false);
         AddObjectProperty("Projeto_FatorEscala", gxTv_SdtProjeto_Projeto_fatorescala, false);
         AddObjectProperty("Projeto_FatorMultiplicador", gxTv_SdtProjeto_Projeto_fatormultiplicador, false);
         AddObjectProperty("Projeto_ConstACocomo", gxTv_SdtProjeto_Projeto_constacocomo, false);
         AddObjectProperty("Projeto_Incremental", gxTv_SdtProjeto_Projeto_incremental, false);
         AddObjectProperty("Projeto_Identificador", gxTv_SdtProjeto_Projeto_identificador, false);
         AddObjectProperty("Projeto_Objetivo", gxTv_SdtProjeto_Projeto_objetivo, false);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtProjeto_Projeto_dtinicio)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtProjeto_Projeto_dtinicio)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtProjeto_Projeto_dtinicio)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("Projeto_DTInicio", sDateCnv, false);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtProjeto_Projeto_dtfim)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtProjeto_Projeto_dtfim)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtProjeto_Projeto_dtfim)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("Projeto_DTFim", sDateCnv, false);
         AddObjectProperty("Projeto_PrazoPrevisto", gxTv_SdtProjeto_Projeto_prazoprevisto, false);
         AddObjectProperty("Projeto_EsforcoPrevisto", gxTv_SdtProjeto_Projeto_esforcoprevisto, false);
         AddObjectProperty("Projeto_CustoPrevisto", gxTv_SdtProjeto_Projeto_custoprevisto, false);
         AddObjectProperty("Projeto_AreaTrabalhoCodigo", gxTv_SdtProjeto_Projeto_areatrabalhocodigo, false);
         AddObjectProperty("Projeto_AreaTrabalhoDescricao", gxTv_SdtProjeto_Projeto_areatrabalhodescricao, false);
         AddObjectProperty("Projeto_GpoObjCtrlCodigo", gxTv_SdtProjeto_Projeto_gpoobjctrlcodigo, false);
         AddObjectProperty("Projeto_GpoObjCtrlNome", gxTv_SdtProjeto_Projeto_gpoobjctrlnome, false);
         AddObjectProperty("Projeto_SistemaCodigo", gxTv_SdtProjeto_Projeto_sistemacodigo, false);
         AddObjectProperty("Projeto_SistemaNome", gxTv_SdtProjeto_Projeto_sistemanome, false);
         AddObjectProperty("Projeto_SistemaSigla", gxTv_SdtProjeto_Projeto_sistemasigla, false);
         AddObjectProperty("Projeto_ModuloCodigo", gxTv_SdtProjeto_Projeto_modulocodigo, false);
         AddObjectProperty("Projeto_ModuloNome", gxTv_SdtProjeto_Projeto_modulonome, false);
         AddObjectProperty("Projeto_ModuloSigla", gxTv_SdtProjeto_Projeto_modulosigla, false);
         AddObjectProperty("Projeto_AnexoSequecial", gxTv_SdtProjeto_Projeto_anexosequecial, false);
         if ( gxTv_SdtProjeto_Anexos != null )
         {
            AddObjectProperty("Anexos", gxTv_SdtProjeto_Anexos, includeState);
         }
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtProjeto_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtProjeto_Initialized, false);
            AddObjectProperty("Projeto_Codigo_Z", gxTv_SdtProjeto_Projeto_codigo_Z, false);
            AddObjectProperty("Projeto_Nome_Z", gxTv_SdtProjeto_Projeto_nome_Z, false);
            AddObjectProperty("Projeto_Sigla_Z", gxTv_SdtProjeto_Projeto_sigla_Z, false);
            AddObjectProperty("Projeto_TipoProjetoCod_Z", gxTv_SdtProjeto_Projeto_tipoprojetocod_Z, false);
            AddObjectProperty("Projeto_TecnicaContagem_Z", gxTv_SdtProjeto_Projeto_tecnicacontagem_Z, false);
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtProjeto_Projeto_previsao_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtProjeto_Projeto_previsao_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtProjeto_Projeto_previsao_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("Projeto_Previsao_Z", sDateCnv, false);
            AddObjectProperty("Projeto_GerenteCod_Z", gxTv_SdtProjeto_Projeto_gerentecod_Z, false);
            AddObjectProperty("Projeto_ServicoCod_Z", gxTv_SdtProjeto_Projeto_servicocod_Z, false);
            AddObjectProperty("Projeto_Custo_Z", gxTv_SdtProjeto_Projeto_custo_Z, false);
            AddObjectProperty("Projeto_Prazo_Z", gxTv_SdtProjeto_Projeto_prazo_Z, false);
            AddObjectProperty("Projeto_Esforco_Z", gxTv_SdtProjeto_Projeto_esforco_Z, false);
            AddObjectProperty("Projeto_SistemaCod_Z", gxTv_SdtProjeto_Projeto_sistemacod_Z, false);
            AddObjectProperty("Projeto_Status_Z", gxTv_SdtProjeto_Projeto_status_Z, false);
            AddObjectProperty("Projeto_FatorEscala_Z", gxTv_SdtProjeto_Projeto_fatorescala_Z, false);
            AddObjectProperty("Projeto_FatorMultiplicador_Z", gxTv_SdtProjeto_Projeto_fatormultiplicador_Z, false);
            AddObjectProperty("Projeto_ConstACocomo_Z", gxTv_SdtProjeto_Projeto_constacocomo_Z, false);
            AddObjectProperty("Projeto_Incremental_Z", gxTv_SdtProjeto_Projeto_incremental_Z, false);
            AddObjectProperty("Projeto_Identificador_Z", gxTv_SdtProjeto_Projeto_identificador_Z, false);
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtProjeto_Projeto_dtinicio_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtProjeto_Projeto_dtinicio_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtProjeto_Projeto_dtinicio_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("Projeto_DTInicio_Z", sDateCnv, false);
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtProjeto_Projeto_dtfim_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtProjeto_Projeto_dtfim_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtProjeto_Projeto_dtfim_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("Projeto_DTFim_Z", sDateCnv, false);
            AddObjectProperty("Projeto_PrazoPrevisto_Z", gxTv_SdtProjeto_Projeto_prazoprevisto_Z, false);
            AddObjectProperty("Projeto_EsforcoPrevisto_Z", gxTv_SdtProjeto_Projeto_esforcoprevisto_Z, false);
            AddObjectProperty("Projeto_CustoPrevisto_Z", gxTv_SdtProjeto_Projeto_custoprevisto_Z, false);
            AddObjectProperty("Projeto_AreaTrabalhoCodigo_Z", gxTv_SdtProjeto_Projeto_areatrabalhocodigo_Z, false);
            AddObjectProperty("Projeto_AreaTrabalhoDescricao_Z", gxTv_SdtProjeto_Projeto_areatrabalhodescricao_Z, false);
            AddObjectProperty("Projeto_GpoObjCtrlCodigo_Z", gxTv_SdtProjeto_Projeto_gpoobjctrlcodigo_Z, false);
            AddObjectProperty("Projeto_GpoObjCtrlNome_Z", gxTv_SdtProjeto_Projeto_gpoobjctrlnome_Z, false);
            AddObjectProperty("Projeto_SistemaCodigo_Z", gxTv_SdtProjeto_Projeto_sistemacodigo_Z, false);
            AddObjectProperty("Projeto_SistemaNome_Z", gxTv_SdtProjeto_Projeto_sistemanome_Z, false);
            AddObjectProperty("Projeto_SistemaSigla_Z", gxTv_SdtProjeto_Projeto_sistemasigla_Z, false);
            AddObjectProperty("Projeto_ModuloCodigo_Z", gxTv_SdtProjeto_Projeto_modulocodigo_Z, false);
            AddObjectProperty("Projeto_ModuloNome_Z", gxTv_SdtProjeto_Projeto_modulonome_Z, false);
            AddObjectProperty("Projeto_ModuloSigla_Z", gxTv_SdtProjeto_Projeto_modulosigla_Z, false);
            AddObjectProperty("Projeto_AnexoSequecial_Z", gxTv_SdtProjeto_Projeto_anexosequecial_Z, false);
            AddObjectProperty("Projeto_TipoProjetoCod_N", gxTv_SdtProjeto_Projeto_tipoprojetocod_N, false);
            AddObjectProperty("Projeto_Introducao_N", gxTv_SdtProjeto_Projeto_introducao_N, false);
            AddObjectProperty("Projeto_Previsao_N", gxTv_SdtProjeto_Projeto_previsao_N, false);
            AddObjectProperty("Projeto_GerenteCod_N", gxTv_SdtProjeto_Projeto_gerentecod_N, false);
            AddObjectProperty("Projeto_ServicoCod_N", gxTv_SdtProjeto_Projeto_servicocod_N, false);
            AddObjectProperty("Projeto_Esforco_N", gxTv_SdtProjeto_Projeto_esforco_N, false);
            AddObjectProperty("Projeto_FatorEscala_N", gxTv_SdtProjeto_Projeto_fatorescala_N, false);
            AddObjectProperty("Projeto_FatorMultiplicador_N", gxTv_SdtProjeto_Projeto_fatormultiplicador_N, false);
            AddObjectProperty("Projeto_ConstACocomo_N", gxTv_SdtProjeto_Projeto_constacocomo_N, false);
            AddObjectProperty("Projeto_Incremental_N", gxTv_SdtProjeto_Projeto_incremental_N, false);
            AddObjectProperty("Projeto_Identificador_N", gxTv_SdtProjeto_Projeto_identificador_N, false);
            AddObjectProperty("Projeto_Objetivo_N", gxTv_SdtProjeto_Projeto_objetivo_N, false);
            AddObjectProperty("Projeto_DTInicio_N", gxTv_SdtProjeto_Projeto_dtinicio_N, false);
            AddObjectProperty("Projeto_DTFim_N", gxTv_SdtProjeto_Projeto_dtfim_N, false);
            AddObjectProperty("Projeto_PrazoPrevisto_N", gxTv_SdtProjeto_Projeto_prazoprevisto_N, false);
            AddObjectProperty("Projeto_EsforcoPrevisto_N", gxTv_SdtProjeto_Projeto_esforcoprevisto_N, false);
            AddObjectProperty("Projeto_CustoPrevisto_N", gxTv_SdtProjeto_Projeto_custoprevisto_N, false);
            AddObjectProperty("Projeto_AreaTrabalhoCodigo_N", gxTv_SdtProjeto_Projeto_areatrabalhocodigo_N, false);
            AddObjectProperty("Projeto_AreaTrabalhoDescricao_N", gxTv_SdtProjeto_Projeto_areatrabalhodescricao_N, false);
            AddObjectProperty("Projeto_GpoObjCtrlCodigo_N", gxTv_SdtProjeto_Projeto_gpoobjctrlcodigo_N, false);
            AddObjectProperty("Projeto_GpoObjCtrlNome_N", gxTv_SdtProjeto_Projeto_gpoobjctrlnome_N, false);
            AddObjectProperty("Projeto_SistemaCodigo_N", gxTv_SdtProjeto_Projeto_sistemacodigo_N, false);
            AddObjectProperty("Projeto_SistemaNome_N", gxTv_SdtProjeto_Projeto_sistemanome_N, false);
            AddObjectProperty("Projeto_SistemaSigla_N", gxTv_SdtProjeto_Projeto_sistemasigla_N, false);
            AddObjectProperty("Projeto_ModuloCodigo_N", gxTv_SdtProjeto_Projeto_modulocodigo_N, false);
            AddObjectProperty("Projeto_ModuloNome_N", gxTv_SdtProjeto_Projeto_modulonome_N, false);
            AddObjectProperty("Projeto_ModuloSigla_N", gxTv_SdtProjeto_Projeto_modulosigla_N, false);
            AddObjectProperty("Projeto_AnexoSequecial_N", gxTv_SdtProjeto_Projeto_anexosequecial_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Projeto_Codigo" )]
      [  XmlElement( ElementName = "Projeto_Codigo"   )]
      public int gxTpr_Projeto_codigo
      {
         get {
            return gxTv_SdtProjeto_Projeto_codigo ;
         }

         set {
            if ( gxTv_SdtProjeto_Projeto_codigo != value )
            {
               gxTv_SdtProjeto_Mode = "INS";
               this.gxTv_SdtProjeto_Projeto_codigo_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_nome_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_sigla_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_tipoprojetocod_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_tecnicacontagem_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_previsao_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_gerentecod_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_servicocod_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_custo_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_prazo_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_esforco_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_sistemacod_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_status_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_fatorescala_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_fatormultiplicador_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_constacocomo_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_incremental_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_identificador_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_dtinicio_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_dtfim_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_prazoprevisto_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_esforcoprevisto_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_custoprevisto_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_areatrabalhocodigo_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_areatrabalhodescricao_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_gpoobjctrlcodigo_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_gpoobjctrlnome_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_sistemacodigo_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_sistemanome_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_sistemasigla_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_modulocodigo_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_modulonome_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_modulosigla_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_anexosequecial_Z_SetNull( );
               if ( gxTv_SdtProjeto_Anexos != null )
               {
                  GxSilentTrnGridCollection collectionAnexos = gxTv_SdtProjeto_Anexos ;
                  SdtProjeto_Anexos currItemAnexos ;
                  short idx = 1 ;
                  while ( idx <= collectionAnexos.Count )
                  {
                     currItemAnexos = ((SdtProjeto_Anexos)collectionAnexos.Item(idx));
                     currItemAnexos.gxTpr_Mode = "INS";
                     currItemAnexos.gxTpr_Modified = 1;
                     idx = (short)(idx+1);
                  }
               }
            }
            gxTv_SdtProjeto_Projeto_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Projeto_Nome" )]
      [  XmlElement( ElementName = "Projeto_Nome"   )]
      public String gxTpr_Projeto_nome
      {
         get {
            return gxTv_SdtProjeto_Projeto_nome ;
         }

         set {
            gxTv_SdtProjeto_Projeto_nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Projeto_Sigla" )]
      [  XmlElement( ElementName = "Projeto_Sigla"   )]
      public String gxTpr_Projeto_sigla
      {
         get {
            return gxTv_SdtProjeto_Projeto_sigla ;
         }

         set {
            gxTv_SdtProjeto_Projeto_sigla = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Projeto_TipoProjetoCod" )]
      [  XmlElement( ElementName = "Projeto_TipoProjetoCod"   )]
      public int gxTpr_Projeto_tipoprojetocod
      {
         get {
            return gxTv_SdtProjeto_Projeto_tipoprojetocod ;
         }

         set {
            gxTv_SdtProjeto_Projeto_tipoprojetocod_N = 0;
            gxTv_SdtProjeto_Projeto_tipoprojetocod = (int)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_tipoprojetocod_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_tipoprojetocod_N = 1;
         gxTv_SdtProjeto_Projeto_tipoprojetocod = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_tipoprojetocod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_TecnicaContagem" )]
      [  XmlElement( ElementName = "Projeto_TecnicaContagem"   )]
      public String gxTpr_Projeto_tecnicacontagem
      {
         get {
            return gxTv_SdtProjeto_Projeto_tecnicacontagem ;
         }

         set {
            gxTv_SdtProjeto_Projeto_tecnicacontagem = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Projeto_Introducao" )]
      [  XmlElement( ElementName = "Projeto_Introducao"   )]
      public String gxTpr_Projeto_introducao
      {
         get {
            return gxTv_SdtProjeto_Projeto_introducao ;
         }

         set {
            gxTv_SdtProjeto_Projeto_introducao_N = 0;
            gxTv_SdtProjeto_Projeto_introducao = (String)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_introducao_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_introducao_N = 1;
         gxTv_SdtProjeto_Projeto_introducao = "";
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_introducao_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Escopo" )]
      [  XmlElement( ElementName = "Projeto_Escopo"   )]
      public String gxTpr_Projeto_escopo
      {
         get {
            return gxTv_SdtProjeto_Projeto_escopo ;
         }

         set {
            gxTv_SdtProjeto_Projeto_escopo = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Projeto_Previsao" )]
      [  XmlElement( ElementName = "Projeto_Previsao"  , IsNullable=true )]
      public string gxTpr_Projeto_previsao_Nullable
      {
         get {
            if ( gxTv_SdtProjeto_Projeto_previsao == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtProjeto_Projeto_previsao).value ;
         }

         set {
            gxTv_SdtProjeto_Projeto_previsao_N = 0;
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtProjeto_Projeto_previsao = DateTime.MinValue;
            else
               gxTv_SdtProjeto_Projeto_previsao = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Projeto_previsao
      {
         get {
            return gxTv_SdtProjeto_Projeto_previsao ;
         }

         set {
            gxTv_SdtProjeto_Projeto_previsao_N = 0;
            gxTv_SdtProjeto_Projeto_previsao = (DateTime)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_previsao_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_previsao_N = 1;
         gxTv_SdtProjeto_Projeto_previsao = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_previsao_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_GerenteCod" )]
      [  XmlElement( ElementName = "Projeto_GerenteCod"   )]
      public int gxTpr_Projeto_gerentecod
      {
         get {
            return gxTv_SdtProjeto_Projeto_gerentecod ;
         }

         set {
            gxTv_SdtProjeto_Projeto_gerentecod_N = 0;
            gxTv_SdtProjeto_Projeto_gerentecod = (int)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_gerentecod_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_gerentecod_N = 1;
         gxTv_SdtProjeto_Projeto_gerentecod = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_gerentecod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_ServicoCod" )]
      [  XmlElement( ElementName = "Projeto_ServicoCod"   )]
      public int gxTpr_Projeto_servicocod
      {
         get {
            return gxTv_SdtProjeto_Projeto_servicocod ;
         }

         set {
            gxTv_SdtProjeto_Projeto_servicocod_N = 0;
            gxTv_SdtProjeto_Projeto_servicocod = (int)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_servicocod_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_servicocod_N = 1;
         gxTv_SdtProjeto_Projeto_servicocod = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_servicocod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Custo" )]
      [  XmlElement( ElementName = "Projeto_Custo"   )]
      public double gxTpr_Projeto_custo_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtProjeto_Projeto_custo) ;
         }

         set {
            gxTv_SdtProjeto_Projeto_custo = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Projeto_custo
      {
         get {
            return gxTv_SdtProjeto_Projeto_custo ;
         }

         set {
            gxTv_SdtProjeto_Projeto_custo = (decimal)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_custo_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_custo = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_custo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Prazo" )]
      [  XmlElement( ElementName = "Projeto_Prazo"   )]
      public short gxTpr_Projeto_prazo
      {
         get {
            return gxTv_SdtProjeto_Projeto_prazo ;
         }

         set {
            gxTv_SdtProjeto_Projeto_prazo = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_prazo_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_prazo = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_prazo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Esforco" )]
      [  XmlElement( ElementName = "Projeto_Esforco"   )]
      public short gxTpr_Projeto_esforco
      {
         get {
            return gxTv_SdtProjeto_Projeto_esforco ;
         }

         set {
            gxTv_SdtProjeto_Projeto_esforco_N = 0;
            gxTv_SdtProjeto_Projeto_esforco = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_esforco_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_esforco_N = 1;
         gxTv_SdtProjeto_Projeto_esforco = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_esforco_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_SistemaCod" )]
      [  XmlElement( ElementName = "Projeto_SistemaCod"   )]
      public int gxTpr_Projeto_sistemacod
      {
         get {
            return gxTv_SdtProjeto_Projeto_sistemacod ;
         }

         set {
            gxTv_SdtProjeto_Projeto_sistemacod = (int)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_sistemacod_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_sistemacod = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_sistemacod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Status" )]
      [  XmlElement( ElementName = "Projeto_Status"   )]
      public String gxTpr_Projeto_status
      {
         get {
            return gxTv_SdtProjeto_Projeto_status ;
         }

         set {
            gxTv_SdtProjeto_Projeto_status = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Projeto_FatorEscala" )]
      [  XmlElement( ElementName = "Projeto_FatorEscala"   )]
      public double gxTpr_Projeto_fatorescala_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtProjeto_Projeto_fatorescala) ;
         }

         set {
            gxTv_SdtProjeto_Projeto_fatorescala_N = 0;
            gxTv_SdtProjeto_Projeto_fatorescala = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Projeto_fatorescala
      {
         get {
            return gxTv_SdtProjeto_Projeto_fatorescala ;
         }

         set {
            gxTv_SdtProjeto_Projeto_fatorescala_N = 0;
            gxTv_SdtProjeto_Projeto_fatorescala = (decimal)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_fatorescala_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_fatorescala_N = 1;
         gxTv_SdtProjeto_Projeto_fatorescala = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_fatorescala_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_FatorMultiplicador" )]
      [  XmlElement( ElementName = "Projeto_FatorMultiplicador"   )]
      public double gxTpr_Projeto_fatormultiplicador_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtProjeto_Projeto_fatormultiplicador) ;
         }

         set {
            gxTv_SdtProjeto_Projeto_fatormultiplicador_N = 0;
            gxTv_SdtProjeto_Projeto_fatormultiplicador = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Projeto_fatormultiplicador
      {
         get {
            return gxTv_SdtProjeto_Projeto_fatormultiplicador ;
         }

         set {
            gxTv_SdtProjeto_Projeto_fatormultiplicador_N = 0;
            gxTv_SdtProjeto_Projeto_fatormultiplicador = (decimal)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_fatormultiplicador_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_fatormultiplicador_N = 1;
         gxTv_SdtProjeto_Projeto_fatormultiplicador = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_fatormultiplicador_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_ConstACocomo" )]
      [  XmlElement( ElementName = "Projeto_ConstACocomo"   )]
      public double gxTpr_Projeto_constacocomo_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtProjeto_Projeto_constacocomo) ;
         }

         set {
            gxTv_SdtProjeto_Projeto_constacocomo_N = 0;
            gxTv_SdtProjeto_Projeto_constacocomo = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Projeto_constacocomo
      {
         get {
            return gxTv_SdtProjeto_Projeto_constacocomo ;
         }

         set {
            gxTv_SdtProjeto_Projeto_constacocomo_N = 0;
            gxTv_SdtProjeto_Projeto_constacocomo = (decimal)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_constacocomo_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_constacocomo_N = 1;
         gxTv_SdtProjeto_Projeto_constacocomo = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_constacocomo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Incremental" )]
      [  XmlElement( ElementName = "Projeto_Incremental"   )]
      public bool gxTpr_Projeto_incremental
      {
         get {
            return gxTv_SdtProjeto_Projeto_incremental ;
         }

         set {
            gxTv_SdtProjeto_Projeto_incremental_N = 0;
            gxTv_SdtProjeto_Projeto_incremental = value;
         }

      }

      public void gxTv_SdtProjeto_Projeto_incremental_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_incremental_N = 1;
         gxTv_SdtProjeto_Projeto_incremental = false;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_incremental_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Identificador" )]
      [  XmlElement( ElementName = "Projeto_Identificador"   )]
      public String gxTpr_Projeto_identificador
      {
         get {
            return gxTv_SdtProjeto_Projeto_identificador ;
         }

         set {
            gxTv_SdtProjeto_Projeto_identificador_N = 0;
            gxTv_SdtProjeto_Projeto_identificador = (String)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_identificador_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_identificador_N = 1;
         gxTv_SdtProjeto_Projeto_identificador = "";
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_identificador_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Objetivo" )]
      [  XmlElement( ElementName = "Projeto_Objetivo"   )]
      public String gxTpr_Projeto_objetivo
      {
         get {
            return gxTv_SdtProjeto_Projeto_objetivo ;
         }

         set {
            gxTv_SdtProjeto_Projeto_objetivo_N = 0;
            gxTv_SdtProjeto_Projeto_objetivo = (String)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_objetivo_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_objetivo_N = 1;
         gxTv_SdtProjeto_Projeto_objetivo = "";
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_objetivo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_DTInicio" )]
      [  XmlElement( ElementName = "Projeto_DTInicio"  , IsNullable=true )]
      public string gxTpr_Projeto_dtinicio_Nullable
      {
         get {
            if ( gxTv_SdtProjeto_Projeto_dtinicio == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtProjeto_Projeto_dtinicio).value ;
         }

         set {
            gxTv_SdtProjeto_Projeto_dtinicio_N = 0;
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtProjeto_Projeto_dtinicio = DateTime.MinValue;
            else
               gxTv_SdtProjeto_Projeto_dtinicio = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Projeto_dtinicio
      {
         get {
            return gxTv_SdtProjeto_Projeto_dtinicio ;
         }

         set {
            gxTv_SdtProjeto_Projeto_dtinicio_N = 0;
            gxTv_SdtProjeto_Projeto_dtinicio = (DateTime)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_dtinicio_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_dtinicio_N = 1;
         gxTv_SdtProjeto_Projeto_dtinicio = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_dtinicio_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_DTFim" )]
      [  XmlElement( ElementName = "Projeto_DTFim"  , IsNullable=true )]
      public string gxTpr_Projeto_dtfim_Nullable
      {
         get {
            if ( gxTv_SdtProjeto_Projeto_dtfim == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtProjeto_Projeto_dtfim).value ;
         }

         set {
            gxTv_SdtProjeto_Projeto_dtfim_N = 0;
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtProjeto_Projeto_dtfim = DateTime.MinValue;
            else
               gxTv_SdtProjeto_Projeto_dtfim = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Projeto_dtfim
      {
         get {
            return gxTv_SdtProjeto_Projeto_dtfim ;
         }

         set {
            gxTv_SdtProjeto_Projeto_dtfim_N = 0;
            gxTv_SdtProjeto_Projeto_dtfim = (DateTime)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_dtfim_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_dtfim_N = 1;
         gxTv_SdtProjeto_Projeto_dtfim = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_dtfim_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_PrazoPrevisto" )]
      [  XmlElement( ElementName = "Projeto_PrazoPrevisto"   )]
      public short gxTpr_Projeto_prazoprevisto
      {
         get {
            return gxTv_SdtProjeto_Projeto_prazoprevisto ;
         }

         set {
            gxTv_SdtProjeto_Projeto_prazoprevisto_N = 0;
            gxTv_SdtProjeto_Projeto_prazoprevisto = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_prazoprevisto_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_prazoprevisto_N = 1;
         gxTv_SdtProjeto_Projeto_prazoprevisto = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_prazoprevisto_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_EsforcoPrevisto" )]
      [  XmlElement( ElementName = "Projeto_EsforcoPrevisto"   )]
      public short gxTpr_Projeto_esforcoprevisto
      {
         get {
            return gxTv_SdtProjeto_Projeto_esforcoprevisto ;
         }

         set {
            gxTv_SdtProjeto_Projeto_esforcoprevisto_N = 0;
            gxTv_SdtProjeto_Projeto_esforcoprevisto = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_esforcoprevisto_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_esforcoprevisto_N = 1;
         gxTv_SdtProjeto_Projeto_esforcoprevisto = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_esforcoprevisto_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_CustoPrevisto" )]
      [  XmlElement( ElementName = "Projeto_CustoPrevisto"   )]
      public double gxTpr_Projeto_custoprevisto_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtProjeto_Projeto_custoprevisto) ;
         }

         set {
            gxTv_SdtProjeto_Projeto_custoprevisto_N = 0;
            gxTv_SdtProjeto_Projeto_custoprevisto = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Projeto_custoprevisto
      {
         get {
            return gxTv_SdtProjeto_Projeto_custoprevisto ;
         }

         set {
            gxTv_SdtProjeto_Projeto_custoprevisto_N = 0;
            gxTv_SdtProjeto_Projeto_custoprevisto = (decimal)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_custoprevisto_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_custoprevisto_N = 1;
         gxTv_SdtProjeto_Projeto_custoprevisto = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_custoprevisto_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_AreaTrabalhoCodigo" )]
      [  XmlElement( ElementName = "Projeto_AreaTrabalhoCodigo"   )]
      public int gxTpr_Projeto_areatrabalhocodigo
      {
         get {
            return gxTv_SdtProjeto_Projeto_areatrabalhocodigo ;
         }

         set {
            gxTv_SdtProjeto_Projeto_areatrabalhocodigo_N = 0;
            gxTv_SdtProjeto_Projeto_areatrabalhocodigo = (int)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_areatrabalhocodigo_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_areatrabalhocodigo_N = 1;
         gxTv_SdtProjeto_Projeto_areatrabalhocodigo = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_areatrabalhocodigo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_AreaTrabalhoDescricao" )]
      [  XmlElement( ElementName = "Projeto_AreaTrabalhoDescricao"   )]
      public String gxTpr_Projeto_areatrabalhodescricao
      {
         get {
            return gxTv_SdtProjeto_Projeto_areatrabalhodescricao ;
         }

         set {
            gxTv_SdtProjeto_Projeto_areatrabalhodescricao_N = 0;
            gxTv_SdtProjeto_Projeto_areatrabalhodescricao = (String)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_areatrabalhodescricao_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_areatrabalhodescricao_N = 1;
         gxTv_SdtProjeto_Projeto_areatrabalhodescricao = "";
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_areatrabalhodescricao_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_GpoObjCtrlCodigo" )]
      [  XmlElement( ElementName = "Projeto_GpoObjCtrlCodigo"   )]
      public int gxTpr_Projeto_gpoobjctrlcodigo
      {
         get {
            return gxTv_SdtProjeto_Projeto_gpoobjctrlcodigo ;
         }

         set {
            gxTv_SdtProjeto_Projeto_gpoobjctrlcodigo_N = 0;
            gxTv_SdtProjeto_Projeto_gpoobjctrlcodigo = (int)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_gpoobjctrlcodigo_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_gpoobjctrlcodigo_N = 1;
         gxTv_SdtProjeto_Projeto_gpoobjctrlcodigo = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_gpoobjctrlcodigo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_GpoObjCtrlNome" )]
      [  XmlElement( ElementName = "Projeto_GpoObjCtrlNome"   )]
      public String gxTpr_Projeto_gpoobjctrlnome
      {
         get {
            return gxTv_SdtProjeto_Projeto_gpoobjctrlnome ;
         }

         set {
            gxTv_SdtProjeto_Projeto_gpoobjctrlnome_N = 0;
            gxTv_SdtProjeto_Projeto_gpoobjctrlnome = (String)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_gpoobjctrlnome_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_gpoobjctrlnome_N = 1;
         gxTv_SdtProjeto_Projeto_gpoobjctrlnome = "";
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_gpoobjctrlnome_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_SistemaCodigo" )]
      [  XmlElement( ElementName = "Projeto_SistemaCodigo"   )]
      public int gxTpr_Projeto_sistemacodigo
      {
         get {
            return gxTv_SdtProjeto_Projeto_sistemacodigo ;
         }

         set {
            gxTv_SdtProjeto_Projeto_sistemacodigo_N = 0;
            gxTv_SdtProjeto_Projeto_sistemacodigo = (int)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_sistemacodigo_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_sistemacodigo_N = 1;
         gxTv_SdtProjeto_Projeto_sistemacodigo = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_sistemacodigo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_SistemaNome" )]
      [  XmlElement( ElementName = "Projeto_SistemaNome"   )]
      public String gxTpr_Projeto_sistemanome
      {
         get {
            return gxTv_SdtProjeto_Projeto_sistemanome ;
         }

         set {
            gxTv_SdtProjeto_Projeto_sistemanome_N = 0;
            gxTv_SdtProjeto_Projeto_sistemanome = (String)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_sistemanome_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_sistemanome_N = 1;
         gxTv_SdtProjeto_Projeto_sistemanome = "";
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_sistemanome_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_SistemaSigla" )]
      [  XmlElement( ElementName = "Projeto_SistemaSigla"   )]
      public String gxTpr_Projeto_sistemasigla
      {
         get {
            return gxTv_SdtProjeto_Projeto_sistemasigla ;
         }

         set {
            gxTv_SdtProjeto_Projeto_sistemasigla_N = 0;
            gxTv_SdtProjeto_Projeto_sistemasigla = (String)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_sistemasigla_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_sistemasigla_N = 1;
         gxTv_SdtProjeto_Projeto_sistemasigla = "";
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_sistemasigla_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_ModuloCodigo" )]
      [  XmlElement( ElementName = "Projeto_ModuloCodigo"   )]
      public int gxTpr_Projeto_modulocodigo
      {
         get {
            return gxTv_SdtProjeto_Projeto_modulocodigo ;
         }

         set {
            gxTv_SdtProjeto_Projeto_modulocodigo_N = 0;
            gxTv_SdtProjeto_Projeto_modulocodigo = (int)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_modulocodigo_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_modulocodigo_N = 1;
         gxTv_SdtProjeto_Projeto_modulocodigo = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_modulocodigo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_ModuloNome" )]
      [  XmlElement( ElementName = "Projeto_ModuloNome"   )]
      public String gxTpr_Projeto_modulonome
      {
         get {
            return gxTv_SdtProjeto_Projeto_modulonome ;
         }

         set {
            gxTv_SdtProjeto_Projeto_modulonome_N = 0;
            gxTv_SdtProjeto_Projeto_modulonome = (String)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_modulonome_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_modulonome_N = 1;
         gxTv_SdtProjeto_Projeto_modulonome = "";
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_modulonome_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_ModuloSigla" )]
      [  XmlElement( ElementName = "Projeto_ModuloSigla"   )]
      public String gxTpr_Projeto_modulosigla
      {
         get {
            return gxTv_SdtProjeto_Projeto_modulosigla ;
         }

         set {
            gxTv_SdtProjeto_Projeto_modulosigla_N = 0;
            gxTv_SdtProjeto_Projeto_modulosigla = (String)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_modulosigla_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_modulosigla_N = 1;
         gxTv_SdtProjeto_Projeto_modulosigla = "";
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_modulosigla_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_AnexoSequecial" )]
      [  XmlElement( ElementName = "Projeto_AnexoSequecial"   )]
      public short gxTpr_Projeto_anexosequecial
      {
         get {
            return gxTv_SdtProjeto_Projeto_anexosequecial ;
         }

         set {
            gxTv_SdtProjeto_Projeto_anexosequecial_N = 0;
            gxTv_SdtProjeto_Projeto_anexosequecial = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_anexosequecial_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_anexosequecial_N = 1;
         gxTv_SdtProjeto_Projeto_anexosequecial = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_anexosequecial_IsNull( )
      {
         return false ;
      }

      public class gxTv_SdtProjeto_Anexos_SdtProjeto_Anexos_80compatibility:SdtProjeto_Anexos {}
      [  SoapElement( ElementName = "Anexos" )]
      [  XmlArray( ElementName = "Anexos"  )]
      [  XmlArrayItemAttribute( Type= typeof( SdtProjeto_Anexos ), ElementName= "Projeto.Anexos"  , IsNullable=false)]
      public GxSilentTrnGridCollection gxTpr_Anexos_GxSilentTrnGridCollection
      {
         get {
            if ( gxTv_SdtProjeto_Anexos == null )
            {
               gxTv_SdtProjeto_Anexos = new GxSilentTrnGridCollection( context, "Projeto.Anexos", "GxEv3Up14_MeetrikaVs3", "SdtProjeto_Anexos", "GeneXus.Programs");
            }
            return (GxSilentTrnGridCollection)gxTv_SdtProjeto_Anexos ;
         }

         set {
            if ( gxTv_SdtProjeto_Anexos == null )
            {
               gxTv_SdtProjeto_Anexos = new GxSilentTrnGridCollection( context, "Projeto.Anexos", "GxEv3Up14_MeetrikaVs3", "SdtProjeto_Anexos", "GeneXus.Programs");
            }
            gxTv_SdtProjeto_Anexos = (GxSilentTrnGridCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public GxSilentTrnGridCollection gxTpr_Anexos
      {
         get {
            if ( gxTv_SdtProjeto_Anexos == null )
            {
               gxTv_SdtProjeto_Anexos = new GxSilentTrnGridCollection( context, "Projeto.Anexos", "GxEv3Up14_MeetrikaVs3", "SdtProjeto_Anexos", "GeneXus.Programs");
            }
            return gxTv_SdtProjeto_Anexos ;
         }

         set {
            gxTv_SdtProjeto_Anexos = value;
         }

      }

      public void gxTv_SdtProjeto_Anexos_SetNull( )
      {
         gxTv_SdtProjeto_Anexos = null;
         return  ;
      }

      public bool gxTv_SdtProjeto_Anexos_IsNull( )
      {
         if ( gxTv_SdtProjeto_Anexos == null )
         {
            return true ;
         }
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtProjeto_Mode ;
         }

         set {
            gxTv_SdtProjeto_Mode = (String)(value);
         }

      }

      public void gxTv_SdtProjeto_Mode_SetNull( )
      {
         gxTv_SdtProjeto_Mode = "";
         return  ;
      }

      public bool gxTv_SdtProjeto_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtProjeto_Initialized ;
         }

         set {
            gxTv_SdtProjeto_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Initialized_SetNull( )
      {
         gxTv_SdtProjeto_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Codigo_Z" )]
      [  XmlElement( ElementName = "Projeto_Codigo_Z"   )]
      public int gxTpr_Projeto_codigo_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_codigo_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_codigo_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Nome_Z" )]
      [  XmlElement( ElementName = "Projeto_Nome_Z"   )]
      public String gxTpr_Projeto_nome_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_nome_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_nome_Z = (String)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_nome_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_nome_Z = "";
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_nome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Sigla_Z" )]
      [  XmlElement( ElementName = "Projeto_Sigla_Z"   )]
      public String gxTpr_Projeto_sigla_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_sigla_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_sigla_Z = (String)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_sigla_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_sigla_Z = "";
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_sigla_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_TipoProjetoCod_Z" )]
      [  XmlElement( ElementName = "Projeto_TipoProjetoCod_Z"   )]
      public int gxTpr_Projeto_tipoprojetocod_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_tipoprojetocod_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_tipoprojetocod_Z = (int)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_tipoprojetocod_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_tipoprojetocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_tipoprojetocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_TecnicaContagem_Z" )]
      [  XmlElement( ElementName = "Projeto_TecnicaContagem_Z"   )]
      public String gxTpr_Projeto_tecnicacontagem_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_tecnicacontagem_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_tecnicacontagem_Z = (String)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_tecnicacontagem_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_tecnicacontagem_Z = "";
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_tecnicacontagem_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Previsao_Z" )]
      [  XmlElement( ElementName = "Projeto_Previsao_Z"  , IsNullable=true )]
      public string gxTpr_Projeto_previsao_Z_Nullable
      {
         get {
            if ( gxTv_SdtProjeto_Projeto_previsao_Z == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtProjeto_Projeto_previsao_Z).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtProjeto_Projeto_previsao_Z = DateTime.MinValue;
            else
               gxTv_SdtProjeto_Projeto_previsao_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Projeto_previsao_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_previsao_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_previsao_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_previsao_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_previsao_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_previsao_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_GerenteCod_Z" )]
      [  XmlElement( ElementName = "Projeto_GerenteCod_Z"   )]
      public int gxTpr_Projeto_gerentecod_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_gerentecod_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_gerentecod_Z = (int)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_gerentecod_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_gerentecod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_gerentecod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_ServicoCod_Z" )]
      [  XmlElement( ElementName = "Projeto_ServicoCod_Z"   )]
      public int gxTpr_Projeto_servicocod_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_servicocod_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_servicocod_Z = (int)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_servicocod_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_servicocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_servicocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Custo_Z" )]
      [  XmlElement( ElementName = "Projeto_Custo_Z"   )]
      public double gxTpr_Projeto_custo_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtProjeto_Projeto_custo_Z) ;
         }

         set {
            gxTv_SdtProjeto_Projeto_custo_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Projeto_custo_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_custo_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_custo_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_custo_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_custo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_custo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Prazo_Z" )]
      [  XmlElement( ElementName = "Projeto_Prazo_Z"   )]
      public short gxTpr_Projeto_prazo_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_prazo_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_prazo_Z = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_prazo_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_prazo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_prazo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Esforco_Z" )]
      [  XmlElement( ElementName = "Projeto_Esforco_Z"   )]
      public short gxTpr_Projeto_esforco_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_esforco_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_esforco_Z = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_esforco_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_esforco_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_esforco_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_SistemaCod_Z" )]
      [  XmlElement( ElementName = "Projeto_SistemaCod_Z"   )]
      public int gxTpr_Projeto_sistemacod_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_sistemacod_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_sistemacod_Z = (int)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_sistemacod_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_sistemacod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_sistemacod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Status_Z" )]
      [  XmlElement( ElementName = "Projeto_Status_Z"   )]
      public String gxTpr_Projeto_status_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_status_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_status_Z = (String)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_status_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_status_Z = "";
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_status_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_FatorEscala_Z" )]
      [  XmlElement( ElementName = "Projeto_FatorEscala_Z"   )]
      public double gxTpr_Projeto_fatorescala_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtProjeto_Projeto_fatorescala_Z) ;
         }

         set {
            gxTv_SdtProjeto_Projeto_fatorescala_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Projeto_fatorescala_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_fatorescala_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_fatorescala_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_fatorescala_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_fatorescala_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_fatorescala_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_FatorMultiplicador_Z" )]
      [  XmlElement( ElementName = "Projeto_FatorMultiplicador_Z"   )]
      public double gxTpr_Projeto_fatormultiplicador_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtProjeto_Projeto_fatormultiplicador_Z) ;
         }

         set {
            gxTv_SdtProjeto_Projeto_fatormultiplicador_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Projeto_fatormultiplicador_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_fatormultiplicador_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_fatormultiplicador_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_fatormultiplicador_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_fatormultiplicador_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_fatormultiplicador_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_ConstACocomo_Z" )]
      [  XmlElement( ElementName = "Projeto_ConstACocomo_Z"   )]
      public double gxTpr_Projeto_constacocomo_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtProjeto_Projeto_constacocomo_Z) ;
         }

         set {
            gxTv_SdtProjeto_Projeto_constacocomo_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Projeto_constacocomo_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_constacocomo_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_constacocomo_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_constacocomo_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_constacocomo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_constacocomo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Incremental_Z" )]
      [  XmlElement( ElementName = "Projeto_Incremental_Z"   )]
      public bool gxTpr_Projeto_incremental_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_incremental_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_incremental_Z = value;
         }

      }

      public void gxTv_SdtProjeto_Projeto_incremental_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_incremental_Z = false;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_incremental_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Identificador_Z" )]
      [  XmlElement( ElementName = "Projeto_Identificador_Z"   )]
      public String gxTpr_Projeto_identificador_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_identificador_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_identificador_Z = (String)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_identificador_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_identificador_Z = "";
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_identificador_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_DTInicio_Z" )]
      [  XmlElement( ElementName = "Projeto_DTInicio_Z"  , IsNullable=true )]
      public string gxTpr_Projeto_dtinicio_Z_Nullable
      {
         get {
            if ( gxTv_SdtProjeto_Projeto_dtinicio_Z == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtProjeto_Projeto_dtinicio_Z).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtProjeto_Projeto_dtinicio_Z = DateTime.MinValue;
            else
               gxTv_SdtProjeto_Projeto_dtinicio_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Projeto_dtinicio_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_dtinicio_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_dtinicio_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_dtinicio_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_dtinicio_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_dtinicio_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_DTFim_Z" )]
      [  XmlElement( ElementName = "Projeto_DTFim_Z"  , IsNullable=true )]
      public string gxTpr_Projeto_dtfim_Z_Nullable
      {
         get {
            if ( gxTv_SdtProjeto_Projeto_dtfim_Z == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtProjeto_Projeto_dtfim_Z).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtProjeto_Projeto_dtfim_Z = DateTime.MinValue;
            else
               gxTv_SdtProjeto_Projeto_dtfim_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Projeto_dtfim_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_dtfim_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_dtfim_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_dtfim_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_dtfim_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_dtfim_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_PrazoPrevisto_Z" )]
      [  XmlElement( ElementName = "Projeto_PrazoPrevisto_Z"   )]
      public short gxTpr_Projeto_prazoprevisto_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_prazoprevisto_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_prazoprevisto_Z = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_prazoprevisto_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_prazoprevisto_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_prazoprevisto_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_EsforcoPrevisto_Z" )]
      [  XmlElement( ElementName = "Projeto_EsforcoPrevisto_Z"   )]
      public short gxTpr_Projeto_esforcoprevisto_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_esforcoprevisto_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_esforcoprevisto_Z = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_esforcoprevisto_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_esforcoprevisto_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_esforcoprevisto_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_CustoPrevisto_Z" )]
      [  XmlElement( ElementName = "Projeto_CustoPrevisto_Z"   )]
      public double gxTpr_Projeto_custoprevisto_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtProjeto_Projeto_custoprevisto_Z) ;
         }

         set {
            gxTv_SdtProjeto_Projeto_custoprevisto_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Projeto_custoprevisto_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_custoprevisto_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_custoprevisto_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_custoprevisto_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_custoprevisto_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_custoprevisto_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_AreaTrabalhoCodigo_Z" )]
      [  XmlElement( ElementName = "Projeto_AreaTrabalhoCodigo_Z"   )]
      public int gxTpr_Projeto_areatrabalhocodigo_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_areatrabalhocodigo_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_areatrabalhocodigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_areatrabalhocodigo_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_areatrabalhocodigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_areatrabalhocodigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_AreaTrabalhoDescricao_Z" )]
      [  XmlElement( ElementName = "Projeto_AreaTrabalhoDescricao_Z"   )]
      public String gxTpr_Projeto_areatrabalhodescricao_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_areatrabalhodescricao_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_areatrabalhodescricao_Z = (String)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_areatrabalhodescricao_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_areatrabalhodescricao_Z = "";
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_areatrabalhodescricao_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_GpoObjCtrlCodigo_Z" )]
      [  XmlElement( ElementName = "Projeto_GpoObjCtrlCodigo_Z"   )]
      public int gxTpr_Projeto_gpoobjctrlcodigo_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_gpoobjctrlcodigo_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_gpoobjctrlcodigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_gpoobjctrlcodigo_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_gpoobjctrlcodigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_gpoobjctrlcodigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_GpoObjCtrlNome_Z" )]
      [  XmlElement( ElementName = "Projeto_GpoObjCtrlNome_Z"   )]
      public String gxTpr_Projeto_gpoobjctrlnome_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_gpoobjctrlnome_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_gpoobjctrlnome_Z = (String)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_gpoobjctrlnome_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_gpoobjctrlnome_Z = "";
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_gpoobjctrlnome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_SistemaCodigo_Z" )]
      [  XmlElement( ElementName = "Projeto_SistemaCodigo_Z"   )]
      public int gxTpr_Projeto_sistemacodigo_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_sistemacodigo_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_sistemacodigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_sistemacodigo_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_sistemacodigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_sistemacodigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_SistemaNome_Z" )]
      [  XmlElement( ElementName = "Projeto_SistemaNome_Z"   )]
      public String gxTpr_Projeto_sistemanome_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_sistemanome_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_sistemanome_Z = (String)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_sistemanome_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_sistemanome_Z = "";
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_sistemanome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_SistemaSigla_Z" )]
      [  XmlElement( ElementName = "Projeto_SistemaSigla_Z"   )]
      public String gxTpr_Projeto_sistemasigla_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_sistemasigla_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_sistemasigla_Z = (String)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_sistemasigla_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_sistemasigla_Z = "";
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_sistemasigla_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_ModuloCodigo_Z" )]
      [  XmlElement( ElementName = "Projeto_ModuloCodigo_Z"   )]
      public int gxTpr_Projeto_modulocodigo_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_modulocodigo_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_modulocodigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_modulocodigo_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_modulocodigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_modulocodigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_ModuloNome_Z" )]
      [  XmlElement( ElementName = "Projeto_ModuloNome_Z"   )]
      public String gxTpr_Projeto_modulonome_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_modulonome_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_modulonome_Z = (String)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_modulonome_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_modulonome_Z = "";
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_modulonome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_ModuloSigla_Z" )]
      [  XmlElement( ElementName = "Projeto_ModuloSigla_Z"   )]
      public String gxTpr_Projeto_modulosigla_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_modulosigla_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_modulosigla_Z = (String)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_modulosigla_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_modulosigla_Z = "";
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_modulosigla_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_AnexoSequecial_Z" )]
      [  XmlElement( ElementName = "Projeto_AnexoSequecial_Z"   )]
      public short gxTpr_Projeto_anexosequecial_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_anexosequecial_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_anexosequecial_Z = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_anexosequecial_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_anexosequecial_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_anexosequecial_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_TipoProjetoCod_N" )]
      [  XmlElement( ElementName = "Projeto_TipoProjetoCod_N"   )]
      public short gxTpr_Projeto_tipoprojetocod_N
      {
         get {
            return gxTv_SdtProjeto_Projeto_tipoprojetocod_N ;
         }

         set {
            gxTv_SdtProjeto_Projeto_tipoprojetocod_N = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_tipoprojetocod_N_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_tipoprojetocod_N = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_tipoprojetocod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Introducao_N" )]
      [  XmlElement( ElementName = "Projeto_Introducao_N"   )]
      public short gxTpr_Projeto_introducao_N
      {
         get {
            return gxTv_SdtProjeto_Projeto_introducao_N ;
         }

         set {
            gxTv_SdtProjeto_Projeto_introducao_N = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_introducao_N_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_introducao_N = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_introducao_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Previsao_N" )]
      [  XmlElement( ElementName = "Projeto_Previsao_N"   )]
      public short gxTpr_Projeto_previsao_N
      {
         get {
            return gxTv_SdtProjeto_Projeto_previsao_N ;
         }

         set {
            gxTv_SdtProjeto_Projeto_previsao_N = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_previsao_N_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_previsao_N = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_previsao_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_GerenteCod_N" )]
      [  XmlElement( ElementName = "Projeto_GerenteCod_N"   )]
      public short gxTpr_Projeto_gerentecod_N
      {
         get {
            return gxTv_SdtProjeto_Projeto_gerentecod_N ;
         }

         set {
            gxTv_SdtProjeto_Projeto_gerentecod_N = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_gerentecod_N_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_gerentecod_N = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_gerentecod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_ServicoCod_N" )]
      [  XmlElement( ElementName = "Projeto_ServicoCod_N"   )]
      public short gxTpr_Projeto_servicocod_N
      {
         get {
            return gxTv_SdtProjeto_Projeto_servicocod_N ;
         }

         set {
            gxTv_SdtProjeto_Projeto_servicocod_N = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_servicocod_N_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_servicocod_N = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_servicocod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Esforco_N" )]
      [  XmlElement( ElementName = "Projeto_Esforco_N"   )]
      public short gxTpr_Projeto_esforco_N
      {
         get {
            return gxTv_SdtProjeto_Projeto_esforco_N ;
         }

         set {
            gxTv_SdtProjeto_Projeto_esforco_N = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_esforco_N_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_esforco_N = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_esforco_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_FatorEscala_N" )]
      [  XmlElement( ElementName = "Projeto_FatorEscala_N"   )]
      public short gxTpr_Projeto_fatorescala_N
      {
         get {
            return gxTv_SdtProjeto_Projeto_fatorescala_N ;
         }

         set {
            gxTv_SdtProjeto_Projeto_fatorescala_N = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_fatorescala_N_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_fatorescala_N = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_fatorescala_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_FatorMultiplicador_N" )]
      [  XmlElement( ElementName = "Projeto_FatorMultiplicador_N"   )]
      public short gxTpr_Projeto_fatormultiplicador_N
      {
         get {
            return gxTv_SdtProjeto_Projeto_fatormultiplicador_N ;
         }

         set {
            gxTv_SdtProjeto_Projeto_fatormultiplicador_N = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_fatormultiplicador_N_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_fatormultiplicador_N = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_fatormultiplicador_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_ConstACocomo_N" )]
      [  XmlElement( ElementName = "Projeto_ConstACocomo_N"   )]
      public short gxTpr_Projeto_constacocomo_N
      {
         get {
            return gxTv_SdtProjeto_Projeto_constacocomo_N ;
         }

         set {
            gxTv_SdtProjeto_Projeto_constacocomo_N = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_constacocomo_N_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_constacocomo_N = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_constacocomo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Incremental_N" )]
      [  XmlElement( ElementName = "Projeto_Incremental_N"   )]
      public short gxTpr_Projeto_incremental_N
      {
         get {
            return gxTv_SdtProjeto_Projeto_incremental_N ;
         }

         set {
            gxTv_SdtProjeto_Projeto_incremental_N = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_incremental_N_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_incremental_N = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_incremental_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Identificador_N" )]
      [  XmlElement( ElementName = "Projeto_Identificador_N"   )]
      public short gxTpr_Projeto_identificador_N
      {
         get {
            return gxTv_SdtProjeto_Projeto_identificador_N ;
         }

         set {
            gxTv_SdtProjeto_Projeto_identificador_N = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_identificador_N_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_identificador_N = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_identificador_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Objetivo_N" )]
      [  XmlElement( ElementName = "Projeto_Objetivo_N"   )]
      public short gxTpr_Projeto_objetivo_N
      {
         get {
            return gxTv_SdtProjeto_Projeto_objetivo_N ;
         }

         set {
            gxTv_SdtProjeto_Projeto_objetivo_N = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_objetivo_N_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_objetivo_N = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_objetivo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_DTInicio_N" )]
      [  XmlElement( ElementName = "Projeto_DTInicio_N"   )]
      public short gxTpr_Projeto_dtinicio_N
      {
         get {
            return gxTv_SdtProjeto_Projeto_dtinicio_N ;
         }

         set {
            gxTv_SdtProjeto_Projeto_dtinicio_N = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_dtinicio_N_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_dtinicio_N = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_dtinicio_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_DTFim_N" )]
      [  XmlElement( ElementName = "Projeto_DTFim_N"   )]
      public short gxTpr_Projeto_dtfim_N
      {
         get {
            return gxTv_SdtProjeto_Projeto_dtfim_N ;
         }

         set {
            gxTv_SdtProjeto_Projeto_dtfim_N = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_dtfim_N_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_dtfim_N = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_dtfim_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_PrazoPrevisto_N" )]
      [  XmlElement( ElementName = "Projeto_PrazoPrevisto_N"   )]
      public short gxTpr_Projeto_prazoprevisto_N
      {
         get {
            return gxTv_SdtProjeto_Projeto_prazoprevisto_N ;
         }

         set {
            gxTv_SdtProjeto_Projeto_prazoprevisto_N = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_prazoprevisto_N_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_prazoprevisto_N = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_prazoprevisto_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_EsforcoPrevisto_N" )]
      [  XmlElement( ElementName = "Projeto_EsforcoPrevisto_N"   )]
      public short gxTpr_Projeto_esforcoprevisto_N
      {
         get {
            return gxTv_SdtProjeto_Projeto_esforcoprevisto_N ;
         }

         set {
            gxTv_SdtProjeto_Projeto_esforcoprevisto_N = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_esforcoprevisto_N_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_esforcoprevisto_N = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_esforcoprevisto_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_CustoPrevisto_N" )]
      [  XmlElement( ElementName = "Projeto_CustoPrevisto_N"   )]
      public short gxTpr_Projeto_custoprevisto_N
      {
         get {
            return gxTv_SdtProjeto_Projeto_custoprevisto_N ;
         }

         set {
            gxTv_SdtProjeto_Projeto_custoprevisto_N = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_custoprevisto_N_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_custoprevisto_N = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_custoprevisto_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_AreaTrabalhoCodigo_N" )]
      [  XmlElement( ElementName = "Projeto_AreaTrabalhoCodigo_N"   )]
      public short gxTpr_Projeto_areatrabalhocodigo_N
      {
         get {
            return gxTv_SdtProjeto_Projeto_areatrabalhocodigo_N ;
         }

         set {
            gxTv_SdtProjeto_Projeto_areatrabalhocodigo_N = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_areatrabalhocodigo_N_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_areatrabalhocodigo_N = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_areatrabalhocodigo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_AreaTrabalhoDescricao_N" )]
      [  XmlElement( ElementName = "Projeto_AreaTrabalhoDescricao_N"   )]
      public short gxTpr_Projeto_areatrabalhodescricao_N
      {
         get {
            return gxTv_SdtProjeto_Projeto_areatrabalhodescricao_N ;
         }

         set {
            gxTv_SdtProjeto_Projeto_areatrabalhodescricao_N = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_areatrabalhodescricao_N_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_areatrabalhodescricao_N = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_areatrabalhodescricao_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_GpoObjCtrlCodigo_N" )]
      [  XmlElement( ElementName = "Projeto_GpoObjCtrlCodigo_N"   )]
      public short gxTpr_Projeto_gpoobjctrlcodigo_N
      {
         get {
            return gxTv_SdtProjeto_Projeto_gpoobjctrlcodigo_N ;
         }

         set {
            gxTv_SdtProjeto_Projeto_gpoobjctrlcodigo_N = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_gpoobjctrlcodigo_N_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_gpoobjctrlcodigo_N = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_gpoobjctrlcodigo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_GpoObjCtrlNome_N" )]
      [  XmlElement( ElementName = "Projeto_GpoObjCtrlNome_N"   )]
      public short gxTpr_Projeto_gpoobjctrlnome_N
      {
         get {
            return gxTv_SdtProjeto_Projeto_gpoobjctrlnome_N ;
         }

         set {
            gxTv_SdtProjeto_Projeto_gpoobjctrlnome_N = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_gpoobjctrlnome_N_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_gpoobjctrlnome_N = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_gpoobjctrlnome_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_SistemaCodigo_N" )]
      [  XmlElement( ElementName = "Projeto_SistemaCodigo_N"   )]
      public short gxTpr_Projeto_sistemacodigo_N
      {
         get {
            return gxTv_SdtProjeto_Projeto_sistemacodigo_N ;
         }

         set {
            gxTv_SdtProjeto_Projeto_sistemacodigo_N = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_sistemacodigo_N_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_sistemacodigo_N = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_sistemacodigo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_SistemaNome_N" )]
      [  XmlElement( ElementName = "Projeto_SistemaNome_N"   )]
      public short gxTpr_Projeto_sistemanome_N
      {
         get {
            return gxTv_SdtProjeto_Projeto_sistemanome_N ;
         }

         set {
            gxTv_SdtProjeto_Projeto_sistemanome_N = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_sistemanome_N_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_sistemanome_N = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_sistemanome_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_SistemaSigla_N" )]
      [  XmlElement( ElementName = "Projeto_SistemaSigla_N"   )]
      public short gxTpr_Projeto_sistemasigla_N
      {
         get {
            return gxTv_SdtProjeto_Projeto_sistemasigla_N ;
         }

         set {
            gxTv_SdtProjeto_Projeto_sistemasigla_N = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_sistemasigla_N_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_sistemasigla_N = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_sistemasigla_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_ModuloCodigo_N" )]
      [  XmlElement( ElementName = "Projeto_ModuloCodigo_N"   )]
      public short gxTpr_Projeto_modulocodigo_N
      {
         get {
            return gxTv_SdtProjeto_Projeto_modulocodigo_N ;
         }

         set {
            gxTv_SdtProjeto_Projeto_modulocodigo_N = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_modulocodigo_N_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_modulocodigo_N = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_modulocodigo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_ModuloNome_N" )]
      [  XmlElement( ElementName = "Projeto_ModuloNome_N"   )]
      public short gxTpr_Projeto_modulonome_N
      {
         get {
            return gxTv_SdtProjeto_Projeto_modulonome_N ;
         }

         set {
            gxTv_SdtProjeto_Projeto_modulonome_N = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_modulonome_N_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_modulonome_N = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_modulonome_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_ModuloSigla_N" )]
      [  XmlElement( ElementName = "Projeto_ModuloSigla_N"   )]
      public short gxTpr_Projeto_modulosigla_N
      {
         get {
            return gxTv_SdtProjeto_Projeto_modulosigla_N ;
         }

         set {
            gxTv_SdtProjeto_Projeto_modulosigla_N = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_modulosigla_N_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_modulosigla_N = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_modulosigla_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_AnexoSequecial_N" )]
      [  XmlElement( ElementName = "Projeto_AnexoSequecial_N"   )]
      public short gxTpr_Projeto_anexosequecial_N
      {
         get {
            return gxTv_SdtProjeto_Projeto_anexosequecial_N ;
         }

         set {
            gxTv_SdtProjeto_Projeto_anexosequecial_N = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_anexosequecial_N_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_anexosequecial_N = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_anexosequecial_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtProjeto_Projeto_nome = "";
         gxTv_SdtProjeto_Projeto_sigla = "";
         gxTv_SdtProjeto_Projeto_tecnicacontagem = "";
         gxTv_SdtProjeto_Projeto_introducao = "";
         gxTv_SdtProjeto_Projeto_escopo = "";
         gxTv_SdtProjeto_Projeto_previsao = DateTime.MinValue;
         gxTv_SdtProjeto_Projeto_status = "A";
         gxTv_SdtProjeto_Projeto_identificador = "";
         gxTv_SdtProjeto_Projeto_objetivo = "";
         gxTv_SdtProjeto_Projeto_dtinicio = DateTime.MinValue;
         gxTv_SdtProjeto_Projeto_dtfim = DateTime.MinValue;
         gxTv_SdtProjeto_Projeto_areatrabalhodescricao = "";
         gxTv_SdtProjeto_Projeto_gpoobjctrlnome = "";
         gxTv_SdtProjeto_Projeto_sistemanome = "";
         gxTv_SdtProjeto_Projeto_sistemasigla = "";
         gxTv_SdtProjeto_Projeto_modulonome = "";
         gxTv_SdtProjeto_Projeto_modulosigla = "";
         gxTv_SdtProjeto_Mode = "";
         gxTv_SdtProjeto_Projeto_nome_Z = "";
         gxTv_SdtProjeto_Projeto_sigla_Z = "";
         gxTv_SdtProjeto_Projeto_tecnicacontagem_Z = "";
         gxTv_SdtProjeto_Projeto_previsao_Z = DateTime.MinValue;
         gxTv_SdtProjeto_Projeto_status_Z = "";
         gxTv_SdtProjeto_Projeto_identificador_Z = "";
         gxTv_SdtProjeto_Projeto_dtinicio_Z = DateTime.MinValue;
         gxTv_SdtProjeto_Projeto_dtfim_Z = DateTime.MinValue;
         gxTv_SdtProjeto_Projeto_areatrabalhodescricao_Z = "";
         gxTv_SdtProjeto_Projeto_gpoobjctrlnome_Z = "";
         gxTv_SdtProjeto_Projeto_sistemanome_Z = "";
         gxTv_SdtProjeto_Projeto_sistemasigla_Z = "";
         gxTv_SdtProjeto_Projeto_modulonome_Z = "";
         gxTv_SdtProjeto_Projeto_modulosigla_Z = "";
         sTagName = "";
         sDateCnv = "";
         sNumToPad = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "projeto", "GeneXus.Programs.projeto_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtProjeto_Projeto_prazo ;
      private short gxTv_SdtProjeto_Projeto_esforco ;
      private short gxTv_SdtProjeto_Projeto_prazoprevisto ;
      private short gxTv_SdtProjeto_Projeto_esforcoprevisto ;
      private short gxTv_SdtProjeto_Projeto_anexosequecial ;
      private short gxTv_SdtProjeto_Initialized ;
      private short gxTv_SdtProjeto_Projeto_prazo_Z ;
      private short gxTv_SdtProjeto_Projeto_esforco_Z ;
      private short gxTv_SdtProjeto_Projeto_prazoprevisto_Z ;
      private short gxTv_SdtProjeto_Projeto_esforcoprevisto_Z ;
      private short gxTv_SdtProjeto_Projeto_anexosequecial_Z ;
      private short gxTv_SdtProjeto_Projeto_tipoprojetocod_N ;
      private short gxTv_SdtProjeto_Projeto_introducao_N ;
      private short gxTv_SdtProjeto_Projeto_previsao_N ;
      private short gxTv_SdtProjeto_Projeto_gerentecod_N ;
      private short gxTv_SdtProjeto_Projeto_servicocod_N ;
      private short gxTv_SdtProjeto_Projeto_esforco_N ;
      private short gxTv_SdtProjeto_Projeto_fatorescala_N ;
      private short gxTv_SdtProjeto_Projeto_fatormultiplicador_N ;
      private short gxTv_SdtProjeto_Projeto_constacocomo_N ;
      private short gxTv_SdtProjeto_Projeto_incremental_N ;
      private short gxTv_SdtProjeto_Projeto_identificador_N ;
      private short gxTv_SdtProjeto_Projeto_objetivo_N ;
      private short gxTv_SdtProjeto_Projeto_dtinicio_N ;
      private short gxTv_SdtProjeto_Projeto_dtfim_N ;
      private short gxTv_SdtProjeto_Projeto_prazoprevisto_N ;
      private short gxTv_SdtProjeto_Projeto_esforcoprevisto_N ;
      private short gxTv_SdtProjeto_Projeto_custoprevisto_N ;
      private short gxTv_SdtProjeto_Projeto_areatrabalhocodigo_N ;
      private short gxTv_SdtProjeto_Projeto_areatrabalhodescricao_N ;
      private short gxTv_SdtProjeto_Projeto_gpoobjctrlcodigo_N ;
      private short gxTv_SdtProjeto_Projeto_gpoobjctrlnome_N ;
      private short gxTv_SdtProjeto_Projeto_sistemacodigo_N ;
      private short gxTv_SdtProjeto_Projeto_sistemanome_N ;
      private short gxTv_SdtProjeto_Projeto_sistemasigla_N ;
      private short gxTv_SdtProjeto_Projeto_modulocodigo_N ;
      private short gxTv_SdtProjeto_Projeto_modulonome_N ;
      private short gxTv_SdtProjeto_Projeto_modulosigla_N ;
      private short gxTv_SdtProjeto_Projeto_anexosequecial_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtProjeto_Projeto_codigo ;
      private int gxTv_SdtProjeto_Projeto_tipoprojetocod ;
      private int gxTv_SdtProjeto_Projeto_gerentecod ;
      private int gxTv_SdtProjeto_Projeto_servicocod ;
      private int gxTv_SdtProjeto_Projeto_sistemacod ;
      private int gxTv_SdtProjeto_Projeto_areatrabalhocodigo ;
      private int gxTv_SdtProjeto_Projeto_gpoobjctrlcodigo ;
      private int gxTv_SdtProjeto_Projeto_sistemacodigo ;
      private int gxTv_SdtProjeto_Projeto_modulocodigo ;
      private int gxTv_SdtProjeto_Projeto_codigo_Z ;
      private int gxTv_SdtProjeto_Projeto_tipoprojetocod_Z ;
      private int gxTv_SdtProjeto_Projeto_gerentecod_Z ;
      private int gxTv_SdtProjeto_Projeto_servicocod_Z ;
      private int gxTv_SdtProjeto_Projeto_sistemacod_Z ;
      private int gxTv_SdtProjeto_Projeto_areatrabalhocodigo_Z ;
      private int gxTv_SdtProjeto_Projeto_gpoobjctrlcodigo_Z ;
      private int gxTv_SdtProjeto_Projeto_sistemacodigo_Z ;
      private int gxTv_SdtProjeto_Projeto_modulocodigo_Z ;
      private decimal gxTv_SdtProjeto_Projeto_custo ;
      private decimal gxTv_SdtProjeto_Projeto_fatorescala ;
      private decimal gxTv_SdtProjeto_Projeto_fatormultiplicador ;
      private decimal gxTv_SdtProjeto_Projeto_constacocomo ;
      private decimal gxTv_SdtProjeto_Projeto_custoprevisto ;
      private decimal gxTv_SdtProjeto_Projeto_custo_Z ;
      private decimal gxTv_SdtProjeto_Projeto_fatorescala_Z ;
      private decimal gxTv_SdtProjeto_Projeto_fatormultiplicador_Z ;
      private decimal gxTv_SdtProjeto_Projeto_constacocomo_Z ;
      private decimal gxTv_SdtProjeto_Projeto_custoprevisto_Z ;
      private String gxTv_SdtProjeto_Projeto_nome ;
      private String gxTv_SdtProjeto_Projeto_sigla ;
      private String gxTv_SdtProjeto_Projeto_tecnicacontagem ;
      private String gxTv_SdtProjeto_Projeto_status ;
      private String gxTv_SdtProjeto_Projeto_gpoobjctrlnome ;
      private String gxTv_SdtProjeto_Projeto_sistemasigla ;
      private String gxTv_SdtProjeto_Projeto_modulonome ;
      private String gxTv_SdtProjeto_Projeto_modulosigla ;
      private String gxTv_SdtProjeto_Mode ;
      private String gxTv_SdtProjeto_Projeto_nome_Z ;
      private String gxTv_SdtProjeto_Projeto_sigla_Z ;
      private String gxTv_SdtProjeto_Projeto_tecnicacontagem_Z ;
      private String gxTv_SdtProjeto_Projeto_status_Z ;
      private String gxTv_SdtProjeto_Projeto_gpoobjctrlnome_Z ;
      private String gxTv_SdtProjeto_Projeto_sistemasigla_Z ;
      private String gxTv_SdtProjeto_Projeto_modulonome_Z ;
      private String gxTv_SdtProjeto_Projeto_modulosigla_Z ;
      private String sTagName ;
      private String sDateCnv ;
      private String sNumToPad ;
      private DateTime gxTv_SdtProjeto_Projeto_previsao ;
      private DateTime gxTv_SdtProjeto_Projeto_dtinicio ;
      private DateTime gxTv_SdtProjeto_Projeto_dtfim ;
      private DateTime gxTv_SdtProjeto_Projeto_previsao_Z ;
      private DateTime gxTv_SdtProjeto_Projeto_dtinicio_Z ;
      private DateTime gxTv_SdtProjeto_Projeto_dtfim_Z ;
      private bool gxTv_SdtProjeto_Projeto_incremental ;
      private bool gxTv_SdtProjeto_Projeto_incremental_Z ;
      private String gxTv_SdtProjeto_Projeto_introducao ;
      private String gxTv_SdtProjeto_Projeto_escopo ;
      private String gxTv_SdtProjeto_Projeto_objetivo ;
      private String gxTv_SdtProjeto_Projeto_identificador ;
      private String gxTv_SdtProjeto_Projeto_areatrabalhodescricao ;
      private String gxTv_SdtProjeto_Projeto_sistemanome ;
      private String gxTv_SdtProjeto_Projeto_identificador_Z ;
      private String gxTv_SdtProjeto_Projeto_areatrabalhodescricao_Z ;
      private String gxTv_SdtProjeto_Projeto_sistemanome_Z ;
      private Assembly constructorCallingAssembly ;
      [ObjectCollection(ItemType=typeof( SdtProjeto_Anexos ))]
      private GxSilentTrnGridCollection gxTv_SdtProjeto_Anexos=null ;
   }

   [DataContract(Name = @"Projeto", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtProjeto_RESTInterface : GxGenericCollectionItem<SdtProjeto>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtProjeto_RESTInterface( ) : base()
      {
      }

      public SdtProjeto_RESTInterface( SdtProjeto psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Projeto_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Projeto_codigo
      {
         get {
            return sdt.gxTpr_Projeto_codigo ;
         }

         set {
            sdt.gxTpr_Projeto_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Projeto_Nome" , Order = 1 )]
      [GxSeudo()]
      public String gxTpr_Projeto_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Projeto_nome) ;
         }

         set {
            sdt.gxTpr_Projeto_nome = (String)(value);
         }

      }

      [DataMember( Name = "Projeto_Sigla" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Projeto_sigla
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Projeto_sigla) ;
         }

         set {
            sdt.gxTpr_Projeto_sigla = (String)(value);
         }

      }

      [DataMember( Name = "Projeto_TipoProjetoCod" , Order = 3 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Projeto_tipoprojetocod
      {
         get {
            return sdt.gxTpr_Projeto_tipoprojetocod ;
         }

         set {
            sdt.gxTpr_Projeto_tipoprojetocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Projeto_TecnicaContagem" , Order = 4 )]
      [GxSeudo()]
      public String gxTpr_Projeto_tecnicacontagem
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Projeto_tecnicacontagem) ;
         }

         set {
            sdt.gxTpr_Projeto_tecnicacontagem = (String)(value);
         }

      }

      [DataMember( Name = "Projeto_Introducao" , Order = 5 )]
      public String gxTpr_Projeto_introducao
      {
         get {
            return sdt.gxTpr_Projeto_introducao ;
         }

         set {
            sdt.gxTpr_Projeto_introducao = (String)(value);
         }

      }

      [DataMember( Name = "Projeto_Escopo" , Order = 6 )]
      public String gxTpr_Projeto_escopo
      {
         get {
            return sdt.gxTpr_Projeto_escopo ;
         }

         set {
            sdt.gxTpr_Projeto_escopo = (String)(value);
         }

      }

      [DataMember( Name = "Projeto_Previsao" , Order = 7 )]
      [GxSeudo()]
      public String gxTpr_Projeto_previsao
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Projeto_previsao) ;
         }

         set {
            sdt.gxTpr_Projeto_previsao = DateTimeUtil.CToD2( (String)(value));
         }

      }

      [DataMember( Name = "Projeto_GerenteCod" , Order = 8 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Projeto_gerentecod
      {
         get {
            return sdt.gxTpr_Projeto_gerentecod ;
         }

         set {
            sdt.gxTpr_Projeto_gerentecod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Projeto_ServicoCod" , Order = 9 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Projeto_servicocod
      {
         get {
            return sdt.gxTpr_Projeto_servicocod ;
         }

         set {
            sdt.gxTpr_Projeto_servicocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Projeto_Custo" , Order = 10 )]
      [GxSeudo()]
      public String gxTpr_Projeto_custo
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Projeto_custo, 12, 2)) ;
         }

         set {
            sdt.gxTpr_Projeto_custo = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "Projeto_Prazo" , Order = 11 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Projeto_prazo
      {
         get {
            return sdt.gxTpr_Projeto_prazo ;
         }

         set {
            sdt.gxTpr_Projeto_prazo = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Projeto_Esforco" , Order = 12 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Projeto_esforco
      {
         get {
            return sdt.gxTpr_Projeto_esforco ;
         }

         set {
            sdt.gxTpr_Projeto_esforco = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Projeto_SistemaCod" , Order = 13 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Projeto_sistemacod
      {
         get {
            return sdt.gxTpr_Projeto_sistemacod ;
         }

         set {
            sdt.gxTpr_Projeto_sistemacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Projeto_Status" , Order = 14 )]
      [GxSeudo()]
      public String gxTpr_Projeto_status
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Projeto_status) ;
         }

         set {
            sdt.gxTpr_Projeto_status = (String)(value);
         }

      }

      [DataMember( Name = "Projeto_FatorEscala" , Order = 15 )]
      [GxSeudo()]
      public Nullable<decimal> gxTpr_Projeto_fatorescala
      {
         get {
            return sdt.gxTpr_Projeto_fatorescala ;
         }

         set {
            sdt.gxTpr_Projeto_fatorescala = (decimal)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Projeto_FatorMultiplicador" , Order = 16 )]
      [GxSeudo()]
      public Nullable<decimal> gxTpr_Projeto_fatormultiplicador
      {
         get {
            return sdt.gxTpr_Projeto_fatormultiplicador ;
         }

         set {
            sdt.gxTpr_Projeto_fatormultiplicador = (decimal)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Projeto_ConstACocomo" , Order = 17 )]
      [GxSeudo()]
      public Nullable<decimal> gxTpr_Projeto_constacocomo
      {
         get {
            return sdt.gxTpr_Projeto_constacocomo ;
         }

         set {
            sdt.gxTpr_Projeto_constacocomo = (decimal)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Projeto_Incremental" , Order = 18 )]
      [GxSeudo()]
      public bool gxTpr_Projeto_incremental
      {
         get {
            return sdt.gxTpr_Projeto_incremental ;
         }

         set {
            sdt.gxTpr_Projeto_incremental = value;
         }

      }

      [DataMember( Name = "Projeto_Identificador" , Order = 19 )]
      [GxSeudo()]
      public String gxTpr_Projeto_identificador
      {
         get {
            return sdt.gxTpr_Projeto_identificador ;
         }

         set {
            sdt.gxTpr_Projeto_identificador = (String)(value);
         }

      }

      [DataMember( Name = "Projeto_Objetivo" , Order = 20 )]
      public String gxTpr_Projeto_objetivo
      {
         get {
            return sdt.gxTpr_Projeto_objetivo ;
         }

         set {
            sdt.gxTpr_Projeto_objetivo = (String)(value);
         }

      }

      [DataMember( Name = "Projeto_DTInicio" , Order = 21 )]
      [GxSeudo()]
      public String gxTpr_Projeto_dtinicio
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Projeto_dtinicio) ;
         }

         set {
            sdt.gxTpr_Projeto_dtinicio = DateTimeUtil.CToD2( (String)(value));
         }

      }

      [DataMember( Name = "Projeto_DTFim" , Order = 22 )]
      [GxSeudo()]
      public String gxTpr_Projeto_dtfim
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Projeto_dtfim) ;
         }

         set {
            sdt.gxTpr_Projeto_dtfim = DateTimeUtil.CToD2( (String)(value));
         }

      }

      [DataMember( Name = "Projeto_PrazoPrevisto" , Order = 23 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Projeto_prazoprevisto
      {
         get {
            return sdt.gxTpr_Projeto_prazoprevisto ;
         }

         set {
            sdt.gxTpr_Projeto_prazoprevisto = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Projeto_EsforcoPrevisto" , Order = 24 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Projeto_esforcoprevisto
      {
         get {
            return sdt.gxTpr_Projeto_esforcoprevisto ;
         }

         set {
            sdt.gxTpr_Projeto_esforcoprevisto = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Projeto_CustoPrevisto" , Order = 25 )]
      [GxSeudo()]
      public String gxTpr_Projeto_custoprevisto
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Projeto_custoprevisto, 12, 2)) ;
         }

         set {
            sdt.gxTpr_Projeto_custoprevisto = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "Projeto_AreaTrabalhoCodigo" , Order = 26 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Projeto_areatrabalhocodigo
      {
         get {
            return sdt.gxTpr_Projeto_areatrabalhocodigo ;
         }

         set {
            sdt.gxTpr_Projeto_areatrabalhocodigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Projeto_AreaTrabalhoDescricao" , Order = 27 )]
      [GxSeudo()]
      public String gxTpr_Projeto_areatrabalhodescricao
      {
         get {
            return sdt.gxTpr_Projeto_areatrabalhodescricao ;
         }

         set {
            sdt.gxTpr_Projeto_areatrabalhodescricao = (String)(value);
         }

      }

      [DataMember( Name = "Projeto_GpoObjCtrlCodigo" , Order = 28 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Projeto_gpoobjctrlcodigo
      {
         get {
            return sdt.gxTpr_Projeto_gpoobjctrlcodigo ;
         }

         set {
            sdt.gxTpr_Projeto_gpoobjctrlcodigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Projeto_GpoObjCtrlNome" , Order = 29 )]
      [GxSeudo()]
      public String gxTpr_Projeto_gpoobjctrlnome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Projeto_gpoobjctrlnome) ;
         }

         set {
            sdt.gxTpr_Projeto_gpoobjctrlnome = (String)(value);
         }

      }

      [DataMember( Name = "Projeto_SistemaCodigo" , Order = 30 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Projeto_sistemacodigo
      {
         get {
            return sdt.gxTpr_Projeto_sistemacodigo ;
         }

         set {
            sdt.gxTpr_Projeto_sistemacodigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Projeto_SistemaNome" , Order = 31 )]
      [GxSeudo()]
      public String gxTpr_Projeto_sistemanome
      {
         get {
            return sdt.gxTpr_Projeto_sistemanome ;
         }

         set {
            sdt.gxTpr_Projeto_sistemanome = (String)(value);
         }

      }

      [DataMember( Name = "Projeto_SistemaSigla" , Order = 32 )]
      [GxSeudo()]
      public String gxTpr_Projeto_sistemasigla
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Projeto_sistemasigla) ;
         }

         set {
            sdt.gxTpr_Projeto_sistemasigla = (String)(value);
         }

      }

      [DataMember( Name = "Projeto_ModuloCodigo" , Order = 33 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Projeto_modulocodigo
      {
         get {
            return sdt.gxTpr_Projeto_modulocodigo ;
         }

         set {
            sdt.gxTpr_Projeto_modulocodigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Projeto_ModuloNome" , Order = 34 )]
      [GxSeudo()]
      public String gxTpr_Projeto_modulonome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Projeto_modulonome) ;
         }

         set {
            sdt.gxTpr_Projeto_modulonome = (String)(value);
         }

      }

      [DataMember( Name = "Projeto_ModuloSigla" , Order = 35 )]
      [GxSeudo()]
      public String gxTpr_Projeto_modulosigla
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Projeto_modulosigla) ;
         }

         set {
            sdt.gxTpr_Projeto_modulosigla = (String)(value);
         }

      }

      [DataMember( Name = "Projeto_AnexoSequecial" , Order = 36 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Projeto_anexosequecial
      {
         get {
            return sdt.gxTpr_Projeto_anexosequecial ;
         }

         set {
            sdt.gxTpr_Projeto_anexosequecial = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Anexos" , Order = 37 )]
      public GxGenericCollection<SdtProjeto_Anexos_RESTInterface> gxTpr_Anexos
      {
         get {
            return new GxGenericCollection<SdtProjeto_Anexos_RESTInterface>(sdt.gxTpr_Anexos) ;
         }

         set {
            value.LoadCollection(sdt.gxTpr_Anexos);
         }

      }

      public SdtProjeto sdt
      {
         get {
            return (SdtProjeto)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtProjeto() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 102 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
