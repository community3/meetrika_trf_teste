/*
               File: PRC_VerificaDemandaViculada
        Description: Verifica se a Demanda esta vinculada a outra
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/16/2020 2:4:56.47
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_verificademandaviculada : GXProcedure
   {
      public prc_verificademandaviculada( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_verificademandaviculada( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo ,
                           int aP1_ContagemResultado_ContratadaCod ,
                           out bool aP2_IsVinculada )
      {
         this.AV9ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV10ContagemResultado_ContratadaCod = aP1_ContagemResultado_ContratadaCod;
         this.AV8IsVinculada = false ;
         initialize();
         executePrivate();
         aP2_IsVinculada=this.AV8IsVinculada;
      }

      public bool executeUdp( int aP0_ContagemResultado_Codigo ,
                              int aP1_ContagemResultado_ContratadaCod )
      {
         this.AV9ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV10ContagemResultado_ContratadaCod = aP1_ContagemResultado_ContratadaCod;
         this.AV8IsVinculada = false ;
         initialize();
         executePrivate();
         aP2_IsVinculada=this.AV8IsVinculada;
         return AV8IsVinculada ;
      }

      public void executeSubmit( int aP0_ContagemResultado_Codigo ,
                                 int aP1_ContagemResultado_ContratadaCod ,
                                 out bool aP2_IsVinculada )
      {
         prc_verificademandaviculada objprc_verificademandaviculada;
         objprc_verificademandaviculada = new prc_verificademandaviculada();
         objprc_verificademandaviculada.AV9ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_verificademandaviculada.AV10ContagemResultado_ContratadaCod = aP1_ContagemResultado_ContratadaCod;
         objprc_verificademandaviculada.AV8IsVinculada = false ;
         objprc_verificademandaviculada.context.SetSubmitInitialConfig(context);
         objprc_verificademandaviculada.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_verificademandaviculada);
         aP2_IsVinculada=this.AV8IsVinculada;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_verificademandaviculada)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV8IsVinculada = false;
         /* Using cursor P00YM2 */
         pr_default.execute(0, new Object[] {AV9ContagemResultado_Codigo, AV10ContagemResultado_ContratadaCod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A602ContagemResultado_OSVinculada = P00YM2_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P00YM2_n602ContagemResultado_OSVinculada[0];
            A490ContagemResultado_ContratadaCod = P00YM2_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00YM2_n490ContagemResultado_ContratadaCod[0];
            A456ContagemResultado_Codigo = P00YM2_A456ContagemResultado_Codigo[0];
            AV8IsVinculada = true;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00YM2_A602ContagemResultado_OSVinculada = new int[1] ;
         P00YM2_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P00YM2_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00YM2_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00YM2_A456ContagemResultado_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_verificademandaviculada__default(),
            new Object[][] {
                new Object[] {
               P00YM2_A602ContagemResultado_OSVinculada, P00YM2_n602ContagemResultado_OSVinculada, P00YM2_A490ContagemResultado_ContratadaCod, P00YM2_n490ContagemResultado_ContratadaCod, P00YM2_A456ContagemResultado_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV9ContagemResultado_Codigo ;
      private int AV10ContagemResultado_ContratadaCod ;
      private int A602ContagemResultado_OSVinculada ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A456ContagemResultado_Codigo ;
      private String scmdbuf ;
      private bool AV8IsVinculada ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n490ContagemResultado_ContratadaCod ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00YM2_A602ContagemResultado_OSVinculada ;
      private bool[] P00YM2_n602ContagemResultado_OSVinculada ;
      private int[] P00YM2_A490ContagemResultado_ContratadaCod ;
      private bool[] P00YM2_n490ContagemResultado_ContratadaCod ;
      private int[] P00YM2_A456ContagemResultado_Codigo ;
      private bool aP2_IsVinculada ;
   }

   public class prc_verificademandaviculada__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00YM2 ;
          prmP00YM2 = new Object[] {
          new Object[] {"@AV9ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV10ContagemResultado_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00YM2", "SELECT TOP 1 [ContagemResultado_OSVinculada], [ContagemResultado_ContratadaCod], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE ([ContagemResultado_OSVinculada] = @AV9ContagemResultado_Codigo) AND ([ContagemResultado_ContratadaCod] = @AV10ContagemResultado_ContratadaCod) ORDER BY [ContagemResultado_OSVinculada] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YM2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
