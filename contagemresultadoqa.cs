/*
               File: ContagemResultadoQA
        Description: Contagem Resultado QA
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:17:56.19
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemresultadoqa : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel2"+"_"+"CONTAGEMRESULTADOQA_RESPOSTA") == 0 )
         {
            A1984ContagemResultadoQA_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1984ContagemResultadoQA_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1984ContagemResultadoQA_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX2ASACONTAGEMRESULTADOQA_RESPOSTA4Y220( A1984ContagemResultadoQA_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel3"+"_"+"CONTAGEMRESULTADOQA_CONTRATANTEDOPARA") == 0 )
         {
            A1985ContagemResultadoQA_OSCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1985ContagemResultadoQA_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1985ContagemResultadoQA_OSCod), 6, 0)));
            A1992ContagemResultadoQA_ParaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1992ContagemResultadoQA_ParaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1992ContagemResultadoQA_ParaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX3ASACONTAGEMRESULTADOQA_CONTRATANTEDOPARA4Y220( A1985ContagemResultadoQA_OSCod, A1992ContagemResultadoQA_ParaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel4"+"_"+"CONTAGEMRESULTADOQA_CONTRATANTEDOUSER") == 0 )
         {
            A1985ContagemResultadoQA_OSCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1985ContagemResultadoQA_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1985ContagemResultadoQA_OSCod), 6, 0)));
            A1988ContagemResultadoQA_UserCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1988ContagemResultadoQA_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1988ContagemResultadoQA_UserCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX4ASACONTAGEMRESULTADOQA_CONTRATANTEDOUSER4Y220( A1985ContagemResultadoQA_OSCod, A1988ContagemResultadoQA_UserCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_8") == 0 )
         {
            A1985ContagemResultadoQA_OSCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1985ContagemResultadoQA_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1985ContagemResultadoQA_OSCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_8( A1985ContagemResultadoQA_OSCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_9") == 0 )
         {
            A1988ContagemResultadoQA_UserCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1988ContagemResultadoQA_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1988ContagemResultadoQA_UserCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_9( A1988ContagemResultadoQA_UserCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_12") == 0 )
         {
            A1989ContagemResultadoQA_UserPesCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1989ContagemResultadoQA_UserPesCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1989ContagemResultadoQA_UserPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1989ContagemResultadoQA_UserPesCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_12( A1989ContagemResultadoQA_UserPesCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_10") == 0 )
         {
            A1992ContagemResultadoQA_ParaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1992ContagemResultadoQA_ParaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1992ContagemResultadoQA_ParaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_10( A1992ContagemResultadoQA_ParaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_13") == 0 )
         {
            A1993ContagemResultadoQA_ParaPesCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1993ContagemResultadoQA_ParaPesCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1993ContagemResultadoQA_ParaPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1993ContagemResultadoQA_ParaPesCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_13( A1993ContagemResultadoQA_ParaPesCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_11") == 0 )
         {
            A1996ContagemResultadoQA_RespostaDe = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1996ContagemResultadoQA_RespostaDe = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1996ContagemResultadoQA_RespostaDe", StringUtil.LTrim( StringUtil.Str( (decimal)(A1996ContagemResultadoQA_RespostaDe), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_11( A1996ContagemResultadoQA_RespostaDe) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Contagem Resultado QA", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtContagemResultadoQA_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contagemresultadoqa( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contagemresultadoqa( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_4Y220( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_4Y220e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_4Y220( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_4Y220( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_4Y220e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Contagem Resultado QA", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_ContagemResultadoQA.htm");
            wb_table3_28_4Y220( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_4Y220e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_4Y220e( true) ;
         }
         else
         {
            wb_table1_2_4Y220e( false) ;
         }
      }

      protected void wb_table3_28_4Y220( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_4Y220( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_4Y220e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoQA.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoQA.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoQA.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_4Y220e( true) ;
         }
         else
         {
            wb_table3_28_4Y220e( false) ;
         }
      }

      protected void wb_table4_34_4Y220( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoqa_codigo_Internalname, "Resultado QA_Codigo", "", "", lblTextblockcontagemresultadoqa_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoQA.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoQA_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1984ContagemResultadoQA_Codigo), 6, 0, ",", "")), ((edtContagemResultadoQA_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1984ContagemResultadoQA_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1984ContagemResultadoQA_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoQA_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoQA_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoQA.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoqa_oscod_Internalname, "Resultado QA_OSCod", "", "", lblTextblockcontagemresultadoqa_oscod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoQA.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoQA_OSCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1985ContagemResultadoQA_OSCod), 6, 0, ",", "")), ((edtContagemResultadoQA_OSCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1985ContagemResultadoQA_OSCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1985ContagemResultadoQA_OSCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoQA_OSCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoQA_OSCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoQA.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_demandafm_Internalname, "N� OS", "", "", lblTextblockcontagemresultado_demandafm_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoQA.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_DemandaFM_Internalname, A493ContagemResultado_DemandaFM, StringUtil.RTrim( context.localUtil.Format( A493ContagemResultado_DemandaFM, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_DemandaFM_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultado_DemandaFM_Enabled, 0, "text", "", 80, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContagemResultadoQA.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoqa_datahora_Internalname, "Data", "", "", lblTextblockcontagemresultadoqa_datahora_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoQA.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContagemResultadoQA_DataHora_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoQA_DataHora_Internalname, context.localUtil.TToC( A1986ContagemResultadoQA_DataHora, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A1986ContagemResultadoQA_DataHora, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoQA_DataHora_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoQA_DataHora_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "DataHora", "right", false, "HLP_ContagemResultadoQA.htm");
            GxWebStd.gx_bitmap( context, edtContagemResultadoQA_DataHora_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContagemResultadoQA_DataHora_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContagemResultadoQA.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoqa_texto_Internalname, "Texto", "", "", lblTextblockcontagemresultadoqa_texto_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoQA.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContagemResultadoQA_Texto_Internalname, A1987ContagemResultadoQA_Texto, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", 0, 1, edtContagemResultadoQA_Texto_Enabled, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_ContagemResultadoQA.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoqa_usercod_Internalname, "De", "", "", lblTextblockcontagemresultadoqa_usercod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoQA.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoQA_UserCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1988ContagemResultadoQA_UserCod), 6, 0, ",", "")), ((edtContagemResultadoQA_UserCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1988ContagemResultadoQA_UserCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1988ContagemResultadoQA_UserCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoQA_UserCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoQA_UserCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoQA.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoqa_userpescod_Internalname, "Pes Cod", "", "", lblTextblockcontagemresultadoqa_userpescod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoQA.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoQA_UserPesCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1989ContagemResultadoQA_UserPesCod), 6, 0, ",", "")), ((edtContagemResultadoQA_UserPesCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1989ContagemResultadoQA_UserPesCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1989ContagemResultadoQA_UserPesCod), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoQA_UserPesCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoQA_UserPesCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoQA.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoqa_userpesnom_Internalname, "De", "", "", lblTextblockcontagemresultadoqa_userpesnom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoQA.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoQA_UserPesNom_Internalname, StringUtil.RTrim( A1990ContagemResultadoQA_UserPesNom), StringUtil.RTrim( context.localUtil.Format( A1990ContagemResultadoQA_UserPesNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoQA_UserPesNom_Jsonclick, 0, "BootstrapAttribute100", "", "", "", 1, edtContagemResultadoQA_UserPesNom_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContagemResultadoQA.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoqa_contratantedouser_Internalname, "Do User", "", "", lblTextblockcontagemresultadoqa_contratantedouser_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoQA.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoQA_ContratanteDoUser_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1991ContagemResultadoQA_ContratanteDoUser), 6, 0, ",", "")), ((edtContagemResultadoQA_ContratanteDoUser_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1991ContagemResultadoQA_ContratanteDoUser), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1991ContagemResultadoQA_ContratanteDoUser), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoQA_ContratanteDoUser_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoQA_ContratanteDoUser_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoQA.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoqa_paracod_Internalname, "Para", "", "", lblTextblockcontagemresultadoqa_paracod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoQA.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoQA_ParaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1992ContagemResultadoQA_ParaCod), 6, 0, ",", "")), ((edtContagemResultadoQA_ParaCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1992ContagemResultadoQA_ParaCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1992ContagemResultadoQA_ParaCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,84);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoQA_ParaCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoQA_ParaCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoQA.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoqa_parapescod_Internalname, "Pes Cod", "", "", lblTextblockcontagemresultadoqa_parapescod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoQA.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoQA_ParaPesCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1993ContagemResultadoQA_ParaPesCod), 6, 0, ",", "")), ((edtContagemResultadoQA_ParaPesCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1993ContagemResultadoQA_ParaPesCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1993ContagemResultadoQA_ParaPesCod), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoQA_ParaPesCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoQA_ParaPesCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoQA.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoqa_parapesnom_Internalname, "Para", "", "", lblTextblockcontagemresultadoqa_parapesnom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoQA.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoQA_ParaPesNom_Internalname, StringUtil.RTrim( A1994ContagemResultadoQA_ParaPesNom), StringUtil.RTrim( context.localUtil.Format( A1994ContagemResultadoQA_ParaPesNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoQA_ParaPesNom_Jsonclick, 0, "BootstrapAttribute100", "", "", "", 1, edtContagemResultadoQA_ParaPesNom_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContagemResultadoQA.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoqa_contratantedopara_Internalname, "Do Para", "", "", lblTextblockcontagemresultadoqa_contratantedopara_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoQA.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoQA_ContratanteDoPara_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1995ContagemResultadoQA_ContratanteDoPara), 6, 0, ",", "")), ((edtContagemResultadoQA_ContratanteDoPara_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1995ContagemResultadoQA_ContratanteDoPara), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1995ContagemResultadoQA_ContratanteDoPara), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoQA_ContratanteDoPara_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoQA_ContratanteDoPara_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoQA.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoqa_respostade_Internalname, "QA_Resposta De", "", "", lblTextblockcontagemresultadoqa_respostade_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoQA.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoQA_RespostaDe_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1996ContagemResultadoQA_RespostaDe), 6, 0, ",", "")), ((edtContagemResultadoQA_RespostaDe_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1996ContagemResultadoQA_RespostaDe), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1996ContagemResultadoQA_RespostaDe), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,104);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoQA_RespostaDe_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoQA_RespostaDe_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoQA.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoqa_resposta_Internalname, "Resposta", "", "", lblTextblockcontagemresultadoqa_resposta_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoQA.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoQA_Resposta_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1998ContagemResultadoQA_Resposta), 6, 0, ",", "")), ((edtContagemResultadoQA_Resposta_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1998ContagemResultadoQA_Resposta), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1998ContagemResultadoQA_Resposta), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoQA_Resposta_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoQA_Resposta_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoQA.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_4Y220e( true) ;
         }
         else
         {
            wb_table4_34_4Y220e( false) ;
         }
      }

      protected void wb_table2_5_4Y220( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoQA.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoQA.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoQA.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoQA.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoQA.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoQA.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoQA.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoQA.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoQA.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoQA.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoQA.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoQA.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoQA.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoQA.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoQA.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoQA.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_4Y220e( true) ;
         }
         else
         {
            wb_table2_5_4Y220e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoQA_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoQA_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADOQA_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoQA_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1984ContagemResultadoQA_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1984ContagemResultadoQA_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1984ContagemResultadoQA_Codigo), 6, 0)));
               }
               else
               {
                  A1984ContagemResultadoQA_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoQA_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1984ContagemResultadoQA_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1984ContagemResultadoQA_Codigo), 6, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoQA_OSCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoQA_OSCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADOQA_OSCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoQA_OSCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1985ContagemResultadoQA_OSCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1985ContagemResultadoQA_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1985ContagemResultadoQA_OSCod), 6, 0)));
               }
               else
               {
                  A1985ContagemResultadoQA_OSCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoQA_OSCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1985ContagemResultadoQA_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1985ContagemResultadoQA_OSCod), 6, 0)));
               }
               A493ContagemResultado_DemandaFM = cgiGet( edtContagemResultado_DemandaFM_Internalname);
               n493ContagemResultado_DemandaFM = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A493ContagemResultado_DemandaFM", A493ContagemResultado_DemandaFM);
               if ( context.localUtil.VCDateTime( cgiGet( edtContagemResultadoQA_DataHora_Internalname), 2, 0) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Data"}), 1, "CONTAGEMRESULTADOQA_DATAHORA");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoQA_DataHora_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1986ContagemResultadoQA_DataHora = (DateTime)(DateTime.MinValue);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1986ContagemResultadoQA_DataHora", context.localUtil.TToC( A1986ContagemResultadoQA_DataHora, 8, 5, 0, 3, "/", ":", " "));
               }
               else
               {
                  A1986ContagemResultadoQA_DataHora = context.localUtil.CToT( cgiGet( edtContagemResultadoQA_DataHora_Internalname));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1986ContagemResultadoQA_DataHora", context.localUtil.TToC( A1986ContagemResultadoQA_DataHora, 8, 5, 0, 3, "/", ":", " "));
               }
               A1987ContagemResultadoQA_Texto = cgiGet( edtContagemResultadoQA_Texto_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1987ContagemResultadoQA_Texto", A1987ContagemResultadoQA_Texto);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoQA_UserCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoQA_UserCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADOQA_USERCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoQA_UserCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1988ContagemResultadoQA_UserCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1988ContagemResultadoQA_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1988ContagemResultadoQA_UserCod), 6, 0)));
               }
               else
               {
                  A1988ContagemResultadoQA_UserCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoQA_UserCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1988ContagemResultadoQA_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1988ContagemResultadoQA_UserCod), 6, 0)));
               }
               A1989ContagemResultadoQA_UserPesCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoQA_UserPesCod_Internalname), ",", "."));
               n1989ContagemResultadoQA_UserPesCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1989ContagemResultadoQA_UserPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1989ContagemResultadoQA_UserPesCod), 6, 0)));
               A1990ContagemResultadoQA_UserPesNom = StringUtil.Upper( cgiGet( edtContagemResultadoQA_UserPesNom_Internalname));
               n1990ContagemResultadoQA_UserPesNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1990ContagemResultadoQA_UserPesNom", A1990ContagemResultadoQA_UserPesNom);
               A1991ContagemResultadoQA_ContratanteDoUser = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoQA_ContratanteDoUser_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1991ContagemResultadoQA_ContratanteDoUser", StringUtil.LTrim( StringUtil.Str( (decimal)(A1991ContagemResultadoQA_ContratanteDoUser), 6, 0)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoQA_ParaCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoQA_ParaCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADOQA_PARACOD");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoQA_ParaCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1992ContagemResultadoQA_ParaCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1992ContagemResultadoQA_ParaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1992ContagemResultadoQA_ParaCod), 6, 0)));
               }
               else
               {
                  A1992ContagemResultadoQA_ParaCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoQA_ParaCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1992ContagemResultadoQA_ParaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1992ContagemResultadoQA_ParaCod), 6, 0)));
               }
               A1993ContagemResultadoQA_ParaPesCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoQA_ParaPesCod_Internalname), ",", "."));
               n1993ContagemResultadoQA_ParaPesCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1993ContagemResultadoQA_ParaPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1993ContagemResultadoQA_ParaPesCod), 6, 0)));
               A1994ContagemResultadoQA_ParaPesNom = StringUtil.Upper( cgiGet( edtContagemResultadoQA_ParaPesNom_Internalname));
               n1994ContagemResultadoQA_ParaPesNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1994ContagemResultadoQA_ParaPesNom", A1994ContagemResultadoQA_ParaPesNom);
               A1995ContagemResultadoQA_ContratanteDoPara = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoQA_ContratanteDoPara_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1995ContagemResultadoQA_ContratanteDoPara", StringUtil.LTrim( StringUtil.Str( (decimal)(A1995ContagemResultadoQA_ContratanteDoPara), 6, 0)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoQA_RespostaDe_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoQA_RespostaDe_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADOQA_RESPOSTADE");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoQA_RespostaDe_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1996ContagemResultadoQA_RespostaDe = 0;
                  n1996ContagemResultadoQA_RespostaDe = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1996ContagemResultadoQA_RespostaDe", StringUtil.LTrim( StringUtil.Str( (decimal)(A1996ContagemResultadoQA_RespostaDe), 6, 0)));
               }
               else
               {
                  A1996ContagemResultadoQA_RespostaDe = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoQA_RespostaDe_Internalname), ",", "."));
                  n1996ContagemResultadoQA_RespostaDe = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1996ContagemResultadoQA_RespostaDe", StringUtil.LTrim( StringUtil.Str( (decimal)(A1996ContagemResultadoQA_RespostaDe), 6, 0)));
               }
               n1996ContagemResultadoQA_RespostaDe = ((0==A1996ContagemResultadoQA_RespostaDe) ? true : false);
               A1998ContagemResultadoQA_Resposta = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoQA_Resposta_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1998ContagemResultadoQA_Resposta", StringUtil.LTrim( StringUtil.Str( (decimal)(A1998ContagemResultadoQA_Resposta), 6, 0)));
               /* Read saved values. */
               Z1984ContagemResultadoQA_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1984ContagemResultadoQA_Codigo"), ",", "."));
               Z1986ContagemResultadoQA_DataHora = context.localUtil.CToT( cgiGet( "Z1986ContagemResultadoQA_DataHora"), 0);
               Z1985ContagemResultadoQA_OSCod = (int)(context.localUtil.CToN( cgiGet( "Z1985ContagemResultadoQA_OSCod"), ",", "."));
               Z1988ContagemResultadoQA_UserCod = (int)(context.localUtil.CToN( cgiGet( "Z1988ContagemResultadoQA_UserCod"), ",", "."));
               Z1992ContagemResultadoQA_ParaCod = (int)(context.localUtil.CToN( cgiGet( "Z1992ContagemResultadoQA_ParaCod"), ",", "."));
               Z1996ContagemResultadoQA_RespostaDe = (int)(context.localUtil.CToN( cgiGet( "Z1996ContagemResultadoQA_RespostaDe"), ",", "."));
               n1996ContagemResultadoQA_RespostaDe = ((0==A1996ContagemResultadoQA_RespostaDe) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A1984ContagemResultadoQA_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1984ContagemResultadoQA_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1984ContagemResultadoQA_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll4Y220( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes4Y220( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption4Y0( )
      {
      }

      protected void ZM4Y220( short GX_JID )
      {
         if ( ( GX_JID == 7 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1986ContagemResultadoQA_DataHora = T004Y3_A1986ContagemResultadoQA_DataHora[0];
               Z1985ContagemResultadoQA_OSCod = T004Y3_A1985ContagemResultadoQA_OSCod[0];
               Z1988ContagemResultadoQA_UserCod = T004Y3_A1988ContagemResultadoQA_UserCod[0];
               Z1992ContagemResultadoQA_ParaCod = T004Y3_A1992ContagemResultadoQA_ParaCod[0];
               Z1996ContagemResultadoQA_RespostaDe = T004Y3_A1996ContagemResultadoQA_RespostaDe[0];
            }
            else
            {
               Z1986ContagemResultadoQA_DataHora = A1986ContagemResultadoQA_DataHora;
               Z1985ContagemResultadoQA_OSCod = A1985ContagemResultadoQA_OSCod;
               Z1988ContagemResultadoQA_UserCod = A1988ContagemResultadoQA_UserCod;
               Z1992ContagemResultadoQA_ParaCod = A1992ContagemResultadoQA_ParaCod;
               Z1996ContagemResultadoQA_RespostaDe = A1996ContagemResultadoQA_RespostaDe;
            }
         }
         if ( GX_JID == -7 )
         {
            Z1984ContagemResultadoQA_Codigo = A1984ContagemResultadoQA_Codigo;
            Z1986ContagemResultadoQA_DataHora = A1986ContagemResultadoQA_DataHora;
            Z1987ContagemResultadoQA_Texto = A1987ContagemResultadoQA_Texto;
            Z1985ContagemResultadoQA_OSCod = A1985ContagemResultadoQA_OSCod;
            Z1988ContagemResultadoQA_UserCod = A1988ContagemResultadoQA_UserCod;
            Z1992ContagemResultadoQA_ParaCod = A1992ContagemResultadoQA_ParaCod;
            Z1996ContagemResultadoQA_RespostaDe = A1996ContagemResultadoQA_RespostaDe;
            Z493ContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
            Z1989ContagemResultadoQA_UserPesCod = A1989ContagemResultadoQA_UserPesCod;
            Z1990ContagemResultadoQA_UserPesNom = A1990ContagemResultadoQA_UserPesNom;
            Z1993ContagemResultadoQA_ParaPesCod = A1993ContagemResultadoQA_ParaPesCod;
            Z1994ContagemResultadoQA_ParaPesNom = A1994ContagemResultadoQA_ParaPesNom;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
      }

      protected void Load4Y220( )
      {
         /* Using cursor T004Y10 */
         pr_default.execute(8, new Object[] {A1984ContagemResultadoQA_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound220 = 1;
            A1986ContagemResultadoQA_DataHora = T004Y10_A1986ContagemResultadoQA_DataHora[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1986ContagemResultadoQA_DataHora", context.localUtil.TToC( A1986ContagemResultadoQA_DataHora, 8, 5, 0, 3, "/", ":", " "));
            A493ContagemResultado_DemandaFM = T004Y10_A493ContagemResultado_DemandaFM[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A493ContagemResultado_DemandaFM", A493ContagemResultado_DemandaFM);
            n493ContagemResultado_DemandaFM = T004Y10_n493ContagemResultado_DemandaFM[0];
            A1987ContagemResultadoQA_Texto = T004Y10_A1987ContagemResultadoQA_Texto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1987ContagemResultadoQA_Texto", A1987ContagemResultadoQA_Texto);
            A1990ContagemResultadoQA_UserPesNom = T004Y10_A1990ContagemResultadoQA_UserPesNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1990ContagemResultadoQA_UserPesNom", A1990ContagemResultadoQA_UserPesNom);
            n1990ContagemResultadoQA_UserPesNom = T004Y10_n1990ContagemResultadoQA_UserPesNom[0];
            A1994ContagemResultadoQA_ParaPesNom = T004Y10_A1994ContagemResultadoQA_ParaPesNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1994ContagemResultadoQA_ParaPesNom", A1994ContagemResultadoQA_ParaPesNom);
            n1994ContagemResultadoQA_ParaPesNom = T004Y10_n1994ContagemResultadoQA_ParaPesNom[0];
            A1985ContagemResultadoQA_OSCod = T004Y10_A1985ContagemResultadoQA_OSCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1985ContagemResultadoQA_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1985ContagemResultadoQA_OSCod), 6, 0)));
            A1988ContagemResultadoQA_UserCod = T004Y10_A1988ContagemResultadoQA_UserCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1988ContagemResultadoQA_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1988ContagemResultadoQA_UserCod), 6, 0)));
            A1992ContagemResultadoQA_ParaCod = T004Y10_A1992ContagemResultadoQA_ParaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1992ContagemResultadoQA_ParaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1992ContagemResultadoQA_ParaCod), 6, 0)));
            A1996ContagemResultadoQA_RespostaDe = T004Y10_A1996ContagemResultadoQA_RespostaDe[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1996ContagemResultadoQA_RespostaDe", StringUtil.LTrim( StringUtil.Str( (decimal)(A1996ContagemResultadoQA_RespostaDe), 6, 0)));
            n1996ContagemResultadoQA_RespostaDe = T004Y10_n1996ContagemResultadoQA_RespostaDe[0];
            A1989ContagemResultadoQA_UserPesCod = T004Y10_A1989ContagemResultadoQA_UserPesCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1989ContagemResultadoQA_UserPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1989ContagemResultadoQA_UserPesCod), 6, 0)));
            n1989ContagemResultadoQA_UserPesCod = T004Y10_n1989ContagemResultadoQA_UserPesCod[0];
            A1993ContagemResultadoQA_ParaPesCod = T004Y10_A1993ContagemResultadoQA_ParaPesCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1993ContagemResultadoQA_ParaPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1993ContagemResultadoQA_ParaPesCod), 6, 0)));
            n1993ContagemResultadoQA_ParaPesCod = T004Y10_n1993ContagemResultadoQA_ParaPesCod[0];
            ZM4Y220( -7) ;
         }
         pr_default.close(8);
         OnLoadActions4Y220( ) ;
      }

      protected void OnLoadActions4Y220( )
      {
         GXt_int1 = A1998ContagemResultadoQA_Resposta;
         new prc_qa_resposta(context ).execute( ref  A1984ContagemResultadoQA_Codigo, out  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1984ContagemResultadoQA_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1984ContagemResultadoQA_Codigo), 6, 0)));
         A1998ContagemResultadoQA_Resposta = GXt_int1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1998ContagemResultadoQA_Resposta", StringUtil.LTrim( StringUtil.Str( (decimal)(A1998ContagemResultadoQA_Resposta), 6, 0)));
         GXt_int1 = A1995ContagemResultadoQA_ContratanteDoPara;
         new prc_contratantedousuario(context ).execute( ref  A1985ContagemResultadoQA_OSCod, ref  A1992ContagemResultadoQA_ParaCod, out  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1985ContagemResultadoQA_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1985ContagemResultadoQA_OSCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1992ContagemResultadoQA_ParaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1992ContagemResultadoQA_ParaCod), 6, 0)));
         A1995ContagemResultadoQA_ContratanteDoPara = GXt_int1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1995ContagemResultadoQA_ContratanteDoPara", StringUtil.LTrim( StringUtil.Str( (decimal)(A1995ContagemResultadoQA_ContratanteDoPara), 6, 0)));
         GXt_int1 = A1991ContagemResultadoQA_ContratanteDoUser;
         new prc_contratantedousuario(context ).execute( ref  A1985ContagemResultadoQA_OSCod, ref  A1988ContagemResultadoQA_UserCod, out  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1985ContagemResultadoQA_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1985ContagemResultadoQA_OSCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1988ContagemResultadoQA_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1988ContagemResultadoQA_UserCod), 6, 0)));
         A1991ContagemResultadoQA_ContratanteDoUser = GXt_int1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1991ContagemResultadoQA_ContratanteDoUser", StringUtil.LTrim( StringUtil.Str( (decimal)(A1991ContagemResultadoQA_ContratanteDoUser), 6, 0)));
      }

      protected void CheckExtendedTable4Y220( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         GXt_int1 = A1998ContagemResultadoQA_Resposta;
         new prc_qa_resposta(context ).execute( ref  A1984ContagemResultadoQA_Codigo, out  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1984ContagemResultadoQA_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1984ContagemResultadoQA_Codigo), 6, 0)));
         A1998ContagemResultadoQA_Resposta = GXt_int1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1998ContagemResultadoQA_Resposta", StringUtil.LTrim( StringUtil.Str( (decimal)(A1998ContagemResultadoQA_Resposta), 6, 0)));
         /* Using cursor T004Y4 */
         pr_default.execute(2, new Object[] {A1985ContagemResultadoQA_OSCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado QA_OSCod'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOQA_OSCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoQA_OSCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A493ContagemResultado_DemandaFM = T004Y4_A493ContagemResultado_DemandaFM[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A493ContagemResultado_DemandaFM", A493ContagemResultado_DemandaFM);
         n493ContagemResultado_DemandaFM = T004Y4_n493ContagemResultado_DemandaFM[0];
         pr_default.close(2);
         GXt_int1 = A1995ContagemResultadoQA_ContratanteDoPara;
         new prc_contratantedousuario(context ).execute( ref  A1985ContagemResultadoQA_OSCod, ref  A1992ContagemResultadoQA_ParaCod, out  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1985ContagemResultadoQA_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1985ContagemResultadoQA_OSCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1992ContagemResultadoQA_ParaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1992ContagemResultadoQA_ParaCod), 6, 0)));
         A1995ContagemResultadoQA_ContratanteDoPara = GXt_int1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1995ContagemResultadoQA_ContratanteDoPara", StringUtil.LTrim( StringUtil.Str( (decimal)(A1995ContagemResultadoQA_ContratanteDoPara), 6, 0)));
         GXt_int1 = A1991ContagemResultadoQA_ContratanteDoUser;
         new prc_contratantedousuario(context ).execute( ref  A1985ContagemResultadoQA_OSCod, ref  A1988ContagemResultadoQA_UserCod, out  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1985ContagemResultadoQA_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1985ContagemResultadoQA_OSCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1988ContagemResultadoQA_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1988ContagemResultadoQA_UserCod), 6, 0)));
         A1991ContagemResultadoQA_ContratanteDoUser = GXt_int1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1991ContagemResultadoQA_ContratanteDoUser", StringUtil.LTrim( StringUtil.Str( (decimal)(A1991ContagemResultadoQA_ContratanteDoUser), 6, 0)));
         if ( ! ( (DateTime.MinValue==A1986ContagemResultadoQA_DataHora) || ( A1986ContagemResultadoQA_DataHora >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Data fora do intervalo", "OutOfRange", 1, "CONTAGEMRESULTADOQA_DATAHORA");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoQA_DataHora_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T004Y5 */
         pr_default.execute(3, new Object[] {A1988ContagemResultadoQA_UserCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado QA_Usuario'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOQA_USERCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoQA_UserCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1989ContagemResultadoQA_UserPesCod = T004Y5_A1989ContagemResultadoQA_UserPesCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1989ContagemResultadoQA_UserPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1989ContagemResultadoQA_UserPesCod), 6, 0)));
         n1989ContagemResultadoQA_UserPesCod = T004Y5_n1989ContagemResultadoQA_UserPesCod[0];
         pr_default.close(3);
         /* Using cursor T004Y8 */
         pr_default.execute(6, new Object[] {n1989ContagemResultadoQA_UserPesCod, A1989ContagemResultadoQA_UserPesCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1990ContagemResultadoQA_UserPesNom = T004Y8_A1990ContagemResultadoQA_UserPesNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1990ContagemResultadoQA_UserPesNom", A1990ContagemResultadoQA_UserPesNom);
         n1990ContagemResultadoQA_UserPesNom = T004Y8_n1990ContagemResultadoQA_UserPesNom[0];
         pr_default.close(6);
         /* Using cursor T004Y6 */
         pr_default.execute(4, new Object[] {A1992ContagemResultadoQA_ParaCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado QA_Para'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOQA_PARACOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoQA_ParaCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1993ContagemResultadoQA_ParaPesCod = T004Y6_A1993ContagemResultadoQA_ParaPesCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1993ContagemResultadoQA_ParaPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1993ContagemResultadoQA_ParaPesCod), 6, 0)));
         n1993ContagemResultadoQA_ParaPesCod = T004Y6_n1993ContagemResultadoQA_ParaPesCod[0];
         pr_default.close(4);
         /* Using cursor T004Y9 */
         pr_default.execute(7, new Object[] {n1993ContagemResultadoQA_ParaPesCod, A1993ContagemResultadoQA_ParaPesCod});
         if ( (pr_default.getStatus(7) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1994ContagemResultadoQA_ParaPesNom = T004Y9_A1994ContagemResultadoQA_ParaPesNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1994ContagemResultadoQA_ParaPesNom", A1994ContagemResultadoQA_ParaPesNom);
         n1994ContagemResultadoQA_ParaPesNom = T004Y9_n1994ContagemResultadoQA_ParaPesNom[0];
         pr_default.close(7);
         /* Using cursor T004Y7 */
         pr_default.execute(5, new Object[] {n1996ContagemResultadoQA_RespostaDe, A1996ContagemResultadoQA_RespostaDe});
         if ( (pr_default.getStatus(5) == 101) )
         {
            if ( ! ( (0==A1996ContagemResultadoQA_RespostaDe) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado QA_Resposta De'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOQA_RESPOSTADE");
               AnyError = 1;
               GX_FocusControl = edtContagemResultadoQA_RespostaDe_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         pr_default.close(5);
      }

      protected void CloseExtendedTableCursors4Y220( )
      {
         pr_default.close(2);
         pr_default.close(3);
         pr_default.close(6);
         pr_default.close(4);
         pr_default.close(7);
         pr_default.close(5);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_8( int A1985ContagemResultadoQA_OSCod )
      {
         /* Using cursor T004Y11 */
         pr_default.execute(9, new Object[] {A1985ContagemResultadoQA_OSCod});
         if ( (pr_default.getStatus(9) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado QA_OSCod'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOQA_OSCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoQA_OSCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A493ContagemResultado_DemandaFM = T004Y11_A493ContagemResultado_DemandaFM[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A493ContagemResultado_DemandaFM", A493ContagemResultado_DemandaFM);
         n493ContagemResultado_DemandaFM = T004Y11_n493ContagemResultado_DemandaFM[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A493ContagemResultado_DemandaFM)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(9) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(9);
      }

      protected void gxLoad_9( int A1988ContagemResultadoQA_UserCod )
      {
         /* Using cursor T004Y12 */
         pr_default.execute(10, new Object[] {A1988ContagemResultadoQA_UserCod});
         if ( (pr_default.getStatus(10) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado QA_Usuario'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOQA_USERCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoQA_UserCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1989ContagemResultadoQA_UserPesCod = T004Y12_A1989ContagemResultadoQA_UserPesCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1989ContagemResultadoQA_UserPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1989ContagemResultadoQA_UserPesCod), 6, 0)));
         n1989ContagemResultadoQA_UserPesCod = T004Y12_n1989ContagemResultadoQA_UserPesCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1989ContagemResultadoQA_UserPesCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(10) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(10);
      }

      protected void gxLoad_12( int A1989ContagemResultadoQA_UserPesCod )
      {
         /* Using cursor T004Y13 */
         pr_default.execute(11, new Object[] {n1989ContagemResultadoQA_UserPesCod, A1989ContagemResultadoQA_UserPesCod});
         if ( (pr_default.getStatus(11) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1990ContagemResultadoQA_UserPesNom = T004Y13_A1990ContagemResultadoQA_UserPesNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1990ContagemResultadoQA_UserPesNom", A1990ContagemResultadoQA_UserPesNom);
         n1990ContagemResultadoQA_UserPesNom = T004Y13_n1990ContagemResultadoQA_UserPesNom[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1990ContagemResultadoQA_UserPesNom))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(11) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(11);
      }

      protected void gxLoad_10( int A1992ContagemResultadoQA_ParaCod )
      {
         /* Using cursor T004Y14 */
         pr_default.execute(12, new Object[] {A1992ContagemResultadoQA_ParaCod});
         if ( (pr_default.getStatus(12) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado QA_Para'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOQA_PARACOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoQA_ParaCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1993ContagemResultadoQA_ParaPesCod = T004Y14_A1993ContagemResultadoQA_ParaPesCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1993ContagemResultadoQA_ParaPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1993ContagemResultadoQA_ParaPesCod), 6, 0)));
         n1993ContagemResultadoQA_ParaPesCod = T004Y14_n1993ContagemResultadoQA_ParaPesCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1993ContagemResultadoQA_ParaPesCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(12) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(12);
      }

      protected void gxLoad_13( int A1993ContagemResultadoQA_ParaPesCod )
      {
         /* Using cursor T004Y15 */
         pr_default.execute(13, new Object[] {n1993ContagemResultadoQA_ParaPesCod, A1993ContagemResultadoQA_ParaPesCod});
         if ( (pr_default.getStatus(13) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1994ContagemResultadoQA_ParaPesNom = T004Y15_A1994ContagemResultadoQA_ParaPesNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1994ContagemResultadoQA_ParaPesNom", A1994ContagemResultadoQA_ParaPesNom);
         n1994ContagemResultadoQA_ParaPesNom = T004Y15_n1994ContagemResultadoQA_ParaPesNom[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1994ContagemResultadoQA_ParaPesNom))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(13) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(13);
      }

      protected void gxLoad_11( int A1996ContagemResultadoQA_RespostaDe )
      {
         /* Using cursor T004Y16 */
         pr_default.execute(14, new Object[] {n1996ContagemResultadoQA_RespostaDe, A1996ContagemResultadoQA_RespostaDe});
         if ( (pr_default.getStatus(14) == 101) )
         {
            if ( ! ( (0==A1996ContagemResultadoQA_RespostaDe) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado QA_Resposta De'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOQA_RESPOSTADE");
               AnyError = 1;
               GX_FocusControl = edtContagemResultadoQA_RespostaDe_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(14) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(14);
      }

      protected void GetKey4Y220( )
      {
         /* Using cursor T004Y17 */
         pr_default.execute(15, new Object[] {A1984ContagemResultadoQA_Codigo});
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound220 = 1;
         }
         else
         {
            RcdFound220 = 0;
         }
         pr_default.close(15);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T004Y3 */
         pr_default.execute(1, new Object[] {A1984ContagemResultadoQA_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM4Y220( 7) ;
            RcdFound220 = 1;
            A1984ContagemResultadoQA_Codigo = T004Y3_A1984ContagemResultadoQA_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1984ContagemResultadoQA_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1984ContagemResultadoQA_Codigo), 6, 0)));
            A1986ContagemResultadoQA_DataHora = T004Y3_A1986ContagemResultadoQA_DataHora[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1986ContagemResultadoQA_DataHora", context.localUtil.TToC( A1986ContagemResultadoQA_DataHora, 8, 5, 0, 3, "/", ":", " "));
            A1987ContagemResultadoQA_Texto = T004Y3_A1987ContagemResultadoQA_Texto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1987ContagemResultadoQA_Texto", A1987ContagemResultadoQA_Texto);
            A1985ContagemResultadoQA_OSCod = T004Y3_A1985ContagemResultadoQA_OSCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1985ContagemResultadoQA_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1985ContagemResultadoQA_OSCod), 6, 0)));
            A1988ContagemResultadoQA_UserCod = T004Y3_A1988ContagemResultadoQA_UserCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1988ContagemResultadoQA_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1988ContagemResultadoQA_UserCod), 6, 0)));
            A1992ContagemResultadoQA_ParaCod = T004Y3_A1992ContagemResultadoQA_ParaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1992ContagemResultadoQA_ParaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1992ContagemResultadoQA_ParaCod), 6, 0)));
            A1996ContagemResultadoQA_RespostaDe = T004Y3_A1996ContagemResultadoQA_RespostaDe[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1996ContagemResultadoQA_RespostaDe", StringUtil.LTrim( StringUtil.Str( (decimal)(A1996ContagemResultadoQA_RespostaDe), 6, 0)));
            n1996ContagemResultadoQA_RespostaDe = T004Y3_n1996ContagemResultadoQA_RespostaDe[0];
            Z1984ContagemResultadoQA_Codigo = A1984ContagemResultadoQA_Codigo;
            sMode220 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load4Y220( ) ;
            if ( AnyError == 1 )
            {
               RcdFound220 = 0;
               InitializeNonKey4Y220( ) ;
            }
            Gx_mode = sMode220;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound220 = 0;
            InitializeNonKey4Y220( ) ;
            sMode220 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode220;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey4Y220( ) ;
         if ( RcdFound220 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound220 = 0;
         /* Using cursor T004Y18 */
         pr_default.execute(16, new Object[] {A1984ContagemResultadoQA_Codigo});
         if ( (pr_default.getStatus(16) != 101) )
         {
            while ( (pr_default.getStatus(16) != 101) && ( ( T004Y18_A1984ContagemResultadoQA_Codigo[0] < A1984ContagemResultadoQA_Codigo ) ) )
            {
               pr_default.readNext(16);
            }
            if ( (pr_default.getStatus(16) != 101) && ( ( T004Y18_A1984ContagemResultadoQA_Codigo[0] > A1984ContagemResultadoQA_Codigo ) ) )
            {
               A1984ContagemResultadoQA_Codigo = T004Y18_A1984ContagemResultadoQA_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1984ContagemResultadoQA_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1984ContagemResultadoQA_Codigo), 6, 0)));
               RcdFound220 = 1;
            }
         }
         pr_default.close(16);
      }

      protected void move_previous( )
      {
         RcdFound220 = 0;
         /* Using cursor T004Y19 */
         pr_default.execute(17, new Object[] {A1984ContagemResultadoQA_Codigo});
         if ( (pr_default.getStatus(17) != 101) )
         {
            while ( (pr_default.getStatus(17) != 101) && ( ( T004Y19_A1984ContagemResultadoQA_Codigo[0] > A1984ContagemResultadoQA_Codigo ) ) )
            {
               pr_default.readNext(17);
            }
            if ( (pr_default.getStatus(17) != 101) && ( ( T004Y19_A1984ContagemResultadoQA_Codigo[0] < A1984ContagemResultadoQA_Codigo ) ) )
            {
               A1984ContagemResultadoQA_Codigo = T004Y19_A1984ContagemResultadoQA_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1984ContagemResultadoQA_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1984ContagemResultadoQA_Codigo), 6, 0)));
               RcdFound220 = 1;
            }
         }
         pr_default.close(17);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey4Y220( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtContagemResultadoQA_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert4Y220( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound220 == 1 )
            {
               if ( A1984ContagemResultadoQA_Codigo != Z1984ContagemResultadoQA_Codigo )
               {
                  A1984ContagemResultadoQA_Codigo = Z1984ContagemResultadoQA_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1984ContagemResultadoQA_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1984ContagemResultadoQA_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CONTAGEMRESULTADOQA_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoQA_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtContagemResultadoQA_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update4Y220( ) ;
                  GX_FocusControl = edtContagemResultadoQA_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1984ContagemResultadoQA_Codigo != Z1984ContagemResultadoQA_Codigo )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtContagemResultadoQA_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert4Y220( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTAGEMRESULTADOQA_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtContagemResultadoQA_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtContagemResultadoQA_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert4Y220( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A1984ContagemResultadoQA_Codigo != Z1984ContagemResultadoQA_Codigo )
         {
            A1984ContagemResultadoQA_Codigo = Z1984ContagemResultadoQA_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1984ContagemResultadoQA_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1984ContagemResultadoQA_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CONTAGEMRESULTADOQA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoQA_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtContagemResultadoQA_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound220 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "CONTAGEMRESULTADOQA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoQA_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtContagemResultadoQA_OSCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart4Y220( ) ;
         if ( RcdFound220 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultadoQA_OSCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd4Y220( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound220 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultadoQA_OSCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound220 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultadoQA_OSCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart4Y220( ) ;
         if ( RcdFound220 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound220 != 0 )
            {
               ScanNext4Y220( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultadoQA_OSCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd4Y220( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency4Y220( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T004Y2 */
            pr_default.execute(0, new Object[] {A1984ContagemResultadoQA_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoQA"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z1986ContagemResultadoQA_DataHora != T004Y2_A1986ContagemResultadoQA_DataHora[0] ) || ( Z1985ContagemResultadoQA_OSCod != T004Y2_A1985ContagemResultadoQA_OSCod[0] ) || ( Z1988ContagemResultadoQA_UserCod != T004Y2_A1988ContagemResultadoQA_UserCod[0] ) || ( Z1992ContagemResultadoQA_ParaCod != T004Y2_A1992ContagemResultadoQA_ParaCod[0] ) || ( Z1996ContagemResultadoQA_RespostaDe != T004Y2_A1996ContagemResultadoQA_RespostaDe[0] ) )
            {
               if ( Z1986ContagemResultadoQA_DataHora != T004Y2_A1986ContagemResultadoQA_DataHora[0] )
               {
                  GXUtil.WriteLog("contagemresultadoqa:[seudo value changed for attri]"+"ContagemResultadoQA_DataHora");
                  GXUtil.WriteLogRaw("Old: ",Z1986ContagemResultadoQA_DataHora);
                  GXUtil.WriteLogRaw("Current: ",T004Y2_A1986ContagemResultadoQA_DataHora[0]);
               }
               if ( Z1985ContagemResultadoQA_OSCod != T004Y2_A1985ContagemResultadoQA_OSCod[0] )
               {
                  GXUtil.WriteLog("contagemresultadoqa:[seudo value changed for attri]"+"ContagemResultadoQA_OSCod");
                  GXUtil.WriteLogRaw("Old: ",Z1985ContagemResultadoQA_OSCod);
                  GXUtil.WriteLogRaw("Current: ",T004Y2_A1985ContagemResultadoQA_OSCod[0]);
               }
               if ( Z1988ContagemResultadoQA_UserCod != T004Y2_A1988ContagemResultadoQA_UserCod[0] )
               {
                  GXUtil.WriteLog("contagemresultadoqa:[seudo value changed for attri]"+"ContagemResultadoQA_UserCod");
                  GXUtil.WriteLogRaw("Old: ",Z1988ContagemResultadoQA_UserCod);
                  GXUtil.WriteLogRaw("Current: ",T004Y2_A1988ContagemResultadoQA_UserCod[0]);
               }
               if ( Z1992ContagemResultadoQA_ParaCod != T004Y2_A1992ContagemResultadoQA_ParaCod[0] )
               {
                  GXUtil.WriteLog("contagemresultadoqa:[seudo value changed for attri]"+"ContagemResultadoQA_ParaCod");
                  GXUtil.WriteLogRaw("Old: ",Z1992ContagemResultadoQA_ParaCod);
                  GXUtil.WriteLogRaw("Current: ",T004Y2_A1992ContagemResultadoQA_ParaCod[0]);
               }
               if ( Z1996ContagemResultadoQA_RespostaDe != T004Y2_A1996ContagemResultadoQA_RespostaDe[0] )
               {
                  GXUtil.WriteLog("contagemresultadoqa:[seudo value changed for attri]"+"ContagemResultadoQA_RespostaDe");
                  GXUtil.WriteLogRaw("Old: ",Z1996ContagemResultadoQA_RespostaDe);
                  GXUtil.WriteLogRaw("Current: ",T004Y2_A1996ContagemResultadoQA_RespostaDe[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContagemResultadoQA"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert4Y220( )
      {
         BeforeValidate4Y220( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4Y220( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM4Y220( 0) ;
            CheckOptimisticConcurrency4Y220( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4Y220( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert4Y220( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004Y20 */
                     pr_default.execute(18, new Object[] {A1986ContagemResultadoQA_DataHora, A1987ContagemResultadoQA_Texto, A1985ContagemResultadoQA_OSCod, A1988ContagemResultadoQA_UserCod, A1992ContagemResultadoQA_ParaCod, n1996ContagemResultadoQA_RespostaDe, A1996ContagemResultadoQA_RespostaDe});
                     A1984ContagemResultadoQA_Codigo = T004Y20_A1984ContagemResultadoQA_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1984ContagemResultadoQA_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1984ContagemResultadoQA_Codigo), 6, 0)));
                     pr_default.close(18);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoQA") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption4Y0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load4Y220( ) ;
            }
            EndLevel4Y220( ) ;
         }
         CloseExtendedTableCursors4Y220( ) ;
      }

      protected void Update4Y220( )
      {
         BeforeValidate4Y220( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4Y220( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4Y220( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4Y220( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate4Y220( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004Y21 */
                     pr_default.execute(19, new Object[] {A1986ContagemResultadoQA_DataHora, A1987ContagemResultadoQA_Texto, A1985ContagemResultadoQA_OSCod, A1988ContagemResultadoQA_UserCod, A1992ContagemResultadoQA_ParaCod, n1996ContagemResultadoQA_RespostaDe, A1996ContagemResultadoQA_RespostaDe, A1984ContagemResultadoQA_Codigo});
                     pr_default.close(19);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoQA") ;
                     if ( (pr_default.getStatus(19) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoQA"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate4Y220( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption4Y0( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel4Y220( ) ;
         }
         CloseExtendedTableCursors4Y220( ) ;
      }

      protected void DeferredUpdate4Y220( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate4Y220( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4Y220( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls4Y220( ) ;
            AfterConfirm4Y220( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete4Y220( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T004Y22 */
                  pr_default.execute(20, new Object[] {A1984ContagemResultadoQA_Codigo});
                  pr_default.close(20);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoQA") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound220 == 0 )
                        {
                           InitAll4Y220( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption4Y0( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode220 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel4Y220( ) ;
         Gx_mode = sMode220;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls4Y220( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            GXt_int1 = A1998ContagemResultadoQA_Resposta;
            new prc_qa_resposta(context ).execute( ref  A1984ContagemResultadoQA_Codigo, out  GXt_int1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1984ContagemResultadoQA_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1984ContagemResultadoQA_Codigo), 6, 0)));
            A1998ContagemResultadoQA_Resposta = GXt_int1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1998ContagemResultadoQA_Resposta", StringUtil.LTrim( StringUtil.Str( (decimal)(A1998ContagemResultadoQA_Resposta), 6, 0)));
            /* Using cursor T004Y23 */
            pr_default.execute(21, new Object[] {A1985ContagemResultadoQA_OSCod});
            A493ContagemResultado_DemandaFM = T004Y23_A493ContagemResultado_DemandaFM[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A493ContagemResultado_DemandaFM", A493ContagemResultado_DemandaFM);
            n493ContagemResultado_DemandaFM = T004Y23_n493ContagemResultado_DemandaFM[0];
            pr_default.close(21);
            /* Using cursor T004Y24 */
            pr_default.execute(22, new Object[] {A1988ContagemResultadoQA_UserCod});
            A1989ContagemResultadoQA_UserPesCod = T004Y24_A1989ContagemResultadoQA_UserPesCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1989ContagemResultadoQA_UserPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1989ContagemResultadoQA_UserPesCod), 6, 0)));
            n1989ContagemResultadoQA_UserPesCod = T004Y24_n1989ContagemResultadoQA_UserPesCod[0];
            pr_default.close(22);
            /* Using cursor T004Y25 */
            pr_default.execute(23, new Object[] {n1989ContagemResultadoQA_UserPesCod, A1989ContagemResultadoQA_UserPesCod});
            A1990ContagemResultadoQA_UserPesNom = T004Y25_A1990ContagemResultadoQA_UserPesNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1990ContagemResultadoQA_UserPesNom", A1990ContagemResultadoQA_UserPesNom);
            n1990ContagemResultadoQA_UserPesNom = T004Y25_n1990ContagemResultadoQA_UserPesNom[0];
            pr_default.close(23);
            GXt_int1 = A1991ContagemResultadoQA_ContratanteDoUser;
            new prc_contratantedousuario(context ).execute( ref  A1985ContagemResultadoQA_OSCod, ref  A1988ContagemResultadoQA_UserCod, out  GXt_int1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1985ContagemResultadoQA_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1985ContagemResultadoQA_OSCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1988ContagemResultadoQA_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1988ContagemResultadoQA_UserCod), 6, 0)));
            A1991ContagemResultadoQA_ContratanteDoUser = GXt_int1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1991ContagemResultadoQA_ContratanteDoUser", StringUtil.LTrim( StringUtil.Str( (decimal)(A1991ContagemResultadoQA_ContratanteDoUser), 6, 0)));
            /* Using cursor T004Y26 */
            pr_default.execute(24, new Object[] {A1992ContagemResultadoQA_ParaCod});
            A1993ContagemResultadoQA_ParaPesCod = T004Y26_A1993ContagemResultadoQA_ParaPesCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1993ContagemResultadoQA_ParaPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1993ContagemResultadoQA_ParaPesCod), 6, 0)));
            n1993ContagemResultadoQA_ParaPesCod = T004Y26_n1993ContagemResultadoQA_ParaPesCod[0];
            pr_default.close(24);
            /* Using cursor T004Y27 */
            pr_default.execute(25, new Object[] {n1993ContagemResultadoQA_ParaPesCod, A1993ContagemResultadoQA_ParaPesCod});
            A1994ContagemResultadoQA_ParaPesNom = T004Y27_A1994ContagemResultadoQA_ParaPesNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1994ContagemResultadoQA_ParaPesNom", A1994ContagemResultadoQA_ParaPesNom);
            n1994ContagemResultadoQA_ParaPesNom = T004Y27_n1994ContagemResultadoQA_ParaPesNom[0];
            pr_default.close(25);
            GXt_int1 = A1995ContagemResultadoQA_ContratanteDoPara;
            new prc_contratantedousuario(context ).execute( ref  A1985ContagemResultadoQA_OSCod, ref  A1992ContagemResultadoQA_ParaCod, out  GXt_int1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1985ContagemResultadoQA_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1985ContagemResultadoQA_OSCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1992ContagemResultadoQA_ParaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1992ContagemResultadoQA_ParaCod), 6, 0)));
            A1995ContagemResultadoQA_ContratanteDoPara = GXt_int1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1995ContagemResultadoQA_ContratanteDoPara", StringUtil.LTrim( StringUtil.Str( (decimal)(A1995ContagemResultadoQA_ContratanteDoPara), 6, 0)));
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T004Y28 */
            pr_default.execute(26, new Object[] {A1984ContagemResultadoQA_Codigo});
            if ( (pr_default.getStatus(26) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado QA"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(26);
         }
      }

      protected void EndLevel4Y220( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete4Y220( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(21);
            pr_default.close(22);
            pr_default.close(24);
            pr_default.close(23);
            pr_default.close(25);
            context.CommitDataStores( "ContagemResultadoQA");
            if ( AnyError == 0 )
            {
               ConfirmValues4Y0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(21);
            pr_default.close(22);
            pr_default.close(24);
            pr_default.close(23);
            pr_default.close(25);
            context.RollbackDataStores( "ContagemResultadoQA");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart4Y220( )
      {
         /* Using cursor T004Y29 */
         pr_default.execute(27);
         RcdFound220 = 0;
         if ( (pr_default.getStatus(27) != 101) )
         {
            RcdFound220 = 1;
            A1984ContagemResultadoQA_Codigo = T004Y29_A1984ContagemResultadoQA_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1984ContagemResultadoQA_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1984ContagemResultadoQA_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext4Y220( )
      {
         /* Scan next routine */
         pr_default.readNext(27);
         RcdFound220 = 0;
         if ( (pr_default.getStatus(27) != 101) )
         {
            RcdFound220 = 1;
            A1984ContagemResultadoQA_Codigo = T004Y29_A1984ContagemResultadoQA_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1984ContagemResultadoQA_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1984ContagemResultadoQA_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd4Y220( )
      {
         pr_default.close(27);
      }

      protected void AfterConfirm4Y220( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert4Y220( )
      {
         /* Before Insert Rules */
         A1986ContagemResultadoQA_DataHora = DateTimeUtil.ServerNow( context, "DEFAULT");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1986ContagemResultadoQA_DataHora", context.localUtil.TToC( A1986ContagemResultadoQA_DataHora, 8, 5, 0, 3, "/", ":", " "));
      }

      protected void BeforeUpdate4Y220( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete4Y220( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete4Y220( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate4Y220( )
      {
         /* Before Validate Rules */
         if ( (0==A1996ContagemResultadoQA_RespostaDe) )
         {
            A1996ContagemResultadoQA_RespostaDe = 0;
            n1996ContagemResultadoQA_RespostaDe = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1996ContagemResultadoQA_RespostaDe", StringUtil.LTrim( StringUtil.Str( (decimal)(A1996ContagemResultadoQA_RespostaDe), 6, 0)));
            n1996ContagemResultadoQA_RespostaDe = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1996ContagemResultadoQA_RespostaDe", StringUtil.LTrim( StringUtil.Str( (decimal)(A1996ContagemResultadoQA_RespostaDe), 6, 0)));
         }
      }

      protected void DisableAttributes4Y220( )
      {
         edtContagemResultadoQA_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoQA_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoQA_Codigo_Enabled), 5, 0)));
         edtContagemResultadoQA_OSCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoQA_OSCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoQA_OSCod_Enabled), 5, 0)));
         edtContagemResultado_DemandaFM_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_DemandaFM_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_DemandaFM_Enabled), 5, 0)));
         edtContagemResultadoQA_DataHora_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoQA_DataHora_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoQA_DataHora_Enabled), 5, 0)));
         edtContagemResultadoQA_Texto_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoQA_Texto_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoQA_Texto_Enabled), 5, 0)));
         edtContagemResultadoQA_UserCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoQA_UserCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoQA_UserCod_Enabled), 5, 0)));
         edtContagemResultadoQA_UserPesCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoQA_UserPesCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoQA_UserPesCod_Enabled), 5, 0)));
         edtContagemResultadoQA_UserPesNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoQA_UserPesNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoQA_UserPesNom_Enabled), 5, 0)));
         edtContagemResultadoQA_ContratanteDoUser_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoQA_ContratanteDoUser_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoQA_ContratanteDoUser_Enabled), 5, 0)));
         edtContagemResultadoQA_ParaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoQA_ParaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoQA_ParaCod_Enabled), 5, 0)));
         edtContagemResultadoQA_ParaPesCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoQA_ParaPesCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoQA_ParaPesCod_Enabled), 5, 0)));
         edtContagemResultadoQA_ParaPesNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoQA_ParaPesNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoQA_ParaPesNom_Enabled), 5, 0)));
         edtContagemResultadoQA_ContratanteDoPara_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoQA_ContratanteDoPara_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoQA_ContratanteDoPara_Enabled), 5, 0)));
         edtContagemResultadoQA_RespostaDe_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoQA_RespostaDe_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoQA_RespostaDe_Enabled), 5, 0)));
         edtContagemResultadoQA_Resposta_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoQA_Resposta_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoQA_Resposta_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues4Y0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206216175871");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contagemresultadoqa.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1984ContagemResultadoQA_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1984ContagemResultadoQA_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1986ContagemResultadoQA_DataHora", context.localUtil.TToC( Z1986ContagemResultadoQA_DataHora, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "Z1985ContagemResultadoQA_OSCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1985ContagemResultadoQA_OSCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1988ContagemResultadoQA_UserCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1988ContagemResultadoQA_UserCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1992ContagemResultadoQA_ParaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1992ContagemResultadoQA_ParaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1996ContagemResultadoQA_RespostaDe", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1996ContagemResultadoQA_RespostaDe), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contagemresultadoqa.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "ContagemResultadoQA" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contagem Resultado QA" ;
      }

      protected void InitializeNonKey4Y220( )
      {
         A1986ContagemResultadoQA_DataHora = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1986ContagemResultadoQA_DataHora", context.localUtil.TToC( A1986ContagemResultadoQA_DataHora, 8, 5, 0, 3, "/", ":", " "));
         A1991ContagemResultadoQA_ContratanteDoUser = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1991ContagemResultadoQA_ContratanteDoUser", StringUtil.LTrim( StringUtil.Str( (decimal)(A1991ContagemResultadoQA_ContratanteDoUser), 6, 0)));
         A1995ContagemResultadoQA_ContratanteDoPara = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1995ContagemResultadoQA_ContratanteDoPara", StringUtil.LTrim( StringUtil.Str( (decimal)(A1995ContagemResultadoQA_ContratanteDoPara), 6, 0)));
         A1998ContagemResultadoQA_Resposta = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1998ContagemResultadoQA_Resposta", StringUtil.LTrim( StringUtil.Str( (decimal)(A1998ContagemResultadoQA_Resposta), 6, 0)));
         A1985ContagemResultadoQA_OSCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1985ContagemResultadoQA_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1985ContagemResultadoQA_OSCod), 6, 0)));
         A493ContagemResultado_DemandaFM = "";
         n493ContagemResultado_DemandaFM = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A493ContagemResultado_DemandaFM", A493ContagemResultado_DemandaFM);
         A1987ContagemResultadoQA_Texto = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1987ContagemResultadoQA_Texto", A1987ContagemResultadoQA_Texto);
         A1988ContagemResultadoQA_UserCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1988ContagemResultadoQA_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1988ContagemResultadoQA_UserCod), 6, 0)));
         A1989ContagemResultadoQA_UserPesCod = 0;
         n1989ContagemResultadoQA_UserPesCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1989ContagemResultadoQA_UserPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1989ContagemResultadoQA_UserPesCod), 6, 0)));
         A1990ContagemResultadoQA_UserPesNom = "";
         n1990ContagemResultadoQA_UserPesNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1990ContagemResultadoQA_UserPesNom", A1990ContagemResultadoQA_UserPesNom);
         A1992ContagemResultadoQA_ParaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1992ContagemResultadoQA_ParaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1992ContagemResultadoQA_ParaCod), 6, 0)));
         A1993ContagemResultadoQA_ParaPesCod = 0;
         n1993ContagemResultadoQA_ParaPesCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1993ContagemResultadoQA_ParaPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1993ContagemResultadoQA_ParaPesCod), 6, 0)));
         A1994ContagemResultadoQA_ParaPesNom = "";
         n1994ContagemResultadoQA_ParaPesNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1994ContagemResultadoQA_ParaPesNom", A1994ContagemResultadoQA_ParaPesNom);
         A1996ContagemResultadoQA_RespostaDe = 0;
         n1996ContagemResultadoQA_RespostaDe = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1996ContagemResultadoQA_RespostaDe", StringUtil.LTrim( StringUtil.Str( (decimal)(A1996ContagemResultadoQA_RespostaDe), 6, 0)));
         n1996ContagemResultadoQA_RespostaDe = ((0==A1996ContagemResultadoQA_RespostaDe) ? true : false);
         Z1986ContagemResultadoQA_DataHora = (DateTime)(DateTime.MinValue);
         Z1985ContagemResultadoQA_OSCod = 0;
         Z1988ContagemResultadoQA_UserCod = 0;
         Z1992ContagemResultadoQA_ParaCod = 0;
         Z1996ContagemResultadoQA_RespostaDe = 0;
      }

      protected void InitAll4Y220( )
      {
         A1984ContagemResultadoQA_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1984ContagemResultadoQA_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1984ContagemResultadoQA_Codigo), 6, 0)));
         InitializeNonKey4Y220( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20206216175881");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contagemresultadoqa.js", "?20206216175882");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblockcontagemresultadoqa_codigo_Internalname = "TEXTBLOCKCONTAGEMRESULTADOQA_CODIGO";
         edtContagemResultadoQA_Codigo_Internalname = "CONTAGEMRESULTADOQA_CODIGO";
         lblTextblockcontagemresultadoqa_oscod_Internalname = "TEXTBLOCKCONTAGEMRESULTADOQA_OSCOD";
         edtContagemResultadoQA_OSCod_Internalname = "CONTAGEMRESULTADOQA_OSCOD";
         lblTextblockcontagemresultado_demandafm_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_DEMANDAFM";
         edtContagemResultado_DemandaFM_Internalname = "CONTAGEMRESULTADO_DEMANDAFM";
         lblTextblockcontagemresultadoqa_datahora_Internalname = "TEXTBLOCKCONTAGEMRESULTADOQA_DATAHORA";
         edtContagemResultadoQA_DataHora_Internalname = "CONTAGEMRESULTADOQA_DATAHORA";
         lblTextblockcontagemresultadoqa_texto_Internalname = "TEXTBLOCKCONTAGEMRESULTADOQA_TEXTO";
         edtContagemResultadoQA_Texto_Internalname = "CONTAGEMRESULTADOQA_TEXTO";
         lblTextblockcontagemresultadoqa_usercod_Internalname = "TEXTBLOCKCONTAGEMRESULTADOQA_USERCOD";
         edtContagemResultadoQA_UserCod_Internalname = "CONTAGEMRESULTADOQA_USERCOD";
         lblTextblockcontagemresultadoqa_userpescod_Internalname = "TEXTBLOCKCONTAGEMRESULTADOQA_USERPESCOD";
         edtContagemResultadoQA_UserPesCod_Internalname = "CONTAGEMRESULTADOQA_USERPESCOD";
         lblTextblockcontagemresultadoqa_userpesnom_Internalname = "TEXTBLOCKCONTAGEMRESULTADOQA_USERPESNOM";
         edtContagemResultadoQA_UserPesNom_Internalname = "CONTAGEMRESULTADOQA_USERPESNOM";
         lblTextblockcontagemresultadoqa_contratantedouser_Internalname = "TEXTBLOCKCONTAGEMRESULTADOQA_CONTRATANTEDOUSER";
         edtContagemResultadoQA_ContratanteDoUser_Internalname = "CONTAGEMRESULTADOQA_CONTRATANTEDOUSER";
         lblTextblockcontagemresultadoqa_paracod_Internalname = "TEXTBLOCKCONTAGEMRESULTADOQA_PARACOD";
         edtContagemResultadoQA_ParaCod_Internalname = "CONTAGEMRESULTADOQA_PARACOD";
         lblTextblockcontagemresultadoqa_parapescod_Internalname = "TEXTBLOCKCONTAGEMRESULTADOQA_PARAPESCOD";
         edtContagemResultadoQA_ParaPesCod_Internalname = "CONTAGEMRESULTADOQA_PARAPESCOD";
         lblTextblockcontagemresultadoqa_parapesnom_Internalname = "TEXTBLOCKCONTAGEMRESULTADOQA_PARAPESNOM";
         edtContagemResultadoQA_ParaPesNom_Internalname = "CONTAGEMRESULTADOQA_PARAPESNOM";
         lblTextblockcontagemresultadoqa_contratantedopara_Internalname = "TEXTBLOCKCONTAGEMRESULTADOQA_CONTRATANTEDOPARA";
         edtContagemResultadoQA_ContratanteDoPara_Internalname = "CONTAGEMRESULTADOQA_CONTRATANTEDOPARA";
         lblTextblockcontagemresultadoqa_respostade_Internalname = "TEXTBLOCKCONTAGEMRESULTADOQA_RESPOSTADE";
         edtContagemResultadoQA_RespostaDe_Internalname = "CONTAGEMRESULTADOQA_RESPOSTADE";
         lblTextblockcontagemresultadoqa_resposta_Internalname = "TEXTBLOCKCONTAGEMRESULTADOQA_RESPOSTA";
         edtContagemResultadoQA_Resposta_Internalname = "CONTAGEMRESULTADOQA_RESPOSTA";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Contagem Resultado QA";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         edtContagemResultadoQA_Resposta_Jsonclick = "";
         edtContagemResultadoQA_Resposta_Enabled = 0;
         edtContagemResultadoQA_RespostaDe_Jsonclick = "";
         edtContagemResultadoQA_RespostaDe_Enabled = 1;
         edtContagemResultadoQA_ContratanteDoPara_Jsonclick = "";
         edtContagemResultadoQA_ContratanteDoPara_Enabled = 0;
         edtContagemResultadoQA_ParaPesNom_Jsonclick = "";
         edtContagemResultadoQA_ParaPesNom_Enabled = 0;
         edtContagemResultadoQA_ParaPesCod_Jsonclick = "";
         edtContagemResultadoQA_ParaPesCod_Enabled = 0;
         edtContagemResultadoQA_ParaCod_Jsonclick = "";
         edtContagemResultadoQA_ParaCod_Enabled = 1;
         edtContagemResultadoQA_ContratanteDoUser_Jsonclick = "";
         edtContagemResultadoQA_ContratanteDoUser_Enabled = 0;
         edtContagemResultadoQA_UserPesNom_Jsonclick = "";
         edtContagemResultadoQA_UserPesNom_Enabled = 0;
         edtContagemResultadoQA_UserPesCod_Jsonclick = "";
         edtContagemResultadoQA_UserPesCod_Enabled = 0;
         edtContagemResultadoQA_UserCod_Jsonclick = "";
         edtContagemResultadoQA_UserCod_Enabled = 1;
         edtContagemResultadoQA_Texto_Enabled = 1;
         edtContagemResultadoQA_DataHora_Jsonclick = "";
         edtContagemResultadoQA_DataHora_Enabled = 1;
         edtContagemResultado_DemandaFM_Jsonclick = "";
         edtContagemResultado_DemandaFM_Enabled = 0;
         edtContagemResultadoQA_OSCod_Jsonclick = "";
         edtContagemResultadoQA_OSCod_Enabled = 1;
         edtContagemResultadoQA_Codigo_Jsonclick = "";
         edtContagemResultadoQA_Codigo_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GX2ASACONTAGEMRESULTADOQA_RESPOSTA4Y220( int A1984ContagemResultadoQA_Codigo )
      {
         GXt_int1 = A1998ContagemResultadoQA_Resposta;
         new prc_qa_resposta(context ).execute( ref  A1984ContagemResultadoQA_Codigo, out  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1984ContagemResultadoQA_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1984ContagemResultadoQA_Codigo), 6, 0)));
         A1998ContagemResultadoQA_Resposta = GXt_int1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1998ContagemResultadoQA_Resposta", StringUtil.LTrim( StringUtil.Str( (decimal)(A1998ContagemResultadoQA_Resposta), 6, 0)));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1998ContagemResultadoQA_Resposta), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void GX3ASACONTAGEMRESULTADOQA_CONTRATANTEDOPARA4Y220( int A1985ContagemResultadoQA_OSCod ,
                                                                       int A1992ContagemResultadoQA_ParaCod )
      {
         GXt_int1 = A1995ContagemResultadoQA_ContratanteDoPara;
         new prc_contratantedousuario(context ).execute( ref  A1985ContagemResultadoQA_OSCod, ref  A1992ContagemResultadoQA_ParaCod, out  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1985ContagemResultadoQA_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1985ContagemResultadoQA_OSCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1992ContagemResultadoQA_ParaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1992ContagemResultadoQA_ParaCod), 6, 0)));
         A1995ContagemResultadoQA_ContratanteDoPara = GXt_int1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1995ContagemResultadoQA_ContratanteDoPara", StringUtil.LTrim( StringUtil.Str( (decimal)(A1995ContagemResultadoQA_ContratanteDoPara), 6, 0)));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1995ContagemResultadoQA_ContratanteDoPara), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void GX4ASACONTAGEMRESULTADOQA_CONTRATANTEDOUSER4Y220( int A1985ContagemResultadoQA_OSCod ,
                                                                       int A1988ContagemResultadoQA_UserCod )
      {
         GXt_int1 = A1991ContagemResultadoQA_ContratanteDoUser;
         new prc_contratantedousuario(context ).execute( ref  A1985ContagemResultadoQA_OSCod, ref  A1988ContagemResultadoQA_UserCod, out  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1985ContagemResultadoQA_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1985ContagemResultadoQA_OSCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1988ContagemResultadoQA_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1988ContagemResultadoQA_UserCod), 6, 0)));
         A1991ContagemResultadoQA_ContratanteDoUser = GXt_int1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1991ContagemResultadoQA_ContratanteDoUser", StringUtil.LTrim( StringUtil.Str( (decimal)(A1991ContagemResultadoQA_ContratanteDoUser), 6, 0)));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1991ContagemResultadoQA_ContratanteDoUser), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         GX_FocusControl = edtContagemResultadoQA_OSCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Contagemresultadoqa_codigo( int GX_Parm1 ,
                                                    DateTime GX_Parm2 ,
                                                    String GX_Parm3 ,
                                                    int GX_Parm4 ,
                                                    int GX_Parm5 ,
                                                    int GX_Parm6 ,
                                                    int GX_Parm7 ,
                                                    int GX_Parm8 )
      {
         A1984ContagemResultadoQA_Codigo = GX_Parm1;
         A1986ContagemResultadoQA_DataHora = GX_Parm2;
         A1987ContagemResultadoQA_Texto = GX_Parm3;
         A1985ContagemResultadoQA_OSCod = GX_Parm4;
         A1988ContagemResultadoQA_UserCod = GX_Parm5;
         A1992ContagemResultadoQA_ParaCod = GX_Parm6;
         A1996ContagemResultadoQA_RespostaDe = GX_Parm7;
         n1996ContagemResultadoQA_RespostaDe = false;
         A1998ContagemResultadoQA_Resposta = GX_Parm8;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         GXt_int1 = A1998ContagemResultadoQA_Resposta;
         new prc_qa_resposta(context ).execute( ref  A1984ContagemResultadoQA_Codigo, out  GXt_int1) ;
         A1998ContagemResultadoQA_Resposta = GXt_int1;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A493ContagemResultado_DemandaFM = "";
            n493ContagemResultado_DemandaFM = false;
            A1989ContagemResultadoQA_UserPesCod = 0;
            n1989ContagemResultadoQA_UserPesCod = false;
            A1990ContagemResultadoQA_UserPesNom = "";
            n1990ContagemResultadoQA_UserPesNom = false;
            A1993ContagemResultadoQA_ParaPesCod = 0;
            n1993ContagemResultadoQA_ParaPesCod = false;
            A1994ContagemResultadoQA_ParaPesNom = "";
            n1994ContagemResultadoQA_ParaPesNom = false;
         }
         isValidOutput.Add(context.localUtil.TToC( A1986ContagemResultadoQA_DataHora, 10, 8, 0, 3, "/", ":", " "));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1985ContagemResultadoQA_OSCod), 6, 0, ".", "")));
         isValidOutput.Add(A1987ContagemResultadoQA_Texto);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1988ContagemResultadoQA_UserCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1992ContagemResultadoQA_ParaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1996ContagemResultadoQA_RespostaDe), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1998ContagemResultadoQA_Resposta), 6, 0, ".", "")));
         isValidOutput.Add(A493ContagemResultado_DemandaFM);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1995ContagemResultadoQA_ContratanteDoPara), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1991ContagemResultadoQA_ContratanteDoUser), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1989ContagemResultadoQA_UserPesCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A1990ContagemResultadoQA_UserPesNom));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1993ContagemResultadoQA_ParaPesCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A1994ContagemResultadoQA_ParaPesNom));
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1984ContagemResultadoQA_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(context.localUtil.TToC( Z1986ContagemResultadoQA_DataHora, 10, 8, 0, 0, "/", ":", " "));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1985ContagemResultadoQA_OSCod), 6, 0, ",", "")));
         isValidOutput.Add(Z1987ContagemResultadoQA_Texto);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1988ContagemResultadoQA_UserCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1992ContagemResultadoQA_ParaCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1996ContagemResultadoQA_RespostaDe), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1998ContagemResultadoQA_Resposta), 6, 0, ".", "")));
         isValidOutput.Add(Z493ContagemResultado_DemandaFM);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1995ContagemResultadoQA_ContratanteDoPara), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1991ContagemResultadoQA_ContratanteDoUser), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1989ContagemResultadoQA_UserPesCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( Z1990ContagemResultadoQA_UserPesNom));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1993ContagemResultadoQA_ParaPesCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( Z1994ContagemResultadoQA_ParaPesNom));
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemresultadoqa_oscod( int GX_Parm1 ,
                                                   String GX_Parm2 )
      {
         A1985ContagemResultadoQA_OSCod = GX_Parm1;
         A493ContagemResultado_DemandaFM = GX_Parm2;
         n493ContagemResultado_DemandaFM = false;
         /* Using cursor T004Y23 */
         pr_default.execute(21, new Object[] {A1985ContagemResultadoQA_OSCod});
         if ( (pr_default.getStatus(21) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado QA_OSCod'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOQA_OSCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoQA_OSCod_Internalname;
         }
         A493ContagemResultado_DemandaFM = T004Y23_A493ContagemResultado_DemandaFM[0];
         n493ContagemResultado_DemandaFM = T004Y23_n493ContagemResultado_DemandaFM[0];
         pr_default.close(21);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A493ContagemResultado_DemandaFM = "";
            n493ContagemResultado_DemandaFM = false;
         }
         isValidOutput.Add(A493ContagemResultado_DemandaFM);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemresultadoqa_usercod( int GX_Parm1 ,
                                                     int GX_Parm2 ,
                                                     int GX_Parm3 ,
                                                     String GX_Parm4 ,
                                                     int GX_Parm5 )
      {
         A1988ContagemResultadoQA_UserCod = GX_Parm1;
         A1989ContagemResultadoQA_UserPesCod = GX_Parm2;
         n1989ContagemResultadoQA_UserPesCod = false;
         A1985ContagemResultadoQA_OSCod = GX_Parm3;
         A1990ContagemResultadoQA_UserPesNom = GX_Parm4;
         n1990ContagemResultadoQA_UserPesNom = false;
         A1991ContagemResultadoQA_ContratanteDoUser = GX_Parm5;
         /* Using cursor T004Y24 */
         pr_default.execute(22, new Object[] {A1988ContagemResultadoQA_UserCod});
         if ( (pr_default.getStatus(22) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado QA_Usuario'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOQA_USERCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoQA_UserCod_Internalname;
         }
         A1989ContagemResultadoQA_UserPesCod = T004Y24_A1989ContagemResultadoQA_UserPesCod[0];
         n1989ContagemResultadoQA_UserPesCod = T004Y24_n1989ContagemResultadoQA_UserPesCod[0];
         pr_default.close(22);
         /* Using cursor T004Y25 */
         pr_default.execute(23, new Object[] {n1989ContagemResultadoQA_UserPesCod, A1989ContagemResultadoQA_UserPesCod});
         if ( (pr_default.getStatus(23) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1990ContagemResultadoQA_UserPesNom = T004Y25_A1990ContagemResultadoQA_UserPesNom[0];
         n1990ContagemResultadoQA_UserPesNom = T004Y25_n1990ContagemResultadoQA_UserPesNom[0];
         pr_default.close(23);
         GXt_int1 = A1991ContagemResultadoQA_ContratanteDoUser;
         new prc_contratantedousuario(context ).execute( ref  A1985ContagemResultadoQA_OSCod, ref  A1988ContagemResultadoQA_UserCod, out  GXt_int1) ;
         A1991ContagemResultadoQA_ContratanteDoUser = GXt_int1;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1989ContagemResultadoQA_UserPesCod = 0;
            n1989ContagemResultadoQA_UserPesCod = false;
            A1990ContagemResultadoQA_UserPesNom = "";
            n1990ContagemResultadoQA_UserPesNom = false;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1989ContagemResultadoQA_UserPesCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A1990ContagemResultadoQA_UserPesNom));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1991ContagemResultadoQA_ContratanteDoUser), 6, 0, ".", "")));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemresultadoqa_paracod( int GX_Parm1 ,
                                                     int GX_Parm2 ,
                                                     int GX_Parm3 ,
                                                     String GX_Parm4 ,
                                                     int GX_Parm5 )
      {
         A1992ContagemResultadoQA_ParaCod = GX_Parm1;
         A1993ContagemResultadoQA_ParaPesCod = GX_Parm2;
         n1993ContagemResultadoQA_ParaPesCod = false;
         A1985ContagemResultadoQA_OSCod = GX_Parm3;
         A1994ContagemResultadoQA_ParaPesNom = GX_Parm4;
         n1994ContagemResultadoQA_ParaPesNom = false;
         A1995ContagemResultadoQA_ContratanteDoPara = GX_Parm5;
         /* Using cursor T004Y26 */
         pr_default.execute(24, new Object[] {A1992ContagemResultadoQA_ParaCod});
         if ( (pr_default.getStatus(24) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado QA_Para'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOQA_PARACOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoQA_ParaCod_Internalname;
         }
         A1993ContagemResultadoQA_ParaPesCod = T004Y26_A1993ContagemResultadoQA_ParaPesCod[0];
         n1993ContagemResultadoQA_ParaPesCod = T004Y26_n1993ContagemResultadoQA_ParaPesCod[0];
         pr_default.close(24);
         /* Using cursor T004Y27 */
         pr_default.execute(25, new Object[] {n1993ContagemResultadoQA_ParaPesCod, A1993ContagemResultadoQA_ParaPesCod});
         if ( (pr_default.getStatus(25) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1994ContagemResultadoQA_ParaPesNom = T004Y27_A1994ContagemResultadoQA_ParaPesNom[0];
         n1994ContagemResultadoQA_ParaPesNom = T004Y27_n1994ContagemResultadoQA_ParaPesNom[0];
         pr_default.close(25);
         GXt_int1 = A1995ContagemResultadoQA_ContratanteDoPara;
         new prc_contratantedousuario(context ).execute( ref  A1985ContagemResultadoQA_OSCod, ref  A1992ContagemResultadoQA_ParaCod, out  GXt_int1) ;
         A1995ContagemResultadoQA_ContratanteDoPara = GXt_int1;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1993ContagemResultadoQA_ParaPesCod = 0;
            n1993ContagemResultadoQA_ParaPesCod = false;
            A1994ContagemResultadoQA_ParaPesNom = "";
            n1994ContagemResultadoQA_ParaPesNom = false;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1993ContagemResultadoQA_ParaPesCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A1994ContagemResultadoQA_ParaPesNom));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1995ContagemResultadoQA_ContratanteDoPara), 6, 0, ".", "")));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemresultadoqa_respostade( int GX_Parm1 )
      {
         A1996ContagemResultadoQA_RespostaDe = GX_Parm1;
         n1996ContagemResultadoQA_RespostaDe = false;
         /* Using cursor T004Y30 */
         pr_default.execute(28, new Object[] {n1996ContagemResultadoQA_RespostaDe, A1996ContagemResultadoQA_RespostaDe});
         if ( (pr_default.getStatus(28) == 101) )
         {
            if ( ! ( (0==A1996ContagemResultadoQA_RespostaDe) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado QA_Resposta De'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOQA_RESPOSTADE");
               AnyError = 1;
               GX_FocusControl = edtContagemResultadoQA_RespostaDe_Internalname;
            }
         }
         pr_default.close(28);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(21);
         pr_default.close(22);
         pr_default.close(24);
         pr_default.close(28);
         pr_default.close(23);
         pr_default.close(25);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z1986ContagemResultadoQA_DataHora = (DateTime)(DateTime.MinValue);
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblockcontagemresultadoqa_codigo_Jsonclick = "";
         lblTextblockcontagemresultadoqa_oscod_Jsonclick = "";
         lblTextblockcontagemresultado_demandafm_Jsonclick = "";
         A493ContagemResultado_DemandaFM = "";
         lblTextblockcontagemresultadoqa_datahora_Jsonclick = "";
         A1986ContagemResultadoQA_DataHora = (DateTime)(DateTime.MinValue);
         lblTextblockcontagemresultadoqa_texto_Jsonclick = "";
         A1987ContagemResultadoQA_Texto = "";
         lblTextblockcontagemresultadoqa_usercod_Jsonclick = "";
         lblTextblockcontagemresultadoqa_userpescod_Jsonclick = "";
         lblTextblockcontagemresultadoqa_userpesnom_Jsonclick = "";
         A1990ContagemResultadoQA_UserPesNom = "";
         lblTextblockcontagemresultadoqa_contratantedouser_Jsonclick = "";
         lblTextblockcontagemresultadoqa_paracod_Jsonclick = "";
         lblTextblockcontagemresultadoqa_parapescod_Jsonclick = "";
         lblTextblockcontagemresultadoqa_parapesnom_Jsonclick = "";
         A1994ContagemResultadoQA_ParaPesNom = "";
         lblTextblockcontagemresultadoqa_contratantedopara_Jsonclick = "";
         lblTextblockcontagemresultadoqa_respostade_Jsonclick = "";
         lblTextblockcontagemresultadoqa_resposta_Jsonclick = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         Z1987ContagemResultadoQA_Texto = "";
         Z493ContagemResultado_DemandaFM = "";
         Z1990ContagemResultadoQA_UserPesNom = "";
         Z1994ContagemResultadoQA_ParaPesNom = "";
         T004Y10_A1984ContagemResultadoQA_Codigo = new int[1] ;
         T004Y10_A1986ContagemResultadoQA_DataHora = new DateTime[] {DateTime.MinValue} ;
         T004Y10_A493ContagemResultado_DemandaFM = new String[] {""} ;
         T004Y10_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         T004Y10_A1987ContagemResultadoQA_Texto = new String[] {""} ;
         T004Y10_A1990ContagemResultadoQA_UserPesNom = new String[] {""} ;
         T004Y10_n1990ContagemResultadoQA_UserPesNom = new bool[] {false} ;
         T004Y10_A1994ContagemResultadoQA_ParaPesNom = new String[] {""} ;
         T004Y10_n1994ContagemResultadoQA_ParaPesNom = new bool[] {false} ;
         T004Y10_A1985ContagemResultadoQA_OSCod = new int[1] ;
         T004Y10_A1988ContagemResultadoQA_UserCod = new int[1] ;
         T004Y10_A1992ContagemResultadoQA_ParaCod = new int[1] ;
         T004Y10_A1996ContagemResultadoQA_RespostaDe = new int[1] ;
         T004Y10_n1996ContagemResultadoQA_RespostaDe = new bool[] {false} ;
         T004Y10_A1989ContagemResultadoQA_UserPesCod = new int[1] ;
         T004Y10_n1989ContagemResultadoQA_UserPesCod = new bool[] {false} ;
         T004Y10_A1993ContagemResultadoQA_ParaPesCod = new int[1] ;
         T004Y10_n1993ContagemResultadoQA_ParaPesCod = new bool[] {false} ;
         T004Y4_A493ContagemResultado_DemandaFM = new String[] {""} ;
         T004Y4_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         T004Y5_A1989ContagemResultadoQA_UserPesCod = new int[1] ;
         T004Y5_n1989ContagemResultadoQA_UserPesCod = new bool[] {false} ;
         T004Y8_A1990ContagemResultadoQA_UserPesNom = new String[] {""} ;
         T004Y8_n1990ContagemResultadoQA_UserPesNom = new bool[] {false} ;
         T004Y6_A1993ContagemResultadoQA_ParaPesCod = new int[1] ;
         T004Y6_n1993ContagemResultadoQA_ParaPesCod = new bool[] {false} ;
         T004Y9_A1994ContagemResultadoQA_ParaPesNom = new String[] {""} ;
         T004Y9_n1994ContagemResultadoQA_ParaPesNom = new bool[] {false} ;
         T004Y7_A1996ContagemResultadoQA_RespostaDe = new int[1] ;
         T004Y7_n1996ContagemResultadoQA_RespostaDe = new bool[] {false} ;
         T004Y11_A493ContagemResultado_DemandaFM = new String[] {""} ;
         T004Y11_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         T004Y12_A1989ContagemResultadoQA_UserPesCod = new int[1] ;
         T004Y12_n1989ContagemResultadoQA_UserPesCod = new bool[] {false} ;
         T004Y13_A1990ContagemResultadoQA_UserPesNom = new String[] {""} ;
         T004Y13_n1990ContagemResultadoQA_UserPesNom = new bool[] {false} ;
         T004Y14_A1993ContagemResultadoQA_ParaPesCod = new int[1] ;
         T004Y14_n1993ContagemResultadoQA_ParaPesCod = new bool[] {false} ;
         T004Y15_A1994ContagemResultadoQA_ParaPesNom = new String[] {""} ;
         T004Y15_n1994ContagemResultadoQA_ParaPesNom = new bool[] {false} ;
         T004Y16_A1996ContagemResultadoQA_RespostaDe = new int[1] ;
         T004Y16_n1996ContagemResultadoQA_RespostaDe = new bool[] {false} ;
         T004Y17_A1984ContagemResultadoQA_Codigo = new int[1] ;
         T004Y3_A1984ContagemResultadoQA_Codigo = new int[1] ;
         T004Y3_A1986ContagemResultadoQA_DataHora = new DateTime[] {DateTime.MinValue} ;
         T004Y3_A1987ContagemResultadoQA_Texto = new String[] {""} ;
         T004Y3_A1985ContagemResultadoQA_OSCod = new int[1] ;
         T004Y3_A1988ContagemResultadoQA_UserCod = new int[1] ;
         T004Y3_A1992ContagemResultadoQA_ParaCod = new int[1] ;
         T004Y3_A1996ContagemResultadoQA_RespostaDe = new int[1] ;
         T004Y3_n1996ContagemResultadoQA_RespostaDe = new bool[] {false} ;
         sMode220 = "";
         T004Y18_A1984ContagemResultadoQA_Codigo = new int[1] ;
         T004Y19_A1984ContagemResultadoQA_Codigo = new int[1] ;
         T004Y2_A1984ContagemResultadoQA_Codigo = new int[1] ;
         T004Y2_A1986ContagemResultadoQA_DataHora = new DateTime[] {DateTime.MinValue} ;
         T004Y2_A1987ContagemResultadoQA_Texto = new String[] {""} ;
         T004Y2_A1985ContagemResultadoQA_OSCod = new int[1] ;
         T004Y2_A1988ContagemResultadoQA_UserCod = new int[1] ;
         T004Y2_A1992ContagemResultadoQA_ParaCod = new int[1] ;
         T004Y2_A1996ContagemResultadoQA_RespostaDe = new int[1] ;
         T004Y2_n1996ContagemResultadoQA_RespostaDe = new bool[] {false} ;
         T004Y20_A1984ContagemResultadoQA_Codigo = new int[1] ;
         T004Y23_A493ContagemResultado_DemandaFM = new String[] {""} ;
         T004Y23_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         T004Y24_A1989ContagemResultadoQA_UserPesCod = new int[1] ;
         T004Y24_n1989ContagemResultadoQA_UserPesCod = new bool[] {false} ;
         T004Y25_A1990ContagemResultadoQA_UserPesNom = new String[] {""} ;
         T004Y25_n1990ContagemResultadoQA_UserPesNom = new bool[] {false} ;
         T004Y26_A1993ContagemResultadoQA_ParaPesCod = new int[1] ;
         T004Y26_n1993ContagemResultadoQA_ParaPesCod = new bool[] {false} ;
         T004Y27_A1994ContagemResultadoQA_ParaPesNom = new String[] {""} ;
         T004Y27_n1994ContagemResultadoQA_ParaPesNom = new bool[] {false} ;
         T004Y28_A1996ContagemResultadoQA_RespostaDe = new int[1] ;
         T004Y28_n1996ContagemResultadoQA_RespostaDe = new bool[] {false} ;
         T004Y29_A1984ContagemResultadoQA_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         T004Y30_A1996ContagemResultadoQA_RespostaDe = new int[1] ;
         T004Y30_n1996ContagemResultadoQA_RespostaDe = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemresultadoqa__default(),
            new Object[][] {
                new Object[] {
               T004Y2_A1984ContagemResultadoQA_Codigo, T004Y2_A1986ContagemResultadoQA_DataHora, T004Y2_A1987ContagemResultadoQA_Texto, T004Y2_A1985ContagemResultadoQA_OSCod, T004Y2_A1988ContagemResultadoQA_UserCod, T004Y2_A1992ContagemResultadoQA_ParaCod, T004Y2_A1996ContagemResultadoQA_RespostaDe, T004Y2_n1996ContagemResultadoQA_RespostaDe
               }
               , new Object[] {
               T004Y3_A1984ContagemResultadoQA_Codigo, T004Y3_A1986ContagemResultadoQA_DataHora, T004Y3_A1987ContagemResultadoQA_Texto, T004Y3_A1985ContagemResultadoQA_OSCod, T004Y3_A1988ContagemResultadoQA_UserCod, T004Y3_A1992ContagemResultadoQA_ParaCod, T004Y3_A1996ContagemResultadoQA_RespostaDe, T004Y3_n1996ContagemResultadoQA_RespostaDe
               }
               , new Object[] {
               T004Y4_A493ContagemResultado_DemandaFM, T004Y4_n493ContagemResultado_DemandaFM
               }
               , new Object[] {
               T004Y5_A1989ContagemResultadoQA_UserPesCod, T004Y5_n1989ContagemResultadoQA_UserPesCod
               }
               , new Object[] {
               T004Y6_A1993ContagemResultadoQA_ParaPesCod, T004Y6_n1993ContagemResultadoQA_ParaPesCod
               }
               , new Object[] {
               T004Y7_A1996ContagemResultadoQA_RespostaDe
               }
               , new Object[] {
               T004Y8_A1990ContagemResultadoQA_UserPesNom, T004Y8_n1990ContagemResultadoQA_UserPesNom
               }
               , new Object[] {
               T004Y9_A1994ContagemResultadoQA_ParaPesNom, T004Y9_n1994ContagemResultadoQA_ParaPesNom
               }
               , new Object[] {
               T004Y10_A1984ContagemResultadoQA_Codigo, T004Y10_A1986ContagemResultadoQA_DataHora, T004Y10_A493ContagemResultado_DemandaFM, T004Y10_n493ContagemResultado_DemandaFM, T004Y10_A1987ContagemResultadoQA_Texto, T004Y10_A1990ContagemResultadoQA_UserPesNom, T004Y10_n1990ContagemResultadoQA_UserPesNom, T004Y10_A1994ContagemResultadoQA_ParaPesNom, T004Y10_n1994ContagemResultadoQA_ParaPesNom, T004Y10_A1985ContagemResultadoQA_OSCod,
               T004Y10_A1988ContagemResultadoQA_UserCod, T004Y10_A1992ContagemResultadoQA_ParaCod, T004Y10_A1996ContagemResultadoQA_RespostaDe, T004Y10_n1996ContagemResultadoQA_RespostaDe, T004Y10_A1989ContagemResultadoQA_UserPesCod, T004Y10_n1989ContagemResultadoQA_UserPesCod, T004Y10_A1993ContagemResultadoQA_ParaPesCod, T004Y10_n1993ContagemResultadoQA_ParaPesCod
               }
               , new Object[] {
               T004Y11_A493ContagemResultado_DemandaFM, T004Y11_n493ContagemResultado_DemandaFM
               }
               , new Object[] {
               T004Y12_A1989ContagemResultadoQA_UserPesCod, T004Y12_n1989ContagemResultadoQA_UserPesCod
               }
               , new Object[] {
               T004Y13_A1990ContagemResultadoQA_UserPesNom, T004Y13_n1990ContagemResultadoQA_UserPesNom
               }
               , new Object[] {
               T004Y14_A1993ContagemResultadoQA_ParaPesCod, T004Y14_n1993ContagemResultadoQA_ParaPesCod
               }
               , new Object[] {
               T004Y15_A1994ContagemResultadoQA_ParaPesNom, T004Y15_n1994ContagemResultadoQA_ParaPesNom
               }
               , new Object[] {
               T004Y16_A1996ContagemResultadoQA_RespostaDe
               }
               , new Object[] {
               T004Y17_A1984ContagemResultadoQA_Codigo
               }
               , new Object[] {
               T004Y18_A1984ContagemResultadoQA_Codigo
               }
               , new Object[] {
               T004Y19_A1984ContagemResultadoQA_Codigo
               }
               , new Object[] {
               T004Y20_A1984ContagemResultadoQA_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T004Y23_A493ContagemResultado_DemandaFM, T004Y23_n493ContagemResultado_DemandaFM
               }
               , new Object[] {
               T004Y24_A1989ContagemResultadoQA_UserPesCod, T004Y24_n1989ContagemResultadoQA_UserPesCod
               }
               , new Object[] {
               T004Y25_A1990ContagemResultadoQA_UserPesNom, T004Y25_n1990ContagemResultadoQA_UserPesNom
               }
               , new Object[] {
               T004Y26_A1993ContagemResultadoQA_ParaPesCod, T004Y26_n1993ContagemResultadoQA_ParaPesCod
               }
               , new Object[] {
               T004Y27_A1994ContagemResultadoQA_ParaPesNom, T004Y27_n1994ContagemResultadoQA_ParaPesNom
               }
               , new Object[] {
               T004Y28_A1996ContagemResultadoQA_RespostaDe
               }
               , new Object[] {
               T004Y29_A1984ContagemResultadoQA_Codigo
               }
               , new Object[] {
               T004Y30_A1996ContagemResultadoQA_RespostaDe
               }
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short GX_JID ;
      private short RcdFound220 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z1984ContagemResultadoQA_Codigo ;
      private int Z1985ContagemResultadoQA_OSCod ;
      private int Z1988ContagemResultadoQA_UserCod ;
      private int Z1992ContagemResultadoQA_ParaCod ;
      private int Z1996ContagemResultadoQA_RespostaDe ;
      private int A1984ContagemResultadoQA_Codigo ;
      private int A1985ContagemResultadoQA_OSCod ;
      private int A1992ContagemResultadoQA_ParaCod ;
      private int A1988ContagemResultadoQA_UserCod ;
      private int A1989ContagemResultadoQA_UserPesCod ;
      private int A1993ContagemResultadoQA_ParaPesCod ;
      private int A1996ContagemResultadoQA_RespostaDe ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int edtContagemResultadoQA_Codigo_Enabled ;
      private int edtContagemResultadoQA_OSCod_Enabled ;
      private int edtContagemResultado_DemandaFM_Enabled ;
      private int edtContagemResultadoQA_DataHora_Enabled ;
      private int edtContagemResultadoQA_Texto_Enabled ;
      private int edtContagemResultadoQA_UserCod_Enabled ;
      private int edtContagemResultadoQA_UserPesCod_Enabled ;
      private int edtContagemResultadoQA_UserPesNom_Enabled ;
      private int A1991ContagemResultadoQA_ContratanteDoUser ;
      private int edtContagemResultadoQA_ContratanteDoUser_Enabled ;
      private int edtContagemResultadoQA_ParaCod_Enabled ;
      private int edtContagemResultadoQA_ParaPesCod_Enabled ;
      private int edtContagemResultadoQA_ParaPesNom_Enabled ;
      private int A1995ContagemResultadoQA_ContratanteDoPara ;
      private int edtContagemResultadoQA_ContratanteDoPara_Enabled ;
      private int edtContagemResultadoQA_RespostaDe_Enabled ;
      private int A1998ContagemResultadoQA_Resposta ;
      private int edtContagemResultadoQA_Resposta_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int Z1989ContagemResultadoQA_UserPesCod ;
      private int Z1993ContagemResultadoQA_ParaPesCod ;
      private int idxLst ;
      private int Z1998ContagemResultadoQA_Resposta ;
      private int Z1995ContagemResultadoQA_ContratanteDoPara ;
      private int Z1991ContagemResultadoQA_ContratanteDoUser ;
      private int GXt_int1 ;
      private String sPrefix ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtContagemResultadoQA_Codigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblockcontagemresultadoqa_codigo_Internalname ;
      private String lblTextblockcontagemresultadoqa_codigo_Jsonclick ;
      private String edtContagemResultadoQA_Codigo_Jsonclick ;
      private String lblTextblockcontagemresultadoqa_oscod_Internalname ;
      private String lblTextblockcontagemresultadoqa_oscod_Jsonclick ;
      private String edtContagemResultadoQA_OSCod_Internalname ;
      private String edtContagemResultadoQA_OSCod_Jsonclick ;
      private String lblTextblockcontagemresultado_demandafm_Internalname ;
      private String lblTextblockcontagemresultado_demandafm_Jsonclick ;
      private String edtContagemResultado_DemandaFM_Internalname ;
      private String edtContagemResultado_DemandaFM_Jsonclick ;
      private String lblTextblockcontagemresultadoqa_datahora_Internalname ;
      private String lblTextblockcontagemresultadoqa_datahora_Jsonclick ;
      private String edtContagemResultadoQA_DataHora_Internalname ;
      private String edtContagemResultadoQA_DataHora_Jsonclick ;
      private String lblTextblockcontagemresultadoqa_texto_Internalname ;
      private String lblTextblockcontagemresultadoqa_texto_Jsonclick ;
      private String edtContagemResultadoQA_Texto_Internalname ;
      private String lblTextblockcontagemresultadoqa_usercod_Internalname ;
      private String lblTextblockcontagemresultadoqa_usercod_Jsonclick ;
      private String edtContagemResultadoQA_UserCod_Internalname ;
      private String edtContagemResultadoQA_UserCod_Jsonclick ;
      private String lblTextblockcontagemresultadoqa_userpescod_Internalname ;
      private String lblTextblockcontagemresultadoqa_userpescod_Jsonclick ;
      private String edtContagemResultadoQA_UserPesCod_Internalname ;
      private String edtContagemResultadoQA_UserPesCod_Jsonclick ;
      private String lblTextblockcontagemresultadoqa_userpesnom_Internalname ;
      private String lblTextblockcontagemresultadoqa_userpesnom_Jsonclick ;
      private String edtContagemResultadoQA_UserPesNom_Internalname ;
      private String A1990ContagemResultadoQA_UserPesNom ;
      private String edtContagemResultadoQA_UserPesNom_Jsonclick ;
      private String lblTextblockcontagemresultadoqa_contratantedouser_Internalname ;
      private String lblTextblockcontagemresultadoqa_contratantedouser_Jsonclick ;
      private String edtContagemResultadoQA_ContratanteDoUser_Internalname ;
      private String edtContagemResultadoQA_ContratanteDoUser_Jsonclick ;
      private String lblTextblockcontagemresultadoqa_paracod_Internalname ;
      private String lblTextblockcontagemresultadoqa_paracod_Jsonclick ;
      private String edtContagemResultadoQA_ParaCod_Internalname ;
      private String edtContagemResultadoQA_ParaCod_Jsonclick ;
      private String lblTextblockcontagemresultadoqa_parapescod_Internalname ;
      private String lblTextblockcontagemresultadoqa_parapescod_Jsonclick ;
      private String edtContagemResultadoQA_ParaPesCod_Internalname ;
      private String edtContagemResultadoQA_ParaPesCod_Jsonclick ;
      private String lblTextblockcontagemresultadoqa_parapesnom_Internalname ;
      private String lblTextblockcontagemresultadoqa_parapesnom_Jsonclick ;
      private String edtContagemResultadoQA_ParaPesNom_Internalname ;
      private String A1994ContagemResultadoQA_ParaPesNom ;
      private String edtContagemResultadoQA_ParaPesNom_Jsonclick ;
      private String lblTextblockcontagemresultadoqa_contratantedopara_Internalname ;
      private String lblTextblockcontagemresultadoqa_contratantedopara_Jsonclick ;
      private String edtContagemResultadoQA_ContratanteDoPara_Internalname ;
      private String edtContagemResultadoQA_ContratanteDoPara_Jsonclick ;
      private String lblTextblockcontagemresultadoqa_respostade_Internalname ;
      private String lblTextblockcontagemresultadoqa_respostade_Jsonclick ;
      private String edtContagemResultadoQA_RespostaDe_Internalname ;
      private String edtContagemResultadoQA_RespostaDe_Jsonclick ;
      private String lblTextblockcontagemresultadoqa_resposta_Internalname ;
      private String lblTextblockcontagemresultadoqa_resposta_Jsonclick ;
      private String edtContagemResultadoQA_Resposta_Internalname ;
      private String edtContagemResultadoQA_Resposta_Jsonclick ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z1990ContagemResultadoQA_UserPesNom ;
      private String Z1994ContagemResultadoQA_ParaPesNom ;
      private String sMode220 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private DateTime Z1986ContagemResultadoQA_DataHora ;
      private DateTime A1986ContagemResultadoQA_DataHora ;
      private bool entryPointCalled ;
      private bool n1989ContagemResultadoQA_UserPesCod ;
      private bool n1993ContagemResultadoQA_ParaPesCod ;
      private bool n1996ContagemResultadoQA_RespostaDe ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n1990ContagemResultadoQA_UserPesNom ;
      private bool n1994ContagemResultadoQA_ParaPesNom ;
      private String A1987ContagemResultadoQA_Texto ;
      private String Z1987ContagemResultadoQA_Texto ;
      private String A493ContagemResultado_DemandaFM ;
      private String Z493ContagemResultado_DemandaFM ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] T004Y10_A1984ContagemResultadoQA_Codigo ;
      private DateTime[] T004Y10_A1986ContagemResultadoQA_DataHora ;
      private String[] T004Y10_A493ContagemResultado_DemandaFM ;
      private bool[] T004Y10_n493ContagemResultado_DemandaFM ;
      private String[] T004Y10_A1987ContagemResultadoQA_Texto ;
      private String[] T004Y10_A1990ContagemResultadoQA_UserPesNom ;
      private bool[] T004Y10_n1990ContagemResultadoQA_UserPesNom ;
      private String[] T004Y10_A1994ContagemResultadoQA_ParaPesNom ;
      private bool[] T004Y10_n1994ContagemResultadoQA_ParaPesNom ;
      private int[] T004Y10_A1985ContagemResultadoQA_OSCod ;
      private int[] T004Y10_A1988ContagemResultadoQA_UserCod ;
      private int[] T004Y10_A1992ContagemResultadoQA_ParaCod ;
      private int[] T004Y10_A1996ContagemResultadoQA_RespostaDe ;
      private bool[] T004Y10_n1996ContagemResultadoQA_RespostaDe ;
      private int[] T004Y10_A1989ContagemResultadoQA_UserPesCod ;
      private bool[] T004Y10_n1989ContagemResultadoQA_UserPesCod ;
      private int[] T004Y10_A1993ContagemResultadoQA_ParaPesCod ;
      private bool[] T004Y10_n1993ContagemResultadoQA_ParaPesCod ;
      private String[] T004Y4_A493ContagemResultado_DemandaFM ;
      private bool[] T004Y4_n493ContagemResultado_DemandaFM ;
      private int[] T004Y5_A1989ContagemResultadoQA_UserPesCod ;
      private bool[] T004Y5_n1989ContagemResultadoQA_UserPesCod ;
      private String[] T004Y8_A1990ContagemResultadoQA_UserPesNom ;
      private bool[] T004Y8_n1990ContagemResultadoQA_UserPesNom ;
      private int[] T004Y6_A1993ContagemResultadoQA_ParaPesCod ;
      private bool[] T004Y6_n1993ContagemResultadoQA_ParaPesCod ;
      private String[] T004Y9_A1994ContagemResultadoQA_ParaPesNom ;
      private bool[] T004Y9_n1994ContagemResultadoQA_ParaPesNom ;
      private int[] T004Y7_A1996ContagemResultadoQA_RespostaDe ;
      private bool[] T004Y7_n1996ContagemResultadoQA_RespostaDe ;
      private String[] T004Y11_A493ContagemResultado_DemandaFM ;
      private bool[] T004Y11_n493ContagemResultado_DemandaFM ;
      private int[] T004Y12_A1989ContagemResultadoQA_UserPesCod ;
      private bool[] T004Y12_n1989ContagemResultadoQA_UserPesCod ;
      private String[] T004Y13_A1990ContagemResultadoQA_UserPesNom ;
      private bool[] T004Y13_n1990ContagemResultadoQA_UserPesNom ;
      private int[] T004Y14_A1993ContagemResultadoQA_ParaPesCod ;
      private bool[] T004Y14_n1993ContagemResultadoQA_ParaPesCod ;
      private String[] T004Y15_A1994ContagemResultadoQA_ParaPesNom ;
      private bool[] T004Y15_n1994ContagemResultadoQA_ParaPesNom ;
      private int[] T004Y16_A1996ContagemResultadoQA_RespostaDe ;
      private bool[] T004Y16_n1996ContagemResultadoQA_RespostaDe ;
      private int[] T004Y17_A1984ContagemResultadoQA_Codigo ;
      private int[] T004Y3_A1984ContagemResultadoQA_Codigo ;
      private DateTime[] T004Y3_A1986ContagemResultadoQA_DataHora ;
      private String[] T004Y3_A1987ContagemResultadoQA_Texto ;
      private int[] T004Y3_A1985ContagemResultadoQA_OSCod ;
      private int[] T004Y3_A1988ContagemResultadoQA_UserCod ;
      private int[] T004Y3_A1992ContagemResultadoQA_ParaCod ;
      private int[] T004Y3_A1996ContagemResultadoQA_RespostaDe ;
      private bool[] T004Y3_n1996ContagemResultadoQA_RespostaDe ;
      private int[] T004Y18_A1984ContagemResultadoQA_Codigo ;
      private int[] T004Y19_A1984ContagemResultadoQA_Codigo ;
      private int[] T004Y2_A1984ContagemResultadoQA_Codigo ;
      private DateTime[] T004Y2_A1986ContagemResultadoQA_DataHora ;
      private String[] T004Y2_A1987ContagemResultadoQA_Texto ;
      private int[] T004Y2_A1985ContagemResultadoQA_OSCod ;
      private int[] T004Y2_A1988ContagemResultadoQA_UserCod ;
      private int[] T004Y2_A1992ContagemResultadoQA_ParaCod ;
      private int[] T004Y2_A1996ContagemResultadoQA_RespostaDe ;
      private bool[] T004Y2_n1996ContagemResultadoQA_RespostaDe ;
      private int[] T004Y20_A1984ContagemResultadoQA_Codigo ;
      private String[] T004Y23_A493ContagemResultado_DemandaFM ;
      private bool[] T004Y23_n493ContagemResultado_DemandaFM ;
      private int[] T004Y24_A1989ContagemResultadoQA_UserPesCod ;
      private bool[] T004Y24_n1989ContagemResultadoQA_UserPesCod ;
      private String[] T004Y25_A1990ContagemResultadoQA_UserPesNom ;
      private bool[] T004Y25_n1990ContagemResultadoQA_UserPesNom ;
      private int[] T004Y26_A1993ContagemResultadoQA_ParaPesCod ;
      private bool[] T004Y26_n1993ContagemResultadoQA_ParaPesCod ;
      private String[] T004Y27_A1994ContagemResultadoQA_ParaPesNom ;
      private bool[] T004Y27_n1994ContagemResultadoQA_ParaPesNom ;
      private int[] T004Y28_A1996ContagemResultadoQA_RespostaDe ;
      private bool[] T004Y28_n1996ContagemResultadoQA_RespostaDe ;
      private int[] T004Y29_A1984ContagemResultadoQA_Codigo ;
      private int[] T004Y30_A1996ContagemResultadoQA_RespostaDe ;
      private bool[] T004Y30_n1996ContagemResultadoQA_RespostaDe ;
      private GXWebForm Form ;
   }

   public class contagemresultadoqa__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new UpdateCursor(def[19])
         ,new UpdateCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new ForEachCursor(def[26])
         ,new ForEachCursor(def[27])
         ,new ForEachCursor(def[28])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT004Y10 ;
          prmT004Y10 = new Object[] {
          new Object[] {"@ContagemResultadoQA_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Y4 ;
          prmT004Y4 = new Object[] {
          new Object[] {"@ContagemResultadoQA_OSCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Y5 ;
          prmT004Y5 = new Object[] {
          new Object[] {"@ContagemResultadoQA_UserCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Y8 ;
          prmT004Y8 = new Object[] {
          new Object[] {"@ContagemResultadoQA_UserPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Y6 ;
          prmT004Y6 = new Object[] {
          new Object[] {"@ContagemResultadoQA_ParaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Y9 ;
          prmT004Y9 = new Object[] {
          new Object[] {"@ContagemResultadoQA_ParaPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Y7 ;
          prmT004Y7 = new Object[] {
          new Object[] {"@ContagemResultadoQA_RespostaDe",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Y11 ;
          prmT004Y11 = new Object[] {
          new Object[] {"@ContagemResultadoQA_OSCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Y12 ;
          prmT004Y12 = new Object[] {
          new Object[] {"@ContagemResultadoQA_UserCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Y13 ;
          prmT004Y13 = new Object[] {
          new Object[] {"@ContagemResultadoQA_UserPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Y14 ;
          prmT004Y14 = new Object[] {
          new Object[] {"@ContagemResultadoQA_ParaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Y15 ;
          prmT004Y15 = new Object[] {
          new Object[] {"@ContagemResultadoQA_ParaPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Y16 ;
          prmT004Y16 = new Object[] {
          new Object[] {"@ContagemResultadoQA_RespostaDe",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Y17 ;
          prmT004Y17 = new Object[] {
          new Object[] {"@ContagemResultadoQA_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Y3 ;
          prmT004Y3 = new Object[] {
          new Object[] {"@ContagemResultadoQA_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Y18 ;
          prmT004Y18 = new Object[] {
          new Object[] {"@ContagemResultadoQA_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Y19 ;
          prmT004Y19 = new Object[] {
          new Object[] {"@ContagemResultadoQA_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Y2 ;
          prmT004Y2 = new Object[] {
          new Object[] {"@ContagemResultadoQA_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Y20 ;
          prmT004Y20 = new Object[] {
          new Object[] {"@ContagemResultadoQA_DataHora",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoQA_Texto",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@ContagemResultadoQA_OSCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoQA_UserCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoQA_ParaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoQA_RespostaDe",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Y21 ;
          prmT004Y21 = new Object[] {
          new Object[] {"@ContagemResultadoQA_DataHora",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoQA_Texto",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@ContagemResultadoQA_OSCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoQA_UserCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoQA_ParaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoQA_RespostaDe",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoQA_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Y22 ;
          prmT004Y22 = new Object[] {
          new Object[] {"@ContagemResultadoQA_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Y28 ;
          prmT004Y28 = new Object[] {
          new Object[] {"@ContagemResultadoQA_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Y29 ;
          prmT004Y29 = new Object[] {
          } ;
          Object[] prmT004Y23 ;
          prmT004Y23 = new Object[] {
          new Object[] {"@ContagemResultadoQA_OSCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Y24 ;
          prmT004Y24 = new Object[] {
          new Object[] {"@ContagemResultadoQA_UserCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Y25 ;
          prmT004Y25 = new Object[] {
          new Object[] {"@ContagemResultadoQA_UserPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Y26 ;
          prmT004Y26 = new Object[] {
          new Object[] {"@ContagemResultadoQA_ParaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Y27 ;
          prmT004Y27 = new Object[] {
          new Object[] {"@ContagemResultadoQA_ParaPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Y30 ;
          prmT004Y30 = new Object[] {
          new Object[] {"@ContagemResultadoQA_RespostaDe",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T004Y2", "SELECT [ContagemResultadoQA_Codigo], [ContagemResultadoQA_DataHora], [ContagemResultadoQA_Texto], [ContagemResultadoQA_OSCod] AS ContagemResultadoQA_OSCod, [ContagemResultadoQA_UserCod] AS ContagemResultadoQA_UserCod, [ContagemResultadoQA_ParaCod] AS ContagemResultadoQA_ParaCod, [ContagemResultadoQA_RespostaDe] AS ContagemResultadoQA_RespostaDe FROM [ContagemResultadoQA] WITH (UPDLOCK) WHERE [ContagemResultadoQA_Codigo] = @ContagemResultadoQA_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004Y2,1,0,true,false )
             ,new CursorDef("T004Y3", "SELECT [ContagemResultadoQA_Codigo], [ContagemResultadoQA_DataHora], [ContagemResultadoQA_Texto], [ContagemResultadoQA_OSCod] AS ContagemResultadoQA_OSCod, [ContagemResultadoQA_UserCod] AS ContagemResultadoQA_UserCod, [ContagemResultadoQA_ParaCod] AS ContagemResultadoQA_ParaCod, [ContagemResultadoQA_RespostaDe] AS ContagemResultadoQA_RespostaDe FROM [ContagemResultadoQA] WITH (NOLOCK) WHERE [ContagemResultadoQA_Codigo] = @ContagemResultadoQA_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004Y3,1,0,true,false )
             ,new CursorDef("T004Y4", "SELECT [ContagemResultado_DemandaFM] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultadoQA_OSCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004Y4,1,0,true,false )
             ,new CursorDef("T004Y5", "SELECT [Usuario_PessoaCod] AS ContagemResultadoQA_UserPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultadoQA_UserCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004Y5,1,0,true,false )
             ,new CursorDef("T004Y6", "SELECT [Usuario_PessoaCod] AS ContagemResultadoQA_ParaPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultadoQA_ParaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004Y6,1,0,true,false )
             ,new CursorDef("T004Y7", "SELECT [ContagemResultadoQA_Codigo] AS ContagemResultadoQA_RespostaDe FROM [ContagemResultadoQA] WITH (NOLOCK) WHERE [ContagemResultadoQA_Codigo] = @ContagemResultadoQA_RespostaDe ",true, GxErrorMask.GX_NOMASK, false, this,prmT004Y7,1,0,true,false )
             ,new CursorDef("T004Y8", "SELECT [Pessoa_Nome] AS ContagemResultadoQA_UserPesNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultadoQA_UserPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004Y8,1,0,true,false )
             ,new CursorDef("T004Y9", "SELECT [Pessoa_Nome] AS ContagemResultadoQA_ParaPesNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultadoQA_ParaPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004Y9,1,0,true,false )
             ,new CursorDef("T004Y10", "SELECT TM1.[ContagemResultadoQA_Codigo], TM1.[ContagemResultadoQA_DataHora], T2.[ContagemResultado_DemandaFM], TM1.[ContagemResultadoQA_Texto], T4.[Pessoa_Nome] AS ContagemResultadoQA_UserPesNom, T6.[Pessoa_Nome] AS ContagemResultadoQA_ParaPesNom, TM1.[ContagemResultadoQA_OSCod] AS ContagemResultadoQA_OSCod, TM1.[ContagemResultadoQA_UserCod] AS ContagemResultadoQA_UserCod, TM1.[ContagemResultadoQA_ParaCod] AS ContagemResultadoQA_ParaCod, TM1.[ContagemResultadoQA_RespostaDe] AS ContagemResultadoQA_RespostaDe, T3.[Usuario_PessoaCod] AS ContagemResultadoQA_UserPesCod, T5.[Usuario_PessoaCod] AS ContagemResultadoQA_ParaPesCod FROM ((((([ContagemResultadoQA] TM1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = TM1.[ContagemResultadoQA_OSCod]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = TM1.[ContagemResultadoQA_UserCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) INNER JOIN [Usuario] T5 WITH (NOLOCK) ON T5.[Usuario_Codigo] = TM1.[ContagemResultadoQA_ParaCod]) LEFT JOIN [Pessoa] T6 WITH (NOLOCK) ON T6.[Pessoa_Codigo] = T5.[Usuario_PessoaCod]) WHERE TM1.[ContagemResultadoQA_Codigo] = @ContagemResultadoQA_Codigo ORDER BY TM1.[ContagemResultadoQA_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004Y10,100,0,true,false )
             ,new CursorDef("T004Y11", "SELECT [ContagemResultado_DemandaFM] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultadoQA_OSCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004Y11,1,0,true,false )
             ,new CursorDef("T004Y12", "SELECT [Usuario_PessoaCod] AS ContagemResultadoQA_UserPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultadoQA_UserCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004Y12,1,0,true,false )
             ,new CursorDef("T004Y13", "SELECT [Pessoa_Nome] AS ContagemResultadoQA_UserPesNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultadoQA_UserPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004Y13,1,0,true,false )
             ,new CursorDef("T004Y14", "SELECT [Usuario_PessoaCod] AS ContagemResultadoQA_ParaPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultadoQA_ParaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004Y14,1,0,true,false )
             ,new CursorDef("T004Y15", "SELECT [Pessoa_Nome] AS ContagemResultadoQA_ParaPesNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultadoQA_ParaPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004Y15,1,0,true,false )
             ,new CursorDef("T004Y16", "SELECT [ContagemResultadoQA_Codigo] AS ContagemResultadoQA_RespostaDe FROM [ContagemResultadoQA] WITH (NOLOCK) WHERE [ContagemResultadoQA_Codigo] = @ContagemResultadoQA_RespostaDe ",true, GxErrorMask.GX_NOMASK, false, this,prmT004Y16,1,0,true,false )
             ,new CursorDef("T004Y17", "SELECT [ContagemResultadoQA_Codigo] FROM [ContagemResultadoQA] WITH (NOLOCK) WHERE [ContagemResultadoQA_Codigo] = @ContagemResultadoQA_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004Y17,1,0,true,false )
             ,new CursorDef("T004Y18", "SELECT TOP 1 [ContagemResultadoQA_Codigo] FROM [ContagemResultadoQA] WITH (NOLOCK) WHERE ( [ContagemResultadoQA_Codigo] > @ContagemResultadoQA_Codigo) ORDER BY [ContagemResultadoQA_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004Y18,1,0,true,true )
             ,new CursorDef("T004Y19", "SELECT TOP 1 [ContagemResultadoQA_Codigo] FROM [ContagemResultadoQA] WITH (NOLOCK) WHERE ( [ContagemResultadoQA_Codigo] < @ContagemResultadoQA_Codigo) ORDER BY [ContagemResultadoQA_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004Y19,1,0,true,true )
             ,new CursorDef("T004Y20", "INSERT INTO [ContagemResultadoQA]([ContagemResultadoQA_DataHora], [ContagemResultadoQA_Texto], [ContagemResultadoQA_OSCod], [ContagemResultadoQA_UserCod], [ContagemResultadoQA_ParaCod], [ContagemResultadoQA_RespostaDe]) VALUES(@ContagemResultadoQA_DataHora, @ContagemResultadoQA_Texto, @ContagemResultadoQA_OSCod, @ContagemResultadoQA_UserCod, @ContagemResultadoQA_ParaCod, @ContagemResultadoQA_RespostaDe); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT004Y20)
             ,new CursorDef("T004Y21", "UPDATE [ContagemResultadoQA] SET [ContagemResultadoQA_DataHora]=@ContagemResultadoQA_DataHora, [ContagemResultadoQA_Texto]=@ContagemResultadoQA_Texto, [ContagemResultadoQA_OSCod]=@ContagemResultadoQA_OSCod, [ContagemResultadoQA_UserCod]=@ContagemResultadoQA_UserCod, [ContagemResultadoQA_ParaCod]=@ContagemResultadoQA_ParaCod, [ContagemResultadoQA_RespostaDe]=@ContagemResultadoQA_RespostaDe  WHERE [ContagemResultadoQA_Codigo] = @ContagemResultadoQA_Codigo", GxErrorMask.GX_NOMASK,prmT004Y21)
             ,new CursorDef("T004Y22", "DELETE FROM [ContagemResultadoQA]  WHERE [ContagemResultadoQA_Codigo] = @ContagemResultadoQA_Codigo", GxErrorMask.GX_NOMASK,prmT004Y22)
             ,new CursorDef("T004Y23", "SELECT [ContagemResultado_DemandaFM] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultadoQA_OSCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004Y23,1,0,true,false )
             ,new CursorDef("T004Y24", "SELECT [Usuario_PessoaCod] AS ContagemResultadoQA_UserPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultadoQA_UserCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004Y24,1,0,true,false )
             ,new CursorDef("T004Y25", "SELECT [Pessoa_Nome] AS ContagemResultadoQA_UserPesNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultadoQA_UserPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004Y25,1,0,true,false )
             ,new CursorDef("T004Y26", "SELECT [Usuario_PessoaCod] AS ContagemResultadoQA_ParaPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultadoQA_ParaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004Y26,1,0,true,false )
             ,new CursorDef("T004Y27", "SELECT [Pessoa_Nome] AS ContagemResultadoQA_ParaPesNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultadoQA_ParaPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004Y27,1,0,true,false )
             ,new CursorDef("T004Y28", "SELECT TOP 1 [ContagemResultadoQA_Codigo] AS ContagemResultadoQA_RespostaDe FROM [ContagemResultadoQA] WITH (NOLOCK) WHERE [ContagemResultadoQA_RespostaDe] = @ContagemResultadoQA_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004Y28,1,0,true,true )
             ,new CursorDef("T004Y29", "SELECT [ContagemResultadoQA_Codigo] FROM [ContagemResultadoQA] WITH (NOLOCK) ORDER BY [ContagemResultadoQA_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004Y29,100,0,true,false )
             ,new CursorDef("T004Y30", "SELECT [ContagemResultadoQA_Codigo] AS ContagemResultadoQA_RespostaDe FROM [ContagemResultadoQA] WITH (NOLOCK) WHERE [ContagemResultadoQA_Codigo] = @ContagemResultadoQA_RespostaDe ",true, GxErrorMask.GX_NOMASK, false, this,prmT004Y30,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((int[]) buf[6])[0] = rslt.getInt(7) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(7);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((int[]) buf[6])[0] = rslt.getInt(7) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(7);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((int[]) buf[9])[0] = rslt.getInt(7) ;
                ((int[]) buf[10])[0] = rslt.getInt(8) ;
                ((int[]) buf[11])[0] = rslt.getInt(9) ;
                ((int[]) buf[12])[0] = rslt.getInt(10) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(10);
                ((int[]) buf[14])[0] = rslt.getInt(11) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(11);
                ((int[]) buf[16])[0] = rslt.getInt(12) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(12);
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 13 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 21 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 22 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 23 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 24 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 25 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 26 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 27 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 28 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 18 :
                stmt.SetParameterDatetime(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                stmt.SetParameter(5, (int)parms[4]);
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[6]);
                }
                return;
             case 19 :
                stmt.SetParameterDatetime(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                stmt.SetParameter(5, (int)parms[4]);
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[6]);
                }
                stmt.SetParameter(7, (int)parms[7]);
                return;
             case 20 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 21 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 22 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 23 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 24 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 25 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 26 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 28 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
