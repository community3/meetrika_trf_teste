/*
               File: AutorizacaoConsumoGeneral
        Description: Autorizacao Consumo General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/6/2020 23:56:16.65
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class autorizacaoconsumogeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public autorizacaoconsumogeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public autorizacaoconsumogeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_AutorizacaoConsumo_Codigo )
      {
         this.A1774AutorizacaoConsumo_Codigo = aP0_AutorizacaoConsumo_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A1774AutorizacaoConsumo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1774AutorizacaoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1774AutorizacaoConsumo_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A1774AutorizacaoConsumo_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAND2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV14Pgmname = "AutorizacaoConsumoGeneral";
               context.Gx_err = 0;
               WSND2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Autorizacao Consumo General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205623561672");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("autorizacaoconsumogeneral.aspx") + "?" + UrlEncode("" +A1774AutorizacaoConsumo_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA1774AutorizacaoConsumo_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA1774AutorizacaoConsumo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_AUTORIZACAOCONSUMO_VIGENCIAINICIO", GetSecureSignedToken( sPrefix, A1775AutorizacaoConsumo_VigenciaInicio));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_AUTORIZACAOCONSUMO_VIGENCIAFIM", GetSecureSignedToken( sPrefix, A1776AutorizacaoConsumo_VigenciaFim));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_AUTORIZACAOCONSUMO_UNIDADEMEDICAOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1777AutorizacaoConsumo_UnidadeMedicaoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_AUTORIZACAOCONSUMO_QUANTIDADE", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1779AutorizacaoConsumo_Quantidade), "ZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = sPrefix + "hsh" + "AutorizacaoConsumoGeneral";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1777AutorizacaoConsumo_UnidadeMedicaoCod), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("autorizacaoconsumogeneral:[SendSecurityCheck value for]"+"AutorizacaoConsumo_UnidadeMedicaoCod:"+context.localUtil.Format( (decimal)(A1777AutorizacaoConsumo_UnidadeMedicaoCod), "ZZZZZ9"));
      }

      protected void RenderHtmlCloseFormND2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("autorizacaoconsumogeneral.js", "?20205623561674");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "AutorizacaoConsumoGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Autorizacao Consumo General" ;
      }

      protected void WBND0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "autorizacaoconsumogeneral.aspx");
            }
            wb_table1_2_ND2( true) ;
         }
         else
         {
            wb_table1_2_ND2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_ND2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTND2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Autorizacao Consumo General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPND0( ) ;
            }
         }
      }

      protected void WSND2( )
      {
         STARTND2( ) ;
         EVTND2( ) ;
      }

      protected void EVTND2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPND0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPND0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11ND2 */
                                    E11ND2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPND0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12ND2 */
                                    E12ND2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPND0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13ND2 */
                                    E13ND2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPND0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14ND2 */
                                    E14ND2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPND0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPND0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEND2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormND2( ) ;
            }
         }
      }

      protected void PAND2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFND2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV14Pgmname = "AutorizacaoConsumoGeneral";
         context.Gx_err = 0;
      }

      protected void RFND2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00ND2 */
            pr_default.execute(0, new Object[] {A1774AutorizacaoConsumo_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A1779AutorizacaoConsumo_Quantidade = H00ND2_A1779AutorizacaoConsumo_Quantidade[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1779AutorizacaoConsumo_Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(A1779AutorizacaoConsumo_Quantidade), 3, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AUTORIZACAOCONSUMO_QUANTIDADE", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1779AutorizacaoConsumo_Quantidade), "ZZ9")));
               A1778AutorizacaoConsumo_UnidadeMedicaoNom = H00ND2_A1778AutorizacaoConsumo_UnidadeMedicaoNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1778AutorizacaoConsumo_UnidadeMedicaoNom", A1778AutorizacaoConsumo_UnidadeMedicaoNom);
               n1778AutorizacaoConsumo_UnidadeMedicaoNom = H00ND2_n1778AutorizacaoConsumo_UnidadeMedicaoNom[0];
               A1777AutorizacaoConsumo_UnidadeMedicaoCod = H00ND2_A1777AutorizacaoConsumo_UnidadeMedicaoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1777AutorizacaoConsumo_UnidadeMedicaoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1777AutorizacaoConsumo_UnidadeMedicaoCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AUTORIZACAOCONSUMO_UNIDADEMEDICAOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1777AutorizacaoConsumo_UnidadeMedicaoCod), "ZZZZZ9")));
               A1776AutorizacaoConsumo_VigenciaFim = H00ND2_A1776AutorizacaoConsumo_VigenciaFim[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1776AutorizacaoConsumo_VigenciaFim", context.localUtil.Format(A1776AutorizacaoConsumo_VigenciaFim, "99/99/99"));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AUTORIZACAOCONSUMO_VIGENCIAFIM", GetSecureSignedToken( sPrefix, A1776AutorizacaoConsumo_VigenciaFim));
               A1775AutorizacaoConsumo_VigenciaInicio = H00ND2_A1775AutorizacaoConsumo_VigenciaInicio[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1775AutorizacaoConsumo_VigenciaInicio", context.localUtil.Format(A1775AutorizacaoConsumo_VigenciaInicio, "99/99/99"));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AUTORIZACAOCONSUMO_VIGENCIAINICIO", GetSecureSignedToken( sPrefix, A1775AutorizacaoConsumo_VigenciaInicio));
               A82Contrato_DataVigenciaInicio = H00ND2_A82Contrato_DataVigenciaInicio[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A82Contrato_DataVigenciaInicio", context.localUtil.Format(A82Contrato_DataVigenciaInicio, "99/99/99"));
               A83Contrato_DataVigenciaTermino = H00ND2_A83Contrato_DataVigenciaTermino[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A83Contrato_DataVigenciaTermino", context.localUtil.Format(A83Contrato_DataVigenciaTermino, "99/99/99"));
               A74Contrato_Codigo = H00ND2_A74Contrato_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9")));
               A1778AutorizacaoConsumo_UnidadeMedicaoNom = H00ND2_A1778AutorizacaoConsumo_UnidadeMedicaoNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1778AutorizacaoConsumo_UnidadeMedicaoNom", A1778AutorizacaoConsumo_UnidadeMedicaoNom);
               n1778AutorizacaoConsumo_UnidadeMedicaoNom = H00ND2_n1778AutorizacaoConsumo_UnidadeMedicaoNom[0];
               A82Contrato_DataVigenciaInicio = H00ND2_A82Contrato_DataVigenciaInicio[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A82Contrato_DataVigenciaInicio", context.localUtil.Format(A82Contrato_DataVigenciaInicio, "99/99/99"));
               A83Contrato_DataVigenciaTermino = H00ND2_A83Contrato_DataVigenciaTermino[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A83Contrato_DataVigenciaTermino", context.localUtil.Format(A83Contrato_DataVigenciaTermino, "99/99/99"));
               /* Execute user event: E12ND2 */
               E12ND2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WBND0( ) ;
         }
      }

      protected void STRUPND0( )
      {
         /* Before Start, stand alone formulas. */
         AV14Pgmname = "AutorizacaoConsumoGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11ND2 */
         E11ND2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContrato_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9")));
            A83Contrato_DataVigenciaTermino = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContrato_DataVigenciaTermino_Internalname), 0));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A83Contrato_DataVigenciaTermino", context.localUtil.Format(A83Contrato_DataVigenciaTermino, "99/99/99"));
            A82Contrato_DataVigenciaInicio = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContrato_DataVigenciaInicio_Internalname), 0));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A82Contrato_DataVigenciaInicio", context.localUtil.Format(A82Contrato_DataVigenciaInicio, "99/99/99"));
            A1775AutorizacaoConsumo_VigenciaInicio = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtAutorizacaoConsumo_VigenciaInicio_Internalname), 0));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1775AutorizacaoConsumo_VigenciaInicio", context.localUtil.Format(A1775AutorizacaoConsumo_VigenciaInicio, "99/99/99"));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AUTORIZACAOCONSUMO_VIGENCIAINICIO", GetSecureSignedToken( sPrefix, A1775AutorizacaoConsumo_VigenciaInicio));
            A1776AutorizacaoConsumo_VigenciaFim = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtAutorizacaoConsumo_VigenciaFim_Internalname), 0));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1776AutorizacaoConsumo_VigenciaFim", context.localUtil.Format(A1776AutorizacaoConsumo_VigenciaFim, "99/99/99"));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AUTORIZACAOCONSUMO_VIGENCIAFIM", GetSecureSignedToken( sPrefix, A1776AutorizacaoConsumo_VigenciaFim));
            A1777AutorizacaoConsumo_UnidadeMedicaoCod = (int)(context.localUtil.CToN( cgiGet( edtAutorizacaoConsumo_UnidadeMedicaoCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1777AutorizacaoConsumo_UnidadeMedicaoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1777AutorizacaoConsumo_UnidadeMedicaoCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AUTORIZACAOCONSUMO_UNIDADEMEDICAOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1777AutorizacaoConsumo_UnidadeMedicaoCod), "ZZZZZ9")));
            A1778AutorizacaoConsumo_UnidadeMedicaoNom = StringUtil.Upper( cgiGet( edtAutorizacaoConsumo_UnidadeMedicaoNom_Internalname));
            n1778AutorizacaoConsumo_UnidadeMedicaoNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1778AutorizacaoConsumo_UnidadeMedicaoNom", A1778AutorizacaoConsumo_UnidadeMedicaoNom);
            A1779AutorizacaoConsumo_Quantidade = (short)(context.localUtil.CToN( cgiGet( edtAutorizacaoConsumo_Quantidade_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1779AutorizacaoConsumo_Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(A1779AutorizacaoConsumo_Quantidade), 3, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AUTORIZACAOCONSUMO_QUANTIDADE", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1779AutorizacaoConsumo_Quantidade), "ZZ9")));
            /* Read saved values. */
            wcpOA1774AutorizacaoConsumo_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA1774AutorizacaoConsumo_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = sPrefix + "hsh" + "AutorizacaoConsumoGeneral";
            A1777AutorizacaoConsumo_UnidadeMedicaoCod = (int)(context.localUtil.CToN( cgiGet( edtAutorizacaoConsumo_UnidadeMedicaoCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1777AutorizacaoConsumo_UnidadeMedicaoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1777AutorizacaoConsumo_UnidadeMedicaoCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_AUTORIZACAOCONSUMO_UNIDADEMEDICAOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1777AutorizacaoConsumo_UnidadeMedicaoCod), "ZZZZZ9")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1777AutorizacaoConsumo_UnidadeMedicaoCod), "ZZZZZ9");
            hsh = cgiGet( sPrefix+"hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("autorizacaoconsumogeneral:[SecurityCheckFailed value for]"+"AutorizacaoConsumo_UnidadeMedicaoCod:"+context.localUtil.Format( (decimal)(A1777AutorizacaoConsumo_UnidadeMedicaoCod), "ZZZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11ND2 */
         E11ND2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11ND2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         bttBtnupdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Visible), 5, 0)));
         bttBtndelete_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Visible), 5, 0)));
      }

      protected void nextLoad( )
      {
      }

      protected void E12ND2( )
      {
         /* Load Routine */
         edtAutorizacaoConsumo_UnidadeMedicaoNom_Link = formatLink("viewunidademedicao.aspx") + "?" + UrlEncode("" +A1777AutorizacaoConsumo_UnidadeMedicaoCod) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtAutorizacaoConsumo_UnidadeMedicaoNom_Internalname, "Link", edtAutorizacaoConsumo_UnidadeMedicaoNom_Link);
      }

      protected void E13ND2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("autorizacaoconsumo.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A1774AutorizacaoConsumo_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E14ND2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("autorizacaoconsumo.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A1774AutorizacaoConsumo_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV14Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "AutorizacaoConsumo";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "AutorizacaoConsumo_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7AutorizacaoConsumo_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_ND2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_ND2( true) ;
         }
         else
         {
            wb_table2_8_ND2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_ND2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_56_ND2( true) ;
         }
         else
         {
            wb_table3_56_ND2( false) ;
         }
         return  ;
      }

      protected void wb_table3_56_ND2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_ND2e( true) ;
         }
         else
         {
            wb_table1_2_ND2e( false) ;
         }
      }

      protected void wb_table3_56_ND2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, bttBtnupdate_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_AutorizacaoConsumoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtndelete_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_AutorizacaoConsumoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_56_ND2e( true) ;
         }
         else
         {
            wb_table3_56_ND2e( false) ;
         }
      }

      protected void wb_table2_8_ND2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockautorizacaoconsumo_codigo_Internalname, "Consumo_Codigo", "", "", lblTextblockautorizacaoconsumo_codigo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AutorizacaoConsumoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtAutorizacaoConsumo_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1774AutorizacaoConsumo_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1774AutorizacaoConsumo_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAutorizacaoConsumo_Codigo_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_AutorizacaoConsumoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_codigo_Internalname, "Contrato", "", "", lblTextblockcontrato_codigo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AutorizacaoConsumoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Codigo_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_AutorizacaoConsumoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_datavigenciatermino_Internalname, "Vig�ncia T�rmino", "", "", lblTextblockcontrato_datavigenciatermino_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AutorizacaoConsumoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContrato_DataVigenciaTermino_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContrato_DataVigenciaTermino_Internalname, context.localUtil.Format(A83Contrato_DataVigenciaTermino, "99/99/99"), context.localUtil.Format( A83Contrato_DataVigenciaTermino, "99/99/99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_DataVigenciaTermino_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_AutorizacaoConsumoGeneral.htm");
            GxWebStd.gx_bitmap( context, edtContrato_DataVigenciaTermino_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_AutorizacaoConsumoGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_datavigenciainicio_Internalname, "Vig�ncia Inicio", "", "", lblTextblockcontrato_datavigenciainicio_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AutorizacaoConsumoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContrato_DataVigenciaInicio_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContrato_DataVigenciaInicio_Internalname, context.localUtil.Format(A82Contrato_DataVigenciaInicio, "99/99/99"), context.localUtil.Format( A82Contrato_DataVigenciaInicio, "99/99/99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_DataVigenciaInicio_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_AutorizacaoConsumoGeneral.htm");
            GxWebStd.gx_bitmap( context, edtContrato_DataVigenciaInicio_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_AutorizacaoConsumoGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockautorizacaoconsumo_vigenciainicio_Internalname, "Consumo_Vigencia Inicio", "", "", lblTextblockautorizacaoconsumo_vigenciainicio_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AutorizacaoConsumoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtAutorizacaoConsumo_VigenciaInicio_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtAutorizacaoConsumo_VigenciaInicio_Internalname, context.localUtil.Format(A1775AutorizacaoConsumo_VigenciaInicio, "99/99/99"), context.localUtil.Format( A1775AutorizacaoConsumo_VigenciaInicio, "99/99/99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAutorizacaoConsumo_VigenciaInicio_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_AutorizacaoConsumoGeneral.htm");
            GxWebStd.gx_bitmap( context, edtAutorizacaoConsumo_VigenciaInicio_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_AutorizacaoConsumoGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockautorizacaoconsumo_vigenciafim_Internalname, "Consumo_Vigencia Fim", "", "", lblTextblockautorizacaoconsumo_vigenciafim_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AutorizacaoConsumoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtAutorizacaoConsumo_VigenciaFim_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtAutorizacaoConsumo_VigenciaFim_Internalname, context.localUtil.Format(A1776AutorizacaoConsumo_VigenciaFim, "99/99/99"), context.localUtil.Format( A1776AutorizacaoConsumo_VigenciaFim, "99/99/99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAutorizacaoConsumo_VigenciaFim_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_AutorizacaoConsumoGeneral.htm");
            GxWebStd.gx_bitmap( context, edtAutorizacaoConsumo_VigenciaFim_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_AutorizacaoConsumoGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockautorizacaoconsumo_unidademedicaocod_Internalname, "de Medi��o", "", "", lblTextblockautorizacaoconsumo_unidademedicaocod_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AutorizacaoConsumoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtAutorizacaoConsumo_UnidadeMedicaoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1777AutorizacaoConsumo_UnidadeMedicaoCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1777AutorizacaoConsumo_UnidadeMedicaoCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAutorizacaoConsumo_UnidadeMedicaoCod_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_AutorizacaoConsumoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockautorizacaoconsumo_unidademedicaonom_Internalname, "de Medi��o", "", "", lblTextblockautorizacaoconsumo_unidademedicaonom_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AutorizacaoConsumoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtAutorizacaoConsumo_UnidadeMedicaoNom_Internalname, StringUtil.RTrim( A1778AutorizacaoConsumo_UnidadeMedicaoNom), StringUtil.RTrim( context.localUtil.Format( A1778AutorizacaoConsumo_UnidadeMedicaoNom, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtAutorizacaoConsumo_UnidadeMedicaoNom_Link, "", "", "", edtAutorizacaoConsumo_UnidadeMedicaoNom_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_AutorizacaoConsumoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockautorizacaoconsumo_quantidade_Internalname, "Quantidade", "", "", lblTextblockautorizacaoconsumo_quantidade_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AutorizacaoConsumoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtAutorizacaoConsumo_Quantidade_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1779AutorizacaoConsumo_Quantidade), 3, 0, ",", "")), context.localUtil.Format( (decimal)(A1779AutorizacaoConsumo_Quantidade), "ZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAutorizacaoConsumo_Quantidade_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "Quantidade", "right", false, "HLP_AutorizacaoConsumoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_ND2e( true) ;
         }
         else
         {
            wb_table2_8_ND2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A1774AutorizacaoConsumo_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1774AutorizacaoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1774AutorizacaoConsumo_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAND2( ) ;
         WSND2( ) ;
         WEND2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA1774AutorizacaoConsumo_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAND2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "autorizacaoconsumogeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAND2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A1774AutorizacaoConsumo_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1774AutorizacaoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1774AutorizacaoConsumo_Codigo), 6, 0)));
         }
         wcpOA1774AutorizacaoConsumo_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA1774AutorizacaoConsumo_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A1774AutorizacaoConsumo_Codigo != wcpOA1774AutorizacaoConsumo_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA1774AutorizacaoConsumo_Codigo = A1774AutorizacaoConsumo_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA1774AutorizacaoConsumo_Codigo = cgiGet( sPrefix+"A1774AutorizacaoConsumo_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA1774AutorizacaoConsumo_Codigo) > 0 )
         {
            A1774AutorizacaoConsumo_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA1774AutorizacaoConsumo_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1774AutorizacaoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1774AutorizacaoConsumo_Codigo), 6, 0)));
         }
         else
         {
            A1774AutorizacaoConsumo_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A1774AutorizacaoConsumo_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAND2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSND2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSND2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A1774AutorizacaoConsumo_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1774AutorizacaoConsumo_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA1774AutorizacaoConsumo_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A1774AutorizacaoConsumo_Codigo_CTRL", StringUtil.RTrim( sCtrlA1774AutorizacaoConsumo_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEND2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205623561716");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("autorizacaoconsumogeneral.js", "?20205623561716");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockautorizacaoconsumo_codigo_Internalname = sPrefix+"TEXTBLOCKAUTORIZACAOCONSUMO_CODIGO";
         edtAutorizacaoConsumo_Codigo_Internalname = sPrefix+"AUTORIZACAOCONSUMO_CODIGO";
         lblTextblockcontrato_codigo_Internalname = sPrefix+"TEXTBLOCKCONTRATO_CODIGO";
         edtContrato_Codigo_Internalname = sPrefix+"CONTRATO_CODIGO";
         lblTextblockcontrato_datavigenciatermino_Internalname = sPrefix+"TEXTBLOCKCONTRATO_DATAVIGENCIATERMINO";
         edtContrato_DataVigenciaTermino_Internalname = sPrefix+"CONTRATO_DATAVIGENCIATERMINO";
         lblTextblockcontrato_datavigenciainicio_Internalname = sPrefix+"TEXTBLOCKCONTRATO_DATAVIGENCIAINICIO";
         edtContrato_DataVigenciaInicio_Internalname = sPrefix+"CONTRATO_DATAVIGENCIAINICIO";
         lblTextblockautorizacaoconsumo_vigenciainicio_Internalname = sPrefix+"TEXTBLOCKAUTORIZACAOCONSUMO_VIGENCIAINICIO";
         edtAutorizacaoConsumo_VigenciaInicio_Internalname = sPrefix+"AUTORIZACAOCONSUMO_VIGENCIAINICIO";
         lblTextblockautorizacaoconsumo_vigenciafim_Internalname = sPrefix+"TEXTBLOCKAUTORIZACAOCONSUMO_VIGENCIAFIM";
         edtAutorizacaoConsumo_VigenciaFim_Internalname = sPrefix+"AUTORIZACAOCONSUMO_VIGENCIAFIM";
         lblTextblockautorizacaoconsumo_unidademedicaocod_Internalname = sPrefix+"TEXTBLOCKAUTORIZACAOCONSUMO_UNIDADEMEDICAOCOD";
         edtAutorizacaoConsumo_UnidadeMedicaoCod_Internalname = sPrefix+"AUTORIZACAOCONSUMO_UNIDADEMEDICAOCOD";
         lblTextblockautorizacaoconsumo_unidademedicaonom_Internalname = sPrefix+"TEXTBLOCKAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM";
         edtAutorizacaoConsumo_UnidadeMedicaoNom_Internalname = sPrefix+"AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM";
         lblTextblockautorizacaoconsumo_quantidade_Internalname = sPrefix+"TEXTBLOCKAUTORIZACAOCONSUMO_QUANTIDADE";
         edtAutorizacaoConsumo_Quantidade_Internalname = sPrefix+"AUTORIZACAOCONSUMO_QUANTIDADE";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtAutorizacaoConsumo_Quantidade_Jsonclick = "";
         edtAutorizacaoConsumo_UnidadeMedicaoNom_Jsonclick = "";
         edtAutorizacaoConsumo_UnidadeMedicaoCod_Jsonclick = "";
         edtAutorizacaoConsumo_VigenciaFim_Jsonclick = "";
         edtAutorizacaoConsumo_VigenciaInicio_Jsonclick = "";
         edtContrato_DataVigenciaInicio_Jsonclick = "";
         edtContrato_DataVigenciaTermino_Jsonclick = "";
         edtContrato_Codigo_Jsonclick = "";
         edtAutorizacaoConsumo_Codigo_Jsonclick = "";
         bttBtndelete_Visible = 1;
         bttBtnupdate_Visible = 1;
         edtAutorizacaoConsumo_UnidadeMedicaoNom_Link = "";
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13ND2',iparms:[{av:'A1774AutorizacaoConsumo_Codigo',fld:'AUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E14ND2',iparms:[{av:'A1774AutorizacaoConsumo_Codigo',fld:'AUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV14Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A1775AutorizacaoConsumo_VigenciaInicio = DateTime.MinValue;
         A1776AutorizacaoConsumo_VigenciaFim = DateTime.MinValue;
         GXKey = "";
         forbiddenHiddens = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00ND2_A1774AutorizacaoConsumo_Codigo = new int[1] ;
         H00ND2_A1779AutorizacaoConsumo_Quantidade = new short[1] ;
         H00ND2_A1778AutorizacaoConsumo_UnidadeMedicaoNom = new String[] {""} ;
         H00ND2_n1778AutorizacaoConsumo_UnidadeMedicaoNom = new bool[] {false} ;
         H00ND2_A1777AutorizacaoConsumo_UnidadeMedicaoCod = new int[1] ;
         H00ND2_A1776AutorizacaoConsumo_VigenciaFim = new DateTime[] {DateTime.MinValue} ;
         H00ND2_A1775AutorizacaoConsumo_VigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         H00ND2_A82Contrato_DataVigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         H00ND2_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         H00ND2_A74Contrato_Codigo = new int[1] ;
         A1778AutorizacaoConsumo_UnidadeMedicaoNom = "";
         A82Contrato_DataVigenciaInicio = DateTime.MinValue;
         A83Contrato_DataVigenciaTermino = DateTime.MinValue;
         hsh = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         lblTextblockautorizacaoconsumo_codigo_Jsonclick = "";
         lblTextblockcontrato_codigo_Jsonclick = "";
         lblTextblockcontrato_datavigenciatermino_Jsonclick = "";
         lblTextblockcontrato_datavigenciainicio_Jsonclick = "";
         lblTextblockautorizacaoconsumo_vigenciainicio_Jsonclick = "";
         lblTextblockautorizacaoconsumo_vigenciafim_Jsonclick = "";
         lblTextblockautorizacaoconsumo_unidademedicaocod_Jsonclick = "";
         lblTextblockautorizacaoconsumo_unidademedicaonom_Jsonclick = "";
         lblTextblockautorizacaoconsumo_quantidade_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA1774AutorizacaoConsumo_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.autorizacaoconsumogeneral__default(),
            new Object[][] {
                new Object[] {
               H00ND2_A1774AutorizacaoConsumo_Codigo, H00ND2_A1779AutorizacaoConsumo_Quantidade, H00ND2_A1778AutorizacaoConsumo_UnidadeMedicaoNom, H00ND2_n1778AutorizacaoConsumo_UnidadeMedicaoNom, H00ND2_A1777AutorizacaoConsumo_UnidadeMedicaoCod, H00ND2_A1776AutorizacaoConsumo_VigenciaFim, H00ND2_A1775AutorizacaoConsumo_VigenciaInicio, H00ND2_A82Contrato_DataVigenciaInicio, H00ND2_A83Contrato_DataVigenciaTermino, H00ND2_A74Contrato_Codigo
               }
            }
         );
         AV14Pgmname = "AutorizacaoConsumoGeneral";
         /* GeneXus formulas. */
         AV14Pgmname = "AutorizacaoConsumoGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short A1779AutorizacaoConsumo_Quantidade ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A1774AutorizacaoConsumo_Codigo ;
      private int wcpOA1774AutorizacaoConsumo_Codigo ;
      private int A74Contrato_Codigo ;
      private int A1777AutorizacaoConsumo_UnidadeMedicaoCod ;
      private int bttBtnupdate_Visible ;
      private int bttBtndelete_Visible ;
      private int AV7AutorizacaoConsumo_Codigo ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV14Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String scmdbuf ;
      private String A1778AutorizacaoConsumo_UnidadeMedicaoNom ;
      private String edtContrato_Codigo_Internalname ;
      private String edtContrato_DataVigenciaTermino_Internalname ;
      private String edtContrato_DataVigenciaInicio_Internalname ;
      private String edtAutorizacaoConsumo_VigenciaInicio_Internalname ;
      private String edtAutorizacaoConsumo_VigenciaFim_Internalname ;
      private String edtAutorizacaoConsumo_UnidadeMedicaoCod_Internalname ;
      private String edtAutorizacaoConsumo_UnidadeMedicaoNom_Internalname ;
      private String edtAutorizacaoConsumo_Quantidade_Internalname ;
      private String hsh ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String edtAutorizacaoConsumo_UnidadeMedicaoNom_Link ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockautorizacaoconsumo_codigo_Internalname ;
      private String lblTextblockautorizacaoconsumo_codigo_Jsonclick ;
      private String edtAutorizacaoConsumo_Codigo_Internalname ;
      private String edtAutorizacaoConsumo_Codigo_Jsonclick ;
      private String lblTextblockcontrato_codigo_Internalname ;
      private String lblTextblockcontrato_codigo_Jsonclick ;
      private String edtContrato_Codigo_Jsonclick ;
      private String lblTextblockcontrato_datavigenciatermino_Internalname ;
      private String lblTextblockcontrato_datavigenciatermino_Jsonclick ;
      private String edtContrato_DataVigenciaTermino_Jsonclick ;
      private String lblTextblockcontrato_datavigenciainicio_Internalname ;
      private String lblTextblockcontrato_datavigenciainicio_Jsonclick ;
      private String edtContrato_DataVigenciaInicio_Jsonclick ;
      private String lblTextblockautorizacaoconsumo_vigenciainicio_Internalname ;
      private String lblTextblockautorizacaoconsumo_vigenciainicio_Jsonclick ;
      private String edtAutorizacaoConsumo_VigenciaInicio_Jsonclick ;
      private String lblTextblockautorizacaoconsumo_vigenciafim_Internalname ;
      private String lblTextblockautorizacaoconsumo_vigenciafim_Jsonclick ;
      private String edtAutorizacaoConsumo_VigenciaFim_Jsonclick ;
      private String lblTextblockautorizacaoconsumo_unidademedicaocod_Internalname ;
      private String lblTextblockautorizacaoconsumo_unidademedicaocod_Jsonclick ;
      private String edtAutorizacaoConsumo_UnidadeMedicaoCod_Jsonclick ;
      private String lblTextblockautorizacaoconsumo_unidademedicaonom_Internalname ;
      private String lblTextblockautorizacaoconsumo_unidademedicaonom_Jsonclick ;
      private String edtAutorizacaoConsumo_UnidadeMedicaoNom_Jsonclick ;
      private String lblTextblockautorizacaoconsumo_quantidade_Internalname ;
      private String lblTextblockautorizacaoconsumo_quantidade_Jsonclick ;
      private String edtAutorizacaoConsumo_Quantidade_Jsonclick ;
      private String sCtrlA1774AutorizacaoConsumo_Codigo ;
      private DateTime A1775AutorizacaoConsumo_VigenciaInicio ;
      private DateTime A1776AutorizacaoConsumo_VigenciaFim ;
      private DateTime A82Contrato_DataVigenciaInicio ;
      private DateTime A83Contrato_DataVigenciaTermino ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1778AutorizacaoConsumo_UnidadeMedicaoNom ;
      private bool returnInSub ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00ND2_A1774AutorizacaoConsumo_Codigo ;
      private short[] H00ND2_A1779AutorizacaoConsumo_Quantidade ;
      private String[] H00ND2_A1778AutorizacaoConsumo_UnidadeMedicaoNom ;
      private bool[] H00ND2_n1778AutorizacaoConsumo_UnidadeMedicaoNom ;
      private int[] H00ND2_A1777AutorizacaoConsumo_UnidadeMedicaoCod ;
      private DateTime[] H00ND2_A1776AutorizacaoConsumo_VigenciaFim ;
      private DateTime[] H00ND2_A1775AutorizacaoConsumo_VigenciaInicio ;
      private DateTime[] H00ND2_A82Contrato_DataVigenciaInicio ;
      private DateTime[] H00ND2_A83Contrato_DataVigenciaTermino ;
      private int[] H00ND2_A74Contrato_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class autorizacaoconsumogeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00ND2 ;
          prmH00ND2 = new Object[] {
          new Object[] {"@AutorizacaoConsumo_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00ND2", "SELECT T1.[AutorizacaoConsumo_Codigo], T1.[AutorizacaoConsumo_Quantidade], T2.[UnidadeMedicao_Nome] AS AutorizacaoConsumo_UnidadeMedicaoNom, T1.[AutorizacaoConsumo_UnidadeMedicaoCod] AS AutorizacaoConsumo_UnidadeMedicaoCod, T1.[AutorizacaoConsumo_VigenciaFim], T1.[AutorizacaoConsumo_VigenciaInicio], T3.[Contrato_DataVigenciaInicio], T3.[Contrato_DataVigenciaTermino], T1.[Contrato_Codigo] FROM (([AutorizacaoConsumo] T1 WITH (NOLOCK) INNER JOIN [UnidadeMedicao] T2 WITH (NOLOCK) ON T2.[UnidadeMedicao_Codigo] = T1.[AutorizacaoConsumo_UnidadeMedicaoCod]) INNER JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE T1.[AutorizacaoConsumo_Codigo] = @AutorizacaoConsumo_Codigo ORDER BY T1.[AutorizacaoConsumo_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00ND2,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(5) ;
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(6) ;
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(7) ;
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(8) ;
                ((int[]) buf[9])[0] = rslt.getInt(9) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
