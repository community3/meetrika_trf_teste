/*
               File: type_SdtContagemResultadoChckLstLog
        Description: Contagem Resultado Chck Lst Log
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:17:24.86
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "ContagemResultadoChckLstLog" )]
   [XmlType(TypeName =  "ContagemResultadoChckLstLog" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtContagemResultadoChckLstLog : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContagemResultadoChckLstLog( )
      {
         /* Constructor for serialization */
         gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_datahora = (DateTime)(DateTime.MinValue);
         gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstdes = "";
         gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuarionome = "";
         gxTv_SdtContagemResultadoChckLstLog_Mode = "";
         gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_datahora_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuarionome_Z = "";
      }

      public SdtContagemResultadoChckLstLog( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV820ContagemResultadoChckLstLog_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV820ContagemResultadoChckLstLog_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"ContagemResultadoChckLstLog_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "ContagemResultadoChckLstLog");
         metadata.Set("BT", "ContagemResultadoChckLstLog");
         metadata.Set("PK", "[ \"ContagemResultadoChckLstLog_Codigo\" ]");
         metadata.Set("PKAssigned", "[ \"ContagemResultadoChckLstLog_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"CheckList_Codigo\" ],\"FKMap\":[ \"ContagemResultadoChckLstLog_ChckLstCod-CheckList_Codigo\" ] },{ \"FK\":[ \"ContagemResultado_Codigo\" ],\"FKMap\":[ \"ContagemResultadoChckLstLog_OSCodigo-ContagemResultado_Codigo\" ] },{ \"FK\":[ \"NaoConformidade_Codigo\" ],\"FKMap\":[  ] },{ \"FK\":[ \"Usuario_Codigo\" ],\"FKMap\":[ \"ContagemResultadoChckLstLog_UsuarioCod-Usuario_Codigo\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadochcklstlog_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadochcklstlog_oscodigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadochcklstlog_datahora_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadochcklstlog_chcklstcod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Naoconformidade_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadochcklstlog_usuariocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadochcklstlog_pessoacod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadochcklstlog_usuarionome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadochcklstlog_etapa_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadochcklstlog_chcklstcod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadochcklstlog_chcklstdes_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Naoconformidade_codigo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadochcklstlog_pessoacod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadochcklstlog_usuarionome_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadochcklstlog_etapa_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtContagemResultadoChckLstLog deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtContagemResultadoChckLstLog)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtContagemResultadoChckLstLog obj ;
         obj = this;
         obj.gxTpr_Contagemresultadochcklstlog_codigo = deserialized.gxTpr_Contagemresultadochcklstlog_codigo;
         obj.gxTpr_Contagemresultadochcklstlog_oscodigo = deserialized.gxTpr_Contagemresultadochcklstlog_oscodigo;
         obj.gxTpr_Contagemresultadochcklstlog_datahora = deserialized.gxTpr_Contagemresultadochcklstlog_datahora;
         obj.gxTpr_Contagemresultadochcklstlog_chcklstcod = deserialized.gxTpr_Contagemresultadochcklstlog_chcklstcod;
         obj.gxTpr_Contagemresultadochcklstlog_chcklstdes = deserialized.gxTpr_Contagemresultadochcklstlog_chcklstdes;
         obj.gxTpr_Naoconformidade_codigo = deserialized.gxTpr_Naoconformidade_codigo;
         obj.gxTpr_Contagemresultadochcklstlog_usuariocod = deserialized.gxTpr_Contagemresultadochcklstlog_usuariocod;
         obj.gxTpr_Contagemresultadochcklstlog_pessoacod = deserialized.gxTpr_Contagemresultadochcklstlog_pessoacod;
         obj.gxTpr_Contagemresultadochcklstlog_usuarionome = deserialized.gxTpr_Contagemresultadochcklstlog_usuarionome;
         obj.gxTpr_Contagemresultadochcklstlog_etapa = deserialized.gxTpr_Contagemresultadochcklstlog_etapa;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Contagemresultadochcklstlog_codigo_Z = deserialized.gxTpr_Contagemresultadochcklstlog_codigo_Z;
         obj.gxTpr_Contagemresultadochcklstlog_oscodigo_Z = deserialized.gxTpr_Contagemresultadochcklstlog_oscodigo_Z;
         obj.gxTpr_Contagemresultadochcklstlog_datahora_Z = deserialized.gxTpr_Contagemresultadochcklstlog_datahora_Z;
         obj.gxTpr_Contagemresultadochcklstlog_chcklstcod_Z = deserialized.gxTpr_Contagemresultadochcklstlog_chcklstcod_Z;
         obj.gxTpr_Naoconformidade_codigo_Z = deserialized.gxTpr_Naoconformidade_codigo_Z;
         obj.gxTpr_Contagemresultadochcklstlog_usuariocod_Z = deserialized.gxTpr_Contagemresultadochcklstlog_usuariocod_Z;
         obj.gxTpr_Contagemresultadochcklstlog_pessoacod_Z = deserialized.gxTpr_Contagemresultadochcklstlog_pessoacod_Z;
         obj.gxTpr_Contagemresultadochcklstlog_usuarionome_Z = deserialized.gxTpr_Contagemresultadochcklstlog_usuarionome_Z;
         obj.gxTpr_Contagemresultadochcklstlog_etapa_Z = deserialized.gxTpr_Contagemresultadochcklstlog_etapa_Z;
         obj.gxTpr_Contagemresultadochcklstlog_chcklstcod_N = deserialized.gxTpr_Contagemresultadochcklstlog_chcklstcod_N;
         obj.gxTpr_Contagemresultadochcklstlog_chcklstdes_N = deserialized.gxTpr_Contagemresultadochcklstlog_chcklstdes_N;
         obj.gxTpr_Naoconformidade_codigo_N = deserialized.gxTpr_Naoconformidade_codigo_N;
         obj.gxTpr_Contagemresultadochcklstlog_pessoacod_N = deserialized.gxTpr_Contagemresultadochcklstlog_pessoacod_N;
         obj.gxTpr_Contagemresultadochcklstlog_usuarionome_N = deserialized.gxTpr_Contagemresultadochcklstlog_usuarionome_N;
         obj.gxTpr_Contagemresultadochcklstlog_etapa_N = deserialized.gxTpr_Contagemresultadochcklstlog_etapa_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoChckLstLog_Codigo") )
               {
                  gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoChckLstLog_OSCodigo") )
               {
                  gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_oscodigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoChckLstLog_DataHora") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_datahora = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_datahora = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoChckLstLog_ChckLstCod") )
               {
                  gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoChckLstLog_ChckLstDes") )
               {
                  gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstdes = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "NaoConformidade_Codigo") )
               {
                  gxTv_SdtContagemResultadoChckLstLog_Naoconformidade_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoChckLstLog_UsuarioCod") )
               {
                  gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuariocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoChckLstLog_PessoaCod") )
               {
                  gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_pessoacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoChckLstLog_UsuarioNome") )
               {
                  gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuarionome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoChckLstLog_Etapa") )
               {
                  gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_etapa = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtContagemResultadoChckLstLog_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtContagemResultadoChckLstLog_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoChckLstLog_Codigo_Z") )
               {
                  gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoChckLstLog_OSCodigo_Z") )
               {
                  gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_oscodigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoChckLstLog_DataHora_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_datahora_Z = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_datahora_Z = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoChckLstLog_ChckLstCod_Z") )
               {
                  gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstcod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "NaoConformidade_Codigo_Z") )
               {
                  gxTv_SdtContagemResultadoChckLstLog_Naoconformidade_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoChckLstLog_UsuarioCod_Z") )
               {
                  gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuariocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoChckLstLog_PessoaCod_Z") )
               {
                  gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_pessoacod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoChckLstLog_UsuarioNome_Z") )
               {
                  gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuarionome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoChckLstLog_Etapa_Z") )
               {
                  gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_etapa_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoChckLstLog_ChckLstCod_N") )
               {
                  gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstcod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoChckLstLog_ChckLstDes_N") )
               {
                  gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstdes_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "NaoConformidade_Codigo_N") )
               {
                  gxTv_SdtContagemResultadoChckLstLog_Naoconformidade_codigo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoChckLstLog_PessoaCod_N") )
               {
                  gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_pessoacod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoChckLstLog_UsuarioNome_N") )
               {
                  gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuarionome_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoChckLstLog_Etapa_N") )
               {
                  gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_etapa_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "ContagemResultadoChckLstLog";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("ContagemResultadoChckLstLog_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultadoChckLstLog_OSCodigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_oscodigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( (DateTime.MinValue==gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_datahora) )
         {
            oWriter.WriteStartElement("ContagemResultadoChckLstLog_DataHora");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_datahora)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_datahora)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_datahora)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_datahora)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_datahora)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_datahora)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("ContagemResultadoChckLstLog_DataHora", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteElement("ContagemResultadoChckLstLog_ChckLstCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultadoChckLstLog_ChckLstDes", StringUtil.RTrim( gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstdes));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("NaoConformidade_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoChckLstLog_Naoconformidade_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultadoChckLstLog_UsuarioCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuariocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultadoChckLstLog_PessoaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_pessoacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultadoChckLstLog_UsuarioNome", StringUtil.RTrim( gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuarionome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultadoChckLstLog_Etapa", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_etapa), 1, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtContagemResultadoChckLstLog_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoChckLstLog_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultadoChckLstLog_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultadoChckLstLog_OSCodigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_oscodigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            if ( (DateTime.MinValue==gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_datahora_Z) )
            {
               oWriter.WriteStartElement("ContagemResultadoChckLstLog_DataHora_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_datahora_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_datahora_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_datahora_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "T";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_datahora_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_datahora_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_datahora_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("ContagemResultadoChckLstLog_DataHora_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
               }
            }
            oWriter.WriteElement("ContagemResultadoChckLstLog_ChckLstCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstcod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("NaoConformidade_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoChckLstLog_Naoconformidade_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultadoChckLstLog_UsuarioCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuariocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultadoChckLstLog_PessoaCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_pessoacod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultadoChckLstLog_UsuarioNome_Z", StringUtil.RTrim( gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuarionome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultadoChckLstLog_Etapa_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_etapa_Z), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultadoChckLstLog_ChckLstCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstcod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultadoChckLstLog_ChckLstDes_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstdes_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("NaoConformidade_Codigo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoChckLstLog_Naoconformidade_codigo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultadoChckLstLog_PessoaCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_pessoacod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultadoChckLstLog_UsuarioNome_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuarionome_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultadoChckLstLog_Etapa_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_etapa_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("ContagemResultadoChckLstLog_Codigo", gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_codigo, false);
         AddObjectProperty("ContagemResultadoChckLstLog_OSCodigo", gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_oscodigo, false);
         datetime_STZ = gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_datahora;
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ContagemResultadoChckLstLog_DataHora", sDateCnv, false);
         AddObjectProperty("ContagemResultadoChckLstLog_ChckLstCod", gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstcod, false);
         AddObjectProperty("ContagemResultadoChckLstLog_ChckLstDes", gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstdes, false);
         AddObjectProperty("NaoConformidade_Codigo", gxTv_SdtContagemResultadoChckLstLog_Naoconformidade_codigo, false);
         AddObjectProperty("ContagemResultadoChckLstLog_UsuarioCod", gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuariocod, false);
         AddObjectProperty("ContagemResultadoChckLstLog_PessoaCod", gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_pessoacod, false);
         AddObjectProperty("ContagemResultadoChckLstLog_UsuarioNome", gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuarionome, false);
         AddObjectProperty("ContagemResultadoChckLstLog_Etapa", gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_etapa, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtContagemResultadoChckLstLog_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtContagemResultadoChckLstLog_Initialized, false);
            AddObjectProperty("ContagemResultadoChckLstLog_Codigo_Z", gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_codigo_Z, false);
            AddObjectProperty("ContagemResultadoChckLstLog_OSCodigo_Z", gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_oscodigo_Z, false);
            datetime_STZ = gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_datahora_Z;
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("ContagemResultadoChckLstLog_DataHora_Z", sDateCnv, false);
            AddObjectProperty("ContagemResultadoChckLstLog_ChckLstCod_Z", gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstcod_Z, false);
            AddObjectProperty("NaoConformidade_Codigo_Z", gxTv_SdtContagemResultadoChckLstLog_Naoconformidade_codigo_Z, false);
            AddObjectProperty("ContagemResultadoChckLstLog_UsuarioCod_Z", gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuariocod_Z, false);
            AddObjectProperty("ContagemResultadoChckLstLog_PessoaCod_Z", gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_pessoacod_Z, false);
            AddObjectProperty("ContagemResultadoChckLstLog_UsuarioNome_Z", gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuarionome_Z, false);
            AddObjectProperty("ContagemResultadoChckLstLog_Etapa_Z", gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_etapa_Z, false);
            AddObjectProperty("ContagemResultadoChckLstLog_ChckLstCod_N", gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstcod_N, false);
            AddObjectProperty("ContagemResultadoChckLstLog_ChckLstDes_N", gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstdes_N, false);
            AddObjectProperty("NaoConformidade_Codigo_N", gxTv_SdtContagemResultadoChckLstLog_Naoconformidade_codigo_N, false);
            AddObjectProperty("ContagemResultadoChckLstLog_PessoaCod_N", gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_pessoacod_N, false);
            AddObjectProperty("ContagemResultadoChckLstLog_UsuarioNome_N", gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuarionome_N, false);
            AddObjectProperty("ContagemResultadoChckLstLog_Etapa_N", gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_etapa_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "ContagemResultadoChckLstLog_Codigo" )]
      [  XmlElement( ElementName = "ContagemResultadoChckLstLog_Codigo"   )]
      public int gxTpr_Contagemresultadochcklstlog_codigo
      {
         get {
            return gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_codigo ;
         }

         set {
            if ( gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_codigo != value )
            {
               gxTv_SdtContagemResultadoChckLstLog_Mode = "INS";
               this.gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_codigo_Z_SetNull( );
               this.gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_oscodigo_Z_SetNull( );
               this.gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_datahora_Z_SetNull( );
               this.gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstcod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoChckLstLog_Naoconformidade_codigo_Z_SetNull( );
               this.gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuariocod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_pessoacod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuarionome_Z_SetNull( );
               this.gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_etapa_Z_SetNull( );
            }
            gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultadoChckLstLog_OSCodigo" )]
      [  XmlElement( ElementName = "ContagemResultadoChckLstLog_OSCodigo"   )]
      public int gxTpr_Contagemresultadochcklstlog_oscodigo
      {
         get {
            return gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_oscodigo ;
         }

         set {
            gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_oscodigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultadoChckLstLog_DataHora" )]
      [  XmlElement( ElementName = "ContagemResultadoChckLstLog_DataHora"  , IsNullable=true )]
      public string gxTpr_Contagemresultadochcklstlog_datahora_Nullable
      {
         get {
            if ( gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_datahora == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_datahora).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_datahora = DateTime.MinValue;
            else
               gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_datahora = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultadochcklstlog_datahora
      {
         get {
            return gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_datahora ;
         }

         set {
            gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_datahora = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultadoChckLstLog_ChckLstCod" )]
      [  XmlElement( ElementName = "ContagemResultadoChckLstLog_ChckLstCod"   )]
      public int gxTpr_Contagemresultadochcklstlog_chcklstcod
      {
         get {
            return gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstcod ;
         }

         set {
            gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstcod_N = 0;
            gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstcod = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstcod_SetNull( )
      {
         gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstcod_N = 1;
         gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstcod = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstcod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoChckLstLog_ChckLstDes" )]
      [  XmlElement( ElementName = "ContagemResultadoChckLstLog_ChckLstDes"   )]
      public String gxTpr_Contagemresultadochcklstlog_chcklstdes
      {
         get {
            return gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstdes ;
         }

         set {
            gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstdes_N = 0;
            gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstdes = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstdes_SetNull( )
      {
         gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstdes_N = 1;
         gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstdes = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstdes_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "NaoConformidade_Codigo" )]
      [  XmlElement( ElementName = "NaoConformidade_Codigo"   )]
      public int gxTpr_Naoconformidade_codigo
      {
         get {
            return gxTv_SdtContagemResultadoChckLstLog_Naoconformidade_codigo ;
         }

         set {
            gxTv_SdtContagemResultadoChckLstLog_Naoconformidade_codigo_N = 0;
            gxTv_SdtContagemResultadoChckLstLog_Naoconformidade_codigo = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoChckLstLog_Naoconformidade_codigo_SetNull( )
      {
         gxTv_SdtContagemResultadoChckLstLog_Naoconformidade_codigo_N = 1;
         gxTv_SdtContagemResultadoChckLstLog_Naoconformidade_codigo = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoChckLstLog_Naoconformidade_codigo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoChckLstLog_UsuarioCod" )]
      [  XmlElement( ElementName = "ContagemResultadoChckLstLog_UsuarioCod"   )]
      public int gxTpr_Contagemresultadochcklstlog_usuariocod
      {
         get {
            return gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuariocod ;
         }

         set {
            gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuariocod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultadoChckLstLog_PessoaCod" )]
      [  XmlElement( ElementName = "ContagemResultadoChckLstLog_PessoaCod"   )]
      public int gxTpr_Contagemresultadochcklstlog_pessoacod
      {
         get {
            return gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_pessoacod ;
         }

         set {
            gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_pessoacod_N = 0;
            gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_pessoacod = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_pessoacod_SetNull( )
      {
         gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_pessoacod_N = 1;
         gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_pessoacod = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_pessoacod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoChckLstLog_UsuarioNome" )]
      [  XmlElement( ElementName = "ContagemResultadoChckLstLog_UsuarioNome"   )]
      public String gxTpr_Contagemresultadochcklstlog_usuarionome
      {
         get {
            return gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuarionome ;
         }

         set {
            gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuarionome_N = 0;
            gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuarionome = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuarionome_SetNull( )
      {
         gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuarionome_N = 1;
         gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuarionome = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuarionome_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoChckLstLog_Etapa" )]
      [  XmlElement( ElementName = "ContagemResultadoChckLstLog_Etapa"   )]
      public short gxTpr_Contagemresultadochcklstlog_etapa
      {
         get {
            return gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_etapa ;
         }

         set {
            gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_etapa_N = 0;
            gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_etapa = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_etapa_SetNull( )
      {
         gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_etapa_N = 1;
         gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_etapa = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_etapa_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtContagemResultadoChckLstLog_Mode ;
         }

         set {
            gxTv_SdtContagemResultadoChckLstLog_Mode = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoChckLstLog_Mode_SetNull( )
      {
         gxTv_SdtContagemResultadoChckLstLog_Mode = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoChckLstLog_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtContagemResultadoChckLstLog_Initialized ;
         }

         set {
            gxTv_SdtContagemResultadoChckLstLog_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoChckLstLog_Initialized_SetNull( )
      {
         gxTv_SdtContagemResultadoChckLstLog_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoChckLstLog_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoChckLstLog_Codigo_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoChckLstLog_Codigo_Z"   )]
      public int gxTpr_Contagemresultadochcklstlog_codigo_Z
      {
         get {
            return gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_codigo_Z ;
         }

         set {
            gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_codigo_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoChckLstLog_OSCodigo_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoChckLstLog_OSCodigo_Z"   )]
      public int gxTpr_Contagemresultadochcklstlog_oscodigo_Z
      {
         get {
            return gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_oscodigo_Z ;
         }

         set {
            gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_oscodigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_oscodigo_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_oscodigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_oscodigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoChckLstLog_DataHora_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoChckLstLog_DataHora_Z"  , IsNullable=true )]
      public string gxTpr_Contagemresultadochcklstlog_datahora_Z_Nullable
      {
         get {
            if ( gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_datahora_Z == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_datahora_Z).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_datahora_Z = DateTime.MinValue;
            else
               gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_datahora_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultadochcklstlog_datahora_Z
      {
         get {
            return gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_datahora_Z ;
         }

         set {
            gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_datahora_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_datahora_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_datahora_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_datahora_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoChckLstLog_ChckLstCod_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoChckLstLog_ChckLstCod_Z"   )]
      public int gxTpr_Contagemresultadochcklstlog_chcklstcod_Z
      {
         get {
            return gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstcod_Z ;
         }

         set {
            gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstcod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstcod_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstcod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstcod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "NaoConformidade_Codigo_Z" )]
      [  XmlElement( ElementName = "NaoConformidade_Codigo_Z"   )]
      public int gxTpr_Naoconformidade_codigo_Z
      {
         get {
            return gxTv_SdtContagemResultadoChckLstLog_Naoconformidade_codigo_Z ;
         }

         set {
            gxTv_SdtContagemResultadoChckLstLog_Naoconformidade_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoChckLstLog_Naoconformidade_codigo_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoChckLstLog_Naoconformidade_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoChckLstLog_Naoconformidade_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoChckLstLog_UsuarioCod_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoChckLstLog_UsuarioCod_Z"   )]
      public int gxTpr_Contagemresultadochcklstlog_usuariocod_Z
      {
         get {
            return gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuariocod_Z ;
         }

         set {
            gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuariocod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuariocod_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuariocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuariocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoChckLstLog_PessoaCod_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoChckLstLog_PessoaCod_Z"   )]
      public int gxTpr_Contagemresultadochcklstlog_pessoacod_Z
      {
         get {
            return gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_pessoacod_Z ;
         }

         set {
            gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_pessoacod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_pessoacod_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_pessoacod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_pessoacod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoChckLstLog_UsuarioNome_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoChckLstLog_UsuarioNome_Z"   )]
      public String gxTpr_Contagemresultadochcklstlog_usuarionome_Z
      {
         get {
            return gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuarionome_Z ;
         }

         set {
            gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuarionome_Z = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuarionome_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuarionome_Z = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuarionome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoChckLstLog_Etapa_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoChckLstLog_Etapa_Z"   )]
      public short gxTpr_Contagemresultadochcklstlog_etapa_Z
      {
         get {
            return gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_etapa_Z ;
         }

         set {
            gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_etapa_Z = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_etapa_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_etapa_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_etapa_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoChckLstLog_ChckLstCod_N" )]
      [  XmlElement( ElementName = "ContagemResultadoChckLstLog_ChckLstCod_N"   )]
      public short gxTpr_Contagemresultadochcklstlog_chcklstcod_N
      {
         get {
            return gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstcod_N ;
         }

         set {
            gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstcod_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstcod_N_SetNull( )
      {
         gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstcod_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstcod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoChckLstLog_ChckLstDes_N" )]
      [  XmlElement( ElementName = "ContagemResultadoChckLstLog_ChckLstDes_N"   )]
      public short gxTpr_Contagemresultadochcklstlog_chcklstdes_N
      {
         get {
            return gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstdes_N ;
         }

         set {
            gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstdes_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstdes_N_SetNull( )
      {
         gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstdes_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstdes_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "NaoConformidade_Codigo_N" )]
      [  XmlElement( ElementName = "NaoConformidade_Codigo_N"   )]
      public short gxTpr_Naoconformidade_codigo_N
      {
         get {
            return gxTv_SdtContagemResultadoChckLstLog_Naoconformidade_codigo_N ;
         }

         set {
            gxTv_SdtContagemResultadoChckLstLog_Naoconformidade_codigo_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoChckLstLog_Naoconformidade_codigo_N_SetNull( )
      {
         gxTv_SdtContagemResultadoChckLstLog_Naoconformidade_codigo_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoChckLstLog_Naoconformidade_codigo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoChckLstLog_PessoaCod_N" )]
      [  XmlElement( ElementName = "ContagemResultadoChckLstLog_PessoaCod_N"   )]
      public short gxTpr_Contagemresultadochcklstlog_pessoacod_N
      {
         get {
            return gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_pessoacod_N ;
         }

         set {
            gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_pessoacod_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_pessoacod_N_SetNull( )
      {
         gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_pessoacod_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_pessoacod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoChckLstLog_UsuarioNome_N" )]
      [  XmlElement( ElementName = "ContagemResultadoChckLstLog_UsuarioNome_N"   )]
      public short gxTpr_Contagemresultadochcklstlog_usuarionome_N
      {
         get {
            return gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuarionome_N ;
         }

         set {
            gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuarionome_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuarionome_N_SetNull( )
      {
         gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuarionome_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuarionome_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoChckLstLog_Etapa_N" )]
      [  XmlElement( ElementName = "ContagemResultadoChckLstLog_Etapa_N"   )]
      public short gxTpr_Contagemresultadochcklstlog_etapa_N
      {
         get {
            return gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_etapa_N ;
         }

         set {
            gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_etapa_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_etapa_N_SetNull( )
      {
         gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_etapa_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_etapa_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_datahora = (DateTime)(DateTime.MinValue);
         gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstdes = "";
         gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuarionome = "";
         gxTv_SdtContagemResultadoChckLstLog_Mode = "";
         gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_datahora_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuarionome_Z = "";
         sTagName = "";
         sDateCnv = "";
         sNumToPad = "";
         datetime_STZ = (DateTime)(DateTime.MinValue);
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "contagemresultadochcklstlog", "GeneXus.Programs.contagemresultadochcklstlog_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_etapa ;
      private short gxTv_SdtContagemResultadoChckLstLog_Initialized ;
      private short gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_etapa_Z ;
      private short gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstcod_N ;
      private short gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstdes_N ;
      private short gxTv_SdtContagemResultadoChckLstLog_Naoconformidade_codigo_N ;
      private short gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_pessoacod_N ;
      private short gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuarionome_N ;
      private short gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_etapa_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_codigo ;
      private int gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_oscodigo ;
      private int gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstcod ;
      private int gxTv_SdtContagemResultadoChckLstLog_Naoconformidade_codigo ;
      private int gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuariocod ;
      private int gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_pessoacod ;
      private int gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_codigo_Z ;
      private int gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_oscodigo_Z ;
      private int gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstcod_Z ;
      private int gxTv_SdtContagemResultadoChckLstLog_Naoconformidade_codigo_Z ;
      private int gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuariocod_Z ;
      private int gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_pessoacod_Z ;
      private String gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuarionome ;
      private String gxTv_SdtContagemResultadoChckLstLog_Mode ;
      private String gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_usuarionome_Z ;
      private String sTagName ;
      private String sDateCnv ;
      private String sNumToPad ;
      private DateTime gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_datahora ;
      private DateTime gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_datahora_Z ;
      private DateTime datetime_STZ ;
      private String gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstdes ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"ContagemResultadoChckLstLog", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtContagemResultadoChckLstLog_RESTInterface : GxGenericCollectionItem<SdtContagemResultadoChckLstLog>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContagemResultadoChckLstLog_RESTInterface( ) : base()
      {
      }

      public SdtContagemResultadoChckLstLog_RESTInterface( SdtContagemResultadoChckLstLog psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ContagemResultadoChckLstLog_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultadochcklstlog_codigo
      {
         get {
            return sdt.gxTpr_Contagemresultadochcklstlog_codigo ;
         }

         set {
            sdt.gxTpr_Contagemresultadochcklstlog_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultadoChckLstLog_OSCodigo" , Order = 1 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultadochcklstlog_oscodigo
      {
         get {
            return sdt.gxTpr_Contagemresultadochcklstlog_oscodigo ;
         }

         set {
            sdt.gxTpr_Contagemresultadochcklstlog_oscodigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultadoChckLstLog_DataHora" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultadochcklstlog_datahora
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Contagemresultadochcklstlog_datahora) ;
         }

         set {
            sdt.gxTpr_Contagemresultadochcklstlog_datahora = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "ContagemResultadoChckLstLog_ChckLstCod" , Order = 3 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultadochcklstlog_chcklstcod
      {
         get {
            return sdt.gxTpr_Contagemresultadochcklstlog_chcklstcod ;
         }

         set {
            sdt.gxTpr_Contagemresultadochcklstlog_chcklstcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultadoChckLstLog_ChckLstDes" , Order = 4 )]
      public String gxTpr_Contagemresultadochcklstlog_chcklstdes
      {
         get {
            return sdt.gxTpr_Contagemresultadochcklstlog_chcklstdes ;
         }

         set {
            sdt.gxTpr_Contagemresultadochcklstlog_chcklstdes = (String)(value);
         }

      }

      [DataMember( Name = "NaoConformidade_Codigo" , Order = 5 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Naoconformidade_codigo
      {
         get {
            return sdt.gxTpr_Naoconformidade_codigo ;
         }

         set {
            sdt.gxTpr_Naoconformidade_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultadoChckLstLog_UsuarioCod" , Order = 6 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultadochcklstlog_usuariocod
      {
         get {
            return sdt.gxTpr_Contagemresultadochcklstlog_usuariocod ;
         }

         set {
            sdt.gxTpr_Contagemresultadochcklstlog_usuariocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultadoChckLstLog_PessoaCod" , Order = 7 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultadochcklstlog_pessoacod
      {
         get {
            return sdt.gxTpr_Contagemresultadochcklstlog_pessoacod ;
         }

         set {
            sdt.gxTpr_Contagemresultadochcklstlog_pessoacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultadoChckLstLog_UsuarioNome" , Order = 8 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultadochcklstlog_usuarionome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultadochcklstlog_usuarionome) ;
         }

         set {
            sdt.gxTpr_Contagemresultadochcklstlog_usuarionome = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultadoChckLstLog_Etapa" , Order = 9 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contagemresultadochcklstlog_etapa
      {
         get {
            return sdt.gxTpr_Contagemresultadochcklstlog_etapa ;
         }

         set {
            sdt.gxTpr_Contagemresultadochcklstlog_etapa = (short)(value.HasValue ? value.Value : 0);
         }

      }

      public SdtContagemResultadoChckLstLog sdt
      {
         get {
            return (SdtContagemResultadoChckLstLog)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtContagemResultadoChckLstLog() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 27 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
